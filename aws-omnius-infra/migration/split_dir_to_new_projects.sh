#!/usr/bin/env bash

echo "Please select the subdirectory you want to split"
read -p "Ansible? (Y/N): " ansible
if [[ ("$ansible" == "Y") || ("$ansible" == "y") ]]; then
    echo "Selected ansible to split"
else
    read -p "Terraform? (Y/N): " terraform
    if [[ ("$terraform" == "Y") || ("$terraform" == "y") ]]; then
        echo "Selected terraform=$terraform"
    else
        echo " Please select Ansible or Terraform"
        exit 1
    fi
fi

repo_name_to_split=""
new_repo_name_for_subdir=""

while [[ -z $repo_name_to_split ]]; do
    read -p "Please enter the existing repo name to split: " repo_name_to_split && [[ ! -z $repo_name_to_split ]] || echo "Cannot be empty"
done

while [[ -z $new_repo_name_for_subdir ]]; do
    read -p "Please enter the new repo name for the subdirectory:" new_repo_name_for_subdir && [[ ! -z $new_repo_name_for_subdir ]] || echo "Cannot be empty"
done

if [[ ! -d $new_repo_name_for_subdir ]]; then
    git clone git@bitbucket.org:dynalean/${repo_name_to_split}.git $new_repo_name_for_subdir
    pushd $new_repo_name_for_subdir

else
    echo "$new_repo_name_for_subdir already exists"
    exit 1
fi

git remote rm origin

if [[ ("$ansible" == "Y") || ("$ansible" == "y") ]]; then
    git filter-branch --tag-name-filter cat --prune-empty -f --subdirectory-filter ansible -- --all
    git filter-branch --force --index-filter "git rm -r --cached --ignore-unmatch nginx-common nginx-proxy" --prune-empty --tag-name-filter cat -- --all
    git filter-branch --force --index-filter 'git ls-files | grep -E "^([^/]+/){2,3}vfde|^([^/]+/){2,3}vznl|^([^/]+/){2,3}delta|^([^/]+/){2,3}caiway|vfde" | xargs git rm -rf --cached --ignore-unmatch' --prune-empty --tag-name-filter cat -- --all
    for i in ../migration/.editorconfig ../migration/.gitignore ../migration/Jenkinsfile; do
        cp -p $i .
    done
    git add .
    git commit -m "Complete the Migration"
fi

if [[ ("$terraform" == "Y") || ("$terraform" == "y") ]]; then
    git filter-branch --force --index-filter "git rm -r --cached --ignore-unmatch ansible aws-k8s-shell-kops aws-ssh-config Jenkinsfile" --prune-empty --tag-name-filter cat -- --all
    git filter-branch --force --index-filter 'git ls-files | grep -E "^([^/]+/){2,3}vfde|^([^/]+/){2,3}vznl|^([^/]+/){2,3}delta|^([^/]+/){2,3}caiway" | xargs git rm -rf --cached --ignore-unmatch' --prune-empty --tag-name-filter cat -- --all

fi

git reset --hard
git for-each-ref --format="%(refname)" refs/original/ | xargs -n 1 git update-ref -d
git reflog expire --expire=now --all
git gc --aggressive --prune=now

git remote add origin git@bitbucket.org:dynalean/${new_repo_name_for_subdir}.git
#git push --all origin
#
