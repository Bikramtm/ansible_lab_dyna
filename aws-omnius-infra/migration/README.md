# Migrate the Repo steps

1. Copy the migration folder above the repo aws-omnius-infra location
    `git clone git@bitbucket.org:dynalean/aws-omnius-infra.git aws-omnius-infra`
    `git checkout <branch>` (if required)
    `cp -r aws-omnius-infra/migration .`

2. Start the migration for Ansible

    `/bin/bash migration/split_dir_to_new_projects.sh`

    1. Please select the subdirectory you want to split
       Ansible? (Y/N): Y

    2. Please enter the existing repo name to split:
       aws-omnius-infra

    3. Please enter the new repo name for the subdirectory:
       platform-ansible

