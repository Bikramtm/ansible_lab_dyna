# Ansible usage documentation

Ansible playbooks can be executed from the ansible server on AWS. For easy usage it is recommended to load the aws pem key on your ssh agent.

```bash
ssh-add ~/my_terraform_key.pem
```

As the ansible server is not directly exposed to the public internet we have to hop via the publicly available nginx server.

Following .ssh/config can be used to setup your local environment for easy access to the ansible server.

```
Host aws-nginx-proxy
   HostName ec2-18-185-233-20.eu-central-1.compute.amazonaws.com
   Port 22
   User centos
   IdentityFile ~/dclab-frankfurt.pem

Host aws-ansible
   HostName ip-20-0-1-59.eu-central-1.compute.internal
   ProxyCommand ssh aws-nginx-proxy -W %h:%p
   Port 22
   User centos
```

Run the ansible role common-bastion to configure the servers from ansible server.

1. login to ansible server, download ansible and git
2. download the codes from bitbucket
3. Run the roles in below order
   1. nginx-proxy
   2. common-bastion
   3. cent-jenkins
   4. nexus

# Executing
All playbooks can be executed from an `ansible` server.

```bash
ansible-playbook -i inventories/staging/vznl-st2/hosts --private-key ~/ansible.pem --vault-password-file .vault_password --tags "untagged,vznl-st2" update_ose.yml
```

Please note that when providing tags, ansible will on default only execute the tags that are provided. Since not all plays have tags assigned the tag `untagged` can be used to also include the ones that have no tag.

# Development / Testing

## Using Vagrant

To run a test of a playbook locally, update the `inventory` and `playbook` from the `Vagrantfile`.
Also remind that there should be a `.vault_password` file available next to the Vagrantfile containing the actual `ansible-vault` password

Below a few command on how to run and use the box:

```bash
// Spin up a new box
vagrant up

// Destroy the existing box
vagrant destroy

// SSH into the virtualbox
vagrant ssh

// Provision
vagrant provision
```

## Using Docker

The Jenkins ansible development environment uses the docker-compose setup. How to run this?

```bash
$ docker-compose up -d
$ docker-compose exec ansible bash
[root@0a38ee6ac3e1 ansible]$ yum install sudo
echo ${VAULT_PASS:-password} > /tmp/.vault_password && ansible-playbook -i inventories/development/jenkins --skip-tags lvm playbooks/install/jenkins-master.yml
```

Please note: the docker container by default has no sudo installed which is required in some parts of the ansible script.

## Using Molecules

https://molecule.readthedocs.io/en/stable/installation.html

Added molecules to test the roles during development purpose.

1. docker -v
2. python -V
3. ansible --version
4. molecule --version

Make sure all above tools installed in MAC/LinuxVM, Ansible master is not possible to install in Windows as far we know.

# Test Molecules

Each individual ansible roles need to be developed to test the roles use below commands.
Use appropiate docker image to run the ansible roles.

```bash
$ molecule converge
```

To run with Ansible tags

```bash
$ molecule converge -- -vvv --skip-tags install,lvm,jobs
```

# Jenkins Trobleshooting

1. If we have issues with jenkins (like throttling) we get grafana alert in the channel.
2. Immedialty login the jenkins and check the jobs which throttle jenkins.
3. Go to Jenkins->Manage Jenkins->script console --> Run the below script to cancel all the jobs

```
import hudson.model.*
def queue = Hudson.instance.queue
println "Queue contains ${queue.items.length} items"
queue.clear()
println "Queue cleared"
```
4. This script will kill all jobs in queue. So we can keep the jenkins in check.
5. If you see the Automation Jenkins slave is struck for a long time, please delete the slaves.

# For long run

1. Remove the branches in vdfe & vznl , which are not using actively used. Since there is no CI happening on the Branch/Commit this download the branch weekly once and try to build the job. So this comes to download like tsunami. This will throttle the jenkins for sure.

Upgrade the Jenkins for Plugins issues. Lot of plugins are outdated, this give more issues in Pipeline jobs.

Upgrade Jenkins Master to C5 or M5 for better performance if you dont get support.
