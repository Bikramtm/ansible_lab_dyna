hosts:
  mysql: ip-20-0-1-68.eu-central-1.compute.internal
  redis: ip-20-0-1-68.eu-central-1.compute.internal
  rabbitmq: ip-20-0-1-68.eu-central-1.compute.internal
  ose:  ip-20-0-1-68.eu-central-1.compute.internal
  oil: ip-20-0-1-68.eu-central-1.compute.internal

redis:
  password: "{{ vault.redis.password }}"
  enablerepo: epel
  epel_repo_url: "https://dl.fedoraproject.org/pub/epel/epel-release-latest-{{ ansible_distribution_major_version }}.noarch.rpm"
  epel_repo_gpg_key_url: "/etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-{{ ansible_distribution_major_version }}"
  epel_repofile_path: "/etc/yum.repos.d/epel.repo"
  package: redis
  ipv4_addr: '\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}'
  bind_addr: 0.0.0.0
  daemon: redis
  conf_path: /etc/redis.conf
  databases: 32

mysql:
  root_password: "{{ vault.mysql.root_password }}"
  dyna_admin_password: "{{ vault.mysql.dyna_admin_password }}"
  admin_user: dynaadmin
  repo: "https://dev.mysql.com/get/mysql57-community-release-el7-8.noarch.rpm"
  packages:
    - mysql
    - mysql-server
    - MySQL-python
  bind_address: '0.0.0.0'
  port: "3306"
  log_error: /var/log/mysql_error.log
  expire_logs_days: "10"
  max_allowed_packet: "16M"
  table_open_cache: "12000"
  max_heap_table_size: "64M"
  tmp_table_size: "64M"
  read_priv_buffer_size: "8M"
  join_buffer_size: "2M"
  query_cache_size: "128M"
  query_cache_limit: "64M"
  query_cache_type: "1"
  performance_schema: "OFF"
  mysqldump_max_allowed_packet: "16M"
  max_binlog_size: "100M"
  isamchk_key_buffer: "16M"
  innodb_buffer_pool_size: "1024M"
  innodb_buffer_pool_instances: "4"
  max_connections: "151"


rabbitmq:
  admin_username: admin
  admin_pass: "{{ vault.rabbitmq.admin }}"
  host: localhost
  vhosts:
    - /
    - claro

  users:
    - username: admin
      password: "{{ vault.rabbitmq.admin }}"
      tags: "administrator"
      permissions:
        - vhost: /
          configure_priv: .*
          read_priv: .*
          write_priv: .*
        - vhost: claro
          configure_priv: .*
          read_priv: .*
          write_priv: .*

    - username: statusdaemon
      password: "{{ vault.rabbitmq.statusdaemon }}"
      tags: "monitoring"
      permissions:
        - vhost: claro
          configure_priv: ''
          read_priv: .*
          write_priv: .*

    - username: oil
      password: "{{ vault.rabbitmq.oil }}"
      tags: "monitoring"
      permissions:
        - vhost: claro
          configure_priv: ''
          read_priv: .*
          write_priv: .*

    - username: customer
      password: "{{ vault.rabbitmq.customer }}"
      tags: "monitoring"
      permissions:
        - vhost: claro
          configure_priv: .*
          read_priv: .*
          write_priv: .*

nginx:
  version: "nginx-1.12.2"
  vhosts:
  - server_name: serviceability
    port: 9002
    web_root: /opt/omnius/serviceability/public
    log_dir: /var/log/nginx/omnius
    log_name: serviceability
    includes:
      - name: serviceability
        template: php
        fpm_pool: omnius-serviceability-php-fpm
  - server_name: validateaddress
    port: 9003
    web_root: /opt/omnius/validateaddress/public
    log_dir: /var/log/nginx/omnius
    log_name: validateaddress
    includes:
      - name: validateaddress
        template: php
        fpm_pool: omnius-validateaddress-php-fpm
  - server_name: shoppingcart
    port: 9007
    web_root: /opt/omnius/shoppingcart/public
    log_dir: /var/log/nginx/omnius
    log_name: shoppingcart
    includes:
      - name: shoppingcart
        template: php
        fpm_pool: omnius-shoppingcart-php-fpm
  - server_name: order
    port: 9008
    web_root: /opt/omnius/ordermanagement/public
    log_dir: /var/log/nginx/omnius
    log_name: order
    includes:
      - name: order
        template: php
        fpm_pool: omnius-ordermanagement-php-fpm
  - server_name: telesales-demo-claro.dynacommercelab.com
    port: 80
    web_root: /opt/omnius/ose
    log_dir: /var/log/nginx/omnius
    log_name: ose-telesales
    includes:
      - name: ose-telesales
        template: mage-php
        fpm_pool: omnius-ose-php-fpm
        mage_run_code: telesales
        mage_env_code: BAU
        mage_run_type: store
        mage_dev_mode: false
        https: "on"
      - name: serviceability-proxy-telesales
        template: api-proxy
        path: /api/v1/serviceability
        pass: http://localhost:9002
        extras:
          - "add_header 'Access-Control-Allow-Origin' 'telesales-demo-claro.dynacommercelab.com';"
      - name: validateaddress-proxy-telesales
        template: api-proxy
        path: /api/v1/address
        pass: http://localhost:9003
        extras:
          - "add_header 'Access-Control-Allow-Origin' 'telesales-demo-claro.dynacommercelab.com';"
      - name: customersearch-proxy-telesales
        template: api-proxy
        rewrite: ^/api/v1/customer/(.*)$ /customer/$1 break;
        path: /api/v1/customer
        pass: http://localhost:9005
        extras:
          - "add_header 'Access-Control-Allow-Origin' 'telesales-demo-claro.dynacommercelab.com';"
      - name: coc-proxy-telesales
        template: api-proxy
        rewrite: /api/v1/coc(.*) $1  break;
        path: /api/v1/coc
        pass: http://localhost:9006
        extras:
          - "add_header 'Access-Control-Allow-Origin' 'telesales-demo-claro.dynacommercelab.com';"
      - name: shoppingcart-proxy-telesales
        template: api-proxy
        path: /api/v1/shoppingcart
        pass: http://localhost:9007
        extras:
          - "add_header 'Access-Control-Allow-Origin' 'telesales-demo-claro.dynacommercelab.com';"
      - name: order-proxy-telesales
        template: api-proxy
        path: /api/v1/order
        pass: http://localhost:9008
        extras:
          - "add_header 'Access-Control-Allow-Origin' 'telesales-demo-claro.dynacommercelab.com';"
      - name: multimapper-proxy-telesales
        template: api-proxy
        rewrite: ^/api/v1/multimapper/(.*)$ /$1 break;
        path: /api/v1/multimapper
        pass: "http://{{ hosts.ose }}:9009"
        extras:
          - "add_header 'Access-Control-Allow-Origin' 'telesales-demo-claro.dynacommercelab.com';"
      - name: installedbase-proxy-telesales
        template: api-proxy
        rewrite: ^/api/v1/installedbase/(.*)$ /$1 break;
        path: /api/v1/installedbase
        pass: "http://{{ hosts.ose }}:9010"
        extras:
          - "add_header 'Access-Control-Allow-Origin' 'telesales-demo-claro.dynacommercelab.com';"
  - server_name: retail-demo-claro.dynacommercelab.com
    port: 80
    web_root: /opt/omnius/ose
    log_dir: /var/log/nginx/omnius
    log_name: ose-retail
    includes:
      - name: ose-retail
        template: mage-php
        fpm_pool: omnius-ose-php-fpm
        mage_run_code: default
        mage_env_code: BAU
        mage_run_type: store
        mage_dev_mode: false
        https: "on"
      - name: serviceability-proxy-retail
        template: api-proxy
        path: /api/v1/serviceability
        pass: http://localhost:9002
        extras:
          - "add_header 'Access-Control-Allow-Origin' 'retail-demo-claro.dynacommercelab.com';"
      - name: validateaddress-proxy-retail
        template: api-proxy
        path: /api/v1/address
        pass: http://localhost:9003
        extras:
          - "add_header 'Access-Control-Allow-Origin' 'retail-demo-claro.dynacommercelab.com';"
      - name: customersearch-proxy-retail
        template: api-proxy
        rewrite: ^/api/v1/customer/(.*)$ /customer/$1 break;
        path: /api/v1/customer
        pass: http://localhost:9005
        extras:
          - "add_header 'Access-Control-Allow-Origin' 'retail-demo-claro.dynacommercelab.com';"
      - name: coc-proxy-retail
        template: api-proxy
        rewrite: /api/v1/coc(.*) $1  break;
        path: /api/v1/coc
        pass: http://localhost:9006
        extras:
          - "add_header 'Access-Control-Allow-Origin' 'retail-demo-claro.dynacommercelab.com';"
      - name: shoppingcart-proxy-retail
        template: api-proxy
        path: /api/v1/shoppingcart
        pass: http://localhost:9007
        extras:
          - "add_header 'Access-Control-Allow-Origin' 'retail-demo-claro.dynacommercelab.com';"
      - name: order-proxy-retail
        template: api-proxy
        path: /api/v1/order
        pass: http://localhost:9008
        extras:
          - "add_header 'Access-Control-Allow-Origin' 'retail-demo-claro.dynacommercelab.com';"
      - name: multimapper-proxy-retail
        template: api-proxy
        rewrite: ^/api/v1/multimapper/(.*)$ /$1 break;
        path: /api/v1/multimapper
        pass: "http://{{ hosts.ose }}:9009"
        extras:
          - "add_header 'Access-Control-Allow-Origin' 'retail-demo-claro.dynacommercelab.com';"
      - name: installedbase-proxy-retail
        template: api-proxy
        rewrite: ^/api/v1/installedbase/(.*)$ /$1 break;
        path: /api/v1/installedbase
        pass: "http://{{ hosts.ose }}:9010"
        extras:
          - "add_header 'Access-Control-Allow-Origin' 'retail-demo-claro.dynacommercelab.com';"
  - server_name: indirect-demo-claro.dynacommercelab.com
    port: 80
    web_root: /opt/omnius/ose
    log_dir: /var/log/nginx/omnius
    log_name: ose-indirect
    includes:
      - name: ose-indirect
        template: mage-php
        fpm_pool: omnius-ose-php-fpm
        mage_run_code: indirect
        mage_env_code: BAU
        mage_run_type: store
        mage_dev_mode: false
        https: "on"
      - name: serviceability-proxy-indirect
        template: api-proxy
        path: /api/v1/serviceability
        pass: http://localhost:9002
        extras:
          - "add_header 'Access-Control-Allow-Origin' 'indirect-demo-claro.dynacommercelab.com';"
      - name: validateaddress-proxy-indirect
        template: api-proxy
        path: /api/v1/address
        pass: http://localhost:9003
        extras:
          - "add_header 'Access-Control-Allow-Origin' 'indirect-demo-claro.dynacommercelab.com';"
      - name: customersearch-proxy-indirect
        template: api-proxy
        rewrite: ^/api/v1/customer/(.*)$ /customer/$1 break;
        path: /api/v1/customer
        pass: http://localhost:9005
        extras:
          - "add_header 'Access-Control-Allow-Origin' 'indirect-demo-claro.dynacommercelab.com';"
      - name: coc-proxy-indirect
        template: api-proxy
        rewrite: /api/v1/coc(.*) $1  break;
        path: /api/v1/coc
        pass: http://localhost:9006
        extras:
          - "add_header 'Access-Control-Allow-Origin' 'indirect-demo-claro.dynacommercelab.com';"
      - name: shoppingcart-proxy-indirect
        template: api-proxy
        path: /api/v1/shoppingcart
        pass: http://localhost:9007
        extras:
          - "add_header 'Access-Control-Allow-Origin' 'indirect-demo-claro.dynacommercelab.com';"
      - name: order-proxy-indirect
        template: api-proxy
        path: /api/v1/order
        pass: http://localhost:9008
        extras:
          - "add_header 'Access-Control-Allow-Origin' 'indirect-demo-claro.dynacommercelab.com';"
      - name: multimapper-proxy-indirect
        template: api-proxy
        rewrite: ^/api/v1/multimapper/(.*)$ /$1 break;
        path: /api/v1/multimapper
        pass: "http://{{ hosts.ose }}:9009"
        extras:
          - "add_header 'Access-Control-Allow-Origin' 'indirect-demo-claro.dynacommercelab.com';"
      - name: installedbase-proxy-indirect
        template: api-proxy
        rewrite: ^/api/v1/installedbase/(.*)$ /$1 break;
        path: /api/v1/installedbase
        pass: "http://{{ hosts.ose }}:9010"
        extras:
          - "add_header 'Access-Control-Allow-Origin' 'indirect-demo-claro.dynacommercelab.com';"

  - server_name: restapi-demo-claro.dynacommercelab.com
    port: 80
    web_root: /opt/omnius/ose
    log_dir: /var/log/nginx/omnius
    log_name: ose-restapi
    includes:
      - name: ose-restapi
        template: mage-php
        fpm_pool: omnius-ose-php-fpm
        mage_run_code: vodafone_webshop
        mage_env_code: BAU
        mage_run_type: store
        mage_dev_mode: false
        https: "on"
      - name: restapi-proxy-restapi
        template: vznl-restapi
      - name: serviceability-proxy-restapi
        template: api-proxy
        path: /api/v1/serviceability
        pass: http://localhost:9002
        extras:
          - "add_header 'Access-Control-Allow-Origin' 'restapi-demo-claro.dynacommercelab.com';"
      - name: validateaddress-proxy-restapi
        template: api-proxy
        path: /api/v1/address
        pass: http://localhost:9003
        extras:
          - "add_header 'Access-Control-Allow-Origin' 'restapi-demo-claro.dynacommercelab.com';"
      - name: customersearch-proxy-restapi
        template: api-proxy
        rewrite: ^/api/v1/customer/(.*)$ /customer/$1 break;
        path: /api/v1/customer
        pass: http://localhost:9005
        extras:
          - "add_header 'Access-Control-Allow-Origin' 'restapi-demo-claro.dynacommercelab.com';"
      - name: coc-proxy-restapi
        template: api-proxy
        rewrite: /api/v1/coc(.*) $1  break;
        path: /api/v1/coc
        pass: http://localhost:9006
        extras:
          - "add_header 'Access-Control-Allow-Origin' 'restapi-demo-claro.dynacommercelab.com';"
      - name: shoppingcart-proxy-restapi
        template: api-proxy
        path: /api/v1/shoppingcart
        pass: http://localhost:9007
        extras:
          - "add_header 'Access-Control-Allow-Origin' 'restapi-demo-claro.dynacommercelab.com';"
      - name: order-proxy-restapi
        template: api-proxy
        path: /api/v1/order
        pass: http://localhost:9008
        extras:
          - "add_header 'Access-Control-Allow-Origin' 'restapi-demo-claro.dynacommercelab.com';"
      - name: multimapper-proxy-restapi
        template: api-proxy
        rewrite: ^/api/v1/multimapper/(.*)$ /$1 break;
        path: /api/v1/multimapper
        pass: "http://{{ hosts.ose }}:9009"
        extras:
          - "add_header 'Access-Control-Allow-Origin' 'restapi-demo-claro.dynacommercelab.com';"
      - name: installedbase-proxy-restapi
        template: api-proxy
        rewrite: ^/api/v1/installedbase/(.*)$ /$1 break;
        path: /api/v1/installedbase
        pass: "http://{{ hosts.ose }}:9010"
        extras:
          - "add_header 'Access-Control-Allow-Origin' 'restapi-demo-claro.dynacommercelab.com';"

php:
  version: 71
  repo_path: "http://rpms.remirepo.net/enterprise/remi-release-7.rpm"
  settings:
    - section: Date
      option: date.timezone
      value: Europe/Amsterdam
    - section: PHP
      option: max_execution_time
      value: 18000
    - section: PHP
      option: memory_limit
      value: 512M
    - section: PHP
      option: max_input_vars
      value: 2000
    - section: PHP
      option: allow_url_include
      value: "On"
    - section: PHP
      option: error_reporting
      value: E_ALL

ose:
  package: "https://nexus.dynacommercelab.com/repository/rpm-packages/vznl/omnius-ose-5.0-SNAPSHOT.noarch.rpm"
  dbseed: "https://nexus.dynacommercelab.com/repository/db-seeds/vznl/omnius-ose-4.1-SNAPSHOT.sql.gz"
  mysql:
    host: "{{ hosts.mysql }}"
    port: 3306
    db: "claro"
    username: "claro"
    password: "{{ vault.ose.mysql.password }}"
    encoding: "utf8"
  redis_session:
    host: "{{ hosts.redis }}"
    password: "{{ vault.ose.redis_session.password }}"
    port: 6379
    db: 0
  backend_options:
    server: "{{ hosts.redis }}"
    password: "{{ vault.ose.backend_options.password }}"
    port: 6379
    database: 1
  logging:
    redis:
      active: 0
      host: "{{ hosts.redis }}"
      password: "{{ vault.ose.logging.redis.password }}"
      database: 10
      port: 6379
      keyPrefix: omnius-ose
      stream: default
      loglevel: DEBUG
    stream:
      active: 1
      loglevel: DEBUG
  configuration:
    - key: vodafone_service/customer_sync_sftp/hostname
      value: 127.0.0.1
    - key: vodafone_service/customer_sync_sftp/username
      value: ose
    - key: vodafone_service/customer_sync_sftp/password
      value: "{{ vault.sftp.ose }}"
    - key: vodafone_service/customer_sync_sftp/path
      value: /var/sftp/customer
    - key: vodafone_service/axi_transactions_sftp/hostname
      value: 127.0.0.1
    - key: vodafone_service/axi_transactions_sftp/username
      value: ose
    - key: vodafone_service/axi_transactions_sftp/password
      value: "{{ vault.sftp.ose }}"
    - key: vodafone_service/axi_transactions_sftp/path
      value: /var/sftp/axi/transactions/Inbox
    - key: vodafone_service/axi_stock_sftp/hostname
      value: 127.0.0.1
    - key: vodafone_service/axi_stock_sftp/username
      value: ose
    - key: vodafone_service/axi_stock_sftp/password
      value: "{{ vault.sftp.ose }}"
    - key: vodafone_service/axi_stock_sftp/path
      value: /var/sftp/axi/stockamounts/
    - key: vodafone_service/inlife/wsdl
      value: http://hnesb2-ms1/bsl/dyna2bsl/dynaservice/9.0?wsdl
    - key: vodafone_service/inlife/usage_url
      value: http://hnesb2-ms1/bsl/dyna2bsl/dynaservice/9.0
    - key: vodafone_service/postcode/adapter_choice
      value: PostcodeApiNu
    - key: vodafone_service/postcode/api_key
      value: ttwGr86IALZNj69L2V6G4XlqQ06hNXI52A3ST7xa
    - key: vodafone_service_coc_adapter_choice
      value: Coc
    - key: vodafone_service/coc/usage_url
      value: /proxy.php?url=/api/v1/coc
    - key: vodafone_service_ilt_use_stubs
      value: 0
    - key: vodafone_service/communication_email/adapter
      value: Dyna_Communication_Adapter_Email_StubAdapterFactory
    - key: vodafone_service/communication_sms/adapter
      value: Dyna_Communication_Adapter_Sms_StubAdapterFactory
    - key: vodafone_service/smartvalidation/use_stubs
      value: 1
    - key: vodafone_service/create_basket/adapter_choice
      value: Vznl_CreateBasket_Adapter_Stub_Factory
    - key: vodafone_service/additemtobasket/adapter_choice
      value: Vznl_AddItemToBasket_Adapter_Stub_Factory
    - key: vodafone_service/reset_basket/adapter_choice
      value: Vznl_ResetBasket_Adapter_Stub_Factory
    - key: vodafone_service/validate_basket/adapter_choice
      value: Vznl_ValidateBasket_Adapter_Stub_Factory
    - key: vodafone_service/gettelephonenumber/adapter_choice
      value: Vznl_GetTelephoneNumbers_Adapter_Stub_Factory
    - key: vodafone_service/reserveTelephoneNumber/adapter_choice
      value: Vznl_ReserveTelephoneNumber_Adapter_Stub_Factory
    - key: vodafone_service/validate_address_api/api_end_point
      value: /proxy.php?url=/api/v1/address/validate
    - key: vodafone_service/serviceability_api/api_end_point
      value: /proxy.php?url=/api/v1/serviceability/check
    - key: vodafone_service/validate_serial_number/adapter_choice
      value: Vznl_ValidateSerialNumber_Adapter_Stub_Factory
    - key: vodafone_service/contract_sftp/hostname
      value: 127.0.0.1
    - key: vodafone_service/contract_sftp/username
      value: ose
    - key: vodafone_service/contract_sftp/password
      value: "{{ vault.sftp.ose }}"
    - key: vodafone_service/contract_sftp/path
      value: /var/sftp/contracts
    - key: vodafone_service/general/use_stubs
      value: 0
    - key: omnius_service/customer_search_implementation_configuration/rest_api_end_point
      value: /proxy.php?url=/api/v1/customer

  custom:
    jobs:
      max_consumers: 1
      min_consumer_overhead: 0
      host: "{{ hosts.redis }}"
      port: 6379
      database: 26
      password: "{{ vault.ose.backend_options.password }}"
  storeviews:
    stores:
      - name: telesales
        base_url: https://telesales-demo-claro.dynacommercelab.com/
        scope_id: 0
      - name: retail
        base_url: https://retail-demo-claro.dynacommercelab.com/
        scope_id: 1
      - name: telesales
        base_url: https://telesales-demo-claro.dynacommercelab.com/
        scope_id: 4
      - name: indirect
        base_url: https://indirect-demo-claro.dynacommercelab.com/
        scope_id: 5
  customer:
    apiUrl: "/proxy.php?url=/api/v1/customer"
  phpfpm:
    pools:
      - name: omnius-ose
        settings:
          - option: user
            value: omnius-ose
          - option: group
            value: omnius-ose
          - option: listen
            value: /run/php-fpm/omnius-ose-php-fpm.sock
          - option: listen.owner
            value: nginx
          - option: listen.group
            value: nginx
  rabbitmq:
    vhost: claro
    queues:
      - magentostatus
      - DynaGroupIn_AXTransactionPushMessages
      - deletelogmessage
      - ecommercein
      - esblog

statusdaemon:
  package: "https://nexus.dynacommercelab.com/repository/rpm-packages/statusdaemon/omnius-statusdaemon-1.1-SNAPSHOT.noarch.rpm"
  config:
    php_script: /opt/omnius/ose/shell/statusdaemon/esb_parser.php
    rabbitmq:
      username: statusdaemon
      password: "{{ vault.statusdaemon.rabbitmq_password }}"
      hostname: "{{ hosts.rabbitmq }}"
      vhost: claro
      port: 5672
      queue: magentostatus
      receive_timeout: 1000
      message_interval: 10
      wait_internal: 0
      max_messages: 100
    influxdb:
      host: https://telegraf-influxdb.dynacommercelab.com
      port: 443
      username: vznlstatusdaemon
      password: "{{ vault.statusdaemon.influxdb_password }}"
      database: Ubuy
      inflow: statusdaemon_inflow
      delay: statusdaemon_delay
    log_path: /var/log/omnius/statusdaemon/

serviceability:
  package: https://nexus.dynacommercelab.com/repository/rpm-packages/serviceability/omnius-serviceability-2.4-SNAPSHOT.noarch.rpm
  adapter: https://nexus.dynacommercelab.com/repository/rpm-packages/serviceability/omnius-serviceability-adapter-peal-2.4-SNAPSHOT.noarch.rpm
  phpfpm:
    pools:
      - name: omnius-serviceability
        settings:
          - option: user
            value: omnius-serviceability
          - option: group
            value: omnius-serviceability
          - option: listen
            value: /run/php-fpm/omnius-serviceability-php-fpm.sock
          - option: listen.owner
            value: nginx
          - option: listen.group
            value: nginx
  config:
    - section: null
      option: APP_ENV
      value: dev
    - section: null
      option: APP_DEBUG
      value: false
    - section: null
      option: APP_URL
      value: http://localhost
    - section: null
      option: LOG_CHANNEL
      value: single
    - section: null
      option: BROADCAST_DRIVER
      value: file
    - section: null
      option: CACHE_DRIVER
      value: file
    - section: null
      option: SESSION_DRIVER
      value: file
    - section: null
      option: SESSION_LIFETIME
      value: 120
    - section: null
      option: ADAPTER
      value: Dynacommerce\Serviceability\V1\Adapter\PealNL\Adapter
    - section: null
      option: ADAPTER_PEALNL_ENDPOINT
      value: https://dev-vnext.dynacommercelab.com/wiremock/vznl/serviceability/
    - section: null
      option: ADAPTER_PEALNL_CTY
      value: NL
    - section: null
      option: ADAPTER_PEALNL_CHANNEL
      value: UBUY
    - section: null
      option: ADAPTER_PEALNL_USERNAME
      value: myusername
    - section: null
      option: ADAPTER_PEALNL_PASSWORD
      value: mypassword
    - section: null
      option: JWT_ENABLED
      value: 'false'

validateaddress:
  package: https://nexus.dynacommercelab.com/repository/rpm-packages/validateaddress/omnius-validateaddress-2.1-SNAPSHOT.noarch.rpm
  adapter: https://nexus.dynacommercelab.com/repository/rpm-packages/validateaddress/omnius-validateaddress-adapter-peal-2.1-SNAPSHOT.noarch.rpm
  phpfpm:
    pools:
      - name: omnius-validateaddress
        settings:
          - option: user
            value: omnius-validateaddress
          - option: group
            value: omnius-validateaddress
          - option: listen
            value: /run/php-fpm/omnius-validateaddress-php-fpm.sock
          - option: listen.owner
            value: nginx
          - option: listen.group
            value: nginx
  config:
    - section: null
      option: APP_ENV
      value: dev
    - section: null
      option: APP_DEBUG
      value: false
    - section: null
      option: APP_URL
      value: http://localhost
    - section: null
      option: LOG_CHANNEL
      value: single
    - section: null
      option: BROADCAST_DRIVER
      value: file
    - section: null
      option: CACHE_DRIVER
      value: file
    - section: null
      option: SESSION_DRIVER
      value: file
    - section: null
      option: SESSION_LIFETIME
      value: 120
    - section: null
      option: ADAPTER
      value: Dynacommerce\ValidateAddress\V1\Adapter\PealNL\Adapter
    - section: null
      option: ADAPTER_PEALNL_ENDPOINT
      value: https://dev-vnext.dynacommercelab.com/wiremock/vznl/validateaddress/
    - section: null
      option: ADAPTER_PEALNL_CTY
      value: NL
    - section: null
      option: ADAPTER_PEALNL_CHANNEL
      value: UBUY
    - section: null
      option: ADAPTER_PEALNL_USERNAME
      value: myusername
    - section: null
      option: ADAPTER_PEALNL_PASSWORD
      value: mypassword
    - section: null
      option: JWT_ENABLED
      value: 'false'

sftp:
  groupname: sftpusers
  dirs:
    - /var/sftp/axi/expectedgoods
    - /var/sftp/axi/invoices
    - /var/sftp/axi/stockamounts/Inbox
    - /var/sftp/axi/transactions/Inbox
    - /var/sftp/coc
    - /var/sftp/customer
    - /var/sftp/ecommerce
    - /var/sftp/contracts
  users:
    - username: ose
      homedir: /var/sftp
      group: sftpusers
    - username: oil
      homedir: /var/sftp
      group: sftpusers

customersearch:
  elasticsearch:
    cluster: elasticsearch-dev-omnius-vnext
    host: es5-transport.dynacommercelab.com
    api_host: https://es5.dynacommercelab.com
    port: 9300
    auth: false
    username: elastic
    password: "{{ vault.customersearch.elasticsearch.password }}"
    admin_user: admin
    admin_password: "{{ vault.customersearch.elasticsearch.admin_password }}"
    prefix: "{{ env }}-"
  database:
    options: ''
    host: "{{ hosts.mysql }}"
    name: customersearch_st7
    port: 3306
    user: customersearch_st7
    password: "{{ vault.customersearch.database.password }}"
  queue:
    type: rabbitmq
    topic: customers
    fallback: customers
  rabbitmq:
    host: "{{ hosts.rabbitmq }}"
    vhost: claro
    username: customer
    password: "{{ vault.customersearch.rabbitmq.password }}"
  redis:
    host: "{{ hosts.redis }}"
    port: 6379
    expire: 3600
    database: 27
    password: "{{ vault.customersearch.redis.password }}"
  api:
    package: https://nexus.dynacommercelab.com/repository/rpm-packages/customer-search/omnius-customer-search-api-0.5.11-1.noarch.rpm
    port: 9005
    adapterconfig:
      - key: adapter.vznl.bsl.header.username
        value: omniusvnextcustomer
      - key: adapter.vznl.bsl.header.password
        value: omniusvnextcustomer
      - key: adapter.vznl.bsl.header.dealercode
        value: 00811111
      - key: adapter.vznl.bsl.header.endusername
        value: omniusvnextcustomersearchagent
      - key: adapter.vznl.peal.channel.name
        value: UBUY
      - key: adapter.vznl.peal.channel.code
        value: NL
  indexer:
    package: https://nexus.dynacommercelab.com/repository/rpm-packages/customer-search/omnius-customer-search-esIndexer-0.5.11-1.noarch.rpm
  adapters:
    - https://nexus.dynacommercelab.com/repository/rpm-packages/customer-search/omnius-customer-search-adapter-vznl-peal-0.6_SNAPSHOT-1.noarch.rpm
    - https://nexus.dynacommercelab.com/repository/rpm-packages/customer-search/omnius-customer-search-adapter-vznl-bsl-0.6_SNAPSHOT-1.noarch.rpm

coc:
  database:
    options: ''
    host: "{{ hosts.mysql }}"
    name: coc_claro
    port: 3306
    user: coc_claro
    password: "{{ vault.coc.api.password }}"
  api:
    package: "https://nexus.dynacommercelab.com/repository/rpm-packages/coc-service/omnius-coc-api-1.1_SNAPSHOT-1.noarch.rpm"
    port: 9006
  cli:
    package: "https://nexus.dynacommercelab.com/repository/rpm-packages/coc-service/omnius-coc-cli-1.1_SNAPSHOT-1.noarch.rpm"
    enabled: 'true'
    cron: 0 44 15 * * ?
    delimiter:
      row: \n
      column: \\|
    mode: sftp
    host: localhost
    port: 22
    username: ose
    password: "{{ vault.coc.sftp.password }}"
    path: /var/sftp/coc
    passive_mode: 'false'

ose_order:
  package: https://nexus.dynacommercelab.com/repository/rpm-packages/ose-order-api/omnius-ose-order-api-1.3-SNAPSHOT.noarch.rpm
  config:
    - section: null
      option: MAGENTO_STORE_ID
      value: 5
    - section: null
      option: PROJECT_NAME
      value: vznl
    - section: null
      option: JWT_ENABLED
      value: 'false'

ose_shoppingcart:
  package: "https://nexus.dynacommercelab.com/repository/rpm-packages/ose-shoppingcart-api/omnius-ose-shoppingcart-api-1.3-SNAPSHOT.noarch.rpm"
  config:
    - section: null
      option: MAGENTO_STORE_ID
      value: 5
    - section: null
      option: CUSTOMER_ID_ATTRIBUTE_FIELD
      value: ban
    - section: null
      option: JWT_ENABLED
      value: 'false'

multimapper:
  package: https://nexus.dynacommercelab.com/repository/rpm-packages/multimapper/omnius-multimapper-api-0.2_SNAPSHOT-1.noarch.rpm
  port: 9009
  loglevel: WARN
  database:
    options: ''
    host: "{{ hosts.mysql }}"
    name: multimapper_claro
    port: 3306
    user: multimapper_claro
    password: "{{ vault.multimapper.database.password }}"
