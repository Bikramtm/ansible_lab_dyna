hosts:
  mysql: "127.0.0.1"
  redis: "127.0.0.1"
  ose: "127.0.0.1"
  varnish:  "127.0.0.1"

redis:
  enablerepo: epel
  epel_repo_url: "https://dl.fedoraproject.org/pub/epel/epel-release-latest-{{ ansible_distribution_major_version }}.noarch.rpm"
  epel_repo_gpg_key_url: "/etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-{{ ansible_distribution_major_version }}"
  epel_repofile_path: "/etc/yum.repos.d/epel.repo"
  package: redis
  ipv4_addr: '\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}'
  bind_addr: 0.0.0.0
  daemon: redis
  conf_path: /etc/redis.conf
  databases: 32

mysql:
  telegraf: false
  low_password_policy: true
  root_password: "{{ vault.mysql.root_password }}"
  dyna_admin_password: "{{ vault.mysql.dyna_admin_password }}"
  admin_user: dynaadmin
  repo: "https://dev.mysql.com/get/mysql57-community-release-el7-8.noarch.rpm"
  packages:
    - mysql
    - mysql-server
    - MySQL-python
  bind_address: "0.0.0.0"
  port: "3306"
  log_error: /var/log/mysql_error.log
  expire_logs_days: "10"
  max_allowed_packet: "16M"
  table_open_cache: "12000"
  max_heap_table_size: "64M"
  tmp_table_size: "64M"
  read_priv_buffer_size: "8M"
  join_buffer_size: "2M"
  query_cache_size: "128M"
  query_cache_limit: "64M"
  query_cache_type: "1"
  performance_schema: "OFF"
  mysqldump_max_allowed_packet: "16M"
  max_binlog_size: "100M"
  isamchk_key_buffer: "16M"
  innodb_buffer_pool_size: "1024M"
  innodb_buffer_pool_instances: "4"
  max_connections: "151"

nginx:
  version: "nginx-1.12.2"
  media_browse: true
  vhosts:
    - server_name: uct
      port: 9021
      web_root: /vagrant/uct-api/public
      log_dir: /var/log/nginx/omnius
      log_name: omnius-uct
      includes:
        - name: omnius-uct
          template: php
          fpm_pool: www-data-php-fpm
    - server_name: um
      port: 9022
      web_root: /vagrant/um-api/public
      log_dir: /var/log/nginx/omnius
      log_name: omnius-um
      includes:
        - name: omnius-um
          template: php
          fpm_pool: www-data-php-fpm
    - server_name: sales-api
      port: 9023
      web_root: /vagrant/restapi/public
      log_dir: /var/log/nginx/omnius
      log_name: omnius-sales-api
      includes:
        - name: omnius-sales-api
          template: php
          fpm_pool: www-data-php-fpm
    - server_name: telesales.dev.dynacommerce.io
      port: 8080
      web_root: /vagrant
      log_dir: /var/log/nginx/omnius
      log_name: ose-telesales
      includes:
        - name: ose-telesales
          template: mage-php
          fpm_pool: www-data-php-fpm
          mage_run_code: telesales
          mage_env_code: BAU
          mage_run_type: store
          mage_dev_mode: true
          https: "on"
        - name: uct-proxy
          template: api-proxy
          path: /webhooks/v1/uct
          pass: "http://{{ hosts.ose }}:9021"
          extras:
            - "add_header 'Access-Control-Allow-Origin' 'telesales.dev.dynacommerce.io';"
        - name: um-proxy
          template: api-proxy
          path: /usermanagement/api/v1
          pass: "http://{{ hosts.ose }}:9022"
          extras:
            - "add_header 'Access-Control-Allow-Origin' 'telesales.dev.dynacommerce.io';"
        - name: sales-api
          template: api-proxy
          rewrite: ^/restapi/api/v1/(.*)$ /$1 break;
          path: /restapi/api/v1
          pass: "http://{{ hosts.ose }}:9023"
          extras:
            - "add_header 'Access-Control-Allow-Origin' 'telesales.dev.dynacommerce.io';"
    - server_name: retail.dev.dynacommerce.io
      port: 8080
      web_root: /vagrant
      log_dir: /var/log/nginx/omnius
      log_name: ose-retail
      includes:
        - name: ose-retail
          template: mage-php
          fpm_pool: www-data-php-fpm
          mage_run_code: retail
          mage_env_code: BAU
          mage_run_type: store
          mage_dev_mode: true
          https: "on"

php:
  version: 71
  repo_path: "http://rpms.remirepo.net/enterprise/remi-release-7.rpm"
  settings:
    - section: Date
      option: date.timezone
      value: Europe/Amsterdam
    - section: PHP
      option: max_execution_time
      value: 18000
    - section: PHP
      option: memory_limit
      value: 512M
    - section: PHP
      option: max_input_vars
      value: 2000
    - section: PHP
      option: allow_url_include
      value: "On"
    - section: PHP
      option: error_reporting
      value: E_ALL

vagrant:
  mysql:
    host: "{{ hosts.mysql }}"
    port: 3306
    db: "ose"
    username: "ose"
    password: "{{ vault.ose.mysql.password }}"
    encoding: "utf8"
