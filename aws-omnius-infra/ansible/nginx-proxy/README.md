### General steps to execute ansible playbook

1. Login to ansible server. (install ansible and git if required)
2. Pull the latest code in the ansible server, under your home directory
3. Go to the components directory i.e aws-ominus-infra/ansible/<components> example
4. Edit the inventory update the server private ip
5. Review all the Variables, change the remote_user to ansible user if required
6. Check any secrets.yml is available
7. Change the remote_user variable under vars/all.yml to ansible user (if you want to run this playbook as ansible user)
7. Run ```ansible-playbook -i inventory <component>.yml```



To run nginx role some prerequisites are required

a) A record in route53( nginx.dynacommercelab.com) should be available to create letsencrypt ssl
b) http should be open to letsencrypt url to get back the ssl, else it fails to write back the ssl
c) SSL  will be staging with ansible scripts, to move production login servers and run certbot
d) http(port 80) should be open to world, https we can fliter
e) Nginx will be attached to WFH security group, add port 80 can be accesed to everyone.

To run the playbook, fill all the variables in vars/all.yml and /var/nginx.yml

# Steps:

1. Login AWS console edit Security group  dclab-wfh-sg, add inbound port 80 for everyone.
2. Edit the Variables under ansible/nexus/vars/nginx.yml
  2.1. Edit IP addresss  for Jenkins/Nexus/Dockerhub  if any ST box added
  2.2  change the staging = false
3. Run playbook to configure the nginx

```ansible-playbook -i inventory --private-key ~/key/rajesh.pem nginx.yml```
