#!/bin/bash

inventory=$1
shift #remove first argument
plays=$@

already_up=$(docker ps -qf name=ansible)


if [[ -z "$inventory" || -z "$plays" ]] ; then
  echo No inventory and plays supplied...
  exit 1
fi

echo
echo Using inventory: $inventory
echo Running following plays: $plays

if [ -z "$already_up" ] ; then
  docker-compose up -d
  sleep 2
  if [[ "$OSTYPE" -eq 'msys' ]] ; then
    winpty docker-compose exec ansible bash -c "yum install -y sudo"
  else
    docker-compose exec ansible bash -c "yum install -y sudo"
  fi
else
  echo
  echo Continue on existing container instance...
  echo To start fresh run 'docker-compose down'.
fi

if [[ "$OSTYPE" -eq 'msys' ]] ; then
  winpty docker-compose exec -w //etc/ansible ansible bash -c "echo ${VAULT_PASS:-password} > /tmp/.vault_password && ansible-playbook -i ${inventory} ${plays} --skip-tags lvm"
else
  docker-compose exec -w /etc/ansible ansible bash -c "echo ${VAULT_PASS:-password} > /tmp/.vault_password && ansible-playbook -i ${inventory} ${plays} --skip-tags lvm"
fi
