import jenkins.model.*
import hudson.model.Item
import hudson.security.*
import groovy.json.JsonSlurper

def user_json = '''
${user}
'''

def jenkins_user = new JsonSlurper().parseText(user_json)

def instance = Jenkins.getInstance()

def hudsonRealm = new HudsonPrivateSecurityRealm(false)
def strategy = instance.getAuthorizationStrategy() ?: new GlobalMatrixAuthorizationStrategy()

if (!(strategy instanceof GlobalMatrixAuthorizationStrategy)) {
  strategy = new GlobalMatrixAuthorizationStrategy()
}

hudsonRealm.createAccount(jenkins_user.username, jenkins_user.password)

for (permission in jenkins_user.permissions) {
  switch (permission) {
    case "admin":
      strategy.add(Jenkins.ADMINISTER, jenkins_user.username);
      break;
    case "build":
      strategy.add(Item.BUILD, jenkins_user.username);
      break;
    case "cancel":
      strategy.add(Item.CANCEL, jenkins_user.username);
      break;
    case "discover":
      strategy.add(Item.DISCOVER, jenkins_user.username);
      break;
    case "workspace":
      strategy.add(Item.WORKSPACE, jenkins_user.username);
      break;
    case "item-read":
      strategy.add(Item.READ, jenkins_user.username);
      break;
    case "read":
      strategy.add(Jenkins.READ, jenkins_user.username);
      break;
  }
}

instance.setAuthorizationStrategy(strategy)
instance.setSecurityRealm(hudsonRealm)
instance.save()
