import groovy.json.JsonSlurper
import jenkins.model.Jenkins
import jenkins.plugins.nodejs.tools.NodeJSInstallation
import jenkins.plugins.nodejs.tools.NodeJSInstaller
import hudson.model.JDK
import hudson.tools.JDKInstaller
import hudson.tasks.Maven.MavenInstallation
import hudson.tasks.Maven.MavenInstaller
import hudson.plugins.gradle.GradleInstallation
import hudson.plugins.gradle.GradleInstaller
import hudson.tools.InstallSourceProperty
import hudson.tools.ToolProperty
import hudson.tools.ToolPropertyDescriptor
import hudson.util.DescribableList
import org.jenkinsci.plugins.docker.commons.tools.DockerTool
import org.jenkinsci.plugins.docker.commons.tools.DockerToolInstaller
import org.jenkinsci.plugins.golang.GolangInstallation
import org.jenkinsci.plugins.golang.GolangInstaller
import hudson.plugins.sonar.SonarRunnerInstallation
import hudson.plugins.sonar.SonarRunnerInstaller

def type = '${type}'

def installations_json = '''
${installations}
'''

def tool_installations = new JsonSlurper().parseText(installations_json)

if (tool_installations.size() == 0 || type == '') {
  return ''
}

Jenkins instance = Jenkins.getInstance()

switch (type) {
  case 'jdk':
    def jdk = instance.getExtensionList(JDK.DescriptorImpl.class)[0]
    JDK[] installations = tool_installations.collect { it -> new JDK(it.name, '', [new InstallSourceProperty([new JDKInstaller(it.version.toString(), true)])]) }
    jdk.setInstallations(installations)
    jdk.save()
    return 'updated jdk installations'
    break
  case 'gradle':
    def gradle = instance.getExtensionList(hudson.plugins.gradle.Gradle.DescriptorImpl.class)[0]
    GradleInstallation[] installations = tool_installations.collect { it -> new GradleInstallation(it.name, '', [new InstallSourceProperty([new GradleInstaller(it.version.toString())])]) }
    gradle.setInstallations(installations)
    gradle.save()
    return 'updated gradle installations'
    break
  case 'maven':
    def maven = instance.getExtensionList(hudson.tasks.Maven.DescriptorImpl.class)[0]
    MavenInstallation[] installations = tool_installations.collect { it -> new MavenInstallation(it.name, '', [new InstallSourceProperty([new MavenInstaller(it.version.toString())])]) }
    maven.setInstallations(installations)
    maven.save()
    return 'updated maven installations'
    break
  case 'golang':
    def golang = instance.getExtensionList(GolangInstallation.DescriptorImpl.class)[0]
    GolangInstallation[] installations = tool_installations.collect { it -> new GolangInstallation(it.name, '', [new InstallSourceProperty([new GolangInstaller(it.version.toString())])]) }
    golang.setInstallations(installations)
    golang.save()
    return 'updated go installations'
    break
  case 'nodejs':
    def nodejs = instance.getExtensionList(NodeJSInstallation.DescriptorImpl.class)[0]
    NodeJSInstallation[] installations = tool_installations.collect { it -> new NodeJSInstallation(it.name, '', [new InstallSourceProperty([new NodeJSInstaller(it.version.toString(), it.global_packages, it.refresh)])]) }
    nodejs.setInstallations(installations)
    nodejs.save()
    return 'updated nodejs installations'
    break
  case 'docker':
    def docker = instance.getExtensionList(DockerTool.DescriptorImpl.class)[0]
    DockerTool[] installations = tool_installations.collect { it -> new DockerTool(it.name, '', [new InstallSourceProperty([new DockerToolInstaller('', it.version.toString())])]) }
    docker.setInstallations(installations)
    docker.save()
    return 'updated docker installations'
    break
  case 'sonarqube':
    def sonarqube = instance.getExtensionList(SonarRunnerInstallation.DescriptorImpl.class)[0]
    SonarRunnerInstallation[] installations = tool_installations.collect { it -> new SonarRunnerInstallation(it.name, '', [new InstallSourceProperty([new SonarRunnerInstaller(it.version.toString())])]) }
    sonarqube.setInstallations(installations)
    sonarqube.save()
    return 'updated sonarqube installations'
    break
  default:
    return 'invalid type'
}

