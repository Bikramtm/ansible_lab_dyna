import groovy.json.JsonSlurper
import com.amazonaws.services.ec2.model.InstanceType
import com.cloudbees.jenkins.plugins.awscredentials.AWSCredentialsImpl
import com.cloudbees.plugins.credentials.*
import com.cloudbees.plugins.credentials.domains.Domain
import hudson.model.*
import hudson.plugins.ec2.AmazonEC2Cloud
import hudson.plugins.ec2.AMITypeData
import hudson.plugins.ec2.EC2Tag
import hudson.plugins.ec2.SlaveTemplate
import hudson.plugins.ec2.SpotConfiguration
import hudson.plugins.ec2.UnixData
import hudson.plugins.ec2.WindowsData
import jenkins.model.Jenkins

def jenkins_master_private_key = '''
${private_key}
'''
def init_script = '''
${init_script}
'''
def config_json = '''
${config}
'''

def ec2config = new JsonSlurper().parseText(config_json)

// get Jenkins instance
Jenkins jenkins = Jenkins.getInstance()

jenkins.clouds.clear()

ec2config.clouds.each { c ->

  // https://github.com/jenkinsci/ec2-plugin/blob/ec2-1.38/src/main/java/hudson/plugins/ec2/SlaveTemplate.java
  List<SlaveTemplate> templates = c.slaveTemplates.collect { t ->
    new SlaveTemplate(
      t.ami, // ami
      '', // zone
      null,
      t.securitygroup,
      t.remote_fs, // remote fs
      InstanceType.fromValue(t.type),
      false, // ebs optimized
      t.labels,
      Node.Mode.EXCLUSIVE,
      t.description,
      t.initscript ? init_script.trim() + ' ' +t.ansible_tags : '',
      t.temp_dir, // temp dir
      '', // user data
      t.executors.toString(), // number of executors
      t.remote_user, // remote admin
      t.ami_type == 'unix' ? new UnixData(null, null, null, '22'): new WindowsData(ec2config.ami_password, false, '300'),
      '', // jvm opts
      false, // stop on terminate
      t.subnetid, // subnet id
      t.tags.collect { tag -> new EC2Tag(tag.key, tag.value) },
      t.terminate_time.toString(), // idle termination minutes
      true, // use private dns name
      t.instancecap.toString(), // instance cap
      '', // iam instance profile
      true, // delete root on termination
      false, // use ephemeral devices
      false, // use dedicated tenancy
      '300', // launch timeout
      false, // associate public ip
      t.devicemap, // custom device mapping
      t.sshconnect, // connect by ssh process
      false, // connect via public IP
    )
  }

  // https://github.com/jenkinsci/ec2-plugin/blob/ec2-1.38/src/main/java/hudson/plugins/ec2/AmazonEC2Cloud.java
  jenkins.clouds.add(new AmazonEC2Cloud(
    c.name,
    true, // use instance profile for credentials
    'jenkins-aws-key', // credentials id
    c.region, // region
    jenkins_master_private_key, // private key
    '2', // instance cap
    templates,
    '',
    ''
  ))
}

jenkins.save()
