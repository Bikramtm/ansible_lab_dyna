# Nexus

This role is intended to run after the ec2 instance creation, This will provide the login access to other members with their public key.

### General steps to execute ansible playbook

1. Login to ansible server. (install ansible and git if required)
2. Pull the latest code in the ansible server, under your home directory
3. Go to the components directory i.e aws-ominus-infra/ansible
4. Review all the Variables in inventories, change the remote_user to ansible user if required
5. Run `ansible-playbook -i inventories/production/nexus/hosts  playbooks/install/nexus.yml`


### Nexus general

Nexus for dyna is running already in  AWS and take a daily backup to S3 Bucket s3://sonatype-nexus3-backup


# Prepare for a backup to S3

1. Login to nexus and install awscli (already installed)
2. Configure aws credentails to use S3
3. Transfer all the files to S3 bucket
4. Server running in AWS has awscli configured, just initate the copy again.
```
$ aws s3 sync /nexus-s3data/nexus_app_data/blobs s3://sonatype-nexus3-backup
````
4. This will take a backup of default directory in S3 bucket.
5. Login https://nexus.dynacommercelab.com ,  Run a backup/Export the configuration for metabackup.
6. Tasks -> Export configuration & metadata for backup -> Run
7. Backup files will be saved under the location /var/backups/nexus, from CLI of nexus server backup these files in S3

```
aws s3 sync /var/backups/nexus/ s3://sonatype-nexus3-backup/nexus-backup/
```

# Prepare for Nexus Restore

1. Follow the general steps to run the ansible playbook (explained above)
2. Edit all the vars for nexus, make false if any  taks to be skipped.
3. Run the play book, `ansible-playbook -i inventories/production/continious-development/hosts  nexus.yml`


# Post nexus configuration

Under Security > Realms, enable the “Docker Bearer Token Realm”
Uncheck “Force basic authentication” in the repository configuration docker-hosted repo
