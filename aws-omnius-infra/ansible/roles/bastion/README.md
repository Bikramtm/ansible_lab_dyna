# User management

This role is intended to run after the ec2 instance creation, This will provide the login access to other members with their public key.

### General steps to execute ansible playbooks

1. Login to ansible server. (install ansible and git if required)
   ```
       sudo yum install -y git ansible expect
   ```
2. Pull the latest code in the ansible server, under your home directory
3. Check the vault password
4. Review all the Variables, change the remote_user to ansible user if required
5. Run `ansible-playbook -i inventory bastion.yml --ask-vault-pass`

```ansible-playbook -i inventories/development/bastion/hosts playbooks/install/bastion.yml --vault-password-file .vault_password```

### Execute User management role

Edit the inventory file to add public or private  (use private ip if tunnel is setup already)

```
[nginx]
20.0.0.123

[nat]
20.0.0.71   ansible_user=ec2-user   ### vpc-nat

[jenkins-master]
20.0.1.173
```

Note : Configure the ssh proxy to make ansible to work with MAC. Else run this playbook from Ansible server

Add the user variables if required,


Modify the inventory as explained above.

Run the ansible command to configure the users in all servers created in EC2

Run ```ansible-playbook -i inventory bastion.yml --vault-id infra@prompt```
    ( provide the vault password)


