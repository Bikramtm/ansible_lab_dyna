resource "aws_iam_instance_profile" "ec2-ssm-role-profile" {
  name = "ec2-ssm-role-profile"
  role = "${aws_iam_role.ec2-ssm-role.name}"
}

resource "aws_iam_role" "ec2-ssm-role" {
  name = "ec2-ssm-role"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Principal": {
          "Service": "ec2.amazonaws.com"
        },
        "Action": "sts:AssumeRole"
      }
    ]
  }
EOF
}

resource "aws_iam_policy" "ssm_role_policy" {
  name        = "ssm_role_policy"
  description = "SSM Policy"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "ec2:Describe*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "ec2-role-policy" {
  role       = "${aws_iam_role.ec2-ssm-role.id}"
  policy_arn = "${aws_iam_policy.ssm_role_policy.arn}"
}

resource "aws_iam_role_policy_attachment" "ec2-ssm-role-policy" {
  role       = "${aws_iam_role.ec2-ssm-role.id}"
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2RoleforSSM"
}

resource "aws_ssm_document" "ssm_document_domain" {
  name          = "ssm_document_dynacommercelab.com"
  document_type = "Command"

  content = <<DOC
{
    "schemaVersion": "1.2",
    "description": "Automatic Domain Join Configuration",
    "runtimeConfig": {
        "aws:domainJoin": {
            "properties": {
                "directoryId": "d-996735551f",
                "directoryName": "dynacommercelab.com",
                "dnsIpAddresses": ["20.0.0.91", "20.0.2.30"]
            }
        }
    }
}
DOC
}
