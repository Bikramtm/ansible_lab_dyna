provider "aws" {
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region     = "${var.aws_region}"
}

terraform {
  backend "s3" {
    bucket         = "dyna-terraform-state"
    key            = "email.tfstate"
    region         = "eu-central-1"
    dynamodb_table = "terraform-lock"
  }
}

# dynacommercelab.com
resource "aws_ses_domain_identity" "dynacommercelab" {
  domain = "dynacommercelab.com"
}

resource "aws_ses_domain_dkim" "dynacommercelab" {
  domain = "dynacommercelab.com"
}

resource "aws_route53_record" "dynacommercelab_com_amazonses_verification_record" {
  zone_id = "${var.zone_id}"
  name    = "_amazonses.dynacommercelab.com"
  type    = "TXT"
  ttl     = "600"
  records = ["${aws_ses_domain_identity.dynacommercelab.verification_token}"]
}

resource "aws_route53_record" "dynacommercelab_com_amazonses_dkim" {
  count   = 3
  zone_id = "${var.zone_id}"
  name    = "${element(aws_ses_domain_dkim.dynacommercelab.dkim_tokens, count.index)}._domainkey.dynacommercelab.com"
  type    = "CNAME"
  ttl     = "600"
  records = ["${element(aws_ses_domain_dkim.dynacommercelab.dkim_tokens, count.index)}.dkim.amazonses.com"]
}
