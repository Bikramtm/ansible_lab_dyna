provider "aws" {
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region     = "${var.aws_region}"
}

terraform {
  backend "s3" {
    bucket         = "techm-terraform-state"
    key            = "OmniusBM_Network.tfstate"
    region         = "eu-central-1"
    dynamodb_table = "terraform-lock"
  }
}

module "acm" {
  source                            = "../../modules/acm"
  domain_name                       = "*.omniusbm.com"
  hosted_zone_id                    = "${var.zone_id}"
  allow_validation_record_overwrite = false
}

module "elb" {
  source = "../../modules/elb"

  name                        = "OmniusBM"
  security_groups             = "${module.vpc.security_group_default}"
  subnets                     = "${module.vpc.subnet_private_id}"
  internal                    = "false"
  cross_zone_load_balancing   = "true"
  idle_timeout                = "60"
  connection_draining         = "false"
  connection_draining_timeout = "300"
  tags                        = "OmniusBM"
  listener                    = "${var.listener}"
  access_logs                 = "${var.access_logs}"
  health_check                = "${var.health_check}"
  number_of_instances         = "${var.number_of_instances}"
  instances                   = "${var.instances}"
}
