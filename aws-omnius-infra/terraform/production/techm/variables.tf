variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "aws_key_name" {}
variable "aws_key_path" {}

variable "aws_region" {
  description = "EC2 Region for the VPC"
  default     = "eu-central-1"
}

variable "project" {
  description = "The resource project"
  default     = "techmlab"
}

variable "environment" {
  description = "Environment for the resource"
  default     = "OmniusBM"
}

variable "aws_account" {
  description = "AWS account for TechM to be used"
  default     = "826053957657"
}

variable "iam_users" {
  description = "AWS IAM user for Dynacommerce"

  default = {
    "rs00629314@techmahindra.com" = "Rajesh S"
    "RK00629318@TechMahindra.com" = "Ranjith KA"
  }
}

variable "source_ip" {
  description = "Source IP to allow modification"

  default = {
    "93.117.239.90/32" = "sittard"
    "93.117.239.92/32" = "banaglore"
  }
}
