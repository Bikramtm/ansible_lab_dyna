provider "aws" {
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region     = "${var.aws_region}"
}

## Enable once you configure the DynamoDB and S3,
terraform {
  backend "s3" {
    bucket         = "techm-terraform-state"
    key            = "backend.tfstate"
    region         = "eu-central-1"
    dynamodb_table = "terraform-lock"
  }
}

module "backend" {
  source            = "../../modules/backend"
  dynamodb_table    = "yes"
  table_name        = "terraform-lock"
  hash_key          = "LockID"
  create_s3         = "yes"
  s3_bucket_backend = "techm-terraform-state"
  environment       = "${var.environment}"
  project           = "${var.project}"
  source_ip         = "${var.source_ip}"
  iam_users         = "${var.iam_users}"
  aws_account       = "${var.aws_account}"
}
