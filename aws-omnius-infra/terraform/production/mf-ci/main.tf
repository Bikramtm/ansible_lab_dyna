provider "aws" {
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region     = "${var.aws_region}"
}

#### Backend for tfstate file
terraform {
  backend "s3" {
    bucket         = "dyna-terraform-state"
    key            = "mf-cd.tfstate"
    region         = "eu-central-1"
    dynamodb_table = "terraform-lock"
  }
}

data "terraform_remote_state" "network" {
  backend = "s3"

  config {
    bucket = "dyna-terraform-state"
    key    = "network.tfstate"
    region = "eu-central-1"
  }
}

module "nexus" {
  source                 = "../../modules/nexus"
  aws_region             = "${var.aws_region}"
  security_group_default = "${data.terraform_remote_state.network.security_group_megafone}"
  subnet_id              = "${data.terraform_remote_state.network.subnet_public_id}"
  project                = "megafone"
  dns_record             = "yes"
  nginx_name             = "artifacts"
  nexus_name             = "mf-nexus"
  docker_name            = "mf-dockerhub"
  nexus_extra_vol        = "no"
  nexus_volume_size      = ""
  key_name               = "${var.aws_key_name}"
  zone_name              = "${var.zone_name}"
  zone_id                = "${var.zone_id}"
  create_s3              = "yes"
  nexus_eip              = "yes"
  ass_public_ip          = "true"
  s3-bucket              = "mf-cd-nexus3-backup"
}
