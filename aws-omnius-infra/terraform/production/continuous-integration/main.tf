provider "aws" {
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region     = "${var.aws_region}"
}

#### Backend for tfstate file
terraform {
  backend "s3" {
    bucket         = "dyna-terraform-state"
    key            = "continuous-integration.tfstate"
    region         = "eu-central-1"
    dynamodb_table = "terraform-lock"
  }
}

data "terraform_remote_state" "network" {
  backend = "s3"

  config {
    bucket = "dyna-terraform-state"
    key    = "network.tfstate"
    region = "eu-central-1"
  }
}

module "jenkins-master" {
  source                 = "../../modules/jenkins-master"
  aws_region             = "${var.aws_region}"
  security_group_wfh     = "${data.terraform_remote_state.network.security_group_wfh}"
  security_group_default = "${data.terraform_remote_state.network.security_group_default}"
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  project                = "dclab"
  key_name               = "${var.aws_key_name}"
  zone_name              = "${var.zone_name}"
  zone_id                = "${var.zone_id}"
}

module "nexus" {
  source                 = "../../modules/nexus"
  aws_region             = "${var.aws_region}"
  security_group_default = "${data.terraform_remote_state.network.security_group_default}"
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  project                = "dclab"
  dns_record             = "yes"
  nginx_name             = "nginx"
  nexus_name             = "nexus"
  docker_name            = "dockerhub"
  nexus_extra_vol        = "yes"
  nexus_volume_size      = "300"
  key_name               = "${var.aws_key_name}"
  zone_name              = "${var.zone_name}"
  zone_id                = "${var.zone_id}"
  create_s3              = "no"
  nexus_eip              = "no"
  ass_public_ip          = "false"
  s3-bucket              = "sonatype-nexus3-backup"
}
