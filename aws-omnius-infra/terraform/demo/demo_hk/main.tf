provider "aws" {
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region     = "${var.aws_region}"
}

#### Backend for tfstate file
terraform {
  backend "s3" {
    bucket         = "dyna-terraform-state"
    key            = "demo-hk-hutch.tfstate"
    region         = "eu-central-1"
    dynamodb_table = "terraform-lock"
  }
}

data "terraform_remote_state" "network" {
  backend = "s3"

  config {
    bucket = "dyna-terraform-state"
    key    = "network.tfstate"
    region = "eu-central-1"
  }
}

##### HK  terraform Resources #####

resource "aws_instance" "demo_webserver_hk" {
  ami                    = "${lookup(var.ami_st_ena, var.aws_region)}"
  instance_type          = "t3.xlarge"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  key_name               = "${var.aws_key_name}"

  tags {
    Name        = "${var.project}-demo-hk-aoi"
    Environment = "${var.project}-demo-hk"
    Project     = "${var.project}"
    Role        = "webserver,database,rabbitmq,redis"
  }

  root_block_device {
    volume_size           = "60"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_route53_record" "demo_hk_record" {
  count   = "${length(var.hk_dns)}"
  zone_id = "${var.zone_id}"
  name    = "${element(var.hk_dns, count.index)}"
  type    = "CNAME"
  ttl     = "300"
  records = ["nginx.${var.zone_name}"]
}

resource "aws_route53_record" "demo_hk_record_priv" {
  count   = "${length(var.hk_dns)}"
  zone_id = "${var.zone_id_priv}"
  name    = "${element(var.hk_dns, count.index)}"
  type    = "CNAME"
  ttl     = "300"
  records = ["nginx.${var.zone_name}"]
}
