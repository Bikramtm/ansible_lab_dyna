output "DEMO_WEB1" {
  value = "${aws_instance.demo_webserver_demo1.private_dns}"
}

output "DEMO_DB" {
  value = "${aws_instance.demo_db_server.private_dns}"
}

output "DEMO_RABBITMQ" {
  value = "${aws_instance.demo_rabbitmq_server.private_dns}"
}

output "DEMO_REDIS" {
  value = "${aws_instance.demo_redis_server.private_dns}"
}
