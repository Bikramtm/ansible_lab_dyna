variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "aws_key_name" {}
variable "aws_key_path" {}

variable "project" {
  description = "Created by terraform"
  default     = "demo"
}

variable "aws_region" {
  description = "EC2 Region for the VPC"
  default     = "eu-central-1"
}

variable "zone_name" {
  description = "DNS name for Zone"
  default     = "dynacommercelab.com"
}

variable "environment" {
  description = "Environment for the resource"
  default     = "demo"
}

#-------------------------------------------------------------------------------
# Zone ID to create existing zone
#-------------------------------------------------------------------------------

variable "zone_id" {
  description = "dynacommecelab.com ZONE ID"
  default     = "ZJKKGPVBFJK09"
}

variable "zone_id_priv" {
  description = "dynacommecelab.com ZONE ID"
  default     = "Z1EJMG1AOLMAH"
}

variable "ami_demo" {
  type        = "map"
  description = "AMIs by region"

  default = {
    eu-central-1 = "ami-dd3c0f36"
  }
}

variable "demo1_dns" {
  default = [
    "demo1-retail.dynacommercelab.com",
    "demo1-indirect.dynacommercelab.com",
    "demo1-telesales.dynacommercelab.com",
    "demo1-restapi.dynacommercelab.com",
    "demo1-rabbitmq.dynacommercelab.com",
  ]
}
