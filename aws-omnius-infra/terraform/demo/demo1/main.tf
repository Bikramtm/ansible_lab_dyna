provider "aws" {
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region     = "${var.aws_region}"
}

#### Backend for tfstate file
terraform {
  backend "s3" {
    bucket         = "dyna-terraform-state"
    key            = "demo.tfstate"
    region         = "eu-central-1"
    dynamodb_table = "terraform-lock"
  }
}

data "terraform_remote_state" "network" {
  backend = "s3"

  config {
    bucket = "dyna-terraform-state"
    key    = "network.tfstate"
    region = "eu-central-1"
  }
}

resource "aws_instance" "demo_db_server" {
  ami                    = "${lookup(var.ami_demo, var.aws_region)}"
  instance_type          = "t3.xlarge"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  key_name               = "${var.aws_key_name}"

  tags {
    Name        = "${var.project}-database"
    Environment = "${var.project}-demo"
    Project     = "${var.project}"
    Role        = "database"
  }

  volume_tags {
    Name        = "${var.project}-demo-database-vol"
    Environment = "${var.project}-demo"
    Project     = "${var.project}"
    Role        = "volume"
  }

  root_block_device {
    volume_size           = "60"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_instance" "demo_rabbitmq_server" {
  ami                    = "${lookup(var.ami_demo, var.aws_region)}"
  instance_type          = "t3.large"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  key_name               = "${var.aws_key_name}"

  tags {
    Name        = "${var.project}-rabbitmq"
    Environment = "${var.project}-demo"
    Project     = "${var.project}"
    Role        = "rabbitmq"
  }

  volume_tags {
    Name        = "${var.project}-demo-rabbitmq-vol"
    Environment = "${var.project}-demo"
    Project     = "${var.project}"
    Role        = "volume"
  }

  root_block_device {
    volume_size           = "20"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_instance" "demo_redis_server" {
  ami                    = "${lookup(var.ami_demo, var.aws_region)}"
  instance_type          = "t3.large"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  key_name               = "${var.aws_key_name}"

  tags {
    Name        = "${var.project}-redis"
    Environment = "${var.project}-demo"
    Project     = "${var.project}"
    Role        = "redis"
  }

  volume_tags {
    Name        = "${var.project}-demo-redis-vol"
    Environment = "${var.project}-demo"
    Project     = "${var.project}"
    Role        = "volume"
  }

  root_block_device {
    volume_size           = "20"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

##### ST1 Terraform Resources #####
resource "aws_instance" "demo_webserver_demo1" {
  ami                    = "${lookup(var.ami_demo, var.aws_region)}"
  instance_type          = "t3.xlarge"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  key_name               = "${var.aws_key_name}"

  volume_tags {
    Name        = "${var.project}-demo1-web-vol"
    Environment = "${var.project}-demo1"
    Project     = "${var.project}"
    Role        = "volume"
  }

  tags {
    Name        = "${var.project}-demo1-web"
    Environment = "${var.project}-demo1"
    Project     = "${var.project}"
    Role        = "webserver"
    Start       = "StartWeekDays"
    Stop        = "StopWeekDays"
  }

  root_block_device {
    volume_size           = "30"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_route53_record" "demo_demo1_dns_record" {
  count   = "${length(var.demo1_dns)}"
  zone_id = "${var.zone_id}"
  name    = "${element(var.demo1_dns, count.index)}"
  type    = "CNAME"
  ttl     = "300"
  records = ["nginx.${var.zone_name}"]
}

resource "aws_route53_record" "demo_demo1_dns_record_priv" {
  count   = "${length(var.demo1_dns)}"
  zone_id = "${var.zone_id_priv}"
  name    = "${element(var.demo1_dns, count.index)}"
  type    = "CNAME"
  ttl     = "300"
  records = ["nginx.${var.zone_name}"]
}
