 <powershell>
  Set-ExecutionPolicy unrestricted -Force
  $instanceId = (Invoke-RestMethod -Method Get -Uri http://169.254.169.254/latest/meta-data/instance-id)
  $hostname2 = ((Get-EC2Instance -InstanceId $instanceId).instances.tag | Where-Object { $_.Key -eq "hostname"})
  $hostname = $hostname2.Value
  Rename-Computer -NewName $hostname -Restart -Force
  </powershell>
