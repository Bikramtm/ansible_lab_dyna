# AWS OMNIUS INFRA #

# Index
* [Naming conventions](NAMING.md)


## Terraform Version and  Required Packages for MAC

  - Terraform v0.11.7
  - coreutils
  - aws cli [AWS cli installation guide](https://docs.aws.amazon.com/cli/latest/userguide/installing.html)
  - jq

```bash
brew install -y terraform coreutils jq
pip install awscli
```

Below you can find documentation on how to use the tooling to configure and maintain the network infrastructure for Omnius.

### First time setup

### Prerequisites

AWS credentials to access you AWS via AWS CLI

1. Create a IAM user (Admin access) in AWS and generate the access key and secrets for the user
2. Create a Keypair under EC2 service for the region to use, download the Private key and save it as your key path
   https://eu-central-1.console.aws.amazon.com/ec2/v2/home?region=eu-central-1#KeyPairs:sort=keyName

### How to configure your credentials ###

Make sure to put your terraform.tfvars file into place. Copy paste the example file and modify the file accordingly to use your personal credentials.

```bash
cp terraform/terraform.tfvars.example terraform/production/network/terraform.tfvars
// change the contents of the file terraform/production/network/terraform.tfvars with your credentials
cp terraform/production/network/terraform.tfvars terraform/production/continuous-integration//terraform.tfvars
```

Also make sure the aws cli is configured with oyur credentials. Make sure you provide your own credentials, don't use below example literally.

```bash
$ aws configure
AWS Access Key ID [None]: AKIADYVXWJ33ELHDX5GQ
AWS Secret Access Key [None]: ZMiwKV7yojE8UbydVf9k2782KPtmSn2xRpR8Jexd
Default region name [None]: eu-central-1
Default output format [None]: json
```

This Terraform code use Backend as S3 bucket, So it required S3 bucket to save the tfstate file.

For this project,  dyna-terraform-state S3 bucket created already in Frankfurt location.
Lock the state file is achieved via DyanamoDB Table, terraform-lock table is created  for this purpose

The following details are in the main.tf file of your terraform environment. For example: production/network/main.tf.

```
bucket = "dyna-terraform-state"
key = "network.tfstate"
region = "eu-central-1"
dynamodb_table = "terraform-lock"
```

*If you are creating new environment or want to test something make sure you create a new terraform environment which also connects to a new s3 bucket and dynamodb table to share the state with the team.*

In Route53 dynnacomerlab.com was purchased and this hosted zone will be used for this project.
variables need to be set before terraform planning.

The terraform environments are segregated into 2 parts. 1 part sets up the network and vpc parts. The other terraform environment puts in place the servers for a specific environment.

### Initialize

In-order to initialize terraform to set the backend location, execute the following command.

```bash
cd terraform/
```

First we have to initialize the network for the terraform environments, then we can create the different server groups within the created network.

```bash
./init.sh production/network
./init.sh production/continuous-intergration
```

### Plan and Apply Networking for dclab

VPC and other Networks will be created under production/network terraform folder
Check the variables.tf under production/network, This code will support for Frankfurt and Singapore Locations only.
project name will be used for tagging all AWS resource.
zone name and zone_id are Prerequisites

```bash
./plan.sh production/network
```

Verify the details and apply the code.

```bash
./apply.sh production/network
```

### Plan and apply continuous-intergration

All other machines will be created under production/continuous-intergration terraform folder
This will use the data from production/network tfstate file.
Modify production/continuous-intergration/main.tf if any EC2 machines is not Required, i.e remove the module from main.tf

```bash
./plan.sh production/continuous-integration
```

Verify the details and apply the code.

```bash
./apply.sh production/continuous-integration
```

### Windows servers

Note: To add Windows resource, you need to verfiy the server is already in domain (this is manual steps)
1. Login to termainal server (3.120.127.66)
2. Open Active Directory and user  managment in the server,
3. Check for the dyncommercelab.com domain and check the computers in the domain.
4. If you see the same host name already registerd, delete in joined computers and DNS records.
5. DNS records are manully managed, DNS is available in the same server, location under desktop
