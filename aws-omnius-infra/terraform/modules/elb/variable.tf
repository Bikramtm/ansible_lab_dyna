variable "name" {}

variable "security_groups" {}

variable "subnets" {}

variable "internal" {}

variable "cross_zone_load_balancing" {}

variable "idle_timeout" {}

variable "connection_draining" {}

variable "connection_draining_timeout" {}

variable "tags" {}

variable "listener" {}

variable "access_logs" {}

variable "health_check" {}
