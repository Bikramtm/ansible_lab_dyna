resource "aws_route53_zone" "pzone" {
  count   = "${var.primary_zone  == "true" ? 1: 0}"
  name    = "${var.zone_name}"
  comment = "${var.project}-zone"

  tags {
    Name = "${var.project}-zone"
  }
}

resource "aws_route53_zone" "zone" {
  name    = "${var.project}.${var.zone_name}"
  comment = "${var.project}-bzone"
}

resource "aws_route53_record" "dev-ns1" {
  count   = "${var.primary_zone == "true" ? 1: 0}"
  zone_id = "${aws_route53_zone.pzone.zone_id}"
  name    = "${var.project}.${var.zone_name}"
  type    = "NS"
  ttl     = "30"

  records = [
    "${aws_route53_zone.zone.name_servers.0}",
    "${aws_route53_zone.zone.name_servers.1}",
    "${aws_route53_zone.zone.name_servers.2}",
    "${aws_route53_zone.zone.name_servers.3}",
  ]
}

resource "aws_route53_record" "dev-ns" {
  count   = "${var.primary_zone == "true" ? 0: 1}"
  zone_id = "${var.zone_id}"
  name    = "${var.project}"
  type    = "NS"
  ttl     = "300"

  records = [
    "${aws_route53_zone.zone.name_servers.0}",
    "${aws_route53_zone.zone.name_servers.1}",
    "${aws_route53_zone.zone.name_servers.2}",
    "${aws_route53_zone.zone.name_servers.3}",
  ]
}
