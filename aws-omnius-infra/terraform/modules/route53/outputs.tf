output "zone_id" {
  value = "${aws_route53_zone.zone.id}"
}

output "zone_name" {
  value = "${var.zone_name}"
}
