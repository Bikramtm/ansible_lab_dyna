resource "aws_iam_role" "jenkins" {
  name               = "${var.project}-jenkins"
  assume_role_policy = "${file("${path.module}/role-jenkins.json")}"
}

resource "aws_iam_policy" "jenkins-policy" {
  name        = "${var.project}-jenkins-policy"
  description = "Jenkins EC2 policy"
  policy      = "${file("${path.module}/jenkins-policy.json")}"
}

resource "aws_iam_policy_attachment" "jenkins-attach" {
  name       = "${var.project}-jenkins-attachment"
  roles      = ["${aws_iam_role.jenkins.name}"]
  policy_arn = "${aws_iam_policy.jenkins-policy.arn}"
}

resource "aws_iam_instance_profile" "jenkins-profile" {
  name = "${var.project}-jenkins-profile"
  role = "${aws_iam_role.jenkins.name}"
}

resource "aws_route53_record" "jenkins_record" {
  zone_id = "${var.zone_id}"
  name    = "jenkins.${var.zone_name}"
  type    = "CNAME"
  ttl     = "300"
  records = ["nginx.${var.zone_name}"]
}

##### Jenkins EC2

resource "aws_instance" "jenkins-master-v2" {
  ami                         = "${lookup(var.jenkins-ami-ena, var.aws_region)}"
  instance_type               = "t3.small"
  key_name                    = "${var.key_name}"
  vpc_security_group_ids      = ["${var.security_group_default}"]
  subnet_id                   = "${var.subnet_id}"
  iam_instance_profile        = "${var.project}-jenkins-profile"
  associate_public_ip_address = "false"

  credit_specification {
    cpu_credits = "unlimited"
  }

  tags {
    Name        = "${var.project}-jenkins-master"
    Environment = "${var.project}-jenkins"
    Project     = "${var.project}"
    Role        = "jenkins"
  }

  volume_tags {
    Name        = "${var.project}-jenkins-master-v2-vol-1"
    Environment = "continuous-integration"
    Project     = "${var.project}"
    Role        = "volume"
  }

  root_block_device {
    volume_size           = "20"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_ebs_volume" "jenkins-master-v2-vol-2" {
  availability_zone = "${var.aws_region}a"
  size              = 250
  type              = "gp2"

  tags {
    Name        = "${var.project}-jenkins-master-v2-vol-2"
    Environment = "continuous-integration"
    Project     = "${var.project}"
    Role        = "volume"
  }
}

resource "aws_volume_attachment" "jenkins-master-v2-vol-2-attach" {
  device_name = "/dev/sdb"
  volume_id   = "${aws_ebs_volume.jenkins-master-v2-vol-2.id}"
  instance_id = "${aws_instance.jenkins-master-v2.id}"
}

resource "aws_ebs_volume" "jenkins-master-v2-vol-3" {
  availability_zone = "${var.aws_region}a"
  size              = 200
  type              = "gp2"

  tags {
    Name        = "${var.project}-jenkins-master-v2-vol-3"
    Environment = "continuous-integration"
    Project     = "${var.project}"
    Role        = "volume-2"
  }
}

resource "aws_volume_attachment" "jenkins-master-v2-vol-3-attach" {
  device_name = "/dev/sdd"
  volume_id   = "${aws_ebs_volume.jenkins-master-v2-vol-3.id}"
  instance_id = "${aws_instance.jenkins-master-v2.id}"
}

##### Jenkins Update 9/11

resource "aws_instance" "jenkins-master-v3" {
  ami                         = "${lookup(var.jenkins-ami-ena, var.aws_region)}"
  instance_type               = "t3.small"
  key_name                    = "${var.key_name}"
  vpc_security_group_ids      = ["${var.security_group_default}"]
  subnet_id                   = "${var.subnet_id}"
  iam_instance_profile        = "${var.project}-jenkins-profile"
  associate_public_ip_address = "false"

  credit_specification {
    cpu_credits = "unlimited"
  }

  tags {
    Name        = "${var.project}-jenkins-master-v3"
    Environment = "${var.project}-jenkins"
    Project     = "${var.project}"
    Role        = "jenkins"
  }

  volume_tags {
    Name        = "${var.project}-jenkins-master-v3-vol-1"
    Environment = "continuous-integration"
    Project     = "${var.project}"
    Role        = "volume"
  }

  root_block_device {
    volume_size           = "20"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_ebs_volume" "jenkins-master-v3-vol-2" {
  availability_zone = "${var.aws_region}a"
  size              = 250
  type              = "gp2"

  tags {
    Name        = "${var.project}-jenkins-master-v3-vol-2"
    Environment = "continuous-integration"
    Project     = "${var.project}"
    Role        = "volume"
  }
}

resource "aws_volume_attachment" "jenkins-master-v3-vol-2-attach" {
  device_name = "/dev/sdb"
  volume_id   = "${aws_ebs_volume.jenkins-master-v3-vol-2.id}"
  instance_id = "${aws_instance.jenkins-master-v3.id}"
}
