{
	"Version": "2012-10-17",
	"Statement": [{
		"Effect": "Deny",
		"NotPrincipal": {
			"AWS": [
				${iam_user}
			]
		},
		"Action": [
			"s3:GetObject",
			"s3:GetBucketLocation",
			"s3:PutObject",
			"s3:ListBucket",
			"s3:DeleteObject"
		],
		"Resource": ["arn:aws:s3:::${bucket_name}",
			"arn:aws:s3:::${bucket_name}/*"
		],
		"Condition": {
			"NotIpAddress": {
				"aws:SourceIp": ${source_ip}
			}
		}
	}]
}
