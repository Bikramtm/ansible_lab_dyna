data "template_file" "iam-users-auth" {
  count = "${length(var.iam_users)}"

  template = <<JSON
  "arn:aws:iam::$${aws_account}:user/$${iam_user}"
  JSON

  vars {
    aws_account = "${var.aws_account}"
    iam_user    = "${element(keys(var.iam_users), count.index)}"
  }
}

data "template_file" "s3-bucket-policy" {
  template = "${file("${path.module}/policies/policy-s3-bucket.json.tpl")}"

  vars {
    bucket_name = "${var.s3_bucket_backend}"
    source_ip   = "${jsonencode(keys(var.source_ip))}"
    iam_user    = "${join(",\n", data.template_file.iam-users-auth.*.rendered)}"
  }
}

resource "null_resource" "export_rendered_template" {
  triggers {
    build_number = "${timestamp()}"
  }

  provisioner "local-exec" {
    command = "cat > test_output.json <<EOL\n${join(",\n", data.template_file.s3-bucket-policy.*.rendered)}\nEOL"
  }
}

resource "aws_dynamodb_table" "dynamodb_table" {
  count          = "${var.dynamodb_table == "no" ? 0 : 1}"
  name           = "${var.table_name}"
  hash_key       = "${var.hash_key}"
  read_capacity  = 5
  write_capacity = 5

  attribute {
    name = "${var.hash_key}"
    type = "S"
  }

  tags = {
    Name        = "${var.project}-dynamodb-table-1"
    Environment = "${var.environment}"
  }
}

resource "aws_s3_bucket" "techm-terraform-state" {
  count  = "${var.create_s3 == "no" ? 0 : 1}"
  bucket = "${var.s3_bucket_backend}"
  acl    = "private"

  versioning {
    enabled = true
  }

  logging {
    target_bucket = "${aws_s3_bucket.tf_log_bucket.id}"
    target_prefix = "log/"
  }

  lifecycle {
    prevent_destroy = true
  }

  tags = {
    Name        = "${var.project}-backend-s3"
    Environment = "${var.environment}"
  }
}

resource "aws_s3_bucket" "tf_log_bucket" {
  count  = "${var.create_s3 == "no" ? 0 : 1}"
  bucket = "${var.s3_bucket_backend}-logs"
  acl    = "log-delivery-write"

  tags = {
    Name        = "${var.project}-backend-s3-logs"
    Environment = "${var.environment}"
  }

  lifecycle_rule {
    id      = "log"
    enabled = true

    prefix = "log/"

    tags = {
      "rule"      = "log"
      "autoclean" = "true"
    }

    expiration {
      days = 30
    }
  }
}

resource "aws_s3_bucket_policy" "techm-tf-bucket-policy" {
  count      = "${var.create_s3 == "no" ? 0 : 1}"
  depends_on = ["aws_s3_bucket.techm-terraform-state"]
  bucket     = "${aws_s3_bucket.techm-terraform-state.id}"
  policy     = "${data.template_file.s3-bucket-policy.rendered}"
}
