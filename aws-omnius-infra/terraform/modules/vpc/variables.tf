# Please do not populate these 4 variables in here.
# They will be populated from secrets.tfvars
#variable "aws_access_key" {}
#variable "aws_secret_key" {}
#variable "aws_key_path" {}
#variable "aws_key_name" {}

#variables from main file
variable "project" {}

variable "aws_region" {}
variable "aws_key_name" {}
variable "zone_id_priv" {}

variable "natami" {
  type        = "map"
  description = "Nat Ami"

  default = {
    ap-southeast-1 = "ami-b082dae2"
    eu-central-1   = "ami-2d53e442"
  }
}

variable "vpc_cidr" {
  description = "CIDR for the whole VPC"
  default     = "20.0.0.0/16"
}

variable "public_subnet_cidr" {
  description = "CIDR for the Public Subnet"
  default     = "20.0.0.0/24"
}

variable "private_subnet_cidr" {
  description = "CIDR for the Private Subnet"
  default     = "20.0.1.0/24"
}

variable "wfh_cidrs" {
  description = "CIDR for the Private Subnet"
  type        = "list"

  default = ["137.97.89.217/32", "85.147.250.89/32", "93.117.239.90/32"]
}

variable "k8s_private" {
  description = "Load balancer urls"

  default = [
    "packagist",
    "sonarqube",
    "vagrant",
    "dev-api-vnext",
    "dev-vnext",
    "docs-vnext",
    "dev-vnext-demo",
    "elasticsearch",
    "admin",
    "cerebro",
    "es5",
    "kibana",
    "telegraf-influxdb",
    "vagrant",
  ]
}
