output "security_group_nat" {
  value = "${aws_security_group.nat.id}"
}

output "subnet_private_id" {
  value = "${aws_subnet.subnet-private.id}"
}

output "subnet_public_id" {
  value = "${aws_subnet.subnet-public.id}"
}

output "security_group_wfh" {
  value = "${aws_security_group.wfh.id}"
}

output "security_group_default" {
  value = "${aws_vpc.default.default_security_group_id}"
}

output "bitbucket_ipsv4_sg" {
  value = "${aws_security_group.bitbucket_ipsv4_sg.id}"
}

output "bitbucket_ipsv6_sg" {
  value = "${aws_security_group.bitbucket_ipsv6_sg.id}"
}

output "security_group_megafone" {
  value = "${aws_security_group.megafone.id}"
}
