#!/bin/bash

# Filter IPv4 addresses matching CIDR patterns
IPv4=$(curl -s https://ip-ranges.atlassian.com/ | jq .items[].cidr | sed -e 's/"//g' | grep -oE "^([0-9]{1,3}\.){3}[0-9]{1,3}(\/([0-9]|[1-2][0-9]|3[0-2]))?$")

jq -n --arg ipsv4 "$IPv4" '{"bitbucket_ipsv4":$ipsv4}'
