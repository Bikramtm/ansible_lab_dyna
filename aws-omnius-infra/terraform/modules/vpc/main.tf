resource "aws_vpc" "default" {
  cidr_block           = "${var.vpc_cidr}"
  enable_dns_hostnames = true

  tags {
    Name = "${var.project}-vpc"
  }
}

resource "aws_internet_gateway" "default" {
  vpc_id = "${aws_vpc.default.id}"

  tags {
    Name = "${var.project}-igw"
  }
}

data "external" "bitbucket_ipsv4" {
  program = ["bash", "${path.module}/external-scripts/fetch-bitbucket-ipsv4.sh"]
}

data "external" "bitbucket_ipsv6" {
  program = ["bash", "${path.module}/external-scripts/fetch-bitbucket-ipsv6.sh"]
}

locals {
  bitbucket_ipsv4 = ["${split("\n", data.external.bitbucket_ipsv4.result.bitbucket_ipsv4)}"]
  bitbucket_ipsv6 = ["${split("\n", data.external.bitbucket_ipsv6.result.bitbucket_ipsv6)}"]
}

resource "aws_default_security_group" "default" {
  vpc_id = "${aws_vpc.default.id}"

  ingress {
    protocol  = -1
    self      = true
    from_port = 0
    to_port   = 0
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "${var.project}-default"
  }
}

resource "aws_security_group" "nat" {
  name        = "${var.project}-vpc-nat"
  description = "Allow traffic for webserver"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["${var.private_subnet_cidr}"]
    description = "Allow Port 80 in public"
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["${var.private_subnet_cidr}"]
    description = "Allow Port 443 in private"
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${var.private_subnet_cidr}"]
    description = "Allow Port 22 in Private server to call back git via ssh"
  }

  ingress {
    from_port   = 9418
    to_port     = 9418
    protocol    = "tcp"
    cidr_blocks = ["${var.private_subnet_cidr}"]
    description = "Allow Port 9418 to public (git:// protocol)"
  }

  ingress {
    from_port   = 8086
    to_port     = 8086
    protocol    = "tcp"
    cidr_blocks = ["${var.private_subnet_cidr}"]
    description = "Allow Port 8086 to public to access influxdb"
  }

  ingress {
    from_port   = 9300
    to_port     = 9300
    protocol    = "tcp"
    cidr_blocks = ["${var.private_subnet_cidr}"]
    description = "Allow Port 9300 to private subnet to access elasticsearch transport"
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
    description      = "Allow all outbound connection"
  }

  vpc_id = "${aws_vpc.default.id}"

  tags {
    Name = "${var.project}-NATSG"
  }
}

resource "aws_security_group" "wfh" {
  name        = "${var.project}-wfh-sg"
  description = "WFH White List"

  lifecycle {
    create_before_destroy = true
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["109.232.45.2/32"]
    description = "Openline jenkins public ip"
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["139.47.185.6/32"]
    description = "Vznl illionx ip"
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["139.47.185.7/32"]
    description = "Vznl illionx ip"
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["109.232.45.2/32"]
    description = "Openline jenkins public ip"
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["93.117.239.90/32"]
    description = "Sittard Office IP"
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["93.117.239.90/32"]
    description = "Sittard Office IP"
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["93.117.239.90/32"]
    description = "Sittard Office IP"
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["84.28.134.217/32"]
    description = "Sittard BU Office IP"
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["84.28.134.217/32"]
    description = "Sittard BU Office IP"
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["84.28.134.217/32"]
    description = "Sittard BU Office IP"
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["${aws_eip.nat-eip.public_ip}/32"]
    description = "Private Network NAT IP"
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["3.127.203.245/32", "54.93.220.201/32", "18.185.86.6/32", "18.197.60.87/32", "18.195.95.175/32", "52.59.223.30/32"]
    description = "K8S nodes"
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["14.143.133.197/32", "14.143.28.84/32"]
    description = "India Office public IP"
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["14.143.133.197/32", "14.143.28.84/32"]
    description = "India Office public IP"
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["14.143.133.197/32", "14.143.28.84/32"]
    description = "India Office public IP"
  }

  ingress {
    from_port   = 3389
    to_port     = 3389
    protocol    = "tcp"
    cidr_blocks = ["93.117.239.90/32"]
    description = "Sittard Office IP"
  }

  ingress {
    from_port   = 3389
    to_port     = 3389
    protocol    = "tcp"
    cidr_blocks = ["84.28.134.217/32"]
    description = "Sittard BU Office IP"
  }

  ingress {
    from_port   = 3389
    to_port     = 3389
    protocol    = "tcp"
    cidr_blocks = ["14.143.133.197/32"]
    description = "India Office public IP"
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["203.143.188.11/32", "203.143.184.13/32", "203.143.186.22/32"]
    description = "TechM - Pune HJ NAT IPs"
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["203.143.188.11/32", "203.143.184.13/32", "203.143.186.22/32"]
    description = "TechM - Pune HJ  NAT IPs"
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["203.143.188.11/32", "203.143.184.13/32", "203.143.186.22/32"]
    description = "TechM - Pune HJ NAT IPs"
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["202.67.5.150/32", "202.67.5.117/32"]
    description = "TechM - Pune Sharda NAT IPs"
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["202.67.5.150/32", "202.67.5.117/32"]
    description = "TechM - Pune Sharda  NAT IPs"
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["202.67.5.150/32", "202.67.5.117/32"]
    description = "TechM - Pune Sharda NAT IPs"
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["119.151.4.34/32", "119.151.5.60/32", "119.151.20.21/32"]
    description = "TechM - Hyd NAT IPs"
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["119.151.4.34/32", "119.151.5.60/32", "119.151.20.21/32"]
    description = "TechM - Hyd  NAT IPs"
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["119.151.4.34/32", "119.151.5.60/32", "119.151.20.21/32"]
    description = "TechM - Hyd NAT IPs"
  }

  vpc_id = "${aws_vpc.default.id}"

  tags {
    Name = "${var.project}-wfh-sg"
  }
}

resource "aws_security_group" "bitbucket_ipsv4_sg" {
  name        = "bitbucket-ipsv4-sg"
  description = "Bitbucket ipsv4 security group"
  vpc_id      = "${aws_vpc.default.id}"

  lifecycle {
    create_before_destroy = true
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["${local.bitbucket_ipsv4}"]
    description = "bitbucket.org public IPs"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "${var.project}-bitbucket-ipv4-sg"
  }
}

resource "aws_security_group" "bitbucket_ipsv6_sg" {
  name        = "bitbucket-ipsv6-sg"
  description = "Bitbucket ipsv6 security group"
  vpc_id      = "${aws_vpc.default.id}"

  lifecycle {
    create_before_destroy = true
  }

  ingress {
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    ipv6_cidr_blocks = ["${local.bitbucket_ipsv6}"]
    description      = "bitbucket.org public IPs"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "${var.project}-bitbucket-ipv6-sg"
  }
}

resource "aws_security_group" "megafone" {
  name        = "megafone"
  description = "Megafone security group"
  vpc_id      = "${aws_vpc.default.id}"

  lifecycle {
    create_before_destroy = true
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["${aws_eip.nat-eip.public_ip}/32"]
    description = "Private Network NAT IP"
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${aws_eip.nat-eip.public_ip}/32"]
    description = "Private Network NAT IP for Ansible"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "${var.project}-megafone-ipv4-sg"
  }
}

/*
    NAT Instance
  */

resource "aws_instance" "nat" {
  ami                         = "${lookup(var.natami, var.aws_region)}" # this is a special ami preconfigured to do NAT
  availability_zone           = "${var.aws_region}a"
  instance_type               = "t2.micro"
  key_name                    = "${var.aws_key_name}"
  vpc_security_group_ids      = ["${aws_security_group.nat.id}"]
  subnet_id                   = "${aws_subnet.subnet-public.id}"
  associate_public_ip_address = true
  source_dest_check           = false

  tags {
    Name = "${var.project}-vpc-nat"
  }

  volume_tags {
    Name = "${var.project}-nat-vols"
  }

  root_block_device {
    volume_size           = "20"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_eip" "nat-eip" {
  instance = "${aws_instance.nat.id}"
  vpc      = true

  tags {
    Name = "${var.project}-nat-eip"
  }
}

/*
  Public Subnet
*/
resource "aws_subnet" "subnet-public" {
  vpc_id = "${aws_vpc.default.id}"

  cidr_block        = "${var.public_subnet_cidr}"
  availability_zone = "${var.aws_region}a"

  tags {
    Name = "${var.project}-Public-Subnet"
  }
}

resource "aws_route_table" "route-public" {
  vpc_id = "${aws_vpc.default.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.default.id}"
  }

  tags {
    Name = "${var.project}-Public-Subnet"
  }
}

resource "aws_route_table_association" "route-ass-public" {
  subnet_id      = "${aws_subnet.subnet-public.id}"
  route_table_id = "${aws_route_table.route-public.id}"
}

/*
  Private Subnet
*/
resource "aws_subnet" "subnet-private" {
  vpc_id = "${aws_vpc.default.id}"

  cidr_block        = "${var.private_subnet_cidr}"
  availability_zone = "${var.aws_region}a"

  tags {
    Name = "${var.project}-Private-Subnet"
  }
}

resource "aws_route_table" "route-private" {
  vpc_id = "${aws_vpc.default.id}"

  route {
    cidr_block  = "0.0.0.0/0"
    instance_id = "${aws_instance.nat.id}"
  }

  tags {
    Name = "${var.project}-Private-Subnet"
  }
}

resource "aws_route_table_association" "route-ass-private" {
  subnet_id      = "${aws_subnet.subnet-private.id}"
  route_table_id = "${aws_route_table.route-private.id}"
}

resource "aws_route53_zone_association" "private_zone" {
  zone_id = "${var.zone_id_priv}"
  vpc_id  = "${aws_vpc.default.id}"
}
