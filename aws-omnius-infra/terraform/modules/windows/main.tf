resource "aws_instance" "windowsterminal" {
  ami                         = "${var.ami_win}"
  instance_type               = "${var.instance_type}"
  key_name                    = "${var.key_name}"
  vpc_security_group_ids      = ["${var.security_group_wfh}", "${var.security_group_default}"]
  subnet_id                   = "${var.subnet_id}"
  associate_public_ip_address = "true"
  iam_instance_profile        = "${var.ec2-ssm-role-profile}"

  ebs_block_device {
    device_name           = "/dev/sdb"
    volume_size           = "${var.ebs_volume_size}"
    volume_type           = "gp2"
    delete_on_termination = true
  }

  tags {
    Name        = "${var.project}-${var.win_name}"
    Environment = "${var.environment}"
    Project     = "${var.project}"
    Role        = "Windows Terminal Server"
    hostname    = "${var.win_name}"
  }

  volume_tags {
    Name        = "${var.project}-${var.win_name}-vols"
    Environment = "${var.environment}"
    Project     = "${var.project}"
    Role        = "Windows Terminal Server"
  }

  root_block_device {
    volume_size           = "${var.root_vol_size}"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_eip" "windows-eip" {
  count    = "${var.eip == "no" ? 0 : 1}"
  instance = "${aws_instance.windowsterminal.id}"
  vpc      = true

  tags {
    Name        = "${var.project}-${var.win_name}-eip"
    Environment = "${var.environment}"
    Project     = "${var.project}"
    Role        = "Windows Terminal Server"
  }
}

resource "aws_route53_record" "windowsterminal_record" {
  count   = "${var.eip == "no" ? 0 : 1}"
  zone_id = "${var.zone_id}"
  name    = "${var.win_name}.${var.zone_name}"
  type    = "A"
  ttl     = "300"
  records = ["${aws_eip.windows-eip.public_ip}"]
}

resource "aws_ssm_association" "add_windows_terminal_to_domain" {
  count       = "${var.ssm == "no" ? 0 : 1}"
  name        = "${var.domain_name}"
  instance_id = "${aws_instance.windowsterminal.id}"
}
