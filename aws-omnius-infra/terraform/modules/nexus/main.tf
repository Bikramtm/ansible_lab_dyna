data "template_file" "s3-bucket-policy" {
  template = "${file("${path.module}/policies/policy-s3-bucket.json.tpl")}"

  vars {
    bucket_name = "${var.s3-bucket}"
  }
}

resource "aws_s3_bucket" "bucket" {
  count  = "${var.create_s3 == "no" ? 0 : 1}"
  bucket = "${var.s3-bucket}"
  acl    = "private"

  tags = {
    Name        = "S3 Bucket for Nexus"
    Environment = "${var.project}"
  }
}

resource "aws_iam_role" "nexus-s3" {
  name               = "${var.project}-nexus-s3"
  assume_role_policy = "${file("${path.module}/policies/role-policy.json")}"
}

resource "aws_iam_policy" "nexus-s3-policy" {
  name        = "${var.project}-nexus-s3-policy"
  description = "Nexus S3 policy"
  policy      = "${data.template_file.s3-bucket-policy.rendered}"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_iam_policy_attachment" "nexus-s3-attach" {
  name       = "${var.project}-nexus-s3-attachment"
  roles      = ["${aws_iam_role.nexus-s3.name}"]
  policy_arn = "${aws_iam_policy.nexus-s3-policy.arn}"
}

resource "aws_iam_instance_profile" "nexus-s3-profile" {
  name = "${var.project}-nexus-s3-profile"
  role = "${aws_iam_role.nexus-s3.name}"
}

resource "aws_route53_record" "nexus_record" {
  count   = "${var.dns_record == "no" ? 0 : 1}"
  zone_id = "${var.zone_id}"
  name    = "${var.nexus_name}.${var.zone_name}"
  type    = "CNAME"
  ttl     = "300"
  records = ["${var.nginx_name}.${var.zone_name}"]
}

resource "aws_route53_record" "dockerhub_record" {
  count   = "${var.dns_record == "no" ? 0 : 1}"
  zone_id = "${var.zone_id}"
  name    = "${var.docker_name}.${var.zone_name}"
  type    = "CNAME"
  ttl     = "300"
  records = ["${var.nginx_name}.${var.zone_name}"]
}
