resource "aws_instance" "nexus" {
  ami                         = "${lookup(var.nexus-amis, var.aws_region)}"
  instance_type               = "t2.medium"
  key_name                    = "${var.key_name}"
  vpc_security_group_ids      = ["${var.security_group_default}"]
  subnet_id                   = "${var.subnet_id}"
  iam_instance_profile        = "${var.project}-nexus-s3-profile"
  associate_public_ip_address = "${var.ass_public_ip}"

  ebs_block_device {
    device_name           = "/dev/sdb"
    volume_size           = 200
    volume_type           = "gp2"
    delete_on_termination = true
  }

  tags {
    Name        = "${var.project}-nexus"
    Environment = "continuous-integration"
    Project     = "${var.project}"
    Role        = "Nexus"
  }

  volume_tags {
    Name        = "${var.project}-nexus-vol"
    Environment = "continuous-integration"
    Project     = "${var.project}"
    Role        = "volume"
  }

  lifecycle {
    ignore_changes = ["private_ip", "root_block_device", "ebs_block_device"]
  }

  root_block_device {
    volume_size           = "30"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_ebs_volume" "nexus-vol-2" {
  count             = "${var.nexus_extra_vol == "no" ? 0 : 1}"
  availability_zone = "${var.aws_region}a"
  size              = "${var.nexus_volume_size}"
  type              = "gp2"

  tags {
    Name        = "${var.project}-nexus-vol-2"
    Environment = "continuous-integration"
    Project     = "${var.project}"
    Role        = "volume"
  }
}

resource "aws_volume_attachment" "nexus-vol-2-attach" {
  count       = "${var.nexus_extra_vol == "no" ? 0 : 1}"
  device_name = "/dev/sdc"
  volume_id   = "${aws_ebs_volume.nexus-vol-2.id}"
  instance_id = "${aws_instance.nexus.id}"
}

resource "aws_ebs_volume" "nexus-vol-3" {
  count             = "${var.nexus_extra_vol == "no" ? 0 : 1}"
  availability_zone = "${var.aws_region}a"
  size              = "${var.nexus_volume_size}"
  type              = "gp2"

  tags {
    Name        = "${var.project}-nexus-vol-3"
    Environment = "continuous-integration"
    Project     = "${var.project}"
    Role        = "volume"
  }
}

resource "aws_volume_attachment" "nexus-vol-3-attach" {
  count       = "${var.nexus_extra_vol == "no" ? 0 : 1}"
  device_name = "/dev/sde"
  volume_id   = "${aws_ebs_volume.nexus-vol-3.id}"
  instance_id = "${aws_instance.nexus.id}"
}


resource "aws_eip" "nexus-eip" {
  count    = "${var.nexus_eip == "no" ? 0 : 1}"
  instance = "${aws_instance.nexus.id}"
  vpc      = true

  tags {
    Name = "${var.project}-nexus-eip"
  }
}

resource "aws_route53_record" "nexus_public_record" {
  count   = "${var.nexus_eip == "no" ? 0 : 1}"
  zone_id = "${var.zone_id}"
  name    = "${var.nginx_name}.${var.zone_name}"
  type    = "A"
  ttl     = "300"
  records = ["${aws_eip.nexus-eip.public_ip}"]
}
