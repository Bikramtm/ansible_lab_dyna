# Naming conventions

## Tags

Every Terraform instance will hold certain tags:

- **Environment**
  The name of the environment. Is it an acceptance or staging environment. This is a comma seperated list in case it is used for multiple environments

- **Name**
  The functional name of the instance that has been deployed

- **Project**
  Which project it is for

- **Role**
  The role of the server itself. This can be a comma seperated list if a server has multiple roles

## Name Types

| Name     | Description             |
| -------- | ----------------------- |
| aoi      | All in One              |
| mysql    | MySQL server            |
| web      | Webserver               |
| redis    | Redis cache             |
| rabbitmq | RabbitMQ instance       |
| java     | Java application server |
| mssql    | MsSQL server            |
| biztalk  | MS Biztalk server       |

## Examples

We have a first ST environment for a customer called MyCustomer. This server will be both webserver as database server at the same time

| Tag         | Value                      |
| ----------- | -------------------------- |
| Environment | staging-mycustomer-st1     |
| Name        | staging-mycystomer-st1-aio |
| Project     | staging-mycustomer         |
| Role        | webserver,database         |

We have a second ST environment for a customer called MyCustomer. This server will be a webserver

| Tag         | Value                      |
| ----------- | -------------------------- |
| Environment | staging-mycustomer-st2     |
| Name        | staging-mycystomer-st2-web |
| Project     | staging-mycustomer         |
| Role        | webserver                  |

We have a generic database server for MyCustomer which is shared accross all the ST environments

| Tag         | Value                                          |
| ----------- | ---------------------------------------------- |
| Environment | staging-mycustomer-st1, staging-mycystomer-st2 |
| Name        | staging-mycystomer-database                    |
| Project     | staging-mycustomer                             |
| Role        | database                                       |
