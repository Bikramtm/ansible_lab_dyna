variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "aws_key_name" {}
variable "aws_key_path" {}

variable "project" {
  description = "Created by terraform"
  default     = "staging-vznl"
}

variable "aws_region" {
  description = "EC2 Region for the VPC"
  default     = "eu-central-1"
}

variable "zone_name" {
  description = "DNS name for Zone"
  default     = "dynacommercelab.com"
}

variable "environment" {
  description = "Environment for the resource"
  default     = "staging"
}

#-------------------------------------------------------------------------------
# Zone ID to create existing zone
#-------------------------------------------------------------------------------

variable "zone_id" {
  description = "dynacommecelab.com ZONE ID"
  default     = "ZJKKGPVBFJK09"
}

variable "zone_id_priv" {
  description = "dynacommecelab.com ZONE ID"
  default     = "Z1EJMG1AOLMAH"
}

variable "ami_st" {
  type        = "map"
  description = "AMIs by region"

  default = {
    ap-southeast-1 = "ami-16a4fe6a"
    eu-central-1   = "ami-337be65c"
  }
}

variable "ami_st_ena" {
  type        = "map"
  description = "AMIs by region"

  default = {
    eu-central-1 = "ami-dd3c0f36"
  }
}

variable "st1_dns" {
  default = [
    "st1-retail-vznl.dynacommercelab.com",
    "st1-indirect-vznl.dynacommercelab.com",
    "st1-telesales-vznl.dynacommercelab.com",
    "st1-restapi-vznl.dynacommercelab.com",
    "st1-rabbitmq-vznl.dynacommercelab.com",
  ]
}

variable "st2_dns" {
  default = [
    "st2-retail-vznl.dynacommercelab.com",
    "st2-indirect-vznl.dynacommercelab.com",
    "st2-telesales-vznl.dynacommercelab.com",
    "st2-restapi-vznl.dynacommercelab.com",
    "st2-rabbitmq-vznl.dynacommercelab.com",
  ]
}

variable "st3_dns" {
  default = [
    "st3-retail-vznl.dynacommercelab.com",
    "st3-indirect-vznl.dynacommercelab.com",
    "st3-telesales-vznl.dynacommercelab.com",
    "st3-restapi-vznl.dynacommercelab.com",
    "st3-rabbitmq-vznl.dynacommercelab.com",
  ]
}

variable "st4_dns" {
  default = [
    "st4-retail-vznl.dynacommercelab.com",
    "st4-indirect-vznl.dynacommercelab.com",
    "st4-telesales-vznl.dynacommercelab.com",
    "st4-restapi-vznl.dynacommercelab.com",
    "st4-rabbitmq-vznl.dynacommercelab.com",
  ]
}

variable "st5_dns" {
  default = [
    "st5-retail-vznl.dynacommercelab.com",
    "st5-indirect-vznl.dynacommercelab.com",
    "st5-telesales-vznl.dynacommercelab.com",
    "st5-restapi-vznl.dynacommercelab.com",
    "st5-rabbitmq-vznl.dynacommercelab.com",
  ]
}

variable "st6_dns" {
  default = [
    "st6-retail-vznl.dynacommercelab.com",
    "st6-indirect-vznl.dynacommercelab.com",
    "st6-telesales-vznl.dynacommercelab.com",
    "st6-restapi-vznl.dynacommercelab.com",
    "st6-rabbitmq-vznl.dynacommercelab.com",
  ]
}

variable "st7_dns" {
  default = [
    "st7-retail-vznl.dynacommercelab.com",
    "st7-indirect-vznl.dynacommercelab.com",
    "st7-telesales-vznl.dynacommercelab.com",
    "st7-restapi-vznl.dynacommercelab.com",
    "st7-rabbitmq-vznl.dynacommercelab.com",
  ]
}