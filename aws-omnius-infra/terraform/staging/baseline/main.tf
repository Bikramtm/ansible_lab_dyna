provider "aws" {
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region     = "${var.aws_region}"
}

#### Backend for tfstate file
terraform {
  backend "s3" {
    bucket         = "dyna-terraform-state"
    key            = "staging-baseline.tfstate"
    region         = "eu-central-1"
    dynamodb_table = "terraform-lock"
  }
}

data "terraform_remote_state" "network" {
  backend = "s3"

  config {
    bucket = "dyna-terraform-state"
    key    = "network.tfstate"
    region = "eu-central-1"
  }
}

##### ST1 Terraform Resources #####
resource "aws_instance" "baseline_st1_aio" {
  ami                    = "${lookup(var.ami_st, var.aws_region)}"
  instance_type          = "t3.xlarge"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  key_name               = "${var.aws_key_name}"

  tags {
    Name        = "${var.project}-st1-aio"
    Environment = "${var.project}-st1"
    Project     = "${var.project}"
    Role        = "Webserver"
    Start       = "StartWeekDays"
    Stop        = "StopWeekDays"
    Role        = "webserver,database,rabbitmq,redis"
  }

  volume_tags {
    Name        = "${var.project}-st1-vol"
    Environment = "${var.project}-st1"
    Project     = "${var.project}"
    Role        = "volume"
  }

  root_block_device {
    volume_size           = "30"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_route53_record" "st1_dns_record" {
  count   = "${length(var.st1_dns)}"
  zone_id = "${var.zone_id}"
  name    = "${element(var.st1_dns, count.index)}"
  type    = "CNAME"
  ttl     = "300"
  records = ["nginx.${var.zone_name}"]
}

resource "aws_route53_record" "st1_dns_record_priv" {
  count   = "${length(var.st1_dns)}"
  zone_id = "${var.zone_id_priv}"
  name    = "${element(var.st1_dns, count.index)}"
  type    = "CNAME"
  ttl     = "300"
  records = ["nginx.${var.zone_name}"]
}
