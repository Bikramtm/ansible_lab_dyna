<?php
header('Access-Control-Allow-Origin: *');
header("Content-type:application/json");

$response = array(
    'links' => array(
    	'self' => 'http://st3-telesales-vznl.dynacommercelab.com/api/v1/address/validate'
    ),
	'data' => array(
	    array(
		    'type' => 'address',
		    'id' => '1',
		    'attributes' => array(
			    "street" => "Ghoerlepad",
                "houseNumber" => 59,
                "houseNumberAddition" => null,
                "postalCode" => "8638XY",
                "city" => "Nes (Dongeradeel)",
                "country" => "Netherlands",
                "locationId" => "4525"	
		    )
	    )
	)
);

echo json_encode($response);
