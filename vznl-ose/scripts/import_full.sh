#!/bin/bash

#SLACK_HOOK="https://hooks.slack.com/services/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"

#Determines if we need to slack the messages
MESSAGE_TO_SLACK=false

#Determines if this script will be run in the VFDE environment
#(if true then message will not be send towards slack)
VZNL_ENVIRONMENT=false

#Determines if slack messages needs to be send
DO_CURL=true

#Determines if a full backup of the database needs to be created before starting
CREATE_DATABASE_BACKUP=true

#Determines if archive logging needs to be created
CREATE_ARCHIVE_LOGGING=true

# Determines if the catalog needs to be purged at the start of the import
DO_PURGE_CATALOG=false

#Determines the import types wich needs to be imported
DO_IMPORT_PACKAGE_TYPES=true
DO_IMPORT_PACKAGE_CREATION_TYPES=true
DO_IMPORT_PRODUCTS=true
#DO_IMPORT_COMMUNICATION_VARIABLES=true
DO_IMPORT_CATEGORIES=true
DO_IMPORT_COMP_RULES=true
DO_IMPORT_SALES_RULES=true
DO_IMPORT_MULTIMAPPERS=true
#DO_IMPORT_BUNDLES=true
#DO_IMPORT_CAMPAIGNS=true
DO_IMPORT_VERSIONS=true
#DO_IMPORT_ACTIVITY_CODES=true
#DO_IMPORT_TYPE_SUBTYPE_COMBINATIONS=true

#Determines if a full reindex is needed after the script import
DO_REINDEX=true

#Determines if an export summary needs to be created after the import script completion
DO_EXPORT_SUMMARY=true

#Regex which is used for integer validation
INT_REG='^[0-9]+$'

#Determines if the specified stack needs to be imported
MOBILE=false
FIXED=false
#CAMPAIGNS=false
#BUNDLES=false
#REFERENCE=false
#COMMUNICATION_VARIABLES=false

#Determines if all stacks need to be imported
IMPORT_ALL_STACKS=false

#Move old import files from var/import
MOVE_OLD_FILES=true

#Execution id which is used for the sandbox execution
EXECUTION_ID=false

#Determines if SFTP needs to be used
USE_SFTP=false

#Determines if SFTP debug information needs to be shown
USE_SFTP_DEBUG=false

#Determines if the script needs to be silent (So that minimum messages will be send towards slack)
SILENT_MODE=true


FILES=(
    'Mobile_Package_Type.csv'
    'Mobile_PackageCreation.csv'
    'Mobile_Accessory.csv'
    'Mobile_Addon.csv'
    'Mobile_Device.csv'
    'Mobile_DeviceRC.csv'
    'Mobile_Promotion.csv'
    'Mobile_Subscription.csv'
    'Mobile_ProductFamily.csv'
    'Mobile_ProductVersion.csv'
    'Fixed_Package_Type.csv'
    'Fixed_PackageCreation.csv'
    'Fixed_Bundle_Products.csv'
    'Fixed_Gen_Products.csv'
    'Fixed_Int_Products.csv'
    'Fixed_Tel_Products.csv'
    'Fixed_TV_Products.csv'
    'Fixed_Promotions.csv'
    'Fixed_Multimapper_peal.csv'
    'Fixed_CategoryTree.csv'
    'Fixed_Categories.csv'
    'Fixed_Comp_Rules.csv'
    'Fixed_Sales_Rules.csv'
    'Fixed_ProductFamily.csv'
    'Fixed_ProductVersion.csv'
    'Fixed_Version.xml'
);

#DO NOT EDIT BELOW THIS LINE

#Determines the document root for the script to start from
DOCUMENT_ROOT="$(dirname "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )" )"

SAVE_PURGE_VALUE=$DO_PURGE_CATALOG

while getopts m:f:k:c:b:i:s:d:p:v:a:t:z:r:l:e:n:x: option
do
    case "${option}"
    in
        t) MESSAGE_TO_SLACK=${OPTARG};;
        v) IS_VZNL_ENVIRONMENT=${OPTARG};;
        p) PHP_PATH=${OPTARG};;
        d) DOCUMENT_ROOT=${OPTARG};;
        m) MOBILE=${OPTARG};;
        f) FIXED=${OPTARG};;
        k) CABLE=${OPTARG};;
        a) CABLE_ARTIFACT=${OPTARG}; DO_PURGE_CATALOG=false;;
        r) REFERENCE=${OPTARG};;
        c) CAMPAIGNS=${OPTARG};;
        b) BUNDLES=${OPTARG};;
        i) EXECUTION_ID=${OPTARG};;
        z) MOVE_OLD_FILES=${OPTARG};;
        s) USE_SFTP=${OPTARG}; DO_PURGE_CATALOG=false;; #when importing only a certain stack we don't need to clear the whole catalog
        l) LOGFILE=${OPTARG};;
        e) ERROR_LOGFILE=${OPTARG};;
        x) FLUSH_REDIS=${OPTARG};;
    esac
done

IMPORT_FOLDER=$DOCUMENT_ROOT/var/import
LOCAL_XML=$DOCUMENT_ROOT/app/etc/local.xml;
IMPORT_XML=$DOCUMENT_ROOT/app/etc/import.xml;

curl_command() {
if [ "$VZNL_ENVIRONMENT" == false ] && [ "$MESSAGE_TO_SLACK" == true ]; then
    curl -X POST --data-urlencode "payload={ \"channel\": \"#vfziggo_dev_st_alerts\", \"username\": \"VFZIGGO Import Bot\", \"icon_emoji\": \":cop:\", \"text\": $1}"  $SLACK_HOOK
else
    echo $1
fi
}

#@todo we should have an argument that can be passed to bypass checks and prompt for user input of data that can't be read automatically
#command -v redis-cli >/dev/null 2>&1 || { curl_command "\"redis-cli is required but it's not installed. Aborting.\""; exit 1; }
command -v xml2 >/dev/null 2>&1 || { curl_command "\"xml2 is required but it's not installed. Please hardcode MYSQL and REDIS connection parameters. Aborting.\""; exit 1; }
if $USE_SFTP; then
    command -v sshpass >/dev/null 2>&1 || { curl_command "\"sshpass is required but it's not installed. Aborting.\""; exit 1; }
fi

read_xml() {
    echo $(xml2 < $1 | grep -m 1 $2 | cut -d"=" -f2);
}

PHP_WHICH="$(which php)"
PHP=$PHP_PATH
if [ -z $PHP ]; then
    echo "Php binary path set to the value specified in import.xml"
    PHP_XML=$(read_xml $IMPORT_XML "config/php/path");
    if [ -n "$PHP_XML" ]; then
        PHP=$PHP_XML
    else
        echo "Php binary path invalid in import.xml, setting to default path"
        if [ -z $PHP_WHICH ]; then
            PHP=/usr/bin/php
        else
            PHP=$PHP_WHICH
        fi
    fi
fi

VZNL_ENVIRONMENT=$IS_VZNL_ENVIRONMENT
if [ -z $VZNL_ENVIRONMENT ]; then
    VZNL_ENVIRONMENT=false;
else
    echo "Vodafone environment detected"
    VZNL_ENVIRONMENT=true;
fi

if $MOBILE || $FIXED; then
    DO_REINDEX=true;
else
    DO_REINDEX=false;
fi

if  $MOBILE && $FIXED; then
    DO_PURGE_CATALOG=$SAVE_PURGE_VALUE;
else
    DO_PURGE_CATALOG=false;
fi

if  $MOBILE || $FIXED || $CAMPAIGNS || $REFERENCE; then
    IMPORT_ALL_STACKS=false;
fi

if $IMPORT_ALL_STACKS; then
    DO_REINDEX=true;
    DO_PURGE_CATALOG=$SAVE_PURGE_VALUE;
fi

START=$(date +%s)

redis_flush() {
    if [ $(command -v redis-cli) ]; then
        redis-cli -h $REDIS_SESSION_HOST -n $REDIS_SESSION_DATABASE flushdb && redis-cli -h $REDIS_CACHE_SERVER -n $REDIS_CACHE_DATABASE flushdb
    fi
}

failed_or_processed() {
    find $DOCUMENT_ROOT/var/log/ -name '*.log' -size 0 -print0 2>/dev/null | xargs -0 rm 2>/dev/null
    LOG_FILE=$DOCUMENT_ROOT/var/log/stack_import_$1_error-$DATE.log;
    if [ ! -f $LOG_FILE ]; then
        PASSED_IMPORTS+=("$2")
        PASSED_IMPORTS_STACK+=("$1")
    else
        FAILED_IMPORTS+=("$2")
        FAILED_IMPORTS_STACK+=("$1")
    fi
}

import_command()
{
    if [ -z ${EXECUTION_ID+x} ]; then
        EXECUTION_TEXT=""
    else
        EXECUTION_TEXT=" with executionId $EXECUTION_ID"
    fi
    if [ "$DO_CURL" == true ] && [ "$SILENT_MODE" == false ]; then curl_command "\"Importing $1 from file $2$EXECUTION_TEXT\""; fi
    $PHP $DOCUMENT_ROOT/shell/import.php --type $1 --file $2 --executionId $EXECUTION_ID --stack $3 --fullImport true
    failed_or_processed $3 $2
    if [ "$DO_CURL" == true ]; then curl_command "\"Imported $1 from file $2$EXECUTION_TEXT\""; fi
}

import_xml_command()
{
    if [ -z ${EXECUTION_ID+x} ]; then
        EXECUTION_TEXT=""
    else
        EXECUTION_TEXT=" with executionId $EXECUTION_ID"
    fi
    if [ "$DO_CURL" == true ] && [ "$SILENT_MODE" == false ]; then curl_command "\"Importing $1 from file $2$EXECUTION_TEXT\""; fi
    $PHP $DOCUMENT_ROOT/shell/import_xml.php --type $1 --file $2 --executionId $EXECUTION_ID --stack $3 --fullImport true
    failed_or_processed $3 $2
    if [ "$DO_CURL" == true ]; then curl_command "\"Imported $1 from file $2$EXECUTION_TEXT\""; fi
}

displaytime() {
  local T=$1
  local D=$((T/60/60/24))
  local H=$((T/60/60%24))
  local M=$((T/60%60))
  local S=$((T%60))
  (( $D > 0 )) && duration+=$(printf '%d d ' $D)
  (( $H > 0 )) && duration+=$(printf '%d h ' $H)
  (( $M > 0 )) && duration+=$(printf '%d m ' $M)
  (( $D > 0 || $H > 0 || $M > 0 )) && duration+=$(printf 'and ')
  duration+=$(printf '%d s\n' $S)
  echo $duration
}

indexer_mode() {
    echo "Setting mode $1 on all indexers"
    #@todo list of indexers should be dynamically loaded, not hardcoded
    $PHP $DOCUMENT_ROOT/shell/indexer.php --mode-$1 catalog_product_attribute --executionId $EXECUTION_ID
    $PHP $DOCUMENT_ROOT/shell/indexer.php --mode-$1 catalog_product_price --executionId $EXECUTION_ID
    $PHP $DOCUMENT_ROOT/shell/indexer.php --mode-$1 catalog_url --executionId $EXECUTION_ID
#   $PHP $DOCUMENT_ROOT/shell/indexer.php --mode-$1 catalog_product_flat --executionId $EXECUTION_ID
#   $PHP $DOCUMENT_ROOT/shell/indexer.php --mode-$1 catalog_category_flat --executionId $EXECUTION_ID
    $PHP $DOCUMENT_ROOT/shell/indexer.php --mode-$1 catalog_category_product --executionId $EXECUTION_ID
    $PHP $DOCUMENT_ROOT/shell/indexer.php --mode-$1 catalogsearch_fulltext --executionId $EXECUTION_ID
    $PHP $DOCUMENT_ROOT/shell/indexer.php --mode-$1 cataloginventory_stock --executionId $EXECUTION_ID
    $PHP $DOCUMENT_ROOT/shell/indexer.php --mode-$1 tag_summary --executionId $EXECUTION_ID
    $PHP $DOCUMENT_ROOT/shell/indexer.php --mode-$1 product_match_whitelist --executionId $EXECUTION_ID
    $PHP $DOCUMENT_ROOT/shell/indexer.php --mode-$1 product_match_defaulted --executionId $EXECUTION_ID
    $PHP $DOCUMENT_ROOT/shell/indexer.php --mode-$1 rule_match_combinations --executionId $EXECUTION_ID
    $PHP $DOCUMENT_ROOT/shell/indexer.php --mode-$1 product_match_services --executionId $EXECUTION_ID
    $PHP $DOCUMENT_ROOT/shell/indexer.php --mode-$1 product_match_removal_index --executionId $EXECUTION_ID
    $PHP $DOCUMENT_ROOT/shell/indexer.php --mode-$1 mixmatch_combinations --executionId $EXECUTION_ID
}

cd ~/
DATE=$(date +"%Y-%m-%d")

empty_stack() {
    if [ "$DO_CURL" == true ] && [ "$SILENT_MODE" == false ]; then curl_command "\"Emptying stack $1 for import with executionId $EXECUTION_ID\""; fi
    $PHP $DOCUMENT_ROOT/shell/emptyStack.php --stack $1 --date $DATE --executionId $EXECUTION_ID --no-reindex true
    if [ "$DO_CURL" == true ]; then curl_command "\"Stack $1 is now empty. Proceeding with import executionId $EXECUTION_ID\""; fi
}

create_symlinks() {
#$1 param is the STACK
    for FILE in ${FILES[@]}; do
        FILE_STACK=$(echo $FILE | cut -d"_" -f1)
        REST=$(echo $FILE | cut -d"_" -f2)
        #if _ is not in the file name we have a special case (Bundles.xml,Campaigns.xml)
        if [[ $FILE_STACK == $REST ]]; then
            FILE_STACK=$(echo $FILE | cut -d"s" -f1)
        fi
        if [[ ${1^^} == ${FILE_STACK^^} ]]; then
            FILE_NAME=$(echo $FILE | cut -d"." -f1)
            FILE_DATE=_"$(ls -p $IMPORT_FOLDER | grep -v / | grep -i $FILE_NAME | sort -n | head -1 | grep -Po '.*(?=\.)' | grep -o ................$)";
            FILE_EXT=$(echo $FILE | cut -d"." -f2)
            ln -s -f ${IMPORT_FOLDER}/${FILE_NAME}${FILE_DATE}.${FILE_EXT} ${IMPORT_FOLDER}/${FILE_NAME}.${FILE_EXT}
        fi
    done
}

#maybe prompt user input for MySQL and Redis params if xml2 is not available?
MYSQL_HOST=$(read_xml $LOCAL_XML "config/global/resources/default_setup/connection/host");
MYSQL_USERNAME=$(read_xml $LOCAL_XML "config/global/resources/default_setup/connection/username");
MYSQL_PASSWORD=$(read_xml $LOCAL_XML "config/global/resources/default_setup/connection/password");
MYSQL_DATABASE=$(read_xml $LOCAL_XML "config/global/resources/default_setup/connection/dbname");
REDIS_SESSION_HOST=$(read_xml $LOCAL_XML "config/global/redis_session/host");
REDIS_SESSION_DATABASE=$(read_xml $LOCAL_XML "config/global/redis_session/db");
REDIS_CACHE_SERVER=$(read_xml $LOCAL_XML "config/global/cache/backend_options/server");
REDIS_CACHE_DATABASE=$(read_xml $LOCAL_XML "config/global/cache/backend_options/database");

if $USE_SFTP; then
    SFTP_HOSTNAME=$(read_xml $IMPORT_XML "config/sftp/hostname");
    SFTP_PORT=$(read_xml $IMPORT_XML "config/sftp/port");
    SFTP_USERNAME=$(read_xml $IMPORT_XML "config/sftp/username");
    SFTP_PASSWORD=$(read_xml $IMPORT_XML "config/sftp/password");
    SFTP_PATH=$(read_xml $IMPORT_XML "config/sftp/path");

    if $USE_SFTP_DEBUG; then
        echo "SFTP Credentials:"
        echo "$SFTP_HOSTNAME"
        echo "$SFTP_PORT"
        echo "$SFTP_USERNAME"
        echo "$SFTP_PASSWORD"
        echo "$SFTP_PATH"
    fi

    if [ ! -z "$SFTP_HOSTNAME" -a "$SFTP_HOSTNAME" != " " ] &&
       [ ! -z "$SFTP_PORT" -a "$SFTP_PORT" != " " ] &&
       [ ! -z "$SFTP_USERNAME" -a "$SFTP_USERNAME" != " " ] &&
       [ ! -z "$SFTP_PASSWORD" -a "$SFTP_PASSWORD" != " " ] &&
       [ ! -z "$SFTP_PATH" -a "$SFTP_PATH" != " " ]; then

        if $USE_SFTP_DEBUG; then
            echo "Starting SFTP"
        fi

        SFTP_LOG_PATH="Log"
        SFTP_PROCESSED_PATH="Processed"
        SFTP_FAILED_PATH="Failed"
        if $MOVE_OLD_FILES; then
            mkdir -p $DOCUMENT_ROOT/var/import/old 2>/dev/null
            find . -type l -exec rm {} \  2>/dev/null

            #We get a permission denied if previous files already existed and therefore cannot move the files
            #(We need to pipe yes to the command)
            if $FIXED; then
                mv -f $DOCUMENT_ROOT/var/import/Fixed_*.csv $DOCUMENT_ROOT/var/import/old/ 2>/dev/null
                mv -f $DOCUMENT_ROOT/var/import/Fixed_*.xml $DOCUMENT_ROOT/var/import/old/ 2>/dev/null
                mv -f $DOCUMENT_ROOT/var/import/Fixed_*.json $DOCUMENT_ROOT/var/import/old/ 2>/dev/null
                mv -f $DOCUMENT_ROOT/var/import/Fixed_*.phar $DOCUMENT_ROOT/var/import/old/ 2>/dev/null
            fi
            if $MOBILE; then
                mv -f $DOCUMENT_ROOT/var/import/Mobile_*.csv $DOCUMENT_ROOT/var/import/old/ 2>/dev/null
                mv -f $DOCUMENT_ROOT/var/import/Mobile_*.xml $DOCUMENT_ROOT/var/import/old/ 2>/dev/null
                mv -f $DOCUMENT_ROOT/var/import/Mobile_*.json $DOCUMENT_ROOT/var/import/old/ 2>/dev/null
                mv -f $DOCUMENT_ROOT/var/import/Mobile_*.phar $DOCUMENT_ROOT/var/import/old/ 2>/dev/null
            fi
        fi

        stack_command() {
            STACK=$1;
            case "$STACK"
                in
                    Cable_Rules_Plugin) STACK_FOLDER='Cable';;
                    FN) STACK_FOLDER='Fixed';;
                    *) STACK_FOLDER=$STACK;;
            esac
            STACKS+=("$1");

            mkdir -p $DOCUMENT_ROOT/var/import/temp
            COMMAND="sshpass -p $SFTP_PASSWORD scp -oStrictHostKeyChecking=no -r -P $SFTP_PORT $SFTP_USERNAME@$SFTP_HOSTNAME:$SFTP_PATH$STACK_FOLDER/* $DOCUMENT_ROOT/var/import/temp";

            if $USE_SFTP_DEBUG; then
                echo "EXECUTED COMMAND FOR STACK FOLDER MOVING TO IMPORT FOLDER:" >&2 ;
                echo $COMMAND >&2 ;
            fi

            OUTPUT=`$COMMAND`

            if $USE_SFTP_DEBUG; then
                echo "COMMAND OUTPUT FOR STACK FOLDER MOVING TO IMPORT FOLDER:" >&2 ;
            fi

            echo $OUTPUT;

            if [ $? -eq 0 ]; then
                echo "Copied import files for $STACK stack from remote server";
            else
                if $USE_SFTP_DEBUG; then
                    echo "Cannot copy to remote server => COMMAND OUTPUT FOR STACK FOLDER MOVING TO IMPORT FOLDER:" >&2 ;
                    echo $OUTPUT >&2 ;
                fi

                echo "Could not copy files for $STACK stack from remote server -> command failed" >&2 ; exit 1;
            fi

            STACK_DATE="$(ls -p $DOCUMENT_ROOT/var/import/temp | grep -v / | sort -n | head -1 | grep -Po '.*(?=\.)' | grep -o ................$)"
            mv -f $DOCUMENT_ROOT/var/import/temp/* $DOCUMENT_ROOT/var/import
            rm -R $DOCUMENT_ROOT/var/import/temp

            if [ -n "$STACK_DATE" ]; then
                STACK_DATE="_$STACK_DATE"
            fi

            if [ -n "$STACK_DATE" ]; then
                if [ "$DO_CURL" == true ] && [ "$SILENT_MODE" == false ]; then curl_command "\"Creating symbolic links for the $STACK stack.\""; fi
                create_symlinks $STACK
                if [ "$DO_CURL" == true ]; then curl_command "\"Symbolic link creation finished for the $STACK stack.\""; fi
            fi
        }

        if $IMPORT_ALL_STACKS; then
            if $USE_SFTP_DEBUG; then
                echo "Enabling all stacks" >&2 ;
            fi

            MOBILE=true
            FIXED=true
#            CAMPAIGNS=true
#            REFERENCE=true

            if $USE_SFTP_DEBUG; then
                echo "Stacks enabled" >&2 ;
            fi
        fi

        if $MOBILE; then stack_command "Mobile" > $DOCUMENT_ROOT/var/log/stack_import_Mobile-$DATE.log 2> $DOCUMENT_ROOT/var/log/stack_import_Mobile-error-$DATE.log; fi
        if $FIXED; then stack_command "FN" > $DOCUMENT_ROOT/var/log/stack_import_Fixed-$DATE.log 2> $DOCUMENT_ROOT/var/log/stack_import_Fixed-error-$DATE.log; fi
#        if $CAMPAIGNS; then stack_command "Campaign" > $DOCUMENT_ROOT/var/log/stack_import_Campaigns-$DATE.log 2> $DOCUMENT_ROOT/var/log/stack_import_Campaigns_error-$DATE.log; fi
#        if $REFERENCE; then stack_command "Reference" > $DOCUMENT_ROOT/var/log/stack_import_Reference-$DATE.log 2> $DOCUMENT_ROOT/var/log/stack_import_Reference_error-$DATE.log; fi
    else
        echo curl_command "\"Missing SFTP parameters in app/etc/import.xml\"";
    fi
fi

if [ "$DO_CURL" == true ]; then curl_command "\"Starting import for $DOCUMENT_ROOT\""; fi
if [ "$DO_CURL" == true ] && [ "$SILENT_MODE" == false ]; then curl_command "\"Moving logs from yesterday in old folder of $DOCUMENT_ROOT\""; fi

if $CREATE_ARCHIVE_LOGGING; then
    if [ 0 -lt $(ls $DOCUMENT_ROOT/var/log/*.* 2>/dev/null | wc -w) ]; then
        LOG_DATE=$(date --date='-1 day' +"%Y-%m-%d");
        if [[ -e "$DOCUMENT_ROOT/var/log/old/$LOG_DATE" ]]; then
            LOG_DATE=$(date +"%Y-%m-%d");
        fi
        mkdir -p $DOCUMENT_ROOT/var/log/old/$LOG_DATE
        mv -f $DOCUMENT_ROOT/var/log/*.log $DOCUMENT_ROOT/var/log/old/$LOG_DATE
        mv -f $DOCUMENT_ROOT/var/log/*.gz $DOCUMENT_ROOT/var/log/old/$LOG_DATE
    fi
fi

if [ "$DO_CURL" == true ] && [ "$SILENT_MODE" == false ]; then curl_command "\"Catalog update of $DOCUMENT_ROOT started.\""; fi

if $CREATE_DATABASE_BACKUP; then
    if [ "$DO_CURL" == true ]; then curl_command "\"Creating DB backup of $DOCUMENT_ROOT.\""; fi
    echo "Starting full backup of $MYSQL_DATABASE"
    set -o pipefail

    if [[ ! -e "$DOCUMENT_ROOT/var/export" ]]; then
        mkdir "$DOCUMENT_ROOT/var/export"
    fi

    mysqldump -h$MYSQL_HOST -u$MYSQL_USERNAME  --default-character-set=utf8 -p$MYSQL_PASSWORD $MYSQL_DATABASE > "$DOCUMENT_ROOT/var/export/fullbackup_-$DATE-_-$MYSQL_DATABASE-.sql"
    DB_ARCHIVE="$DOCUMENT_ROOT/var/export/fullbackup_-$DATE-_-$MYSQL_DATABASE-.sql.gz";
    if [ -f $DB_ARCHIVE ]; then
        mv -f $DB_ARCHIVE $DB_ARCHIVE".old"
    fi
    gzip -f "$DOCUMENT_ROOT/var/export/fullbackup_-$DATE-_-$MYSQL_DATABASE-.sql" $DB_ARCHIVE
    if [ "$?" -eq 0 ]; then
        echo "Full backup created of $MYSQL_DATABASE"
    else
        echo "Could not create database backup of $MYSQL_DATABASE -> command failed" >&2 ; exit 1;
    fi
fi

cd $DOCUMENT_ROOT/shell

indexer_mode "manual"

if $DO_PURGE_CATALOG; then
    if [ "$DO_CURL" == true ]; then curl_command "\"Cleaning catalog data from DB of $DOCUMENT_ROOT.\""; fi
    $PHP $DOCUMENT_ROOT/shell/cleancatalogfromdatabase.php --executionId $EXECUTION_ID
else
    if $MOBILE; then
        empty_stack "Mobile";
    fi
    if $FIXED; then
        empty_stack "Fixed";
    fi
#    if $CAMPAIGNS; then
#        empty_stack "Campaign";
#    fi

#@todo investigate if this is still needed
#    $PHP $DOCUMENT_ROOT/shell/indexer.php --reindexall
fi

if $DO_IMPORT_PACKAGE_TYPES; then
    if $FIXED || $IMPORT_ALL_STACKS; then
        STACK="Fixed";
        import_command "packageTypes" "Fixed_Package_Type.csv" $STACK >> $DOCUMENT_ROOT/var/log/stack_import_$STACK-$DATE.log 2>> $DOCUMENT_ROOT/var/log/stack_import_$STACK-error-$DATE.log
    fi
    if $MOBILE || $IMPORT_ALL_STACKS; then
        STACK="Mobile";
        import_command "packageTypes" "Mobile_Package_Type.csv" $STACK >> $DOCUMENT_ROOT/var/log/stack_import_$STACK-$DATE.log 2>> $DOCUMENT_ROOT/var/log/stack_import_$STACK-error-$DATE.log
    fi
fi

if $DO_IMPORT_PACKAGE_CREATION_TYPES; then
        if $FIXED || $IMPORT_ALL_STACKS; then
            STACK="Fixed";
            import_command "packageCreationTypes" "Fixed_PackageCreation.csv" $STACK >> $DOCUMENT_ROOT/var/log/stack_import_$STACK-$DATE.log 2>> $DOCUMENT_ROOT/var/log/stack_import_$STACK-error-$DATE.log
        fi
        if $MOBILE || $IMPORT_ALL_STACKS; then
            STACK="Mobile";
            import_command "packageCreationTypes" "Mobile_PackageCreation.csv" $STACK >> $DOCUMENT_ROOT/var/log/stack_import_$STACK-$DATE.log 2>> $DOCUMENT_ROOT/var/log/stack_import_$STACK-error-$DATE.log
        fi
fi

if $DO_IMPORT_PRODUCTS; then
    if $FIXED || $IMPORT_ALL_STACKS; then
        STACK="Fixed";
        import_command "fixedProduct" "Fixed_Bundle_Products.csv" $STACK >> $DOCUMENT_ROOT/var/log/stack_import_$STACK-$DATE.log 2>> $DOCUMENT_ROOT/var/log/stack_import_$STACK-error-$DATE.log
        import_command "fixedProduct" "Fixed_Gen_Products.csv" $STACK >> $DOCUMENT_ROOT/var/log/stack_import_$STACK-$DATE.log 2>> $DOCUMENT_ROOT/var/log/stack_import_$STACK-error-$DATE.log
        import_command "fixedProduct" "Fixed_Int_Products.csv" $STACK >> $DOCUMENT_ROOT/var/log/stack_import_$STACK-$DATE.log 2>> $DOCUMENT_ROOT/var/log/stack_import_$STACK-error-$DATE.log
        import_command "fixedProduct" "Fixed_Tel_Products.csv" $STACK >> $DOCUMENT_ROOT/var/log/stack_import_$STACK-$DATE.log 2>> $DOCUMENT_ROOT/var/log/stack_import_$STACK-error-$DATE.log
        import_command "fixedProduct" "Fixed_TV_Products.csv" $STACK >> $DOCUMENT_ROOT/var/log/stack_import_$STACK-$DATE.log 2>> $DOCUMENT_ROOT/var/log/stack_import_$STACK-error-$DATE.log
        import_command "fixedProduct" "Fixed_Promotions.csv" $STACK >> $DOCUMENT_ROOT/var/log/stack_import_$STACK-$DATE.log 2>> $DOCUMENT_ROOT/var/log/stack_import_$STACK-error-$DATE.log
        import_command "productVersion" "Fixed_ProductVersion.csv" $STACK >> $DOCUMENT_ROOT/var/log/stack_import_$STACK-$DATE.log 2>> $DOCUMENT_ROOT/var/log/stack_import_$STACK-error-$DATE.log
        import_command "productFamily" "Fixed_ProductFamily.csv" $STACK >> $DOCUMENT_ROOT/var/log/stack_import_$STACK-$DATE.log 2>> $DOCUMENT_ROOT/var/log/stack_import_$STACK-error-$DATE.log
    fi
    if $MOBILE || $IMPORT_ALL_STACKS; then
        STACK="Mobile";
        import_command "mobileProduct" "Mobile_Subscription.csv" $STACK >> $DOCUMENT_ROOT/var/log/stack_import_$STACK-$DATE.log 2>> $DOCUMENT_ROOT/var/log/stack_import_$STACK-error-$DATE.log
        import_command "mobileProduct" "Mobile_Device.csv" $STACK >> $DOCUMENT_ROOT/var/log/stack_import_$STACK-$DATE.log 2>> $DOCUMENT_ROOT/var/log/stack_import_$STACK-error-$DATE.log
        import_command "mobileProduct" "Mobile_DeviceRC.csv" $STACK >> $DOCUMENT_ROOT/var/log/stack_import_$STACK-$DATE.log 2>> $DOCUMENT_ROOT/var/log/stack_import_$STACK-error-$DATE.log
        import_command "mobileProduct" "Mobile_Addon.csv" $STACK >> $DOCUMENT_ROOT/var/log/stack_import_$STACK-$DATE.log 2>> $DOCUMENT_ROOT/var/log/stack_import_$STACK-error-$DATE.log
        import_command "mobileProduct" "Mobile_Accessory.csv" $STACK >> $DOCUMENT_ROOT/var/log/stack_import_$STACK-$DATE.log 2>> $DOCUMENT_ROOT/var/log/stack_import_$STACK-error-$DATE.log
        import_command "mobileProduct" "Mobile_Promotion.csv" $STACK >> $DOCUMENT_ROOT/var/log/stack_import_$STACK-$DATE.log 2>> $DOCUMENT_ROOT/var/log/stack_import_$STACK-error-$DATE.log
        import_command "productVersion" "Mobile_ProductVersion.csv" $STACK >> $DOCUMENT_ROOT/var/log/stack_import_$STACK-$DATE.log 2>> $DOCUMENT_ROOT/var/log/stack_import_$STACK-error-$DATE.log
        import_command "productFamily" "Mobile_ProductFamily.csv" $STACK >> $DOCUMENT_ROOT/var/log/stack_import_$STACK-$DATE.log 2>> $DOCUMENT_ROOT/var/log/stack_import_$STACK-error-$DATE.log
    fi
fi

if $DO_IMPORT_CATEGORIES; then
    if $FIXED || $IMPORT_ALL_STACKS; then
        STACK="Fixed";
        #import_command "categories" "Fixed_Categories.csv" $STACK >> $DOCUMENT_ROOT/var/log/stack_import_$STACK-$DATE.log 2>> $DOCUMENT_ROOT/var/log/stack_import_$STACK-error-$DATE.log
        import_command "categoriesTree" "Fixed_CategoryTree.csv" $STACK >> $DOCUMENT_ROOT/var/log/stack_import_$STACK-$DATE.log 2>> $DOCUMENT_ROOT/var/log/stack_import_$STACK-error-$DATE.log
        import_command "categoriesTreeProducts" "Fixed_Categories.csv" $STACK >> $DOCUMENT_ROOT/var/log/stack_import_$STACK-$DATE.log 2>> $DOCUMENT_ROOT/var/log/stack_import_$STACK-error-$DATE.log
    fi
    if $MOBILE || $IMPORT_ALL_STACKS; then
        STACK="Mobile";
#        import_command "categories" "Mobile_Categories.csv" $STACK >> $DOCUMENT_ROOT/var/log/stack_import_$STACK-$DATE.log 2>> $DOCUMENT_ROOT/var/log/stack_import_$STACK-error-$DATE.log
#        import_command "categories" "Mobile_Device_Categories.csv" $STACK >> $DOCUMENT_ROOT/var/log/stack_import_$STACK-$DATE.log 2>> $DOCUMENT_ROOT/var/log/stack_import_$STACK-error-$DATE.log
    fi
    $PHP $DOCUMENT_ROOT/shell/indexer.php --reindex catalog_product_attribute --executionId $EXECUTION_ID
    $PHP $DOCUMENT_ROOT/shell/indexer.php --reindex catalog_product_price --executionId $EXECUTION_ID
    $PHP $DOCUMENT_ROOT/shell/indexer.php --reindex catalog_category_product --executionId $EXECUTION_ID
fi

if $DO_IMPORT_COMP_RULES; then
    if $FIXED || $IMPORT_ALL_STACKS; then
        STACK="Fixed";
        #commented old csv importing of FN comp rules
        import_command "productMatchRules" "Fixed_Comp_Rules.csv" $STACK >> $DOCUMENT_ROOT/var/log/stack_import_$STACK-$DATE.log 2>> $DOCUMENT_ROOT/var/log/stack_import_$STACK-error-$DATE.log
    fi
    if $MOBILE || $IMPORT_ALL_STACKS; then
        STACK="Mobile";
#        import_command "productMatchRules" "Mobile_Comp_Rules.csv" $STACK >> $DOCUMENT_ROOT/var/log/stack_import_$STACK-$DATE.log 2>> $DOCUMENT_ROOT/var/log/stack_import_$STACK-error-$DATE.log
#        import_command "productMatchRules" "Mobile_Comp_Rules_Article_Constraints.csv" $STACK >> $DOCUMENT_ROOT/var/log/stack_import_$STACK-$DATE.log 2>> $DOCUMENT_ROOT/var/log/stack_import_$STACK-error-$DATE.log
#        import_command "productMatchRules" "Mobile_Comp_Rules_Footnote.csv" $STACK >> $DOCUMENT_ROOT/var/log/stack_import_$STACK-$DATE.log 2>> $DOCUMENT_ROOT/var/log/stack_import_$STACK-error-$DATE.log
#        import_command "mixmatch" "Mobile_Mixmatch_Complete.csv" $STACK >> $DOCUMENT_ROOT/var/log/stack_import_$STACK-$DATE.log 2>> $DOCUMENT_ROOT/var/log/stack_import_$STACK-error-$DATE.log
    fi

fi
if $DO_IMPORT_SALES_RULES; then
    if $FIXED || $IMPORT_ALL_STACKS; then
        STACK="Fixed";
        import_command "fixedSalesRules" "Fixed_Sales_Rules.csv" $STACK >> $DOCUMENT_ROOT/var/log/stack_import_$STACK-$DATE.log 2>> $DOCUMENT_ROOT/var/log/stack_import_$STACK-error-$DATE.log
    fi
    if $MOBILE || $IMPORT_ALL_STACKS; then
        STACK="Mobile";
#        import_command "mobileSalesRules" "Mobile_Sales_Rules_PostPaid.csv" $STACK >> $DOCUMENT_ROOT/var/log/stack_import_$STACK-$DATE.log 2>> $DOCUMENT_ROOT/var/log/stack_import_$STACK-error-$DATE.log
#        import_command "mobileSalesRules" "Mobile_Sales_Rules_Campaigns.csv" $STACK >> $DOCUMENT_ROOT/var/log/stack_import_$STACK-$DATE.log 2>> $DOCUMENT_ROOT/var/log/stack_import_$STACK-error-$DATE.log
    fi
fi

if $DO_IMPORT_MULTIMAPPERS; then
    if $FIXED || $IMPORT_ALL_STACKS; then
        STACK="Fixed";
        import_command "multimapper" "Fixed_Multimapper_peal.csv" $STACK >> $DOCUMENT_ROOT/var/log/stack_import_$STACK-$DATE.log 2>> $DOCUMENT_ROOT/var/log/stack_import_$STACK-error-$DATE.log
    fi

#    if $MOBILE || $IMPORT_ALL_STACKS; then
#        STACK="Mobile";
#        import_command "multimapper" "Mobile_Multimapper_Kias.csv" $STACK >> $DOCUMENT_ROOT/var/log/stack_import_$STACK-$DATE.log 2>> $DOCUMENT_ROOT/var/log/stack_import_$STACK-error-$DATE.log
#        import_command "multimapper" "Mobile_Multimapper_SAP.csv" $STACK >> $DOCUMENT_ROOT/var/log/stack_import_$STACK-$DATE.log 2>> $DOCUMENT_ROOT/var/log/stack_import_$STACK-error-$DATE.log
#    fi
fi

#if $DO_IMPORT_CAMPAIGNS; then
#    if $CAMPAIGNS || $IMPORT_ALL_STACKS; then
#        STACK="Campaign";
#        import_command "campaignOffer" "Campaign_Offers.xml" $STACK >> $DOCUMENT_ROOT/var/log/stack_import_$STACK-$DATE.log 2>> $DOCUMENT_ROOT/var/log/stack_import_$STACK-error-$DATE.log
#        import_command "campaign" "Campaigns.xml" $STACK >> $DOCUMENT_ROOT/var/log/stack_import_$STACK-$DATE.log 2>> $DOCUMENT_ROOT/var/log/stack_import_$STACK-error-$DATE.log
#    fi
#fi

#if $DO_IMPORT_COMMUNICATION_VARIABLES; then
#    if $COMMUNICATION_VARIABLES || $IMPORT_ALL_STACKS; then
#        STACK="CommunicationVariables";
#        if [ "$DO_CURL" == true ] && [ "$SILENT_MODE" == false ]; then curl_command "\"Importing Communication Variable reference DB of $DOCUMENT_ROOT.\""; fi
#        import_command "communicationVariables" "CommunicationVariables.csv" $STACK >> $DOCUMENT_ROOT/var/log/stack_import_$STACK-$DATE.log 2>> $DOCUMENT_ROOT/var/log/stack_import_$STACK-error-$DATE.log
#    fi
#fi

if $DO_IMPORT_VERSIONS; then
#    if $MOBILE || $IMPORT_ALL_STACKS; then
#        STACK="Mobile";
#        import_command "logfile" "Mobile_Version.xml" $STACK >> $DOCUMENT_ROOT/var/log/stack_import_$STACK-$DATE.log 2>> $DOCUMENT_ROOT/var/log/stack_import_$STACK-error-$DATE.log
#    fi
#
    if $FIXED || $IMPORT_ALL_STACKS; then
        STACK="Fixed";
        import_command "logfile" "Fixed_Version.xml" $STACK >> $DOCUMENT_ROOT/var/log/stack_import_$STACK-$DATE.log 2>> $DOCUMENT_ROOT/var/log/stack_import_$STACK-error-$DATE.log
    fi
#
#    if $CAMPAIGNS || $IMPORT_ALL_STACKS; then
#        STACK="Campaign"
#        import_command "logfile" "Campaign_Version.xml" $STACK >> $DOCUMENT_ROOT/var/log/stack_import_$STACK-$DATE.log 2>> $DOCUMENT_ROOT/var/log/stack_import_$STACK-error-$DATE.log
#    fi
fi

#if $REFERENCE || $IMPORT_ALL_STACKS; then
#    STACK="Reference"
#    import_command "serviceProviders" "Reference_OSF_ServiceProvider.csv" $STACK >> $DOCUMENT_ROOT/var/log/stack_import_$STACK-$DATE.log 2>> $DOCUMENT_ROOT/var/log/stack_import_$STACK-error-$DATE.log
#    import_command "vodafoneShip2Stores" "Reference_Ship2Store_List.csv" $STACK >> $DOCUMENT_ROOT/var/log/stack_import_$STACK-$DATE.log 2>> $DOCUMENT_ROOT/var/log/stack_import_$STACK-error-$DATE.log
#    import_xml_command "activityCodes" "Reference_activityCodes.xml" $STACK >> $DOCUMENT_ROOT/var/log/stack_import_$STACK-$DATE.log 2>> $DOCUMENT_ROOT/var/log/stack_import_$STACK-error-$DATE.log
#    import_xml_command "typeSubtypeCombinations" "Reference_typeSubtypeCombinations.xml" $STACK >> $DOCUMENT_ROOT/var/log/stack_import_$STACK-$DATE.log 2>> $DOCUMENT_ROOT/var/log/stack_import_$STACK-error-$DATE.log
#    import_command "phoneBookKeywords" "Reference_Stichwort_list.csv" $STACK >> $DOCUMENT_ROOT/var/log/stack_import_$STACK-$DATE.log 2>> $DOCUMENT_ROOT/var/log/stack_import_$STACK-error-$DATE.log
#    import_command "phoneBookIndustries" "Reference_telefonbuch-branchenliste.csv" $STACK >> $DOCUMENT_ROOT/var/log/stack_import_$STACK-$DATE.log 2>> $DOCUMENT_ROOT/var/log/stack_import_$STACK-error-$DATE.log
#    import_command "optionTypes" "Reference_option_type.csv" $STACK >> $DOCUMENT_ROOT/var/log/stack_import_$STACK-$DATE.log 2>> $DOCUMENT_ROOT/var/log/stack_import_$STACK-error-$DATE.log
#fi

if $DO_REINDEX; then
    if [ "$DO_CURL" == true ]; then curl_command "\"Reindexing everything in Catalog DB of $DOCUMENT_ROOT.\""; fi
    $PHP $DOCUMENT_ROOT/shell/indexer.php --reindex rule_match_combinations --executionId $EXECUTION_ID
    $PHP $DOCUMENT_ROOT/shell/indexer.php --reindex product_match_whitelist --executionId $EXECUTION_ID
    $PHP $DOCUMENT_ROOT/shell/indexer.php --reindex product_match_defaulted --executionId $EXECUTION_ID
    $PHP $DOCUMENT_ROOT/shell/indexer.php --reindex product_match_services --executionId $EXECUTION_ID
    $PHP $DOCUMENT_ROOT/shell/indexer.php --reindex product_match_removal_index --executionId $EXECUTION_ID
    $PHP $DOCUMENT_ROOT/shell/indexer.php --reindex mixmatch_combinations --executionId $EXECUTION_ID

fi

if [ "$DO_CURL" == true ] && [ "$SILENT_MODE" == false ]; then curl_command "\"Resetting log file permissions in $DOCUMENT_ROOT/var/log.\""; fi
chmod 777 -R $DOCUMENT_ROOT/var/log/*

indexer_mode "realtime"

if [ "$DO_CURL" == true ]; then curl_command "\"Flushing Redis Session DB $REDIS_SESSION_DATABASE and Redis Cache DB $REDIS_CACHE_DATABASE.\""; fi
redis_flush

if $DO_EXPORT_SUMMARY; then
    if [ "$DO_CURL" == true ]; then curl_command "\"Generating summary report of $DOCUMENT_ROOT Catalog Update.\""; fi
    EXPORT_DATE=$(date --date='-12 hours' +"%Y-%m-%d %H:%M:%S");
    $PHP $DOCUMENT_ROOT/shell/export.php --type summary --startDate "$EXPORT_DATE" --executionId $EXECUTION_ID;
fi

if [[ $EXECUTION_ID =~ $INT_REG ]] ; then
    $PHP $DOCUMENT_ROOT/shell/export.php --type executionImport --executionId $EXECUTION_ID;
fi

if $USE_SFTP; then

if [ ! -z "$SFTP_HOSTNAME" -a "$SFTP_HOSTNAME" != " " ] &&
   [ ! -z "$SFTP_PORT" -a "$SFTP_PORT" != " " ] &&
   [ ! -z "$SFTP_USERNAME" -a "$SFTP_USERNAME" != " " ] &&
   [ ! -z "$SFTP_PASSWORD" -a "$SFTP_PASSWORD" != " " ] &&
   [ ! -z "$SFTP_PATH" -a "$SFTP_PATH" != " " ]; then

        if $USE_SFTP_DEBUG; then
            echo "Starting SFTP"
        fi
		echo $FAILED_IMPORTS;

        for i in "${!FAILED_IMPORTS[@]}"; do
            FILE="${FAILED_IMPORTS[$i]}";
            STACK="${FAILED_IMPORTS_STACK[$i]}";
            if [[ "$FILE" == "" ]]; then break; fi

            case "$STACK"
                in
                    Mobile)
                        STACK_DATE=_"$(ls -p $DOCUMENT_ROOT/var/import | grep -v / | grep -i 'mobile' | sort -n | head -1 | grep -Po '.*(?=\.)' | grep -o ................$)"
                    ;;
                    Fixed)
                        STACK_DATE=_"$(ls -p $DOCUMENT_ROOT/var/import | grep -v / | grep -i 'fn' | sort -n | head -1 | grep -Po '.*(?=\.)' | grep -o ................$)"
                    ;;
#                    Campaigns)
#                        STACK_DATE=$CAMPAIGNS_STACK_DATE
#                    ;;
#                    Reference)
#                        STACK_DATE=$REFERENCE_STACK_DATE
#                    ;;
            esac

            IFS=. read IMPORT_FILE IMPORT_EXTENSION <<< $FILE
            FILE_TO_MOVE=$IMPORT_FILE$STACK_DATE.$IMPORT_EXTENSION;

            COMMAND="sshpass -p $SFTP_PASSWORD scp -oStrictHostKeyChecking=no -r -P $SFTP_PORT $SFTP_USERNAME@$SFTP_HOSTNAME:$SFTP_PATH$STACK/$FILE_TO_MOVE $SFTP_PATH$SFTP_FAILED_PATH/";

            if $USE_SFTP_DEBUG; then
                echo "TO BE EXECUTED COMMAND FOR FILE TO BE MOVED TO FAILED FOLDER:" >&2 ;
                echo $COMMAND >&2 ;

                OUTPUT=`$COMMAND`
                echo "COMMAND OUTPUT FOR FILE TO BE MOVED TO FAILED FOLDER:" >&2 ;
                echo $OUTPUT >&2 ;
            else
                OUTPUT=`$COMMAND`
                echo $OUTPUT;
            fi

            if [ $? -eq 0 ]; then
                echo "Moved $FILE from stack $STACK to Failed Folder";
            else
                if $USE_SFTP_DEBUG; then
                    echo "Moving $FILE from stack $STACK to Failed Folder -> command failed => COMMAND OUTPUT FOR FILE TO BE MOVED TO FAILED FOLDER:" >&2 ;
                    echo $OUTPUT >&2 ;
                fi

                echo "Moving $FILE from stack $STACK to Failed Folder -> command failed -> command failed" >&2 ; exit 1;
            fi
        done

        for i in "${!PASSED_IMPORTS[@]}"; do
            FILE="${PASSED_IMPORTS[$i]}";
            STACK="${PASSED_IMPORTS_STACK[$i]}";
            if [[ "$FILE" == "" ]]; then break; fi

            case "$STACK"
                in
                    Mobile)
                        STACK_DATE=_"$(ls -p $DOCUMENT_ROOT/var/import | grep -v / | grep -i 'mobile' | sort -n | head -1 | grep -Po '.*(?=\.)' | grep -o ................$)"
                    ;;
                    Fixed)
                        STACK_DATE=_"$(ls -p $DOCUMENT_ROOT/var/import | grep -v / | grep -i 'fn' | sort -n | head -1 | grep -Po '.*(?=\.)' | grep -o ................$)"
                    ;;
#                    Campaigns)
#                        STACK_DATE=$CAMPAIGNS_STACK_DATE
#                    ;;
#                    Reference)
#                        STACK_DATE=$REFERENCE_STACK_DATE
#                    ;;
            esac

            IFS=. read IMPORT_FILE IMPORT_EXTENSION <<< $FILE
            FILE_TO_MOVE=$IMPORT_FILE$STACK_DATE.$IMPORT_EXTENSION;

            COMMAND="sshpass -p $SFTP_PASSWORD scp -oStrictHostKeyChecking=no -r -P $SFTP_PORT $SFTP_USERNAME@$SFTP_HOSTNAME:$SFTP_PATH$STACK/$FILE_TO_MOVE $SFTP_PATH$SFTP_PROCESSED_PATH/";

            if $USE_SFTP_DEBUG; then
                echo "TO BE EXECUTED COMMAND FOR FILE TO BE MOVED TO PROCESSED FOLDER:" >&2 ;
                echo $COMMAND >&2 ;

                OUTPUT=`$COMMAND`
                echo "COMMAND OUTPUT FOR FILE TO BE MOVED TO PROCESSED FOLDER:" >&2 ;
                echo $OUTPUT >&2 ;
            else
                OUTPUT=`$COMMAND`
                echo $OUTPUT;
            fi

            if [ $? -eq 0 ]; then
                echo "Moved $FILE from stack $STACK to Processed Folder";
            else
                if $USE_SFTP_DEBUG; then
                    echo "Moving $FILE from stack $STACK to Processed Folder -> command failed => COMMAND OUTPUT FOR FILE TO BE MOVED TO PROCESSED FOLDER:" >&2 ;
                    echo $OUTPUT >&2 ;
                fi

                echo "Moving $FILE from stack $STACK to Processed Folder -> command failed -> command failed" >&2 ; exit 1;
            fi
        done

        if [ 0 -lt $(ls $DOCUMENT_ROOT/var/log/*.log 2>/dev/null | wc -w) ]; then
            COMMAND="sshpass -p $SFTP_PASSWORD scp -oStrictHostKeyChecking=no -r -P $SFTP_PORT $SFTP_USERNAME@$SFTP_HOSTNAME:$DOCUMENT_ROOT/var/log/*.log $SFTP_PATH$SFTP_LOG_PATH/";

            if $USE_SFTP_DEBUG; then
                echo "TO BE EXECUTED COMMAND FOR LOGFILES TO BE MOVED TO LOG FOLDER:" >&2 ;
                echo $COMMAND >&2 ;

                OUTPUT=`$COMMAND`
                echo "COMMAND OUTPUT FOR LOGFILES TO BE MOVED TO LOG FOLDER:" >&2 ;
                echo $OUTPUT >&2 ;
            else
                OUTPUT=`$COMMAND`
                echo $OUTPUT;
            fi

            if [ $? -eq 0 ]; then
                echo "Moved logfiles from import to Log Folder";
            else
                if $USE_SFTP_DEBUG; then
                    echo "Moved logfiles from import to Log Folder -> command failed => COMMAND OUTPUT FOR LOGFILES TO BE MOVED TO LOG FOLDER:" >&2 ;
                    echo $OUTPUT >&2 ;
                fi

                echo "Moved logfiles from import to Log Folder -> command failed -> command failed" >&2 ; exit 1;
            fi
        fi
    fi
fi

if [ "$DO_CURL" == true ] && [ "$SILENT_MODE" == false ]; then curl_command "\"Catalog update of $DOCUMENT_ROOT finished.\""; fi

file="$DOCUMENT_ROOT/media/APP_VERSION.php";
if [ -f $file ]; then
   rm $file;
fi

END=$(date +%s)
echo $(date +"<?php return '|%m-%d|%H-%M';") > "$DOCUMENT_ROOT/media/APP_VERSION.php"
if [ "$DO_CURL" == true ] && [ "$SILENT_MODE" == false ]; then curl_command "\"APP version updated for catalog of $DOCUMENT_ROOT.\""; fi
if [ "$DO_CURL" == true ]; then curl_command "\"Finished import for $DOCUMENT_ROOT in $(displaytime $(($END - $START)))\""; fi
if $MOVE_OLD_FILES; then
    mkdir -p $DOCUMENT_ROOT/var/import/old 2>/dev/null
    find . -type l -exec rm {} \  2>/dev/null

    #We get a permission denied if previous files already existed and therefore cannot move the files
    #(We need to pipe yes to the command)
    if $FIXED; then
        mv -f $DOCUMENT_ROOT/var/import/Fixed_*.csv $DOCUMENT_ROOT/var/import/old/ 2>/dev/null
        mv -f $DOCUMENT_ROOT/var/import/Fixed_*.xml $DOCUMENT_ROOT/var/import/old/ 2>/dev/null
        mv -f $DOCUMENT_ROOT/var/import/Fixed_*.json $DOCUMENT_ROOT/var/import/old/ 2>/dev/null
        mv -f $DOCUMENT_ROOT/var/import/Fixed_*.phar $DOCUMENT_ROOT/var/import/old/ 2>/dev/null
    fi
    if $MOBILE; then
        mv -f $DOCUMENT_ROOT/var/import/Mobile_*.csv $DOCUMENT_ROOT/var/import/old/ 2>/dev/null
        mv -f $DOCUMENT_ROOT/var/import/Mobile_*.xml $DOCUMENT_ROOT/var/import/old/ 2>/dev/null
        mv -f $DOCUMENT_ROOT/var/import/Mobile_*.json $DOCUMENT_ROOT/var/import/old/ 2>/dev/null
        mv -f $DOCUMENT_ROOT/var/import/Mobile_*.phar $DOCUMENT_ROOT/var/import/old/ 2>/dev/null
    fi
fi
