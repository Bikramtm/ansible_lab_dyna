<?php
require_once 'repairDbClass.php';

/*
 * Remove a store view from the DB with code + all associated data and perhaps archive option
 * */
class Tools_Db_Repair_Stores extends Tools_Db_Repair
{
    const DAYS_LIMIT = 100;

    /**
     * Initialize application and parse input parameters
     */
    public function __construct()
    {
        parent::__construct();
        $this->logFile = "repairdb_fix_stores_".date('Y_m_d').".log";
    }

    /**
     * Run script
     */
    public function run()
    {
        $this->log("Start script.");
        $this->log($this->getReadOnly());

        $actions   = [];
        $dateLimit = date('Y-m-d 00:00:00', strtotime("-".self::DAYS_LIMIT." days"));
        $stores    = $this->readConnection->fetchAll("SELECT * FROM `core_store` WHERE `is_active` = 1 AND `code` NOT IN ('admin', 'default')");

        foreach ($stores as $store) {
            $quotes = $this->readConnection
                ->fetchRow("SELECT entity_id FROM `sales_flat_quote` WHERE `store_id` = :storeId AND `updated_at` >= ':updated' LIMIT 1",
                    array(
                        ':storeId' => $store['store_id'],
                        ':updated' => $dateLimit
                    ));

            $orders = $this->readConnection
                ->fetchRow("SELECT entity_id FROM `sales_flat_order` WHERE `store_id` = :storeId AND `updated_at` >= ':updated' LIMIT 1",
                    array(
                        ':storeId' => $store['store_id'],
                        ':updated' => $dateLimit
                    ));

            if (!$quotes && !$orders) {
                $actions[] = $store['store_id'];
                $this->log("Found only old records for {$store['name']}; store({$store['store_id']}) marked as inactive.");
            }
        }

        // if in readonly mode, just return the required actions
        if ($this->getReadOnly() == true) {
            $this->log("Stop script.");
            return $actions;
        }

        // update db records
        foreach ($actions as $storeId) {
            $sql = "UPDATE `core_store` SET `is_active` = 0 WHERE `store_id` = :storeId";
            $bind = array(':storeId' => $storeId);
            $this->log("Running: ".$sql);
            $this->writeConnection->exec($sql, $bind);
        }

        $this->log("Stop script.");
        return $this;
    }
}
