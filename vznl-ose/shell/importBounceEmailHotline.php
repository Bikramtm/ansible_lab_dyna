<?php
/**
 * Copyright (c) 2017. Dynacommerce .
 */

require_once 'abstract.php';

class Dyna_ImportBounceEmail_Hotline extends Mage_Shell_Abstract
{

    protected $_normalText  = "\033[32m";
    protected $_boldText    = "\033[36m";
    protected $_whiteText   = "\033[0m";
    protected $_failedText  = "\033[31m";
    protected $_successMsg;
    protected $_errorMsg;
    protected $_xmlPath;
    protected $_xml;

    protected function _importXml($importXml)
    {
        $groupData = Mage::getResourceModel('agent/dealergroup_collection')
        ->addFieldToFilter('name', $importXml->DealerGroupName)->getData();
        $groupId = $groupData[0]['group_id'];
        $groupDealerData = Mage::getResourceModel('agent/dealer_collection')
        ->getDealersByGroups($groupId)->getData();

    	if(isset($importXml->HotlineNumber)){
    	    $hotlineNumber = $importXml->HotlineNumber->CountryCode . '/' . $importXml->HotlineNumber->PhoneNumber;
        }

        if(isset($importXml->BounceEmailAddress)){
    	    $bounceEmail = $importXml->BounceEmailAddress;
        }

        if(isset($bounceEmail) || isset($hotlineNumber)){

            echo $this->_normalText .
            'Importing Bouncing Email and Hotline Number for ' .
            $this->_boldText .
            (string)$importXml->DealerGroupName .
            $this->_normalText .
            ' ... ';

            foreach ($groupDealerData as $dealersData) {
                $model = Mage::getModel("agent/dealer")->load($dealersData['dealer_id']);
                $model->setHotlineNumber($hotlineNumber)
                ->setBounceInfoEmailAddress($bounceEmail);

                try {
                    $model->save();
                    echo $this->_normalText . "Done";
                    $this->_successMsg = ' === SUCCESS === '. 'Importing Bouncing Email and Hotline Number for ' . (string)$importXml->DealerGroupName . '... Done';
                    $this->_writelog($this->_successMsg);
                } catch (Exception $e) {
                    echo $this->_failedText . $e->getMessage();
                    $this->_errorMsg = ' === ERROR === '. $e->getMessage() . '... Failed';
                    $this->_writelog($this->_errorMsg);
                }
            }

            echo $this->_whiteText . "\n";

        }

        return $this;

    }

    protected function _writelog($message){
        Mage::log($message, null, "import-bounce-email-hotline-numbers.log");
    }

    public function run()
    {
        $this->_xmlPath = Mage::getBaseDir('var') . '/import/dealer-data/DealerInfos.xml';

        if(is_file($this->_xmlPath)){
            $this->_xml = simplexml_load_file($this->_xmlPath);
            foreach ($this->_xml->DealerInfo as $dealerInfoXml) {
                $this->_importXml($dealerInfoXml);
            }
        } else{
            $this->_errorMsg = ' === ERROR === '. 'Xml file not available on the path.';
            $this->_writelog($this->_errorMsg);
            echo $this->_failedText . $this->_errorMsg . $this->_normalText . "\n";
        }
    }
}

$import = new Dyna_ImportBounceEmail_Hotline();
$import->run();