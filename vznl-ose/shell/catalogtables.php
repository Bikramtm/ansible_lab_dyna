<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

require_once 'abstract.php';

/**
 * Class Dyna_CatalogTables_Cli
 */
abstract class Dyna_CatalogTables_Cli extends Mage_Shell_Abstract
{
    // Why are these separate from the CATALOGTABLES?
    const MULTIMAPPERTABLES = array(
        'dyna_multi_mapper',
        'dyna_multi_mapper_addons');

    const CATALOGTABLES = array(
        'agent_commision',
        'bundle_actions',
        'bundle_campaign',
        'bundle_campaign_dealercode',
        'bundle_campaign_offer',
        'bundle_campaign_offer_relation',
        'bundle_conditions',
        'bundle_rules',
        'bundle_hints',
        'bundle_process_context',
        'catalog_category_anc_categs_index_idx',
        'catalog_category_anc_categs_index_tmp',
        'catalog_category_anc_products_index_idx',
        'catalog_category_anc_products_index_tmp',
        'catalog_category_entity',
        'catalog_category_entity_datetime',
        'catalog_category_entity_decimal',
        'catalog_category_entity_int',
        'catalog_category_entity_text',
        'catalog_category_entity_varchar',
        'catalog_category_flat_store_1',
        'catalog_category_flat_store_2',
        'catalog_category_flat_store_3',
        'catalog_category_product',
        'catalog_category_product_index',
        'catalog_category_product_index_enbl_idx',
        'catalog_category_product_index_enbl_tmp',
        'catalog_category_product_index_idx',
        'catalog_category_product_index_tmp',
        'catalog_compare_item',
        'catalog_import_log',
        'catalog_eav_attribute',
        'catalog_mixmatch',
        'catalog_package_types',
        'catalog_package_subtypes',
        'catalog_product_bundle_option',
        'catalog_product_bundle_option_value',
        'catalog_product_bundle_price_index',
        'catalog_product_bundle_selection',
        'catalog_product_bundle_selection_price',
        'catalog_product_bundle_stock_index',
        'catalog_product_enabled_index',
        'catalog_product_entity',
        'catalog_product_entity_datetime',
        'catalog_product_entity_decimal',
        'catalog_product_entity_gallery',
        'catalog_product_entity_group_price',
        'catalog_product_entity_int',
        'catalog_product_entity_media_gallery',
        'catalog_product_entity_media_gallery_value',
        'catalog_product_entity_text',
        'catalog_product_entity_tier_price',
        'catalog_product_entity_varchar',
        'catalog_product_flat_1',
        'catalog_product_flat_2',
        'catalog_product_index_eav',
        'catalog_product_index_eav_decimal',
        'catalog_product_index_eav_decimal_idx',
        'catalog_product_index_eav_decimal_tmp',
        'catalog_product_index_eav_idx',
        'catalog_product_index_eav_tmp',
        'catalog_product_index_group_price',
        'catalog_product_index_price',
        'catalog_product_index_price_bundle_idx',
        'catalog_product_index_price_bundle_opt_idx',
        'catalog_product_index_price_bundle_opt_tmp',
        'catalog_product_index_price_bundle_sel_idx',
        'catalog_product_index_price_bundle_sel_tmp',
        'catalog_product_index_price_bundle_tmp',
        'catalog_product_index_price_cfg_opt_agr_idx',
        'catalog_product_index_price_cfg_opt_agr_tmp',
        'catalog_product_index_price_cfg_opt_idx',
        'catalog_product_index_price_cfg_opt_tmp',
        'catalog_product_index_price_downlod_idx',
        'catalog_product_index_price_downlod_tmp',
        'catalog_product_index_price_final_idx',
        'catalog_product_index_price_final_tmp',
        'catalog_product_index_price_idx',
        'catalog_product_index_price_opt_agr_idx',
        'catalog_product_index_price_opt_agr_tmp',
        'catalog_product_index_price_opt_idx',
        'catalog_product_index_price_opt_tmp',
        'catalog_product_index_price_tmp',
        'catalog_product_index_tier_price',
        'catalog_product_index_website',
        'catalog_product_link',
        'catalog_product_link_attribute',
        'catalog_product_link_attribute_decimal',
        'catalog_product_link_attribute_int',
        'catalog_product_link_attribute_varchar',
        'catalog_product_link_type',
        'catalog_product_option',
        'catalog_product_option_price',
        'catalog_product_option_title',
        'catalog_product_option_type_price',
        'catalog_product_option_type_title',
        'catalog_product_option_type_value',
        'catalog_product_relation',
        'catalog_product_super_attribute',
        'catalog_product_super_attribute_label',
        'catalog_product_super_attribute_pricing',
        'catalog_product_super_link',
        'catalog_product_website',
        'cataloginventory_stock',
        'cataloginventory_stock_item',
        'cataloginventory_stock_status',
        'cataloginventory_stock_status_idx',
        'cataloginventory_stock_status_tmp',
        'catalogrule',
        'catalogrule_affected_product',
        'catalogrule_customer_group',
        'catalogrule_group_website',
        'catalogrule_product',
        'catalogrule_product_price',
        'catalogrule_website',
        'catalogsearch_fulltext',
        'catalogsearch_query',
        'catalogsearch_result',
        'coupon_aggregated',
        'coupon_aggregated_order',
        'coupon_aggregated_updated',
        'downloadable_link',
        'downloadable_link_price',
        'downloadable_link_purchased',
        'downloadable_link_purchased_item',
        'downloadable_link_title',
        'downloadable_sample',
        'downloadable_sample_title',
        'dyna_customer_activity_codes',
        'dyna_customer_type_subtype_combinations',
        'dyna_mixmatch_flat',
        'dyna_multi_mapper_index_process_context',
        'eav_attribute',
        'eav_attribute_group',
        'eav_attribute_label',
        'eav_attribute_option',
        'eav_attribute_option_value',
        'eav_attribute_set',
        'eav_entity',
        'eav_entity_attribute',
        'eav_entity_datetime',
        'eav_entity_decimal',
        'eav_entity_int',
        'eav_entity_text',
        'eav_entity_type',
        'eav_entity_varchar',
        'eav_form_element',
        'eav_form_fieldset',
        'eav_form_fieldset_label',
        'eav_form_type',
        'eav_form_type_entity',
        'operator_service_providers',
        'package_creation_groups',
        'package_creation_types',
        'package_subtype_cardinality',
        'product_family',
        'product_match_rule',
        'product_match_defaulted_index',
        'product_match_mandatory_index',
        'product_match_rule_process_context',
        'product_match_rule_removal_not_allowed',
        'product_match_rule_source_target_index',
        'product_match_rule_website',
        'product_match_service_index',
        'product_match_whitelist_index',
        'product_version',
        'salesrule',
        'salesrule_coupon',
        'salesrule_coupon_usage',
        'salesrule_customer',
        'salesrule_customer_group',
        'salesrule_label',
        'salesrule_product_attribute',
        'salesrule_website',
        'sales_rule_product_match',
        'vodafone_ship2stores',
        'weee_discount',
        'weee_tax',
        'dyna_phone_book_industries',
        'dyna_phone_book_keywords',
        'product_match_whitelist_index_process_context_set',
        'checkout_option_type_referencetable',
    );

    const SKIPCATALOGTABLESFORCLEANUP = [
        'cataloginventory_stock',
        'catalog_eav_attribute',
        'eav_attribute',
        'eav_attribute_group',
        'eav_attribute_label',
        'eav_attribute_option',
        'eav_attribute_option_value',
        'eav_attribute_set',
        'eav_entity',
        'eav_entity_attribute',
        'eav_entity_datetime',
        'eav_entity_decimal',
        'eav_entity_int',
        'eav_entity_text',
        'eav_entity_type',
        'eav_entity_varchar',
        'eav_form_element',
        'eav_form_fieldset',
        'eav_form_fieldset_label',
        'eav_form_type',
        'eav_form_type_entity',
        'process_context',
        'provis_sales_id',
    ];

    const DEFAULT_CATEGORIES_QUERIES = [
        'INSERT INTO `catalog_category_entity` (`entity_id`, `entity_type_id`, `attribute_set_id`, `parent_id`, `created_at`, `updated_at`, `path`, `position`, `level`, `children_count`) VALUES (1, 3, 0, 0, \'2012-06-17 22:20:47\', \'2012-06-17 22:20:47\', \'1\', 0, 0, 1), (2, 3, 3, 1, \'2012-06-17 22:20:47\', \'2012-06-17 22:20:47\', \'1/2\', 1, 1, 0);',
        'INSERT INTO `catalog_category_entity_int` (`value_id`, `entity_type_id`, `attribute_id`, `store_id`, `entity_id`, `value`) VALUES (1, 3, 67, 0, 1, 1), (2, 3, 67, 1, 1, 1), (3, 3, 42, 0, 2, 1), (4, 3, 67, 0, 2, 1), (5, 3, 42, 1, 2, 1), (6, 3, 67, 1, 2, 1);',
        'INSERT INTO `catalog_category_entity_varchar` (`value_id`, `entity_type_id`, `attribute_id`, `store_id`, `entity_id`, `value`) VALUES (1, 3, 41, 0, 1, \'Root Catalog\'), (2, 3, 41, 1, 1, \'Root Catalog\'), (3, 3, 43, 1, 1, \'root-catalog\'), (4, 3, 41, 0, 2, \'Default Category\'), (5, 3, 41, 1, 2, \'Default Category\'), (6, 3, 49, 1, 2, \'PRODUCTS\'), (7, 3, 43, 1, 2, \'default-category\');'
    ];


    protected function getFullCatalog()
    {
        return array_merge(self::CATALOGTABLES, self::MULTIMAPPERTABLES);
    }

    /**
     * @param string $tableName
     *
     * @return mixed
     */
    protected function tableExist($tableName)
    {
        $result = self::getConnection()->isTableExists($tableName);
        return $result;
    }

    /**
     * @param string $tableName
     *
     * @return mixed
     */
    protected function truncateTable($tableName)
    {
        $result = self::getConnection()->truncateTable($tableName);
        return $result;
    }

    /**
     * @return mixed
     */
    protected function disableForeignKeyChecks()
    {
        $query = "SET foreign_key_checks = 0;";
        $result = self::executeSql($query);
        return $result;

    }

    /**
     * @return mixed
     */
    protected function enableForeignKeyChecks()
    {
        $query = "SET foreign_key_checks = 1;";
        $result = self::executeSql($query);
        return $result;
    }

    /**
     * @param string $msg
     */
    protected function _writeLine($msg)
    {
        echo $msg . PHP_EOL;
    }

    /**
     * Get database connection
     * @return mixed
     */
    protected static function getConnection()
    {
        return Mage::getSingleton('core/resource')
          ->getConnection(Mage_Core_Model_Resource::DEFAULT_WRITE_RESOURCE);
    }

    /**
     * Execute SQL query on the default database connection
     *
     * @param string $query
     *
     * @return mixed
     */
    protected static function executeSql($query)
    {
        return self::getConnection()->query($query);
    }
}
