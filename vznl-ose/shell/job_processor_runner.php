<?php

require_once 'abstract.php';

class Dyna_Job_Processor_Runner extends Mage_Shell_Abstract
{
    public function __construct()
    {
        $this->_appCode = 'telesales';
        $this->_parseArgs();

        if ($appCode = $this->getArg('appCode')) {
            $this->_appCode = $appCode;
        }

        parent::__construct();
    }

    /**
     * Run script
     *
     */
    public function run()
    {
        if ($this->getArg('help')) {
            $this->usageHelp();
            return;
        } else {
            $job = $this->getArg('job');
            if (!$job) {
                throw new Exception('No job provided');
            }

            $dbRepository = Mage::getModel('dyna_job/jobRepository_database');
            $content = $dbRepository->retrieve($job);
        }

        $standaloneJobRunner = Mage::helper('dyna_job')->buildStandaloneRunner();
        $jobRan = $standaloneJobRunner->run($content);
        if ($jobRan === Dyna_Job_Model_StandaloneJobRunner::JOB_PROCESSED) {
            $dbRepository->cleanup($job);
        }
    }

    /**
     * Retrieve Usage Help Message
     *
     */
    public function usageHelp()
    {
        return <<<USAGE
Usage:  php job_processor_runner.php -- [options]
   appCode                    Magento app code
   help                          This help

USAGE;
    }
}

$shell = new Dyna_Job_Processor_Runner();
$shell->run();
