<?php
require_once 'abstract.php';

class Dyna_VFDE_StartDaemons extends Mage_Shell_Abstract{


    /**
     * Starts the Job/Queue and SAP deamon when calling this script
     */

    CONST JOB_DAEMON_DIR = "omnius2-jobdaemon";
    CONST JOB_DAEMON_CONFIG = "Dynacommerce.JobQueueService.exe.config";
    CONST JOB_DAEMON_EXE = "Dynacommerce.JobQueueService.exe";

    CONST STATUS_DAEMON_DIR = "omnius2-statusdaemon";
    CONST STATUS_DAEMON_CONFIG = "Dynacommerce.StatusQueueService.exe.config";
    CONST STATUS_DAEMON_EXE = "Dynacommerce.StatusQueueService.exe";

    CONST SAP_DAEMON_DIR = "omnius2-sapdaemon";
    CONST SAP_DAEMON_CONFIG = "Dyna.MoveQueueOSE2OIL.exe.config";
    CONST SAP_DAEMON_EXE = "Dyna.MoveQueueOSE2OIL.exe";

    public function run()
    {
        $baseDirectory = Mage::getBaseDir() . DS . 'shell';

        $jobDaemonConfig = file_get_contents($baseDirectory.DS.self::JOB_DAEMON_DIR.DS.self::JOB_DAEMON_CONFIG);
        $statusDaemonConfig = file_get_contents($baseDirectory.DS.self::STATUS_DAEMON_DIR.DS.self::STATUS_DAEMON_CONFIG);
        $sapDaemonConfig = file_get_contents($baseDirectory.DS.self::SAP_DAEMON_DIR.DS.self::SAP_DAEMON_CONFIG);

        $jobLockFile = $baseDirectory.DS.self::JOB_DAEMON_DIR . DS . self::JOB_DAEMON_DIR . '.lock ';
        $statusLockFile = $baseDirectory.DS.self::STATUS_DAEMON_DIR . DS . self::STATUS_DAEMON_DIR . '.lock ';
        $sapLockFile = $baseDirectory.DS.self::SAP_DAEMON_DIR . DS . self::SAP_DAEMON_DIR . '.lock ';

        if(!$jobDaemonConfig){
            Mage::throwException('Could not open Job Daemon Config File!');
            die();
        } else {
            $jobStoreconfig = array(
                'PhpExecPath' => Mage::getStoreConfig('omnius_service/job_queue/php_path') ?? '/usr/bin/php',
                'PhpScriptPath' => $baseDirectory . DS . 'job_processor_runner.php',
                'RabbitUsername' => Mage::getStoreConfig('omnius_service/job_queue/amqp_user'),
                'RabbitPassword' => Mage::getStoreConfig('omnius_service/job_queue/amqp_pass'),
                'RabbitHostName' => Mage::getStoreConfig('omnius_service/job_queue/amqp_host'),
                'RabbitVirtualHost' => Mage::getStoreConfig('omnius_service/job_queue/amqp_vhost'),
                'RabbitPort' => Mage::getStoreConfig('omnius_service/job_queue/amqp_port'),
                'RabbitJobQueue' => Mage::getStoreConfig('omnius_service/job_queue/amqp_queue'),
                'RabbitJobRetryExchange' => Mage::getStoreConfig('omnius_service/job_queue/amqp_exchange'),
                'RabbitJobRetryRoutingKey' => Mage::getStoreConfig('omnius_service/job_queue/amqp_retry_queue'),
                'MaxNumberOfRetries' => Mage::getStoreConfig('omnius_service/job_queue/amqp_retries')
            );

            file_put_contents($baseDirectory . DS . self::JOB_DAEMON_DIR . DS . self::JOB_DAEMON_CONFIG,
                $this->replaceXMLValues($jobDaemonConfig, $jobStoreconfig)
            );
        }

        if(!$statusDaemonConfig){
            Mage::throwException('Could not open Status Daemon Config File!');
            die();
        } else {
            $statusStoreConfig = array(
                'PhpExecPath' => Mage::getStoreConfig('omnius_service/job_queue/php_path') ?? '/usr/bin/php',
                'PhpScriptPath' => $baseDirectory . DS . 'amqp/consumer.php',
                'RabbitUsername' => Mage::getStoreConfig('omnius_service/ampq_consumer_settings/amqp_user'),
                'RabbitPassword' => Mage::getStoreConfig('omnius_service/ampq_consumer_settings/amqp_pass'),
                'RabbitHostName' => Mage::getStoreConfig('omnius_service/ampq_consumer_settings/amqp_host'),
                'RabbitVirtualHost' => Mage::getStoreConfig('omnius_service/ampq_consumer_settings/amqp_vhost'),
                'RabbitPort' => Mage::getStoreConfig('omnius_service/ampq_consumer_settings/amqp_port'),
                'RabbitQueue' => Mage::getStoreConfig('omnius_service/ampq_consumer_settings/amqp_queue')
            );

            file_put_contents($baseDirectory . DS . self::STATUS_DAEMON_DIR . DS . self::STATUS_DAEMON_CONFIG,
                $this->replaceXMLValues($statusDaemonConfig, $statusStoreConfig)
            );
        }

        if(!$sapDaemonConfig){
            Mage::throwException('Could not open SAP Daemon Config File!');
            die();
        } else {
            $sapConfig = array(
                'OSEQueueHostname' => Mage::getStoreConfig('omnius_service/job_queue/amqp_host'),
                'OSEQueueName' => 'dyna.ose.saporders',
                'OSEQueueUserName' => Mage::getStoreConfig('omnius_service/ampq_consumer_settings/amqp_user'),
                'OSEQueuePassword' => Mage::getStoreConfig('omnius_service/ampq_consumer_settings/amqp_pass'),
                'OILQueueName' => 'dyna.oil.saporders',
                'OILQueueUserName' => Mage::getStoreConfig('omnius_service/job_queue/amqp_user'),
                'OILQueuePassword' => Mage::getStoreConfig('omnius_service/job_queue/amqp_pass')
            );

            //Set the MYSQL connection string in a different way then the other config
            $databaseTemplate = 'server=%s;userid=%s;password=%s;database=%s'; //NOSONAR (suppresses Sonar warnings)
            $databaseDataFile = file_get_contents(Mage::getBaseDir() . DS . 'app' . DS . 'etc' . DS . 'local.xml');
            if($databaseDataFile){
                $startIndex = strpos($databaseDataFile, '<default_setup>');
                $databaseTemplate = sprintf($databaseTemplate
                    , $this->extractString($databaseDataFile, '<host>' , '</host>', $startIndex)
                    , $this->extractString($databaseDataFile, '<username>' , '</username>', $startIndex)
                    , $this->extractString($databaseDataFile, '<password>' , '</password>', $startIndex)
                    , $this->extractString($databaseDataFile, '<dbname>' , '</dbname>', $startIndex)
                );

                $configStartString = '<add name="MySQLConnectingString" connectionString="';
                $configEndString = '" providerName=';
                $configStart = strpos($sapDaemonConfig, $configStartString) + strlen($configStartString);
                $configEnd = strpos($sapDaemonConfig, $configEndString, $configStart);

                substr_replace($sapDaemonConfig, $databaseTemplate, $configStart, $configEnd - $configStart);

            } else {
                Mage::throwException('Could not open local.xml!');
                die();
            }

            file_put_contents($baseDirectory . DS . self::SAP_DAEMON_DIR. DS . self::SAP_DAEMON_CONFIG,
                $this->replaceXMLValues($sapDaemonConfig, $sapConfig));
        }

        echo 'Killing running daemons' . PHP_EOL;
        exec('cat ' . $jobLockFile . ' | xargs kill -9');
        exec('cat ' . $statusLockFile . ' | xargs kill -9');
        exec('cat ' . $sapLockFile . ' | xargs kill -9');

        echo 'Deleting lock files' . PHP_EOL;
        exec('rm -rf ' . $jobLockFile);
        exec('rm -rf ' . $statusLockFile);
        exec('rm -rf ' . $sapLockFile);

        echo 'Start Job daemon' . PHP_EOL;
        exec('nohup /usr/bin/mono /usr/lib/mono/4.5/mono-service.exe -l:'
            . $jobLockFile
            . '-d:' . $baseDirectory.DS.self::JOB_DAEMON_DIR.DS
            . ' ' . $baseDirectory.DS.self::JOB_DAEMON_DIR.DS.self::JOB_DAEMON_EXE
            . ' >/dev/null 2>&1 &'
        );

        echo 'Start Status daemon' . PHP_EOL;
        exec('nohup /usr/bin/mono /usr/lib/mono/4.5/mono-service.exe -l:'
            . $statusLockFile
            . '-d:' . $baseDirectory.DS.self::STATUS_DAEMON_DIR.DS
            . ' ' . $baseDirectory.DS.self::STATUS_DAEMON_DIR.DS.self::STATUS_DAEMON_EXE
            . ' >/dev/null 2>&1 &'
        );

        echo 'Start SAP daemon' . PHP_EOL;
        exec('nohup /usr/bin/mono /usr/lib/mono/4.5/mono-service.exe -l:'
            . $sapLockFile
            . '-d:' . $baseDirectory.DS.self::SAP_DAEMON_DIR.DS
            . ' ' . $baseDirectory.DS.self::SAP_DAEMON_DIR.DS.self::SAP_DAEMON_EXE
            . ' >/dev/null 2>&1 &'
        );

        echo 'Deamons started' . PHP_EOL;
    }

    /**
     * Made specifically for adjusting the C# config files.
     * SimpleXML does not cooperate with these files
     *
     * Just looks for substring and replaces by using substring's index
     *
     * @param string $string
     * @param array $variables
     * @return string $string
     */
    public function replaceXMLValues($string, $variables)
    {
        foreach ($variables as $key => $value){
            $startPosition = strpos($string, $key);

            $searchString = 'value=';
            $valuePositionStart = strpos($string, $searchString, $startPosition) + strlen($searchString);
            $valuePositionEnd = strpos($string, '/>', $valuePositionStart);
            $string = substr_replace($string, '"' . $value . '"' , $valuePositionStart, ($valuePositionEnd - $valuePositionStart));
        }

        return $string;
    }

    /**
     * Searches for a tag and retrieves its value
     * @param $string   String  String to search
     * @param $start    String  Starting tag
     * @param $end      String  Ending tag
     * @param $offset   Int     Search offset
     * @return String
     */
    function extractString($string, $start, $end, $offset = 0) {
        $string = " ".$string;
        $ini = strpos($string, $start, $offset);
        if ($ini == 0){
            return '';
        }
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }
}

$start = new Dyna_VFDE_StartDaemons();
$start->run();