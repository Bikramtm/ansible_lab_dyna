SET FOREIGN_KEY_CHECKS=0;
ALTER TABLE `agent_role`
CHANGE COLUMN `role_id` `role_id` INT(11) UNSIGNED NOT NULL COMMENT 'Role_id' ,
ADD COLUMN `role_description` VARCHAR(200) NULL DEFAULT NULL AFTER `role`,
ADD UNIQUE INDEX `role_id_UNIQUE` (`role_id` ASC);

ALTER TABLE `admin_user`
ADD COLUMN `locked_account` TINYINT(1) NULL DEFAULT NULL AFTER `rp_token_created_at`;

ALTER TABLE `agent`
ADD COLUMN `usertype` VARCHAR(64) NULL DEFAULT NULL AFTER `created_at`,
ADD COLUMN `ngumid` TEXT NULL DEFAULT NULL AFTER `usertype`,
ADD COLUMN `hash_method` VARCHAR(20) NULL DEFAULT NULL AFTER `ngumid`,
ADD COLUMN `additional_role_ids` TEXT NULL DEFAULT NULL AFTER `hash_method`,
ADD COLUMN `externalmail` TEXT NULL DEFAULT NULL AFTER `additional_role_ids`,
ADD COLUMN `tokens_generated` INT(11) NULL DEFAULT NULL AFTER `externalmail`,
ADD COLUMN `digest_auth_hash` VARCHAR(50) NULL DEFAULT NULL AFTER `tokens_generated`,
ADD COLUMN `partnermanager` TEXT NULL DEFAULT NULL AFTER `digest_auth_hash`,
ADD COLUMN `reportingto` TEXT NULL DEFAULT NULL AFTER `partnermanager`,
ADD COLUMN `orgunitcode` TEXT NULL DEFAULT NULL AFTER `reportingto`,
ADD COLUMN `orgunitlong` TEXT NULL DEFAULT NULL AFTER `orgunitcode`,
ADD COLUMN `location` TEXT NULL DEFAULT NULL AFTER `orgunitlong`,
ADD COLUMN `workplace` TEXT NULL DEFAULT NULL AFTER `location`,
ADD COLUMN `red_sales_id` VARCHAR(64) NULL DEFAULT NULL AFTER `workplace`,
ADD COLUMN `other_red_sales_ids` TEXT NULL DEFAULT NULL AFTER `red_sales_id`;

ALTER TABLE `agent`
DROP FOREIGN KEY `fk_agent_role`;

ALTER TABLE `agent_role`
CHANGE COLUMN `role_id` `entity_id` INT(11) UNSIGNED NOT NULL COMMENT 'Role_id' ;

ALTER TABLE `agent`
DROP INDEX `fk_agent_role` ;

ALTER TABLE `agent`
ADD INDEX `fk_agent_role_idx` (`role_id` ASC);

ALTER TABLE `agent`
ADD CONSTRAINT `fk_agent_role`
  FOREIGN KEY (`role_id`)
  REFERENCES `agent_role` (`entity_id`)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT;
ALTER TABLE `agent_role`
ADD COLUMN `role_id` INT(11) UNSIGNED NOT NULL AFTER `role_description`;

ALTER TABLE `dealer`
CHANGE COLUMN `street` `street` TEXT NULL DEFAULT NULL COMMENT 'Street' ,
CHANGE COLUMN `postcode` `postcode` VARCHAR(64) NULL DEFAULT NULL COMMENT 'Postcode' ,
CHANGE COLUMN `city` `city` VARCHAR(64) NULL DEFAULT NULL COMMENT 'City' ,
ADD COLUMN `hotline_number` VARCHAR(50) NULL AFTER `is_deleted`,
ADD COLUMN `vf_dealer_code_kias` VARCHAR(20) NULL DEFAULT NULL AFTER `hotline_number`,
ADD COLUMN `vf_dealer_code_fn` VARCHAR(20) NULL DEFAULT NULL AFTER `vf_dealer_code_kias`,
ADD COLUMN `vf_dealer_code_cable` VARCHAR(20) NULL DEFAULT NULL AFTER `vf_dealer_code_fn`,
ADD COLUMN `bounce_info_email_address` VARCHAR(255) NULL DEFAULT NULL AFTER `vf_dealer_code_cable`;

ALTER TABLE `dealer_group`
CHANGE COLUMN `name` `name` TEXT NULL DEFAULT NULL COMMENT 'Name' ,
ADD COLUMN `description` TEXT NULL DEFAULT NULL AFTER `name`;

drop table catalog_product_stock;

-- Give all roles a default role_id (PK as default)
update agent_role set role_id = entity_id;
SET FOREIGN_KEY_CHECKS=1;