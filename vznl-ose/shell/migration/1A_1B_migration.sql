SET @@session.sql_mode ="ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION";
SET SESSION optimizer_switch='index_merge=on,index_merge_union=on,index_merge_sort_union=on,index_merge_intersection=on,engine_condition_pushdown=on,index_condition_pushdown=on,mrr=on,mrr_cost_based=on,block_nested_loop=off,batched_key_access=off,materialization=on,semijoin=on,loosescan=on,firstmatch=on,duplicateweedout=on,subquery_materialization_cost_based=on,use_index_extensions=on,condition_fanout_filter=on,derived_merge=on';
SET GLOBAL connect_timeout=1800;
SET GLOBAL wait_timeout=1800;
SET GLOBAL interactive_timeout=1800;

-- New tables, complete add

CREATE TABLE `compatability` (
                                 `rule_id`  int(11) NOT NULL AUTO_INCREMENT ,
                                 `product_left`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
                                 `category_left`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
                                 `operator`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
                                 `product_right`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
                                 `category_right`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
                                 PRIMARY KEY (`rule_id`)
)
    ENGINE=InnoDB
    DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
    ROW_FORMAT=Compact;

CREATE TABLE `customer_flowpassword` (
                                         `flowpassword_id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Flow password Id' ,
                                         `ip`  varchar(50) NOT NULL COMMENT 'User IP' ,
                                         `email`  varchar(255) NOT NULL COMMENT 'Requested email for change' ,
                                         `requested_date`  varchar(255) NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Requested date for change' ,
                                         PRIMARY KEY (`flowpassword_id`),
                                         INDEX `IDX_CUSTOMER_FLOWPASSWORD_EMAIL` (`email`) USING BTREE ,
                                         INDEX `IDX_CUSTOMER_FLOWPASSWORD_IP` (`ip`) USING BTREE ,
                                         INDEX `IDX_CUSTOMER_FLOWPASSWORD_REQUESTED_DATE` (`requested_date`) USING BTREE
)
    ENGINE=InnoDB
    DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
    COMMENT='Customer flow password'
    ROW_FORMAT=Compact;

CREATE TABLE `hawaii_specgroup` (
                                    `entity_id`  int(11) NOT NULL AUTO_INCREMENT ,
                                    `name`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
                                    `hawaii_ref`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
                                    PRIMARY KEY (`entity_id`)
)
    ENGINE=InnoDB
    DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
    ROW_FORMAT=Compact;

CREATE TABLE `mixmatch` (
                            `mixmatch_id`  int(11) NOT NULL AUTO_INCREMENT ,
                            `phone_sku`  varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
                            `subscriotion_sku`  varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
                            `price`  varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
                            PRIMARY KEY (`mixmatch_id`)
)
    ENGINE=InnoDB
    DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
    ROW_FORMAT=Compact;

CREATE TABLE `purges` (
                          `purge_id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Purge ID' ,
                          `entity_type`  int(10) UNSIGNED NOT NULL COMMENT 'Type of purged entity' ,
                          `entity_id`  int(10) UNSIGNED NOT NULL COMMENT 'Idenity of purged entity' ,
                          `purge_time`  timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Purge timestamp' ,
                          `crawl_time`  timestamp NULL DEFAULT NULL COMMENT 'Crawl timestamp' ,
                          `is_crawled`  tinyint(1) NOT NULL COMMENT 'Was this purge recrawled' ,
                          PRIMARY KEY (`purge_id`)
)
    ENGINE=InnoDB
    DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
    COMMENT='purges'
    ROW_FORMAT=Compact;

-- Missing: values
CREATE TABLE `partial_termination_reason` (
                                              `entity_id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID' ,
                                              `code`  varchar(255) NULL DEFAULT NULL COMMENT 'Partial termination main reason code' ,
                                              `en_value`  varchar(255) NULL DEFAULT NULL COMMENT 'English Translation' ,
                                              `nl_value`  varchar(255) NULL DEFAULT NULL COMMENT 'Dutch Translation' ,
                                              `created_at`  datetime NULL DEFAULT NULL COMMENT 'Created' ,
                                              `updated_at`  datetime NULL DEFAULT NULL COMMENT 'Updated' ,
                                              PRIMARY KEY (`entity_id`),
                                              UNIQUE INDEX `UNQ_PARTIAL_TERMINATION_REASON_CODE` (`code`) USING BTREE
)
    ENGINE=InnoDB
    DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
    COMMENT='Partial termination reasons'
    ROW_FORMAT=Compact;

-- Missing: values
CREATE TABLE `partial_termination_subreason` (
                                                 `entity_id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID' ,
                                                 `main_reason_id`  int(10) UNSIGNED NOT NULL COMMENT 'Partial Termination Main Reason Id' ,
                                                 `code`  varchar(255) NULL DEFAULT NULL COMMENT 'Partial termination subreason code' ,
                                                 `en_value`  varchar(255) NULL DEFAULT NULL COMMENT 'English Translation' ,
                                                 `nl_value`  varchar(255) NULL DEFAULT NULL COMMENT 'Dutch Translation' ,
                                                 `include_competitor`  int(11) NULL DEFAULT NULL COMMENT 'Include or Exclude Competitor' ,
                                                 `created_at`  datetime NULL DEFAULT NULL COMMENT 'Created' ,
                                                 `updated_at`  datetime NULL DEFAULT NULL COMMENT 'Updated' ,
                                                 PRIMARY KEY (`entity_id`),
                                                 UNIQUE INDEX `UNQ_PARTIAL_TERMINATION_SUBREASON_CODE` (`code`) USING BTREE
)
    ENGINE=InnoDB
    DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
    COMMENT='Partial termination subreasons'
    ROW_FORMAT=Compact;

-- Missing: values
CREATE TABLE `vznl_peal_validationerrorcode` (
                                                 `entity_id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID' ,
                                                 `code`  varchar(250) NULL DEFAULT NULL COMMENT 'Code' ,
                                                 `error`  varchar(250) NULL DEFAULT NULL COMMENT 'Error' ,
                                                 `created_at`  datetime NULL DEFAULT NULL COMMENT 'Created' ,
                                                 `updated_at`  datetime NULL DEFAULT NULL COMMENT 'Updated' ,
                                                 PRIMARY KEY (`entity_id`),
                                                 UNIQUE INDEX `UNQ_VZNL_PEAL_VALIDATIONERRORCODE_CODE` (`code`) USING BTREE
)
    ENGINE=InnoDB
    COMMENT='Vznl Peal Validation Error Codes'
    ROW_FORMAT=Compact;

-- Changes: DEFAULT value set to NULL, may cause issues
-- ALTER TABLE `admin_user` MODIFY COLUMN `created`  timestamp NULL DEFAULT NULL COMMENT 'User Created Time' AFTER `password`;
-- ALTER TABLE `agent` MODIFY COLUMN `created_at`  timestamp NULL DEFAULT NULL COMMENT 'Created At' AFTER `last_login_date`;

ALTER TABLE `bundle_campaign`
    ADD COLUMN `dealer_id`  text NULL DEFAULT NULL COMMENT 'Dealer Id' AFTER `default`,
    ADD COLUMN `dealer_group`  text NULL DEFAULT NULL COMMENT 'Dealer Group' AFTER `dealer_id`,
    ADD COLUMN `product_segment`  text NULL DEFAULT NULL COMMENT 'product_segment' AFTER `dealer_group`,
    ADD COLUMN `channel`  text NULL DEFAULT NULL COMMENT 'Channel' AFTER `product_segment`;

ALTER TABLE `bundle_rules`
    MODIFY COLUMN `bundle_type`  varchar(45) NULL DEFAULT NULL COMMENT 'Used for determining what type of bundle it is: RedPlus / MarketingBundle / SurfSofortDSL / SurfSofortKIP / Other' AFTER `description`,
    ADD COLUMN `effective_date`  date NULL DEFAULT NULL COMMENT 'Rule start date' AFTER `bundle_type`,
    ADD COLUMN `expiration_date`  date NULL DEFAULT NULL COMMENT 'Rule end date' AFTER `effective_date`,
    DROP COLUMN `ogw_orchestration_type`;

ALTER TABLE `catalog_package`
    ADD COLUMN `imei_registration_date`  datetime NULL DEFAULT NULL AFTER `imei`,
    MODIFY COLUMN `fixed_to_mobile`  varchar(50) NULL DEFAULT NULL AFTER `imei_registration_date`,
    MODIFY COLUMN `contract_nr`  varchar(50) NULL DEFAULT NULL AFTER `refund_reason`,
    MODIFY COLUMN `service_address_id`  varchar(45) NULL DEFAULT NULL AFTER `activity_type`,
    ADD COLUMN `fixed_number_porting_status`  int(11) NULL DEFAULT NULL COMMENT 'Number porting enable or disable' AFTER `service_address_id`,
    ADD COLUMN `fixed_first_line_phone`  varchar(12) NULL DEFAULT NULL COMMENT 'Fixed first line phone number' AFTER `fixed_number_porting_status`,
    ADD COLUMN `fixed_second_line_phone`  varchar(12) NULL DEFAULT NULL COMMENT 'Fixed second line phone number' AFTER `fixed_first_line_phone`,
    ADD COLUMN `fixed_overstappen_status`  int(11) NULL DEFAULT NULL COMMENT 'Fixed overstappen enable or disable' AFTER `fixed_second_line_phone`,
    ADD COLUMN `fixed_overstappen_current_provider`  text NULL DEFAULT NULL COMMENT 'Fixed overstappen current provider' AFTER `fixed_overstappen_status`,
    ADD COLUMN `fixed_overstappen_delivery_date`  text NULL DEFAULT NULL COMMENT 'Fixed overstappen delivery wish date' AFTER `fixed_overstappen_current_provider`,
    ADD COLUMN `fixed_overstappen_contract_id`  text NULL DEFAULT NULL COMMENT 'Fixed overstappen contract id' AFTER `fixed_overstappen_delivery_date`,
    ADD COLUMN `fixed_overstappen_termination_fee`  int(11) NULL DEFAULT NULL COMMENT 'Fixed overstappen termination fee' AFTER `fixed_overstappen_contract_id`,
    MODIFY COLUMN `additional_attributes`  text NULL DEFAULT NULL AFTER `fixed_overstappen_termination_fee`,
    MODIFY COLUMN `applied_promos_order`  text NULL DEFAULT NULL COMMENT 'The selection order of manually(stackable) applied promo rules' AFTER `additional_attributes`,
    ADD COLUMN `is_offer`  tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Package is added from offer or not' AFTER `applied_promos_order`,
    DROP COLUMN `refund_reason_message`,
    DROP INDEX `IDX_CATALOG_PACKAGE_CREDITCHECK_STATUS_UPDATED`,
    DROP INDEX `IDX_CATALOG_PACKAGE_PORTING_UPDATE`;

-- Changes column to bigint(20) from int(11)
ALTER TABLE `customer_address_entity_text`
    MODIFY COLUMN `value_id`  bigint(20) NOT NULL AUTO_INCREMENT FIRST;

-- Changes column to bigint(20) from int(11)
ALTER TABLE `customer_address_entity_varchar`
    MODIFY COLUMN `value_id`  bigint(20) NOT NULL AUTO_INCREMENT FIRST;

-- Changes: DEFAULT value set to NULL, may cause issues
-- ALTER TABLE `customer_entity` MODIFY COLUMN `created_at`  timestamp NULL DEFAULT NULL COMMENT 'Created At' AFTER `store_id`;
-- ALTER TABLE `customer_entity` MODIFY COLUMN `updated_at`  timestamp NULL DEFAULT NULL COMMENT 'Updated At' AFTER `created_at`;

-- Changes key to bigint(20) from int(11)
ALTER TABLE `customer_entity_datetime`
    MODIFY COLUMN `value_id`  bigint(20) NOT NULL AUTO_INCREMENT FIRST;

-- Changes key to bigint(20) from int(11)
ALTER TABLE `customer_entity_varchar`
    MODIFY COLUMN `value_id`  bigint(20) NOT NULL AUTO_INCREMENT FIRST;

ALTER TABLE `dealer`
    MODIFY COLUMN `name`  varchar(255) NULL DEFAULT NULL COMMENT 'Dealer name' AFTER `dealer_id`,
    MODIFY COLUMN `street`  text NULL DEFAULT NULL COMMENT 'Street' AFTER `dealer_type`,
    MODIFY COLUMN `call_center_activity`  varchar(20) NULL DEFAULT NULL COMMENT 'call_center_activity' AFTER `bounce_info_email_address`,
    MODIFY COLUMN `line_of_business`  varchar(20) NULL DEFAULT 'Combined' COMMENT 'line_of_business' AFTER `call_center_activity`,
    MODIFY COLUMN `primary_brand`  varchar(20) NULL DEFAULT '' COMMENT 'Primary Brand' AFTER `line_of_business`,
    MODIFY COLUMN `sales_code`  varchar(45) NULL DEFAULT NULL AFTER `primary_brand`,
    MODIFY COLUMN `sales_channel`  varchar(45) NULL DEFAULT NULL AFTER `sales_code`,
    MODIFY COLUMN `sales_location`  varchar(45) NULL DEFAULT NULL AFTER `sales_channel`,
    MODIFY COLUMN `sender_domain`  varchar(45) NULL DEFAULT NULL COMMENT 'Sender Domain' AFTER `sales_location`,
    MODIFY COLUMN `description`  varchar(255) NULL DEFAULT NULL COMMENT 'Description' AFTER `name`,
    MODIFY COLUMN `postcode`  varchar(64) NULL DEFAULT NULL COMMENT 'Postcode' AFTER `street`;

ALTER TABLE `dyna_communication_log`
    MODIFY COLUMN `superorder_id`  int(11) NULL DEFAULT NULL COMMENT 'Superorder_id' AFTER `log_id`,
    MODIFY COLUMN `type`  varchar(255) NOT NULL COMMENT 'Type' AFTER `superorder_id`,
    MODIFY COLUMN `rowkey`  varchar(255) NOT NULL COMMENT 'Rowkey' AFTER `type`;

ALTER TABLE `dyna_perf_log_data`
    MODIFY COLUMN `id`  bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Id' FIRST,
    MODIFY COLUMN `calltype`  varchar(25) NOT NULL COMMENT 'Calltype' AFTER `id`,
    MODIFY COLUMN `callname`  varchar(45) NOT NULL COMMENT 'Callname' AFTER `calltype`,
    MODIFY COLUMN `startedat`  datetime NULL DEFAULT NULL COMMENT 'Startedat' AFTER `callname`,
    MODIFY COLUMN `server`  varchar(16) NOT NULL COMMENT 'Server' AFTER `startedat`,
    MODIFY COLUMN `duration`  int(10) UNSIGNED NOT NULL COMMENT 'Duration' AFTER `server`,
    COMMENT='dyna_perf_log_data';

ALTER TABLE `product_family`
    MODIFY COLUMN `product_family_id`  varchar(255) NULL DEFAULT '' COMMENT 'Number of the family where the product is part' AFTER `entity_id`,
    MODIFY COLUMN `product_family_name`  varchar(255) NULL DEFAULT '' COMMENT 'Name of the family where to product belong to' AFTER `lifecycle_status`;

ALTER TABLE `product_version`
    MODIFY COLUMN `product_family_id`  varchar(255) NULL DEFAULT NULL COMMENT 'Number of the family where the product is part ' AFTER `lifecycle_status`;

ALTER TABLE `sales_flat_order`
    MODIFY COLUMN `customer_issue_date`  date NULL DEFAULT NULL COMMENT 'ID issue date' AFTER `customer_issuing_country`,
    MODIFY COLUMN `customer_valid_until`  datetime NULL DEFAULT NULL AFTER `customer_issue_date`,
    MODIFY COLUMN `fixed_payment_method_monthly_charges`  varchar(100) NULL DEFAULT NULL COMMENT 'Payment Method Monthly Charges from validatecart services' AFTER `banknumber_accountname`,
    MODIFY COLUMN `credit_check_result`  int(11) NULL DEFAULT NULL COMMENT 'Credit Check Result' AFTER `fixed_bill_distribution_method`,
    MODIFY COLUMN `contractant_issue_date`  date NULL DEFAULT NULL COMMENT 'ID issue date' AFTER `contractant_id_number`,
    DROP INDEX `IDX_SALES_FLAT_ORDER_EDITED`;

ALTER TABLE `sales_flat_order`
    MODIFY COLUMN `contractant_valid_until`  datetime NULL DEFAULT NULL AFTER `contractant_issue_date`,
    MODIFY COLUMN `ziggo_telephone`  text NULL DEFAULT NULL COMMENT 'Ziggo telephone number' AFTER `additional_email`,
    MODIFY COLUMN `hidden_maf_tax_amount`  decimal(12,4) NULL DEFAULT 0.0000 COMMENT 'Hidden Monthly Annual Fee Tax Amount' AFTER `ziggo_email`,
    MODIFY COLUMN `customer_number`  varchar(20) NULL DEFAULT NULL AFTER `base_payment_percentage_fee`,
    MODIFY COLUMN `hardware_name`  varchar(100) NULL DEFAULT NULL AFTER `fixed_delivery_type`,
    MODIFY COLUMN `serial_number`  varchar(45) NULL DEFAULT NULL AFTER `hardware_name`,
    MODIFY COLUMN `order_type`  varchar(50) NULL DEFAULT NULL COMMENT 'order type' AFTER `peal_different_address_id`,
    ADD COLUMN `footprint`  varchar(100) NULL DEFAULT NULL AFTER `edited_packages_ids`;

ALTER TABLE `sales_flat_order_address`
    ADD COLUMN `additional_line`  text NULL DEFAULT NULL COMMENT 'Additional address line' AFTER `postcode`,
    MODIFY COLUMN `lastname`  varchar(255) NULL DEFAULT NULL COMMENT 'Lastname' AFTER `additional_line`,
    ADD COLUMN `fixed_street`  text NULL DEFAULT NULL COMMENT 'billing address type' AFTER `delivery_store_id`,
    ADD COLUMN `fixed_city`  text NULL DEFAULT NULL COMMENT 'billing address type' AFTER `fixed_street`,
    ADD COLUMN `fixed_postcode`  text NULL DEFAULT NULL COMMENT 'billing address type' AFTER `fixed_city`;

ALTER TABLE `sales_flat_order_item`
    MODIFY COLUMN `created_at`  timestamp NULL DEFAULT NULL COMMENT 'Created At' AFTER `store_id`,
    MODIFY COLUMN `updated_at`  timestamp NULL DEFAULT NULL COMMENT 'Updated At' AFTER `created_at`,
    MODIFY COLUMN `is_defaulted`  tinyint(1) NULL DEFAULT NULL COMMENT 'Attribute to know if the item was added by a defaulted rule or manually from configurator' AFTER `item_doa`,
    MODIFY COLUMN `pack_product_type`  varchar(20) NULL DEFAULT NULL COMMENT 'Product type / used for deleted products' AFTER `mixmatch_tax`,
    MODIFY COLUMN `adyen_pre_order`  tinyint(1) NULL DEFAULT NULL COMMENT 'Adyen Pre Order' AFTER `pack_product_type`,
    MODIFY COLUMN `applied_rule_names`  text NULL DEFAULT NULL AFTER `is_service_defaulted`,
    MODIFY COLUMN `bundle_component`  int(11) NULL DEFAULT 0 AFTER `applied_rule_names`,
    MODIFY COLUMN `hardware_name`  varchar(100) NULL DEFAULT NULL AFTER `offer_type`,
    MODIFY COLUMN `serial_number`  varchar(45) NULL DEFAULT NULL AFTER `hardware_name`,
    MODIFY COLUMN `new_hardware`  int(11) NULL DEFAULT 0 COMMENT 'bom id from peal validate basket call' AFTER `added_via_peal`;

ALTER TABLE `sales_flat_quote`
    MODIFY COLUMN `created_at`  timestamp NULL DEFAULT NULL COMMENT 'Created At' AFTER `store_id`,
    MODIFY COLUMN `updated_at`  timestamp NULL DEFAULT NULL COMMENT 'Updated At' AFTER `created_at`,
    MODIFY COLUMN `customer_issue_date`  date NULL DEFAULT NULL COMMENT 'ID issue date' AFTER `customer_issuing_country`,
    MODIFY COLUMN `customer_valid_until`  datetime NULL DEFAULT NULL AFTER `customer_issue_date`,
    MODIFY COLUMN `fixed_payment_method_monthly_charges`  varchar(100) NULL DEFAULT NULL COMMENT 'Payment Method Monthly Charges from validatecart services' AFTER `banknumber_accountname`,
    MODIFY COLUMN `credit_check_result`  int(11) NULL DEFAULT NULL COMMENT 'Credit Check Result' AFTER `fixed_bill_distribution_method`,
    MODIFY COLUMN `contractant_issue_date`  date NULL DEFAULT NULL COMMENT 'ID issue date' AFTER `contractant_id_number`,
    MODIFY COLUMN `contractant_valid_until`  datetime NULL DEFAULT NULL AFTER `contractant_issue_date`,
    MODIFY COLUMN `ziggo_telephone`  text NULL DEFAULT NULL COMMENT 'Ziggo telephone number' AFTER `additional_email`,
    MODIFY COLUMN `shipping_data`  text NULL DEFAULT NULL COMMENT 'Shipping address details' AFTER `ziggo_email`,
    MODIFY COLUMN `hawaii_quote_errors`  text NULL DEFAULT NULL COMMENT 'Errors in the saveQuote' AFTER `superorder_number`,
    MODIFY COLUMN `customer_number`  varchar(20) NULL DEFAULT NULL AFTER `dummy_email`,
    MODIFY COLUMN `hardware_name`  varchar(100) NULL DEFAULT NULL AFTER `fixed_delivery_type`,
    MODIFY COLUMN `serial_number`  varchar(45) NULL DEFAULT NULL AFTER `hardware_name`,
    ADD COLUMN `footprint`  varchar(100) NULL DEFAULT NULL AFTER `order_type`;

ALTER TABLE `sales_flat_quote_address`
    MODIFY COLUMN `created_at`  timestamp NULL DEFAULT NULL COMMENT 'Created At' AFTER `quote_id`,
    MODIFY COLUMN `updated_at`  timestamp NULL DEFAULT NULL COMMENT 'Updated At' AFTER `created_at`,
    ADD COLUMN `additional_line`  text NULL DEFAULT NULL COMMENT 'Additional address line' AFTER `postcode`,
    MODIFY COLUMN `country_id`  varchar(255) NULL DEFAULT NULL COMMENT 'Country Id' AFTER `additional_line`,
    ADD COLUMN `fixed_street`  text NULL DEFAULT NULL COMMENT 'billing address type' AFTER `initial_mixmatch_maf_total`,
    ADD COLUMN `fixed_postcode`  text NULL DEFAULT NULL COMMENT 'billing address type' AFTER `fixed_street`,
    ADD COLUMN `fixed_city`  text NULL DEFAULT NULL COMMENT 'billing address type' AFTER `fixed_postcode`;

ALTER TABLE `sales_flat_quote_address_item`
    MODIFY COLUMN `created_at`  timestamp NULL DEFAULT NULL COMMENT 'Created At' AFTER `quote_item_id`,
    MODIFY COLUMN `updated_at`  timestamp NULL DEFAULT NULL COMMENT 'Updated At' AFTER `created_at`,
    ADD COLUMN `contract_start_date`  datetime NULL DEFAULT NULL COMMENT 'Contract Start Date' AFTER `product_visibility`;

ALTER TABLE `sales_flat_quote_item`
    MODIFY COLUMN `quote_id`  int(10) UNSIGNED NULL DEFAULT NULL COMMENT 'Quote Id' AFTER `item_id`,
    MODIFY COLUMN `created_at`  timestamp NULL DEFAULT NULL COMMENT 'Created At' AFTER `quote_id`,
    MODIFY COLUMN `updated_at`  timestamp NULL DEFAULT NULL COMMENT 'Updated At' AFTER `created_at`,
    MODIFY COLUMN `hardware_name`  varchar(100) NULL DEFAULT NULL AFTER `has_non_standard_rate`,
    MODIFY COLUMN `serial_number`  varchar(45) NULL DEFAULT NULL AFTER `hardware_name`,
    MODIFY COLUMN `additional_attributes`  text NULL DEFAULT NULL AFTER `serial_number`,
    MODIFY COLUMN `ean_number`  varchar(50) NULL DEFAULT NULL COMMENT 'EAN number' AFTER `additional_attributes`,
    MODIFY COLUMN `new_hardware`  int(1) NULL DEFAULT 0 COMMENT 'bom id from peal validate basket call' AFTER `added_via_peal`;

ALTER TABLE `sales_flat_quote_item_option`
    MODIFY COLUMN `option_id`  bigint(20) NOT NULL AUTO_INCREMENT FIRST;

ALTER TABLE `sales_rule_product_match`
    ADD COLUMN `package_type`  int(11) NULL DEFAULT NULL COMMENT 'package_type fk' AFTER `process_context`;

ALTER TABLE `salesrule`
    ADD COLUMN `package_type`  varchar(255) NULL DEFAULT NULL COMMENT 'Package Type' AFTER `promotion_label`;

ALTER TABLE `status_history`
    MODIFY COLUMN `entity_id`  bigint(20) NOT NULL AUTO_INCREMENT FIRST;

ALTER TABLE `superorder`
    MODIFY COLUMN `has_loan`  int(11) NULL DEFAULT NULL AFTER `new_customer`,
    MODIFY COLUMN `order_guid`  varchar(255) NULL DEFAULT NULL AFTER `ilt_selection_initial`,
    MODIFY COLUMN `customer_number`  varchar(20) NULL DEFAULT NULL AFTER `order_guid`;

ALTER TABLE `vznl_contract_sftp_jobs`
    MODIFY COLUMN `file_name`  text NOT NULL AFTER `job_id`;

ALTER TABLE `vznl_peal_errorcode`
    ADD COLUMN `service`  varchar(255) NULL DEFAULT NULL COMMENT 'service name' AFTER `entity_id`,
    MODIFY COLUMN `error`  varchar(250) NULL DEFAULT NULL COMMENT 'Error' AFTER `service`,
    ADD COLUMN `translation`  varchar(255) NULL DEFAULT NULL COMMENT 'Translation' AFTER `error`,
    MODIFY COLUMN `created_at`  datetime NULL DEFAULT NULL COMMENT 'Created' AFTER `translation`,
    MODIFY COLUMN `code`  int(11) NULL DEFAULT NULL COMMENT 'Code' AFTER `updated_at`;

-- ALTER TABLE `catalog_package`
--     ADD CONSTRAINT `fk_catalog_package_order_id` FOREIGN KEY (`order_id`) REFERENCES `superorder` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
--     ADD CONSTRAINT `fk_catalog_package_quote_id` FOREIGN KEY (`quote_id`) REFERENCES `sales_flat_quote` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- Constrants and indexes

-- ALTER TABLE `sales_flat_order`
--     ADD CONSTRAINT `FK_SALES_FLAT_ORDER_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE SET NULL ON UPDATE CASCADE,
--     ADD CONSTRAINT `FK_SALES_FLAT_ORDER_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- ALTER TABLE `sales_flat_order_item`
--     ADD CONSTRAINT `FK_SALES_FLAT_ORDER_ITEM_ORDER_ID_SALES_FLAT_ORDER_ENTITY_ID` FOREIGN KEY (`order_id`) REFERENCES `sales_flat_order` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
--     ADD CONSTRAINT `FK_SALES_FLAT_ORDER_ITEM_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- ALTER TABLE `sales_flat_quote`
--     ADD CONSTRAINT `FK_SALES_FLAT_QUOTE_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- ALTER TABLE `sales_flat_quote_address`
--     ADD CONSTRAINT `FK_SALES_FLAT_QUOTE_ADDRESS_QUOTE_ID_SALES_FLAT_QUOTE_ENTITY_ID` FOREIGN KEY (`quote_id`) REFERENCES `sales_flat_quote` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- ALTER TABLE `sales_flat_quote_item`
--     ADD CONSTRAINT `FK_B201DEB5DE51B791AF5C5BF87053C5A7` FOREIGN KEY (`parent_item_id`) REFERENCES `sales_flat_quote_item` (`item_id`) ON DELETE CASCADE ON UPDATE CASCADE,
--     ADD CONSTRAINT `FK_SALES_FLAT_QUOTE_ITEM_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
--     ADD CONSTRAINT `FK_SALES_FLAT_QUOTE_ITEM_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE;

CREATE INDEX `alternate_quote_id` ON `sales_flat_quote_item`(`alternate_quote_id`) USING BTREE;

-- ALTER TABLE `superorder_history`
--     ADD CONSTRAINT `fk_superorder_id` FOREIGN KEY (`superorder_id`) REFERENCES `superorder` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- ALTER TABLE `superorder_history_item`
--     ADD CONSTRAINT `fk_superorder_history_id` FOREIGN KEY (`superorder_history_id`) REFERENCES `superorder_history` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- CREATE INDEX `FK_BUNDLE_RULES_WEBSITE_ID_CORE_WEBSITE_WEBSITE_ID` ON `bundle_rules`(`website_id`) USING BTREE;

-- CREATE UNIQUE INDEX `895CDBBAF6EB81BD061AF8691DE06061` ON `catalog_mixmatch`(`target_sku`, `source_sku`, `device_subscription_sku`, `website_id`) USING BTREE;

-- CREATE INDEX `IDX_CATALOG_PACKAGE_TYPE` ON `catalog_package`(`type`, `sale_type`) USING BTREE;

-- CREATE INDEX `IDX_IDX_CAT_PACKAGE_STS_VF_STS_CODE_PORTING_STS_CREDITCHECK_STS` ON `catalog_package`(`status`, `vf_status_code`, `porting_status`, `creditcheck_status`) USING BTREE;

-- CREATE INDEX `idx_catalog_package_creditcheck_update_and_status` ON `catalog_package`(`creditcheck_status_updated`, `creditcheck_status`) USING BTREE;

-- CREATE INDEX `customer_ctn` ON `customer_ctn`(`ctn`) USING BTREE;

-- CREATE INDEX `IDX_CUSTOMER_ENTITY_PAN` ON `customer_entity`(`pan`) USING BTREE;

-- CREATE INDEX `IDX_CUSTOMER_ENTITY_AIL` ON `customer_entity`(`account_identifier_linkid`) USING BTREE;

-- CREATE INDEX `IDX_CUSTOMER_ENTITY_AIM` ON `customer_entity`(`account_identifier_mobile`) USING BTREE;

-- CREATE INDEX `IDX_CUSTOMER_ENTITY_AIF` ON `customer_entity`(`account_identifier_fixed`) USING BTREE;

-- CREATE INDEX `idx_dyna_communication_job_email_code` ON `dyna_communication_job`(`email_code`) USING BTREE;

-- CREATE INDEX `idx_dyna_email_job_log_recipient` ON `dyna_communication_log`(`recipient`, `sub_type`, `superorder_id`) USING BTREE;

-- CREATE INDEX `idx_dyna_email_job_log_searchby` ON `dyna_communication_log`(`recipient`, `created_at`, `sub_type`) USING BTREE;

-- CREATE INDEX `IDX_SALES_FLAT_ORDER_NAME` ON `sales_flat_order`(`customer_middlename`, `customer_lastname`, `customer_firstname`) USING BTREE;

-- CREATE INDEX `IDX_SUPERORDER_CREATED_DEALER` ON `superorder`(`created_dealer_id`) USING BTREE;

-- CREATE INDEX `9D616D2B6749C258B6E816EEF9C52CD6` ON `superorder`(`agent_id`, `customer_id`, `is_vf_only`, `order_status`, `error_code`) USING BTREE;

-- CREATE UNIQUE INDEX `UNQ_VZNL_PEAL_ERRORCODE_CODE` ON `vznl_peal_errorcode`(`code`) USING BTREE;

-- CREATE UNIQUE INDEX `UNQ_VZNL_WHITELIST_ACTION_SKU` ON `vznl_whitelist_action`(`sku`) USING BTREE;

CREATE INDEX `IDX_ROLE_ID_FK_ROLE_ID` ON `agent_role`(`role_id`) USING BTREE;

CREATE INDEX `IDX_CATALOG_PACKAGE_CC_STATUS` ON `catalog_package`(`creditcheck_status`) USING BTREE;

-- CREATE INDEX `IDX_CATALOG_PACKAGE_CREDITCHECK_STATUS` ON `catalog_package`(`creditcheck_status`) USING BTREE;

-- CREATE INDEX `IDX_CAT_PACKAGE_STS_VF_STS_CODE_PORTING_STS_CREDITCHECK_STS` ON `catalog_package`(`status`, `vf_status_code`, `porting_status`, `creditcheck_status`) USING BTREE;

-- CREATE INDEX `IDX_CUSTOMER_CTN` ON `customer_ctn`(`ctn`) USING BTREE;

CREATE INDEX `IDX_CUSTOMER_ENTITY_PAN_ENTITY_TYPE_ID` ON `customer_entity`(`pan`, `entity_type_id`) USING BTREE;

CREATE INDEX `IDX_CUSTOMER_ENTITY_BAN_ENTITY_TYPE_ID` ON `customer_entity`(`ban`, `entity_type_id`) USING BTREE;

CREATE INDEX `IDX_CUSTOMER_ENTITY_ACCOUNT_IDENTIFIER_MOBILE_ENTITY_TYPE_ID` ON `customer_entity`(`account_identifier_mobile`, `entity_type_id`) USING BTREE;

CREATE INDEX `IDX_CUSTOMER_ENTITY_ACCOUNT_IDENTIFIER_FIXED_ENTITY_TYPE_ID` ON `customer_entity`(`account_identifier_fixed`, `entity_type_id`) USING BTREE;

CREATE INDEX `fk_dealer_core_store` ON `dealer`(`store_id`) USING BTREE;

CREATE INDEX `IDX_SALES_FLAT_ORDER_ADDRESS_PARENT_ID_ADDRESS_TYPE` ON `sales_flat_order_address`(`parent_id`, `address_type`) USING BTREE;

-- CREATE INDEX `alternate_quote_id` ON `sales_flat_quote_item`(`alternate_quote_id`) USING BTREE;

-- CREATE INDEX `fk_superorder_customer_id` ON `superorder`(`customer_id`) USING BTREE;

-- CREATE INDEX `IDX_SUPERORDER_DATE` ON `superorder`(`created_at`) USING BTREE;

-- CREATE INDEX `IDX_SUPERORDER_IS_VF_ONLY` ON `superorder`(`is_vf_only`) USING BTREE;

-- CREATE INDEX `IDX_SPRORDER_AGENT_ID_CSTR_ID_IS_VF_ONLY_ORDER_STS_ERROR_CODE` ON `superorder`(`agent_id`, `customer_id`, `is_vf_only`, `order_status`, `error_code`) USING BTREE;

TRUNCATE log_customer;
TRUNCATE log_quote;
TRUNCATE log_summary;
TRUNCATE log_summary_type;
TRUNCATE log_url;
TRUNCATE log_url_info;
TRUNCATE log_visitor;
TRUNCATE log_visitor_info;
TRUNCATE log_visitor_online;


