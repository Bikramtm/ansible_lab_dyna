SET @firstDatabaseName = 'omnius1';
SET @secondDatabaseName = 'omnius_ose';

SELECT * FROM
  (SELECT
      cl.TABLE_NAME, CONCAT(cl.TABLE_NAME, ' [', cl.COLUMN_NAME, ', ', cl.COLUMN_TYPE, ']') tableRowType
    FROM information_schema.columns cl,  information_schema.TABLES ss
    WHERE
      cl.TABLE_NAME = ss.TABLE_NAME AND
      cl.TABLE_SCHEMA = @firstDatabaseName AND
      ss.TABLE_TYPE IN('BASE TABLE', 'VIEW')
    ORDER BY
      cl.table_name ) AS t1
LEFT JOIN
  (SELECT
      cl.TABLE_NAME, CONCAT(cl.TABLE_NAME, ' [', cl.COLUMN_NAME, ', ', cl.COLUMN_TYPE, ']') tableRowType
    FROM information_schema.columns cl,  information_schema.TABLES ss
    WHERE
      cl.TABLE_NAME = ss.TABLE_NAME AND
      cl.TABLE_SCHEMA = @secondDatabaseName AND
      ss.TABLE_TYPE IN('BASE TABLE', 'VIEW')
    ORDER BY
      cl.table_name ) AS t2 ON t1.tableRowType = t2.tableRowType
WHERE
  t2.tableRowType IS NULL

UNION

SELECT * FROM
  (SELECT
      cl.TABLE_NAME, CONCAT(cl.TABLE_NAME, ' [', cl.COLUMN_NAME, ', ', cl.COLUMN_TYPE, ']') tableRowType
    FROM information_schema.columns cl,  information_schema.TABLES ss
    WHERE
      cl.TABLE_NAME = ss.TABLE_NAME AND
      cl.TABLE_SCHEMA = @firstDatabaseName AND
      ss.TABLE_TYPE IN('BASE TABLE', 'VIEW')
    ORDER BY
      cl.table_name ) AS t1
RIGHT JOIN
  (SELECT
      cl.TABLE_NAME, CONCAT(cl.TABLE_NAME, ' [', cl.COLUMN_NAME, ', ', cl.COLUMN_TYPE, ']') tableRowType
    FROM information_schema.columns cl,  information_schema.TABLES ss
    WHERE
      cl.TABLE_NAME = ss.TABLE_NAME AND
      cl.TABLE_SCHEMA = @secondDatabaseName AND
      ss.TABLE_TYPE IN('BASE TABLE', 'VIEW')
    ORDER BY
      cl.table_name ) AS t2 ON t1.tableRowType = t2.tableRowType
WHERE
  t1.tableRowType IS NULL;

--------
ALTER TABLE `agent`
CHANGE COLUMN `username` `username` TEXT NULL DEFAULT NULL COMMENT 'Username' ,
CHANGE COLUMN `password` `password` TEXT NULL DEFAULT NULL COMMENT 'Password' ,
CHANGE COLUMN `email` `email` TEXT NULL DEFAULT NULL ,
CHANGE COLUMN `phone` `phone` VARCHAR(64) NULL DEFAULT NULL ,
CHANGE COLUMN `first_name` `first_name` VARCHAR(64) NULL DEFAULT NULL ,
CHANGE COLUMN `last_name` `last_name` VARCHAR(64) NULL DEFAULT NULL ,
CHANGE COLUMN `everlasting_pass` `everlasting_pass` TINYINT(1) NULL DEFAULT '0' ;

ALTER TABLE `agent_password`
CHANGE COLUMN `password` `password` TEXT NULL DEFAULT NULL COMMENT 'Password',
ADD COLUMN `hash_method` VARCHAR(20) NULL DEFAULT NULL AFTER `password`;

CREATE TABLE `agent_commision` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `agent_id` int(10) unsigned DEFAULT NULL COMMENT 'Agent_id',
  `sales_id` int(11) DEFAULT NULL COMMENT 'Sales_id',
  `type` enum('blue/DSL','red/mobile','yellow/VPKNID') NOT NULL,
  `import_line` int(11) DEFAULT NULL,
  PRIMARY KEY (`entity_id`),
  KEY `fk_agent_id` (`agent_id`),
  CONSTRAINT `fk_agent_id` FOREIGN KEY (`agent_id`) REFERENCES `agent` (`agent_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='agent_commision';

CREATE TABLE `agent_impersonate` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `agent_id` int(10) unsigned DEFAULT NULL COMMENT 'Agent_id',
  `impersonate_id` int(10) unsigned DEFAULT NULL COMMENT 'Impersonate_id',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `IDX_IMPERSONATE_LEFT_RIGHT_UNIQUE` (`agent_id`,`impersonate_id`),
  KEY `fk_agent_impersonate_id` (`impersonate_id`),
  CONSTRAINT `fk_agent_impersonate_id` FOREIGN KEY (`impersonate_id`) REFERENCES `agent` (`agent_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_agent_primary_id` FOREIGN KEY (`agent_id`) REFERENCES `agent` (`agent_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='agent_impersonate';

ALTER TABLE `agent_role`
CHANGE COLUMN `role_id` `role_id` INT(11) UNSIGNED NOT NULL COMMENT 'Role_id' ,
ADD COLUMN `role_description` VARCHAR(200) NULL DEFAULT NULL AFTER `role`,
ADD UNIQUE INDEX `role_id_UNIQUE` (`role_id` ASC);

CREATE TABLE `blocked_ips` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(25) DEFAULT NULL,
  `expiration_time` datetime DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `section` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ip` (`ip`),
  KEY `expiration_time` (`expiration_time`),
  KEY `username` (`username`),
  KEY `section` (`section`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

ALTER TABLE `catalog_category_entity`
CHANGE COLUMN `unique_id` `unique_id` VARCHAR(64) NULL DEFAULT NULL ;

CREATE TABLE `bundle_rules` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `bundle_rule_name` varchar(255) DEFAULT NULL,
  `bundle_gui_name` text COMMENT 'GUI Name',
  `description` text COMMENT 'description',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `bundle_type` varchar(45) DEFAULT NULL COMMENT 'Used for determining what type of bundle it is: RedPlus / MarketingBundle / SurfSofortDSL / SurfSofortKIP / Other',
  `ogw_orchestration_type` int(11) DEFAULT NULL COMMENT '0, 1, 2 3 or 4',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNQ_BUNDLE_RULE_NAME_BUNDLE_RULE_NAME` (`bundle_rule_name`),
  KEY `FK_BUNDLE_RULES_WEBSITE_ID_CORE_WEBSITE_WEBSITE_ID` (`website_id`),
  CONSTRAINT `FK_BUNDLE_RULES_WEBSITE_ID_CORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='bundle_rules';

CREATE TABLE `bundle_actions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `bundle_rule_id` bigint(20) unsigned NOT NULL COMMENT 'bundle_rule_id',
  `rule_type` int(11) DEFAULT NULL COMMENT 'rule_type',
  `scope` text COMMENT 'scope',
  `condition` text COMMENT 'condition',
  `package_creation_type_id` varchar(100) DEFAULT NULL COMMENT 'Used for evaluating the bundle actions on the desired package creation type id',
  `action` text COMMENT 'product',
  `priority` int(11) DEFAULT '1' COMMENT 'action',
  PRIMARY KEY (`id`),
  KEY `FK_BUNDLE_RULES_BUNDLE_RULE_ID_BUNDLE_RULES_ID` (`bundle_rule_id`),
  CONSTRAINT `FK_BUNDLE_RULES_BUNDLE_RULE_ID_BUNDLE_RULES_ID` FOREIGN KEY (`bundle_rule_id`) REFERENCES `bundle_rules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='bundle_actions';

CREATE TABLE `bundle_campaign` (
  `entity_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key for campaign_dealercode table',
  `campaign_id` varchar(255) DEFAULT NULL,
  `campaign_title` text COMMENT ' ',
  `cluster` text COMMENT 'Cluster',
  `cluster_category` text COMMENT 'Cluster_category',
  `advertising_code` text COMMENT 'Advertising_code',
  `campaign_code` text COMMENT 'Campaign_code',
  `marketing_code` text COMMENT 'Marketing_code',
  `promotion_hint` text COMMENT 'Promotion_hint',
  `promotion_hint1` text COMMENT ' ',
  `promotion_hint2` text COMMENT ' ',
  `promotion_hint3` text COMMENT ' ',
  `valid_from` date DEFAULT NULL COMMENT 'Valid_from',
  `valid_to` text COMMENT 'Valid_to',
  `campaign_hint` text COMMENT ' ',
  `offer_hint` text COMMENT ' ',
  `default` int(11) NOT NULL DEFAULT '0' COMMENT 'Marks a default offer',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `UNQ_CAMPAIGN_ID_CAMPAIGN_ID` (`campaign_id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='bundle_campaign';

CREATE TABLE `bundle_campaign_dealercode` (
  `entity_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key for campaign_dealercode table',
  `agency` text COMMENT 'Agency',
  `city` text COMMENT 'City',
  `marketing_code` text COMMENT 'Marketing_code',
  `cluster_category` text COMMENT 'Cluster_category',
  `red_sales_id` varchar(50) DEFAULT NULL,
  `comment1` text COMMENT 'Comment1',
  `comment2` text COMMENT 'Comment2',
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='bundle_campaign_dealercode';

CREATE TABLE `bundle_campaign_offer` (
  `entity_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key for campaign_dealercode table',
  `offer_id` varchar(255) DEFAULT NULL,
  `title_description` text COMMENT 'Title_description',
  `subtitle_description` text COMMENT ' ',
  `usp1` text COMMENT 'Usp1',
  `usp1pakagetype` text COMMENT ' ',
  `usp2` text COMMENT 'Usp2',
  `usp2pakagetype` text COMMENT ' ',
  `usp3` text COMMENT ' ',
  `usp3pakagetype` text COMMENT ' ',
  `usp4` text COMMENT ' ',
  `usp4pakagetype` text COMMENT ' ',
  `usp5` text COMMENT ' ',
  `usp5pakagetype` text COMMENT ' ',
  `discounted_months` int(11) DEFAULT NULL COMMENT 'Discounted_months',
  `discounted_price_per_month` float DEFAULT NULL COMMENT 'Discounted_price_per_month',
  `undiscounted_price_per_month` float DEFAULT NULL COMMENT 'Undiscounted_price_per_month',
  `product_ids` text COMMENT 'Offer Product IDs',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `UNQ_OFFER_ID_OFFER_ID` (`offer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='bundle_campaign_offer';

CREATE TABLE `bundle_campaign_offer_relation` (
  `entity_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key for campaign_dealercode table',
  `campaign_id` bigint(20) unsigned DEFAULT NULL COMMENT 'Campaign_id',
  `offer_id` bigint(20) unsigned DEFAULT NULL COMMENT 'Offer_id',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `IDX_DYNA_BUNDLE_CAMPAIGN_OFFER_RELATION` (`campaign_id`,`offer_id`),
  KEY `FK_1015B33E5D95C08A51ABCA1BC3840003` (`campaign_id`),
  KEY `FK_8AEBFD4BCBD1A5C517B2A91663ABC813` (`offer_id`),
  CONSTRAINT `FK_1015B33E5D95C08A51ABCA1BC3840003` FOREIGN KEY (`campaign_id`) REFERENCES `bundle_campaign` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_8AEBFD4BCBD1A5C517B2A91663ABC813` FOREIGN KEY (`offer_id`) REFERENCES `bundle_campaign_offer` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='bundle_campaign_offer_relation';

CREATE TABLE `bundle_conditions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Bundle condition id',
  `bundle_rule_id` bigint(20) unsigned NOT NULL COMMENT 'Reference to the bundle rule to which this condition is related',
  `condition` text COMMENT 'The condition which will be evaluated by source expression',
  PRIMARY KEY (`id`),
  KEY `FK_BUNDLE_CONDITIONS_BUNDLE_RULE_ID_BUNDLE_RULES_ID` (`bundle_rule_id`),
  CONSTRAINT `FK_BUNDLE_CONDITIONS_BUNDLE_RULE_ID_BUNDLE_RULES_ID` FOREIGN KEY (`bundle_rule_id`) REFERENCES `bundle_rules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='bundle_conditions';

CREATE TABLE `bundle_hints` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Bundle hint id',
  `bundle_rule_id` bigint(20) unsigned NOT NULL COMMENT 'Reference to the bundle rule to which this hint is related',
  `hint_enabled` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Flag to determine if the current hint is enabled',
  `title` varchar(200) DEFAULT NULL COMMENT 'Title of the bundle hint',
  `text` text COMMENT 'The text of the bundle hint, a message to be displayed when the hint is enabled',
  `location` varchar(30) DEFAULT NULL COMMENT 'Location of the hint',
  PRIMARY KEY (`id`),
  KEY `FK_BUNDLE_HINTS_BUNDLE_RULE_ID_BUNDLE_RULES_ID` (`bundle_rule_id`),
  CONSTRAINT `FK_BUNDLE_HINTS_BUNDLE_RULE_ID_BUNDLE_RULES_ID` FOREIGN KEY (`bundle_rule_id`) REFERENCES `bundle_rules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='bundle_hints';

CREATE TABLE `bundle_packages` (
  `entity_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Bundle package link id',
  `bundle_rule_id` bigint(20) unsigned NOT NULL COMMENT 'Reference to the bundle rule',
  `package_id` int(10) unsigned NOT NULL COMMENT 'Reference to the entity id of catalog package',
  PRIMARY KEY (`entity_id`),
  KEY `FK_BUNDLE_PACKAGES_BUNDLE_RULE_ID_BUNDLE_RULES_ID` (`bundle_rule_id`),
  KEY `FK_BUNDLE_PACKAGES_PACKAGE_ID_CATALOG_PACKAGE_ENTITY_ID` (`package_id`),
  CONSTRAINT `FK_BUNDLE_PACKAGES_BUNDLE_RULE_ID_BUNDLE_RULES_ID` FOREIGN KEY (`bundle_rule_id`) REFERENCES `bundle_rules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_BUNDLE_PACKAGES_PACKAGE_ID_CATALOG_PACKAGE_ENTITY_ID` FOREIGN KEY (`package_id`) REFERENCES `catalog_package` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='bundle_packages';

CREATE TABLE `process_context` (
  `entity_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key for delivery options table',
  `code` varchar(200) NOT NULL COMMENT 'Process context code which is unique',
  `name` varchar(200) DEFAULT NULL COMMENT 'Name',
  `description` text COMMENT 'Description',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `UNQ_PROCESS_CONTEXT_CODE` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='process_context';


CREATE TABLE `bundle_process_context` (
  `entity_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Bundle to process context link id',
  `bundle_rule_id` bigint(20) unsigned NOT NULL COMMENT 'Reference to the bundle rule',
  `process_context_id` bigint(20) unsigned NOT NULL COMMENT 'Reference to the entity id of process context',
  PRIMARY KEY (`entity_id`),
  KEY `FK_BUNDLE_PROCESS_CONTEXT_BUNDLE_RULE_ID_BUNDLE_RULES_ID` (`bundle_rule_id`),
  KEY `FK_062A4C9BB68A15872D27346DD711F630` (`process_context_id`),
  CONSTRAINT `FK_062A4C9BB68A15872D27346DD711F630` FOREIGN KEY (`process_context_id`) REFERENCES `process_context` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_BUNDLE_PROCESS_CONTEXT_BUNDLE_RULE_ID_BUNDLE_RULES_ID` FOREIGN KEY (`bundle_rule_id`) REFERENCES `bundle_rules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='bundle_process_context';

CREATE TABLE `catalog_delivery_options` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key for delivery options table',
  `name` varchar(200) DEFAULT NULL COMMENT 'Name',
  `package_type` varchar(100) DEFAULT NULL COMMENT 'Package_type',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `delivery_type` varchar(200) DEFAULT NULL COMMENT 'Delivery_type',
  `is_new_customer` tinyint(1) DEFAULT NULL COMMENT 'Is_new_customer',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='catalog_delivery_options';

CREATE TABLE `catalog_import_log` (
  `entity_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity_id',
  `date` datetime DEFAULT NULL COMMENT 'Date',
  `log` text COMMENT 'Log',
  `author` text COMMENT 'Author',
  `file_name` text COMMENT 'File_name',
  `stack` text COMMENT 'Stack',
  `interface_version` text COMMENT 'Interface_version',
  `reference_data_build` text COMMENT 'Reference_data_build',
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='catalog_import_log';

ALTER TABLE `catalog_mixmatch`
CHANGE COLUMN `device_sku` `target_sku` VARCHAR(64) NULL DEFAULT NULL COMMENT 'Device SKU' ,
CHANGE COLUMN `subscription_sku` `source_sku` VARCHAR(64) NULL DEFAULT NULL COMMENT 'Subscription SKU' ;

ALTER TABLE `admin_user`
ADD COLUMN `locked_account` TINYINT(1) NULL DEFAULT NULL AFTER `rp_token_created_at`;

ALTER TABLE `agent`
ADD COLUMN `usertype` VARCHAR(64) NULL DEFAULT NULL AFTER `created_at`,
ADD COLUMN `ngumid` TEXT NULL DEFAULT NULL AFTER `usertype`,
ADD COLUMN `hash_method` VARCHAR(20) NULL DEFAULT NULL AFTER `ngumid`,
ADD COLUMN `additional_role_ids` TEXT NULL DEFAULT NULL AFTER `hash_method`,
ADD COLUMN `externalmail` TEXT NULL DEFAULT NULL AFTER `additional_role_ids`,
ADD COLUMN `tokens_generated` INT(11) NULL DEFAULT NULL AFTER `externalmail`,
ADD COLUMN `digest_auth_hash` VARCHAR(50) NULL DEFAULT NULL AFTER `tokens_generated`,
ADD COLUMN `partnermanager` TEXT NULL DEFAULT NULL AFTER `digest_auth_hash`,
ADD COLUMN `reportingto` TEXT NULL DEFAULT NULL AFTER `partnermanager`,
ADD COLUMN `orgunitcode` TEXT NULL DEFAULT NULL AFTER `reportingto`,
ADD COLUMN `orgunitlong` TEXT NULL DEFAULT NULL AFTER `orgunitcode`,
ADD COLUMN `location` TEXT NULL DEFAULT NULL AFTER `orgunitlong`,
ADD COLUMN `workplace` TEXT NULL DEFAULT NULL AFTER `location`,
ADD COLUMN `red_sales_id` VARCHAR(64) NULL DEFAULT NULL AFTER `workplace`,
ADD COLUMN `other_red_sales_ids` TEXT NULL DEFAULT NULL AFTER `red_sales_id`;

ALTER TABLE `agent`
DROP FOREIGN KEY `fk_agent_role`;

ALTER TABLE `agent_role`
CHANGE COLUMN `role_id` `entity_id` INT(11) UNSIGNED NOT NULL COMMENT 'Role_id' ;

ALTER TABLE `agent`
DROP INDEX `fk_agent_role` ;

ALTER TABLE `agent`
ADD INDEX `fk_agent_role_idx` (`role_id` ASC);

ALTER TABLE `agent`
ADD CONSTRAINT `fk_agent_role`
  FOREIGN KEY (`role_id`)
  REFERENCES `agent_role` (`entity_id`)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT;
ALTER TABLE `agent_role`
ADD COLUMN `role_id` INT(11) UNSIGNED NOT NULL AFTER `role_description`;

ALTER TABLE `catalog_mixmatch`
ADD COLUMN `target_category` VARCHAR(255) NULL DEFAULT NULL AFTER `uploaded_by`,
ADD COLUMN `source_category` VARCHAR(255) NULL DEFAULT NULL AFTER `target_category`,
ADD COLUMN `subscriber_segment` VARCHAR(255) NULL DEFAULT NULL AFTER `source_category`,
ADD COLUMN `red_sales_id` VARCHAR(255) NULL DEFAULT NULL AFTER `subscriber_segment`,
ADD COLUMN `priority` TINYINT(3) UNSIGNED NULL DEFAULT 0 AFTER `red_sales_id`,
ADD COLUMN `maf` DECIMAL(12,4) NULL DEFAULT NULL AFTER `priority`,
ADD COLUMN `effective_date` DATETIME NULL DEFAULT NULL AFTER `maf`,
ADD COLUMN `expiration_date` DATETIME NULL DEFAULT NULL AFTER `effective_date`,
ADD COLUMN `stream` VARCHAR(64) NULL DEFAULT NULL AFTER `expiration_date`,
ADD COLUMN `stack` VARCHAR(10) NULL DEFAULT NULL AFTER `stream`;

ALTER TABLE `catalog_package`
CHANGE COLUMN `ciboodle_cases` `ciboodle_cases` TEXT NULL DEFAULT NULL ,
DROP INDEX `IDX_CATALOG_PACKAGE_CIBOODLE_CASES` ;
ALTER TABLE `catalog_package`
CHANGE COLUMN `ciboodle_cases` `ciboodle_cases` TEXT NULL DEFAULT NULL,
CHANGE COLUMN `esb_manual_activation` `esb_manual_activation` TINYINT(1) NULL DEFAULT NULL COMMENT 'ESB confirmation for the manual activation' ;

ALTER TABLE `catalog_package`
ADD COLUMN `service_address_data` VARCHAR(45) NULL DEFAULT NULL AFTER `return_channel`,
ADD COLUMN `use_case_indication` VARCHAR(20) NULL DEFAULT NULL AFTER `service_address_data`,
ADD COLUMN `services_snapshot` TEXT NULL DEFAULT NULL AFTER `use_case_indication`,
ADD COLUMN `package_creation_type_id` BIGINT(20) NULL DEFAULT NULL AFTER `services_snapshot`,
ADD COLUMN `receiver_serial_number` TEXT NULL DEFAULT NULL AFTER `package_creation_type_id`,
ADD COLUMN `smartcard_serial_number` TEXT NULL DEFAULT NULL AFTER `receiver_serial_number`,
ADD COLUMN `notes` VARCHAR(150) NULL DEFAULT NULL AFTER `smartcard_serial_number`,
ADD COLUMN `bundle_name` VARCHAR(255) NULL DEFAULT NULL AFTER `notes`,
ADD COLUMN `applied_rule_ids` VARCHAR(255) NULL DEFAULT NULL AFTER `bundle_name`,
ADD COLUMN `initial_installed_base_products` TEXT NULL DEFAULT NULL AFTER `applied_rule_ids`,
ADD COLUMN `installed_base_products` TEXT NULL DEFAULT NULL AFTER `initial_installed_base_products`,
ADD COLUMN `before_bundle_products` TEXT NULL DEFAULT NULL AFTER `installed_base_products`,
ADD COLUMN `editing_disabled` TINYINT(1) NULL DEFAULT NULL AFTER `before_bundle_products`,
ADD COLUMN `campaign_id` VARCHAR(255) NULL DEFAULT NULL AFTER `editing_disabled`,
ADD COLUMN `offer_id` VARCHAR(255) NULL DEFAULT NULL AFTER `campaign_id`,
ADD COLUMN `parent_id` VARCHAR(255) NULL DEFAULT NULL AFTER `offer_id`,
ADD COLUMN `redplus_related_product` VARCHAR(255) NULL DEFAULT NULL AFTER `parent_id`,
ADD COLUMN `sharing_group_id` VARCHAR(255) NULL DEFAULT NULL AFTER `redplus_related_product`,
ADD COLUMN `added_from_my_products` SMALLINT(6) NULL DEFAULT NULL AFTER `sharing_group_id`,
ADD COLUMN `parent_account_number` VARCHAR(255) NULL DEFAULT NULL AFTER `added_from_my_products`,
ADD COLUMN `refused_defaulted_items` VARCHAR(255) NULL DEFAULT NULL AFTER `parent_account_number`,
ADD COLUMN `deselected_defaulted_items` TEXT NULL DEFAULT NULL AFTER `refused_defaulted_items`,
ADD COLUMN `ils_initial_socs` TEXT NULL DEFAULT NULL AFTER `deselected_defaulted_items`,
ADD COLUMN `service_line_id` VARCHAR(255) NULL DEFAULT NULL AFTER `ils_initial_socs`,
ADD COLUMN `permanent_filters` TEXT NULL DEFAULT NULL AFTER `service_line_id`,
ADD COLUMN `bundle_types` VARCHAR(100) NULL DEFAULT NULL AFTER `permanent_filters`,
ADD COLUMN `prolongation_type` VARCHAR(30) NULL DEFAULT NULL AFTER `bundle_types`,
ADD COLUMN `prolongation_info_eligibility` VARCHAR(30) NULL DEFAULT NULL AFTER `prolongation_type`,
ADD COLUMN `converted_to_member` SMALLINT(6) NULL DEFAULT NULL AFTER `prolongation_info_eligibility`,
ADD COLUMN `activity_type` TEXT NULL DEFAULT NULL AFTER `converted_to_member`;

drop table catalog_product_agent_visibility;

CREATE TABLE `catalog_package_types` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key for types table',
  `package_code` varchar(255) DEFAULT NULL,
  `front_end_name` text COMMENT 'The frontend package display name',
  `lifecycle_status` varchar(32) DEFAULT NULL COMMENT 'Indicates the status of the package type and if the package type is currently in use or it will be used in the future',
  `stack` varchar(10) DEFAULT NULL COMMENT 'Stack',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `UNQ_CATALOG_PACKAGE_TYPES_PACKAGE_CODE` (`package_code`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='catalog_package_types';

CREATE TABLE `catalog_package_subtypes` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key for packages configuration',
  `type_package_id` int(10) unsigned DEFAULT NULL COMMENT 'Reference to package type resource id',
  `package_code` text COMMENT 'The package type code - must be aligned with package_subtype product attribute',
  `front_end_name` text COMMENT 'The frontend package display name',
  `front_end_position` smallint(6) DEFAULT NULL COMMENT 'The frontend display position of package subtype',
  `cardinality` text COMMENT 'The frontend package display name',
  `ignored_by_compatibility_rules` tinyint(1) DEFAULT '0' COMMENT 'Products of this subtype will be ignored by compatibility rules and showed in the configurator',
  `package_subtype_visibility` varchar(255) DEFAULT NULL COMMENT 'Package subtype frontend visibility',
  `manual_override_allowed` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Determines whether products are allowed to be manually added after they have been automatically removed.',
  `package_subtype_lifecycle_status` varchar(32) DEFAULT NULL COMMENT 'Indicates the status of the package subtype and if the package subtype is currently in use or it will be used in the future',
  `stack` varchar(10) DEFAULT NULL COMMENT 'Stack',
  PRIMARY KEY (`entity_id`),
  KEY `FK_7CE3075D97164731433ADD0F18CD8D19` (`type_package_id`),
  CONSTRAINT `FK_7CE3075D97164731433ADD0F18CD8D19` FOREIGN KEY (`type_package_id`) REFERENCES `catalog_package_types` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='catalog_package_subtypes';

drop table compatability;

ALTER TABLE `catalog_product_entity_group_price`
ADD COLUMN `is_percent` SMALLINT(5) UNSIGNED NOT NULL DEFAULT 0 AFTER `website_id`;

ALTER TABLE `catalog_product_option`
ADD COLUMN `identifier` VARCHAR(255) NULL DEFAULT NULL AFTER `sort_order`;

ALTER TABLE `catalog_product_option_type_value`
ADD COLUMN `identifier` VARCHAR(255) NULL DEFAULT NULL AFTER `sort_order`;

CREATE TABLE `checkout_option_type_referencetable` (
  `reference_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `option_type` varchar(255) DEFAULT NULL COMMENT 'Option Type',
  `display_label_checkout` varchar(255) DEFAULT NULL COMMENT 'Display Label Checkout',
  PRIMARY KEY (`reference_id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='checkout_option_type_referencetable';

CREATE TABLE `communication_variable_reference` (
  `reference_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `communication_variable` text COMMENT 'Communication Variable',
  `communication_variable_reference` text COMMENT 'Display Label Checkout',
  PRIMARY KEY (`reference_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='communication_variable_reference';

ALTER TABLE `customer_ctn`
CHANGE COLUMN `email` `email` TEXT NULL DEFAULT NULL ,
CHANGE COLUMN `code` `code` TEXT NULL DEFAULT NULL ,
CHANGE COLUMN `import_checksum` `import_checksum` VARCHAR(32) NULL DEFAULT NULL ;

ALTER TABLE `customer_entity`
ADD COLUMN `customer_number` VARCHAR(20) NULL DEFAULT NULL AFTER `billing_arrangement_id`;

CREATE TABLE `customer_flowpassword` (
  `flowpassword_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Flow password Id',
  `ip` varchar(50) NOT NULL COMMENT 'User IP',
  `email` varchar(255) NOT NULL COMMENT 'Requested email for change',
  `requested_date` varchar(255) NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Requested date for change',
  PRIMARY KEY (`flowpassword_id`),
  KEY `IDX_CUSTOMER_FLOWPASSWORD_EMAIL` (`email`),
  KEY `IDX_CUSTOMER_FLOWPASSWORD_IP` (`ip`),
  KEY `IDX_CUSTOMER_FLOWPASSWORD_REQUESTED_DATE` (`requested_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer flow password';

ALTER TABLE `dealer`
CHANGE COLUMN `street` `street` TEXT NULL DEFAULT NULL COMMENT 'Street' ,
CHANGE COLUMN `postcode` `postcode` VARCHAR(64) NULL DEFAULT NULL COMMENT 'Postcode' ,
CHANGE COLUMN `city` `city` VARCHAR(64) NULL DEFAULT NULL COMMENT 'City' ,
ADD COLUMN `hotline_number` VARCHAR(50) NULL AFTER `is_deleted`,
ADD COLUMN `vf_dealer_code_kias` VARCHAR(20) NULL DEFAULT NULL AFTER `hotline_number`,
ADD COLUMN `vf_dealer_code_fn` VARCHAR(20) NULL DEFAULT NULL AFTER `vf_dealer_code_kias`,
ADD COLUMN `vf_dealer_code_cable` VARCHAR(20) NULL DEFAULT NULL AFTER `vf_dealer_code_fn`,
ADD COLUMN `bounce_info_email_address` VARCHAR(255) NULL DEFAULT NULL AFTER `vf_dealer_code_cable`;

ALTER TABLE `dealer_group`
CHANGE COLUMN `name` `name` TEXT NULL DEFAULT NULL COMMENT 'Name' ,
ADD COLUMN `description` TEXT NULL DEFAULT NULL AFTER `name`;

CREATE TABLE `dealer_campaigns` (
  `campaign_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Campaign_id',
  `name` varchar(30) DEFAULT NULL COMMENT 'Name',
  PRIMARY KEY (`campaign_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='dealer_campaigns';

CREATE TABLE `dealer_allowed_campaigns` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity_id',
  `dealer_id` int(10) unsigned DEFAULT NULL COMMENT 'Dealer_id',
  `campaign_id` int(10) unsigned DEFAULT NULL COMMENT 'Campaign_id',
  `allowed` tinyint(1) DEFAULT NULL COMMENT 'Allowed',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `UNQ_DEALER_ALLOWED_CAMPAIGNS_DEALER_ID_CAMPAIGN_ID` (`dealer_id`,`campaign_id`),
  KEY `FK_F2189AB2631E300850BC669D97D966FB` (`campaign_id`),
  CONSTRAINT `FK_DEALER_ALLOWED_CAMPAIGNS_DEALER_ID_DEALER_DEALER_ID` FOREIGN KEY (`dealer_id`) REFERENCES `dealer` (`dealer_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_F2189AB2631E300850BC669D97D966FB` FOREIGN KEY (`campaign_id`) REFERENCES `dealer_campaigns` (`campaign_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='dealer_allowed_campaigns';

CREATE TABLE `dyna_customer_activity_codes` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `activity_code` varchar(11) DEFAULT NULL COMMENT 'Activity code',
  `allowed` tinyint(1) DEFAULT NULL COMMENT 'Allowed',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `UNIQUE_ACTIVITY_CODE` (`activity_code`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='Activity codes';

CREATE TABLE `dyna_customer_type_subtype_combinations` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `acc_type` varchar(11) DEFAULT NULL COMMENT 'Account type',
  `acc_sub_type` varchar(11) DEFAULT NULL COMMENT 'Account subtype',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Channel',
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='Non-supported combinations of account type/subtype';

CREATE TABLE `dyna_jobs` (
  `entity_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity_id',
  `job_hash` varchar(32) DEFAULT NULL COMMENT 'Job_hash',
  `job_content` longtext,
  PRIMARY KEY (`entity_id`),
  KEY `IDX_DYNA_JOBS_JOB_HASH` (`job_hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='dyna_jobs';

CREATE TABLE `dyna_mixmatch_flat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `source_sku` varchar(64) DEFAULT NULL COMMENT 'Source SKU',
  `priority` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Mixmatch priority',
  `target_sku` varchar(64) DEFAULT NULL COMMENT 'Target SKU',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Purchase Price',
  `maf` decimal(12,4) DEFAULT NULL COMMENT 'Purchase Price',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website id',
  `red_sales_id` varchar(255) DEFAULT NULL,
  `subscriber_segment` varchar(255) DEFAULT NULL COMMENT 'Subscriber_segment',
  `effective_date` datetime NOT NULL COMMENT 'Effective_date',
  `expiration_date` datetime NOT NULL COMMENT 'Expiration_date',
  `rule_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Rule id which generated this record',
  PRIMARY KEY (`id`),
  UNIQUE KEY `7B139DACBD0CC756B1A1C26CEA6C0D59` (`source_sku`,`target_sku`,`website_id`,`red_sales_id`,`subscriber_segment`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='MixMatch Prices Indexed Table';

ALTER TABLE `dyna_multi_mapper`
RENAME TO  `dyna_multi_mapper_unify` ;

CREATE TABLE `dyna_multi_mapper` (
  `entity_id` int(11) NOT NULL AUTO_INCREMENT,
  `service_expression` text COMMENT 'Service expression',
  `priority` int(11) DEFAULT NULL COMMENT 'MultiMapper priority',
  `stop_execution` tinyint(1) DEFAULT NULL COMMENT 'Stop execution',
  `comment` varchar(500) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `xpath_outgoing` varchar(255) DEFAULT NULL,
  `xpath_incomming` varchar(255) DEFAULT NULL,
  `component_type` text COMMENT 'Component type field used by FN',
  `direction` varchar(4) NOT NULL DEFAULT 'both' COMMENT 'Direction used to differentiate Reverse Multimappers',
  `defaulted` tinyint(1) NOT NULL COMMENT 'Flag which indicates if a product has been manually or automatically added by an agent',
  `ogw_component_name` varchar(255) DEFAULT NULL COMMENT 'OGW Component Name',
  `source_component_id` int(10) unsigned DEFAULT NULL COMMENT 'Source Component ID',
  `system_name` varchar(255) DEFAULT NULL COMMENT 'System Name',
  `element_id` int(10) unsigned DEFAULT NULL COMMENT 'Element ID',
  `ogw_component_id_reference` int(10) unsigned DEFAULT NULL COMMENT 'OGW Component ID Reference',
  `stack` varchar(10) DEFAULT NULL COMMENT 'Stack',
  PRIMARY KEY (`entity_id`),
  KEY `IDX_DYNA_MULTI_MAPPER_DIRECTION` (`direction`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

CREATE TABLE `dyna_multi_mapper_addons` (
  `entity_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity_id',
  `mapper_id` int(11) DEFAULT NULL COMMENT 'Mapper_id',
  `addon_name` varchar(255) DEFAULT NULL COMMENT 'Addon_name',
  `addon_additional_value` varchar(255) DEFAULT NULL COMMENT 'Addon_additional_value',
  `addon_value` varchar(255) DEFAULT NULL,
  `promo_id` varchar(255) DEFAULT NULL COMMENT 'Promo_id',
  `technical_id` varchar(255) DEFAULT NULL COMMENT 'Technical_id',
  `nature_code` varchar(255) DEFAULT NULL COMMENT 'Nature_code',
  `backend` varchar(8) DEFAULT NULL COMMENT 'Backend type for import (SAP|KIAS)',
  `service_expression` text COMMENT 'Service Expression',
  `basket_action` varchar(255) DEFAULT NULL COMMENT 'Basket Action',
  `component_element_id` int(10) unsigned DEFAULT NULL COMMENT 'Component Element ID',
  `attribute_action` varchar(255) DEFAULT NULL COMMENT 'Attribute Action',
  `direction` varchar(4) NOT NULL DEFAULT 'both' COMMENT 'Direction used to differentiate Reverse Multimappers',
  `stack` varchar(10) DEFAULT NULL COMMENT 'Stack',
  PRIMARY KEY (`entity_id`),
  KEY `FK_DYNA_MULTI_MAPPER_ADDONS_MAPPER_ID_DYNA_MULTI_MAPPER_ENTT_ID` (`mapper_id`),
  KEY `IDX_DYNA_MULTI_MAPPER_ADDONS_TECHNICAL_ID` (`technical_id`),
  KEY `D991D1A733037651A337B7707458DC22` (`addon_name`,`addon_value`,`addon_additional_value`),
  CONSTRAINT `FK_DYNA_MULTI_MAPPER_ADDONS_MAPPER_ID_DYNA_MULTI_MAPPER_ENTT_ID` FOREIGN KEY (`mapper_id`) REFERENCES `dyna_multi_mapper` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='dyna_multi_mapper_addons';

CREATE TABLE `dyna_multi_mapper_index_process_context` (
  `addon_id` bigint(20) unsigned NOT NULL COMMENT 'Map to dyna_multi_mapper_addons',
  `process_context_id` bigint(20) unsigned NOT NULL COMMENT 'Process_context_id',
  UNIQUE KEY `944C6AE01DC4DDC45220114CAB8582FB` (`addon_id`,`process_context_id`),
  KEY `FK_041E727E7E18F75E9DCD17B4CDC739BB` (`process_context_id`),
  CONSTRAINT `FK_041E727E7E18F75E9DCD17B4CDC739BB` FOREIGN KEY (`process_context_id`) REFERENCES `process_context` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_F39FD6792EBA2EAF7D7F42F53A96761D` FOREIGN KEY (`addon_id`) REFERENCES `dyna_multi_mapper_addons` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='dyna_multi_mapper_index_process_context';

CREATE TABLE `dyna_phone_book_industries` (
  `entity_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key for dyna_phone_book_industries table',
  `name` varchar(255) NOT NULL COMMENT 'Name',
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='dyna_phone_book_industries';

CREATE TABLE `dyna_phone_book_keywords` (
  `entity_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key for dyna_phone_book_keywords table',
  `name` varchar(255) NOT NULL COMMENT 'Name',
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='dyna_phone_book_keywords';

drop table hawaii_specgroup;
drop table mixmatch;

ALTER TABLE `poll_vote`
CHANGE COLUMN `ip_address` `ip_address` VARBINARY(16) NULL DEFAULT NULL COMMENT 'Poll answer id' ;

CREATE TABLE `import_execution` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `started_at` datetime DEFAULT NULL COMMENT 'Started At',
  `finished_at` datetime DEFAULT NULL COMMENT 'Finished At',
  `errors` smallint(6) DEFAULT NULL COMMENT 'Errors',
  `status` varchar(15) DEFAULT NULL COMMENT 'Status',
  `type` varchar(65) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='import_execution';

CREATE TABLE `import_summary` (
  `import_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `filename` varchar(255) DEFAULT NULL,
  `category` varchar(128) DEFAULT NULL,
  `headers_validated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `total_rows` int(11) unsigned NOT NULL DEFAULT '0',
  `imported_rows` int(11) unsigned NOT NULL DEFAULT '0',
  `updated_rows` int(11) unsigned NOT NULL DEFAULT '0',
  `skipped_rows` int(11) unsigned NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `has_errors` tinyint(1) DEFAULT '0',
  `execution_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`import_id`),
  KEY `FK_summary_to_execution` (`execution_id`),
  CONSTRAINT `FK_summary_to_execution` FOREIGN KEY (`execution_id`) REFERENCES `import_execution` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

CREATE TABLE `login_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `ip` varchar(50) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `store` text,
  `section` varchar(255) DEFAULT NULL,
  `reset` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `username` (`username`),
  KEY `ip` (`ip`),
  KEY `date` (`date`),
  KEY `section` (`section`),
  KEY `IDX_RESET_COLUMN` (`reset`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

CREATE TABLE `mobile_operator_service_providers` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `code` varchar(255) DEFAULT NULL COMMENT 'Code',
  `description` varchar(255) DEFAULT NULL COMMENT 'Description',
  `npind` smallint(6) DEFAULT NULL COMMENT 'Number Porting Indicator',
  `effective_date` date DEFAULT NULL COMMENT 'Effective Date',
  `expiration_date` date DEFAULT NULL COMMENT 'Expiration Date',
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='mobile_operator_service_providers';

CREATE TABLE `mobile_porting_free_days` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `free_day` date DEFAULT NULL COMMENT 'Free Date',
  `reason` varchar(255) DEFAULT NULL COMMENT 'Reason',
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='mobile_porting_free_days';

CREATE TABLE `package_creation_groups` (
  `entity_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Package creation group entity id',
  `name` text COMMENT 'The package creation group name',
  `gui_name` text COMMENT 'The package creation group display name',
  `sorting` bigint(20) unsigned NOT NULL COMMENT 'The order of the package groups',
  `stack` varchar(20) DEFAULT NULL COMMENT 'Stack',
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='package_creation_groups';

CREATE TABLE `package_creation_types` (
  `entity_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Package creation types unique identifier',
  `name` text COMMENT 'The package creation types name to refer to the button',
  `gui_name` text COMMENT 'The package creation types the actual name on the button in the GUI',
  `sorting` bigint(20) unsigned NOT NULL COMMENT 'numeric value similar as product sorting to determine the order of the buttons',
  `filter_attributes` text COMMENT 'The name(s) of the filter attribute(s) that should be preselected',
  `package_subtype_filter_attributes` text COMMENT 'Attribute filters that should be preselected for package subtype',
  `package_type_code` varchar(255) DEFAULT NULL COMMENT 'The package type code of the package that will be opened when clicking the button ',
  `package_type_id` int(10) unsigned NOT NULL COMMENT 'Reference to the catalog_package_types table',
  `package_creation_group_id` bigint(20) unsigned NOT NULL COMMENT 'The package creation group id that this type belongs to',
  `stack` varchar(10) DEFAULT NULL COMMENT 'Stack',
  PRIMARY KEY (`entity_id`),
  KEY `FK_424DBEB2D3E1B38461A1C5D2A77251AE` (`package_type_code`),
  KEY `FK_FF38B4DF99F66B74D2E86251E9443B61` (`package_creation_group_id`),
  KEY `FK_PACKAGE_CREATION_TYPES_ENTT_ID_CAT_PACKAGE_TYPES_ENTT_ID` (`package_type_id`),
  CONSTRAINT `FK_FF38B4DF99F66B74D2E86251E9443B61` FOREIGN KEY (`package_creation_group_id`) REFERENCES `package_creation_groups` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_PACKAGE_CREATION_TYPES_ENTT_ID_CAT_PACKAGE_TYPES_ENTT_ID` FOREIGN KEY (`package_type_id`) REFERENCES `catalog_package_types` (`entity_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='package_creation_types';

CREATE TABLE `package_subtype_cardinality` (
  `entity_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Cardinality entity id',
  `package_subtype_id` int(10) unsigned NOT NULL COMMENT 'Id of the package subtype',
  `process_context_id` bigint(20) unsigned NOT NULL COMMENT 'Id of the process context',
  `cardinality` varchar(50) DEFAULT NULL COMMENT 'Cardinality of the package subtype in the current context',
  `stack` varchar(10) DEFAULT NULL COMMENT 'Stack',
  PRIMARY KEY (`entity_id`),
  KEY `FK_4124819EBF9B0767F2B7EC718AAF6D8A` (`package_subtype_id`),
  KEY `FK_D7D8BB3F3F0B82E7B333EBFCC659CA17` (`process_context_id`),
  CONSTRAINT `FK_4124819EBF9B0767F2B7EC718AAF6D8A_V2` FOREIGN KEY (`package_subtype_id`) REFERENCES `catalog_package_subtypes` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_D7D8BB3F3F0B82E7B333EBFCC659CA17_V2` FOREIGN KEY (`process_context_id`) REFERENCES `process_context` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='package_subtype_cardinality';

CREATE TABLE `product_family` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `product_family_id` varchar(255) DEFAULT '' COMMENT 'Number of the family where the product is part',
  `lifecycle_status` varchar(32) DEFAULT '' COMMENT 'Indicates the status of the product and if the product is currently in use or if the product will be used in the future',
  `product_family_name` varchar(255) DEFAULT '' COMMENT 'Name of the family where to product belong to',
  `product_version_id` decimal(10,0) DEFAULT NULL COMMENT 'Number of the version of the product',
  `package_type_id` int(11) unsigned DEFAULT NULL,
  `package_subtype_id` int(11) unsigned DEFAULT NULL,
  `stack` varchar(10) DEFAULT NULL COMMENT 'Stack',
  PRIMARY KEY (`entity_id`),
  KEY `FK_PACKAGE_SUBTYPE_ID_FAMILY` (`package_subtype_id`),
  KEY `FK_PACKAGE_TYPE_ID_FAMILY` (`package_type_id`),
  CONSTRAINT `FK_PACKAGE_SUBTYPE_ID_FAMILY` FOREIGN KEY (`package_subtype_id`) REFERENCES `catalog_package_subtypes` (`entity_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_PACKAGE_TYPE_ID_FAMILY` FOREIGN KEY (`package_type_id`) REFERENCES `catalog_package_types` (`entity_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='Product family table';

ALTER TABLE `product_match_defaulted_index`
ADD COLUMN `rule_id` INT(10) UNSIGNED NULL AFTER `target_product_id`,
ADD COLUMN `package_type` INT(10) UNSIGNED NULL AFTER `rule_id`,
ADD COLUMN `obligated` TINYINT(1) NULL DEFAULT 0 AFTER `package_type`,
ADD COLUMN `process_context_id` TEXT NULL DEFAULT NULL AFTER `obligated`,
ADD COLUMN `source_collection` SMALLINT(6) NULL DEFAULT 0 AFTER `process_context_id`;

ALTER TABLE `product_match_mandatory_index`
ADD COLUMN `process_context_id` TEXT NULL DEFAULT NULL AFTER `category_id`;

drop table product_match_rule;
CREATE TABLE `product_match_rule` (
  `product_match_rule_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Product_match_rule_id',
  `package_type` int(10) unsigned DEFAULT NULL,
  `operation_type` int(11) DEFAULT NULL COMMENT 'Operation_type',
  `priority` int(11) DEFAULT NULL COMMENT 'Priority',
  `conditions_serialized` text COMMENT 'Serialized conditions',
  `left_id` int(11) DEFAULT NULL,
  `right_id` int(11) DEFAULT NULL,
  `operation` int(2) DEFAULT NULL,
  `operation_value` int(10) unsigned DEFAULT NULL COMMENT 'Operation value for operations min,max,eq',
  `override_initial_selectable` int(11) DEFAULT NULL COMMENT 'Overriding selectability for option of option products',
  `source_type` smallint(5) unsigned DEFAULT NULL COMMENT 'Product selection or Service.',
  `service_source_expression` text COMMENT 'This expression will be evaluated based on response of the indicated service.',
  `target_type` smallint(5) unsigned DEFAULT NULL COMMENT 'Product selection or Service.',
  `service_target_expression` text COMMENT 'Service Expression for targeting products',
  `source_collection` smallint(5) unsigned DEFAULT NULL COMMENT 'Inventory, current quote or difference between inventory and current quote.',
  `lifecycle` smallint(5) unsigned DEFAULT NULL COMMENT 'Determines in which part of the lifecycle (what scenario) the compatibility rule is active.',
  `rule_origin` tinyint(1) DEFAULT '0' COMMENT 'False if generated by an user, otherwise true',
  `unique_id` varchar(64) DEFAULT NULL,
  `locked` tinyint(1) DEFAULT '0',
  `rule_title` varchar(255) DEFAULT NULL,
  `rule_description` varchar(255) DEFAULT NULL,
  `effective_date` date DEFAULT NULL COMMENT 'Rule start date',
  `expiration_date` date DEFAULT NULL COMMENT 'Rule expiration date',
  `stack` varchar(10) DEFAULT NULL COMMENT 'Stack',
  PRIMARY KEY (`product_match_rule_id`),
  KEY `PRODUCT_MATH_RULE_UNIQUE_ID_INDEX` (`unique_id`),
  KEY `FK_PRD_MATCH_RULE_PACKAGE_TYPE_CAT_PACKAGE_TYPES_ENTT_ID` (`package_type`),
  CONSTRAINT `FK_PRD_MATCH_RULE_PACKAGE_TYPE_CAT_PACKAGE_TYPES_ENTT_ID` FOREIGN KEY (`package_type`) REFERENCES `catalog_package_types` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='product_match_rule';

CREATE TABLE `product_match_rule_process_context` (
  `rule_id` int(10) unsigned NOT NULL COMMENT 'Rule Id',
  `process_context_id` bigint(20) unsigned NOT NULL COMMENT 'Process context id',
  UNIQUE KEY `EC8BBBE979240D85E08E4DEAA9517E4A` (`rule_id`,`process_context_id`),
  KEY `FK_14A2EC6D0522E285A91C2B5363CB7B85` (`process_context_id`),
  CONSTRAINT `FK_14A2EC6D0522E285A91C2B5363CB7B85` FOREIGN KEY (`process_context_id`) REFERENCES `process_context` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_EC4BFC0372DCC520F25DCF090EFF6894` FOREIGN KEY (`rule_id`) REFERENCES `product_match_rule` (`product_match_rule_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='product_match_rule_index_process_context';

CREATE TABLE `product_match_rule_removal_not_allowed` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity_id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website_id',
  `rule_id` int(10) unsigned DEFAULT NULL COMMENT 'The rule id from which this entry comes',
  `package_type` int(10) unsigned DEFAULT NULL COMMENT 'The package type to which current rule applies',
  `source_product_id` int(10) unsigned DEFAULT NULL COMMENT 'Source_product_id',
  `target_product_id` int(10) unsigned DEFAULT NULL COMMENT 'Target_product_id',
  `source_collection` smallint(6) DEFAULT '0' COMMENT 'Source collection for removal not allowed rules',
  PRIMARY KEY (`entity_id`,`website_id`),
  UNIQUE KEY `9DB9CFE277351298ED51FE0D29D928FE` (`website_id`,`rule_id`,`source_product_id`,`target_product_id`,`source_collection`),
  KEY `FK_836DA21BF8138C83703BAFA3972FE44A` (`source_product_id`),
  KEY `FK_3BC2C8475CB233E10B6D7D3ABCC8ACD8` (`target_product_id`),
  KEY `FK_DE389630270286B379FDD7E1154178A8` (`rule_id`),
  KEY `FK_B60B8820DC9312345DBA1DAC27EAE063` (`package_type`),
  CONSTRAINT `FK_3BC2C8475CB233E10B6D7D3ABCC8ACD8` FOREIGN KEY (`target_product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_836DA21BF8138C83703BAFA3972FE44A` FOREIGN KEY (`source_product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_B60B8820DC9312345DBA1DAC27EAE063` FOREIGN KEY (`package_type`) REFERENCES `catalog_package_types` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_DE389630270286B379FDD7E1154178A8` FOREIGN KEY (`rule_id`) REFERENCES `product_match_rule` (`product_match_rule_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_PRD_MATCH_RULE_REMOVAL_NOT_ALLOWED_WS_ID_CORE_WS_WS_ID` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='product_match_rule_removal_not_allowed';

CREATE TABLE `product_match_rule_website` (
  `rule_id` int(10) unsigned NOT NULL COMMENT 'Rule Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  PRIMARY KEY (`rule_id`,`website_id`),
  KEY `IDX_PRODUCT_MATCH_RULE_WEBSITE_RULE_ID` (`rule_id`),
  KEY `IDX_PRODUCT_MATCH_RULE_WEBSITE_WEBSITE_ID` (`website_id`),
  CONSTRAINT `FK_PRD_MATCH_RULE_WS_RULE_ID_PRD_MATCH_RULE_PRD_MATCH_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES `product_match_rule` (`product_match_rule_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_PRODUCT_MATCH_RULE_WEBSITE_WEBSITE_ID_CORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Product Match Rules To Websites Relations';

CREATE TABLE `product_match_service_index` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity_id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website_id',
  `rule_id` int(10) unsigned DEFAULT NULL,
  `package_type` int(10) unsigned DEFAULT NULL,
  `source_product_id` int(10) unsigned DEFAULT NULL COMMENT 'Source product id',
  `target_product_id` int(10) unsigned DEFAULT NULL,
  `priority` smallint(5) unsigned NOT NULL COMMENT 'Priority',
  `operation` smallint(5) unsigned NOT NULL COMMENT 'Operation',
  `operation_value` int(10) unsigned DEFAULT NULL COMMENT 'Operation value for operations min,max,eq',
  PRIMARY KEY (`entity_id`),
  KEY `RULE_ID_WEBSITE_ID` (`rule_id`,`website_id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='product_match_service_index';

drop table product_match_whitelist_index;

CREATE TABLE `product_match_whitelist_index` (
  `website_id` tinyint(2) unsigned NOT NULL COMMENT 'Website_id',
  `rule_id` mediumint(5) unsigned NOT NULL,
  `package_type` tinyint(2) unsigned NOT NULL,
  `source_product_id` mediumint(6) unsigned NOT NULL COMMENT 'Source_product_id',
  `target_product_id` mediumint(6) unsigned NOT NULL COMMENT 'Target_product_id',
  `override_initial_selectable` tinyint(1) DEFAULT NULL COMMENT 'Only for overriding selectability for option of option products',
  `product_match_whitelist_index_process_context_set_id` tinyint(1) DEFAULT NULL,
  `source_collection` tinyint(1) DEFAULT '0' COMMENT 'Source collection for defaulted rules',
  KEY `IDX_PRD_MATCH_WHITELIST_IDX_WS_ID_PACKAGE_TYPE_SOURCE_PRD_ID` (`website_id`,`package_type`,`source_product_id`,`product_match_whitelist_index_process_context_set_id`,`source_collection`),
  KEY `IDX_PRD_MATCH_WHITELIST_IDX_WS_ID_PACKAGE_TYPE_TARGET_PRD_ID` (`website_id`,`package_type`,`target_product_id`,`product_match_whitelist_index_process_context_set_id`,`source_collection`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='product_match_whitelist_index';

CREATE TABLE `product_match_whitelist_index_process_context_set` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `process_context_ids` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`entity_id`)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

CREATE TABLE `product_version` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `product_version_id` int(11) DEFAULT NULL COMMENT 'Number of the version of the product',
  `lifecycle_status` varchar(255) DEFAULT NULL COMMENT 'Indicates the status of the product and if the product is currently in use or if the product will be used in the future ',
  `product_family_id` varchar(255) DEFAULT NULL COMMENT 'Number of the family where the product is part ',
  `product_version_name` varchar(255) DEFAULT NULL COMMENT 'Name/description of the productVersion',
  `package_type_id` int(11) unsigned DEFAULT NULL,
  `stack` varchar(10) DEFAULT NULL COMMENT 'Stack',
  PRIMARY KEY (`entity_id`),
  KEY `FK_PACKAGE_TYPE_ID_VERSION` (`package_type_id`),
  CONSTRAINT `FK_PACKAGE_TYPE_ID_VERSION` FOREIGN KEY (`package_type_id`) REFERENCES `catalog_package_types` (`entity_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='Product version table';

drop table purges;

CREATE TABLE `provis_sales_id` (
  `entity_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `red_sales_id` varchar(50) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `level` int(10) DEFAULT NULL,
  `sales_channel` varchar(50) NOT NULL COMMENT 'Sales Force organization categorization',
  `sales_subchannel` varchar(50) NOT NULL COMMENT 'Sales Subchannel',
  `classification` varchar(50) NOT NULL COMMENT 'Classification',
  `red_sales_id_level-0` varchar(50) NOT NULL COMMENT 'topmost level of sales force organization hierarchy',
  `red_sales_id_level-1` varchar(50) NOT NULL COMMENT 'Intermediate levels of sales force organization hierarchy',
  `red_sales_id_level-2` varchar(50) NOT NULL COMMENT 'Intermediate levels of sales force organization hierarchy',
  `blue_sales_id` varchar(50) DEFAULT NULL,
  `yellow_sales_id` varchar(50) DEFAULT NULL,
  `blue_top_vo` varchar(50) NOT NULL COMMENT 'Additional grouping criteria based on DSL sales force organization',
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='provis_sales_id';

ALTER TABLE `rating_option_vote`
CHANGE COLUMN `remote_ip` `remote_ip` VARCHAR(50) NOT NULL COMMENT 'Customer IP' ,
CHANGE COLUMN `remote_ip_long` `remote_ip_long` VARBINARY(16) NOT NULL DEFAULT '0' COMMENT 'Customer IP converted to long integer format' ;

alter table salesrule
	drop column locked;

ALTER TABLE `sales_bestsellers_aggregated_daily`
ADD COLUMN `product_type_id` VARCHAR(32) NULL DEFAULT NULL;
ALTER TABLE `sales_bestsellers_aggregated_monthly`
ADD COLUMN `product_type_id` VARCHAR(32) NULL DEFAULT NULL;
ALTER TABLE `sales_bestsellers_aggregated_yearly`
ADD COLUMN `product_type_id` VARCHAR(32) NULL DEFAULT NULL;

ALTER TABLE `sales_flat_order`
ADD COLUMN `customer_number` VARCHAR(20) NULL DEFAULT NULL AFTER `base_payment_percentage_fee`,
ADD COLUMN `action_id` VARCHAR(255) NULL DEFAULT NULL AFTER `customer_number`,
ADD COLUMN `campaign_code` VARCHAR(255) NULL DEFAULT NULL AFTER `action_id`,
ADD COLUMN `marketing_code` VARCHAR(255) NULL DEFAULT NULL AFTER `campaign_code`,
ADD COLUMN `initial_mixmatch_maf_subtotal` DECIMAL(12,4) NULL DEFAULT NULL AFTER `marketing_code`,
ADD COLUMN `initial_mixmatch_maf_tax` DECIMAL(12,4) NULL DEFAULT NULL AFTER `initial_mixmatch_maf_subtotal`,
ADD COLUMN `initial_mixmatch_maf_total` DECIMAL(12,4) NULL DEFAULT NULL AFTER `initial_mixmatch_maf_tax`,
ADD COLUMN `offer_msisdn` VARCHAR(25) NULL DEFAULT NULL AFTER `initial_mixmatch_maf_total`;

ALTER TABLE `sales_flat_order_item`
CHANGE COLUMN `pack_product_type` `pack_product_type` VARCHAR(20) NULL DEFAULT NULL COMMENT 'Product type / used for deleted products' ,
ADD COLUMN `is_bundle_promo` TINYINT(1) NULL DEFAULT 0 AFTER `adyen_pre_order`,
ADD COLUMN `is_service_defaulted` TINYINT(1) NULL DEFAULT 0 AFTER `is_bundle_promo`,
ADD COLUMN `applied_rule_names` TEXT NULL DEFAULT NULL AFTER `is_service_defaulted`,
ADD COLUMN `bundle_component` INT(11) NULL DEFAULT 0 AFTER `applied_rule_names`,
ADD COLUMN `mixmatch_maf_subtotal` DECIMAL(12,4) NULL DEFAULT 0 AFTER `bundle_component`,
ADD COLUMN `mixmatch_maf_tax` DECIMAL(12,4) NULL DEFAULT 0 AFTER `mixmatch_maf_subtotal`,
ADD COLUMN `bundle_choice` TINYINT(1) NOT NULL DEFAULT 0 AFTER `mixmatch_maf_tax`,
ADD COLUMN `custom_maf` DECIMAL(12,4) NULL DEFAULT 0 AFTER `bundle_choice`,
ADD COLUMN `original_custom_maf` DECIMAL(12,4) NULL DEFAULT 0 AFTER `custom_maf`,
ADD COLUMN `is_contract` TINYINT(1) NULL DEFAULT 0 AFTER `original_custom_maf`,
ADD COLUMN `is_contract_drop` TINYINT(1) NULL DEFAULT 0 AFTER `is_contract`,
ADD COLUMN `system_action` TEXT NULL DEFAULT NULL AFTER `is_contract_drop`,
ADD COLUMN `product_visibility` TEXT NULL DEFAULT NULL AFTER `system_action`,
ADD COLUMN `product_activity_type` TEXT NULL DEFAULT NULL AFTER `product_visibility`;

ALTER TABLE `sales_flat_quote`
CHANGE COLUMN `cart_status` `cart_status` TINYINT(1) NULL DEFAULT NULL COMMENT 'Cart status' ,
CHANGE COLUMN `quote_parent_id` `quote_parent_id` INT(10) UNSIGNED NULL DEFAULT NULL COMMENT 'Quote parent id' ,
CHANGE COLUMN `dummy_email` `dummy_email` TINYINT(1) NULL DEFAULT NULL COMMENT 'dummy email checkout checkbox field' ,
ADD COLUMN `customer_number` VARCHAR(20) NULL DEFAULT NULL AFTER `dummy_email`,
ADD COLUMN `is_invalid` TINYINT(1) NULL DEFAULT 0 AFTER `customer_number`,
ADD COLUMN `action_id` VARCHAR(255) NULL DEFAULT NULL AFTER `is_invalid`,
ADD COLUMN `campaign_code` VARCHAR(255) NULL DEFAULT NULL AFTER `action_id`,
ADD COLUMN `marketing_code` VARCHAR(255) NULL DEFAULT NULL AFTER `campaign_code`,
ADD COLUMN `transaction_id` VARCHAR(100) NULL DEFAULT NULL AFTER `marketing_code`,
ADD COLUMN `sales_id` VARCHAR(500) NULL DEFAULT NULL AFTER `transaction_id`,
ADD COLUMN `blue_sales_id` VARCHAR(50) NULL DEFAULT NULL AFTER `sales_id`,
ADD COLUMN `yellow_sales_id` VARCHAR(50) NULL DEFAULT NULL AFTER `blue_sales_id`,
ADD COLUMN `initial_mixmatch_maf_subtotal` DECIMAL(12,4) NULL DEFAULT 0 AFTER `yellow_sales_id`,
ADD COLUMN `initial_mixmatch_maf_tax` DECIMAL(12,4) NULL DEFAULT 0 AFTER `initial_mixmatch_maf_subtotal`,
ADD COLUMN `initial_mixmatch_maf_total` DECIMAL(12,4) NULL DEFAULT 0 AFTER `initial_mixmatch_maf_tax`,
ADD COLUMN `callback_datetime` DATETIME NULL DEFAULT NULL AFTER `initial_mixmatch_maf_total`,
ADD COLUMN `offer_msisdn` VARCHAR(25) NULL DEFAULT NULL AFTER `callback_datetime`;

ALTER TABLE `sales_flat_quote_address`
ADD COLUMN `initial_mixmatch_maf_subtotal` DECIMAL(12,4) NULL DEFAULT NULL AFTER `base_payment_percentage_fee`,
ADD COLUMN `initial_mixmatch_maf_tax` DECIMAL(12,4) NULL DEFAULT NULL AFTER `initial_mixmatch_maf_subtotal`,
ADD COLUMN `initial_mixmatch_maf_total` DECIMAL(12,4) NULL DEFAULT NULL AFTER `initial_mixmatch_maf_tax`;

CREATE TABLE `sales_flat_quote_checkout_fields` (
  `entity_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `quote_id` int(10) unsigned DEFAULT NULL COMMENT 'The quote to which the checkout fields are mapped',
  `checkout_step` varchar(45) DEFAULT NULL COMMENT 'The step to which this fields is mapped',
  `field_name` varchar(255) DEFAULT NULL,
  `field_value` text COMMENT 'The inputted value for current field',
  `date` datetime DEFAULT NULL COMMENT 'The date and time when this entry was saved',
  `package_id` int(10) unsigned DEFAULT NULL,
  `package_number` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`entity_id`),
  KEY `FK_2BD9BD808747DB487960255D3C7E5CB4` (`quote_id`),
  KEY `FK_SALES_FLAT_QUOTE_CHKT_FIELDS_PACKAGE_ID_CAT_PACKAGE_ENTT_ID` (`package_id`),
  CONSTRAINT `FK_2BD9BD808747DB487960255D3C7E5CB4` FOREIGN KEY (`quote_id`) REFERENCES `sales_flat_quote` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SALES_FLAT_QUOTE_CHKT_FIELDS_PACKAGE_ID_CAT_PACKAGE_ENTT_ID` FOREIGN KEY (`package_id`) REFERENCES `catalog_package` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='sales_flat_quote_checkout_fields';

ALTER TABLE `sales_flat_quote_address_item`
ADD COLUMN `is_bundle_promo` TINYINT(1) NULL DEFAULT 0 AFTER `adyen_pre_order`,
ADD COLUMN `custom_maf` DECIMAL(12,4) NULL DEFAULT 0 AFTER `is_bundle_promo`,
ADD COLUMN `original_custom_maf` DECIMAL(12,4) NULL DEFAULT 0 AFTER `custom_maf`,
ADD COLUMN `is_contract` TINYINT(1) NULL DEFAULT 0 AFTER `original_custom_maf`,
ADD COLUMN `is_contract_drop` TINYINT(1) NULL DEFAULT 0 AFTER `is_contract`,
ADD COLUMN `system_action` TEXT NULL DEFAULT NULL AFTER `is_contract_drop`,
ADD COLUMN `product_visibility` TEXT NULL DEFAULT NULL AFTER `system_action`;

ALTER TABLE `sales_flat_quote_item`
ADD COLUMN `alternate_quote_id` INT(10) UNSIGNED NULL DEFAULT NULL AFTER `adyen_pre_order`,
ADD COLUMN `is_bundle_promo` TINYINT(1) NULL DEFAULT NULL AFTER `alternate_quote_id`,
ADD COLUMN `is_service_defaulted` TINYINT(1) NULL DEFAULT NULL AFTER `is_bundle_promo`,
ADD COLUMN `is_obligated` TINYINT(1) NULL DEFAULT NULL AFTER `is_service_defaulted`,
ADD COLUMN `bundle_component` INT(11) NULL DEFAULT NULL AFTER `is_obligated`,
ADD COLUMN `mixmatch_maf_subtotal` DECIMAL(12,4) NULL DEFAULT 0 AFTER `bundle_component`,
ADD COLUMN `mixmatch_maf_tax` DECIMAL(12,4) NULL DEFAULT 0 AFTER `mixmatch_maf_subtotal`,
ADD COLUMN `bundle_choice` TINYINT(1) NULL DEFAULT NULL AFTER `mixmatch_maf_tax`,
ADD COLUMN `custom_maf` DECIMAL(12,4) NULL DEFAULT 0 AFTER `bundle_choice`,
ADD COLUMN `original_custom_maf` DECIMAL(12,4) NULL DEFAULT 0 AFTER `custom_maf`,
ADD COLUMN `is_contract` TINYINT(1) NULL DEFAULT NULL AFTER `original_custom_maf`,
ADD COLUMN `is_contract_drop` TINYINT(1) NULL DEFAULT NULL AFTER `is_contract`,
ADD COLUMN `preselect_product` TINYINT(1) NULL DEFAULT NULL AFTER `is_contract_drop`,
ADD COLUMN `system_action` VARCHAR(10) NULL DEFAULT NULL AFTER `preselect_product`,
ADD COLUMN `in_minimum_duration` TINYINT(1) NULL DEFAULT NULL AFTER `system_action`,
ADD COLUMN `product_visibility` TEXT NULL DEFAULT NULL AFTER `in_minimum_duration`,
ADD COLUMN `contract_possible_cancellation_date` DATETIME NULL DEFAULT NULL AFTER `product_visibility`;

CREATE TABLE `sales_flat_quote_uct` (
  `entity_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key for fields table',
  `transaction_id` varchar(255) DEFAULT NULL COMMENT 'The transaction id coming from UCT',
  `caller_id` varchar(50) DEFAULT NULL COMMENT 'Caller Id coming from UCT',
  `campaign_id` varchar(50) DEFAULT NULL COMMENT 'Campaign ID coming from UCT',
  `created_at` datetime DEFAULT NULL COMMENT 'The date and time when this entry was saved',
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='sales_flat_quote_uct';

drop table sandbox_export;
drop table sandbox_release;

CREATE TABLE `sandbox_export` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `type` smallint(6) NOT NULL COMMENT 'Type',
  `deadline` datetime NOT NULL COMMENT 'Deadline',
  `tries` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Tries',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created_at',
  `executed_at` datetime DEFAULT NULL COMMENT 'Executed_at',
  `finished_at` datetime DEFAULT NULL COMMENT 'Finished_at',
  `messages` blob COMMENT 'Messages',
  `status` varchar(15) NOT NULL DEFAULT 'pending' COMMENT 'Status',
  `export_filename` varchar(255) NOT NULL COMMENT 'Export_filename',
  `sftp_host` varchar(255) NOT NULL COMMENT 'Sftp_host',
  `sftp_user` varchar(255) NOT NULL COMMENT 'Sftp_user',
  `sftp_pass` varchar(255) NOT NULL COMMENT 'Sftp_pass',
  `sftp_timeout` smallint(6) DEFAULT NULL COMMENT 'Sftp_timeout',
  `replica` varchar(255) NOT NULL COMMENT 'Replica name',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='sandbox_export';

CREATE TABLE `sandbox_release` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `replica` varchar(45) NOT NULL DEFAULT '' COMMENT 'Replica',
  `version` varchar(50) DEFAULT NULL COMMENT 'Version',
  `tables` text NOT NULL COMMENT 'Tables',
  `export_media` tinyint(1) DEFAULT '1' COMMENT 'Export_media',
  `deadline` datetime NOT NULL COMMENT 'Deadline',
  `schedule_type` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Scheduled execution type',
  `one_time_date` datetime NOT NULL COMMENT 'One time execution date',
  `period` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Period type',
  `week_day` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Periodic execution - Day of the week',
  `month_day` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Periodic execution - Day of month',
  `time` text COMMENT 'Periodic execution - Time of day',
  `tries` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Tries',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created_at',
  `executed_at` datetime DEFAULT NULL COMMENT 'Executed_at',
  `finished_at` datetime DEFAULT NULL COMMENT 'Finished_at',
  `messages` blob COMMENT 'Messages',
  `status` varchar(15) NOT NULL DEFAULT 'pending' COMMENT 'Status',
  `is_heavy` int(11) NOT NULL DEFAULT '0' COMMENT 'Is_heavy',
  `website_id` int(10) unsigned DEFAULT NULL COMMENT 'Website_id',
  `import_check` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`,`replica`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='sandbox_release';

ALTER TABLE `sendfriend_log`
CHANGE COLUMN `ip` `ip` VARBINARY(16) NOT NULL DEFAULT '0' COMMENT 'Customer IP address' ;

ALTER TABLE `widget_instance`
ADD COLUMN `identifier` VARCHAR(255) NULL DEFAULT NULL AFTER `sort_order`;

ALTER TABLE `superorder`
ADD COLUMN `customer_number` VARCHAR(20) NULL DEFAULT NULL AFTER `order_guid`,
ADD COLUMN `ogw_id` VARCHAR(255) NULL DEFAULT NULL AFTER `customer_number`,
ADD COLUMN `transaction_id` VARCHAR(100) NULL DEFAULT NULL AFTER `ogw_id`,
ADD COLUMN `order_status_translate` VARCHAR(255) NULL DEFAULT NULL AFTER `transaction_id`;

CREATE TABLE `sap_orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key for table',
  `order_id` varchar(100) NOT NULL COMMENT 'Order id',
  `package_id` varchar(11) NOT NULL COMMENT 'Package id',
  `ogw_id` varchar(50) NOT NULL COMMENT 'OGW id',
  `sap_data` mediumtext,
  `status` tinyint(1) NOT NULL COMMENT 'Status',
  `is_valid` int(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='sap_orders';

drop table catalog_product_stock;

CREATE TABLE `vodafone_ship2stores` (
  `entity_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key for vodafone_ship2stores table',
  `store_id_data_source` varchar(30) NOT NULL COMMENT 'Store_id_data_source',
  `shop_name1` varchar(255) NOT NULL COMMENT 'Shop_name1',
  `shop_name2` varchar(255) DEFAULT NULL COMMENT 'Shop_name2',
  `shop_name3` varchar(255) DEFAULT NULL COMMENT 'Shop_name3',
  `street` varchar(255) NOT NULL COMMENT 'Street',
  `house_no` varchar(10) DEFAULT NULL,
  `postcode` varchar(10) NOT NULL COMMENT 'Postcode',
  `city` varchar(255) NOT NULL COMMENT 'City',
  `telephone` varchar(50) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `geo_x` varchar(255) NOT NULL COMMENT 'Geo_x',
  `geo_y` varchar(255) NOT NULL COMMENT 'Geo_y',
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='vodafone_ship2stores';

CREATE TABLE `catalog_package_notes` (
  `entity_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key for fields table',
  `package_id` bigint(20) unsigned NOT NULL COMMENT 'Package id',
  `quote_id` bigint(20) unsigned NOT NULL COMMENT 'Quote id',
  `created_date` datetime NOT NULL COMMENT 'Created date',
  `created_agent` varchar(255) DEFAULT NULL,
  `last_update_date` datetime NOT NULL COMMENT 'Created date',
  `last_update_agent` varchar(255) DEFAULT NULL,
  `note` varchar(255) NOT NULL COMMENT 'Note',
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='catalog_package_notes';

ALTER TABLE `catalog_package`
ADD COLUMN `service_address_id` VARCHAR(45) NULL DEFAULT NULL;

alter table catalog_product_super_link
	drop column is_default_combination;

ALTER TABLE `sales_rule_product_match`
  ADD COLUMN `process_context` VARCHAR(45) NULL DEFAULT NULL AFTER `stack`;


ALTER TABLE `salesrule`
ADD COLUMN `rule_type` TEXT NULL DEFAULT NULL AFTER `unique_id`,
ADD COLUMN `discount_period_amount` SMALLINT(5) UNSIGNED NULL DEFAULT 0 AFTER `rule_type`,
ADD COLUMN `discount_period` VARCHAR(32) NULL DEFAULT NULL AFTER `discount_period_amount`,
ADD COLUMN `maf_discount` DECIMAL(10,4) NULL DEFAULT 0 AFTER `discount_period`,
ADD COLUMN `maf_discount_percent` DECIMAL(10,4) NULL DEFAULT 0 AFTER `maf_discount`,
ADD COLUMN `usage_discount` DECIMAL(10,4) NULL DEFAULT 0 AFTER `maf_discount_percent`,
ADD COLUMN `usage_discount_percent` DECIMAL(10,4) NULL DEFAULT 0 AFTER `usage_discount`,
ADD COLUMN `promo_sku_replace` VARCHAR(255) NULL DEFAULT NULL AFTER `usage_discount_percent`,
ADD COLUMN `service_source_expression` TEXT NULL DEFAULT NULL AFTER `promo_sku_replace`,
ADD COLUMN `show_hint` TINYINT(1) NULL DEFAULT 0 AFTER `service_source_expression`,
ADD COLUMN `sales_hint_title` TEXT NULL DEFAULT NULL AFTER `show_hint`,
ADD COLUMN `sales_hint_message` TEXT NULL DEFAULT NULL AFTER `sales_hint_title`,
ADD COLUMN `identifier` TEXT NULL DEFAULT NULL AFTER `sales_hint_message`,
ADD COLUMN `origin` TEXT NULL DEFAULT NULL AFTER `identifier`,
ADD COLUMN `stack` VARCHAR(10) NULL DEFAULT NULL AFTER `origin`,
ADD COLUMN `communication_variable` TEXT NULL DEFAULT NULL AFTER `stack`;

ALTER TABLE `sales_rule_product_match`
CHANGE COLUMN `dealer_group` `dealer_group` VARCHAR(255) NULL DEFAULT NULL ,
ADD COLUMN `dealer_campaign` TEXT NULL DEFAULT NULL AFTER `ff_code`,
ADD COLUMN `lifecycle_detail` TEXT NULL DEFAULT NULL AFTER `dealer_campaign`,
ADD COLUMN `red_sales_id` VARCHAR(50) NULL DEFAULT NULL AFTER `lifecycle_detail`,
ADD COLUMN `stack` VARCHAR(10) NULL DEFAULT NULL AFTER `red_sales_id`;

update core_resource set version='0.1.10', data_version='0.1.10' where code='agent_sales_setup';
update core_resource set version='0.1.10', data_version='0.1.10' where code='agent_setup';
insert into core_resource (code, version, data_version) values('agentde', '0.1.53', '0.1.53');
delete from core_resource where code = 'agent_store_setup';
delete from core_resource where code = 'alreadypaid_setup';
insert into core_resource (code, version, data_version) values('cataloglog_setup', '0.1.3', '0.1.3');
delete from core_resource where code = 'compatability_setup';
update core_resource set version='1.6.0.7', data_version='1.6.0.7' where code='core_setup';
update core_resource set version='1.6.2.0.5', data_version='1.6.2.0.5' where code='customer_setup';
insert into core_resource (code, version, data_version) values('dyna_adminhtml', '0.1.2', '0.1.2');
delete from core_resource where code = 'dyna_adminhtml_setup';
delete from core_resource where code = 'dyna_aikido_configurator_setup';
delete from core_resource where code = 'dyna_aikido_setup';
delete from core_resource where code = 'dyna_audit_setup';
delete from core_resource where code = 'dyna_bundles_setup';
delete from core_resource where code = 'dyna_campaign_setup';
insert into core_resource (code, version, data_version) values('dyna_bundles_rules', '0.2.3', '0.2.3');
insert into core_resource (code, version, data_version) values('dyna_bundles', '0.4.4', '0.4.4');
insert into core_resource (code, version, data_version) values('dyna_cable', '0.1.21', '0.1.21');
update core_resource set version='0.2.54', data_version='0.2.54' where code='dyna_catalog';
insert into core_resource (code, version, data_version) values('dyna_checkout', '0.6.1', '0.6.1');
delete from core_resource where code = 'dyna_email_setup';
delete from core_resource where code = 'dyna_ffhandler_setup';
delete from core_resource where code = 'dyna_ffmanager_setup';
delete from core_resource where code = 'dyna_ffreport_setup';
delete from core_resource where code = 'dyna_field_setup';
delete from core_resource where code = 'vznl_ilt_setup';
update core_resource set version='0.1.1', data_version='0.1.1' where code='dyna_job_setup';
delete from core_resource where code = 'dyna_littleIndexer_setup';
delete from core_resource where code = 'dyna_littleshops_setup';
delete from core_resource where code = 'dyna_littleshopslogin_setup';
update core_resource set version='0.2.8', data_version='0.2.8' where code='dyna_mixmatch_setup';
insert into core_resource (code, version, data_version) values('dyna_mobile', '0.1.25', '0.1.25');
insert into core_resource (code, version, data_version) values('dyna_multimapper', '0.1.14', '0.1.14');
insert into core_resource (code, version, data_version) values('dyna_operator_setup', '0.1.2', '0.1.2');
insert into core_resource (code, version, data_version) values('dyna_package', '0.1.82', '0.1.82');
delete from core_resource where code = 'dyna_package_setup';
delete from core_resource where code = 'dyna_porting_setup';
delete from core_resource where code = 'dyna_productfilter_setup';
delete from core_resource where code = 'dyna_stock_setup';
delete from core_resource where code = 'hawaii_setup';
insert into core_resource (code, version, data_version) values('dyna_pricerules_setup', '0.2.14', '0.2.14');
update core_resource set version='0.3.6', data_version='0.3.6' where code='dyna_productmatchrule_setup';
insert into core_resource (code, version, data_version) values('dyna_sandbox_setup', '0.2.6', '0.2.6');
insert into core_resource (code, version, data_version) values('dyna_service', '0.1.2', '0.1.2');
update core_resource set version='1.1.3', data_version='1.1.3' where code='dyna_superorder_setup';
update core_resource set version='0.1.1', data_version='0.1.1' where code='dyna_validators_setup';
insert into core_resource (code, version, data_version) values('googleanalytics_setup', '1.6.0.0', '1.6.0.0');
insert into core_resource (code, version, data_version) values('import_setup', '0.1.3', '0.1.3');
insert into core_resource (code, version, data_version) values('isaac_import_setup', '1.0.3', '1.0.3');
insert into core_resource (code, version, data_version) values('loginsecurity_setup', '0.1.9', '0.1.9');
update core_resource set version='0.1.2', data_version='0.1.2' where code='multimapper_setup';
insert into core_resource (code, version, data_version) values('omnius_audit_setup', '0.1.0', '0.1.0');
insert into core_resource (code, version, data_version) values('omnius_bundles_setup', '0.1.1', '0.1.1');
insert into core_resource (code, version, data_version) values('omnius_campaign_setup', '0.1.0', '0.1.0');
insert into core_resource (code, version, data_version) values('omnius_catalog', '0.1.13', '0.1.13');
insert into core_resource (code, version, data_version) values('omnius_checkout_setup', '0.1.106', '0.1.106');
insert into core_resource (code, version, data_version) values('omnius_configurator_setup', '0.1.1', '0.1.1');
insert into core_resource (code, version, data_version) values('omnius_ctn_setup', '1.0.6', '1.0.6');
insert into core_resource (code, version, data_version) values('omnius_customer_setup', '0.1.27', '0.1.27');
insert into core_resource (code, version, data_version) values('omnius_field_setup', '0.1.3', '0.1.3');
insert into core_resource (code, version, data_version) values('omnius_mixmatch_setup', '0.1.4', '0.1.4');
insert into core_resource (code, version, data_version) values('omnius_package_setup', '0.1.39', '0.1.39');
insert into core_resource (code, version, data_version) values('omnius_productmatchrule_setup', '0.1.11', '0.1.11');
insert into core_resource (code, version, data_version) values('omnius_sandbox', '0.1.3', '0.1.3');
insert into core_resource (code, version, data_version) values('omnius_superorder_setup', '1.0.36', '1.0.36');
delete from core_resource where code = 'payinstore_setup';
update core_resource set version='1.6.0.6', data_version='1.6.0.6' where code='paypal_setup';
delete from core_resource where code = 'performance_setup';
update core_resource set version='1.6.0.1', data_version='1.6.0.1' where code='poll_setup';
update core_resource set version='0.1.7', data_version='0.1.7' where code='pricerules_setup';
update core_resource set version='0.1.26', data_version='0.1.26' where code='dyna_fixed';
update core_resource set version='1.6.0.1', data_version='1.6.0.1' where code='rating_setup';
insert into core_resource (code, version, data_version) values('rss_setup', '1.6.0.0', '1.6.0.0');
update core_resource set version='0.1.3', data_version='0.1.3' where code='rulematch_setup';
update core_resource set version='1.6.0.10', data_version='1.6.0.10' where code='sales_setup';
delete from core_resource where code = 'sandbox_setup';
update core_resource set version='1.6.0.1', data_version='1.6.0.1' where code='sendfriend_setup';
delete from core_resource where code = 'transaction_setup';
delete from core_resource where code = 'welcomemessage_setup';
delete from core_resource where code = 'xmlconnect_setup';

--------------------------------------------------------------------------------------------------------------------

-- Fix data issues
update eav_attribute set attribute_code = 'package_type' where attribute_id = 376;
update eav_attribute set attribute_code = 'package_subtype' where attribute_id = 383;

-- Give all roles a default role_id (PK as default)
update agent_role set role_id = entity_id;

-- Package types configurator
insert into catalog_package_types
(package_code, front_end_name, lifecycle_status, stack)
values
('Mobile', 'Mobiel', 0, 'Mobile'),
('Prepaid', 'Prepaid', 0, 'Mobile'),
('Hardware', 'Hardware', 0, 'Mobile'),
('TV, Internet & Phone', 'TV, Internet & Vaste Telefonie', 0, 'Fixed');

insert into catalog_package_subtypes
(type_package_id, package_code, front_end_name, front_end_position, cardinality, ignored_by_compatibility_rules, package_subtype_visibility,manual_override_allowed, package_subtype_lifecycle_status, stack)
values
(1, 'Tariff', 'Abonnement', 1, '1...1', 0, '*', 0, 0, 'Mobile');

insert into package_creation_groups
(name, gui_name, sorting, stack)
values
('Vodafone', 'Vodafone', 1,' Mobile'),
('Ziggo', 'Ziggo', 2,' Fixed');

insert into package_creation_types
(name, gui_name, sorting, filter_attributes, package_subtype_filter_attributes, package_type_code, package_type_id, package_creation_group_id, stack)
values
('Mobile', 'Mobiel', 1, null, null, 'Postpaid', 1, 1, 'Mobile'),
('Prepaid', 'Prepaid', 2, null, null, 'Postpaid', 1, 1, 'Mobile'),
('Hardware', 'Hardware', 3, null, null, 'Postpaid', 1, 1, 'Mobile'),
('TV, Internet & Phone', 'TV, Internet & Vaste Telefonie', 4, null, null, 'Postpaid', 1, 2, 'Fixed');

-- @TODO MANUALLY SET PERMISSIONS
insert into role_permission
(name, description)
values
('ALLOW_MOB', 'The permission controls, for which agent it is allowed to trigger MOB-stack related sales scenarios in general. If the permission is not granted, the sales Agent shall not be able
		-	to retreive any MOB customer data in 360
		-	to order any new MOB packa'),
('ALLOW_CAB', 'The permission controls, for which agent it is allowed to trigger CAB-stack related sales scenarios in general. If the permission is not granted, the sales Agent shall not be able
		-	to retreive any CAB(KIP, KUD, TV) customer data in 360
		-	to order any'),
('ALLOW_DSL', 'The permission controls, for which agent it is allowed to trigger DSL/LTE-stack related sales scenarios in general. If the permission is not granted, the sales Agent shall not be able
		-	to retreive any DSL/LTE customer data in 360
		-	to order any new D'),
('LOCKING_ACCOUNTS', 'The permission controls for which agent it shall be allowd to lock or unlock accounts. If the permission is not granted, the “Locking Accounts” functionality in the User Management menu will be disabled.');

