<?php

require_once '../abstract.php';

/**
 * Class Dyna_Logstash_Config_Generator
 */
class Dyna_Logstash_Config_Generator extends Mage_Shell_Abstract
{

    #WARNING: The LNP and ACC/PROD patterns directories have a different location : /etc/logstash/conf.d/patterns/
    #(this needs to be changed manually inside the filter files)

    protected $environments = [
        'st' => [ #local testing environment
            'st1' => [
                'output_hosts' => '127.0.0.1:9200',
                'redis_host' => 'kibana.test.dynalean.eu',
                'redis_port' => 6379,
                'db' => 8,
                'nodes' => [
                    'LNXVFDEWEB101-T',
                ],
            ]
        ],
        'sit' => [ #System Integration testing
            'sit1' => [
                'output_hosts' => '10.97.109.74:9200',
                'redis_host' => 'localhost',
                'redis_port' => 6282,
                'db' => 8,
                'nodes' => [
                    'deossvvr',
                    'deosswvr',
                    'deossxvr',
                    'deossyvr'
                ],
            ],
            'sit2' => [
                'output_hosts' => '10.97.109.74:9201',
                'redis_host' => 'localhost',
                'redis_port' => 6380,
                'db' => 8,
                'nodes' => [
                    'deossvvr',
                    'deosswvr',
                    'deossxvr',
                    'deossyvr'
                ],
            ],
            'sit3' => [
                'output_hosts' => '10.97.109.74:9202',
                'redis_host' => 'localhost',
                'redis_port' => 6381,
                'db' => 8,
                'nodes' => [
                    'deossvvr',
                    'deosswvr',
                    'deossxvr',
                    'deossyvr'
                ],
            ],
            'sit4' => [
                'output_hosts' => '10.97.109.74:9203',
                'redis_host' => 'localhost',
                'redis_port' => 6382,
                'db' => 8,
                'nodes' => [
                    'deossvvr',
                    'deosswvr',
                    'deossxvr',
                    'deossyvr'
                ],
            ],
        ],
        'lnp' => [ #Load and performance testing
            'lnp' => [
                'output_hosts' => '10.97.109.122:9200',
                'redis_host' => 'localhost',
                'redis_port' => 6379,
                'db' => 8,
                'nodes' => [
                    'deosljvr',
                    'deoslkvr',
                    'deosllvr',
                    'deoslmvr',
                    'deoslnvr',
                    'deoslovr',
                    'deoslpvr',
                ],
            ]
        ],
        'acc' => [ #Acceptance testing
            'acc1' => [
                'output_hosts' => 'https://osfacc-dyn-elast-srch.vodafone.de:9200',
                'redis_host' => '46.190.171.52',
                'redis_port' => 6379,
                'db' => 8,
                'nodes' => [
                    'deosamvr',
                    'deosanvr',
                    'deosaivr',
                    'deosajvr',
                ],
            ]
        ],
        'prod' => [ #Production
            'prod1' => [
                'output_hosts' => 'https://osfprd-dyn-elast-srch.vodafone.de:9200',
                'redis_host' => '195.233.7.193',
                'redis_port' => 6379,
                'db' => 8,
                'nodes' => [
                    'deospaivr',
                    'deospajvr',
                    'deospakvr',
                    'deospalvr',
                    'deospamvr',
                    'deospanvr',
                    'deospaovr',
                    'deospapvr',
                    'deospaqvr',
                    'deosparvr',
                    'deospasvr',
                    'deospatvr',
                    'deospauvr',
                    'deospavvr',
                    'deospaxvr',
                    'deospbavr',
                    'deospbbvr',
                ],
            ]
        ],
    ];
    public $currentEnvironment;
    public $skipFiles = ['.', '..'];
    public $context = ['conf.d', 'patterns'];
    private $debugMode;

    public function __construct()
    {
        parent::__construct();
        if(in_array($this->getArg('environment'), array_keys($this->environments))){
            $this->currentEnvironment = $this->getArg('environment');
        } else {
            throw new Exception('Environment not found');
        }

        $this->debugMode = $this->getArg('debug') === "y";
    }

    /**
     * Run script
     */
    public function run()
    {
        // Cleanup old mess
        $this->cleanup();

        // Lets create env root dirs
        $this->createRootDirectories();

        // Parse configs
        $configs = scandir('./conf.d/');
        $patterns = scandir('./patterns/');

        foreach($this->environments[$this->currentEnvironment] as $env => $data)
        {
            // Generate conf.d
            $this->log('Generating conf.d for "'.$env.':');
            foreach ($configs as $file)
            {
                // Skip if not needed
                if (!in_array($file, $this->skipFiles) && $file !== basename(__FILE__)) {
                    $this->log('- '.$file);
                    $content = file_get_contents('./conf.d/' . $file);

                    if ($file == "output.conf")
                    {
                        if ($this->debugMode) {
                            $content = str_replace('stdout OUTPUTMODE', "stdout { codec => rubydebug }", $content);
                        }
                        else
                        {
                            $content = str_replace('stdout OUTPUTMODE', "", $content);
                        }
                    }

                    $node = str_replace('{{db}}', $data['db'], $content);
                    $node = str_replace('{{redis_host}}', $data['redis_host'], $node);
                    $node = str_replace('{{redis_port}}', $data['redis_port'], $node);
                    $node = str_replace('{{output_hosts}}', $data['output_hosts'], $node);
                    if (preg_match('#.*{{server}}.*#', $content, $match)) {
                        $newNode = '';
                        foreach ($data['nodes'] as $element) {
                            $res = str_replace('{{server}}', $element, $node);
                            $newNode .= $res;
                        }
                        $this->putNode($file, $newNode, $this->getRootDir().$env.DIRECTORY_SEPARATOR.'conf.d'.DIRECTORY_SEPARATOR);
                    } else{
                        $this->putNode($file, $node, $this->getRootDir().$env.DIRECTORY_SEPARATOR.'conf.d'.DIRECTORY_SEPARATOR);
                    }
                }
            }
            // Generate patterns
            $this->log('Generating patterns for "'.$env.':');
            foreach ($patterns as $file)
            {
                // Skip if not needed
                if (!in_array($file, $this->skipFiles) && $file !== basename(__FILE__)) {
                    $this->log('- '.$file);
                    $content = file_get_contents('./patterns/' . $file);
                    $content = str_replace("{{env}}", $env, $content);
                    $this->putNode($file, $content, $this->getRootDir().$env.DIRECTORY_SEPARATOR.'patterns'.DIRECTORY_SEPARATOR);
                }
            }
            $this->log('');
        }

        $url = sys_get_temp_dir().DIRECTORY_SEPARATOR.'logstash-filters'.DIRECTORY_SEPARATOR;
        echo "\033[36m".date('Y-m-d H:i:s').": Finished writing logstash filters\n\033[0m".date('Y-m-d H:i:s').": Download files at: ".$url."\n";
    }

    /**
     * Create the rootdir based on the selected environment
     */
    protected function createRootDirectories()
    {
        $this->log('Create root directories:');
        foreach($this->environments[$this->currentEnvironment] as $env => $data)
        {
            shell_exec('mkdir -p '.$this->getRootDir().$env);
            shell_exec('mkdir -p '.$this->getRootDir().$env.DIRECTORY_SEPARATOR.'conf.d');
            shell_exec('mkdir -p '.$this->getRootDir().$env.DIRECTORY_SEPARATOR.'patterns');
            $this->log("- ".$this->getRootDir().$env);
        }
    }

    /**
     * Generate log
     * @param $message
     * @param bool $newline
     */
    public function log($message, $newline=true)
    {
        echo date('Y-m-d H:i:s').": ".$message;
        if($newline){
            echo "\n";
        }
    }

    /**
     * Cleanup all the old stuff
     */
    public function cleanup()
    {
        $this->log('Purging all old resources');
        return shell_exec('rm -rf '.sys_get_temp_dir().DIRECTORY_SEPARATOR.'logstash-filters');
    }

    /**
     * Get the root dir
     * @return string
     */
    protected function getRootDir()
    {
        return sys_get_temp_dir().DIRECTORY_SEPARATOR.'logstash-filters'.DIRECTORY_SEPARATOR;
    }

    /**
     * Put the log node in temp dir
     * @param $name
     * @param $data
     */
    public function putNode($name, $data, $rootpath)
    {
        file_put_contents($rootpath.$name, $data);
    }

    /**
     * Retrieve Usage Help Message
     */
    public function usageHelp()
    {
        return <<<USAGE
Usage:  php -f generate_filters.php -- [options]

  --environment <filter>                Environment name
  --debug <filter>                      Debug mode or not (default: n)
  
  help                                  This help

USAGE;
    }
}

$generator = new Dyna_Logstash_Config_Generator();
$generator->run();
