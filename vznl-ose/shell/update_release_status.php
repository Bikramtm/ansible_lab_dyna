<?php

require_once 'abstract.php';

/**
 * Class Dyna_Update_ReleaseStatus
 */
class Dyna_Update_ReleaseStatus extends Mage_Shell_Abstract
{
    /**
     * Run script
     *
     */
    public function run()
    {
        $releaseId = $this->getArg('release_id');
        $replicaId = $this->getArg('replica_id');

        /** @var Dyna_Sandbox_Model_Release $release */
        $release = Mage::getModel('sandbox/release')->load($releaseId);
        if ( ! $release->getId()) {
            Mage::log(sprintf('Release with ID %s not found', $releaseId));
        }
        /** @var Dyna_Sandbox_Model_Replica $replica */
        $replica = Mage::getModel('sandbox/replica')->load($replicaId);
        if ( ! $replica->getId()) {
            Mage::log(sprintf('Replica with ID %s not found', $replicaId));
        }

        /** @var Dyna_Sandbox_Model_Publisher $publisher */
        $publisher = Mage::getModel('sandbox/publisher');
        /** @var Dyna_Sandbox_Model_Sftp $sftp */
        $sftp = Mage::getModel('sandbox/sftp');

        $release
            ->addMessage('Sql import successfully made')
            ->save();

        if(!$replica->isRemote()){
            $sftp->open($replica->getSshConfig());

            if ('success' != ($output = trim($sftp->getConnection()->exec($publisher->getCacheWarmerCommand($replica, $release->getIsHeavy()))))) {
                throw new Exception(sprintf('Cache warmer command did not succeed. Output: %s', $output));
            }
        }

        return $release
            ->setFinishedAt(strftime('%Y-%m-%d %H:%M:%S', time()))
            ->setStatus(Dyna_Sandbox_Model_Release::STATUS_SUCCESS)
            ->addMessage('Release successfully published', $release::MESSAGE_SUCCESS)
            ->save();
    }

    /**
     * Retrieve Usage Help Message
     */
    public function usageHelp()
    {
        $file = basename(__FILE__);
        return <<<USAGE
Usage:  php -f $file -- [options]

  help							This help
USAGE;
    }
}

$processor = new Dyna_Update_ReleaseStatus();
$processor->run();