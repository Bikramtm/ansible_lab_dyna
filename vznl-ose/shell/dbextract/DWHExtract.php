<?php

require_once  __DIR__ . '/../abstract.php';

class Omnius_VFDE_DWHExtract extends Mage_Shell_Abstract
{
    CONST CONFIG_FILE = 'shell/dbextract/DWHExtract.xml';
    CONST LOCK_FILE = 'shell/dbextract/DWHExtract.lock';
    CONST LOG_FILE = 'DWHExtract.log';
    CONST RESULT_FOLDER = 'shell/dbextract/results';

    CONST ROW_LIMIT = 500;

    private $parsedConfig = null;
    private $databaseConnection = null;
    private $lastRun = '1970-01-01';
    private $attempts = 0;
    private $resultFiles = array();

    private $defaultSchema = null;
    private $dbextractSchema = 'dbextract';

    /**
     * Checks whether the log file is existent or not.
     */
    private function lockFileExists()
    {
        if (file_exists(Mage::getBaseDir() . DS . $this::LOCK_FILE)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Tries to open and write to the lock file.
     */
    private function createLockfile()
    {
        $fileStream = fopen(Mage::getBaseDir() . DS . $this::LOCK_FILE, "w");
        fwrite($fileStream, date('Y-m-d H:i:s'));
    }

    /**
     * Delete lock file
     */
    private function deleteLockFile()
    {
        unlink(Mage::getBaseDir() . DS . $this::LOCK_FILE);
    }

    /**
     * Executes a non-query (UPDATE/TRUNCATE)
     * @param $query        string  The query
     */
    private function executeQuery($query)
    {
        $this->databaseConnection->exec($query);
    }

    /**
     * Executes a query and returns the first result
     * @param $query        string  The query
     * @param $field        string  The field
     * @return string
     */
    private function getDatabaseField($query, $field)
    {
        $result = $this->databaseConnection->fetchAll($query);
        if ($result[0]) {
            return $result[0][$field];
        } else {
            return null;
        }
    }

    /**
     * Transfers the data from application to external database
     * @param $tableName    string  The name of the target table
     * @param $query        string  The query
     * @param $parameters   array   Array of parameter values
     */
    private function transferData($tableName, $query, $parameters)
    {
        foreach ($parameters as $key => $value){
            $query = str_replace(':'.$key, $value, $query);
        }

        $this->databaseConnection->exec('INSERT INTO dbextract.' . $tableName . ' ' . $query);
    }

    /**
     * Sends a mail, depending on the type
     * @param $type     string  The type of email
     */
    private function sendMail($type)
    {
        $mailHeaders = array(
            'MIME-Version: 1.0',
            'Content-type: text/html; charset=iso-8859-1',
            'From: ' . $this->parsedConfig->getNode('email/sender/name') . ' <' . $this->parsedConfig->getNode('email/sender/email') . '>'
        );
        $recipients = $this->parsedConfig->getNode('email/destination/email');
        $subject = null;
        $body = null;

        switch ($type) {
            case 'failure' :
                $subject = "Data extracting FAILED";
                $body = $this->parsedConfig->getNode('email/content_failed');
                break;
            case 'success' :
                $subject = "Data extracting SUCCESS";
                $body = $this->parsedConfig->getNode('email/content_success');
                break;
        }

        mail($recipients, $subject, $body, implode("\r\n", $mailHeaders));
    }

    /**
     * Reset the attempts count in the database
     * @param $resetLockFile    bool    If the lock file needs to be reset
     */
    private function resetAttempts($resetLockFile)
    {
        if ($resetLockFile) {
            $this->deleteLockFile();
        }
    }

    /**
     * After successful run, record the time and reset the attempts
     */
    private function recordLastRun()
    {
        $this->executeQuery("TRUNCATE TABLE " . $this->dbextractSchema . ".lastrun; INSERT INTO " . $this->dbextractSchema . ".lastrun (`date_time`) VALUES (NOW() - INTERVAL 10 MINUTE)");
        $this->resetAttempts(true);
    }

    /**
     * Converts the results from the temp table to CSV
     * @param $fileName     string  Filename of output
     * @param $query        string  The query
     * @param $delimiter    string  Delimiter for CSV file
     * @return string       Filename of result file
     */
    private function saveToCSV($fileName, $table, $delimiter = '|')
    {
        /** Double check if the folder exists */
        if (!file_exists(Mage::getBaseDir() . DS . $this::RESULT_FOLDER)) {
            mkdir(Mage::getBaseDir() . DS . $this::RESULT_FOLDER);
        }

        /** Open file pointer and put in columns + data */
        $filePointer = fopen(Mage::getBaseDir() . DS . $this::RESULT_FOLDER . DS . $fileName, 'a');

        if($filePointer){
            $headers = $this->databaseConnection->fetchAll("SELECT * FROM dbextract.$table LIMIT 1");

            /** First fetch the columns from the first row */
            $columns = array();
            foreach ($headers[0] as $columnName => $value) {
                $columns[] = $columnName;
            }
            fputcsv($filePointer, $columns, $delimiter);

            /** Not the most best way to do this, but the core/resource_iterator does not support databases outside Magento*/
            $offSet = 0;
            while(!empty($allRows = $this->databaseConnection->fetchAll("SELECT * FROM dbextract.$table LIMIT $offSet," . $this::ROW_LIMIT))){
                foreach ($allRows as $row) {
                    $row = array_map(
                        function ($item) {
                            return str_replace(["\n", "\r"], '', $item);
                        }, $row
                    );
                    fputcsv($filePointer, $row, $delimiter);
                }

                $offSet += $this::ROW_LIMIT;
            }

            fclose($filePointer);
            return Mage::getBaseDir() . DS . $this::RESULT_FOLDER . DS . $fileName;

        } else {
            Mage::throwException("Could not create CSV file!");
        }
    }

    /**
     * Transfers the files via FTP
     */
    private function transferFTP()
    {
        try{
            $hostname = $this->parsedConfig->getNode('ftp/host');
            $port = $this->parsedConfig->getNode('ftp/port');
            $username = $this->parsedConfig->getNode('ftp/username');
            $password = $this->parsedConfig->getNode('ftp/password');
            $directory = $this->parsedConfig->getNode('ftp/filepath');

            foreach ($this->resultFiles as $file){
                echo 'sshpass -p "' . $password . '" scp -P ' . $port .' -r ' . $file . ' ' . $username . '@' . $hostname . ':' . $directory;
                exec('sshpass -p "' . $password . '" scp -P ' . $port .' -r ' . $file . ' ' . $username . '@' . $hostname . ':' . $directory);

                unlink($file);
            }

        } catch (Exception $e){
            $this->throwErrorAndDie($e->getMessage(), true);
        }
    }

    /**
     * @param string $message
     * @param bool $die
     */
    private function throwErrorAndDie(string $message, bool $die = true)
    {
        Mage::log($message, 3, $this::LOG_FILE);
        echo $message . PHP_EOL;

        foreach ($this->resultFiles as $file) {
            unlink($file);
        }

        if ($die) {
            $this->resetAttempts(true);
            die();
        }
    }

    /**
     * Main entry point for running the extract.
     */
    public function run()
    {
        ini_set('memory_limit', '512M');
        $lastRun = $this->getArg('lastrun');

        /** Initialize the parameters*/
        $this->defaultSchema = Mage::getConfig()->getResourceConnectionConfig("default_setup")->dbname;
        $this->databaseConnection = Mage::getSingleton('core/resource')->getConnection('core_write');
        $this->parsedConfig = new Varien_Simplexml_Config(Mage::getBaseDir() . DS . $this::CONFIG_FILE);

        /** Check if lock file exists, if not, continue */
        if (!$this->lockFileExists()) {
            try {
                /** Create the lock file, so no duplicate process will start */
                $this->createLockfile();
                /** Check if the application has the correct database structure(s) */

                $database = $this->getDatabaseField($this->parsedConfig->getNode('querys/check_database_exists'), 'SCHEMA_NAME');
                if (!$database) {
                    $this->throwErrorAndDie("DWHExtract database is non existent!");
                }

                /** Clean up the temporary local database */
                $this->executeQuery($this->parsedConfig->getNode('querys/clean_temp_data'));

                /** Get information about any previous run */
                if (!$lastRun || $lastRun == "") {
                    $this->lastRun = $this->getDatabaseField($this->parsedConfig->getNode('querys/get_last_run'), 'date_time') ?: $this->lastRun;
                } else {
                    $this->lastRun = date("Y-m-d H:i:s", strtotime($lastRun));
                }

                /** Transfer ALL the data */
                $this->executeQuery("USE " . $this->defaultSchema . ";");

                $this->transferData('header_extract'
                    , $this->parsedConfig->getNode('querys/header_order')
                    , array('fromDate' => "'$this->lastRun'"));

                $this->transferData('header_extract'
                    , $this->parsedConfig->getNode('querys/header_cart')
                    , array('fromDate' => "'$this->lastRun'"));

                $this->transferData('details_extract'
                    , $this->parsedConfig->getNode('querys/details_order')
                    , array('fromDate' => "'$this->lastRun'"));

                $this->transferData('details_extract'
                    , $this->parsedConfig->getNode('querys/details_cart')
                    , array('fromDate' => "'$this->lastRun'"));

                $this->transferData('bundle_package_extract'
                    , $this->parsedConfig->getNode('querys/bundlepackage_order')
                    , array('fromDate' => "'$this->lastRun'"));

                $this->transferData('bundle_package_extract'
                    , $this->parsedConfig->getNode('querys/bundlepackage_cart')
                    , array('fromDate' => "'$this->lastRun'"));

                $this->transferData('bundle_info_extract'
                    , $this->parsedConfig->getNode('querys/bundleinfo_order')
                    , array('fromDate' => "'$this->lastRun'"));

                $this->transferData('bundle_info_extract'
                    , $this->parsedConfig->getNode('querys/bundleinfo_cart')
                    , array('fromDate' => "'$this->lastRun'"));

                $currentDate = date('YmdHis');

                /** Create CSV files from data */
                $this->resultFiles[] = $this->saveToCSV('VFDEExtract-Header-' . $currentDate . '.csv', 'header_extract');
                $this->resultFiles[] = $this->saveToCSV('VFDEExtract-Details-' . $currentDate . '.csv', 'details_extract');
                $this->resultFiles[] = $this->saveToCSV('VFDEExtract-BundlePackage-' . $currentDate . '.csv', 'bundle_package_extract');
                $this->resultFiles[] = $this->saveToCSV('VFDEExtract-BundleInfo-' . $currentDate . '.csv', 'bundle_info_extract');

                /** Transfer the resultfiles to FTP */
                $this->transferFTP();

                /** Send success email and record the run data */
                //$this->sendMail('success');
                $this->recordLastRun();

            } catch (\Exception $e) {
                $this->throwErrorAndDie($e->getMessage(), false);

                $this->attempts++;
                //$this->sendMail('failure');

                if ($this->attempts > 5) {
                    $this->throwErrorAndDie("Exceeded the maximum amount of retries, exiting!");
                } else {
                    $this->run();
                }
            }
        } else {
            $this->throwErrorAndDie("Lock file exists, exiting");
        }
    }
}

$dwhExtract = new Omnius_VFDE_DWHExtract();
$dwhExtract->run();