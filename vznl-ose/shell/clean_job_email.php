<?php

require_once 'vznl_abstract.php';

/**
 * Class Vznl_Clean_Job_Email
 */
class Vznl_Clean_Job_Email extends Vznl_Shell_Abstract
{
    /** @var float */
    protected $_startTime = 0.0;
    protected $enabled = false;
    protected $deleteOlderThanDays = 7;

    /**
     * Initialize application and parse input parameters
     */
    public function __construct()
    {
        $this->_lockExpireInterval = 14400;
        $this->_startTime = microtime(true);
        parent::__construct();
    }

    /**
     * @param $data
     * @return array
     */
    protected function toArray($data)
    {
        $items = array();
        if (is_null($data)) {
            return $data;
        } elseif (is_scalar($data)) {
            return $data;
        } elseif (is_array($data)) {
            foreach ($data as $key => $value) {
                $items[$key] = $this->toArray($value);
            }
        } elseif ($data instanceof Varien_Data_Collection) {
            foreach ($data as $key => $value) {
                $items[$key] = $this->toArray($value);
            }
        } elseif ($data instanceof Varien_Object) {
            foreach ($data->getData() as $key => $value) {
                $items[$key] = $this->toArray($value);
            }
        } elseif (is_object($data)) {
            foreach (get_object_vars($data) as $key => $value) {
                $items[$key] = $this->toArray($value);
            }
        }
        return $items;
    }

    /**
     * Run script
     */
    public function run()
    {
        try {
            if(!$this->hasLock()) {
                $this->createLock();
                if (0 === ($interval = (int)trim(Mage::getStoreConfig('cleanup_configuration/emailjob_cleanup/cleanup_interval')))) {
                    throw new Exception('Invalid setting for the cleanup interval. Must be a valid digit bigger than 0.');
                }
                $now = new DateTime();
                $date = new DateTime(sprintf('-%s days', $interval));
                $emailJobCollection = Mage::getModel('communication/job')->getCollection()
                    ->addFieldToFilter('deadline', array('lt' => $date->format('Y-m-d H:i:s')))
                    ->addFieldToFilter('type', Vznl_Communication_Model_Job::TYPE_EMAIL);

                if ($this->enabled) {
                    $filename = sprintf('job_email_backup_%s.tar.gz',
                        strtr($now->format('c'), array(':' => '', '-' => '', '+' => '')));
                    $tmpDir = sprintf('%s/job_email_backup_%s', sys_get_temp_dir(), md5(time() . rand(0, 100)));
                    if (!@mkdir($tmpDir)) {
                        throw new Exception(sprintf('Could not create temp dir (%s)', $tmpDir));
                    }
                }

                $emailJobs = 0;

                //@todo check model
                /** @var Vznl_Communication_Model_Job $emailJob */
                foreach ($emailJobCollection->getItems() as $emailJob) {
                    if ($this->enabled) {
                        $tempFile = tempnam($tmpDir, sprintf('job_email_%s_', $emailJob->getId()));
                        $data = $emailJob->toArray();
                        file_put_contents($tempFile, json_encode($data));
                    }
                    ++$emailJobs;
                }

                $coreResource = Mage::getSingleton('core/resource');
                $conn = $coreResource->getConnection('core_read');

                if ($this->enabled) {
                    $command = sprintf(
                        'cd %s && tar -zcvf %s %s/ && mv %s %s',
                        sys_get_temp_dir(),
                        $filename,
                        str_replace(sprintf('%s/', sys_get_temp_dir()), '', $tmpDir),
                        $filename,
                        sprintf('%s/export/%s', Mage::getBaseDir('var'), $filename)
                    );

                    if (false === system($command)) {
                        throw new Exception(sprintf('Could not create archive (%s).', $command));
                    } else {
                        if (!$this->getArg('q')) {
                            $this->printLn(sprintf('Backup of %s email jobs made in %s', $emailJobs,
                                sprintf('%s/export/%s', Mage::getBaseDir('var'), $filename)));
                        }
                    }
                }

                $deleteStmt = sprintf("DELETE FROM `dyna_communication_job` WHERE (`deadline` < '%s')",
                    $date->format('Y-m-d H:i:s'));
                $conn->query($deleteStmt);
                if (!$this->getArg('q')) {
                    $this->printLn(sprintf('Deleted %s email jobs in %ss', $emailJobs,
                        number_format(microtime(true) - $this->_startTime, 3)));
                }
                if ($this->enabled) {
                    //delete older archives
                    foreach (glob("/tmp/job_email_backup_*") as $oldFile) {
                        if ((time() - filectime($oldFile)) / 86400 >= $this->deleteOlderThanDays) {
                            unlink($oldFile);
                        }
                    }
                }
                $this->removeLock();
                exit(0);
            }else{
                throw new Exception('Cannot start script due to existing lock: '.$this->getLockPath());
            }
        } catch (Exception $e) {
            $this->printLn($e->getMessage());
            $this->printLn($e->getTraceAsString());
            exit(1);
        }
    }

    /**
     * @param $message
     */
    protected function printLn($message)
    {
        if (is_array($message)) {
            foreach ($message as $line) {
                echo sprintf("%s\n\r", $line);
            }
        } else {
            echo sprintf("%s\n\r", $message);
        }
    }

    /**
     * Display help on CLI
     * @return string
     */
    public function usageHelp()
    {
        $f = basename(__FILE__);
        return <<< USAGE
Usage: php ${f} [options]

  -q            Quiet, do not output to cli
  -h            Short alias for help
  help          This help
USAGE;
    }
}

$shell = new Vznl_Clean_Job_Email();
$shell->run();
