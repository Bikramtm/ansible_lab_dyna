<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

require_once 'abstract.php';
require_once 'stopwatch.php';

/**
 * Class Dyna_Sandbox_Cli
 */
class Dyna_Sandbox_Cli extends Mage_Shell_Abstract
{

    protected $_filename;
    protected $_path;
    /** @var Omnius_Sandbox_Model_Sftp */
    protected $_sftpClient;
    protected $_sftpArgs;
    protected $_exportId;

    /**
     * Run script
     *
     */
    public function run() {
        if ($this->getArg('exportProductsXML')) {
            if ($this->getArg('exportId')) {
                $this->_exportId = $this->getArg('exportId');
            }
            if ($this->getArg('filename')) {
                $this->_filename = $this->getArg('filename');
            } else {
                $this->_filename = 'products_'.time().'.xml';
            }
            if ($this->getArg('path')) {
                $this->_path = $this->getArg('path');
            } else {
                $this->_path =  Mage::getBaseDir('var') . DS . 'export' . DS;
            }
            if ($this->getArg('sftp')) {
                $this->_sftpArgs = unserialize(base64_decode($this->getArg('sftp')));
            }
            $this->exportProductsXml();
        } elseif ($this->getArg('createRelease')) {
            if ($this->getArg('target') ) {
                foreach (Mage::getSingleton('sandbox/config')->getReplicas() as $replica) {
                    if ($replica->getName() == $this->getArg('target')) {
                        $release['replica'] = $this->getArg('target');
                        break;
                    }
                }
                $message = '';
                if (!empty($release['replica'])) {
                    $release['version'] = $this->getArg('version');

                    if (strlen($this->getArg('deadline')) == 19) {
                        $release['deadline'] = $this->getArg('deadline');
                    } elseif (strlen($this->getArg('deadline')) == 10) {
                        $release['deadline'] = $this->getArg('deadline') . ' 00:00:00';
                    }

                    $release['schedule_type'] = $this->getArg('type') == 'once' ?: 'once'; //Schedule Type hardcoded to ONCE
                    if ($release['schedule_type'] == 'once') {
                        $date_type = 'one_time_date'; //other field(s) for other schedule types?
                    }

                    if (strlen($this->getArg('date')) == 19) {
                        $release[$date_type] = $this->getArg('date');
                    } elseif (strlen($this->getArg('date')) == 10) {
                        $release[$date_type] = $this->getArg('date') . ' 00:00:00';
                    }

                    $release['import_check'] = 0;
                    if ($this->getArg('import_check') == 'yes') {
                        $release['import_check'] = 1;
                    }

                    $release['export_media'] = 1;
                    if ($this->getArg('export_media') && $this->getArg('export_media') == 'no') {
                        $release['export_media'] = 0;
                    }

                    $release['is_heavy'] = 1;
                    if ($this->getArg('is_heavy') && $this->getArg('is_heavy') == 'no') {
                        $release['is_heavy'] = 0;
                    }
                    if (empty($release['version'])) {
                        $message .= 'Release Version (--version) is mandatory. None provided.' . PHP_EOL;
                    }
                    if (empty($release['deadline'])) {
                        $message .= 'Release Publish Deadline (--deadline) is mandatory. Format is YYYY-MM-DD[ HH:MM].' . PHP_EOL;
                    }
                    if (empty($message)) {
                        $model = Mage::getModel('sandbox/release')
                            ->addData($release)
                            ->save();
                    }
                } else {
                    $message .= 'Release Target (--target) is mandatory. Please specify a valid target Replica.' . PHP_EOL;
                }
                if (!empty($message)) {
                    throw new Exception($message);
                } else {
                    echo 'Release "' . $release['version'] . '(' . $model->getId() . ')" saved.';
                }
            }

        } elseif ($this->getArg('stopImport')) {
            if (!$executionId = $this->getArg('executionId')) {
                echo "Exiting. Please provide --executionId\n";
                return;
            }
            $response = Mage::helper('dyna_sandbox')->getRunningImportPIDs($executionId);
            if ($response['exitCode'] == 0) {
                $killed = 0;
                foreach ($response['output'] as $k => $line) {
                    $line = explode(',', $line);
                    if ($line[11] !== "sandbox_ops.php"){
                        //$line[1] = Process PID
                        $kill_command = "kill " . $line[1];
                        @exec($kill_command, $out, $exit);
                        if ($exit != 0) {
                            echo "Failed to kill process with PID " . $line[1] . "\n";
                        } else {
                            echo "Killed process with PID " . $line[1] . "\n";
                            $killed++;
                        }
                    }
                }
                if ($killed == count($response['output']) - 1) {
                    /** @var Dyna_Sandbox_Model_ImportInformation $executionModel */
                    $executionModel = Mage::getModel('dyna_sandbox/importInformation');
                    $executionModel->load((int)$executionId)
                        ->setStatus($executionModel::STATUS_KILLED)
                        ->setFinishedAt(date("Y-m-d H:i:s", Mage::getModel('core/date')->timestamp()))
                        ->save();
                    /** @var Mage_Core_Model_Factory $factory */
                    $factory = new Mage_Core_Model_Factory();
                    /** @var Mage_Index_Model_Indexer $indexer */
                    $indexer = $factory->getSingleton($factory->getIndexClassAlias());
                    $collection = $indexer->getProcessesCollection();
                    /** @var Mage_Index_Model_Process $process */
                    foreach ($collection as $process) {
                        if ($process->getIndexer()->isVisible() === false) {
                            continue;
                        }
                        //@todo Maybe we should check if the indexer that's running is one of the indexers run by import_full.sh
                        if ($process->getStatus() == 'working') {
                            $process->setStatus('require_reindex');
                            $process->save();
                        }
                    }
                } else {
                    echo "Couldn't kill all import execution processes.";
                }
                return;
            } else {
                echo "Exiting. Couldn't find any processes with that executionId\n";
                return;
            }
        }

    }

    protected function exportProductsXml() {
        if ($this->_exportId) {
            /** @var Dyna_Sandbox_Model_Export $export */
            $export = Mage::getSingleton("dyna_sandbox/export")->load($this->_exportId);
        }
        $localPath = Mage::getBaseDir('var') . DS . 'export' . DS;
        $localFile = 'products_'.time().'.xml';
        $productsGz = Mage::helper('vznl_sandbox')->exportProductsXml($localPath, $localFile);
        $export->addMessage('Exported to temporary file');
        $export->addMessage('Uploading to server');
        if (!empty($productsGz)) {
            $targetFileFullpath = $this->_path . $this->_filename . '.gz';
            /** @var Omnius_Sandbox_Model_Sftp $sftpConnection */
            // @todo upgrade Omnius_Sandbox_Model_Sftp which uses lib/phpseclib which has methods deprecated in PHP 7, resulting in entries in system.log
            $sftpConnection = $this->getSftp();
            $sftpConnection->open($this->_sftpArgs);
            if ($sftpConnection->cd($this->_path)) {
                $succeeded = $sftpConnection->write(
                    $targetFileFullpath,
                    $productsGz,
                    NET_SFTP_LOCAL_FILE
                );
                if ( ! $succeeded) {
                    $sftpConnection->close();
                    throw new Exception(sprintf("Could not copy export file (%s).", $this->_filename));
                } else {
                    $rmCommand = 'rm ' . $productsGz;
                    @exec($rmCommand);
                    if ($this->_exportId) {
                        $export->addMessage('Upload succeeded');
                    }
                    $export
                        ->setFinishedAt(strftime('%Y-%m-%d %H:%M:%S', time()))
                        ->setStatus(Omnius_Sandbox_Model_Export::STATUS_SUCCESS)
                        ->addMessage(sprintf('Export successfully made at %s', $targetFileFullpath), Omnius_Sandbox_Model_Export::MESSAGE_SUCCESS)
                        ->save();
                }
            } else {
                $sftpConnection->close();
                throw new Exception(sprintf('Could not cd to export path ("%s")', $this->_path));
            }
        } else {
            throw new Exception('Export unsuccessful.');
        }
    }

    /**
     * @return Omnius_Sandbox_Model_Sftp
     */
    protected function getSftp()
    {
        if ( ! $this->_sftpClient) {
            $this->_sftpClient = new Omnius_Sandbox_Model_Sftp();
        }
        return $this->_sftpClient;
    }

    /**
     * Retrieve Usage Help Message
     *
     */
    public function usageHelp()
    {
        return <<<USAGE
Usage:

* Export Products XML
---- php sandbox_ops.php --exportProductsXML --exportId <id> --filename <products.xml> --path </var/www/website/var/export/>
     --exportId <id>                                                   Export Process ID
     --filename <products.xml>                                         Export File name
     --path </var/www/website/var/export/>                             Path to Export File
     
* Create New Release
---- php sandbox_ops.php --createRelease --target <replica_name> --version <version> --deadline <datetime> --type <schedule_type> --date <datetime> --import_check <yes|no> --export_media <yes|no> --is_heavy <yes|no>
     --target <replica_name>                                           (mandatory) Target Replica Name
     --version <version>                                               (mandatory) Name your Release Version
     --deadline <"YYYY-MM-DD HH:MM:SS">                                (mandatory) Publish before this time and date
     --type <schedule_type>                                            One time or Periodic. Currently only <once> (ONE TIME) is supported
     --date <"YYYY-MM-DD HH:MM:SS">                                    Publish on this time and date or before the Deadline
     --import_check <yes|no>                                           Only publish if last import ready and successful
     --export_media <yes|no>                                           Handle Media also?
     --is_heavy <yes|no>                                               If true, the heavy indexers and cache building process will be executed also

* Stop a running Import Execution
---- php sandbox_ops.php --stopImport --executionId <id>

* This Help
---- php -f sandbox_ops.php help
USAGE;
    }
}

$shell = new Dyna_Sandbox_Cli();
$shell->run();