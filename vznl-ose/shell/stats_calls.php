<?php
require_once 'vznl_abstract.php';

class Vznl_Stats_Calls_Cli extends Vznl_Shell_Abstract
{

    public function __construct()
    {
        $this->_lockExpireInterval = 900; // 5min run so lock at 15min
        parent::__construct();
    }

    private function _writeLine($msg)
    {
        echo $msg . PHP_EOL;
    }

    private function _renderCallGraph($callType, $callName, $serverIp, $lastRun, $writeConnection)
    {
        echo $callType . ' ' . $callName . "\r\n";
        $serverTimeOffset = 1;

        exec("find ".MAGENTO_ROOT."/var/log/services/$callType -type f -newermt '$lastRun' | xargs grep -l '$callName' | xargs grep -E 'Date:|X-START-DATE:'", $output);

        for ($i = 0; $i < count($output); $i++) {
            if (strpos($output[$i], 'X-START-DATE:') === false) {
                continue; // Skip, not supported
            }
            if (!isset($output[$i+1])) {
                continue; //Skip if not found
            }
            if (strpos($output[$i+1], 'Date:') === false) {
                // Skip scenarios when the call did not finish, and there is no Date, as it will show error and provide wrong timers
                // TODO: handle this situation somehow, because if all calls fail currently the smiley stays green
                continue;
            }

            $startRaw = explode('X-START-DATE:', $output[$i])[1];
            $start = strtotime($startRaw);
            $endRaw = explode('Date:', $output[$i+1])[1];
            $end = strtotime($endRaw);
            $timeTaken = ($end - $start + $serverTimeOffset);

            if ($timeTaken > -2) { // Take out anomalies
                // Insert into DB
                $query = "INSERT INTO dyna_perf_log_data (calltype, callname, startedat, server, duration) VALUES ('$callType', '$callName', '" . date('y-m-d H:i:s', $start) . "', '$serverIp', '$timeTaken')";
                $writeConnection->query($query);
                echo $startRaw . ' Call took ' . $timeTaken . "\r\n";
            }
        }
    }

    /**
     * Run script
     *
     */
    public function run()
    {
        try {
            if(!$this->hasLock()) {
                $this->createLock();
                $resource = Mage::getSingleton('core/resource');
                $readConnection = $resource->getConnection('core_read');
                $writeConnection = $resource->getConnection('core_write');

                $host= gethostname();
                $serverIp = gethostbyname($host);

                $query = "SELECT timestamp FROM dyna_perf_log_run WHERE server = '$serverIp'";
                $lastRun = $readConnection->fetchOne($query);
                if (!$lastRun) {
                    $lastRun = date('y-m-d H:i:s', strtotime('-1 day'));
                }

                $callTypes = array(
                    array('DealerAdapter', 'RetrieveCustomerCreditProfileRequest'),
                    array('DealerAdapter', 'RetrieveCustomerInfoRequest'),
                    array('EbsOrchestration', 'ProcessOrderRequest'),
                    array('Approval', 'ApprovePackagesRequest'),
                    array('Axi', 'GetProductStockRequest'),
                    array('Axi', 'LockOrderRequest'),
                    array('Axi', 'UnLockOrderRequest'),
                    array('Inlife', 'SearchOrderRequest'),
                    array('Inlife', 'addAddOnRequest'),
                    array('Inlife', 'ChangePricePlanRequest'),
                    array('Inlife', 'changeSIMRequest'),
                    array('Inlife', 'GetAddOnsRequest'),
                    array('Inlife', 'getAreaCodesRequest'),
                    array('Inlife', 'GetAssignedProductDetailsRequest'),
                    array('Inlife', 'GetAvailableFixedNumbersRequest'),
                    array('Inlife', 'GetEligiblePricePlansRequest'),
                    array('Inlife', 'GetProductSettingsRequest'),
                    array('Inlife', 'OrderSIMRequest'),
                    array('Inlife', 'RemoveAddonRequest'),
                    array('Inlife', 'searchAssignedProductRequest'),
                    array('Inlife', 'updateProductSettingsRequest'),
                	array('RetrieveCustomerInfo', 'RetrieveCustomerInfoRequest'),
                	array('LockManager', 'LockManagerRequest'),
                );

                foreach ($callTypes as $callTypePair) {
                    $this->_renderCallGraph($callTypePair[0], $callTypePair[1], $serverIp, $lastRun, $writeConnection);
                }

                $runTimestamp = date('y-m-d H:i:s', strtotime('+1 hour')); // TODO: Fix timezone offset
                $query = "INSERT INTO dyna_perf_log_run (server, timestamp) VALUES ('$serverIp', '$runTimestamp') ON DUPLICATE KEY UPDATE timestamp='$runTimestamp'";
                $writeConnection->query($query);
                $this->removeLock();
            } else{
                throw new Exception('Cannot start script due to existing lock: '.$this->getLockPath());
            }
        } catch (Exception $e) {
            Mage::logException($e);
            Zend_Debug::dump($e->getMessage());
        }

    }

    /**
     * Retrieve Usage Help Message
     */
    public function usageHelp()
    {
        $file = basename(__FILE__);

        return <<<USAGE
Usage:  php -f $file -- [options]

  help                            This help

USAGE;
    }
}

$stats = new Vznl_Stats_Calls_Cli();
$stats->run();
