#!/usr/bin/env bash
DOCUMENT_ROOT="$(dirname "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )" )"
DATE=$(date +"%Y-%m-%d")
command -v xml2 >/dev/null 2>&1 || { echo >&2 "xml2 is required but it's not installed. Please hardcode MYSQL connection parameters. Aborting."; exit 1; }
#maybe prompt user input for MySQL params if xml2 is not available?
MYSQL_HOST="$(xml2 < $DOCUMENT_ROOT/app/etc/local.xml | grep -m 1 config/global/resources/default_setup/connection/host | cut -d"=" -f2)"
MYSQL_USERNAME="$(xml2 < $DOCUMENT_ROOT/app/etc/local.xml | grep -m 1 config/global/resources/default_setup/connection/username | cut -d"=" -f2)"
MYSQL_PASSWORD="$(xml2 < $DOCUMENT_ROOT/app/etc/local.xml | grep -m 1 config/global/resources/default_setup/connection/password | cut -d"=" -f2)"
MYSQL_DATABASE="$(xml2 < $DOCUMENT_ROOT/app/etc/local.xml | grep -m 1 config/global/resources/default_setup/connection/dbname | cut -d"=" -f2)"

if [ "$1" == "disable" ]; then
    echo "disabling magento admin account....";
    set -o pipefail
    mysql -h"$MYSQL_HOST" -u"$MYSQL_USERNAME" -p"$MYSQL_PASSWORD" $MYSQL_DATABASE -e "UPDATE admin_user SET is_active = 0 WHERE username='admin';";
    if [ "$?" -eq 0 ]; then
        echo "Magento admin disabled on $MYSQL_DATABASE"
    else
        echo "Admin could not be disabled -> command failed"
    fi
elif [ "$1" == "enable" ]; then
    echo "enabling magento admin account....";
    set -o pipefail
    mysql -h"$MYSQL_HOST" -u"$MYSQL_USERNAME" -p"$MYSQL_PASSWORD" $MYSQL_DATABASE -e "UPDATE admin_user SET is_active = 1 WHERE username='admin';";
    if [ "$?" -eq 0 ]; then
        echo "Magento admin enabled on $MYSQL_DATABASE"
    else
        echo "Admin could not be disabled -> command failed"
    fi
else
        echo "Usage:";
        echo "-------------";
        echo "./disableMagentoAdmin.sh disable";
        echo "To disable the default magento admin account";
        echo "-------------";
        echo "./disableMagentoAdmin.sh enable";
        echo "To enable the default magento admin account";
fi