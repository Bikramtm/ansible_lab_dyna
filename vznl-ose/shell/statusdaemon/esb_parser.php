<?php

require_once __DIR__ . '/../vznl_abstract.php';

/**
 * Class Dyna_Job_Status
 */
class Dyna_Esb_Parser extends Vznl_Shell_Abstract
{

    public function __construct()
    {
        parent::__construct();
        Mage::getSingleton('core/translate')->init('frontend');
    }

    /**
     * Method to log
     */
    private function _logStatusDaemonMessage($message, $file = '')
    {
    	$baseLogDir = Mage::getBaseDir('var') . DS . 'log' . DS . 'status_daemon';
    	if (!is_dir($baseLogDir)) {
    		mkdir($baseLogDir, 0777, true);
    	}

    	$file = empty($file) ? 'statusdeamon' : $file;
    	$logFile = $baseLogDir . DS . sprintf($file . "_%s", date('Ymd')) . '.log';

    	// since Mage::log does not support custom directory using file_put_contents
    	file_put_contents($logFile, var_export($message, true) . PHP_EOL, FILE_APPEND);
    }

    /**
     * Run script
     */
    public function run()
    {
        $data = trim($this->getArg('c'));
        if (!$data) {
            echo $this->usageHelp();
            exit(1);
        }
        try {
            $data = explode(';', $data);
            $this->_logStatusDaemonMessage(date("Y-m-d h:i:s"), 'request');
            $this->_logStatusDaemonMessage($data, 'request');
            $method = $this->getMethod(array_shift($data));

            $helper = Mage::helper('vznl_esb');
            $data = implode(';', $data);
            if (method_exists($helper, $method)) {
                $returnData = $helper->{$method}($data);
                $this->_logStatusDaemonMessage(date("Y-m-d h:i:s") . ': Return data for ' . $method . ';' . $data, 'response');
                $this->_logStatusDaemonMessage(date("Y-m-d h:i:s") . ': ' . $returnData, 'response');
                echo $returnData;
            } else {
            	$message = 'Invalid method provided';
            	$this->_logStatusDaemonMessage(date("Y-m-d h:i:s") . ': Error for data ' . $method . ';' . $data . '-' . $message, 'error');
                echo $message;
                exit(1);
            }
        } catch (Exception $e) {
        	$message = date("Y-m-d h:i:s") . ': Exception for data ' . $method . ';' . implode(';', $data) . '-' . $e->getMessage();
        	$this->_logStatusDaemonMessage($message, 'exception');
        	$this->_logStatusDaemonMessage($e->getTraceAsString(), 'exception');
        	echo $e->getMessage();
        	exit(1);
        }
        exit(0);
    }


    protected function getMethod($method)
    {
        $array = explode('.', $method);
        if (isset($array[1])) {
            $method = strtolower($array[1]);
        } else {
            $method = strtolower($array[0]);
        }

        switch ($method) {
            case "setdeliverydetails" :
                return 'setDeliveryDetails';
                break;
            case "setordermsdisn" :
                return 'setOrderMsdisn';
                break;
            case "setnumberportingstatus" :
                return 'setNumberportingStatus';
                break;
            case "setcreditcheckstatus" :
                return 'setCreditcheckStatus';
                break;
            case "setvfcreditstatus" :
                return 'setVfCreditStatus';
                break;
            case "setvfstatuscode" :
                return 'setVfStatusCode';
                break;
            case "setvfordernr" :
                return 'setVfOrderNr';
                break;
            case "setzgordernr" :
                return 'setZgOrderNr';
                break;
            case "setcustomerban" :
                return 'setCustomerBan';
                break;
            case "setdeliveryorderstatus" :
                return 'setDeliveryOrderStatus';
                break;
            case "setsaleorderstatus" :
                return 'setSaleOrderStatus';
                break;
            case "setsaleordererror" :
                return 'setSaleOrderError';
                break;
            case "setciboodleid" :
                return 'setCiboodleId';
                break;
            case "sethomedeliverystatus" :
                return 'setHomeDeliveryStatus';
                break;
            case "setpackagestatus" :
                return 'setPackageStatus';
                break;
            case "sendgarantmail" :
                return 'sendGarantMail';
                break;
            case "setorderpackageapproval" :
                return 'setOrderPackageApproval';
                break;
            case "setactualportingdate" :
                return 'setActualPortingDate';
                break;
            case "setmanualactivation" :
                return 'setManualActivation';
                break;
            case "sendcommunicationitemsforvalidationapproved" :
                return 'sendCommunicationItemsForValidationApproved';
                break;
            case "sendcommunicationitemsforfixedvalidationapproved" :
                return 'sendCommunicationItemsForFixedValidationApproved';
                break;
            default:
                throw new Exception('Invalid method received');
                break;
        }
    }

    /**
     * Retrieve Usage Help Message
     */
    public function usageHelp()
    {
        $file = basename(__FILE__);

        return <<<USAGE
Usage:  php -f $file -- [options]

  help         This help
  -c command   Execute the esc command with given params

USAGE;
    }
}

$import = new Dyna_Esb_Parser();
$import->run();
