#!/bin/bash
targetPath=../statusdaemon
daemonFile="master-5.zip"

# Download daemon
wget -P ${targetPath} http://nexus.devtools.io/repository/omnius-daemons/statusdaemon/${daemonFile}

# Unzip daemon
cd ${targetPath}
unzip ${daemonFile}
mv NLog.config NLog.config.dist
rm ${daemonFile}