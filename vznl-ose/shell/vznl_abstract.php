<?php
/**
 * Copyright (c) 2019. Dynacommerce B.V.
 */

require_once 'abstract.php';

/**
 * Class Vznl_Shell_Abstract
 */
abstract class Vznl_Shell_Abstract extends Mage_Shell_Abstract
{

    protected $_lockExpireInterval = 90;

    /**
     * Initialize application
     */
    public function __construct()
    {
        parent::__construct();
        if (!defined('MAGENTO_ROOT')) {
            define('MAGENTO_ROOT', __DIR__ .'/..');
        }
    }

    /**
     * Check if the called script has a lock active or not
     * @return bool
     */
    public function hasLock()
    {
        if(file_exists($this->getLockPath())){
            if( (filemtime($this->getLockPath()) + $this->_lockExpireInterval) < time()){
                $this->removeLock();
                return false;
            }
            return true;
        }
        return false;
    }

    /**
     * Remove the actual lock
     * @return bool
     */
    public function removeLock()
    {
        unlink($this->getLockPath());
        return true;
    }

    /**
     * Create an actual lock
     * @return bool
     */
    public function createLock()
    {
        file_put_contents($this->getLockPath(), 'lock');
        return true;
    }

    /**
     * Get the path of the lock file for a given script
     * @return string
     */
    public function getLockPath()
    {
        return strtolower(DS.'tmp'.DS.basename(get_called_class()).'_'.Mage::getStoreConfig('cleanup_configuration/environment/prefix').'.lock');
    }

}
