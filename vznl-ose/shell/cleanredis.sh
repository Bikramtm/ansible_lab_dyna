#!/bin/bash
DOCUMENT_ROOT="$(dirname "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )" )"
REDIS_SESSION_HOST="$(xml2 < $DOCUMENT_ROOT/app/etc/local.xml | grep -m 1 config/global/redis_session/host | cut -d"=" -f2)"
REDIS_SESSION_DATABASE="$(xml2 < $DOCUMENT_ROOT/app/etc/local.xml | grep -m 1 config/global/redis_session/db | cut -d"=" -f2)"
REDIS_CACHE_SERVER="$(xml2 < $DOCUMENT_ROOT/app/etc/local.xml | grep -m 1 config/global/cache/backend_options/server | cut -d"=" -f2)"
REDIS_CACHE_DATABASE="$(xml2 < $DOCUMENT_ROOT/app/etc/local.xml | grep -m 1 config/global/cache/backend_options/database | cut -d"=" -f2)"
if [ "$1" = "session" ]; then
    echo "Clearing redis session"
    redis-cli -h $REDIS_SESSION_HOST -n $REDIS_SESSION_DATABASE flushdb
    echo "Redis session has been cleared"
else
    if [ "$1" = "cache" ]; then
        echo "Clearing redis cache"
        redis-cli -h $REDIS_SESSION_HOST -n $REDIS_CACHE_DATABASE flushdb
        echo "Redis cache has been cleared"

    else
        if [ "$1" = "both" ]; then
            echo "Clearing redis data (both session and cache)"
            redis-cli -h $REDIS_SESSION_HOST -n $REDIS_SESSION_DATABASE flushdb && redis-cli -h $REDIS_CACHE_SERVER -n $REDIS_CACHE_DATABASE flushdb
            echo "Brand new redis"
        fi
    fi
fi
