<?php
require_once 'repairDbClass.php';
require_once 'repairDbSql4.php';

class Tools_Db_Repair_Structure extends Tools_Db_Repair
{
    protected $_resource;
    protected $config;
    protected $actionList;
    protected $logFile;

    /**
     * Init class
     */
    public function __construct()
    {
        parent::__construct();

        // to be updated
        $this->config['reference']['prefix']   = ''; // same prefix must be used for both databases
        $this->config['reference']['hostname'] = 'localhost';
        $this->config['reference']['database'] = 'omnius_vfde_st3';
        $this->config['reference']['username'] = 'omnius_vfde_dev';
        $this->config['reference']['password'] = 'password';

        $this->config['corrupted']['prefix']   = ''; // same prefix must be used for both databases
        $this->config['corrupted']['hostname'] = 'localhost';
        $this->config['corrupted']['database'] = 'omnius_vfde_dev';
        $this->config['corrupted']['username'] = 'omnius_vfde_dev';
        $this->config['corrupted']['password'] = 'password';

        // do not update below this line
        $this->actionList = array(
            'charset'    => array(),
            'engine'     => array(),
            'column'     => array(),
            'index'      => array(),
            'table'      => array(),
            'invalid_fk' => array(),
            'constraint' => array()
        );

        $this->logFile = 'repairdb_fix_db_'.date('Y_m_d').'.log';
        $this->_resource = new Tools_Db_Repair_Mysql4();
        $this->_resource->setConnection($this->config['corrupted'], Tools_Db_Repair_Mysql4::TYPE_CORRUPTED);
        $this->_resource->setConnection($this->config['reference'], Tools_Db_Repair_Mysql4::TYPE_REFERENCE);
    }

    /**
     * Run action
     * @return Tools_Db_Repair_Action|Array
     * @throws Exception
     */
    public function run()
    {
        $this->log('Starting script.');
        if (!$this->_resource->checkInnodbSupport(Tools_Db_Repair_Mysql4::TYPE_CORRUPTED)) {
            throw new Exception($this->log('Corrupted database doesn\'t support InnoDB storage engine'));
        }

        $compare = $this->_resource->compareResource();

        $referenceTables = $this->_resource->getTables(Tools_Db_Repair_Mysql4::TYPE_REFERENCE);
        $corruptedTables = $this->_resource->getTables(Tools_Db_Repair_Mysql4::TYPE_CORRUPTED);

        $type            = Tools_Db_Repair_Mysql4::TYPE_CORRUPTED;
        $error           = array();
        $success         = array();
        $requiredActions = array();

        // collect action list
        foreach ($referenceTables as $table => $tableProp) {
            if (!isset($corruptedTables[$table])) {
                $this->actionList['table'][] = array(
                    'msg'   => $this->log(sprintf('Add missing table "%s"', $table)),
                    'sql'   => $tableProp['create_sql']
                );
                $requiredActions[] = sprintf('Add missing table "%s"', $table);
            } else {
                // check charset
                if ($tableProp['charset'] != $corruptedTables[$table]['charset']) {
                    $this->actionList['charset'][] = array(
                        'msg'     => $this->log(sprintf('Change charset on table "%s" from %s to %s', $table, $corruptedTables[$table]['charset'], $tableProp['charset'])),
                        'table'   => $table,
                        'charset' => $tableProp['charset'],
                        'collate' => $tableProp['collate']
                    );

                    $requiredActions[] = sprintf('Change charset on table "%s" from %s to %s', $table, $corruptedTables[$table]['charset'], $tableProp['charset']);
                }

                // check storage
                if ($tableProp['engine'] != $corruptedTables[$table]['engine']) {
                    $this->actionList['engine'][] = array(
                        'msg'    => $this->log(sprintf('Change storage engine type on table "%s" from %s to %s', $table, $corruptedTables[$table]['engine'], $tableProp['engine'])),
                        'table'  => $table,
                        'engine' => $tableProp['engine']
                    );

                    $requiredActions[] = sprintf('Change storage engine type on table "%s" from %s to %s', $table, $corruptedTables[$table]['engine'], $tableProp['engine']);
                }

                // validate columns
                $fieldList = array_diff_key($tableProp['fields'], $corruptedTables[$table]['fields']);
                if ($fieldList) {
                    $fieldActionList = array();
                    foreach ($fieldList as $fieldKey => $fieldProp) {
                        $afterField = $this->_resource->arrayPrevKey($tableProp['fields'], $fieldKey);
                        $fieldActionList[] = array(
                            'column'    => $fieldKey,
                            'config'    => $fieldProp,
                            'after'     => $afterField
                        );
                    }

                    $this->actionList['column'][] = array(
                        'msg'    => $this->log(sprintf('Add missing field(s) "%s" to table "%s"', join(', ', array_keys($fieldList)), $table)),
                        'table'  => $table,
                        'action' => $fieldActionList
                    );

                    $requiredActions[] = sprintf('Add missing field(s) "%s" to table "%s"', join(', ', array_keys($fieldList)), $table);
                }

                //validate indexes
                $keyList = array_diff_key($tableProp['keys'], $corruptedTables[$table]['keys']);
                if ($keyList) {
                    $keyActionList = array();
                    foreach ($keyList as $keyProp) {
                        $keyActionList[] = array(
                            'config' => $keyProp
                        );
                    }

                    $this->actionList['index'][] = array(
                        'msg'    => $this->log(sprintf('Add missing index(es) "%s" to table "%s"', join(', ', array_keys($keyList)), $table)),
                        'table'  => $table,
                        'action' => $keyActionList
                    );

                    $requiredActions[] = sprintf('Add missing index(es) "%s" to table "%s"', join(', ', array_keys($keyList)), $table);
                }

                // validate invalid foreign keys
                foreach ($corruptedTables[$table]['constraints'] as $fk => $fkProp) {
                    if (!array_key_exists($fk, $tableProp['constraints'])) {
                        $this->actionList['invalid_fk'][] = array(
                            'msg' => $this->log(sprintf('Remove invalid foreign key(s) "%s" from table "%s"', $fk, $table)),
                            'table'      => $table,
                            'constraint' => $fkProp['fk_name']
                        );

                        $requiredActions[] = sprintf('Remove invalid foreign key(s) "%s" from table "%s"', $fk, $table);
                        unset($corruptedTables[$table]['constraints'][$fk]);
                    }
                }

                // validate foreign keys
                $constraintList = array_diff_key($tableProp['constraints'], $corruptedTables[$table]['constraints']);
                if ($constraintList) {
                    $constraintActionList = array();
                    foreach ($constraintList as $constraintConfig) {
                        $constraintActionList[] = array(
                            'config'    => $constraintConfig
                        );
                    }

                    $this->actionList['constraint'][] = array(
                        'msg'    => $this->log(sprintf('Add missing foreign key(s) "%s" to table "%s"', join(', ', array_keys($constraintList)), $table)),
                        'table'  => $table,
                        'action' => $constraintActionList
                    );

                    $requiredActions[] = sprintf('Add missing foreign key(s) "%s" to table "%s"', join(', ', array_keys($constraintList)), $table);
                }

                // validate triggers
                $triggersList = array_diff_key($tableProp['triggers'], $corruptedTables[$table]['triggers']);
                if ($triggersList) {
                    $triggerActionList = array();
                    foreach ($triggersList as $triggerConfig) {
                        $triggerActionList[] = array(
                            'config' => $triggerConfig
                        );
                    }

                    $this->actionList['trigger'][] = array(
                        'msg'    => $this->log(sprintf('You have missed trigger(s) "%s" to table "%s"', join(', ', array_keys($triggersList)), $table)),
                        'table'  => $table,
                        'action' => $triggerActionList
                    );

                    $requiredActions[] = sprintf('You have missed trigger(s) "%s" to table "%s"', join(', ', array_keys($triggersList)), $table);
                }
            }
        }

        // if in readonly mode, stop here and return actions required
        if ($this->getReadOnly() == true) {
            $this->log('Stop script.');
            return $requiredActions;
        }

        // update db records
        $this->_resource->start($type);
        foreach ($this->actionList['charset'] as $actionProp) {
            $this->_resource->begin($type);
            try {
                $this->_resource->changeTableCharset(
                    $actionProp['table'],
                    $type,
                    $actionProp['charset'],
                    $actionProp['collate']
                );
                $this->_resource->commit($type);
                $success[] = $actionProp['msg'];
            } catch (Exception $e) {
                $this->_resource->rollback($type);
                $error[] = $e->getMessage();
            }
        }

        foreach ($this->actionList['engine'] as $actionProp) {
            $this->_resource->begin($type);
            try {
                $this->_resource->changeTableEngine($actionProp['table'], $type, $actionProp['engine']);
                $this->_resource->commit($type);
                $success[] = $actionProp['msg'];
            } catch (Exception $e) {
                $this->_resource->rollback($type);
                $error[] = $e->getMessage();
            }
        }

        foreach ($this->actionList['column'] as $actionProp) {
            $this->_resource->begin($type);
            try {
                foreach ($actionProp['action'] as $action) {
                    $this->_resource->addColumn(
                        $actionProp['table'],
                        $action['column'],
                        $action['config'],
                        $type,
                        $action['after']
                    );
                }
                $this->_resource->commit($type);
                $success[] = $actionProp['msg'];
            } catch (Exception $e) {
                $this->_resource->rollback($type);
                $error[] = $e->getMessage();
            }
        }

        foreach ($this->actionList['index'] as $actionProp) {
            $this->_resource->begin($type);
            try {
                foreach ($actionProp['action'] as $action) {
                    $this->_resource->addKey($actionProp['table'], $action['config'], $type);
                }
                $this->_resource->commit($type);
                $success[] = $actionProp['msg'];
            } catch (Exception $e) {
                $this->_resource->rollback($type);
                $error[] = $e->getMessage();
            }
        }

        foreach ($this->actionList['table'] as $actionProp) {
            $this->_resource->begin($type);
            try {
                $this->_resource->sqlQuery($actionProp['sql'], $type);
                $this->_resource->commit($type);
                $success[] = $actionProp['msg'];
            } catch (Exception $e) {
                $this->_resource->rollback($type);
                $error[] = $e->getMessage();
            }
        }

        foreach ($this->actionList['invalid_fk'] as $actionProp) {
            $this->_resource->begin($type);
            try {
                $this->_resource->dropConstraint($actionProp['table'], $actionProp['constraint'], $type);
                $this->_resource->commit($type);
                $success[] = $actionProp['msg'];
            } catch (Exception $e) {
                $this->_resource->rollback($type);
                $error[] = $e->getMessage();
            }
        }

        foreach ($this->actionList['constraint'] as $actionProp) {
            $this->_resource->begin($type);
            try {
                foreach ($actionProp['action'] as $action) {
                    $this->_resource->addConstraint($action['config'], $type);
                }
                $this->_resource->commit($type);
                $success[] = $actionProp['msg'];
            } catch (Exception $e) {
                $this->_resource->rollback($type);
                $error[] = $e->getMessage();
            }
        }

        if (isset($this->actionList['trigger'])) {
            foreach ($this->actionList['trigger'] as $actionProp) {
                $error[] = $actionProp['msg'];
            }
        }

        $this->_resource->finish($type);
        $this->log('Stop script.');
        return $this;
    }
}
