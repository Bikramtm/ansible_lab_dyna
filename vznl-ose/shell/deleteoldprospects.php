<?php

require_once 'abstract.php';

/*
 * Class Update_Prospects
 */
class Update_Prospects extends Mage_Shell_Abstract
{
    private $prospectsCollection;

    public function run(){
        // 6 months ago timestamp
        $periodTimestamp = date('Y-m-d',strtotime('-6 months'));

        $this->prospectsCollection = $this->getProspectCustomers($periodTimestamp);

        // Delete quotes of prospect customers
        foreach($this->prospectsCollection as $prospectCustomer){
            $this->deleteQuoteByCustomerId($prospectCustomer->getId(),$periodTimestamp);
        }

        // Delete prospect customers
        $this->deleteProspects($this->prospectsCollection);
    }

    /*
     * Get prospect customers older than 6 months (configurable timestamp)
     */
    private function getProspectCustomers($timestamp) {
        return Mage::getModel('customer/customer')
                                ->getCollection()
                                ->addAttributeToFilter('is_prospect', 1)
                                ->addAttributeToFilter('created_at', array('lteq' => $timestamp));
    }

    /**
     * Delete saved carts and offers for a specified prospect customer
     * @param $prospectCustomerId
     * @param $timestamp
     */
    private function deleteQuoteByCustomerId($prospectCustomerId,$timestamp){
        try{
            $quoteCollection = Mage::getModel('sales/quote')->getCollection()
                ->addFieldToFilter('customer_id', $prospectCustomerId);

            $isActiveProspect = false;
            foreach($quoteCollection as $quote){
                $updatedAtTimestamp = strtotime($quote->getData('updated_at'));
                $givenTimestamp = strtotime($timestamp);
                // check if quote has been updated
                if($updatedAtTimestamp > $givenTimestamp){
                    // only quotes with cart_status = 1 / 2 are taken into consideration
                    if((($quote->getData('cart_status') == Omnius_Checkout_Model_Sales_Quote::CART_STATUS_SAVED)
                        || ($quote->getData('cart_status') == Omnius_Checkout_Model_Sales_Quote::CART_STATUS_RISK))
                        || $quote->getIsOffer()){
                        // Update prospects delete list by removing those who have saved shopping carts/quotes recently updated
                        $this->removeActiveProspect($prospectCustomerId);
                        $isActiveProspect = true;
                    }
                }
            }
            // if shopping cart/s are not recently updated delete them
            if(!$isActiveProspect){
                $this->deleteQuote($quoteCollection);
            }
        } catch(Exception $e) {
            print('Error deleting quote: ' . $e->getMessage());
        }
    }

    /**
     * Remove active prospect from those who are going to be deleted
     * @param $prospectCustomerId
     */
    private function removeActiveProspect($prospectCustomerId)
    {
        foreach($this->prospectsCollection as $prospect){
            if($prospect->getData('entity_id') == $prospectCustomerId){
                $this->prospectsCollection->removeItemByKey($prospect->getId());
            }
        }
    }

    /**
     * Delete quote/s from collection
     * @param $quoteCollection
     */
    private function deleteQuote($quoteCollection)
    {
        foreach($quoteCollection as $quote){
            if ($quote->getIsOffer()) {
                // make coupon reusable
                Mage::helper('pricerules')->decrementAll($quote);
            }
            $quote->setIsActive(false);
            $quote->delete();
        }
    }
    /**
     * Delete given prospect customers
     * @param $collection
     */
    private function deleteProspects($collection){
        foreach($collection as $prospectCustomer){
            $prospectCustomer->delete();
        }
    }
}

$shell = new Update_Prospects();
$shell->run();