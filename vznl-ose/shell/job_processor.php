<?php

require_once 'abstract.php';

class Dyna_Job_Processor extends Mage_Shell_Abstract
{
    public function __construct()
    {
        global $argv;
        $this->_appCode = 'telesales';
        if(isset($argv[1]) && $argv[1] !='-h') {
            $this->_appCode = $argv[1];
        }

        parent::__construct();
    }

    /**
     * Run script
     *
     */
    public function run()
    {
        if ($this->getArg('help')) {
            $this->usageHelp();
        } else {
            $helper = Mage::helper('dyna_job');
            $consumer = $helper->buildConsumer();
            $consumer->consume();
        }
    }

    /**
     * Retrieve Usage Help Message
     *
     */
    public function usageHelp()
    {
        return <<<USAGE
Usage:  php job_processor.php <appCode> -- [options]
   appCode                    Magento app code
   -h                          This help

USAGE;
    }
}

$shell = new Dyna_Job_Processor();
$shell->run();
