#!/bin/bash
echo "Hello "$USER" welcome"
echo "With this script you can configure Varnish as desired"
PS3='Please enter your choice: '
options=("Varnish production" "Varnish no cache" "Varnish no CSS/JS cache" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "Varnish production")
            sudo cp /vagrant/shell/vcl4.0-production.vcl /etc/varnish/default.vcl
            sudo /etc/init.d/varnish stop
            sudo /etc/init.d/varnish start
            echo "Production VCL applied"
            break
            ;;
        "Varnish no cache")
            sudo cp /vagrant/shell/vcl4.0-nocache.vcl /etc/varnish/default.vcl
            sudo /etc/init.d/varnish stop
            sudo /etc/init.d/varnish start
            echo "No cache VCL applied"
            break
            ;;
        "Varnish no CSS/JS cache")
            sudo cp /vagrant/shell/vcl4.0-nocssjscache.vcl /etc/varnish/default.vcl
            sudo /etc/init.d/varnish stop
            sudo /etc/init.d/varnish start
            echo "No CSS/JS cache applied"
            break
            ;;
        "Quit")
            break
            ;;
        *) echo invalid option;;
    esac
done
