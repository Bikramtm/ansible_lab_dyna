<?php
require_once 'abstract.php';

class Dyna_Convert_Cli extends Mage_Shell_Abstract
{
    public function run()
    {
        $xmlPath = $this->getArg('xmlPath');
        $jsonPath = $this->getArg('jsonPath');
        if(!$xmlPath && !$jsonPath){
            echo $this->usageHelp();
            exit(1);
        }
        if(!$xmlPath || substr(strrchr($xmlPath,'.'),1) != 'xml' || !file_exists(Mage::getBaseDir('base') . DIRECTORY_SEPARATOR . $xmlPath)){
            fwrite(STDERR, 'Missing xml path' . PHP_EOL);
            exit(1);
        }
        if(substr(strrchr($jsonPath,'.'),1) != 'json'){
            fwrite(STDERR, 'Not a valid json path' . PHP_EOL);
            exit(1);
        }

        $xml = file_get_contents(Mage::getBaseDir('base') . DIRECTORY_SEPARATOR . $xmlPath);

        // SimpleXML seems to have problems with the colon ":" in the <xxx:yyy> response tags, so take them out
        $xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$3", $xml);
        $xml = simplexml_load_string($xml);
        $json = json_encode($xml);
        $responseArray = json_decode($json,true);

        file_put_contents(Mage::getBaseDir('base') . DIRECTORY_SEPARATOR . $jsonPath, json_encode($responseArray, JSON_UNESCAPED_SLASHES));
    }

    public function usageHelp()
    {
        return <<<USAGE
Usage:  php convertStub.php -- [options]

  --xmlPath <path to xml>            - The path to the xml file relative to application root location (e.g. var/test.xml)
  --jsonPath <path to json>            - The path where the json file will be generated relative to application root location (e.g. var/test.xml)
  help                                      - This help

USAGE;
    }
}

$convert = new Dyna_Convert_Cli();
$convert->run();
