clear
echo "---------------------------"
echo "-See Logs"
echo "-Logged in as: $USER"
echo "---------------------------"
echo ""
echo "---System log---"
tput setaf 4; sudo tail /vagrant/var/log/system.log;
tput sgr0;echo "---Kernel log---"
tput setaf 6; sudo tail /var/log/kern.log;
tput sgr0; echo "---Nginx---"
tput sgr0; echo "Nginx access.log: "; tput setaf 1; sudo tail /var/log/nginx/access.log;
tput sgr0; echo "Nginx error.log: "; tput setaf 1; sudo tail /var/log/nginx/error.log;
tput sgr0; echo "Telesales access.log: "; tput setaf 1; sudo tail /var/log/nginx/telesales.dev.dynacommerce.io_access.log;
tput sgr0; echo "Telesales error.log: "; tput setaf 1; sudo tail /var/log/nginx/telesales.dev.dynacommerce.io_error.log;
tput sgr0; echo "---PHP---"
sudo tail /var/log/php7.0-fpm.log;
tput sgr0; echo "---MySQL---"
tput sgr0; echo "mysql.log: "; tput setaf 1; sudo tail /var/log/mysql.log
tput sgr0; echo "mysql error.log: "; tput setaf 1; sudo tail /var/log/mysql/error.log;
tput sgr0; echo "---Redis---"
tput sgr0; echo "redis.log: "; tput setaf 3; sudo tail /var/log/redis/redis.log;
tput sgr0; echo "redis-server.log: "; tput setaf 3; sudo tail /var/log/redis/redis-server.log;
tput sgr0; echo "---Magento---"
tput sgr0; echo "CableOmniusRules: "; tput setaf 5; sudo tail /vagrant/var/log/vfde/CableOmniusRules.log;
tput sgr0; echo "Package-validation: "; tput setaf 5; sudo tail /vagrant/var/log/vfde/packages-validation.log;
tput sgr0; echo "BundleRules: "; tput setaf 5; sudo tail /vagrant/var/log/vfde/bundleRules.log;
tput sgr0;
echo "---------------------------"
echo "-End of logfiles"
echo "---------------------------"