<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

require_once 'abstract.php';

class Dyna_CatalogImport_Cli extends Mage_Shell_Abstract
{
    private $hostname = null;
    private $username = null;
    private $password = null;
    private $databaseName = null;
    private $temporaryDirectory = "/tmp"; #Temporary linux folder
    private $catalogFile = "";

    public function run()
    {
        $tempFolder = $this->getArg('temp');
        $importCatalogFile = $this->getArg('import');

        if ($tempFolder) {
            $this->temporaryDirectory = $tempFolder;
        }

        if ($importCatalogFile) {
            $this->catalogFile = $importCatalogFile;
        }

        if (strlen($this->catalogFile) == 0) {
            $this->_writeLine('The catalog cannot be imported because its import file location was not set!');
            exit(1);
        }

        $config = Mage::getConfig()->getResourceConnectionConfig("default_setup");

        $this->waitForApproval($config->dbname);

        $this->_writeLine('Connecting to database');

        $databaseInfo = array(
            "host" => $config->host,
            "user" => $config->username,
            "pass" => $config->password,
            "dbname" => $config->dbname
        );

        $hostname = $databaseInfo["host"];
        $username = $databaseInfo["user"];
        $password = $databaseInfo["pass"];
        $databaseName = $databaseInfo["dbname"];

        if ($databaseName == "magento") {
            $this->_writeLine('Database could not be found, cannot continue script');
            exit;
        }

        if (isset($this->temporaryDirectory) && isset($this->catalogFile)) {
            $this->_writeLine('Starting import of catalog to ' . $databaseName);
            $this->import_sqlFile($hostname, $username, $password, $databaseName, $importCatalogFile);
        }
        else
        {
            $this->_writeLine('Temporary directory or catalogFile not set, cannot continue script.');
        }
    }

    function waitForApproval($databaseName)
    {
        $this->_writeLine("Are you sure you want to import the catalog into $databaseName ?  Type 'yes' to continue");

        $handle = fopen ("php://stdin","r");
        $line = fgets($handle);

        if (trim($line) != 'yes')
        {
            $this->_writeLine("ABORTING SCRIPT!");
            exit(1);
        }
        fclose($handle);
        $this->_writeLine(¨);
    }

    /**
     * @param string $msg
     */
    protected function _writeLine($msg)
    {
        echo $msg . PHP_EOL;
    }

    function import_sqlFile($host, $user, $pass, $name, $catalog)
    {
        if (!file_exists($catalog))
        {
            $this->_writeLine("Catalog file does not exist, aborting script");
            exit(1);
        }

        if (($unzipLocation = $this->extractCatalog($catalog)) !== false) {

            $files = scandir($unzipLocation);
            foreach ($files as $file) {
                $databaseFileName = $file;
            }

            if (isset($databaseFileName) && strlen($databaseFileName) > 0) {
                $result = $this->importSqlToDatabase($host, $user, $pass, $name, $unzipLocation . DIRECTORY_SEPARATOR . $databaseFileName);

                if ($result)
                {
                    $this->copyCableArtifactFile($unzipLocation, Mage::getBaseDir('lib'));
                    $this->copyCatalogFile($unzipLocation, Mage::getBaseDir('media'));
                }
            }

            $removeCurrentTarFiles = "rm -R $unzipLocation";

            system($removeCurrentTarFiles, $returnvalue);
            if ($returnvalue == 0) {
                $this->_writeLine("temporary catalog directory removed");
            } else {
                $this->_writeLine("Cannot temporary catalog directory");
            }

            $this->_writeLine("Import catalog script " . (($result) ? "success" : "failed" ));
            if (isset($result) && $result)
            {
                exit(0);
            }
            else
            {
                exit(1);
            }
        } else {
            $this->_writeLine("Catalog could not be extracted, aborting script");
            exit(1);
        }
    }

    private function importSqlToDatabase($host, $user, $pass, $name, $databaseFile)
    {
        $command = "mysql -h$host -u$user --default-character-set=utf8  -p$pass $name < $databaseFile";
        $this->_writeLine("Importing catalog to database");
        system($command, $returnvalue);

        if ($returnvalue == 0) {
            $this->_writeLine("Catalog was imported successfully into '$name'");
            return true;
        }
        else
        {
            $this->_writeLine("Catalog could not be imported!");
            return false;
        }
    }

    private function extractCatalog($catalog)
    {
        $unzipLocation = $this->temporaryDirectory . "/import_" . date('Ymd_hms');

        if (!file_exists($unzipLocation))
        {
            mkdir($unzipLocation, 0777, true);
        }

        $unzipCurrentTarFile = "gzip -dc $catalog | tar -xf - -C $unzipLocation --strip-components 1";
        $extractCurrentGzFile = "for i in $unzipLocation/*.gz; do gunzip \"\$i\"; done";
        system($unzipCurrentTarFile, $returnvalue);

        if ($returnvalue == 0) {
            $this->_writeLine("Catalog extracted");

            system($extractCurrentGzFile, $returnvalue2);

            if ($returnvalue2 == 0) {
                $this->_writeLine("gz file unzipped");
            }
            else
            {
                $this->_writeLine("Cannot unzip the tar file");
                return false;
            }
            return $unzipLocation;
        }
        return false;
    }

    public function copyCatalogFile($catalogSource, $catalogDestination)
    {
        $command = "cp $catalogSource" . DIRECTORY_SEPARATOR . "APP_VERSION.php $catalogDestination";
        system($command, $returnvalue);

        if ($returnvalue == 0)
        {

            $this->_writeLine("Catalog file copied to destination");
            return true;
        }
        else
        {
            $this->_writeLine("Catalog file could not be copied to its destination");
            return false;
        }
    }

    public function copyCableArtifactFile($catalogSource, $catalogDestination)
    {
        $command = "cp $catalogSource" . DIRECTORY_SEPARATOR . "cable-rules-plugin.phar $catalogDestination";
        system($command, $returnvalue);

        if ($returnvalue == 0)
        {

            $this->_writeLine("Cable Artifact file copied to destination");
            return true;
        }
        else
        {
            $this->_writeLine("Cable Artifact file could not be copied to its destination");
            return false;
        }
    }

    /**
     * Retrieve Usage Help Message
     */
    public function usageHelp()
    {
        return <<<USAGE
Usage:  php file      
  help				This help
  --import            Imports the given catalog file into the specific environment's database
  temp              Use specified temporary directory for temp files    (Default: /tmp)
  
  Example: php importcatalogtodatabase.php --import /tmp/catalogexport_2017-05-05-12-17-49_127.0.0.1_omnius_vfde_st3.tar.gz [imports the catalog to the environments database]
USAGE;
    }
}

// Example
//php importcatalogtodatabase.php --import EXPORTEDCATALOGFILEPATH

$import = new Dyna_CatalogImport_Cli();
$import->run();
