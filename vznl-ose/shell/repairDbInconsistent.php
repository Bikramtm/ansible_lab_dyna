<?php
require_once 'repairDbClass.php';

/*
 * Removes inconsistent data from tables with broken foreign key references
 * */
class Tools_Db_Repair_Inconsistent extends Tools_Db_Repair
{
    const SQL_FOREIGN   = "SELECT TABLE_NAME, COLUMN_NAME, REFERENCED_TABLE_NAME, REFERENCED_COLUMN_NAME, CONSTRAINT_NAME
                           FROM information_schema.KEY_COLUMN_USAGE 
                           WHERE TABLE_SCHEMA = '%s' AND 
                           CONSTRAINT_NAME != 'PRIMARY' AND 
                           NOT ISNULL(`REFERENCED_TABLE_NAME`) AND 
                           NOT ISNULL(`REFERENCED_COLUMN_NAME`);";
    const SQL_SEARCH    = "SELECT * FROM `%s` WHERE `%s` NOT IN (SELECT `%s` FROM `%s`);";
    const SQL_UPDATE    = "UPDATE `%s` SET `%s` = NULL WHERE `%s` = '%s';";
    const SQL_COL_INFO  = "SHOW FULL COLUMNS FROM `%s` WHERE ";

    /**
     * Tools_Db_Repair_Inconsistent constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->logFile = "repairdb_fix_inconsistent_".date('Y_m_d').".log";
    }

    /**
     * Run script
     */
    public function run()
    {
        $this->log("Start script.");

        $actions     = [];
        $foreignKeys = $this->searchForeignKeys();

        foreach ($foreignKeys as $foreignKey) {
            $sql = sprintf(
                self::SQL_SEARCH,
                $foreignKey['TABLE_NAME'],
                $foreignKey['COLUMN_NAME'],
                $foreignKey['REFERENCED_COLUMN_NAME'],
                $foreignKey['REFERENCED_TABLE_NAME']
            );

            //$this->log("Running: ".$sql);
            $results = $this->readConnection->fetchAll($sql);


            foreach ($results as $result) {
                $this->log("Found orphan row: ".json_encode($foreignKey)." - ".json_encode($result));
                $colInfo = $this->getTableInfo($foreignKey, false);

                if ($colInfo['Null'] == 'YES') {
                    $primaryCol = $this->getTableInfo($foreignKey, true);

                    if ($primaryCol) {
                        $actions[] = sprintf(self::SQL_UPDATE, $foreignKey['TABLE_NAME'], $foreignKey['COLUMN_NAME'], $primaryCol['Field'], $result[$primaryCol['Field']]);
                    } else {
                        $this->log(sprintf("Orphan field(%s) not updated; no primary key found.", $foreignKey['COLUMN_NAME']));
                    }
                } else {
                    $this->log(sprintf("Orphan field(%s) not allowed to be updated as null.", $foreignKey['COLUMN_NAME']));
                }
            }
        }

        // if in readonly mode, just return the required actions
        if ($this->getReadOnly() == true) {
            $this->log("Stop script.");
            return ($actions) ? ["Found inconsistent data from tables with broken foreign key references."] : [];
        }

        // update db records
        foreach ($actions as $action) {
            $this->log("Running: ".$action);
            $this->log("Orphan field updated as null.");
            $this->writeConnection->exec($action);
        }

        $this->log("Stop script.");
        return $this;
    }

    private function getTableInfo($foreignKey, $getPrimary = false)
    {
        return $this->readConnection->fetchRow(
            sprintf(self::SQL_COL_INFO, $foreignKey['TABLE_NAME']).(($getPrimary) ? "`Key` = 'PRI'" : "`Field` = '{$foreignKey['COLUMN_NAME']}'")
        );
    }

    /**
     * Search all tables/columns with foreign key
     * @return mixed
     */
    private function searchForeignKeys()
    {
        return $this->readConnection->fetchAll(sprintf(self::SQL_FOREIGN, $this->dbName));
    }
}
