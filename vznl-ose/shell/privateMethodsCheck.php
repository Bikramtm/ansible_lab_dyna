<?php
define('DS', '/');
$root = dirname(dirname(__FILE__)) . DS . 'app' . DS . 'code' . DS;

switch ($_SERVER['argv'][2]) {
    case 'local':
        // todo: grep for files
        // grep -rl "private function " * in app/code/local

        $root .= 'local';

        $files = [
            "Aleron75/Magemonolog/lib/composer/ClassLoader.php",
            "Aleron75/Magemonolog/lib/justinrainbow/json-schema/src/JsonSchema/Constraints/NumberConstraint.php",
            "Aleron75/Magemonolog/lib/justinrainbow/json-schema/src/JsonSchema/Constraints/StringConstraint.php",
            "Aleron75/Magemonolog/lib/justinrainbow/json-schema/src/JsonSchema/Uri/Retrievers/Curl.php",
            "Aleron75/Magemonolog/lib/justinrainbow/json-schema/src/JsonSchema/Uri/Retrievers/FileGetContents.php",
            "Aleron75/Magemonolog/lib/justinrainbow/json-schema/tests/JsonSchema/Tests/Constraints/TypeTest.php",
            "Aleron75/Magemonolog/lib/justinrainbow/json-schema/tests/JsonSchema/Tests/Drafts/BaseDraftTestCase.php",
            "Aleron75/Magemonolog/lib/justinrainbow/json-schema/tests/JsonSchema/Tests/Uri/UriRetrieverTest.php",
            "Aleron75/Magemonolog/lib/monolog/monolog/doc/04-extending.md",
            "Aleron75/Magemonolog/lib/monolog/monolog/src/Monolog/Formatter/HtmlFormatter.php",
            "Aleron75/Magemonolog/lib/monolog/monolog/src/Monolog/Formatter/NormalizerFormatter.php",
            "Aleron75/Magemonolog/lib/monolog/monolog/src/Monolog/Handler/CubeHandler.php",
            "Aleron75/Magemonolog/lib/monolog/monolog/src/Monolog/Handler/FleepHookHandler.php",
            "Aleron75/Magemonolog/lib/monolog/monolog/src/Monolog/Handler/FlowdockHandler.php",
            "Aleron75/Magemonolog/lib/monolog/monolog/src/Monolog/Handler/HipChatHandler.php",
            "Aleron75/Magemonolog/lib/monolog/monolog/src/Monolog/Handler/PHPConsoleHandler.php",
            "Aleron75/Magemonolog/lib/monolog/monolog/src/Monolog/Handler/PushoverHandler.php",
            "Aleron75/Magemonolog/lib/monolog/monolog/src/Monolog/Handler/SlackHandler.php",
            "Aleron75/Magemonolog/lib/monolog/monolog/src/Monolog/Handler/SocketHandler.php",
            "Aleron75/Magemonolog/lib/monolog/monolog/src/Monolog/Handler/StreamHandler.php",
            "Aleron75/Magemonolog/lib/monolog/monolog/src/Monolog/Handler/SyslogUdpHandler.php",
            "Aleron75/Magemonolog/lib/monolog/monolog/src/Monolog/Processor/IntrospectionProcessor.php",
            "Aleron75/Magemonolog/lib/monolog/monolog/src/Monolog/Processor/WebProcessor.php",
            "Aleron75/Magemonolog/lib/monolog/monolog/tests/Monolog/Formatter/GelfMessageFormatterTest.php",
            "Aleron75/Magemonolog/lib/monolog/monolog/tests/Monolog/Handler/FlowdockHandlerTest.php",
            "Aleron75/Magemonolog/lib/monolog/monolog/tests/Monolog/Handler/HipChatHandlerTest.php",
            "Aleron75/Magemonolog/lib/monolog/monolog/tests/Monolog/Handler/LogEntriesHandlerTest.php",
            "Aleron75/Magemonolog/lib/monolog/monolog/tests/Monolog/Handler/PushoverHandlerTest.php",
            "Aleron75/Magemonolog/lib/monolog/monolog/tests/Monolog/Handler/RavenHandlerTest.php",
            "Aleron75/Magemonolog/lib/monolog/monolog/tests/Monolog/Handler/SlackHandlerTest.php",
            "Aleron75/Magemonolog/lib/monolog/monolog/tests/Monolog/Handler/SocketHandlerTest.php",
            "Aleron75/Magemonolog/lib/predis/predis/bin/create-single-file",
            "Aleron75/Magemonolog/lib/predis/predis/examples/debuggable_connection.php",
            "Aleron75/Magemonolog/lib/predis/predis/src/Client.php",
            "Aleron75/Magemonolog/lib/predis/predis/src/Cluster/Distributor/HashRing.php",
            "Aleron75/Magemonolog/lib/predis/predis/src/Command/ZSetUnionStore.php",
            "Aleron75/Magemonolog/lib/predis/predis/src/Connection/AbstractConnection.php",
            "Aleron75/Magemonolog/lib/predis/predis/src/Connection/PhpiredisSocketConnection.php",
            "Aleron75/Magemonolog/lib/predis/predis/src/Connection/PhpiredisStreamConnection.php",
            "Aleron75/Magemonolog/lib/predis/predis/src/Connection/WebdisConnection.php",
            "Aleron75/Magemonolog/lib/predis/predis/src/Monitor/Consumer.php",
            "Aleron75/Magemonolog/lib/predis/predis/src/Pipeline/Pipeline.php",
            "Aleron75/Magemonolog/lib/predis/predis/src/Profile/Factory.php",
            "Aleron75/Magemonolog/lib/predis/predis/src/PubSub/Consumer.php",
            "Aleron75/Magemonolog/lib/predis/predis/src/Transaction/MultiExec.php",
            "Aleron75/Magemonolog/Model/HandlerWrapper/RedisHandler.php",
            "Aleron75/Magemonolog/Model/Logwriter.php",
            "Dyna/Address/Helper/Data.php",
            "Dyna/Address/Model/Client/CheckServiceAbilityClient.php",
            "Dyna/AgentDE/Model/Import/PhoneBookIndustries.php",
            "Dyna/AgentDE/Model/Import/PhoneBookKeywords.php",
            "Dyna/AgentDE/Model/Import/VodafoneShip2Stores.php",
            "Dyna/Bundles/controllers/Adminhtml/ActionsController.php",
            "Dyna/Bundles/controllers/Adminhtml/BundlesController.php",
            "Dyna/Bundles/controllers/Adminhtml/CampaignsController.php",
            "Dyna/Bundles/controllers/Adminhtml/DealercodeController.php",
            "Dyna/Bundles/controllers/Adminhtml/OffersController.php",
            "Dyna/Bundles/controllers/Adminhtml/RulesController.php",
            "Dyna/Bundles/controllers/IndexController.php",
            "Dyna/Bundles/Helper/Data.php",
            "Dyna/Bundles/Helper/Redplus.php",
            "Dyna/Bundles/Model/Expression/PackageDummyScope.php",
            "Dyna/Bundles/Model/Expression/QuotePackageDummyScope.php",
            "Dyna/Cable/Model/Import/CableProduct.php",
            "Dyna/Cable/Model/Import/CableProductJsonFile.php",
            "Dyna/Catalog/Model/Import/CategoriesTree.php",
            "Dyna/Catalog/Model/Product/Import/CheckoutProduct.php",
            "Dyna/Checkout/Block/Call.php",
            "Dyna/Checkout/Block/Cart/Steps/SaveChangeProvider.php",
            "Dyna/Checkout/Block/Offer.php",
            "Dyna/Checkout/controllers/CartController.php",
            "Dyna/Checkout/controllers/IndexController.php",
            "Dyna/Checkout/Helper/Data.php",
            "Dyna/Checkout/Helper/Layout.php",
            "Dyna/Checkout/Model/Client/ConvertAndValidateIBANClient.php",
            "Dyna/Checkout/Model/Client/GetPayerAndPaymentInfoClient.php",
            "Dyna/Checkout/Model/Client/SAPShippingConditionClient.php",
            "Dyna/Checkout/Model/Client/ShippingFeeCalculateClient.php",
            "Dyna/Checkout/Model/Sales/Quote.php",
            "Dyna/Configurator/controllers/InitController.php",
            "Dyna/Configurator/Model/Expression/Availability.php",
            "Dyna/Customer/controllers/CallController.php",
            "Dyna/Customer/controllers/DetailsController.php",
            "Dyna/Customer/controllers/IndexController.php",
            "Dyna/Customer/controllers/SearchController.php",
            "Dyna/Customer/Helper/Customer.php",
            "Dyna/Customer/Helper/Search.php",
            "Dyna/Customer/Helper/Validation.php",
            "Dyna/Customer/Model/Client/CreateOrAddLinkClient.php",
            "Dyna/Customer/Model/Client/HouseholdMembersClient.php",
            "Dyna/Customer/Model/Client/ObjectSearchClient.php",
            "Dyna/Customer/Model/Client/PotentialLinksClient.php",
            "Dyna/Customer/Model/Client/RetrieveCustomerInfo.php",
            "Dyna/Customer/Model/Client/SearchCustomerClient.php",
            "Dyna/Customer/Model/Client/SearchCustomerFromLegacyClient.php",
            "Dyna/Fixed/Model/Import/FixedProduct.php",
            "Dyna/Import/Helper/Data.php",
            "Dyna/Import/Model/Generator/CategoryDefinitionAbstract.php",
            "Dyna/Import/Model/Generator/CategoryProductsAbstract.php",
            "Dyna/Import/Model/Generator/Fixed/Definition/ProductAbstract.php",
            "Dyna/Import/Model/Generator/Mobile/CategoryAbstract.php",
            "Dyna/Magemonolog/Model/Logwriter.php",
            "Dyna/MixMatch/Model/Indexer/Mixmatch.php",
            "Dyna/Mobile/Model/Import/MobileCategory.php",
            "Dyna/Mobile/Model/Import/MobileProduct.php",
            "Dyna/MultiMapper/Model/Addon.php",
            "Dyna/MultiMapper/Model/Importer.php",
            "Dyna/MultiMapper/Model/ImporterXML.php",
            "Dyna/MultiMapper/Model/Mapper.php",
            "Dyna/Operator/Model/Import/ServiceProvider.php",
            "Dyna/Package/Model/Import/Package.php",
            "Dyna/Package/Model/Package.php",
            "Dyna/ProductMatchRule/Model/Importer.php",
            "Dyna/ProductMatchRule/Model/ImporterXML.php",
            "Dyna/Sandbox/Helper/Data.php",
            "Dyna/Service/Model/DotAccessor.php",
            "Dyna/Superorder/Helper/Esb.php",
            "Dyna/Superorder/Model/Client/SendDocumentClient.php",
            "Dyna/Superorder/Model/Client/SubmitOrderClient/Customer.php",
            "Dyna/Superorder/Model/Client/SubmitOrderClient/Package.php",
            "Dyna/Superorder/Model/Client/SubmitOrderClient.php",
            "Dyna/Superorder/Model/Offer.php"

        ];

        break;
    case 'community':
        // todo:
        // grep -rl "private function " * in app/code/community
        $root .= 'community';
        $files = [
            "Aoe/Scheduler/Model/Resource/Job.php",
            "Omnius/Bundles/controllers/Adminhtml/BundlesController.php",
            "Omnius/Bundles/controllers/Adminhtml/CategoriesController.php",
            "Omnius/Bundles/controllers/Adminhtml/PackagesController.php",
            "Omnius/Checkout/Block/Cart.php",
            "Omnius/Checkout/controllers/CartController.php",
            "Omnius/Configurator/controllers/CartController.php",
            "Omnius/Customer/controllers/Customer/SearchController.php",
            "Omnius/Customer/Helper/Data.php",
            "Omnius/Operator/controllers/Adminhtml/FixedController.php",
            "Omnius/Operator/controllers/Adminhtml/NetworkController.php",
            "Omnius/Operator/controllers/Adminhtml/OperatorController.php",
            "Omnius/Operator/controllers/Adminhtml/ServiceController.php",
            "Omnius/PriceRules/Model/Observer.php",
            "Omnius/Sandbox/Model/CheckAttributes.php",
            "Omnius/Sandbox/Model/Demux.php",
            "Omnius/Sandbox/Model/Publisher.php",
            "Omnius/Sandbox/Model/QueryMapper.php",
            "Omnius/Service/Model/DotAccessor.php"
        ];

        break;
}


function space($item)
{
    return str_replace(' ', '', $item);
}

foreach ($files as $file) {
    // echo $file . PHP_EOL;
    $content = file_get_contents($root . DS . $file);
    preg_match_all('/private function (.*) ?\(/i', $content, $matches);
    $found = false;
    foreach ($matches[1] as $match) {
        $ok = preg_match_all('/\$([a-zA-Z0-9]+)->' . $match . '/', $content, $usageMatches);
        if ($ok === false) {
            echo "Could not process file " . $file . PHP_EOL;
            continue;
        }

        $usages = array_unique(array_map('space', $usageMatches[1]));

        foreach ($usages as $usage) {
            if ($usage != 'this') {
                $found = true;
                var_dump($usages, $match);
                echo PHP_EOL;
            }
        }
    }

    if ($found) {
        echo 'FOUND IN' . $file . PHP_EOL;
    }
}
