<?php
require_once getcwd().'/app/Mage.php';
Mage::app(0);

@set_time_limit(0);

class Tools_Db_Repair
{
    protected $step;
    protected $dbConfig;
    protected $dbName;
    protected $readConnection;
    protected $writeConnection;
    protected $logFile;
    protected $readOnly;

    /**
     * Tools_Db_Repair constructor.
     */
    public function __construct()
    {
        $this->step            = 0;
        $this->dbConfig        = Mage::getConfig()->getResourceConnectionConfig("default_setup");
        $this->dbName          = (string)Mage::getConfig()->getNode('global/resources/default_setup/connection/dbname');
        $this->readOnly        = true;
        $this->readConnection  = Mage::getSingleton('core/resource')->getConnection('core_read');
        $this->writeConnection = Mage::getSingleton('core/resource')->getConnection('core_write');
    }

    /**
     * @param $value
     */
    public function setReadOnly($value)
    {
        $this->readOnly = (bool)$value;
    }

    /**
     * @return bool
     */
    public function getReadOnly()
    {
        return (bool)$this->readOnly;
    }

    /**
     * @param $value
     */
    public function setLogFile($value)
    {
        $this->logFile = $value;
    }

    public function getLogFile()
    {
        return $this->logFile;
    }

    protected function log($message)
    {
        Mage::log($message, null, $this->logFile);
        return $message;
    }
}
