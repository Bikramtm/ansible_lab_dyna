<?php

require_once 'vznl_abstract.php';

/**
 * Class Vznl_Communication_Process_Cli
 */
class Vznl_Communication_Process_Cli extends Vznl_Shell_Abstract
{
    /**
     * Initialize application and parse input parameters
     */
    public function __construct()
    {
        $this->_lockExpireInterval = 480; // 4m
        parent::__construct();
    }

    /**
     * Run script
     */
    public function run()
    {
        try {
            if (!$this->hasLock()) {
                $this->createLock();

                $cron = new Vznl_Communication_Model_Cron();
                $cron->processJobs();

                $this->removeLock();
            } else {
                throw new Exception('Cannot start script due to existing lock: ' . $this->getLockPath());
            }
        } catch (Exception $e) {
            Mage::logException($e);
            Zend_Debug::dump($e->getMessage());
        }
    }

    /**
     * Display help on CLI
     * @return string
     */
    public function usageHelp()
    {
        $f = basename(__FILE__);
        return <<< USAGE
Usage: php ${f} [options]

  -q            Quiet, do not output to cli
  -h            Short alias for help
  help          This help
USAGE;
    }
}

$shell = new Vznl_Communication_Process_Cli();
$shell->run();
