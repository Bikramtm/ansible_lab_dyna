<?php
/**
 * Copyright (c) 2019. Dynacommerce B.V.
 */

require_once 'vznl_abstract.php';

/**
 * Class Vznl_Contract_Create_Cli
 * Create contract files
 */
class Vznl_Contract_Create_Cli extends Vznl_Shell_Abstract
{
    /**
     * Run script
     */
    public function run()
    {
        Mage::app()->getTranslator()->init(Mage_Core_Model_App_Area::AREA_FRONTEND);

        $pdfHelper = Mage::helper('vznl_checkout/pdf');

        $orders = $this->getArg('o');
        $orderList = explode(',', $orders);
        $orderList = array_map('trim', $orderList);

        foreach ($orderList as $incrementId) {
            $deliveryOrder = Mage::getModel('sales/order')->loadByIncrementId($incrementId);

            if (empty($deliveryOrder)) {
                print "No delivery order ID found for this incrementId: " . $incrementId;
                break;
            }

            $superOrder = Mage::getModel('superorder/superorder')->load($deliveryOrder->getSuperorderId());

            $appEmulation = Mage::getSingleton('core/app_emulation');
            $storeId = Mage::getModel('core/website')->load($superOrder->getWebsiteId())->getDefaultStore()->getId();
            $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($storeId);

            if ($deliveryOrder->hasMobilePackage() && $deliveryOrder->isHomeDelivery()) {
                // Contract doesn't need logo when created for printing in warehouse
                $contractOutput = Mage::helper('vznl_checkout/sales')->generateContract($deliveryOrder, true, false);
                /** @var vznl_checkout_Helper_Pdf */
                $pdfHelper->saveContract($contractOutput, $deliveryOrder->getIncrementId(), 'pdf');
                // Add the loadOverview
                if ($deliveryOrder->isLoanRequired()) {
                    $pdfHelper->saveLoanOverview($deliveryOrder, false);
                }
            }
            $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
        }
    }

    /**
     * Display help on CLI
     * @return string
     */
    public function usageHelp()
    {
        $f = basename(__FILE__);
        return <<< USAGE
Usage: php ${f} [options]

  -o            Order(s) ID, comma separated
  -q            Quiet, do not output to cli
  -h            Short alias for help
  help          This help
USAGE;
    }
}

$shell = new Vznl_Contract_Create_Cli();
$shell->run();
