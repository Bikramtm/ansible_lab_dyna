<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Shell
 * @copyright  Copyright (c) 2006-2016 X.commerce, Inc. and affiliates (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

require_once  __DIR__ . '/../vznl_abstract.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class Dyna_AMQP_Consumer extends Vznl_Shell_Abstract
{
    public function run()
    {
        $data = trim($this->getArg('c'));
        if (!$data) {
            echo "No mesages provided";
            exit(1);
        }

        $data = explode(';', $data);
        file_put_contents(__DIR__ . '/log' . date("Ymd") . '.log', var_export(date("Y-m-d h:i:s"), true) . PHP_EOL, FILE_APPEND);
        file_put_contents(__DIR__ . '/log' . date("Ymd") . '.log', var_export($data, true) . PHP_EOL, FILE_APPEND);
        $method = $this->getMethod(array_shift($data));
        try {
            $helper = Mage::helper('dyna_superorder/esb');
            if (method_exists($helper, $method)) {
                $returnData = $helper->{$method}(implode(';', $data));
                file_put_contents(__DIR__ . '/script_returns' . date("Ymd") . '.log', date("Y-m-d h:i:s") . ': Return data for' . implode(';', $data) . PHP_EOL, FILE_APPEND);
                file_put_contents(__DIR__ . '/script_returns' . date("Ymd") . '.log', date("Y-m-d h:i:s") . ': ' . $returnData . PHP_EOL, FILE_APPEND);
                echo $returnData;
            }
            else {
                echo 'Invalid method provided ' . $method;
            }
        } catch (Exception $e) {
            file_put_contents(__DIR__ . '/exception' . date("Ymd") . '.log', date("Y-m-d h:i:s") . ': ' . $e->getMessage() . PHP_EOL, FILE_APPEND);
            file_put_contents(__DIR__ . '/exception' . date("Ymd") . '.log', $e->getTraceAsString() . PHP_EOL, FILE_APPEND);
            exit(1);
        }
        exit(0);
    }


    protected function getMethod($method)
    {
        $array = explode('.', $method);
        if (isset($array[1])) {
            $method = strtolower($array[1]);
        } else {
            $method = strtolower($array[0]);
        }

        switch ($method) {
            case "setdeliverydetails" :
                return 'setDeliveryDetails';
                break;
            case "setordermsdisn" :
                return 'setOrderMsdisn';
                break;
            case "setnetworkstatus" :
                return 'setNetworkStatus';
                break;
            case "setvfcreditstatus" :
                return 'setVfCreditStatus';
                break;
            case "setvfstatuscode" :
                return 'setVfStatusCode';
                break;
            case "setvfordernr" :
                return 'setVfOrderNr';
                break;
            case "setzgordernr" :
                return 'setZgOrderNr';
                break;
            case "setcustomerban" :
                return 'setCustomerBan';
                break;
            case "setdeliveryorderstatus" :
                return 'setDeliveryOrderStatus';
                break;
            case "setsaleorderstatus" :
                return 'setSaleOrderStatus';
                break;
            case "setsaleordererror" :
                return 'setSaleOrderError';
                break;
            case "setciboodleid" :
                return 'setCiboodleId';
                break;
            case "sethomedeliverystatus" :
                return 'setHomeDeliveryStatus';
                break;
            case "setpackagestatus" :
                return 'setPackageStatus';
                break;
            case "sendgarantmail" :
                return 'sendGarantMail';
                break;
            case "setorderpackageapproval" :
                return 'setOrderPackageApproval';
                break;
            case "setactualportingdate" :
                return 'setActualPortingDate';
                break;
            case "setmanualactivation" :
                return 'setManualActivation';
                break;
            case "settemporarycustid" :
                return 'setTemporaryCustomerId';
                break;
            case "updatetemporarycustid" :
                return 'updateTemporaryCustomerId';
                break;
            default:
                throw new Exception('Invalid method received: ' . $method);
                break;
        }
    }

    /**
     * Retrieve Usage Help Message
     *
     */
    public function usageHelp()
    {
        $file = basename(__FILE__);
        return <<<USAGE
Usage:  php -f $file -- [options]

  -c command                            Execute the esc command with given params
  help                                  This help

USAGE;
    }
}

$shell = new Dyna_AMQP_Consumer();
$shell->run();
