<?php
require_once 'abstract.php';

class Dyna_RepairDb extends Mage_Shell_Abstract
{
    protected $types = [
        "backup",
        "orders",
        "quotes",
        "stores",
        "structure",
        "inconsistent",
        "autoincrement",
        "salesRules"
    ];

    /**
     * Run script based on $type
     */
    public function run()
    {
        $type = $this->getArg('type');

        if (!$type || !in_array($type, $this->types)) {
            $this->_writeLine(printf('Invalid type, please specify [ %s ].', implode('/', $this->types)));
            return;
        }

        switch ($type) {
            case "structure":
                require_once 'repairDbStructure.php';
                $repairDb = new Tools_Db_Repair_Structure();
                $repairDb->setReadOnly(false);
                $repairDb->run();
                break;

            case "autoincrement":
                require_once 'repairDbAutoincrement.php';
                $autoincrement = new Tools_Db_Repair_Autoincrement();
                $autoincrement->setReadOnly(false);
                $autoincrement->run();
                break;

            case "inconsistent":
                require_once 'repairDbInconsistent.php';
                $inconsistent = new Tools_Db_Repair_Inconsistent();
                $inconsistent->setReadOnly(false);
                $inconsistent->run();
                break;

            case "orders":
                require_once 'repairDbOrders.php';
                $orders = new Tools_Db_Repair_Orders();
                $orders->setReadOnly(false);
                $orders->run();
                break;

            case "quotes":
                require_once 'repairDbQuotes.php';
                $quotes = new Tools_Db_Repair_Quotes();
                $quotes->setReadOnly(false);
                $quotes->run();
                break;

            case "stores":
                require_once 'repairDbStores.php';
                $stores = new Tools_Db_Repair_Stores();
                $stores->setReadOnly(false);
                $stores->run();
                break;

            case "backup":
                require_once 'repairDbBackup.php';
                $backup = new Tools_Db_Perform_Backup();
                $backup->run();
                break;

            case "salesRules":
                require_once 'repairDbSalesRules.php';
                $stores = new Tools_Db_Repair_SalesRules();
                $stores->setReadOnly(false);
                $stores->run();
                break;
        }
    }

    /**
     * Retrieve Usage Help Message
     *
     */
    public function usageHelp()
    {
        $types = implode('/', $this->types);

        return <<<USAGE
Usage:  php shell/repairDb.php -- [options]

  help  This help
  type  Repair type [$types]

USAGE;
    }

    /**
     * Echo log message to console
     * @param $msg
     */
    private function _writeLine($msg)
    {
        echo $msg . PHP_EOL;
    }
}

$repairDb = new Dyna_RepairDb();
$repairDb->run();


