<?php

class Tools_Db_Repair_Mysql4
{
    const TYPE_CORRUPTED    = 'corrupted';
    const TYPE_REFERENCE    = 'reference';
    const RESERVED_WORDS    = array('CURRENT_TIMESTAMP');

    /**
     * Corrupted Database resource
     *
     * @var resource
     */
    protected $_corrupted;

    /**
     * Reference Database resource
     *
     * @var resource
     */
    protected $_reference;

    /**
     * Config
     *
     * @var array
     */
    protected $_config = array();

    /**
     * Set connection
     *
     * @param array $config
     * @param string $type
     * @return Tools_Db_Repair
     * @throws Exception
     */
    public function setConnection(array $config, $type)
    {
        if ($type == self::TYPE_CORRUPTED) {
            $connection = &$this->_corrupted;
        } elseif ($type == self::TYPE_REFERENCE) {
            $connection = &$this->_reference;
        } else {
            throw new Exception($this->log('Unknown connection type'));
        }

        $required = array('hostname', 'username', 'password', 'database', 'prefix');
        foreach ($required as $field) {
            if (!array_key_exists($field, $config)) {
                throw new Exception($this->log(sprintf('Please specify %s for %s database connection', $field, $type)));
            }
        }

        if (!$connection = @mysqli_connect($config['hostname'], $config['username'], $config['password'], $config['database'])) {
            throw new Exception($this->log(sprintf('%s database connection error: %s', ucfirst($type), mysqli_connect_error())));
        }
        /*if (!@mysqli_select_db($connection, $config['database'])) {
            throw new Exception($this->log(sprintf('Cannot select %s database (%s): %s', $type, $config['database'], mysqli_connect_error())));
        }*/

        mysqli_query($connection, 'SET NAMES utf8');

        $this->_config[$type] = $config;

        return $this;
    }

    /**
     * Check exists connections
     * @return bool
     * @throws Exception
     */
    protected function _checkConnection()
    {
        if (is_null($this->_corrupted)) {
            throw new Exception($this->log(sprintf('Invalid %s database connection', self::TYPE_CORRUPTED)));
        }
        if (is_null($this->_reference)) {
            throw new Exception($this->log(sprintf('Invalid %s database connection', self::TYPE_REFERENCE)));
        }
        return true;
    }

    /**
     * Retrieve table name
     *
     * @param string $table
     * @param string $type
     * @return string
     */
    public function getTable($table, $type)
    {
        $prefix = $this->_config[$type]['prefix'];
        return $prefix . $table;
    }

    /**
     * Retrieve connection resource
     *
     * @param string $type
     * @return resource
     * @throws Exception
     */
    protected function _getConnection($type)
    {
        if ($type == self::TYPE_CORRUPTED) {
            return $this->_corrupted;
        } elseif ($type == self::TYPE_REFERENCE) {
            return $this->_reference;
        } else {
            throw new Exception($this->log(sprintf('Unknown connection type "%s"', $type)));
        }
    }

    /**
     * Check connection type
     *
     * @param string $type
     * @return bool
     *
     * @throws Exception
     */
    protected function _checkType($type)
    {
        if ($type == self::TYPE_CORRUPTED || $type == self::TYPE_REFERENCE) {
            return true;
        } else {
            throw new Exception($this->log(sprintf('Unknown connection type "%s"', $type)));
        }
    }

    /**
     * Check exists table
     *
     * @param string $table
     * @param string $type
     * @return bool
     */
    public function tableExists($table, $type)
    {
        $this->_checkConnection();
        $this->_checkType($type);

        $sql = $this->_quote("SHOW TABLES LIKE ?", $this->getTable($table, $type), $this->_getConnection($type));
        $res = mysqli_query($this->_getConnection($type), $sql);
        $this->log("Running query: ".$sql);
        if (!mysqli_fetch_row($res)) {
            return false;
        }
        return true;
    }

    /**
     * Simple quote SQL statement
     * supported ? or %[type] sprintf format
     *
     * @param string $statement
     * @param array $bind
     * @return string
     */
    protected function _quote($statement, $bind = array(), $link)
    {
        $statement = str_replace('?', '%s', $statement);
        if (!is_array($bind)) {
            $bind = array($bind);
        }
        foreach ($bind as $k => $v) {
            if (is_numeric($v)) {
                $bind[$k] = $v;
            } elseif (is_null($v)) {
                $bind[$k] = 'NULL';
            } else {
                $bind[$k] = "'" . mysqli_escape_string($link, $v) . "'";
            }
        }
        return vsprintf($statement, $bind);
    }

    /**
     * Compare core_resource version
     * @return array
     * @throws Exception
     */
    public function compareResource()
    {
        if (!$this->tableExists('core_resource', self::TYPE_CORRUPTED)) {
            throw new Exception($this->log(sprintf('%s DB doesn\'t seem to be a valid Magento database', self::TYPE_CORRUPTED)));
        }
        if (!$this->tableExists('core_resource', self::TYPE_REFERENCE)) {
            throw new Exception($this->log(sprintf('%s DB doesn\'t seem to be a valid Magento database', self::TYPE_REFERENCE)));
        }

        $corrupted = $reference = array();

        $sql = "SELECT * FROM `{$this->getTable('core_resource', self::TYPE_CORRUPTED)}`";
        $res = mysqli_query($this->_getConnection(self::TYPE_CORRUPTED), $sql);
        $this->log("Running query: ".$sql);
        while ($row = mysqli_fetch_assoc($res)) {
            $corrupted[$row['code']] = $row['version'];
        }

        $sql = "SELECT * FROM `{$this->getTable('core_resource', self::TYPE_REFERENCE)}`";
        $res = mysqli_query($this->_getConnection(self::TYPE_REFERENCE), $sql);
        $this->log("Running query: ".$sql);
        while ($row = mysqli_fetch_assoc($res)) {
            $reference[$row['code']] = $row['version'];
        }

        $compare = array();
        foreach ($reference as $k => $v) {
            if (!isset($corrupted[$k])) {
                $compare[] = $this->log(sprintf('Module "%s" is not installed in corrupted DB', $k));
            } elseif ($corrupted[$k] != $v) {
                $compare[] = $this->log(sprintf('Module "%s" has wrong version %s in corrupted DB (reference DB contains "%s" ver. %s)', $k, $corrupted[$k], $k, $v));
            }
        }

        return $compare;
    }

    /**
     * Check db support of InnoDb Engine
     *
     * @param string $type
     * @return bool
     */
    public function checkInnodbSupport($type)
    {
        $res = $this->sqlQuery('SELECT VERSION()', $type);
        $version = mysqli_fetch_row($res);
        if (version_compare($version[0], '5.6.0') >= 0) {
            $res = $this->sqlQuery('SHOW ENGINES', $type);
            while ($row = mysqli_fetch_assoc($res)) {
                if ($row['Engine'] == 'InnoDB' && strtoupper($row['Transactions']) == 'YES') {
                    return true;
                }
            }
            return false;
        }

        $sql = $this->_quote("SHOW VARIABLES LIKE ?", 'have_innodb', $this->_getConnection($type));
        $res = $this->sqlQuery($sql, $type);
        $row = mysqli_fetch_assoc($res);
        if ($row && strtoupper($row['Value']) == 'YES') {
            return true;
        }
        return false;
    }

    /**
     * Apply to Database needed settings
     *
     * @param string $type
     * @return Tools_Db_Repair_Mysql4_Mysql4
     */
    public function start($type)
    {
        $this->sqlQuery('/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */', $type);
        $this->sqlQuery('/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */', $type);
        $this->sqlQuery('/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */', $type);
        $this->sqlQuery('/*!40101 SET NAMES utf8 */', $type);
        $this->sqlQuery('/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */', $type);
        $this->sqlQuery('/*!40103 SET TIME_ZONE=\'+00:00\' */', $type);
        $this->sqlQuery('/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */', $type);
        $this->sqlQuery('/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */', $type);
        $this->sqlQuery('/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE=\'NO_AUTO_VALUE_ON_ZERO\' */', $type);
        $this->sqlQuery('/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */', $type);

        return $this;
    }

    /**
     * Return old settings to database (applied in start method)
     *
     * @param string $type
     * @return Tools_Db_Repair_Mysql4
     */
    public function finish($type)
    {
        $this->sqlQuery('/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */', $type);
        $this->sqlQuery('/*!40101 SET SQL_MODE=@OLD_SQL_MODE */', $type);
        $this->sqlQuery('/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */', $type);
        $this->sqlQuery('/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */', $type);
        $this->sqlQuery('/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */', $type);
        $this->sqlQuery('/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */', $type);
        $this->sqlQuery('/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */', $type);
        $this->sqlQuery('/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */', $type);

        return $this;
    }

    /**
     * Begin transaction
     *
     * @param string $type
     * @return Tools_Db_Repair_Mysql4
     */
    public function begin($type)
    {
        $this->sqlQuery('START TRANSACTION', $type);
        return $this;
    }

    /**
     * Commit transaction
     *
     * @param string $type
     * @return Tools_Db_Repair_Mysql4
     */
    public function commit($type)
    {
        $this->sqlQuery('COMMIT', $type);
        return $this;
    }

    /**
     * Rollback transaction
     *
     * @param string $type
     * @return Tools_Db_Repair_Mysql4
     */
    public function rollback($type)
    {
        $this->sqlQuery('ROLLBACK', $type);
        return $this;
    }

    /**
     * Retrieve table properties as array
     * fields, keys, constraints, engine, charset, create
     *
     * @param string $table
     * @param string $type
     * @return array
     */
    public function getTableProperties($table, $type)
    {
        $this->_checkConnection();
        $this->_checkType($type);

        if (!$this->tableExists($table, $type)) {
            return false;
        }

        $tableName = $this->getTable($table, $type);
        $prefix    = $this->_config[$type]['prefix'];
        $tableProp = array(
            'fields'      => array(),
            'keys'        => array(),
            'constraints' => array(),
            'engine'      => 'MYISAM',
            'charset'     => 'utf8',
            'collate'     => null,
            'create_sql'  => null,
            'triggers'    => array()
        );

        // collect fields
        $sql = "SHOW FULL COLUMNS FROM `{$tableName}`";
        $res = mysqli_query($this->_getConnection($type), $sql);
        $this->log("Running query: ".$sql);

        while ($row = mysqli_fetch_row($res)) {
            $tableProp['fields'][$row[0]] = array(
                'type'      => $row[1],
                'is_null'   => strtoupper($row[3]) == 'YES' ? true : false,
                'default'   => $row[5],
                'extra'     => $row[6],
                'collation' => $row[2],
            );
        }

        // create sql
        $sql = "SHOW CREATE TABLE `{$tableName}`";
        $res = mysqli_query($this->_getConnection($type), $sql);
        $row = mysqli_fetch_row($res);
        $this->log("Running query: ".$sql);

        $tableProp['create_sql'] = $row[1];

        // collect keys
        $regExp  = '#(PRIMARY|UNIQUE|FULLTEXT|FOREIGN)?\s+KEY\s+(`[^`]+` )?(\([^\)]+\))#';
        $matches = array();
        preg_match_all($regExp, $tableProp['create_sql'], $matches, PREG_SET_ORDER);
        foreach ($matches as $match) {
            if (isset($match[1]) && $match[1] == 'PRIMARY') {
                $keyName = 'PRIMARY';
            } elseif (isset($match[1]) && $match[1] == 'FOREIGN') {
                continue;
            } else {
                $keyName = substr($match[2], 1, -2);
            }
            $fields = $fieldsMatches = array();
            preg_match_all("#`([^`]+)`#", $match[3], $fieldsMatches, PREG_SET_ORDER);
            foreach ($fieldsMatches as $field) {
                $fields[] = $field[1];
            }

            $tableProp['keys'][strtoupper($keyName)] = array(
                'type'   => !empty($match[1]) ? $match[1] : 'INDEX',
                'name'   => $keyName,
                'fields' => $fields
            );
        }

        // collect CONSTRAINT
        $matches = array();
        preg_match_all($regExp, $tableProp['create_sql'], $matches, PREG_SET_ORDER);

        foreach ($matches as $match) {
            $tableProp['constraints'][strtoupper($match[1])] = array(
                'fk_name'   => strtoupper($match[1]),
                'ref_db'    => isset($match[3]) ? $match[3] : null,
                'pri_table' => $table,
                'pri_field' => $match[2],
                'ref_table' => (isset($match[4])) ? substr($match[4], strlen($prefix)) : '',
                'ref_field' => isset($match[5]) ? $match[5] : '',
                'on_delete' => isset($match[6]) ? $match[7] : '',
                'on_update' => isset($match[8]) ? $match[9] : ''
            );
        }

        // engine
        $regExp = "#(ENGINE|TYPE)="
            . "(MEMORY|HEAP|INNODB|MYISAM|ISAM|BLACKHOLE|BDB|BERKELEYDB|MRG_MYISAM|ARCHIVE|CSV|EXAMPLE)"
            . "#i";
        $match  = array();
        if (preg_match($regExp, $tableProp['create_sql'], $match)) {
            $tableProp['engine'] = strtoupper($match[2]);
        }

        //charset
        $regExp = "#DEFAULT CHARSET=([a-z0-9]+)( COLLATE=([a-z0-9_]+))?#i";
        $match  = array();
        if (preg_match($regExp, $tableProp['create_sql'], $match)) {
            $tableProp['charset'] = strtolower($match[1]);
            if (isset($match[3])) {
                $tableProp['collate'] = $match[3];
            }
        }

        $sql = "SHOW TRIGGERS LIKE '${tableName}'";
        $res = mysqli_query($this->_getConnection($type), $sql);
        $this->log("Running query: ".$sql);

        while ($row = mysqli_fetch_assoc($res)) {
            $triggerName = strtolower($row["Trigger"]);
            $tableProp['triggers'][$triggerName] = $row;
        }

        return $tableProp;
    }

    public function getTables($type)
    {
        $this->_checkConnection();
        $this->_checkType($type);
        $prefix = $this->_config[$type]['prefix'];

        $tables = array();

        $sql = 'SHOW TABLES';
        $res = mysqli_query($this->_getConnection($type), $sql);
        $this->log("Running query: ".$sql);
        while ($row = mysqli_fetch_row($res)) {
            $tableName = substr($row[0], strlen($prefix));
            $tables[$tableName] = $this->getTableProperties($tableName, $type);
        }

        return $tables;
    }

    /**
     * Add constraint
     *
     * @param array $config
     * @param string $type
     * @return Tools_Db_Repair_Mysql4
     * @throws Exception
     */
    public function addConstraint(array $config, $type)
    {
        $this->_checkConnection();
        $this->_checkType($type);

        $required = array('fk_name', 'pri_table', 'pri_field', 'ref_table', 'ref_field', 'on_update', 'on_delete');
        foreach ($required as $field) {
            if (!array_key_exists($field, $config)) {
                throw new Exception(sprintf('Cannot create CONSTRAINT: invalid required config parameter "%s"', $field));
            }
        }

        if ($config['on_delete'] == '' || strtoupper($config['on_delete']) == 'CASCADE'
            || strtoupper($config['on_delete']) == 'RESTRICT') {
            $sql = "DELETE `p`.* FROM `{$this->getTable($config['pri_table'], $type)}` AS `p`"
                . " LEFT JOIN `{$this->getTable($config['ref_table'], $type)}` AS `r`"
                . " ON `p`.`{$config['pri_field']}` = `r`.`{$config['ref_field']}`"
                . " WHERE `p`.`{$config['pri_field']}` IS NOT NULL"
                . " AND `r`.`{$config['ref_field']}` IS NULL";
            $this->sqlQuery($sql, $type);
        } elseif (strtoupper($config['on_delete']) == 'SET NULL') {
            $sql = "UPDATE `{$this->getTable($config['pri_table'], $type)}` AS `p`"
                . " LEFT JOIN `{$this->getTable($config['ref_table'], $type)}` AS `r`"
                . " ON `p`.`{$config['pri_field']}` = `r`.`{$config['ref_field']}`"
                . " SET `p`.`{$config['pri_field']}`=NULL"
                . " WHERE `p`.`{$config['pri_field']}` IS NOT NULL"
                . " AND `r`.`{$config['ref_field']}` IS NULL";
            $this->sqlQuery($sql, $type);
        }

        $sql = "ALTER TABLE `{$this->getTable($config['pri_table'], $type)}`"
            . " ADD CONSTRAINT `{$config['fk_name']}`"
            . " FOREIGN KEY (`{$config['pri_field']}`)"
            . " REFERENCES `{$this->getTable($config['ref_table'], $type)}`"
            . "  (`{$config['ref_field']}`)";
        if (!empty($config['on_delete'])) {
            $sql .= ' ON DELETE ' . strtoupper($config['on_delete']);
        }
        if (!empty($config['on_update'])) {
            $sql .= ' ON UPDATE ' . strtoupper($config['on_update']);
        }

        $this->sqlQuery($sql, $type);

        return $this;
    }

    /**
     * Drop Foreign Key from table
     * @param string $table
     * @param string $foreignKey
     * @param string $type
     * @return $this
     */
    public function dropConstraint($table, $foreignKey, $type)
    {
        $this->_checkConnection();
        $this->_checkType($type);

        $sql = "ALTER TABLE `{$table}` DROP FOREIGN KEY `{$foreignKey}`";
        $this->sqlQuery($sql, $type);

        return $this;
    }

    /**
     * Add column to table
     * @param string $table
     * @param string $column
     * @param array $config
     * @param string $type
     * @param string|false|null $after
     * @return $this
     * @throws Exception
     */
    public function addColumn($table, $column, array $config, $type, $after = null)
    {
        $this->_checkConnection();
        $this->_checkType($type);

        if (!$this->tableExists($table, $type)) {
            return $this;
        }

        $required = array('type', 'is_null', 'default');
        foreach ($required as $field) {
            if (!array_key_exists($field, $config)) {
                throw new Exception($this->log(sprintf('Cannot create COLUMN: invalid required config parameter "%s"', $field)));
            }
        }

        $default = (in_array($config['default'], self::RESERVED_WORDS)) ? $config['default'] : "'{$config['default']}'";
        $sql = "ALTER TABLE `{$this->getTable($table, $type)}` ADD COLUMN `{$column}`"
            . " {$config['type']}"
            . ($config['is_null'] ? "" : " NOT NULL")
            . ($default ? " DEFAULT {$default}" : "")
            . (!empty($config['extra']) ? " {$config['extra']}" : "");
        if ($after === false) {
            $sql .= " FIRST";
        } elseif (!is_null($after)) {
            $sql .= " AFTER `{$after}`";
        }

        $this->sqlQuery($sql, $type);

        return $this;
    }

    /**
     * Add primary|unique|fulltext|index to table
     * @param string $table
     * @param array $config
     * @param string $type
     * @return Tools_Db_Repair_Mysql4
     * @throws Exception
     */
    public function addKey($table, array $config, $type)
    {
        $this->_checkConnection();
        $this->_checkType($type);

        if (!$this->tableExists($table, $type)) {
            return $this;
        }

        $required = array('type', 'name', 'fields');
        foreach ($required as $field) {
            if (!array_key_exists($field, $config)) {
                throw new Exception($this->log(sprintf('Cannot create KEY: invalid required config parameter "%s"', $field)));
            }
        }

        switch (strtolower($config['type'])) {
            case 'primary':
                $condition = "PRIMARY KEY";
                break;
            case 'unique':
                $condition = "UNIQUE `{$config['name']}`";
                break;
            case 'fulltext':
                $condition = "FULLTEXT `{$config['name']}`";
                break;
            default:
                $condition = "INDEX `{$config['name']}`";
                break;
        }
        if (!is_array($config['fields'])) {
            $config['fields'] = array($config['fields']);
        }

        $sql = "ALTER TABLE `{$this->getTable($table, $type)}` ADD {$condition}"
            . " (`" . join("`,`", $config['fields']) . "`)";
        $this->sqlQuery($sql, $type);

        return $this;
    }

    /**
     * Change table storage engine
     *
     * @param string $table
     * @param string $engine
     * @param string $type
     * @return Tools_Db_Repair_Mysql4
     */
    public function changeTableEngine($table, $type, $engine)
    {
        $this->_checkConnection();
        $this->_checkType($type);

        $sql = "ALTER TABLE `{$this->getTable($table, $type)}` ENGINE={$engine}";
        $this->sqlQuery($sql, $type);

        return $this;
    }

    /**
     * Change table storage engine
     *
     * @param string $table
     * @param string $charset
     * @param string $type
     * @return Tools_Db_Repair_Mysql4
     */
    public function changeTableCharset($table, $type, $charset, $collate = null)
    {
        $this->_checkConnection();
        $this->_checkType($type);

        $sql = "ALTER TABLE `{$this->getTable($table, $type)}` DEFAULT CHARACTER SET={$charset}";
        if ($collate) {
            $sql .= " COLLATE {$collate}";
        }
        $this->sqlQuery($sql, $type);

        return $this;
    }

    /**
     * Run SQL query
     * @param string $sql
     * @param string $type
     * @return resource
     * @throws Exception
     */
    public function sqlQuery($sql, $type)
    {
        $this->_checkConnection();
        $this->_checkType($type);
        $this->log("Running query: ".$sql);
        if (!$res = @mysqli_query($this->_getConnection($type), $sql)) {
            throw new Exception($this->log(sprintf(
                "Error #%d: %s on SQL: %s",
                mysqli_sqlstate($this->_getConnection($type)),
                mysqli_sqlstate($this->_getConnection($type)),
                $sql
            )));
        }
        return $res;
    }

    /**
     * Retrieve previous key from array by key
     *
     * @param array $array
     * @param mixed $key
     * @return mixed
     */
    public function arrayPrevKey(array $array, $key)
    {
        $prev = false;
        foreach ($array as $k => $v) {
            if ($k == $key) {
                return $prev;
            }
            $prev = $k;
        }
    }

    /**
     * Retrieve next key from array by key
     *
     * @param array $array
     * @param mixed $key
     * @return mixed
     */
    public function arrayNextKey(array $array, $key)
    {
        $next = false;
        foreach ($array as $k => $v) {
            if ($next === true) {
                return $k;
            }
            if ($k == $key) {
                $next = true;
            }
        }
        return false;
    }

    private function log($message)
    {
        $structure = new Tools_Db_Repair_Structure();
        Mage::log($message, null, $structure->getLogFile());
        return $message;
    }
}
