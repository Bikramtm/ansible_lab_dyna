<?php
require_once 'vznl_abstract.php';

class Vznl_Influx_Sendlogs_Cli extends Vznl_Shell_Abstract
{

    public function __construct()
    {
        $this->_lockExpireInterval = 900; // 5min run so lock at 15min
        parent::__construct();
    }

    /**
     * Run script
     */
    public function run()
    {
        try {
            if (!$this->hasLock()) {
                $this->createLock();
                /** @var Vznl_InfluxLogs_Model_Observer $influxLogs */
                $influxLogs = Mage::getModel('vznl_influxlogs/observer');
                $influxLogs->initAllActiveLogsFetch();
                $this->removeLock();
            }
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            Mage::logException($e);
            Zend_Debug::dump($e->getMessage());
        } catch (Exception $e) {
            Mage::logException($e);
            Zend_Debug::dump($e->getMessage());
        }
    }

    /**
     * Retrieve Usage Help Message
     */
    public function usageHelp()
    {
        $file = basename(__FILE__);
        return <<<USAGE
Usage:  php -f $file -- [options]

  help                            This help

USAGE;
    }
}

$stats = new Vznl_Influx_Sendlogs_Cli();
$stats->run();
