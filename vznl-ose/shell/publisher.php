<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Shell
 * @copyright  Copyright (c) 2006-2016 X.commerce, Inc. and affiliates (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

require_once 'abstract.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class Dyna_AMQP_Publisher extends Mage_Shell_Abstract
{
    const HOST = "w12vfdbiz101-t.dynafix.nl";
    const PORT = "8100";
    const USER = "dyna.oil";
    const PASS = "Dyn@0i1";
    const VHOST = "/";
    /**
     * Run script
     *
     */
    public function run()
    {
        $msg = $this->getArg("message");
        if($msg) {
            $exchange = 'dyna.ose.direct';
            $queue = 'dyna.ose.setstatus';
            $connection = new AMQPStreamConnection(self::HOST, self::PORT, self::USER, self::PASS, self::VHOST);
            $channel = $connection->channel();
            /*
                The following code is the same both in the consumer and the producer.
                In this way we are sure we always have a queue to consume from and an
                    exchange where to publish messages.
            */
            $messageBody = $msg;
            $message = new AMQPMessage($messageBody, array('content_type' => 'text/plain', 'delivery_mode' => AMQPMessage::DELIVERY_MODE_NON_PERSISTENT));
            $channel->basic_publish($message, $exchange);
            $channel->close();
            $connection->close();
        } else {
            echo $this->usageHelp();
        }
    }

    /**
     * Retrieve Usage Help Message
     *
     */
    public function usageHelp()
    {
        return <<<USAGE
Usage:  php -f publisher.php  --message [message]
USAGE;
    }
}

$shell = new Dyna_AMQP_Publisher();
$shell->run();
