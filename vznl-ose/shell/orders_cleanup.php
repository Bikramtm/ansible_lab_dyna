<?php

/**
 * Shell script to delete the old orders permanently based on the number of days that have passed since the order creation
 * (configurable admin setting)
 */

require_once 'vznl_abstract.php';

ini_set('memory_limit', '2048M');
ini_set('max_execution_time', 1200);

/**
 * Magento Log Shell Script
 *
 * @category    Dyna
 * @author      Dyna
 */
class Vznl_Orders_Cleaner extends Vznl_Shell_Abstract
{
    private $cliDebug = true;
    private $logFileDebug = true;

    private function log($message)
    {
        if(!$this->logFileDebug) {
            return;
        }
        Mage::log($message, null, "orders-cleanup-".date("Y-m-d.")."log");
    }

    /**
     * Echo log message to console
     * @param $msg
     */
    private function _writeLine($msg)
    {
        echo $msg . PHP_EOL;
    }

    /**
     * Run script
     *
     */
    public function run()
    {
        if ($noOfDaysForCleanup = Mage::getStoreConfig('omnius_general/cleanup_settings/no_of_days_order_alive')) {

            $date = date('Y-m-d H:i:s', strtotime('-'.$noOfDaysForCleanup.' day', strtotime(now())));

            /* @var Omnius_Superorder_Model_Superorder $lastestSuperOrderForDeletion */
            $lastestSuperOrderForDeletion = Mage::getModel('superorder/superorder')->getCollection()
                ->addFieldToFilter('created_at', ['lteq' => $date])
                ->setOrder('entity_id', 'DESC')
                ->setPageSize(1, 1)
                ->getFirstItem();

            /* @var Omnius_Superorder_Model_Mysql4_Superorder_Collection $superOrders */
            $superOrders = Mage::getModel('superorder/superorder')->getCollection()
                ->addFieldToFilter('entity_id', ['lteq' => $lastestSuperOrderForDeletion->getId()])
                ->setPageSize(100)
                ->setCurPage(1);

            if(!$superOrders->getSize()) {
                $msg = "No super orders were found older than ". $date;
                //write cli log
                if ($this->cliDebug) {
                    $this->_writeLine($msg);
                }
                $this->log($msg);
                return;
            }

            $msg = "Starting deleting orders older than ".$noOfDaysForCleanup." days, meaning older than: " . $date;
            if ($this->cliDebug) {
                $this->_writeLine($msg);
            }
            $this->log($msg);

            $deletedSuperOrdersIds = [];
            foreach($superOrders as $order)
            {
                /** @var Vznl_Superorder_Model_Superorder $order */
                $id = $order->getId();

                try{
                    $order->delete();
                    $msg = "super order #".$id." is removed";
                    if ($this->cliDebug) {
                        $this->_writeLine($msg);
                    }
                    $this->log($msg);
                    $this->clearHistorySuperorderRecords($id);
                } catch(Exception $e){
                    $msg = "super order #".$id." could not be removed: ".$e->getMessage();
                    if ($this->cliDebug) {
                        $this->_writeLine($msg);
                    }
                    $this->log($msg);
                    $id = null;
                }
                if($id) {
                    $deletedSuperOrdersIds[] = $id;
                }
            }

            $orders = Mage::getModel('sales/order')->getCollection()
                ->addFieldToFilter('superorder_id', array('in' => $deletedSuperOrdersIds));

            foreach($orders as $order)
            {
                /** @var Vznl_Checkout_Model_Sales_Order $order */
                $orderId = $order->getId();
                $orderPackaes = $order->getPackages();

                try{
                    $order->delete();
                    $msg = "sales order #".$orderId." is removed";
                    if ($this->cliDebug) {
                        $this->_writeLine($msg);
                    }
                    $this->log($msg);
                    $this->clearHistoryOrderRecords($orderId);
                    $this->clearHistoryPackageRecords($orderPackaes);
                }catch(Exception $e){
                    $msg = "sales order #".$orderId." could not be removed: ".$e->getMessage();
                    if ($this->cliDebug) {
                        $this->_writeLine($msg);
                    }
                    $this->log($msg);
                }
            }

            $this->log($msg);
            $msg = "Finished orders deletion... ";
            if ($this->cliDebug) {
                $this->_writeLine($msg);
            }
            $this->log($msg);
        }
        else {
            $msg = "The number of days should be set in Admin. Only the orders older than that number of days will be deleted";
            if ($this->cliDebug) {
                $this->_writeLine($msg);
            }
            $this->log($msg);
            echo $this->usageHelp();
        }
    }

    /**
     * Clear history superorder entries
     * @param int $superorderId
     * @return void
     * @throws Exception
     */
    private function clearHistorySuperorderRecords(
        int $superorderId
    ):void {
        $superordersList = Mage::getModel('superorder/statusHistory')
            ->getCollection()
            ->addFieldToFilter('superorder_id', $superorderId);

        foreach ($superordersList as $item) {
            /* @var Omnius_Superorder_Model_StatusHistory $item */
            $item->delete();
        }
        $msg = "super order history entry #".$superorderId." is removed";
        if ($this->cliDebug) {
            $this->_writeLine($msg);
        }
        $this->log($msg);
    }

    /**
     * Clear history order entries
     * @param int $superorderId
     * @return void
     * @throws Exception
     */
    private function clearHistoryOrderRecords(
        int $orderId
    ):void {
        $ordersList = Mage::getModel('superorder/statusHistory')
            ->getCollection()
            ->addFieldToFilter('order_id', $orderId);

        foreach ($ordersList as $item) {
            /* @var Omnius_Superorder_Model_StatusHistory $item */
            $item->delete();
        }
        $msg = "sales order history entry #".$orderId." is removed";
        if ($this->cliDebug) {
            $this->_writeLine($msg);
        }
        $this->log($msg);
    }

    /**
     * Clear history package entries
     * @param int $superorderId
     * @return void
     * @throws Exception
     */
    private function clearHistoryPackageRecords(
        array $packages
    ):void {
        $packagesIds = [];
        foreach ($packages as $package) {
            $packagesIds[] = $package->getId();
        }
        if (!empty($packagesIds)){
            $packageLists = Mage::getModel('superorder/statusHistory')->getCollection()
                ->addFieldToFilter('package_id', ['in' => $packagesIds]);

            foreach ($packageLists as $item) {
                /* @var Omnius_Superorder_Model_StatusHistory $item */
                $item->delete();
            }
            $msg = "packages history data with the following ids: ".implode(', ', $packagesIds)." have been removed";
            if ($this->cliDebug) {
                $this->_writeLine($msg);
            }
            $this->log($msg);
        }
    }

    /**
     * Retrieve Usage Help Message
     *
     */
    public function usageHelp()
    {
        return <<<USAGE
Usage:  php orders_cleanup.php 
  help              This help

USAGE;
    }
}

$shell = new Vznl_Orders_Cleaner();
$shell->run();
