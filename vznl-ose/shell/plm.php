<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

require_once 'dyna_abstract.php';

/**
 * Class Dyna_PLM_CLI
 */
class Dyna_PLM_CLI extends Dyna_Shell_Abstract
{
    protected $files_total = 0;
    /** @var Vznl_Sandbox_Helper_Data $sandboxHelper */
    protected $sandboxHelper;

    public function run()
    {
        parent::run();

        $stacks = strtolower($this->getArg('importStack'));
        $this->_args['importFile'] = trim($this->getArg('importFile'));
        if (empty($stacks) && !isset($this->supportedFilenames[$this->_args['importFile']])) {
            die($this->usageHelp());
        }
        if (isset($this->supportedFilenames[$this->_args['importFile']])) {
            $stacks = $this->supportedFilenames[$this->_args['importFile']]['stack'];
        }

        $this->_args['importStack'] = explode(',', $stacks);
        $this->_args['importStack'] = array_map("ucfirst", $this->_args['importStack']);
        $this->_args['importStack'] = array_map("trim", $this->_args['importStack']);
        $this->sandboxHelper = Mage::helper("vznl_sandbox");
        foreach ($this->_args['importStack'] as $stack) {
            $this->files_total += count($this->sandboxHelper->_fileToStack[$this->sandboxHelper->_labelToKeyword[$stack]]);
        }

        $this->logfilePrefix = 'PLM_' . implode('_', $this->_args['importStack']);
        $this->logfileSuffix = '_' . $this->datetime . '.log';

        $messages = $this->sandboxHelper->checkRunningImports();
        if (count($messages) == 1) {
            /** @var Dyna_Sandbox_Model_ImportInformation $executionModel */
            $executionModel = Mage::getModel('dyna_sandbox/importInformation');
            $executionModel->setType(json_encode($this->_args['importStack']));
            $executionModel->setErrors(0);
            $executionModel->setStartedAt(date("Y-m-d H:i:s", Mage::getModel('core/date')->timestamp()));
            $executionModel->setStatus($executionModel::STATUS_RUNNING);
            $executionModel->save();

            if ($executionModel->getId()) {
                if (empty($this->_args['importFile'])) {
                    $this->importStack($executionModel);
                } else {
                    $this->importFile($executionModel);
                }
            }
        } elseif (count($messages) > 1) {
            $interruptScript = false;
            $i = 1;
            foreach ($messages as $message) {
                if ($i == count($messages)) {
                    $interruptScript = true;
                }
                $this->handleOutputWithInterruptOrNot($message, 'STDERR', $interruptScript);
                $i++;
            }
        }
    }

    /**
     * Imports a stack or multiple stacks
     * @var Dyna_Sandbox_Model_ImportInformation $executionModel */
    protected function importStack($executionModel) {
        $this->handleOutputWithInterruptOrNot('Import started for stacks: ' . implode(',', $this->_args['importStack']) . '. This may take a while.', 'STDOUT');
        $this->sandboxHelper->setLogfilePrefix($this->logfilePrefix);
        $this->sandboxHelper->setLogfileSuffix($this->logfileSuffix);
        $this->sandboxHelper->performImport($executionModel);

        $this->finishExecution($executionModel);
    }

    /**
     * Imports a single File
     * @var Dyna_Sandbox_Model_ImportInformation $executionModel */
    protected function importFile($executionModel) {
        $this->files_total = 1; //We are importing a single file
        $script = $this->supportedFilenames[$this->_args['importFile']]['script'] ?? 'import.php';
        $logFile = Mage::getBaseDir('base') . DS . 'var/log/' . $this->logfilePrefix . '_' . $this->_args['importFile'] . $this->logfileSuffix;
        $logFileError = Mage::getBaseDir('base') . DS . 'var/log/' . $this->logfilePrefix . '_' . $this->_args['importFile'] . '-error' . $this->logfileSuffix;
        $command = 'nohup ' . $this->getPHPPath() . ' ' . $script
            . ' --stack ' . current($this->_args['importStack'])
            . ' --type ' . $this->supportedFilenames[$this->_args['importFile']]['import_type']
            . ' --file ' . $this->_args['importFile']
            . ' --executionId ' . $executionModel->getId()
            . ' --fullImport false'
            . " >" . $logFile . " 2>" . $logFileError . " &";
        @exec($command);

        $this->finishExecution($executionModel);
    }

    /**
     * Checks status of import Execution and sets proper status
     * @var Dyna_Sandbox_Model_ImportInformation $executionModel
     */
    protected function finishExecution($executionModel) {
        $executionId = $executionModel->getId();
        $retries = 1;
        while ($retries <= 3) {
            $importCollection = Mage::getModel('import/summary')
                ->getCollection()
                ->addFieldToFilter('execution_id', $executionId);
            $files_skipped = 0;
            $files_imported = 0;
            foreach ($importCollection as $item) {
                if ($item->getData('headers_validated') == 0 || $item->getData('has_errors') == 1) {
                    $files_skipped++;
                } else {
                    $files_imported++;
                }
            }
            $pids = $this->sandboxHelper->getRunningImportPIDs($executionId);
            sleep(5);
            if (($pids['exitCode'] != 0 || count($pids['output']) == 0)) {
                $retries++;
            }
            if ($pids['exitCode'] == 0 && count($pids['output'] > 0)) {
                $retries = 1;
            }
        }
        if ($this->files_total && $this->files_total != $files_imported && $this->sandboxHelper->getStatusForImportExecution($executionId) !== $executionModel::STATUS_KILLED) {
                $this->sandboxHelper->setStatusForImportExecution($executionId, $executionModel::STATUS_ERROR);
        } elseif ($this->files_total && $this->files_total == $files_imported) {
            $this->sandboxHelper->setStatusForImportExecution($executionId, $executionModel::STATUS_SUCCESS);
        }
    }

    /**
     * Retrieve Usage Help Message
     */
    public function usageHelp()
    {
        $supportedFilenames = implode(',', array_keys($this->supportedFilenames));
        return <<<USAGE
Usage:  php plm.php -- [options]
  --importStack          Import this/these stack(s) (comma separated values like mobile,cable,cable_artifact,fixed,bundles,campaigns,reference,salesid) (used separately from --importFile)
  
  --importFile           Import this file (filename must exist in <env>/var/import) (used separately from --importStack)
                         <filename> should be one of $supportedFilenames
                         
  --help          This help

USAGE;
    }
}

$plm = new Dyna_PLM_CLI();
$plm->run();