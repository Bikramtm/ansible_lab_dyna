<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

require_once 'dyna_abstract.php';

/**
 * Class Dyna_Import_Cli
 */
class Dyna_Empty_Stack extends Dyna_Shell_Abstract
{
    protected $emptyStackMessages = [
    ];

    public function run()
    {
        parent::run();
        $this->time::start('total');
        $this->messages = array_replace($this->messages, $this->emptyStackMessages);

        if (!$this->setStack())
        {
            $this->handleOutputWithInterruptOrNot(
                $this->getMessage('noStack'),
                'STDERR',
                true
            );
        }
        $this->logfilePrefix = 'stack_import_' . $this->stack;

        if (!$this->setChannel())
        {
            $this->handleOutputWithInterruptOrNot(
                $this->getMessage('noChannelUseDefault'),
                'STDOUT',
                false
            );
        }

        switch ($this->stack)
        {
            case 'Fixed':
            case 'Mobile':
            case 'Cable':
                $this->emptyProductsStack();
                break;
            case 'Campaign':
                $this->emptyCampaignsStack();
                break;
            case 'SalesId':
                $this->emptySalesIdStack();
                break;
            case 'Bundles':
                $this->emptyBundlesStack();
                break;
            case 'Reference':
                $this->emptyReferenceStack();
                break;
            case 'Provis':
                $this->emptyProvis();
                break;
            default:
                break;
        }

        if ($this->getArg('no-reindex') !== 'true') {
            if ($this->stack !== 'Campaign') {
                $this->reindexAll();
            }
        }

        $this->handleOutputWithInterruptOrNot(
            $this->getMessage('timeTaken', [
                'what' => sprintf('emptying stack %s', $this->stack),
                'timeElapsed' => $this->time::elapsed('total')
            ]),
            'STDOUT',
            false
        );
    }

    protected function emptyProductsStack()
    {
        $this->deletePackageTypes();
        $this->deleteProducts();
        $this->deleteProductFamilies();
        $this->deleteProductVersions();
        $this->deleteCategories();
        $this->deleteProductMatchRules();
        $this->deleteMixmatches();
        $this->deleteSalesRules();
        $this->deleteMultimappers();
        $this->deleteCatalogVersion();
    }

    protected function emptyCampaignsStack()
    {
        $this->deleteCampaignOffers();
        $this->deleteCampaigns();
        $this->deleteCatalogVersion();
    }

    protected function emptySalesIdStack()
    {
        $this->deleteSalesIdDealercodes();
    }

    protected function emptyBundlesStack()
    {
        $this->deleteCategories();
        $this->deleteBundles();
        $this->deleteCatalogVersion();
    }

    protected function emptyReferenceStack()
    {
        $this->deleteServiceProviders();
        $this->deleteShip2Store();
        $this->deleteActivityCodes();
        $this->deleteTypeSubtypeCombinations();
        $this->deletePhoneBookKeywords();
        $this->deletePhoneBookIndustries();
    }

    protected function emptyProvis()
    {
        $this->deleteProvis();
    }

    /**
     * What is imported together is deleted together
     */
    protected function deleteFlatEntities($entities, $stack = null) {
        if ($stack !== false) {
            $stack = $this->stack;
            $likeStack = "%$stack%";
        }

        foreach ($entities as $name => $entity) {
            $this->time::start($entity);
            $table = Mage::getModel($entity)->getResource()->getMainTable();

            $select = sprintf("SELECT COUNT(*) FROM %s", $table);
            if ($stack !== false && strtolower($stack) !== 'campaign') {
                $select .= sprintf(" WHERE `stack` LIKE '%s'", $likeStack);
            }

            if ($entity == 'salesrule/rule') {
                $select .= " AND `sr_unique_id` is NULL";
            }

            if ($entity == 'dyna_package/packageSubtypeCardinality') {
                $select .= " OR `stack` is NULL";
            }

            $totalDbEntries = $noDbEntries = (int)$this->getConnection()->fetchOne($select);

            if ($noDbEntries > 0) {
                do {
                    $query = sprintf('SET FOREIGN_KEY_CHECKS = 0; DELETE FROM `%s`', $table);
                    if ($stack !== false && strtolower($stack) !== 'campaign') {
                        $query .= sprintf(" WHERE `stack` LIKE '%s'", $likeStack);
                        $query .= sprintf(" OR `stack` is null");
                    }
                    $query .= sprintf(" LIMIT %s", self::BATCH_SIZE);
                    $delete = $this->getConnection()->query($query);
                    $noDbEntries -= self::BATCH_SIZE;
                    $delete->closeCursor();
                } while ($noDbEntries >= 0);

                if (!empty($name)) {
                    $this->handleOutputWithInterruptOrNot(
                        $this->getMessage('deleted', [
                            'what' => $name,
                            'count' => $totalDbEntries,
                            'timeElapsed' => $this->time::elapsed($entity)
                        ]),
                        'STDOUT',
                        false
                    );
                }
            } else {
                if (!empty($name)) {
                    $this->handleOutputWithInterruptOrNot(
                        $this->getMessage('alreadyEmpty', [
                            'what' => ucfirst($name)
                        ]),
                        'STDOUT',
                        false
                    );
                }
            }
        }
    }

    protected function deleteEAVEntities($entities) {
        foreach ($entities as $name => $entity) {
            $this->time::start($entity);
            /** @var Dyna_Catalog_Model_Resource_Product_Collection $collection */
            $collection = Mage::getModel($entity)->getCollection()->addAttributeToFilter(array(
                array('attribute'=> 'stack','null' => true),
                array('attribute'=> 'stack','eq' => $this->stack)
            ));

            if ($entity == 'catalog_category' && $this->stack != 'Bundles') {
                $collection = $this->excludeCategoriesWithProducts($collection);
            }

            //remove the unused Root Categories
            if ($entity == 'catalog/category') {
                //Delete entries in catalog_category_product with same categoryId.
                $coreConnection = Mage::getSingleton('core/resource');
                $connection = $coreConnection->getConnection('core_write');
                $tableName = 'catalog_category_product';
                foreach ($collection as $coll) {
                    $connection->delete($tableName,
                        ['category_id = ?' => $coll->getId()]);
                }

                $collectionDefault = Mage::getModel($entity)->getCollection()->addAttributeToFilter('level', 1);
                $stores = Mage::getModel('core/store_group')->getCollection();
                $rootCategories = [];

                foreach ($stores as $store) {
                    if (!in_array($store->getData('root_category_id'), $rootCategories)) {
                        $rootCategories[] = $store->getData('root_category_id');
                    }
                }

                foreach ($collectionDefault as $item) {
                    if (!in_array($item->getId(), $rootCategories)) {
                        $item->load($item->getId())->delete();
                    }
                }
            }

            $noProductsForStack = $collection->count();
            $collection = $collection->setPageSize(self::BATCH_SIZE);

            if ($noProductsForStack > 0) {
                $noTotalPages = $collection->getLastPageNumber();
                $noCurrentPage = 1;

                do {
                    $collection->setCurPage($noCurrentPage);
                    $collection->load()->delete();
                    $noCurrentPage++;
                    $collection->clear();
                } while ($noCurrentPage <= $noTotalPages);

                $this->handleOutputWithInterruptOrNot(
                    $this->getMessage('deleted', [
                        'what' => $name,
                        'count' => $noProductsForStack,
                        'timeElapsed' => $this->time::elapsed($entity)
                    ]),
                    'STDOUT',
                    false
                );
            } else {
                if (!empty($name)) {
                    $this->handleOutputWithInterruptOrNot(
                        $this->getMessage('alreadyEmpty', [
                            'what' => ucfirst($name)
                        ]),
                        'STDOUT',
                        false
                    );
                }
            }
        }
    }

    // Don't delete categories which still have products, except for Bundles stack
    protected function excludeCategoriesWithProducts($collection) {
        $this->time::start('category_reindex');
        try {
            exec($this->getPHPPath() . ' ' . $this->getShellDir() . '/indexer.php --reindex catalog_category_product', $out, $exit);
            if ($exit != 0) {
                Mage::throwException('catalog_category_product_index failed, deleteCategories will not work properly.');
            }
        } catch (Exception $e) {
            $this->handleOutputWithInterruptOrNot(
                $e->getMessage(),
                'STDERR',
                true
            );
        }
        $collection->getSelect()->joinLeft(
            ['i' => 'catalog_category_product_index'],
            'main_table.entity_id IS NULL',
            []
        )->group('main_table.entity_id');

        $this->handleOutputWithInterruptOrNot(
            $this->getMessage('timeTaken', [
                'what' => 'category_reindex',
                'timeElapsed' => $this->time::elapsed('category_reindex')
            ]),
            'STDOUT',
            false
        );
        return $collection;
    }

    protected function reindexAll() {
        $this->time::start('reindex_all');

        $this->handleOutputWithInterruptOrNot(
            $this->getMessage('started', [
                'what' => 'reindex_all'
            ]),
            'STDOUT',
            false
        );

        try {
            exec( $this->getPHPPath() . ' ' . $this->getShellDir() . '/indexer.php --reindexall', $out, $exit);
            if ($exit != 0) {
                Mage::throwException('Reindex all failed, Reindexing failed after emptying ' . $this->stack . ' stack. FE may not be accurate.');
            }
        } catch (Exception $e) {
            $this->handleOutputWithInterruptOrNot(
                $e->getMessage(),
                'STDERR',
                true
            );
        }

        $this->handleOutputWithInterruptOrNot(
            $this->getMessage('timeTaken', [
                'what' => 'reindex_all',
                'timeElapsed' => $this->time::elapsed('reindex_all')
            ]),
            'STDOUT',
            false
        );
    }

    /**
     * Deletes Products
     */
    protected function deleteProducts()
    {
        $entities = [
            'product(s)' => 'catalog/product',
        ];
        $this->deleteEAVEntities($entities);
    }

    /**
     * Deletes Categories
     */
    protected function deleteCategories()
    {
        $entities = [
            'category/categories' => 'catalog/category',
        ];
        $this->deleteEAVEntities($entities);
    }

    /**
     * Deletes Package Types and Subtypes
     */
    protected function deletePackageTypes()
    {
        $entities = [
            'dyna_package/packageSubtypeCardinality',
        ];
        $this->deleteFlatEntities($entities);
    }

    /**
     * Deletes Product Families
     */
    protected function deleteProductFamilies()
    {
        $entities = [
            'Product Family(es)' => 'dyna_catalog/productFamily',
        ];
        $this->deleteFlatEntities($entities);
    }

    /**
     * Deletes Product Versions
     */
    protected function deleteProductVersions()
    {
        $entities = [
            'Product Version(s)' => 'dyna_catalog/productVersion',
        ];
        $this->deleteFlatEntities($entities);
    }

    /**
     * Deletes Product Match Rules
     */
    protected function deleteProductMatchRules()
    {
        $entities = [
            'Product Match Rule(s)' => 'productmatchrule/rule',
        ];
        $this->deleteFlatEntities($entities);
    }

    /**
     * Deletes Mixmatches
     */
    protected function deleteMixmatches()
    {
        $entities = [
            'Mixmatch(es)' => 'omnius_mixmatch/price',
        ];
        $this->deleteFlatEntities($entities);
    }

    /**
     * Deletes Sales Rules
     */
    protected function deleteSalesRules()
    {
        $entities = [
            'Sales Rule(s)' => 'salesrule/rule',
        ];
        $this->deleteFlatEntities($entities);
    }

    /**
     * Deletes Multimappers
     */
    protected function deleteMultimappers()
    {
        $entities = [
            'Multimapper(s)' => 'multimapper/mapper',
            'Multimapper Addon(s)' => 'dyna_multimapper/addon',
        ];
        $this->deleteFlatEntities($entities);
    }

    /**
     * Deletes Campaign Offers
     */
    protected function deleteCampaignOffers()
    {
        $entities = [
            'Campaign Offer(s)' => 'dyna_bundles/campaignOffer',
        ];
        $this->deleteFlatEntities($entities, false);
    }

    /**
     * Deletes Campaigns
     */
    protected function deleteCampaigns()
    {
        $entities = [
            'Campaign(s)' => 'dyna_bundles/campaign',
        ];
        $this->deleteFlatEntities($entities, false);
    }

    /**
     * Deletes SalesId Dealercodes
     */
    protected function deleteSalesIdDealercodes()
    {
        $entities = [
            'SalesId Dealercode(s)' => 'dyna_bundles/campaignDealercode',
        ];
        $this->deleteFlatEntities($entities, false);
    }

    /**
     * Deletes Catalog Version
     */
    protected function deleteCatalogVersion()
    {
        $entities = [
            'Catalog Version' => 'cataloglog/cataloglog',
        ];
        $this->deleteFlatEntities($entities);
    }

    /**
     * Deletes Bundles
     */
    protected function deleteBundles()
    {
        $entities = [
            'Bundle Rule(s)' => 'dyna_bundles/bundleRule',
        ];
        $this->deleteFlatEntities($entities, false);
    }

    /**
     * Deletes Service Providers
     */
    protected function deleteServiceProviders()
    {
        $entities = [
            'Service Provider(s)' => 'dyna_operator/serviceProvider',
        ];
        $this->deleteFlatEntities($entities, false);
    }

    /**
     * Deletes Ship2Store List
     */
    protected function deleteShip2Store()
    {
        $entities = [
            'Ship2Stores List' => 'agentde/vodafoneShip2Stores',
        ];
        $this->deleteFlatEntities($entities, false);
    }

    /**
     * Deletes Activity Codes
     */
    protected function deleteActivityCodes()
    {
        $entities = [
            'Activity Codes' => 'dyna_customer/activityCodes',
        ];
        $this->deleteFlatEntities($entities, false);
    }

    /**
     * Deletes Type-Subtype Combinations
     */
    protected function deleteTypeSubtypeCombinations()
    {
        $entities = [
            'Type-Subtype Combinations' => 'dyna_customer/typeSubtypeCombinations',
        ];
        $this->deleteFlatEntities($entities, false);
    }

    /**
     * Deletes Phonebook Keywords
     */
    protected function deletePhoneBookKeywords()
    {
        $entities = [
            'Phonebook Keywords' => 'agentde/phoneBookKeywords',
        ];
        $this->deleteFlatEntities($entities, false);
    }

    /**
     * Deletes Phonebook Industries
     */
    protected function deletePhoneBookIndustries()
    {
        $entities = [
            'Phonebook Industries' => 'agentde/phoneBookIndustries',
        ];
        $this->deleteFlatEntities($entities, false);
    }

    /**
     * Deletes Provis SalesIds
     */
    protected function deleteProvis()
    {
        $entities = [
            'Provis SalesIds' => 'agentde/provisSales',
        ];
        $this->deleteFlatEntities($entities, false);
    }


    /**
     * Retrieve Usage Help Message
     */
    public function usageHelp()
    {
        $supportedStacks = implode('/', $this->supportedStacks);
        return <<<USAGE
Usage:  php file -- [options]

  help							This help
  stack							Stacks [$supportedStacks]

USAGE;
    }
}

$operation = new Dyna_Empty_Stack();
$operation->run();
