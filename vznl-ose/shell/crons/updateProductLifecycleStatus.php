<?php
/**
 * Php script for updating products' lifecycle status based on their effective and expiration dates
 * Copyright (c) 2017. Dynacommerce B.V.
 */

require_once '../abstract.php';

class Dyna_Cron_ProductLifecycleStatus_Cli extends Mage_Shell_Abstract
{
    public function run()
    {
        echo "[" . date("Y-m-d H:i:s") .  "] Started updateProductLifeCycleStatus based on products' effective and expiration dates\n";
        /** @var Dyna_Catalog_Model_Cron $updater */
        $updater = Mage::getModel('dyna_catalog/cron');
        $updater->updateProductLifecycleStatus();
        echo "[" . date("Y-m-d H:i:s") .  "] Executed successfully\n";
    }
}

$convert = new Dyna_Cron_ProductLifecycleStatus_Cli();
$convert->run();
