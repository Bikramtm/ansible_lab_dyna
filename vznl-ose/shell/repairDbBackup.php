<?php

require_once 'repairDbClass.php';
require_once 'repairDbStructure.php';
require_once 'repairDbAutoincrement.php';
require_once 'repairDbInconsistent.php';
require_once 'repairDbOrders.php';
require_once 'repairDbQuotes.php';
require_once 'repairDbStores.php';

class Tools_Db_Perform_Backup extends Tools_Db_Repair
{

    public function __construct()
    {
        parent::__construct();
        $this->logFile = "repairdb_perform_backup_".date('Y_m_d').".log";
    }

    public function run()
    {
        $this->log('Start script.');

        $repairDb = new Tools_Db_Repair_Structure();
        $repairRes = $repairDb->run();

        $autoincrement = new Tools_Db_Repair_Autoincrement();
        $autoincrementRes = $autoincrement->run();

        $inconsistent = new Tools_Db_Repair_Inconsistent();
        $inconsistentRes = $inconsistent->run();

        //$orders = new Tools_Db_Repair_Orders();
        //$ordersRes = $orders->run();

        //$quotes = new Tools_Db_Repair_Quotes();
        //$quotesRes = $quotes->run();

        $stores = new Tools_Db_Repair_Stores();
        $storesRes = $stores->run();

        $response = array_merge($repairRes, $autoincrementRes, $inconsistentRes, $ordersRes, $quotesRes, $storesRes);

        if (!empty($response)) {
            $this->log("Found some probles with database; no backup started.");
            $this->log($response);
        } else {
            $this->log("Starting full backup of ".$this->dbConfig->dbname);

            $backup_file = "fullbackup_".date("Y-m-d-H-i-s")."{$this->dbConfig->host}_{{$this->dbConfig->dbname}}.gz";
            $command     = "mysqldump -u{$this->dbConfig->username} -p{$this->dbConfig->password} {$this->dbConfig->dbname} | gzip > $backup_file";

            system($command, $returnvalue);
            $this->log("Full backup created of ".$this->dbConfig->dbname);
        }


        $this->log('Stop script.');
    }
}
