<?php
require_once 'abstract.php';

/**
 * Class Dyna_Replenish_Stocks
 */
class Dyna_Replenish_Stocks extends Mage_Shell_Abstract
{
    protected static $time;
    protected $_connection;
    protected $_prev = array();
    protected $_inserts = array();
    protected $_flushOnMemory = 256000000; //256MB
    protected $_maxPacketsLength = 5000000; //5MB

    public static function log($message=null)
    {
        if (!self::$time) {
            self::$time = microtime(true);
        }

        $units = array('B', 'KB', 'MB', 'GB', 'TB');
        $bytes = max(memory_get_peak_usage(true), 0);
        $pow = $bytes ? floor(log($bytes, 1024)) : 0;
        $pow = min($pow, count($units) - 1);
        $bytes /= pow(1024, $pow);
        $result = number_format(round($bytes, 2), 2) . ' ' . $units[$pow];
        unset($units, $bytes, $pow);

        $text = sprintf(
            '[%s][%ss][%s]%s%s',
            now(),
            number_format(microtime(true) - self::$time, 3),
            $result,
            ($message ? (PHP_SAPI==='cli' ? sprintf("\e[33;1m %s\e[0m", $message) : $message) : ''),
            PHP_SAPI==='cli' ? "\r\n" : '<br/>'
        );

        if (PHP_SAPI==='cli') {
            echo sprintf("\033[42m%s\033[49m", $text);
        } else {
            echo $text;
        }
    }

    public function __construct()
    {
        parent::__construct();

        $this->_connection = Mage::getSingleton('core/resource')->getConnection('core_write');

        /** convert memory limit to bytes */
        $value = trim(trim(ini_get('memory_limit')));
        $unit = strtolower(substr($value, -1, 1));
        $value = (int) $value;
        switch($unit) {
            case 'g':
                $value *= 1024;
            // no break (cumulative multiplier)
            case 'm':
                $value *= 1024;
            // no break (cumulative multiplier)
            case 'k':
                $value *= 1024;
        }
        $this->_flushOnMemory = $value - (0.4 * $value); //40% less than limit
        unset($value);
        unset($unit);

        $data = $this->_connection->fetchAll('SHOW VARIABLES;');
        foreach ($data as &$var) {
            if ($var['Variable_name'] == 'max_allowed_packet') {
                $limit = (int) $var['Value'];
                $this->_maxPacketsLength = $limit - (0.20 * $limit); //20% less then limit
                break;
            }
        }
        unset($data);
        unset($limit);
    }

    /**
     * @return bool
     */
    protected function isMemoryExceeded()
    {
        return memory_get_usage(true) >= $this->_flushOnMemory;
    }

    /**
     * Check is we approach the PHP memory
     * limit. If the current used memory exceeds
     * the current limit, all gathered data untill
     * this moment will be flushed to the database
     */
    protected function _assertMemory()
    {
        if ($this->isMemoryExceeded()) {
            self::log('MEMORY EXCEEDED');
            $this->_insertMultiple();
        }
    }

    /**
     * Builds INSERT statements and executes them
     * Iterates over the items withing the $_prev array
     * and builds the INSERT statements, always checking
     * if we approach the MySQL max_allowed_packet limit.
     * If we approach the limit too much, we execute the current
     * SQL statement and start building the statements for the remaining items
     */
    protected function _insertMultiple()
    {
        self::log('FLUSHING');
        $values = '';
        foreach ($this->_inserts as $_key => $insert) {

            $values .= sprintf('(%s,%s,%s,%s,%s),', $insert[0], $insert[1], 1000, 0, time());

            if (strlen($values) >= $this->_maxPacketsLength) {
                self::log('MAX PACKETS');
                $sql = sprintf(
                    'INSERT INTO `dyna_store_stock` (product_id,store_id,qty,backorder_qty,last_modified) VALUES %s ON DUPLICATE KEY UPDATE product_id=VALUES(product_id);' . PHP_EOL,
                    trim($values, ',')
                );
                $this->_connection->query($sql);
                unset($sql);
                $values = '';
            }
            unset($this->_inserts[$_key]);
        }

        if ($values) {
            $sql = sprintf(
                'INSERT INTO `dyna_store_stock` (product_id,store_id,qty,backorder_qty,last_modified) VALUES %s ON DUPLICATE KEY UPDATE product_id=VALUES(product_id);' . PHP_EOL,
                trim($values, ',')
            );
            unset($values);
            $this->_connection->query($sql);
            unset($sql);
        }
    }


    /**
     * Run script
     *
     */
    public function run()
    {
        self::log('START');
        $products = array();
        $storeIds = array_keys(Mage::app()->getStores());
        $appEmulation = Mage::getSingleton('core/app_emulation');
        $dealerStoreCodes = $this->_connection->fetchCol('SELECT DISTINCT `axi_store_code` from `dealer` WHERE `axi_store_code` IS NOT NULL');
        $subTypeAttr = Mage::getSingleton("eav/config")->getAttribute('catalog_product', Dyna_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR);

        foreach ($storeIds as $storeId) {
            $products[$storeId] = isset($products[$storeId]) ? $products[$storeId] : array();

            $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($storeId);

            $subTypeValues = array_filter(array_map(function($item) {
                if (isset($item['value']) && in_array($item['label'], array('Device', 'Simcard', 'Accessoire'))) {
                    return $item['value'];
                }
                return false;
            }, $subTypeAttr->getSource()->getAllOptions(true, true)));

            /** @var Dyna_Catalog_Model_Resource_Product_Collection $collection */
            $collection = Mage::getResourceModel('catalog/product_collection');
            $collection
                ->addAttributeToFilter('is_deleted', array('neq' => 1))
                ->addAttributeToFilter('status', array('gt' => 0))
                ->addAttributeToFilter('type_id', Mage_Catalog_Model_Product_Type::TYPE_SIMPLE)
                ->addAttributeToFilter(Dyna_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR, array("in" => $subTypeValues))
                ->addStoreFilter();

            foreach ($collection->getAllIds() as $productId) {
                foreach ($dealerStoreCodes as $storeCode) {
                    $this->_inserts[] = array($productId, $storeCode);
                    $this->_assertMemory();
                    rand(0, 1000) == 200 ? self::log('RECORDS: ' . count($this->_inserts)) : null;
                }
            }

            unset($collection);

            $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
        }
        $this->_insertMultiple();

        self::log('END');
    }

    /**
     * Retrieve Usage Help Message
     */
    public function usageHelp()
    {
        $file = basename(__FILE__);
        return <<<USAGE
Usage:  php -f $file -- [options]

  help							This help

USAGE;
    }
}

$test = new Dyna_Replenish_Stocks();
$test->run();
