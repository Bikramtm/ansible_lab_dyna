<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

require_once 'abstract.php';
require_once 'stopwatch.php';

/**
 * Class Dyna_Export_Cli
 */
class Dyna_Export_Cli extends Mage_Shell_Abstract
{
    // enable/disable cli debug
    protected $cliDebug = true;
    protected $cliMessages =[
        'invalidType' => 'Invalid type, please specify [ %s ].',
        'invalidDate' => 'Invalid date for %s please specify the date like YYYY-MM-DD.',
        'invalidInterval' => 'Start date is bigger than End date, please change one of them.'
    ];
    protected $types =[
        'summary',
        'executionImport'
    ];

    protected $_intervalStart = null;
    protected $_intervalEnd = null;
    protected $_exportFile ='';

    protected $_importCategoryTypes = [
        'bundle',
        'campaign',
        'mobile',
        'fixedNet',
        'cable'
    ];

    protected $_csvDelimiter = ';';
    /**
     * Run appropriate export script based on the --type param
     *
     */
    public function run()
    {
        /**
         * Get arguments from cli
         */
        $type = $this->getArg('type');
        if (!$type || !in_array($type, $this->types)) {
            $this->_writeLine(printf($this->cliMessages['invalidType'],  implode('/', $this->types)));
            return;
        }
        $this->_intervalStart = $this->getArg('startDate');
        if (empty($this->_intervalStart)) {
            $this->_intervalStart = date('Y-m-d H:m:s', strtotime('-12 hours')); //default start date is 12 hours ago, aligned for automated imports
        } elseif (strlen($this->_intervalStart) == 10) {
            $this->_intervalStart .= ' 00:00:00';
        }
        $this->_intervalEnd = $this->getArg('endDate');
        if (strlen($this->_intervalEnd) == 10) {
            $this->_intervalEnd .= ' 23:59:59';
        }

        //check start date
        if($this->_intervalStart && !$this->validateDate($this->_intervalStart)){
            $this->_writeLine(printf($this->cliMessages['invalidDate'], 'startDate'));
            return false;
        }
        //check end date
        if($this->_intervalEnd && !$this->validateDate($this->_intervalEnd)){
            $this->_writeLine(printf($this->cliMessages['invalidDate'], 'endDate'));
            return false;
        }
        //check if dates are set and if together form a valid interval
        if($this->_intervalStart && $this->_intervalEnd && $this->_intervalStart > $this->_intervalEnd){
            $this->_writeLine($this->cliMessages['invalidInterval']);
            return false;
        }
        /** Start count time */
        StopWatch::start();

        //call the function
        switch ($type){
            case 'summary':
                $this->_getSummaryReport();
                break;
            case 'executionImport':
                $this->_updateExecutionImportStatus();
                break;
        }

        //write time
        $this->_writeLine(sprintf('Time taken for import %s', StopWatch::elapsed()));
    }

    private function _getSummaryReport()
    {
        //output data to put into export file
        $outputData = '';
        /** @var Dyna_Import_Model_Summary $model */
        $summaryModel = Mage::getModel("import/summary");
        //filename for export
        $this->_exportFile = 'Summary_Report_' . date('Y-m-d') . "_" . time();
        //export headers
        $headers = [
            'category',
            'file',
            'description'
        ];

        //summary collection
        $collection = $summaryModel
            ->getCollection();
        if ($this->_intervalStart) {
            $collection->addFieldToFilter('created_at', array('gteq' => $this->_intervalStart));
        }
        if ($this->_intervalEnd) {
            $collection->addFieldToFilter('created_at', array('lteq' => $this->_intervalEnd));
        }
        $collection->setOrder('category','ASC');
        $totalFilesToImport = count($collection);
        //if no data
        if($totalFilesToImport == 0){
            $this->exportFile('Imports were not made during the specified period');
            return;
        }

        //add headers to the csv file
        $outputData .= implode($this->_csvDelimiter,$headers) . PHP_EOL;

        $totalRowsToImport = 0;
        $totalRowsImported = 0;
        $totalRowsSkipped = 0;
        $totalFilesImported = 0;
        $filesWithSkippedRows = [];
        $filesSkippedEntirely = [];

        //parse collection
        foreach ($collection as $model) {
            //get data of row
            $modelData = $model->getData();
            //check if file has skipped rows | this will be displayed at the end of the export file
            if($modelData['skipped_rows'] != 0){
                $filesWithSkippedRows[] = $modelData;
            }
            //add data to output
            $outputData .= $modelData['category'] . $this->_csvDelimiter . $modelData['filename'] . $this->_csvDelimiter;
            if($modelData['has_errors'] != 1 && $modelData['has_errors'] == 0){
                $outputData .= 'Header Summary Imported:';
                if($modelData['headers_validated'] == 1){
                    $outputData .= 'Successful,';
                    $outputData .= 'Import contains in total ' . $modelData['total_rows'].' Rows, ';
                    $outputData .= 'Successfully imported ' . $modelData['imported_rows'].' rows, ';
                    $outputData .= 'Skipped rows with error ' . $modelData['skipped_rows'];
                    //update data for total output
                    $totalRowsToImport += $modelData['total_rows'];
                    $totalRowsImported += $modelData['imported_rows'];
                    $totalRowsSkipped  += $modelData['skipped_rows'];
                    $totalFilesImported++;
                }else{
                    $outputData .= 'Unsuccessful, Skipped import file';
                }
            }else{
                $filesSkippedEntirely[] = $modelData;
                $outputData .= 'File skipped with errors.';
            }


            $outputData .= PHP_EOL;
        }
        //total output
        $outputData .= PHP_EOL. '---------------------------' . PHP_EOL;
        $outputData .= 'Total: '.PHP_EOL;
        $outputData .= 'Total No of Files for import: ' . $totalFilesToImport . PHP_EOL;
        $outputData .= 'Total No of Files imported: ' . $totalFilesImported . PHP_EOL;
        $outputData .= count($filesSkippedEntirely) > 0 ? ('Total No of Files skipped in import: ' . count($filesSkippedEntirely) . PHP_EOL) : '';
        $outputData .= 'Total No of Rows for import: ' . $totalRowsToImport . PHP_EOL;
        $outputData .= 'Total No of Rows imported: ' . $totalRowsImported . PHP_EOL;
        $outputData .= 'Total No of Rows skipped: ' . $totalRowsSkipped . PHP_EOL;

        //output files that have skipped rows
        foreach ($filesWithSkippedRows as $errorFile){
            // Error Log File Name1: X, Total Errors: Y
            $outputData .= 'Error Log File ' .$errorFile['filename'].', Total Errors: '.$errorFile['skipped_rows'] . PHP_EOL;
        }

        //output files that were skipped
        if (count($filesSkippedEntirely) > 0) {
            $outputData .= 'Error Log Files Skipped: ';
        }
        $count = 0;
        foreach ($filesSkippedEntirely as $errorFile){
            // Error Log Files Skipped: File 1, File 2 etc.
            $count++;
            $outputData .= $errorFile['filename'] . ($count < count($filesSkippedEntirely) ? ', ' : '');
        }
        if (count($filesSkippedEntirely) > 0) {
            $outputData .= PHP_EOL;
        }
        $this->exportFile($outputData);
    }

    /**
     * Update an execution import 
     */
    function _updateExecutionImportStatus()
    {
        $executionId = $this->getArg('executionId');
        /** @var Dyna_Sandbox_Model_ImportInformation $executionModel */
        $executionModel =  Mage::getModel('dyna_sandbox/importInformation');
        $executionModel = $executionModel->load((int)$executionId);
        $types = json_decode($executionModel->getType());
        $types = array_map("ucfirst", $types);
        $total_files = 0;
        foreach ($types as $type) {
            $total_files += count(Mage::helper('dyna_sandbox')->_fileToStack[$type]);
        }
        $importCollection = Mage::getModel('import/summary')
            ->getCollection()
            ->addFieldToFilter('execution_id', $executionId);
        $skipped_files = 0;
        foreach ($importCollection as $item) {
            if ($item->getData('headers_validated') == 0) {
                $skipped_files += 1;
            }
        }
        $imported_files = $importCollection->getSize();
        if ($total_files && $imported_files == $total_files) {
            $status = $executionModel::STATUS_SUCCESS;
        } else {
            $status = $executionModel::STATUS_ERROR;
        }
        $executionModel->setStatus($status)
            ->setFinishedAt(date("Y-m-d H:i:s", Mage::getModel('core/date')->timestamp()))
            ->save();
        Mage::unregister('sandbox_import_no_redis_logging');
    }

    /**
     * Create export file
     * @param string $outputData
     * @param string $extension
     */
    function exportFile($outputData = '', $extension = '.csv')
    {
        //path to file in export dir
        $pathToFile = $this->getExportDir(). DS . ($this->_exportFile ?? time()) . $extension;
        //flag for successfully export
        $exportedSummaryReport = false;
        try{
            //save export file
            $exportedSummaryReport =  file_put_contents($pathToFile, $outputData, LOCK_EX|(is_file($pathToFile) ? FILE_APPEND  : 0));
        } catch (Exception $e) {
            $this->_writeLine("ERR: error writing ', ". $e->getMessage());
        }

        $this->_writeLine('Export of "Summary Import of logs" file was: ' . ($exportedSummaryReport ? 'Successful (find the exported file in var/export/)' : 'Unsuccessful' ));
    }

    /**
     * Validate if string match the format date
     * @param $string
     * @return bool
     */
    function validateDate($string)
    {
        $date = DateTime::createFromFormat('Y-m-d H:i:s', $string);
        return $date && $date->format('Y-m-d H:i:s') === $string;
    }

    /**
     * Echo log message to console
     * @param $msg
     */
    private function _writeLine($msg)
    {
        echo $msg . PHP_EOL;
    }

    /**
     * Get path to export dir
     * @return string
     */
    private function getExportDir()
    {
        $exportDir = Mage::getBaseDir('export');
        if (!is_dir($exportDir) || !is_writable($exportDir)) {
            print_r('Writings problem for dir "export"');
            exit;
        }
        return $exportDir;
    }
    /**
     * Retrieve Usage Help Message
     */
    public function usageHelp()
    {
        $availableTypes = implode('/', $this->types);
        return <<<USAGE
Usage:  php file -- [options]

  help							This help
  type							Export type [$availableTypes]
  startDate                     Specify the start date of the export interval data (YYYY-mm-dd)
  endDate                       Specify the end date of the export interval data (YYYY-mm-dd)
  website						Websites to import

USAGE;
    }
}

// Example
//php import.php --type mobileProduct --file mobileHw.csv

$import = new Dyna_Export_Cli();
$import->run();
