<?php

/**
 * Its actually the same as scripts/post_activate.php, only this one works on the ST
 */

require_once 'abstract.php';
require_once 'dyna_abstract.php';

class Dyna_VFDE_Post_Activate extends Mage_Shell_Abstract
{
    private $structureDirs = [
        '/shell',
        '/media',
        '/var/stubs',
        '/var/import',
        '/var/export',
        '/app/code/community/Omnius/Proxy',
        '/app/code/community/Omnius/Proxy/blocks',
        '/app/code/community/Omnius/Proxy/controllers',
        '/shell/dbextract',
        '/shell/dbextract/results'
    ];

    function run()
    {
        try {
            $errorMessage = "";

            // Create default folder structure if not exist
            foreach ($this->structureDirs as $dir) {
                $directory = Mage::getBaseDir() . "$dir/";

                if (!file_exists($directory)) {
                    echo 'Create directory ' . $directory . PHP_EOL;
                    $old_umask = umask(0);
                    if (!mkdir($directory, 0777, true)) {
                        $errorMessage .= "Failed to create structured folder '$directory', please create this folder manually with 0777 permissions\n";
                    }
                    umask($old_umask);
                } else {
                    echo 'Apply rights to ' . $directory . PHP_EOL;
                    $output = array();
                    $return_var = -1;
                    exec("chmod 777 -R $directory", $output, $return_var);

                    if ($return_var != 0) {
                        $errorMessage .= "Failed to set permissions for existing structured folder '$directory', please set 0777 permissions for this folder manually\n";
                    }
                }
            }

            if(strlen($errorMessage) > 0){
                throw new Exception($errorMessage);
            }

            echo 'Done applying folders and rights' . PHP_EOL;

        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
}

$postActivate = new Dyna_VFDE_Post_Activate();
$postActivate->run();