<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

require_once 'abstract.php';
require_once 'stopwatch.php';

/**
 * Class Dyna_Shell_Abstract
 */
abstract class Dyna_Shell_Abstract extends Mage_Shell_Abstract
{
    const BATCH_SIZE = 500;
    protected $logfilePrefix;
    protected $logfileSuffix;
    /** @var  StopWatch $time */
    protected $time;
    protected $_flushOnMemory = 128000000; //256MB
    protected $_maxPacketsLength = 102400; //5MB
    /** @var  Varien_Db_Adapter_Interface $connection */
    protected $connection;
    protected $stack = null;
    protected $channel = "Telesales";
    protected $datetime;
    protected $date;
    // $supportedStacks is the list of stacks supported by shell/emptyStack.php
    protected $supportedStacks = [
        'Fixed',
        'Mobile',
        'Cable',
        'Campaign',
        'Bundle',
        'Salesid',
        'Reference',
        'Provis',
    ];
    // $ignoredStackValues contains any stack values used that are not supported by shell/emptyStack.php
    protected $ignoredStackValues = [
        'Cable_Rules_Plugin',
        'OptionTypes',
        'CommunicationVariables',
    ];
    // @todo $supportedFilenames will be replaced by dynamically loaded data from xml
    protected $supportedFilenames = [
        'FN_PackageTypes.csv' => [
            'stack' => 'Fixed',
            'import_type' => 'packageTypes',
        ],
        'FN_PackageCreation.csv' => [
            'stack' => 'Fixed',
            'import_type' => 'packageCreationTypes',
        ],
        'FN_Hardware.csv' => [
            'stack' => 'Fixed',
            'import_type' => 'fixedProduct',
        ],
        'FN_SalesPackage.csv' => [
            'stack' => 'Fixed',
            'import_type' => 'fixedProduct',
        ],
        'FN_Options.csv' => [
            'stack' => 'Fixed',
            'import_type' => 'fixedProduct',
        ],
        'FN_Promotions.csv' => [
            'stack' => 'Fixed',
            'import_type' => 'fixedProduct',
        ],
        'FN_Accessory.csv' => [
            'stack' => 'Fixed',
            'import_type' => 'fixedProduct',
        ],
        'FN_CategoryTree.csv' => [
            'stack' => 'Fixed',
            'import_type' => 'categoriesTree',
        ],
        'FN_Categories.csv' => [
            'stack' => 'Fixed',
            'import_type' => 'categoriesTreeProducts',
        ],
//       'FN_Comp_Rules.csv'
        'FN_Comp_Rules.xml' => [
            'stack' => 'Fixed',
            'import_type' => 'productMatchRules',
            'script' => 'import_xml.php',
        ],
        'FN_SalesRules.csv' => [
            'stack' => 'Fixed',
            'import_type' => 'fixedSalesRules',
        ],
//       'FN_Multimapper_Hardware.csv'
//       'FN_Multimapper_SalesPackage.csv'
//       'FN_Multimapper_Options.csv'
//       'FN_Multimapper_Promotion.csv'
//       'FN_Multimapper_Accessory.csv'
        'FN_Mapper.xml' => [
            'stack' => 'Fixed',
            'import_type' => 'multimapper',
            'script' => 'import_xml.php',
        ],
        'FN_ProductFamily.csv' => [
            'stack' => 'Fixed',
            'import_type' => 'productFamily',
        ],
        'FN_ProductVersion.csv' => [
            'stack' => 'Fixed',
            'import_type' => 'productVersion',
        ],
        'FN_Version.xml' => [
            'stack' => 'Fixed',
            'import_type' => 'logfile',
        ],
        'Mobile_Package_Types.csv' => [
            'stack' => 'Mobile',
            'import_type' => 'packageTypes',
        ],
        'Mobile_PackageCreation.csv' => [
            'stack' => 'Mobile',
            'import_type' => 'packageCreationTypes',
        ],
        'Mobile_Tariff.csv' => [
            'stack' => 'Mobile',
            'import_type' => 'mobileProduct',
        ],
        'Mobile_Hardware.csv' => [
            'stack' => 'Mobile',
            'import_type' => 'mobileProduct',
        ],
        'Mobile_Footnote.csv' => [
            'stack' => 'Mobile',
            'import_type' => 'mobileProduct',
        ],
        'Mobile_Service.csv' => [
            'stack' => 'Mobile',
            'import_type' => 'mobileProduct',
        ],
        'Mobile_Categories.csv' => [
            'stack' => 'Mobile',
            'import_type' => 'categories',
        ],
        'Mobile_Device_Categories.csv' => [
            'stack' => 'Mobile',
            'import_type' => 'categories',
        ],
        'Mobile_Comp_Rules.csv' => [
            'stack' => 'Mobile',
            'import_type' => 'productMatchRules',
        ],
        'Mobile_Comp_Rules_Article_Constraints.csv' => [
            'stack' => 'Mobile',
            'import_type' => 'productMatchRules',
        ],
        'Mobile_Comp_Rules_Footnote.csv' => [
            'stack' => 'Mobile',
            'import_type' => 'productMatchRules',
        ],
        'Mobile_Mixmatch_Complete.csv' => [
            'stack' => 'Mobile',
            'import_type' => 'mixmatch',
        ],
        'Mobile_Sales_Rules_PostPaid.csv' => [
            'stack' => 'Mobile',
            'import_type' => 'mobileSalesRules',
        ],
        'Mobile_Sales_Rules_Campaigns.csv' => [
            'stack' => 'Mobile',
            'import_type' => 'mobileSalesRules',
        ],
        'Mobile_Multimapper_Kias.csv' => [
            'stack' => 'Mobile',
            'import_type' => 'multimapper',
        ],
        'Mobile_Multimapper_SAP.csv' => [
            'stack' => 'Mobile',
            'import_type' => 'multimapper',
        ],
        'Mobile_ProductFamily.csv' => [
            'stack' => 'Mobile',
            'import_type' => 'productFamily',
        ],
        'Mobile_ProductVersion.csv' => [
            'stack' => 'Mobile',
            'import_type' => 'productVersion',
        ],
        'Mobile_Version.xml' => [
            'stack' => 'Mobile',
            'import_type' => 'logfile',
        ],
        'Cable_Package_Types.csv' => [
            'stack' => 'Cable',
            'import_type' => 'packageTypes',
        ],
        'Cable_PackageCreation.csv' => [
            'stack' => 'Cable',
            'import_type' => 'packageCreationTypes',
        ],
        'Cable_Products.json' => [
            'stack' => 'Cable',
            'import_type' => 'cableProduct',
        ],
        'Cable_Categories.csv' => [
            'stack' => 'Cable',
            'import_type' => 'categories',
        ],
        'Cable_Compatibility.csv' => [
            'stack' => 'Cable',
            'import_type' => 'productMatchRules',
        ],
        'Cable_Sales_Rules.csv' => [
            'stack' => 'Cable',
            'import_type' => 'salesRules',
        ],
        'Cable_Multimapper.csv' => [
            'stack' => 'Cable',
            'import_type' => 'multimapper',
        ],
        'Cable_Version.xml' => [
            'stack' => 'Cable',
            'import_type' => 'logfile',
        ],
        'Cable_Rules_Plugin.phar' => [
            'stack' => 'Cable_Rules_Plugin',
            'import_type' => 'cableArtifact',
        ],
        'Campaign_Offers.xml' => [
            'stack' => 'Campaign',
            'import_type' => 'campaignOffer',
        ],
        'Campaigns.xml' => [
            'stack' => 'Campaign',
            'import_type' => 'campaign',
        ],
        'Campaign_Version.xml' => [
            'stack' => 'Campaign',
            'import_type' => 'logfile',
        ],
        'Bundles.xml' => [
            'stack' => 'Bundle',
            'import_type' => 'bundles',
        ],
        'Bundle_Categories.csv' => [
            'stack' => 'Bundle',
            'import_type' => 'categories',
        ],
        'Bundle_Version.xml' => [
            'stack' => 'Bundle',
            'import_type' => 'logfile',
        ],
        'Reference_OSF_ServiceProvider.csv' => [
            'stack' => 'Reference',
            'import_type' => 'serviceProviders',
        ],
        'Reference_Ship2Store_List.csv' => [
            'stack' => 'Reference',
            'import_type' => 'vodafoneShip2Stores',
        ],
        'Reference_activityCodes.xml' => [
            'stack' => 'Reference',
            'import_type' => 'activityCodes',
            'script' => 'import_xml.php',
        ],
        'Reference_typeSubtypeCombinations.xml' => [
            'stack' => 'Reference',
            'import_type' => 'typeSubtypeCombinations',
            'script' => 'import_xml.php',
        ],
        'Reference_Stichwort_list.csv' => [
            'stack' => 'Reference',
            'import_type' => 'phoneBookKeywords',
        ],
        'Reference_telefonbuch-branchenliste.csv' => [
            'stack' => 'Reference',
            'import_type' => 'phoneBookIndustries',
        ],
        'Reference_option_type.csv' => [
            'stack' => 'Reference',
            'import_type' => 'optionTypes',
        ],
        'SalesId_Dealercode_Mapping.xml' => [
            'stack' => 'Salesid',
            'import_type' => 'salesIdDealercode',
        ],
        'SalesId_Version.xml' => [
            'stack' => 'Salesid',
            'import_type' => 'logfile',
        ],
        'CommunicationVariables.csv' => [
            'stack' => 'CommunicationVariables',
            'import_type' => 'communicationVariables',
        ]
    ];
    protected $messages = [
        'noStack' => 'Invalid stack "%s" provided. Please use one of %s.',
        'noChannelUseDefault' => 'No channel specified, using "Telesales"',
        'timeTaken' => 'Time taken for %s: %s',
        'stackTimeTaken' => '[%s] Time taken for %s: %s',
        'deleted' => '[%s] Deleted %s %s in %s',
        'alreadyEmpty' => '[%s] %s is already empty',
        'started' => '[%s] Started %s',
    ];

    public function run()
    {
        $this->time = new StopWatch();
        $this->_showHelp();
        $runDate = $this->getArg('date');
        if (!empty($runDate)) {
            $this->date = $runDate;
            if (strlen($runDate) == 10) {
                $this->datetime = $this->date . '-' . date('H-i-s');
            }
        } else {
            $this->date = date('Y-m-d');
            $this->datetime = date('Y-m-d-H-i-s');
        }
        $this->logfilePrefix = $this->getArg('stack') ?: '';
        $this->logfileSuffix = '-' . $this->date . '.log';
    }

    /**
     * Setup memory limits for execution
     */
    protected function initMemoryLimits()
    {
        /** convert memory limit to bytes */
        $value = trim(trim(ini_get('memory_limit')));
        if ($value == -1) {
            $value = $this->_getMachineMaxMemory();
        } else {
            $unit = strtolower(substr($value, -1, 1));
            $value = (int)$value;
            switch ($unit) {
                case 'g':
                    $value *= 1024;
                // no break (cumulative multiplier)
                case 'm':
                    $value *= 1024;
                // no break (cumulative multiplier)
                case 'k':
                    $value *= 1024;
                default:
            }
        }
        //60% less than limit
        $newFlushLimit = (int)($value - (0.6 * $value));
        $this->_flushOnMemory = $newFlushLimit > $this->_flushOnMemory ? $this->_flushOnMemory : $newFlushLimit;
        unset($value);
        unset($unit);

        $data = $this->getConnection()->fetchAll('SHOW VARIABLES;');
        foreach ($data as &$var) {
            if ($var['Variable_name'] == 'max_allowed_packet') {
                $limit = (int)$var['Value'];
                //40% less then limit
                $newPackLength = (int)($limit - (0.20 * $limit));
                $this->_maxPacketsLength = $newPackLength > $this->_maxPacketsLength ? $this->_maxPacketsLength : $newPackLength;
                break;
            }
        }
        unset($var);
        unset($data);
        unset($limit);
    }

    /**
     * Limit to 1GB if cannot be accessed
     * @return float
     */
    protected function _getMachineMaxMemory()
    {
        if (!is_readable("/proc/meminfo")) {
            return 1024 * 1024 * 1024;
        }

        $data = explode(PHP_EOL, file_get_contents("/proc/meminfo"));
        $memInfo = [];
        foreach ($data as $line) {
            $values = explode(":", $line);
            if (count($values) == 2) {
                list($key, $val) = $values;
                $memInfo[$key] = trim($val);
            } else {
                $memInfo[$line] = null;
            }
        }
        if (empty($memInfo['MemTotal'])) {
            $memInfo['MemTotal'] = 0;
        }
        $maxMemory = 1024 * ((int) trim(trim($memInfo['MemTotal'], 'kb')));

        return max($maxMemory * 0.2, 1024 * 1024 * 1024);
    }

    /**
     * @return Varien_Db_Adapter_Interface
     */
    protected function getConnection()
    {
        if ($this->connection == null) {
            /** @var Mage_Core_Model_Resource $adapter */
            $adapter = Mage::getSingleton('core/resource');
            $this->connection = $adapter->getConnection('core_write');
        }
        return $this->connection;
    }

    protected function setStack() {
        $stack = $this->getArg('stack');
        if ($stack === false || in_array($stack, $this->ignoredStackValues)) {
            return true;
        }
        if (!isset($this->stack) && !empty($stack) && in_array($stack, $this->supportedStacks)) {
            $this->stack = $stack;
        }
        return $this->stack;
    }

    protected function setChannel() {
        $channel = $this->getArg('channel');
        if (!isset($this->channel) && !empty($channel)) {
            $this->channel = $channel;
        }
        return $this->channel;
    }

    protected function getMessage($key, $args = []) {
        switch ($key) {
            case 'noStack':
                $msg = sprintf($this->messages[$key], $this->getArg('stack'), implode(', ', $this->supportedStacks));
                break;
            case 'timeTaken':
                $msg = sprintf($this->messages[$key], $args['what'], $args['timeElapsed']);
                break;
            case 'stackTimeTaken':
                $msg = sprintf($this->messages[$key], $this->stack, $args['what'], $args['timeElapsed']);
                break;
            case 'deleted':
                $args['what'] = $args['count'] === 1 ? $args['what'] : $args['what'] . 's';
                $msg = sprintf($this->messages[$key], $this->stack, $args['count'], $args['what'], $args['timeElapsed']);
                break;
            case 'started':
            case 'alreadyEmpty':
                $msg = sprintf($this->messages[$key], $this->stack, $args['what']);
                break;
            default:
                $msg = $this->messages[$key];
                break;
        }

        return $msg;
    }

    /**
     * Output on screen messages and log to file
     * @todo Nice to have: Post to Slack
     * @param $msg
     * @param string $output
     * @param bool $interruptScript
     */
    protected function handleOutputWithInterruptOrNot($msg, $output = '', $interruptScript = false) {
        switch ($output) {
            case 'STDERR':
                fwrite(STDERR, $msg . PHP_EOL);
                $logfile = $this->logfilePrefix . '-error' . $this->logfileSuffix;
                Mage::log($msg, Zend_Log::ERR, $logfile);
                break;
            case 'STDOUT':
                fwrite(STDOUT, $msg . PHP_EOL);
                $logfile = $this->logfilePrefix . $this->logfileSuffix;
                Mage::log($msg, Zend_Log::DEBUG, $logfile);
                break;
            default:
                break;
        }

        if ($interruptScript) {
            exit(1);
        }
    }

    /**
     * Get environment path to PHP cli binary
     */
    protected function getPHPPath() {
        $phpPath = '/usr/bin/php';
        if ($importXMLfile = file_get_contents(Mage::getBaseDir('etc').DS.'import.xml')) {
            $import_xml = new Omnius_Core_Model_SimpleDOM($importXMLfile);
            $config = new Varien_Simplexml_Config;
            $config->loadString($import_xml->asXML());
            if ($phpPath = $config->getXpath('php/path')) {
                $phpPath = (string) $config->getXpath('php/path')[0];
            }
        }

        return $phpPath;
    }

    protected function getShellDir() {
        return Mage::getBaseDir().DS.'shell';
    }
}
