<?php
require_once 'app' . DIRECTORY_SEPARATOR . 'Mage.php';

use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;

/**
 * Producable errors
 *
 * 1: Invalid session match
 * 2: Invalid url proxy given
 * 3: Invalid global url
 * 4: Invalid request method provided
 * 5: No agent logged in
 */
class ProxyException extends Exception
{
    public function __construct($message = "", $code = 0, int $httpCode = 200, Throwable $previous = null)
    {
        $this->httpCode = $httpCode;
        parent::__construct($message, $code, $previous);
    }
}

class Proxy
{
    const VALID_METHODS = [
        'PUT',
        'DELETE',
        'POST',
        'GET',
        'PATCH',
        'HEAD',
        'OPTIONS',
        'TRACE',
        'CONNECT'
    ];

    const INDIRECT_SEARCH_URL = '/api/v1/customer/search/indirect';

    const CREATE_NEW_PARTY = '/api/v1/customer/search/request/create';

    /**
     * @var Vznl_Core_Helper_Data
     */
    private $coreHelper;

    /**
     * @var string
     */
    private $executionTimeId;

    /**
     * @var string
     */
    private $requestTimeId;

    /**
     * Constructor
     * @throws Exception
     */
    public function __construct()
    {
        define('CM_REDISSESSION_LOCKING_ENABLED', false);
        Mage::app($_SERVER['MAGE_RUN_CODE']);
        Mage::getSingleton("core/session", array("name" => "frontend"));
        $this->coreHelper = Mage::helper('vznl_core');
        $this->executionTimeId = $this->coreHelper->initTimeMeasurement();
        $this->validateSession();
        $this->validateActiveUser();
        $this->validateOrderAccessPermission();
    }

    /**
     * @return string
     * @throws Exception
     */
    public function call():string
    {
        $request_headers = $this->getRequestHeaders();
        $request_params = $this->getRequestParams();
        $request_url = $this->getCallableUrl();

        $ch = curl_init($request_url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_PROXY, '');
        curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, false);
        // add data for POST, PUT or DELETE requests
        if ('POST' == $this->getRequestMethod()) {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $request_params);
        } elseif ('PUT' == $this->getRequestMethod() || 'DELETE' == $this->getRequestMethod()) {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $this->getRequestMethod());
            curl_setopt($ch, CURLOPT_POSTFIELDS, $this->getRequestMethod());
        }

        $this->requestTimeId = $this->coreHelper->initTimeMeasurement();

        // Retrieve response (headers and content)
        $response = curl_exec($ch);
        $errorCode = curl_errno($ch);
        $errorMessage = curl_error($ch);
        curl_close($ch);

        if ($errorCode !== 0) {
            // Save logs
            $this->logRequestsDetails(
                $request_headers,
                $request_url,
                $request_params,
                null,
                null
            );
            // Throw exception
            throw new ProxyException($errorMessage, $errorCode, 500);
        } else {
            // Split response to header and content
            list($response_headers, $response_content) = preg_split('/(\r\n){2}/', $response, 2);
            // Save logs
            $this->logRequestsDetails(
                $request_headers,
                $request_url,
                $request_params,
                $response_headers,
                $response_content
            );
            // (re-)send the headers
            $response_headers = preg_split('/(\r\n){1}/', $response_headers);
            foreach ($response_headers as $response_header) {
                // Rewrite the `Location` header, so clients will also use the proxy for redirects.
                if (preg_match('/^Location:/', $response_header)) {
                    list(, $value) = preg_split('/: /', $response_header, 2);
                    $response_header = 'Location: ' . $_SERVER['REQUEST_URI'] . '?csurl=' . $value;
                }
                if (!preg_match('/^(Transfer-Encoding):/', $response_header)) {
                    header($response_header, false);
                }
            }

            if ($_GET['url'] == self::CREATE_NEW_PARTY) {
                $this->passwordCache($response_content);
            }

        }
        return $response_content;
    }

    /**
     * Validate if an active user is logged in
     * @throws Exception
     * @return void
     */
    private function validateActiveUser():void
    {
        $session = Mage::getSingleton('customer/session');
        $agent = $session->getAgent();
        if (!$agent) {
            throw new ProxyException('No agent logged in', 5, 403);
        }
        if ($session->getData('store_code') != Mage::app()->getWebsite()->getCode()){
            throw new ProxyException($session->getData('store_code').'Missing agent permissions for store view', 5, 403);
        }

    }

    /**
     * Validate Customer Search with Order for Indirect Agent
     * @return void
     * @throws Exception
     */
    private function validateOrderAccessPermission():void
    {
        $session = Mage::getSingleton('customer/session');
        if (Mage::app()->getWebsite()->getCode() == Vznl_Agent_Model_Website::WEBSITE_INDIRECT_CODE && $_GET['url'] == self::INDIRECT_SEARCH_URL) {

            preg_match('/{"customerAccountInfo.orderNumbers":.*?\}/', $this->getRequestParams(), $match);
            if (isset($match[0])) {
                $postData = json_decode($match[0],true);
                if (!$session->getAgent()->isCurrentDealerOrder($postData['customerAccountInfo.orderNumbers']))
                    throw new ProxyException('<b>OOOPS!</b> It`s not possible to search for this order ID, because it was placed by another dealer', 6, 403);
            }
        }
    }

    /**
     * @return string
     * @throws Exception
     */
    private function getRequestMethod():string
    {
        if (!in_array($_SERVER['REQUEST_METHOD'], self::VALID_METHODS)) {
            throw new ProxyException('Invalid request method provided', 4, 400);
        }
        return $_SERVER['REQUEST_METHOD'];
    }

    /**
     * @return bool
     * @throws Exception
     */
    private function validateSession():bool
    {
        $sessionId = Mage::getSingleton("core/session")->getEncryptedSessionId();
        $cookieId = $_COOKIE['frontend'];
        if ($sessionId !== $cookieId) {
            throw new ProxyException('Invalid active session given', 1, 403);
        }
        return true;
    }

    /**
     * Get the domain from where the proxy is called
     * @return string
     */
    private function getProxyDomain():string
    {
        return 'http' . ($_SERVER['HTTPS'] === 'on' ? 's':'') . '://' . $_SERVER['HTTP_HOST'];
    }

    /**
     * @return string
     * @throws ProxyException
     */
    private function getCallableUrl():string
    {
        $proxiedUrl = $_GET['url'];
        $requestUrl = $this->getProxyDomain() . $proxiedUrl;
        if (substr($proxiedUrl, 0, 1) !== '/') { // Needed since validate_url filter will accept full urls as param
            throw new ProxyException('Invalid proxy forward given', 2);
        }
        if (!filter_var($requestUrl, FILTER_VALIDATE_URL, FILTER_FLAG_PATH_REQUIRED)) {
            throw new ProxyException('Invalid request uri given!', 3);
        }
        return $requestUrl;
    }

    /**
     * @throws Exception
     */
    private function getRequestParams()
    {
        $request_method = $this->getRequestMethod();
        if ('GET' == $request_method) {
            $request_params = $_GET;
        } elseif ('POST' == $request_method) {
            $request_params = $_POST;
            if (empty($request_params)) {
                $data = file_get_contents('php://input');
                if (!empty($data)) {
                    $request_params = $data;
                }
            }
            if ($_GET['url'] == self::CREATE_NEW_PARTY) {
                $request_params = $this->removeDobForBusiness($request_params);
            }
        } elseif ('PUT' == $request_method || 'DELETE' == $request_method) {
            $request_params = file_get_contents('php://input');
        } else {
            $request_params = null;
        }
        return $request_params;
    }

    /**
     * @param string $requestParams
     * @return string
     */
    private function removeDobForBusiness(string $requestParams):string
     {
         $requestData = json_decode($requestParams , true); // $requestParams is the json array before decoding
         if ($requestData['customerType'] == "organization" && isset($requestData['dateOfBirth']) && $requestData['dateOfBirth'] == "") {
             unset($requestData['dateOfBirth']);
         }
         return json_encode($requestData);
    }

    /**
     * @return array
     */
    private function getRequestHeaders():array
    {
        $request_headers = [];
        foreach ($_SERVER as $key => $value) {
            if (strpos($key, 'HTTP_') === 0  ||  strpos($key, 'CONTENT_') === 0) {
                $headername = str_replace('_', ' ', str_replace('HTTP_', '', $key));
                $headername = str_replace(' ', '-', ucwords(strtolower($headername)));
                if (!in_array($headername, array( 'Host', 'X-Proxy-Url','Content-Length','Accept'))) {
                    // $request_headers[] = "$headername: $value";
                }
                if (!in_array("Content-Type: application/json", $request_headers)) {
                    $request_headers[] = "Content-Type: application/json";
                }
            }
        }
        $request_headers[] = 'Authorization: Bearer '.$this->getJWTToken();
        return $request_headers;
    }

    /**
     * Gett he JWT token
     * @return string
     */
    private function getJWTToken():?string
    {
        //$jwtPrivateKey = Mage::getStoreConfig('jwt/private');
        $session = Mage::getSingleton('customer/session');
        $agent = $session->getAgent();
        $signer = new Sha256();
        //  $keychain = new Keychain();
        // In case there is no session, fail on CS permissions instead of PHP error 500 while accessing null $agent
        $token = '';
        if (!empty($agent)) {
            $token = (new Builder())
                //->setIssuer($this->getProxyDomain())
                //->setSubject($agent->getId())
                //->setIssuedAt(time())
                //->setExpiration(time() + 300)
                ->set('id',$agent->getId())
                ->set('scopes', $this->getJWTScopes())
                ->sign($signer, Vznl_Customer_Helper_Search::JWT_CUSTOMER_SEARCH_SECRET)
                ->getToken();
        }
        return $token;
    }

    /**
     * Get the JWT scopes
     * @return array
     */
    private function getJWTScopes():?array
    {
        /* @var $session Vznl_Customer_Model_Session */
        $session = Mage::getSingleton('customer/session');
        return $session->getAgent()->getJwtDealerScopes();
    }

    /**
     * Cache customer password
     * @param string $response
     * @return void
     * @throws Exception
     */
    private function passwordCache(string $response):void
    {
        if ($_GET['url'] == self::CREATE_NEW_PARTY) {
            $dataSavetoSession = false;
            $headerData = json_decode($this->getRequestParams(),true);
            $responseData = json_decode($response,true);
            $customerAccountIdentifier = $responseData['data']['customerAccountIdentifiers'][0];
            $session = Mage::getSingleton('customer/session');
            if (!empty($headerData['password']) && !empty($customerAccountIdentifier)) {
                $dataSavetoSession = array("customerAccountIdentifier" => $customerAccountIdentifier, "passwordCached" => $headerData['password']);
            }
            $session->setCustomerCachedHeader($dataSavetoSession);
        }
    }

    /**
     * Propare log to be saved to file
     * @param array|null $requestHeaders
     * @param string $request
     * @param $requestParameters
     * @param string|null $responseHeaders
     * @param string|null $response
     * @return void
     * @throws Mage_Core_Model_Store_Exception
     */
    private function logRequestsDetails(
        ?array $requestHeaders,
        string $request,
        $requestParameters,
        ?string $responseHeaders,
        ?string $response
    ):void
    {
        $name = $this->getApiNameForLogs();
        // Implode headers to receive string from array
        if (is_array($requestHeaders)) {
            $requestHeaders = implode("\n", $requestHeaders);
        }
        // Add parameters details
        if (!empty($requestParameters)) {
            $request = $request . "\n\nParameters Data:\n" . implode( "\n", $requestParameters );
        }
        // Save log to file
        $this->coreHelper->transferLog(
            $request,
            $response,
            $name,
            [
                'execution' => $this->executionTimeId,
                'request' => $this->requestTimeId
            ],
            'PROXY',
            $requestHeaders,
            $responseHeaders
        );
    }

    /**
     * Get API name for logs
     * @return string
     */
    private function getApiNameForLogs():string
    {
        preg_match('/(\/)?api\/v[0-9]+\/([^?]+)?/', $_GET['url'], $apiName);
        if (isset($apiName[2]) && !empty($apiName[2])) {
            return str_replace('/', '', ucwords($apiName[2], '/'));
        }
        return 'ExternalCallNameUnknown';
    }

}

try {
    $proxy = new Proxy();
    echo $proxy->call();
    exit();
} catch (Exception $e) {
    $data['errors'][] = [
        'status' => $e->getCode(),
        'detail' => $e->getMessage()
    ];
    http_response_code($e->httpCode);
    header('Content-Type: application/json');
    echo json_encode($data);
}
