<?php

class OmniusSnappyPdf extends Knp\Snappy\Pdf
{
    /**
     * @var array
     */
    protected $options = [
        'encoding' => 'UTF-8',
        'dpi' => 300,
    ];

    /**
     * OmniusSnappyPdf constructor.
     * @param array|null $options
     */
    public function __construct(array $options = null)
    {
        parent::__construct(MAGENTO_ROOT.'/vendor/bin/wkhtmltopdf-amd64');
        if(!is_null($options)) {
            $options = array_merge($this->options, $options);
            $this->setOptions($options);
        }
    }
}