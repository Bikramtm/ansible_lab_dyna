<?php
require_once __DIR__ . '/../vendor/dompdf/dompdf/lib/html5lib/Tokenizer.php';
class OmniusPdf extends \Dompdf\Dompdf
{
    public function __construct($options = null)
    {
        // DOMPDF options
        if($options == null){
            $options = new \Dompdf\Options();
        }
        $options->setFontDir('skin/frontend/vodafone/default/fonts/test');
        $options->setDefaultFont('vodafonerg');
        $options->setIsPhpEnabled(true);
        $options->setIsHtml5ParserEnabled(true); // For combining multiple pdf outputs
        $options->setIsFontSubsettingEnabled(true);
        $options->setDefaultPaperSize('A4');

        $this->setPaper('A4', 'landscape');
        parent::__construct($options);
    }
}
