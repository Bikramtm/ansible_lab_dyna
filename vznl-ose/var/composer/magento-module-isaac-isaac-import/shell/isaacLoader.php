<?php
/**
* ISAAC ISAAC_Import
*
* @category ISAAC
* @package ISAAC_Import
* @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
* @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
*/
require_once dirname($_SERVER['SCRIPT_NAME']) . DIRECTORY_SEPARATOR . 'abstract.php';

class ISAAC_Import_Shell_Loader extends Mage_Shell_Abstract
{

    /** @var ISAAC_Import_Helper_Data $helper */
    protected $helper;

    /** @var ISAAC_Import_Loader $loaderClass */
    protected $loaderClass;

    /** @var string $inputFilePath */
    protected $inputFilePath;

    /** */
    public function _construct()
    {
        parent::_construct();
        $this->helper = Mage::helper('isaac_import');
    }

    /** */
    public function run()
    {
        if (!$this->getArg('loader') || !$this->getArg('file')) {
            echo $this->usageHelp();
            exit(0);
        }
        $this->initializePhpConfiguration();
        $profile = (bool) $this->getArg('profile');
        if ($profile) {
            Varien_Profiler::enable();
        }
        try {
            $logDescription = sprintf(
                'loading values from %s using loader class %s',
                $this->getArg('file'),
                $this->getArg('loader')
            );
            $this->helper->logMessage('started ' . $logDescription);
            $this->importValuesFromFile($this->getArg('file'), $this->getLoader($this->getArg('loader')));
            $this->helper->logMessage('finished ' . $logDescription);
            if ($profile) {
                Varien_Profiler::disable();
                /** @var ISAAC_Import_Helper_Profiler $profilerHelper */
                $profilerHelper = Mage::helper('isaac_import/profiler');
                $this->helper->logMessage('profiling results' . PHP_EOL . $profilerHelper->renderAsText());
            }
            exit($this->helper->getLoaderExitCodeSuccess());
        } catch (Exception $e) {
            $errorMessage = 'error ' . $logDescription . ': ' . $e->getMessage() . PHP_EOL;
            $errorMessage .= 'stack trace:' . PHP_EOL;
            $errorMessage .= $e->getTraceAsString();
            fwrite(STDERR, $errorMessage);
            $this->helper->logMessage($errorMessage);
            $this->helper->emailMessage('', 'error', $errorMessage);
            exit(1);
        }
    }

    /**
     * @return string
     */
    public function usageHelp()
    {
        $usageHelp =  'Usage: php ' . $_SERVER['argv'][0] . ' --loader LOADER --file FILE [--profile]' . PHP_EOL;
        $usageHelp .= PHP_EOL;
        $usageHelp .= 'This will load serialized import values from FILE using loader class LOADER.' . PHP_EOL;
        $usageHelp .= PHP_EOL;
        $usageHelp .= 'When --profile is supplied, profiling results will be added to the log.' . PHP_EOL;
        return $usageHelp;
    }

    /**
     *
     */
    protected function initializePhpConfiguration()
    {
        //enable all errors
        error_reporting(E_ALL | E_STRICT);
        //fail on assertions
        assert_options(ASSERT_BAIL, true);
        //track errors (used to capture error messages in the variable $php_errormsg)
        ini_set('track_errors', true);
    }

    /**
     * @param string $loaderClass
     * @return ISAAC_Import_Loader
     * @throws Mage_Core_Exception
     */
    protected function getLoader($loaderClass)
    {
        $loader = Mage::getModel($loaderClass);
        if (!$loader) {
            Mage::throwException('could not instantiate loader class "' . $loaderClass . '"');
        }
        if (!$loader instanceof ISAAC_Import_Loader) {
            Mage::throwException('loader class ' . get_class($loader) . ' should be an instance of ISAAC_Import_Loader');
        }
        return $loader;
    }

    /**
     * @param string $filePath
     * @param ISAAC_Import_Loader $loader
     * @throws Mage_Core_Exception
     */
    protected function importValuesFromFile($filePath, ISAAC_Import_Loader $loader)
    {
        Varien_Profiler::start(__METHOD__);
        $fileContents = @file_get_contents($filePath);
        if ($fileContents === false) {
            Mage::throwException('could not read contents from ' . $filePath . ' (' . $php_errormsg . ')');
        }
        $importValues = @unserialize($fileContents);
        if ($importValues === false) {
            Mage::throwException('could not unserialize data from ' . $filePath . ' (' . $php_errormsg . ')');
        }
        if (!is_array($importValues)) {
            Mage::throwException('unserialized data from ' . $filePath . ' is not an array');
        }
        $loader->setImportValues($importValues);
        $loader->import();
        Varien_Profiler::stop(__METHOD__);
    }
}

$shell = new ISAAC_Import_Shell_Loader();
$shell->run();
