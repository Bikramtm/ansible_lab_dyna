<?php
/**
* ISAAC ISAAC_Import
*
* @category ISAAC
* @package ISAAC_Import
* @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
* @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
*/
require_once dirname($_SERVER['SCRIPT_NAME']) . DIRECTORY_SEPARATOR . 'abstract.php';

class ISAAC_Import_Shell_Importer extends Mage_Shell_Abstract
{

    /** @var ISAAC_Import_Helper_Data $helper */
    protected $helper;

    /** @var ISAAC_Import_Model_Configuration_Group_Repository $configurationGroupRepository */
    protected $configurationGroupRepository;

    /** @var ISAAC_Import_Model_Configuration_Group[] $activeConfigurationGroups */
    protected $activeConfigurationGroups = [];

    /** @var bool */
    protected $cleanupAfterImport = true;

    /** @var bool */
    protected $reindex = true;

    /** @var bool */
    protected $profile = false;

    /** */
    public function _construct()
    {
        parent::_construct();
        $this->helper = Mage::helper('isaac_import');
        $this->configurationGroupRepository = Mage::getSingleton('isaac_import/configuration_group_repository');
    }

    /**
     *
     */
    public function run()
    {
        if ($this->getArg('dev-help')) {
            echo $this->getDeveloperUsageHelp();
            exit(0);
        }
        if (!$this->getArg('import-group') && !$this->getArg('import')) {
            echo $this->usageHelp();
            exit(0);
        }
        if ($this->getArg('import-group') === true) {
            fwrite(STDERR, 'no argument supplied to --import-group option' . PHP_EOL);
            exit(1);
        }
        if ($this->getArg('import') === true) {
            fwrite(STDERR, 'no argument supplied to --import option' . PHP_EOL);
            exit(1);
        }
        if (count(array_filter([$this->getArg('import-group'), $this->getArg('import')])) > 1) {
            fwrite(STDERR, 'cannot use --import-group and --import simultaneously' . PHP_EOL);
            exit(1);
        }
        if ($this->getArg('no-cleanup')) {
            $this->setCleanupAfterImport(false);
        }
        if ($this->getArg('no-reindex')) {
            $this->setReindex(false);
        }
        if ($this->getArg('profile')) {
            $this->setProfile(true);
            Varien_Profiler::enable();
        }
        $this->initializePhpConfiguration();
        if ($this->directoryIsLocked()) {
            fwrite(STDERR, 'directory is locked; quitting' . "\n");
            exit(1);
        }
        try {
            $this->lockDirectory();
        } catch (Exception $e) {
            //could not lock directory
            fwrite(STDERR, 'could not lock directory (' . $e->getMessage() . '); quitting' . PHP_EOL);
            exit(1);
        }
        try {
            $this->import();
            $exitCode = 0;
        } catch (Exception $e) {
            fwrite(STDERR, 'import error: ' . $e->getMessage() . PHP_EOL);
            $this->helper->logMessage('import error: ' . $e->getMessage());
            $this->helper->emailMessage('', 'error', $e->getMessage());
            $exitCode = 1;
        }
        if ($this->getProfile()) {
            Varien_Profiler::disable();
            /** @var ISAAC_Import_Helper_Profiler $profilerHelper */
            $profilerHelper = Mage::helper('isaac_import/profiler');
            $this->helper->logMessage('profiling results' . PHP_EOL . $profilerHelper->renderAsText());
        }
        $this->unlockDirectory();
        exit($exitCode);
    }

    /**
     * @return string
     */
    public function usageHelp()
    {
        $usageHelp = 'Usage: php ' . $_SERVER['argv'][0] . '[OPTION..]' . PHP_EOL;
        $usageHelp .= 'The following OPTION\'s are supported:' . PHP_EOL;
        $usageHelp .= '--import-group IMPORT_GROUP,...  Import IMPORT_GROUP\'s consecutively (see below)' . PHP_EOL;
        $usageHelp .= '--import IMPORT_SINGLE,...       Import IMPORT_SINGLE\'s consecutively (see below)' . PHP_EOL;
        $usageHelp .= '--no-cleanup                     Do not perform cleanup operations (if any) after each import type' . PHP_EOL;
        $usageHelp .= '--no-reindex                     Do not switch to manual mode during import and reindex afterwards' . PHP_EOL;
        $usageHelp .= '--profile                        Add profiling results to the log' . PHP_EOL;
        $usageHelp .= '--help                           Print this help message' . PHP_EOL;
        $usageHelp .= '--dev-help                       Print help information for developers' . PHP_EOL;
        $usageHelp .= PHP_EOL;
        if (count($this->getActiveConfigurationGroups()) == 0) {
            $usageHelp .= 'No IMPORT_GROUP\'s found; see --dev-help on how to add them.' . PHP_EOL;
            return $usageHelp;
        }
        $usageHelp .= 'The following IMPORT_GROUP\'s are supported:' . PHP_EOL;
        foreach ($this->getActiveConfigurationGroups() as $configurationGroup) {
            $usageHelp .= '  ' . $configurationGroup->getCliCode() . PHP_EOL;
            foreach ($this->helper->explodeOnNewLines($configurationGroup->getCliDescription()) as $descriptionLine) {
                $usageHelp .= '    ' . $descriptionLine . PHP_EOL;
            }
            $usageHelp .= PHP_EOL;
        }
        $usageHelp .= PHP_EOL;
        $usageHelp .= 'The following IMPORT_SINGLE\'s are supported: ' . PHP_EOL;
        foreach ($this->getActiveConfigurationGroups() as $configurationGroup) {
            $activeConfigurations = $this->getActiveConfigurations($configurationGroup);
            if (count($activeConfigurations) == 0) {
                $usageHelp .= 'No IMPORT_SINGLE\'s found for IMPORT_GROUP '  . $configurationGroup->getCliCode() . '; see --dev-help on how to add them.' . PHP_EOL;
                return $usageHelp;
            }
            foreach ($activeConfigurations as $configuration) {
                $usageHelp .= '  ' . $configuration->getCliCode() . ' (group ' . $configurationGroup->getCliCode() . ')' . PHP_EOL;
                foreach ($this->helper->explodeOnNewLines($configuration->getCliDescription()) as $descriptionLine) {
                    $usageHelp .= '    ' . $descriptionLine . PHP_EOL;
                }
                $usageHelp .= PHP_EOL;
            }
        }
        return $usageHelp;
    }

    /**
     * @return string
     */
    public function getDeveloperUsageHelp()
    {
        $result = 'Import groups and single imports can be defined for the cli interface in a module\'s config.xml.' . PHP_EOL;
        $result .= PHP_EOL;
        $result .= 'An import group can be defined as follows:' . PHP_EOL;
        $result .= '- add a node with a unique id to the path \'default/isaac_import/group\'' . PHP_EOL;
        $result .= '- this node should contain the following subnodes: ' . PHP_EOL;
        $result .= '    cli_code             : used as import group identifier in the cli interface (duplicates are allowed)' . PHP_EOL;
        $result .= '    cli_description      : used for cli help message' . PHP_EOL;
        $result .= '    configuration        : one or more single imports' . PHP_EOL;
        $result .= PHP_EOL;
        $result .= 'A single import can be defined as follows:' . PHP_EOL;
        $result .= '- add a node with a unique id to the path \'default/isaac_import/group/CONFIGURATION_GROUP_ID/configuration\'' . PHP_EOL;
        $result .= '- this node should contain the following subnodes: ' . PHP_EOL;
        $result .= '    cli_code             : used as single import identifier in the cli interface (duplicates are allowed)' . PHP_EOL;
        $result .= '    cli_description      : used for the cli help message' . PHP_EOL;
        $result .= '    generator_class      : the name of the generator class' . PHP_EOL;
        $result .= '    loader_class         : the name of the loader class' . PHP_EOL;
        $result .= '    generator_init_params: constructor arguments for the generator class (optional)' . PHP_EOL;
        $result .= '    batch_size           : the size of batches in which import values are processed (optional, default 0)' . PHP_EOL;
        $result .= '    indexers             : codes of indexers that are affected by the import (optional)' . PHP_EOL;
        $result .= '    loader_via_exec      : execute loader via external script (optional, default false)' . PHP_EOL;
        $result .= PHP_EOL;
        $result .= 'It is also possible to run imports without the cli interface, by directly using the corresponding models:' . PHP_EOL;
        $result .= '- group imports: \'isaac_import/group_import\', initialized using \'isaac_import/configuration_group\'' . PHP_EOL;
        $result .= '- single imports: \'isaac_import/import\', initialized using \'isaac_import/configuration\'' . PHP_EOL;
        $result .= PHP_EOL;
        $result .= 'The configuration classes are initialized using an array as a constructor argument, in which:' . PHP_EOL;
        $result .= '- the corresponding XML nodes are represented by keys and values' . PHP_EOL;
        $result .= '- the id should be supplied as array key \'id\'' . PHP_EOL;
        $result .= '- array keys \'cli_code\' and \'cli_description\' are optional and not used' . PHP_EOL;
        return $result;
    }

    /**
     * @param bool $cleanupAfterImport
     * @return $this
     */
    public function setCleanupAfterImport($cleanupAfterImport)
    {
        $this->cleanupAfterImport = $cleanupAfterImport;
        return $this;
    }

    /**
     * @return bool
     */
    public function getCleanupAfterImport()
    {
        return $this->cleanupAfterImport;
    }

    /**
     * @param bool $reindex
     * @return $this
     */
    public function setReindex($reindex)
    {
        $this->reindex = $reindex;
        return $this;
    }

    /**
     * @return bool
     */
    public function getReindex()
    {
        return $this->reindex;
    }

    /**
     * @return boolean
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * @param boolean $profile
     * @return $this
     */
    public function setProfile($profile)
    {
        $this->profile = $profile;
        return $this;
    }

    /**
     * @return ISAAC_Import_Model_Configuration_Group[]
     * @throws Mage_Core_Exception
     */
    protected function getActiveConfigurationGroups()
    {
        if (!$this->activeConfigurationGroups) {
            $this->activeConfigurationGroups = [];
            foreach ($this->configurationGroupRepository->findAll() as $configurationGroup) {
                if ($configurationGroup->getActive()) {
                    $this->activeConfigurationGroups[] = $configurationGroup;
                }
            }
        }
        return $this->activeConfigurationGroups;
    }

    /**
     * @param ISAAC_Import_Model_Configuration_Group $configurationGroup
     * @return ISAAC_Import_Model_Configuration[]
     */
    protected function getActiveConfigurations(ISAAC_Import_Model_Configuration_Group $configurationGroup)
    {
        $activeConfigurations = [];
        foreach ($configurationGroup->getConfigurations() as $configuration) {
            if ($configuration->getActive()) {
                $activeConfigurations[] = $configuration;
            }
        }
        return $activeConfigurations;
    }

    /**
     *
     */
    protected function initializePhpConfiguration()
    {
        //enable all errors
        error_reporting(E_ALL | E_STRICT);
        //fail on assertions
        assert_options(ASSERT_BAIL, true);
        //track errors (used to capture error messages in the variable $php_errormsg)
        ini_set('track_errors', true);
    }

    /**
     *
     */
    protected function import()
    {
        Varien_Profiler::start(__METHOD__);
        if ($this->getArg('import-group')) {
            $configurationGroups = $this->getActiveConfigurationGroupsByCodes(explode(',', $this->getArg('import-group')));
            foreach ($configurationGroups as $configurationGroup) {
                $this->importGroup($configurationGroup);
            }
        }
        if ($this->getArg('import')) {
            $configurations = $this->getActiveConfigurationsByCodes(explode(',', $this->getArg('import')));
            foreach ($configurations as $configuration) {
                $this->importSingle($configuration);
            }
        }
        Varien_Profiler::stop(__METHOD__);
    }

    /**
     * @param string[] $groupCodes
     * @return ISAAC_Import_Model_Configuration_Group[]
     * @throws Exception
     */
    protected function getActiveConfigurationGroupsByCodes(array $groupCodes) {
        $configurationGroups = [];
        foreach ($groupCodes as $groupCode) {
            $configurationGroupFound = false;
            foreach ($this->getActiveConfigurationGroups() as $configurationGroup) {
                if ($configurationGroup->getCliCode() == $groupCode) {
                    $configurationGroups[] = $configurationGroup;
                    $configurationGroupFound = true;
                }
            }
            if (!$configurationGroupFound) {
                throw new Exception('import group code "' . $groupCode . '" does not exist');
            }
        }
        return $configurationGroups;
    }

    /**
     * @param string[] $codes
     * @return ISAAC_Import_Model_Configuration[]
     * @throws Exception
     */
    protected function getActiveConfigurationsByCodes(array $codes) {
        $configurations = [];
        foreach ($codes as $code) {
            $configurationFound = false;
            foreach ($this->getActiveConfigurationGroups() as $configurationGroup) {
                foreach ($this->getActiveConfigurations($configurationGroup) as $configuration) {
                    if ($configuration->getCliCode() == $code) {
                        $configurations[] = $configuration;
                        $configurationFound = true;
                    }
                }
            }
            if (!$configurationFound) {
                throw new Exception('import code "' . $code . '" does not exist');
            }
        }
        return $configurations;
    }

    /**
     * @param ISAAC_Import_Model_Configuration_Group $configurationGroup
     */
    protected function importGroup(ISAAC_Import_Model_Configuration_Group $configurationGroup)
    {
        /** @var ISAAC_Import_Model_Group_Importer $groupImporter */
        $groupImporter = Mage::getModel('isaac_import/group_importer', $configurationGroup);
        $groupImporter->setReindex($this->getReindex());
        $groupImporter->setCleanup($this->getCleanupAfterImport());
        $groupImporter->setProfile($this->getProfile());
        $groupImporter->import();
    }

    /**
     * @param ISAAC_Import_Model_Configuration $configuration
     */
    protected function importSingle(ISAAC_Import_Model_Configuration $configuration)
    {
        /** @var ISAAC_Import_Model_Importer $importer */
        $importer = Mage::getModel('isaac_import/importer', $configuration);
        $importer->setReindex($this->getReindex());
        $importer->setCleanup($this->getCleanupAfterImport());
        $importer->setProfile($this->getProfile());
        $importer->import();
    }

    /**
     * @return bool
     */
    protected function directoryIsLocked()
    {
        if (!file_exists($this->getLockFilename())) {
            return false;
        }
        return posix_kill($this->getLockingPid(), 0);
    }

    /**
     * @return $this
     * @throws Mage_Core_Exception
     */
    protected function lockDirectory()
    {
        $pid = getmypid();
        if ($pid === false) {
            throw new RuntimeException('Could not get pid of the current process');
        }
        $this->setLockingPid($pid);
        return $this;
    }

    /**
     *
     */
    protected function unlockDirectory()
    {
        if (file_exists($this->getLockFilename())) {
            unlink($this->getLockFilename());
        }
    }

    /**
     * @return string
     */
    protected function getLockFilename()
    {
        return Mage::getBaseDir('var') . '/import/.import_values_dir.pid';
    }

    /**
     * @return string
     */
    protected function getLockingPid()
    {
        return trim(file_get_contents($this->getLockFilename()));
    }

    /**
     * @param string $pid
     * @return $this
     */
    protected function setLockingPid($pid)
    {
        file_put_contents($this->getLockFilename(), $pid);
        return $this;
    }

}

$shell = new ISAAC_Import_Shell_Importer();
$shell->run();
