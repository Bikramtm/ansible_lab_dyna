<?php
/**
 * ISAAC ISAAC_Import
 *
 * @category ISAAC
 * @package ISAAC_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */
class ISAAC_Import_Helper_Property_Category extends Mage_Core_Helper_Abstract
{

    /** @var null|ISAAC_Import_Model_Property[] */
    protected $propertiesByAttributeCode = array();

    /** @var ISAAC_Import_Helper_Data */
    protected $importHelper;

    /** @var Mage_Eav_Model_Config */
    protected $eavConfig;

    /**
     *
     */
    public function __construct()
    {
        $this->importHelper = Mage::helper('isaac_import');
        $this->eavConfig = Mage::getSingleton('eav/config');
    }

    /**
     * @param string $attributeCode
     * @return ISAAC_Import_Model_Property|null
     */
    public function getPropertyByAttributeCode($attributeCode) {
        Varien_Profiler::start(__METHOD__);
        if (!array_key_exists($attributeCode, $this->propertiesByAttributeCode)) {
            $attribute = $this->eavConfig->getAttribute(Mage_Catalog_Model_Category::ENTITY, $attributeCode);
            if (!$attribute || !$attribute->getId()) {
                Mage::throwException('could not find attribute with code ' . $attributeCode);
            }
            $propertyType = $this->getPropertyTypeByAttribute($attribute);
            /** @var ISAAC_Import_Model_Property $property */
            $property = Mage::getModel('isaac_import/property');
            $property->initialize($attributeCode, $this->isEmptyAllowed($attribute), $propertyType);

            $this->propertiesByAttributeCode[$attributeCode] = $property;
        }
        Varien_Profiler::stop(__METHOD__);
        return $this->propertiesByAttributeCode[$attributeCode];
    }

    /**
     * @param Mage_Eav_Model_Entity_Attribute_Abstract $attribute
     * @return string
     */
    protected function getPropertyTypeByAttribute(Mage_Eav_Model_Entity_Attribute_Abstract $attribute) {
        switch ($attribute->getData('frontend_input')) {
            case 'price':
                return ISAAC_Import_Model_Property::PROPERTY_TYPE_NUMBER;
            case 'weight':
                return ISAAC_Import_Model_Property::PROPERTY_TYPE_NUMBER;
            case 'boolean':
                return ISAAC_Import_Model_Property::PROPERTY_TYPE_BIT;
            case 'text':
                switch ($attribute->getData('frontend_class')) {
                    //Note: only validation of numbers and digits is currently supported, validation of other types is desirable
                    case 'validate-number':
                        return ISAAC_Import_Model_Property::PROPERTY_TYPE_NUMBER;
                    case 'validate-digits':
                        return ISAAC_Import_Model_Property::PROPERTY_TYPE_UNSIGNED_INTEGER;
                }
            case 'select':
                switch ($attribute->getData('source_model')) {
                    case 'eav/entity_attribute_source_boolean':
                        return ISAAC_Import_Model_Property::PROPERTY_TYPE_BIT;
                }
        }
        return ISAAC_Import_Model_Property::PROPERTY_TYPE_STRING;
    }

    /**
     * @param Mage_Eav_Model_Entity_Attribute_Abstract $attribute
     * @return bool
     */
    protected function isEmptyAllowed(Mage_Eav_Model_Entity_Attribute_Abstract $attribute)
    {
        if (!$attribute->getData('is_required')) {
            return true;
        }

        if (in_array($attribute->getData('frontend_input'), array('price', 'weight', 'select'))) {
            return true;
        }

        if ($attribute->getData('source_model') === 'eav/entity_attribute_source_boolean') {
            return true;
        }

        return false;
    }
}