<?php
/**
 * ISAAC ISAAC_Import
 *
 * @category ISAAC
 * @package ISAAC_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */

abstract class ISAAC_Import_Model_Generator_Csv_File extends ISAAC_Import_Model_Extract_Transform_Generator
{
    /**
     * @var string
     */
    protected $importFilePath = '';

    /**
     * @param array $initParams
     */
    public function __construct(array $initParams)
    {
        $requiredFields = ['importFileName'];
        foreach ($requiredFields as $requiredField) {
            if (!array_key_exists($requiredField, $initParams)) {
                Mage::throwException(sprintf(
                    'could not create import configuration: required field %s is missing',
                    $requiredField
                ));
            }
        }

        $this->importFilePath = $initParams['importFileName'];
    }

    /**
     * @inheritDoc
     */
    public function getExtractIterator()
    {
        $csvIterator = new ISAAC_Import_CSV_Dict_Reader($this->getImportFilePath());
        $csvIterator->setDelimiter(',');
        $csvIterator->setSkipEmptyLines(true);
        $csvIterator->setTrimKeys(true);
        $csvIterator->setTrimValues(true);

        return $csvIterator;
    }

    /**
     * @return string
     */
    public function getImportFilePath()
    {
        return $this->importFilePath;
    }
}
