<?php
/**
 * ISAAC ISAAC_Import
 *
 * @category ISAAC
 * @package ISAAC_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */

class ISAAC_Import_Transformer_Group_AllProducts implements ISAAC_Import_TransformerInterface
{
    /**
     * @param array $parameters
     * @return array
     */
    public function apply($parameters)
    {
        return $parameters;
    }
}
