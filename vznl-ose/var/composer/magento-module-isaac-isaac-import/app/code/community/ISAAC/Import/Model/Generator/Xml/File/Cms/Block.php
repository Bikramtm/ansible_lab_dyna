<?php
/**
 * ISAAC ISAAC_Import
 *
 * @category ISAAC
 * @package ISAAC_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (http://www.isaac.nl)
 
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */
 
class ISAAC_Import_Model_Generator_Xml_File_Cms_Block extends ISAAC_Import_Model_Generator_Xml_File
{
    /**
     * @return string
     */
    protected function getValidationSchema()
    {
        return <<<XML
<?xml version="1.0" encoding="utf-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified">
    <xs:element name="blocks">
        <xs:complexType>
            <xs:sequence>
                <xs:element maxOccurs="unbounded" ref="block"/>
            </xs:sequence>
        </xs:complexType>
    </xs:element>
    <xs:element name="block">
        <xs:complexType>
            <xs:all>
                <xs:element ref="flags"/>
                <xs:element ref="identifier"/>
                <xs:element ref="store_code" minOccurs="0" maxOccurs="1"/>
                <xs:element ref="title" minOccurs="0" maxOccurs="1"/>
                <xs:element ref="content" minOccurs="0" maxOccurs="1"/>
                <xs:element ref="is_active" minOccurs="0" maxOccurs="1"/>
            </xs:all>
        </xs:complexType>
    </xs:element>
    <xs:element name="flags" type="xs:string"/>
    <xs:element name="identifier" type="xs:NCName"/>
    <xs:element name="store_code" type="xs:string"/>
    <xs:element name="title" type="xs:string"/>
    <xs:element name="content" type="xs:string"/>
    <xs:element name="is_active" type="xs:integer"/>
</xs:schema>
XML;
    }

    /**
     * @inheritdoc
     */
    public function transformValue($value, $key)
    {
        if (!$value instanceof SimpleXMLIterator) {
            Mage::throwException('value needs to be instance of SimpleXMLIterator.');
        }

        /** @var SimpleXMLIterator $value */
        /** @var SimpleXMLIterator $identifier */
        /** @var SimpleXMLIterator $flags */
        /** @var SimpleXMLIterator $storeCode */
        /** @var SimpleXMLIterator $title */
        /** @var SimpleXMLIterator $content */
        /** @var SimpleXMLIterator $isActive */

        $identifier = $value->identifier;
        $flags = $value->flags;
        $storeCode = $value->store_code;
        $title = $value->title;
        $content = $value->content;
        $isActive = $value->is_active;

        /** @var ISAAC_Import_Model_Import_Value_Cms_Block $importValueCmsBlock */
        $importValueCmsBlock = Mage::getModel('isaac_import/import_value_cms_block', $identifier->__toString());
        $importValueCmsBlock->setLoadingFlags(explode(',', $flags->__toString()));
        $importValueCmsBlock->setStoreCode($storeCode->__toString());

        if ($title->count()) {
            $importValueCmsBlock->setTitle($title->__toString());
        }
        if ($content->count()) {
            $importValueCmsBlock->setContent($content->__toString());
        }
        if ($isActive->count()) {
            $importValueCmsBlock->setIsActive($isActive->__toString());
        }

        return $importValueCmsBlock;
    }

    /**
     * @inheritdoc
     */
    public function cleanup()
    {
    }
}
