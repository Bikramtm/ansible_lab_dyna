<?php
/**
 * ISAAC ISAAC_Import
 *
 * @category ISAAC
 * @package ISAAC_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */

abstract class ISAAC_Import_Model_Loader_Model extends ISAAC_Import_Loader
{
    /** @var ISAAC_Import_Helper_Data */
    protected $importHelper;

    /** @var ISAAC_Import_Helper_Format */
    protected $formatHelper;

    /**
     * ISAAC_Import_Model_Loader_Model constructor.
     */
    public function __construct()
    {
        $this->importHelper = Mage::helper('isaac_import/data');
        $this->formatHelper = Mage::helper('isaac_import/format');
    }

    /**
     * @param ISAAC_Import_Model_Import_Value $importValue
     * @param Mage_Core_Model_Abstract $model
     */
    protected function importModel(ISAAC_Import_Model_Import_Value $importValue, Mage_Core_Model_Abstract $model)
    {
        if ($model->getId()) {
            $this->importExistingModel($importValue, $model);
        } else {
            $this->importNewModel($importValue, $model);
        }
    }

    /**
     * @param ISAAC_Import_Model_Import_Value $importValue
     * @param Mage_Core_Model_Abstract $model
     */
    protected function importExistingModel(
        ISAAC_Import_Model_Import_Value $importValue,
        Mage_Core_Model_Abstract $model
    ) {
        if ($importValue->isCreate() && !$importValue->isUpdate()) {
            $this->importHelper->logMessage('model exists but is flagged for create');
        } elseif ($importValue->isUpdate()) {
            $this->updateModel($model, $importValue);
        } elseif ($importValue->isDelete()) {
            $this->deleteModel($model, $importValue);
        }
    }

    /**
     * @param ISAAC_Import_Model_Import_Value $importValue
     * @param Mage_Core_Model_Abstract $model
     */
    protected function importNewModel(
        ISAAC_Import_Model_Import_Value $importValue,
        Mage_Core_Model_Abstract $model
    ) {
        if ($importValue->isUpdate() && !$importValue->isCreate()) {
            $this->importHelper->logMessage('model does not exist but is flagged for update');
        } elseif ($importValue->isDelete()) {
            $this->importHelper->logMessage('model does not exist but is flagged for delete');
        } elseif ($importValue->isCreate()) {
            $this->createModel($model, $importValue);
        }
    }

    /**
     * @param Mage_Core_Model_Abstract $model
     * @param ISAAC_Import_Model_Import_Value $importValue
     * @return void
     */
    protected function createModel(Mage_Core_Model_Abstract $model, ISAAC_Import_Model_Import_Value $importValue)
    {
        if (!$model->isObjectNew()) {
            Mage::throwException('could not create model because it was not marked as new');
        }

        $modelValuesToUpdate = $this->getModelValuesToUpdate($model, $importValue);
        $this->importHelper->logDebugMessage($this->formatHelper->formatDataChangesHeader(
            $importValue->getEntityName(),
            $importValue->__toString(),
            true
        ));
        foreach ($modelValuesToUpdate as $key => $value) {
            $this->importHelper->logDebugMessage(
                $this->formatHelper->formatDataChange($key, $model->getData($key), $value)
            );
            $model->setData($key, $value);
        }
        $this->beforeSave($model, $importValue, true);
        $model->save();
        $this->importHelper->logMessage(sprintf(
            'created %s %s (added fields: %s)',
            $importValue->getEntityName(),
            $importValue->__toString(),
            implode(', ', array_keys($modelValuesToUpdate))
        ));
        $this->afterSave($model, $importValue, true);
    }

    /**
     * @param Mage_Core_Model_Abstract $model
     * @param ISAAC_Import_Model_Import_Value $importValue
     * @return void
     */
    protected function updateModel(Mage_Core_Model_Abstract $model, ISAAC_Import_Model_Import_Value $importValue)
    {
        if ($model->isObjectNew()) {
            Mage::throwException('could not update model because it was marked as new');
        }

        $modelValuesToUpdate = $this->getModelValuesToUpdate($model, $importValue);
        if (empty($modelValuesToUpdate)) {
            $this->modelUpToDate($model, $importValue);

            return;
        }
        foreach ($model->getData() as $key => $value) {
            if (!array_key_exists($key, $modelValuesToUpdate) && $this->unsetAllowed($model, $key)) {
                $model->unsetData($key);
            }
        }
        $this->importHelper->logDebugMessage($this->formatHelper->formatDataChangesHeader(
            $importValue->getEntityName(),
            $importValue->__toString(),
            false
        ));
        foreach ($modelValuesToUpdate as $key => $value) {
            $this->importHelper->logDebugMessage(
                $this->formatHelper->formatDataChange($key, $model->getData($key), $value)
            );
            $model->setData($key, $value);
        }
        $this->beforeSave($model, $importValue, false);
        $model->save();
        $this->importHelper->logMessage(sprintf(
            'updated %s %s (updated fields: %s)',
            $importValue->getEntityName(),
            $importValue->__toString(),
            implode(', ', array_keys($modelValuesToUpdate))
        ));
        $this->afterSave($model, $importValue, false);
    }

    /**
     * @param Mage_Core_Model_Abstract $model
     * @param ISAAC_Import_Model_Import_Value $importValue
     * @return void
     */
    protected function deleteModel(Mage_Core_Model_Abstract $model, ISAAC_Import_Model_Import_Value $importValue)
    {
        if ($model->isObjectNew()) {
            Mage::throwException('could not delete model because it was marked as new');
        }

        $this->beforeDelete($model, $importValue);

        Mage::register('isSecureArea', true);
        $model->delete();
        Mage::unregister('isSecureArea');

        $this->importHelper->logMessage(sprintf(
            'deleted %s %s',
            $importValue->getEntityName(),
            $importValue->__toString()
        ));
        $this->importHelper->logDebugMessage(
            $this->formatHelper->formatDataChangesDeletion($importValue->getEntityName(), $importValue->__toString())
        );

        $this->afterDelete($model, $importValue);
    }

    /**
     * @param Mage_Core_Model_Abstract $model
     * @param $key
     * @return bool
     */
    protected function unsetAllowed(Mage_Core_Model_Abstract $model, $key)
    {
        return false;
    }

    /**
     * @param Mage_Core_Model_Abstract $model
     * @param ISAAC_Import_Model_Import_Value $importValue
     */
    protected function modelUpToDate(
        Mage_Core_Model_Abstract $model,
        ISAAC_Import_Model_Import_Value $importValue
    ) {
        $this->importHelper->logMessage(
            sprintf('%s %s is up to date', $importValue->getEntityName(), $importValue->__toString())
        );
        $this->importHelper->logDebugMessage(
            $this->formatHelper->formatNoDataChanges($importValue->getEntityName(), $importValue->__toString())
        );
    }

    /**
     * @param Mage_Core_Model_Abstract $model
     * @param ISAAC_Import_Model_Import_Value $importValue
     * @param bool $isObjectNew
     */
    protected function beforeSave(
        Mage_Core_Model_Abstract $model,
        ISAAC_Import_Model_Import_Value $importValue,
        $isObjectNew
    ) {
    }

    /**
     * @param Mage_Core_Model_Abstract $model
     * @param ISAAC_Import_Model_Import_Value $importValue
     * @param bool $isObjectNew
     */
    protected function afterSave(
        Mage_Core_Model_Abstract $model,
        ISAAC_Import_Model_Import_Value $importValue,
        $isObjectNew
    ) {
    }

    /**
     * @param Mage_Core_Model_Abstract $model
     * @param ISAAC_Import_Model_Import_Value $importValue
     */
    protected function beforeDelete(
        Mage_Core_Model_Abstract $model,
        ISAAC_Import_Model_Import_Value $importValue
    ) {
    }

    /**
     * @param Mage_Core_Model_Abstract $model
     * @param ISAAC_Import_Model_Import_Value $importValue
     */
    protected function afterDelete(
        Mage_Core_Model_Abstract $model,
        ISAAC_Import_Model_Import_Value $importValue
    ) {
    }

    /**
     * @param Mage_Core_Model_Abstract $model
     * @param ISAAC_Import_Model_Import_Value $importValue
     * @return array
     */
    abstract protected function getModelValuesToUpdate(
        Mage_Core_Model_Abstract $model,
        ISAAC_Import_Model_Import_Value $importValue
    );
}
