<?php
/**
 * ISAAC ISAAC_Import
 *
 * @category ISAAC
 * @package ISAAC_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (http://www.isaac.nl)
 
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */
 
class ISAAC_Import_Model_Generator_Xml_File_Cms_Widget extends ISAAC_Import_Model_Generator_Xml_File
{
    /**
     * @return string
     */
    protected function getValidationSchema()
    {
        return <<<XML
<?xml version="1.0" encoding="utf-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified">
    <xs:element name="widgets">
        <xs:complexType>
            <xs:sequence>
                <xs:element maxOccurs="unbounded" ref="widget"/>
            </xs:sequence>
        </xs:complexType>
    </xs:element>
    <xs:element name="widget">
        <xs:complexType>
            <xs:all>
                <xs:element ref="flags"/>
                <xs:element ref="identifier"/>
                <xs:element ref="type" minOccurs="0" maxOccurs="1"/>
                <xs:element ref="store_code" minOccurs="0" maxOccurs="1"/>
                <xs:element ref="title" minOccurs="0" maxOccurs="1"/>
                <xs:element ref="package_theme" minOccurs="0" maxOccurs="1"/>
                <xs:element ref="sort_order" minOccurs="0" maxOccurs="1"/>
                <xs:element ref="parameters" minOccurs="0" maxOccurs="1"/>
                <xs:element ref="page_groups" minOccurs="0" maxOccurs="1"/>
            </xs:all>
        </xs:complexType>
    </xs:element>
    <xs:element name="flags" type="xs:string"/>
    <xs:element name="type" type="xs:string"/>
    <xs:element name="identifier" type="xs:string"/>
    <xs:element name="store_code" type="xs:NCName"/>
    <xs:element name="title" type="xs:string"/>
    <xs:element name="package_theme" type="xs:string"/>
    <xs:element name="sort_order" type="xs:integer"/>
    <xs:element name="parameters">
        <xs:complexType>
            <xs:sequence>
                <xs:any minOccurs="0" maxOccurs="unbounded" processContents="lax" />
            </xs:sequence>
        </xs:complexType>
    </xs:element>
    <xs:element name="page_groups">
        <xs:complexType>
            <xs:sequence>
                <xs:element maxOccurs="unbounded" ref="page_group"/>
            </xs:sequence>
        </xs:complexType>
    </xs:element>
    <xs:element name="page_group">
        <xs:complexType>
            <xs:sequence>
                <xs:element ref="type"/>
                <xs:any maxOccurs="unbounded" processContents="lax" />
            </xs:sequence>
        </xs:complexType>
    </xs:element>
</xs:schema>
XML;
    }

    /**
     * @inheritdoc
     */
    public function transformValue($value, $key)
    {
        if (!$value instanceof SimpleXMLIterator) {
            Mage::throwException('value needs to be instance of SimpleXMLIterator.');
        }

        /** @var SimpleXMLIterator $value */
        /** @var SimpleXMLIterator $identifier */
        /** @var SimpleXMLIterator $flags */
        /** @var SimpleXMLIterator $type */
        /** @var SimpleXMLIterator $title */
        /** @var SimpleXMLIterator $packageTheme */
        /** @var SimpleXMLIterator $storeCode */
        /** @var SimpleXMLIterator $sortOrder */
        /** @var SimpleXMLIterator $parameters */
        /** @var SimpleXMLIterator $pageGroups */

        $identifier = $value->identifier;
        $flags      = $value->flags;
        $type       = $value->type;
        $title      = $value->title;
        $packageTheme = $value->package_theme;
        $storeCode  = $value->store_code;
        $sortOrder  = $value->sort_order;
        $parameters = $value->parameters;
        $pageGroups = $value->page_groups;

        /** @var ISAAC_Import_Model_Import_Value_Cms_Widget $importValueCmsWidget */
        $importValueCmsWidget = Mage::getModel('isaac_import/import_value_cms_widget', $identifier->__toString());

        $loadingFlags = explode(',', $flags->__toString());
        $importValueCmsWidget->setLoadingFlags($loadingFlags);

        if ($type->count()) {
            $importValueCmsWidget->setType($type->__toString());
        }
        if ($storeCode->count()) {
            $importValueCmsWidget->setStoreCode($storeCode->__toString());
        }
        if ($title->count()) {
            $importValueCmsWidget->setTitle($title->__toString());
        }
        if ($packageTheme->count()) {
            $importValueCmsWidget->setPackageTheme($packageTheme->__toString());
        }
        if ($sortOrder->count()) {
            $importValueCmsWidget->setSortOrder($sortOrder->__toString());
        }
        if ($parameters->count()) {
            $importValueCmsWidget->setWidgetParameters($this->convertXmlToArray($parameters));
        }
        if ($pageGroups->count() && $pageGroups->page_group->count()) {
            foreach ($pageGroups->page_group as $pageGroup) {
                /** @var SimpleXMLElement $pageGroup */
                $importValueCmsWidget->addPageGroup($this->convertXmlToArray($pageGroup));
            }
        }

        return $importValueCmsWidget;
    }

    /**
     * @inheritdoc
     */
    public function cleanup()
    {
    }
}
