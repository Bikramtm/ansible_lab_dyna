<?php
/**
 * ISAAC ISAAC_Import
 *
 * @category ISAAC
 * @package ISAAC_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */

class ISAAC_Import_Model_Import_Value_Cms_Block extends ISAAC_Import_Model_Import_Value
{
    /** @var string */
    protected $title;

    /** @var string */
    protected $content;

    /** @var boolean */
    protected $isActive;

    /** @var string */
    protected $storeCode = Mage_Core_Model_Store::ADMIN_CODE;

    /**
     * @inheritdoc
     */
    public function getEntityName()
    {
        return 'block';
    }

    /**
     * @return string
     */
    public function __toString()
    {
        $result = $this->getIdentifier();
        $title = $this->getTitle();
        if ($title) {
            $result .= ' (' . $title . ')';
        }
        $result .= ' for store ' . $this->getStoreCode();
        return $result;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @param boolean $isActive
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    }

    /**
     * @return string
     */
    public function getStoreCode()
    {
        return $this->storeCode;
    }

    /**
     * @param string $storeCode
     */
    public function setStoreCode($storeCode)
    {
        $this->storeCode = $storeCode;
    }
}
