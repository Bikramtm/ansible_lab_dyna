<?php
/**
 * ISAAC ISAAC_Import
 *
 * @category ISAAC
 * @package ISAAC_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */

class ISAAC_Import_Model_Generator_Xml_File_Catalog_Category extends ISAAC_Import_Model_Generator_Xml_File
{
    const XSD_PREFIX = 'xsd';
    const XML_SCHEMA_NAMESPACE = 'http://www.w3.org/2001/XMLSchema';

    const XML_SCHEMA_TYPE_INT = 'INT_TYPE';
    const XML_SCHEMA_TYPE_DECIMAL = 'DECIMAL_TYPE';
    const XML_SCHEMA_TYPE_TEXT = 'TEXT_TYPE';
    const XML_SCHEMA_TYPE_VARCHAR = 'VARCHAR_TYPE';
    const XML_SCHEMA_TYPE_DATETIME = 'DATETIME_TYPE';

    /**
     * @var array
     */
    protected $schemaTypes = [
        self::XML_SCHEMA_TYPE_INT => self::XSD_PREFIX . ':int',
        self::XML_SCHEMA_TYPE_DECIMAL => self::XSD_PREFIX . ':decimal',
        self::XML_SCHEMA_TYPE_TEXT => self::XSD_PREFIX . ':string',
        self::XML_SCHEMA_TYPE_VARCHAR => self::XSD_PREFIX . ':normalizedString',
        self::XML_SCHEMA_TYPE_DATETIME => self::XSD_PREFIX . ':dateTime'
    ];

    /**
     * @var array
     */
    protected $schemaTypeRestrictions = [
        self::XML_SCHEMA_TYPE_VARCHAR => [
            self::XSD_PREFIX . ':maxLength' => 255
        ]
    ];

    /**
     * @inheritDoc
     */
    protected function getValidationSchema()
    {
        return $this->generateValidationSchema();
    }

    /**
     * @inheritdoc
     */
    public function transformValue($value, $key)
    {
        if (!$value instanceof SimpleXMLIterator) {
            Mage::throwException('value needs to be instance of SimpleXMLIterator.');
        }
        
        $valueAsArray = $this->convertXmlToArray($value);

        /** @var ISAAC_Import_Model_Import_Value_Catalog_Category $importValueCatalogCategory */
        $importValueCatalogCategory = Mage::getModel(
            'isaac_import/import_value_catalog_category',
            $valueAsArray['category_code']
        );

        $importValueCatalogCategory->setLoadingFlags(explode(',', $valueAsArray['flags']));
        $importValueCatalogCategory->setStoreCode($valueAsArray['store_code']);

        if (isset($valueAsArray['parent'])) {
            $importValueCatalogCategory->setParent($valueAsArray['parent']);
            unset($valueAsArray['parent']);
        }

        unset($valueAsArray['flags']);
        unset($valueAsArray['store_code']);
        unset($valueAsArray['category_code']);

        $importValueCatalogCategory->addDataValues($valueAsArray);

        return $importValueCatalogCategory;
    }

    /**
     * @inheritDoc
     */
    public function cleanup()
    {
    }

    /**
     * @return string
     */
    protected function generateValidationSchema()
    {
        $schema = new SimpleXMLElement(sprintf(
            '<%1$s:schema xmlns:%1$s="%2$s" elementFormDefault="qualified"/>',
            self::XSD_PREFIX,
            self::XML_SCHEMA_NAMESPACE
        ));

        $this->addValidationSchemaTypes($schema);

        $categories = $schema->addChild(self::XSD_PREFIX . ':element');
        $categories->addAttribute('name', 'categories');

        $categories = $categories->addChild(self::XSD_PREFIX . ':complexType');
        $categoriesSequence = $categories->addChild(self::XSD_PREFIX . ':sequence');
        $categoriesSequenceCategory = $categoriesSequence->addChild(self::XSD_PREFIX . ':element');
        $categoriesSequenceCategory->addAttribute('name', 'category');
        $categoriesSequenceCategory->addAttribute('maxOccurs', 'unbounded');

        $sequenceComplexType = $categoriesSequenceCategory->addChild(self::XSD_PREFIX . ':complexType');
        $sequenceComplexTypeAll = $sequenceComplexType->addChild(self::XSD_PREFIX . ':all');

        foreach ($this->getRequiredValidationTypesByAttributeCode() as $attributeName => $attributeType) {
            $categoriesSequence = $sequenceComplexTypeAll->addChild(self::XSD_PREFIX . ':element');
            $categoriesSequence->addAttribute('name', $attributeName);
            $categoriesSequence->addAttribute('type', $attributeType);
        }

        foreach ($this->getOptionalValidationTypesByAttributeCode() as $attributeName => $attributeType) {
            $categoriesSequence = $sequenceComplexTypeAll->addChild(self::XSD_PREFIX . ':element');
            $categoriesSequence->addAttribute('name', $attributeName);
            $categoriesSequence->addAttribute('type', $attributeType);
            $categoriesSequence->addAttribute('minOccurs', 0);
            $categoriesSequence->addAttribute('maxOccurs', 1);
        }

        return $schema->asXML();
    }

    /**
     * @return array
     */
    protected function getRequiredValidationTypesByAttributeCode()
    {
        return [
            'flags' => static::XML_SCHEMA_TYPE_VARCHAR,
            'category_code' => static::XML_SCHEMA_TYPE_VARCHAR,
        ];
    }

    /**
     * @return array
     */
    protected function getOptionalValidationTypesByAttributeCode()
    {
        $validationSchemaAttributes = [
            'store_code' => static::XML_SCHEMA_TYPE_VARCHAR,
            'parent' => static::XML_SCHEMA_TYPE_VARCHAR
        ];

        /** @var Mage_Catalog_Model_Resource_Category_Attribute_Collection $categoryAttributeCollection */
        $categoryAttributeCollection = Mage::getResourceModel('catalog/category_attribute_collection');
        $categoryAttributeCollectionSelect = $categoryAttributeCollection->getSelect();
        $categoryAttributeCollectionSelect->reset(Zend_Db_Select::COLUMNS);
        $categoryAttributeCollectionSelect->columns([
            'main_table.attribute_code',
            'main_table.backend_type',
            'main_table.source_model',
        ]);
        $categoryAttributeCollectionSelect->where('main_table.backend_type != ?', 'static');
        $categoryAttributeCollectionSelect->where('main_table.attribute_code NOT IN (?)', [
            'category_code',
            'url_path',
            'all_children',
            'children',
            'children_count',
        ]);

        $attributes = $categoryAttributeCollection->getConnection()->fetchAll($categoryAttributeCollectionSelect);
        foreach ($attributes as $attribute) {
            $validationSchemaAttributes[$attribute['attribute_code']] = $this->getValidationSchemaAttributeType(
                $attribute['backend_type'],
                $attribute['source_model']
            );
        }

        return $validationSchemaAttributes;
    }

    /**
     * @param SimpleXMLElement $root
     */
    protected function addValidationSchemaTypes(SimpleXMLElement $root)
    {
        foreach ($this->schemaTypes as $typeName => $xsdType) {
            $validationSchemaType = $root->addChild(self::XSD_PREFIX . ':simpleType');
            $validationSchemaType->addAttribute('name', $typeName);
            $validationSchemaTypeRestriction = $validationSchemaType->addChild(self::XSD_PREFIX . ':restriction');
            $validationSchemaTypeRestriction->addAttribute('base', $xsdType);

            if (!array_key_exists($typeName, $this->schemaTypeRestrictions)) {
                continue;
            }

            foreach ($this->schemaTypeRestrictions[$typeName] as $restrictionName => $restrictionValue) {
                $restriction = $validationSchemaTypeRestriction->addChild($restrictionName);
                $restriction->addAttribute('value', $restrictionValue);
            }
        }
    }

    /**
     * @param $backendType
     * @param $sourceModel
     * @return string
     * @throws Mage_Core_Exception
     */
    protected function getValidationSchemaAttributeType($backendType, $sourceModel)
    {
        switch ($backendType) {
            case 'varchar':
                $attributeType = self::XML_SCHEMA_TYPE_VARCHAR;
                break;
            case 'text':
                $attributeType = self::XML_SCHEMA_TYPE_TEXT;
                break;
            case 'datetime':
                $attributeType = self::XML_SCHEMA_TYPE_DATETIME;
                break;
            case 'decimal':
                $attributeType = self::XML_SCHEMA_TYPE_DECIMAL;
                break;
            case 'int':
                switch ($sourceModel) {
                    case 'eav/entity_attribute_source_boolean':
                        $attributeType = self::XML_SCHEMA_TYPE_INT;
                        break;
                    default:
                        $attributeType = self::XML_SCHEMA_TYPE_VARCHAR;
                        break;
                }
                break;
            default:
                throw new Mage_Core_Exception(sprintf('Unknown backend type %s', $backendType));
        }

        return $attributeType;
    }
}
