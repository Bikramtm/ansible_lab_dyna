<?php
/**
 * ISAAC ISAAC_Import
 *
 * @category ISAAC
 * @package ISAAC_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */

class ISAAC_Import_Model_Observer
{
    /**
     * @param Varien_Event_Observer $event
     */
    public function widgetWidgetInstanceSaveBefore(Varien_Event_Observer $event)
    {
        /** @var Mage_Widget_Model_Widget_Instance $widgetInstance */
        $widgetInstance = $event->getData('data_object');

        /** @var Mage_Widget_Model_Resource_Widget_Instance $widgetInstanceResource */
        $widgetInstanceResource = $widgetInstance->getResource();

        $identifier = $widgetInstance->getData('identifier');
        $storeIds = $widgetInstance->getData('store_ids');

        /** @var Mage_Widget_Model_Widget_Instance $widgetInstance */
        $readConnection = $widgetInstanceResource->getReadConnection();
        $selectWidgetInstanceQuery = $readConnection->select()
            ->from($widgetInstanceResource->getMainTable())
            ->where('identifier = ?', $identifier)
            ->where('store_ids = ?', $storeIds);

        if (!$widgetInstance->isObjectNew()) {
            $selectWidgetInstanceQuery->where(
                $widgetInstanceResource->getIdFieldName() . ' != ?',
                $widgetInstance->getId()
            );
        }

        $selectWidgetInstanceResult = $readConnection->query($selectWidgetInstanceQuery);
        if ($selectWidgetInstanceResult->rowCount() > 0) {
            /** @var ISAAC_Import_Helper_Data $isaacImportDataHelper */
            $isaacImportDataHelper = Mage::helper('isaac_import/data');
            Mage::throwException($isaacImportDataHelper->__(
                'A widget instance with identifier %s already exists for store(s) %s',
                $identifier,
                implode($this->getStoreNames(explode(', ', $storeIds)))
            ));
        }
    }

    /**
     * @param array $storeIds
     * @return array
     */
    protected function getStoreNames(array $storeIds)
    {
        $storeNames = [];
        /** @var Mage_Core_Model_Store $store */
        foreach (Mage::app()->getStores(true) as $store) {
            if (in_array($store->getId(), $storeIds)) {
                $storeNames[] = $store->getName();
            }
        }

        return $storeNames;
    }
}
