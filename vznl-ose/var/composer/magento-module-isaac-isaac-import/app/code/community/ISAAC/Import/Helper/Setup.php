<?php
/**
 * ISAAC ISAAC_Import
 *
 * @category ISAAC
 * @package ISAAC_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */

abstract class ISAAC_Import_Helper_Setup
{
    public function load($path)
    {
        /** @var Mage_Core_Model_App_Emulation $appEmulation */
        $appEmulation = Mage::getSingleton('core/app_emulation');
        $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation(
            Mage_Core_Model_App::ADMIN_STORE_ID,
            Mage_Core_Model_App_Area::AREA_ADMINHTML
        );

        $configurationData = array(
            'id' => $this->getIdentifier(),
            'generator_class' => $this->getGenerator($this->getGeneratorFormatByFilePath($path)),
            'generator_init_params' => array('importFileName' => $path),
            'loader_class' => $this->getLoader()
        );

        /** @var ISAAC_Import_Model_Configuration $configuration */
        $configuration = Mage::getModel('isaac_import/configuration', $configurationData);

        /** @var ISAAC_Import_Model_Importer $importer */
        $importer = Mage::getModel('isaac_import/importer', $configuration);
        $importer->setReindex(false);
        $importer->setCleanup(false);
        $importer->import();

        $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
    }

    /**
     * @param $filePath
     * @return string
     */
    public function getGeneratorFormatByFilePath($filePath)
    {
        return pathinfo($filePath, PATHINFO_EXTENSION);
    }

    /**
     * @return string
     */
    abstract public function getIdentifier();

    /**
     * @param string $format
     * @return string
     */
    abstract public function getGenerator($format);

    /**
     * @return string
     */
    abstract public function getLoader();
}
