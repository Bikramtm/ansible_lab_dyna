<?php
/**
 * ISAAC ISAAC_Import
 *
 * @category ISAAC
 * @package ISAAC_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */

class ISAAC_Import_Helper_Setup_Cms_Widget extends ISAAC_Import_Helper_Setup
{
    /**
     * @inheritDoc
     */
    public function getIdentifier()
    {
        return 'isaac_import_cms_widget';
    }

    /**
     * @inheritDoc
     */
    public function getGenerator($format)
    {
        $generator = '';
        switch ($format) {
            case 'xml':
                $generator = 'isaac_import/generator_xml_file_cms_widget';
                break;
            default:
                Mage::throwException('Unsupported generator format');
        }

        return $generator;
    }

    /**
     * @inheritDoc
     */
    public function getLoader()
    {
        return 'isaac_import/loader_model_cms_widget_singly';
    }
}
