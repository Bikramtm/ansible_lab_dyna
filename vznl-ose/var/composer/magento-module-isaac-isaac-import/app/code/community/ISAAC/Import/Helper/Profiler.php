<?php
/**
 * ISAAC ISAAC_Import
 *
 * @category ISAAC
 * @package ISAAC_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */
class ISAAC_Import_Helper_Profiler extends Mage_Core_Helper_Data
{
    /**
     * @param string $lineSeparator
     * @param string $fieldSeparator
     * @return string
     */
    public function renderAsText($lineSeparator = PHP_EOL, $fieldSeparator = ', ')
    {
        $out  = 'Memory usage: real: ' . memory_get_usage(true) . ', emalloc: ' . memory_get_usage() . $lineSeparator;
        $headerFieldNames = ['Time', 'Cnt', 'Emalloc', 'RealMem', 'Code Profiler'];
        $out .= implode($fieldSeparator, $headerFieldNames) . $lineSeparator;
        foreach (Varien_Profiler::getTimers() as $name => $timer) {
            $sum = Varien_Profiler::fetch($name, 'sum');
            $count = Varien_Profiler::fetch($name, 'count');
            $realmem = Varien_Profiler::fetch($name, 'realmem');
            $emalloc = Varien_Profiler::fetch($name, 'emalloc');
            if ($sum < .0010 && $count < 10 && $emalloc < 10000) {
                continue;
            }
            $values = [number_format($sum, 4), $count, number_format($emalloc), number_format($realmem), $name];
            $out .= implode($fieldSeparator, $values) . $lineSeparator;
        }
        $writeConnection = Mage::getSingleton('core/resource')->getConnection('core_write');
        $out .= print_r(Varien_Profiler::getSqlProfiler($writeConnection), true);

        return $out;
    }

}