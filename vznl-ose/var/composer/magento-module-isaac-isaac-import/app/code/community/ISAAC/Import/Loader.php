<?php
/**
 * ISAAC ISAAC_Import
 *
 * @category ISAAC
 * @package ISAAC_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */
abstract class ISAAC_Import_Loader
{
    /** @var array */
    protected $importValues = array();

    /**
     * @param ISAAC_Import_Model_Import_Value[] $importValues
     */
    public function setImportValues(array $importValues)
    {
        $this->importValues = array();
        foreach ($importValues as $importValue) {
            $this->addImportValue($importValue);
        }
    }

    /**
     * @return ISAAC_Import_Model_Import_Value[]
     */
    public function getImportValues()
    {
        return $this->importValues;
    }

    /**
     * @param ISAAC_Import_Model_Import_Value $importValue
     */
    public function addImportValue(ISAAC_Import_Model_Import_Value $importValue)
    {
        if (!$this->supportsImportValue($importValue)) {
            Mage::throwException(sprintf(
                'loader %s does not support import value %s',
                get_class($this),
                get_class($importValue)
            ));
        }
        $this->importValues[] = $importValue;
    }

    /**
     * @param ISAAC_Import_Model_Import_Value $importValue
     * @return bool
     */
    abstract protected function supportsImportValue(ISAAC_Import_Model_Import_Value $importValue);

    /**
     * @return void
     */
    abstract public function import();
}
