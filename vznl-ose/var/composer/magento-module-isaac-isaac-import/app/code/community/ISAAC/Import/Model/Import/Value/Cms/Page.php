<?php
/**
 * ISAAC ISAAC_Import
 *
 * @category ISAAC
 * @package ISAAC_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */

class ISAAC_Import_Model_Import_Value_Cms_Page extends ISAAC_Import_Model_Import_Value
{
    /** @var string */
    protected $title;

    /** @var string */
    protected $rootTemplate;

    /** @var string */
    protected $contentHeading;

    /** @var string */
    protected $content;

    /** @var string */
    protected $layoutUpdateXml;

    /** @var boolean */
    protected $isActive;

    /** @var string */
    protected $storeCode = Mage_Core_Model_Store::ADMIN_CODE;

    /**
     * @inheritdoc
     */
    public function getEntityName()
    {
        return 'page';
    }

    /**
     * @return string
     */
    public function __toString()
    {
        $result = $this->getIdentifier();
        $title = $this->getTitle();
        if ($title) {
            $result .= ' (' . $title . ')';
        }
        $result .= ' for store ' . $this->getStoreCode();
        return $result;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getRootTemplate()
    {
        return $this->rootTemplate;
    }

    /**
     * @param string $rootTemplate
     */
    public function setRootTemplate($rootTemplate)
    {
        $this->rootTemplate = $rootTemplate;
    }

    /**
     * @return string
     */
    public function getContentHeading()
    {
        return $this->contentHeading;
    }

    /**
     * @param string $contentHeading
     */
    public function setContentHeading($contentHeading)
    {
        $this->contentHeading = $contentHeading;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getLayoutUpdateXml()
    {
        return $this->layoutUpdateXml;
    }

    /**
     * @param string $layoutUpdateXml
     */
    public function setLayoutUpdateXml($layoutUpdateXml)
    {
        $this->layoutUpdateXml = $layoutUpdateXml;
    }

    /**
     * @return string
     */
    public function getStoreCode()
    {
        return $this->storeCode;
    }

    /**
     * @param string $storeCode
     */
    public function setStoreCode($storeCode)
    {
        $this->storeCode = $storeCode;
    }

    /**
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @param boolean $isActive
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    }
}
