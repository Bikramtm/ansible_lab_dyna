<?php
/**
 * ISAAC ISAAC_Import
 *
 * @category ISAAC
 * @package ISAAC_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */

class ISAAC_Import_Model_Loader_Model_Catalog_Product_Collection extends ISAAC_Import_Model_Loader_Model_Collection
{
    /**
     * @var ISAAC_Import_Helper_Import_Catalog_Product
     */
    protected $isaacImportCatalogProductHelper;

    /**
     * @var Mage_Eav_Model_Config
     */
    protected $eavConfig;

    /**
     * @var null|array
     */
    protected $mandatoryAttributeCodes;

    /** @var string[] $mandatoryAttributeCodesForSavingAsKeys */
    protected $mandatoryAttributeCodesForSavingAsKeys = array();

    /**
     * @inheritDoc
     */
    public function __construct()
    {
        parent::__construct();

        $this->isaacImportCatalogProductHelper = Mage::helper('isaac_import/import_catalog_product');
        $this->eavConfig = Mage::getModel('eav/config');
    }

    /**
     * @param ISAAC_Import_Model_Import_Value $importValue
     * @return bool
     */
    protected function supportsImportValue(ISAAC_Import_Model_Import_Value $importValue)
    {
        return $this->isaacImportCatalogProductHelper->supportsImportValue($importValue);
    }

    /**
     * @inheritDoc
     */
    protected function getNewModelByImportValue(ISAAC_Import_Model_Import_Value $importValue)
    {
        /** @var ISAAC_Import_Model_Import_Value_Catalog_Product $importValue */
        $storeId = $this->getStoreIdByStoreCode($importValue->getStoreCode());
        if ($storeId !== Mage_Core_Model_App::ADMIN_STORE_ID) {
            Mage::throwException(sprintf(
                'product %s cannot be created as this is not an admin import',
                $importValue->__toString()
            ));
        }

        /** @var ISAAC_Import_Model_Import_Value_Catalog_Product $importValue */
        /** @var Mage_Catalog_Model_Product $product */
        $product = Mage::getModel('catalog/product');
        $product->setData('store_id', $storeId);
        if ($importValue->hasDataValue('_attribute_set')) {
            $attributeSetId = $this->isaacImportCatalogProductHelper->getProductAttributeSetIdByName(
                $importValue->getDataValue('_attribute_set')
            );
            $product->setData('attribute_set_id', $attributeSetId);
        } else {
            $product->setData('attribute_set_id', $product->getDefaultAttributeSetId());
        }
        $product->setData('sku', $importValue->getIdentifier());
        if ($importValue->hasDataValue('_entity_type')) {
            $product->setData('type_id', $importValue->getDataValue('_entity_type'));
        } else {
            $product->setData('type_id', Mage_Catalog_Model_Product_Type::TYPE_SIMPLE);
        }

        return $product;
    }

    /**
     * @inheritDoc
     */
    protected function getModelValuesToUpdate(
        Mage_Core_Model_Abstract $model,
        ISAAC_Import_Model_Import_Value $importValue
    ) {
        return $this->isaacImportCatalogProductHelper->getModelValuesToUpdate($model, $importValue);
    }

    /**
     * @inheritDoc
     */
    protected function unsetAllowed(Mage_Core_Model_Abstract $model, $key)
    {
        if (empty($this->mandatoryAttributeCodesForSavingAsKeys)) {
            $attributeCodesAsKeys = [];
            foreach ($this->getCatalogProductTableColumnNames() as $columnName) {
                $attributeCodesAsKeys[$columnName] = 1;
            }
            foreach ($this->getMandatoryAttributeCodes() as $mandatoryAttributeCode) {
                $attributeCodesAsKeys[$mandatoryAttributeCode] = 1;
            }
            $this->mandatoryAttributeCodesForSavingAsKeys = $attributeCodesAsKeys;
        }
        return !isset($this->mandatoryAttributeCodesForSavingAsKeys[$key]);
    }

    /**
     * @inheritDoc
     */
    protected function beforeSave(
        Mage_Core_Model_Abstract $model,
        ISAAC_Import_Model_Import_Value $importValue,
        $isObjectNew
    ) {
        /** @var Mage_Catalog_Model_Product $model */
        $model->validate();
    }

    /**
     * @inheritDoc
     */
    protected function afterSave(
        Mage_Core_Model_Abstract $model,
        ISAAC_Import_Model_Import_Value $importValue,
        $isObjectNew
    ) {
        return $this->isaacImportCatalogProductHelper->afterSave($model, $importValue, $isObjectNew);
    }

    /**
     * @inheritDoc
     */
    public function getGroupValueByImportValue(ISAAC_Import_Model_Import_Value $importValue)
    {
        /** @var ISAAC_Import_Model_Import_Value_Catalog_Product $importValue  */
        return $importValue->getStoreCode();
    }

    /**
     * @inheritDoc
     */
    public function getCollectionByGroupValueAndImportValues($groupValue, array $importValues)
    {
        $hasStockItems = false;
        $hasProductOptions = false;
        $skusAsKeysForCurrentStore = [];
        $attributeCodesToSelectAsKeys = [];

        /** @var ISAAC_Import_Model_Import_Value_Catalog_Product $importValue */
        foreach ($this->getImportValues() as $importValue) {
            $skusAsKeysForCurrentStore[$importValue->getIdentifier()] = 1;
            if (!$hasStockItems && $importValue->hasDataValue('_stock_item')) {
                $hasStockItems = true;
            }
            if (!$hasProductOptions && $importValue->hasDataValue('_product_options')) {
                $hasProductOptions = true;
            }
            foreach ($this->getAttributeCodesToSelect($importValue) as $attributeCode) {
                if (!isset($attributeCodesToSelectAsKeys[$attributeCode])) {
                    $attributeCodesToSelectAsKeys[$attributeCode] = 1;
                }
            }
        }

        $storeId = $this->getStoreIdByStoreCode($groupValue);

        /** @var Mage_Catalog_Model_Resource_Product_Collection $productCollection */
        $productCollection = Mage::getResourceModel('catalog/product_collection');
        $productCollection->setStore($storeId);
        $productCollection->addStoreFilter($storeId);
        $productCollection->addAttributeToFilter('sku', array('in' => array_keys($skusAsKeysForCurrentStore)));
        $productCollection->addAttributeToSelect('description');
        $productCollection->addAttributeToSelect(array_keys($attributeCodesToSelectAsKeys));
        if ($hasStockItems) {
            $productCollection->setFlag('require_stock_items', true);
        }
        $productCollection->addWebsiteNamesToResult();
        if ($hasProductOptions) {
            $productCollection->addOptionsToResult();
        }

        $products = [];
        foreach ($productCollection as $product) {
            /** @var Mage_Catalog_Model_Product $product */
            $createdAtAttribute = $this->eavConfig->getAttribute(Mage_Catalog_Model_Product::ENTITY, 'created_at');
            $createdAtAttribute->getBackend()->afterLoad($product);

            $product->setData('store_id', $storeId);
            $product->setData('website_ids', $product->getData('websites'));
            $products[$product->getSku()] = $product;
        }

        return $products;
    }

    /**
     * @param $storeCode
     * @return int
     */
    protected function getStoreIdByStoreCode($storeCode)
    {
        return (int) Mage::app()->getStore($storeCode)->getId();
    }

    /**
     * @param ISAAC_Import_Model_Import_Value_Catalog_Product $importValue
     * @return string[]
     */
    protected function getAttributeCodesToSelect(ISAAC_Import_Model_Import_Value_Catalog_Product $importValue)
    {
        $attributeCodesToSelect = $this->getMandatoryAttributeCodes();
        foreach ($importValue->getDataValues() as $attributeCode => $importAttributeValue) {
            if (!$this->importHelper->startsWith($attributeCode, '_')) {
                $attributeCodesToSelect[] = $attributeCode;
            } elseif ($attributeCode == '_specifications') {
                $attributeCodesToSelect[] = 'specifications';
            } elseif ($attributeCode == '_attachments') {
                foreach ($this->getAttachmentImportValuesAttributeCodes($importAttributeValue) as
                         $attachmentAttributeCode) {
                    $attributeCodesToSelect[] = $attachmentAttributeCode;
                }
            } elseif ($attributeCode == '_images') {
                $mediaAttributeCodes = $this->isaacImportCatalogProductHelper
                    ->getImageImportValuesMediaAttributeCodes($importAttributeValue);
                foreach ($mediaAttributeCodes as $mediaAttributeCode) {
                    $attributeCodesToSelect[] = $mediaAttributeCode;
                }
            }
        }
        return $attributeCodesToSelect;
    }

    /**
     * @param array $attachmentImportValues
     * @return string[]
     */
    protected function getAttachmentImportValuesAttributeCodes(array $attachmentImportValues)
    {
        return array_keys($attachmentImportValues);
    }

    /**
     * @return string[]
     */
    protected function getCatalogProductTableColumnNames()
    {
        /** @var Mage_Core_Model_Resource $coreResource */
        $coreResource = Mage::getSingleton('core/resource');
        $tableName = $coreResource->getTableName('catalog/product');
        return array_keys($coreResource->getConnection('core_read')->describeTable($tableName));
    }

    /**
     * Mandatory attribute codes for loading and saving (to avoid resetting of the attributes to their default values)
     * @see http://stackoverflow.com/a/5721037/1010649
     * @return string[]
     */
    public function getMandatoryAttributeCodes()
    {
        if ($this->mandatoryAttributeCodes === null) {
            $attributeCodes = $this->getAttributeCodesWithDefaultValueAndDefaultBackendModel();
            $attributeCodes[] = 'url_key';
            $attributeCodes[] = 'store_id';

            $this->mandatoryAttributeCodes = $attributeCodes;
        }

        return $this->mandatoryAttributeCodes;
    }

    /**
     * @return array
     */
    protected function getAttributeCodesWithDefaultValueAndDefaultBackendModel()
    {
        $entityType = $this->eavConfig->getEntityType(Mage_Catalog_Model_Product::ENTITY);

        /** @var Mage_Eav_Model_Resource_Entity_Attribute_Collection $attributeCollection */
        $attributeCollection = Mage::getResourceModel('eav/entity_attribute_collection');
        $attributeCollection->addFieldToSelect('attribute_code');
        $attributeCollection->addFieldToFilter('entity_type_id', $entityType->getEntityTypeId());
        $attributeCollection->getSelect()
            ->where('default_value IS NOT NULL AND default_value != "0"')
            ->where("backend_model IN ('', ?) OR backend_model IS NULL", Mage_Eav_Model_Entity::DEFAULT_BACKEND_MODEL);

        return $attributeCollection->getConnection()->fetchCol($attributeCollection->getSelect());
    }

}
