<?php
/**
 * ISAAC ISAAC_Import
 *
 * @category ISAAC
 * @package ISAAC_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */

Mage::log('Running install file: ' . __FILE__, null, 'install.log', true);

/** @var Mage_Catalog_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();

$entityTypeId     = $installer->getEntityTypeId('catalog_category');
$attributeSetId   = $installer->getDefaultAttributeSetId($entityTypeId);


$installer->updateAttribute('catalog_category', 'category_code', 'unique', true);


$installer->getConnection()->resetDdlCache();

$installer->endSetup();

Mage::log('Finished install file: ' . __FILE__, null, 'install.log', true);
