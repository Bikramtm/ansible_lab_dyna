<?php
/**
 * ISAAC ISAAC_Import
 *
 * @category ISAAC
 * @package ISAAC_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */

class ISAAC_Import_Model_Import_Value_Catalog_Category extends ISAAC_Import_Model_Import_Value
{
    /** @var string */
    protected $storeCode = Mage_Core_Model_Store::ADMIN_CODE;

    /** @var string */
    protected $parent;

    /** @var array */
    protected $dataValues = array();

    /** @var ISAAC_Import_Helper_Property_Category $propertyCategoryHelper */
    protected $propertyCategoryHelper;

    /**
     * @inheritdoc
     */
    public function __construct($identifier)
    {
        parent::__construct($identifier);
        $this->propertyCategoryHelper = Mage::helper('isaac_import/property_category');
    }

    /**
     * @inheritdoc
     */
    public function getEntityName()
    {
        return 'category';
    }

    /**
     * @return string
     */
    public function __toString()
    {
        $result = $this->getIdentifier();
        $name = $this->getDataValue('name');
        if ($name) {
            $result .= ' (' . $name . ')';
        }
        $result .= ' for store ' . $this->getStoreCode();
        return $result;
    }

    /**
     * @return string
     */
    public function getStoreCode()
    {
        return $this->storeCode;
    }

    /**
     * @param string $storeCode
     */
    public function setStoreCode($storeCode)
    {
        $this->storeCode = $storeCode;
    }

    /**
     * @return string
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param string $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    }

    /**
     * @return array
     */
    public function getDataValues()
    {
        return $this->dataValues;
    }

    /**
     * @param string $key
     * @return bool
     */
    public function hasDataValue($key)
    {
        return array_key_exists($key, $this->dataValues);
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function getDataValue($key)
    {
        if (array_key_exists($key, $this->dataValues)) {
            return $this->dataValues[$key];
        }
        return null;
    }

    /**
     * @param array $values
     * @return $this
     */
    public function addDataValues(array $values)
    {
        foreach ($values as $key => $value) {
            $this->addDataValue($key, $value);
        }
        return $this;
    }

    /**
     * @param string $key
     * @param mixed $value
     * @return $this
     */
    public function addDataValue($key, $value)
    {
        $this->validateDataValue($key, $value);
        $this->dataValues[$key] = $value;
        return $this;
    }

    /**
     * @param string $key
     * @param mixed $value
     * @throws Exception
     * @return $this
     */
    public function validateDataValue($key, $value) {
        try {
            $property = $this->propertyCategoryHelper->getPropertyByAttributeCode($key);
            if ($property) {
                $property->validateValue($value);
            }
        } catch (Exception $e) {
            Mage::throwException(sprintf(
                'import value %s has invalid field %s (%s)',
                $this->__toString(),
                $key,
                $e->getMessage()
            ));
        }
        return $this;
    }


    /**
     * @return string
     */
    public function getName()
    {
        return $this->getDataValue('name');
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->addDataValue('name', $name);
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->getDataValue('description');
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->addDataValue('description', $description);
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->getDataValue('image');
    }

    /**
     * @param string $image
     */
    public function setImage($image)
    {
        $this->addDataValue('image', $image);
    }

    /**
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->getDataValue('is_active');
    }

    /**
     * @param boolean $isActive
     */
    public function setIsActive($isActive)
    {
        $this->addDataValue('is_active', $isActive);
    }

    /**
     * @return boolean
     */
    public function getIsAnchor()
    {
        return $this->getDataValue('is_anchor');
    }

    /**
     * @param boolean $isAnchor
     */
    public function setIsAnchor($isAnchor)
    {
        $this->addDataValue('is_anchor', $isAnchor);
    }

    /**
     * @return boolean
     */
    public function getIncludeInMenu()
    {
        return $this->getDataValue('include_in_menu');
    }

    /**
     * @param boolean $includeInMenu
     */
    public function setIncludeInMenu($includeInMenu)
    {
        $this->addDataValue('include_in_menu', $includeInMenu);
    }

    /**
     * @return string
     */
    public function getPageLayout()
    {
        return $this->getDataValue('page_layout');
    }

    /**
     * @param string $pageLayout
     */
    public function setPageLayout($pageLayout)
    {
        $this->addDataValue('page_layout', $pageLayout);
    }

    /**
     * @return string
     */
    public function getDisplayMode()
    {
        return $this->getDataValue('display_mode');
    }

    /**
     * @param string $displayMode
     */
    public function setDisplayMode($displayMode)
    {
        $this->addDataValue('display_mode', $displayMode);
    }

    /**
     * @return string
     */
    public function getCustomLayoutUpdate()
    {
        return $this->getDataValue('custom_layout_update');
    }

    /**
     * @param string $customLayoutUpdate
     */
    public function setCustomLayoutUpdate($customLayoutUpdate)
    {
        $this->addDataValue('custom_layout_update', $customLayoutUpdate);
    }

    /**
     * @return boolean
     */
    public function getApplyToProducts()
    {
        return $this->getDataValue('apply_to_products');
    }

    /**
     * @param boolean $applyToProducts
     */
    public function setApplyToProducts($applyToProducts)
    {
        $this->addDataValue('apply_to_products', $applyToProducts);
    }

    /**
     * @return boolean
     */
    public function getAssignProductsAutomatically()
    {
        return $this->getDataValue('assign_products_automatically');
    }

    /**
     * @param boolean $assignProductsAutomatically
     */
    public function setAssignProductsAutomatically($assignProductsAutomatically)
    {
        $this->addDataValue('assign_products_automatically', $assignProductsAutomatically);
    }

    /**
     * @return boolean
     */
    public function getCustomUseParentSettings()
    {
        return $this->getDataValue('custom_use_parent_settings');
    }

    /**
     * @param boolean $customUseParentSettings
     */
    public function setCustomUseParentSettings($customUseParentSettings)
    {
        $this->addDataValue('custom_use_parent_settings', $customUseParentSettings);
    }

    /**
     * @return string
     */
    public function getUrlKey()
    {
        return $this->getDataValue('url_key');
    }

    /**
     * @param string $urlKey
     */
    public function setUrlKey($urlKey)
    {
        $this->addDataValue('url_key', $urlKey);
    }
}
