<?php
/**
 * ISAAC ISAAC_Import
 *
 * @category ISAAC
 * @package ISAAC_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */

class ISAAC_Import_Helper_Import_Attribute_Option extends Mage_Core_Helper_Abstract
{
    /** @var ISAAC_Import_Helper_Data */
    protected $importHelperData;

    /** @var Mage_Eav_Model_Config */
    protected $eavConfig;

    /**
     * ISAAC_Import_Helper_Catalog constructor.
     */
    public function __construct()
    {
        $this->importHelperData = Mage::helper('isaac_import/data');
        $this->eavConfig = Mage::getSingleton('eav/config');
    }

    /**
     * @param mixed $entityType
     * @param string $attributeCode
     * @param int $attributeOptionId
     * @param string $attributeOptionValue
     * @param int $storeId
     * @return bool
     * @throws Exception
     * @throws Mage_Core_Exception
     */
    public function updateOptionStoreValues(
        $entityType,
        $attributeCode,
        $attributeOptionId,
        $attributeOptionValue,
        $storeId
    ) {
        if (empty($attributeOptionValue)) {
            $this->importHelperData->logMessage(sprintf(
                'not extending option with attribute code "%s" with new import value: import value is empty',
                $attributeCode
            ));
            return false;
        }
        $optionStoreValues = $this->getOptionStoreValues($attributeOptionId);
        if ($optionStoreValues) {
            if (!array_key_exists($storeId, $optionStoreValues) ||
                $optionStoreValues[$storeId] != $attributeOptionValue
            ) {
                $optionStoreValues[$storeId] = $attributeOptionValue;
                /** @var Mage_Catalog_Model_Resource_Eav_Attribute $attribute */
                $attribute = Mage::getModel('catalog/resource_eav_attribute');
                $attribute->loadByCode($entityType, $attributeCode);
                $currentData = $attribute->getData();
                $currentData['option']['value'][$attributeOptionId] = $optionStoreValues;
                $attribute->addData($currentData);
                $attribute->save();

                $this->importHelperData->logMessage(sprintf(
                    'updated %s attribute option for attribute "%s" and default option value "%s"' .
                    ' (option id "%s"): set option value "%s" for store %s',
                    $entityType,
                    $attributeCode,
                    $optionStoreValues[Mage_Core_Model_App::ADMIN_STORE_ID],
                    $attributeOptionId,
                    $optionStoreValues[$storeId],
                    $storeId
                ));
            }
        }
        return false;
    }


    /**
     * @param int $attributeOptionId
     * @return string[]
     */
    public function getOptionStoreValues($attributeOptionId)
    {
        $optionStoreValues = array();
        $optionDefaultValue = $this->getSingleOptionStoreValue($attributeOptionId, Mage_Core_Model_App::ADMIN_STORE_ID);
        if ($optionDefaultValue) {
            $optionStoreValues[Mage_Core_Model_App::ADMIN_STORE_ID] = $optionDefaultValue;
            foreach (Mage::app()->getStores(false) as $store) {
                /** @var Mage_Core_Model_Store $store */
                if ($optionValue = $this->getSingleOptionStoreValue($attributeOptionId, $store->getId())) {
                    $optionStoreValues[$store->getId()] = $optionValue;
                }
            }
        }
        return $optionStoreValues;
    }

    /**
     * @param int $attributeOptionId
     * @param int $storeId
     * @return string
     */
    public function getSingleOptionStoreValue($attributeOptionId, $storeId)
    {
        $optionCollection = Mage::getResourceModel('eav/entity_attribute_option_collection')
            ->setStoreFilter($storeId, false)
            ->addFieldToFilter('main_table.option_id', $attributeOptionId);
        foreach ($optionCollection as $option) {
            /** @var Varien_Object $option */
            return $option->getData('value');
        }
        return '';
    }

    /**
     * Look up a dropdown attribute's option id, and create the attribute option when it does not exist
     *
     * @param mixed $entityType
     * @param string $attributeCode
     * @param string $attributeOptionLabel
     * @param int|null $storeId
     * @return int
     * @throws Mage_Core_Exception
     */
    public function getDropdownAttributeOptionId(
        $entityType,
        $attributeCode,
        $attributeOptionLabel,
        $storeId = null
    ) {
        Varien_Profiler::start(__METHOD__);
        //check input parameters
        assert(preg_match('/^[a-z][a-z0-9_]*$/', $attributeCode));

        //handle case of an empty attributeOptionLabel
        if ($attributeOptionLabel == '') {
            Varien_Profiler::stop(__METHOD__);
            return null;
        }

        //find option id of option label $attributeOptionLabel
        $attributeOptionId = $this->findOptionId($entityType, $attributeOptionLabel, $attributeCode, $storeId);
        if ($attributeOptionId === false) {
            //option label does not yet exist; create it
            $this->createDropdownAttributeOption($entityType, $attributeCode, $attributeOptionLabel);
            //find id of newly created option
            $attributeOptionId = $this->findOptionId($entityType, $attributeOptionLabel, $attributeCode, $storeId);
            if ($attributeOptionId === false) {
                throw new Mage_Core_Exception(sprintf(
                    'could not obtain id of newly created %s attribute "%s" option "%s"',
                    $this->eavConfig->getEntityType($entityType)->getEntityTypeCode(),
                    $attributeCode,
                    $attributeOptionLabel
                ));
            }
        }
        assert($attributeOptionId !== false);
        Varien_Profiler::stop(__METHOD__);

        return $attributeOptionId;
    }

    /**
     * Find attribute option id by store label (textual value)
     *
     * @param string $optionLabel an option label
     * @param string $entityType a valid entity type
     * @param string $attributeCode a valid attribute code
     * @param int $storeId a valid store id (or null for the admin store)
     * @return int|false
     */
    public function findOptionId($entityType, $optionLabel, $attributeCode, $storeId = null)
    {
        //check input parameters
        assert(!is_null($optionLabel));

        //look up attribute id in the database
        /** @var Mage_Eav_Model_Entity_Attribute $attribute */
        $attribute = $this->eavConfig->getAttribute($entityType, $attributeCode);
        if (!$attribute || !$attribute->getId()) {
            Mage::throwException('attribute with code ' . $attributeCode . ' does not exist');
        }

        if (($attribute->getData('frontend_input') !== 'select') &&
            ($attribute->getData('frontend_input') !== 'multiselect')
        ) {
            Mage::throwException('attribute with code ' . $attributeCode . ' is not a select or multiselect');
        }

        //retrieve all options (do not retrieve all options for dropdown attributes)
        if ($attribute->getData('source_model') == '' ||
            $attribute->getData('source_model') == 'eav/entity_attribute_source_table'
        ) {
            /** @var Mage_Eav_Model_Resource_Entity_Attribute_Option_Collection $collection */
            $collection = Mage::getResourceModel('eav/entity_attribute_option_collection')
                ->setPositionOrder('asc')
                ->setAttributeFilter($attribute->getId())
                ->setStoreFilter($storeId);
            //Up to Magento 1.5.1, the column name should be 'store_default_value' instead of 'tsv'
            $collection->getSelect()->where('tsv.value = ? OR tdv.value = ?', $optionLabel);
            $collection->load();
            $options = $collection->toOptionArray();
        } else {
            $options = $attribute->getSource()->getAllOptions();
        }

        // look up option id in the array
        foreach ($options as $item) {
            if ($item['label'] == $optionLabel) {
                return $item['value'];
            }
        }
        return false;
    }

    /**
     * Create a database entry for a dropdown attribute option
     *
     * @param mixed $entityType a valid entity type
     * @param string $attributeCode an attribute code
     * @param string $value an option value
     * @throws Mage_Core_Exception
     */
    public function createDropdownAttributeOption($entityType, $attributeCode, $value)
    {
        //check input parameters
        assert(!is_null($attributeCode));
        assert(!is_null($value));

        //create setup object
        $setup = new Mage_Eav_Model_Entity_Setup('core_setup');

        $attribute = $this->eavConfig->getAttribute($entityType, $attributeCode);
        if (($attribute->getData('source_model') != '') &&
            ($attribute->getData('source_model') !== 'eav/entity_attribute_source_table')
        ) {
            throw new Mage_Core_Exception(sprintf(
                'not allowed to add option "%s" to attribute "%s" with source model "%s"',
                $value,
                $attributeCode,
                $attribute->getData('source_model')
            ));
        }

        //create option data structure
        $option['attribute_id'] = $attribute->getId();
        $option['value'][0][Mage_Core_Model_App::ADMIN_STORE_ID] = $value;

        //add option to database
        $setup->addAttributeOption($option);

        //log result
        $this->importHelperData->logMessage(sprintf(
            'extended %s attribute "%s" with option "%s"',
            $this->eavConfig->getEntityType($entityType)->getEntityTypeCode(),
            $attributeCode,
            $value
        ));
    }
}
