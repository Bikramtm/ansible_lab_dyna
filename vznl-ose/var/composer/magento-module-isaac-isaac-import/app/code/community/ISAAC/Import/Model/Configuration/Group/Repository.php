<?php
/**
 * ISAAC ISAAC_Import
 *
 * @category ISAAC
 * @package ISAAC_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */
class ISAAC_Import_Model_Configuration_Group_Repository
{
    /**
     * @return ISAAC_Import_Model_Configuration_Group[]
     */
    public function findAll()
    {
        $configurationGroupObjects = [];
        $configurationGroupRoot = Mage::getConfig()->getNode('default/isaac_import/group');
        if (!$configurationGroupRoot) {
            return $configurationGroupObjects;
        }
        foreach ($configurationGroupRoot->children() as $configurationGroupId => $configurationGroupNode) {
            /** @var Mage_Core_Model_Config_Element $configurationGroupNode */
            $configurationGroupData = $configurationGroupNode->asArray();
            $configurationGroupData['id'] = $configurationGroupId;
            $configurationGroupObjects[$configurationGroupId] =
                Mage::getModel('isaac_import/configuration_group', $configurationGroupData);
        }
        return $configurationGroupObjects;
    }

    /**
     * @param $configurationGroupId
     * @return ISAAC_Import_Model_Configuration_Group
     * @throws Mage_Core_Exception
     */
    public function findById($configurationGroupId)
    {
        foreach ($this->findAll() as $configurationGroup) {
            if ($configurationGroup->getId() === $configurationGroupId) {
                return $configurationGroup;
            }
        }

        throw new Mage_Core_Exception(sprintf('Could not find import group by id: %s', $configurationGroupId));
    }
}