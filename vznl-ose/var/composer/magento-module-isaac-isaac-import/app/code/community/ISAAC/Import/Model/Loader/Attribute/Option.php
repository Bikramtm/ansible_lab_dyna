<?php
/**
 * ISAAC ISAAC_Import
 *
 * @category ISAAC
 * @package ISAAC_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */

class ISAAC_Import_Model_Loader_Attribute_Option extends ISAAC_Import_Loader
{
    /**
     * @var Mage_Eav_Model_Config
     */
    protected $eavConfig;

    /**
     * @var Mage_Eav_Model_Entity_Setup
     */
    protected $eavEntitySetup;

    /**
     * @var ISAAC_Import_Helper_Import_Attribute_Option
     */
    protected $isaacImportAttributeOptionHelper;

    /**
     * ISAAC_Import_Model_Loader_Attribute_Option constructor.
     */
    public function __construct()
    {
        $this->eavConfig = Mage::getModel('eav/config');
        $this->eavEntitySetup = new Mage_Eav_Model_Entity_Setup('core_setup');
        $this->isaacImportAttributeOptionHelper = Mage::helper('isaac_import/import_attribute_option');
    }

    /**
     * @inheritDoc
     */
    protected function supportsImportValue(ISAAC_Import_Model_Import_Value $importValue)
    {
        return $importValue instanceof ISAAC_Import_Model_Import_Value_Attribute_Option;
    }

    /**
     * @inheritDoc
     */
    public function import()
    {
        /** @var ISAAC_Import_Model_Import_Value_Attribute_Option $importValue */
        foreach ($this->getImportValues() as $importValue) {
            if ($importValue->isCreate() || $importValue->isUpdate()) {
                $this->createOrUpdateAttributeOption($importValue);
            } elseif ($importValue->isDelete()) {
                $this->deleteAttributeOption($importValue);
            }
        }
    }

    /**
     * @param ISAAC_Import_Model_Import_Value_Attribute_Option $importValue
     * @throws Mage_Core_Exception
     */
    protected function createOrUpdateAttributeOption(ISAAC_Import_Model_Import_Value_Attribute_Option $importValue)
    {
        $attribute = $this->eavConfig->getAttribute(
            $importValue->getEntityType(),
            $importValue->getAttributeCode()
        );

        if (!$attribute->getId()) {
            throw new Mage_Core_Exception(sprintf(
                'Unknown %s attribute "%s"',
                $importValue->getEntityType(),
                $importValue->getAttributeCode()
            ));
        }

        $optionId = $this->isaacImportAttributeOptionHelper->findOptionId(
            $importValue->getEntityType(),
            $importValue->getIdentifier(),
            $attribute->getAttributeCode()
        );

        if ($optionId === false) {
            $this->isaacImportAttributeOptionHelper->createDropdownAttributeOption(
                $importValue->getEntityType(),
                $attribute->getAttributeCode(),
                $importValue->getIdentifier()
            );
            $optionId = $this->isaacImportAttributeOptionHelper->findOptionId(
                $importValue->getEntityType(),
                $importValue->getIdentifier(),
                $attribute->getAttributeCode()
            );
            if ($optionId === false) {
                throw new Mage_Core_Exception(sprintf(
                    'could not obtain id of newly created attribute option "%s"',
                    $importValue->getIdentifier()
                ));
            }
        }

        $this->eavEntitySetup->addAttributeOption([
            'attribute_id' => $attribute->getId(),
            'value' => [$optionId => $this->getAttributeOptionStoreLabelsById($importValue)]
        ]);
    }

    /**
     * @param ISAAC_Import_Model_Import_Value_Attribute_Option $importValue
     */
    protected function deleteAttributeOption(ISAAC_Import_Model_Import_Value_Attribute_Option $importValue)
    {
        $attribute = $this->eavConfig->getAttribute(
            Mage_Catalog_Model_Product::ENTITY,
            $importValue->getAttributeCode()
        );

        $optionId = $this->isaacImportAttributeOptionHelper->findOptionId(
            $importValue->getEntityType(),
            $importValue->getIdentifier(),
            $attribute->getAttributeCode()
        );

        if ($optionId !== false) {
            $this->eavEntitySetup->addAttributeOption([
                'delete' => [$optionId => true],
                'value' => [$optionId => []]
            ]);
        }
    }

    /**
     * @param ISAAC_Import_Model_Import_Value_Attribute_Option $importValue
     * @return array
     */
    protected function getAttributeOptionStoreLabelsById(ISAAC_Import_Model_Import_Value_Attribute_Option $importValue)
    {
        $storeLabelsById = [
            Mage_Core_Model_App::ADMIN_STORE_ID => $importValue->getIdentifier()
        ];

        foreach ($importValue->getStoreLabels() as $storeCode => $label) {
            $storeLabelsById[Mage::app()->getStore($storeCode)->getId()] = $label;
        }

        return $storeLabelsById;
    }
}
