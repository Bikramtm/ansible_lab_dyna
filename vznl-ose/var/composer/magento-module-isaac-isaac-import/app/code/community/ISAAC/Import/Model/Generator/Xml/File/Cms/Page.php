<?php
/**
 * ISAAC ISAAC_Import
 *
 * @category ISAAC
 * @package ISAAC_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (http://www.isaac.nl)
 
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */
 
class ISAAC_Import_Model_Generator_Xml_File_Cms_Page extends ISAAC_Import_Model_Generator_Xml_File
{
    /**
     * @return string
     */
    protected function getValidationSchema()
    {
        return <<<XML
<?xml version="1.0" encoding="utf-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified">
    <xs:element name="pages">
        <xs:complexType>
            <xs:sequence>
                <xs:element maxOccurs="unbounded" ref="page"/>
            </xs:sequence>
        </xs:complexType>
    </xs:element>
    <xs:element name="page">
        <xs:complexType>
            <xs:all>
                <xs:element ref="flags"/>
                <xs:element ref="identifier"/>
                <xs:element ref="store_code"/>
                <xs:element ref="title" minOccurs="0" maxOccurs="1"/>
                <xs:element ref="root_template" minOccurs="0" maxOccurs="1"/>
                <xs:element ref="content_heading" minOccurs="0" maxOccurs="1"/>
                <xs:element ref="content" minOccurs="0" maxOccurs="1"/>
                <xs:element ref="layout_update_xml" minOccurs="0" maxOccurs="1"/>
                <xs:element ref="is_active" minOccurs="0" maxOccurs="1"/>
                <xs:element ref="sort_order" minOccurs="0" maxOccurs="1"/>
                <xs:element ref="custom_theme" minOccurs="0" maxOccurs="1"/>
                <xs:element ref="custom_root_template" minOccurs="0" maxOccurs="1"/>
                <xs:element ref="custom_layout_update_xml" minOccurs="0" maxOccurs="1"/>
                <xs:element ref="custom_theme_from" minOccurs="0" maxOccurs="1"/>
                <xs:element ref="custom_theme_to" minOccurs="0" maxOccurs="1"/>
                <xs:element ref="meta_keywords" minOccurs="0" maxOccurs="1"/>
                <xs:element ref="meta_description" minOccurs="0" maxOccurs="1"/>
            </xs:all>
        </xs:complexType>
    </xs:element>
    <xs:element name="flags" type="xs:string"/>
    <xs:element name="identifier" type="xs:NCName"/>
    <xs:element name="store_code" type="xs:NCName"/>
    <xs:element name="title" type="xs:string"/>
    <xs:element name="root_template" type="xs:NCName"/>
    <xs:element name="content_heading" type="xs:string"/>
    <xs:element name="content" type="xs:string"/>
    <xs:element name="layout_update_xml" type="xs:string"/>
    <xs:element name="is_active" type="xs:string"/>
    <xs:element name="sort_order" type="xs:string"/>
    <xs:element name="custom_theme" type="xs:string"/>
    <xs:element name="custom_root_template" type="xs:string"/>
    <xs:element name="custom_layout_update_xml" type="xs:string"/>
    <xs:element name="custom_theme_from" type="xs:string"/>
    <xs:element name="custom_theme_to" type="xs:string"/>
    <xs:element name="meta_keywords" type="xs:string"/>
    <xs:element name="meta_description" type="xs:string"/>
</xs:schema>
XML;
    }

    /**
     * @inheritdoc
     */
    public function transformValue($value, $key)
    {
        if (!$value instanceof SimpleXMLIterator) {
            Mage::throwException('value needs to be instance of SimpleXMLIterator.');
        }

        /** @var SimpleXMLIterator $value */
        /** @var SimpleXMLIterator $identifier */
        /** @var SimpleXMLIterator $flags */
        /** @var SimpleXMLIterator $storeCode */
        /** @var SimpleXMLIterator $title */
        /** @var SimpleXMLIterator $rootTemplate */
        /** @var SimpleXMLIterator $contentHeading */
        /** @var SimpleXMLIterator $content */
        /** @var SimpleXMLIterator $layoutUpdateXml */
        /** @var SimpleXMLIterator $isActive */

        $identifier         = $value->identifier;
        $flags              = $value->flags;
        $storeCode          = $value->store_code;
        $title              = $value->title;
        $rootTemplate       = $value->root_template;
        $contentHeading     = $value->content_heading;
        $content            = $value->content;
        $layoutUpdateXml    = $value->layout_update_xml;
        $isActive           = $value->is_active;

        /** @var ISAAC_Import_Model_Import_Value_Cms_Page $importValueCmsPage */
        $importValueCmsPage = Mage::getModel('isaac_import/import_value_cms_page', $identifier->__toString());
        $importValueCmsPage->setLoadingFlags(explode(',', $flags->__toString()));
        $importValueCmsPage->setStoreCode($storeCode->__toString());

        if ($title->count()) {
            $importValueCmsPage->setTitle($title->__toString());
        }
        if ($rootTemplate->count()) {
            $importValueCmsPage->setRootTemplate($rootTemplate->__toString());
        }
        if ($contentHeading->count()) {
            $importValueCmsPage->setContentHeading($contentHeading->__toString());
        }
        if ($content->count()) {
            $importValueCmsPage->setContent($content->__toString());
        }
        if ($layoutUpdateXml->count()) {
            $importValueCmsPage->setLayoutUpdateXml($layoutUpdateXml->__toString());
        }
        if ($isActive->count()) {
            $importValueCmsPage->setIsActive($isActive->__toString());
        }

        return $importValueCmsPage;
    }

    /**
     * @inheritdoc
     */
    public function cleanup()
    {
    }
}
