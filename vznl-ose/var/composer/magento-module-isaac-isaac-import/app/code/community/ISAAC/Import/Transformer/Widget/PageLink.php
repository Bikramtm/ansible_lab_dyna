<?php
/**
 * ISAAC ISAAC_Import
 *
 * @category ISAAC
 * @package ISAAC_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */


class ISAAC_Import_Transformer_Widget_PageLink implements ISAAC_Import_TransformerInterface
{
    /**
     * @param array $parameters
     * @return array
     */
    public function apply($parameters)
    {
        $storeId = array_key_exists('store_code', $parameters) ?
            $this->getStoreIdByStoreCode($parameters['store_code']) :
            null;

        $parameters['page_id'] = $this->getPageIdByIdentifier($parameters['page_id'], $storeId);

        return $parameters;
    }

    /**
     * Gets the storeId for the specified $storeCode.
     * @param sting $storeCode
     * @return int
     */
    protected function getStoreIdByStoreCode($storeCode)
    {
        return is_numeric($storeCode) ?
            $storeCode :
            Mage::app()->getStore($storeCode)->getId() ;
    }

    /**
     * @param string $identifier
     * @param null|int $storeId
     * @return int
     */
    protected function getPageIdByIdentifier($identifier, $storeId = null)
    {
        /** @var Mage_Cms_Model_Page $pageModel */
        $pageModel = Mage::getModel('cms/page');
        if (is_null($storeId)) {
            $pageId = $pageModel->load($identifier, 'identifier')->getPageId();
        } else {
            $pageId = $pageModel->getCollection()
                ->addStoreFilter($storeId)
                ->addFieldToFilter('identifier', array('eq' => $identifier))
                ->setPageSize(1)
                ->getFirstItem()
                ->getPageId();
        }
        return $pageId;
    }
}
