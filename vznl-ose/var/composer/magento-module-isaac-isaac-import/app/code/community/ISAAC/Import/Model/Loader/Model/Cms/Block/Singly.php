<?php
/**
 * ISAAC ISAAC_Import
 *
 * @category ISAAC
 * @package ISAAC_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (http://www.isaac.nl)
 
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */

class ISAAC_Import_Model_Loader_Model_Cms_Block_Singly extends ISAAC_Import_Model_Loader_Model_Singly
{
    /**
     * @inheritdoc
     */
    protected function supportsImportValue(ISAAC_Import_Model_Import_Value $importValue)
    {
        return $importValue instanceof ISAAC_Import_Model_Import_Value_Cms_Block;
    }

    /**
     * @inheritdoc
     */
    protected function getModelByImportValue(ISAAC_Import_Model_Import_Value $importValue)
    {
        /** @var ISAAC_Import_Model_Import_Value_Cms_Block $importValue */
        /** @var Mage_Cms_Model_Block $cmsBlockModel */
        $cmsBlockModel = Mage::getModel('cms/block');
        $cmsBlockModel->setData('store_id', $this->getStoreIdByStoreCode($importValue->getStoreCode()));
        $cmsBlockModel->load($importValue->getIdentifier(), 'identifier');

        if (!$cmsBlockModel->getId()) {
            $cmsBlockModel->setData('identifier', $importValue->getIdentifier());
        }

        return $cmsBlockModel;
    }

    /**
     * @inheritDoc
     */
    protected function getModelValuesToUpdate(
        Mage_Core_Model_Abstract $model,
        ISAAC_Import_Model_Import_Value $importValue
    ) {
        /** @var ISAAC_Import_Model_Import_Value_Cms_Block $importValue */
        $importValuesToModelValuesMapping = [
            'title'         => $importValue->getTitle(),
            'content'       => $importValue->getContent(),
            'is_active'     => $importValue->getIsActive(),
        ];

        $modelValuesToUpdate = [];
        foreach ($importValuesToModelValuesMapping as $modelKey => $newValue) {
            if ($newValue !== null && $model->getData($modelKey) !== $newValue) {
                $modelValuesToUpdate[$modelKey] = $newValue;
            }
        }

        $storeIds = array($this->getStoreIdByStoreCode($importValue->getStoreCode()));
        if ($model->getData('store_id') !== $storeIds) {
            $modelValuesToUpdate['stores'] = $storeIds;
        }

        return $modelValuesToUpdate;
    }

    /**
     * @param Mage_Core_Model_Abstract $model
     * @param $key
     * @return bool
     */
    protected function unsetAllowed(Mage_Core_Model_Abstract $model, $key)
    {
        return false;
    }

    /**
     * @param string $storeCode
     * @return int
     */
    protected function getStoreIdByStoreCode($storeCode)
    {
        return Mage::app()->getStore($storeCode)->getId();
    }
}
