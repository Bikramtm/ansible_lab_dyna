<?php
/**
 * ISAAC ISAAC_Import
 *
 * @category ISAAC
 * @package ISAAC_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */
class ISAAC_Import_Model_Import_Value_Catalog_Product extends ISAAC_Import_Model_Import_Value
{

    /** @var string */
    protected $storeCode = Mage_Core_Model_Store::ADMIN_CODE;

    /** @var array */
    protected $dataValues = array();

    /** @var ISAAC_Import_Helper_Property_Product $propertyProductHelper */
    protected $propertyProductHelper;

    /**
     * @inheritdoc
     */
    public function __construct($identifier)
    {
        parent::__construct($identifier);
        $this->propertyProductHelper = Mage::helper('isaac_import/property_product');
    }

    /**
     * @return string
     */
    public function getEntityName()
    {
        return 'product';
    }

    /**
     * @return string
     */
    public function __toString()
    {
        $result = $this->getIdentifier();
        $name = $this->getDataValue('name');
        if ($name) {
            $result .= ' (' . $name . ')';
        }
        $result .= ' for store ' . $this->getStoreCode();
        return $result;
    }

    /**
     * @return string
     */
    public function getStoreCode()
    {
        return $this->storeCode;
    }

    /**
     * @param string $storeCode
     */
    public function setStoreCode($storeCode)
    {
        $this->storeCode = $storeCode;
    }

    /**
     * @return array
     */
    public function getDataValues()
    {
        return $this->dataValues;
    }

    /**
     * @param string $key
     * @return bool
     */
    public function hasDataValue($key)
    {
        return array_key_exists($key, $this->dataValues);
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function getDataValue($key)
    {
        if (array_key_exists($key, $this->dataValues)) {
            return $this->dataValues[$key];
        }
        return null;
    }

    /**
     * @param array $values
     * @return $this
     */
    public function addDataValues(array $values)
    {
        foreach ($values as $key => $value) {
            $this->addDataValue($key, $value);
        }
        return $this;
    }

    /**
     * @param string $key
     * @param mixed $value
     * @return $this
     */
    public function addDataValue($key, $value)
    {
        $this->validateDataValue($key, $value);
        $this->dataValues[$key] = $value;
        return $this;
    }

    /**
     * @param string $key
     * @param mixed $value
     * @throws Exception
     * @return $this
     */
    public function validateDataValue($key, $value) {
        try {
            $property = $this->propertyProductHelper->getPropertyByAttributeCode($key);
            if ($property) {
                $property->validateValue($value);
            }
        } catch (Exception $e) {
            Mage::throwException(sprintf(
                'import value "%s" has invalid field "%s" (%s)',
                $this->__toString(),
                $key,
                $e->getMessage()
            ));
        }
        return $this;
    }

}