<?php
/**
 * ISAAC ISAAC_Import
 *
 * @category ISAAC
 * @package ISAAC_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */
class ISAAC_Import_Model_Importer
{

    /** @var ISAAC_Import_Model_Configuration $configuration */
    protected $configuration;

    /** @var ISAAC_Import_Helper_Data $helper */
    protected $helper;

    /** @var bool */
    protected $reindex = true;

    /** @var bool */
    protected $cleanup = true;

    /** @var bool */
    protected $profile = false;

    /** @var bool */
    protected $indexersDisabled = false;

    /** @var string */
    protected $tempFilePath;

    /** @var string */
    protected $loaderScriptName;

    /**
     * @param ISAAC_Import_Model_Configuration $configuration
     */
    public function __construct(ISAAC_Import_Model_Configuration $configuration)
    {
        $this->configuration = $configuration;
        $this->helper = Mage::helper('isaac_import');
        $tempFolder = Mage::getBaseDir('var') . '/tmp';
        if (!is_dir($tempFolder)) {
            mkdir($tempFolder);
        }
        $this->tempFilePath = tempnam($tempFolder, 'isaac_import_');
        $this->loaderScriptName = Mage::getBaseDir() . '/shell/isaacLoader.php';
    }

    /**
     *
     */
    public function __destruct()
    {
        if (file_exists($this->tempFilePath)) {
            unlink($this->tempFilePath);
        }
    }

    /**
     * @param bool $reindex
     * @return $this
     */
    public function setReindex($reindex)
    {
        $this->reindex = $reindex;
        return $this;
    }

    /**
     * @param bool $cleanup
     * @return $this
     */
    public function setCleanup($cleanup)
    {
        $this->cleanup = $cleanup;
        return $this;
    }

    /**
     * @param bool $profile
     * @return $this
     */
    public function setProfile($profile)
    {
        $this->profile = $profile;
        return $this;
    }

    public function import()
    {
        try {
            Mage::dispatchEvent($this->getEventPrefix() . '_before', ['configuration' => $this->configuration]);
            if ($this->configuration->getActive()) {
                $generator = $this->getGenerator();
                $this->helper->logMessage(sprintf(
                    'started importing values for %s importer',
                    $this->configuration->getId()
                ));
                $batchSize = $this->configuration->getBatchSize();
                if ($batchSize == 0) {
                    $this->loadValues(iterator_to_array($generator->getIterator(), false));
                } else {
                    $chunkGenerator = new ISAAC_Import_Chunking_Iterator($generator->getIterator(), $batchSize);
                    foreach ($chunkGenerator as $chunkKey => $importValues) {
                        $message = sprintf('importing batch %d of size %d', $chunkKey + 1, $batchSize);
                        $this->helper->logMessage('started ' . $message);
                        $this->loadValues($importValues);
                        $this->helper->logMessage('finished ' . $message);
                    }
                }
                $this->helper->logMessage(sprintf(
                    'finished importing values for %s importer',
                    $this->configuration->getId()
                ));
                $this->updateIndexers();
                if ($this->cleanup) {
                    $this->helper->logMessage('started cleanup for ' . $this->configuration->getId() . ' importer');
                    $generator->cleanup();
                    $this->helper->logMessage('finished cleanup for ' . $this->configuration->getId() . ' importer');
                }
            }
            Mage::dispatchEvent($this->getEventPrefix() . '_after', ['configuration' => $this->configuration]);
        } catch (Exception $exception) {
            Mage::dispatchEvent($this->getEventPrefix() . '_error', [
                'configuration' => $this->configuration,
                'exception' => $exception
            ]);
            throw $exception;
        }
    }

    /**
     * @return string
     */
    protected function getEventPrefix()
    {
        return 'isaac_import_' . $this->configuration->getId() . '_import';
    }

    /**
     * @return ISAAC_Import_Generator
     * @throws Mage_Core_Exception
     */
    protected function getGenerator()
    {
        $generatorClass = $this->configuration->getGeneratorClass();
        $generatorInitParams = $this->configuration->getGeneratorInitParams();
        $generator = Mage::getModel($generatorClass, $generatorInitParams);
        if (!$generator) {
            Mage::throwException('could not instantiate generator class "' . $generatorClass . '"');
        }
        if (!$generator instanceof ISAAC_Import_Generator) {
            Mage::throwException('generator class ' . get_class($generator) . ' should be an instance of ISAAC_Import_Generator');
        }
        return $generator;
    }

    /**
     * @param array $importValues
     * @internal param Iterator $batchIterator
     */
    public function loadValues(array $importValues)
    {
        Varien_Profiler::start(__METHOD__);
        if (empty($importValues)) {
            return;
        }
        if ($this->reindex && !$this->indexersDisabled) {
            $indexerCodes = $this->configuration->getIndexers();
            $this->helper->setIndexModeForIndexers($indexerCodes, Mage_Index_Model_Process::MODE_MANUAL);
            $this->indexersDisabled = true;
        }
        $this->executeKeepAliveQuery();
        $this->executeLoader($importValues);
        Varien_Profiler::stop(__METHOD__);
    }

    protected function updateIndexers()
    {
        if ($this->reindex && $this->indexersDisabled) {
            $indexerCodes = $this->configuration->getIndexers();
            $this->helper->setIndexModeForIndexers($indexerCodes, Mage_Index_Model_Process::MODE_REAL_TIME);
            $this->helper->reindexViaExecForIndexers($indexerCodes, false);
            $this->indexersDisabled = false;
        }
    }

    /**
     *
     */
    protected function executeKeepAliveQuery()
    {
        $keepAliveQuery = 'SELECT 1';
        Mage::getSingleton('core/resource')->getConnection('core_read')->fetchAll($keepAliveQuery);
    }

    /**
     * @param array $importValues
     * @throws Mage_Core_Exception
     */
    protected function executeLoader(array $importValues)
    {
        Varien_Profiler::start(__METHOD__);
        if ($this->configuration->getLoaderViaExec()) {
            $result = @file_put_contents($this->tempFilePath, serialize($importValues));
            if ($result === false) {
                Mage::throwException('could not write import values to temp file ' . $this->tempFilePath . ' (' . $php_errormsg . ')');
            }
            $outputLines = [];
            $exitCode = 0;
            exec($this->getLoaderShellCommand(), $outputLines, $exitCode);
            foreach ($outputLines as $outputLine) {
                $this->helper->logMessage($outputLine);
            }
            if ($exitCode != $this->helper->getLoaderExitCodeSuccess()) {
                Mage::throwException('loader returned non-success exit code ' . $exitCode . ' on execution of loader shell command ' . $this->getLoaderShellCommand());
            }
        } else {
            $loader = $this->getLoader();
            $loader->setImportValues($importValues);
            $loader->import();
        }
        Varien_Profiler::stop(__METHOD__);
    }

    /**
     * @return string
     */
    protected function getLoaderShellCommand()
    {
        $shellCommand = 'php ' . escapeshellarg($this->loaderScriptName);
        $shellCommand .= ' --loader ' . escapeshellarg($this->configuration->getLoaderClass());
        $shellCommand .= ' --file ' . escapeshellarg($this->tempFilePath);
        if ($this->profile) {
            $shellCommand .= ' --profile';
            return $shellCommand;
        }
        return $shellCommand;
    }

    /**
     * @return ISAAC_Import_Loader
     * @throws Mage_Core_Exception
     */
    protected function getLoader()
    {
        $loaderClass = $this->configuration->getLoaderClass();
        $loader = Mage::getModel($loaderClass);
        if (!$loader) {
            Mage::throwException('could not instantiate loader class "' . $loaderClass . '"');
        }
        if (!$loader instanceof ISAAC_Import_Loader) {
            Mage::throwException('loader class ' . get_class($loader) . ' should be an instance of ISAAC_Import_Loader');
        }
        return $loader;
    }

}
