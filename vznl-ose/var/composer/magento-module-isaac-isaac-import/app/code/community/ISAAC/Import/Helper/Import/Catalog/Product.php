<?php
/**
 * ISAAC ISAAC_Import
 *
 * @category ISAAC
 * @package ISAAC_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */

class ISAAC_Import_Helper_Import_Catalog_Product extends Mage_Core_Helper_Abstract
{
    /**
     * @var ISAAC_Import_Helper_Data
     */
    protected $importHelper;

    /**
     * @var ISAAC_Import_Helper_Catalog
     */
    protected $catalogHelper;

    /**
     * @var Mage_Catalog_Model_Product
     */
    protected $productModel;

    /**
     * @var Mage_Eav_Model_Config
     */
    protected $eavConfig;

    /**
     * @var Mage_Catalog_Model_Product_Attribute_Backend_Media
     */
    protected $mediaGalleryBackend;

    /**
     * @var array
     */
    protected $storeIdsByCode = [];

    /**
     * @var int[]
     */
    protected $productAttributeSetIdsByName = array();

    /**
     * ISAAC_Import_Helper_Import_Catalog_Product constructor.
     */
    public function __construct()
    {
        $this->importHelper = Mage::helper('isaac_import/data');
        $this->catalogHelper = Mage::helper('isaac_import/catalog');
        $this->productModel = Mage::getModel('catalog/product');
        $this->eavConfig = Mage::getModel('eav/config');
        $this->mediaGalleryBackend = $this->productModel->getResource()->getAttribute('media_gallery')->getBackend();
    }

    /**
     * @param ISAAC_Import_Model_Import_Value $importValue
     * @return bool
     */
    public function supportsImportValue(ISAAC_Import_Model_Import_Value $importValue)
    {
        return $importValue instanceof ISAAC_Import_Model_Import_Value_Catalog_Product;
    }

    /**
     * @inheritdoc
     */
    public function getModelValuesToUpdate(
        Mage_Core_Model_Abstract $model,
        ISAAC_Import_Model_Import_Value $importValue
    ) {
        /** @var ISAAC_Import_Model_Import_Value_Catalog_Product $importValue */
        /** @var Mage_Catalog_Model_Product $model */
        $attributeImportValues = [];
        foreach ($importValue->getDataValues() as $attributeCode => $attributeValue) {
            if (!$this->importHelper->startsWith($attributeCode, '_')) {
                $attributeImportValues[$attributeCode] = $attributeValue;
            }
        }

        $modelValuesToUpdate = $this->catalogHelper->getAttributeValuesToUpdate(
            $model,
            $attributeImportValues,
            $this->getStoreId($importValue)
        );

        //update url key and save rewite history (if needed) when updating a product
        if ($model->getId() &&
            $importValue->hasDataValue('url_key') &&
            $model->getData('url_key') != $importValue->getDataValue('url_key')
        ) {
            $modelValuesToUpdate['save_rewrites_history'] = true;
        }

        //update websites
        if ($importValue->hasDataValue('_websites')) {
            $modelWebsiteIds = $model->getWebsiteIds();
            $importValueWebsiteIds = $this->getWebsiteIds($importValue->getDataValue('_websites'));
            sort($modelWebsiteIds, SORT_NUMERIC);
            sort($importValueWebsiteIds, SORT_NUMERIC);
            if ($modelWebsiteIds !== $importValueWebsiteIds) {
                $modelValuesToUpdate['website_ids'] = $importValueWebsiteIds;
            }
        }

        //update specifications
        if ($importValue->hasDataValue('_specifications')) {
            $specificationsAttributeValue = $this->getSpecificationsByImportValue($importValue);
            if ($specificationsAttributeValue !== $model->getData('specifications')) {
                $modelValuesToUpdate['specifications'] = $specificationsAttributeValue;
            }
        }

        //update attachments
        if ($importValue->hasDataValue('_attachments')) {
            $attachmentsByImportValue = $this->getAttachmentsByImportValue($importValue);
            foreach ($attachmentsByImportValue as $attachmentAttributeCode => $attachments) {
                $attachmentAttributeValue = json_encode($attachments);
                if ($attachmentAttributeValue !== $model->getData($attachmentAttributeCode)) {
                    $modelValuesToUpdate[$attachmentAttributeCode] = $attachmentAttributeValue;
                }
            }
        }

        //update upsells
        if ($importValue->hasDataValue('_up_sells')) {
            $upSellsByImportValue = $this->getUpSellsByImportValue($model, $importValue);
            if ($upSellsByImportValue != $this->getUpSellsByProduct($model)) {
                $modelValuesToUpdate['up_sell_link_data'] = $upSellsByImportValue;
            }
        }

        //update cross sells
        if ($importValue->hasDataValue('_cross_sells')) {
            $crossSellsByImportValue = $this->getCrossSellsByImportValue($model, $importValue);
            if ($crossSellsByImportValue != $this->getCrossSellsByProduct($model)) {
                $modelValuesToUpdate['cross_sell_link_data'] = $crossSellsByImportValue;
            }
        }

        //update related links
        if ($importValue->hasDataValue('_related_links')) {
            $relatedLinksByImportValue = $this->getRelatedLinksByImportValue($model, $importValue);
            if ($relatedLinksByImportValue != $this->getRelatedLinksByProduct($model)) {
                $modelValuesToUpdate['related_link_data'] = $relatedLinksByImportValue;
            }
        }

        //update stock status
        if ($importValue->hasDataValue('_stock_item') && is_array($importValue->getDataValue('_stock_item'))) {
            $stockItemArray = $model->getData('stock_item') ? $model->getData('stock_item')->toArray() : array();
            $stockItemUpdated = false;
            foreach ($importValue->getDataValue('_stock_item') as $stockItemImportKey => $stockItemImportValue) {
                $stockItemValue = $this->formatStockItemValue($stockItemImportKey, $stockItemImportValue);
                if (!array_key_exists($stockItemImportKey, $stockItemArray) ||
                    $stockItemArray[$stockItemImportKey] != $stockItemValue
                ) {
                    $stockItemArray[$stockItemImportKey] = $stockItemValue;
                    $stockItemUpdated = true;
                }
            }
            if ($stockItemUpdated) {
                $modelValuesToUpdate['stock_data'] = $stockItemArray;
            }
        }

        //update images
        if ($importValue->hasDataValue('_images')) {
            $imageImportValues = $importValue->getDataValue('_images');
            $galleryImagesToUpdate = $this->getGalleryImagesToUpdate(
                $model,
                $this->getImageImportValuesMediaAttributeCodes($imageImportValues)
            );
            if (!$this->galleryImagesUpToDate($galleryImagesToUpdate, $imageImportValues)) {
                $this->updateGalleryImages($galleryImagesToUpdate, $imageImportValues, $model);
                //Note: update $updatedAttributeValues such that it reflects the changes made to $product
                $modelValuesToUpdate['media_gallery'] = $model->getData('media_gallery');
                foreach ($this->getImageImportValuesMediaAttributeCodes($imageImportValues) as $mediaAttributeCode) {
                    $modelValuesToUpdate[$mediaAttributeCode] = $model->getData($mediaAttributeCode);
                }
            }
        }

        //update categories
        if ($this->isAdminImport($importValue) && $importValue->hasDataValue('_category_codes')) {
            $newCategoryIds = array_merge(
                $this->getManuallyAssignedCategoryIdsByProduct($model),
                $this->getCategoryIdsByImportValue($importValue)
            );
            sort($newCategoryIds, SORT_NUMERIC);
            if ($newCategoryIds != $model->getCategoryIds()) {
                $modelValuesToUpdate['category_ids'] = $newCategoryIds;
            }
        }

        //update configurable products
        if ($this->isAdminImport($importValue) &&
            $importValue->hasDataValue('_configurable_products_data') &&
            is_array($importValue->getDataValue('_configurable_products_data'))
        ) {
            if (isset($modelValuesToUpdate['price'])) {
                unset($modelValuesToUpdate['price']);
            }
            foreach ($this->getConfigurableProductValues($model, $importValue) as $attributeCode => $value) {
                $modelValuesToUpdate[$attributeCode] = $value;
            }
        }

        //update custom product options
        if ($this->isAdminImport($importValue) && $importValue->hasDataValue('_product_options')) {
            $productOptionsByProduct = $this->getProductOptionsByProduct($model);
            $updatedProductOptionsWithImportValue = $this->getMergedProductOptionsWithImportValue(
                $productOptionsByProduct,
                $importValue->getDataValue('_product_options')
            );
            if ($updatedProductOptionsWithImportValue != $productOptionsByProduct) {
                $modelValuesToUpdate['product_options'] = $updatedProductOptionsWithImportValue;
                $modelValuesToUpdate['can_save_custom_options'] = true;
            }
        }

        return $modelValuesToUpdate;
    }

    /**
     * @param string $key
     * @param $value
     * @return string
     */
    protected function formatStockItemValue($key, $value)
    {
        if (in_array($key, ['qty', 'min_qty', 'min_sale_qty', 'max_sale_qty', 'notify_stock_qty', 'qty_increments'])) {
            return $this->catalogHelper->formatDecimal($value);
        }
        if (strpos($key, 'use_config_') === 0 || strpos($key, 'is_') === 0) {
            return $value ? '1' : '0';
        }
        if (in_array($key, ['backorders', 'manage_stock', 'stock_status_changed_auto', 'enable_qty_increments'])) {
            return $value ? '1' : '0';
        }
        return (string) $value;
    }

    /**
     * @param ISAAC_Import_Model_Import_Value_Catalog_Product $importValue
     * @return string
     */
    protected function getSpecificationsByImportValue(ISAAC_Import_Model_Import_Value_Catalog_Product $importValue)
    {
        if (!$importValue->hasDataValue('_specifications')) {
            return '';
        }
        $specificationsImportValue = $importValue->getDataValue('_specifications');
        if (!is_array($specificationsImportValue)) {
            return '';
        }
        if (count($specificationsImportValue) == 0) {
            return '';
        }
        $specifications = array();
        foreach ($specificationsImportValue as $specificationImportValue) {
            if (!isset($specificationImportValue['label']) || !isset($specificationImportValue['value'])) {
                continue;
            }
            $specificationLabel = $specificationImportValue['label'];
            if (isset($specifications[$specificationLabel])) {
                continue;
            }
            $specificationValue = $specificationImportValue['value'];
            $specifications[$specificationLabel] = $specificationValue;
        }
        return json_encode($specifications);
    }

    /**
     * @param ISAAC_Import_Model_Import_Value_Catalog_Product $importValue
     * @return array
     */
    protected function getAttachmentsByImportValue(ISAAC_Import_Model_Import_Value_Catalog_Product $importValue)
    {
        $attachments = array();
        if (!$importValue->hasDataValue('_attachments')) {
            return $attachments;
        }
        $attachmentsImportValue = $importValue->getDataValue('_attachments');
        if (!is_array($attachmentsImportValue)) {
            return $attachments;
        }
        if (count($attachmentsImportValue) == 0) {
            return $attachments;
        }
        foreach ($attachmentsImportValue as $attachmentAttributeCode => $attachmentImportValues) {
            $attribute = $this->eavConfig->getAttribute(Mage_Catalog_Model_Product::ENTITY, $attachmentAttributeCode);
            if (!$attribute || !$attribute->getId()) {
                $this->importHelper->logMessage('warning: attribute ' . $attachmentAttributeCode . ' does not exist');
                continue;
            }
            foreach ($attachmentImportValues as $attachmentImportValue) {
                if (!isset($attachmentImportValue['label']) || !isset($attachmentImportValue['file_path'])) {
                    continue;
                }
                $attachmentLabel = $attachmentImportValue['label'];
                if (isset($attachments[$attachmentLabel])) {
                    continue;
                }
                $attachmentFilePathWithoutMediaDir = str_replace(
                    Mage::getBaseDir('media') . DS,
                    '',
                    $this->importAttachment(
                        $attachmentImportValue['file_path'],
                        $attachmentAttributeCode
                    )
                );
                $attachments[$attachmentAttributeCode][$attachmentFilePathWithoutMediaDir] = $attachmentLabel;
            }
        }
        return $attachments;
    }

    /**
     * @param string $importFilePath
     * @param string $attachmentAttribute
     * @return string
     * @throws Exception
     * @throws Mage_Core_Exception
     */
    protected function importAttachment($importFilePath, $attachmentAttribute)
    {
        $importedFilePath = $this->getProductAttachmentFolder($attachmentAttribute) . basename($importFilePath);
        if (file_exists($importedFilePath) &&
            $this->importHelper->hasEqualFileContents($importFilePath, $importedFilePath)
        ) {
            //file is up to date
            return $importedFilePath;
        }
        if (!is_dir($this->getProductAttachmentFolder($attachmentAttribute))) {
            if (!mkdir($this->getProductAttachmentFolder($attachmentAttribute), 0777, true)) {
                Mage::throwException(sprintf(
                    'folder %s could not be created',
                    $this->getProductAttachmentFolder($attachmentAttribute)
                ));
            }
        }
        if (!is_writable($this->getProductAttachmentFolder($attachmentAttribute))) {
            Mage::throwException(sprintf(
                'folder %s is not writable',
                $this->getProductAttachmentFolder($attachmentAttribute)
            ));
        }
        if (file_exists($importedFilePath) && !is_writable($importedFilePath)) {
            Mage::throwException('file ' . $importedFilePath . ' is not writable');
        }
        $result = @copy($importFilePath, $importedFilePath);
        if (!$result) {
            $lastError = error_get_last();
            Mage::throwException(sprintf(
                'error copying %s to %s with type \'%s\' and message \'%s\'',
                $importFilePath,
                $importedFilePath,
                $lastError['type'],
                $lastError['message']
            ));
        }
        $this->importHelper->logMessage(sprintf(
            'successfully copied import file %s to %s',
            $importFilePath,
            $importedFilePath
        ));
        return $importedFilePath;
    }

    /**
     * @param string $attachmentAttribute
     * @return string
     */
    protected function getProductAttachmentFolder($attachmentAttribute)
    {
        return Mage::getBaseDir('media') . DS .
        'catalog' . DS .
        'product' . DS .
        'attachment' . DS .
        $attachmentAttribute . DS;
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @param ISAAC_Import_Model_Import_Value_Catalog_Product $importValue
     * @return array
     */
    protected function getUpSellsByImportValue(
        Mage_Catalog_Model_Product $product,
        ISAAC_Import_Model_Import_Value_Catalog_Product $importValue
    ) {
        return $this->getLinksByImportValue($product, $importValue->getDataValue('_up_sells'));
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @param ISAAC_Import_Model_Import_Value_Catalog_Product $importValue
     * @return array
     */
    protected function getCrossSellsByImportValue(
        Mage_Catalog_Model_Product $product,
        ISAAC_Import_Model_Import_Value_Catalog_Product $importValue
    ) {
        return $this->getLinksByImportValue($product, $importValue->getDataValue('_cross_sells'));
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @param ISAAC_Import_Model_Import_Value_Catalog_Product $importValue
     * @return array
     */
    protected function getRelatedLinksByImportValue(
        Mage_Catalog_Model_Product $product,
        ISAAC_Import_Model_Import_Value_Catalog_Product $importValue
    ) {
        return $this->getLinksByImportValue($product, $importValue->getDataValue('_related_links'));
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @param $linksImportValue
     * @return array
     */
    protected function getLinksByImportValue(Mage_Catalog_Model_Product $product, $linksImportValue)
    {
        if (!is_array($linksImportValue)) {
            return array();
        }
        $linksData = array();
        foreach ($linksImportValue as $linkImportValue) {
            if (!isset($linkImportValue['linked_sku'])) {
                continue;
            }
            $linkedProductId = $product->getIdBySku($linkImportValue['linked_sku']);
            if (!$linkedProductId) {
                continue;
            }
            if (!array_key_exists($linkedProductId, $linksData)) {
                $linksData[$linkedProductId] = array();
            }
            $linksData[$linkedProductId]['position'] = isset($linkImportValue['position']) ?
                $linkImportValue['position'] :
                0;

            $linksData[$linkedProductId]['qty'] = $this->catalogHelper->formatDecimal(
                isset($linkImportValue['qty']) ? $linkImportValue['qty'] : 0
            );
        }
        return $linksData;
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @return array
     */
    protected function getUpSellsByProduct(Mage_Catalog_Model_Product $product)
    {
        $upSellData = array();
        foreach ($product->getUpSellLinkCollection() as $link) {
            /** @var Mage_Catalog_Model_Product_Link $link */
            $upSellData[$link->getLinkedProductId()]['position'] = $link->getData('position');
            $upSellData[$link->getLinkedProductId()]['qty'] = ($link->getData('qty') == '') ?
                $this->catalogHelper->formatDecimal(0) :
                $link->getData('qty');
        }
        return $upSellData;
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @return array
     */
    protected function getCrossSellsByProduct(Mage_Catalog_Model_Product $product)
    {
        $crossSellData = array();
        foreach ($product->getCrossSellLinkCollection() as $link) {
            /** @var Mage_Catalog_Model_Product_Link $link */
            $crossSellData[$link->getLinkedProductId()]['position'] = $link->getData('position');
            $crossSellData[$link->getLinkedProductId()]['qty'] = ($link->getData('qty') == '') ?
                $this->catalogHelper->formatDecimal(0) :
                $link->getData('qty');
        }
        return $crossSellData;
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @return array
     */
    protected function getRelatedLinksByProduct(Mage_Catalog_Model_Product $product)
    {
        $relatedLinkData = array();
        foreach ($product->getRelatedLinkCollection() as $link) {
            /** @var Mage_Catalog_Model_Product_Link $link */
            $relatedLinkData[$link->getLinkedProductId()]['position'] = $link->getData('position');
            $relatedLinkData[$link->getLinkedProductId()]['qty'] = ($link->getData('qty') == '') ?
                $this->catalogHelper->formatDecimal(0) :
                $link->getData('qty');
        }
        return $relatedLinkData;
    }

    /**
     * @param array $productOptionsByProduct
     * @param array $productOptionsByImportValue
     * @return array
     */
    protected function getMergedProductOptionsWithImportValue(
        array $productOptionsByProduct,
        array $productOptionsByImportValue
    ) {
        $processedOptionIdentifiers = [];

        foreach ($productOptionsByProduct as $option_id => $productOption) {
            $optionIdentifier = isset($productOption['identifier']) ? trim($productOption['identifier']) : '';
            $processedOptionIdentifiers[] = $optionIdentifier;

            if (($optionIdentifier != '') && (isset($productOptionsByImportValue[$optionIdentifier]))) {
                // update/merge options (that are present in both)
                $productOptionsByProduct[$option_id] = $this->mergeProductOption(
                    $optionIdentifier,
                    $productOption,
                    $productOptionsByImportValue[$optionIdentifier]
                );
            } else {
                // leave options untouched (that are only present in the product)
                continue;
            }
        }

        // add option (that are only present in the import value)
        foreach ($productOptionsByImportValue as $optionIdentifier => $productOptionValue) {
            if (!in_array($optionIdentifier, $processedOptionIdentifiers) && !isset($productOptionValue['is_delete'])) {
                $productOptionsByProduct[] = $this->mergeProductOption($optionIdentifier, [], $productOptionValue);
            }
        }

        return $productOptionsByProduct;
    }

    /**
     * @param string $optionIdentifier
     * @param array $productOptionProduct
     * @param array $productOptionImportValue
     * @return array
     */
    protected function mergeProductOption(
        $optionIdentifier,
        array $productOptionProduct,
        array $productOptionImportValue
    ) {
        $processedOptionValueIdentifiers = [];

        $productOptionImportValue['identifier'] = $optionIdentifier;
        foreach ($productOptionImportValue as $key => $value) {
            if (!$this->importHelper->startsWith($key, '_') && ($productOptionProduct[$key] != $value)) {
                $productOptionProduct[$key] = $value;
            }
        }

        // merge options
        if (isset($productOptionProduct['values']) && is_array($productOptionProduct['values'])) {
            foreach ($productOptionProduct['values'] as $value_id => $productOptionValue) {
                $optionValueIdentifier = isset($productOptionValue['identifier']) ?
                    trim($productOptionValue['identifier']) :
                    '';
                $processedOptionValueIdentifiers[] = $optionValueIdentifier;

                if (($optionValueIdentifier != '') &&
                    (isset($productOptionImportValue['_values'][$optionValueIdentifier]))
                ) {
                    // update options values (that are present in both)
                    foreach ($productOptionImportValue['_values'][$optionValueIdentifier] as $key => $value) {
                        if ($productOptionValue[$key] != $value) {
                            $productOptionValue[$key] = $value;
                        }
                    }
                } else {
                    // remove option values (that are only present in the product)
                    $productOptionValue['is_delete'] = true;
                }

                $productOptionProduct['values'][$value_id] = $productOptionValue;
            }
        }

        // add option values (that are only present in the import value)
        if (isset($productOptionImportValue['_values']) && is_array($productOptionImportValue['_values'])) {
            if (!isset($productOptionProduct['values']) || !is_array($productOptionProduct['values'])) {
                $productOptionProduct['values'] = [];
            }
            foreach ($productOptionImportValue['_values'] as $identifier => $productOptionValue) {
                if (!in_array($identifier, $processedOptionValueIdentifiers)) {
                    $productOptionValue['identifier'] = $identifier;
                    $productOptionProduct['values'][] = $productOptionValue;
                }
            }
        }

        return $productOptionProduct;
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @param string[] $importMediaAttributeCodes
     * @return array
     * @throws Mage_Core_Exception
     */
    protected function getGalleryImagesToUpdate(
        Mage_Catalog_Model_Product $product,
        $importMediaAttributeCodes = array()
    ) {
        $galleryImagesToUpdate = array();
        $mediaAttributeCodesToKeep = array_diff(
            $this->getMediaAttributeCodes($product->getData('attribute_set_id')),
            $importMediaAttributeCodes
        );
        $mediaGallery = $this->getMediaGallery($product);
        foreach ($mediaGallery['images'] as $image) {
            if (!array_key_exists('file', $image)) {
                Mage::throwException('media gallery image data structure does not have a file entry');
            }
            if (count($mediaAttributeCodesToKeep) > 0 &&
                in_array($image['file'], $product->toArray($mediaAttributeCodesToKeep))
            ) {
                continue;
            }
            $galleryImagesToUpdate[] = $image;
        }
        return $galleryImagesToUpdate;
    }

    /**
     * @param $attributeSetId
     * @return string[]
     */
    protected function getMediaAttributeCodes($attributeSetId)
    {
        Varien_Profiler::start(__METHOD__);
        /** @var Mage_Catalog_Model_Resource_Product_Attribute_Collection $productAttributeCollection */
        $productAttributeCollection = Mage::getResourceModel('catalog/product_attribute_collection');
        $productAttributeCollection->setFrontendInputTypeFilter('media_image');
        $productAttributeCollection->setAttributeSetFilter($attributeSetId);
        $select = $productAttributeCollection->getSelect();
        $select->reset(Zend_Db_Select::COLUMNS);
        $select->columns('main_table.attribute_code');
        $mediaAttributeCodes = $productAttributeCollection->getConnection()->fetchCol($select);
        Varien_Profiler::stop(__METHOD__);
        return $mediaAttributeCodes;
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @return int
     */
    protected function getMaximumImagePosition(Mage_Catalog_Model_Product $product)
    {
        $maximumImagePosition = 0;
        $mediaGallery = $this->getMediaGallery($product);
        foreach ($mediaGallery['images'] as $image) {
            if (array_key_exists('removed', $image) && $image['removed']) {
                continue;
            }
            if (array_key_exists('position', $image) && $image['position'] > $maximumImagePosition) {
                $maximumImagePosition = $image['position'];
            }
        }
        return $maximumImagePosition;
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @return array
     */
    protected function getMediaGallery(Mage_Catalog_Model_Product $product)
    {
        if (!$product->hasData('media_gallery')) {
            $this->mediaGalleryBackend->afterLoad($product);
        }

        return $product->getData('media_gallery');
    }

    /**
     * @param array $galleryImages
     * @param array $imageImportValues
     * @return bool
     * @throws Mage_Core_Exception
     */
    protected function galleryImagesUpToDate(array $galleryImages, array $imageImportValues)
    {
        if (count($galleryImages) != count($imageImportValues)) {
            return false;
        }
        foreach ($galleryImages as $index => $galleryImage) {
            if (!array_key_exists($index, $imageImportValues)) {
                Mage::throwException('image import values array does not have index ' . $index);
            }
            if (!$this->galleryImageUpToDate($galleryImage, $imageImportValues[$index])) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param array $galleryImage
     * @param array $imageImportValue
     * @return bool
     * @throws Exception
     */
    protected function galleryImageUpToDate(array $galleryImage, array $imageImportValue)
    {
        $galleryImageLabel = array_key_exists('label', $galleryImage) ? $galleryImage['label'] : '';
        $imageLabelImportValue = array_key_exists('label', $imageImportValue) ? $imageImportValue['label'] : '';
        if ($galleryImageLabel != $imageLabelImportValue) {
            return false;
        }
        $galleryImageDisabled = array_key_exists('disabled', $galleryImage) ? $galleryImage['disabled'] : 0;
        $imageDisabledImportValue = array_key_exists('disabled', $imageImportValue) ? $imageImportValue['disabled'] : 0;
        if ($galleryImageDisabled != $imageDisabledImportValue) {
            return false;
        }
        $galleryImageFilePath = Mage::getBaseDir('media') . DS . 'catalog' . DS . 'product' . $galleryImage['file'];
        $imageFilePathImportValue = $imageImportValue['file_path'];
        $correctFileName = Mage_Core_Model_File_Uploader::getCorrectFileName(basename($imageFilePathImportValue));

        if ($this->importHelper->stripMagentoImageSuffix(basename($galleryImageFilePath)) !=
            $this->importHelper->stripMagentoImageSuffix(basename($correctFileName))
        ) {
            return false;
        }
        if (!$this->importHelper->hasEqualFileContents($galleryImageFilePath, $imageFilePathImportValue)) {
            return false;
        }
        return true;
    }

    /**
     * @param array $galleryImages
     * @param array $imageImportValues
     * @param Mage_Catalog_Model_Product $product
     */
    protected function updateGalleryImages(
        array $galleryImages,
        array $imageImportValues,
        Mage_Catalog_Model_Product $product
    ) {
        //remove gallery images
        foreach ($galleryImages as $galleryImage) {
            $this->mediaGalleryBackend->removeImage($product, $galleryImage['file']);
            $this->importHelper->logMessage('removed image ' . $galleryImage['file']);
        }
        //clear media attributes
        $this->mediaGalleryBackend->clearMediaAttribute(
            $product,
            $this->getImageImportValuesMediaAttributeCodes($imageImportValues)
        );
        //add images
        $imagePosition = $this->getMaximumImagePosition($product) + 1;
        foreach ($imageImportValues as $imageImportValue) {
            $imageImportFilePath = $imageImportValue['file_path'];
            if (!is_readable($imageImportFilePath)) {
                $this->importHelper->logMessage(sprintf(
                    'error: cannot import file "%s" (file is not readable)',
                    $imageImportFilePath
                ));
                continue;
            }
            $importMediaAttributes = array_key_exists('media_attributes', $imageImportValue) ?
                $imageImportValue['media_attributes'] :
                array();

            $imageFilePath = $this->mediaGalleryBackend->addImage(
                $product,
                $imageImportFilePath,
                $importMediaAttributes,
                false,
                false
            );
            $this->mediaGalleryBackend->updateImage($product, $imageFilePath, array('position' => $imagePosition));
            if (array_key_exists('label', $imageImportValue)) {
                $this->mediaGalleryBackend->updateImage(
                    $product,
                    $imageFilePath,
                    array('label' => $imageImportValue['label'])
                );
            }
            if (array_key_exists('disabled', $imageImportValue)) {
                $this->mediaGalleryBackend->updateImage(
                    $product,
                    $imageFilePath,
                    array('disabled' => $imageImportValue['disabled'])
                );
            }
            $this->importHelper->logMessage('added image ' . $imageFilePath);
            $imagePosition++;
        }
    }

    /**
     * @param array $imageImportValues
     * @return string[]
     */
    public function getImageImportValuesMediaAttributeCodes(array $imageImportValues)
    {
        $mediaAttributeCodes = array();
        foreach ($imageImportValues as $imageImportValue) {
            if (!array_key_exists('media_attributes', $imageImportValue)) {
                continue;
            }
            if (!is_array($imageImportValue['media_attributes'])) {
                continue;
            }
            foreach ($imageImportValue['media_attributes'] as $mediaAttributeCode) {
                $mediaAttributeCodes[] = $mediaAttributeCode;
            }
        }
        return $mediaAttributeCodes;
    }

    /**
     * @param Mage_Catalog_Model_Product $model
     * @param ISAAC_Import_Model_Import_Value $importValue
     * @return array
     * @throws Mage_Core_Exception
     */
    protected function getConfigurableProductValues(
        Mage_Catalog_Model_Product $model,
        ISAAC_Import_Model_Import_Value $importValue
    ) {
        /** @var Mage_Catalog_Model_Product $model */
        /** @var ISAAC_Import_Model_Import_Value_Catalog_Product $importValue */
        $configurableProductValues = [];
        $configurableAttributeIds = [];

        $configurableProductPrice = null;
        $importConfigurableProductsData = $importValue->getDataValue('_configurable_products_data');

        //Calculate the lowest price
        foreach ($importConfigurableProductsData as $attributeCode => $simplePricesBySku) {
            $lowestPriceByAttribute = min($simplePricesBySku);
            if ($configurableProductPrice === null || $lowestPriceByAttribute < $configurableProductPrice) {
                $configurableProductPrice = $lowestPriceByAttribute;
            }

            $attribute = $this->eavConfig->getAttribute(Mage_Catalog_Model_Product::ENTITY, $attributeCode);
            $configurableAttributeIds[] = $attribute->getId();
        }

        //This is the case when there are no simple skus defined
        $configurableProductPrice = $this->catalogHelper->formatDecimal($configurableProductPrice);
        if ($configurableProductPrice !== null && $model->getData('price') !== $configurableProductPrice) {
            $configurableProductValues['price'] = $configurableProductPrice;
        }

        if (count($configurableAttributeIds)) {
            /** @var Mage_Catalog_Model_Product_Type_Configurable $typeInstance */
            $typeInstance = $model->getTypeInstance();

            //Configurable attribute ids can only be set for new products
            if (count($typeInstance->getConfigurableAttributesAsArray()) == 0) {
                $typeInstance->setUsedProductAttributeIds($configurableAttributeIds);
            }

            //Configurable attributes data (used for saving configurable_attributes_data
            $oldConfigurableAttributesData = $typeInstance->getConfigurableAttributesAsArray();
            $configurableAttributesData = $oldConfigurableAttributesData;

            //Build configurable products array
            $configurableProductsData = [];

            //Configurable options data (used in the frontend. This contains product information by option)
            $configurableAttributeOptionsData = $typeInstance->getConfigurableOptions($model);

            //Loop trough each configurable attribute
            foreach ($configurableAttributesData as $configurableAttributeIndex => $configurableAttributeData) {
                $attributeCode = $configurableAttributeData['attribute_code'];
                $attributeId = $configurableAttributeData['attribute_id'];
                /** @var Mage_Eav_Model_Entity_Attribute $attribute */
                $attribute = $this->eavConfig->getAttribute(Mage_Catalog_Model_Product::ENTITY, $attributeCode);

                //If attribute code available in import value, go further, else unset products for attribute
                if (array_key_exists(
                    $attributeCode,
                    $importConfigurableProductsData
                )) {
                    //Loop trough Magento options. Update or remove values based on the import value
                    foreach ($configurableAttributeOptionsData[$attributeId] as
                             $configurableAttributeOptionKey => $configurableAttributeOptionData) {
                        $configurableAttributeOptionSku = $configurableAttributeOptionData['sku'];

                        if (isset($importConfigurableProductsData[$attributeCode][$configurableAttributeOptionSku])) {
                            $importPrice =
                                $importConfigurableProductsData[$attributeCode][$configurableAttributeOptionSku];

                            //Update the price
                            $configurableAttributesData[$configurableAttributeIndex]['values']
                            [$configurableAttributeOptionKey]['pricing_value'] = $this->catalogHelper->formatDecimal(
                                $importPrice - $configurableProductPrice
                            );

                            //Add to the configurable products data array
                            $configurableProductsData[$model->getIdBySku($configurableAttributeOptionSku)] =
                                $configurableAttributesData
                                [$configurableAttributeIndex]['values'][$configurableAttributeOptionKey];

                            //Remove sku and price from import value to know the new skus
                            unset($importConfigurableProductsData[$attributeCode][$configurableAttributeOptionSku]);
                        } else {
                            //Option not in import values. Remove it
                            unset(
                                $configurableAttributesData[$configurableAttributeIndex]['values']
                                [$configurableAttributeOptionKey]
                            );
                        }
                    }

                    //Loop to the new import values and add them to Magento array
                    foreach ($importConfigurableProductsData[$attributeCode] as $importSku => $importPrice) {
                        $simpleProductId = $model->getIdBySku($importSku);
                        $simpleOptionId = Mage::getResourceSingleton('catalog/product')->getAttributeRawValue(
                            $simpleProductId,
                            $attributeId,
                            $this->getStoreId($importValue)
                        );

                        if (!$simpleOptionId) {
                            $this->importHelper->logMessage(
                                'Skipped configurable product option for product: %s, attribute: %s',
                                $importSku,
                                $attributeCode
                            );

                            continue;
                        }

                        $attributeData = [
                            'label' => $attribute->getSource()->getOptionText($simpleOptionId),
                            'attribute_id' => $attributeId,
                            'value_index' => $simpleOptionId,
                            'is_percent' => false,
                            'pricing_value' => $this->catalogHelper->formatDecimal(
                                $importPrice - $configurableProductPrice
                            ),
                        ];

                        $configurableAttributesData[$configurableAttributeIndex]['values'][] = $attributeData;
                        $configurableProductsData[$simpleProductId] = $attributeData;
                    }
                } else {
                    unset($configurableAttributesData[$configurableAttributeIndex]['values']);
                }
            }

            //When product has changes, update the configurable values
            if ($oldConfigurableAttributesData != $configurableAttributesData) {
                $this->importHelper->logMessage('Configurable product %s has changes.');

                $configurableProductValues['can_save_configurable_attributes'] = true;
                $configurableProductValues['configurable_products_data'] = $configurableProductsData;
                $configurableProductValues['configurable_attributes_data'] = $configurableAttributesData;
            }
        }

        return $configurableProductValues;
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @return array
     */
    protected function getManuallyAssignedCategoryIdsByProduct(Mage_Catalog_Model_Product $product)
    {
        $categoryIds = $product->getCategoryIds();
        if (count($categoryIds) === 0) {
            return array();
        }
        $categoryCollection = Mage::getModel('catalog/category')->getCollection();
        $categoryCollection->addFieldToFilter('assign_products_automatically', 1);
        $categoryCollection->addFieldToFilter('entity_id', array('in', $categoryIds));
        $automaticallyAssignedCategoryIds = $categoryCollection->getAllIds();
        $manuallyAssignedCategoryIds = array_diff($categoryIds, $automaticallyAssignedCategoryIds);
        return $manuallyAssignedCategoryIds;
    }

    /**
     * @param ISAAC_Import_Model_Import_Value_Catalog_Product $importValue
     * @return int[]
     */
    protected function getCategoryIdsByImportValue(ISAAC_Import_Model_Import_Value_Catalog_Product $importValue)
    {
        if (!$importValue->hasDataValue('_category_codes')) {
            return array();
        }
        $categoryCodes = $importValue->getDataValue('_category_codes');
        if (!is_array($categoryCodes)) {
            return array();
        }
        if (count($categoryCodes) == 0) {
            return array();
        }
        $categoryCollection = Mage::getModel('catalog/category')->getCollection();
        $categoryCollection->addFieldToFilter('assign_products_automatically', 1);
        $categoryCollection->addFieldToFilter('category_code', array('in' => $categoryCodes));
        $categoryIds = array();
        foreach ($categoryCollection as $category) {
            /** @var Mage_Catalog_Model_Category $category */
            $categoryIds[] = $category->getId();
        }
        return $categoryIds;
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @return array
     */
    protected function getProductOptionsByProduct(Mage_Catalog_Model_Product $product)
    {
        $options = [];
        if (!is_array($product->getOptions()) || count($product->getOptions()) < 1) {
            return $options;
        }

        /** @var Mage_Catalog_Model_Product_Option $optionModel */
        foreach ($product->getOptions() as $optionModel) {
            $optionArray = $optionModel->getData();
            $optionArray['values'] = [];

            /** @var Mage_Catalog_Model_Product_Option_Value $valueModel */
            foreach ($optionModel->getValues() as $valueModel) {
                $optionArray['values'][$valueModel->getData('option_type_id')] = $valueModel->getData();
            }

            $options[$optionModel->getId()] = $optionArray;
        }

        return $options;
    }

    /**
     * @param ISAAC_Import_Model_Import_Value_Catalog_Product $importValue
     * @return int
     */
    protected function getStoreId(ISAAC_Import_Model_Import_Value_Catalog_Product $importValue)
    {
        $storeCode = $importValue->getStoreCode();
        if (!array_key_exists($storeCode, $this->storeIdsByCode)) {
            /** @var Mage_Core_Model_Store $store */
            $store = Mage::getModel('core/store')->load($storeCode);

            $this->storeIdsByCode[$storeCode] = $store->getId();
        }
        return $this->storeIdsByCode[$storeCode];
    }

    /**
     * @param ISAAC_Import_Model_Import_Value_Catalog_Product $importValue
     * @return bool
     */
    protected function isAdminImport(ISAAC_Import_Model_Import_Value_Catalog_Product $importValue)
    {
        return $this->getStoreId($importValue) == Mage_Core_Model_App::ADMIN_STORE_ID;
    }

    /**
     * @param string[]|null $websiteCodes
     * @return int[]
     */
    protected function getWebsiteIds(array $websiteCodes)
    {
        $websiteIds = array();
        /** @var Mage_Core_Model_Website $website */
        foreach (Mage::app()->getWebsites() as $website) {
            if (in_array($website->getCode(), $websiteCodes)) {
                $websiteIds[] = $website->getId();
            }
        }
        return $websiteIds;
    }

    /**
     * @inheritDoc
     */
    public function afterSave(
        Mage_Core_Model_Abstract $model,
        ISAAC_Import_Model_Import_Value $importValue,
        $isObjectNew
    ) {
        /** @var Mage_Catalog_Model_Product $model */
        $model->getOptionInstance()->unsetOptions()->setProduct()->clearInstance();
    }

    /**
      * @param string $attributeSetName
      * @return int
      * @throws Mage_Core_Exception
    */
    public function getProductAttributeSetIdByName($attributeSetName)
    {
        if (!array_key_exists($attributeSetName, $this->productAttributeSetIdsByName)) {
            $attributeSetCollection = Mage::getResourceModel('eav/entity_attribute_set_collection');
            $attributeSetCollection->setEntityTypeFilter(
                $this->eavConfig->getEntityType(Mage_Catalog_Model_Product::ENTITY)->getEntityTypeId()
            );
            $attributeSetCollection->addFieldToFilter('attribute_set_name', $attributeSetName);
            /** @var Mage_Eav_Model_Entity_Attribute_Set $attributeSet */
            $attributeSet = $attributeSetCollection->getFirstItem();
            if (!$attributeSet->getId()) {
                throw new Mage_Core_Exception(sprintf('product attribute set %s does not exist', $attributeSetName));
            }
            $this->productAttributeSetIdsByName[$attributeSetName] = $attributeSet->getId();
        }
        return $this->productAttributeSetIdsByName[$attributeSetName];
    }
}
