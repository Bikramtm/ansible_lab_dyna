# ISAAC\_Import Magento module #

The ISAAC\_Import Magento module provides a framework to import content into Magento.

Features:

* Project-specific imports can be configured via XML, and run via a provided shell script.
* Setup scripts can use the ISAAC\_Import module to load content in Magento from a predefined XML file format.

## Project-specific imports ##

The import module allows project-imports by means of a shell script.

The task of importing Magento contents is split into three parts:

1. Configuration of importers and import groups
2. Generation of import values
3. Loading of import values

### Shell script ###

The script `isaacImporter.php` allows the import of import groups and single imports via the shell.

The shell script provides additional options to enable profiling, and disabling cleanup and/or indexing.

See `isaacImporter.php --help` for the list of current import groups, importers and options for your Magento project.

### Configuration ###

Importers and import group can be configured via a module's `config.xml`.

See `isaacImporter.php --dev-help` for information on how to add importers and import groups, and the list of current features.

### Generation of import values ###

Each importer should provide a generation class to create import values.

This class should implement the `ISAAC_Import_Generator` interface, which is an `IteratorAggregate`, i.e. it has a `getIterator` method that should return an iterator.

The ISAAC\_Import module provides a number of generation classes and iterators that should aid in creation of custom generation classes:

* The class `isaac_import/extract_transform_generator` class splits generation in extraction (by means of an iterator) and transformation of extracted data.
* To aid the extraction process, generic iterators are provided to read data from CSV and XML files, and for chunking, grouping and mapping data.

Also, classes are provided to generate import values from a predefined XML format for categories and CMS blocks, CMS pages and widgets.

### Loading of import values ###

Each importer should provide a loader class for loading import values.

This class should extend from the `ISAAC_Import_Loader` base class.

The ISAAC\_Import modules provides standard `singly` (one model at a time) loaders for the following entity types:

* Categories
* Products
* CMS pages
* CMS blocks
* Widgets

For products, also a `collection` loader is provided which has improved performance over the `singly` loader.

Also, for all Magento models, a skeleton loader implementation `isaac_import/loader_model` has been made, which reduces the creation of a loader to the implementation of two methods:

1. `getModelByImportValue`: obtain an (existing or new) model by an import value
2. `getModelValuesToUpdate`: retrieve all model values to update based on the current model and an import value

## Setup scripts ##

The ISAAC\_Import module provides helper methods for loading XML content from file for a number of entity types.

The following entity types are currently supported:

* Categories
* CMS pages
* CMS blocks
* Widgets

It is also possible to create setup helper methods for other entity types: for this an XML file generation class and a loader class need to be provided.
