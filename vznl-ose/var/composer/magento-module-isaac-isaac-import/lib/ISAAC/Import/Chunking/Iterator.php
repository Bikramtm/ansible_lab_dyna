<?php
/**
 * ISAAC ISAAC_Import
 *
 * @category ISAAC
 * @package ISAAC_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */
/**
 * Iterator to group iterator values in chunks
 */
class ISAAC_Import_Chunking_Iterator implements OuterIterator
{
	/** @var Iterator $innerIterator */
	protected $innerIterator;

	/** @var int $chunkSize */
	protected $chunkSize;

	/** @var int $chunkKey */
	protected $chunkKey = -1;

	/** @var array $chunkValues */
	protected $chunkValues = [];

	/**
	 * @param Iterator $innerIterator
	 * @param int $chunkSize
     */
	public function __construct(Iterator $innerIterator, $chunkSize)
	{
		$this->innerIterator = $innerIterator;
		if (!is_numeric($chunkSize) || $chunkSize <= 0) {
			throw new InvalidArgumentException('chunk size should be a number larger than 0');
		}
		$this->chunkSize = $chunkSize;
	}

	/**
	 * @return Iterator
     */
	public function getInnerIterator()
	{
		return $this->innerIterator;
	}

	/**
	 *
     */
	public function rewind()
	{
		$this->innerIterator->rewind();
		$this->chunkKey = -1;
		$this->chunkValues = [];
		$this->calculateNextChunk();
	}

	/**
	 * @return bool
     */
	public function valid()
	{
		return (!empty($this->chunkValues)) || $this->innerIterator->valid();
	}

	/**
	 * @return array
     */
	public function current()
	{
		return $this->chunkValues;
	}

	/**
	 * @return int
     */
	public function key()
	{
		return $this->chunkKey;
	}

	/**
	 *
     */
	public function next()
	{
		$this->calculateNextChunk();
	}

	/**
	 *
     */
	protected function calculateNextChunk()
	{
		$chunkValues = [];
		$numberOfValuesInChunk = 0;
		while ($this->innerIterator->valid() && $numberOfValuesInChunk < $this->chunkSize) {
			$chunkValues[] = $this->innerIterator->current();
			$numberOfValuesInChunk += 1;
			$this->innerIterator->next();
		}
		$this->chunkValues = $chunkValues;
		$this->chunkKey += 1;
	}
}

