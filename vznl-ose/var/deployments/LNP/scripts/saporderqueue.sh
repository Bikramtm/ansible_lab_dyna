#!/bin/bash -v

environmentsArray=(1,2,3,4,'a')

echo "LNP Deployment script for SAPOrder queue daemon (Dyna.MoveQueueOSE2OIL) started"

function installlnp
{
	INPUT_ENVIRONMENT=$1
	DOCROOT_DIR='/var/SP/data/dynalean/services/'
	ENVIRONMENT='osf-telesales-lnp'$INPUT_ENVIRONMENT'.vodafone.de'
	DEPLOYMENT_DIR='/var/SP/data/dynalean'
	APP_DIR="${DOCROOT_DIR}${ENVIRONMENT}/Dyna.MoveQueueOSE2OIL"
	DATE=`date +"%Y-%m-%d %H:%M"`

	if ! [ -d "$APP_DIR" ]; then
	    mkdir -p $APP_DIR
		echo "Application directory '$APP_DIR' created"
	fi

	if ! [ -f "$DEPLOYMENT_DIR/sap.tar.gz" ]; then
		echo "Deployment package '$DEPLOYMENT_DIR/sap.tar.gz' doesnt exist"
		exit;
	fi

    echo "Do you want to run the service as a nohup after deployment? (will be run in background) [y/n]"
    read RUN_SERVICE

	# Put in new code
	cd $APP_DIR

	# match all files except
	find . -xdev -type f \
	    ! -wholename './Dyna.MoveQueueOSE2OIL.exe.config' \
	    ! -wholename './extralog.log' \
	    ! -wholename './*.log' \
	    -delete

	tar xf $DEPLOYMENT_DIR/sap.tar.gz -C $APP_DIR

	# Set permissions
	#chmod -R g+rw $APP_DIR
	#chmod -R 777 $APP_DIR/app/code/community/Dyna/Proxy

	find -type d -exec chmod 755 {} \;
	find -type f -exec chmod 644 {} \;
	chmod +x Dyna.MoveQueueOSE2OIL.exe;

	# Restart services
	#$APP_DIR + '/mono_restart.sh > /dev/null 2>&1'

    if [ "$RUN_SERVICE" == "y" ]; then
	# Start the service using nohup
	nohup mono-service -d $APP_DIR -l $APP_DIR $APP_DIR/Dyna.MoveQueueOSE2OIL.exe &
	echo "Service started"
	fi
}

if [ "$INPUT_ENVIRONMENT" == "a" ]; then
	installlnp 1
	installlnp 2
	installlnp 3
	installlnp 4
else
	installlnp $INPUT_ENVIRONMENT
fi

	echo "Deployment succeeded"
