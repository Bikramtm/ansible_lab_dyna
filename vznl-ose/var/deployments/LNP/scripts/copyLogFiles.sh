#!/bin/bash -v

#This script copies all the log files from all the LNP machine to the current directory

host="osf-telesales-lnp.vodafone.de"
OSEVersion="417-164821_671"

### Do not change below this line ###
docroot="/usr/local/zend/var/apps/http/${host}/80/${OSEVersion}"

for i in {i,j,k,l,m,n,o,p}
    do
        scp -r deosl${i}vr:/usr/local/zend/var/apps/http/${docroot}/var/log ./deosl${i}vr_log
done
