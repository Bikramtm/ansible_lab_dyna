#!/bin/bash -v

environmentsArray=(1,2,3,4,'a')

echo "SIT Deployment script for OSE started"

echo "Which build version is this going to be?" "e.g. 376"
read BUILD_VERSION

echo "For which environment is this script running?" [${environmentsArray[@]}] "a = all environments"
read INPUT_ENVIRONMENT

if ! [[ " ${environmentsArray[@]} " =~ "${INPUT_ENVIRONMENT}" ]]; then
	echo "Invalid environment specified"
	exit;
fi

function postStatus
{
    curl -X POST --data "payload={ \"username\": \"$1\", \"icon_emoji\": \":octopus:\",  \"text\": \"$2\"}" https://hooks.slack.com/services/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
}

function installSit
{
	INPUT_ENVIRONMENT=$1
	DOCROOT_DIR='/var/SP/data/docroots/'
	ENVIRONMENT='osf-telesales-sit'$INPUT_ENVIRONMENT'.vodafone.de'
	DEPLOYMENT_DIR='/var/SP/data/dynalean'
	APP_DIR="${DOCROOT_DIR}${ENVIRONMENT}"
	DATE=`date +"%Y-%m-%d %H:%M"`

	if ! [ -d "$APP_DIR" ]; then
		echo "Application directory '$APP_DIR' doesnt exist"
		exit;
	fi

	if ! [ -f "$DEPLOYMENT_DIR/release.tar.gz" ]; then
		echo "Deployment package '$DEPLOYMENT_DIR/release.tar.gz' doesnt exist"
		exit;
	fi

    echo "Deploying to '$APP_DIR' NOW!"

    postStatus "Deployment-Bot-VFDE-OSE", "Deploying OSE ${BUILD_VERSION} to SIT ${ENVIRONMENT}"

	# Put application in maintenance
	touch $APP_DIR/maintenance.flag

	# Put in new code
	cd $APP_DIR

	# match all files except
	find . -xdev -type f \
	    ! -wholename './app/etc/local.xml' \
	    ! -wholename './app/etc/jobs.xml' \
	    ! -wholename './app/etc/monolog.xml' \
	    ! -wholename './app/etc/sandbox.xml' \
	    ! -wholename './app/etc/replicas.xml' \
	    ! -wholename './shell/amqp/ESBQueueReaderMonoService.exe.config' \
	    ! -wholename './index.php' \
	    ! -wholename './var/*' -a \
	    ! -wholename './media/*' -a \
	    ! -wholename './maintenance.flag' \
	    -delete

	# match all directories that are now empty ...
	find  . -xdev -type d \
	    ! -wholename './var' -a \
	    ! -wholename './media' -a \
	    -empty \
	    -delete

	tar xf $DEPLOYMENT_DIR/release.tar.gz -C $APP_DIR

	# Set permissions
	#chmod -R g+rw $APP_DIR
	#chmod -R 777 $APP_DIR/app/code/community/Dyna/Proxy

	find -type d -exec chmod 755 {} \;
	find -type f -exec chmod 644 {} \;
	chmod 777 -R var media

	# Configs
	# Local
	#echo '${bamboo.application_config_local}' > $APP_DIR + '/app/etc/local.xml'
	# Version label
	echo "<?php return 'B${BUILD_VERSION} ${DATE}';" >$APP_DIR'/app/etc/SOFTWARE_VERSION.php'
	# Jobs
	#echo '${bamboo.application_config_jobs}' > $APP_DIR + '/app/etc/jobs.xml'
	# Monolog
	#echo '${bamboo.application_config_monolog}' > $APP_DIR + '/app/etc/monolog.xml'
	# AMQP
	#echo '${bamboo.application_config_amqp}' > $APP_DIR + '/shell/amqp/ESBQueueReaderMonoService.exe.config'
	# Sandbox
	#echo '${bamboo.application_config_replicas}' > $APP_DIR + '/app/etc/replicas.xml'
	#echo '${bamboo.application_config_sandbox}' > $APP_DIR + '/app/etc/sandbox.xml'

	# Restart services
	#$APP_DIR + '/shell/mono_restart.sh > /dev/null 2>&1'

	# Clear cache
	# Varnish
	#curl -X BAN 127.0.0.1
	# Redis

	redisIP=10.97.109.73

	echo "Flushing redis cache for SIT${1}"
	if [ "$1" == "1" ]; then
	{
	   redisPort=6382
	}
	elif [ "$1" == "2" ]; then
	{
	   redisPort=6379
	}
	elif [ "$1" == "3" ]; then
	{
	   redisPort=6380
	}
	elif [ "$1" == "4" ]; then
	{
	   redisPort=6381
	}
	fi

	redis-cli -h $redisIP -p $redisPort "FLUSHALL"

	# Remove maintenance
	rm -f $APP_DIR/maintenance.flag

    echo "Deployment succeeded"
	postStatus "Deployment-Bot-VFDE-OSE", "Deployment OSE ${BUILD_VERSION} of SIT ${ENVIRONMENT} done"
}

if [ "$INPUT_ENVIRONMENT" == "a" ]; then
	installSit 1
	installSit 2
	installSit 3
	installSit 4
else
	installSit $INPUT_ENVIRONMENT
fi

echo "Deployment succeeded"
