##############################
## UPDATE SCRIPT FOR ENDPOINTS
### SIT1 ###
##############################

# Set webroot
UPDATE `omnius_osf1`.`core_config_data` SET `value` = 'https://osf-telesales-sit1.vodafone.de/' WHERE `path` = 'web/unsecure/base_url';
UPDATE `omnius_osf1`.`core_config_data` SET `value` = 'https://osf-telesales-sit1.vodafone.de/' WHERE `path` = 'web/secure/base_url';

# set the product visibility enabled
UPDATE `omnius_osf1`.`core_config_data` SET `value` = 1 WHERE `path` = 'catalog/frontend/product_visibility_enabled';

#DISABLE STUB USAGE
UPDATE `omnius_osf1`.`core_config_data` SET `value` = 0 WHERE `path` = 'omnius_service/general/use_stubs';

#SET OGW AS PARTYNAME
UPDATE `omnius_osf1`.`core_config_data` SET `value` = 'OGW' WHERE `path` = 'omnius_service/service_settings_header/party_name';

#ENDPOINT FOR POTENTIAL LINKS => OIL
UPDATE `omnius_osf1`.`core_config_data` SET `value` = 'http://deossbvw.dc-ratingen.de:8008/Middleware.VFDE.I1001_SearchPotentialLinks.Wcf/WcfService_Middleware_VFDE_I1001_SearchPotentialLinks_App.svc?wsdl' WHERE `path` = 'omnius_service/potential_links/wsdl_potential_links';

#ENDPOINT FOR SEND DOCUMENT => OIL
UPDATE `omnius_osf1`.`core_config_data` SET `value` = 'http://deossbvw.dc-ratingen.de:8008/Middleware.VFDE.I1011_SendDocument.Wcf/WcfService_Middleware_VFDE_I1011_SendDocument_App.svc?wsdl' WHERE `path` = 'omnius_service/send_document/wsdl_send_document';

#ENDPOINT FOR CREATE MANUAL WORKITEM => OIL
UPDATE `omnius_osf1`.`core_config_data` SET `value` = 'http://deossbvw.dc-ratingen.de:8008/Middleware.OIL.I13_CreateManualWorkItem.Wcf/WcfService_Middleware_OIL_I13_CreateManualWorkItem_App.svc?wsdl' WHERE `path` = 'omnius_service/create_manual_work_item/wsdl_create_manual_work_item';

#ENDPOINT FOR SEARCHCUSTOMER => OIL
UPDATE `omnius_osf1`.`core_config_data` SET `value` = 'http://deossbvw.dc-ratingen.de:8008/Middleware.OIL.I01_SearchCustomer.Wcf/WcfService_Middleware_OIL_I01_SearchCustomer_App.svc?wsdl' WHERE `path` = 'omnius_service/customer_search/wsdl';

#ENDPOINT FOR SEARCHCUSTOMER USAGE URL => OIL
UPDATE `omnius_osf1`.`core_config_data` SET `value` = 'http://deossbvw.dc-ratingen.de:8008/Middleware.OIL.I01_SearchCustomer.Wcf/WcfService_Middleware_OIL_I01_SearchCustomer_App.svc' WHERE `path` = 'omnius_service/customer_search/usage_url';

#ENDPOINT FOR SEARCHCUSTOMERFROMLEGACY => OIL
UPDATE `omnius_osf1`.`core_config_data` SET `value` = 'http://deossbvw.dc-ratingen.de:8008/Middleware.VFDE.I1003_SearchCustomerFromLegacy.Wcf/WcfService_Middleware_VFDE_I1003_SearchCustomerFromLegacy_App.svc?wsdl' WHERE `path` = 'omnius_service/customer_search_legacy/wsdl';

#ENDPOINT FOR SEARCHCUSTOMERFROMLEGACY USAGE URL => OIL
UPDATE `omnius_osf1`.`core_config_data` SET `value` = 'http://deossbvw.dc-ratingen.de:8008/Middleware.VFDE.I1003_SearchCustomerFromLegacy.Wcf/WcfService_Middleware_VFDE_I1003_SearchCustomerFromLegacy_App.svc' WHERE `path` = 'omnius_service/customer_search_legacy/usage_url';

#ENDPOINT FOR RETRIEVECUSTOMERINFO => OIL
UPDATE `omnius_osf1`.`core_config_data` SET `value` = 'http://deossbvw.dc-ratingen.de:8008/Middleware.OIL.I07_RetrieveCustomerInfo.Wcf/WcfService_Middleware_OIL_I07_RetrieveCustomerInfo_App.svc?wsdl' WHERE `path` = 'omnius_service/customer_info/wsdl_customer_info';

#ENDPOINT FOR RETRIEVECREDITPROFILE => OIL
UPDATE `omnius_osf1`.`core_config_data` SET `value` = 'http://deossbvw.dc-ratingen.de:8008/Middleware.OIL.I02_RetrieveCustomerCreditProfile.Wcf/WcfService_Middleware_OIL_I02_RetrieveCustomerCreditProfile_App.svc?wsdl' WHERE `path` = 'omnius_service/customer_credit_profile/wsdl_customer_credit_profile';

#ENDPOINT FOR CREATEORADDLINK => OIL
UPDATE `omnius_osf1`.`core_config_data` SET `value` = 'http://deossbvw.dc-ratingen.de:8008/Middleware.VFDE.I1002_CreateOrAddLink.Wcf/WcfService_Middleware_VFDE_I1002_CreateOrAddLink_App.svc?wsdl' WHERE `path` = 'omnius_service/create_add_link/wsdl_create_add_link';

#ENDPOINT FOR INVALIDPRODUCTS => OGW
UPDATE `omnius_osf1`.`core_config_data` SET `value` = 'https://deossevr.dc-ratingen.de:8002/VFDEGetInvalidProductsEG/VFDE' WHERE `path` = 'omnius_service/invalid_products/endpoint_invalid_products';

#WSDL FOR INVALIDPRODUCTS => OGW
UPDATE `omnius_osf1`.`core_config_data` SET `value` = 'https://deossevr.dc-ratingen.de:8002/VFDEGetInvalidProductsEG/VFDE?wsdl' WHERE `path` = 'omnius_service/invalid_products/wsdl_invalid_products';

#ENDPOINT FOR GETIPEQUIPMENTFEATURES => OGW
UPDATE `omnius_osf1`.`core_config_data` SET `value` = 'https://deossevr.dc-ratingen.de:8002/VFDEGetIpEquipmentFeaturesEG/VFDE' WHERE `path` = 'omnius_service/ip_equipment_features/endpoint_ip_equipment_features';

#WSDL FOR GETIPEQUIPMENTFEATURES => OGW
UPDATE `omnius_osf1`.`core_config_data` SET `value` = 'https://deossevr.dc-ratingen.de:8002/VFDEGetIpEquipmentFeaturesEG/VFDE?wsdl' WHERE `path` = 'omnius_service/ip_equipment_features/wsdl_ip_equipment_features';

#ENDPOINT FOR NONSTANDARDRATE => OGW
UPDATE `omnius_osf1`.`core_config_data` SET `value` = 'https://deossevr.dc-ratingen.de:8002/VFDEGetNonStandardRateEG/VFDE' WHERE `path` = 'omnius_service/non_standard_rate/endpoint_non_standard_rate';

#WSDL FOR NONSTANDARDRATE => OGW
UPDATE `omnius_osf1`.`core_config_data` SET `value` = 'https://deossevr.dc-ratingen.de:8002/VFDEGetNonStandardRateEG/VFDE?wsdl' WHERE `path` = 'omnius_service/non_standard_rate/wsdl_non_standard_rate';

#ENDPOINT FOR GETSMARTCARDCOMPATIBILITY => OGW
UPDATE `omnius_osf1`.`core_config_data` SET `value` = 'https://deossevr.dc-ratingen.de:8002/VFDEGetSmartCardCompatibilityEG/VFDE' WHERE `path` = 'omnius_service/smart_card_compatibility/endpoint_smart_card_compatibility';

#WSDL FOR GETSMARTCARDCOMPATIBILITY => OGW
UPDATE `omnius_osf1`.`core_config_data` SET `value` = 'https://deossevr.dc-ratingen.de:8002/VFDEGetSmartCardCompatibilityEG/VFDE?wsdl' WHERE `path` = 'omnius_service/smart_card_compatibility/wsdl_smart_card_compatibility';

#ENDPOINT FOR GETSMARTCARDTYPE => OGW
UPDATE `omnius_osf1`.`core_config_data` SET `value` = 'https://deossevr.dc-ratingen.de:8002/VFDEGetSmartCardTypeEG/VFDE' WHERE `path` = 'omnius_service/smart_card_type/endpoint_smart_card_type';

#WSDL FOR GETSMARTCARDTYPE => OGW
UPDATE `omnius_osf1`.`core_config_data` SET `value` = 'https://deossevr.dc-ratingen.de:8002/VFDEGetSmartCardTypeEG/VFDE?wsdl' WHERE `path` = 'omnius_service/smart_card_type/wsdl_smart_card_type';

#ENDPOINT FOR GETTVEQUIPMENTFEATURES => OGW
UPDATE `omnius_osf1`.`core_config_data` SET `value` = 'https://deossevr.dc-ratingen.de:8002/VFDEGetTvEquipmentFeaturesEG/VFDE?wsdl' WHERE `path` = 'omnius_service/tv_equipment_features/wsdl_tv_equipment_features';

#ENDPOINT FOR GETTVEQUIPMENTFEATURES ENDPOINT => OGW
UPDATE `omnius_osf1`.`core_config_data` SET `value` = 'https://deossevr.dc-ratingen.de:8002/VFDEGetTvEquipmentFeaturesEG/VFDE' WHERE `path` = 'omnius_service/tv_equipment_features/endpoint_tv_equipment_features';

#ENDPOINT FOR GETCONFIGURATIONDATA => OGW
UPDATE `omnius_osf1`.`core_config_data` SET `value` = 'https://deossevr.dc-ratingen.de:8002/VFDEGetConfigurationDataEG/VFDE?wsdl' WHERE `path` = 'omnius_service/get_configuration_data/wsdl_get_configuration_data';

#ENDPOINT FOR GETCONFIGURATIONDATA USAGE URL => OGW
UPDATE `omnius_osf1`.`core_config_data` SET `value` = 'https://deossevr.dc-ratingen.de:8002/VFDEGetConfigurationDataEG/VFDE' WHERE `path` = 'omnius_service/get_configuration_data/endpoint_get_configuration_data';

#ENDPOINT FOR SEARCHPOTENTIALLINKS => OIL
UPDATE `omnius_osf1`.`core_config_data` SET `value` = 'http://deossbvw.dc-ratingen.de:8008/Middleware.VFDE.I1001_SearchPotentialLinks.Wcf/WcfService_Middleware_VFDE_I1001_SearchPotentialLinks_App.svc?wsdl' WHERE `path` = 'omnius_service/service_ability/wsdl_potential_links';

#ENDPOINT FOR SERVICEABILITY => OIL
UPDATE `omnius_osf1`.`core_config_data` SET `value` = 'http://deossbvw.dc-ratingen.de:8008/Middleware.OIL.I08_CheckServiceAbility.Wcf/WcfService_Middleware_OIL_I08_CheckServiceAbility_App.svc?wsdl' WHERE `path` = 'omnius_service/service_ability/wsdl';

#ENDPOINT FOR VALIDATEADDRESS => OIL
UPDATE `omnius_osf1`.`core_config_data` SET `value` = 'http://deossbvw.dc-ratingen.de:8008/Middleware.OIL.I03_ValidateAddress.Wcf/WcfService_Middleware_OIL_I03_ValidateAddress_App.svc?wsdl' WHERE `path` = 'omnius_service/validate_address/wsdl';

#ENDPOINT FOR CONVERTORVALIDATEIBAN => OIL
UPDATE `omnius_osf1`.`core_config_data` SET `value` = 'http://deossbvw.dc-ratingen.de:8008/Middleware.OIL.I04_ConvertAndValidateIBAN.Wcf/WcfService_Middleware_OIL_I04_ConvertAndValidateIBAN_App.svc?wsdl' WHERE `path` = 'omnius_service/convert_iban/wsdl_convert_iban';

#ENDPOINT FOR GETPAYERANDPAYMENTINFOR => OIL
UPDATE `omnius_osf1`.`core_config_data` SET `value` = 'http://deossbvw.dc-ratingen.de:8008/Middleware.VFDE.I1004_GetPayerAndPaymentInfo.Wcf/WcfService_Middleware_VFDE_I1004_GetPayerAndPaymentInfo_App.svc?wsdl' WHERE `path` = 'omnius_service/payer_and_payment_info/wsdl';

#ENDPOINT FOR SUBMITORDER => OIL
UPDATE `omnius_osf1`.`core_config_data` SET `value` = 'http://deossbvw.dc-ratingen.de:8008/Middleware.OIL.I09_SubmitOrder.Wcf/WcfService_Middleware_OIL_I09_SubmitOrder_App.svc?wsdl' WHERE `path` = 'omnius_service/process_order/wsdl_process_order';

#ENDPOINT FOR SHIPPINGCONDITION => OIL => SAP
UPDATE `omnius_osf1`.`core_config_data` SET `value` = 'http://deossbvw.dc-ratingen.de:8008/Middleware.OIL.I05_ShippingCondition.Wcf/WcfService_Middleware_OIL_I05_ShippingCondition_App.svc?wsdl' WHERE `path` = 'omnius_service/sap_shipping_condition/wsdl';

#ENDPOINT FOR SHIPPINGFEECALCULATE => OIL => SAP
UPDATE `omnius_osf1`.`core_config_data` SET `value` = 'http://deossbvw.dc-ratingen.de:8008/Middleware.OIL.I06_ShippingFeeCalculate.Wcf/WcfService_Middleware_OIL_I06_ShippingFeeCalculate_App.svc?wsdl' WHERE `path` = 'omnius_service/shipping_fee/wsdl';

#ENDPOINT FOR HOUSEHOLDMEMBERS => OIL
UPDATE `omnius_osf1`.`core_config_data` SET `value` = 'http://deossbvw.dc-ratingen.de:8008/Middleware.OIL.I14_GetHouseHoldMembers.Wcf/WcfService_Middleware_OIL_I14_GetHouseHoldMembers_App.svc' WHERE `path` = 'omnius_service/household_members/endpoint_household_members';
UPDATE `omnius_osf1`.`core_config_data` SET `value` = 'http://deossbvw.dc-ratingen.de:8008/Middleware.OIL.I14_GetHouseHoldMembers.Wcf/WcfService_Middleware_OIL_I14_GetHouseHoldMembers_App.svc?wsdl' WHERE `path` = 'omnius_service/household_members/wsdl_household_members';

#ENDPOINT FOR OBJECT_SEARCH => OIL
UPDATE `omnius_osf1`.`core_config_data` SET `value` = 'http://deossbvw.dc-ratingen.de:8008/Middleware.OIL.I16_ObjectSearch.Wcf/WcfService_Middleware_OIL_I16_ObjectSearch_App.svc' WHERE `path` = 'omnius_service/object_search/usage_url';
UPDATE `omnius_osf1`.`core_config_data` SET `value` = 'http://deossbvw.dc-ratingen.de:8008/Middleware.OIL.I16_ObjectSearch.Wcf/WcfService_Middleware_OIL_I16_ObjectSearch_App.svc?wsdl' WHERE `path` = 'omnius_service/object_search/wsdl';
