#!/bin/bash

#ZEND SETUP VARIABLES
zpack="/usr/local/zend/bin/zdpack"
zend="/var/SP/data/dynalean/Zend"
testing=true
createDatabaseBackup=false

# ENVIRONMENTLOCATIONS FOR PACKAGES
osePackageInput="/var/SP/data/docroots/osf-telesales-sit3.vodafone.de/"
uctPackageInput="/var/SP/data/docroots/osf-api-sit3.vodafone.de/uct/"
umPackageInput="/var/SP/data/docroots/osf-api-sit3.vodafone.de/um/"

echo "Do you want to create a database backup for the package? y/n"
	read CREATEBACKUP

	if [ ${CREATEBACKUP} = "y" ]; then
	    createDatabaseBackup=true
        fi

# SETUP TESTING VARIABLES (IF USED)
if $testing; then
   echo "Testing mode initialised"
   zend="."
   osePackageInput="./test/ose/"
   uctPackageInput="./test/uct/"
   umPackageInput="./test/um/"
fi

if $createDatabaseBackup; then 
   databaseCredentialsOK=false
   while [[ $databaseCredentialsOK != true ]]
   do
   	echo "DATABASE HOST:"
   	read DATABASE_HOST

   	echo "DATABASE USERNAME:"
   	read DATABASE_USERNAME

   	echo "DATABASE PASSWORD:"
   	read -s DATABASE_PASSWORD

   	echo "DATABASE NAME:"
   	read DATABASE_NAME

	echo "Are the database entries correct? y/n"
	read DATABASEOK

	if [ ${DATABASEOK} = "y" ]; then
	    databaseCredentialsOK=true
        fi
   done
   
   echo "Generating database backup"	
   mysqldump -u${DATABASE_USERNAME} -h${DATABASE_HOST} --default-character-set=utf8 -p${DATABASE_PASSWORD} ${DATABASE_NAME} | gzip > $zend/backup.sql.gz
  
   echo "Backup generation done"	
fi

outputDirectory="$zend/output"

#PACKAGE SKELETON DIRECTORIES
osePackageDirectory="$zend/skeletons/ose"
uctPackageDirectory="$zend/skeletons/uct"
umPackageDirectory="$zend/skeletons/um"

#PACKAGF OUTPUT DIRECTORIES
oseOutputFile="$outputDirectory/ose.zpk"
uctOutputFile="$outputDirectory/uct.zpk"
umOutputFile="$outputDirectory/um.zpk"

excludeLogFolder=true

echo "===========OSE=================="

if [[ ! -d $outputDirectory ]]; then
 echo "Creating output directory '$outputDirectory'"  
 mkdir $outputDirectory
fi

if [[ -f $oseOuputFile ]] ; then
    echo "Removing old OSE package '$oseOutputFile'"
    rm $oseOutputFile
fi

if [[ -d $osePackageDirectory/data ]] ; then
    echo "Remove old OSE data from '$osePackageDirectory/data'"
    rm -rf $osePackageDirectory/data/*
fi

if [[ -d $osePackageInput ]] ; then
    copyCommand="${osePackageInput} ${osePackageDirectory}/data/"
    excludeCommand=""

    if $excludeLogFolder; then
         echo "Copying is set to exclude the logging,report,import, call and session directory"
         excludeCommand=" --exclude=var/log --exclude=var/report --exclude=var/session --exclude=var/import --exclude=media/calls"
    fi

    echo "Copying OSE from '$osePackageInput' to '$osePackageDirectory/data'"  
    rsync -a -f "+ */" -f"- *" ${copyCommand}
    rsync -avr ${copyCommand}${excludeCommand} > /dev/null

    if $createDatabaseBackup; then
        echo "Copying MYSQL binary to OSE skeleton"
        cp -a /bin/mysql $osePacakageDirectory/data/var

        echo "Moving DB backup to OSE skeleton"
        mv $zend/backup.sql.gz $osePackageDirectory/data/var
    fi
fi


if [ -f $osePackageDirectory/data/app/etc/SOFTWARE_VERSION.php ]; then
  build=$(grep -oP 'B[0-9]+' $osePackageDirectory/data/app/etc/SOFTWARE_VERSION.php)
fi

if [ -f $osePackageDirectory/deployment.xml ]; then
  sed -i "s/<release>.*<\/release>/<release>${build}<\/release>/g" $osePackageDirectory/deployment.xml
fi

if $testing; then
   echo "OSE package is not generated because of testing mode"
else
   echo "Generating new OSE package"
   $zpack pack $osePackageDirectory --output-dir=$outputDirectory
fi

echo "===========UCT API=================="

if [[ -f $uctOutputFile ]] ; then
   echo "Removing old UCT package '$uctOutputFile'"    
   rm $uctOutputFile
fi

if [[ -d $uctPackageDirectory/data ]] ; then
    echo "Remove old UCT data from '$uctPackageDirectory/data'"
    rm -rf $uctPackageDirectory/data/*
fi

if [[ -d $uctPackageInput ]] ; then
    copyCommand="${uctPackageInput} ${uctPackageDirectory}/data/"
    excludeCommand=""

    if $excludeLogFolder; then
         echo "Copying is set to exclude the logging and tests directory"
         excludeCommand=" --exclude=storage/logs --exclude=tests"
    fi

    echo "Copying UCT from '$uctPackageInput' to '$uctPackageDirectory/data'"  
    rsync -a -f "+ */" -f"- *" ${copyCommand}
    rsync -avr ${copyCommand}${excludeCommand} > /dev/null
fi

if $testing; then
   echo "UCT package is not generated because of testing mode"
else
   echo "Generating new UCT package"
   $zpack pack $uctPackageDirectory --output-dir=$outputDirectory
fi

echo "===========USER MANAGEMENT API=================="

if [[ -f $umOutputFile ]] ; then
   echo "Removing old UM package '$umOutputFile'"    
   rm $umOutputFile
fi

if [[ -d $umPackageDirectory/data ]] ; then
    echo "Remove old UM data from '$umPackageDirectory/data'"
    rm -rf $umPackageDirectory/data/*
fi

if [[ -d $umPackageInput ]] ; then
    copyCommand="${umPackageInput} ${umPackageDirectory}/data/"
    excludeCommand=""

    if $excludeLogFolder; then
         echo "Copying is set to exclude the logging and tests directory"
         excludeCommand=" --exclude=storage/logs --exclude=tests"
    fi

    echo "Copying UM from '$umPackageInput' to '$umPackageDirectory/data'"  
    rsync -a -f "+ */" -f"- *" ${copyCommand}
    rsync -avr ${copyCommand}${excludeCommand} > /dev/null
fi

if $testing; then
   echo "UM package is not generated because of testing mode"
else
   echo "Generating new UM package"
   $zpack pack $umPackageDirectory --output-dir=$outputDirectory
fi

echo "Generating tar file of the output contents"

tar -czvf packages.tar.gz -C $outputDirectory .

echo "Done"
