<?php
/* The script post_stage.php will be executed after the staging process ends. This will allow
 * users to perform some actions on the source tree or server before an attempt to
 * activate the app is made. For example, this will allow creating a new DB schema
 * and modifying some file or directory permissions on staged source files
 * The following environment variables are accessable to the script:
 * 
 * - ZS_RUN_ONCE_NODE - a Boolean flag stating whether the current node is
 *   flagged to handle "Run Once" actions. In a cluster, this flag will only be set when
 *   the script is executed on once cluster member, which will allow users to write
 *   code that is only executed once per cluster for all different hook scripts. One example
 *   for such code is setting up the database schema or modifying it. In a
 *   single-server setup, this flag will always be set.
 * - ZS_WEBSERVER_TYPE - will contain a code representing the web server type
 *   ("IIS" or "APACHE")
 * - ZS_WEBSERVER_VERSION - will contain the web server version
 * - ZS_WEBSERVER_UID - will contain the web server user id
 * - ZS_WEBSERVER_GID - will contain the web server user group id
 * - ZS_PHP_VERSION - will contain the PHP version Zend Server uses
 * - ZS_APPLICATION_BASE_DIR - will contain the directory to which the deployed
 *   application is staged.
 * - ZS_CURRENT_APP_VERSION - will contain the version number of the application
 *   being installed, as it is specified in the package descriptor file
 * - ZS_PREVIOUS_APP_VERSION - will contain the previous version of the application
 *   being updated, if any. If this is a new installation, this variable will be
 *   empty. This is useful to detect update scenarios and handle upgrades / downgrades
 *   in hook scripts
 */

$appLocation = getenv('ZS_APPLICATION_BASE_DIR');

$logFile = $appLocation.'/storage/logs/lumen.log';
$envFile = $appLocation.'/.env';


//create log file if necessary
if (!file_exists($logFile)) {
	touch($logFile);
}

//set storage permission
$iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($appLocation.'/storage'));
foreach($iterator as $item) {
    chmod($item, 0777);
}

//create config file
$config = array(
	'APP_ENV='.getenv('ZS_APP_ENV'),
	'APP_DEBUG='.getenv('ZS_APP_DEBUG'),
	'APP_KEY='.getenv('ZS_APP_KEY'),

	'DB_CONNECTION='.getenv('ZS_DB_CONNECTION'),
	'DB_HOST='.getenv('ZS_DB_HOST'),
	'DB_PORT='.getenv('ZS_DB_PORT'),
	'DB_DATABASE='.getenv('ZS_DB_DATABASE'),
	'DB_USERNAME='.getenv('ZS_DB_USERNAME'),
	'DB_PASSWORD='.getenv('ZS_DB_PASSWORD'),

	'REDIS_CLUSTER='.getenv('ZS_REDIS_CLUSTER'),
	'REDIS_HOST='.getenv('ZS_REDIS_HOST'),
	'REDIS_PORT='.getenv('ZS_REDIS_PORT'),
	'REDIS_DATABASE='.getenv('ZS_REDIS_DATABASE'),
	'REDIS_PASSWORD='.getenv('ZS_REDIS_PASSWORD'),

	'CACHE_DRIVER='.getenv('ZS_CACHE_DRIVER'),
	'QUEUE_DRIVER='.getenv('ZS_QUEUE_DRIVER'),

	'DIGEST_REALM='.getenv('ZS_DIGEST_REALM'),
	'DIGEST_USER='.getenv('ZS_DIGEST_USER'),
	'DIGEST_PASS='.getenv('ZS_DIGEST_PASS'),

	'MAGENTO_PASSWORD_SALT='.getenv('ZS_MAGENTO_PASSWORD_SALT'),
);

if (file_exists($envFile)) {
    unlink($envFile);
}

touch($envFile);
file_put_contents($envFile, implode("\n", $config));
