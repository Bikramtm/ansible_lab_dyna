<?php
/* The script post_activate.php allow users to "clean up" after a new version of the application
 * is activated, for example by removing a "Down for Maintenance" message
 * set in Pre-activate.
 * The following environment variables are accessable to the script:
 * 
 * - ZS_RUN_ONCE_NODE - a Boolean flag stating whether the current node is
 *   flagged to handle "Run Once" actions. In a cluster, this flag will only be set when
 *   the script is executed on once cluster member, which will allow users to write
 *   code that is only executed once per cluster for all different hook scripts. One example
 *   for such code is setting up the database schema or modifying it. In a
 *   single-server setup, this flag will always be set.
 * - ZS_WEBSERVER_TYPE - will contain a code representing the web server type
 *   ("IIS" or "APACHE")
 * - ZS_WEBSERVER_VERSION - will contain the web server version
 * - ZS_WEBSERVER_UID - will contain the web server user id
 * - ZS_WEBSERVER_GID - will contain the web server user group id
 * - ZS_PHP_VERSION - will contain the PHP version Zend Server uses
 * - ZS_APPLICATION_BASE_DIR - will contain the directory to which the deployed
 *   application is staged.
 * - ZS_CURRENT_APP_VERSION - will contain the version number of the application
 *   being installed, as it is specified in the package descriptor file
 * - ZS_PREVIOUS_APP_VERSION - will contain the previous version of the application
 *   being updated, if any. If this is a new installation, this variable will be
 *   empty. This is useful to detect update scenarios and handle upgrades / downgrades
 *   in hook scripts
 */  


// Create connection
$conn = new mysqli(getenv('ZS_DB_HOST'), getenv('ZS_DB_USERNAME'), getenv('ZS_DB_PASSWORD'), getenv('ZS_DB_NAME'));

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

# Set webroot
$sql1 = "UPDATE `".getenv('ZS_DB_NAME')."`.`core_config_data` SET `value` = '".getenv('ZS_BASE_URL_SECURE')."' WHERE `path` = 'web/unsecure/base_url'";
$sql2 = "UPDATE `".getenv('ZS_DB_NAME')."``core_config_data` SET `value` = '".getenv('ZS_BASE_URL_UNSECURE')."' WHERE `path` = 'web/secure/base_url'";
# set the product visibility enabled
$sql3 = "UPDATE `core_config_data` SET `value` = 1 WHERE `path` = 'catalog/frontend/product_visibility_enabled'";
#DISABLE STUB USAGE
$sql4 = "UPDATE `core_config_data` SET `value` = 0 WHERE `path` = 'omnius_service/general/use_stubs'";
#SET OGW AS PARTYNAME
$sql5 = "UPDATE `core_config_data` SET `value` = 'OGW' WHERE `path` = 'omnius_service/service_settings_header/party_name'";

#change password
$sql6 = "UPDATE `admin_user` SET `password` = 'c71fba4ed840df3e6c19d5e0b67335a3:BawaVeKuXHrdth1qY1sabUofS72A3vmY' WHERE `username` = 'admin'";

$conn->query($sql1);
$conn->query($sql2);
$conn->query($sql3);
$conn->query($sql4);
$conn->query($sql5);
$conn->query($sql6);

$conn->close();
