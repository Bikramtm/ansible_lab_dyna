#!/bin/bash

PIDS=`ps aux | grep $1 | grep -v grep | awk '{ print $2 }'`

for PID in $PIDS
do
    pwdx $PID
done

