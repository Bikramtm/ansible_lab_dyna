##############################
## UPDATE SCRIPT FOR ENDPOINTS
### ST10 ###
##############################

# Set webroot
UPDATE `omnius_vfde_st10`.`core_config_data` SET `value` = 'http://telesales-omnius-vf-de.st10.ynad.biz/' WHERE `path` = 'web/unsecure/base_url';
UPDATE `omnius_vfde_st10`.`core_config_data` SET `value` = 'http://telesales-omnius-vf-de.st10.ynad.biz/' WHERE `path` = 'web/secure/base_url';

# set the product visibility enabled
UPDATE `omnius_vfde_st10`.`core_config_data` SET `value` = 1 WHERE `path` = 'catalog/frontend/product_visibility_enabled';

#DISABLE STUB USAGE
UPDATE `omnius_vfde_st10`.`core_config_data` SET `value` = 0 WHERE `path` = 'omnius_service/general/use_stubs';

#SET OGW AS PARTYNAME
UPDATE `omnius_vfde_st10`.`core_config_data` SET `value` = 'OGW' WHERE `path` = 'omnius_service/service_settings_header/party_name';

#WSDL FOR POTENTIAL LINKS => OIL
UPDATE `omnius_vfde_st10`.`core_config_data` SET `value` = 'http://w12vfdbiz102-t/Middleware.VFDE.I1001_SearchPotentialLinks.Wcf/WcfService_Middleware_VFDE_I1001_SearchPotentialLinks_App.svc?wsdl' WHERE `path` = 'omnius_service/potential_links/wsdl_potential_links';

#ENDPOINT FOR POTENTIAL LINKS => OIL
UPDATE `omnius_vfde_st10`.`core_config_data` SET `value` = 'http://w12vfdbiz102-t/Middleware.VFDE.I1001_SearchPotentialLinks.Wcf/WcfService_Middleware_VFDE_I1001_SearchPotentialLinks_App.svc' WHERE `path` = 'omnius_service/potential_links/endpoint_potential_links';

#WSDL FOR SEND DOCUMENT => OIL
UPDATE `omnius_vfde_st10`.`core_config_data` SET `value` = 'http://w12vfdbiz102-t/Middleware.VFDE.I1011_SendDocument.Wcf/WcfService_Middleware_VFDE_I1011_SendDocument_App.svc?wsdl' WHERE `path` = 'omnius_service/send_document/wsdl_send_document';

#ENDPOINT FOR SEND DOCUMENT => OIL
UPDATE `omnius_vfde_st10`.`core_config_data` SET `value` = 'http://w12vfdbiz102-t/Middleware.VFDE.I1011_SendDocument.Wcf/WcfService_Middleware_VFDE_I1011_SendDocument_App.svc' WHERE `path` = 'omnius_service/send_document/endpoint_send_document';

#WSDL FOR CREATE MANUAL WORKITEM => OIL
UPDATE `omnius_vfde_st10`.`core_config_data` SET `value` = 'http://w12vfdbiz102-t/Middleware.OIL.I13_CreateManualWorkItem.Wcf/WcfService_Middleware_OIL_I13_CreateManualWorkItem_App.svc?wsdl' WHERE `path` = 'omnius_service/create_manual_work_item/wsdl_create_manual_work_item';

#ENDPOINT FOR CREATE MANUAL WORKITEM => OIL
UPDATE `omnius_vfde_st10`.`core_config_data` SET `value` = 'http://w12vfdbiz102-t/Middleware.OIL.I13_CreateManualWorkItem.Wcf/WcfService_Middleware_OIL_I13_CreateManualWorkItem_App.svc' WHERE `path` = 'omnius_service/create_manual_work_item/endpoint_create_manual_work_item';

#WSDL FOR SEARCHCUSTOMER => OIL
UPDATE `omnius_vfde_st10`.`core_config_data` SET `value` = 'http://w12vfdbiz102-t/Middleware.OIL.I01_SearchCustomer.Wcf/WcfService_Middleware_OIL_I01_SearchCustomer_App.svc?wsdl' WHERE `path` = 'omnius_service/customer_search/wsdl';

#ENDPOINT FOR SEARCHCUSTOMER USAGE URL => OIL
UPDATE `omnius_vfde_st10`.`core_config_data` SET `value` = 'http://w12vfdbiz102-t/Middleware.OIL.I01_SearchCustomer.Wcf/WcfService_Middleware_OIL_I01_SearchCustomer_App.svc' WHERE `path` = 'omnius_service/customer_search/usage_url';

#WSDL FOR SEARCHCUSTOMERFROMLEGACY => OIL
UPDATE `omnius_vfde_st10`.`core_config_data` SET `value` = 'http://w12vfdbiz102-t/Middleware.VFDE.I1003_SearchCustomerFromLegacy.Wcf/WcfService_Middleware_VFDE_I1003_SearchCustomerFromLegacy_App.svc?wsdl' WHERE `path` = 'omnius_service/customer_search_legacy/wsdl';

#ENDPOINT FOR SEARCHCUSTOMERFROMLEGACY USAGE URL => OIL
UPDATE `omnius_vfde_st10`.`core_config_data` SET `value` = 'http://w12vfdbiz102-tMiddleware.VFDE.I1003_SearchCustomerFromLegacy.Wcf/WcfService_Middleware_VFDE_I1003_SearchCustomerFromLegacy_App.svc' WHERE `path` = 'omnius_service/customer_search_legacy/usage_url';

#WSDL FOR RETRIEVECUSTOMERINFO => OIL
UPDATE `omnius_vfde_st10`.`core_config_data` SET `value` = 'http://w12vfdbiz102-t/Middleware.OIL.I07_RetrieveCustomerInfo.Wcf/WcfService_Middleware_OIL_I07_RetrieveCustomerInfo_App.svc?wsdl' WHERE `path` = 'omnius_service/customer_info/wsdl_customer_info';

#ENDPOINT FOR RETRIEVECUSTOMERINFO => OIL
UPDATE `omnius_vfde_st10`.`core_config_data` SET `value` = 'http://w12vfdbiz102-t/Middleware.OIL.I07_RetrieveCustomerInfo.Wcf/WcfService_Middleware_OIL_I07_RetrieveCustomerInfo_App.svc' WHERE `path` = 'omnius_service/customer_info/endpoint_customer_info';

#WSDL FOR RETRIEVECREDITPROFILE => OIL
UPDATE `omnius_vfde_st10`.`core_config_data` SET `value` = 'http://w12vfdbiz102-t/Middleware.OIL.I02_RetrieveCustomerCreditProfile.Wcf/WcfService_Middleware_OIL_I02_RetrieveCustomerCreditProfile_App.svc?wsdl' WHERE `path` = 'omnius_service/customer_credit_profile/wsdl_customer_credit_profile';

#ENDPOINT FOR RETRIEVECREDITPROFILE => OIL
UPDATE `omnius_vfde_st10`.`core_config_data` SET `value` = 'http://w12vfdbiz102-t/Middleware.OIL.I02_RetrieveCustomerCreditProfile.Wcf/WcfService_Middleware_OIL_I02_RetrieveCustomerCreditProfile_App.svc' WHERE `path` = 'omnius_service/customer_credit_profile/endpoint_customer_credit_profile';

#WSDL FOR CREATEORADDLINK => OIL
UPDATE `omnius_vfde_st10`.`core_config_data` SET `value` = 'http://w12vfdbiz102-t/Middleware.VFDE.I1002_CreateOrAddLink.Wcf/WcfService_Middleware_VFDE_I1002_CreateOrAddLink_App.svc?wsdl' WHERE `path` = 'omnius_service/create_add_link/wsdl_create_add_link';

#ENDPOINT FOR CREATEORADDLINK => OIL
UPDATE `omnius_vfde_st10`.`core_config_data` SET `value` = 'http://w12vfdbiz102-t/Middleware.VFDE.I1002_CreateOrAddLink.Wcf/WcfService_Middleware_VFDE_I1002_CreateOrAddLink_App.svc?wsdl' WHERE `path` = 'omnius_service/create_add_link/endopoint_create_add_link';

#WSDL FOR INVALIDPRODUCTS => OGW
UPDATE `omnius_vfde_st10`.`core_config_data` SET `value` = 'http://w12vfdbiz102-t/DynaLeanStub/wsdl/OGW_GetInvalidProducts_WS.wsdl' WHERE `path` = 'omnius_service/invalid_products/wsdl_invalid_products';

#ENDPOINT FOR INVALIDPRODUCTS => OGW
UPDATE `omnius_vfde_st10`.`core_config_data` SET `value` = 'http://w12vfdbiz102-t/DynaLeanStub/wsdl/OGW_GetInvalidProducts_WS.wsdl' WHERE `path` = 'omnius_service/invalid_products/endpoint_invalid_products';

#WSDL FOR GETIPEQUIPMENTFEATURES => OGW
UPDATE `omnius_vfde_st10`.`core_config_data` SET `value` = 'http://w12vfdbiz102-t/DynaLeanStub/wsdl/OGW_GetIpEquipmentFeatures_WS.wsdl' WHERE `path` = 'omnius_service/ip_equipment_features/wsdl_ip_equipment_features';

#ENDPOINT FOR GETIPEQUIPMENTFEATURES => OGW
UPDATE `omnius_vfde_st10`.`core_config_data` SET `value` = 'http://w12vfdbiz102-t/DynaLeanStub/wsdl/OGW_GetIpEquipmentFeatures_WS.wsdl' WHERE `path` = 'omnius_service/ip_equipment_features/endpoint_ip_equipment_features';

#WSDL FOR NONSTANDARDRATE => OGW
UPDATE `omnius_vfde_st10`.`core_config_data` SET `value` = 'http://w12vfdbiz102-t/DynaLeanStub/wsdl/OGW_GetNonStandardRates_WS.wsdl' WHERE `path` = 'omnius_service/non_standard_rate/wsdl_non_standard_rate';

#ENDPOINT FOR NONSTANDARDRATE => OGW
UPDATE `omnius_vfde_st10`.`core_config_data` SET `value` = 'http://w12vfdbiz102-t/DynaLeanStub/wsdl/OGW_GetNonStandardRates_WS.wsdl' WHERE `path` = 'omnius_service/non_standard_rate/endpoint_non_standard_rate';

#WSDL FOR GETSMARTCARDCOMPATIBILITY => OGW
UPDATE `omnius_vfde_st10`.`core_config_data` SET `value` = 'http://w12vfdbiz102-t/DynaLeanStub/wsdl/OGW_GetSmartCardCompatibility_WS.wsdl' WHERE `path` = 'omnius_service/smart_card_compatibility/wsdl_smart_card_compatibility';

#ENDPOINT FOR GETSMARTCARDCOMPATIBILITY => OGW
UPDATE `omnius_vfde_st10`.`core_config_data` SET `value` = 'http://w12vfdbiz102-t/DynaLeanStub/wsdl/OGW_GetSmartCardCompatibility_WS.wsdl' WHERE `path` = 'omnius_service/smart_card_compatibility/endpoint_smart_card_compatibility';

#WSDL FOR GETSMARTCARDTYPE => OGW
UPDATE `omnius_vfde_st10`.`core_config_data` SET `value` = 'http://w12vfdbiz102-t/DynaLeanStub/wsdl/OGW_GetSmartCardTypeWS.wsdl' WHERE `path` = 'omnius_service/smart_card_type/wsdl_smart_card_type';

#ENDPOINT FOR GETSMARTCARDTYPE => OGW
UPDATE `omnius_vfde_st10`.`core_config_data` SET `value` = 'http://w12vfdbiz102-t/DynaLeanStub/wsdl/OGW_GetSmartCardTypeWS.wsdl' WHERE `path` = 'omnius_service/smart_card_type/endpoint_smart_card_type';

#WSDL FOR GETTVEQUIPMENTFEATURES => OGW
UPDATE `omnius_vfde_st10`.`core_config_data` SET `value` = 'http://w12vfdbiz102-t/DynaLeanStub/wsdl/OGW_GetTvEquipmentFeatures_WS.wsdl' WHERE `path` = 'omnius_service/tv_equipment_features/wsdl_tv_equipment_features';

#ENDPOINT FOR GETTVEQUIPMENTFEATURES ENDPOINT => OGW
UPDATE `omnius_vfde_st10`.`core_config_data` SET `value` = 'http://w12vfdbiz102-t/DynaLeanStub/wsdl/OGW_GetTvEquipmentFeatures_WS.wsdl' WHERE `path` = 'omnius_service/tv_equipment_features/endpoint_tv_equipment_features';

#WSDL FOR GETCONFIGURATIONDATA => OGW
UPDATE `omnius_vfde_st10`.`core_config_data` SET `value` = 'http://w12vfdbiz102-t/DynaLeanStub/wsdl/OGW_GetConfigurationData_WS.wsdl' WHERE `path` = 'omnius_service/get_configuration_data/wsdl_get_configuration_data';

#ENDPOINT FOR GETCONFIGURATIONDATA USAGE URL => OGW
UPDATE `omnius_vfde_st10`.`core_config_data` SET `value` = 'http://w12vfdbiz102-t/DynaLeanStub/wsdl/OGW_GetConfigurationData_WS.wsdl' WHERE `path` = 'omnius_service/get_configuration_data/endpoint_get_configuration_data';

#WSDL FOR SEARCHPOTENTIALLINKS => OIL
UPDATE `omnius_vfde_st10`.`core_config_data` SET `value` = 'http://w12vfdbiz102-t/Middleware.VFDE.I1001_SearchPotentialLinks.Wcf/WcfService_Middleware_VFDE_I1001_SearchPotentialLinks_App.svc?wsdl' WHERE `path` = 'omnius_service/service_ability/wsdl_potential_links';

#WSDL FOR SERVICEABILITY => OIL
UPDATE `omnius_vfde_st10`.`core_config_data` SET `value` = 'http://w12vfdbiz102-t/Middleware.OIL.I08_CheckServiceAbility.Wcf/WcfService_Middleware_OIL_I08_CheckServiceAbility_App.svc?wsdl' WHERE `path` = 'omnius_service/service_ability/wsdl';

#ENDPOINT FOR SERVICEABILITY => OIL
UPDATE `omnius_vfde_st10`.`core_config_data` SET `value` = 'http://w12vfdbiz102-t/Middleware.OIL.I08_CheckServiceAbility.Wcf/WcfService_Middleware_OIL_I08_CheckServiceAbility_App.svc' WHERE `path` = 'omnius_service/service_ability/endpoint';

#WSDL FOR VALIDATEADDRESS => OIL
UPDATE `omnius_vfde_st10`.`core_config_data` SET `value` = 'http://w12vfdbiz102-t/Middleware.OIL.I03_ValidateAddress.Wcf/WcfService_Middleware_OIL_I03_ValidateAddress_App.svc?wsdl' WHERE `path` = 'omnius_service/validate_address/wsdl';

#ENDPOINT FOR VALIDATEADDRESS => OIL
UPDATE `omnius_vfde_st10`.`core_config_data` SET `value` = 'http://w12vfdbiz102-t/Middleware.OIL.I03_ValidateAddress.Wcf/WcfService_Middleware_OIL_I03_ValidateAddress_App.svc?wsdl' WHERE `path` = 'omnius_service/validate_address/endpoint';

#WSDL FOR CONVERTORVALIDATEIBAN => OIL
UPDATE `omnius_vfde_st10`.`core_config_data` SET `value` = 'http://w12vfdbiz102-t/Middleware.OIL.I04_ConvertAndValidateIBAN.Wcf/WcfService_Middleware_OIL_I04_ConvertAndValidateIBAN_App.svc?wsdl' WHERE `path` = 'omnius_service/convert_iban/wsdl_convert_iban';

#ENDPOINT FOR CONVERTORVALIDATEIBAN => OIL
UPDATE `omnius_vfde_st10`.`core_config_data` SET `value` = 'http://w12vfdbiz102-t/Middleware.OIL.I04_ConvertAndValidateIBAN.Wcf/WcfService_Middleware_OIL_I04_ConvertAndValidateIBAN_App.svc' WHERE `path` = 'omnius_service/convert_iban/usage_url';

#WSDL FOR GETPAYERANDPAYMENTINFOR => OIL
UPDATE `omnius_vfde_st10`.`core_config_data` SET `value` = 'http://w12vfdbiz102-t/Middleware.VFDE.I1004_GetPayerAndPaymentInfo.Wcf/WcfService_Middleware_VFDE_I1004_GetPayerAndPaymentInfo_App.svc?wsdl' WHERE `path` = 'omnius_service/payer_and_payment_info/wsdl';

#ENDPOINT FOR GETPAYERANDPAYMENTINFOR => OIL
UPDATE `omnius_vfde_st10`.`core_config_data` SET `value` = 'http://w12vfdbiz102-t/Middleware.VFDE.I1004_GetPayerAndPaymentInfo.Wcf/WcfService_Middleware_VFDE_I1004_GetPayerAndPaymentInfo_App.svc' WHERE `path` = 'omnius_service/payer_and_payment_info/endpoint';

#WSDL FOR SUBMITORDER => OIL
UPDATE `omnius_vfde_st10`.`core_config_data` SET `value` = 'http://w12vfdbiz102-t/Middleware.OIL.I09_SubmitOrder.Wcf/WcfService_Middleware_OIL_I09_SubmitOrder_App.svc?wsdl' WHERE `path` = 'omnius_service/process_order/wsdl_process_order';

#ENDPOINT FOR SUBMITORDER => OIL
UPDATE `omnius_vfde_st10`.`core_config_data` SET `value` = 'http://w12vfdbiz102-t/Middleware.OIL.I09_SubmitOrder.Wcf/WcfService_Middleware_OIL_I09_SubmitOrder_App.svc' WHERE `path` = 'omnius_service/process_order/endpoint_process_order';

#WSDL FOR SHIPPINGCONDITION => OIL => SAP
UPDATE `omnius_vfde_st10`.`core_config_data` SET `value` = 'http://w12vfdbiz102-t/Middleware.OIL.I05_ShippingCondition.Wcf/WcfService_Middleware_OIL_I05_ShippingCondition_App.svc?wsdl' WHERE `path` = 'omnius_service/sap_shipping_condition/wsdl';

#ENDPOINT FOR SHIPPINGCONDITION => OIL => SAP
UPDATE `omnius_vfde_st10`.`core_config_data` SET `value` = 'http://w12vfdbiz102-t/Middleware.OIL.I05_ShippingCondition.Wcf/WcfService_Middleware_OIL_I05_ShippingCondition_App.svc' WHERE `path` = 'omnius_service/sap_shipping_condition/usage_url';

#WSDL FOR SHIPPINGFEECALCULATE => OIL => SAP
UPDATE `omnius_vfde_st10`.`core_config_data` SET `value` = 'http://w12vfdbiz102-t/Middleware.OIL.I06_ShippingFeeCalculate.Wcf/WcfService_Middleware_OIL_I06_ShippingFeeCalculate_App.svc?wsdl' WHERE `path` = 'omnius_service/shipping_fee/wsdl';
