<?php
/**
 * Usage: POST: URL/creditcheck.php
 * Response Ex.:
 * {
 *  "data": {
 *      "superorder_status": false,
 *      "1": {
 *          "performed": false,
 *          "customer_ban": "35533322",
 *          "package_esb_number": null,
 *          "package_order_number": "21086.1"
 *      }
 *  },
 *  "error": false
 * }
 */
namespace creditcheck;

date_default_timezone_set("Europe/Amsterdam");

//for potential future use
//$date = new \DateTime();
//$date = $date->format('Y-m-d');
//$salt = 'gj940gGRE4hdtfmv9D43490';
//$token = md5($date . $salt);

if(!isset($_COOKIE['frontend'])) {
    header('HTTP/1.0 403 Forbidden');
    echo 'Forbidden!';
    die;
}

if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

    define(__NAMESPACE__ . '\SO_VALIDATED', 'Validation approved');
    define(__NAMESPACE__ . '\SO_WAITING_FOR_VALIDATION', 'Waiting for Validation');
    define(__NAMESPACE__ . '\SO_PRE_INITIAL', 'In initial queue');
    define(__NAMESPACE__ . '\SO_INITIAL', 'Waiting for validation: processing started');
    define(__NAMESPACE__ . '\SO_WAITING_VALIDATION_ACCEPTED_AXI', 'Waiting for validation: accepted by AXI');
    define(__NAMESPACE__ . '\SO_STATUS_REFERRED', 'Validation Referred');
    define(__NAMESPACE__ . '\SO_FULFILL_PROGRESS', 'Fulfillment in progress');
    define(__NAMESPACE__ . '\SO_FULFILLED', 'Fulfilled');
    define(__NAMESPACE__ . '\SO_VALIDATED_PARTIALLY', 'Validation Partially Approved');
    define(__NAMESPACE__ . '\SO_CANCELLED', 'Cancelled');

    define(__NAMESPACE__ . '\ESB_PACKAGE_STATUS_FAILED', 'Validation Failed');

    define(__NAMESPACE__ . '\ESB_PACKAGE_NETWORK_STATUS_CC_PENDING', 'PENDING');
    define(__NAMESPACE__ . '\ESB_PACKAGE_NETWORK_STATUS_CC_APPROVED', 'APPROVED');
    define(__NAMESPACE__ . '\ESB_PACKAGE_NETWORK_STATUS_CC_REFERRED', 'REFERRED');
    define(__NAMESPACE__ . '\ESB_PACKAGE_NETWORK_STATUS_CC_PARTIAL', 'PARTIAL');
    define(__NAMESPACE__ . '\ESB_PACKAGE_NETWORK_STATUS_CC_ADDITIONNAL_INFO_REQUIRED', 'ADDITIONNAL_INFO_REQUIRED');
    define(__NAMESPACE__ . '\ESB_PACKAGE_NETWORK_STATUS_CC_REJECTED', 'REJECTED');
    define(__NAMESPACE__ . '\ESB_PACKAGE_NETWORK_STATUS_CC_VALIDATION_FAILED', 'VALIDATION_FAILED');
    define(__NAMESPACE__ . '\ESB_PACKAGE_NETWORK_STATUS_CC_INITIAL', 'INITIAL');

    define(__NAMESPACE__ . '\PACKAGE_SUCCESS', 'success');
    define(__NAMESPACE__ . '\PACKAGE_WAITING', 'waiting');
    define(__NAMESPACE__ . '\PACKAGE_FAILED', 'failed');
    define(__NAMESPACE__ . '\PACKAGE_REFERRED', 'referred');
    define(__NAMESPACE__ . '\PACKAGE_PARTIAL', 'partial');
    define(__NAMESPACE__ . '\MANUAL_ACTIVATION', 'manual_activation');
    define(__NAMESPACE__ . '\PACKAGE_REJECTED', 'rejected');

    $databaseConfigXmlPath = realpath(dirname(__FILE__)) . '/app/etc/local.xml';
    // Default language is NL
    $translateCsvPath = realpath(dirname(__FILE__)) . '/app/locale/nl_NL/Vznl_Checkout.csv';

    /**
     * Translate function. Uses the file specified by $translateCsvPath
     * @param string $key
     * @return string mixed
     */
    function __($key)
    {
        global $translateCsvPath;
        $result = $key;
        if (($handle = fopen($translateCsvPath, "r")) !== false) {
            while (($data = fgetcsv($handle, 1000, ",")) !== false) {
                if ($data[0] == $key) {
                    $result = $data[1];
                    break;
                }
            }
            fclose($handle);
        }

        return $result;
    }

    /**
     * Check the status of a package from the current quote after it has been turned into an order
     * @param object $package
     * @return bool
     */
    function validateCreditCheck($package)
    {
        $manualActivation = $package->EsbManualActivation || $package->ManualActivationReason;
        if (!$package || $package->CreditcheckStatus == ESB_PACKAGE_NETWORK_STATUS_CC_PENDING || $package->CreditcheckStatus == ESB_PACKAGE_NETWORK_STATUS_CC_INITIAL) {
            if ($package->Status == ESB_PACKAGE_STATUS_FAILED || $package->Status == SO_CANCELLED) {
                return $manualActivation ? MANUAL_ACTIVATION : PACKAGE_FAILED;
            } else {
                return false;
            }
        } elseif ($package->CreditcheckStatus == ESB_PACKAGE_NETWORK_STATUS_CC_APPROVED) {
            return PACKAGE_SUCCESS;
        } elseif ($manualActivation) {
            return MANUAL_ACTIVATION;
        } elseif ($package->CreditcheckStatus == ESB_PACKAGE_NETWORK_STATUS_CC_VALIDATION_FAILED) {
            return PACKAGE_FAILED;
        } elseif (in_array($package->CreditcheckStatus, [ESB_PACKAGE_NETWORK_STATUS_CC_REFERRED, ESB_PACKAGE_NETWORK_STATUS_CC_ADDITIONNAL_INFO_REQUIRED])) {
            return PACKAGE_REFERRED;
        } elseif ($package->CreditcheckStatus === ESB_PACKAGE_NETWORK_STATUS_CC_PARTIAL) {
            return PACKAGE_PARTIAL;
        }elseif ($package->CreditcheckStatus == ESB_PACKAGE_NETWORK_STATUS_CC_REJECTED) {
            return PACKAGE_REJECTED;
        } else {
            return PACKAGE_WAITING;
        }
    }

    /**
     * Get the corresponding html for each status
     * @param object $package
     * @param string $status
     * @return string
     */
    function creditCheckHtml($package, $status)
    {
        $imgDir = '/skin/frontend/vznl/default/images/';
        $imgTemplate = '<img src="' . $imgDir . '%s" alt="ILT" onerror="this.src=\'' . $imgDir . '%s\'" height="20px">';
        $htmlTemplate = '<span class="%s"><table><tr><td>%s</td><td>%s</td></tr><tr><td style="padding-left: 3em"></td></tr><tr><td></td><td style="color: black">%s</td></tr></table></span>';

        switch ($status) {
            case PACKAGE_WAITING :
                return __('Waiting for credit check');

            case PACKAGE_SUCCESS :
                $spanClass = 'credit-check-success bold';
                $imageHtml = sprintf($imgTemplate, 'creditcheck/ICO_ILT-Successful.svg', 'creditcheck/ICO_ILT-Successful.svg');
                $infoMessage = __('The order is accepted');

                if (!empty($package->IsRetention) && $package->IsRetention) {
                    $colouredMessage =  __('Order validation completed successfully');
                } else {
                    $colouredMessage = __('Credit check completed successfully');
                }
                return sprintf($htmlTemplate, $spanClass, $imageHtml, $colouredMessage, $infoMessage);

            case PACKAGE_FAILED :
            case PACKAGE_REFERRED :
            case PACKAGE_PARTIAL:
            case PACKAGE_REJECTED :
                $spanClass = 'credit-check-error';
                $imageHtml = '';
                $infoMessage = '';

                if($package){
                    switch ($package->CreditcheckStatus){
                        case ESB_PACKAGE_NETWORK_STATUS_CC_REFERRED:
                            $spanClass = 'credit-check-partial';
                            $imageHtml = sprintf($imgTemplate, 'creditcheck/ICO_ILT-Partial.svg', 'creditcheck/ICO_ILT-Partial.png');
                            $colouredMessage = __('Creditcheck Referred');
                            $colouredMessage .= '<br/>' . str_replace('#', '<br/>', $package->VfStatusDesc);
                            break;

                        case ESB_PACKAGE_NETWORK_STATUS_CC_PARTIAL:
                            $spanClass = 'credit-check-partial';
                            $imageHtml = sprintf($imgTemplate, 'creditcheck/ICO_ILT-Partial.svg', 'creditcheck/ICO_ILT-Partial.png');
                            $colouredMessage = '<br/>' .str_replace('#', '<br/>', $package->VfStatusDesc);
                            break;

                        case ESB_PACKAGE_NETWORK_STATUS_CC_REJECTED:
                            $imageHtml = sprintf($imgTemplate, 'creditcheck/ICO_ILT-Rejected.svg', 'creditcheck/ICO_ILT-Rejected.png');
                            $colouredMessage = __('Creditcheck Rejected');
                            $colouredMessage .= '<br/>' .str_replace('#', '<br/>', $package->VfStatusDesc);
                            break;

                        case ESB_PACKAGE_NETWORK_STATUS_CC_ADDITIONNAL_INFO_REQUIRED:
                            $spanClass = 'credit-check-partial';
                            $imageHtml = sprintf($imgTemplate, 'creditcheck/ICO_ILT-Partial.svg', 'creditcheck/ICO_ILT-Partial.png');
                            $colouredMessage = __('Additional information requested');
                            $colouredMessage .= '<br/>' .str_replace('#', '<br/>', $package->VfStatusDesc);
                            break;

                        default:
                            $imageHtml = sprintf($imgTemplate, 'creditcheck/ICO_ILT-Rejected.svg', 'creditcheck/ICO_ILT-Rejected.png');
                            $colouredMessage = __('Error occurred while validating the order Dealer Adapter') . ' (' . (!empty($package)) ? $package->VfStatusCode : '' . ')';
                            $colouredMessage .= '<br/>' .str_replace('#', '<br/>', $package->VfStatusDesc);
                            break;
                    }
                } else{
                    $imageHtml = sprintf($imgTemplate, 'creditcheck/ICO_ILT-Rejected.svg', 'creditcheck/ICO_ILT-Rejected.png');
                    $colouredMessage = __('Creditcheck Rejected');
                }

                return sprintf($htmlTemplate, $spanClass, $imageHtml, $colouredMessage, $infoMessage);

            case MANUAL_ACTIVATION:
                $spanClass = 'credit-check-error';
                $imageHtml = sprintf($imgTemplate, 'creditcheck/ICO_ILT-Rejected.svg', 'creditcheck/ICO_ILT-Rejected.png');
                $colouredMessage = __('This package is manually connected');
                $infoMessage = '';

                return sprintf($htmlTemplate, $spanClass, $imageHtml, $colouredMessage, $infoMessage);
        }
    }

    /**
     * Log request to audit
     */
    function logRequest()
    {
        $logDir = __DIR__ . DIRECTORY_SEPARATOR . join(DIRECTORY_SEPARATOR, array('var', 'log', 'security', 'audit')) . DIRECTORY_SEPARATOR; //we assure that this script is in the root of the project
        if (!@realpath($logDir)) {
            @mkdir($logDir, 0777, true);
        }
        $logFile = sprintf('%saudit_frontend_controller_%s.log', $logDir, date("dmY"));
        if ($h = @fopen($logFile, 'a+')) {
            $data = array(
                gmdate('Y-m-d H:i:s'),
                getPost('website_code'),
                getPost('agent_id'),
                getPost('dealer_id'),
                getPost('dealer_group_ids'),
                getPost('axi_store_id'),
                'creditcheck_script',
                json_encode($_POST),
                null,
            );
            @fputs($h, join('|', $data) . PHP_EOL);
            @fclose($h);
            unset($data);
            unset($logFile);
            unset($logDir);
        }
    }

    /**
     * Main creditcheck function
     * @return mixed
     */
    function performCreditCheck()
    {
        if ('POST' == $_SERVER['REQUEST_METHOD']) {
            global $pdo;
            try {
                $orderNo = getPost('order_id');
                $packageIds = json_decode(getPost('package_ids'));
                $retentionIds = json_decode(getPost('retention_ids'));
                $packages = array();

                $stmt = $pdo->prepare("SELECT s.entity_id AS Id, s.order_status AS OrderStatus, s.error_code AS ErrorCode, s.order_number AS OrderNumber, c.ban AS CustomerBan
                      FROM superorder s INNER JOIN customer_entity c ON s.customer_id = c.entity_id WHERE s.order_number = :order_no LIMIT 1;");
                $stmt->bindParam(':order_no', $orderNo);
                $stmt->execute();
                $superOrder = $stmt->fetch(\PDO::FETCH_OBJ);
                unset($stmt);

                if (!empty($packageIds)) {
                    $stmt = $pdo->prepare("SELECT p.manual_activation_reason as ManualActivationReason, p.esb_manual_activation as EsbManualActivation, p.package_id AS PackageId, p.status AS Status, p.vf_status_code AS VfStatusCode, p.vf_status_desc AS VfStatusDesc, p.package_esb_number AS PackageEsbNumber, p.creditcheck_status AS CreditcheckStatus
                          FROM catalog_package p WHERE p.order_id = :order_id AND p.package_id IN (" . implode(',', $packageIds) . ") ORDER BY p.package_id;");
                    $stmt->bindParam(':order_id', $superOrder->Id);
                    $stmt->execute();
                    $packages = $stmt->fetchAll(\PDO::FETCH_OBJ);
                    unset($stmt);
                }

                $result['data'] = ['creditcheck_in_progress' => false];
                $validated [] = strtolower(SO_VALIDATED);
                $validated [] = strtolower(SO_FULFILL_PROGRESS);
                $validated [] = strtolower(SO_FULFILLED);
                if (in_array(strtolower($superOrder->OrderStatus), [strtolower(SO_WAITING_FOR_VALIDATION), strtolower(SO_INITIAL), strtolower(SO_PRE_INITIAL), strtolower(SO_WAITING_VALIDATION_ACCEPTED_AXI)])) {
                    //still waiting validation
                    $result['data']['superorder_status'] = false;
                } elseif (in_array(strtolower($superOrder->OrderStatus), $validated)) {
                    //passed validation
                    $result['data']['superorder_status'] = true;
                } else {
                    //none of the above cases results in validation failed
                    $result['data']['superorder_status'] = -1;
                    $result['data']['creditcheck_in_progress'] = $superOrder->OrderStatus;
                }
                if ($superOrder->ErrorCode) {
                    $result['super_order_error'] = $superOrder->ErrorCode;
                }

                $failedCC = false;

                foreach ($packages as $package) {
                    $packageId = $package->PackageId;
                    switch (validateCreditCheck($package)) {
                        case PACKAGE_WAITING :
                            $result['data'][$packageId]['performed'] = true;
                            $result['data'][$packageId]['complete'] = false;
                            $result['data'][$packageId]['html'] = creditCheckHtml($package, PACKAGE_WAITING);
                            break;
                        case PACKAGE_FAILED :
                            $result['data'][$packageId]['performed'] = true;
                            $result['data'][$packageId]['complete'] = false;
                            $result['data'][$packageId]['code'] = (!empty($package)) ? $package->VfStatusCode : '';
                            $result['data'][$packageId]['html'] = creditCheckHtml($package, PACKAGE_FAILED);
                            if ($package->VfStatusCode) {
                                $failedCC = true;
                            }
                            break;
                        case PACKAGE_SUCCESS :
                            $result['data'][$packageId]['performed'] = true;
                            $result['data'][$packageId]['complete'] = true;
                            if (in_array($packageId, $retentionIds)) {
                                $package->IsRetention = true;
                            }
                            $result['data'][$packageId]['html'] = creditCheckHtml($package, PACKAGE_SUCCESS);
                            break;
                        case PACKAGE_PARTIAL :
                            $result['data'][$packageId]['performed'] = true;
                            $result['data'][$packageId]['complete'] = false;
                            $result['data'][$packageId]['repeat_cc'] = false;
                            $result['data'][$packageId]['code'] = (!empty($package)) ? $package->VfStatusCode : '';
                            $result['data'][$packageId]['description'] =(!empty($package)) ? $package->VfStatusDesc : '';
                            $result['data'][$packageId]['html'] = creditCheckHtml($package, PACKAGE_PARTIAL);
                            if ($package->VfStatusCode) {
                                $failedCC = true;
                            }
                            break;
                        case PACKAGE_REFERRED :
                            $result['data'][$packageId]['performed'] = true;
                            $result['data'][$packageId]['complete'] = false;
                            $result['data'][$packageId]['repeat_cc'] = true;
                            $result['data'][$packageId]['code'] = (!empty($package)) ? $package->VfStatusCode : '';
                            $result['data'][$packageId]['description'] = (!empty($package)) ? $package->VfStatusDesc : '';
                            $result['data'][$packageId]['html'] = creditCheckHtml($package, PACKAGE_REFERRED);
                            if ($package->VfStatusCode) {
                                $failedCC = true;
                            }
                            break;
                        case MANUAL_ACTIVATION :
                            $result['data'][$packageId]['performed'] = true;
                            $result['data'][$packageId]['complete'] = false;
                            $result['data'][$packageId]['manual_activation'] = true;
                            $result['data'][$packageId]['code'] = (!empty($package)) ? $package->VfStatusCode : '';
                            $result['data'][$packageId]['html'] = creditCheckHtml($package, MANUAL_ACTIVATION);
                            if ($package->VfStatusCode) {
                                $failedCC = true;
                            }
                            break;
                        case PACKAGE_REJECTED :
                            $result['data'][$packageId]['performed'] = true;
                            $result['data'][$packageId]['complete'] = false;
                            $result['data'][$packageId]['rejected_cc'] = true;
                            $result['data'][$packageId]['code'] = (!empty($package)) ? $package->VfStatusCode : '';
                            $result['data'][$packageId]['html'] = creditCheckHtml($package, PACKAGE_REJECTED);
                            if ($package->VfStatusCode) {
                                $failedCC = true;
                            }
                            break;
                        default :
                            $result['data'][$packageId]['performed'] = false;
                    }
                    $result['data'][$packageId]['customer_ban'] = $superOrder->CustomerBan;
                    $result['data'][$packageId]['package_esb_number'] = $package->PackageEsbNumber;
                    $result['data'][$packageId]['package_order_number'] = $superOrder->OrderNumber . "." . $packageId;
                    if (isset($result['data'][$packageId]['html'])) {
                        $result['data'][$packageId]['html'] = utf8_encode($result['data'][$packageId]['html']);
                    }
                }

                // If the CC has failed, save the quote as saved shopping cart
                if ($failedCC) {
                    $result['failed_cc'] = true;
                }

                $result['error'] = false;

                return $result;
            } catch (\Exception $e) {
                // Set fail response for other errors
                $result['error'] = true;
                $result['message'] = __($e->getMessage());

                return $result;
            }
        }
    }

    /**
     * @param $data
     * @param string $istr
     * @return string
     */
    function prettyJson($data, $istr = '    ')
    {
        if (defined('JSON_PRETTY_PRINT')) {
            return json_encode($data, JSON_PRETTY_PRINT);
        } else {
            $json = json_encode($data);
            $result = '';
            for ($p = $q = $i = 0; isset($json[$p]); $p++) {
                $json[$p] == '"' && ($p > 0 ? $json[$p - 1] : '') != '\\' && $q = !$q;
                if (!$q && strchr(" \t\n", $json[$p])) {
                    continue;
                }
                if (strchr('}]', $json[$p]) && !$q && $i--) {
                    strchr('{[', $json[$p - 1]) || $result .= "\n" . str_repeat($istr, $i);
                }
                $result .= $json[$p];
                if (strchr(',{[', $json[$p]) && !$q) {
                    $i += strchr('{[', $json[$p]) === false ? 0 : 1;
                    strchr('}]', $json[$p + 1]) || $result .= "\n" . str_repeat($istr, $i);
                }
            }

            return $result;
        }
    }

    /**
     * Handle the request
     */
    try {
        logRequest();
        $config = new \SimpleXMLElement(@file_get_contents($databaseConfigXmlPath));
        $options = $config->xpath('global/resources/default_setup/connection');
        $pdo = new \PDO(
            'mysql:host=' . (string) $options[0]->host . ';dbname=' . (string) $options[0]->dbname,
            (string) $options[0]->username,
            (string) $options[0]->password,
            array(
                \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
                \PDO::ATTR_EMULATE_PREPARES => false,
                \PDO::MYSQL_ATTR_FOUND_ROWS => true,
            )
        );

        $res = performCreditCheck();
    } catch (\Exception $e) {
        $res = array(
            'error' => true,
            'message' => $e->getMessage(),
        );
    }

    /**
     * Output response
     */
    header('Content-Type: application/json');
    header("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
    header("Pragma: no-cache");
    header("Expires: Thu, 19 Nov 1981 08:52:00 GMT");
    echo prettyJson($res);
}

    /**
     * Get variable values from $_POST
     * @param string|null $key
     * @param null $default
     * @return string|null
     */
    function getPost($key = null, $default = null)
    {
        if (null === $key) {
            return $_POST;
        }

        return (isset($_POST[$key])) ? $_POST[$key] : $default;
    }
