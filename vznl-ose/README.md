# Install

## Prerequisites
- Omnius2-full (https://bitbucket.org/dynalean/omnius2-full)
- git-lfs (https://git-lfs.github.com/)
- Poedit (https://poedit.net/)
- NodeJS 10
- NPM 6

## Development
Please review the readme mentioned in the `omnius2-full` how to set up a baseline development environment

### Box Logins
[Frontend login](https://telesales.dev.dynacommerce.io/agent/account/login)
```bash
user: demo-agent
passwd: Dyna123
```

[Implement login](https://telesales.dev.dynacommerce.io/implement)
```bash
user: admin
passwd: netrom4
```

## PHP check
To run
```bash
composer check
```

## Webpack
To use webpackage, there are multiple levels to start. To create assets webpack can be build using dev flag. This means
that the bundling will happen. For production the same happens but all dev references are kept aside

To start developing we suggest to run webpack with watchmode. This will automaticly rebuild your assets while files are changed
```bash
npm run build
npm run build:dev
npm run build:watch
```

## Translations
To compile the translation files from `po` towards json, run below command:
```bash
npm run build:translations
```

**PoEdit Translation scanning**

To scan for new translations you need to implement a custom gettext parser for `.jsx` files since on default they are not recognized.
To do this go to the `PoEdit` preferences and select the `Extractors` tab. Add a new Custom Extractor and name it `JSX`. Below settings should be used to get the parser working:
* Language: `JSX`
* List of extenstions: `*.jsx`
* Command to extract: `‪xgettext --language=Javascript --force-po -o %o %C %K %F`
* An item in keyword list: `‪-k%k`
* Item in input files list: `%f`
* Source code charset: `‪--from-code=%c`

After that you should be able to scan the source and find translations in `JSX` files as well

# Test
## PHPUnit
To execute the unittests tests please run below commands:
```sh
composer test
```
Please note that `codecoverage` can be added by running PHPUnit with one of the below params:
```sh
composer test-coverage:html
composer test-coverage:clover
composer test-coverage:junit
```

# React components
## CustomerSearch
To active this new React component, it can be turned on/off via below setting:

```System -> Configuration -> Omnius Settings -> Services settings -> Customer Search Configuration```

# Packaging
### Prerequisites
- [Zend-sdk](https://github.com/zend-patterns/ZendServerSDK)
- [Package FPM](http://fpm.readthedocs.io/en/latest/installing.html)

run `./package.sh <version> <type>` in the package folder
An example: `./package.sh 1 zpk` to create a zpk package.
