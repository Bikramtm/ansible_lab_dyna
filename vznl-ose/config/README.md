# How to
This configuration file holds all the `React` configuration settings. 
Main reason why this file is outside of the react folder that we want the application to natively listen on:
`<domain.tld>/config/config.json`

This file will be later moved inside the main `React` applet once `Magento` will be removed

## CustomerSearch
Since customer search has an internal hardcoded load of the `search.conf.json` instead of passing it by reference, we are
forced to keep this configuration file in here to support legacy. The full configuration from `config.json` is already passed
as reference but not used since the component is not ready to receive it yet

### Configuration files
Configuration files related to this project has been placed in folder omnius-vnext/customer-search

search_fields_config.json file is used to configure sales channels and adapters, search result fields, field contraints, field combinations, etc. specific to sales channels.

customer_linking_config.json file is used to configure potential matches criteria specific to customer type and to set cardinality for linking customer based on line of business.