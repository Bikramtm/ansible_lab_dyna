# Migration log

## 2.1.x to 2.2.0
The major change in this release is that the new Customer Search & Link is integrated. On default this feature will be disabled and can be enabled by config at:
`Admin / System / Configuration / Service Settings / Customer Search Configuration`
To customize this feature you should create your own adapter and configure it in the config namespace `omnius_service_customer_search_implementation_configuration_customer_search_adapter`. This adapter should implement the corresponding `AdapterInterface`
Configuration endpoints on where this service can be found should be done on filesystem level. The configurations are available in `./config/config.json and ./config/search.config.json`. A readme on how to use them are available in that specific configuration folder

### Packaging
Since the CustomerSearch&Link is build in React, the app should be packaged as well. Therefor the package.json is now extended to have a package for both React as for the legacy webpack for Magento.

## 2.0.x to 2.1.0

### Vodefone specific style files are renamed.
The Vodafone specific files are renamed to match the baseline strategy. If you extend the load of this file in your
local theme extend, please upgrade the reference
```
  /skin/frontend/omnius/default/css/de.css ->  /skin/frontend/omnius/default/css/app.css
```

### Implemented webpack.
With the introduction of `Webpack` the bundled asset is loaded only. This means that instead of loading the old
`de.css` we now load the `app.css`. With webpack this is bundled one level deeper to a dist reference as mentioned below

Add in `local.xml` (app/design/frontend/omnius/default/layout/local.xml)
```
<action method="addCss">
    <stylesheet>dist/css/app.css</stylesheet>
</action>

```
-Remove from `local.xml` (app/design/frontend/omnius/default/layout/local.xml)

```
    <action method="addCss">
          <stylesheet>css/vfde-svg-fonts.css</stylesheet>
      </action>
     <action method="addCss">
          <stylesheet>css/bootstrap.min.css</stylesheet>
      </action>
      <action method="addCss">
          <stylesheet>css/bootstrap-switch.css</stylesheet>
      </action>
      <action method="addCss">
          <stylesheet>css/bootstrap-tagsinput.css</stylesheet>
      </action>
      <action method="addCss">
          <stylesheet>css/bootstrapXL.css</stylesheet>
      </action>
      <action method="addCss">
          <stylesheet>css/jquery.slidepanel.css</stylesheet>
      </action>
      <action method="addCss">
          <stylesheet>jquery-ui.min.css</stylesheet>
      </action>
      <action method="addCss">
          <stylesheet>jquery-ui.structure.min.css</stylesheet>
      </action>
      <action method="addCss">
          <stylesheet>jquery-ui.theme.min.css</stylesheet>
      </action>
      <action method="addCss">
          <stylesheet>css/jquery-ui-theme.css</stylesheet>
      </action>
      <action method="addCss">
          <stylesheet>css/legacy.css</stylesheet>
      </action>
```


### Added Elastic Search & Link
The react ui component from Product has been built into an executable js file using webpack bundling feature and integrated with the baseline OSE.

Prerequisites:
* npm
* docker
* customer API

#### Customer API
Please note that the customer search api (https://bitbucket.org/dynalean/customer) should be up and running in order to make product search & link working in Baseline OSE. You can start customer api by install.sh script in omnius2-full . The script is as follows:
```./install.sh customer```

#### Customer-UI React component implementation to OSE
To convert the react component into an implementable js output format , you have to use npm tool . So, Please make sure that you have npm installed in your system. The steps are:

* ```npm install```
* ```npm run build```

#### Switch to the older version
From latest release, Baseline OSE will be implementing Product Elastic Search component as default . So, If anyone wanted to go back to the old search , they can do it by disabling the Customer Search Configuration option in Services Settings through Admin Panel

```System -> Configuration -> Omnius Settings -> Services settings -> Customer Search Configuration```

#### More Info
More information on elastic search implementation will be available in https://bitbucket.org/dynalean/customer/src/da36d3c7e1a6118e12f71b26c5395f9b8b90a934/?at=develop

Two main notes regarding project implementation are:

* **Custom CSS files are to be added in ```skin/frontend/omnius/default/css/style/customer-search.css``` where you can add project specific changes (can do VFDE/VZNL specific CSS changes as needed). The changes can be like logo of the icon /buttons. This file is included as part of webpack bundling process. So , once you changed the file, please keep in mind that you should run ```npm run build``` to ensure that the output bundled css file got the change reflected.**

* **Custom JS files are currently added in ```react/src/searchCustomer.js``` file. This currently handles the callback functions which can be called when some click or other events occurs in the search portion. This allows us to have kind of customization in our end to be performed when some events are happened inside the react module. For eg in this particular case , we needed to open a drawer when the user clicks on Search button which was not there in the react component before. So , now we may need to call some js functions when such a button click happens. This can be handled by adding a prop in the react component which is responsible and write that prop value as a js function in our end. These functions are also embedded in the bundled final js output, so whenever you make a change, you should run ```npm run build``` to make it reflecting in the dashboard.**


