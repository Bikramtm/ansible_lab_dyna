import i18n from 'i18next';
import XHR from 'i18next-xhr-backend';
import { reactI18nextModule } from 'react-i18next';
i18n
  .use(XHR)
  .use(reactI18nextModule)
  .init({
    load: 'languageOnly',
    backend: {
      loadPath: '/dist/locales/{{lng}}/{{ns}}.json'
    },
    lng: document.documentElement.lang || 'en',
    fallbackLng: {
      'en-GB': 'en',
      'en-US': 'en'
    },
    ns: ['baseline'],
    defaultNS: 'baseline',
    fallbackNS: 'baseline',
    debug: false,
    keySeparator: '##',
    interpolation: {
      escapeValue: false,
      formatSeparator: ',',
      format: function (value, format) {
        if (format === 'uppercase') return value.toUpperCase();
        return value;
      }
    },
    react: {
      wait: true,
      bindI18n: 'languageChanged loaded',
      bindStore: 'added removed',
      nsMode: 'default'
    }
  });
