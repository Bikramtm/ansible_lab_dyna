/* eslint-disable import/first */
import React from 'react';
import ReactDOM from 'react-dom';

import registerServiceWorker from './registerServiceWorker';
import './i18n'; // initialized i18next instance
import CustomerSearch from './containers/CustomerSearch/CustomerSearch';
import MyLinkedAccount from './vznl/containers/Customer/MyLinkedAccount';
import MyOrders from './containers/Customer/MyOrders';
import MyBaskets from './vznl/containers/Customer/MyBaskets';
import ConfigProvider from './ConfigProvider';
import '@omnius/react-ui-elements/dist/ui-elements.css';
import AppContext from './AppContext';
import {Popup} from "@omnius/react-ui-elements";

const eventEmitter = require('event-emitter');
window.eventEmitter = eventEmitter();

const customerSearchContainer = document.getElementById('customer-search-container');
if (customerSearchContainer) {
  const CustomerSearchComponent = () =>
    (
      <ConfigProvider>
        <AppContext.Consumer>
          {
            (context) => {
              // This is a way to read salesChannel from PHP render dom
              const salesChannel = document.getElementById('customer-search-container');
              const { customerSearchConfig } = context;
              const lineofBusiness = document.getElementById('dealer_line_of_business');
              if (!customerSearchConfig) {
                return null;
              }

              return <CustomerSearch
                config={customerSearchConfig}
                salesChannel={salesChannel.dataset.saleschannel}
                lineofBusiness = {lineofBusiness.value}
                afterProceedCallback={customerSearchCallBack}
                uhelpAppUrl={uhelpAppUrl}
              />;
            }
          }
        </AppContext.Consumer>
      </ConfigProvider>
    );

  ReactDOM.render(
    <CustomerSearchComponent />,
    customerSearchContainer
  );
}

registerServiceWorker();

window.eventEmitter.on('CUSTOMER_LINKED_RENDER', () => {
  const customerLinkedAccountContainers = document.querySelector('#customer-linked_account');
  if(customerLinkedAccountContainers) {
    ReactDOM.render(
      <MyLinkedAccount />,
      customerLinkedAccountContainers
    );
  }
});

window.eventEmitter.on('CUSTOMER_ORDERS_RENDER', () => {
  const customerOrdersContainers = document.querySelector('#customer-screen-orders');
  if(customerOrdersContainers) {
    ReactDOM.render(
      <MyOrders />,
      customerOrdersContainers
    );
  }
});

window.eventEmitter.on('CUSTOMER_BASKETS_RENDER', () => {
  const customerDataPanelContainer = document.querySelector('#cart-overview');
  if(customerDataPanelContainer) {
    ReactDOM.render(
      <MyBaskets />,
      customerDataPanelContainer
    );
  }
});

window.eventEmitter.on('ORDER_DETAILS_UNMOUNT', () => {
  const customerOrdersContainers = document.querySelector('#customer-screen-orders');
  if(customerOrdersContainers) {
    ReactDOM.unmountComponentAtNode(customerOrdersContainers);
  }
});

window.eventEmitter.on('ORDER_STATUS_RENDER', () => {
  jQuery( "#customer-search-results .order-status" ).each(function() {
    const contents = jQuery('span',this).text();
    ReactDOM.render(
      <Popup
        trigger={
          <span>{contents}</span>
        }
        content={contents}
        on='hover'
        inverted
        position='top center'
        hoverable
        style={{
          padding: '4px 7px'
        }}
      />,
      this
    );
  });
});