const AppContext = process.env.NODE_ENV_TEST ?
  require('./context-test').default :
  require('./context').default;

export default AppContext;