import { ExternalSearchOptions } from '@omnius/customer-ui';
import _ from 'lodash';
import MapAdapterNames from './MapAdapterNames';
import MapFieldNames from './MapFieldNames';
import moment from 'moment/moment';

export default class ExternalSearchSetting extends ExternalSearchOptions {
  // eslint-disable-next-line no-useless-constructor
  constructor(customer) {
    super(customer);
  }

  findNode(name, currentNode) {
    let i,
      currentChild,
      result;

    if (name === currentNode.name) {
      return currentNode;
    } else {

      if (currentNode.fields) {
        for (i = 0; i < currentNode.fields.length; i += 1) {
          currentChild = currentNode.fields[i];

          // Search in the current child
          result = this.findNode(name, currentChild);

          // Return the result if the node has been found
          if (result !== false) {
            return result;
          }
        }
      }

      // The node has not been found and we have no more options
      return false;
    }
  }

  createFieldState(fieldObj, fieldValue) {
    return { type: 'value', fullName: fieldObj.fullName, name: fieldObj.name, value: fieldValue };
  }

  convertToDateFormat(date, dateFormat) {
    const dateFormatForMoment = dateFormat.toUpperCase();
    return moment(date).format(dateFormatForMoment)
  }

  getFieldStateDetails(fieldName, fieldValue, activeAdapter, adapter) {
    const field = this.findNode(fieldName, activeAdapter.searchForm);
    if (field && fieldValue) {
      if (fieldValue instanceof Array && fieldValue.length > 0) {
        return this.createFieldState(field, fieldValue[0]);
      } else if (typeof (fieldValue) === 'string') {
        const value = fieldName === 'adapter' ? adapter : fieldName === 'dateOfBirth' ?
          this.convertToDateFormat(fieldValue, activeAdapter.dateFormat) : fieldValue;
        return this.createFieldState(field, value);
      }
    }
  }

  getDetails(customer, activeAdapter, adapter, fieldState) {
    let fieldValue, fieldName, fieldData;
    Object.keys(customer).forEach(key => {
      fieldValue = customer[key];
      if (MapFieldNames.hasOwnProperty(key)) {
        if (key === '_party') {
          this.getDetails(fieldValue, activeAdapter, adapter, fieldState);
        } if (key === '_contactDetails') {
          fieldValue.forEach(nestedKey => {
            this.getDetails(nestedKey.value, activeAdapter, adapter, fieldState);
          });
        }
        else {
          fieldName = MapFieldNames[key].fieldName;
          if (fieldName) {
            fieldData = this.getFieldStateDetails(fieldName, fieldValue, activeAdapter, adapter);
            if (fieldData) {
              fieldState.push(fieldData);
            }
          }
        }
      }
    });

    return fieldState;
  }

  getMappedParams(adapters: Object, saleschannel: String) {
    let fieldState = [], fieldData;
    const customer = this.data;
    const lob = customer._lineOfBusiness ? customer._lineOfBusiness.toLowerCase() : customer._lineOfBusiness;
    const adapter = MapAdapterNames[lob] ? MapAdapterNames[lob] : MapAdapterNames.other;
    const activeAdapter =
      _.find(adapters, adapterObj => adapterObj.name === adapter) || {};

    fieldState = this.getDetails(customer, activeAdapter, adapter, []);
    const fieldValueIndex = _.findIndex(
      fieldState,
      field => field.type === 'value' && field.name === 'customerType' && field.value === 'organization'
    );

    if (fieldValueIndex > -1) {
      fieldData = this.getFieldStateDetails('lessThanThreePhone', 'true', activeAdapter, adapter);
      if (fieldData) {
        fieldState.push(fieldData);
      }
    }

    return { externalSearchOptions: fieldState, activeAdapter: activeAdapter };
  }
}
