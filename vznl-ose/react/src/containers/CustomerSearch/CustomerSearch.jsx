/*
 * Copyright (c) 2018. Dynacommerce B.V.
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { I18n } from 'react-i18next';
import classnames from 'classnames';
import _ from 'lodash';

import { Form, SearchResults, getServices, SearchConfigFactory, AuthenticationModal } from '@omnius/customer-ui';

// import ColumnMapping from '@omnius/customer-ui/dist/lib/model/Table/ColumnMapping';
// import ColumnComponentMapper from '@omnius/customer-ui/dist/lib/component/resultTable/columnComponentMapper';
// import Column from '@omnius/customer-ui/dist/lib/component/resultTable/column/column';
// import config from '@omnius/customer-ui/dist/lib/config/customerColumnMap';
// import SearchResults from '@omnius/customer-ui/dist/lib/searchResults';

// import { Form, getServices } from '@omnius/customer-ui';
import { Drawer, Header, Modal, Button, Banner } from '@omnius/react-ui-elements';
// import moment from 'moment/moment';

import './CustomerSearch.scss';
import SearchIcon from './search-icon.svg';
import Proceed from './Proceed';
import { MdEdit } from 'react-icons/md';
import ExternalSearchSetting from './ExternalSearchSetting';


class CustomerSearch extends Component {

  state = {
    config: null,
    linkingConfig: null,
    drawerVisible: false,
    customer: null,
    passwordError : false
  };

  constructor(props) {
    super(props);

    this.proceed = new Proceed();
    this.services = getServices(this.props.config);
  }

  componentDidMount() {
    var url =  window.location.href;
    if(url.indexOf('checkout/cart/') === -1) {
      var request = new Request(this.props.config.apiUrl + '/search/config?salesChannel=' + this.props.salesChannel, {
        credentials: 'same-origin'
      });
      fetch(request)
        .then(function (response) {
          return response.json()
        }).then(json => {
          const searchConfig = SearchConfigFactory.create(json, this.props.salesChannel, this.props.config.apiUrl);
          this.setState({
            config: searchConfig
          });
        }).catch(function (ex) {
          console.error(ex)
        });

      fetch(this.props.config.apiUrl + '/search/linking/config', { credentials: 'same-origin' })
        .then(function (response) {
          return response.json()
        }).then(json => {
          this.setState({
            linkingConfig: json
          });
        }).catch(function (ex) {
          console.error(ex)
        });
    }
  }

  componentWillMount() {
    document.addEventListener('click', this.handleClick, false);
  }

  componentWillUnMount() {
    document.removeEventListener('click', this.handleClick, false);
  }

  handleClick = (event) => {
    if (event && event.target && event.target.getAttribute('data-search-results-drawer-visibility') === 'hidden') {
      this.setState({ drawerVisible: false });
    }
  }

  onSearchClickHandler = (form, salesChannel, ignoredStatuses, statusHandling) => {
    this.setState({ drawerVisible: true });
    setTimeout(() => {
      this.services.searchHandler(form, salesChannel, ignoredStatuses, statusHandling);
    }, 500); // Wait 500ms before executing this call, should be replaced with callback in the drawer once the package
    // is finally updated in react-ui-elements

  };

  onCloseClickHandler = () => {
    this.setState({ drawerVisible: !this.state.drawerVisible });
  }

  onProceedClickHandler = async (...args) => {
    const event = _.get(args, 0);
    const account = _.get(args, 1);
    const errorCallback = _.get(args, 2);

    if (this.props.beforeProceedCallback !== undefined) {
      this.props.beforeProceedCallback(event, account);
    }

    // Proceed to 360
    await this.proceed.loadCustomer(account, data => {

      if (data.wrong_password) {
        this.setState({ passwordError: true, customerData: data.requestData });
      }

      if (errorCallback) {
        errorCallback();
      }
    });

    if (this.props.afterProceedCallback !== undefined) {
      this.props.afterProceedCallback(event, account);
    }
  };

  handleEditCustomer(data) {
    this.setState({ customer: data });
  }

  searchAgain() {
    const { customer } = this.state;
    const linkedCustomer = customer._linkId && customer._linkId.length ? true : false;
    const lob = customer._lineOfBusiness;
    const searchAgainNotAllowed = linkedCustomer || lob === 'fixed' ? true : false;

    if (searchAgainNotAllowed) {
      this.setState({ customer: null });
    } else {
      this.setState({ customer: null, externalSearchSetting: new ExternalSearchSetting(customer) });
    }

  }

  handleDismiss() {
    this.setState({ customer: null });
  }

  updateCustomer() {
    const { customer } = this.state;
    if (this.props.uhelpAppUrl) {
      const url = this.props.uhelpAppUrl + customer._accountNumber;
      var redirectWindow = window.open(url, '_blank');
      redirectWindow.location;
    } else {
      console.log('U Help url is not configured in admin')
    }

  }

  getContactDetails(partyInfo, type) {

    const contactType = type.split('_');
    const contactDetails = partyInfo._contactDetails ? partyInfo._contactDetails : null;

    if (contactDetails) {
      const fieldType = _.find(
        contactDetails,
        detail => detail.contactType === contactType[0]
      );
      const fieldValue = fieldType ? fieldType.value : null;

      switch (contactType[0]) {
      case 'email':
        return fieldValue && fieldValue.emailAddress;
      case 'phone':
        return fieldValue && fieldValue.number;
      case 'address':
        return fieldValue && fieldValue[contactType[1]];

      default: return null;
      }
    }
    else {
      return '';
    }
  }

  updateCustomerDialog(t) {
    let updateCustomerDialog = null;
    const { customer } = this.state;
    const { salesChannel } = this.props;
    const externalSearchSettingObj = new ExternalSearchSetting(customer);
    
    if (customer) {
      let alertMessage;
      const customerType = customer._party._type;
      const name = customerType === 'individual' ?
        customer._party._firstName + ' ' + customer._party._lastName :
        customer._party._name;
      const dob = customerType === 'individual' ?
        externalSearchSettingObj.convertToDateFormat(customer._party._dateOfBirth, 'DD-MM-YYYY')
        : null;
      const email = this.getContactDetails(customer._party, 'email');
      const phoneNumber = this.getContactDetails(customer._party, 'phone');
      const linkedCustomer = customer._linkId && customer._linkId.length ? true : false;
      const lob = customer._lineOfBusiness;
      const error = salesChannel === 'indirect' || linkedCustomer || lob === 'fixed' ? true : false;
      const firstHeader = customerType === 'individual' ? t('CUSTOMER INFORMATION') : t('COMPANY INFORMATION');
      const postalCode = this.getContactDetails(customer._party, 'address_postalCode');
      const houseNumber = this.getContactDetails(customer._party, 'address_houseNumber');
      const street = this.getContactDetails(customer._party, 'address_street');
      const city = this.getContactDetails(customer._party, 'address_city');

      if (error) {
        alertMessage = salesChannel === 'indirect' ?
          t('You\'re not allowed to change data. Please create a support ticket.')
          : linkedCustomer && lob === 'mobile' ? t('You\'re not allowed to change data. Changing data can break the current link.') : t('You\'re not allowed to change data.');
      }

      updateCustomerDialog =
        <Modal className='vn-dialog u-help' closeOnDimmerClick={true} closeOnEscape={true} closeIcon={true} open={true}
          onClose={this.handleDismiss.bind(this)}>
          <div className='u-help-modal'>
            <Modal.Header>{t('Change customer details')}</Modal.Header>
            <Modal.Content>
              <Modal.Description>
                {error && <div className='u-help-error'><Banner type='warning' text={alertMessage} /></div>}
                <div className='u-help-content'>
                  <div
                    style={{
                      width: '471px',
                      height: '30px',
                      fontSize: '24px',
                      fontWeight: 'normal'
                    }}
                  >
                    {t('Customer details')}
                  </div>
                  <div>
                    <div className='u-help-label' style={{ marginTop: '13px' }}>
                      {firstHeader}
                    </div>
                    <table style={{ marginTop: '20px' }}>
                      {customerType === 'individual' && <tbody>
                        <tr>
                          <td>{t('Customer ID')}:</td>
                          <td>{customer._accountNumber}</td>
                        </tr>
                        <tr>
                          <td>{t('Customer type')}:</td>
                          <td>{customerType}</td>
                        </tr>
                        <tr>
                          <td>{t('Customer name')}:</td>
                          <td>{name}</td>
                        </tr>
                        {dob && <tr>
                          <td>{t('Date of birth')}:</td>
                          <td>{dob}</td>
                        </tr>}
                        {email && <tr>
                          <td> {t('Email')}: </td>
                          <td>{email}</td>
                        </tr>}
                        {phoneNumber && <tr>
                          <td>{t('Phone number')}:</td>
                          <td>{phoneNumber}</td>
                        </tr>}
                      </tbody>
                      }
                      {customerType === 'organization' &&
                        <tbody>
                          <tr>
                            <td>{t('Customer ID')}:</td>
                            <td>{customer._accountNumber}</td>
                          </tr>
                          <tr>
                            <td>{t('Chamber of commerce number')}:</td>
                            <td>{customer._party._chamberOfCommerce}</td>
                          </tr>
                          <tr>
                            <td>{t('Company name')}:</td>
                            <td>{customer._party._name}</td>
                          </tr>
                          <tr>
                            <td>{t('Address')}:</td>
                            <td>
                              <span>{street}&nbsp;{houseNumber} </span>
                              <br></br>
                              <span>{postalCode}&nbsp; {city} </span>
                            </td>
                          </tr>
                        </tbody>
                      }
                    </table>
                  </div>
                  <div className='u-help-label' style={{ marginTop: '20px' }}>{t('ADDRESS DETAILS')}</div>
                  <table style={{ marginTop: '20px' }}>
                    <tbody>
                      <tr>
                        <td>{t('Address')}: </td>
                        <td>
                          <span>{street}&nbsp;{houseNumber}</span>
                          <br></br>
                          <span>{postalCode}&nbsp;{city}</span>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <div />
                </div>
              </Modal.Description>
            </Modal.Content>
            <Modal.Actions>
              <Button className='button-no-color' primary onClick={this.searchAgain.bind(this)} disabled={error}>
                {t('Search again')}
              </Button>
              <Button className='button-yellow-red' primary onClick={this.updateCustomer.bind(this)} disabled={error} >
                {t('Edit details')}
              </Button>
            </Modal.Actions>
          </div>
        </Modal>;
    }
    return updateCustomerDialog;

  }

  errorHandler = (error)=> {
    this.proceed.handleCustomerSearchErrors(error);
  }

  extractErrorMessages(error) {
    let errorMessage = '';
    if (error && error['errors']) {
      error['errors'].forEach(error => (errorMessage += error.detail));
    } else {
      errorMessage = error.message;
    }
    return errorMessage;
  }

  buildFormDataForLegacySearch(password) {
    const { customerData } = this.state;
    const formData = {};
    const partyInfo = customerData._party;
    const customerType = partyInfo._type;
    const adapter =
      customerData._lineOfBusiness === 'mobile'
        ? 'vodafone_search_customer'
        : 'fixed_search_customer';
    const contactDetails = partyInfo._contactDetails;
    const phoneNumber = contactDetails.find(this.getPhoneNumber);

    if (customerType === 'individual') {
      formData.dateOfBirth = partyInfo._dateOfBirth;
    } else if (customerType === 'organization') {
      formData.chamberOfCommerce = partyInfo._chamberOfCommerce;
    }

    formData.accountNumber = customerData._accountNumber;
    formData.phoneNumber = phoneNumber.value.number;
    formData.adapter = adapter;
    formData.customerType = customerType;
    formData.salesChannel = this.props.salesChannel;
    formData.password = password;
    return formData;
  }

  getPhoneNumber(contactDetails) {
    return contactDetails.contactType === 'phone';
  }

  handlePassword = (event, password, errorCallback) => {
    const formData = JSON.stringify(this.buildFormDataForLegacySearch(password));
    const request = new Request(
      this.props.config.apiUrl + '/search/request/create',
      {
        credentials: 'same-origin',
        method: 'POST',
        body: formData
      }
    );

    fetch(request)
      .then(function(response) {
        return response
          .json()
          .then(json => (response.ok ? json : Promise.reject(json)));
      })
      .then(() => {
        const customerData = this.state.customerData;
        this.setState({ passwordError: false });
        customerData._party._meta.customerpassword = password;
        this.proceed.loadCustomer(customerData);
      })
      .catch(error => {
        errorCallback(this.extractErrorMessages(error));
      });
  };

  handleAuthModalClose = () => {
    this.setState({ passwordError: false });
  };

  renderSearchFormWithDrawer(t) {
    const { salesChannel } = this.props;
    const { config, drawerVisible, linkingConfig, passwordError } = this.state;
    const backdropClassName = classnames('drawer-backdrop ', drawerVisible ? 'visible' : '');

    if (config && linkingConfig) {
      const updateCustomerDialog = this.updateCustomerDialog(t);
      return (
        <React.Fragment>
          {passwordError && (
            <AuthenticationModal
              onChange={this.handlePassword}
              onClose={this.handleAuthModalClose}
            />
          )}
          <Header className='title' as='h2'><img src={SearchIcon} alt={t('Customer Search')} /> {t('Customer Search')}</Header>
          <Form
            salesChannel={salesChannel}
            searchHandler={this.onSearchClickHandler}
            errorSubject={this.services.formErrorsSubject}
            config={config}
            oldSearchConfig={config}
            loadingSubject={this.services.loadingSubject}
            externalSearchSetting={this.state.externalSearchSetting}
            onError={this.errorHandler}
          />
          <Drawer
            direction='left'
            visible={drawerVisible}
            animation='overlay'
            width='wide'
            allowClose
            toggleVisibility={this.onCloseClickHandler}>

            <SearchResults
              resultSubject={this.services.customerSubject}
              resultMetaSubject={this.services.resultMetaSubject}
              clearSubject={this.services.clearSubject}
              credentialService={this.services.credentialService}
              quickSearchService={this.services.quickSearchService}
              potentialService={this.services.potentialLinkService}
              customerFieldResolver={this.services.customerFieldResolver}
              config={config}
              linkingConfig={linkingConfig}
              linkingService={this.services.linkingService}
              proceedCallback={this.onProceedClickHandler}
              loadingSubject={this.services.loadingSubject}
              contextMenu={[
                {
                  name: t('Edit'),
                  icon: <MdEdit />,
                  callback: (event, data) => { this.handleEditCustomer(data) }
                }
              ]}
            />
            {updateCustomerDialog}
          </Drawer>
          <div id='drawer-backdrop' className={backdropClassName} />
        </React.Fragment>
      );
    }
    return null;
  }

  render() {
    return (
      <I18n ns={['default']}>
        {(t) => this.renderSearchFormWithDrawer(t)}
      </I18n>
    );
  }
}

CustomerSearch.propTypes = {
  salesChannel: PropTypes.string.isRequired,
  config: PropTypes.object.isRequired,
  uhelpAppUrl: PropTypes.string.isRequired
};

export default CustomerSearch;
