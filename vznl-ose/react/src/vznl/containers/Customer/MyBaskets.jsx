/* eslint-disable quotes */
/*
 * Copyright (c) 2018. Dynacommerce B.V.
 */

import React, {Component} from 'react';
import { Tab, Table, Popup, EmptyState, StackIndicator } from '@omnius/react-ui-elements';
import _ from 'lodash';
import { MdRemoveRedEye, MdEdit, MdDelete, MdEmail } from 'react-icons/md';
import RedWarningIcon from './warning-critical.svg';
import YellowWarningIcon from './warning.svg';
import PackageIcon from './PackageIcon';
import './MyBaskets.scss';

class MyBaskets extends Component {
  state = {
    baskets: {},
    offers: {},
    riskOrders: {},
    basketsCount: 0,
    offersCount: 0,
    riskOrdersCount: 0
  };

  componentDidMount = () => {
    this.fetchData().then(data => {
      const baskets = data["body"]["baskets"];
      let basketsCount = baskets.length;
      const offers = data["body"]["offers"];
      let offersCount = offers.length;
      const riskOrders = data["body"]["riskOrders"];
      let riskOrdersCount = riskOrders.length;
      let permissions = data["body"]["permissions"];
      this.setState({
        baskets,
        offers,
        riskOrders,
        basketsCount,
        offersCount,
        riskOrdersCount,
        permissions
      })
    });
  };

  fetchData = async () => {
    try {
      const post_key = document.getElementsByName('post_key')[0].defaultValue;
      const url = '/dataimport/index/loadMyBaskets';
      const headers = {
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'X-Requested-With': 'XMLHttpRequest',
        'PostKey': post_key
      };
      const data = await fetch(url, {
        method: 'POST', // using post to avoid csrf token issues
        headers,
        credentials: 'same-origin'
      });
      return data.json();
    } catch (err) {
      console.error(err);
    }
  };

  lineOfBuisnessLabels = props =>
    <StackIndicator stacks={
      [{
        name: "Mobile",
        cssClass: "stack1",
        enabled: props.value.has_mobile
      },{
        name: "Fixed",
        cssClass: "stack2",
        enabled: props.value.has_fixed
      }]
    }
    />;

  packagesCell = props => <div className="mybaskets-packages-icon"><PackageIcon /> {props.value}</div>;

  warningMessage = props => {
    if (props.value.expired) {
      return <React.Fragment>
        {props.value.date}{' '}
        <Popup
          trigger={
            <img src={RedWarningIcon} alt="!"/>
          }
          header= {Translator.translate('Shoppingbag is no longer valid')}
          content={Translator.translate('Shoppingbag has expired and is no longer valid')}
          basic
          on='hover'
          size='small'
          position='bottom left'
          style={{
            padding: '14px',
            fontSize: '14px',
            fontWeight : 'normal'
          }}
        />
      </React.Fragment>;
    }
    return <React.Fragment>{props.value.date}</React.Fragment>;
  };

  columnsList = [
    {
      header: Translator.translate('Created'),
      accessor: 'date',
      Cell: this.warningMessage
    }, {
      header: Translator.translate('Channel'),
      accessor: 'channel'
    }, {
      header: 'LOB',
      accessor: 'lob',
      Cell: this.lineOfBuisnessLabels
    }, {
      header: Translator.translate('Packages'),
      accessor: 'package_count',
      Cell: this.packagesCell
    }
  ];

  transformTableDataRiskOrders = riskOrders => {
    const array = [];
    Object.keys(riskOrders).forEach(key => {
      let item = _.get(riskOrders,key);
      array.push({
        data:{
          date: {date: item.created_date, expired: item.is_expired},
          raw_date: item.created_date,
          channel: item.channel,
          lob: item.lob,
          id: item.id,
          is_offer: item.is_offer,
          item_content: item.item_content
        },
        contextMenu:[]
      });

      if(this.state.permissions.change_saved_cart) {
        array[key].contextMenu.push({
          name: Translator.translate('Confirm'),
          icon: <MdEdit />,
          callback: function(event) {
            confirmRisk(item.id);
            event.stopPropagation();
          }
        });
      }
      if(this.state.permissions.delete_saved_cart) {
        array[key].contextMenu.push({
          name: Translator.translate('Delete'),
          icon: <MdDelete />,
          callback: function(event) {
            deleteQuote(item.id, item.cart_type);
            event.stopPropagation();
          }
        });
      }
    });
    return array;
  };

  transformTableDataOffers = offers => {
    const array = [];
    Object.keys(offers).forEach(key => {
      let item = _.get(offers,key);
      array.push({
        data:{
          date: {date: item.created_date, expired: item.is_expired},
          raw_date: item.created_date,
          channel: item.channel,
          lob: item.lob,
          id: item.id,
          is_offer: item.is_offer,
          item_content: item.item_content
        },
        contextMenu:[]
      });
      if(!item.is_expired && this.state.permissions.change_saved_offer) {
        array[key].contextMenu.push({
          name: Translator.translate('Confirm'),
          icon: this.getMenuElement(item, <MdEdit />),
          callback: function(event) {
            confirmOffer(item.id);
            event.stopPropagation();
          }
        });
      }
      if(!item.is_expired) {
        array[key].contextMenu.push({
          name: Translator.translate('Send via email'),
          icon: this.getMenuElement(item, <MdEmail />),
          callback: function (event) {
            showSaveCartModal(true, item.id, 1);
            event.stopPropagation();
          }
        });
      }
      if(this.state.permissions.delete_saved_offer) {
        array[key].contextMenu.push({
          name: Translator.translate('Delete'),
          icon: <MdDelete />,
          callback: function(event) {
            deleteQuote(item.id, item.cart_type);
            event.stopPropagation();
          }
        });
      }
    });
    return array;
  };

  getMenuElement = (item, component) => {
    if (item.is_expired) {
      return <div className="baskets-disabled">{component}</div>;
    }
    return component;
  };

  transformTableDataBaskets = baskets => {
    const array = [];
    Object.keys(baskets).forEach(key => {
      let item = _.get(baskets,key);
      array.push({
        data:{
          date: {date: item.created_date, expired: item.is_expired},
          raw_date: item.created_date,
          channel: item.channel,
          packages_count: item.packages_count,
          lob: item.lob,
          id: item.id,
          is_offer: item.is_offer,
          item_content: item.item_content,
          'package_count': item.package_count
        },
        contextMenu:[]
      });
      if (this.state.permissions.change_saved_cart) {
        array[key].contextMenu.push({
          name: Translator.translate('Change'),
          icon: this.getMenuElement(item, <MdEdit />),
          callback: function(event) {
            if (!item.is_expired) {
              reloadQuote(item.id, item.is_offer);
            }
            event.stopPropagation();
          }
        });
      }
      array[key].contextMenu.push({
        name: Translator.translate('Send via email'),
        icon: this.getMenuElement(item, <MdEmail />),
        callback: function (event) {
          if (!item.is_expired) {
            showSaveCartModal(true, item.id);
          }
          event.stopPropagation();
        }
      });
      if(this.state.permissions.delete_saved_cart) {
        array[key].contextMenu.push({
          name: Translator.translate('Delete'),
          icon: <MdDelete/>,
          callback: function (event) {
            deleteQuote(item.id, item.cart_type);
            event.stopPropagation();
          }
        });
      }
    });
    return array;
  };

  rowClickAction = (value, event) => {
    if(this.state.permissions.view_saved_offer && value.is_offer === '1') {
      showDetailsCartModal(value.id, value.type, value.raw_date);
      event.stopPropagation();
    } else if(this.state.permissions.view_saved_cart) {
      showDetailsCartModal(value.id, value.type, value.raw_date);
      event.stopPropagation();
    }
  };

  emptyContainer = (header, contents) => {
    return <EmptyState title={header}><span>{contents}</span></EmptyState>;
  };

  render() {
    let panes = [];
    if (this.state.riskOrdersCount) {
      panes = [
        {
          menuItem: (
            <React.Fragment>
              <img src={YellowWarningIcon} alt="!"/> Risk orders ({this.state.riskOrdersCount})
            </React.Fragment>
          ),
          render: () => <Table data={this.transformTableDataRiskOrders(this.state.riskOrders)} columns={this.columnsList} onRowClick={this.rowClickAction} />
        }, {
          menuItem: `(${Translator.translate('Offers')}) (${this.state.offersCount})`,
          render: () => this.state.offersCount ?
            <Table data={this.transformTableDataOffers(this.state.offers)} columns={this.columnsList} onRowClick={this.rowClickAction} /> :
            this.emptyContainer(Translator.translate('No offers'), Translator.translate('If the customer has offers you\'ll see them here'))
        }, {
          menuItem: `${Translator.translate('Carts')} (${this.state.basketsCount})`,
          render: () => this.state.basketsCount ?
            <Table data={this.transformTableDataBaskets(this.state.baskets)} columns={this.columnsList} onRowClick={this.rowClickAction} /> :
            this.emptyContainer(Translator.translate('No saved shopping bags'), Translator.translate('If the customer has saved shopping bags you\'ll see them here'))
        }
      ];
    } else if(this.state.basketsCount || this.state.offersCount) {
      panes = [
        {
          menuItem: `${Translator.translate('Offers')} (${this.state.offersCount})`,
          render: () => this.state.offersCount ?
            <Table data={this.transformTableDataOffers(this.state.offers)} columns={this.columnsList} onRowClick={this.rowClickAction} /> :
            this.emptyContainer(Translator.translate('No offers'), Translator.translate('If the customer has offers you\'ll see them here'))
        },{
          menuItem: `${Translator.translate('Carts')} (${this.state.basketsCount})`,
          render: () => this.state.basketsCount ?
            <Table data={this.transformTableDataBaskets(this.state.baskets)} columns={this.columnsList} onRowClick={this.rowClickAction} /> :
            this.emptyContainer(Translator.translate('No saved shopping bags'), Translator.translate('If the customer has saved shopping bags you\'ll see them here'))
        }
      ];
    }


    if( panes.length > 0 ) {
      return <Tab panes={panes} />;
    } else {
      return this.emptyContainer(Translator.translate('No saved shopping bags'), Translator.translate('If the customer has saved shopping bags you\'ll see them here'));
    }
  }
}

MyBaskets.propTypes = {
};

export default MyBaskets;
