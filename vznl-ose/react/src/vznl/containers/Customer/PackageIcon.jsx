import React from 'react';

const PackageIcon = () => (
  <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width="14" height="16" viewBox="0 0 14 16">
    <defs>
      <path id="a" d="M0 0h20v20H0z"/>
      <path id="b" d="M10 10.6L17 6l-7 4.6zm0 6.2l-6-3.4V7l6 3.6v6.2zM10 2L6.9 3.7 14 8v3.3l-1 .6V8.6L6 4.3 3 6v8l7 4 7-4V6l-7-4z"/>
    </defs>
    <g fill="none" fill-rule="evenodd" transform="matrix(-1 0 0 1 17 -2)">
      <mask id="c" fill="#fff">
        <use xlinkHref="#b"/>
      </mask>
      <g fill="#2D4552" mask="url(#c)">
        <path d="M0 0h20v20H0z"/>
      </g>
    </g>
  </svg>
);
  
export default PackageIcon;