/*
 * Copyright (c) 2018. Dynacommerce B.V.
 */
'use strict';

import React from 'react';
import '../../bootstrap';
import { shallow } from 'enzyme';
import { expect } from 'chai';

const config = {
  "apiUrl" : "dummy"
};

describe('CustomerSearch', () => {

  it.skip('should render the SearchForm to DOM', () => {
  });

  it.skip('should render the Drawer with SearchResults to DOM', () => {
  });

  it.skip('should render the open Drawer with state set to visible', () => {
  });

  it.skip('should render the close Drawer on default state', () => {
  });

  it.skip('should close the Drawer when close handler is called', () => {
  });
});

