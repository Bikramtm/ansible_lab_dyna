<?php
/**
 * Save logs in app/etc/zenddeployment.log file
 *
 * @param string $message
 */

const OSETEMPLOCATION = "/tmp/OSE_DEPLOYMENT/";

function zendLog(string $message, string $type="INFO")
{
    $timestampedMessage = date("Y-m-d H:i:s") . sprintf("[%s]", getenv('ZS_DEPLOYED_BY')) . " [$type]: " . $message;
    $logFile = OSETEMPLOCATION . 'deployment_' . getenv('ZS_CURRENT_APP_VERSION') . '.log';

    if (file_exists($logFile))
    {
        $fh = fopen($logFile, 'a');
        fwrite($fh, $timestampedMessage . "\n");
    }
    else
    {
        if (!file_exists(OSETEMPLOCATION))
        {
            mkdir(OSETEMPLOCATION, 0777, true);
            zendLog("OSE temporary directory created");
            zendLog($message);
            return;
        }

        $fh = fopen($logFile, 'w');
        fwrite($fh, $timestampedMessage . "\n");
    }
    fclose($fh);
}

/**
 * @param string $value
 */
function changePhpIniValue(string $key, string $value)
{
    zendLog($key." updated to  to: " . $value);
    $phpini = '/usr/local/zend/etc/php.ini';
    $original = file_get_contents($phpini);
    $currentValue = ini_get($key);
    $contents = preg_replace('/'.$key.' *= *'.$currentValue.'/', $key.'='.$value, $original);
    file_put_contents($phpini, $contents);
    chgrp($phpini, 'zend');
    chown($phpini, 'zend');
    zendLog($key." changed to: " . ini_get($key));
}

/**
 * Remove the old zend packages which where deployed (keep the last 2 packages)
 */
function removeOldZendPackages()
{
    zendLog("Removing old zend packages (keeping the last 2 packages)");
    exec("cd /usr/local/zend/tmp/gui && rm `ls /usr/local/zend/tmp/gui -t | grep zpk | awk 'NR>2'`");
}

/**
 * Cleaning corrupted folder
 *
 * @param string $dir
 */
function cleanCorruptFolder(string $dir)
{
    zendLog("Cleaning corrupted folder '$dir'");

    $files = array_diff(scandir($dir), array('.', '..', '_docroot_'));
    foreach ($files as $file) {
        (is_dir("$dir/$file")) ? cleanCorruptFolder("$dir/$file") : unlink("$dir/$file");
    }
}

/**
 * Set permission for directory and sub directories
 *
 * @param $path
 * @param int $permission
 */
function setDirectoryPermission(string $path, int $permission = 0777)
{
    $files = array_diff(scandir($path), array('.', '..'));

    foreach ($files as $file)
    {
        if (is_dir("$path/$file"))
        {
            zendLog("Setting permissions for directory: '$path/$file' to '" . decoct($permission) . "'");
            chmod("$path/$file", $permission);
            setDirectoryPermission("$path/$file", $permission);
        }
        else
        {
            zendLog("Setting permissions for: '$path/$file' to '" . decoct($permission) . "'");
            chmod("$path/$file", $permission);
        }
    }
}

/**
 * If debug-mode.lock file exist debug mode is on
 *
 * @return bool
 */
function debugMode()
{
    return file_exists(__DIR__ . '/debug-mode.lock');
}

class OSEException extends Exception
{
    public function __construct($message, $code = 0, Exception $previous = null)
    {
        zendLog($message);
        echo "$message";
        exit(1);
    }
}
