#!/usr/bin/env bash

function packageZpk() {
    PROJECTROOT=$1
    PACKAGE_NAME=$2
    BUILD_NO=$3
    APP_VERISON=$4
    PACKAGE_TYPE=$5

    echo "- Generating package for type: $PACKAGE_TYPE"
    mkdir -p $PROJECTROOT/build/$PACKAGE_TYPE
    mkdir -p $PROJECTROOT/build/package/opt/omnius/ose/zpk
    cp ${PROJECTROOT}/deployment.xml ${PROJECTROOT}/build/package/opt/omnius/ose/deployment.xml
    cp ${PROJECTROOT}/deployment.properties ${PROJECTROOT}/build/package/opt/omnius/ose/deployment.properties
    cp -Rf ${PROJECTROOT}/package/zpk/* ${PROJECTROOT}/build/package/opt/omnius/ose/zpk/

    # Update release
    xmlstarlet edit -L -N zend=http://www.zend.com/server/deployment-descriptor/1.0 -u "/zend:package/zend:version/zend:release" -v ${APP_VERSION}-${BUILD_NO} ./build/package/opt/omnius/ose/deployment.xml

    echo "- Creating ZPK package"
    /usr/local/bin/zs-client packZpk --folder=${PROJECTROOT}/build/package/opt/omnius/ose \
        --destination=${PROJECTROOT}/build/zpk \
        --name=${PACKAGE_NAME}-${BUILD_NO}.zpk \
        --version=${APP_VERSION}

    # Cleanup
    echo "- Cleaning up temp files"
    rm -f ${PROJECTROOT}/build/package/opt/omnius/ose/deployment.xml
    rm -f ${PROJECTROOT}/build/package/opt/omnius/ose/deployment.properties
    rm -rf ${PROJECTROOT}/build/package/opt/omnius/ose/zpk
}



