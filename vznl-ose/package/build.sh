#!/usr/bin/env bash

#
##
## USAGE: __PROG__
##
## This __PROG__ script can be used to package the code as rpm or deb
##
## __PROG__ has a dependency on FPM. See README.md for more information.
##
## __PROG__ <buildnumber> <resourcetype> <--obfuscate>
##
## resourcetype:
## - rpm
## - deb
##
## Examples:
##   __PROG__
##   __PROG__ --obfuscate

#------------------------------------------------------------------------------
# Init variables
#------------------------------------------------------------------------------
SCRIPTPATH=''
PROJECTROOT=''
me=`basename "$0"`
APP_NAME=''
APP_VERSION=''
PACKAGE_NAME=''
BUILD_NO=$1
PACKAGE_TYPE=$2
VENDOR="Dynacommerce"
ADAPTERS_LIST=()
OBFUSCATATION_ENABLED=0

#------------------------------------------------------------------------------
# Methods
#------------------------------------------------------------------------------
function usage () {
    grep '^##' "$0" | sed -e 's/^##//' -e "s/__PROG__/$me/" 1>&2
}
function setScriptAndProjectPath() {
    case "$(uname -s)" in
        Linux*)
            SCRIPTPATH=$(dirname "$(readlink -f "$0")")
            PROJECTROOT=$SCRIPTPATH/..
        ;;
        Darwin*)
            SCRIPTPATH=$(dirname "$(greadlink -f "$0")")
            PROJECTROOT=$SCRIPTPATH/..
        ;;
        *)
            echo "Could not determine script path"
            exit 1
            ;;
    esac
}

function prepackage() {
    # Empty dir
    rm -rf $PROJECTROOT/build/*

    # Create root dirs
    mkdir -p $PROJECTROOT/build/package/etc/omnius/ose
    mkdir -p $PROJECTROOT/build/package/etc/omnius/ose/conf.d
    mkdir -p $PROJECTROOT/build/package/etc/omnius/ose/conf.d/config
    mkdir -p $PROJECTROOT/build/package/var/log/omnius/ose
    #mkdir -p $PROJECTROOT/build/package/etc/logrotate.d
    mkdir -p $PROJECTROOT/build/package/etc/cron.d
    mkdir -p $PROJECTROOT/build/package/opt/omnius/ose

    # Copy all asset files
    echo "Syncing assets"
    rsync --copy-links -azm \
        --exclude '/app/code/community/Dyna' \
        --exclude '/app/code/community/Omnius' \
        --exclude '/app/code/local' \
        --exclude '/build' \
        --exclude '/.vagrant' \
        --exclude '/.git*' \
        --exclude '/.editorconfig' \
        --exclude '/.eslintrc' \
        --exclude '/CHANGELOG.md' \
        --exclude '/composer.json' \
        --exclude '/composer.lock' \
        --exclude '/obfuscator.yml' \
        --exclude '/deployment.properties' \
        --exclude '/deployment.xml' \
        --exclude '/Jenkinsfile' \
        --exclude '/MIGRATION.md' \
        --exclude '/package.json' \
        --exclude '/package-lock.json' \
        --exclude '/phpunit.xml.dist' \
        --exclude '/README.md' \
        --exclude '/ruleset.xml' \
        --exclude '/ruleset_md.xml' \
        --exclude '/node_modules' \
        --exclude '/package' \
        --exclude '/react' \
        --exclude '/sonar-project.properties' \
        --exclude '/webpack.config.js' \
        --exclude '/tests' \
        --exclude '/scripts' \
        --exclude '/var/data' \
        --exclude '/var/deployments' \
        --exclude '/var/design-reference' \
        --exclude '/dev' \
        --exclude '/ruleset_cs.xml' \
        --exclude '/var/data/seed.sql.gz' \
        --exclude '/var/xsd' \
        $PROJECTROOT/* \
        $PROJECTROOT/build/package/opt/omnius/ose
    echo "- Done"
    echo ""

    # Copy cron
    echo "Setup cron.d"
    cp -Rf $PROJECTROOT/package/cron $PROJECTROOT/build/package/etc/cron.d/omnius-ose
    chmod 0644 $PROJECTROOT/build/package/etc/cron.d/*
    echo "- Done"
    echo ""

    # Redo copy for obfuscation files
    if [[ $OBFUSCATATION_ENABLED == 1 ]]; then
        echo "Obfuscation enabled, scrambling the package now!"
        mkdir -p $PROJECTROOT/build/package/opt/omnius/ose/app/code/community/Dyna
        mkdir -p $PROJECTROOT/build/package/opt/omnius/ose/app/code/community/Omnius

        obfuscate obfuscate "$PROJECTROOT/app/code/community/Dyna/*" "$PROJECTROOT/build/package/opt/omnius/ose/app/code/community/Dyna" --config=$PROJECTROOT/obfuscator.yml -q
        obfuscate obfuscate "$PROJECTROOT/app/code/community/Omnius/*" "$PROJECTROOT/build/package/opt/omnius/ose/app/code/community/Omnius" --config=$PROJECTROOT/obfuscator.yml -q

        TOTAL_LOCAL_FILES=$(find $PROJECTROOT/app/code/local -type f -name "*.php" | wc -l)
        echo "- Total of $TOTAL_LOCAL_FILES local PHP files found"
        if [[ $TOTAL_LOCAL_FILES > 0 ]]; then
            echo "- Found files, lets scramble these too"
            mkdir -p $PROJECTROOT/build/package/opt/omnius/ose/app/code/local
            obfuscate obfuscate "$PROJECTROOT/app/code/local/*" "$PROJECTROOT/build/package/opt/omnius/ose/app/code/local" --config=$PROJECTROOT/obfuscator.yml -q
        fi
        echo "- Scrambling done"
        echo ""
    else
        # Copy all app files
        echo "Syncing app files"
        cp -Rf $PROJECTROOT/app/code/community/Dyna $PROJECTROOT/build/package/opt/omnius/ose/app/code/community/Dyna
        cp -Rf $PROJECTROOT/app/code/community/Omnius $PROJECTROOT/build/package/opt/omnius/ose/app/code/community/Omnius
        cp -Rf $PROJECTROOT/app/code/local $PROJECTROOT/build/package/opt/omnius/ose/app/code/local
        echo "- Done"
        echo ""
    fi

    # Setup config files
    echo "Syncing config files"
    rsync -az $PROJECTROOT/config/config.json.dist $PROJECTROOT/build/package/etc/omnius/ose/conf.d/config/config.json
    rsync -az $PROJECTROOT/config/search.config.json.dist $PROJECTROOT/build/package/etc/omnius/ose/conf.d/config/search.config.json

    rsync -az $PROJECTROOT/app/etc/local.xml.dist $PROJECTROOT/build/package/etc/omnius/ose/conf.d/local.xml
    rsync -az $PROJECTROOT/app/etc/replicas.xml.dist $PROJECTROOT/build/package/etc/omnius/ose/conf.d/replicas.xml
    rsync -az $PROJECTROOT/app/etc/sandbox.xml.dist $PROJECTROOT/build/package/etc/omnius/ose/conf.d/sandbox.xml
    rsync -az $PROJECTROOT/app/etc/job_config.xml.dist $PROJECTROOT/build/package/etc/omnius/ose/conf.d/job_config.xml
    rsync -az $PROJECTROOT/app/etc/import.xml.dist $PROJECTROOT/build/package/etc/omnius/ose/conf.d/import.xml
    rsync -az $PROJECTROOT/app/etc/jobs.xml.dist $PROJECTROOT/build/package/etc/omnius/ose/conf.d/jobs.xml
    echo "- Done"
    echo ""

    # Set version label
    echo "Setup package version label"
    SOFTWARE_LABEL=$(composer config version)
    echo "<?php return '${SOFTWARE_LABEL}';" > $PROJECTROOT/build/package/opt/omnius/ose/app/etc/SOFTWARE_VERSION.php
    echo "- Done"
    echo ""
}

function setScriptArguments()
{
    for argument in "$@"; do
      case $argument in
        "--obfuscate")
          #  OBFUSCATATION_ENABLED=1
        ;;
        *)
            usage
            exit 1
        ;;
      esac
    done
}

function validatePrerequisites()
{
    # Check if obfuscate is installed
    #which obfuscate > /dev/null
    #if [ $? != 0 ]; then
    #    echo "- Obfuscate library not found"
    #    exit 1
    #fi

    for argument in "$@"; do
      case $argument in
        "--obfuscate")
        ;;
        *)
            usage
            exit 1
        ;;
      esac
    done
}

#------------------------------------------------------------------------------
# Execution
#------------------------------------------------------------------------------
setScriptAndProjectPath
validatePrerequisites
setScriptArguments "${@}"
prepackage
