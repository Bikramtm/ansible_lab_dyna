#!/usr/bin/env bash

echo "- Symlink configs"
ln -s /etc/omnius/ose/conf.d/config/*.json /opt/omnius/ose/config/
ln -s /etc/omnius/ose/conf.d/*.xml /opt/omnius/ose/app/etc/
rm -rf /opt/omnius/ose/var/log
ln -s /var/log/omnius/ose/ /opt/omnius/ose/var/log

echo "- Application permissions"
chown -R omnius-ose:omnius-ose /opt/omnius/ose
chown -R omnius-ose:omnius-ose /etc/omnius/ose
chown -R omnius-ose:omnius-ose /var/log/omnius/ose
chmod +s /opt/omnius/ose
chmod +s /etc/omnius/ose
chmod +s /var/log/omnius/ose

echo "- Tooling permissions"
chmod +x /opt/omnius/ose/shell/n98-magerun.phar
chmod +x /opt/omnius/ose/bin/*
chmod +x /opt/omnius/ose/vendor/bin/*
