<?php

use PHPUnit\Framework\TestCase;

class Mage_Core_Model_Session_Abstract_Varien_Test extends TestCase
{
	private function getInstance()
	{
		return $obj = new Mage_Core_Model_Session_Abstract_Varien;
	}

	public function testgetSessionHosts()
	{
		$this->assertInternalType('array',$this->getInstance()->getSessionHosts());
		$this->assertInternalType('array',$this->getInstance()->getValidateHttpUserAgentSkip());
	}

	public function testsetSessionHosts()
	{
		$this->assertInstanceOf(Mage_Core_Model_Session_Abstract_Varien::class,$this->getInstance()->setSessionHosts([]));
		$this->assertInstanceOf(Mage_Core_Model_Session_Abstract_Varien::class,$this->getInstance()->unsetAll());
		$this->assertInstanceOf(Mage_Core_Model_Session_Abstract_Varien::class,$this->getInstance()->clear());
	}

	public function testgetSessionName()
	{
		$this->assertEquals('PHPSESSID',$this->getInstance()->getSessionName());
	}

	public function testgetSessionSaveMethod()
	{
		$this->assertEquals('files',$this->getInstance()->getSessionSaveMethod());
	}

	public function testTrue()
	{
		$this->assertTrue($this->getInstance()->useValidateRemoteAddr());
		$this->assertTrue($this->getInstance()->useValidateHttpVia());
		$this->assertTrue($this->getInstance()->useValidateHttpXForwardedFor());
		$this->assertTrue($this->getInstance()->useValidateHttpUserAgent());
		$this->assertTrue($this->getInstance()->useValidateSessionPasswordTimestamp());
		$this->assertTrue($this->getInstance()->useValidateSessionExpire());
	}

	public function teststart()
	{
		$this->assertInstanceOf(Mage_Core_Model_Session_Abstract_Varien::class,$this->getInstance()->start());
	}

	public function testgetSessionSavePath()
	{
		$this->assertInternalType('string',$this->getInstance()->getSessionSavePath());
	}
}