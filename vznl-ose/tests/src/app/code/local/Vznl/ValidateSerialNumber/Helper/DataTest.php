<?php

use PHPUnit\Framework\TestCase;

class Vznl_ValidateSerialNumber_Helper_DataTest extends TestCase
{
    public function testGetStubClient()
    {
        $helper = Mage::helper('vznl_validateserialnumber');
        $stubClient = new Omnius_Service_Model_Client_StubClient;
        $this->assertEquals($stubClient, $helper->getStubClient());
    }

    /**
     * @param $value
     * @return PHPUnit_Framework_MockObject_MockObject
     */
    protected function mockGetIsStub($value)
    {
        $mock = $this->getMockBuilder(Vznl_ValidateSerialNumber_Helper_Data::class)
            ->setMethods([
                'getAdapterChoice',
                'getLogin',
                'getPassword'
            ])->getMock();
        $mock->method('getAdapterChoice')->willReturn($value);
        $mock->method('getLogin')->willReturn('demouser');
        $mock->method('getPassword')->willReturn('demopassword');
        return $mock;
    }

    public function testValidateSerialNumberIsMapped()
    {
        $mock = $this->mockGetIsStub('Vznl_ValidateSerialNumber_Adapter_Stub_Factory');
        $this->assertInternalType('array', $mock->validateSerialNumber('4000055045800'));
        $mock = $this->mockGetIsStub('invalid');
        $this->assertInternalType('string', $mock->validateSerialNumber('4000055045800'));
    }

    public function testGetAdapterChoice()
    {
        $this->assertEquals(Mage::helper('vznl_validateserialnumber')->getAdapterChoice(), 'Stub');
    }
}