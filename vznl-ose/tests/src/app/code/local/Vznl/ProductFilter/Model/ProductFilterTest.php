<?php

use PHPUnit\Framework\TestCase;


class Vznl_Product_Filter_Model_Test extends TestCase
{

    private function getInstance()
    {
        return Mage::getModel('productfilter/filter');
    }

    public function testInstance()
    {
        $this->assertInstanceOf('Vznl_ProductFilter_Model_Filter', $this->getInstance());
    }

    public function testCreateRule()
    {
        $this->expectException("Exception");

        $this->getInstance()->createRule("999990", "88888");
    }

}
