<?php

use PHPUnit\Framework\TestCase;

class Vznl_Eav_Model_Entity_Type_Test extends TestCase
{
	public function testFetchNewIncrementId()
	{
		$entity_type = new Vznl_Eav_Model_Entity_Type;
		$this->assertEquals($entity_type->fetchNewIncrementId(), Vznl_Checkout_Model_Sales_Order::INCREMENT_ID_PREFIX);
	}
}