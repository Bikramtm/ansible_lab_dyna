<?php

use PHPUnit\Framework\TestCase;

class Vznl_Ilt_Helper_Data_Test extends TestCase
{
    /**
     * @return Vznl_Ilt_Helper_Data
     */
    protected function getInstance()
    {
        return Mage::helper('ilt');
    }

    public function testCorrectHelper()
    {
        $this->assertInstanceOf('Vznl_Ilt_Helper_Data', $this->getInstance());
    }

    public function testFamilyTypesGetter()
    {
        $this->assertInternalType('array', $this->getInstance()->getAllowedFamilyTypes());
    }

    public function testAllowedFamilyTypes()
    {
        $allowed = $this->getInstance()->getAllowedFamilyTypes();
        foreach($allowed as $key)
        {
            $this->assertTrue($this->getInstance()->isValidFamilyType($key));
        }
        $this->assertFalse($this->getInstance()->isValidFamilyType('invalid'));
    }
}
