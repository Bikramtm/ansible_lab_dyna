<?php
use PHPUnit\Framework\TestCase;

/**
 * Class Vznl_Customer_Adapter_Search_Adapter_Test
 */
class Vznl_Customer_Adapter_Search_Adapter_Test extends TestCase
{
    const VODAFONE_CUSTOMER_ID = 414;
    const ZIGGO_CUSTOMER_ID = 415;
    const VODAFONE_CUSTOMER_SEARCH_STACK = 'BSL';
    const ZIGGO_CUSTOMER_SEARCH_STACK = 'PEAL';

    public function testIfReturnsVodafoneSpecificCustomerId()
    {
        $adapter = new Vznl_Customer_Adapter_Search_Adapter();
        $stubData = $adapter->getStubOfModeOne();
        $mappedData = $adapter->mapMode1($stubData);
        $customer = $adapter->getCustomerSearchStackSpecificCustomer($mappedData, self::VODAFONE_CUSTOMER_SEARCH_STACK);
        $this->assertEquals($customer['global_id'], self::VODAFONE_CUSTOMER_ID);
    }

    public function testIfReturnsZiggoSpecificCustomerId()
    {
        $adapter = new Vznl_Customer_Adapter_Search_Adapter();
        $stubData = $adapter->getStubOfModeOne();
        $mappedData = $adapter->mapMode1($stubData);
        $customer = $adapter->getCustomerSearchStackSpecificCustomer($mappedData, self::ZIGGO_CUSTOMER_SEARCH_STACK);
        $this->assertEquals($customer['global_id'], self::ZIGGO_CUSTOMER_ID);
    }
}
