<?php

use PHPUnit\Framework\TestCase;

/**
 * Class Vznl_Customer_Helper_Data_Test
 */
class Vznl_Customer_Helper_Data_Test extends TestCase
{
    /**
     * @var string
     */
    protected $usageUrl = 'http://example.com/';

    /**
     * Set instance of object
     *
     * @return Vznl_Customer_Helper_Data
     */
    protected function getInstance()
    {
        return Mage::helper('vznl_customer/data');
    }

    /**
     * Test if it uses correct class
     */
    public function testCorrectHelper()
    {
        $this->assertInstanceOf('Vznl_Customer_Helper_Data', $this->getInstance());
    }

    /**
     * Test getUsageUrl with no errors
     */
    public function testGetUsageUrl()
    {
        $usageUrl = $this->mockDataHelper()->getUsageUrl('usage_url');
        $this->assertInternalType('string', $usageUrl);
        $this->assertEquals($this->usageUrl, $usageUrl);
    }

    /**
     * Test getVodafoneCustomerConfig with error thrown
     */
    public function testGetVodafoneCustomerConfig()
    {
        $this->expectException(InvalidArgumentException::class);

        $this->getInstance()->getVodafoneCustomerConfig(['test']);
    }

    /**
     * @return Vznl_Customer_Helper_Data
     */
    protected function mockDataHelper()
    {
        $mock = $this->getMockBuilder(Vznl_Customer_Helper_Data::class)
            ->disableOriginalConstructor()
            ->setMethods(['getVodafoneCustomerConfig'])
            ->getMock();
        $mock
            ->method('getVodafoneCustomerConfig')
            ->willReturn($this->usageUrl);

        return $mock;
    }

    public function testGetPrivacyDirectoryValueForSms()
    {
        $instance = $this->getInstance();

        $customer = $this->getCustomer();
        $customer->setData('privacy_sms', true);
        $this->assertEquals(true, $instance->getPrivacyDirectoryValue('SMS', $customer));

        $customer->setData('privacy_sms', false);
        $this->assertEquals(false, $instance->getPrivacyDirectoryValue('SMS', $customer));
    }

    public function testGetPrivacyDirectoryValueForEmail()
    {
        $instance = $this->getInstance();

        $customer = $this->getCustomer();
        $customer->setData('privacy_email', true);
        $this->assertEquals(true, $instance->getPrivacyDirectoryValue('EMAIL', $customer));

        $customer->setData('privacy_email', false);
        $this->assertEquals(false, $instance->getPrivacyDirectoryValue('EMAIL', $customer));
    }

    public function testGetPrivacyDirectoryValueForInvalid()
    {
        $instance = $this->getInstance();

        $customer = $this->getCustomer();
        $this->expectException(InvalidArgumentException::class);
        $instance->getPrivacyDirectoryValue('foobar', $customer);
    }

    public function testGetPrivacyDirectoryValueForDirectoryListing()
    {
        $instance = $this->getInstance();

        $customer = $this->getCustomer();
        $customer->setData('privacy_phone_books', false);
        $customer->setData('privacy_number_information', true);
        $this->assertEquals('Info', $instance->getPrivacyDirectoryValue('DIRECTORY_LISTING', $customer));

        $customer->setData('privacy_phone_books', true);
        $customer->setData('privacy_number_information', false);
        $this->assertEquals('InfoGids', $instance->getPrivacyDirectoryValue('DIRECTORY_LISTING', $customer));

        $customer->setData('privacy_phone_books', false);
        $customer->setData('privacy_number_information', false);
        $this->assertEquals('No', $instance->getPrivacyDirectoryValue('DIRECTORY_LISTING', $customer));

        $customer->setData('privacy_phone_books', true);
        $customer->setData('privacy_number_information', true);
        $this->assertEquals('InfoGids', $instance->getPrivacyDirectoryValue('DIRECTORY_LISTING', $customer));
    }


    public function testGetPrivacyDirectoryValueForPhonebookListing()
    {
        $instance = $this->getInstance();
        $customer = $this->getCustomer();
        $customer->setData('privacy_phone_books', false);
        $customer->setData('privacy_number_information', true);
        $this->assertEquals('Info', $instance->getPrivacyDirectoryValue('PHONEBOOK_LISTING', $customer));

        $customer->setData('privacy_phone_books', true);
        $customer->setData('privacy_number_information', false);
        $this->assertEquals('InfoGids', $instance->getPrivacyDirectoryValue('PHONEBOOK_LISTING', $customer));

        $customer->setData('privacy_phone_books', false);
        $customer->setData('privacy_number_information', false);
        $this->assertEquals('No', $instance->getPrivacyDirectoryValue('PHONEBOOK_LISTING', $customer));

        $customer->setData('privacy_phone_books', true);
        $customer->setData('privacy_number_information', true);
        $this->assertEquals('InfoGids', $instance->getPrivacyDirectoryValue('PHONEBOOK_LISTING', $customer));
    }

    private function getCustomer()
    {
        return Mage::getModel('customer/customer');
    }
}
