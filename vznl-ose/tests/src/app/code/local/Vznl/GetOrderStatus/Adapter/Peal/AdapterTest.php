<?php

use PHPUnit\Framework\TestCase;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Exception\RequestException;

class Vznl_GetOrderStatus_Adapter_Peal_AdapterTest extends TestCase
{
    private function getClient(int $httpResponse = 200, $stub = 200)
    {
        switch ($httpResponse) {
            case 200:
                $mock = new MockHandler([
                    $this->getMockResponse($stub)
                ]);
                break;
            case 400:
                $mock = new MockHandler([
                    new Response(400, [], ''),
                ]);
                break;
            case 500:
                $mock = new MockHandler([
                    new RequestException("Error Communicating with Server", new Request('GET', 'test'))
                ]);
                break;
        }
        $handler = HandlerStack::create($mock);
        return new Client(['handler' => $handler]);
    }

    private function getMockResponse($stub): Response
    {
        return new Response(200, [], file_get_contents(Mage::getModuleDir('etc', 'Vznl_GetOrderStatus') . '/stubs/GetOrderStatus/result.json'));
    }

    protected function mockGetOrderStatusHelperFunctions($user = '', $password = '')
    {
        $mock = $this->getMockBuilder(Vznl_GetOrderStatus_Helper_Data::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'getLogin',
                'getPassword'
            ])
            ->getMock();
        $mock->method('getLogin')
            ->willReturn('demouser');
        $mock->method('getPassword')
            ->willReturn('demopassword');
        return $mock;
    }

    protected function mockAdapterFunctions($user = '', $password = '')
    {
        $endpoint = 'www.dynacommerce.com/';
        $channel = 'channel';
        $cty = 'NL';
        $mock = $this->getMockBuilder(Vznl_GetOrderStatus_Adapter_Peal_Adapter::class)
            ->setConstructorArgs(array($this->getClient(), $endpoint, $channel, $cty))
            ->setMethods([
                'getHelper',
            ])
            ->getMock();
        $mock->method('getHelper')
            ->willReturn($this->mockGetOrderStatusHelperFunctions($user, $password));
        return $mock;
    }

    public function testCallIsMapped()
    {
        $endpoint = 'www.dynacommerce.com';
        $channel = 'channel';
        $cty = 'NL';
        $adapter = new Vznl_GetOrderStatus_Adapter_Peal_Adapter($this->getClient(), $endpoint, $channel, $cty);
        $mockCall = $this->mockAdapterFunctions('demouser', 'demopassword');
        $response = $mockCall->call('399991603');
        $this->assertInternalType("array", $response);
        $adapter = new Vznl_GetOrderStatus_Adapter_Peal_Adapter($this->getClient(), $endpoint, $channel, $cty);
        $this->assertInternalType("array", $adapter->call('399991603'));
    }

    public function testOrderNumberNotProvided()
    {
        $endpoint = 'www.dynacommerce.com';
        $channel = 'channel';
        $cty = 'NL';
        $adapter = new Vznl_GetOrderStatus_Adapter_Peal_Adapter($this->getClient(), $endpoint, $channel, $cty);
        $this->assertContains('No order number provided to adapter', $adapter->call(NULL));
    }

    public function testGetUrlMapped()
    {
        $endpoint = 'www.dynacommerce.com/';
        $channel = 'channel';
        $cty = 'NL';
        $adapter = new Vznl_GetOrderStatus_Adapter_Peal_Adapter($this->getClient(), $endpoint, $channel, $cty);
        $url = $adapter->getUrl('399991603');
        $testUrl = "www.dynacommerce.com/399991603?cty=NL&chl=channel";
        $this->assertEquals($testUrl, $url);
    }


    public function test400ResponseFromService()
    {
        $adapter = new Vznl_GetOrderStatus_Adapter_Peal_Adapter($this->getClient(400), 'www.dynacommerce.com/', 'channel', 'NL');
        $mockCall = $this->mockGetOrderStatusHelperFunctions();
        $response = $adapter->call('399991603');
        $this->assertEquals($response['error'], true);
        $this->assertEquals($response['statusCode'], 400);
    }

    public function test500ResponseFromService()
    {
        $adapter = new Vznl_GetOrderStatus_Adapter_Peal_Adapter($this->getClient(500), 'www.dynacommerce.com/', 'channel', 'NL');
        $mockCall = $this->mockGetOrderStatusHelperFunctions();
        $response = $adapter->call('399991603');
        $this->assertEquals($response['error'], true);
        $this->assertContains($response['statusCode'], array(500, 0));
    }
}
