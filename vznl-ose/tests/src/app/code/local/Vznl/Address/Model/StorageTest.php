<?php

use PHPUnit\Framework\TestCase;

class Vznl_Address_Model_Storage_Test extends TestCase
{
	public function testgetServiceAddress()
	{
		$address = $this->getMockBuilder(Vznl_Address_Model_Storage::class)
			->disableOriginalConstructor()
            ->setMethods([
                'getData'
            ])->getMock();
        $address->method('getData')->willReturn(array(
        	'StreetName' => 'Sittard',
        	'house_number' => 100,
        	'postcode' => '6131AT',
        	'CityName' => 'Sittard',
        	'house_addition' => 'A',
        	'District' => 'Sittard',
        	'Postbox' => '6131AT',
        	'house_number' => 1
        ));

		$this->assertInternalType('array', $address->getServiceAddress());
		$this->assertInternalType('string', $address->getServiceAddress(true));
	}

	public function testgetServiceAddresswillnull()
	{
		$address = $this->getMockBuilder(Vznl_Address_Model_Storage::class)
			->disableOriginalConstructor()
            ->setMethods([
                'getData'
            ])->getMock();
        $address->method('getData')->willReturn(false);

		$this->assertInternalType('array', $address->getServiceAddress());
		$this->assertInternalType('string', $address->getServiceAddress(true));
	}
}