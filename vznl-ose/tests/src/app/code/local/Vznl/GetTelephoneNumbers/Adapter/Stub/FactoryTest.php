<?php

use PHPUnit\Framework\TestCase;

class Vznl_GetTelephoneNumbers_Adapter_Stub_FactoryTest extends TestCase
{
    public function testFactoryCreated()
    {
        $adapter = Vznl_GetTelephoneNumbers_Adapter_Stub_Factory::create();
        $this->assertInstanceOf(Vznl_GetTelephoneNumbers_Adapter_Stub_Adapter::class, $adapter);
    }
}
