<?php

use PHPUnit\Framework\TestCase;

class Vznl_GetTelephoneNumbers_Helper_DataTest extends TestCase
{
    public function testGetStubClient()
    {
        $helper = Mage::helper('vznl_getTelephoneNumbers');
        $stubClient = new Omnius_Service_Model_Client_StubClient;
        $this->assertEquals($stubClient, $helper->getStubClient());
    }

    /**
     * @param $value
     * @return PHPUnit_Framework_MockObject_MockObject
     */
    protected function mockGetIsStub($value)
    {
        $mock = $this->getMockBuilder(Vznl_GetTelephoneNumbers_Helper_Data::class)
            ->setMethods([
                'getAdapterChoice',
                'getLogin',
                'getPassword'
            ])->getMock();
        $mock->method('getAdapterChoice')->willReturn($value);
        $mock->method('getLogin')->willReturn('demouser');
        $mock->method('getPassword')->willReturn('demopassword');
        return $mock;
    }

    public function getMockValidateAddress()
    {
        $validateAddress = array(
            'endpoint' => 'www.dynacommerce.com',
            'channel' => 'channel',
            'cty' => 'NL',
            'houseFlatNumber' => 1,
            'houseFlatExt' => 'A',
            'postCode' => '6221KL',
            'returnset' => 5,
            'consecutiveNum' => 0,
            'customerType' => 'Business',
            'footPrint' => 'ziggo',
        );
        return $validateAddress;
    }

    public function testGetTelephoneNumbersIsMapped()
    {
        $mock = $this->mockGetIsStub('Vznl_GetTelephoneNumbers_Adapter_Stub_Factory');
        $this->assertInternalType('array', $mock->getTelephoneNumbers($this->getMockValidateAddress()));
        $mock = $this->mockGetIsStub('invalid');
        $this->assertInternalType('string', $mock->getTelephoneNumbers($this->getMockValidateAddress()));
    }

    public function testGetAdapterChoice()
    {
        $this->assertEquals(Mage::helper('vznl_getTelephoneNumbers')->getAdapterChoice(), 'Stub');
    }
}