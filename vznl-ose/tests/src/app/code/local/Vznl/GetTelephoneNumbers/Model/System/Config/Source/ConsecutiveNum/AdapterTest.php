<?php

use PHPUnit\Framework\TestCase;

class Vznl_GetTelephoneNumbers_Model_System_Config_Source_ConsecutiveNum_AdapterTest extends TestCase
{
    public function testToArray()
    {
      $entity_type = new Vznl_GetTelephoneNumbers_Model_System_Config_Source_ConsecutiveNum_Adapter;
      $this->assertInternalType('array', $entity_type->toArray());
    }
}