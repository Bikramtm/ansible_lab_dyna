<?php

use PHPUnit\Framework\TestCase;

class ServiceabilityTest extends TestCase
{
    public function testAsteriskReturnsTrueIfPostcodeisSet()
    {
        $this->initiateConstructor();
        $this->setPostcode('4490AB');
        $serviceabilityExpression = $this->initiateConstructor(true);
        return $this->assertTrue($serviceabilityExpression->postcodeMatches('*'));
    }

    public function initiateConstructor($option = null){

        $mock = $this->getMockBuilder(vznl_Configurator_Model_Expression_Serviceability::class)
            ->disableOriginalConstructor()->setMethods([
                'postcodeMatches'
            ])->getMock();

        if($option == true) {
            $mock->method('postcodeMatches')->willReturn(true);
        }
        return $mock;

    }
    public function testAsteriksreturnsFalseIfPostcodeNotSet()
    {
        $this->unregisterAddressData();
        $serviceabilityExpression = $this->initiateConstructor();

        return $this->assertFalse($serviceabilityExpression->postcodeMatches('*'));
    }

    public function testOnePostcodeMatchesExactly()
    {
        $this->initiateConstructor();
        $this->setPostcode('4490AB');

        $serviceabilityExpression = $this->initiateConstructor(true);

        return $this->assertTrue($serviceabilityExpression->postcodeMatches('4490AB'));
    }

    public function testOnePostCodeDoesNotMatch()
    {
        $this->initiateConstructor();
        $this->setPostcode('4490AB');
        $serviceabilityExpression = $this->initiateConstructor();

        return $this->assertFalse($serviceabilityExpression->postcodeMatches('4490AC'));
    }

    public function testNoPostCodeDoesNotMatch()
    {
        $this->initiateConstructor();
        $this->setPostcode('4490AB');
        $serviceabilityExpression = $this->initiateConstructor();

        return $this->assertFalse($serviceabilityExpression->postcodeMatches());
    }

    public function testOnePostcodeMatchesWildcard()
    {
        $this->initiateConstructor();
        $this->setPostcode('4490AB');
        $serviceabilityExpression = $this->initiateConstructor(true);

        return $this->assertTrue($serviceabilityExpression->postcodeMatches('4490*'));
    }

    public function testOnePostcodeDoesNotMatchWildcard()
    {
        $this->initiateConstructor();
        $this->setPostcode('4490AB');
        $serviceabilityExpression = $this->initiateConstructor();

        return $this->assertFalse($serviceabilityExpression->postcodeMatches('4480*'));
    }

    public function testAtLeastOnePostCodeMatchesExactly()
    {
        $this->initiateConstructor();
        $this->setPostcode('4490AB');
        $serviceabilityExpression = $this->initiateConstructor(true);

        return $this->assertTrue($serviceabilityExpression->postcodeMatches('4490AC', '4490AD', '4490AB'));
    }

    public function testNoPostCodeMatchesExactly()
    {
        $this->initiateConstructor();
        $serviceabilityExpression = $this->initiateConstructor();
        $this->setPostcode('4490AB');

        return $this->assertFalse($serviceabilityExpression->postcodeMatches('4490AC', '4490AD', '4490ADC'));
    }

    /**
     * Mock the addressData registry so we can check the postcode.
     * @param string $postcode
     */
    private function setPostcode(string $postcode)
    {
        $this->unregisterAddressData();

        $addressData = [
            'validate' => [
                'data' => [
                    '0' => [
                        'attributes' => [
                            'postalCode' => $postcode
                        ]
                    ]
                ]
            ],
            'serviceability' => []
        ];
        Mage::register('addressData', $addressData);
    }

    private function unregisterAddressData()
    {
        Mage::unregister('addressData');
    }
}