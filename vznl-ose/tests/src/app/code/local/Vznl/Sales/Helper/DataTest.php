<?php

use PHPUnit\Framework\TestCase;

class Vznl_Sales_Helper_Data_Test extends TestCase
{
	/**
      * @runInSeparateProcess
      * @preserveGlobalState disabled
      */
	public function testAdditionalValidation()
	{
		$validateHelper = Mockery::mock('overload:Vznl_ValidateCart_Helper_Data')->makePartial();
		$validateHelper->shouldReceive('validate')->once();
		$validateHelper->shouldReceive('createPrioritizedLists')->once();

		$obj = new Vznl_Sales_Helper_Data;
		$this->assertNull($obj->additionalValidation());
	}
}
