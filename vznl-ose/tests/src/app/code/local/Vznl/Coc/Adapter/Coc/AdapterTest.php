<?php

namespace tests\src\app\code\local\Vznl\Coc\Adapter\Coc;

use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Mage;
use PHPUnit\Framework\TestCase;
use Vznl_Coc_Adapter_Coc_Adapter;

class AdapterTest extends TestCase
{
    private $getMockResponseFromFile;

    protected function helperFunction()
    {

        $requestMock = new MockHandler([

            new Response(200,[],$this->getMockResponseFromFile),
            new Response(200,[],$this->getMockResponseFromFile),
            new Response(200,[],$this->getMockResponseFromFile),
        ]);

        $handler = HandlerStack::create($requestMock);

        return $handler;

    }

    protected function mockHelper($handler)
    {
        $mockData = [
            'service_url' => '/test',
            'client_options' => [
                'handler' => $handler
            ]
        ];

        $mock = $this->getMockBuilder(Vznl_Coc_Adapter_Coc_Adapter::class)
            ->setMethods(['logAction'])
            ->setConstructorArgs(array($mockData))
            ->getMock();

        $mock->method('logAction')->willReturn(null);

        return $mock;

    }

    public function testSend()
    {
        $this->getMockResponseFromFile = '{"creation_time":"12/12/2020","chamber_of_commerce":"123","company_name":"123","house_number":"123","zip_code":"123","id":"123"}';

        $handler = $this->helperFunction();

        $init    = $this->mockHelper($handler);

        $this->assertInternalType('array',
            $init->send(1234));

        $this->assertArrayHasKey('k_v_k_number',
            $init->send(1234));
    }

    //function is commented because it logs error message to phpunit console
//    public function testSendForException()
//    {
//        $this->expectException(\Exception::class);
//
//        $requestMock = new MockHandler([
//            new RequestException('Error Communicating with Server', new Request('GET', 'test'))
//        ]);
//
//        $handler = HandlerStack::create($requestMock);
//
//        $init   = $this->mockHelper($handler);
//
//        $init->send(1234);
//
//    }

    public function testSendForNull()
    {
        $this->getMockResponseFromFile = '';

        $handler = $this->helperFunction();

        $init    = $this->mockHelper($handler);

        $this->assertNull($init->send(1234));
    }

}
