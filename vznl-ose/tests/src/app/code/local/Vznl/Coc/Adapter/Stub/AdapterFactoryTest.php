<?php


namespace tests\src\app\code\local\Vznl\Coc\Adapter\Stub;


use PHPUnit\Framework\TestCase;
use Vznl_Coc_Adapter_Stub_Adapter;
use Vznl_Coc_Adapter_Stub_AdapterFactory;

class AdapterFactoryTest extends TestCase
{
    public function testStaticCreate()
    {
        $this->assertInstanceOf(Vznl_Coc_Adapter_Stub_Adapter::class,Vznl_Coc_Adapter_Stub_AdapterFactory::create(['test']));
    }
}
