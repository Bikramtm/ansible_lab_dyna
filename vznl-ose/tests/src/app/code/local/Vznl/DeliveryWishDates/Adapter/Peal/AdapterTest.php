<?php

use PHPUnit\Framework\TestCase;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Exception\RequestException;

class Vznl_DeliveryWishDates_Adapter_Peal_AdapterTest extends TestCase
{
    /**
     * Random basket ID.
     * @var int
     */
    private $basketId = 1;

    /**
     * Random customer type.
     * @var string
     */
    private $customerType = 'example';

    /**
     * Random delivery method.
     * @var string
     */
    private $deliveryMethod = 'LSP';

    private $isOverstappen = false;
    private $newHardware = false;

    /**
     * @param int $httpResponse
     * @param int $stub
     * @return Client
     */
    private function getClient(int $httpResponse = 200, $stub = 200)
    {
        switch ($httpResponse) {
            case 200:
                $mock = new MockHandler([
                    $this->getMockResponse($stub)
                ]);
                break;
            case 400:
                $mock = new MockHandler([
                    new Response(400, [], ''),
                ]);
                break;
            case 500:
                $mock = new MockHandler([
                    new RequestException("Error Communicating with Server", new Request('GET', 'test'))
                ]);
                break;
        }
        $handler = HandlerStack::create($mock);
        return new Client(['handler' => $handler]);
    }

    public function testGetUrlMapped()
    {
        $endpoint = 'www.dynacommerce.com/resource/deliverywishdates';
        $channel = 'channel';
        $cty = 'NL';
        $adapter = new Vznl_DeliveryWishDates_Adapter_Peal_Adapter($this->getClient(), $endpoint, $channel, $cty);
        $url = $adapter->getUrl();
        $testUrl = "www.dynacommerce.com/resource/deliverywishdates?cty=NL&chl=channel";
        $this->assertEquals($testUrl, $url);
    }

    public function test400ResponseFromService()
    {
        $adapter = new Vznl_DeliveryWishDates_Adapter_Peal_Adapter($this->getClient(400), 'www.dynacommerce.com', 'channelname', 'chl');
        $response = $adapter->call($this->basketId, $this->customerType, $this->deliveryMethod, $this->isOverstappen, $this->newHardware);
        $this->assertEquals($response['error'], true);
        $this->assertEquals($response['code'], 400);
    }

    public function test500ResponseFromService()
    {
        $adapter = new Vznl_DeliveryWishDates_Adapter_Peal_Adapter($this->getClient(500), 'www.dynacommerce.com', 'channelname', 'chl');
        $response = $adapter->call($this->basketId, $this->customerType, $this->deliveryMethod, $this->isOverstappen, $this->newHardware);
        $this->assertEquals($response['error'], true);
    }

    private function getMockResponse($stub)
    {
        return new Response(200, [], file_get_contents(Mage::getModuleDir('etc', 'Vznl_DeliveryWishDates') . '/stubs/DeliveryWishDates/result.json'));
    }
}
