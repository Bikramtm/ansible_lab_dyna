<?php

use PHPUnit\Framework\TestCase;

class Vznl_DeliveryWishDates_Adapter_Stub_AdapterTest extends TestCase
{
    public function testCallIsMapped()
    {
        $adapter = new Vznl_DeliveryWishDates_Adapter_Stub_Adapter();
        $response = $adapter->call($basketId = 1, $customerType = 'example', $deliveryMethod = "LSP", $isOverstappen = true, $newHardware = false);
        $this->assertInternalType("array", $response);
    }
}
