<?php


namespace tests\src\app\code\local\Vznl\SuperOrder\Helper;

use PHPUnit\Framework\TestCase;

class DataTest extends TestCase
{
    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testCheckIfSoFromOnlineEditedOnTelesales()
    {
        $mock = $this->getMockBuilder('Vznl_Superorder_Helper_Data')
            ->setMethods([
                'getOrderEdit',
                'isTelesalesLine'
            ])->getMock();

        $mock->method('getOrderEdit')
            ->willReturn('44');

        $this->assertFalse($mock->checkIfSoFromOnlineEditedOnTelesales());
        $this->assertFalse($mock->checkIfSoFromOnlineEditedOnTelesales());

    }

}
