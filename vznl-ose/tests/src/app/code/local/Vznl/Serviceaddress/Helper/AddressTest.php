<?php

use PHPUnit\Framework\TestCase;

class Vznl_Serviceaddress_Helper_Address_Test extends TestCase
{

	private function getInstance()
	{
		return Mage::helper('vznl_serviceaddress/address');
	}

	public function testGetDefaultServiceAddress()
	{
		$customer = new Mage_Customer_Model_Customer;
		$customer->setData('default_service_address_id',1);

		$this->assertEquals($this->getInstance()->getDefaultServiceAddress($customer), $customer->getData('default_service_address_id'));
	}

	public function testGetDefaultCustomerServiceAddress()
	{
		$mockCustomer = $this->getMockBuilder(Mage_Customer_Model_Customer::class)
            ->disableOriginalConstructor()
            ->setMethods(['getPrimaryAddress'])
            ->getMock();
        $mockCustomer
            ->method('getPrimaryAddress')
            ->willReturn(true);

		$this->assertTrue($this->getInstance()->getDefaultCustomerServiceAddress($mockCustomer));
	}
}