<?php

use PHPUnit\Framework\TestCase;

class Vznl_Serviceaddress_Model_Adminhtml_Observer_Test extends TestCase
{
    private function mockPost()
    {
        $postMock = $this->getMockBuilder(stdClass::class)
            ->setMethods(['getPost'])
            ->getMock();
        $postMock
            ->method('getPost')
            ->willReturn([
                'account' => ['default_serviceaddress' => 1]
            ]);
        return $postMock;
    }

    /**
      * @runInSeparateProcess
      * @preserveGlobalState disabled
      */
	public function testCheckCustomerServiceAddress()
	{

        $customer = new Mage_Customer_Model_Customer;

		$observer = $this->getMockBuilder(stdClass::class)
            ->setMethods(['getCustomer','getRequest'])
            ->getMock();
        $observer
            ->method('getCustomer')
            ->willReturn($customer);
        $observer
            ->method('getRequest')
            ->willReturn($this->mockPost());

		$obj = new Vznl_Serviceaddress_Model_Adminhtml_Observer;
	    $obj->checkCustomerServiceAddress($observer);
        $this->assertEquals($customer->getData('default_service_address_id'), 1);
	}
}