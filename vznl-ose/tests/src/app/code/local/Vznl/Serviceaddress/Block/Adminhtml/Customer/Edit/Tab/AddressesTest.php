<?php

use PHPUnit\Framework\TestCase;

class Vznl_Serviceaddress_Block_Adminhtml_Customer_Edit_Tab_Addresses_Test extends TestCase
{
	public function testConstruct()
	{
		$obj = new Vznl_Serviceaddress_Block_Adminhtml_Customer_Edit_Tab_Addresses;
		$this->assertEquals($obj->getTemplate(), 'vznl_serviceaddress/tab/addresses.phtml');
	}
}