<?php

use PHPUnit\Framework\TestCase;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Exception\RequestException;

class Vznl_ResetBasket_Helper_DataTest extends TestCase
{

    public function testGetStubClient()
    {
        $helper = Mage::helper('vznl_resetbasket');
        $stubClient = new Omnius_Service_Model_Client_StubClient;
        $this->assertEquals($stubClient, $helper->getStubClient());
    }

    protected function mockGetIsStub($value)
    {
        $mock = $this->getMockBuilder(Vznl_ResetBasket_Helper_Data::class)
            ->setMethods([
                'getAdapterChoice',
                'getLogin',
                'getPassword'
            ])->getMock();
        $mock->method('getAdapterChoice')->willReturn($value);
        $mock->method('getLogin')->willReturn('demouser');
        $mock->method('getPassword')->willReturn('demopassword');
        return $mock;
    }

    public function testResetBasketIsMapped()
    {
        $mock = $this->mockGetIsStub('Vznl_ResetBasket_Adapter_Stub_Factory');
        $this->assertInternalType('array', $mock->resetBasket('4000055045800'));
        $mock = $this->mockGetIsStub('invalid');
        $this->assertInternalType('string', $mock->resetBasket('4000055045800'));
    }
} 