<?php

use PHPUnit\Framework\TestCase;

class Vznl_Communication_Helper_Log_Test extends TestCase
{
    protected function getInstance()
    {
        return Mage::helper('communication/log');
    }

    public function testCorrectHelper()
    {
        $this->assertInstanceOf('Vznl_Communication_Helper_Log', $this->getInstance());
    }
}