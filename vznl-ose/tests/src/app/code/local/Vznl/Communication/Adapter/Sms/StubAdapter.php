<?php

use PHPUnit\Framework\TestCase;

class Vznl_Communication_Adapter_Sms_StubAdapter_Test extends TestCase
{
    public function testValidSend()
    {
        $adapter = Vznl_Communication_Adapter_Sms_StubAdapterFactory::create();
        $jobMock = $this->getMockBuilder('Vznl_Communication_Model_Job')
            ->setMethods([
                'getReceiver',
                'getEmailCode',
            ])->getMock();
        $jobMock->method('getReceiver')
            ->willReturn('0687654321');
        $jobMock->method('getEmailCode')
            ->willReturn('SMSTEMPLATE');

        $this->assertTrue($adapter->send($jobMock));
    }

    public function testInvalidSend()
    {
        $adapter = Vznl_Communication_Adapter_Sms_StubAdapterFactory::create();
        $jobMock = $this->getMockBuilder('Vznl_Communication_Model_Job')
            ->setMethods([
                'getReceiver',
                'getEmailCode',
            ])->getMock();

        $jobMock->method('getReceiver')
            ->willReturn('0612345678');
        $jobMock->method('getEmailCode')
            ->willReturn('SMSTEMPLATE');

        $this->assertFalse($adapter->send());
    }

    public function testStubCantGiveAnException()
    {
        $adapter = Vznl_Communication_Adapter_Sms_StubAdapterFactory::create();
        $this->assertEquals($adapter->getLastException(), null);
    }
}
