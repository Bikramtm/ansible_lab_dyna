<?php

use Zend\Soap\Client;
use PHPUnit\Framework\TestCase;

class Vznl_Communication_Adapter_Sms_SendOutBoundNotificationAdapter_Test extends TestCase
{


    const WSDL = __DIR__ . '/SendOutBoundNotificationAdapterTest.wsdl';

    public function testValidEndpointResponse()
    {
        $soapClientMock = $this->getMockFromWsdl(self::WSDL, Zend\Soap\Client::class);
        $soapClientMock->method('__call')
            ->willReturn(true);

        $adapter = new Vznl_Communication_Adapter_Sms_SendOutBoundNotificationAdapter($soapClientMock, $this->getLogger());
        $response = $adapter->send($this->getJobMock());
        $this->assertTrue($response);
    }

    /**
     * Create a mocked job
     * @return PHPUnit_Framework_MockObject_MockObject
     */
    protected function getJobMock()
    {
        $jobMock = $this->getMockBuilder('Vznl_Communication_Model_Job')
            ->setMethods([
                'getReceiver',
                'getEmailCode',
                'getParameters'
            ])->getMock();

        $jobMock->method('getReceiver')
            ->willReturn('0612345678');
        $jobMock->method('getEmailCode')
            ->willReturn('SMSTEMPLATE');
        $jobMock->method('getParameters')
            ->willReturn([
                'dealer_code' => 12345,
                'template_parameters' => [
                    'name' => 'F. Bar',
                    'order_nr' => '60000234345'
                ]
            ]);
        return $jobMock;
    }

    protected function getLogger()
    {
        $loggerWriter = $this->getMockBuilder('Aleron75_Magemonolog_Model_Logger')
            ->setConstructorArgs(array('unittest/services/Communication/sms/' . \Ramsey\Uuid\Uuid::uuid4() . '.log'))
            ->setMethods([
                'getLogger',
                'log'
            ])->getMock();

        return $loggerWriter;
    }
}
