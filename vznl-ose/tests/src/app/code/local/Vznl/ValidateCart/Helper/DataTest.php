<?php

use PHPUnit\Framework\TestCase;

class Vznl_ValidateCart_Helper_DataTest extends TestCase
{
    private function createQuoteItemMock($sku, $action, $priority, &$products, &$sortAddons)
    {
        $productMock = $this->getMockBuilder(Vznl_Checkout_Model_Sales_Quote_Item::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'getSku'
            ])
            ->getMock();
        $productMock->method('getSku')->willReturn($sku);

        $products[] = $productMock;
        $sortAddons[$sku] = [
            'action' => $action,
            'priority' => $priority
        ];
    }

    public function testPrioritizedListCreation()
    {
        $helper = Mage::helper('vznl_validatecart');

        $products = [];
        $sortAddons = [];
        $this->createQuoteItemMock("tarif A", "remove", "1", $products, $sortAddons);
        $this->createQuoteItemMock("tarif B", "add", "1", $products, $sortAddons);
        $this->createQuoteItemMock("addon 1", "add", "6", $products, $sortAddons);
        $this->createQuoteItemMock("addon 2", "add", "2", $products, $sortAddons);
        $this->createQuoteItemMock("addon 3", "add", "4", $products, $sortAddons);
        $this->createQuoteItemMock("addon 4", "remove", "2", $products, $sortAddons);

        $list = $helper->createPrioritizedList($products, $sortAddons);
        $this->assertEquals(6, count($list));

        $expectedList = [
            "tarif B", // add, 1
            "addon 2", // add, 2
            "addon 3", // add, 4
            "addon 1", // add, 6
            "addon 4", // remove, 2
            "tarif A"  // remove, 1
        ];

        foreach ($list as $index => $item) {
            $this->assertEquals($expectedList[$index], $item->getSku());
        }
    }
}
