<?php

namespace tests\src\app\code\local\Vznl\RemoveItemFromBasket\Adapter\Peal;

use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Request;
use Mage;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Response;
use Vznl_RemoveItemFromBasket_Adapter_Peal_Adapter;
use Vznl_RemoveItemFromBasket_Helper_Data;

class AdapterTest extends TestCase
{
    public function testCallForNullBasketId(){

        $dependency = new Client();

        $Adapter = new Vznl_RemoveItemFromBasket_Adapter_Peal_Adapter($dependency,'testendpoint','telesales','NL');
        $this->assertContains('No basketId provided to adapter',$Adapter->call(null));
    }

    protected function getMockerHelperFunctions($dependency){

        $adapterMock = $this->getMockBuilder(Vznl_RemoveItemFromBasket_Adapter_Peal_Adapter::class)
            ->setConstructorArgs(array($dependency,'www.dynacommerce.com/','channel','NL'))
            ->setMethods(['getHelper'])
            ->getMock();

        $adapterHelperMock = $this->getMockBuilder(Vznl_RemoveItemFromBasket_Helper_Data::class)
            ->disableOriginalConstructor()
            ->setMethods(['getLogin','getPassword'])
            ->getMock();

        $adapterHelperMock->method('getLogin')->willReturn('demouser');

        $adapterHelperMock->method('getPassword')->willReturn('demopassword');

        $adapterMock->method('getHelper')->willReturn($adapterHelperMock);

        return $adapterMock;
    }

    public function testCallForValidateBasket(){

        $getMockResponseFromFile = file_get_contents(Mage::getModuleDir('etc', 'Vznl_RemoveItemFromBasket') . '/stubs/RemoveItemFromBasket/result.json');

        $requestMock = new MockHandler([
            new Response(200,[],$getMockResponseFromFile),
            new Response(200,[],$getMockResponseFromFile)
        ]);

        $handler     = HandlerStack::create($requestMock);

        $dependency  = new Client(['handler' => $handler]);

        $adapterMock = $this->getMockerHelperFunctions($dependency);

        $this->assertInternalType('array',$adapterMock->call('4000055045800'));

        $this->assertEquals('4000054909875',$adapterMock->call('4000055045800')['basketId']);
    }

    public function testCallFor500Response()
    {
        $requestMock =  new MockHandler([
            new RequestException('Oops ! Something went wrong',new Request('POST','test')),
            new RequestException('Oops ! Something went wrong',new Request('POST','test'))
        ]);

        $handler = HandlerStack::create($requestMock);

        $dependency = new Client(['handler' => $handler]);

        $adapterMock = $this->getMockerHelperFunctions($dependency);

        $this->assertEquals($adapterMock->call('4000054909875')['error'],true);

        $this->assertContains($adapterMock->call('4000054909875')['message'],'Oops ! Something went wrong');
    }

    public function testGetUrl()
    {
        $adapter = new Vznl_RemoveItemFromBasket_Adapter_Peal_Adapter(new Client(),'www.dynacommerce.com/','telesales','NL');

        $this->assertEquals('www.dynacommerce.com/4000054909875/items/remove?cty=NL&chl=telesales',$adapter->getUrl('4000054909875'));

    }

}
