<?php

use PHPUnit\Framework\TestCase;

class OrdersOverviewTest extends TestCase
{
    protected $validBan = '343385464';
    protected $invalidBan = '123456789';
    protected $endpoint = 'rest/orders/overview/';
    
    /**
     * Check if customer as more than 1 order
     */
    public function testCustomerHasOrders()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$this->endpoint.$this->invalidBan);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec ($ch);
        $data = json_decode($response);
        $this->assertGreaterThan(count($data), 1);
    }
    
    /**
     * Check if response is valid with an invalid BAN
     * Api response should be that the customer was not found
     */
    public function testInvalidBan()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$this->endpoint.$this->invalidBan);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec ($ch);
        $data = json_decode($response);
        $this->assertEquals($data->message, 'Invalid Ban'); //@todo catch the correct request once the service is up
    }
    
    /**
     * Test if there is a valid API response with wrong endpoint
     */
    public function testEndpointInvalid()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,API_HOST.$this->endpoint.'test');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec ($ch);
        $this->assertEquals($response, 'Method not implemented');
    }
    
}