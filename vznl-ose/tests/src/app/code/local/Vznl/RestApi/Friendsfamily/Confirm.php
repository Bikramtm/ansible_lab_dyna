<?php

use PHPUnit\Framework\TestCase;

class FriendsfamilyConfirmTest extends TestCase
{
    protected $endpoint = 'rest/friendsfamily/confirm';
    protected $requestEndpoint = 'rest/friendsfamily/request';

    public function testInvalidToken()
    {
        $params = ['token' => 'unknown'];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,API_HOST.$this->endpoint);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->getPostPayload($params));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec ($ch);
        curl_close ($ch);
        $data = json_decode($response);
        $this->assertEquals($data->error[0]->code, '01');
        $this->assertFalse($data->success);
    }

    public function testMismatchFrom()
    {
        $params = ['token' => $this->getValidToken(), 'email'=>'unknown@unknown.tld'];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,API_HOST.$this->endpoint);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->getPostPayload($params));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec ($ch);
        curl_close ($ch);
        $data = json_decode($response);
        $this->assertEquals($data->error[0]->code, '02');
        $this->assertFalse($data->success);
    }

    public function testCodeAlreadyValidated()
    {
        $token = $this->getValidToken();
        $params = ['token' => $token, 'email' => 'ubuymailtest@dynalean.eu'];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,API_HOST.$this->endpoint);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->getPostPayload($params));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec ($ch);
        curl_close ($ch);
        $data = json_decode($response);

        $params = ['token' => $token, 'email' => 'ubuymailtest@dynalean.eu'];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,API_HOST.$this->endpoint);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->getPostPayload($params));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec ($ch);
        curl_close ($ch);
        $data = json_decode($response);
        $this->assertEquals($data->error[0]->code, '03');
        $this->assertFalse($data->success);
    }

    public function testHappyFlow()
    {
        $token = $this->getValidToken();
        $params = ['token' => $token, 'email' => 'ubuymailtest@dynalean.eu'];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,API_HOST.$this->endpoint);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->getPostPayload($params));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec ($ch);
        curl_close ($ch);
        $data = json_decode($response);
        $this->assertNotNull($data->payload->token);
        $this->assertTrue($data->success);
    }
    /**
     * Get a valid token from the webservice
     * @return string
     */
    protected function getValidToken()
    {
        $params = ['from' => 'ubuymailtest@dynalean.eu', 'target'=>'ubuymailtest@dynalean.eu'];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,API_HOST.$this->requestEndpoint);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->getPostPayload($params));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec ($ch);
        curl_close ($ch);
        $data = json_decode($response);
        return $data->payload->token;
    }
      
    /**
     * Get the payload of post data to perform API call
     * @param string $order
     * @param string $email
     * @return array
     */
    protected function getPostPayload(array $data=[])
    {
        $fieldsString = '';
        $fields = [];
        foreach($data as $key => $value){
            $fields[$key] = urlencode($value);
        }
        foreach($fields as $key=>$value) { 
            $fieldsString .= $key.'='.$value.'&'; 
        }
        rtrim($fieldsString, '&');
        return $fieldsString;
    }
}