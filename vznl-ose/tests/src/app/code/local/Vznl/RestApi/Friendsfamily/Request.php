<?php

use PHPUnit\Framework\TestCase;

class FriendsfamilyRequestTest extends TestCase
{
    protected $endpoint = 'rest/friendsfamily/request';

    public function testMissingParams()
    {
        $params = [];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,API_HOST.$this->endpoint);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->getPostPayload($params));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec ($ch);
        curl_close ($ch);
        $data = json_decode($response);
        $this->assertEquals($data->error[0]->code, '01');
        $this->assertFalse($data->success);

        $params = [
            'from' => 'my@email.tld',
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,API_HOST.$this->endpoint);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->getPostPayload($params));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec ($ch);
        curl_close ($ch);
        $data = json_decode($response);
        $this->assertEquals($data->error[0]->code, '02');
        $this->assertFalse($data->success);
    }

    public function testInvalidParams()
    {
        $params = ['from' => 'invalid', 'target'=>'email@domain.tld'];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,API_HOST.$this->endpoint);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->getPostPayload($params));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec ($ch);
        curl_close ($ch);
        $data = json_decode($response);
        $this->assertEquals($data->error[0]->code, '03');
        $this->assertFalse($data->success);

        $params = [
            'from' => 'email@domain.tld',
            'target' => 'invalid'
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,API_HOST.$this->endpoint);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->getPostPayload($params));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec ($ch);
        curl_close ($ch);
        $data = json_decode($response);
        $this->assertEquals($data->error[0]->code, '04');
        $this->assertFalse($data->success);
    }

    public function testBlockedRelation()
    {
        $params = ['from' => 'blocked@blocked.eu', 'target'=>'ubuymailtest@dynalean.eu'];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,API_HOST.$this->endpoint);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->getPostPayload($params));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec ($ch);
        curl_close ($ch);
        $data = json_decode($response);
        $this->assertEquals($data->error[0]->code, '05');
        $this->assertFalse($data->success);
    }
    
    public function testNoRelationFound()
    {
        $params = ['from' => 'unknown@relation.tld', 'target'=>'ubuymailtest@dynalean.eu'];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,API_HOST.$this->endpoint);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->getPostPayload($params));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec ($ch);
        curl_close ($ch);
        $data = json_decode($response);
        $this->assertEquals($data->error[0]->code, '06');
        $this->assertFalse($data->success);
    }

    public function testThresholdExceeded()
    {
        // Request first token
        $params = ['from' => 'valid@valid.eu', 'target'=>'ubuymailtest@dynalean.eu'];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,API_HOST.$this->endpoint);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->getPostPayload($params));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec ($ch);
        curl_close ($ch);

        // Threshold exceed code
        $params = ['from' => 'valid@valid.eu', 'target'=>'ubuymailtest@dynalean.eu'];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,API_HOST.$this->endpoint);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->getPostPayload($params));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec ($ch);
        curl_close ($ch);

        $data = json_decode($response);
        $this->assertEquals($data->error[0]->code, '07');
        $this->assertFalse($data->success);
    }

    /**
     * Get the payload of post data to perform API call
     * @param string $order
     * @param string $email
     * @return array
     */
    protected function getPostPayload(array $data=[])
    {
        $fieldsString = '';
        $fields = [];
        foreach($data as $key => $value){
            $fields[$key] = urlencode($value);
        }
        foreach($fields as $key=>$value) { 
            $fieldsString .= $key.'='.$value.'&'; 
        }
        rtrim($fieldsString, '&');
        return $fieldsString;
    }
}