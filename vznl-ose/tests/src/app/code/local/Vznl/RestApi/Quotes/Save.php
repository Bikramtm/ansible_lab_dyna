<?php

use PHPUnit\Framework\TestCase;

class QuotesSaveTest extends TestCase
{
    protected $cleanData = '{
        "is_business": false,
        "chosen_order_type_is_business": false,
        "dealer_code": "00822033",
        "contracting_party": {
            "billing_customer_id": "343385420",
            "gender": "male",
            "initials": "P",
            "last_name_prefix": "van",
            "last_name": "Jansen",
            "email": "ubuymailtest@dynalean.eu",
            "birthdate": "147394800000",
            "phone_1": "0612345678",
            "phone_2": "",
            "invoice_address": {
                "city": "Venlo",
                "country": "NLD",
                "postal_code": "5911AC",
                "street": "Deken van Oppensingel",
                "house_number": "125",
                "house_number_addition": ""
            }
        },
        "business_details": {
            "contracting_party": {
                "gender": "male",
                "initials": "P",
                "last_name_prefix": "",
                "last_name": "Jansen",
                "birthdate": "147394800000",
                "phone_1": "0612345678",
                "email": "ubuymailtest@dynalean.eu"
            },
            "company": {
                "billing_customer_id": "343386205",
                "coc_number": "14129925",
                "establish_date": "147394800000",
                "legal_form": "7",
                "name": "Company to test with",
                "vat_number": "NL0032421392",
                "address": {
                    "city": "Venlo",
                    "country": "NLD",
                    "postal_code": "5911AC",
                    "street": "Deken van Oppensingel",
                    "house_number": "125",
                    "house_number_addition": ""
                }
            }
        },
        "delivery_options": {
            "selected_delivery_option": "home_delivery",
            "home_delivery": {
                "price": "0,00",
                "address_same_as_invoice": true
            }
        },
        "privacy": {
            "terms_agreed": true,
            "marketing": true,
            "number_info": false
        },
        "payment_details": {
            "account_name": "Pim Jansen",
            "account_nr": "NL66RABO0120785498"
        },
        "identity": {
            "identity_expiry_date": "1453294800000",
            "identity_number": "PJ1234567",
            "identity_type": "PASSPORT",
            "nationality": "NL"
        },
        "payment_method": "adyen_hpp",
        "products": [{
                "package_id": "1",
                "quantity": 1,
                "salestype": "acquisition",
                "ctn": "",
                "device_sku": "2100018",
                "subscription_sku": "4100010",
                "sim_sku": ""
        }]
    }';
    protected $pointer;
    protected $endpoint = 'rest/quotes';
    
    /**
     * Test acquisition happy flow shopping cart mutations
     */
    public function testHappyFlowAcquisition()
    {
        // Init shopping cart
        $this->pointer = json_decode($this->cleanData);
        $response = $this->execute();
        $this->assertNotNull($response->dynalean_quote_id);
        $this->assertCount(0, $response->errors); // Check if there are 0 errors

        // Run a second shopping cart mutation
        $response = $this->execute();
        $this->assertCount(0, $response->errors); // Check if there are 0 errors
    }

    /**
     * Test retention happy flow shopping cart mutations
     */
    public function testHappyFlowRetention()
    {
        // Init shopping cart
        $this->pointer = json_decode($this->cleanData);
        $this->pointer->products[0]->ctn = '31652936081';
        $this->pointer->products[0]->salestype = 'retention';
        $response = $this->execute();
        $this->assertNotNull($response->dynalean_quote_id);
        $this->assertCount(0, $response->errors); // Check if there are 0 errors

        // Run a second shopping cart mutation
        $response = $this->execute();
        $this->assertCount(0, $response->errors); // Check if there are 0 errors
    }

    /**
     * Test acquisition business customer
     */
    public function testBusinessHappyFlow()
    {
        // Init shopping cart
        $this->pointer = json_decode($this->cleanData);
        $this->pointer->is_business = true;
        $this->pointer->chosen_order_type_is_business = true;
        $response = $this->execute();
        $this->assertCount(0, $response->errors); // Check if there are 0 errors
    }

    /**
     * Test retention happy flow shopping cart mutations
     */
    public function testHappyFlowBusinessRetention()
    {
        // Init shopping cart
        $this->pointer = json_decode($this->cleanData);
        $this->pointer->is_business = true;
        $this->pointer->products[0]->ctn = '31652998059';
        $this->pointer->products[0]->salestype = 'retention';
        $response = $this->execute();
        $this->assertNotNull($response->dynalean_quote_id);
        $this->assertCount(0, $response->errors); // Check if there are 0 errors

        // Run a second shopping cart mutation
        $response = $this->execute();
        $this->assertCount(0, $response->errors); // Check if there are 0 errors
    }
    /**
     * Test invalid COC
     */
    public function testBusinessCOC()
    {
        // Init shopping cart
        $this->pointer = json_decode($this->cleanData);
        $this->pointer->is_business = true;
        $this->pointer->chosen_order_type_is_business = true;
        $this->pointer->business_details->company->coc_number = 'abcd';
        $response = $this->execute();
        $this->assertEquals($response->errors[0]->message, 'Field has invalid data');
    }

    /**
     * Test retention flow for unknown customer
     */
    public function testRetentionUnknownCustomer()
    {
        // Init shopping cart
        $this->pointer = json_decode($this->cleanData);
        $this->pointer->contracting_party->billing_customer_id = '1234567890';
        $this->pointer->products[0]->ctn = '316123456789';
        $this->pointer->products[0]->salestype = 'retention';
        $response = $this->execute();
        $errorMessages=[];
        foreach($response->errors as $error){
            $errorMessages[] = $error->message;
        }
        $this->assertNotNull($response->dynalean_quote_id);
        $this->assertContains('No customer found for billing_customer_id "1234567890"', $errorMessages);

        // Run a second shopping cart mutation
        $response = $this->execute();
        $errorMessages=[];
        foreach($response->errors as $error){
            $errorMessages[] = $error->message;
        }
        $this->assertContains('No customer found for billing_customer_id ""', $errorMessages);

        // Init shopping cart
        $this->pointer = json_decode($this->cleanData);
        $this->pointer->is_business = true;
        $this->pointer->business_details->company->billing_customer_id = '1234567890';
        $this->pointer->products[0]->ctn = '31612345678';
        $this->pointer->products[0]->salestype = 'retention';
        $response = $this->execute();
        $errorMessages=[];
        foreach($response->errors as $error){
            $errorMessages[] = $error->message;
        }
        $this->assertNotNull($response->dynalean_quote_id);
        $this->assertContains('No customer found for billing_customer_id "1234567890"', $errorMessages);

        // Run a second shopping cart mutation
        $response = $this->execute();
        $errorMessages=[];
        foreach($response->errors as $error){
            $errorMessages[] = $error->message;
        }
        $this->assertContains('No customer found for billing_customer_id ""', $errorMessages);
    }

    /**
     * The API response on invalid products
     */
    public function testInvalidProducts()
    {
        // Init shopping cart
        $this->pointer = json_decode($this->cleanData);
        $this->pointer->products[0]->device_sku = 123456789;
        $response = $this->execute();

        $errorMessages=[];
        foreach($response->errors as $error){
            $errorMessages[] = $error->message;
        }
        $this->assertNotNull($response->dynalean_quote_id);
        $this->assertContains('Product with SKU "123456789" not found.', $errorMessages);
    }

    /**
     * Validate required fields
     */
    public function testRequiredFields()
    {
        // Init shopping cart
        $this->pointer = json_decode($this->cleanData);
        unset($this->pointer->contracting_party);
        unset($this->pointer->business_details);
        unset($this->pointer->delivery_options);
        unset($this->pointer->payment_details);
        unset($this->pointer->identity);
        unset($this->pointer->dealer_code);
        unset($this->pointer->payment_method);
        unset($this->pointer->privacy);
        $response = $this->execute();

        $errorMessages=[];
        foreach($response->errors as $error){
            $errorMessages[$error->item_key] = $error->item_key.' - '.$error->message;
        }
        $this->assertNotNull($response->dynalean_quote_id);
        $this->assertContains('dealer_code - Dealer code "" not found', $errorMessages);
        $this->assertContains('contracting_party.initials - This is a required field.', $errorMessages);
        $this->assertContains('contracting_party.last_name - This is a required field.', $errorMessages);
        $this->assertContains('contracting_party.gender - This is a required field.', $errorMessages);
        $this->assertContains('contracting_party.birthdate - This is a required field.', $errorMessages);
        $this->assertContains('identity.identity_type - This is a required field.', $errorMessages);
        $this->assertContains('identity.identity_number - This is a required field.', $errorMessages);
        $this->assertContains('identity.id_expiry_date - This is a required field.', $errorMessages);
        $this->assertContains('identity.nationality - This is a required field.', $errorMessages);
        $this->assertContains('contracting_party.invoice_address.street - Dit is een verplicht veld.', $errorMessages);
        $this->assertContains('contracting_party.invoice_address.city - Dit is een verplicht veld.', $errorMessages);
        $this->assertContains('contracting_party.invoice_address.zipcode - Dit is een verplicht veld.', $errorMessages);
        $this->assertContains('contracting_party.invoice_address.country - Dit is een verplicht veld.', $errorMessages);
        $this->assertContains('payment_details.account_nr - Dit is een verplicht veld.', $errorMessages);
        $this->assertContains('payment_details.account_name - Dit is een verplicht veld.', $errorMessages);
        $this->assertContains('payment_method - Payment method "" not supported. Possible values: "adyen_hpp, cashondelivery, checkmo, free".', $errorMessages);
        $this->assertContains('delivery_options - Delivery option "" does not exists. Suggested values: pick_up_in_shop/home_delivery', $errorMessages);
    }

        /**
     * The API response on invalid products
     */
    public function testInvalidPaymentMethod()
    {
        // Init shopping cart
        $this->pointer = json_decode($this->cleanData);
        $response = $this->execute();
        $this->assertNotNull($response->dynalean_quote_id);
        $this->assertCount(0, $response->errors); // Check if there are 0 errors

        // Update payment method
        $this->pointer->payment_method = 'foo';
        $response = $this->execute();
        $this->assertNotNull($response->dynalean_quote_id);
        $this->assertCount(1, $response->errors); // Check for error

        // Update payment method
        $this->pointer->payment_method = 'cashondelivery';
        $response = $this->execute();
        $this->assertNotNull($response->dynalean_quote_id);
        $this->assertCount(0, $response->errors); // Check if there are 0 errors
    }

    /**
     * The API response on invalid package saleType
     */
    public function testInvalidPackageSaleType()
    {
        // Init shopping cart
        $this->pointer = json_decode($this->cleanData);
        $this->pointer->products[0]->salestype = 'unknown';
        $response = $this->execute();
        $this->assertEquals($response->errors[0]->message, 'CTN "" does not belong to the selected customer');
    }

    /**
     * Test the package sequence numbering
     */
    public function testInvalidPackageIDs()
    {
        // Not starting with 1
        $this->pointer = json_decode($this->cleanData);
        $this->pointer->products[0]->package_id = 2; // Not start with 1
        $response = $this->execute();
        $this->assertCount(1, $response->errors);

        // Correct with 2 packages
        $this->pointer = json_decode($this->cleanData);
        $this->pointer->products[1] = clone $this->pointer->products[0];
        $this->pointer->products[0]->package_id = 1; // Not start with 1
        $this->pointer->products[1]->package_id = 2; // Not start with 1
        $response = $this->execute();
        $this->assertCount(0, $response->errors);

        // Missing sequence key
        $this->pointer->products[1]->package_id = 5; // Not start with 1
        $response = $this->execute();
        $this->assertCount(1, $response->errors);
    }
    
    /**
     * Make the API call with the object pointer
     * @return array
     */
    protected function execute()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,API_HOST.$this->endpoint);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($this->pointer));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER,  array('Content-Type: text/plain')); 
        $response = curl_exec ($ch);
        curl_close ($ch);
        
        $this->pointer = json_decode($response);
        return $this->pointer;
    }
}