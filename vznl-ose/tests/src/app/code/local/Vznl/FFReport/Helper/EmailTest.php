<?php

use PHPUnit\Framework\TestCase;

class Vznl_FFReport_Helper_Email_Test extends TestCase
{
	private function getRequestModel()
	{
		return Mage::getModel("ffhandler/request");
	}

	public function testEmailBlocked()
	{
		$requestModel = $this->getRequestModel();
		$this->assertEmpty($this->mockFFReport($requestModel)->emailBlocked('sameple@email.com'));
	}

	private function mockFFReport($value)
	{
		$mock = $this->getMockBuilder(Vznl_FFReport_Helper_Email::class)
		->setMethods([
                'send',
            ])->getMock();
        $mock->method('send')->willReturn($value);
        return $mock;
	}


	public function testNoRelationFound()
	{
		$requestModel = $this->getRequestModel();
		$this->assertNull($this->mockFFReport($requestModel)->noRelationFound($requestModel, []));
	}

	public function testFfCodeGenerated()
	{
		$requestModel = $this->getRequestModel();
		$this->assertNull($this->mockFFReport($requestModel)->ffCodeGenerated($requestModel));
	}

	public function testFfCodeUseReminder()
	{
		$requestModel = $this->getRequestModel();
		$this->assertNull($this->mockFFReport($requestModel)->ffCodeUseReminder($requestModel));
	}

	public function testDispatchFFCodeUsageOverview()
	{
		$requestModel = $this->getRequestModel();
		$this->assertNull($this->mockFFReport($requestModel)->dispatchFFCodeUsageOverview($requestModel));
	}

	public function testRequestSuccessfullyGenerated()
	{
		$requestModel = $this->getRequestModel();
		$this->assertNull($this->mockFFReport($requestModel)->requestSuccessfullyGenerated($requestModel));
	}

	public function testFfCodeUsed()
	{
		$requestModel = $this->getRequestModel();
		$order = new Dyna_Superorder_Model_Superorder;
		$this->assertNull($this->mockFFReport($requestModel)->ffCodeUsed($order, $requestModel));
	}

	public function testSend()
	{
		$obj = Mage::helper("vznl_ffreport/email");
		$this->assertNull($obj->send('',[]));
	}
}