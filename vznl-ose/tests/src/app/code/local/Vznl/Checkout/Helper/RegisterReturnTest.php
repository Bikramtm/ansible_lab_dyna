<?php
use PHPUnit\Framework\TestCase;
class Vznl_Checkout_Helper_RegisterReturn_Test extends TestCase
{
	public function testgetAllowedFields()
	{
		$packageData['complete_return'] = true;
		$triggerComplete = true;        
		$mock = new Vznl_Checkout_Helper_RegisterReturn();		
		$res = $this->invokeMethod($mock, 'getAllowedFields', array($packageData, $triggerComplete));		
		$this->assertInternalType('array', $res);
		$this->assertContains('return_notes', $res);
		$this->assertContains('not_damaged_items', $res);
		$this->assertContains('return_channel', $res);
		if($triggerComplete)
			$this->assertContains('complete_return', $res);
		if (isset($packageData['complete_return']) && $packageData['complete_return'])
			$this->assertContains('return_date', $res);
	}
	/**
	 * Call protected/private method of a class.
	 *
	 * @param object &$object    Instantiated object that we will run method on.
	 * @param string $methodName Method name to call
	 * @param array  $parameters Array of parameters to pass into method.
	 *
	 * @return mixed Method return.
	 */
	public function invokeMethod(&$object, $methodName, array $parameters = array())
	{
		$reflection = new \ReflectionClass(get_class($object));
		$method = $reflection->getMethod($methodName);
		$method->setAccessible(true);
		return $method->invokeArgs($object, $parameters);
	}
}