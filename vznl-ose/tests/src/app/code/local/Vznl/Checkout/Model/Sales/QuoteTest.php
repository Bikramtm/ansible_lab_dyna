<?php

use PHPUnit\Framework\TestCase;

/**
 * Class Vznl_Catalog_Model_Product_Test
 */
class Vznl_Checkout_Model_Sales_Quote_Test extends TestCase
{
    /**
     * Get model instance
     * @return Dyna_Checkout_Model_Sales_Quote
     */
    private function getQuoteInstance()
    {
        return Mage::getModel('sales/quote');
    }

    /**
     * Test correct model instance
     */
    public function testQuoteInstance()
    {
        $this->assertInstanceOf('Vznl_Checkout_Model_Sales_Quote', $this->getQuoteInstance());
    }

    /**
     * test if loan and ilt are required
     */
    public function testIsLoanAndIltRequired()
    {
        $quote = $this->mockQuote();
        $this->assertTrue($quote->isLoanRequired());
        $this->assertTrue($quote->isIltRequired());
    }

    /**
     * test if loan and ilt are not required
     */
    public function testIsLoanAndIltNotRequired()
    {
        $quote = $this->mockQuote(false);
        $this->assertFalse($quote->isLoanRequired());
        $this->assertFalse($quote->isIltRequired());
    }

    /**
     * @return Vznl_Checkout_Model_Sales_Quote
     */
    protected function mockQuote($val = true)
    {
        $quote = $this->getMockBuilder(Vznl_Checkout_Model_Sales_Quote::class)
            ->setMethods([
                'getAllItems',
                'getProductFromItem',
                'getCustomer'
            ])
            ->getMock();

        $quote
            ->method('getAllItems')
            ->willReturn([$this->mockItem($val)]);

        if ($val) {
            $quote
                ->method('getProductFromItem')
                ->with(1000001)
                ->willReturn($this->mockProduct(
                    Vznl_Catalog_Model_Product::LOAN_INDICATED_ATTRIBUTE,
                    Vznl_Catalog_Model_Product::ILT_ATTRIBUTE_VALUE
                ));
        } else {
            $quote
                ->method('getProductFromItem')
                ->with(1000001)
                ->willReturn($this->mockProduct());
        }

        $quote
            ->method('getCustomer')
            ->willReturn($this->mockCustomer());

        return $quote;
    }

    /**
     * Mock product object
     *
     * @param string $attrName
     * @param string $attrValue
     *
     * @return Vznl_Catalog_Model_Product
     */
    protected function mockProduct($attrName = false, $attrValue = false)
    {
        /** @var Vznl_Catalog_Model_Product $product */
        $product = $this->getMockBuilder(Vznl_Catalog_Model_Product::class)
            ->setMethods([
                'getAttributeText',
            ])
            ->getMock();

        if ($attrName && $attrValue) {
            $product
                ->method('getAttributeText')
                ->with($attrName)
                ->willReturn($attrValue);
        }

        return $product;
    }

    /**
     * @return Dyna_Checkout_Model_Sales_Quote_Item
     */
    protected function mockItem($val = true)
    {
        $item = $this->getMockBuilder(Dyna_Checkout_Model_Sales_Quote_Item::class)
            ->setMethods([
                'getProductId',
                'getProduct'
            ])
            ->getMock();

        if ($val) {
            $item
                ->method('getProduct')
                ->willReturn($this->mockProduct(
                    Vznl_Catalog_Model_Product::LOAN_INDICATED_ATTRIBUTE,
                    Vznl_Catalog_Model_Product::ILT_ATTRIBUTE_VALUE
                ));
        } else {
            $item
                ->method('getProduct')
                ->willReturn($this->mockProduct());
        }

        $item
            ->method('getProductId')
            ->willReturn(1000001);

        return $item;
    }

    /**
     * @return Dyna_Customer_Model_Customer
     */
    protected function mockCustomer()
    {
        $customer = $this->getMockBuilder(Dyna_Customer_Model_Customer::class)
            ->getMock();

        return $customer;
    }
}

