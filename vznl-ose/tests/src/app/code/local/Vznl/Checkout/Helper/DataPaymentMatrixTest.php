<?php

use PHPUnit\Framework\TestCase;
/**
 * Created by PhpStorm.
 * User: sander.merks
 * Date: 3-11-2016
 * Time: 1:29 PM
 */
class Vznl_Checkout_Helper_DataPaymentMatrix_Test extends TestCase
{

    /**
     * Checks if getDeliveryOptions returns the expected response when the website is retail.
     */
    public function testGetDeliveryOptionsRetail()
    {
        $helper = $this->getHelperAsMock();
        $expectedResult = ['deliver', 'split', 'pickup', 'direct'];

        // For retail
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE));
        $deliveryOptions = $helper->getDeliveryOptions();
        $this->assertTrue(is_array($deliveryOptions));
        $this->assertCount(count($expectedResult), $deliveryOptions);
        $this->assertArraySubset($expectedResult, $deliveryOptions);

        // For belcompany (Should be the same as retail. Last updated at 03-11-2016)
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_BELCOMPANY_CODE));
        $deliveryOptions = $helper->getDeliveryOptions();
        $this->assertTrue(is_array($deliveryOptions));
        $this->assertCount(count($expectedResult), $deliveryOptions);
        $this->assertArraySubset($expectedResult, $deliveryOptions);
    }

    /**
     * Checks if getDeliveryOptions returns the expected response when the website is retail.
     */
    public function testGetDeliveryOptionsRetailNoSplitDelivery()
    {
        $helper = $this->getHelperAsMock(false);
        $expectedResult = ['deliver', 'pickup', 'direct'];

        // For retail
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE));
        $deliveryOptions = $helper->getDeliveryOptions();
        $this->assertTrue(is_array($deliveryOptions));
        $this->assertCount(count($expectedResult), $deliveryOptions);
        $this->assertArraySubset($expectedResult, $deliveryOptions);

        // For belcompany (Should be the same as retail. Last updated at 03-11-2016)
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_BELCOMPANY_CODE));
        $deliveryOptions = $helper->getDeliveryOptions();
        $this->assertTrue(is_array($deliveryOptions));
        $this->assertCount(count($expectedResult), $deliveryOptions);
        $this->assertArraySubset($expectedResult, $deliveryOptions);
    }

    /**
     * Checks if getDeliveryOptions returns the expected response when the website is telesales.
     */
    public function testGetDeliveryOptionsTelesales()
    {
        $helper = $this->getHelperAsMock();
        $expectedResult = ['deliver', 'split', 'pickup'];

        // For telesales
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_TELESALES_CODE));
        $deliveryOptions = $helper->getDeliveryOptions();
        $this->assertTrue(is_array($deliveryOptions));
        $this->assertCount(count($expectedResult), $deliveryOptions);
        $this->assertArraySubset($expectedResult, $deliveryOptions);

        // For belcompany webshop (Should be the same as telesales. Last updated at 03-11-2016)
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_BC_WEBSHOP_CODE));
        $deliveryOptions = $helper->getDeliveryOptions();
        $this->assertTrue(is_array($deliveryOptions));
        $this->assertCount(count($expectedResult), $deliveryOptions);
        $this->assertArraySubset($expectedResult, $deliveryOptions);

        // For traders (Should be the same as telesales. Last updated at 03-11-2016)
        /*$this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_TRADERS_CODE));
        $deliveryOptions = $helper->getDeliveryOptions();
        $this->assertTrue(is_array($deliveryOptions));
        $this->assertCount(count($expectedResult), $deliveryOptions);
        $this->assertArraySubset($expectedResult, $deliveryOptions);*/
    }

    /**
     * Checks if getDeliveryOptions returns the expected response when the website is webshop(online).
     */
    public function testGetDeliveryOptionsWebshop()
    {
        $helper = $this->getHelperAsMock();
        $expectedResult = ['deliver', 'split'];

        // For webshop (also called online)
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_WEB_SHOP_CODE));
        $deliveryOptions = $helper->getDeliveryOptions();
        $this->assertTrue(is_array($deliveryOptions));
        $this->assertCount(count($expectedResult), $deliveryOptions);
        $this->assertArraySubset($expectedResult, $deliveryOptions);
    }

    /**
     * Checks if getDeliveryOptions returns the expected response when the website is not retail/telesales/webshop(online).
     */
    public function testGetDeliveryOptionsOther()
    {
        $helper = $this->getHelperAsMock();
        $expectedResult = ['deliver', 'split', 'pickup'];

        // The below 2 codes are not specified yet (Last updated at 03-11-2016)
        // For friends and family
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_FRIENDS_AND_FAMILY));
        $deliveryOptions = $helper->getDeliveryOptions();
        $this->assertTrue(is_array($deliveryOptions));
        $this->assertCount(count($expectedResult), $deliveryOptions);
        $this->assertArraySubset($expectedResult, $deliveryOptions);

        // For indirect
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_INDIRECT_CODE));
        $deliveryOptions = $helper->getDeliveryOptions();
        $this->assertTrue(is_array($deliveryOptions));
        $this->assertCount(count($expectedResult), $deliveryOptions);
        $this->assertArraySubset($expectedResult, $deliveryOptions);
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Split and
     * the website is Retail.
     */
    public function testGetPaymentOptionsForDeliveryMethodSplitOnRetail()
    {
        $helper = $this->mockCheckoutHelper(false, false, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, Vznl_Checkout_Model_Sales_Quote_Item::ACQUISITIE);
        $deliveryMethod = 'split';
        $expectedResult = ['split-payment' => true];

        // For retail
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE));
        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));

        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));

        // For belcompany (Should be the same as retail. Last updated at 03-11-2016)
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_BELCOMPANY_CODE));
        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
    }

    /**
     * =================================================================================================================
     * ========== Delivery method: direct ==============================================================================
     * =================================================================================================================
     */

    /**
     * -----------------------------------------------------------------------------------------------------------------
     * ---------- Channel: Retail --------------------------------------------------------------------------------------
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Direct,
     * the website is Retail, a new customer and package has type Acquisition.
     */
    public function testGetPaymentOptionsForDeliveryMethodDirectOnRetailAsNewCustomerWithAcquisition()
    {
        $deliveryMethod = 'direct';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE));
        $helper = $this->mockCheckoutHelper(false, false, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, Vznl_Checkout_Model_Sales_Quote_Item::ACQUISITIE);

        $expectedResult = ['payinstore' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Direct,
     * the website is Retail, a new customer and package has type Retention.
     */
    public function testGetPaymentOptionsForDeliveryMethodDirectOnRetailAsNewCustomerWithRetention()
    {
        $deliveryMethod = 'direct';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE));
        $helper = $this->mockCheckoutHelper(false, false, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, Vznl_Checkout_Model_Sales_Quote_Item::RETENTIE);

        $expectedResult = [];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Direct,
     * the website is Retail, a new customer and package has type Prepaid.
     */
    public function testGetPaymentOptionsForDeliveryMethodDirectOnRetailAsNewCustomerWithPrepaid()
    {
        $deliveryMethod = 'direct';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE));
        $helper = $this->mockCheckoutHelper(false, false, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_PREPAID, Vznl_Checkout_Model_Sales_Quote_Item::HARDWARE);

        $expectedResult = ['payinstore' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Direct,
     * the website is Retail, a new customer and package has type Retention.
     */
    public function testGetPaymentOptionsForDeliveryMethodDirectOnRetailAsNewCustomerWithHardware()
    {
        $deliveryMethod = 'direct';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE));
        $helper = $this->mockCheckoutHelper(false, false, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_HARDWARE, Vznl_Checkout_Model_Sales_Quote_Item::HARDWARE);

        $expectedResult = ['payinstore' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Direct,
     * the website is Retail, an existing customer and package has type Acquisition.
     */
    public function testGetPaymentOptionsForDeliveryMethodDirectOnRetailAsExistingCustomerWithAcquisition()
    {
        $deliveryMethod = 'direct';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE));
        $helper = $this->mockCheckoutHelper(true, false, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, Vznl_Checkout_Model_Sales_Quote_Item::ACQUISITIE);

        $expectedResult = ['payinstore' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Direct,
     * the website is Retail, an existing customer and package has type Retention.
     */
    public function testGetPaymentOptionsForDeliveryMethodDirectOnRetailAsExistingCustomerWithRetention()
    {
        $deliveryMethod = 'direct';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE));
        $helper = $this->mockCheckoutHelper(true, false, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, Vznl_Checkout_Model_Sales_Quote_Item::RETENTIE);

        $expectedResult = ['payinstore' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Direct,
     * the website is Retail, an existing customer and package has type Prepaid.
     */
    public function testGetPaymentOptionsForDeliveryMethodDirectOnRetailAsExistingCustomerWithPrepaid()
    {
        $deliveryMethod = 'direct';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE));
        $helper = $this->mockCheckoutHelper(true, false, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_PREPAID, Vznl_Checkout_Model_Sales_Quote_Item::HARDWARE);

        $expectedResult = ['payinstore' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Direct,
     * the website is Retail, an existing customer and package has type Hardware.
     */
    public function testGetPaymentOptionsForDeliveryMethodDirectOnRetailAsExistingCustomerWithHardware()
    {
        $deliveryMethod = 'direct';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE));
        $helper = $this->mockCheckoutHelper(true, false, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_HARDWARE, Vznl_Checkout_Model_Sales_Quote_Item::HARDWARE);

        $expectedResult = ['payinstore' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Direct,
     * the website is Retail, a new business customer and package has type Acquisition.
     */
    public function testGetPaymentOptionsForDeliveryMethodDirectOnRetailAsNewBusinessCustomerWithAcquisition()
    {
        $deliveryMethod = 'direct';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE));
        $helper = $this->mockCheckoutHelper(false, true, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, Vznl_Checkout_Model_Sales_Quote_Item::ACQUISITIE);

        $expectedResult = ['payinstore' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Direct,
     * the website is Retail, a new business customer and package has type Retention.
     */
    public function testGetPaymentOptionsForDeliveryMethodDirectOnRetailAsNewBusinessCustomerWithRetention()
    {
        $deliveryMethod = 'direct';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE));
        $helper = $this->mockCheckoutHelper(false, true, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, Vznl_Checkout_Model_Sales_Quote_Item::RETENTIE);

        $expectedResult = [];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Direct,
     * the website is Retail, a new business customer and package has type Prepaid.
     */
    public function testGetPaymentOptionsForDeliveryMethodDirectOnRetailAsNewBusinessCustomerWithPrepaid()
    {
        $deliveryMethod = 'direct';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE));
        $helper = $this->mockCheckoutHelper(false, true, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_PREPAID, Vznl_Checkout_Model_Sales_Quote_Item::HARDWARE);

        $expectedResult = ['payinstore' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Direct,
     * the website is Retail, a new business customer and package has type Hardware.
     */
    public function testGetPaymentOptionsForDeliveryMethodDirectOnRetailAsNewBusinessCustomerWithHardware()
    {
        $deliveryMethod = 'direct';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE));
        $helper = $this->mockCheckoutHelper(false, true, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_HARDWARE, Vznl_Checkout_Model_Sales_Quote_Item::HARDWARE);

        $expectedResult = ['payinstore' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Direct,
     * the website is Retail, an existing business customer and package has type Acquisition.
     */
    public function testGetPaymentOptionsForDeliveryMethodDirectOnRetailAsExistingBusinessCustomerWithAcquisition()
    {
        $deliveryMethod = 'direct';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE));
        $helper = $this->mockCheckoutHelper(true, true, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, Vznl_Checkout_Model_Sales_Quote_Item::ACQUISITIE);

        $expectedResult = ['payinstore' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Direct,
     * the website is Retail, an existing business customer and package has type Retention.
     */
    public function testGetPaymentOptionsForDeliveryMethodDirectOnRetailAsExistingBusinessCustomerWithRetention()
    {
        $deliveryMethod = 'direct';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE));
        $helper = $this->mockCheckoutHelper(true, true, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, Vznl_Checkout_Model_Sales_Quote_Item::RETENTIE);

        $expectedResult = ['payinstore' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Direct,
     * the website is Retail, an existing business customer and package has type Prepaid.
     */
    public function testGetPaymentOptionsForDeliveryMethodDirectOnRetailAsExistingBusinessCustomerWithPrepaid()
    {
        $deliveryMethod = 'direct';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE));
        $helper = $this->mockCheckoutHelper(true, true, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_PREPAID, Vznl_Checkout_Model_Sales_Quote_Item::HARDWARE);

        $expectedResult = ['payinstore' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Direct,
     * the website is Retail, an existing business customer and package has type Hardware.
     */
    public function testGetPaymentOptionsForDeliveryMethodDirectOnRetailAsExistingBusinessCustomerWithHardware()
    {
        $deliveryMethod = 'direct';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE));
        $helper = $this->mockCheckoutHelper(true, true, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_HARDWARE, Vznl_Checkout_Model_Sales_Quote_Item::HARDWARE);

        $expectedResult = ['payinstore' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * =================================================================================================================
     * ========== Delivery method: pickup ==============================================================================
     * =================================================================================================================
     */

    /**
     * -----------------------------------------------------------------------------------------------------------------
     * ---------- Channel: Retail --------------------------------------------------------------------------------------
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Pick up,
     * the website is Retail, a new customer and package has type Acquisition.
     */
    public function testGetPaymentOptionsForDeliveryMethodPickUpOnRetailAsNewCustomerWithAcquisition()
    {
        $deliveryMethod = 'pickup';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE));
        $helper = $this->mockCheckoutHelper(false, false, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, Vznl_Checkout_Model_Sales_Quote_Item::ACQUISITIE);

        $expectedResult = ['payinstore' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Pick up,
     * the website is Retail, a new customer and package has type Retention.
     */
    public function testGetPaymentOptionsForDeliveryMethodPickUpOnRetailAsNewCustomerWithRetention()
    {
        $deliveryMethod = 'pickup';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE));
        $helper = $this->mockCheckoutHelper(false, false, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, Vznl_Checkout_Model_Sales_Quote_Item::RETENTIE);

        $expectedResult = [];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Pick up,
     * the website is Retail, a new customer and package has type Prepaid.
     */
    public function testGetPaymentOptionsForDeliveryMethodPickUpOnRetailAsNewCustomerWithPrepaid()
    {
        $deliveryMethod = 'pickup';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE));
        $helper = $this->mockCheckoutHelper(false, false, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_PREPAID, Vznl_Checkout_Model_Sales_Quote_Item::HARDWARE);

        $expectedResult = ['payinstore' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Pick up,
     * the website is Retail, a new customer and package has type Retention.
     */
    public function testGetPaymentOptionsForDeliveryMethodPickUpOnRetailAsNewCustomerWithHardware()
    {
        $deliveryMethod = 'pickup';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE));
        $helper = $this->mockCheckoutHelper(false, false, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_HARDWARE, Vznl_Checkout_Model_Sales_Quote_Item::HARDWARE);

        $expectedResult = ['payinstore' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Pick up,
     * the website is Retail, an existing customer and package has type Acquisition.
     */
    public function testGetPaymentOptionsForDeliveryMethodPickUpOnRetailAsExistingCustomerWithAcquisition()
    {
        $deliveryMethod = 'pickup';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE));
        $helper = $this->mockCheckoutHelper(true, false, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, Vznl_Checkout_Model_Sales_Quote_Item::ACQUISITIE);

        $expectedResult = ['payinstore' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Pick up,
     * the website is Retail, an existing customer and package has type Retention.
     */
    public function testGetPaymentOptionsForDeliveryMethodPickUpOnRetailAsExistingCustomerWithRetention()
    {
        $deliveryMethod = 'pickup';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE));
        $helper = $this->mockCheckoutHelper(true, false, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, Vznl_Checkout_Model_Sales_Quote_Item::RETENTIE);

        $expectedResult = ['payinstore' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Pick up,
     * the website is Retail, an existing customer and package has type Prepaid.
     */
    public function testGetPaymentOptionsForDeliveryMethodPickUpOnRetailAsExistingCustomerWithPrepaid()
    {
        $deliveryMethod = 'pickup';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE));
        $helper = $this->mockCheckoutHelper(true, false, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_PREPAID, Vznl_Checkout_Model_Sales_Quote_Item::HARDWARE);

        $expectedResult = ['payinstore' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Pick up,
     * the website is Retail, an existing customer and package has type Hardware.
     */
    public function testGetPaymentOptionsForDeliveryMethodPickUpOnRetailAsExistingCustomerWithHardware()
    {
        $deliveryMethod = 'pickup';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE));
        $helper = $this->mockCheckoutHelper(true, false, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_HARDWARE, Vznl_Checkout_Model_Sales_Quote_Item::HARDWARE);

        $expectedResult = ['payinstore' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Pick up,
     * the website is Retail, a new business customer and package has type Acquisition.
     */
    public function testGetPaymentOptionsForDeliveryMethodPickUpOnRetailAsNewBusinessCustomerWithAcquisition()
    {
        $deliveryMethod = 'pickup';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE));
        $helper = $this->mockCheckoutHelper(false, true, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, Vznl_Checkout_Model_Sales_Quote_Item::ACQUISITIE);

        $expectedResult = ['payinstore' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Pick up,
     * the website is Retail, a new business customer and package has type Retention.
     */
    public function testGetPaymentOptionsForDeliveryMethodPickUpOnRetailAsNewBusinessCustomerWithRetention()
    {
        $deliveryMethod = 'pickup';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE));
        $helper = $this->mockCheckoutHelper(false, true, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, Vznl_Checkout_Model_Sales_Quote_Item::RETENTIE);

        $expectedResult = [];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Pick up,
     * the website is Retail, a new business customer and package has type Prepaid.
     */
    public function testGetPaymentOptionsForDeliveryMethodPickUpOnRetailAsNewBusinessCustomerWithPrepaid()
    {
        $deliveryMethod = 'pickup';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE));
        $helper = $this->mockCheckoutHelper(false, true, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_PREPAID, Vznl_Checkout_Model_Sales_Quote_Item::HARDWARE);

        $expectedResult = ['payinstore' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Pick up,
     * the website is Retail, a new business customer and package has type Hardware.
     */
    public function testGetPaymentOptionsForDeliveryMethodPickUpOnRetailAsNewBusinessCustomerWithHardware()
    {
        $deliveryMethod = 'pickup';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE));
        $helper = $this->mockCheckoutHelper(false, true, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_HARDWARE, Vznl_Checkout_Model_Sales_Quote_Item::HARDWARE);

        $expectedResult = ['payinstore' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Pick up,
     * the website is Retail, an existing business customer and package has type Acquisition.
     */
    public function testGetPaymentOptionsForDeliveryMethodPickUpOnRetailAsExistingBusinessCustomerWithAcquisition()
    {
        $deliveryMethod = 'pickup';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE));
        $helper = $this->mockCheckoutHelper(true, true, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, Vznl_Checkout_Model_Sales_Quote_Item::ACQUISITIE);

        $expectedResult = ['payinstore' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Pick up,
     * the website is Retail, an existing business customer and package has type Retention.
     */
    public function testGetPaymentOptionsForDeliveryMethodPickUpOnRetailAsExistingBusinessCustomerWithRetention()
    {
        $deliveryMethod = 'pickup';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE));
        $helper = $this->mockCheckoutHelper(true, true, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, Vznl_Checkout_Model_Sales_Quote_Item::RETENTIE);

        $expectedResult = ['payinstore' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Pick up,
     * the website is Retail, an existing business customer and package has type Prepaid.
     */
    public function testGetPaymentOptionsForDeliveryMethodPickUpOnRetailAsExistingBusinessCustomerWithPrepaid()
    {
        $deliveryMethod = 'pickup';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE));
        $helper = $this->mockCheckoutHelper(true, true, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_PREPAID, Vznl_Checkout_Model_Sales_Quote_Item::HARDWARE);

        $expectedResult = ['payinstore' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Pick up,
     * the website is Retail, an existing business customer and package has type Hardware.
     */
    public function testGetPaymentOptionsForDeliveryMethodPickUpOnRetailAsExistingBusinessCustomerWithHardware()
    {
        $deliveryMethod = 'pickup';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE));
        $helper = $this->mockCheckoutHelper(true, true, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_HARDWARE, Vznl_Checkout_Model_Sales_Quote_Item::HARDWARE);

        $expectedResult = ['payinstore' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * -----------------------------------------------------------------------------------------------------------------
     * ---------- Channel: Telesales --------------------------------------------------------------------------------------
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Pick up,
     * the website is Telesales, a new customer and package has type Acquisition.
     */
    public function testGetPaymentOptionsForDeliveryMethodPickUpOnTelesalesAsNewCustomerWithAcquisition()
    {
        $deliveryMethod = 'pickup';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_TELESALES_CODE));
        $helper = $this->mockCheckoutHelper(false, false, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, Vznl_Checkout_Model_Sales_Quote_Item::ACQUISITIE);

        $expectedResult = ['payinstore' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Pick up,
     * the website is Telesales, a new customer and package has type Retention.
     */
    public function testGetPaymentOptionsForDeliveryMethodPickUpOnTelesalesAsNewCustomerWithRetention()
    {
        $deliveryMethod = 'pickup';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_TELESALES_CODE));
        $helper = $this->mockCheckoutHelper(false, false, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, Vznl_Checkout_Model_Sales_Quote_Item::RETENTIE);

        $expectedResult = [];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Pick up,
     * the website is Telesales, a new customer and package has type Prepaid.
     */
    public function testGetPaymentOptionsForDeliveryMethodPickUpOnTelesalesAsNewCustomerWithPrepaid()
    {
        $deliveryMethod = 'pickup';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_TELESALES_CODE));
        $helper = $this->mockCheckoutHelper(false, false, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_PREPAID, Vznl_Checkout_Model_Sales_Quote_Item::HARDWARE);

        $expectedResult = ['payinstore' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Pick up,
     * the website is Telesales, a new customer and package has type Retention.
     */
    public function testGetPaymentOptionsForDeliveryMethodPickUpOnTelesalesAsNewCustomerWithHardware()
    {
        $deliveryMethod = 'pickup';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_TELESALES_CODE));
        $helper = $this->mockCheckoutHelper(false, false, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_HARDWARE, Vznl_Checkout_Model_Sales_Quote_Item::HARDWARE);

        $expectedResult = ['payinstore' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Pick up,
     * the website is Telesales, an existing customer and package has type Acquisition.
     */
    public function testGetPaymentOptionsForDeliveryMethodPickUpOnTelesalesAsExistingCustomerWithAcquisition()
    {
        $deliveryMethod = 'pickup';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_TELESALES_CODE));
        $helper = $this->mockCheckoutHelper(true, false, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, Vznl_Checkout_Model_Sales_Quote_Item::ACQUISITIE);

        $expectedResult = ['payinstore' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Pick up,
     * the website is Telesales, an existing customer and package has type Retention.
     */
    public function testGetPaymentOptionsForDeliveryMethodPickUpOnTelesalesAsExistingCustomerWithRetention()
    {
        $deliveryMethod = 'pickup';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_TELESALES_CODE));
        $helper = $this->mockCheckoutHelper(true, false, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, Vznl_Checkout_Model_Sales_Quote_Item::RETENTIE);

        $expectedResult = ['payinstore' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Pick up,
     * the website is Telesales, an existing customer and package has type Prepaid.
     */
    public function testGetPaymentOptionsForDeliveryMethodPickUpOnTelesalesAsExistingCustomerWithPrepaid()
    {
        $deliveryMethod = 'pickup';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_TELESALES_CODE));
        $helper = $this->mockCheckoutHelper(true, false, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_PREPAID, Vznl_Checkout_Model_Sales_Quote_Item::HARDWARE);

        $expectedResult = ['payinstore' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Pick up,
     * the website is Telesales, an existing customer and package has type Hardware.
     */
    public function testGetPaymentOptionsForDeliveryMethodPickUpOnTelesalesAsExistingCustomerWithHardware()
    {
        $deliveryMethod = 'pickup';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_TELESALES_CODE));
        $helper = $this->mockCheckoutHelper(true, false, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_HARDWARE, Vznl_Checkout_Model_Sales_Quote_Item::HARDWARE);

        $expectedResult = ['payinstore' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Pick up,
     * the website is Telesales, a new business customer and package has type Acquisition.
     */
    public function testGetPaymentOptionsForDeliveryMethodPickUpOnTelesalesAsNewBusinessCustomerWithAcquisition()
    {
        $deliveryMethod = 'pickup';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_TELESALES_CODE));
        $helper = $this->mockCheckoutHelper(false, true, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, Vznl_Checkout_Model_Sales_Quote_Item::ACQUISITIE);

        $expectedResult = ['payinstore' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Pick up,
     * the website is Telesales, a new business customer and package has type Retention.
     */
    public function testGetPaymentOptionsForDeliveryMethodPickUpOnTelesalesAsNewBusinessCustomerWithRetention()
    {
        $deliveryMethod = 'pickup';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_TELESALES_CODE));
        $helper = $this->mockCheckoutHelper(false, true, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, Vznl_Checkout_Model_Sales_Quote_Item::RETENTIE);

        $expectedResult = [];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Pick up,
     * the website is Telesales, a new business customer and package has type Prepaid.
     */
    public function testGetPaymentOptionsForDeliveryMethodPickUpOnTelesalesAsNewBusinessCustomerWithPrepaid()
    {
        $deliveryMethod = 'pickup';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_TELESALES_CODE));
        $helper = $this->mockCheckoutHelper(Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_PREPAID, Vznl_Checkout_Model_Sales_Quote_Item::HARDWARE);

        $expectedResult = ['payinstore' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Pick up,
     * the website is Telesales, a new business customer and package has type Hardware.
     */
    public function testGetPaymentOptionsForDeliveryMethodPickUpOnTelesalesAsNewBusinessCustomerWithHardware()
    {
        $deliveryMethod = 'pickup';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_TELESALES_CODE));
        $helper = $this->mockCheckoutHelper(false, true, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_HARDWARE, Vznl_Checkout_Model_Sales_Quote_Item::HARDWARE);

        $expectedResult = ['payinstore' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Pick up,
     * the website is Telesales, an existing business customer and package has type Acquisition.
     */
    public function testGetPaymentOptionsForDeliveryMethodPickUpOnTelesalesAsExistingBusinessCustomerWithAcquisition()
    {
        $deliveryMethod = 'pickup';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_TELESALES_CODE));
        $helper = $this->mockCheckoutHelper(true, true, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, Vznl_Checkout_Model_Sales_Quote_Item::ACQUISITIE);

        $expectedResult = ['payinstore' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Pick up,
     * the website is Telesales, an existing business customer and package has type Retention.
     */
    public function testGetPaymentOptionsForDeliveryMethodPickUpOnTelesalesAsExistingBusinessCustomerWithRetention()
    {
        $deliveryMethod = 'pickup';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_TELESALES_CODE));
        $helper = $this->mockCheckoutHelper(true, true, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, Vznl_Checkout_Model_Sales_Quote_Item::RETENTIE);

        $expectedResult = ['payinstore' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Pick up,
     * the website is Telesales, an existing business customer and package has type Prepaid.
     */
    public function testGetPaymentOptionsForDeliveryMethodPickUpOnTelesalesAsExistingBusinessCustomerWithPrepaid()
    {
        $deliveryMethod = 'pickup';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_TELESALES_CODE));
        $helper = $this->mockCheckoutHelper(true, true, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_PREPAID, Vznl_Checkout_Model_Sales_Quote_Item::HARDWARE);

        $expectedResult = ['payinstore' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Pick up,
     * the website is Telesales, an existing business customer and package has type Hardware.
     */
    public function testGetPaymentOptionsForDeliveryMethodPickUpOnTelesalesAsExistingBusinessCustomerWithHardware()
    {
        $deliveryMethod = 'pickup';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_TELESALES_CODE));
        $helper = $this->mockCheckoutHelper(true, true, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_HARDWARE, Vznl_Checkout_Model_Sales_Quote_Item::HARDWARE);

        $expectedResult = ['payinstore' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * -----------------------------------------------------------------------------------------------------------------
     * ---------- Channel: Webshop --------------------------------------------------------------------------------------
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Pick up,
     * the website is Webshop, a new customer and package has type Acquisition.
     */
    public function testGetPaymentOptionsForDeliveryMethodPickUpOnWebshopAsNewCustomerWithAcquisition()
    {
        $deliveryMethod = 'pickup';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_WEB_SHOP_CODE));
        $helper = $this->mockCheckoutHelper(false, false, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, Vznl_Checkout_Model_Sales_Quote_Item::ACQUISITIE);

        $expectedResult = [];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Pick up,
     * the website is Webshop, a new customer and package has type Retention.
     */
    public function testGetPaymentOptionsForDeliveryMethodPickUpOnWebshopAsNewCustomerWithRetention()
    {
        $deliveryMethod = 'pickup';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_WEB_SHOP_CODE));
        $helper = $this->mockCheckoutHelper(false, false, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, Vznl_Checkout_Model_Sales_Quote_Item::RETENTIE);

        $expectedResult = [];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Pick up,
     * the website is Webshop, a new customer and package has type Prepaid.
     */
    public function testGetPaymentOptionsForDeliveryMethodPickUpOnWebshopAsNewCustomerWithPrepaid()
    {
        $deliveryMethod = 'pickup';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_WEB_SHOP_CODE));
        $helper = $this->mockCheckoutHelper(false, false, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_PREPAID, Vznl_Checkout_Model_Sales_Quote_Item::HARDWARE);

        $expectedResult = [];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Pick up,
     * the website is Webshop, a new customer and package has type Retention.
     */
    public function testGetPaymentOptionsForDeliveryMethodPickUpOnWebshopAsNewCustomerWithHardware()
    {
        $deliveryMethod = 'pickup';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_WEB_SHOP_CODE));
        $helper = $this->mockCheckoutHelper(false, false, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_HARDWARE, Vznl_Checkout_Model_Sales_Quote_Item::HARDWARE);

        $expectedResult = [];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Pick up,
     * the website is Webshop, an existing customer and package has type Acquisition.
     */
    public function testGetPaymentOptionsForDeliveryMethodPickUpOnWebshopAsExistingCustomerWithAcquisition()
    {
        $deliveryMethod = 'pickup';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_WEB_SHOP_CODE));
        $helper = $this->mockCheckoutHelper(true, false, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, Vznl_Checkout_Model_Sales_Quote_Item::ACQUISITIE);

        $expectedResult = [];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Pick up,
     * the website is Webshop, an existing customer and package has type Retention.
     */
    public function testGetPaymentOptionsForDeliveryMethodPickUpOnWebshopAsExistingCustomerWithRetention()
    {
        $deliveryMethod = 'pickup';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_WEB_SHOP_CODE));
        $helper = $this->mockCheckoutHelper(true, false, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, Vznl_Checkout_Model_Sales_Quote_Item::RETENTIE);

        $expectedResult = [];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Pick up,
     * the website is Webshop, an existing customer and package has type Prepaid.
     */
    public function testGetPaymentOptionsForDeliveryMethodPickUpOnWebshopAsExistingCustomerWithPrepaid()
    {
        $deliveryMethod = 'pickup';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_WEB_SHOP_CODE));
        $helper = $this->mockCheckoutHelper(true, false, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_PREPAID, Vznl_Checkout_Model_Sales_Quote_Item::HARDWARE);

        $expectedResult = [];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Pick up,
     * the website is Webshop, an existing customer and package has type Hardware.
     */
    public function testGetPaymentOptionsForDeliveryMethodPickUpOnWebshopAsExistingCustomerWithHardware()
    {
        $deliveryMethod = 'pickup';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_WEB_SHOP_CODE));
        $helper = $this->mockCheckoutHelper(true, false, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_HARDWARE, Vznl_Checkout_Model_Sales_Quote_Item::HARDWARE);

        $expectedResult = [];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Pick up,
     * the website is Webshop, a new business customer and package has type Acquisition.
     */
    public function testGetPaymentOptionsForDeliveryMethodPickUpOnWebshopAsNewBusinessCustomerWithAcquisition()
    {
        $deliveryMethod = 'pickup';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_WEB_SHOP_CODE));
        $helper = $this->mockCheckoutHelper(false, true, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, Vznl_Checkout_Model_Sales_Quote_Item::ACQUISITIE);

        $expectedResult = [];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Pick up,
     * the website is Webshop, a new business customer and package has type Retention.
     */
    public function testGetPaymentOptionsForDeliveryMethodPickUpOnWebshopAsNewBusinessCustomerWithRetention()
    {
        $deliveryMethod = 'pickup';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_WEB_SHOP_CODE));
        $helper = $this->mockCheckoutHelper(false, true, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, Vznl_Checkout_Model_Sales_Quote_Item::RETENTIE);

        $expectedResult = [];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Pick up,
     * the website is Webshop, a new business customer and package has type Prepaid.
     */
    public function testGetPaymentOptionsForDeliveryMethodPickUpOnWebshopAsNewBusinessCustomerWithPrepaid()
    {
        $deliveryMethod = 'pickup';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_WEB_SHOP_CODE));
        $helper = $this->mockCheckoutHelper(false, true, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_PREPAID, Vznl_Checkout_Model_Sales_Quote_Item::HARDWARE);

        $expectedResult = [];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Pick up,
     * the website is Webshop, a new business customer and package has type Hardware.
     */
    public function testGetPaymentOptionsForDeliveryMethodPickUpOnWebshopAsNewBusinessCustomerWithHardware()
    {
        $deliveryMethod = 'pickup';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_WEB_SHOP_CODE));
        $helper = $this->mockCheckoutHelper(false, true, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_HARDWARE, Vznl_Checkout_Model_Sales_Quote_Item::HARDWARE);

        $expectedResult = [];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Pick up,
     * the website is Webshop, an existing business customer and package has type Acquisition.
     */
    public function testGetPaymentOptionsForDeliveryMethodPickUpOnWebshopAsExistingBusinessCustomerWithAcquisition()
    {
        $deliveryMethod = 'pickup';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_WEB_SHOP_CODE));
        $helper = $this->mockCheckoutHelper(true, true, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, Vznl_Checkout_Model_Sales_Quote_Item::ACQUISITIE);

        $expectedResult = [];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Pick up,
     * the website is Webshop, an existing business customer and package has type Retention.
     */
    public function testGetPaymentOptionsForDeliveryMethodPickUpOnWebshopAsExistingBusinessCustomerWithRetention()
    {
        $deliveryMethod = 'pickup';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_WEB_SHOP_CODE));
        $helper = $this->mockCheckoutHelper(true, true, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, Vznl_Checkout_Model_Sales_Quote_Item::RETENTIE);

        $expectedResult = [];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Pick up,
     * the website is Webshop, an existing business customer and package has type Prepaid.
     */
    public function testGetPaymentOptionsForDeliveryMethodPickUpOnWebshopAsExistingBusinessCustomerWithPrepaid()
    {
        $deliveryMethod = 'pickup';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_WEB_SHOP_CODE));
        $helper = $this->mockCheckoutHelper(true, true, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_PREPAID, Vznl_Checkout_Model_Sales_Quote_Item::HARDWARE);

        $expectedResult = [];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Pick up,
     * the website is Webshop, an existing business customer and package has type Hardware.
     */
    public function testGetPaymentOptionsForDeliveryMethodPickUpOnWebshopAsExistingBusinessCustomerWithHardware()
    {
        $deliveryMethod = 'pickup';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_WEB_SHOP_CODE));
        $helper = $this->mockCheckoutHelper(true, true, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_HARDWARE, Vznl_Checkout_Model_Sales_Quote_Item::HARDWARE);

        $expectedResult = [];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * =================================================================================================================
     * ========== Delivery method: Deliver =============================================================================
     * =================================================================================================================
     */

    /**
     * -----------------------------------------------------------------------------------------------------------------
     * ---------- Channel: Retail --------------------------------------------------------------------------------------
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Deliver,
     * the website is Retail, a new customer and package has type Acquisition.
     */
    public function testGetPaymentOptionsForDeliveryMethodDeliverOnRetailAsNewCustomerWithAcquisition()
    {
        $deliveryMethod = 'deliver';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE));
        $helper = $this->mockCheckoutHelper(false, false, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, Vznl_Checkout_Model_Sales_Quote_Item::ACQUISITIE);

        $expectedResult = ['cashondelivery' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Deliver,
     * the website is Retail, a new customer and package has type Retention.
     */
    public function testGetPaymentOptionsForDeliveryMethodDeliverOnRetailAsNewCustomerWithRetention()
    {
        $deliveryMethod = 'deliver';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE));
        $helper = $this->mockCheckoutHelper(false, false, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, Vznl_Checkout_Model_Sales_Quote_Item::RETENTIE);

        $expectedResult = [];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Deliver,
     * the website is Retail, a new customer and package has type Prepaid.
     */
    public function testGetPaymentOptionsForDeliveryMethodDeliverOnRetailAsNewCustomerWithPrepaid()
    {
        $deliveryMethod = 'deliver';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE));
        $helper = $this->mockCheckoutHelper(false, false, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_PREPAID, Vznl_Checkout_Model_Sales_Quote_Item::HARDWARE);

        $expectedResult = ['cashondelivery' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Deliver,
     * the website is Retail, a new customer and package has type Retention.
     */
    public function testGetPaymentOptionsForDeliveryMethodDeliverOnRetailAsNewCustomerWithHardware()
    {
        $deliveryMethod = 'deliver';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE));
        $helper = $this->mockCheckoutHelper(false, false, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_HARDWARE, Vznl_Checkout_Model_Sales_Quote_Item::HARDWARE);

        $expectedResult = ['cashondelivery' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Deliver,
     * the website is Retail, an existing customer and package has type Acquisition.
     */
    public function testGetPaymentOptionsForDeliveryMethodDeliverOnRetailAsExistingCustomerWithAcquisition()
    {
        $deliveryMethod = 'deliver';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE));
        $helper = $this->mockCheckoutHelper(true, false, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, Vznl_Checkout_Model_Sales_Quote_Item::ACQUISITIE);

        $expectedResult = ['cashondelivery' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Deliver,
     * the website is Retail, an existing customer and package has type Retention.
     */
    public function testGetPaymentOptionsForDeliveryMethodDeliverOnRetailAsExistingCustomerWithRetention()
    {
        $deliveryMethod = 'deliver';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE));
        $helper = $this->mockCheckoutHelper(true, false, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, Vznl_Checkout_Model_Sales_Quote_Item::RETENTIE);

        $expectedResult = ['cashondelivery' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Deliver,
     * the website is Retail, an existing customer and package has type Prepaid.
     */
    public function testGetPaymentOptionsForDeliveryMethodDeliverOnRetailAsExistingCustomerWithPrepaid()
    {
        $deliveryMethod = 'deliver';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE));
        $helper = $this->mockCheckoutHelper(true, false, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_PREPAID, Vznl_Checkout_Model_Sales_Quote_Item::HARDWARE);

        $expectedResult = ['cashondelivery' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Deliver,
     * the website is Retail, an existing customer and package has type Hardware.
     */
    public function testGetPaymentOptionsForDeliveryMethodDeliverOnRetailAsExistingCustomerWithHardware()
    {
        $deliveryMethod = 'deliver';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE));
        $helper = $this->mockCheckoutHelper(true, false, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_HARDWARE, Vznl_Checkout_Model_Sales_Quote_Item::HARDWARE);

        $expectedResult = ['cashondelivery' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Deliver,
     * the website is Retail, a new business customer and package has type Acquisition.
     */
    public function testGetPaymentOptionsForDeliveryMethodDeliverOnRetailAsNewBusinessCustomerWithAcquisition()
    {
        $deliveryMethod = 'deliver';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE));
        $helper = $this->mockCheckoutHelper(false, true, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, Vznl_Checkout_Model_Sales_Quote_Item::ACQUISITIE);

        $expectedResult = ['cashondelivery' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Deliver,
     * the website is Retail, a new business customer and package has type Retention.
     */
    public function testGetPaymentOptionsForDeliveryMethodDeliverOnRetailAsNewBusinessCustomerWithRetention()
    {
        $deliveryMethod = 'deliver';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE));
        $helper = $this->mockCheckoutHelper(false, true, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, Vznl_Checkout_Model_Sales_Quote_Item::RETENTIE);

        $expectedResult = [];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Deliver,
     * the website is Retail, a new business customer and package has type Prepaid.
     */
    public function testGetPaymentOptionsForDeliveryMethodDeliverOnRetailAsNewBusinessCustomerWithPrepaid()
    {
        $deliveryMethod = 'deliver';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE));
        $helper = $this->mockCheckoutHelper(false, true, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_PREPAID, Vznl_Checkout_Model_Sales_Quote_Item::HARDWARE);

        $expectedResult = ['cashondelivery' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Deliver,
     * the website is Retail, a new business customer and package has type Hardware.
     */
    public function testGetPaymentOptionsForDeliveryMethodDeliverOnRetailAsNewBusinessCustomerWithHardware()
    {
        $deliveryMethod = 'deliver';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE));
        $helper = $this->mockCheckoutHelper(false, true, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_HARDWARE, Vznl_Checkout_Model_Sales_Quote_Item::HARDWARE);

        $expectedResult = ['cashondelivery' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Deliver,
     * the website is Retail, an existing business customer and package has type Acquisition.
     */
    public function testGetPaymentOptionsForDeliveryMethodDeliverOnRetailAsExistingBusinessCustomerWithAcquisition()
    {
        $deliveryMethod = 'deliver';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE));
        $helper = $this->mockCheckoutHelper(true, true, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, Vznl_Checkout_Model_Sales_Quote_Item::ACQUISITIE);

        $expectedResult = ['cashondelivery' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Deliver,
     * the website is Retail, an existing business customer and package has type Retention.
     */
    public function testGetPaymentOptionsForDeliveryMethodDeliverOnRetailAsExistingBusinessCustomerWithRetention()
    {
        $deliveryMethod = 'deliver';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE));
        $helper = $this->mockCheckoutHelper(true, true, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, Vznl_Checkout_Model_Sales_Quote_Item::RETENTIE);

        $expectedResult = ['cashondelivery' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Deliver,
     * the website is Retail, an existing business customer and package has type Prepaid.
     */
    public function testGetPaymentOptionsForDeliveryMethodDeliverOnRetailAsExistingBusinessCustomerWithPrepaid()
    {
        $deliveryMethod = 'deliver';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE));
        $helper = $this->mockCheckoutHelper(true, true, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_PREPAID, Vznl_Checkout_Model_Sales_Quote_Item::HARDWARE);

        $expectedResult = ['cashondelivery' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Deliver,
     * the website is Retail, an existing business customer and package has type Hardware.
     */
    public function testGetPaymentOptionsForDeliveryMethodDeliverOnRetailAsExistingBusinessCustomerWithHardware()
    {
        $deliveryMethod = 'deliver';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE));
        $helper = $this->mockCheckoutHelper(true, true, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_HARDWARE, Vznl_Checkout_Model_Sales_Quote_Item::HARDWARE);

        $expectedResult = ['cashondelivery' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * -----------------------------------------------------------------------------------------------------------------
     * ---------- Channel: Telesales --------------------------------------------------------------------------------------
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Deliver,
     * the website is Telesales, a new customer and package has type Acquisition.
     */
    public function testGetPaymentOptionsForDeliveryMethodDeliverOnTelesalesAsNewCustomerWithAcquisition()
    {
        $deliveryMethod = 'deliver';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_TELESALES_CODE));
        $helper = $this->mockCheckoutHelper(false, false, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, Vznl_Checkout_Model_Sales_Quote_Item::ACQUISITIE);

        $expectedResult = ['cashondelivery' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Deliver,
     * the website is Telesales, a new customer and package has type Retention.
     */
    public function testGetPaymentOptionsForDeliveryMethodDeliverOnTelesalesAsNewCustomerWithRetention()
    {
        $deliveryMethod = 'deliver';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_TELESALES_CODE));
        $helper = $this->mockCheckoutHelper(false, false, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, Vznl_Checkout_Model_Sales_Quote_Item::RETENTIE);

        $expectedResult = [];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Deliver,
     * the website is Telesales, a new customer and package has type Prepaid.
     */
    public function testGetPaymentOptionsForDeliveryMethodDeliverOnTelesalesAsNewCustomerWithPrepaid()
    {
        $deliveryMethod = 'deliver';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_TELESALES_CODE));
        $helper = $this->mockCheckoutHelper(false, false, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_PREPAID, Vznl_Checkout_Model_Sales_Quote_Item::HARDWARE);

        $expectedResult = ['cashondelivery' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Deliver,
     * the website is Telesales, a new customer and package has type Retention.
     */
    public function testGetPaymentOptionsForDeliveryMethodDeliverOnTelesalesAsNewCustomerWithHardware()
    {
        $deliveryMethod = 'deliver';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_TELESALES_CODE));
        $helper = $this->mockCheckoutHelper(false, false, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_HARDWARE, Vznl_Checkout_Model_Sales_Quote_Item::HARDWARE);

        $expectedResult = ['cashondelivery' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Deliver,
     * the website is Telesales, an existing customer and package has type Acquisition.
     */
    public function testGetPaymentOptionsForDeliveryMethodDeliverOnTelesalesAsExistingCustomerWithAcquisition()
    {
        $deliveryMethod = 'deliver';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_TELESALES_CODE));
        $helper = $this->mockCheckoutHelper(true, false, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, Vznl_Checkout_Model_Sales_Quote_Item::ACQUISITIE);

        $expectedResult = ['cashondelivery' => true, 'checkmo' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Deliver,
     * the website is Telesales, an existing customer and package has type Retention.
     */
    public function testGetPaymentOptionsForDeliveryMethodDeliverOnTelesalesAsExistingCustomerWithRetention()
    {
        $deliveryMethod = 'deliver';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_TELESALES_CODE));
        $helper = $this->mockCheckoutHelper(true, false, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, Vznl_Checkout_Model_Sales_Quote_Item::RETENTIE);

        $expectedResult = ['cashondelivery' => true, 'checkmo' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Deliver,
     * the website is Telesales, an existing customer and package has type Prepaid.
     */
    public function testGetPaymentOptionsForDeliveryMethodDeliverOnTelesalesAsExistingCustomerWithPrepaid()
    {
        $deliveryMethod = 'deliver';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_TELESALES_CODE));
        $helper = $this->mockCheckoutHelper(true, false, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_PREPAID, Vznl_Checkout_Model_Sales_Quote_Item::HARDWARE);

        $expectedResult = ['cashondelivery' => true, 'checkmo' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Deliver,
     * the website is Telesales, an existing customer and package has type Hardware.
     */
    public function testGetPaymentOptionsForDeliveryMethodDeliverOnTelesalesAsExistingCustomerWithHardware()
    {
        $deliveryMethod = 'deliver';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_TELESALES_CODE));
        $helper = $this->mockCheckoutHelper(true, false, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_HARDWARE, Vznl_Checkout_Model_Sales_Quote_Item::HARDWARE);

        $expectedResult = ['cashondelivery' => true, 'checkmo' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Deliver,
     * the website is Telesales, a new business customer and package has type Acquisition.
     */
    public function testGetPaymentOptionsForDeliveryMethodDeliverOnTelesalesAsNewBusinessCustomerWithAcquisition()
    {
        $deliveryMethod = 'deliver';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_TELESALES_CODE));
        $helper = $this->mockCheckoutHelper(false, true, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, Vznl_Checkout_Model_Sales_Quote_Item::ACQUISITIE);

        $expectedResult = ['cashondelivery' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Deliver,
     * the website is Telesales, a new business customer and package has type Retention.
     */
    public function testGetPaymentOptionsForDeliveryMethodDeliverOnTelesalesAsNewBusinessCustomerWithRetention()
    {
        $deliveryMethod = 'deliver';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_TELESALES_CODE));
        $helper = $this->mockCheckoutHelper(false, true, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, Vznl_Checkout_Model_Sales_Quote_Item::RETENTIE);

        $expectedResult = [];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Deliver,
     * the website is Telesales, a new business customer and package has type Prepaid.
     */
    public function testGetPaymentOptionsForDeliveryMethodDeliverOnTelesalesAsNewBusinessCustomerWithPrepaid()
    {
        $deliveryMethod = 'deliver';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_TELESALES_CODE));
        $helper = $this->mockCheckoutHelper(false, true, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_PREPAID, Vznl_Checkout_Model_Sales_Quote_Item::HARDWARE);

        $expectedResult = ['cashondelivery' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Deliver,
     * the website is Telesales, a new business customer and package has type Hardware.
     */
    public function testGetPaymentOptionsForDeliveryMethodDeliverOnTelesalesAsNewBusinessCustomerWithHardware()
    {
        $deliveryMethod = 'deliver';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_TELESALES_CODE));
        $helper = $this->mockCheckoutHelper(false, true, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_HARDWARE, Vznl_Checkout_Model_Sales_Quote_Item::HARDWARE);

        $expectedResult = ['cashondelivery' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Deliver,
     * the website is Telesales, an existing business customer and package has type Acquisition.
     */
    public function testGetPaymentOptionsForDeliveryMethodDeliverOnTelesalesAsExistingBusinessCustomerWithAcquisition()
    {
        $deliveryMethod = 'deliver';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_TELESALES_CODE));
        $helper = $this->mockCheckoutHelper(true, true, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, Vznl_Checkout_Model_Sales_Quote_Item::ACQUISITIE);

        $expectedResult = ['cashondelivery' => true, 'checkmo' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Deliver,
     * the website is Telesales, an existing business customer and package has type Retention.
     */
    public function testGetPaymentOptionsForDeliveryMethodDeliverOnTelesalesAsExistingBusinessCustomerWithRetention()
    {
        $deliveryMethod = 'deliver';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_TELESALES_CODE));
        $helper = $this->mockCheckoutHelper(true, true, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, Vznl_Checkout_Model_Sales_Quote_Item::RETENTIE);

        $expectedResult = ['cashondelivery' => true, 'checkmo' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Deliver,
     * the website is Telesales, an existing business customer and package has type Prepaid.
     */
    public function testGetPaymentOptionsForDeliveryMethodDeliverOnTelesalesAsExistingBusinessCustomerWithPrepaid()
    {
        $deliveryMethod = 'deliver';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_TELESALES_CODE));
        $helper = $this->mockCheckoutHelper(true, true, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_PREPAID, Vznl_Checkout_Model_Sales_Quote_Item::HARDWARE);

        $expectedResult = ['cashondelivery' => true, 'checkmo' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Deliver,
     * the website is Telesales, an existing business customer and package has type Hardware.
     */
    public function testGetPaymentOptionsForDeliveryMethodDeliverOnTelesalesAsExistingBusinessCustomerWithHardware()
    {
        $deliveryMethod = 'deliver';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_TELESALES_CODE));
        $helper = $this->mockCheckoutHelper(true, true, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_HARDWARE, Vznl_Checkout_Model_Sales_Quote_Item::HARDWARE);

        $expectedResult = ['cashondelivery' => true, 'checkmo' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * -----------------------------------------------------------------------------------------------------------------
     * ---------- Channel: Webshop --------------------------------------------------------------------------------------
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Deliver,
     * the website is Webshop, a new customer and package has type Acquisition.
     */
    public function testGetPaymentOptionsForDeliveryMethodDeliverOnWebshopAsNewCustomerWithAcquisition()
    {
        $deliveryMethod = 'deliver';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_WEB_SHOP_CODE));
        $helper = $this->mockCheckoutHelper(false, false, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, Vznl_Checkout_Model_Sales_Quote_Item::ACQUISITIE);

        $expectedResult = ['cashondelivery' => true, 'adyen_hpp' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Deliver,
     * the website is Webshop, a new customer and package has type Retention.
     */
    public function testGetPaymentOptionsForDeliveryMethodDeliverOnWebshopAsNewCustomerWithRetention()
    {
        $deliveryMethod = 'deliver';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_WEB_SHOP_CODE));
        $helper = $this->mockCheckoutHelper(false, false, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, Vznl_Checkout_Model_Sales_Quote_Item::RETENTIE);

        $expectedResult = [];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Deliver,
     * the website is Webshop, a new customer and package has type Prepaid.
     */
    public function testGetPaymentOptionsForDeliveryMethodDeliverOnWebshopAsNewCustomerWithPrepaid()
    {
        $deliveryMethod = 'deliver';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_WEB_SHOP_CODE));
        $helper = $this->mockCheckoutHelper(false, false, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_PREPAID, Vznl_Checkout_Model_Sales_Quote_Item::HARDWARE);

        $expectedResult = ['cashondelivery' => true, 'adyen_hpp' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Deliver,
     * the website is Webshop, a new customer and package has type Retention.
     */
    public function testGetPaymentOptionsForDeliveryMethodDeliverOnWebshopAsNewCustomerWithHardware()
    {
        $deliveryMethod = 'deliver';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_WEB_SHOP_CODE));
        $helper = $this->mockCheckoutHelper(false, false, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_HARDWARE, Vznl_Checkout_Model_Sales_Quote_Item::HARDWARE);

        $expectedResult = ['cashondelivery' => true, 'adyen_hpp' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Deliver,
     * the website is Webshop, an existing customer and package has type Acquisition.
     */
    public function testGetPaymentOptionsForDeliveryMethodDeliverOnWebshopAsExistingCustomerWithAcquisition()
    {
        $deliveryMethod = 'deliver';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_WEB_SHOP_CODE));
        $helper = $this->mockCheckoutHelper(true, false, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, Vznl_Checkout_Model_Sales_Quote_Item::ACQUISITIE);

        $expectedResult = ['cashondelivery' => true, 'adyen_hpp' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Deliver,
     * the website is Webshop, an existing customer and package has type Retention.
     */
    public function testGetPaymentOptionsForDeliveryMethodDeliverOnWebshopAsExistingCustomerWithRetention()
    {
        $deliveryMethod = 'deliver';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_WEB_SHOP_CODE));
        $helper = $this->mockCheckoutHelper(true, false, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, Vznl_Checkout_Model_Sales_Quote_Item::RETENTIE);

        $expectedResult = ['cashondelivery' => true, 'adyen_hpp' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Deliver,
     * the website is Webshop, an existing customer and package has type Prepaid.
     */
    public function testGetPaymentOptionsForDeliveryMethodDeliverOnWebshopAsExistingCustomerWithPrepaid()
    {
        $deliveryMethod = 'deliver';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_WEB_SHOP_CODE));
        $helper = $this->mockCheckoutHelper(true, false, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_PREPAID, Vznl_Checkout_Model_Sales_Quote_Item::HARDWARE);

        $expectedResult = ['cashondelivery' => true, 'adyen_hpp' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Deliver,
     * the website is Webshop, an existing customer and package has type Hardware.
     */
    public function testGetPaymentOptionsForDeliveryMethodDeliverOnWebshopAsExistingCustomerWithHardware()
    {
        $deliveryMethod = 'deliver';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_WEB_SHOP_CODE));
        $helper = $this->mockCheckoutHelper(true, false, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_HARDWARE, Vznl_Checkout_Model_Sales_Quote_Item::HARDWARE);

        $expectedResult = ['cashondelivery' => true, 'adyen_hpp' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Deliver,
     * the website is Webshop, a new business customer and package has type Acquisition.
     */
    public function testGetPaymentOptionsForDeliveryMethodDeliverOnWebshopAsNewBusinessCustomerWithAcquisition()
    {
        $deliveryMethod = 'deliver';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_WEB_SHOP_CODE));
        $helper = $this->mockCheckoutHelper(false, true, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, Vznl_Checkout_Model_Sales_Quote_Item::ACQUISITIE);

        $expectedResult = ['cashondelivery' => true, 'adyen_hpp' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Deliver,
     * the website is Webshop, a new business customer and package has type Retention.
     */
    public function testGetPaymentOptionsForDeliveryMethodDeliverOnWebshopAsNewBusinessCustomerWithRetention()
    {
        $deliveryMethod = 'deliver';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_WEB_SHOP_CODE));
        $helper = $this->mockCheckoutHelper(false, true, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, Vznl_Checkout_Model_Sales_Quote_Item::RETENTIE);

        $expectedResult = [];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Deliver,
     * the website is Webshop, a new business customer and package has type Prepaid.
     */
    public function testGetPaymentOptionsForDeliveryMethodDeliverOnWebshopAsNewBusinessCustomerWithPrepaid()
    {
        $deliveryMethod = 'deliver';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_WEB_SHOP_CODE));
        $helper = $this->mockCheckoutHelper(false, true,Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_PREPAID, Vznl_Checkout_Model_Sales_Quote_Item::HARDWARE);

        $expectedResult = ['cashondelivery' => true, 'adyen_hpp' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Deliver,
     * the website is Webshop, a new business customer and package has type Hardware.
     */
    public function testGetPaymentOptionsForDeliveryMethodDeliverOnWebshopAsNewBusinessCustomerWithHardware()
    {
        $deliveryMethod = 'deliver';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_WEB_SHOP_CODE));
        $helper = $this->mockCheckoutHelper(false, true, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_HARDWARE, Vznl_Checkout_Model_Sales_Quote_Item::HARDWARE);

        $expectedResult = ['cashondelivery' => true, 'adyen_hpp' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Deliver,
     * the website is Webshop, an existing business customer and package has type Acquisition.
     */
    public function testGetPaymentOptionsForDeliveryMethodDeliverOnWebshopAsExistingBusinessCustomerWithAcquisition()
    {
        $deliveryMethod = 'deliver';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_WEB_SHOP_CODE));
        $helper = $this->mockCheckoutHelper(true, true, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, Vznl_Checkout_Model_Sales_Quote_Item::ACQUISITIE);

        $expectedResult = ['cashondelivery' => true, 'adyen_hpp' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Deliver,
     * the website is Webshop, an existing business customer and package has type Retention.
     */
    public function testGetPaymentOptionsForDeliveryMethodDeliverOnWebshopAsExistingBusinessCustomerWithRetention()
    {
        $deliveryMethod = 'deliver';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_WEB_SHOP_CODE));
        $helper = $this->mockCheckoutHelper(true, true, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, Vznl_Checkout_Model_Sales_Quote_Item::RETENTIE);

        $expectedResult = ['cashondelivery' => true, 'adyen_hpp' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Deliver,
     * the website is Webshop, an existing business customer and package has type Prepaid.
     */
    public function testGetPaymentOptionsForDeliveryMethodDeliverOnWebshopAsExistingBusinessCustomerWithPrepaid()
    {
        $deliveryMethod = 'deliver';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_WEB_SHOP_CODE));
        $helper = $this->mockCheckoutHelper(true, true, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_PREPAID, Vznl_Checkout_Model_Sales_Quote_Item::HARDWARE);

        $expectedResult = ['cashondelivery' => true, 'adyen_hpp' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * Checks if the getPaymentOptionsForDeliveryMethod returns the expected response when delivery method is Deliver,
     * the website is Webshop, an existing business customer and package has type Hardware.
     */
    public function testGetPaymentOptionsForDeliveryMethodDeliverOnWebshopAsExistingBusinessCustomerWithHardware()
    {
        $deliveryMethod = 'deliver';
        $this->assertTrue($this->setCurrentWebsite(Vznl_Agent_Model_Website::WEBSITE_WEB_SHOP_CODE));
        $helper = $this->mockCheckoutHelper(true, true, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_HARDWARE, Vznl_Checkout_Model_Sales_Quote_Item::HARDWARE);

        $expectedResult = ['cashondelivery' => true, 'adyen_hpp' => true];

        $paymentOptions = $helper->getPaymentOptionsForDeliveryMethod($deliveryMethod);
        $this->assertTrue(is_array($paymentOptions));
        $this->assertTrue($this->checkExpectedVsActualPaymentMethods($expectedResult, $paymentOptions));
        $this->registerNormalAgentHelper();
    }

    /**
     * #################################################################################################################
     * ########## Private methods ######################################################################################
     * #################################################################################################################
     */

    /**
     * Get the helper.
     * @return Vznl_Checkout_Helper_Data The checkout helper
     */
    private function getHelperAsMock($hasSplit = true)
    {
        $mockHelper = $this->getMockBuilder('Vznl_Checkout_Helper_Data')
            ->setMethods([
                'hasMoreThanOnePackage',
                ])
            ->getMock();

        $mockHelper->method('hasMoreThanOnePackage')
            ->willReturn($hasSplit);

        return $mockHelper;
    }

    /**
     * @param string $websiteCode One of the codes from Vznl_Agent_Model_Website
     * @return Mage_Core_Model_Website|null The website.
     */
    private function setCurrentWebsite($websiteCode)
    {
        $agentHelperMock = $this->getMockBuilder('Vznl_Agent_Helper_Data')
            ->setMethods([
                'isRetailStore',
                'checkIsWebshop',
                'isTelesalesLine',
                'isRetailLine'])
            ->getMock();

        $retailStore = false;
        $telesalesLine = false;
        $webshop = false;

        if ($websiteCode == Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE
            || $websiteCode == Vznl_Agent_Model_Website::WEBSITE_BELCOMPANY_CODE
        ) {
            $retailStore = true;
        } else if ($websiteCode == Vznl_Agent_Model_Website::WEBSITE_WEB_SHOP_CODE) {
            $webshop = true;
        } else if ($websiteCode == Vznl_Agent_Model_Website::WEBSITE_TELESALES_CODE
            || $websiteCode == Vznl_Agent_Model_Website::WEBSITE_BC_WEBSHOP_CODE
            || $websiteCode == Vznl_Agent_Model_Website::WEBSITE_TRADERS_CODE
        ) {
            $telesalesLine = true;
        }

        $agentHelperMock->method('isRetailStore')
            ->willReturn($retailStore);
        $agentHelperMock->method('checkIsWebshop')
            ->willReturn($webshop);
        $agentHelperMock->method('isTelesalesLine')
            ->willReturn($telesalesLine);
        $agentHelperMock->method('isRetailLine')
            ->willReturn($retailStore);

        Mage::unregister('_helper/agent');
        Mage::register('_helper/agent', $agentHelperMock);

        return true;
    }

    /**
     * Sets the default agent helper back as the registered agent helper.
     */
    private function registerNormalAgentHelper()
    {
        Mage::unregister('_helper/agent');
        Mage::register('_helper/agent', new Vznl_Agent_Helper_Data());
    }

    /**
     * This will mock a few methods of the checkout helper.
     *
     * @param bool $existing Whether a customer is already an existing customer.
     * @param bool $business Whether a customer is a business customer.
     * @param string $type The type of the package.
     * @param string $saleType The sales type of the package.
     * @return Vznl_Checkout_Helper_Data A mocked version of Vznl_Checkout_Helper_Data to be used as checkout helper.
     */
    private function mockCheckoutHelper($existing = false, $business = false, $type = Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE, $saleType = Vznl_Checkout_Model_Sales_Quote_Item::ACQUISITIE)
    {

        $mockHelper = $this->getMockBuilder('Vznl_Checkout_Helper_Data')
            ->setMethods([
                'checkIsRetentionOnly',
                'checkIsAcquisitionOnly',
                'checkHasMobileOnly',
                'checkHasHardwareOnly',
                'checkHasPrepaidOnly',
                'checkIsBusinessCustomer',
                'getCustomer'])
            ->getMock();

        $retentionOnly = false;
        $acquisitionOnly = false;
        switch($saleType){
            case Vznl_Checkout_Model_Sales_Quote_Item::RETENTIE:
                $retentionOnly = true;
                break;
            case Vznl_Checkout_Model_Sales_Quote_Item::ACQUISITIE:
                $acquisitionOnly = true;
                break;
        }

        $hardware = false;
        $prepaid = false;
        $mobile = false;
        switch($type){
            case Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_HARDWARE:
                $hardware = true;
                break;
            case Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_PREPAID:
                $prepaid = true;
                break;
            case Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE:
                $mobile = true;
                break;
        }

        // Set the type and sales type method return values
        $mockHelper->method('checkIsRetentionOnly')
            ->willReturn($retentionOnly);
        $mockHelper->method('checkIsAcquisitionOnly')
            ->willReturn($acquisitionOnly);
        $mockHelper->method('checkHasMobileOnly')
            ->willReturn($mobile);
        $mockHelper->method('checkHasHardwareOnly')
            ->willReturn($hardware);
        $mockHelper->method('checkHasPrepaidOnly')
            ->willReturn($prepaid);

        // Set the is checkIsBusinessCustomer
        $mockHelper->method('checkIsBusinessCustomer')
            ->willReturn($business);

        $mockCustomer = $this->getMockBuilder('Omnius_Customer_Model_Customer_Customer')
            ->setMethods(['getId'])
            ->getMock();
        $mockCustomer->method('getId')
            ->willReturn($existing ? 1 : null);
        $mockHelper->method('getCustomer')
            ->willReturn($mockCustomer);

        return $mockHelper;
    }

    /**
     * Checks whether all expected payments are found in the real payment array.
     * @param $expectedPayments the expected payments
     * @param $realPayments the real payments
     * @return bool whether everything was found
     */
    private function checkExpectedVsActualPaymentMethods($expectedPayments, $actualPayments){
        $expectedFound = 0;
        $unexpectedFound = 0;

        foreach ($actualPayments as $actualPayment => $use){
            // Checks whether the actual found exists in the expected array and if so check if the use of it is the same
            if(isset($expectedPayments[$actualPayment]) && $expectedPayments[$actualPayment] == $use){
                $expectedFound++;
            }elseif ($use){
                // If the actual found was not expected but is going to be used, it is unexpected.
                $unexpectedFound++;
            }
        }
        return $expectedFound == count($expectedPayments);
    }
}
