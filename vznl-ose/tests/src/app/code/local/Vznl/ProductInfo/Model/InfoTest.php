<?php

/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

use PHPUnit\Framework\TestCase;

class Vznl_ProductInfo_Model_Info_Test extends TestCase
{
	protected function addFieldToFilter() 
	{
        $mock = $this->getMockBuilder(Vznl_ProductInfo_Model_Mysql4_Info_Collection::class)
            ->disableOriginalConstructor()
            ->setMethods(['addFieldToFilter','limit','getSelect'])
            ->getMock();
        $mock->method('addFieldToFilter')
            ->willReturnSelf();
        $mock->method('limit')
            ->willReturnSelf();
        $mock->method('getSelect')
            ->willReturnSelf();
        return $mock;
	}

	public function testLoadBySku()
	{
        $mockproduct = $this->getMockBuilder(Vznl_ProductInfo_Model_Info::class)
            ->setMethods(['getCollection'])
            ->getMock();
        $mockproduct->method('getCollection')
            ->willReturn($this->addFieldToFilter());

        $this->assertInstanceOf(Vznl_ProductInfo_Model_Mysql4_Info_Collection::class, $mockproduct->loadBySku(1));
	}
}
