<?php

/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

use PHPUnit\Framework\TestCase;

class Vznl_ProductInfo_Model_Mysql4_Info_Test extends TestCase
{
	public function testConstruct()
	{
		$obj = new Vznl_ProductInfo_Model_Mysql4_Info;
		$this->assertEquals($obj->getIdFieldName(), 'id');
	}
}