<?php

namespace tests\src\app\code\local\Vznl\ReserveTelephoneNumber\Adapter\Peal;

use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Mage;
use Vznl_ReserveTelephoneNumber_Adapter_Peal_Adapter;
use Vznl_ReserveTelephoneNumber_Helper_Data;

class AdapterTest extends \PHPUnit\Framework\TestCase{

    protected function getMockerHelperFunctions($dependency){

        $adapterMock = $this->getMockBuilder(Vznl_ReserveTelephoneNumber_Adapter_Peal_Adapter::class)
            ->setConstructorArgs(array($dependency,'www.dynacommerce.com/','channel','NL','footprint'))
            ->setMethods(['getHelper'])
            ->getMock();

        $adapterHelperMock = $this->getMockBuilder(Vznl_ReserveTelephoneNumber_Helper_Data::class)
            ->disableOriginalConstructor()
            ->setMethods(['getLogin','getPassword'])
            ->getMock();

        $adapterHelperMock->method('getLogin')->willReturn('demouser');

        $adapterHelperMock->method('getPassword')->willReturn('demopassword');

        $adapterMock->method('getHelper')->willReturn($adapterHelperMock);

        return $adapterMock;
    }

    public function testCall()
    {
        $getMockResponseFromFile = file_get_contents(Mage::getModuleDir('etc', 'Vznl_ReserveTelephoneNumber') . '/stubs/ReserveTelephoneNumber/result.json');

        $requestMock = new MockHandler([
            new Response(200,[],$getMockResponseFromFile),
            new Response(200,[],$getMockResponseFromFile)
        ]);

        $handler     = HandlerStack::create($requestMock);

        $dependency  = new Client(['handler' => $handler]);

        $adapter     = $this->getMockerHelperFunctions($dependency);

        $response = $adapter->call([
            'requestType'     => 1,
            'houseFlatNumber' => 1,
            'postCode'        => 1,
            'directoryNumber' => 1,
            'customerType'    => 1,
            'houseFlatExt'    => 1
        ]);

        $this->assertEquals("200",$response['statusCode']);
    }

    public function testGetUrl()
    {
        $adapter = new Vznl_ReserveTelephoneNumber_Adapter_Peal_Adapter(new Client(),'www.dynacommerce.com/','telesales','NL','footprint');

        $this->assertEquals('www.dynacommerce.com/?cty=NL&chl=telesales&requestType=4000054909875&footPrint=Ziggo',$adapter->getUrl('4000054909875'));

    }

    public function testHelperInstance(){

        $adapter = new Vznl_ReserveTelephoneNumber_Adapter_Peal_Adapter(new Client(),'www.dynacommerce.com/','telesales','NL','footprint');

        $this->assertInstanceOf(Vznl_ReserveTelephoneNumber_Helper_Data::class,$adapter->getHelper());
    }
}
