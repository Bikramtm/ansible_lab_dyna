<?php

namespace tests\src\app\code\local\Vznl\FFHandler\Model;

use PHPUnit\Framework\TestCase;
use Vznl_FFHandler_Model_Request;
use Zend_Date;

class RequestTest extends TestCase
{
    public function testGetStatusDescription()
    {
        $mock = $this->getMockBuilder(Vznl_FFHandler_Model_Request::class)
            ->setMethods(['getStatus'])
            ->getMock();

        $mock->method('getStatus')->willReturnOnConsecutiveCalls(1,2,3,4,5,6,9999);

        $this->assertEquals('Waiting',
            $mock->getStatusDescription());

        $this->assertEquals('Completed',
            $mock->getStatusDescription());

        $this->assertEquals('Cancelled by requester',
            $mock->getStatusDescription());

        $this->assertEquals('Failed - requester was blocked',
            $mock->getStatusDescription());

        $this->assertEquals('Failed - requester already blocked',
            $mock->getStatusDescription());

        $this->assertEquals('Failed - no relation was found',
            $mock->getStatusDescription());

        $this->assertNull($mock->getStatusDescription());
    }

    public function testGetValidDateFrom()
    {
        $this->assertInstanceOf(Zend_Date::class,
            (new Vznl_FFHandler_Model_Request())->getValidDateFrom());
    }

    public function testGetValidDateTo()
    {
        $this->assertInstanceOf(Zend_Date::class,
            (new Vznl_FFHandler_Model_Request())->getValidDateTo());
    }

    public function testLoadByConfirmationHash()
    {
        $mock = $this->getMockBuilder(Vznl_FFHandler_Model_Request::class)
            ->setMethods(['load'])
            ->getMock();

        $mock->method('load')
            ->willReturn(true);

        $this->assertTrue($mock->loadByConfirmationHash(12));
    }

    public function testLoadByCode()
    {
        $mock = $this->getMockBuilder(Vznl_FFHandler_Model_Request::class)
            ->setMethods(['load'])
            ->getMock();

        $mock->method('load')
            ->willReturn(true);

        $this->assertTrue($mock->loadByCode(1));
    }

    public function testSetRelation()
    {
        $mock = $this->getMockBuilder(Vznl_FFHandler_Model_Request::class)
            ->setMethods([
                'setRelationId',
                'getId'
            ])->getMock();

        $mock->method('setRelationId')
            ->willReturn(true);

        $mock->method('getId')
            ->willReturn(true);

        $this->assertInternalType('boolean',
            ($mock)->setRelation($mock));
    }

    public function testIsValid()
    {
        $mock = $this->getMockBuilder(Vznl_FFHandler_Model_Request::class)
            ->setMethods([
                'getValidFrom',
                'getValidTo',
                'getValidDateFrom',
                'getValidDateTo'
            ])->getMock();

        $mock->method('getValidFrom')
            ->willReturnOnConsecutiveCalls(false,true);

        $mock->method('getValidTo')
            ->willReturnOnConsecutiveCalls(false,true);

        $mock->method('getValidDateFrom')
            ->willReturnOnConsecutiveCalls((date('Y-m-d',strtotime("-1 day"))),(date('Y-m-d',strtotime("+1 day"))));

        $mock->method('getValidDateTo')
            ->willReturnOnConsecutiveCalls((date('Y-m-d',strtotime("+1 day"))),(date('Y-m-d',strtotime("-1 day"))));


        $this->assertTrue($mock->isValid());

        $this->assertTrue($mock->isValid());

        $this->assertFalse($mock->isValid());

    }

    public function testCanBeUsedByCustomer()
    {
        $mock = $this->getMockBuilder(Vznl_FFHandler_Model_Request::class)
            ->setMethods([
                'getCustomerAuthType',
                'hasCustomerAuthValue',
                'getEnabled',
                'isValid',
                'getUsagePerCode',
                'getRealCodeUsageNr',
                'getUsagePerCustomer',
                'getRealCustomerUsageNr',
                'getStatus',
                'canBeUsed'
            ])->getMock();

        $mock->method('getCustomerAuthType')
            ->willReturnOnConsecutiveCalls(1,89);

        $mock->method('hasCustomerAuthValue')
            ->willReturn(false);

        $mock->method('getEnabled')
            ->willReturn(false);

        $mock->method('isValid')
            ->willReturn(false);

        $mock->method('getUsagePerCode')
            ->willReturn(true);

        $mock->method('getRealCodeUsageNr')
            ->willReturn(0);

        $mock->method('getUsagePerCustomer')
            ->willReturn(1);

        $mock->method('getRealCustomerUsageNr')
            ->willReturn(0);

        $mock->method('getStatus')
            ->willReturn(0);

        $mock->method('canBeUsed')
            ->willReturn(true);

        $this->assertInternalType('boolean',
            $mock->canBeUsedByCustomer('AUTH_CODE%$%^&*',12985));

        $this->assertInternalType('boolean',
            $mock->canBeUsedByCustomer('AUTH_CODE%$%^&*',12985));

    }

}
