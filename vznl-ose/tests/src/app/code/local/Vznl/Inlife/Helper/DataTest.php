<?php

namespace tests\src\app\code\local\Vznl\Inlife\Helper;

use Mage;
use PHPUnit\Framework\TestCase;
use Vznl_Inlife_Helper_Data;

class DataTest extends TestCase
{
    public function testProcessAddonData()
    {
        $getMockResponseFromFile = file_get_contents(Mage::getModuleDir('etc', 'Vznl_Inlife') . '/stubs/GetAddons/get_addons_response.json');

        $init = new Vznl_Inlife_Helper_Data();

        $getMockResponseFromFile = json_decode($getMockResponseFromFile,true);

        $this->assertInternalType('array',
            $init->processAddonData($getMockResponseFromFile['eligible_components']));

        $this->assertInternalType('array',
            $init->processAddonData($getMockResponseFromFile['assigned_components']));

    }

    public function testArray2ul()
    {
        $init = new Vznl_Inlife_Helper_Data();

        $this->assertStringStartsWith('<ul',
            $init->array2ul(['test' => 'test'])
        );
    }

    public function testCanMigratePricePlans()
    {
        $init = new Vznl_Inlife_Helper_Data();

        $this->assertInternalType('boolean',
            $init->canMigratePricePlans());
    }

    public function testHtmlSpecialChars()
    {
        $init = new Vznl_Inlife_Helper_Data();

        $this->assertEquals('&lt;h1&gt;hello&lt;/h1&gt;',
            $init->htmlspecialchars('<h1>hello</h1>'));

    }
}
