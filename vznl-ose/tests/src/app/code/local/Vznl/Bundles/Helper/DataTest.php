<?php

namespace tests\src\app\code\local\Vznl\Bundles\Helper;

use Dyna_Bundles_Model_BundleRule;
use PHPUnit\Framework\TestCase;
use Vznl_Bundles_Helper_Data;

class DataTest extends TestCase
{
    public function testGetCurrentBundle()
    {
        $this->assertNull((new Vznl_Bundles_Helper_Data)->getCurrentBundle());
    }

    public function testSetCurrentBundle()
    {
        $this->assertInstanceOf(Vznl_Bundles_Helper_Data::class,
            (new Vznl_Bundles_Helper_Data)->setCurrentBundle((new Dyna_Bundles_Model_BundleRule)));
    }

    public function testIsSimulation()
    {
        $this->assertFalse((new Vznl_Bundles_Helper_Data())->isSimulation());
    }

    protected function helperFunction()
    {
        $mock = $this->getMockBuilder('Vznl_Bundles_Helper_Data')
            ->setMethods([
                'getMandatoryFamilies',
                'getBundleRuleParser',
                'getOfferProducts',
                'getQuote',
                'getActivePackageId',
                'getAllItems',
                'getPackageId',
                'getBundleComponent',
                'getProductId',
                'savedParserResponseAppliesToQuote',
                'getMandatoryChooseFromFamilies',
            ])
            ->getMock();

        return $mock;
    }

    protected function nonExistingHelperClass()
    {
        $nonExistingClass = $this->getMockBuilder('nonExistingClass')
            ->setMethods([
                'getMandatoryChooseFromFamilies',
                'getMandatoryChooseFromFamiliesFormat',
                'getPackageId',
                'getProductId',
                'getBundleComponent'
            ])->getMock();

        return $nonExistingClass;
    }

    public function testHasMandatoryFamiliesCompleted()
    {
        $mock = $this->helperFunction();

        $mock->method('getMandatoryFamilies')
            ->willReturn(null);

        $this->assertTrue($mock->hasMandatoryFamiliesCompleted());
    }

    public function testIsBundleChoiceFlow()
    {
        $mock = $this->helperFunction();

        $nonExistingClass = $this->nonExistingHelperClass();

        $nonExistingClass->method('getMandatoryChooseFromFamilies')
            ->willReturn('TEST_DATA');

        $mock->method('getBundleRuleParser')
            ->willReturnOnConsecutiveCalls($nonExistingClass,false);

        $mock->method('savedParserResponseAppliesToQuote')
            ->willReturn(true);

        $this->assertTrue($mock->isBundleChoiceFlow());

        $this->assertFalse($mock->isBundleChoiceFlow());
    }

    public function testGetAllMandatoryFamilies()
    {
        $mock = $this->helperFunction();

        $nonExistingClass = $this->nonExistingHelperClass();

        $nonExistingClass->method('getMandatoryChooseFromFamiliesFormat')->willReturn(true);

        $mock->method('getBundleRuleParser')->willReturnOnConsecutiveCalls($nonExistingClass,false);

        $mock->method('savedParserResponseAppliesToQuote')->willReturn(true);

        $this->assertTrue($mock->getAllMandatoryFamilies());

        $this->assertEmpty($mock->getAllMandatoryFamilies());

    }

    public function testProductsInOffer()
    {
        $mock = $this->helperFunction();

        $mock->method('getOfferProducts')->willReturn(['HELLO','FROM','THE','OUTSIDE']);

        $this->assertTrue($mock->productsInOffer(['HELLO','FROM','O','S'],'TEST'));

        $this->assertFalse($mock->productsInOffer(['H','F','O','S'],'TEST'));
    }

    public function testGetSelectSalesIDUrl()
    {
        $this->assertContains('bundles/index/selectSalesId',
            (new Vznl_Bundles_Helper_Data())->getSelectSalesIDUrl());
    }

    public function testClearCachedActiveBundles()
    {
        $this->assertInstanceOf(Vznl_Bundles_Helper_Data::class,(new Vznl_Bundles_Helper_Data())->clearCachedActiveBundles());
    }
 
    public function testCalculateTotalsForProducts()
    {
        $data = [
            'maf'  => 1,
            'type' => 'remove',
            ''
        ];

        $this->assertEquals(-1,(new Vznl_Bundles_Helper_Data())->calculateTotalsForProducts([$data]));

        $data = [
            'maf' => 0,
            'promoDiscount' => 12345
        ];

        $this->assertEquals(-12345,(new Vznl_Bundles_Helper_Data())->calculateTotalsForProducts([$data]));

        $data = [
            'maf' => 2,
            'type' => 'test',
            'promoDiscount' => 12345
        ];

        $this->assertEquals(2,(new Vznl_Bundles_Helper_Data())->calculateTotalsForProducts([$data]));

    }

}
