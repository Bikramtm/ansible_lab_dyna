<?php


namespace tests\src\app\code\local\Vznl\Bundles\Helper;


use PHPUnit\Framework\TestCase;
use Vznl_Bundles_Helper_Expression;

class ExpressionTest extends TestCase
{
    public function testIsSimulateActive()
    {
        $mock = $this->getMockBuilder(Vznl_Bundles_Helper_Expression::class)
            ->disableOriginalConstructor()
            ->setMethods(['isSimulateActive'])
            ->getMock();

        $mock->method('isSimulateActive')->willReturn(false);

        $this->assertFalse(($mock)->isSimulateActive());
    }

    public function testGetSimulatedProductId()
    {
        $mock = $this->getMockBuilder(Vznl_Bundles_Helper_Expression::class)
            ->disableOriginalConstructor()
            ->setMethods(['getSimulatedProductId'])
            ->getMock();

        $mock->method('getSimulatedProductId')->willReturn(null);

        $this->assertNull(($mock)->getSimulatedProductId());
    }

}
