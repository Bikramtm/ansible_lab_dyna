<?php


namespace tests\src\app\code\local\Vznl\GetBasket\Peal;


use GuzzleHttp\Client;

use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Mage;
use PHPUnit\Framework\TestCase;
use Vznl_GetBasket_Adapter_Peal_Adapter;
use Vznl_GetBasket_Helper_Data;

class AdapterTest extends TestCase
{
    protected function getMockerHelperFunctions($dependency){

        $adapterMock = $this->getMockBuilder(Vznl_GetBasket_Adapter_Peal_Adapter::class)
            ->setConstructorArgs(array($dependency,'www.dynacommerce.com/','channel','NL','footprint'))
            ->setMethods(['getHelper'])
            ->getMock();

        $adapterHelperMock = $this->getMockBuilder(Vznl_GetBasket_Adapter_Peal_Adapter::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'getLogin',
                'getPassword',
                'initTimeMeasurement',
                'getVerify',
                'getProxy',
                'transferLog'
            ])->getMock();


        $adapterHelperMock->method('getLogin')
            ->willReturnOnConsecutiveCalls('demouser','demouser','');

        $adapterHelperMock->method('getPassword')
            ->willReturnOnConsecutiveCalls('demopassword','demopassword','');

        $adapterHelperMock->method('initTimeMeasurement')
            ->willReturn(123);

        $adapterHelperMock->method('getVerify')
            ->willReturn(123);

        $adapterHelperMock->method('getProxy')
            ->willReturn(123);

        $adapterHelperMock->method('transferLog')
            ->willReturn(123);

        $adapterMock->method('getHelper')
            ->willReturn($adapterHelperMock);

        return $adapterMock;
    }

    public function testCall()
    {
        $getMockResponseFromFile = file_get_contents(Mage::getModuleDir('etc', 'Vznl_GetBasket') . '/stubs/GetBasket/result.json');

        $requestMock = new MockHandler([
            new Response(200,[],$getMockResponseFromFile),
            new Response(200,[],$getMockResponseFromFile),
            new Response(200,[],$getMockResponseFromFile),
        ]);

        $handler     = HandlerStack::create($requestMock);

        $dependency  = new Client(['handler' => $handler]);

        $adapter     = $this->getMockerHelperFunctions($dependency);

        $response = $adapter->call(1,1,1,1);

        $this->assertInternalType("array",$response);

        $response = $adapter->call(1,1,1,'PRODUCT_ORDER');

        $this->assertInternalType("array",$response);

        $response = $adapter->call(1,1,1,'PRODUCT_ORDER');

        $this->assertInternalType("array",$response);

    }

    public function testGetUrl()
    {
        $adapter = new Vznl_GetBasket_Adapter_Peal_Adapter(new Client(),'www.dynacommerce.com/','telesales','NL');

        $this->assertEquals('www.dynacommerce.com/4000054909875?cty=NL&chl=telesales&orderType=&crossFootPrint=false&moveAddressId=',
            $adapter->getUrl('4000054909875'));

    }

    public function testHelperInstance(){

        $adapter = new Vznl_GetBasket_Adapter_Peal_Adapter(new Client(),'www.dynacommerce.com/','telesales','NL');

        $this->assertInstanceOf(Vznl_GetBasket_Helper_Data::class,
            $adapter->getHelper());
    }

    public function testCallForException(){

        $requestMock = new MockHandler([
            new RequestException('Error Communicating with Server', new Request('GET', 'test'))
        ]);

        $handler     = HandlerStack::create($requestMock);

        $dependency  = new Client(['handler' => $handler]);

        $adapter     = $this->getMockerHelperFunctions($dependency);

        $this->assertEquals('Error Communicating with Server',
            $adapter->call(1,1,1,1)['message']);
    }
}
