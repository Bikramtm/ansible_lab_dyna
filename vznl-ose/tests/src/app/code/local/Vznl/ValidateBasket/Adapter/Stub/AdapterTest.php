<?php

use PHPUnit\Framework\TestCase;

class Vznl_ValidateBasket_Adapter_Stub_AdapterTest extends TestCase
{
    public function testCallIsMapped()
    {
        $adapter = new Vznl_ValidateBasket_Adapter_Stub_Adapter();
        $response = $adapter->call('4000055045800');
        $this->assertInternalType("array", $response);
    }

    public function testCallWhenBasketId()
    {
        $adapter = new Vznl_ValidateBasket_Adapter_Stub_Adapter();
        $response = $adapter->call(Null);
        $this->assertInternalType("string", $response);
    }

}
