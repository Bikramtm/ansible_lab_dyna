<?php

namespace tests\src\app\code\local\Vznl\Utility\Model;

use PHPUnit\Framework\TestCase;
use Vznl_Utility_Helper_Data;

class DataTest extends TestCase
{
    protected function helperFunction()
    {
        $mock = $this->getMockBuilder('FakeModel')
            ->setMethods(['save','load'])
            ->getMock();

        $mock->expects($this->any())
            ->method('save')
            ->willReturn('FakeModel');

        $mock->method('load')
            ->with(12345)
            ->willReturn(true);

        return $mock;
    }
    public function testSaveModel()
    {
        $mock = $this->helperFunction();

        $this->assertNull((new Vznl_Utility_Helper_Data)->saveModel($mock));
    }

    public function testLoadModelById()
    {
        $mock = $this->helperFunction();

        $this->assertTrue((new Vznl_Utility_Helper_Data)->loadModelById($mock,12345));

    }

    public function testGetArrayCount()
    {
        $this->assertEquals(5,(new Vznl_Utility_Helper_Data())->getArrayCount([9,4,3,2,0]));
    }
}
