<?php

use PHPUnit\Framework\TestCase;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Exception\RequestException;

class Vznl_AddItemToBasket_Helper_DataTest extends TestCase
{
    protected function mockGetIsStub($value)
    {
        $mock = $this->getMockBuilder(Vznl_AddItemToBasket_Helper_Data::class)
            ->setMethods([
                'getAdapterChoice',
                'getLogin',
                'getPassword',
                'call'
            ])->getMock();
        $mock->method('getAdapterChoice')->willReturn($value);
        $mock->method('getLogin')->willReturn('demouser');
        $mock->method('getPassword')->willReturn('demopassword');
        $mock->method('call')->willReturn(array());
        return $mock;
    }

    public function testAddItemToBasket()
    {
        $mock = $this->mockGetIsStub('Vznl_AddItemToBasket_Adapter_Stub_Factory');
        $this->assertInternalType('array', $mock->addItemToBasket('4000055045800'));
        $mock = $this->mockGetIsStub('invalid');
        $this->assertInternalType('string', $mock->addItemToBasket('4000055045800'));
    }

    public function testGetAdapterChoice()
    {
        $this->assertEquals(Mage::helper('vznl_additemtobasket')->getAdapterChoice(), 'Stub');
    }
}
	 