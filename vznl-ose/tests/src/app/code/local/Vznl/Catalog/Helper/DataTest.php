<?php

use PHPUnit\Framework\TestCase;

class Vznl_Catalog_Helper_Data_Test extends TestCase
{
    public function testIsSellableNoStockNoBackOrder()
    {
        $product = $this->getProduct();
        $helper = $this->getCatalogDataHelper(false, Vznl_Catalog_Helper_Data::STOCK_TEXT_ENDOFLIFE);

        $stockStatus = 173;
        $product->setStockStatus($stockStatus);
        $response = $helper->checkIfSellable(0, 0, $product);
        $this->assertFalse($response);
        $text = Mage::helper('vznl_catalog')->__(Vznl_Catalog_Helper_Data::STOCK_TEXT_SOLDOUT);
        $this->checkProductStockStatus($product, Vznl_Catalog_Helper_Data::STOCK_CLASS_SOLDOUT, $text, Vznl_Catalog_Helper_Data::STOCK_CLASS_SOLDOUT);

        $stockStatus = 174;
        $product->setStockStatus($stockStatus);
        $response = $helper->checkIfSellable(0, 0, $product);
        $this->assertFalse($response);
        $text = Mage::helper('vznl_catalog')->__(Vznl_Catalog_Helper_Data::STOCK_TEXT_SOLDOUT);
        $this->checkProductStockStatus($product, Vznl_Catalog_Helper_Data::STOCK_CLASS_SOLDOUT, $text, Vznl_Catalog_Helper_Data::STOCK_CLASS_SOLDOUT);

        $stockStatus = 175;
        $product->setStockStatus($stockStatus);
        $response = $helper->checkIfSellable(0, 0, $product);
        $this->assertFalse($response);
        $text = Mage::helper('vznl_catalog')->__(Vznl_Catalog_Helper_Data::STOCK_TEXT_ENDOFLIFE);
        $this->checkProductStockStatus($product, $stockStatus, $text, Vznl_Catalog_Helper_Data::STOCK_CLASS_ENDOFLIFE);

        $stockStatus = 176;
        $product->setStockStatus($stockStatus);
        $response = $helper->checkIfSellable(0, 0, $product);
        $this->assertFalse($response);
        $text = Mage::helper('vznl_catalog')->__(Vznl_Catalog_Helper_Data::STOCK_TEXT_SOLDOUT);
        $this->checkProductStockStatus($product, Vznl_Catalog_Helper_Data::STOCK_CLASS_SOLDOUT, $text, Vznl_Catalog_Helper_Data::STOCK_CLASS_SOLDOUT);
    }

    public function testIsSellableStockNoBackOrder()
    {
        $product = $this->getProduct();
        $helper = $this->getCatalogDataHelper(false, Vznl_Catalog_Helper_Data::STOCK_TEXT_INSTOCK);

        $stockStatus = Vznl_Catalog_Helper_Data::STOCK_CLASS_SOLDOUT;
        $product->setStockStatus($stockStatus);
        $response = $helper->checkIfSellable(10, 0, $product);
        $this->assertTrue($response);
        $this->checkProductStockStatus($product, Vznl_Catalog_Helper_Data::STOCK_CLASS_INSTOCK, Vznl_Catalog_Helper_Data::STOCK_TEXT_INSTOCK, Vznl_Catalog_Helper_Data::STOCK_CLASS_INSTOCK);

        $stockStatus = Vznl_Catalog_Helper_Data::STOCK_CLASS_INSTOCK;
        $product->setStockStatus($stockStatus);
        $response = $helper->checkIfSellable(10, 0, $product);
        $this->assertTrue($response);
        $this->checkProductStockStatus($product, Vznl_Catalog_Helper_Data::STOCK_CLASS_INSTOCK, Vznl_Catalog_Helper_Data::STOCK_TEXT_INSTOCK, Vznl_Catalog_Helper_Data::STOCK_CLASS_INSTOCK);

        $product = $this->getProduct();
        $helper = $this->getCatalogDataHelper(false, Vznl_Catalog_Helper_Data::STOCK_TEXT_ENDOFLIFE);

        $stockStatus = Vznl_Catalog_Helper_Data::STOCK_CLASS_ENDOFLIFE;
        $product->setStockStatus($stockStatus);
        $response = $helper->checkIfSellable(10, 0, $product);
        $this->assertTrue($response);
        $this->checkProductStockStatus($product, Vznl_Catalog_Helper_Data::STOCK_CLASS_INSTOCK, Vznl_Catalog_Helper_Data::STOCK_TEXT_INSTOCK, Vznl_Catalog_Helper_Data::STOCK_CLASS_INSTOCK);

        $product = $this->getProduct();
        $helper = $this->getCatalogDataHelper(false, Vznl_Catalog_Helper_Data::STOCK_TEXT_LIMITED);

        $stockStatus = Vznl_Catalog_Helper_Data::STOCK_CLASS_LIMITED;
        $product->setStockStatus($stockStatus);
        $response = $helper->checkIfSellable(10, 0, $product);
        $this->assertTrue($response);
        $this->checkProductStockStatus($product, Vznl_Catalog_Helper_Data::STOCK_CLASS_INSTOCK, Vznl_Catalog_Helper_Data::STOCK_TEXT_INSTOCK, Vznl_Catalog_Helper_Data::STOCK_CLASS_INSTOCK);

    }

    public function testIsSellableNoStockBackOrder()
    {
        $product = $this->getProduct();
        $helper = $this->getCatalogDataHelper(false, Vznl_Catalog_Helper_Data::STOCK_TEXT_COMINGSOON);
        $product->setAxiThreshold(10);

        $stockStatus = 173;
        $product->setStockStatus($stockStatus);
        $response = $helper->checkIfSellable(0, 0, $product);
        $this->assertTrue($response);
        $text = Mage::helper('vznl_catalog')->__(Vznl_Catalog_Helper_Data::STOCK_TEXT_COMINGSOON);
        $this->checkProductStockStatus($product, $stockStatus, $text, Vznl_Catalog_Helper_Data::STOCK_CLASS_COMINGSOON);

        $stockStatus = 174;
        $product->setStockStatus($stockStatus);
        $response = $helper->checkIfSellable(0, 0, $product);
        $this->assertTrue($response);
        $text = Mage::helper('vznl_catalog')->__(Vznl_Catalog_Helper_Data::STOCK_TEXT_BACKORDER);
        $this->checkProductStockStatus($product, Vznl_Catalog_Helper_Data::STOCK_CLASS_BACKORDER, $text, Vznl_Catalog_Helper_Data::STOCK_CLASS_BACKORDER);

        $product = $this->getProduct();
        $helper = $this->getCatalogDataHelper(false, Vznl_Catalog_Helper_Data::STOCK_TEXT_ENDOFLIFE);
        $product->setAxiThreshold(10);

        $stockStatus = 175;
        $product->setStockStatus($stockStatus);
        $response = $helper->checkIfSellable(0, 0, $product);
        $this->assertFalse($response);
        $text = Mage::helper('vznl_catalog')->__(Vznl_Catalog_Helper_Data::STOCK_TEXT_ENDOFLIFE);
        $this->checkProductStockStatus($product, $stockStatus, $text, Vznl_Catalog_Helper_Data::STOCK_CLASS_ENDOFLIFE);

        $stockStatus = 176;
        $product->setStockStatus($stockStatus);
        $response = $helper->checkIfSellable(0, 0, $product);
        $this->assertFalse($response);
        $text = Mage::helper('vznl_catalog')->__(Vznl_Catalog_Helper_Data::STOCK_TEXT_SOLDOUT);
        $this->checkProductStockStatus($product, Vznl_Catalog_Helper_Data::STOCK_CLASS_SOLDOUT, $text, Vznl_Catalog_Helper_Data::STOCK_CLASS_SOLDOUT);
    }

    public function testIsSellableInOtherStore()
    {
        $product = $this->getProduct();
        $helper = $this->getCatalogDataHelper(true);
        $product->setAxiThreshold(0);

        $stockStatus = 173;
        $product->setStockStatus($stockStatus);
        $response = $helper->checkIfSellable(0, 10, $product);
        $this->assertTrue($response);
        $text = Mage::helper('vznl_catalog')->__(Vznl_Catalog_Helper_Data::STOCK_TEXT_OTHERSTORE);
        $this->checkProductStockStatus($product, Vznl_Catalog_Helper_Data::STOCK_CLASS_OTHERSTORE, $text, Vznl_Catalog_Helper_Data::STOCK_CLASS_OTHERSTORE);

        $stockStatus = 174;
        $product->setStockStatus($stockStatus);
        $response = $helper->checkIfSellable(0, 10, $product);
        $this->assertTrue($response);
        $text = Mage::helper('vznl_catalog')->__(Vznl_Catalog_Helper_Data::STOCK_TEXT_OTHERSTORE);
        $this->checkProductStockStatus($product, Vznl_Catalog_Helper_Data::STOCK_CLASS_OTHERSTORE, $text, Vznl_Catalog_Helper_Data::STOCK_CLASS_OTHERSTORE);

        $stockStatus = 175;
        $product->setStockStatus($stockStatus);
        $response = $helper->checkIfSellable(0, 10, $product);
        $this->assertTrue($response);
        $text = Mage::helper('vznl_catalog')->__(Vznl_Catalog_Helper_Data::STOCK_TEXT_OTHERSTORE);
        $this->checkProductStockStatus($product, Vznl_Catalog_Helper_Data::STOCK_CLASS_OTHERSTORE, $text, Vznl_Catalog_Helper_Data::STOCK_CLASS_OTHERSTORE);

        $stockStatus = 176;
        $product->setStockStatus($stockStatus);
        $response = $helper->checkIfSellable(0, 10, $product);
        $this->assertTrue($response);
        $text = Mage::helper('vznl_catalog')->__(Vznl_Catalog_Helper_Data::STOCK_TEXT_OTHERSTORE);
        $this->checkProductStockStatus($product, Vznl_Catalog_Helper_Data::STOCK_CLASS_OTHERSTORE, $text, Vznl_Catalog_Helper_Data::STOCK_CLASS_OTHERSTORE);
    }

    public function testIsSellableInOtherStoreButBackOrder()
    {
        $product = $this->getProduct();
        $helper = $this->getCatalogDataHelper(true);
        $product->setAxiThreshold(10);

        $stockStatus = 173;
        $product->setStockStatus($stockStatus);
        $response = $helper->checkIfSellable(0, 2, $product);
        $this->assertTrue($response);
        $text = Mage::helper('vznl_catalog')->__(Vznl_Catalog_Helper_Data::STOCK_TEXT_OTHERSTORE);
        $this->checkProductStockStatus($product, Vznl_Catalog_Helper_Data::STOCK_CLASS_OTHERSTORE, $text, Vznl_Catalog_Helper_Data::STOCK_CLASS_OTHERSTORE, 'assertNotEquals');

        $stockStatus = 174;
        $product->setStockStatus($stockStatus);
        $response = $helper->checkIfSellable(0, 2, $product);
        $this->assertTrue($response);
        $text = Mage::helper('vznl_catalog')->__(Vznl_Catalog_Helper_Data::STOCK_TEXT_OTHERSTORE);
        $this->checkProductStockStatus($product, Vznl_Catalog_Helper_Data::STOCK_CLASS_OTHERSTORE, $text, Vznl_Catalog_Helper_Data::STOCK_CLASS_OTHERSTORE, 'assertNotEquals');

        $stockStatus = 175;
        $product->setStockStatus($stockStatus);
        $response = $helper->checkIfSellable(0, 2, $product);
        $this->assertTrue($response);
        $text = Mage::helper('vznl_catalog')->__(Vznl_Catalog_Helper_Data::STOCK_TEXT_OTHERSTORE);
        $this->checkProductStockStatus($product, Vznl_Catalog_Helper_Data::STOCK_CLASS_OTHERSTORE, $text, Vznl_Catalog_Helper_Data::STOCK_CLASS_OTHERSTORE);

        $stockStatus = 176;
        $product->setStockStatus($stockStatus);
        $response = $helper->checkIfSellable(0, 2, $product);
        $this->assertTrue($response);
        $text = Mage::helper('vznl_catalog')->__(Vznl_Catalog_Helper_Data::STOCK_TEXT_OTHERSTORE);
        $this->checkProductStockStatus($product, Vznl_Catalog_Helper_Data::STOCK_CLASS_OTHERSTORE, $text, Vznl_Catalog_Helper_Data::STOCK_CLASS_OTHERSTORE);
    }

    public static function unRegisterCatalogProductStockArrays()
    {
        Mage::unregister('catalog_product_stock_attr');
        Mage::unregister('catalog_product_stock_status_arr');
    }

    /**
     * Mock the catalog helper and put it as singleton
     * @param bool $otherStoreStockAvailable Whether stock in other stores is available.
     * @param string $textValue The default text value.
     * @return Vznl_Catalog_Helper_Data The created mock product
     */
    private function getCatalogDataHelper($otherStoreStockAvailable = true, $textValue = '')
    {
        $mock = $this->getMockBuilder('Vznl_Catalog_Helper_Data')
            ->setMethods([
                'isSellableInOtherStore',
                'getProductStockAttributeValue',
                'getStockStatusArray',
                'isDevMode',
                'getStoreId',
                'getAxiStoreId'
            ])
            ->getMock();

        $mock->method('getProductStockAttributeValue')
            ->willReturn($textValue);
        $mock->method('getStockStatusArray')
            ->willReturn([
                175 => Vznl_Catalog_Helper_Data::STOCK_CLASS_ENDOFLIFE,
                176 => Vznl_Catalog_Helper_Data::STOCK_CLASS_LIMITED,
                173 => Vznl_Catalog_Helper_Data::STOCK_CLASS_COMINGSOON,
                174 => Vznl_Catalog_Helper_Data::STOCK_CLASS_INSTOCK
            ]);
        $mock->method('isSellableInOtherStore')
            ->willReturn($otherStoreStockAvailable);
        $mock->method('isDevMode')
            ->willReturn(0);
        $mock->method('getStoreId')
            ->willReturn(0);
        $mock->method('getAxiStoreId')
            ->willReturn(0);

        return $mock;
    }

    /**
     * Checks the product if it has the given stockstatus, stockstatustext and stockstatusclass.
     * @param Vznl_Catalog_Model_Product $product The product
     * @param $stockStatus string The stockstatus.
     * @param $text string The stockstatustext.
     * @param $class string The stockstatusclass.
     * @param $assertion string The assertionmethod to use. Preferable 'assertEquals' or 'assertNotEquals'.
     */
    private function checkProductStockStatus(Vznl_Catalog_Model_Product $product, $stockStatus, $text, $class, $assertion = 'assertEquals')
    {
        $stockValues = [
            'In stock',
            'Limited stock',
            'Deliverable in other store(s)',
            'Available through backorder',
            'New (coming Soon)',
            'Sold Out',
            'End of life',
            'Long delivery time',
            ''
        ];

        $this->$assertion($stockStatus, $product->getStockStatus());
        $this->assertTrue(in_array($product->getStockStatusText(), $stockValues));
        $this->$assertion($class, $product->getStockStatusClass());
    }

    /**
     * @return Vznl_Catalog_Model_Product
     */
    private function getProduct()
    {
        return Mage::getModel('vznl_catalog/product');
    }
}
