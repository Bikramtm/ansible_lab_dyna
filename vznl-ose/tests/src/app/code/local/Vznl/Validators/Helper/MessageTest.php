<?php


namespace tests\src\app\code\local\Vznl\Validators\Helper;



use PHPUnit\Framework\TestCase;
use Vznl_Validators_Helper_Messages;

class MessageTest extends TestCase
{
    public function testGetMessage(){

        $testCases = new Vznl_Validators_Helper_Messages();

        $this->assertEquals($testCases::INVALID_POSTCODE,$this->helperFunction('nl-postcode'));
        $this->assertEquals($testCases::INVALID_DATE_FORMAT,$this->helperFunction('validate-date-age'));
        $this->assertEquals($testCases::INVALID_DATE_FORMAT,$this->helperFunction('validate-date-age'));
        $this->assertEquals($testCases::INVALID_DATE_FORMAT,$this->helperFunction('validate-date-nl'));
        $this->assertEquals($testCases::INVALID_FUTURE_DATE,$this->helperFunction('validate-date-future'));
        $this->assertEquals($testCases::INVALID_LEGITIMATION_NUMBER,$this->helperFunction('validate-legitimation-number-0'));
        $this->assertEquals($testCases::INVALID_LEGITIMATION_NUMBER,$this->helperFunction('validate-legitimation-number-1-4'));
        $this->assertEquals($testCases::INVALID_LEGITIMATION_NUMBER,$this->helperFunction('validate-legitimation-number-p-nl'));
        $this->assertEquals($testCases::INVALID_LEGITIMATION_NUMBER,$this->helperFunction('validate-legitimation-number-r'));
        $this->assertEquals($testCases::INVALID_LEGITIMATION_NUMBER,$this->helperFunction('validate-legitimation-number-n'));
        $this->assertEquals('Please enter a valid contract number.',$this->helperFunction('validate-np-dot-contract'));
        $this->assertEquals('Please enter a valid contract number.',$this->helperFunction('validate-np-contract'));
        $this->assertEquals('',$this->helperFunction('INVALID'));
    }

    protected function helperFunction($key){

        $init = (new Vznl_Validators_Helper_Messages())->getMessage($key);
        return $init;
    }
}
