<?php
use PHPUnit\Framework\TestCase;

/**
 * Class Vznl_Agent_Helper_Data_Test
 */
class Vznl_Agent_Helper_Data_Test extends TestCase
{
    /**
     * Get model instance
     *
     * @return Vznl_Catalog_Model_Product
     */
    private function getInstance()
    {
        return Mage::helper('vznl_agent/data');
    }

    /**
     * Test of isTelesalesLine for WEBSITE_TELESALES_CODE
     */
    public function testIsTelesalesLineTelesales()
    {
        $helper = $this->getAgentDataHelper(Vznl_Agent_Model_Website::WEBSITE_TELESALES_CODE);
        $websiteCode = $helper->getWebsiteCode();
        $this->assertTrue($helper->isTelesalesLine($websiteCode));
    }

    /**
     * Test of isTelesalesLine for WEBSITE_BC_WEBSHOP_CODE
     */
    public function testIsTelesalesLineBcWebshop()
    {
        $helper = $this->getAgentDataHelper(Vznl_Agent_Model_Website::WEBSITE_BC_WEBSHOP_CODE);
        $websiteCode = $helper->getWebsiteCode();
        $this->assertTrue($helper->isTelesalesLine($websiteCode));
    }

    /**
     * Test of isTelesalesLine for WEBSITE_TRADERS_CODE
     */
    public function testIsTelesalesLineTraders()
    {
        $helper = $this->getAgentDataHelper(Vznl_Agent_Model_Website::WEBSITE_TRADERS_CODE);
        $websiteCode = $helper->getWebsiteCode();
        $this->assertTrue($helper->isTelesalesLine($websiteCode));
    }

    /**
     * Test of isTelesalesLine for WEBSITE_RETAIL_CODE
     */
    public function testIsTelesalesLineRetail()
    {
        $helper = $this->getAgentDataHelper(Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE);
        $websiteCode = $helper->getWebsiteCode();
        $this->assertFalse($helper->isTelesalesLine($websiteCode));
    }

    /**
     * Test of isTelesalesLine for WEBSITE_BELCOMPANY_CODE
     */
    public function testIsTelesalesLineBelcompany()
    {
        $helper = $this->getAgentDataHelper(Vznl_Agent_Model_Website::WEBSITE_BELCOMPANY_CODE);
        $websiteCode = $helper->getWebsiteCode();
        $this->assertFalse($helper->isTelesalesLine($websiteCode));
    }

    /**
     * Test of isRetailStore for WEBSITE_RETAIL_CODE
     */
    public function testIsRetailStoreRetail()
    {
        $helper = $this->getAgentDataHelper(Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE);
        $websiteCode = $helper->getWebsiteCode();
        $this->assertTrue($helper->isRetailStore($websiteCode));
    }

    /**
     * Test of isRetailStore for WEBSITE_BELCOMPANY_CODE
     */
    public function testIsRetailStoreBelcompany()
    {
        $helper = $this->getAgentDataHelper(Vznl_Agent_Model_Website::WEBSITE_BELCOMPANY_CODE);
        $websiteCode = $helper->getWebsiteCode();
        $this->assertTrue($helper->isRetailStore($websiteCode));
    }

    /**
     * Test of isRetailStore for WEBSITE_TELESALES_CODE
     */
    public function testIsRetailStoreTelesales()
    {
        $helper = $this->getAgentDataHelper(Vznl_Agent_Model_Website::WEBSITE_TELESALES_CODE);
        $websiteCode = $helper->getWebsiteCode();
        $this->assertFalse($helper->isRetailStore($websiteCode));
    }

    /**
     * Test of isRetailStore for WEBSITE_BC_WEBSHOP_CODE
     */
    public function testIsRetailStoreBcwebshop()
    {
        $helper = $this->getAgentDataHelper(Vznl_Agent_Model_Website::WEBSITE_BC_WEBSHOP_CODE);
        $websiteCode = $helper->getWebsiteCode();
        $this->assertFalse($helper->isRetailStore($websiteCode));
    }

    /**
     * Test of isRetailStore for WEBSITE_TRADERS_CODE
     */
    public function testIsRetailStoreTraders()
    {
        $helper = $this->getAgentDataHelper(Vznl_Agent_Model_Website::WEBSITE_TRADERS_CODE);
        $websiteCode = $helper->getWebsiteCode();
        $this->assertFalse($helper->isRetailStore($websiteCode));
    }

    /**
     * Mock the agent helper and put it as singleton
     * @param string $textValue
     * @return mixed
     */
    private function getAgentDataHelper($textValue = '')
    {
        $mock = $this->getMockBuilder('Vznl_Agent_Helper_Data')
            ->setMethods([
                'getDealerData',
                'getWebsiteCode',
                'getTelesalesCodes',
                'getRetailStoreCodes'
            ])
            ->getMock();

        $mock->method('getWebsiteCode')
            ->willReturn($textValue);

        $mock->method('getTelesalesCodes')
            ->willReturn([
                Vznl_Agent_Model_Website::WEBSITE_TELESALES_CODE,
                Vznl_Agent_Model_Website::WEBSITE_BC_WEBSHOP_CODE,
                Vznl_Agent_Model_Website::WEBSITE_TRADERS_CODE
            ]);

        $mock->method('getRetailStoreCodes')
            ->willReturn([
                Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE,
                Vznl_Agent_Model_Website::WEBSITE_BELCOMPANY_CODE
            ]);

        return $mock;
    }
}