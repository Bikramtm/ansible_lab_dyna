<?php

use PHPUnit\Framework\TestCase;

class Vznl_Contract_Model_Contractjob_Test extends TestCase
{

	public function invokeMethod(&$object, $methodName, array $parameters = array())
	{
	    $reflection = new \ReflectionClass(get_class($object));
	    $method = $reflection->getMethod($methodName);
	    $method->setAccessible(true);

	    return $method->invokeArgs($object, $parameters);
	}

	public function testConstruct()
	{
		$obj = new Vznl_Contract_Model_Contractjob;
		$this->assertNull($this->invokeMethod($obj,'_construct'));
	}

	private function getInstance()
	{
		$getResMock = $this->getMockBuilder(stdClass::class)
			->setMethods([
				'deleteByJobId',
				'updateByCondition'
			])->getMock();
		$getResMock->method('deleteByJobId')->willReturn(true);
		$getResMock->method('updateByCondition')->willReturn(true);

		$jobAdapter = $this->getMockBuilder(Vznl_Contract_Model_Contractjob::class)
			->setMethods([
				'setData',
				'save',
				'getResource',
				'getTries',
				'getJobId'
			])->getMock();
		$jobAdapter->method('setData')->willReturnSelf();
		$jobAdapter->method('save')->willReturnSelf();
		$jobAdapter->method('getResource')->willReturn($getResMock);
		$jobAdapter->method('getTries')->willReturn(1);
		$jobAdapter->method('getJobId')->willReturn(1);
		return $jobAdapter;
	}

	public function testAddSftpJob()
	{
		$this->assertInstanceOf('Vznl_Contract_Model_Contractjob', $this->getInstance()->addSftpJob('path', 'filename', 'contractData'));
	}

	public function testDeleteByJobId()
	{
		$this->assertTrue($this->getInstance()->deleteByJobId(1));
	}

	public function testUpdateByCondition()
	{
		$this->assertTrue($this->getInstance()->updateByCondition(1, []));
	}

	public function testIncreaseTries()
	{
		$this->assertTrue($this->getInstance()->increaseTries());
	}

	public function testLock()
	{
		$this->assertTrue($this->getInstance()->lock());
	}

	public function testUnlock()
	{
		$this->assertTrue($this->getInstance()->unlock());
	}

	public function testSetErrorMessage()
	{
		$this->assertTrue($this->getInstance()->setErrorMessage('error'));
	}
}