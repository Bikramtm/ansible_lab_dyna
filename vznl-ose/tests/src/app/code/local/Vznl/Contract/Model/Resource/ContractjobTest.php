<?php

use PHPUnit\Framework\TestCase;

class Vznl_Contract_Model_Resource_Contractjob_Test extends TestCase
{
	public function invokeMethod(&$object, $methodName, array $parameters = array())
	{
	    $reflection = new \ReflectionClass(get_class($object));
	    $method = $reflection->getMethod($methodName);
	    $method->setAccessible(true);

	    return $method->invokeArgs($object, $parameters);
	}

	public function testConstruct()
	{
		$obj = new Vznl_Contract_Model_Resource_Contractjob;
		$this->assertNull($this->invokeMethod($obj,'_construct'));
	}

	private function getInstance()
	{
		$mockAdapter = $this->getMockBuilder(Mage_Core_Model_Abstract::class)
			->setMethods([
				'quoteInto',
				'delete',
				'update'
			])->getMock();
		$mockAdapter->method('delete')->willReturn(true);
		$mockAdapter->method('update')->willReturn(true);


		$mock = $this->getMockBuilder(Vznl_Contract_Model_Resource_Contractjob::class)
            ->setMethods([
                'getMainTable',
                '_getWriteAdapter'
            ])->getMock();
        $mock->method('getMainTable')->willReturn('tablename');
        $mock->method('_getWriteAdapter')->willReturn($mockAdapter);

        return $mock;
	}

	public function testDeleteByJobId()
	{
        $this->assertTrue($this->getInstance()->deleteByJobId(1));
	}

	public function testUpdateByCondition()
	{
		$this->assertTrue($this->getInstance()->updateByCondition(1, []));
	}
}
