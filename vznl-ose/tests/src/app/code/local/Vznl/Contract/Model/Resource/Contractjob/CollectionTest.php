<?php

use PHPUnit\Framework\TestCase;

class Vznl_Contract_Model_Resource_Contractjob_Collection_Test extends TestCase
{
	/**
      * @runInSeparateProcess
      * @preserveGlobalState disabled
      */
	public function testConstruct()
	{
		$mock = Mockery::mock('overload:Mage_Core_Model_Resource_Db_Collection_Abstract');
		$mock->shouldReceive('setConnection')->once();
		$mock->shouldReceive('_init')->once();

		$obj = new Vznl_Contract_Model_Resource_Contractjob_Collection;
		$this->assertNull($obj->_construct());
	}
}