<?php

use PHPUnit\Framework\TestCase;

/**
 * Class Dyna_Customer_Helper_Data_Test
 */
class Dyna_Customer_Helper_Data_Test extends TestCase
{
    public function testMapCustomerIDType()
    {
        $type = 'P';
        $serviceArray = $this->getMockedCustomerStub();
        $stub = $this->getMockedCustomerModel($serviceArray);
        $result = $stub->mapCustomerIDType($type);
        $this->assertEquals($type, $result);
    }

    /**
     * @param array
     * @return Dyna_Customer_Helper_Data
     */
    protected function getMockedCustomerModel($serviceArray = [])
    {
        $sessionMock = $this->getMockBuilder(Dyna_Customer_Model_Session::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'getServiceCustomers'
            ])->getMock();
        $sessionMock->method('getServiceCustomers')
            ->willReturn($serviceArray);

        $mock = $this->getMockBuilder(Dyna_Customer_Helper_Data::class)
            ->setMethods([
                'getCustomerSession'
            ])->getMock();
        $mock->method('getCustomerSession')
            ->willReturn($sessionMock);

        return $mock;
    }

    /**
     * @return array
     */
    protected function getMockedCustomerStub()
    {
        return [
            Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS => '',
            Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD => '',
            Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN => ''
        ];
    }
}
