<?php

use PHPUnit\Framework\TestCase;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Exception\ServerException;

/**
 * Class Dyna_Serviceability_Helper_Data_Test
 */
class Dyna_Serviceability_Helper_Data_Test extends TestCase
{
    private $address = array(
        'addressId' => "",
        'street' => "",
        'houseNumber' => "",
        'houseNumberAddition' => "",
        'locationId' => "",
        'postalCode' => "1111AA",
        'city' => "",
        'country' => "",
        'fullAddress' => "",
        'geoCode' =>
            [
                'latitude' => 'string',
                'longitude' => 'string',
                'geographicDatum' => 'string',
            ],
    );

    protected function getInstance()
    {
        return Mage::helper('dyna_serviceability/factory')->create();
    }

    public function testCorrectHelper()
    {
        $this->assertInstanceOf('Dyna_Serviceability_Helper_Data', $this->getInstance());
    }

    public function testInternalServerError()
    {
        $address = $this->address;
        $mock = new MockHandler([
            new Response(500, []),
        ]);
        $handler = HandlerStack::create($mock);
        $client = new Client(['handler' => $handler]);
        $serviceabilityHelper = $this->getInstance();
        $serviceabilityHelper->client = $client;
        $this->expectException(ServerException::class);
        $serviceabilityHelper->getServiceability('GET', $address['postalCode'],
            $address['houseNumber'],
            $address['houseNumberAddition'],
            $address['addressId'],
            $address['street'],
            $address['city'],
            $address['country'],
            $address['locationId']);

    }

    public function testValidServiceabilityResponse()
    {
        $address = $this->address;
        $mock = new MockHandler([
            new Response(200, [], file_get_contents(dirname(__FILE__) . '/200_2.json')),
        ]);
        $handler = HandlerStack::create($mock);
        $client = new Client(['handler' => $handler]);
        $serviceabilityHelper = $this->getInstance();
        $serviceabilityHelper->client = $client;
        $response = $serviceabilityHelper->getServiceability('GET', $address['postalCode'],
            $address['houseNumber'],
            $address['houseNumberAddition'],
            $address['addressId'],
            $address['street'],
            $address['city'],
            $address['country'],
            $address['locationId']);
        $decodedResponse = json_decode($response->getBody(), true);
        $this->assertGreaterThan(0, count($decodedResponse["data"]));
    }

    public function testInvalidServiceabilityResponse()
    {
        $address = $this->address;
        $mock = new MockHandler([
            new Response(200, [], file_get_contents(dirname(__FILE__) . '/200_1.json')),
        ]);
        $handler = HandlerStack::create($mock);
        $client = new Client(['handler' => $handler]);
        $serviceabilityHelper = $this->getInstance();
        $serviceabilityHelper->client = $client;
        $response = $serviceabilityHelper->getServiceability('GET', $address['postalCode'],
            $address['houseNumber'],
            $address['houseNumberAddition'],
            $address['addressId'],
            $address['street'],
            $address['city'],
            $address['country'],
            $address['locationId']);
        $decodedResponse = json_decode($response->getBody(), true);
        $this->assertEquals(0, count($decodedResponse["data"]));
    }

}