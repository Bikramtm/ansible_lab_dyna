<?php

use PHPUnit\Framework\TestCase;

/**
 * Class Omnius_Customer_Model_Customer_Customer_Test
 */
class Omnius_Customer_Adapter_Stub_Factory_Test extends TestCase
{
	public function testCreateMethodReturnsValidObject()
	{
		$factoryObj = $this->getInstance();
		$response = $factoryObj->create();
		$this->assertInstanceOf('Omnius_Customer_Adapter_Stub_Adapter', $response);
	}

	protected function getInstance()
    {
       return new Omnius_Customer_Adapter_Stub_Factory();
    }
}
?>