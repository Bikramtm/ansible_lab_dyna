<?php

use PHPUnit\Framework\TestCase;

/**
 * Class Omnius_Customer_Model_Customer_Customer_Test
 */
class Omnius_Customer_Adapter_Stub_Adapter_Test extends TestCase
{
    public function testProcessReturnsValidCustomerObject()
    {
        $adapterObj = $this->getMockInstance();
        $response = $adapterObj->getProfile($this->getInstanceOfCustomerModel());
        $this->assertInstanceOf('Dyna_Customer_Model_Customer', $response);
    }

    public function testMapCustomerReturnsValidCustomerObject()
    {
        $adapterObj = $this->getMockInstance();
        $response = $this->callMapCustomer($adapterObj);
        $this->assertInstanceOf('Dyna_Customer_Model_Customer', $response);
    }

    public function testMapCustomerResponseParamSoapAddressesReturnsValidAddressObject()
    {
        $adapterObj = $this->getMockInstance();
        $response = $this->callMapCustomer($adapterObj);
        $soapAddresses = $response->getSoapAddresses()[0] ?? [];
        $this->assertInstanceOf('Omnius_Customer_Model_Address', $soapAddresses);
    }

    public function testMapCustomerResponseHasValidCustomerNumber()
    {
        $adapterObj = $this->getMockInstance();
        $response = $this->callMapCustomer($adapterObj);
        $data = $response->getData();
        $customerNumber = $data['customer_number'] ?? "";
        $this->assertNotEmpty($customerNumber);
    }

    public function testMapCustomerResponseHasValidFirstName()
    {
        $adapterObj = $this->getMockInstance();
        $response = $this->callMapCustomer($adapterObj);
        $data = $response->getData();
        $firstName = $data['firstname'] ?? "";
        $this->assertNotEmpty($firstName);
    }

    public function testMapCustomerResponseHasValidBillingAccountObject()
    {
        $adapterObj = $this->getMockInstance();
        $response = $this->callMapCustomer($adapterObj);
        $data = $response->getData();
        $billingAccount = $data['billing_account'] ?? "";
        $this->assertInstanceOf('Varien_Object', $billingAccount);
    }

    public function testMapContactDataReturnsValidData()
    {
        $adapterObj = $this->getMockInstance();
        $data = $adapterObj->getStubOfModeOne();
        $customerData = $data['Customer'][0];
        $response = $adapterObj->mapContactData($customerData);
        $this->assertNotEmpty($response);
    }

    public function testMapPostalAddressDataReturnsValidData()
    {
        $adapterObj = $this->getMockInstance();
        $data = $adapterObj->getStubOfModeOne();
        $customerData = $data['Customer'][0];
        $response = $adapterObj->mapPostalAddressData($customerData);
        $this->assertNotEmpty($response);
    }

    public function testMapBillingDataReturnsValidData()
    {
        $adapterObj = $this->getMockInstance();
        $data = $adapterObj->getStubOfModeOne();
        $customerData = $data['Customer'][0];
        $response = $adapterObj->mapBillingData($customerData);
        $this->assertNotEmpty($response);
    }

    public function testMapCustomerDataReturnsValidData()
    {
        $adapterObj = $this->getMockInstance();
        $data = $adapterObj->getStubOfModeOne();
        $customerData = $data['Customer'][0];
        $bans = $contractTypes = array();
        $response = $adapterObj->mapCustomerData($customerData, $bans, $contractTypes);
        $this->assertNotEmpty($response);
    }

    public function testGetAccessorReturnsValidObject()
    {
        $adapterObj = $this->getMockInstance();
        $accessorResponse = $adapterObj->getAccessor();
        $this->assertInstanceOf('Omnius_Service_Model_DotAccessor', $accessorResponse);
    }

    public function testGetInstalledBaseReturnsValidResponse()
    {
        $adapterObj = $this->getMockInstance();
        $response = $adapterObj->getInstalledBase($this->getInstanceOfCustomerModel());
        $this->assertNotEmpty($response);
    }

    public function testMapSharingGroupReturnsValidData()
    {
        $adapterObj = $this->getInstance();
        $adapterObj->sharingGroupsDetails[] = $this->getSharingGroupMockDataProperty();
        $response = $adapterObj->mapSharingGroup($this->getSharingGroupMockData());
        $this->assertNotEmpty($response);
    }

    public function testmapCustomerOrdersDataReturnsValidDataForMobilePrepaid()
    {
        $adapterObj = $this->getInstance();
        $data = $adapterObj->getStubOfModeTwo();
        $customerData = $data['Customer'][0];
        $customerData['PendingOrder']['LineOfBusiness'] = "Mobile_Prepaid";
        $response = $adapterObj->mapCustomerOrdersData($customerData);
        $this->assertNotEmpty($response);
    }

    public function testmapCustomerOrdersDataReturnsValidDataForCable()
    {
        $adapterObj = $this->getInstance();
        $data = $adapterObj->getStubOfModeTwo();
        $customerData = $data['Customer'][0];
        $customerData['PendingOrder']['LineOfBusiness'] = "Cable";
        $response = $adapterObj->mapCustomerOrdersData($customerData);
        $this->assertNotEmpty($response);
    }

    public function testmapCustomerOrdersDataReturnsValidDataForDSL()
    {
        $adapterObj = $this->getInstance();
        $data = $adapterObj->getStubOfModeTwo();
        $customerData = $data['Customer'][0];
        $customerData['PendingOrder']['LineOfBusiness'] = "DSL";
        $response = $adapterObj->mapCustomerOrdersData($customerData);
        $this->assertNotEmpty($response);
    }

    public function testMapCustomerIfDataHasOnlyOneitem()
    {
        $adapterObj = $this->getMockInstance();
        $data = $adapterObj->getStubOfModeTwo();
        $data["Customer"] = $data["Customer"][0];
        $response = $adapterObj->mapCustomer($data, $this->getInstanceOfCustomerModel());
        $this->assertInstanceOf('Dyna_Customer_Model_Customer', $response);
    }

    public function testMapCustomerIfDataDoesNotHaveValidID()
    {
        $adapterObj = $this->getInstance();
        $data = $adapterObj->getStubOfModeTwo();
        $data["Customer"][0]["ParentAccountNumber"]["ID"] = false;
        $response = $adapterObj->mapCustomer($data, $this->getInstanceOfCustomerModel());
        $this->assertFalse($response);
    }

    public function testMapModeTwoIfDataHasOnlyOneitem()
    {
        $adapterObj = $this->getMockInstance();
        $data = $adapterObj->getStubOfModeTwo();
        $data['Customer'] = $data['Customer'][0];
        $response = $adapterObj->mapMode2($data);
        $this->assertNotEmpty($response);
    }

    public function testMapModeTwoIfSharingGroupDetailsHaveOnlyOneItem()
    {
        $adapterObj = $this->getMockInstance();
        $data = $adapterObj->getStubOfModeTwo();
        $data['Customer'][0]['SharingGroupDetails'] = $data['Customer'][0]['SharingGroupDetails'][0];
        $data['Customer'][0]['SharingGroupDetails'][0] = "";
        $response = $adapterObj->mapMode2($data);
        $this->assertNotEmpty($response);
    }

    public function testmapModeTwoWithSessionSet()
    {
        $adapterObj = $this->getMockInstanceWithSession();
        $data = $adapterObj->getStubOfModeTwo();
        $response = $adapterObj->mapMode2($data);
        $this->assertNotEmpty($response);
    }

    public function testMapCustomerDevicesReturnsValidDataWhenOnlyOneItemIsThere()
    {
        $adapterObj = $this->getMockInstance();
        $accessorResponse = $adapterObj->getAccessor();
        $data = $adapterObj->getStubOfModeTwo();
        $customerData = $data['Customer'][3];
        $devices = $accessorResponse->getValue($customerData, "Device");
        $temp = $devices[0]['Component'];
        unset($devices[0]['Component']);
        $devices[0]['Component'][0] = $temp;
        $adapterObj->accountCategory = Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN;
        $adapterObj->mapCustomerDevices($devices);
        $this->assertNotNull($adapterObj->customerDevices);
    }

    protected function getInstance()
    {
       return new Omnius_Customer_Adapter_Stub_Adapter();
    }

    protected function getInstanceOfCustomerModel()
    {
        return new Dyna_Customer_Model_Customer();
    }

    protected function callMapCustomer($adapterObj)
    {
        $data = $adapterObj->getStubOfModeOne();
        $response = $adapterObj->mapCustomer($data, $this->getInstanceOfCustomerModel());
        return $response;
    }

    protected function getMockInstanceWithSession()
    {
        $value['KIAS']['LEGACY_ID_10'] = array(
            "ID" => "GLOBAL_ID_100"
        );
        $mock = $this->getMockBuilder(Omnius_Customer_Adapter_Stub_Adapter::class)
            ->setMethods([
                'setSessionForServiceCustomers',
                'getSessionForServiceCustomers',
                'getArticleNumberAddons',
                'getsubventionCodeAddons'
            ])->getMock();
        $mock->method('setSessionForServiceCustomers')
            ->willReturn(null);
        $mock->method('getSessionForServiceCustomers')
            ->willReturn($value);
        $mock->method('getArticleNumberAddons')
            ->willReturn([]);
        $mock->method('getsubventionCodeAddons')
            ->willReturn([]);

        return $mock;
    }

    protected function getMockInstance()
    {
        $mock = $this->getMockBuilder(Omnius_Customer_Adapter_Stub_Adapter::class)
            ->setMethods([
                'setSessionForServiceCustomers',
                'getSessionForServiceCustomers',
                'getArticleNumberAddons',
                'getsubventionCodeAddons'
            ])->getMock();
        $mock->method('setSessionForServiceCustomers')
            ->willReturn(null);
        $mock->method('getSessionForServiceCustomers')
            ->willReturn(null);
        $mock->method('getArticleNumberAddons')
            ->willReturn([new Varien_Object()]);
        $mock->method('getsubventionCodeAddons')
            ->willReturn([]);

        return $mock;
    }

    protected function getSharingGroupMockData()
    {
        $mockData = array(
            "LegacyCustomerID" => "LEGACY_ID_2",
            "GroupID" => "10203030",
            "SkeletonContractNumber" => "SK11352",
            "Ban" => "BAN4563",
            "GroupTariffType" => "1234",
            "MemberType" => "M",
            "TariffOption" => "TARIFCODE",
            "EffectiveDate" => "20170221",
            "ExpirationDate" => "20170221"
        );

        return $mockData;
    }

    protected function getSharingGroupMockDataProperty()
    {
        $mockData = array(
            "LegacyCustomerID" => "LEGACY_ID_2",
            "GroupID" => "10203030",
            "GroupStatus" => "1",
            "GroupType" => "123",
            "NumberOfOwner" => "1234",
            "MemberType" => "M",
            "TariffOption" => "TARIFCODE",
            "EffectiveDate" => "20170221",
            "ExpirationDate" => "20170221",
            "NumberOfMember" => "1212"
        );

        return $mockData;
    }

    protected function getStubOfModeTwoThrowsException()
    {
         $mock = $this->getMockBuilder(Omnius_Customer_Adapter_Stub_Adapter::class)
            ->setMethods([
                'getStubOfModeTwo',
            ])->getMock();

        $mock->method('getStubOfModeTwo')
            ->willThrowException(new Exception());

        return $mock;
    }
}
