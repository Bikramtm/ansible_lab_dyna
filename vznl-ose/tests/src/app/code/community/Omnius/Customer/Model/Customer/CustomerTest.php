<?php

use PHPUnit\Framework\TestCase;

/**
 * Class Omnius_Customer_Model_Customer_Customer_Test
 */
class Omnius_Customer_Model_Customer_Customer_Test extends TestCase
{

    protected function getInstance()
    {
        return Mage::getModel('customer/customer');
    }

    public function mapSearchFields(array $requestData) : array
    {
        $data = [
            'customer_number' => $requestData['_accountNumber'],
            'firstname' => $this->mapCustomerFirstName($requestData['_party']),
            'lastname' => $this->mapCustomerLastName($requestData['_party']),
            'dob' => $requestData['_party']['_dateOfBirth'],
            'customer_status' => $requestData['_accountStatus'] === 'active' ? true : false,
            'account_category' => $requestData['_lineOfBusiness'] === 'mobile' ? 'KIAS' : $requestData['_lineOfBusiness'],
            'phone' => isset($requestData['_party']['_groupedDetails']['phone']) ?? $requestData['_party']['_groupedDetails']['phone'][0]['number'],
            'email' => isset($requestData['_party']['_groupedDetails']['email']) ?? $requestData['_party']['_groupedDetails']['email'][0]['emailAddress']
        ];
        return $data;
    }

    public function testMappingReturnsValidCustomerFirstName()
    {
        $customer  = $this->getInstance();
        $mockData = $this->getMockData();
        $data = $customer->mapSearchFields($mockData);
        $this->assertEquals($data['firstname'], 'Dorthy');

        $mockData['_party']['_name'] = 'Foo';
        $data = $customer->mapSearchFields($mockData);
        $this->assertEquals($data['firstname'], 'Foo');
    }

    public function testMappingReturnsValidCustomerLastName()
    {
        $customer  = $this->getInstance();
        $mockData = $this->getMockData();
        $data = $customer->mapSearchFields($mockData);
        $this->assertEquals($data['lastname'], 'Kutch');

        $mockData['_party']['_name'] = 'Bar';
        $data = $customer->mapSearchFields($mockData);
        $this->assertEquals($data['lastname'], 'Bar');
    }

    public function testMappingCustomerNumber()
    {
        $customer  = $this->getInstance();
        $mockData = $this->getMockData();
        $data = $customer->mapSearchFields($mockData);
        $this->assertEquals($data['customer_number'], $mockData['_accountNumber']);
    }

    public function testMappingCustomerDob()
    {
        $customer  = $this->getInstance();
        $mockData = $this->getMockData();
        $data = $customer->mapSearchFields($mockData);
        $this->assertEquals($data['dob'], $mockData['_party']['_dateOfBirth']);
    }

    public function testMappingCustomerStatus()
    {
        $customer  = $this->getInstance();
        $mockData = $this->getMockData();
        $data = $customer->mapSearchFields($mockData);
        $this->assertTrue($data['customer_status']);
    }

    public function testMappingCustomerAccountCategory()
    {
        $customer  = $this->getInstance();
        $mockData = $this->getMockData();

        $mockData['_lineOfBusiness'] = 'mobile';
        $data = $customer->mapSearchFields($mockData);
        $this->assertEquals($data['account_category'], 'KIAS'); // mobile to KIAS mapping

        $mockData['_lineOfBusiness'] = 'fixed';
        $data = $customer->mapSearchFields($mockData);
        $this->assertEquals($data['account_category'], 'fixed'); // 1 on 1 mapping
    }

    public function testMappingCustomerPhone()
    {
        $customer  = $this->getInstance();
        $mockData = $this->getMockData();
        $data = $customer->mapSearchFields($mockData);
        $this->assertEquals($data['phone'], $mockData['_party']['_groupedDetails']['phone'][0]['number']);
    }

    public function testMappingCustomerEmail()
    {
        $customer  = $this->getInstance();
        $mockData = $this->getMockData();
        $data = $customer->mapSearchFields($mockData);
        $this->assertEquals($data['email'], $mockData['_party']['_groupedDetails']['email'][0]['emailAddress']);
    }

    protected function getMockData()
    {
        $mockData = [];
        $mockData['_linkedAccounts']    = array();
        $mockData['_accountIdentifier'] = 'f2e72089-473d-4f98-8210-f796061913f2';
        $mockData['_accountNumber']     = '34501944';
        $mockData['_accountStatus']     = 'active';
        $mockData['_lineOfBusiness']    = 'mobile';
        $mockData['_roleType']          = 'default';
        $mockData['_party']             = array
            (
            '_groupedDetails' => array
            (
                'email'       => array(
                    '0' => array(
                        'emailAddress' => 'testing@dyna.com'
                    )
                ),
                'phone' => array(
                    '0' => array(
                        'number' => '1234567890',
                        'type' => 'default'
                    )
                ),
                'address'     => array(
                    '0' => array(
                        'country' => 'Kyrgyz Republic',
                        'city' => 'city',
                        'street' => 'Street',
                        'houseNumber' => '123213',
                        'district' => '12345',
                        'type' => 'default'
                    )
                ),
            ),
            '_identifier'            => '957e6997-7566-4e6b-ba31-37b40efbc75d',
            '_name'                  => '',
            '_creationTime'          => '2018-05-03T05:25:27',
            '_firstName'             => 'Dorthy',
            '_lastName'              => 'Kutch',
            '_dateOfBirth'           => '2018-05-03T05:25:27',
            '_gender'                => 'male',
            '_salutation'            => 'Mr',
        );
        return $mockData;
    }
}
