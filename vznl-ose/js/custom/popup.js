/*
 * Copyright (c) 2016. Dynacommerce B.V.
 */

var Popup = Class.create({
    initialize: function(url, params) {
        if ($('browser_window') && typeof(Windows) != 'undefined') {
            Windows.focus('browser_window');
            return;
        }

        var dialogWindow = Dialog.info(null, Object.extend({
            closable:true,
            resizable:false,
            draggable:true,
            className:'magento',
            windowClassName:'popup-window',
            title:'Popup Dialog',
            top:50,
            width:500,
            height:300,
            zIndex:1000,
            recenterAuto:false,
            hideEffect:Element.hide,
            showEffect:Element.show,
            id:'browser_window',
            onClose:function (param, el) {
            }
        }, params || {}));
        new Ajax.Updater('modal_dialog_message', url, {evalScripts: true});
    }
//    open: function() {
//        return this.name + ': ' + message;
//    }
});
