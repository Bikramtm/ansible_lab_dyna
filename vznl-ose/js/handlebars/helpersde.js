/*
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** Custom Handlebars helpers used in VFDE **/
(function (Handlebars) {
    var a = function (op, v1, v2) {
        switch (op) {
            case "==":
                return (v1 == v2);
            case "!=":
                return (v1 != v2);
            case "===":
                return (v1 === v2);
            case "!==":
                return (v1 !== v2);
            case "&&":
                return (v1 && v2);
            case "||":
                return (v1 || v2);
            case "<":
                return (v1 < v2);
            case "<=":
                return (v1 <= v2);
            case ">":
                return (v1 > v2);
            case ">=":
                return (v1 >= v2);
            default:
                return eval("" + v1 + op + v2);
        }

    };

    Handlebars.registerHelper('replace', function (find, replace, options) {
        var string = options.fn(this);
        return string.replace(find, replace);
    });

    Handlebars.registerHelper('money', function (string, options) {
        if (string === undefined) {
            return '';
        }
        if (string == false && string !== 0 && options.hash.showZero !== true) {
            return '';
        }
        var prefix = '€ ';
        if (options.hash.space === true) {
            prefix = '<span class="kias-money-space">€</span>';
        }
        var numeric = (+String(string).replace(/[,]/g, '.')).toFixed(2) || '0.00',
            parts = numeric.split('.'),
            isNegative = /^-/.test(String(string));

        if (isNegative) {
            prefix = '-€ ';
        }

        var result = [
            prefix,

            parts[0]
                .split('')
                .reverse()
                .reduce(function (acc, num, i) {
                    return num + (i && !(i % 3) ? '.' : '') + acc;
                }, ','),

            parts[1]
        ].join('');

        //	Strip out cents if not wanted.
        if (options.hash.cents === false) {
            result = result.replace(/,{3}$/, '');
        }

        return result;
    });

    Handlebars.registerHelper('dynamicPartial', function (partial, context, opts) {
        partial = partial.replace(/\//g, '_');
        var func = Handlebars.partials[partial];
        if (!func) {
            return '';
        }

        return new Handlebars.SafeString(func(context));
    });

    Handlebars.registerHelper('columnValue', function (products, raw) {
        raw = undefined != raw;
        var total = 0;
        for (var i = 0; i < products.length; i++) {
            total += parseFloat(products[i]['price']);
        }

        return raw ? Handlebars.helpers['money'](total, {hash: {}}) : total;
    });

    Handlebars.registerHelper('columnValueWithVAT', function (products, vat) {
        var total = Handlebars.helpers['columnValue'](products);
        var result = (vat / 100 * total) + total;

        return Handlebars.helpers['money'](result, {hash: {}});
    });

    Handlebars.registerHelper('math', function (lvalue, operator, rvalue, options) {
        lvalue = parseFloat(lvalue);
        rvalue = parseFloat(rvalue);

        return {
            "+": lvalue + rvalue,
            "-": lvalue - rvalue,
            "*": lvalue * rvalue,
            "/": lvalue / rvalue,
            "%": lvalue % rvalue
        }[operator];
    });

    Handlebars.registerHelper("ifComplex", function (v1, operator1, v2, operator2, v3, operator3, v4, options) {
        var eval1 = a(operator1, v1, v2);
        var eval2 = a(operator3, v3, v4);

        switch (operator2) {
            case "||":
                return (eval1 || eval2) ? options.fn(this) : options.inverse(this);
            case "&&":
                return (eval1 && eval2) ? options.fn(this) : options.inverse(this);
            default:
                return (eval("" + v1 + operator1 + v2 + operator2 + v3 + operator3 + v4) ? options.fn(this) : options.inverse(this));
        }
    });

    Handlebars.registerHelper("ifCond", function (v1, operator, v2, options) {
        return (a(operator, v1, v2) ? options.fn(this) : options.inverse(this));
    });

    Handlebars.registerHelper("ifYesNo", function (v1, options) {
        return ((v1 && (v1 != "0")) ? options.fn(this) : options.inverse(this));
    });

    Handlebars.registerHelper('assertSection', function (filterSection, options) {
        var filterOptions = filterSection.options;
        if (!filterOptions || Object.keys(filterOptions).length == 1) {
            // skip sections with only one filter option
            return '';
        }
        // pass on the context
        return options.fn(filterSection);
    });

    Handlebars.registerHelper("objectLength", function (json) {
        return Object.keys(json).length;
    });

    Handlebars.registerHelper('var', function (name, value, context) {
        this[name] = value;
    });

    Handlebars.registerHelper("debug", function (optionalValue) {
        console.log("called handlebars debugger ====================");
        console.log(this);

        if (optionalValue) {
            console.log(optionalValue, this[optionalValue]);
        }

        console.log("eof handlebars debugger ====================");
    });

    Handlebars.registerHelper('setIndexKey', function (value) {
        this.indexKey = value;
    });
    Handlebars.registerHelper('incrementVariable', function (variableName) {
        this[variableName] = this[variableName] + 1;
    });
    Handlebars.registerHelper('setVariable', function (value, variableName) {
        this[variableName] = value;
    });
    /**
     * Check if an object contains/or not a specific key and that key is not null
     */
    Handlebars.registerHelper('notHasChild', function (needle, haystack) {
        if (haystack) {
            for (i = 0; i < haystack.length; i++) {
                if (haystack[i].hasOwnProperty(needle) && haystack[i][needle] && haystack[i][needle].length > 0) {
                    return false;
                }
            }
        }
        return true;
    });

    // fix for OMNVFDE-2669
    // this will remove the space between addressNumber and addressNumberAddition
    Handlebars.registerHelper('removeAddressAdditionSpace', function (addressNumberString) {
        var newAddressNumberString = (addressNumberString != null) ? addressNumberString.replace(/\s+/g, '') : '';
        return newAddressNumberString;
    });

    Handlebars.registerHelper('compare', function (lvalue, rvalue, options) {
        if (arguments.length < 3)
            throw new Error("Handlerbars Helper 'compare' needs 3 parameters");

        var operator = options.hash.operator || "==";

        var operators = {
            '==': function (l, r) {
                return l == r;
            },
            '===': function (l, r) {
                return l === r;
            },
            '!=': function (l, r) {
                return l != r;
            },
            '<': function (l, r) {
                return l < r;
            },
            '>': function (l, r) {
                return l > r;
            },
            '<=': function (l, r) {
                return l <= r;
            },
            '>=': function (l, r) {
                return l >= r;
            },
            'typeof': function (l, r) {
                return typeof l == r;
            },
            "lastArrayElement": function (l, r) {
                return l == r + 1;
            },
            "caseInsensitiveEq": function (l, r) {
                if (l != null && l != 'undefined' && r != null && r != 'undefined') {
                    return l.toUpperCase() === r.toUpperCase();
                }
                return false;
            },
            "inArray": function (l, stringArray) {
                if (l != null && l != 'undefined' && stringArray != null && stringArray != 'undefined') {
                    if (stringArray.indexOf(',') !== -1) {
                        var array = stringArray.split(",");
                        var arrayLength = array.length;
                        for (var i = 0; i < arrayLength; i++) {
                            if (array[i].toUpperCase() == l.toUpperCase()) {
                                return true;
                            }
                        }
                        return false;
                    }
                    else {
                        return l.toUpperCase() === stringArray.toUpperCase();
                    }
                }
                return false;
            }
        };

        if (!operators[operator])
            throw new Error("Handlerbars Helper 'compare' doesn't know the operator " + operator);

        var result = operators[operator](lvalue, rvalue);

        if (result) {
            return options.fn(this);
        } else {
            return options.inverse(this);
        }
    });

    Handlebars.registerHelper('compareTwoConditions', function (lvalue, rvalue, lSecondValue, rSecondValue, options, logicalOperator) {
        if (arguments.length < 5)
            throw new Error("Handlerbars Helper 'compareTwoConditions' needs 6 parameters");

        var operator = options.hash.operator || "==";
        var logicOperatorString = options.hash.logicalOperator || "||";

        var operators = {
            '==': function (l, r, sl, sr) {
                if (logicOperatorString == "||") {
                    return (l == r || sl == sr);
                }
                if (logicOperatorString == "&&") {
                    return (l == r && sl == sr);
                }
            },
            '===': function (l, r, sl, sr) {
                if (logicOperatorString == "||") {
                    return (l === r || sl === sr);
                }
                if (logicOperatorString == "&&") {
                    return (l === r && sl === sr);
                }
            },
            '!=': function (l, r, sl, sr) {
                if (logicOperatorString == "||") {
                    return (l != r || sl != sr);
                }
                if (logicOperatorString == "&&") {
                    return (l != r && sl != sr);
                }
            },
            '<': function (l, r, sl, sr) {
                if (logicOperatorString == "||") {
                    return (l < r || sl < sr);
                }
                if (logicOperatorString == "&&") {
                    return (l < r && sl < sr);
                }
            },
            '>': function (l, r, sl, sr) {
                if (logicOperatorString == "||") {
                    return (l > r || sl > sr);
                }
                if (logicOperatorString == "&&") {
                    return (l > r && sl > sr);
                }
            },
            '<=': function (l, r, sl, sr) {
                if (logicOperatorString == "||") {
                    return (l <= r || sl <= sr);
                }
                if (logicOperatorString == "&&") {
                    return (l <= r && sl <= sr);
                }
            },
            '>=': function (l, r, sl, sr) {
                if (logicOperatorString == "||") {
                    return (l >= r || sl >= sr);
                }
                if (logicOperatorString == "&&") {
                    return (l >= r && sl >= sr);
                }
            },
            'typeof': function (l, r, sl, sr) {
                if (logicOperatorString == "||") {
                    return (typeof l == r || typeof sl == sr);
                }
                if (logicOperatorString == "&&") {
                    return (typeof l == r && typeof sl == sr);
                }
            },
            "lastArrayElement": function (l, r, sl, sr) {
                if (logicOperatorString == "||") {
                    return (l == r + 1 || sl == sr + 1);
                }
                if (logicOperatorString == "&&") {
                    return (l == r + 1 && sl == sr + 1);
                }
            }
        };
        if (!operators[operator])
            throw new Error("Handlerbars Helper 'compareTwoConditions' doesn't know the operator " + operator);

        var result = operators[operator](lvalue, rvalue, lSecondValue, rSecondValue);

        if (result) {
            return options.fn(this);
        } else {
            return options.inverse(this);
        }
    });

    Handlebars.registerHelper('json', function (context) {
        return JSON.stringify(context);
    });

    Handlebars.registerHelper('getCheckboxIsChecked', function (valueToCompare) {
        return (valueToCompare) ? 'checked' : 'unchecked';
    });

    Handlebars.registerHelper('setClassDependingOnValue', function (valueToCompare, classToAdd) {
        return (valueToCompare) ? classToAdd : '';
    });

    Handlebars.registerHelper('toLowerCase', function (value) {
        if (value && typeof value == 'string') {
            return new Handlebars.SafeString(value.toLowerCase());
        } else {
            return '';
        }
    });

    Handlebars.registerHelper('toUpperCase', function (value) {
        if (value && typeof value == 'string') {
            return new Handlebars.SafeString(value.toUpperCase());
        } else {
            return '';
        }
    });

    Handlebars.registerHelper('setLength', function (obj) {
        this.length = Object.keys(obj).length;
    });

    Handlebars.registerHelper('getDotDateFromLineDate', function (date) {
        if (date !== undefined && date !== "") {
            return date.split("-").reverse().join(".");
        }
        return "";
    });

    /**
     * If the context is an object, it will return the property given as options of that context
     */
    Handlebars.registerHelper('printPropertyIfObject', function (context, options) {
        if (Object.prototype.toString.call(context) === "[object Object]") {
            return context[options];
        } else {
            return context;
        }
    });

    Handlebars.registerHelper("formatDate", function (datetime, format, fallbackValue) {
        if (moment && datetime && (typeof datetime == "string")) {
            var DateFormats = {
                'MMDDYYYY': "MM/DD/YYYY",
                'DDMMYYYY': "DD-MM-YYYY"
            };
            // can use other formats like 'lll' too
            format = DateFormats[format] || format;

            return moment(datetime).format(format);
        } else {
            return (fallbackValue && (typeof fallbackValue != "object") && !datetime) ? fallbackValue : datetime;
        }
    });

    /**
     * OMNVFDE-3120
     * Do nothing
     *
     * @param string localAreaCode
     * @return string
     */
    Handlebars.registerHelper("showLocalAreaCode", function (localAreaCode) {
        if (localAreaCode !== undefined && localAreaCode !== "" && localAreaCode !== null) {
            return "(" + localAreaCode + ")";
        }
        return "";
    });

    Handlebars.registerHelper('capitalize', function (string) {
        string = string.toLowerCase();
        string = string.charAt(0).toUpperCase() + string.slice(1);
        return string
    });

    Handlebars.registerHelper('translate', function (value) {
        return Translator.translate(value);
    });

    Handlebars.registerHelper('updateBillingStatus', function (customerNumber) {
        jQuery(document).on('click', "#products-order-number-kias-" + customerNumber, function (event) {
            customerDe.getTotalBillingDetails(event, jQuery("#products-order-number-kias-" + customerNumber), '#totalBillingDetailsModal-' + customerNumber);
        });
    });

    Handlebars.registerHelper('ifIn', function (elem, list, options) {
        if (list.indexOf(elem) > -1) {
            return options.fn(this);
        }
        return options.inverse(this);
    });

    Handlebars.registerHelper('removeFirstXCharacters', function(numberOfChars, input) {
        if(input != null) {
            return input.substring(numberOfChars);
        } else {
            return '';
        }
    });
})(Handlebars);
