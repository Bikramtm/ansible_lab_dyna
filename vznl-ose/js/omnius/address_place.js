AddressPlace=function(selectedPakageId){
  this.map = null;
  this.geocoder = null;
  this.markersArray = [];
  this.markerLocationArray = [];
  this.locationLists = [];
  this.maspdiv='gmaps_'+selectedPakageId;
  this.dhlDivId='dhl_input_div_'+selectedPakageId;
  this.listDivId='locationList_'+selectedPakageId;
  this.paginationDivId='pagination-demo_'+selectedPakageId;
  this.selectedPakageId=selectedPakageId;
  this.nonDuplicatelocationList = [];
  this.filterShopTypeChecked = [];
  this.toggleDiv='.toggle_'+selectedPakageId;
  this.dhlInputBox = 'dhlInputBox_'+selectedPakageId;


  var self = this;

  this.initMap = function(){
    jQuery('#spinner').show();

    var vfHeadQuarters = new google.maps.LatLng(50.9334846, 6.4969757);

    this.geocoder = new google.maps.Geocoder();
    this.map = new google.maps.Map(document.getElementById(this.maspdiv), {
      zoom: 12,
      center: vfHeadQuarters
    });

    this.geocodeAddress(this.geocoder, this.map);
  };


  this.nonGoogleMap=function(){
    jQuery('[id='+this.maspdiv+']').hide();
    this.fetchPickupLocations(jQuery('[id="delivery['+this.selectedPakageId+'][pickup][store][zipcode]"]').val(), null, null);
  };

  this.geocodeAddress=function(geocoder, map) {
    var address = jQuery('[id="delivery['+this.selectedPakageId+'][pickup][store][zipcode]"]').val();
    var shopImage='../../skin/frontend/vodafone/default/img/mappin.svg';
    if(address) {
      geocoder.geocode({
        componentRestrictions: {
          country: 'DE',
          postalCode: address
        }
      }, function (results, status) {
        if (status === 'OK') {
          map.setCenter(results[0].geometry.location);

          self.fetchPickupLocations(address, results[0].geometry.location.lat, results[0].geometry.location.lng);

          self.markersArray.push(
            new google.maps.Marker({
              map: map,
              position: results[0].geometry.location,
              icon : shopImage,
              visible:false
            })
          );

        } else {
          self.fetchPickupLocations(address, null, null);
          jQuery('#spinner').hide();
        }
      });
    }
  };

  this.clearAllMarkers=function() {
    while(self.markersArray.length) {
      self.markersArray.pop().setMap(null);
    }
  };

  this.hideAllMarkers=function() {
    for(var i=0; i<self.markersArray.length; i++){
      self.markersArray[i].setMap(null);
    }
  };

  this.addAllMarkers=function(markers) {
    while(markers.length) {
      markers.setMap(map);
    }
  };

  this.addMarker=function(options) {
    if(!this.map){
      return false;
    }
    var marker = new google.maps.Marker({
      position: new google.maps.LatLng(options.shopLatitude, options.shopLongitude),
      map: this.map,
      icon: options.icon
    });

    marker.addListener('click', function(){
      //TODO: Save the user click, make item highlighted in list
    });

    var infoWindow = new google.maps.InfoWindow({
      content: options.shopName
    });

    google.maps.event.addListener(marker,'mouseover', (function(marker,infoWindow){
      return function() {
        infoWindow.open(this.map, marker);
      };
    })(marker,infoWindow));

    google.maps.event.addListener(marker,'mouseout', (function(marker,infoWindow){
      return function() {
        infoWindow.close(this.map, marker);
      };
    })(marker,infoWindow));

    return marker;
  };

  this.fetchPickupLocations = function(postcode, lat, long) {
    var params = {
      'postcode': postcode,
      'long': long,
      'lat': lat
    };
    jQuery('#divfilterbutton_'+selectedPakageId+' .multi-select-container .multi-select-menuitems').empty();
    self.filterShopTypeChecked = [];
    self.markerLocationArray = [];
    self.locationLists = [];
    jQuery('#'+self.listDivId).empty();
    jQuery('#'+self.dhlInputBox).empty();
    jQuery('#'+self.paginationDivId).hide();
    jQuery.ajax({
      url: '/checkout/cart/getPickupLocations/',
      type: 'post',
      data: params,
      dataType: 'json',
      cache: false,
      success: function (response) {
        if (response.hasOwnProperty('error') && response.error) {
          var listError =  '<div class="location-error-message" style="text-align:center;padding-left: 100px;">' +
                                        '<div class="location-error-title">' + '<label>No results found in this area <label> </div>' +
                                        '<div class="location-error-description" >' + '<p>Please provide a different postal code or select <br/> a different delivery method.<p></div>'+
                                    '</div>';
          jQuery('#'+self.listDivId).html(listError);

        } else {
          var locations = response.data;
          if(locations){
            for(var i = 0; i<locations.length; i++) {
              var shopType = locations[i].PostalAddress.PickupLocation.Type;
              var shopName = locations[i].PostalAddress.PickupLocation.Name;

              var shopStreet = locations[i].PostalAddress.StreetName;
              var shopHouseNo = locations[i].PostalAddress.StreetNumber;
              var shopPostalCode = locations[i].PostalAddress.PostalZone;
              var shopCity = locations[i].PostalAddress.CityName;
              var shopId = locations[i].PostalAddress.PickupLocation.ID;

              var shopLatitude = locations[i].PostalAddress.LocationCoordinate.LatitudeDirectionCode;
              var shopLongitude = locations[i].PostalAddress.LocationCoordinate.LongitudeDirectionCode;
              var shopDistance = locations[i].PostalAddress.Radius;

              var shopImage = '';
              var shopImage1 = '';

              switch(shopType){
              case '001' :
                shopImage = '../../skin/frontend/vodafone/default/img/dhlpackstationpin.svg';
                shopImage1 = '../../skin/frontend/vodafone/default/img/dhl-packstation.svg';
                break;
              case '002':
                shopImage = '../../skin/frontend/vodafone/default/img/dhlshoppin.svg';
                shopImage1 = '../../skin/frontend/vodafone/default/img/dhl-shop.svg';
                break;
              case '003':
                shopImage = '../../skin/frontend/vodafone/default/img/glsshoppin.svg';
                shopImage1 = '../../skin/frontend/vodafone/default/img/gls.svg';
                break;
              case '004':
                shopImage = '../../skin/frontend/vodafone/default/img/upsshoppin.svg';
                shopImage1 = '../../skin/frontend/vodafone/default/img/ups-shop.svg';
                break;
              case '005':
                shopImage = '../../skin/frontend/vodafone/default/img/vodafoneshoppin.svg';
                shopImage1 = '../../skin/frontend/vodafone/default/img/vodafone-shop.svg';
                break;
              default:
                shopImage = '../../skin/frontend/vodafone/default/img/mappin.svg';
                shopImage1 = '../../skin/frontend/vodafone/default/img/mappin.svg';
              }

              var listItem = {
                map: self.map,
                shopLatitude: shopLatitude,
                shopLongitude: shopLongitude,
                shopType: shopType,
                shopName: shopName,
                shopStreet: shopStreet,
                shopHouseNo: shopHouseNo,
                shopPostalCode: shopPostalCode,
                shopCity: shopCity,
                shopId: shopId,
                shopDistance: shopDistance,
                icon: shopImage,
                shopImage: shopImage1
              };
              self.locationLists.push(listItem);

              var marker = self.addMarker(listItem);

              self.markersArray.push(marker);
              var obj = {
                'shopId': shopId,
                'marker': marker
              };
              self.markerLocationArray.push(obj);

            }

            self.showLocationListing(self.locationLists);
            self.addDHLInputBox();
            self.addFilterOptions();
          } else {
            showModalError('No pick-up points received from service');
          }
        }
        jQuery('#spinner').hide();
      },
      error: function (xhr) {
        jQuery('#spinner').hide();
        showModalError(xhr.statusText);
      }
    });
  };

  this.updateSummary=function(element){
    var shopObject = self.locationLists[jQuery(element).data('shopindex')];

    var street = shopObject.shopStreet;
    var houseNo = shopObject.shopHouseNo;

    switch (shopObject.shopType){
    case '001':
      street = 'Packstation';
      houseNo = shopObject.shopId;
      break;
    case '002':
      street = 'Postfiliale';
      houseNo = shopObject.shopId;
      break;
    }

    jQuery('[class=\'delivery[pickup]['+self.selectedPakageId+'][map]-shop-street-summary\']').text(street);
    jQuery('[class=\'delivery[pickup]['+self.selectedPakageId+'][map]-shop-houseno-summary\']').text(houseNo);
    jQuery('[class=\'delivery[pickup]['+self.selectedPakageId+'][map]-shop-postcode-summary\']').text(shopObject.shopPostalCode);
    jQuery('[class=\'delivery[pickup]['+self.selectedPakageId+'][map]-shop-city-summary\']').text(shopObject.shopCity);
    jQuery('[class=\'delivery[pickup]['+self.selectedPakageId+'][map]-shop-name-summary\']').text(shopObject.shopName);

    jQuery('[id="delivery[pickup]['+self.selectedPakageId+'][map][street]').val(street);
    jQuery('[id="delivery[pickup]['+self.selectedPakageId+'][map][houseNo]').val(houseNo);
    jQuery('[id="delivery[pickup]['+self.selectedPakageId+'][map][shopPostalCode]').val(shopObject.shopPostalCode);
    jQuery('[id="delivery[pickup]['+self.selectedPakageId+'][map][shopCity]').val(shopObject.shopCity);
    jQuery('[id="delivery[pickup]['+self.selectedPakageId+'][map][shopName]').val(shopObject.shopName);
    if(jQuery('[id="delivery[pickup]['+self.selectedPakageId+'][map][DHLCustomerID]').val()){
      jQuery('[id="delivery[pickup]['+self.selectedPakageId+'][map][DHLCustomerID]').val(jQuery('[id="delivery[pickup]['+self.selectedPakageId+'][map][DHLCustomerID]').val());
    }

  };

  /* Sort and show the location list */
  this.showLocationListing=function(locationLists) {
    jQuery('#'+self.paginationDivId).show();
    var listHtml = '';
    var pagecount = 0;

    locationLists = this.sorting(locationLists);
    for (var j = 0; j < locationLists.length; ++j) {
      var shopAddress = locationLists[j].shopStreet + ' '+locationLists[j].shopHouseNo+ ', '+locationLists[j].shopPostalCode+' '+locationLists[j].shopCity;
      pagecount = parseInt(j/5) +1;

      listHtml +=
                '<div class="placediv toggle_'+this.selectedPakageId+' locationpage page'+this.selectedPakageId+pagecount+'"' +
                '     data-shopid="' + locationLists[j].shopId + '" ' +
                '     data-shopIndex="' + j + '" ' +
                '     data-shoptype="' + locationLists[j].shopType + '" >' +
                '   <div>' +
                '       <img class="placeimageD" src="'+ locationLists[j].shopImage+'"/>' +
                '   </div>' +
                '   <div class="location-address" >' +
                '       <div style="display: block; font-weight: Bold;">' +
                '           <span>' + locationLists[j].shopName+'</span> ' +
                '       </div>' +
                '       <div style="display: block;">' +
                '           <p>'+shopAddress+'</p>' +
                '       </div>' +
                '   </div>' +
                '   <div class="distance">' +
                '       <span>'+ locationLists[j].shopDistance+' KM</span>' +
                '   </div>' +
                '   <button class="select-button" style="" type="button"> <span class="select-txt">Select></span> </button>' +
            ' </div>';
    }
    jQuery('#'+self.listDivId).html(listHtml);

    var toggleDiv='.toggle_'+self.selectedPakageId;
    window.pagObj = jQuery('#'+this.paginationDivId).twbsPagination({
      totalPages: pagecount,
      startPage: 1,
      visiblePages: 5,
      first: 'First',
      prev: '<img class="shape" src="../../skin/frontend/vodafone/default/img/prev.svg">',
      next: '<img class="shape" src="../../skin/frontend/vodafone/default/img/next.png">',
      anchorClass: 'page-link',
      activeClass: 'active pagination-page-active',
      onPageClick: function (event, page) {
        jQuery(toggleDiv).removeClass('location-page-active ');
        jQuery('.page'+self.selectedPakageId+page).addClass('location-page-active');
        jQuery(toggleDiv).removeClass('toggle-active');
        jQuery(toggleDiv).find('.select-button').attr('style','background-color:none');
        jQuery(toggleDiv).find('.select-txt').html('Select');
        jQuery(toggleDiv).find('.select-txt').attr('style','color:#009ca7');

        var dhlDiv = jQuery('#'+self.dhlDivId);
        dhlDiv.addClass('hidden');
        dhlDiv.find('[id="delivery[pickup]['+self.selectedPakageId+'][map][DHLCustomerID]"]').removeClass('required-entry').val('').attr('placeholder','DHL Customer ID (Optional)');

      }
    });
  };

  /* Sort the result based on distance descending order */
  this.sorting=function(data) {
    return data.sort(function(a, b) {
      return parseFloat(a.shopDistance) - parseFloat(b.shopDistance);
    });
  };

  this.removeDHLInputBox=function(){
    var dhlDiv = jQuery('#'+this.dhlInputBox);
    jQuery('#'+self.dhlInputBox).empty();

  };

  this.addDHLInputBox = function () {
    var divHtml =   '<div id="dhl_input_div_'+self.selectedPakageId+'" class="dhlIdentificationdiv hidden" ' +
                        '   <div class="dhlIdentification-box">' +
                        '       <div class="input-background">' +
                        '           <label for="dhlIdentification"></label>' +
                        '               <input type="text" id="delivery[pickup]['+self.selectedPakageId+'][map][DHLCustomerID]" ' +
                        '                           name="delivery[pickup]['+ self.selectedPakageId + '][map][DHLCustomerID]" ' +
                        '                           class="form-control input-sm dhl-input-box" value="" placeholder="DHL Customer ID (Optional)" />' +
                        '      </div>' +
                        '   </div>' +
                        '</div>';

    jQuery('#'+self.dhlInputBox).html(divHtml);
  };

  /* add filter options */
  this.addFilterOptions=function() {
    var popupOptionsHtml = '';
    self.nonDuplicatelocationList = this.removeDuplicate(self.locationLists);

    for(var i=0; i<self.nonDuplicatelocationList.length; i++){
      popupOptionsHtml +=  '<div class="multi-select-menuitem "> ' +
                '<span class="Rectangle filter-box-checked"> </span> ' +
                    '<span value="' +self.nonDuplicatelocationList[i].shopType+ '" class="multi-select-item-text">'+self.nonDuplicatelocationList[i].shopName + '</span>' +
                '</div>' ;
      self.filterShopTypeChecked.push(self.nonDuplicatelocationList[i].shopType);
    }

    jQuery('#divfilterbutton_'+self.selectedPakageId+' .multi-select-container .multi-select-menuitems').html(popupOptionsHtml);
  };


  this.removeDuplicate=function(locationList){
    if(!locationList.length) {
      return [];
    }
    var nonDuplicatelocationList = [];
    var arrResult = {};
    for (var i = 0, n = locationList.length; i < n; i++) {
      var item = locationList[i];
      arrResult[ item.ShopName + ' - ' + item.shopType ] = item;
    }
    var i = 0;
    for(var item in arrResult) {
      nonDuplicatelocationList[i++] = arrResult[item];
    }
    return nonDuplicatelocationList;
  };

  this.search = function () {
    var res = Validation.validate(document.getElementById('delivery['+self.selectedPakageId+'][pickup][store][zipcode]'));
    if(res){
      jQuery('#spinner').show();
      self.clearAllMarkers();
      jQuery('#'+self.listDivId).empty();
      jQuery('#'+self.dhlInputBox).empty();
      self.locationLists = [];
      jQuery('#'+self.paginationDivId).twbsPagination('destroy');
      if(self.map){
        self.map.setZoom(12);
        self.geocodeAddress(self.geocoder, self.map);
      } else {
        self.fetchPickupLocations(jQuery('[id="delivery['+self.selectedPakageId+'][pickup][store][zipcode]"]').val(), null, null);
      }
    }
    return res;

  };

  jQuery(document).on('click','#divfilterbutton_'+self.selectedPakageId+' .buttonbounds', function () {
    if(jQuery('#divfilterbutton_'+self.selectedPakageId +' .multi-select-container').hasClass('multi-select-container-open')){
      jQuery('#divfilterbutton_'+self.selectedPakageId +' .multi-select-container').removeClass('multi-select-container-open');
    } else{
      jQuery('#divfilterbutton_'+self.selectedPakageId +' .multi-select-container').addClass('multi-select-container-open');
    }
  });

  jQuery(document).on('click','#divfilterbutton_'+self.selectedPakageId+' .multi-select-menuitem', function (event) {
    var element = jQuery(this).find('span.Rectangle');
    var value = jQuery(this).find('span.multi-select-item-text').attr('value');
    if(element.hasClass('filter-box-checked')) {
      element.removeClass('filter-box-checked');

      // search and remove the element from array
      for (var i=self.filterShopTypeChecked.length-1; i>=0; i--) {
        if (self.filterShopTypeChecked[i] === value) {
          self.filterShopTypeChecked.splice(i, 1);
          break;
        }
      }
    } else {
      element.addClass('filter-box-checked');
      self.filterShopTypeChecked.push(value);
    }

    // reset the map and markers on it
    self.hideAllMarkers();
    jQuery('#'+self.listDivId).empty();
    jQuery('#'+self.paginationDivId).twbsPagination('destroy');
    if(!self.filterShopTypeChecked.length){
      return false;
    }

    var filterLocationLists = self.locationLists.filter(function(v) {
      return self.filterShopTypeChecked.indexOf(v.shopType) > -1;
    });

    if(self.map) {
      /* Add filtered marker on map */
      for (var i = 0; i < filterLocationLists.length; i++) {
        var markerObj = self.markerLocationArray.find(item => item.shopId == filterLocationLists[i]['shopId']);
        var marker = markerObj.marker;
        marker.setMap(self.map);
      }
    }
    /* Update location listing*/
    self.showLocationListing(filterLocationLists);
  });

  jQuery(document).on('click',self.toggleDiv, function (event) {
    var currentElement = jQuery(this);
    // self.removeDHLInputBox();
    var dhlDiv = jQuery('#'+self.dhlDivId);
    dhlDiv.addClass('hidden');
    dhlDiv.find('[id="delivery[pickup]['+self.selectedPakageId+'][map][DHLCustomerID]"]').removeClass('required-entry').val('').attr('placeholder','DHL Customer ID (Optional)');

    if(currentElement.hasClass('toggle-active')){
      currentElement.removeClass('toggle-active');
      currentElement.find('.select-button').attr('style','background-color:none');
      currentElement.find('.select-txt').html('Select');
      currentElement.find('.select-txt').attr('style','color:#009ca7');
      jQuery('[name="delivery['+self.selectedPakageId+'][pickup][store_id]"]').val('');
      jQuery('[id="delivery[pickup]['+self.selectedPakageId+'][map][street]"]').val('');
      jQuery('[id="delivery[pickup]['+self.selectedPakageId+'][map][houseNo]"]').val('');
      jQuery('[id="delivery[pickup]['+self.selectedPakageId+'][map][shopPostalCode]"]').val('');
      jQuery('[id="delivery[pickup]['+self.selectedPakageId+'][map][shopCity]"]').val('');
      jQuery('[id="delivery[pickup]['+self.selectedPakageId+'][map][shopName]"]').val('');

    } else{
      jQuery(self.toggleDiv).removeClass('toggle-active');
      jQuery(self.toggleDiv).find('.select-button').attr('style','background-color:none');
      jQuery(self.toggleDiv).find('.select-txt').html('Select');
      jQuery(self.toggleDiv).find('.select-txt').attr('style','color:#009ca7');

      currentElement.addClass('toggle-active');
      currentElement.find('.select-button').attr('style','background-color:Teal');
      currentElement.find('.select-txt').html('Selected');
      currentElement.find('.select-txt').attr('style','color:white');
      if(jQuery.inArray(currentElement.attr('data-shoptype'), ['001','002']) !== -1){
        var dhlDiv = jQuery('#'+self.dhlDivId);
        dhlDiv.removeClass('hidden');
        dhlDiv.removeClass('required-entry');
        if(currentElement.attr('data-shoptype') === '001') {
          dhlDiv.find('[id="delivery[pickup]['+self.selectedPakageId+'][map][DHLCustomerID]"]').addClass('required-entry').attr('placeholder','DHL Customer ID');
        }
        dhlDiv.insertAfter(currentElement);
      }
      jQuery('[name="delivery['+self.selectedPakageId+'][pickup][store_id]"]').val(currentElement.attr('data-shopid'));

      addressPlace.updateSummary(currentElement);
    }
  });

  jQuery(document).on('mouseover',self.toggleDiv, function (event) {
    if(self.map) {
      test = event;
      /* highlight the respective marker*/
      var shopId = jQuery(this).attr('data-shopid');
      var markerObj = self.markerLocationArray.find(item => item.shopId == shopId);
      var marker = markerObj.marker;
      google.maps.event.trigger(marker, 'mouseover');
    }
  });

  jQuery(document).on('mouseout',self.toggleDiv, function (event) {
    if(self.map) {
      /* remove the highlighted marker*/
      var shopId = jQuery(this).attr('data-shopid');
      var marker = self.markerLocationArray.find(item => item.shopId == shopId).marker;
      google.maps.event.trigger(marker, 'mouseout');
    }
  });

  /* search on click of search icon */
  jQuery('[id="delivery['+self.selectedPakageId+'][pickup][store][search]"]').click(function(){
    self.search();
  });

  /* search on pressing enter button */
  jQuery('#google_maps_form_'+self.selectedPakageId).submit(function(){
    event.preventDefault();
    self.search();
  });

};




AddressDateTime = function(selectedPakageId, isSohoCustomer) {

  this.selectedPakageId = selectedPakageId;
  this.isSohoCustomer = isSohoCustomer;

  /* fetchAvailableShippingOptions function call */
  this.fetchAvailableShippingOptions = function() {
    var params = {
      'postcode': 50189
    };
    jQuery.ajax({
      url: '/checkout/cart/getAvailableShippingOptions/',
      type: 'post',
      data: params,
      dataType: 'json',
      cache: false,
      success: function (response) {
        if (response.hasOwnProperty('error') && response.error) {
          showModalError(response.message);
        } else {
          console.log(response);
        }
      },
      error: function (xhr) {
        jQuery('#spinner').hide();
        showModalError(xhr.statusText);
      }
    });
  };
};
