jQuery(document).on('configurator.initialized', function () {
  var selectedBundleProducts = jQuery('.bundle-choice-wrapper').attr('data-bundle-selected-ids');
  if(selectedBundleProducts) {
    selectedBundleProducts = selectedBundleProducts.split(',');
    if (selectedBundleProducts.length) {
      jQuery.each(selectedBundleProducts, function (index, el) {
        jQuery('.row.item-row.selected[data-id="'+el+'"]').addClass('bundle-choice-selected');
      })
    }
  }
})


function toggleNotificationPanel(containerSelector) {
  var $container = jQuery(containerSelector);
  if ($container.hasClass('open')) {
    $container.find('.notification-content').slideUp(function () {
      configurator.assureElementsHeight();
      $container.find('.slide-up').removeClass('slide-up').addClass('slide-down');
    });
    $container.removeClass('open');
  } else {
    $container.find('.notification-content').slideDown(function () {
      configurator.assureElementsHeight();
      $container.find('.slide-down').removeClass('slide-down').addClass('slide-up');
    });
    $container.addClass('open');
  }
}

function saveBundleChoice(selector) {
  var $container = jQuery(selector);
  var bundleId = $container.attr('data-bundle-id');
  var choices = {};
  var selectedIds = [];
  if($container.length) {
    var $selectedItems = $container.find('input:checked');
    $selectedItems.each(function() {
      choices[jQuery(this).attr('name')] = jQuery(this).attr('value');
      selectedIds.push(jQuery(this).attr('data-id'));
    })
  }
  var parent_class = '.side-cart';
  var $ = jQuery;
  var isCheckout = (arguments.length == 3 && arguments[2] != 0);
  var additional_param = '';

  setPageLoadingState(true);
  jQuery.post(MAIN_URL + 'bundles/index/updateBundleChoice', {products: choices, bundleId: bundleId}, function(response) {
    window.setPageLoadingState && window.setPageLoadingState(true);
    window.setPageOverrideLoading(true);
    if(response.error == true) {
      showModalError(response.message);
      setPageLoadingState(false);
      $('.bundle-choice-wrapper').find(':radio').removeAttr('checked');
      if(response.currentBundleChoices && $.isArray(response.currentBundleChoices)){
        $('.bundle-choice-wrapper').find(':radio[data-id='+response.currentBundleChoices[0]+']').prop('checked',true);
      }
    } else {
      if (isCheckout) {
        additional_param = ', true';
        $('.checkout-totals #cart_totals').replaceWith(response.totals);
        if (typeof response.quoteTotals != 'undefined' && response.quoteTotals) {
          window['quoteGrandTotal'] = response.quoteTotals.quoteGrandTotal.rawValue;
          $('.quoteGrandTotal').text(response.quoteTotals.quoteGrandTotal.formatted);
          $('.split-payment-method[data-package-id="{0}"]'.format(package_id)).find('.amount').text(response.quoteTotals.packageTotal);
          window['checkout'].ignoreOffer = true;
          $('[name="customer[type]"]:checked').trigger('click');
          window['checkout'].ignoreOffer = false;
        }
      }
      $('.sticker #cart_totals').replaceWith(response.totals);
      $('.expanded-shoppingcart-totals #cart_totals').replaceWith(response.totals);

      if (parent_class == '.side-cart' && $('.side-cart').length > 0) {
        $('.side-cart[data-package-id="'+response.activePackageId+'"]').replaceWith(response.rightBlock);
      }
      if (parent_class == '.package-expand' && $('#right-panel-cart').length > 0) {
        $('#right-panel-cart').replaceWith(response.rightBlock);
        $('.cart_packages div[data-package-id="' + package_id + '"]').replaceWith(response.rightBlockCollapsed);
        //$('div[data-package-id]').first().replaceWith(response.rightBlockCollapsed);
        $('#right-panel-cart').show();
      }
      var productId = null;
      jQuery('.bundle-choice-selected').removeClass('bundle-choice-selected selected');
      configurator.bundleChoiceSelectedIds = selectedIds;
      jQuery.each(selectedIds, function(index, value) {
        if(!jQuery('.product-table [data-id=' + value + ']').hasClass('selected')) {
          jQuery('.product-table [data-id=' + value + ']').addClass('selected bundle-choice-selected');
        }
      });
      configurator.reapplyRules(true);
    }
  });
}


jQuery(document).ready(function(){

});
