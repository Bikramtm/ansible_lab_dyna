/*
 * Copyright (c) 2016. Dynacommerce B.V.
 */

(function ($, win) { //SimCardChecker
  function SimCardChecker(options) {
    this.options = options || {};
  }
  /**
     * check if it's ok to proceed to the extended cart (no simonly without sim}
     * @param {boolean} ignoreAlert show alert
     * @return {boolean}
     */
  SimCardChecker.prototype.isSimOnlyWithoutSim = function(ignoreAlert) {
    ignoreAlert = ignoreAlert || false;
    var activePackage = $('.side-cart.active');

    if (
      typeof window.configurator != 'undefined'
            && activePackage.length > 0
            && activePackage.data('package-type') == 'mobile'
            && typeof window.configurator.cart['subscription'] != 'undefined'
            && window.configurator.simType == null
            && !$('.sim-select .radio.sim-old input').is(':checked')
    )
    {

      if (!ignoreAlert){
        $('#simonly-modal')
          .find('.message').html('{0} <br/>'.format(
            Translator.translate('Please select a Sim Card or the \'Using current SIM\' option for the current package')
          )).end().modal();
      }

      return false;
    }

    return true;
  };

  /**
     * checks if the added product is simonly without sim
     * @param product
     * @param hasSim
     * @param id
     */
  SimCardChecker.prototype.checkPackageSubscription = function(product, hasSim, id) {
    if (product.hasOwnProperty('sim_only')) {
      if (!hasSim && product['sim_only']) {
        // doesn't have a sim and is simOnly
        $('.active.side-cart[data-package-id="' + id + '"]').attr('data-simonly', product.name);
      } else {
        $('.active.side-cart[data-package-id="' + id + '"]')
          .removeAttr('data-simonly')
        ;
      }
    }
  };

  SimCardChecker.prototype.setUseMySim = function(packageId) {
    $('.active.side-cart[data-package-id="' + packageId + '"]')
      .removeAttr('data-simonly')
    ;
  };

  win.simCardChecker = new SimCardChecker();
})(jQuery, window); // END SimCardChecker

function applyPromoRule(remove, url) {
  var id;

  if(remove) {
    id = '-1'; // if id is -1 the all the promo rules are removed from cart
  } else {
    id = jQuery('input[name="rule"]:checked').val();
  }

  jQuery.post(url, {id: id}, function(response) {
    if(response.error == false){
      if(id != '-1') {
        togglePromoRules('close');
      } else {
        jQuery('[name=rule]').attr('checked', false);
        jQuery('#remove-promo-rules').hide();
      }
      if(response.totals){
        var totals = jQuery('#cart_totals');
        if(response.hasOwnProperty('totals')) totals.replaceWith(response.totals);
      }

      var activePackage = jQuery('div[data-package-id].active:visible');
      if(response.rightBlock && activePackage.length > 0){
        activePackage.first().replaceWith(response.rightBlock);
      }
    }
  });
}

function hidePopover(id) {
  var p = jQuery('.dot-'+id);
  if (p.length) {
    p.popover('hide');
  }
}

function showProductModal(event, id){
  event.stopPropagation();
  var self = window.configurator;
  var product = self.products['device'].filter(function(el) { return el['entity_id'] == id;}).first();


  self.http.ajax('GET', 'cart.getPopup', {
    'website_id': self.websiteId,
    'product_id': id
  }, true).done(function(data) {
    if (data.hasOwnProperty('error')) {
      showModalError(data.message);
      return;
    }

    product = jQuery.extend(product, data);

    var template = self.getProductPopupTemplate();
    var modal = jQuery(Handlebars.compile(template, {noEscape: true})(product));
    modal
      .find('[data-toggle=tab]').on('click',function(e){
        e.preventDefault();
        jQuery(this).tab('show');
      })
      .end()
      .modal()
      .on('hidden.bs.modal', function () {
        // destroy on close;
        var el = jQuery(this);
        el
          .data('bs.modal', null)
          .remove()
        ;
      })
    ;
  });

  return false;
}
