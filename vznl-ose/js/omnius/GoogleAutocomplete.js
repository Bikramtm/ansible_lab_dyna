'use strict';

(function($){

  window.GoogleAutocomplete = function(params) {
    this.field = params.field;
    this.container = params.container;
    this.placeType = params.placeType ? params.placeType : ['address'];
    this.mappingGoogleResponse = params.mappingGoogleResponse ? params.mappingGoogleResponse : {
      street_number: 'houseNo',
      route: 'street',
      locality: 'city',
      postal_code: 'postcode'
    };
    this.autocompleteObject = null;
    this.bounds = null;
    this.services = null;

    this.initialize = function () {
      var self = this;
      google.maps.event.addDomListener(window, 'load', function () {
        self.initAutocomplete(self);
      });
      return self;
    };
    /**
         * Method callback from google
         */
    this.initAutocomplete = function (self) {
      var searchInput  = $('#' + self.container).find('input[name="' + self.field + '"]')[0];
      ///Create the autocomplete object, restricting the search to the specified type
      self.autocompleteObject = new google.maps.places.Autocomplete((searchInput),{
        types: self.placeType.constructor === Array ? self.placeType : [self.placeType],
        strictBounds : true
      });
      // When the user selects an address from the dropdown, populate the address fields in the form.
      self.autocompleteObject.addListener('place_changed',function(){
        // Get the place details from the autocomplete object.
        self.fillInAddress(self);
      });
      if(address.restrictedCountries){
        var countries = address.restrictedCountries.split(',');
        self.autocompleteObject.setComponentRestrictions({
          country: countries
        });
      }
    };

    this.fillInAddress = function (self) {
      var serviceData = self.autocompleteObject.getPlace();

      var southWestLat = serviceData.geometry.viewport.getSouthWest().lat();
      var southWestLng = serviceData.geometry.viewport.getSouthWest().lng();

      var northEastLat = serviceData.geometry.viewport.getNorthEast().lat();
      var northEastLng = serviceData.geometry.viewport.getNorthEast().lng();

      var southWest = new google.maps.LatLng(southWestLat,southWestLng);
      var northEast = new google.maps.LatLng(northEastLat,northEastLng);

      self.bounds = new google.maps.LatLngBounds(southWest,northEast);

      if(serviceData.hasOwnProperty('address_components')){
        var dataOutput = serviceData.address_components;
        var fields = $('#' + self.container +' input:not([name="' + self.field + '"])');

        fields.val('');
        for (var i = 0; i < dataOutput.length; i++) {
          var addressType = dataOutput[i].types[0];

          if (self.mappingGoogleResponse[addressType] && $.inArray(self.mappingGoogleResponse[addressType], fields ) ) {
            $('#' + self.container + ' input[name="' + self.mappingGoogleResponse[addressType] + '"]').val(dataOutput[i]['long_name']);
          }
        }

        for(var key in self.services) {
          self.services[key].updateBounds(self.bounds);
        }
      }
    };

    this.updateBounds = function (newBounds) {
      var self = this;
      self.autocompleteObject.setBounds(newBounds);
    };
  };

})(jQuery);
