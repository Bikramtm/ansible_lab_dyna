/*
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** Disable prototype plugins that affect jQuery / bootstrap / etc functionality **/
if (Prototype.BrowserFeatures.ElementExtensions) {
    var disablePrototypeJS = function (method, pluginsToDisable) {
            var handler = function (event) {
                event.target[method] = undefined;
                setTimeout(function () {
                    delete event.target[method];
                }, 0);
            };
            pluginsToDisable.each(function (plugin) {
                jQuery(window).on(method + '.bs.' + plugin, handler);
            });
        },
        pluginsToDisable = ['collapse', 'dropdown', 'modal', 'tooltip', 'popover', 'tab'];
    disablePrototypeJS('show', pluginsToDisable);
    disablePrototypeJS('hide', pluginsToDisable);
}

jQuery(document).ready(function($) {
    $( document ).on( 'focus', ':input', function(){
        $( this ).attr( 'autocomplete', 'off' );
    });
    // Initialize tooltips
    $('[data-toggle="tooltip"]').tooltip();

    $('form#login-form').submit(function(){
        $(this).find(':input[type=submit]').prop('disabled', true);
    });
    $('form#login-form input').focus(function() {
        $('form#login-form').find(':input[type=submit]').prop('disabled', false);
    });
    $('form#login-form input').blur(function() {
        $('form#login-form').find(':input[type=submit]').prop('disabled', false);
    });
    if (window['store'] && window['store']['cleanup']) {
        window['store']['cleanup']();
    }

    window.leavePageDialogSwitcher = false;
    window.checkoutDeliverPageDialog = false;

    var theBody         = $('body');
    var footerModals    = $('#footer_modals');
    var htmlDOM         = $('html');
    var popoverClass    = $('.popover');

    $('.customer_search_popover').popover({
        placement: 'right',
        html: true,
        container: 'body',
        trigger: 'click',
        content: function () {
            return $('#search_info_text_popover_content').html();
        }
    }).on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
        $(this).show();
        if (!$('.popover').hasClass('parsed')) {
            $('.popover').css('top', '10px').css('z-index', 20001).css('width', 300 + 'px').addClass('parsed');
        } else {
            $('.popover').removeClass('parsed');
        }
    });

    if($('.side-cart.block .block-container .content.one-off-deal-package').length > 0){
        jQuery('.buttons-container #package-types').addClass('hide');
        $('#show-packages').addClass('hide');
    }

    htmlDOM.on('click', function () {
        if ($('.popover').length) {
            $('.customer_search_popover').popover('hide').show();
            $('.popover').removeClass('parsed');
        }
    });
    var topTabs = $('#top-tabs a');
    if (topTabs.length) {
        $('#top-tabs a').click(function (e) {
            e.preventDefault();
            $(this).tab('show');
        });
        $('#top-tabs a:first').tab('show');
    }

    /*==============*** AGENT DATA INIT ***==============*/
    window.agent = new Agent({
        baseUrl: MAIN_URL
        ,spinner: MAIN_URL + 'skin/frontend/omnius/default/images/spinner.gif'
    });
    $.each({
        'agent.loginOther':'agent/account/loginAsAgent',
        'agent.logoutOther':'agent/account/logoutAsAgent',
        'agent.logout':'agent/account/logout',
        'agent.changePass':'agent/account/changeAgentPassword',
        'agent.list':'agent/account/getAgentsList',
        'agent.switchRole':'agent/account/switchAgentRole',
        'agent.leaveRole':'agent/account/leaveAgentRole'
    }, function(key, url) {
        window.agent.addEndpoint(key, url);
    });

    /*==============*** CUSTOMER DATA INIT ***==============*/
    window.customer = new Customer({
        baseUrl: MAIN_URL
        ,spinner: MAIN_URL + 'skin/frontend/omnius/default/images/spinner.gif'
    });
    $.each({
        'customer.search':'customer/search/index',
        'customer.search.indirect':'customer/search/indirect',
        'customer.load':'customer/details/loadCustomer',
        'customer.household':'customer/details/loadHousehold',
        'customer.householdproducts': 'customerde/details/loadHouseholdProducts',
        'customer.sticker':'customer/details/getSticker',
        'customer.logout':'customer/details/unloadCustomer',
    }, function(key, url) {
        window.customer.addEndpoint(key, url);
    });
    window.customer.init();

    /*==============*** CUSTOMER-DE DATA INIT ***==============*/
    window.customerDe = new CustomerDe({
        baseUrl: MAIN_URL
        , spinner: MAIN_URL + 'skin/frontend/vodafone/default/images/spinner.gif'
    });

    $.each({
        'customer.search': 'customerde/search/getCustomerResults',
        'customer.searchLegacy': 'customerde/search/getCustomerResultsLegacy',
        'customer.searchExact': 'customerde/search/getCustomerResultsExact',
        'customer.load': 'customerde/details/loadCustomer',
        'customer.household': 'customerde/details/loadHousehold',
        'customer.householdproducts': 'customerde/details/loadHouseholdProducts',
        'customer.showRetainable': 'customerde/details/showRetainable'
    }, function (key, url) {
        window.customerDe.addEndpoint(key, url);
    });
    window.customerDe.init();

    /*==============*** SEARCH DATA INIT ***==============*/
    window.search = new Search({events: 'change keyup'});

    theBody.on('click', '.enlargeSearch', function(event) {
        if($('[id="customer[logged]"]').val() == 'false' && typeof checkout !== 'undefined') {
            return;
        }
        if(customerDe.targetPanelId == 'products-content' && !$(event.target).hasClass('expanded')) {
            $('.customer_ctns').find('a#product_info_left').trigger('click');
            return;
        }
        LeftSidebar.toggleLeftSideBar();
    });

    function hideErrorMessages(elem) {
        // fix for identity number error message not hiding when valid input on change identity type
        if ( $(elem).val() != ''
            && Validation.validate(document.getElementById(elem.attr('id'))) === true
        ) {
            $(elem).parent().find('.validation-advice').hide('slow');
        } else if ($(elem).val() == '') {
            $(elem).parent().find('.validation-advice').hide('slow');
        }
    }

    //force correct date format for all fields with the class "date-format"
    theBody.on( "input propertychange", ".date-format", function() {
        var el = $(this);
        el.prop('maxlength', 10);

        var val = $.trim(el.val()).replace(/[^\d\.]/g, '');
        if (val !== $.trim(el.val())) {
            el.val(val);
        }
        var digits = val.replace(/[^\d]/g, '');
        if (digits.length === 8) {
            var formattedDate = moment(digits, 'DDMMYYYY').format('DD.MM.YYYY');
            if (formattedDate !== 'Invalid date') {
                el.val(formattedDate);
                // trigger change event manually for the number porting cancellation date field
                if (el.hasClass('porting-cancellation-date') || el.hasClass('porting-delivery-date')) {
                    el.trigger('change');
                }
                hideErrorMessages(el);
            }
        }
    });


    //force correct date format for all fields with the class "date-format-no-day"
    theBody.on( "input propertychange", ".date-format-no-day", function() {
        var el = $(this);
        el.prop('maxlength', 7);

        var val = $.trim(el.val()).replace(/[^\d\.]/g, '');
        if (val !== $.trim(el.val())) {
            el.val(val);
        }
        var digits = val.replace(/[^\d]/g, '');
        if (digits.length === 6) {
            var formattedDate = moment(digits, 'MMYYYY').format('MM.YYYY');
            if (formattedDate !== 'Invalid date') {
                el.val(formattedDate);
                hideErrorMessages(el);
            }
        }
    });

    //force correct time format for all fields with the class "time-two-digit-format"
    theBody.on( "input propertychange", ".time-format", function() {
        var el = $(this);
        el.prop('maxlength', 5);

        var val = $.trim(el.val()).replace(/[^\d\:]/g, '');
        if (val !== $.trim(el.val())) {
            el.val(val);
        }
        var digits = val.replace(/[^\d]/g, '');
        if (digits.length === 4) {
            var formattedTime = moment(digits, 'hhss').format('HH:ss');
            if (formattedTime !== 'Invalid date') {
                el.val(formattedTime);
                // trigger change event manually for the number porting cancellation date field
                el.trigger('change');

                hideErrorMessages(el);
            }
        }
    });

    theBody.on("click", '.top-menu-buttons .btn',function (e) {
        $('.top-menu-buttons .btn.active').removeClass('active');
        $(this).addClass('active');
    });

    theBody.on('submit', '.loginOtherUserForm',function(e){
        e.preventDefault();
        loginOtherUser();
    });

    theBody.on('click','#home-tabs a',function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

    $('[data-slidebutton-top]').slidepanel({
        orientation: 'top',
        mode: 'overlay',
        static: true,
        container: '#top-panels',
        before_expand: function(elem) {
            $('#top-panels').show();
            $('body').addClass('toPrint');
        },
        after_collapse_callback: function() {
            $('#top-panels').hide();
            $('body').removeClass('toPrint');
        }
    });

    theBody.on('click', '#one-off-deal', function(e) {
        e.preventDefault();
        if ($('.col-right.sidebar .sticker .cart_packages .side-cart:not([data-package-type="template"])').length > 0) {
            $('#create-one-of-deal-modal').modal();
        } else {
            createOneOffDeal();
        }
    });

    theBody.on("click", '.user-menu-buttons .btn',function (e) {
        $('.user-menu-buttons .btn.active').removeClass('active');
        $(this).addClass('active');
    });


    var setContainerHeight = function() {
        var windowHeight = $(window).height();
        var containerOffsetResults = 200;
        var containerOffsetCustomers = 455;
        var containerOffsetCustomerResults = 135;
        var calculateContainerHeightResults = windowHeight - containerOffsetResults;
        var calculateContainerHeightCustomers = windowHeight - containerOffsetCustomers;
        var calculateContainerHeightCustomerResults = windowHeight - containerOffsetCustomerResults;
        $('#top-panels .tab-pane .results').height(calculateContainerHeightResults);
        $('#creditcheck-search-results').height(calculateContainerHeightCustomerResults);
        $('#validation-search-results').height(calculateContainerHeightCustomerResults);
        $('#porting-search-results').height(calculateContainerHeightCustomerResults);
        $('#openorders-search-results').height(calculateContainerHeightCustomerResults);
        $('#customer-screen-orders').height(calculateContainerHeightCustomerResults);
        $('#top-panels #customer_list').height(calculateContainerHeightCustomers);
    };
    var checkContainerExist = setInterval(function () {
        if ($('#top-panels .menu-middle').length) {
            setContainerHeight();
            window.addResizeEvent(function () {
                setContainerHeight();
            });
            clearInterval(checkContainerExist);
        }
    }, 100); // check every 100ms

    theBody.on('click','#cc-tabs a, #np-tabs a, #vc-tabs a',function (e) {
        e.preventDefault();
        $(this).tab('show');
        $('.ajax-details').addClass('hidden');
    });

    theBody.on('click', '.address-customer-pin',function(){
        var state = $(this).data('state');
        var active = Math.abs(parseInt(state)-1);
        var text = $(this).data('state-'+active);
        $(this).text(text).data('state', active);
    });

    theBody.on('click','.ctn-hide',function(){
        $('#ctn-tabs').toggle();
        if ($('#ctn-tabs').is(':visible')){
            $(this).parent().children('#arrow').removeClass('arrow-right');
            $(this).parent().children('#arrow').addClass('arrow-down');
        }else {
            $(this).parent().children('#arrow').removeClass('arrow-down');
            $(this).parent().children('#arrow').addClass('arrow-right');
        }
    });

    theBody.on('click','.customer-info-hide',function(){
        $('#customer-info-section').toggle();
        if ($('#customer-info-section').is(':visible')){
            $(this).parent().children('.customer-info-hide:first-child').removeClass('arrow-right');
            $(this).parent().children('.customer-info-hide:first-child').addClass('arrow-down');
        }else {
            $(this).parent().children('.customer-info-hide:first-child').removeClass('arrow-down');
            $(this).parent().children('.customer-info-hide:first-child').addClass('arrow-right');
        }
    });

    $('.sticker').on('click', '.customer_ctns ul li', function(){
        if($(this).hasClass('active')){
            $('.col-left .reduce').hide();
            // hide the shadow
            $('.col-left').css('box-shadow',"none");
            $('#left-panels').show();
            if (window.configurator) { $('#config-wrapper').hide(); }
        } else {
            if (window.configurator) { $('#config-wrapper').show(); }
            $('.col-left .reduce').show();
            // show the shadow
            $('.col-left').css('box-shadow',"2px 2px 5px 2px #dadada");
            $('#left-panels').hide('slow');
        }
        //remove/add full width when showing/hiding userinfo
        if($(this).hasClass('active') && $(this).find('#customer_info_left').length){
            $('.main .col-main').addClass("main-col-fullwidth");
        }else{
            $('.main .col-main.main-col-fullwidth').removeClass("main-col-fullwidth");
        }
    });

    theBody.on( 'click', '#close_customer_details',function(){
        $('#left-panels').hide('slow');
        // show config wrapper
        if (window.configurator) { $('#config-wrapper').show(); }
        // show the slide button on the left container
        $('.col-left .reduce').show();
        // hide the box-shadow
        $('.col-left').css('box-shadow',"2px 2px 5px 2px #dadada");
        //remove full width when not showing userinfo
        $('.main .col-main.main-col-fullwidth').removeClass("main-col-fullwidth");
    });


    bindSlidepanelLeft();

    // Open a specific customer tab
    var openTab = Mage.Cookies.get('openTab');
    if (openTab != undefined && openTab != '') {
        $(openTab).trigger('click');
        Mage.Cookies.set('openTab', '');
    }

    footerModals.on('click', '#save-cart-modal #change_address', function(){
        $('.email_send_wrapper').toggleClass('hidden');
        var originalEmail = $('#original_email');
        originalEmail.toggleClass('hidden');
        if(originalEmail.hasClass('hidden')){
            $('#change_address_label').html(Translator.translate('Customer email address'));
            $('#email_send').addClass('required-entry validate-email');
        }else{
            $('#change_address_label').html(Translator.translate('Other/customer address'));
            $('#email_send').removeClass('required-entry validate-email');
        }
    });

    footerModals.on('click', '.cancel-cart-button', function () {
        sessionStorage.removeItem('warnings');
        store.remove('redPlusPanelState');
        store.remove('linkedAccountPanelState');
        var modal = $('#cancel-cart-modal');
        modal
            .find('.modal-body')
            .html(modal.find('.after-click-block').html());
        modal.find('.modal-footer button').hide();
        window.canShowSpinner = false;
        if (window.ToggleBullet.switchPending == true) {
            window.ToggleBullet.onSwitchChange('.soho-toggle', 'soho');
            window.ToggleBullet.switchPending = false;
        }

        $.post(MAIN_URL + 'configurator/cart/empty', function () {
            setTimeout(function () {
                modal.modal('hide');
                window.location = "/";
            }, 2000);
        });
        store.set("shownHints", null);
        window.canShowSpinner = true;
    });

    footerModals.on('click', '.logout-customer-button', function () {
        var modal = $('#logout-customer-modal');
        modal.find('.modal-footer button').hide();
        window.canShowSpinner = false;
        customer.logoutCustomer();
        window.canShowSpinner = true;
    });

    //TODO: check if the package is retention by data-sale-type attribute (add class to retention packages)
    $('.block-container .title .mark').each(function() {
        if ($(this).html() === "RETBLU") {
            $(this).parent().parent().addClass('retention-package');
        }
    });

    $('.package-with-ctn').parent().find('.mark_ctn').each(function (i, _el) {
        var el = $(_el);
        el.html('&nbsp;' + (window.customer ? window.customer.formatPhoneNumberInt(el.text()) : el.text()));
    });

    // Call a specific function with the provided arguments
    var callFunc = Mage.Cookies.get('callFunc');

    if (callFunc != undefined && callFunc != '') {
        callFunc = JSON.parse(callFunc);
        var func = callFunc["fn"];
        window[func].apply({},callFunc["args"].toArray());
        Mage.Cookies.set('callFunc', '');
    }

    $.fn.removeClassPrefix=function(e){this.each(function(t,n){var r=n.className.split(" ").filter(function(t){return t.lastIndexOf(e,0)!==0});n.className=r.join(" ")});return this};

    window.duplicatePackage = function(event, element) {
        event.stopPropagation();
        event.preventDefault();
        var packageContainer = $('.cart_packages');
        var sideBlock = $(element).parents('.side-cart');
        var packageId = sideBlock.data('package-id');
        var type = $(element).parents('.side-cart').data('package-type');
        var checkoutButton = jQuery('#configurator_checkout_btn');
        var classList = checkoutButton.attr('class');
        var hasWarningClass = classList.indexOf("btn-warning");
        var oldDisabled = checkoutButton.prop('disabled');
        setPageLoadingState(true);
        $.post(MAIN_URL + 'configurator/cart/duplicatePackage', {'packageId': packageId, 'type': type}, function(data) {
            setPageLoadingState(false);
            if (data.error) {
                showModalError(data.message);
            } else {
                packageContainer.find('.side-cart').removeClass('active');
                var clone = $(data.rightBlock);
                togglePromoRules('close');
                clone
                    .data('package-type', data.type)
                    .data('package-id', data.packageId)
                    .attr('data-package-id', data.packageId)
                    .removeClass('active')
                    .find('.menu-expand')
                    .removeClass('open');
                $('.promo-rules-trigger').addClass('hidden');
                clone.insertAfter(packageContainer.find('.side-cart').last());

                sideBlock.find('.menu-expand').removeClass('open');
                if (!oldDisabled) {
                    data.totals = data.totals.replace('disabled="disabled" ', '');
                }
                if (hasWarningClass && hasWarningClass != -1) {
                    data.totals = data.totals.replace('btn-info ', 'btn-warning ');
                }

                $('.col-right .sticker #cart_totals').replaceWith(data.totals);
                sidebar.correctPricesVisibility();
                clone.find('span.package-type').click();

                if (data.hasOwnProperty('warning') && data.warning) {
                    INIT_CONFIGURATOR_WARNING = data.warning;
                }

                if (window.configurator) {
                    configurator.packageId = data.packageId;
                }
            }
        });
    };

    window.removePackage = function(event, element, expanded) {
        expanded = 'undefined' !== typeof expanded ? expanded : false;
        event.stopPropagation();
        event.preventDefault();
        var modal = $(element).parents('div.modal:first');
        modal.find('.modal-body').find('.after-click-block').removeClass('hide');
        modal.find('.modal-body').find('.before-click-block').addClass('hide');
        modal.find('.modal-title').addClass('hide');
        modal.find('.modal-footer button').addClass('hide');
        var blockPackageId = $(element).attr('data-pack-id-for-removal');
        var sideBlock = $(".side-cart[data-package-id='"+blockPackageId+"']");
        if (!blockPackageId) {
            console.log('Invalid package id: ', blockPackageId);
            return;
        }
        var checkoutButton = $('#configurator_checkout_btn');
        var classList = checkoutButton.attr('class') ? checkoutButton.attr('class') : [];
        var hasWarningClass = classList.indexOf("btn-warning");
        var oldDisabled = checkoutButton.prop('disabled');
        var aDeleteLink = $(sideBlock).find('.icons.delete a');
        aDeleteLink.removeAttr('onclick');
        var bundleId = sideBlock.data('bundle-id');
        // Reset the state of the tax toggle
        store.remove('btwState');
        try {
            customerSection.cancelMoveOffnet();
        } catch (error) {}
        var callback = function(data) {
            if (data.hasOwnProperty('showOneOfDealPopup') && data.showOneOfDealPopup) {
                $('#one-of-deal-modal').data('packageId', blockPackageId).modal();
                return;
            }
            if (window['configurator']) {
                window['configurator'].destroy();
            }
            sideBlock.remove();
            // Remove bundle hints
            if(typeof configurator != 'undefined') {
                configurator.closeBundleDetailsModal();
            }
            if (data) {
                var totals = $('.sidebar #cart_totals');
                if (data.totals && !oldDisabled) {
                    data.totals = data.totals.replace('disabled="disabled" ', '');
                }
                if (data.totals && hasWarningClass && hasWarningClass != -1) {
                    data.totals = data.totals.replace('btn-info ', 'btn-warning ');
                }
                if(data.hasOwnProperty('totals')) totals.replaceWith(data.totals);
                sidebar.correctPricesVisibility();
            }
            // If this is within the checkout, we need to refresh the page
            if (controller && controller == 'cart') {
                $(location).attr('href', MAIN_URL + 'checkout/cart/?step=saveOverview');
            } else {
                if (data.error) {
                    showModalError(data.message);
                } else {
                    window.quoteHash = data.quoteHash;
                    window.salesId = data.salesId;
                    window.processContextCode = data.processContextCode;
                    // Reset packages indexes in right sidebar
                    if (data.hasOwnProperty('packagesIndex') &&
                        data['packagesIndex'] != null &&
                        !$.isEmptyObject(data['packagesIndex'])
                    ) {
                        // Reassigning packages variables because it might be altered by dummy bundle package removal
                        packages = $('.col-right .cart_packages .side-cart.block');
                        $.each(packages, function(index, value){
                            var initialPackageId = $(value).data('package-id');
                            var newPackageId = data['packagesIndex'][initialPackageId];
                            $(value).attr('data-package-id', newPackageId);
                            $(value).data('package-id', newPackageId);
                        });

                        if (typeof window.configurator != 'undefined') {
                            configurator['packageId'] = data['packagesIndex'][configurator['packageId']] ;
                            if (data.rules) {
                                // Refresh Cable rules
                                configurator['cableRules'] = {'rules' : data.rules};
                                configurator.reapplyRules();
                            }
                        }
                        if (data.rightBlock != null) {
                            if (data.packageType) {
                                var rightBlock = $('div[data-package-type="' + data.packageType + '"]:visible').first();
                                rightBlock.replaceWith(data.rightBlock);
                                if (rightBlock.hasClass('active')) {
                                    rightBlock.removeClass('active');
                                }
                                rightBlock.find('.block-container').trigger('click');
                            } else {
                                var rightBlock = jQuery('.cart_packages');
                                rightBlock.replaceWith(data.rightBlock);
                            }
                        }
                    } else {
                        if (data.rightBlock != null) {
                            var rightBlock = $('div[data-package-type].active:visible').first();
                            rightBlock.replaceWith(data.rightBlock);
                        }else{
                            var remainPackages =  $('.cart_packages').find('.side-cart.block:visible');
                            $.each(remainPackages, function(index, value){
                                $(value).remove();
                            });
                            $('#package-types').removeClass('hide');
                        }

                    }
                    $(aDeleteLink).attr('onclick', 'showCancelPackageModal(this)');
                }
            }

            // update create package buttons
            updateCartPackageLine();

            if (expanded) {
                if (!data.complete) {
                    // If no packages are complete collapse the right sidebar
                    $('.sidebar .enlarge').trigger('click');
                }

                if (data.expanded) {
                    // Refresh the expanded cart content
                    $('.col-right #right-panel-cart').remove();
                    $('.col-right').append(data.expanded).find('#right-panel-cart').show();
                }
            }

            if (window.address.customerUctParams == true) {
                window.address.getCallerCampaignData(true, 1);
            }
        };
        if (!blockPackageId) {
            callback(data);
        } else {
            var postData = {'packageId': blockPackageId, 'btwState': jQuery('#cart_totals').data('state')};
            if (expanded) {
                postData['expanded'] = true;
            }

            // @todo on removal of bundle package remove entire bundle
            $.post(MAIN_URL + 'configurator/cart/removePackage', postData, function(data) {
                // clear registered bundle hints on window because of possible collisions for re-indexed package ids
                window.bundleHintShown = [];
                if(data.mustRemoveBundleChoice) {
                    $('#bundle-options').remove()
                }

                if (data.hasOwnProperty('cartIsRedPlusEligible') && data.cartIsRedPlusEligible) {
                    jQuery('#package-types').find("[data-package-creation-type-id='" + REDPLUS_CREATION_TYPE_ID + "']").removeClass('disabled');
                } else {
                    jQuery('#package-types').find("[data-package-creation-type-id='" + REDPLUS_CREATION_TYPE_ID + "']").addClass('disabled');
                }

                callback(data);

                var packages = $('.col-right .cart_packages .side-cart.block');
                var packageTypes = $("#package-types");

                packages.each(function(){
                    if($(this).hasClass('active')){
                        $(this)
                            .removeClass('active')
                            .find('.title i.prs.pull-left')
                            .removeClass('vfde-arrow-up')
                            .addClass('vfde-arrow-down2');
                    }
                });

                if(!packageTypes.hasClass('hide')){
                    packageTypes.addClass('hide');
                }

                setTimeout(function () {
                    modal.modal('hide');
                    modal.find('.modal-body').find('.after-click-block').addClass('hide');
                    modal.find('.modal-body').find('.before-click-block').removeClass('hide');
                    modal.find('.modal-title').removeClass('hide');
                    modal.find('.modal-footer button').removeClass('hide');
                }, 1000);

                if(typeof configurator != null) {
                    if (!data.clearConfigurator) {
                        configurator.getEligibleBundles();
                    }
                }

            });
        }
    };

    function addButtonClass($packageList) {
        var buttonsCount = $packageList.find('> div').length;
        $('#cart-spinner-wrapper').find('.cart_packages').addClass('control-buttons-' + buttonsCount);
    }

    function showAddPackageButtons() {
        var packageList = $('#package-types');
        addButtonClass(packageList);

        packageList.removeClass('hide');
    }

    function hideAddPackageButtons() {
        var packageList = $('#package-types');
        packageList.addClass('hide');
    }

    var openPackage = null;
    if(controller == 'cart') { showAddPackageButtons(); }

    $('#buttons-trigger').on('click', function() {
        var target = $(this).attr('data-target');
        $('#' + target).toggle();
    });

    htmlDOM.on('click', function() {
        $('.menu-expand').removeClass('open');
        $('.menu-trigger').removeClass('open');
        $('.drop-menu').removeClass('open');
        $('.btn-group').removeClass('open');
        $('.dropdown').removeClass('open');
    });

    $(".menu-expand, .drop-menu").on('click',function(e){
        e.stopPropagation();
    });

    $('.sidebar').on('click', '#show-packages', function() {
        if($('#package-types').hasClass('hide')) {
            showAddPackageButtons();
            $('.block-container').removeClass('hidden').addClass('hidden');
        } else {
            hideAddPackageButtons();
            $('.block-container').removeClass('hidden');
        }
    });

    if (getUrlParam('packageId')) {
        jQuery(document).ready(function () {
            var sideBlock = $('.cart_packages [data-package-id="'+getUrlParam('packageId')+'"]');
            if (sideBlock.length && !sideBlock.data('dummy-package')) {
                activateBlock(sideBlock.find('a.side-block-activate'));
            }
        });
    }
    //If we receive the parameter on the URL, init the configurator
    if (getUrlParam('type')) {
        initConfigurator(getUrlParam('type'), null, null, null);
    }

    var leftSide = $('.col-left').outerWidth();
    var rightSide = $('.col-right').outerWidth();

    function highlightErrors(content) {
        content = JSON.parse(content);
        if (!content) {
            return false;
        }
        var active = false;
        var activePackage = $('#cart-spinner-wrapper .cart_packages .active');
        if (activePackage.length) {
            active = activePackage.data('package-id');
        }

        $.each(content, function (id, el) {
            if (el.hasOwnProperty('soc-error')) {
              showModalError(el['soc-error']);
            } else if (active && id == active) {
                // Remove highlight from all sections
                $('#config-wrapper div.incomplete').removeClass('incomplete');
                // Highlight the missing sections
                $.each(el, function (section, message) {
                    $('#' + section + '-block').addClass('incomplete');
                });
            } else {
                // When package is not active highlight the entire package
                $('#cart-spinner-wrapper .cart_packages [data-package-id="' + id + '"]').addClass('incomplete');
            }
        });
    }

    theBody.on('click', '.enlarge', function (e) {
        customerDe.expandRightSidebar();
    });

    theBody.on('click', '#top-panels .additional .top-menu-element.available, #top-panels .ongoing .top-menu-element.available', function(e) {
        $(this).toggleClass('selected');
    });

    theBody.on('click', '#top-panels .top-menu-stop-propagation', function(e) {
        e.stopPropagation();
    });

    theBody.on('click', '.reduce', function () {
        if ($(this).parent().hasClass('smallView')) {
            $(this).parent().removeClass('smallView');
            $(this).parent().addClass('bigView');
            alignRightColumn();
            if ($('.checkout.bigView').length) {
            } else if ($('.col-main.bigView').length) {
                $('.col-main.bigView').removeClass('bigView');
            }
        } else if ($(this).parent().hasClass('bigView')) {
            $(this).parent().removeClass('bigView');
            $(this).parent().addClass('smallView');
            alignRightColumn();
            if ($('.checkout').length) {
            } else if ($('.col-main').length) {
                $('.col-main').addClass('bigView');
            }
        }
    });

    if ($('.checkout').length && $('.col-left.guest').length) {
        $('.col-left .reduce').hide();
    }

    //catch enter pressed on coupon expanded shopping cart
    theBody.on('keypress', '.package-item-description-coupon .cart-coupon-input', function(event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            $(this).blur();
            $('body').focus();
            applyCouponCode($(this).parent(), '.package-expand', window['onCheckout']);
        }
    });

    theBody.on('click', '#right_expanded_packets li a', function(e){
        e.preventDefault();
        var elem = $(this).attr('href'),
            container = $('.expanded-package-details'),
            scrollTo = $(elem);
        $('#right_expanded_packets li').removeClass();
        $(this).parent().addClass('active');
        container.animate({
            scrollTop: scrollTo.offset().top - container.offset().top + container.scrollTop() -10
        });
    });

    htmlDOM.on('click', function(){
        $('.menu-expand').removeClass('open');
        $('.drop-menu').removeClass('open');
        $('.btn-group').removeClass('open');
    });

    theBody.on('click', '.package-expand .nav-tabs a', function(e){
        e.preventDefault();
        $(this).tab('show');
    });

    window['refreshed'] = false;
    $(window).bind('beforeunload',function(){
        window['refreshed'] = true;
    });

    window.addResizeEvent(function() {
        rtime = new Date();
        if (window.timeout === false) {
            window.timeout = true;
            setTimeout(resizeend, delta);
        }
    });

    if (!supports_calc()) {
        window.onresize();
    }

    $(window).on('unload', function (ev) {
        if (window.leavePageDialogSwitcher === true) {
            disableLeavePageDialog();
        }
    });

    // TODO: Check if still needed (was used only in demo)
    // When polling screen is shown, fill bar and redirect to complete (stub mode)
    $("#polling-modal").on('shown.bs.modal', function () {
        var self = $(this);
        for (var i = 1; i <= 8; i++) {
            setTimeout(function (j) {
                self.find('.progress-bar').width((j * 10 + 20) + "%");
            }, 300 * i, i);
        }

        setTimeout(function () {
            window.location = '/checkout/cart/success';
        }, 300 * i+1);
    });
});
