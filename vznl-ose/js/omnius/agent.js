/*
 * Copyright (c) 2016. Dynacommerce B.V.
 */

'use strict';

(function($){
  window.Agent = function() { this.initialize.apply(this, arguments) };
  window.Agent.prototype = $.extend(VEngine.prototype, {

    /**
         * Login as another agent
         * @param username
         */
    loginOtherAgent: function(username) {
      // If agent tries to login another agent while on Checkout delivery show confirmation modal
      if (checkoutDeliverPageDialog === true) {
        jQuery('#checkout_deliver_confirmation').data('function','agent.loginOtherAgent').data('param1', username).appendTo('body').modal();
        return false;
      }
      var self = this;
      var callback = function (data, status) {
        if(data.error) {
          $('#agent-login-error').html(data.message);
        } else {
          setPageLoadingState(true);
          if (!data.is_prospect) {
            sessionStorage.setItem('permissionShowKias', data.show_kias);
            sessionStorage.setItem('permissionShowFn', data.show_fn);
            sessionStorage.setItem('permissionShowKd', data.show_kd);
          }
          window.location.reload(true);
        }
      };
      this.post('agent.loginOther', {username: username}, callback, function(){$('#agent-login-error').html(self.settings.errorMessage);});
    },

    /**
         * Logout from other agent
         */
    logoutOtherAgent: function() {
      // If agent tries to logout while on Checkout delivery show confirmation modal
      if (checkoutDeliverPageDialog === true) {
        jQuery('#checkout_deliver_confirmation').data('function','agent.logoutOtherAgent').appendTo('body').modal();
        return false;
      }

      var self = this;
      var callback = function(data, status) {
        if(data.error) {
          $('#agent-logout-error').html(data.message);
        } else {
          setPageLoadingState(true);
          if (!data.is_prospect) {
            sessionStorage.setItem('permissionShowKias', data.show_kias);
            sessionStorage.setItem('permissionShowFn', data.show_fn);
            sessionStorage.setItem('permissionShowKd', data.show_kd);
          }
          window.location.reload(true);
        }
      };
      this.post('agent.logoutOther', {}, callback, function(){$('#agent-login-error').html(self.settings.errorMessage);});
    },

    /**
         * Logout
         */
    logout: function() {
      // If agent tries to logout while on Checkout delivery show confirmation modal
      if (checkoutDeliverPageDialog === true) {
        jQuery('#checkout_deliver_confirmation').data('function','agent.logout').appendTo('body').modal();
        return false;
      }

      var callback = function(data, status) {
        window.location.reload(true);
      };
      this.post('agent.logout', {}, callback, function(){alert('Failed to logout current agent');});
    },

    /**
         * Change password
         * @param currPass
         * @param newPass
         * @param newPassAgain
         */
    changePassword: function(currPass, newPass, newPassAgain) {
      var data = {'curr-password' : currPass, 'password' : newPass, 'password-again' : newPassAgain };
      var self = this;
      var callback = function(data, status) {
        if(data.error) {
          $('#agent-change-password-error').html(Translator.translate(data.message));
        } else {
          $('#agent-change-password-error').html(Translator.translate(data.message));
        }
      };
      this.post('agent.changePass', data, callback, function(){$('#agent-login-error').html(self.settings.errorMessage);});
    },

    /**
         * Retrieve agent list
         * Pass useCache as true to cache the response
         * as this is a heavy request
         * @param useCache
         */
    getAgents: function(useCache) {
      var self = this;
      var input = $('#agent-username-input');
      var callback = function(data, status) {
        if(data.error) {
          $('#agent-login-error').html(data.message);
        } else {
          input.autocomplete({
            lookup: data.agents
          }).val('');
          input.data('agent-list', data);
        }
      };

      if (useCache && input.data('agent-list')) {
        callback(input.data('agent-list'));
      } else {
        this.post('agent.list', {}, callback, function(){$('#agent-login-error').html(self.settings.errorMessage);});
      }
    }

  });

})(jQuery);
