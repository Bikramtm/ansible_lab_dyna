/*
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** Custom Handlebars helpers used in VFNL **/
(function (Handlebars) {
  Handlebars.registerHelper('ifInArray', function (elem, list, options) {
    var count=list.length;
    for(var i=0;i<count;i++)
    {
      if(list[i].key==elem) {
        return options.fn(this);
      }
    }
    return options.inverse(this);
  });

  Handlebars.registerHelper('ifInId', function (elem, list, options) {
    var count = 0;
    if(list) {
      count = list.length;
    }
    for(var i=0;i<count;i++)
    {
      if(list[i].id==elem) {
        return options.fn(this);
      }
    }
    return options.inverse(this);
  });

  Handlebars.registerHelper('ifInArrayValue', function (elem, list, options) {
    var count = 0;
    if(list) {
      count = list.length;
    }
    for(var i=0;i<count;i++)
    {
      if(list[i]==elem) {
        return options.fn(this);
      }
    }
    return options.inverse(this);
  });

  Handlebars.registerHelper("or",function() {
    var args = Array.prototype.slice.call(arguments);
    var options = args[args.length-1];

    for(var i=0; i<args.length-1; i++){
      if( args[i] ){
        return options.fn(this);
      }
    }

    return options.inverse(this);
  });
})(Handlebars);
