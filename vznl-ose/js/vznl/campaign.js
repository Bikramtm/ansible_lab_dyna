'use strict';

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var Campaign =
/*#__PURE__*/
function () {
  function Campaign(container) {
    _classCallCheck(this, Campaign);

    this.container = container;
    this.topContainer = jQuery('#campaigns-top');
    this.itemsContainer;
    this.paginationContainer;
    this.controls;
    this.page = 1;
    this.triggered=false;
    this.pageCount = 0;
    this.totalItems = 0;
    this.itemsPerPage = this.getItemsPerPage();
    this.offersTemplate = Handlebars.compile(jQuery("#Handlebars-Template-Customer-Campaigns").html());
    this.topTemplate = Handlebars.compile(jQuery("#Handlebars-Template-Customer-Campaigns-Top").html());
    this.getData(0);
    jQuery(window).resize(function () {
      window.campaign.resize();
    });
  }

  _createClass(Campaign, [{
    key: "renderTop",
    value: function renderTop(data) {
      this.topContainer.html(this.topTemplate(data));
    }
  }, {
    key: "renderOffers",
    value: function renderOffers(data) {
      this.container.html(this.offersTemplate(data));
      this.itemsContainer = this.container.find('#campaigns-offers-items');
      this.paginationContainer = this.container.find('#campaigns-offers-pagination');
      this.controls = {
        previous: this.container.find('#campaigns-offers-controls-previous'),
        next: this.container.find('#campaigns-offers-controls-next')
      };
      this.totalItems = this.itemsContainer.children().length;

      if (this.totalItems > 0) {
        this.itemsContainer.css('width', 240 * this.totalItems - 20);
        this.container.removeClass('hidden');
        this.itemsContainer.find('[data-toggle="tooltip"]').tooltip();
        jQuery('#campaigns-offers-empty').addClass('hidden');
      } else {
        this.container.addClass('hidden');
        jQuery('#campaigns-offers-empty').removeClass('hidden');
      }

      this.itemsPerPage = this.getItemsPerPage();
      this.equalizeHeight();
      this.addTooltips();
      this.render();
    }
  }, {
    key: "addTooltips",
    value: function addTooltips() {
      this.itemsContainer.find('.campaigns-offers-item-header-title, .campaigns-offers-item-header-subtitle').each(function () {
        var element = jQuery(this);

        if (element.outerWidth() < element[0].scrollWidth) {
          element.attr({
            'data-toggle': 'tooltip',
            'data-html': 'true',
            'data-container': 'body',
            'data-placement': 'right campaign-tooltip',
            'data-title': element.html()
          });
        }
      });
    }
  }, {
    key: "getItemsPerPage",
    value: function getItemsPerPage() {
      var windowWidth = jQuery(window).width();

      if (this.totalItems >= 4 && windowWidth >= 1653) {
        return 4;
      }

      if (this.totalItems >= 3 && windowWidth >= 1366) {
        return 3;
      }

      return 2;
    }
  }, {
    key: "resize",
    value: function resize() {
      var previousItemsPerPage = this.itemsPerPage;
      this.itemsPerPage = this.getItemsPerPage();

      if (previousItemsPerPage === this.itemsPerPage) {
        return;
      }

      this.render();
    }
  }, {
    key: "equalizeHeight",
    value: function equalizeHeight() {
      var uspContainers = this.itemsContainer.find('.campaigns-offers-item-content-usps');
      var maxHeight = Math.max.apply(null, uspContainers.map(function () {
        return jQuery(this).outerHeight();
      }).get());
      uspContainers.css('min-height', maxHeight);
      var showElements = this.itemsContainer.find('.campaigns-offers-item-content-overlay, .campaigns-offers-item-content-show');

      if (maxHeight < 120) {
        showElements.addClass('hidden');
        this.show(false);
      } else {
        showElements.removeClass('hidden');
      }
    }
  }, {
    key: "render",
    value: function render() {
      this.container.removeClass('campaigns-offers-3 campaigns-offers-4');

      if (this.itemsPerPage > 2) {
        this.topContainer.addClass('campaigns-top-large');
        this.container.addClass('campaigns-offers-' + this.itemsPerPage);
      } else {
        this.topContainer.removeClass('campaigns-top-large');
      }

      this.pageCount = this.totalItems > this.itemsPerPage ? this.totalItems + 1 - this.itemsPerPage : 1;

      if (this.page > this.pageCount) {
        this.page = this.pageCount;
      }

      this.createPagination();
      this.updatePosition();
    }
  }, {
    key: "createPagination",
    value: function createPagination() {
      if (this.pageCount <= 1) {
        this.paginationContainer.html('').addClass('hidden');
        return;
      }

      this.paginationContainer.removeClass('hidden');
      var content = '';

      for (var page = 1; page <= this.pageCount; page++) {
        content += '<li' + (page === this.page ? ' class="active"' : '') + '><a href="#" onclick="window.campaign.setPage(' + page + ');">' + page + '</a></li>';
      }

      this.paginationContainer.html(content);
    }
  }, {
    key: "show",
    value: function show(expand) {
      if (expand) {
        this.itemsContainer.addClass('campaigns-offers-items-expand');
      } else {
        this.itemsContainer.removeClass('campaigns-offers-items-expand');
      }

      return false;
    }
  }, {
    key: "updatePosition",
    value: function updatePosition() {
      this.itemsContainer.css('margin-left', (this.page - 1) * 240 * -1);

      if (this.page === 1) {
        this.controls.previous.addClass('hidden');
      } else {
        this.controls.previous.removeClass('hidden');
      }

      if (this.page === this.pageCount) {
        this.controls.next.addClass('hidden');
      } else {
        this.controls.next.removeClass('hidden');
      }

      this.paginationContainer.children('.active').removeClass('active');
      this.paginationContainer.children(':eq(' + (this.page - 1) + ')').addClass('active');
    }
  }, {
    key: "move",
    value: function move(next) {
      if (next && this.page < this.pageCount) {
        this.page++;
        this.updatePosition();
      } else if (!next && this.page > 1) {
        this.page--;
        this.updatePosition();
      }
    }
  }, {
    key: "setPage",
    value: function setPage(page) {
      this.page = page;
      this.updatePosition();
      return false;
    }
  }, {
    key: "loading",
    value: function loading(active) {
      if (active) {
        jQuery('#campaigns-loading').addClass('campaigns-loading-reload');
      } else {
        jQuery('#homepage-wrapper').addClass('campaigns-initiated');
        jQuery('#campaigns-loading').removeClass('campaigns-loading-reload');
      }
    }
  }, {
    key: "getData",
    value: function getData(defaultCampaign) {
      var self = this;
      this.loading(true);
      var serviceability;

      if(store.get('serviceApiResponse') === null) {
        serviceability = store.get('notServiceable');
      } else {
        serviceability = store.get('serviceApiResponse');
      }
      var requestData = {
        'serviceability': serviceability,
        'defaultCampaign': defaultCampaign
      };
      this.triggered=true;
      jQuery.ajax({
        type: 'POST',
        data: requestData,
        dataType: "json",
        url: '/customerde/details/getCampaigns',
        success: function success(response) {
          if (response.success) {
            if ("has_campaign" in response && response.has_campaign == true) {
              reloadQuote(response.quote_id, 0);
            } else if ("callerData" in response && "campaignData" in response) {
              if (response.salesIds) {
                var salesIds = {
                  data: response.saleIds
                };

                if (response.salesIds.error) {
                  salesIds = response.salesIds;
                }

                window.getVoids(salesIds);
              }

              self.renderTop(response.campaignData);
              self.renderOffers(response.campaignData);
            }
          } else {
            showModalError(response.message);
          }
        },
        complete: function complete(response) {
          self.loading(false);
        }
      });
    }
  }]);

  return Campaign;
}();