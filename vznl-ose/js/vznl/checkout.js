(function ($) {
  Checkout = (function (original) {
    function Checkout() {
      original.apply(this, arguments);

      // Hide the shoppincart when checkout visible
      $('.col-right').hide();
      $('.col-main').addClass('checkout');

      // Hide the left column (customer details / search)
      $('.col-left').removeClass('bigView');
      $('.col-left').addClass('smallView');
      $('.checkout').addClass('bigView');

      this.searchStoreTimeout = null;

      // Overriding methods
      this.firstCheckoutStep = 'saveNumberPorting';
      this.checkCurrentStep = function (currentCheckoutStep) {

        if (this.orderEdit != 0 && (currentCheckoutStep == 'null')) {
          currentCheckoutStep = 'saveSuperOrder';
        } else if (this.orderEdit == 0 && (currentCheckoutStep == 'null')) {
          $('#saveNumberPorting').find('button[type=submit]').prop("disabled", false);
          currentCheckoutStep = this.firstCheckoutStep;
        }

        if (currentCheckoutStep != 'saveCustomer') {
          $('#saveCustomer #create-offer').addClass('hidden');
        } else {
          $('#saveCustomer #create-offer').removeClass('hidden');
        }
        this.currentCheckoutStep = currentCheckoutStep;

        var self = this;
        var active = true;
        var activeSteps = [];
        self.isOffer = 2;
        $('#cart-content').children('.section').each(function (f, elem) {
          if ($(elem).hasClass('skip')) return;
          var cartForm = $(elem).children('.cart');

          if (cartForm.length) {
            var name = $(elem).children('.cart').attr('action');
            self.form[name + 'Data'] = $('#' + name).serialize();
            self.stepsOrder.push(name);
          }
        });

        self.stepsOrder.each(function (name) {
          if ((currentCheckoutStep != 'null') && ($.inArray(currentCheckoutStep, self.stepsOrder) != -1) && active) {
            $('#{0}'.format(name)).parent().removeClass('hidden');

            $('#cart-left').find('[name="' + name + '"]').parent().removeClass('disabled active').addClass('active');
            $('#' + name).find('button[type=submit]').addClass('hide');
            $('#' + name).find('button.back-button').addClass('hide');
            $('#' + name).find('button[type=submit]').parents('.prev-next-section').addClass('hidden');
            activeSteps.push(name);
            if (currentCheckoutStep == name) {
              active = false;
              $('#' + name).find('button[type=submit]').removeClass('hide');
              $('#' + name).find('button.back-button').removeClass('hide');
              $('#' + name).find('button[type=submit]').parents('.prev-next-section').removeClass('hidden');
            }

            self.fillFormData(name);
            self.executeStepCallBacks(name);
          }
        });

        if ((this.orderEdit == 0) && currentCheckoutStep != 'null' && ($.inArray(currentCheckoutStep, self.stepsOrder) != -1)) {
          $('#cart-left').find('[name="' + currentCheckoutStep + '"]').parent().addClass('active');
          $('#' + currentCheckoutStep).find('button[type=submit]').removeClass('hide');
          $('#' + currentCheckoutStep).find('button[type=submit]').parents('.prev-next-section').removeClass('hidden');
          var currentStepDiv = $('#{0}'.format(currentCheckoutStep));

          if (currentStepDiv.length) {
            scrollToExactSection(currentStepDiv);
          }
        }

        this.options.activesteps = activeSteps;

        if (this.orderEdit != 0) {
          this.initialize();
        }
      };

      this.savePreviousStep = function (noErrors, stepName, checkoutAction, thisForm) {
        var self = this;
        var cartCurrentForm = new VarienForm(stepName);
        if (!(stepName in self.form)) {
          // Workaround for retention fix
          noErrors = true;
        } else if (cartCurrentForm.validator && cartCurrentForm.validator.validate() && noErrors) {
          var oldForm = $('#' + stepName);
          oldForm.find('.ajax-validation').fadeOut('slow', function () {
            $(this).remove();
          });
          oldForm.find('.cart-errors').remove();
          if (self.form[stepName + 'Data'] != $('#' + stepName).serialize()) {

            $.ajax({
              type: 'POST',
              url: this.checkoutController + stepName,
              data: oldForm.serializeObject(),
              async: false
            })
                .done(function (response) {
                  var postCallbackTest = self.postCallback(response, oldForm);
                  if (!postCallbackTest) {
                    noErrors = false;
                  } else {
                    // Save data to be able to compare in case of going back some steps
                    self.form[checkoutAction + 'Data'] = thisForm.serialize();
                  }
                });
          }
        } else {
          noErrors = false;
          thisForm.find('#submit').button('reset').removeClass('disabled').prop('disabled', false);
        }
        return noErrors;
      };

      this.submitCart = function (obj, async) {
        async = (async == true || async == 'undefined' || async == undefined);
        var self = this;
        var thisForm = obj;
        if (self.checkoutDisabled()) {
          self.toggleLockAction();
          return false;
        }
        thisForm.find('.ajax-validation').fadeOut('slow', function () {
          $(this).remove();
        });
        thisForm.find('.cart-errors').remove();

        var currentForm = thisForm.attr('id');
        var cartCurrentForm = new VarienForm(currentForm);

        if (cartCurrentForm.validator && cartCurrentForm.validator.validate()) {
          var checkoutAction = thisForm.attr('action');
          var formData = thisForm.serializeObject();
          var stepsOrder = self.stepsOrder;
          var currentStep = stepsOrder.indexOf(checkoutAction);
          var noErrors = true;
          for (var i = currentStep - 1; i >= 0; --i) {
            noErrors = this.savePreviousStep(noErrors, stepsOrder[i], checkoutAction, thisForm);
          }
          if (noErrors) {
            thisForm.find('button[type=submit]').addClass('hide');
            thisForm.find('button.back-button').addClass('hide');
            thisForm.find('button[type=submit]').parents('.prev-next-section').addClass('hidden');
            var nextStep = thisForm.parents('.bs-docs-section').nextAll('.bs-docs-section').first().find('form').attr('id');

            formData['current_step'] = nextStep;
            formData['is_offer'] = self.isOffer;

            // Remove serialized pakket data for every delivery method beside split
            if (formData.hasOwnProperty('delivery') && formData['delivery']['method'] !== 'split') {
              delete formData['delivery']['pakket'];
            }

            // On order save first show the popup, then make the calls
            if (checkoutAction == 'saveOverview') {
              // Hide all active buttons from the overview when save is pressed
              $('#cart-content .new-customer:visible, #cart-content .submit button:visible, #cart-content .expanded-package-container .drop-menu:visible, #cart-content .package-item-description-coupon:visible').addClass('hidden');
              if ($('#current_store_is_telesales').val() != 1) {
                Polling.show();
              }
            }

            if (checkoutAction == 'saveContract') {
              Polling.show();
            }

            if (checkoutAction == 'saveSuperOrder') {
              Polling.show();
              enableLeavePageDialog();
            }
            var checkoutMethod = checkoutAction;

            var that = this;
            setPageLoadingState(true);

            $.ajax({
              type: "POST",
              url: this.checkoutController + checkoutMethod,
              data: formData,
              async: async
            })
                .done(function (response) {
                  // if server validation errors are returned, display them instead of showing modal error
                  if (response.fields) {
                    // hide this modal until further details of its existence at this point
                    if ($('.prolongation-modal').hasClass('modal-dialog')) {
                      $('.prolongation-modal').removeClass('modal-dialog').addClass('modal');
                    }
                  }

                  if (!checkoutAction.indexOf(['saveOverview', 'saveContract', 'saveSuperOrder', 'savePayments', 'saveCustomer'])) {
                    that.updateProductsOnQuote();
                  }

                  if (checkoutMethod != 'saveSuperorderId') {
                    setPageLoadingState(false);
                  }

                  self.checkoutAction = checkoutAction;
                  self.currentCheckoutStep = stepsOrder[currentStep + 1];
                  // RFC-151034 : Alert agent with message "The order content has just been changed, please re-open the order"
                  if (self.checkoutAction == 'saveContract') {
                    if (response.hasOwnProperty('serviceError')) {
                      Polling.hide();
                      showModalError(response.message, response.serviceError);
                      return false;
                    }
                  }

                  var callResponse = self.postCallback(response, thisForm);
                  if (!callResponse) {
                    if (self.checkoutAction == 'saveContract' && !response.hasOwnProperty('serviceError')) {
                      var approveModal = $('#approve-package-fail-modal');
                      approveModal.find('#approve-package-error-msg').html(response.message);
                      approveModal.modal();
                      thisForm.find('.cart-errors').html('');
                    }
                    return false;
                  }

                  if (self.isOffer == 1) {
                    if (response.is_offer == true) {
                      // redirect to home page if the offer has been saved
                      window.location = '/';
                    }
                  } else {
                    if (self.checkoutAction != 'saveContract' && self.checkoutAction != 'saveSuperOrder') {
                      processPreviousSteps(nextStep);
                    }
                    triggerCheckoutEvent('checkout.stepdone', checkoutAction);
                    self.fillFormData(nextStep);
                    self.executeStepCallBacks(nextStep);

                    $('#cart_left_' + formData['current_step']).trigger('click');
                  }

                  if (!(response.fields) && response.error) {
                    showModalError(response.message);
                    setPageLoadingState(false);
                    thisForm.find('button[type=submit]').removeClass('hide');

                    if (response.hasOwnProperty('termsAndConditionsError') && response.termsAndConditionsError) {
                      thisForm.find('button.back-button').removeClass('hide');
                      thisForm.find('button[type=submit]').removeClass('hide');
                      thisForm.find('button[type=submit]').parents('.prev-next-section').removeClass('hidden');

                      //disable buttons
                      $('button[data-step="saveOverview"].submitbutton').prop('disabled', true).addClass('disabled');
                      $('.submit-buttons button.send-offer-btn').prop('disabled', true).addClass('disabled');
                    }

                    return false;
                  }
                });
            return true;
          } else {
            if (obj.attr('id') == 'saveSuperOrder') {
              $('.checkout-totals').find('button.disabled').removeClass('disabled');
            }
            // fix to side-effect of OVG-2151 (VFDED1W3S-1696)
            if (checkoutAction == 'saveOrderOverview') {
              var nextSectionContainer = $('#saveOrderOverviewNextSection'),
                  nextSectionContainerBtn = nextSectionContainer.find('.checkout-continue-btn');
              if (nextSectionContainer.hasClass('hidden') && (nextSectionContainerBtn.hasClass('hide'))) {
                nextSectionContainer.removeClass('hidden');
                nextSectionContainerBtn.removeClass('hide');
              }
            }
            return false;
          }
        } else {
          return false;
        }
      };

      this.postCallback = function (response, changedForm) {
        var self = this;
        var stepsOrder = self.stepsOrder;
        var currentStep = stepsOrder[stepsOrder.indexOf(this.checkoutAction)];
        var currentForm = $('#' + currentStep);
        var lastFormInView = $('#' + self.currentCheckoutStep);
        changedForm.find('.cart-errors').remove();
        currentForm.find('.cart-errors').remove();
        lastFormInView.find('.cart-errors').remove();
        if (response.hasOwnProperty('serviceError') && (jQuery('#current_store_is_telesales').val() == 0)) {
          // Close the popups in case of a service error, because it means there is no polling id and no polling can be made
          if ('saveOverview' == this.checkoutAction) {
            self.showVodError(true, response.serviceError + ' - ' + response.message, 1, 1);
          } else if ('saveContract' == this.checkoutAction) {
            self.showVodError(true, response.serviceError + ' - ' + response.message, 1, 0);
          } else if ('saveSuperOrder' == this.checkoutAction) {
            self.showVodError(true, response.serviceError + ' - ' + response.message, 1, 1);
            disableLeavePageDialog();
          } else {
            showModalError(response.message, response.serviceError);
          }

          changedForm.find('button.back-button').removeClass('hide');
          changedForm.find('button[type=submit]').removeClass('hide').button('reset');
          changedForm.find('button[type=submit]').parents('.prev-next-section').removeClass('hidden');
        } else if (response.error == true && (jQuery('#current_store_is_telesales').val() == 0)) {
          // Close the popups in case of an error, because it means there is no polling id and no polling can be made
          if ('saveOverview' == this.checkoutAction) {
            Polling.hide();
          }

          if ('saveContract' == this.checkoutAction) {
            Polling.hide();
          }

          if ('saveSuperOrder' == this.checkoutAction) {
            Polling.hide();
            disableLeavePageDialog();
          }
          if (response.hasOwnProperty('modalError')) {
            showModalError(Translator.translate(response.modalError, Translator.translate('Error')));
          }

          if (response.unreserved != undefined) { //could not reserve all packages
            //SHOW MODAL
            var reservedModal = $('#reserve-fail-modal');
            reservedModal.find('.failed-reservation').html(response.unreserved);
            var replacementText = $(".failed-reservation").find("p:contains('Mobile package')").text().replace('Mobile package', Translator.translate('Mobile package'));
            $(".failed-reservation").find("p:contains('Mobile package')").text(replacementText);
            reservedModal.modal();
            return;
          }

          var otherAddressField = {};
          window.data = response;
          var errorText = '';
          var focused = false;
          var showOtherAddressFields = false;
          var showCompanyDetailsFields = false;
          for (var i in response.fields) {
            if (response.fields.hasOwnProperty(i)) {
              var label;
              var field = $('[name="' + i + '"]');

              if (!showOtherAddressFields) {
                if (i.indexOf('otherAddress') != -1) {
                  showOtherAddressFields = true;
                  if (i.indexOf('pakket') != -1) {
                    var re = /pakket\]\[(\d+)\]\[otherAddress/g;
                    var packId = i.split(re, 2);
                    otherAddressField[packId[1]] = field;
                  } else {
                    otherAddressField[0] = field;
                  }
                }
                if (showOtherAddressFields) {
                  for (var j in otherAddressField) {
                    showOtherAddress(otherAddressField[j].parents('.otherAddress').removeClass('hidden').find('.editOtherAddress'));
                  }
                }
              }

              if (!showCompanyDetailsFields) {
                if (i.indexOf('company_legal_form') != -1 || i.indexOf('company_vat_id') != -1) {
                  showCompanyDetailsFields = true;
                }

                if (showCompanyDetailsFields) {
                  $('#customer-company-search .enter-yourself').trigger('click');
                }
              }

              field.parent().find('.validation-advice').remove();
              var fieldError = '<div class="validation-advice ajax-validation" style="">' + Translator.translate(response.fields[i]) + '</div>';
              field.addClass('validation-failed').parent().append(fieldError);
              if (field.length && $.trim(field.siblings('label').text()).length) {
                label = $.trim(field.siblings('label').text());
              } else if (field.length && $.trim(field.parent().siblings('label').text()).length) {
                label = $.trim(field.parent().siblings('label').text());
              } else {
                if (field.attr('placeholder') !== undefined) {
                  label = field.attr('placeholder');
                } else {
                  var match = i.match(/\[(.*)\]/);
                  if (match) {
                    label = match[1].split('_').map(function (i) {
                      return i.charAt(0).toUpperCase() + i.slice(1);
                    }).join(' ');
                  } else {
                    label = i.split('_').map(function (i) {
                      return i.charAt(0).toUpperCase() + i.slice(1);
                    }).join(' ');
                  }
                }
              }

              if (!focused && field.parent().get(0)) {
                field.parent().get(0).scrollIntoView();
                focused = true;
              }

              errorText += '<i>' + label + '</i>: ' + response.fields[i] + '<br>';
            }
          }
          if (!response.hasOwnProperty('modalError')) {
            changedForm.find('button[type=submit]').removeClass('hide').button('reset');
            changedForm.find('button[type=submit]').parents('.prev-next-section').removeClass('hidden');
            changedForm.find('button.back-button').removeClass('hide');
            if ('saveOverview' == this.checkoutAction) {
              if(response.ilt_failed == true){
                jQuery('#ilt_store_failed.modal button.btn-default').attr('onclick', 'continueSaveOverview(true)');
                jQuery('#ilt_store_failed.modal button.btn-info').attr('onclick', 'continueSaveOverview(false)');
                jQuery('#ilt_store_failed.modal').appendTo('body').modal({
                  backdrop: 'static',
                  keyboard: false
                });
              } else {
                showModalError(Translator.translate(response.message));
              }
            }
            else {
              if (currentForm.context == changedForm.context) {
                lastFormInView.find('button[type=submit]').removeClass('hide').button('reset');
                lastFormInView.find('button.back-button').removeClass('hide');
                lastFormInView.append('<div class="cart-errors"><strong>' + Translator.translate(response.message) + '</strong>:<br >' + Translator.translate(errorText) + '</div>');
              } else {
                changedForm.find('button[type=submit]').removeClass('hide').button('reset');
                changedForm.find('button.back-button').removeClass('hide');
                changedForm.append('<div class="cart-errors"><strong>' + Translator.translate(response.message) + '</strong>:<br >' + Translator.translate(errorText) + '</div>');
              }
            }
          }

          if ('saveOverview' == this.checkoutAction) {
            $(changedForm.find('button[type=submit]')).addClass('hide');
            $(changedForm.find('button[type=submit]')).parents('.prev-next-section').addClass('hidden');
          }

          return false;
        } else if (response.error == false || ($('#current_store_is_telesales').val() != 0)) {

          if (response.orders != undefined) {
            $('#order-increment-id').text(response.orders);
            $('#order-increment-id-input').val(response.orders);
            customerDe.breadcrumbValue = [
              {text: 'Order confirmed', link: '', type: ''}
            ];
            customerDe.createBreadcrumb();
          }

          if (this.checkoutAction == 'saveOverview' || this.checkoutAction == 'saveSuperOrder') {
            if (response.creditcheck_page) {
              var creditcheck_content = $('#creditcheck-waiting').find('#saveCreditCheck');
              if ($('#creditcheck-waiting') && creditcheck_content) {
                creditcheck_content.replaceWith($(response.creditcheck_page).find('#saveCreditCheck'));
              }
            }
            if ($('#current_store_is_telesales').val() == 0) {
              checkoutDeliverPageDialog = true;
            }
            if (response.contract_page) {
              var directDelivery = $('input[name="delivery[method]"]').val() == 'direct';

              if ((directDelivery && response.skip_number_selection == undefined) || this.store == 'indirect'){
                $('#saveNumberSelection').parent().removeClass('skip');
                $('#cart-left .bs-sidebar .nav li a[href="#number-selection"]').parent().removeClass('hidden');
              } else {
                $('#saveNumberSelection').parent().addClass('skip');
                $('#cart-left .bs-sidebar .nav li a[href="#number-selection"]').parent().addClass('hidden');
                checkoutDeliverPageDialog = false;
              }

              $('#cart-left .bs-sidebar .nav li a[href="#contract"]').parent().removeClass('hidden');
              var contract_content = $('#contract_page');
              if ($('#contract_page') && contract_content) {
                contract_content.replaceWith(response.contract_page);
              }

              if (this.checkoutAction == 'saveSuperOrder') {
                $('#contract-continue-button').parent().addClass('hidden');
                $('#saveContract .checkbox-inline').addClass('hidden');
              }

              // When not direct the contract signing should not happen yet.
              if (!directDelivery && this.store != 'indirect') {
                $('#contract').parent().addClass('hidden');
                $('#saveContract').parent().parent().addClass('skip');
              } else {
                $('#contract').parent().removeClass('hidden');
                $('#saveContract').parent().removeClass('skip');
              }

              if (this.store == 'retail') {
                $('#contract').parent().removeClass('hidden');
                $('#saveContract').parent().removeClass('skip');
              }

              var hnIndex = self.stepsOrder.indexOf("saveNewNetherlands");
              if (self.stepsOrder.indexOf("saveNumberSelection") == -1 && $('#saveNumberSelection').length > 0) {
                if (hnIndex != -1) {
                  self.stepsOrder.splice(hnIndex, 0, 'saveNumberSelection');
                } else {
                  self.stepsOrder.splice(self.stepsOrder.length - 1, 0, 'saveNumberSelection');
                }
              }
              if (self.stepsOrder.indexOf("saveContract") == -1) {
                self.stepsOrder.splice(self.stepsOrder.length - 1, 0, 'saveContract');
              }
            } else {
              $('#saveNumberSelection').parent().addClass('skip');
              $('#saveContract').parent().parent().addClass('skip');
            }

            // Added for RFC-150665/RFC-150663
            if (response.newOrderMessage && response.newOrderMessage.length) {
              $('#showOrderConfirmation #order-confirmation-new-order-message').html(response.newOrderMessage);
            }

            if (response.oldOrderMessage && response.oldOrderMessage.length) {
              $('#showOrderConfirmation #order-confirmation-old-order-message').html(response.oldOrderMessage);
            }

            $('.order-lock').remove();
          }
          if (response.new_order != undefined) {
            $('#new-order-increment-id').data('id', response.new_order_id);
            $('#new-order-increment-id').text(response.new_order);
            $('#new-order-increment-id-input').val(response.new_order);
            $('.new_superorder').removeClass('new_superorder');
          }
          if (this.checkoutAction == changedForm.attr('action') && self.isOffer != 1) {

            if (this.checkoutAction == 'saveOverview' || this.checkoutAction == 'saveSuperOrder') {
              showProcessOrderModal(response.polling_id);
            }
            if(this.checkoutAction == 'saveSuperOrder'){
              jQuery('#cart-left [name="saveCreditCheck"]').parent().removeClass('hidden');
              jQuery('#cart-left [name="saveContract"]').parent().removeClass('hidden');
            }

            if (this.checkoutAction == 'saveOverview' || this.checkoutAction == 'saveSuperOrder') {
              if ($('#saveCreditCheck').length && response.hasOwnProperty('addresses') && response.hasOwnProperty('order_id')) {
                // If the creditcheck step is present and necessary
                var creditStub = response.credit_fail == true;
                window.creditCheckData = {addresses: response.addresses, order_id: response.order_id, creditStub: creditStub, order_no: response.orders};
              }
            }

            if (this.store == 'retail' || this.store == 'belcompany' || this.store == 'indirect') {
              if (this.store == 'retail' || this.store == 'belcompany') {
                if (this.checkoutAction == 'saveDeliveryAddress') {
                  if (response.hasOwnProperty('delivery_steps') && response.delivery_steps == false) {
                    // hide delivery steps
                    $('#saveNumberSelection').parent().addClass('hidden skip');
                    $('#cart-left .bs-sidebar .nav li a[href="#number-selection"]').parent().addClass('hidden');
                    $('#saveContract').parent().addClass('hidden skip');
                    $('#cart-left .bs-sidebar .nav li a[href="#contract"]').parent().addClass('hidden');
                    self.stepsOrder = $.grep(self.stepsOrder, function (value) {
                      return value != 'saveContract' && value != 'saveNumberSelection';
                    });
                  }
                }
              }

              if (this.checkoutAction == 'saveCreditCheck') {
                // check if the number selection step is available
                if ($('#saveNumberSelection').length && response.hasOwnProperty('html')) {
                  updateNumberSelection(response.html);
                }
                // check if the device selection is available
                if ($('#saveDeviceSelection').length && response.hasOwnProperty('device_selection')) {
                  updateDeviceSelection(response.device_selection);
                }
              }

              if (this.checkoutAction == 'saveNumberSelection' && $('#saveContract').length && response.contract != undefined) {
                updateContract(response.contract);
              }
            }

            if ((this.checkoutAction === 'saveDeliveryAddress' || this.checkoutAction === 'savePayments') && response.hasOwnProperty('order_overview_content') && response.hasOwnProperty('overview_content')) {
              $('#save-order-overview-package-content').html(response.order_overview_content);
              $('#save-overview-package-content').html(response.overview_content);
              $('#save-payments-onetime-content').html(response.payments_content);
              jQuery('#save-payments-onetime-content').checkoutBeautifier();
            }

            if (this.checkoutAction == 'saveSuperOrder') {
              if (!response.hasOwnProperty('orders')) {
                Polling.hide();
                window.location = this.baseURL + 'checkout/cart/exitChangeOrder';
              } else {
                Polling.start('saveSuperOrder', response.pool_id);
              }
            }

            if (this.checkoutAction == 'saveCreditCheck') {
              $('#creditcheck-waiting').addClass('hidden');
              $('#credit-check-menu').addClass('disabled').removeClass('active');
              var nextStep = this.stepsOrder[this.stepsOrder.indexOf('saveCreditCheck') + 1];
              if (undefined != nextStep) {
                $('#credit-check-menu').parent().find('[name=' + nextStep + ']').parent().addClass('active');
              }
            }

            if (this.checkoutAction == 'saveContract') {
              self.showVodError(false);
              Polling.start('saveContract', response.pollingId);
            }

            if (this.checkoutAction != 'saveOverview' && this.checkoutAction != 'saveContract' && this.checkoutAction != 'saveSuperOrder') {
              scrollToSection(changedForm);
            }
          }
          self.form[changedForm.attr('action') + 'Data'] = changedForm.serialize();
          return true;
        } else {
          changedForm.append('<div class="cart-errors"><strong>' + Translator.translate('A server error occurred') + '</strong>:<br ></div>');
          return false;
        }
      };

      this.prefillCustomerAddressData = function (data) {
        $('[id="address[otherAddress][city]"]').val(data.city).trigger('input');
        $('[id="address[otherAddress][postcode]"]').val(data.postcode).trigger('input');
        $('[id="address[otherAddress][street]"]').val(data.street_name).trigger('input');
        $('[id="address[otherAddress][houseno]"]').val(data.houseno).trigger('input');
        $('[id="address[otherAddress][addition]"]').val(data.addition).trigger('input');
        if (data.telephone) {
          data.telephone = data.telephone.replace("+", "");
        }
        $('[id="address[telephone]"]').val(data.telephone).trigger('input');
        $('[id="address[fax]"]').val(data.fax).trigger('input');
        //for business address
        $('[id="customer[otherAddress][company_street]"]').val(data.street_name).trigger('input');
        $('[id="customer[otherAddress][company_city]"]').val(data.city).trigger('input');
        $('[id="customer[otherAddress][company_postcode]"]').val(data.postcode).trigger('input');
        $('[id="customer[otherAddress][company_house_nr]"]').val(data.houseno).trigger('input');
        $('[id="customer[otherAddress][company_house_nr_addition]"]').val(data.addition).trigger('input');
      };

      this.prefillCustomerData = function (data) {
        // Business customers
        if (data.is_business == 1) {
          $('[id="customer[company_vat_id]"]').val(data.company_vat_id);
          $('[id="customer[company_coc]"]').val(data.company_coc);
          $('[id="customer[company_name]"]').val(data.company_name);
          $('[id="customer[company][search][company_coc]"]').val(data.company_coc);
          $('[id="customer[contractant_firstname]"]').val(data.contractant_firstname);
          $('[id="customer[contractant_middlename]]"]').val(data.middlename);
          $('[id="customer[contractant_lastname]"]').val(data.lastname);
          $('[id="customer[contractant_dob]"]').val(data.contractant_dob);
          $('[id="customer[contractant_id_number]"]').val(data.contractant_id_number);
          $('[id="customer[contractant_valid_until]"]').val(data.contractant_valid_until);
          $('[id="customer[contractant_issue_date]"]').val(data.contractant_issue_date);
          $('[id="customer[contractant_issuing_country]"]').val(data.contractant_issuing_country);
          $('[id="customer[contractant_id_type]"]').val(data.contractant_id_type);
          if (data.email) {
            $('[id="additional[email][0]"]').val((data.email).split(";")[0]).trigger('input');
          }
          $('[name="customer[contractant_gender]"]').filter('[value="' + data.contractant_gender + '"]').prop('checked', true);
          $('[name="customer[type]"]').filter('[value="' + '1' + '"]').prop('checked', true);
        } else {
          $('[name="customer[gender]"]').filter('[value="' + data.gender + '"]').prop('checked', true);
          $('[id="customer[firstname]"]').val(data.firstname);
          $('[id="customer[middlename]"]').val(data.middlename);
          $('[id="customer[lastname]"]').val(data.lastname);
          $('[id="customer[dob]"]').val(data.dob);
          if (data.email) {
            $('[id="additional[email][0]"]').val((data.email).split(";")[0]).trigger('input');
          }
          $('[id="customer[id_type]"]').val(data.id_type);
          $('[id="customer[issuing_country]"]').val(data.issuing_country);
          $('[id="customer[valid_until]"]').val(data.valid_until);
          $('[id="customer[issue_date]"]').val(data.issue_date);
          $('[id="customer[id_number]"]').val(data.id_number);
          $('[name="customer[type]"]').filter('[value="' + '0' + '"]').prop('checked', true);
        }
        $('[name="address[account_no]"]').val(data.account_no);
        $('[name="address[account_holder]"]').val(data.account_holder);

        // Privacy section
        if (!jQuery.isEmptyObject(data)) {
          if (data.privacy_sms == 1) $('[id="privacy[sms]"]').prop('checked', true); else $('[id="privacy[sms]"]').prop('checked', false);
          if (data.privacy_email == 1) $('[id="privacy[email]"]').prop('checked', true); else  $('[id="privacy[email]"]').prop('checked', false);
          if (data.privacy_mailing == 1) $('[id="privacy[mailing]"]').prop('checked', true); else $('[id="privacy[mailing]"]').prop('checked', false);
          if (data.privacy_telemarketing == 1) $('[id="privacy[telemarketing]"]').prop('checked', true); else $('[id="privacy[telemarketing]"]').prop('checked', false);
          if (data.privacy_number_information == 1) $('[id="privacy[number_information]"]').prop('checked', true); else $('[id="privacy[number_information]"]').prop('checked', false);
          if (data.privacy_phone_books == 1) $('[id="privacy[phone_books]"]').prop('checked', true); else $('[id="privacy[phone_books]"]').prop('checked', false);
          if (data.privacy_disclosure_commercial == 1) $('[id="privacy[disclosure_commercial]"]').prop('checked', true); else $('[id="privacy[disclosure_commercial]"]').prop('checked', false);
          if (data.privacy_market_research == 1) $('[id="privacy[market_research]"]').prop('checked', true); else $('[id="privacy[market_research]"]').prop('checked', false);
          if (data.privacy_sales_activities == 1) $('[id="privacy[sales_activities]"]').prop('checked', true); else $('[id="privacy[sales_activities]"]').prop('checked', false);
        }

      };

      this.prefillCustomer = function (logged, customerData, addressData) {
        var inputs;
        if (logged == 'true') {
          this.prefillCustomerAddressData(addressData);
          this.prefillCustomerData(customerData);
          $('#client_type').hide();
          this.switchClient(customerData.is_business);
          inputs = $('#saveCustomer').serializeObject();
          setOverview(inputs);
          this.setCreditCheck(inputs);
          jQuery('.tooltip.top').hide();
        } else {
          inputs = $('#saveCustomer').serializeObject();
          setOverview(inputs);
          this.prefillCustomerData(customerData);
          if (inputs.address) {
            prefillBillingAddress(inputs.address.otherAddress);
          }
        }

        this.enablePrefilledEmptyFields();
      };

      this.enablePrefilledEmptyFields = function () {
        jQuery('#business-input-address').find('[data-prefill], .validate-select').each(function (index, item) {
          var prefill = jQuery.trim(jQuery(item).data('prefill'));
          var val = jQuery.trim(jQuery(item).val());
          if ((
            jQuery(item).hasClass('required-entry') ||
            jQuery(item).hasClass('validate-select') ||
            jQuery(item).hasClass('validate-house-number')
          ) &&
            prefill.length <= 0 &&
            val.length <= 0 &&
            !jQuery(item).is('[name="customer[otherAddress][company_street]"]') &&
            !jQuery(item).is('[name="customer[otherAddress][company_city]"]')
          ) {
            jQuery(item).removeAttr('readonly').closest('.form-group.disabled').removeClass('disabled');
          }
        });
        if (jQuery('[name="customer[issue_date]"]').hasClass('mobile-only')){
          var issueDate =  '';
        }
        else {
          var issueDate = '#customer\\[issue_date\\],';
        }

        jQuery('#customer_info').find(
          '#address\\[telephone\\],' +
          '#address\\[otherAddress\\]\\[postcode\\],' +
          '#address\\[otherAddress\\]\\[houseno\\]'
        ).each(function (index, item) {
          var prefill = jQuery.trim(jQuery(item).data('prefill'));
          var val = jQuery.trim(jQuery(item).val());
          if (prefill.length <= 0 && val.length <= 0) {
            jQuery(item).removeAttr('readonly').removeAttr('disabled').closest('.form-group.disabled').removeClass('disabled');
          }
        });

        jQuery('#savePaymentMethod').find('[name="payment[account_holder]"]').each(function (index, item) {
          var val = jQuery.trim(jQuery(item).val());
          if (val.length <= 0) {
            jQuery(item).removeAttr('readonly').removeAttr('disabled')
              .closest('.input-wrapper').addClass('is-empty')
              .closest('.form-group.disabled').removeClass('disabled');
          }
        });
      }


      this.addGeneralValidationRules = function () {
        var self = this;
        $.each(this.options.generalValidationRules, function (index, rules) {
          $.each(rules, function (key, value) {
            if (key.match(/\[\]$/)) { // unknown length array => remove class from all fields
              key = key.slice(0, -2); // remove [] at the end
              $('[name^="' + key + '"]').addClass(value);
              return;
            }
            $('[id="' + key + '"]').addClass(value);
          });
        });
      };

      this.storesCache = {};

      this.loadStores = function (radio) {
        var checkout = this;
        var radioElement = $(radio);
        var searchField = radioElement.closest('.radio-item').find('.search-store');
        searchField.val('');
        if (radioElement.is(':checked') === false) {
          return;
        }
        var packageId = radioElement.data('packageId') ? radioElement.data('packageId') : '';
        if (this.storesCache[packageId] !== undefined) {
          this.setSearchStores(searchField, this.storesCache[packageId]);
          return;
        }
        var container = searchField.closest('.typeahead-input');
        container.addClass('typeahead-loading');
        $.ajax( this.checkoutController + 'stockStores', {
          data: {package_id: packageId},
          localCache: false,
          type: 'GET'
        }).done(function (res) {
          container.removeClass('typeahead-loading');
          if (res.hasOwnProperty('serviceError')) {
            showModalError(res.message, res.serviceError);
            return;
          }
          if (res.error) {
            showModalError(res.message);
            return;
          }
          checkout.storesCache[packageId] = res.list;
          checkout.setSearchStores(searchField, res.list);
        });
      };

      this.setSearchStores = function (searchField, list) {
        searchField.closest('.radio-popdown').find('.typeahead-popdown').html(list);
        searchField.focus();
      };

      this.searchStores = function (field, event) {
        var searchField = $(field);
        var searchQuery = $.trim(searchField.val()).replace('"', '').toLowerCase();
        var container = searchField.closest('.typeahead-input');
        var resultsContainer = container.find('.typeahead-popdown');
        if (container.hasClass('typeahead-loading') || searchQuery.length === 0) {
          resultsContainer.parent().removeClass('expanded');
          resultsContainer.children().removeClass('selected');
          return;
        }
        var items = resultsContainer.children('.typeahead-item:not(.typeahead-no-results)');
        if (resultsContainer.data('last') === searchQuery) {
          var selectedItem = items.filter('.selected').first();
          switch (event.which) {
            case 13: // Enter
              selectedItem.trigger('click');
              event.preventDefault();
              break;
            case 38: // Up
            case 40: // Down
              event.preventDefault();
              if (items.length === 0) {
                return;
              }
              var previousSelectedItem = selectedItem;
              if (event.which === 38) {
                selectedItem = selectedItem.prevAll(':not(.hidden)').first();
              } else {
                selectedItem = selectedItem.nextAll(':not(.hidden)').first();
              }
              if (selectedItem.length > 0) {
                previousSelectedItem.removeClass('selected');
                selectedItem.addClass('selected');
              }
              break;
            case 27: // Escape
              searchField.val('').trigger('keyup');
              event.preventDefault();
              break;
            default:
              break;
          }
          return;
        }
        items.addClass('hidden').removeClass('selected');
        var matchItems = items.filter('[data-search*="'+searchQuery+'"]').slice(0,10);
        matchItems.removeClass('hidden');
        if (matchItems.length === 0) {
          if (resultsContainer.children('.typeahead-no-results').length === 0) {
            resultsContainer.append('<li class="typeahead-item typeahead-no-results">' + Translator.translate('No stores found') + '</li>');
          } else {
            resultsContainer.find('.typeahead-no-results').removeClass('hidden');
          }
        } else {
          matchItems.filter(':not(.disabled)').first().addClass('selected');
          resultsContainer.find('.typeahead-no-results').addClass('hidden');
        }
        resultsContainer.parent().addClass('expanded');
        resultsContainer.data('last', searchQuery);
        // Move the container into the viewport if the results are out of bounds
        resultsContainer[0].scrollIntoView({
          behavior: 'smooth',
          block: 'nearest',
          inline: 'start'
        });
      };

      this.setStoreAddress = function (element) {
        var item = $(element);
        var container = item.closest('.radio-item');
        var packageId = container.find('input[data-package-id]').first().data('packageId');
        var stock = item.data('stock');
        if (!stock) {
          return;
        }
        var city = item.data('city');
        var storeId = item.data('storeId');
        if (packageId) {
          $('[id="delivery[pakket]['+packageId+'][store_id]"]').val(storeId).trigger('silentvalidation');
          $('[id="delivery[pakket]['+packageId+'][store]"]').val(city);
        } else {
          $('[id="delivery[pickup][store_id]"]').val(storeId).trigger('silentvalidation');
          $('[id="delivery[pickup][store]"]').val(city);
        }
        container.find('.store-selection-container').addClass('store-selection-found');
        container.find('.store-selection').html(city);
        container.find('.store-stock').html(Translator.translate(stock ? 'In stock' : 'Out of stock'));
        container.find('.typeahead-input').removeClass('expanded');
        container.find('.search-store').val('').blur();
      };

      this.addValidatorRules = function () {
        Validation.add('nl-postcode', this.validatorMessages['nl-postcode'], function (v) {
          return (v == '' || (v == null) || (v.length == 0) || /^[0-9][0-9]{3}\s?[a-zA-Z]{2}$/.test(v));
        });

        Validation.add('validate-date-future', this.validatorMessages['validate-date-future'], function (v, element) {
          var date = new Date();
          var input = v.split('-');
          var test = new Date(input[1] + '/' + input[0] + '/' + input[2]);

          var dateNow = new Date(date.getFullYear(), date.getMonth(), date.getDate());
          return test.valueOf() >= dateNow.valueOf();
        });

        Validation.add('validate-date-age', this.validatorMessages['validate-date-age'], function (v, element) {
          var reg = /^(0[1-9]|[12][0-9]|3[01])([-])(0[1-9]|1[012])\2(\d{4})$/;
          return reg.test(v);
        });

        Validation.add('validate-date-nl', this.validatorMessages['validate-date-nl'], function (v, element) {
          if ((v != '') && (v != null) && (v.length != 0)) {
            var reg = /^(0[1-9]|[12][0-9]|3[01])([-])(0[1-9]|1[012])\2(\d{4})$/;
            return reg.test(v);
          } else {
            return true;
          }
        });

        Validation.add('validate-legitimation-number-0', this.validatorMessages['validate-legitimation-number-0'], function (v, element) {
          v = v.toUpperCase();
          if (v.substring(0, 3) == 'NLD') {
            v = v.substring(3);
          }
          return (v.length === 8 && /^[0-9]+$/.test(v) && !/^(\d)\1*$/.test(v.substr(v.length - 5)));
        });

        Validation.add('validate-legitimation-number-1-4', this.validatorMessages['validate-legitimation-number-1-4'], function (v, element) {
          v = v.toUpperCase();
          if (v.substring(0, 3) == 'NLD') {
            v = v.substring(3);
          }
          //For documents types 0, 1, 2, 3 and 4 = eight numeral digits (no character) and the last five digits are not equal. Do not submit the characters "NLD" in front of the document number.
          return (v.length === 8 && /^[0-9]+$/.test(v) && !/^(\d)\1*$/.test(v.substr(v.length - 5)));
        });

        Validation.add('validate-legitimation-number-p', this.validatorMessages['validate-legitimation-number-p'], function (v, element) {
          //For document type P and Country Code <> NL = no format rules.
          return true;
        });

        Validation.add('validate-legitimation-number-p-nl', this.validatorMessages['validate-legitimation-number-p-nl'], function (v, element) {
          //For document type P and Country Code NL = nine digits, starts with a character ends with a number (in between can be character and numbers). Character (letter) "O" is not valid.
          return (/[A-Z]/.test(v.charAt(0)) && v.length === 9 && /[0-9]/.test(v.charAt(v.length - 1)) && -1 === v.toLowerCase().indexOf('o'));
        });

        Validation.add('validate-legitimation-number-r', this.validatorMessages['validate-legitimation-number-r'], function (v, element) {
          //For document type R = ten numeral digits (no characters).
          return (v.length === 10 && /^[0-9]+$/.test(v));
        });

        Validation.add('validate-legitimation-number-n', this.validatorMessages['validate-legitimation-number-n'], function (v, element) {
          //For document type N = nine digits, starts with a character ends with a number (in between can be character and numbers)
          return (/[A-Z]/.test(v.charAt(0)) && v.length === 9 && /[0-9]/.test(v.charAt(v.length - 1)));
        });

        Validation.add('validate-payment-single', Translator.translate('This is a required field.'), function (v, element) {
          return $(element).parent().find('[name="' + $(element).parent().attr('id') + '"]').val();
        });
      };

      this.acceptTerms = function (self) {
        if ($('#accept_loan').length > 0) {
          if ($('#accept_loan').prop('checked') == true && $('#accept_terms').prop('checked') == true) {
            $('#cart #submit').removeAttr('disabled');
          } else {
            $('#cart #submit').attr('disabled', 'disabled');
          }
        } else if ($('#accept_terms').prop('checked') == true) {
          $('#cart #submit').removeAttr('disabled');
        } else {
          $('#cart #submit').attr('disabled', 'disabled');
        }
      };

      this.switchLock = function (obj, state) {
        setPageLoadingState(true);
        var self = this;
        var reverse = state == 'locked' ? 'unlocked' : 'locked';
        $(obj).prop('disabled', 'disabled');
        if (state == 'unlocked') {
          if ($(obj).data('edited') == 1) {
            $('#order_unlock_modal').appendTo('body').modal();
            setPageLoadingState(false);
            return false;
          }
        }
        var callback = function () {
          $.post(self.cartController + 'lockSuperorder', {
            state: reverse,
            orderId: $(obj).data('order-id')
          }, function (response) {
            if (response.error) {
              if (response.hasOwnProperty('locked')) {
                self.orderLockModal(response.message);
              } else {
                showModalError(response.message);
              }
            } else {
              if (response.hasOwnProperty('edited') && response.edited == 1) {
                window.location = self.baseURL;
              } else {
                self.toggleLockState(obj, state, false);
                self.disableAdyen();
              }
            }
          }).always(function () {
            setPageLoadingState(false);
          });
        };
        if ($(obj).data('force')) {
          $.post(self.cartController + 'lockSuperorder', {
            state: 'locked',
            orderId: $(obj).data('order-id')
          }, function (response) {
            if (response.error) {
              if (response.hasOwnProperty('locked')) {
                self.orderLockModal(response);
              } else {
                showModalError(response.message);
              }
            } else {
              callback();
            }
            $('#cart-top .order-lock button').data('force', false);
          }).always(function () {
            setPageLoadingState(false);
          });
        } else {
          callback();
        }
      };

      this.initBankGenerator = function () {
        // Lets put this method empty for now to prevent handlebars error
      };

      this.setCreditCheck = function (inputs, id) {
        if (id == undefined) {
          id = '#overview-data';
        }
        var html = $(id).html();
        var phtml = $(html).not('#customer_privacy_overview').not('.privacy-separator').not('#customer_privacy_prefilled').not('hr');
        phtml.find('.edit-info').remove();
        $('#creditcheck-data').html(phtml);
      };

      this.searchCompany = function (obj) {
        var container = $(obj).closest('#customer-company-search');
        var cocField = container.find('input[id*="company_coc"]');
        container.find('#search-company-error').remove();
        cocField.trigger('blur');
        if (cocField.hasClass('validation-failed')) {
          cocField.trigger('focus');
          return;
        }
        $.post(
          this.checkoutController + 'searchCompany',
          {company_coc: $.trim(cocField.val())},
          function (result) {
            var serviceError = result.hasOwnProperty('serviceError');
            if (serviceError || result.error) {
              cocField.addClass('validation-failed').closest('.form-group').addClass('error');
              var errorType = serviceError ? 'error' : 'warning';
              if (serviceError) {
                result.message = Translator.translate('Something went wrong connecting to the COC service');
              }
              container.prepend(
                '<div id="search-company-error" class="col-sm-12">' +
                  '<div class="notification-banner ' + errorType + ' new-ui-elements">' +
                    '<div class="notification-banner-text">' +
                      '<div class="notification-banner-title">' + result.message + '</div>' +
                    '</div>' +
                    (serviceError ? '<div class="notification-banner-actions">' +
                      '<button class="btn btn-secondary">' +
                        '<span class="btn-content">' + Translator.translate('More info')+ '</span>' +
                      '</button>' +
                    '</div>' : '') +
                  '</div>' +
                '</div>'
              );
              container.find('#search-company-error button').click(function(){
                showModalError(result.serviceError, Translator.translate('More info'));
              });
              return;
            }
            cocField.removeClass('validation-failed').closest('form-group').removeClass('error');

            $('#business-input-address').hide();
            $('#customer-company-search .enter-yourself').show();

            /** Fill the text section **/
            var company_address_content = $('#company-address-content');
            $.each(result, function (field, value) {
              var find_field = company_address_content.find('[id*="{0}"]'.format(field));
              if (find_field.length) {
                find_field.text(value);
                find_field.trigger('change');
              }
            });
            $('#business-input-address').show();
            /** Fill the company inputs **/
            var company_address_inputs = $('#business-input-address');
            var company_legal_form = company_address_inputs.find('[id="customer[company_legal_form]"]');
            //company_address_inputs.find('[id="customer[company_vat_id]"]').val('');
            company_address_inputs.find('[id="customer[company_address][search][houseno]"]').val(result.houseno != '' ? result.houseno : '').trigger('change');
            company_address_inputs.find('[id="customer[company_address][search][postcode]"]').val(result.postcode != '' ? result.postcode : '').trigger('change');
            company_address_inputs.find('[id="customer[company_vat_id]"]').val(result.company_vat_id != '' ? result.company_vat_id : '').trigger('change');
            company_address_inputs.find('[id="customer[company_name]"]').val(result.company_name != '' ? result.company_name : '').trigger('change');
            company_address_inputs.find('[id="customer[company_coc]"]').val(result.k_v_k_number != '' ? result.k_v_k_number : '').trigger('change');
            company_address_inputs.find('[id="customer[company_date]"]').val(result.date != '' ? result.date : '').trigger('change');
            // company_address_inputs.find('[id="customer[otherAddress][company_street]"]').val(result.street != '' ? result.street : '').trigger('change');
            company_address_inputs.find('[id="customer[otherAddress][company_house_nr]"]').val(result.houseno != '' ? result.houseno : '').trigger('change');
            company_address_inputs.find('[id="customer[otherAddress][company_postcode]"]').val(result.postcode != '' ? result.postcode : '').trigger('change');
            // company_address_inputs.find('[id="customer[otherAddress][company_city]"]').val(result.city != '' ? result.city : '').trigger('change');
            $('#business-search-results-input').removeClass('hidden');
            $('#business-search-results-input-edit').trigger('click');
            cocField.val(result.k_v_k_number);
          }
        );
      };

      this.initSubmitButton = function(dataRef, error, buttonSelector) {
        return;//to avoid enable/disable next button
        var sectionSelector = '[data-ref=' + dataRef + ']';
        if (buttonSelector == undefined || buttonSelector.length == 0) {
          buttonSelector = sectionSelector + ' .section-footer button';
        }

        this.disableSubmitButton(buttonSelector);

        if (error == true) {
          return;
        }

        var checkout = this;
        var fields = [];
        var isSoho = checkout.isSoho();
        if (dataRef == 'customer-details') {
          if (isSoho) {
            fields = this.businesCustomerDetailsFields;
          } else {
            fields = this.customerDetailsFields;
          }
        } else if (dataRef == 'delivery') {

          //delivery method
          var deliveryMethodRadioButton = jQuery('[name="delivery[method]"]:checked');
          jQuery('[name="delivery[method]"], [name="fixed_delivery_method"]').off('.toggle_next_button_stat');
          jQuery('[name="delivery[method]"], [name="fixed_delivery_method"]').on('change.toggle_next_button_stat', function(e) {

            checkout.initDeliveryMethodFields(jQuery(this), buttonSelector);
          });

          if (deliveryMethodRadioButton.length > 0) {
            checkout.initDeliveryMethodFields(deliveryMethodRadioButton, buttonSelector);
          }

          return;
        } else if (dataRef == 'multiple-delivery-packages') {
          var packageCount = jQuery('#package_count').val();

          //packages delivery address
          if (packageCount > 0) {
            for (var i = 1; i <= packageCount; i++) {
              var deliveryMethodRadioButton = jQuery('[name="delivery[pakket][' + i + '][address]"]:checked');

              fields.push('[name="delivery[pakket][' + i + '][address]"]');

              if (deliveryMethodRadioButton.length > 0) {
                checkout.initDeliveryMethodFields(deliveryMethodRadioButton, buttonSelector);
              }
            }
          }

          jQuery(fields.join(',')).off('.toggle_next_button_stat');
          jQuery(fields.join(',')).on('change.toggle_next_button_stat', function(e) {
            checkout.initDeliveryMethodFields(jQuery(this), buttonSelector);
          });

          return;
        } else if (dataRef == 'payment') {
          //payment method
          jQuery('[name="payment[monthly_method]"], [name="billAddress[address_type]"]').off('.toggle_next_button_stat');
          jQuery('[name="payment[monthly_method]"], [name="billAddress[address_type]"]').on('change.toggle_next_button_stat', function(e) {

            checkout.initPaymentMethodFields(jQuery(this), buttonSelector);
          });

          if (jQuery('[name="payment[monthly_method]"]:checked').length > 0) {
            checkout.initPaymentMethodFields(jQuery('[name="payment[monthly_method]"]:checked'), buttonSelector);
          }
          if (jQuery('[name="billAddress[address_type]"]:checked').length > 0) {
            checkout.initPaymentMethodFields(jQuery('[name="billAddress[address_type]"]:checked'), buttonSelector);
          }
          return;
        } else if (dataRef == 'multiple-payment-methods') {
          fields.push(sectionSelector + ' .mobile-payment-validation-choice');
          fields.push('[name="payment[fixed_method]"]');

          jQuery(fields.join(',')).off('.toggle_next_button_stat');
          jQuery(fields.join(',')).on('change.toggle_next_button_stat', function(e) {
            checkout.initPaymentMethodFields(jQuery(this), buttonSelector);
          });

          jQuery(sectionSelector + ' input.mobile-payment-validation-choice:checked, [name="payment[fixed_method]"]:checked').trigger('change');

          return;
        } else if (dataRef == 'billing-address-type') {
          fields.push(sectionSelector + ' [name="billAddress[address_type_fixed]"]');
          jQuery(fields.join(',')).off('toggle_next_button_stat');
          jQuery(fields.join(',')).on('change.toggle_next_button_stat', function(e) {
            checkout.initPaymentMethodFields(jQuery(this), buttonSelector);
          });

          jQuery(sectionSelector + ' [name="billAddress[address_type_fixed]"]:checked').trigger('change');
          return;
        } else if (dataRef == 'number_device_section') {
          jQuery(sectionSelector + ' .required-entry:visible').each(function (i, item) {
            fields.push({id: jQuery(item).attr('id')});
          });

          if (fields.length <= 0) {
            checkout.enableSubmitButton(buttonSelector);
          }
        }
        else if (dataRef == 'credit_check') {
          this.disableSubmitButton(buttonSelector);
        }
        else if (dataRef == 'order-overview') {
          if ((jQuery("input[id='accept_terms']").prop('checked') == false))
          {
            this.disableSubmitButton(buttonSelector);
          } else if ((jQuery("#accept_loan").length > 0) && (jQuery("#accept_loan").prop('checked') == false)) {
            this.disableSubmitButton(buttonSelector);
          } else {
            this.enableSubmitButton(buttonSelector);
          }
        }
        else if (dataRef == 'save_contract') {
          if (jQuery("input[name^='contract[signed_']").prop('checked') == false)
          {
            this.disableSubmitButton(buttonSelector);
          } else {
            this.enableSubmitButton(buttonSelector);
          }
        }
        else {
          // Fallback on other steps
          this.enableSubmitButton(buttonSelector);
        }

        this.collectFieldsAndToggleButton(fields, buttonSelector);
      };

      this.collectFieldsAndToggleButton = function (fields, buttonSelector) {
        if (fields.length > 0) {
          var fieldIds = [];

          for (var i = 0; i < fields.length; i++) {
            fieldIds.push('#' + fields[i].id.replace(/\[/g, '\\[').replace(/]/g, '\\]').replace(/\./g, '\\.'));
          }

          if (fieldIds.length > 0) {
            fieldIdsJoin = jQuery(fieldIds.join(','));
            fieldIdsJoin.off('.toggle_next_button_stat');
            fieldIdsJoin.on('blur.toggle_next_button_stat change.toggle_next_button_stat silentvalidation.toggle_next_button_stat', function (e) {
              checkout.toggleButtonState(buttonSelector, fieldIdsJoin);
            });
            checkout.toggleButtonState(buttonSelector, fieldIdsJoin);
          }
        }
      }

      //payment fields collecting
      this.initPaymentMethodFields = function (radiobutton, buttonSelector) {
        var val = radiobutton.val();
        var name = radiobutton.attr('name');
        var fields = [];

        var monthlyDirectDebit = jQuery('[name="payment[monthly_method]"]');
        if (monthlyDirectDebit.is(':checked') && monthlyDirectDebit.val() == 'direct_debit') {
          fields = fields.concat([
            {id: 'direct-account-no'},
            {id: 'direct-account-holder'}
          ]);
        }

        var billDistributionMethod = jQuery('[name="payment[bill_distribution_method]"]');
        if (billDistributionMethod) {
          fields = fields.concat([
            {id: 'bill_distribution_method'},
          ]);
        }

        if (jQuery('[name^="payment[direct_mobile_"]:checked').length > 0) {
          fields = fields.concat([
            {id: 'mobile-account-no'},
            {id: 'mobile-account-holder'}
          ]);
        }

        if (val == 'multiple_payment' && name == 'payment[monthly_method]') {
          this.initSubmitButton('multiple-payment-methods', false, buttonSelector);
          return;
        }

        if (jQuery('[name^="payment[direct_mobile_"]:checked').length > 0) {
          fields = fields.concat([
            {id: 'mobile-account-no'},
            {id: 'mobile-account-holder'}
          ]);
        }

        if (jQuery('[name="payment[monthly_method]"]:checked').length > 0 &&
            jQuery('[name="payment[monthly_method]"]:checked').val() == 'multiple_payment' &&
            jQuery('[name="payment[fixed_method]"]:checked').length > 0 &&
            jQuery('[name="payment[fixed_method]"]:checked').val() == 'direct_debit_fixed_different') {
          fields = fields.concat([
            {id: 'fixed-account-no-different'},
            {id: 'fixed-account-holder-different'}
          ]);
        }

        if (name == "billAddress[address_type]") {
          if (val == 'diffrent_address') {
            fields = fields.concat([
              {id: 'billAddress-postcode'},
              {id: 'billAddress-houseno'},
              {id: 'billAddress-street'},
              {id: 'billAddress-city'}
            ]);
          } else if (val == 'multiple_address') {
            this.initSubmitButton('billing-address-type', false, buttonSelector);
            return;
          }
        }

        if (jQuery('[name="billAddress[address_type_fixed]"]:checked').length > 0 &&
            jQuery('[name="billAddress[address_type_fixed]"]:checked').val() == 'diffrent_address') {
          fields = fields.concat([
            {id: 'billAddress-postcode'},
            {id: 'billAddress-houseno'},
            {id: 'billAddress-street'},
            {id: 'billAddress-city'}
          ]);
        }

        if (fields.length > 0) {
          this.collectFieldsAndToggleButton(fields, buttonSelector);
        }
      }

      //delivery method fields collecting
      this.initDeliveryMethodFields = function (radiobutton, buttonSelector) {
        var val = radiobutton.val();
        var name = radiobutton.attr('name');
        var fields = [];

        //set delivery date field if exists
        if (jQuery('[name="fixed_delivery_method"]:checked')) {
          var dataDeliveryIndex = jQuery('[name="fixed_delivery_method"]:checked').data('deliverydate-index');
          if (dataDeliveryIndex > 0) {
            fields.push({id: 'delivery_wish_date_' + dataDeliveryIndex});
          }
        }

        if (/delivery\[pakket\]\[([0-9]+)\]\[address\]/g.test(name)) {
          var packageCount = jQuery('#package_count').val();
          var packageId = radiobutton.data('package-id');
          for (var i = 1; i <= packageCount; i++) {
            var packageRadioButton = jQuery('[name="delivery[pakket][' + i + '][address]"]:checked');
            if (packageRadioButton.is(':checked')) {
              if (packageRadioButton.val() == 'store') {
                fields = fields.concat([
                  {id: 'delivery[pakket][' + i + '][store_id]'}
                ]);
              } else if (packageRadioButton.val() == 'other_address') {
                fields = fields.concat([
                  {id: 'delivery[pakket][' + i + '][otherAddress][postcode]'},
                  {id: 'delivery[pakket][' + i + '][otherAddress][houseno]'},
                  {id: 'delivery[pakket][' + i + '][otherAddress][city]'},
                  {id: 'delivery[pakket][' + i + '][otherAddress][street]'}
                ]);
              }
            }
          }
        } else if (name = 'delivery[method]' && val == 'other_address') {
          fields = fields.concat([
            {id: 'otherAddress[postcode]'},
            {id: 'otherAddress[houseno]'},
            {id: 'otherAddress[street]'},
            {id: 'otherAddress[city]'}
          ]);
        } else if (val == 'pickup') {
          if (/delivery\[pakket\]\[([0-9]+)\]\[address\]/g.test(name)) {
            var packageId = radiobutton.data('package-id');
            fields = fields.concat([
              {id: 'delivery[pakket][' + packageId + '][store_id]'}
            ]);
          } else {
            fields = fields.concat([
              {id: 'delivery[pickup][store_id]'}
            ]);
          }
        } else if (val == 'split') {

          this.initSubmitButton('multiple-delivery-packages', false, buttonSelector);
          return;
        } else if (val == 'store') {

          if (/delivery\[pakket\]\[([0-9]+)\]\[address\]/g.test(name)) {
            var packageId = radiobutton.data('package-id');

          }
        } else {

          this.enableSubmitButton(buttonSelector);
        }

        if (fields.length > 0) {
          this.collectFieldsAndToggleButton(fields, buttonSelector);
        }
      }

      this.isSoho = function () {
        return jQuery('select[name="customer[type]"]').val() == 1;
      }

      this.getPackageId = function(element) {
        return jQuery(element).closest('.np-package').data('package-id');
      };

      this.initOffPortingNrFields = function () {
        return;//to avoid enable/disable next button
        jQuery('#saveNumberPorting .np-required-check').on('change', function () {
          var packageId = checkout.getPackageId(this);
          var sectionSelector = '.section[data-ref=number_porting]';
          var buttonSelector = sectionSelector + ' .section-footer button';

          if (!jQuery(this).is(':checked')) {
            var fieldsOff = [
              'numberporting_' + packageId + '_customerno_mobile',
              'numberporting_' + packageId + '_client_id',
              'numberporting_' + packageId + '_sms_code',
              'numberporting_' + packageId + '_sms_mobile'
            ];

            if (fieldsOff.length > 0) {
              for (var i = 0; i < fieldsOff.length; i++) {
                jQuery('#' + fieldsOff[i]).off('.toggle_next_button_stat');

                var ii = checkout.currentNumberPortingFieldsToValid.indexOf('#' + fieldsOff[i]);
                if (ii != -1) {
                  checkout.currentNumberPortingFieldsToValid.splice(ii, 1);
                }
              }
            }

            if (checkout.currentNumberPortingFieldsToValid.length > 0) {
              checkout.toggleButtonState(buttonSelector, jQuery(checkout.currentNumberPortingFieldsToValid.join(',')));
            }
          }

          if (checkout.currentNumberPortingFieldsToValid.length == 0) {
            checkout.enableSubmitButton(buttonSelector);
          }
        });
      };

      this.currentNumberPortingFieldsToValid = [];

      this.initSavePortingSubmitButton = function (packageId) {
        return;//to avoid enable/disable next button
        var sectionSelector = '.section[data-ref=number_porting]';
        var buttonSelector = sectionSelector + ' .section-footer button';
        var selectedRadio = jQuery('#saveNumberPorting [name="numberporting[' + packageId + '][choice]"]:checked');
        var selectedValue = !(typeof selectedRadio == 'undefined') ? jQuery(selectedRadio).val() : 0;
        var selectedcheckbox = jQuery('#saveNumberPorting [name="numberporting[' + packageId + '][check]"]').is(":checked");

        var fields = [];
        var fieldsOff = [];
        if (selectedcheckbox) {
          if (selectedValue == 0) {
            fields = [
              'numberporting_' + packageId + '_sms_mobile',
              'numberporting_' + packageId + '_sms_code',
            ];
            fieldsOff = [
              'numberporting_' + packageId + '_customerno_mobile',
              'numberporting_' + packageId + '_client_id'
            ];
          } else {
            fields = [
              'numberporting_' + packageId + '_customerno_mobile',
              'numberporting_' + packageId + '_client_id'
            ];
            fieldsOff = [
              'numberporting_' + packageId + '_sms_mobile',
              'numberporting_' + packageId + '_sms_code',
            ];
          }

          if (fieldsOff.length > 0) {
            checkout.offNumberPortingFields(fieldsOff);
            fieldIdsJoinOff = jQuery(fieldsOff.join(','));
            fieldIdsJoinOff.off('.toggle_next_button_stat');
          }

          var fieldIds = [];
          if (fields.length > 0) {
            checkout.setNumberPortingFields(fields);

            if (checkout.currentNumberPortingFieldsToValid.length > 0) {
              fieldIdsJoin = jQuery(checkout.currentNumberPortingFieldsToValid.join(','));
              checkout.toggleButtonState(buttonSelector, fieldIdsJoin);
              fieldIdsJoin.off('.toggle_next_button_stat');
              jQuery('body').on('blur.toggle_next_button_stat change.toggle_next_button_stat silentvalidation.toggle_next_button_stat', checkout.currentNumberPortingFieldsToValid.join(','), function (e, param) {
                checkout.toggleButtonState(buttonSelector, fieldIdsJoin);
              });

              checkout.customActionNumberPortingFields(checkout.currentNumberPortingFieldsToValid);
            }
          }
        }
      };

      this.customActionNumberPortingFields = function (fields) {
        for (var i = 0; i < fields.length; i++) {
          if (/numberporting_([0-9]+)_sms_code/g.test(jQuery(fields[i]).attr('id'))) {
            jQuery(fields[i]).on('change.toggle_next_button_stat', function () {
              jQuery(this).removeClass('sms-code-valid').trigger('silentvalidation');
            });
          }
        }
      }

      this.setNumberPortingFields = function (fields) {
        for (var i = 0; i < fields.length; i++) {
          if (checkout.currentNumberPortingFieldsToValid.indexOf('#' + fields[i]) == -1) {
              checkout.currentNumberPortingFieldsToValid.push('#' + fields[i]);
          }
        }
      }

      this.offNumberPortingFields = function (fields) {
        for (var i = 0; i < fields.length; i++) {
          var ii = checkout.currentNumberPortingFieldsToValid.indexOf('#' + fields[i]);
          if (ii != -1) {
              checkout.currentNumberPortingFieldsToValid.splice(ii, 1);
          }
        }
      }

      this.toggleButtonState = function(buttonSelector, fields) {
        var validationErrors = 0;
        if(fields.length > 0) {
          fields.each (function(i, field) {
            var elementId = jQuery(field).attr('id');
              if(!Validation.validate(document.getElementById(elementId), {renderError: false}) ||
                jQuery(field).hasClass('forced-validation-failed')
              ) {
                validationErrors++;
              }
              if (elementId == 'customer[valid_until]' ||
                /delivery\[pakket\]\[([0-9]+)\]\[otherAddress\]\[postcode\]/g.test(elementId) ||
                /delivery\[pakket\]\[([0-9]+)\]\[otherAddress\]\[houseno\]/g.test(elementId) ||
                /delivery\[pakket\]\[([0-9]+)\]\[store\]/g.test(elementId) ||
                /delivery\[pakket\]\[([0-9]+)\]\[store_id\]/g.test(elementId) ||
                elementId == 'address[otherAddress][postcode]' ||
                elementId == 'address[otherAddress][houseno]' ||
                /numberporting_([0-9]+)_client_id/g.test(elementId) ||
                elementId == 'otherAddress[postcode]' ||
                elementId == 'otherAddress[houseno]' ||
                elementId == 'delivery[pickup][store_id]' ||
                elementId == 'fixed-account-no-different' ||
                elementId == 'fixed-account-holder-different' ||
                jQuery(field).hasClass('required-entry')
              ) {
                if (Validation.get('IsEmpty').test(jQuery(field).val())) {
                  validationErrors++;
                }
              } else if (/numberporting_([0-9]+)_sms_code/g.test(elementId)) {
                if (Validation.get('IsEmpty').test(jQuery(field).val()) || !jQuery(field).hasClass('sms-code-valid')) {
                  validationErrors++;
                }
                if(localStorage.getItem('sms-code-valid')){
                  validationErrors = 0;
                }
              }
          });
        }
        if (validationErrors > 0) {
          this.disableSubmitButton(buttonSelector);
        } else {
          this.enableSubmitButton(buttonSelector);
        }
      }

      this.disableSubmitButton = function(buttonSelector) {
        return;//to avoid enable/disable next button
        jQuery(buttonSelector).attr('disabled', true);
        jQuery('.create-offer-modal').addClass('disabled');
      };

      this.enableSubmitButton = function(buttonSelector) {
        return;//to avoid enable/disable next button
        jQuery(buttonSelector).removeAttr('disabled');
        jQuery('.create-offer-modal').removeClass('disabled');
      };

      this.updateLegitimationValidation = function (id) {
        var container = $(id).closest('.identification-container');
        var identificationType = container.find('[name$="id_type]"]');
        if (identificationType.val() !== 'P') {
          return;
        }
        var identificationNumber = container.find('[name$="id_number]"]');
        var countryType = container.find('[name$="issuing_country]"]');
        this.addValidationClass('validate-legitimation-number-' + (countryType.val() === 'NL' ? 'p-nl' : 'p'), identificationNumber);
      };

      this.customerDetailsFields = [
        {id : 'address[telephone]'},
        {id : 'additional[email][0]'},
        {id : 'customer[gender]'},
        {id : 'customer[firstname]'},
        {id : 'customer[lastname]'},
        {id : 'customer[dob]'},
        {id : 'customer[id_type]'},
        {id : 'customer[id_number]'},
        {id : 'customer[valid_until]'},
        {id : 'customer[issuing_country]'},
        {id : 'address[otherAddress][country_id]'},
        {id : 'address[otherAddress][postcode]'},
        {id : 'address[otherAddress][houseno]'},
        {id : 'address[otherAddress][street]'},
        {id : 'address[otherAddress][city]'},
        {id : 'additional[ziggo_telephone]'},
        {id : 'additional[ziggo_email]'},
        {id : 'customer[issue_date]'}
      ];

      this.businesCustomerDetailsFields = [
        {id : 'customer[company_coc]'},
        {id : 'customer[company_name]'},
        {id : 'customer[company_date]'},
        {id : 'customer[company_vat_id]'},
        {id : 'customer[company_legal_form]'},
        {id : 'customer[otherAddress][company_postcode]'},
        {id : 'customer[otherAddress][company_house_nr]'},
        {id : 'customer[otherAddress][company_street]'},
        {id : 'customer[otherAddress][company_city]'},
        {id : 'customer[contractant_firstname]'},
        {id : 'customer[contractant_gender]'},
        {id : 'customer[contractant_lastname]'},
        {id : 'customer[contractant_dob]'},
        {id : 'address[telephone]'},
        {id : 'additional[email][0]'},
        {id : 'customer[contractant_id_type]'},
        {id : 'customer[contractant_id_number]'},
        {id : 'customer[contractant_valid_until]'},
        {id : 'customer[contractant_issue_date]'},
        {id : 'customer[contractant_issuing_country]'},
        {id : 'address[otherAddress][country_id]'},
        {id : 'address[otherAddress][postcode]'},
        {id : 'address[otherAddress][houseno]'},
        {id : 'address[otherAddress][street]'},
        {id : 'address[otherAddress][city]'},
        {id : 'additional[ziggo_telephone]'},
        {id : 'additional[ziggo_email]'}
      ];
    }

    Checkout.prototype = original.prototype;
    Checkout.prototype.constructor = Checkout;
    return Checkout;
  })(Checkout);

  Checkout.prototype.options.activesteps = {
    0: 'saveNumberPorting'
  };
  Checkout.prototype.options.checkoutValidationRules = {id: []};
  Checkout.prototype.options.generalValidationRules = {
    0: {
      'customer[firstname]': 'required-entry validate-name validate-length maximum-length-12'
    },
    1: {
      'customer[firstname]': 'required-entry validate-name validate-length maximum-length-12'
    },
    '-1': {
      'customer[gender]': 'required-entry',
      'customer[lastname]': 'required-entry validate-name validate-length maximum-length-60',
      'customer[dob]': 'required-entry validate-date-nl validate-min-age validate-max-age',
      'address[telephone]': 'validate-phone-number validate-foreign-number required-entry',
      'address[account_no]': 'required-entry',
      'address[account_holder]': 'required-entry validate-length maximum-length-40',
      'additional[email][0]': 'required-entry validate-email validate-backend-email',
      'additional[telephone][]': 'validate-porting-number validate-foreign-number required-entry',
      'address[search][houseno]': 'validate-house-number',
      'searchOtherAddress[houseno]': 'validate-house-number',
      'direct-account-no': 'required-entry',
      'direct-account-holder': 'required-entry validate-length maximum-length-40',
      'payment[account_no]': 'required-entry',
      'payment[account_holder]': 'required-entry validate-length maximum-length-40',
      'customer[company_name]': 'required-entry validate-name',
      'customer[company_date]': 'required-entry validate-date-nl validate-date-past',
      'customer[contractant_lastname]': 'required-entry validate-name validate-length maximum-length-60',
      'customer[contractant_dob]': 'required-entry validate-date-nl validate-min-age validate-max-age'
    }
  };
  Checkout.prototype.showVodError = function (doShow, vodMessage, type, showRetry) {
    //var modal = $(this.modalId);
    var modal = jQuery("#polling-modal");
    if (doShow == true && vodMessage != undefined) {
      if (!$(modal).is(':visible')) {
        modal.appendTo('body').modal();
      }
      Polling.hide();
      // Show error message
      if (showRetry == 0) {
        var button = '<div class="btn" onClick="showModalError(\'' + vodMessage + '\',\'' + Translator.translate('Error message contents:') + '\');">' + Translator.translate('More info') + '</div>';
      } else {
        var button = '<div class="btn" onClick="showModalError(\'' + vodMessage + '\',\'' + Translator.translate('Error message contents:') + '\', false, false, function(){Polling.retry(jQuery(\'#polling-modal .modal-footer\'))},\'' + Translator.translate('Try again') + '\');">' + Translator.translate('More info') + '</div>';
      }
      var message = '<strong>' + Translator.translate('Something went wrong') + '!</strong> ' + Translator.translate('Please contact support or check "more info" for full details.');
      showValidateNotificationBar(message, 'error', button);
    } else {
      // Hide all modals on success
      modal.find('.modal-vod-error-message').html('');
      modal.find('.modal-title-processing').removeClass('hidden');
      modal.find('.modal-title-error').addClass('hidden');
      modal.find('#progress-bar').removeClass('hidden');
      modal.find('#vod-popup-error').addClass('hidden');
      modal.find('.modal-footer').addClass('hidden');
    }
  };
})(jQuery);


(function( $ ) {
  "use strict";

  $.fn.checkout = function( options ) {
    var self = this;
    var settings = $.extend( {
      'buttonOffsetBottom' : 18,
      'buttonOffsetTop' : 71,
      'bulletDistance' : 30
    }, options);

    var generalValidationRules = {
      '0': {
        'customer[firstname]': 'required-entry validate-name',
        'customer[middlename]': 'validate-name validate-length maximum-length-100',
        'customer[lastname]': 'required-entry validate-name validate-length maximum-length-60'
      },
      '1': {
        'customer[firstname]': 'required-entry validate-name',
        'customer[middlename]': 'validate-name validate-length maximum-length-100',
        'customer[lastname]': 'required-entry validate-name validate-length maximum-length-60',
        'customer[contractant_firstname]': 'required-entry validate-name',
        'customer[contractant_lastname]': 'required-entry validate-name validate-length maximum-length-60',
        'customer[contractant_middlename]': 'validate-name validate-length maximum-length-100'
      },
      '-1': {
        'customer[gender]': 'required-entry',
        'customer[lastname]': 'required-entry validate-name validate-length maximum-length-60',
        'customer[dob]': 'required-entry validate-date-nl validate-min-age validate-max-age',
        'address[telephone]': 'validate-phone-number validate-foreign-number required-entry',
        'direct-account-no': 'required-entry validate-iban-number validate-blacklist-check',
        'direct-account-holder': 'required-entry validate-length maximum-length-40',
        'payment[account_no]': 'required-entry',
        'payment[account_holder]': 'required-entry validate-length maximum-length-40',
        'additional[email][0]': 'required-entry validate-email validate-backend-email',
        'additional[telephone][]': 'validate-porting-number validate-foreign-number required-entry',
        'address[search][houseno]': 'validate-house-number',
        'searchOtherAddress[houseno]': 'validate-house-number',
        'customer[company_name]': 'required-entry validate-name',
        'customer[company_date]': 'required-entry validate-date-nl validate-date-past',
        'customer[contractant_lastname]': 'required-entry validate-name validate-length maximum-length-60',
        'customer[contractant_dob]': 'required-entry validate-date-nl validate-min-age validate-max-age'
      }
    };

    const removeGeneralValidationRules = function () {
      var self = this;
      $.each(generalValidationRules, function (index, rules) {
        $.each(rules, function (key, value) {
          if (key.match(/\[\]$/)) { // unknown length array => remove class from all fields
              key = key.slice(0, -2); // remove [] at the end
              $('[name^="' + key + '"]').removeClass(value);
              return;
          }
          value += ' validation-failed validation-passed';
          $('[id="' + key + '"]').removeClass(value);
        });
      });
    };

    const setValidationRules = function(type){
      var self = this;

      $.each(generalValidationRules, function (index, rules) {
        if (type == index) {
          $.each(rules, function (key, value) {
            $('[id="' + key + '"]').addClass(value);
          });

        } else if (index != '-1') {
          $.each(rules, function (key, value) {
            $('[id="' + key + '"]').removeClass(value);
          });
        }
      });

      if ($('[id="customer[logged]"]').val() == 'false') {
        if (type == 0) {
          updateDocumentExpiryDate($('[id="customer[id_type]"]'));
        } else if (type == 1) {
          updateDocumentExpiryDate($('[id="customer[contractant_id_type]"]'));
        }
      }
    };

    const calculateBarSettings = function(){
      settings.barHeight = $(window).height() - 150;
      settings.bar = settings.bullets.find('.breadcrumb-bar .bar');
      settings.barActive = settings.bullets.find('.breadcrumb-bar .bar-active');
    };

    const getAvailableSections = function() {
      let foundItems = [];
      settings.bullets.find('.vertex').each( function(i, el) {
        const section = $(el).data('section');
        const pairedSection = $(self).find('*[data-ref='+section+']');

        if (pairedSection.length > 0) {
          let state;
          if (pairedSection.hasClass('skip')) {
            $(el).addClass('skip');
            state = 'skipped'
          } else if( $(el).hasClass('completed')) {
            state = 'completed'
          } else if( $(el).hasClass('active')) {
            state ='active';
          } else {
            state = 'closed';
          }
          foundItems.push({
            section: pairedSection,
            name: section,
            bullet: el,
            state: state
          });
        }
      });
      return foundItems;
    };
    settings.sections = getAvailableSections();

    const drawForm = function() {
      settings.sections.each(function(n){
        if (n.state === 'completed') {
          $(n.section).show();
          $(n.section).find('.summary').show();
          $(n.section).find('.form').hide();
          $(n.section).find('.section-footer').hide();
        } else if (n.state === 'skip') {
          $(n.section).hide();
        } else if (n.state === 'active') {
          showLoanBanner(n);
          $(n.section).show();
          $(n.section).find('.summary').hide();
          $(n.section).find('.form').show();
          $(n.section).find('.section-footer').show();
        } else {
          $(n.section).hide();
        }
      });
    };

    const showHardwareDetails = function(resp) {
      if (isDeliveryLSP()) {
        jQuery('.hardware-details').hide();
        return false;
      } else {
        jQuery('.hardware-details').show();
        return true;
      }
    };

    const isDeliveryLSP = function () {
      return (jQuery('[name="fixed_delivery_method"]:checked').val() == 'LSP');
    };

    const isDeliverySHOP = function () {
      return (jQuery('[name="fixed_delivery_method"]:checked').val() == 'SHOP');
    };

    const showLoanBanner = function(n) {
      $('.banner-after-summary').remove();
      if($('#show_loan_banner').val() == '1' && $(n.bullet).data("loanbanner")) {
        $(n.section.last()).prepend('<div class="loan-banner banner-after-summary"><img src="/skin/frontend/vznl/default/images/geld-lenen-kost-geld.svg" class="img-responsive" /></div>');
        drawBullets();
      }
    };

    /* Draw the bullets on the page */
    const drawBullets = function(){
      calculateBarSettings();
      $('.vertex').addClass('hidden');

      const sections = {
        sell: ['number_porting', 'customer-details', 'delivery', 'payment', 'order-overview'],
        deliver: ['credit_check', 'number_device_section', 'save_contract']
      };

      const includes = function(container, value) {
        const pos = container.indexOf(value);
        return pos >= 0;
      };

      const sectionsPerPart = {
        sell: settings.sections.filter(function(s) { return includes(sections.sell, s.name)}),
        deliver: settings.sections.filter(function(s) { return includes(sections.deliver, s.name)})
      };

      const positionBullets = function(n, index) {
        if (!isDeliverActive && index === (totalBullets - 1) && $(n.section).hasClass('hidden')) {
          setTimeout(function() {
            settings.sections = getAvailableSections();
            drawBullets();
            drawForm();
          }, 0);
        }
        // Start positioning bullets
        let pos = $(n.section).offset().top - settings.buttonOffsetTop;

        const minPos = index * settings.bulletDistance;
        const maxPos = settings.barHeight - ((totalBullets - index - 1) * settings.bulletDistance);

        if (!$(n.bullet).hasClass('active') || pos >= maxPos) {
          pos = maxPos;
        } else if (pos <= minPos) {
          pos = minPos;
        }

        $(n.bullet).css('top', pos).removeClass('hidden');
        // End positioning bullets

        const currentSection = $(n.bullet).hasClass('active') && !$(n.bullet).hasClass('completed');
        // Adjust length of bar to last bullet
        if (currentSection) {
          $(settings.barActive).height(pos);
        }
        // Adjust length of bar to last bullet
        if (totalBullets === index + 1) {
          $(settings.bar).height(pos);
        }
      };

      // we should check which section is active right now
      const isDeliverActive = !!sectionsPerPart.deliver.filter(function(s) {
        return s.state === 'active'
      }).length;

      let totalBullets;
      if (isDeliverActive) {
        totalBullets = sectionsPerPart.deliver.length;
        sectionsPerPart.deliver.each(positionBullets);
      } else {
        totalBullets = sectionsPerPart.sell.length;
        sectionsPerPart.sell.each(positionBullets);
      }
    };

    const registerButtonClicks = function() {
      var isCustomer = $('#isCustomer').val();
      var isStore = $('#current_website_code').val();
      var isProspect = $('#isprospect').val();
      var isMobileCustomer = $('#ismobilecustomer').val();
      var isMobileOrder = $('#ismobileorder').val();
      var isFixedCustomer = $('#isfixedcustomer').val();
      var isFixedOrder = $('#isfixedorder').val();
      var isBusiness = $('#isbusiness').val();
      var isConverged = isMobileOrder && isFixedOrder;

      if(isCustomer && isProspect == 0 && !isBusiness){
         setValidationRules(0);
      } else {
         setValidationRules(1);
      }

      settings.sections.each(function(n) {
        // Go to next step
        self.hideContract = false;
        self.LSPhideContract = false;
        //$(n.section).find('.section-footer button').click(function(e){
        var currentXhr = null;
        $(n.section).on('click','.section-footer button', function(e) {
          e.preventDefault();
          // Set form in loading state
          const form = $(n.section).find('.form');
          const endpoint = $(form).data('url');
          const form_id = $(form).attr('id');
          const form_validate = $(form).data('validate');
          const validate_response = "true";
          let formFields = {}
          if(form_validate == 'validate'){
            if(!window["validate"+form_id](form)){
              scrollToFirstError(form.find('.validation-failed').first().closest('.form-group'));
              return false;
            }
          }
          $(form).find('textarea,input,select').each(function(i, formField) {
            if($(formField).is(':radio')) {
              if($(formField).is(':checked')) {
                formFields[$(formField).attr('name')] = $(formField).val();
              }
            } else {
              formFields[$(formField).attr('name')] = $(formField).val();
            }
            if (form_id == 'saveNumberPorting') {
              var id=$(formField).attr('id');
              if ($(formField).val() == "") {
                $("."+id+"-summary").parent().hide();
              } else {
                $("."+id+"-summary").parent().show();
              }
            }

          }).filter('[oninput*="checkout.summaryDataSynchronization"]').trigger('input');

          $('[id^=privacy],[id^="contract[sendmail_"]').each(function(i,formField) {
            if($(formField).is(':checked'))
              formFields[$(formField).attr('name')] = 1;
            else
              formFields[$(formField).attr('name')] = 0;
          });

          formFields['current_step'] = form_id;

          if(form_id == 'saveOverview' || form_id == 'saveContract' || form_id == 'saveSuperOrder')
          {
            Polling.show();
          }

          var that = this;

          if ($(this).attr('id') == 'create-offer') {
            self.isOffer = 1;
            formFields['offer_email'] = $('#confirm-offer-form input[name="offer[email_send]"]').val();

          }
          // Add loading class to button
          $(that).addClass('loading');
          formFields['is_offer'] = self.isOffer;
          currentXhr = $.ajax({
            type: "POST",
            method: "POST",
            url: endpoint,
            data: formFields,
            beforeSend: function (xhr) {
              const xcsrf = $('input[name="post_key"]').val();
              xhr.setRequestHeader("PostKey", xcsrf);
              xhr.withCredentials = true;

              if (currentXhr != null) {
                xhr.abort();
                currentXhr.setRequestHeader("PostKey", xcsrf);
              }
            }
          }).success(function(resp){
            if (resp.error == true) {
              window.checkout.disableSubmitButton(jQuery(n.section).find('.section-footer button'));
              if (!(resp.fields)) {
                showModalError(resp.message);
              }
              else if (resp.fields) {
                $.each(resp.fields, function(key, value) {
                  var noticKey = key.replace(/[0-9~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi,'');
                  var noticErrorKey = noticKey + '-error';
                  var elements = document.getElementsByName(key);
                  if (elements[0]) {
                    var noticId = elements[0].getAttribute( 'id' );
                    jQuery('#' + noticId).removeClass('validation-passed').addClass('validation-failed');
                    Validation.createAdvice('required-entry', document.getElementById(noticId), false, value);
                    Validation.showAdvice(document.getElementById(noticId), 'required-entry', value);
                    scrollToFirstError(jQuery('#' + noticId).first().closest('.form-group'));
                  }
                });
              }
              if ('saveOverview' == form_id || 'saveContract' == form_id) {
                Polling.hide();
              }
              return false;
            } else if (resp.hasOwnProperty('serviceError') && ($('#current_store_is_telesales').val() == 0)) {
              // Close the popups in case of a service error, because it means there is no polling id and no polling can be made
              if (form_id == 'saveOverview') {
                self.showVodError(true, resp.serviceError + ' - ' + resp.message, 1, 1);
              } else if (form_id == 'saveContract') {
                self.showVodError(true, resp.serviceError + ' - ' + resp.message, 1, 0);
              } else {
                showModalError(resp.message, resp.serviceError);
              }

          } else if (resp.error == false || (jQuery('#current_store_is_telesales').val() != 0)) {

              if (form_id == 'saveOverview' || form_id == 'saveSuperOrder') {
                if (resp.creditcheck_page) {
                  var creditcheck_content = $('#creditcheck-waiting').find('#saveCreditCheck');
                  if ($('#creditcheck-waiting') && creditcheck_content) {
                    creditcheck_content.replaceWith($(resp.creditcheck_page).find('#saveCreditCheck'));
                  }
                }
                if ($('#current_store_is_telesales').val() == 0) {
                  checkoutDeliverPageDialog = true;
                }
                if(resp.skip_number_selection) {
                  self.hideContract = true;
                }
                if(resp.overview_step == "show_credit_contract") {
                  self.showContract = true;
                }

                if (resp.contract_page) {
                  var directDelivery = $('input[name="delivery[type]"]').val() == 'direct';
                  if (form_id == 'saveSuperOrder' && ((directDelivery && resp.skip_number_selection == undefined) || isStore == 'indirect')){
                    $('[data-ref="your-order"]').addClass('hidden');
                    $('#numberselection_page').removeClass('hidden');
                    $('#saveNumberSelection').parent().removeClass('skip');
                  } else if (form_id == 'saveSuperOrder') {
                    $('#contract_page').remove();
                    $('#numberselection_page').remove();
                    $('[data-section="number_device_section"]').addClass('hidden');
                    $('#saveNumberSelection').parent().addClass('hidden');
                    $('#saveNumberSelection').parent().addClass('skip');
                    checkoutDeliverPageDialog = false;
                  }

                  // When not direct the contract signing should not happen yet.
                  if(!directDelivery && isStore != 'indirect' && form_id == 'saveSuperOrder'){
                      $('[data-section="save_contract"]').addClass('hidden');
                      $('#contract_page').addClass('hidden');
                      $('#contract_page').addClass('skip');
                      $('[data-section="order-confirmation"]').removeClass('hidden');
                      $('[data-ref="order-confirmation"]').show();
                  } else if (form_id == 'saveSuperOrder') {
                      $('#contract').parent().removeClass('hidden');
                      $('#saveContract').parent().removeClass('skip');
                  }

                  var contract_content = $('#contract_page');
                  if ($('#contract_page') && contract_content) {
                    contract_content.html(resp.contract_page);
                  }
                  if (form_id == 'saveSuperOrder' && resp.new_order && (directDelivery || isStore == 'indirect')) {
                    localStorage.setItem('new_order_no', resp.new_order);
                  }
                } else {
                  $('[data-ref="your-order"]').addClass('hidden');
                  $('#saveNumberSelection').parent().addClass('skip');
                  $('#saveContract').parent().parent().addClass('skip');
                }
                //remove service-address section in delivery checkout
                jQuery('.section.service-address').addClass('hidden');
                $('.order-lock').remove();
              }

              if (form_id == form.attr('id') && self.isOffer != 1) {
                if (form_id == 'saveOverview') {
                  // TODO: do we really need to start the polling on Telesales?!
                  var polling = new Polling('saveOverview');
                  polling.updatePolling(resp.polling_id);
                }

                if (form_id == 'saveOverview' || form_id == 'saveSuperOrder') {
                  if ($('#saveCreditCheck').length && resp.hasOwnProperty('addresses') && resp.hasOwnProperty('order_id')) {
                    // If the creditcheck step is present and necessary
                    var creditStub = resp.credit_fail == true;
                    window.creditCheckData = {addresses: resp.addresses, order_id: resp.order_id, creditStub: creditStub, order_no: resp.orders};
                  }
                }
              }

              if (form_id == 'saveContract') {
                //self.showVodError(false);
                Polling.start('saveContract', resp.pollingId);
              }

              if (form_id == 'saveOverview' || form_id == 'saveSuperOrder') {
                var isFixed = $('[name="isFixed"]').val();
                if(isFixed == 0) {
                  $('#order-details-btn').addClass('hidden');
                }
                $('#order-increment-id').html(Translator.translate('Order number:')+' <strong>'+ resp.orders+'</strong>');
                $('#order-number-copy').text(resp.orders);
                $('#modal-order-number').text(Translator.translate('Order')+' '+resp.orders+' '+Translator.translate('number:'));
                $('#order-details-btn').attr('data-ordernumber', resp.orders);
                $("#checkout-confirm-view-order").attr('onclick', 'event.stopPropagation();orderConfirmationViewOrder(event, ' + resp.orders + ')');
                $("#checkout-move-workitem").attr('onclick', 'event.stopPropagation();checkoutMoveWorkitem(event, ' + resp.orders + ')');
                $('#order-confirmation-value').val(resp.orders);
                if (resp.newOrderMessage && resp.newOrderMessage.length)
                  $('#showOrderConfirmation #order-confirmation-new-order-message').html(resp.newOrderMessage);
                if (resp.oldOrderMessage && resp.oldOrderMessage.length)
                  $('#showOrderConfirmation #order-confirmation-old-order-message').html(resp.oldOrderMessage);
              }

              if (resp.new_order != undefined) {
                $('#new-order-increment-id').data('id', resp.new_order_id);
                $('#new-order-increment-id').html('Order number: <strong>'+resp.new_order+'</strong>');
                $('#order-number-copy-new').text(resp.new_order);
                $('#order-details-btn-new').attr('data-ordernumber', resp.new_order);
                $('.new_superorder').removeClass('new_superorder');
                $('#normal-order-message').hide();
                $('#modal-order-number').text(Translator.translate('Order')+' '+resp.orders+' '+Translator.translate('number:'));
                $("#checkout-confirm-view-order").attr('onclick', 'event.stopPropagation();orderConfirmationViewOrder(event, ' + resp.new_order + ')');
                $("#checkout-move-workitem").attr('onclick', 'event.stopPropagation();checkoutMoveWorkitem(event, ' + resp.new_order + ')');
              }
              if (self.isOffer == 1) {
                if (resp.is_offer == true) {
                  // redirect to home page if the offer has been saved
                  window.location = '/';
                  return true;
                }
              }

              hideNoticeBarError('div.show-cart-errors');
              const bullet = settings.bullets.find( ".vertex.active" ).last();
              const nextBullet = $(bullet).next().not('.skip');
              if (form_id != 'saveOverview' && form_id != 'saveSuperOrder' && form_id != 'saveContract') {
                bullet.addClass('completed');
                $(nextBullet).addClass('active');
              }
              const nextSectionName = $(nextBullet).data('section');

              // Map form to summary
              if (form_id != 'saveOverview' && form_id != 'saveSuperOrder' && form_id != 'saveContract') {
                $(n.section).find('.form input,select,textarea').each(function(i,el){
                  const summaryElement = $(n.section).find('.summary');
                  $(el).each(function(index, formElement){
                    const dataElement = $(summaryElement).find('[data-ref="'+$(formElement).attr('name')+'"]');
                    if (dataElement) {
                      $(dataElement).html($(formElement).val());
                    }
                  });
                });
              }
              if (form_id == 'saveNumberPorting') {
                localStorage.setItem('sms-code-valid', 1);
              }
              if (form_id == 'saveCustomer') {
                var isFixed = $("#is_fixed_cust").val();
                var isSoho = $('select[name="customer[type]"]').val();
                var isFixedIlsMove = $('#isFixedIlsMove').val();
                if (isFixed) {
                  var serviceAddress = store.get('serviceApiAddress');
                  var houseno = serviceAddress.housenumber;
                  var addition = serviceAddress.housenumberaddition;
                  var street = serviceAddress.street;
                  var postcode = serviceAddress.postalcode;
                  var city = serviceAddress.city;
                } else {
                  var houseno = $('input[name="address[otherAddress][houseno]"]').val();
                  var addition = $('input[name="address[otherAddress][addition]"]').val();
                  var street = $('input[name="address[otherAddress][street]"]').val();
                  var postcode = $('input[name="address[otherAddress][postcode]"]').val();
                  var city = $('input[name="address[otherAddress][city]"]').val();
                }

                var customerPrefix = '';
                if(isSoho == 1)
                {
                  customerPrefix = 'contractant_';
                }
                var firstName = $('input[name="customer['+customerPrefix+'firstname]"]').val();
                var middleName = $('input[name="customer['+customerPrefix+'middlename]"]').val();
                var lastName = $('input[name="customer['+customerPrefix+'lastname]"]').val();
                var customerName = $.trim(firstName+' '+middleName+' '+lastName);

                var email = $('input[name="additional[email][0]"]').val();
                if (email) {
                  $('[name="incometest[email]"]').val(email).trigger('change');
                } else {
                  $('#block-fill-in-later').addClass('radio-disabled');
                }
                $('[id=customer-details-ils]').html(customerName);
                $('[id^=customer-details-delivery]').html(customerName);
                
                var fullAddress = street+' '+houseno+(addition ? ' '+addition : '')+', '+postcode+', '+city;
                $('[id^=delivery_address_display_]').html(Translator.translate('Send package(s) to')+' '+fullAddress);

                if (isFixedIlsMove == "1") {
                  var bill_houseno = $('input[name="billingAdd[houseno]"]').val();
                  var bill_addition = $('input[name="billingAdd[addition]"]').val();
                  var bill_street = $('input[name="billingAdd[street]"]').val();
                  var bill_postcode = $('input[name="billingAdd[postcode]"]').val();
                  var bill_city = $('input[name="billingAdd[city]"]').val();
                  var fullAddress = bill_street+' '+bill_houseno+(bill_addition ? ' '+bill_addition : '')+', '+bill_postcode+', '+bill_city;
                }

                $('[id^=billing_address_display_]').html(fullAddress);

                $('#billing_address_display_1').html(Translator.translate('Use customer address')+' ('+fullAddress+')');

                // Fill in default address
                $('[name="billingAddress[street]"]').val(street).trigger('change');
                $('[name="billingAddress[houseno]"]').val(houseno).trigger('change');
                $('[name="billingAddress[addition]"]').val(addition).trigger('change');
                $('[name="billingAddress[postcode]"]').val(postcode).trigger('change');
                $('[name="billingAddress[city]"]').val(city).trigger('change');

                // Fill in per-package address
                var packageValue = $('input[name="package_value"]').val();
                if (packageValue >= 1 ) {
                  for(i=1; i<=$("#package_count").val();i++) {
                    $('[name="delivery[pakket]['+i+'][billingAddress][street]"]').val(street).trigger('change');
                    $('[name="delivery[pakket]['+i+'][billingAddress][houseno]"]').val(houseno).trigger('change');
                    $('[name="delivery[pakket]['+i+'][billingAddress][addition]"]').val(addition).trigger('change');
                    $('[name="delivery[pakket]['+i+'][billingAddress][postcode]"]').val(postcode).trigger('change');
                    $('[name="delivery[pakket]['+i+'][billingAddress][city]"]').val(city).trigger('change');
                  }
                }

                //prepare datepicker for delivery date
                if (jQuery('#delivery-date input').length > 0) {
                  $('input[name="delivery[method]"]:checked').trigger('click');
                }

                if (resp.skipHomeDelivery) {
                  $('#billing_address_single_shipping').parent().addClass('hidden');

                  if ($('#current_store_is_telesales').val() == 1) {
                    $('[id="delivery[deliver][billing_address]"]').prop('checked', false);
                    $('#deliver_other_address').prop('checked', true).trigger('change');
                    $('#deliver_other_address').parent().parent().parent().addClass('checked');
                    $('#otherAddress\\[postcode\\], #otherAddress\\[houseno\\]').addClass('required-entry');
                  }
                } else {
                  $('#billing_address_single_shipping').parent().removeClass('hidden');
                  if ($('#current_store_is_telesales').val() == 1) {
                    $('[id="delivery[deliver][billing_address]"]').prop('checked', true).trigger('change');
                    $('#deliver_other_address').prop('checked', false);
                    $('#deliver_other_address').parent().parent().parent().removeClass('checked');
                    $('#otherAddress\\[postcode\\], #otherAddress\\[houseno\\]').removeClass('required-entry');
                  }
                }
                $("#section-summary-saveCustomer [class*='-summary']").each(function() {
                  if ($(this).html().trim() == "")
                    $(this).parent().parent().hide();
                  else
                    $(this).parent().parent().show();
                });
              }

              if (form_id == 'saveIncomeTest') {
                $('.fill-income-details-summary').addClass('hidden');
                var incomeTest = $('input[name="incometest[choice]"]:checked').val();

                if (incomeTest == "fill-in-now") {
                  var familyType = $("#family-type").find("option:selected").text();
                  $('.family-type-summary').html(''+familyType+'');
                  $('.fill-income-details-summary').removeClass('hidden');
                }
              }

              if (form_id == 'saveDeliveryAddress') {
                var isFixed = $("#is_fixed_cust").val();
                var deliveryMethod = $('input[name="delivery[method]"]:checked').val();
                if(isFixed) {
                  var serviceAddress = store.get('serviceApiAddress');
                  var houseNo = serviceAddress.housenumber;
                  var additionNo = serviceAddress.housenumberaddition;
                  var streetName = serviceAddress.street;
                  var postcodeValue = serviceAddress.postalcode;
                  var city = serviceAddress.city;
                } else {
                  var houseNo = $('input[name="address[otherAddress][houseno]"]').val();
                  var additionNo = $('input[name="address[otherAddress][addition]"]').val();
                  var streetName = $('input[name="address[otherAddress][street]"]').val();
                  var postcodeValue = $('input[name="address[otherAddress][postcode]"]').val();
                  var city = $('input[name="address[otherAddress][city]"]').val();
                }

                jQuery('.display_wish_date').html(formFields.delivery_wish_date_LSP);
                if(deliveryMethod == 'direct') {
                  var storeName = $('input[name="delivery[method]"]:checked').attr('data-name');
                  $('[id^=delivery_address_summary_display]').html(storeName);
                  $('#single-package-summary').removeClass('hidden');
                  $('[id^=split-package-summary_]').addClass('hidden');
                  $('#horizontal-line').addClass('hidden');
                  $('#pay_on_delivery').addClass('hidden');
                  $('#pay_on_delivery input').removeAttr('checked');
                  $('#pay_in_store').removeClass('hidden');
                  $('#pay_in_store input').prop('checked',true);
                  $('#pay_on_first_bill').addClass('hidden');
                }
                if(deliveryMethod == 'deliver') {
                  $('[id^=delivery_address_summary_display]').html(streetName+' '+houseNo+' '+additionNo+'<br />'+postcodeValue+' '+city);
                  $('#single-package-summary').removeClass('hidden');
                  $('[id^=split-package-summary_]').addClass('hidden');
                  $('#horizontal-line').addClass('hidden');
                  $('#pay_on_delivery').removeClass('hidden');
                  $('#one-off > .radio:not(#pay_in_store) input').first().prop('checked', true);
                  $('#pay_in_store').addClass('hidden');
                  $('#pay_in_store input').removeAttr('checked');
                  $('#pay_on_first_bill').removeClass('hidden');
                }
                if(deliveryMethod == 'other_address') {
                  var otherHouseNo = $('input[name="otherAddress[houseno]"]').val();
                  var mobileOtherAdd = $('input[name="otherAddress[addition]"]').val();
                  var fixedOtherAdd = $('select[name="otherAddress[addition]"]').val();
                  if (fixedOtherAdd == null) {
                    var otherAdditionNo = mobileOtherAdd;
                  } else {
                    var otherAdditionNo = fixedOtherAdd;
                  }
                  var otherStreetName = $('input[name="otherAddress[street]"]').val();
                  var otherPostcodeValue = $('input[name="otherAddress[postcode]"]').val();
                  var otherCity = $('input[name="otherAddress[city]"]').val();
                  $('[id^=delivery_address_summary_display]').html(otherStreetName+' '+otherHouseNo+' '+otherAdditionNo+'<br />'+otherPostcodeValue+' '+otherCity);
                  $('#single-package-summary').removeClass('hidden');
                  $('[id^=split-package-summary_]').addClass('hidden');
                  $('#horizontal-line').addClass('hidden');
                  $('#pay_on_delivery').removeClass('hidden');
                  $('#one-off > .radio:not(#pay_in_store) input').first().prop('checked', true);
                  $('#pay_in_store').addClass('hidden');
                  $('#pay_in_store input').removeAttr('checked');
                  $('#pay_on_first_bill').removeClass('hidden');
                }
                if(deliveryMethod == 'pickup') {
                  var storeName = $('#search-store').closest('.radio-popdown').find('.store-selection').html();
                  $('[id^=delivery_address_summary_display]').html(storeName);
                  $('#single-package-summary').removeClass('hidden');
                  $('[id^=split-package-summary_]').addClass('hidden');
                  $('#horizontal-line').addClass('hidden');
                  $('#pay_on_delivery').addClass('hidden');
                  $('#pay_on_delivery input').removeAttr('checked');
                  $('#pay_in_store').removeClass('hidden');
                  $('#pay_in_store input').prop('checked',true);
                  $('#pay_on_first_bill').addClass('hidden');
                }
                if(deliveryMethod == 'split') {
                  var fixedPackageId = $("#fixedPkgId").val();
                  for(i=1; i<=$("#package_count").val();i++) {
                    var radioButtonContainer = $('input[name="delivery[pakket]['+i+'][address]"]:checked');
                    var splitDeliveryMethod = radioButtonContainer.val();
                    if(splitDeliveryMethod == 'direct') {
                      var storeName = $('input[name="delivery[pakket]['+i+'][address]"]:checked').attr('data-name');
                      $('[id^=delivery_address_summary_display_'+i+']').html(storeName);
                      $('#split-package-summary_'+i+'').removeClass('hidden');
                      $('#single-package-summary').addClass('hidden');
                      if(i != fixedPackageId) {
                        $('#pay_on_delivery').addClass('hidden');
                        $('#pay_on_delivery input').removeAttr('checked');
                        $('#pay_in_store').removeClass('hidden');
                        $('#pay_in_store input').prop('checked',true);
                        $('#pay_on_first_bill').addClass('hidden');
                      }
                    }
                    if(splitDeliveryMethod == 'billing_address') {
                      $('[id^=delivery_address_summary_display_'+i+']').html(streetName+' '+houseNo+' '+additionNo+'<br />'+postcodeValue+' '+city);
                      $('#split-package-summary_'+i+'').removeClass('hidden');
                      $('#single-package-summary').addClass('hidden');
                      if(i != fixedPackageId) {
                        $('#pay_on_delivery').removeClass('hidden');
                        $('#one-off > .radio:not(#pay_in_store) input').first().prop('checked', true);
                        $('#pay_in_store').addClass('hidden');
                        $('#pay_in_store input').removeAttr('checked');
                        $('#pay_on_first_bill').removeClass('hidden');
                      }
                    }
                    if(splitDeliveryMethod == 'other_address') {
                      var otherHouseNo = $('input[name="delivery[pakket]['+i+'][otherAddress][houseno]"]').val();
                      var fixedOtherAdd = $('select[name="delivery[pakket]['+i+'][otherAddress][addition]"]').val();
                      var mobileOtherAdd = $('input[name="delivery[pakket]['+i+'][otherAddress][addition]"]').val();
                      if (fixedOtherAdd == null) {
                        var otherAdditionNo = mobileOtherAdd;
                      } else {
                        var otherAdditionNo = fixedOtherAdd;
                      }
                      var otherStreetName = $('input[name="delivery[pakket]['+i+'][otherAddress][street]"]').val();
                      var otherPostcodeValue = $('input[name="delivery[pakket]['+i+'][otherAddress][postcode]"]').val();
                      var otherCity = $('input[name="delivery[pakket]['+i+'][otherAddress][city]"]').val();
                      $('[id^=delivery_address_summary_display_'+i+']').html(otherStreetName+' '+otherHouseNo+' '+otherAdditionNo+'<br />'+otherPostcodeValue+' '+otherCity);
                      $('#split-package-summary_'+i+'').removeClass('hidden');
                      $('#single-package-summary').addClass('hidden');
                      if(i != fixedPackageId) {
                        $('#pay_on_delivery').removeClass('hidden');
                        $('#one-off > .radio:not(#pay_in_store) input').first().prop('checked', true);
                        $('#pay_in_store').addClass('hidden');
                        $('#pay_in_store input').removeAttr('checked');
                        $('#pay_on_first_bill').removeClass('hidden');
                      }
                    }
                    if(splitDeliveryMethod == 'store') {
                      var storeName = radioButtonContainer.closest('.radio-item').find('.store-selection').html();
                      $('[id^=delivery_address_summary_display_'+i+']').html(storeName);
                      $('#split-package-summary_'+i+'').removeClass('hidden');
                      $('#single-package-summary').addClass('hidden');
                      if(i != fixedPackageId) {
                        $('#pay_on_delivery').addClass('hidden');
                        $('#pay_on_delivery input').removeAttr('checked');
                        $('#pay_in_store').removeClass('hidden');
                        $('#pay_in_store input').prop('checked',true);
                        $('#pay_on_first_bill').addClass('hidden');
                      }
                    }
                  }
                }
              }

              if (form_id == 'savePaymentMethod') {
                var mobileOneOffPay = $('input[name="payment[oneoff_method]"]:checked').attr('data-fill-value');
                var ibanNumber = $('input[name="payment[account_no_mobile]"]').val();
                var accountholder = $('input[name="payment[account_holder_mobile]"]').val();
                var accholderfixed = $('input[name="payment[account_holder_fixed_different]"]').val();
                if (ibanNumber !=null && accountholder != null) {
                  $('.fixed-account-no-summary').html(''+ibanNumber+'');
                  $('.fixed-account-holder-summary').html(''+accountholder+'');
                }
                if (accholderfixed != null) {
                  $('.fixed-account-holder-different-summary').html(''+accholderfixed+'');
                }
                $('.oneoff-method-summary').html(Translator.translate(mobileOneOffPay));
              }

              if (form_id == 'saveSuperOrder') {
                if (!resp.hasOwnProperty('orders')) {
                  Polling.hide();
                  window.location = this.baseURL + 'checkout/cart/exitChangeOrder';
                } else {
                  Polling.start('saveSuperOrder', resp.pool_id);
                }
              }

              if (form_id == 'saveCreditCheck') {
                $('.section[data-ref="number_device_section"]').show().removeClass('hidden');
                // check if the number selection step is available
                if ($('#saveNumberSelection').length && resp.hasOwnProperty('html')) {
                  updateNumberSelection(resp.html);
                  updateSelectDevice(resp.html);
                  $('#saveNumberSelection .input-wrapper').initInputElement();
                }
                if (jQuery('[name="isEmptyNumberSelection"]').val() == "1") {
                  jQuery('#numberselection_page').remove();
                  jQuery('[data-section="number_device_section"]').hide();
                  jQuery('#contract_page').show();
                  jQuery('[data-section="save_contract"]').addClass('active');
                }
              }

              // Recalc
              settings.sections = getAvailableSections();
              drawForm();
              drawBullets();
              if (form_id == 'saveCustomer') {
                applyEmailTooltips(jQuery('#section-summary-saveCustomer'));
              }

              if (form_id == 'saveOverview') {
                if  (resp.overview_step == 'show_numberselection'){
                  $('.section[data-ref="number_device_section"]').show();
                  $('#numberselection_page').css('padding-top','10px');
                  showHardwareDetails();
                } else if (resp.overview_step == 'show_contract') {
                  jQuery('#numberselection_page').remove();
                  jQuery('[data-section="number_device_section"]').hide();
                  jQuery('#contract_page').show();
                  jQuery('[data-section="save_contract"]').addClass('active');
                }

                if (resp.skip_number_selection_contract)
                {
                  jQuery('#numberselection_page').remove();
                  jQuery('[data-section="number_device_section"]').hide();
                  jQuery('#contract_page').remove();
                  jQuery('.breadcrumb-bar').addClass('hidden');
                  jQuery('[data-ref="number_device_section"]').hide();
                  jQuery('[data-section="save_contract"]').hide();
                  jQuery('[data-ref="order-confirmation"]').show();
                  jQuery('[data-section="order-confirmation"]').removeClass('hidden');
                  jQuery('[data-section="order-confirmation"]').addClass('active');
                }
                localStorage.removeItem('sms-code-valid');
                jQuery('.tip-trig-hover').tooltip();
              }
              if(resp.lsp_delivery) {
                  self.LSPhideContract = true;
              }
              if (form_id == 'saveNumberSelection') {
                $('#numberSelectionSummary').show(1000, function() {
                  $('.addStyleHeader').css('padding-bottom','20px');
                });
                $('.device_name').each(function() {
                  var packageId = $(this).attr('data-package-id');
                  var deviceName = $('input[name="deviceselection['+ packageId +'][device_name]"').val();
                  $('#deviceselection_'+ packageId +'_device_name_summary').html(deviceName);
                });
                $('.mobile_number').each(function() {
                  var packageId = $(this).attr('data-package-id');
                  var mobileNumber = $('input[name="numberselection['+ packageId +'][mobile_number]"').val();
                  $('.numberselection_'+ packageId +'_mobile_number-summary').html(mobileNumber);
                });
                if ((isDeliveryLSP() && self.LSPhideContract) || (isDeliverySHOP() && isStore == 'indirect' && isFixedOrder && !isMobileOrder)) {
                  jQuery('#contract_page').remove();
                  jQuery('.breadcrumb-bar').addClass('hidden');
                  jQuery('[data-ref="number_device_section"]').hide();
                  jQuery('[data-section="save_contract"]').hide();
                  jQuery('[data-ref="order-confirmation"]').show();
                  jQuery('[data-section="order-confirmation"]').removeClass('hidden');
                  jQuery('[data-section="order-confirmation"]').addClass('active completed');
                }
              }

              if (form_id == 'saveCreditCheck' && (self.hideContract || window.hideContract)) {
                jQuery('#contract_page').remove();
                jQuery('#numberselection_page').remove();
                jQuery('[data-section="number_device_section"]').hide();
                jQuery('[data-section="save_contract"]').hide();
                jQuery('[data-ref="number_device_section"]').hide();
                jQuery('[data-ref="credit_check"]').hide();
                jQuery('.breadcrumb-section').addClass('hidden');
                jQuery('[data-ref="order-confirmation"]').show();
                jQuery('[data-section="order-confirmation"]').removeClass('hidden');
                jQuery('[data-section="order-confirmation"]').addClass('active completed');
              }

              if (form_id == 'saveCreditCheck' && self.showContract) {
                jQuery('#numberselection_page').remove();
                jQuery('[data-section="number_device_section"]').hide();
                jQuery('[data-ref="number_device_section"]').hide();
                jQuery('[data-ref="save_contract"]').show();
                jQuery('[data-section="save_contract"]').removeClass('hidden');
                jQuery('[data-section="save_contract"]').addClass('active');
              }

              if (nextSectionName != 'order-confirmation') {
                window.checkout.initSubmitButton(nextSectionName, resp.error);
              }

              // Slide
              if (form_id != 'saveOverview' && form_id != 'saveSuperOrder' && form_id != 'saveContract') {
                settings.sections.each(function(n) {
                  if(n.name === nextSectionName && nextSectionName != 'save_contract') {
                    let headerOffset = 70;
                    let scrollTo = jQuery('#cart-content').scrollTop() + $(n.section).offset().top - headerOffset;
                    $( '#cart-content' ).animate( { scrollTop: scrollTo }, {duration: 500, easing: "swing"} );
                    return;
                  }
                });
              }
              jQuery('.tip-trig-hover-popover').popover({
                trigger: 'hover',
                animation: false
              });
            }
          }).error(function(resp){
            console.log('ERROR!!!!');
          }).always(function(){
            if (form_id != 'saveOverview') {
              $(that).removeClass('loading');
            }
            currentXhr = null;
          });
        });
        // Redo existing steps
        $(n.section).find('.summary button').click(function(e){
          e.preventDefault();
          const sectionClicked = $(this).closest('.section').data('ref');
          let hitClickedElement=false;
          settings.sections.each(function(n) {
            if ($(n.bullet).data('section') === sectionClicked) {
              $(n.bullet).addClass('active');
              $(n.bullet).removeClass('completed');
              $( '#cart-content' ).animate( { scrollTop: $(n.section).offset().top }, {duration: 500, easing: "swing"} );
              hitClickedElement = true;
            } else if(hitClickedElement) {
              $(n.bullet).removeClass('active');
              $(n.bullet).removeClass('completed');
            }
          });
          settings.sections = getAvailableSections();
          drawForm();
          drawBullets();
        });
      });
    };

    registerButtonClicks();
    drawForm();
    $( window ).resize(function() {
      drawBullets();
    });
    $( '#cart-content' ).scroll(function() {
      drawBullets();
    });

    const checkout = $(this);

    // Adding gradient above each section footer wrapper
    $('<div class="section-footer-gradient"></div>').prependTo('.section-footer-wrapper');


    checkout.find('.tip-trig-hover').tooltip({
      trigger: 'hover',
      animation: false
    });
    checkout.find('.tip-trig-hover-left').tooltip({
      trigger: 'hover',
      animation: false,
      placement: 'left'
    });
    checkout.find('.tip-trig-hover-popover').popover({
      trigger: 'hover',
      animation: false
    });

    checkout.find('.form-control[data-toggle="tooltip"]').tooltip({
      trigger: 'click',
      placement: 'top',
      animation: false
    });
    const adjustRadio = function($element) {
      const input = $element.length ? $element : $(this);
      const name = input.attr('name');

      // for each input with group do this
      checkout.find('*[name="' + name + '"]').each(function() {
        if ( $(this).parent().parent().parent('.radio-collection').length ) {
          const element = $(this).closest('.radio-collection');

          // Check if radio button has a popdown.
          const isChecked = $(this).is(':checked');
          if (isChecked) {
            element.addClass('checked');
          } else {
            element.removeClass('checked');
          }
          $(this).trigger('adjustRadioAfter');
        }
      });
    };
    const adjustCheckbox = function($element) {
      const input = $element.length ? $element : $(this);
      // get input group name
      const name = input.attr('name');
      // for each input with group do this
      checkout.find('*[name="' + name + '"]').each(function() {
        if ($(this).closest('.checkbox-collection').length) {
          const element = $(this).closest('.checkbox-collection');
          // Check if radio button has a popdown.
          const isChecked = $(this).is(':checked');
          if (isChecked) {
            element.addClass('checked');
          } else {
            element.removeClass('checked');
          }
        }
      });
    };

    checkout.on('change', '[id="customer[dummy_email]"]', function () {
      var checked = $(this).prop('checked');
      if (checked) {
        $('.additional-email input')
          .removeClass('required-entry validate-email validate-backend-email validation-failed')
          .prop('disabled', true)
          .closest('.form-group')
          .addClass('disabled');
        $('.additional-email input').parent().find('.validation-advice').remove();
        $('.add-address-email').addClass('hidden');
        $('.remove-above').each(function (a, el) {
          $(el).click()
        });
        jQuery('.split-email-button button').addClass('disabled');
      } else {
        $('.additional-email input')
          .addClass('required-entry validate-email validate-backend-email')
          .prop('disabled', false)
          .closest('.form-group')
          .removeClass('disabled');
        $('.add-address-email').removeClass('hidden');
        jQuery('.split-email-button button').removeClass('disabled');
      }

        $('.additional-email input').trigger('blur');
    });

    checkout.on('change', '[id="additional[email][0]"]', function () {
      var empty = $(this).val().trim() == '';
      if (empty) {
        $('[id="customer[dummy_email]"]').closest('.checkbox').removeClass('disabled');
        $('[id="customer[dummy_email]"]').prop('disabled', false);
      } else {
        $('[id="customer[dummy_email]"]').closest('.checkbox').addClass('disabled');
        $('[id="customer[dummy_email]"]').prop('disabled', true);
      }
    });

    checkout.on('click', '.radio input', adjustRadio);
    checkout.on('click', '.checkbox-collection .checkbox input', adjustCheckbox);

    checkout.on('click', '.orderline-advanced.expandable > .orderline-header', function() {
      $(this).parent().toggleClass('expanded');
    });
    checkout.on('change', '.typeahead-input', function(event) {
      event.stopPropagation();
      const typeaheadInput = $(this);
      typeaheadInput.addClass('expanded');
    });
    checkout.find('.form-group textarea').each(function() {
      $(this).parent().append('<div class="textarea-counter"></div>');
    });
    checkout.on('change input', '.form-group textarea', function() {
      const amount = $(this).val().length;
      $(this).parent().find('.textarea-counter').text(amount);
    });

    checkout.on('click', '.contextmenu-action', function(event) {
      event.stopPropagation();
      const contextMenu = $(this);
      contextMenu.toggleClass('expanded');

      if (contextMenu.hasClass('expanded')) {
        const closeContextMenu = function() {
          contextMenu.removeClass('expanded');
          window.removeEventListener('click', function() {closeContextMenu(contextMenu)});
        };
        window.addEventListener('click', function() {closeContextMenu(contextMenu)});
      }
    });

    drawBullets();
  };
}( jQuery ));

jQuery(function ($) {
  /** START save_delivery_address.phtml **/

  // Main delivery settings for each type of delivery
  $('[name="delivery[deliver][address]"]').unbind().bind('change', function () {
    var packageId = '0'; // Default delivery package id
    var container = $('.search-address[data-package-id="' + packageId + '"]');
    if ($(this).val() != 'billing_address' && $(this).val() != 'service_address') {
      container.removeClass('hidden');
    } else {
      container.addClass('hidden');

      var streetField = container.find('.otherAddress input[id*="street"]');
      streetField.parent().find('.ajax-validation').remove();
      streetField.removeClass('required-entry validation-failed');

      container.find('.otherAddress input[id*="houseno"]').removeClass('required-entry validation-failed');
      container.find('.otherAddress input[id*="postcode"]').removeClass('required-entry nl-postcode validation-failed');
      container.find('.otherAddress input[id*="city"]').removeClass('required-entry validation-failed');
    }
  });

  $('#cart input').on('blur change', function (ev) {
    var el = $(this);
    if (el.attr('type') == 'radio') {
      el.parent().parent().find('.validation-failed').removeClass('validation-failed');
      el.parent().parent().find('.ajax-validation').remove();
      el.parent().parent().find('.validation-advice').remove();
    } else {
      if (ev.type == 'blur') {
        el.removeClass('validation-failed');
        el.next('.validation-advice').remove();
        el.parent().find('.ajax-validation').fadeOut('slow', function () {
          $(this).remove();
        });
        if (el.val() != '' || !el.hasClass('required-failed')) {
          var options;
          if (typeof availableDevicesIndirect == "undefined") {
            options = {};
          } else {
            options = {onElementValidate: deviceSelectionValidationCallback};
          }
          Validation.validate(this, options);
        }
      }
      if (ev.type == 'change') {
        el.val(escapeHtml(el.val()));
      }
    }
  });

  // Unbind delivery options settings from baseline
  $('#cart #saveDeliveryAddress .dropdown-menu li a').unbind( "click" );

  // Main delivery options settings
  $('#cart #saveDeliveryAddress .dropdown-menu li a').bind('click', function (e) {
    e.preventDefault();
    e.stopPropagation();
    var self = $(this);
    var value = self.data('value');
    var group = self.parents('.btn-group').first();
    var type = checkout.setDropdownOption(group, self, true);

    // Hide all payment options details
    $('#payment-type .payment-extra').addClass('hidden');

    // Delivery section
    $('#method-pickup input[type="text"]').removeClass('required-entry');

    if (type == 'delivery') {
      // Hide all delivery methods
      $('#cart .delivery-method').addClass('hidden');
      $('#method-' + value).removeClass('hidden');

      if (value == 'pickup') {
        $('#method-pickup input[type="text"]').addClass('required-entry');
      }
      // Set the back to homepage/register button
      if(value == 'direct'){
        $('#confirmation-to-nextstep').text(Translator.translate('Continue at cash register'));
      } else {
        $('#confirmation-to-nextstep').text(Translator.translate('Continue to homepage'));
      }

      setPaymentOptions(value);

      if (value == 'split') {
        $('#method-split .split-payment-method select').addClass('validate-select');
        $('#method-split .split-delivery-method select').trigger('change');
        $('#payment-type #payment_method_text').text(Translator.translate('Split payment'));
        $('#payment-type input[name="payment[method]"]').val('split-payment');
        $('#method-split input[data-store-id][readonly]').each(function () {
          if (!$(this).parents().hasClass('hidden') && $(this).parents('.split-delivery-method').find('select').val() == 'pickup') {
            $(this).addClass('required-entry');
          } else {
            $(this).removeClass('required-entry validation-failed');
          }
        });
      } else {
        $('#method-split input[data-store-id][readonly]').removeClass('required-entry validation-failed');
        $('#method-split .split-payment-method select').removeClass('validate-select');
        var available_payment_options = $('#payment-type [id="payment[method]"] ul li').filter(':not(.hidden)');
        if (available_payment_options.length == 1) {
          available_payment_options.children('a').trigger('click');
        } else {
          $('#payment-type #payment_method_text').text(Translator.translate('Choose'));
          $('#payment-type input[name="payment[method]"]').val('');
        }
      }
    } else {
      // Payment selection section
      if (value.search('split') != -1 && $('#' + value).length > 0) {
        $('#' + value).removeClass('hidden');
      } else {
        $('#payment-type .validate-payment-single').removeClass('validation-failed');
        if ($('#split-' + type).length > 0) {
          $('#split-' + type).addClass('hidden');
        }
        if ($('#payment-type .payment-extra.' + value).length) {
          $('#payment-type .payment-extra.' + value).removeClass('hidden').parent().find('.validation-advice').remove();
        }
      }
    }
  });


});

function escapeHtml(str) {
  if (jQuery.trim(str) != '') {
    var tmp = document.createElement("DIV");
    tmp.innerHTML = str;
    var result = tmp.innerText || tmp.textContent;

    jQuery(tmp).remove();
    if (result != str) {
      return escapeHtml(result);
    } else {
      return result;
    }
  } else {
    return str;
  }
}

function processPreviousSteps(step) {
  if (step == 'saveOverview') {
    var inputs = jQuery('#saveCustomer').serializeObject();
    setOverview(inputs);
    checkout.setCreditCheck(inputs);
  } else if (step == 'saveSuperOrder') {
    if (!jQuery('#saveSuperOrder #accept').hasClass('processed')) {
      // Check if Delivery address or Number Porting have any changes in edit order mode
      if ((undefined != form.saveDeliveryAddressData && form.saveDeliveryAddressData != jQuery('#saveDeliveryAddress').serialize())
          || (undefined != form.saveNumberPortingData && form.saveNumberPortingData != jQuery('#saveNumberPorting').serialize())) {
        // Enable the accept conditions button
        jQuery('#saveSuperOrder #accept').prop('checked', true).parent().removeClass('disabled');
        jQuery('#saveSuperOrder #submit').prop('disabled', false);
      }

      if ((undefined != form.saveDeliveryAddressData && form.saveDeliveryAddressData == jQuery('#saveDeliveryAddress').serialize())
          && (undefined != form.saveNumberPortingData && form.saveNumberPortingData == jQuery('#saveNumberPorting').serialize())) {
        // Disable the accept conditions button
        jQuery('#saveSuperOrder #accept').prop('checked', false).parent().addClass('disabled');
        jQuery('#saveSuperOrder #submit').prop('disabled', true);
        jQuery('#saveDeliveryAddress .validation-failed').removeClass('validation-failed');
        jQuery('#saveDeliveryAddress').find('.ajax-validation').fadeOut('slow', function () {
          jQuery(this).remove();
        });
        jQuery('#saveDeliveryAddress').find('.cart-errors').remove();

        jQuery('#saveNumberPorting .validation-failed').removeClass('validation-failed');
        jQuery('#saveNumberPorting').find('.ajax-validation').fadeOut('slow', function () {
          jQuery(this).remove();
        });
        jQuery('#saveNumberPorting').find('.cart-errors').remove();
      }
    }
  } else if (step == 'showOrderConfirmation' && jQuery('#current_store_is_telesales').val() == 0) {
    disableAll();
  } else if (step == 'saveIncomeTest') {
    var inputs = jQuery('#saveCustomer').serializeObject();
    setIltName(inputs);
  } else if (step == 'saveCreditCheck') {
    if ((jQuery('#creditcheck-data').html() != undefined
        || jQuery('#creditcheck-data').html().indexOf('Will be filled by javascript') > -1)
        && jQuery('#is_order_edit_mode').val() != 0) {
      checkout.setCreditCheck('', '#saveCustomer');
    }
  }
}

function continueSaveOverview(skipIlt) {
  if(skipIlt == true) {
    jQuery('input[id="skip_store_ilt"]').val(2);
  } else {
    jQuery('input[id="skip_store_ilt"]').val(1);
  }
  jQuery('button[data-step="saveOverview"]').click();
}

function continueSaveSuperOrder(skipIlt) {
  if(skipIlt == true) {
    jQuery('input[id="skip_store_ilt"]').val(2);
  } else {
    jQuery('input[id="skip_store_ilt"]').val(1);
  }
  jQuery('button[data-step="saveSuperOrder"]').click();
}

function showProcessOrderModal(id) {
  if (jQuery('#current_store_is_telesales').val() == 1) {
    // Deactivate previous steps and go to next available step
    disableAll();
    jQuery('.bs-docs-section').addClass('hidden');
    jQuery('#cart-left li').addClass('disabled');
    jQuery('#cart-left li a.pre-order').attr('href', '');
    scrollToSection(jQuery('#saveOverview'));
  }

  // TODO: do we really need to start the polling on Telesales?!
  var polling = new Polling('saveOverview');
  polling.updatePolling(id);
}

var deviceSelectionValidationCallback = function (result, elm) {
  if (result == false && jQuery(elm).parent().hasClass('input-group')) {
    var validationAdvice = jQuery(elm).siblings('.validation-advice');
    if (validationAdvice.length > 0) {
      jQuery.each(validationAdvice, function (index, value) {
        jQuery(value).parent().parent().append(jQuery(value));
      });
    }
  }
};

function updateDeviceSelection(data) {
  if (data) {
    var test = jQuery(data).find('#device-selection-content').parent().html();
    jQuery('#device-selection-content').parent().html(test);
    jQuery('#saveDeviceSelection').parent().removeClass('skip');
    jQuery('#cart-left .bs-sidebar .nav li a[href="#device-selection"]').parent().removeClass('hidden');
  }

  jQuery('#device-selection-content input').on('blur change', function (e) {
    var el = jQuery(this);
    el.removeClass('validation-failed');
    el.parent().find('.ajax-validation').fadeOut('slow', function () {
      jQuery(this).remove();
    });

    var options;
    if (typeof availableDevicesIndirect == "undefined") {
      options = {};
    } else {
      options = {onElementValidate: deviceSelectionValidationCallback};
    }
    Validation.validate(this, options);
  });

  if (typeof availableDevicesIndirect != "undefined") {
    //form.saveDeviceSelection.validator.options.onElementValidate = deviceSelectionValidationCallback;
    jQuery('#device-selection-content .device_name').autocomplete({lookup: availableDevicesIndirect});

    var section = jQuery('#saveDeviceSelection');
    section.on('change', '[id^="deviceselection["][id$="][catalog_price_incl_tax]"]', processPricesNumberSelection);
    section.on('change', '[id^="deviceselection["][id$="][catalog_price]"]', processCatalogPrice);
    section.on('change', '[id^="deviceselection["][id$="][mixmatch]"]', processPriceMixmatch);
    section.on('change', '[id^="deviceselection["][id$="][mixmatch_excl]"]', processPriceMixmatchExcl);
  }
}

function processPricesNumberSelection() {
  var packageId = jQuery(this).data('package-id');
  var basePrice = jQuery(this).val();
  if (basePrice.length != 0) {
    var value = formatPrice(basePrice);
    jQuery(this).val(pad(value));
    jQuery('.catalog_price_' + packageId).val(pad(Math.ceil((value / 1.21) * 100) / 100)).trigger('input');
  } else {
    jQuery('.catalog_price_' + packageId).val('').trigger('input');
  }
  Validation.validate(jQuery('.catalog_price_' + packageId)[0]);
}

function processCatalogPrice() {
  var packageId = jQuery(this).data('package-id');
  var basePrice = jQuery(this).val();
  if (basePrice.length != 0) {
    var value = formatPrice(basePrice);
    jQuery(this).val(pad(value));
    jQuery('.catalog_price_incl_tax_' + packageId).val(pad(Math.ceil((value * 1.21) * 100) / 100)).trigger('input');
  } else {
    jQuery('.catalog_price_incl_tax_' + packageId).val('').trigger('input');
  }
  Validation.validate(jQuery('.catalog_price_incl_tax_' + packageId)[0]);
}

function processPriceMixmatch() {
  var packageId = jQuery(this).data('package-id');
  var basePrice = jQuery(this).val();
  if (basePrice.length != 0) {
    var value = formatPrice(basePrice);
    jQuery(this).val(pad(value));
    jQuery('.mixmatch_excl_' + packageId).val(pad(Math.ceil((value / 1.21) * 100) / 100)).trigger('input');
  } else {
    jQuery('.mixmatch_excl_' + packageId).val('').trigger('input');
  }
  Validation.validate(jQuery('.mixmatch_excl_' + packageId)[0]);
}

function processPriceMixmatchExcl() {
  var packageId = jQuery(this).data('package-id');
  var basePrice = jQuery(this).val();
  if (basePrice.length != 0) {
    var value = formatPrice(basePrice);
    jQuery(this).val(pad(value));
    jQuery('.mixmatch_' + packageId).val(pad(Math.ceil((value * 1.21) * 100) / 100)).trigger('input');
  } else {
    jQuery('.mixmatch_' + packageId).val('').trigger('input');
  }
  Validation.validate(jQuery('.mixmatch_' + packageId)[0]);
}

function formatPrice(price) {
  var test = /^([0-9]|\.|\,)+$/.test(price);
  var value = 0;
  if (test) {
    if (price.indexOf(',') != -1) {
      if (price.indexOf('.') != -1 && price.indexOf('.') > price.indexOf(',')) {
        value = price.replace(',', '');
      } else {
        value = price.replace('.', '').replace(',', '.');
      }
    } else {
      if ((("" + price).split(".").length - 1) < 2) {
        value = price;
      } else {
        value = price.replace('.', '');
      }
    }
  }
  return value;
}

function pad(price) {
  var split = ("" + price).split('.');
  var str = '';
  if (split.length - 1 == 1) {
    if (split[1].length < 2) {
      for (var i = split[1].length; i < 2; i++) {
        str += '0';
      }
    }
  } else {
    str = '.00';
  }

  return (price + str).replace('.', ',');
}

function continueToOrder(event, orderId) {
  event.preventDefault();
  window.location.href = '/?orderId=' + orderId;
}

function updateNumberSelectionList(lastCtn) {
  if (!localStorage.getItem('numberList')) {
    jQuery.ajax({
      async: true,
      type: 'POST',
      data: {'last_ctn': lastCtn},
      url: MAIN_URL + 'checkout/index/retrieveNumberList',
      success: function (data) {
        if (data.error) {
          showModalError(data.message);
          jQuery('#search-phone-number').modal('hide');
        } else {
          localStorage.setItem('numberList', JSON.stringify(data.numbers));
          // Clear the numbers after 10 minutes to refresh the list
          setTimeout(function () {
            localStorage.removeItem('numberList');
          }, 10 * 60 * 1000);
          updateNumberSelection();
        }
      },
      error: function () {
        console.log('Failed to get numbers list');
      }
    });
  } else {
    updateNumberSelection();
  }
}

function refreshNumberList(target, packageId) {
  localStorage.removeItem('numberList');
  var target = jQuery('#search-phone-number').data('target-input');
  var lastCtn = jQuery('[id="' + target + '"]').val();
  var packageId = jQuery('#search-phone-number').data('packageid-input');
  var modalDiv = jQuery('#search-phone-number');
  modalDiv.find('.loading-box').show();
  jQuery('#numbersFound').html('');
  if (localStorage.getItem('numberList')) {
    updateNumberSelectionModal(target, packageId);
  } else {
    jQuery(document).ajaxComplete(function(e, xhr) {
      if (e.target.activeElement.id === "choose-mobile-number" && xhr.status === 200) {
        updateNumberSelectionModal(target, packageId);
      } else {
        modalDiv.find('.loading-box').hide();
      }
    });
  }
  updateNumberSelectionList(lastCtn);
}

function showNumberList(target, packageId) {
  var modalDiv = jQuery('#search-phone-number');
  modalDiv.find('.loading-box').show();
  modalDiv.modal();
  // Manually adding target to modal, because in some cases it didn't.
  modalDiv.data('target-input', target);
  if (localStorage.getItem('numberList')) {
    updateNumberSelectionModal(target, packageId);
  } else {
    jQuery(document).ajaxComplete(function(e, xhr) {
      if (e.target.activeElement.id === "choose-mobile-number" && xhr.status === 200) {
        updateNumberSelectionModal(target, packageId);
      } else {
        modalDiv.find('.loading-box').hide();
      }
    });
  }
  updateNumberSelectionList(jQuery('[id="' + target + '"]').val());
}

function updateNumberSelectionModal(target, packageId) {
  var modalDiv = jQuery('#search-phone-number');
  var phoneNo = jQuery('[id="{0}"]'.format(target)).val();

  modalDiv.data('target-input', target);
  modalDiv.data('packageid-input', packageId);
  modalDiv.find('#numbersFound input').filter('[data-phone-number="{0}"]'.format(phoneNo)).first().prop('checked', true);

  modalDiv.find('.loading-box').hide();
}

function redirectToHomePage(flag) {
  var command = { 'flag' : flag };
  jQuery.ajax({
    url: "/checkout/cart/redirectPage",
    type: 'POST',
    data: command,
    success: function(result) {
      if (result == "true") {
        window.location.href = MAIN_URL;
      } else {
        window.location.href = MAIN_URL + 'checkout/cart/cancelDeliverSuperorder';
      }
    }
  });
}

function showConfirmModal() {

  if (checkout.checkoutDisabled()) {
    checkout.toggleLockAction();
    return false;
  }
  jQuery('#order_confirm_modal').appendTo('body').modal();
}
function showRefundModal() {

  if (checkout.checkoutDisabled()) {
    checkout.toggleLockAction();
    return false;
  }
  jQuery('.select-selected').addClass('hidden');
  jQuery('#order_refund_modal').appendTo('body').modal();
  var method = jQuery('.refund_method_select');

  if (method.length) {
    method.trigger('change');
  }
}

function scrollToSection(thisForm) {
  var nextSection = thisForm.parents('.section').nextAll('.section').first();
  if (nextSection.hasClass('skip')) {
    scrollToSection(nextSection.children('.cart'));
    return;
  }

  nextSection.removeClass('hidden');

  if (nextSection.attr('id') == 'numberselection_page' && nextSection.is(':visible')) {
    window.checkout.initSubmitButton(nextSection.data('ref'), false);
  }
  var to_enable = '#cart-left .bs-sidebar .nav li a[href="#' + nextSection.attr('id') + '"]';
  jQuery('#cart-left li').removeClass('active');
  jQuery(to_enable).parent().removeClass("disabled").addClass("active");
  jQuery('#cart-content').scrollspy('refresh');

  var elem = nextSection.find('.checkout-content:first'),
      container = jQuery('#cart-content');

  scrollAnimateTo(elem, container);
}

// Override createAdvice to enable manipulation of where the advice is shown
Object.extend(Validation, {
  createAdvice: function (name, elm, useTitle, customError) {
    var advice;
    var v = Validation.get(name);
    var errorMsg = useTitle ? ((elm && elm.title) ? elm.title : v.error) : v.error;
    if (customError) {
      errorMsg = customError;
    }
    try {
      if (Translator) {
        errorMsg = Translator.translate(errorMsg);
      }
    }
    catch(e) {}

    // Special class to show the advice for the parent element, and make sure it is not repeated
    if ($(elm).hasClassName('parent-shown-advice')) {
      elm = $(elm).up().select('input').first();
      if (advice = Validation.getAdvice(name, elm)) {
        return advice;
      }
    }

    // Since id's have brackets which are seen as an other type
    // of selector than ID, we wil need to escape the brackets.
    const inputId = '#' + Validation.getElmID(elm).replace(/\[/g, '\\[').replace(/]/g, '\\]').replace(/\./g, '\\.');
    const inputEl = jQuery(inputId);

    isCheckout = !!jQuery(elm).closest('#checkout-wrapper');
    isServiceability = !!jQuery(inputId).closest('#topaddressCheckForm');

    // Checkout is using a newer styling than the rest of OSE.
    if (isCheckout || isServiceability) {
      var inputWrapperEl = inputEl.closest('.form-group');
      inputWrapperEl.addClass('error');
      inputEl.attr('data-toggle', 'tooltip');
      inputEl.attr("title",'');
      inputEl.attr("data-original-title", errorMsg);
      return false;
    } else {
      inputEl.attr("data-toggle","tooltip");
      inputEl.attr("title",'');
      inputEl.attr("data-original-title", errorMsg);
      advice = '<div class="tooltip fade top in" id="advice-' + name + '-' + Validation.getElmID(elm) + '"><div class="tooltip-arrow" style="left: 50%;"></div><div class="tooltip-inner">' + errorMsg + '</div></div>';
      Validation.insertAdvice(elm, advice);
      advice = Validation.getAdvice(name, elm);
      if ($(elm).hasClassName('absolute-advice')) {
        var dimensions = $(elm).getDimensions();
        var originalPosition = Position.cumulativeOffset(elm);

        advice._adviceTop = (originalPosition[1] + dimensions.height) + 'px';
        advice._adviceLeft = (originalPosition[0]) + 'px';
        advice._adviceWidth = (dimensions.width) + 'px';
        advice._adviceAbsolutize = true;
      }

      return advice;
    }
  },
  showAdvice : function(elm, advice, adviceName){
    isCheckout = !!jQuery(elm).closest('#checkout-wrapper');
    isServiceability = !!jQuery(elm).hasClass('top-address-validations');
    // Checkout is using a newer styling than the rest of OSE.
    if (isCheckout) {
      if (isServiceability) {
          jQuery(elm).tooltip({trigger: 'hover', placement: 'bottom', animation: false});
      } else {
          jQuery(elm).tooltip({trigger: 'hover', placement: 'top', animation: false});
      }

    } else {
      if(!elm.advices){
        elm.advices = new Hash();
      }
      else{
        elm.advices.each(function(pair){
          if (!advice || pair.value.id != advice.id) {
            // hide non-current advice after delay
            this.hideAdvice(elm, pair.value);
          }
        }.bind(this));
      }
      elm.advices.set(adviceName, advice);
      if(typeof Effect == 'undefined') {
        advice.style.display = 'block';
      } else {
        if(!advice._adviceAbsolutize) {
          new Effect.Appear(advice, {duration : 1 });
        } else {
          Position.absolutize(advice);
          advice.show();
          advice.setStyle({
            'top':advice._adviceTop,
            'left': advice._adviceLeft,
            'width': advice._adviceWidth,
            'z-index': 1000
          });
          advice.addClassName('advice-absolute');
        }
      }
    }
  }
});

function scrollToFirstError(inputEl) {
  if (jQuery('.validation-failed').length !== 0) {
    var scrollTo = jQuery(inputEl);
    var container = jQuery('#cart-content');
    if (scrollTo.length) {
      container.animate({
        scrollTop: scrollTo.offset().top - container.offset().top + container.scrollTop() - 70
      });
    }
  }
}

function summaryDataSynchronization(element, changeValue) {
  changeValue = typeof changeValue !== 'undefined' ? changeValue : false;
  var className = element.attr('id');
  var sourceValue = (element.is('input') || element.is('select')) ? element.val() : element.text();
  if($(element).is(':radio')) {
    className = element.attr('data-fill-name');
    sourceValue = element.attr('data-fill-value');
  }

  var synchronizedElement = jQuery('[class="' + className + '-summary"]');
  if(element.is('select')) {
    var selectOptionText = jQuery('[id=\'' + className + '\'] option[value=\'' + sourceValue + '\']').text();
    if (sourceValue == '') {
      return synchronizedElement.text("No");
    } else {
      return synchronizedElement.text(selectOptionText);
    }
  }
  if(changeValue) {
    return synchronizedElement.val(sourceValue);
  }
  return synchronizedElement.text(sourceValue);
}

function updateNumberSelection(data) {
  if (data) {
    var test = jQuery(data).find('#numbers-list').parent().html();
    var summary = jQuery(data).find('#numbers-list-summary').html();
    jQuery(".autocomplete-suggestions").css("position", "absolute");
    jQuery('#numbers-list').parent().html(test);
    jQuery('#numbers-list-summary').html(summary);
    jQuery('[data-toggle="tooltip"]').tooltip();
    jQuery('.input-wrapper').initInputElement();
    jQuery("*[rel=tooltip]").tooltip();
    jQuery('#numbers-list input').on('blur change', function (e) {
      var el = jQuery(this);
      el.removeClass('validation-failed');
      el.parent().find('.ajax-validation').fadeOut('slow', function () {
        jQuery(this).remove();
      });

      Validation.validate(this);
    });
  }

  if (localStorage.getItem('numberList')) {
    var selectedNumbers = jQuery('input')
      .filter(function() {
        return this.id.match(/numberselection\[\d\]\[mobile_number\]/);
      });

    var selectedNumbersValues = [];
    selectedNumbers.each(function(i, item) {
      selectedNumbersValues.push(jQuery(item).val());
    });

    var numbers = jQuery.parseJSON(localStorage.getItem('numberList'));
    // Fill the modal with the received numbers
    var target = jQuery('#search-phone-number').data('target-input');
    var packageId = jQuery('#search-phone-number').data('packageid-input');
    // Save filtered numbers for use later
    const options = numbers.filter(function (number) {
      return selectedNumbersValues.indexOf(number) < 0;
    }).map(function(number) {
      return (
        '<div class="number-selection-option">' +
          '<div class="number-select-option-image"></div>' +
            number +
            '<button ' +
              'class="btn small select" ' +
              'data-phone-number="' + number + '" ' +
              'onclick="setPhoneNumber(' + number + ')" ' +
              'type="button" ' +
              'id="mobileSelect_' + number + '"' +
            '>' +
              Translator.translate('Select') +
            '</button>' +
        '</div>'
      )
    })
    jQuery('#numbersFound').html(options);
  }
}

function updateSelectDevice(data) {
  if (data) {
    var test = jQuery(data).find('#device-selection').parent().html();
    jQuery('#device-selection').parent().html(test);
    jQuery('#saveNumberSelection').parent().removeClass('skip');
    jQuery('#cart-left .bs-sidebar .nav li a[href="#device-selection"]').parent().removeClass('hidden');
    jQuery('.tip-trig-hover-popover').popover({
      trigger: 'hover',
      animation: false
    });
  }

  jQuery('#saveNumberSelection input').on('blur change', function (e) {
    var el = jQuery(this);
    el.removeClass('validation-failed');
    el.parent().find('.ajax-validation').fadeOut('slow', function () {
        jQuery(this).remove();
    });

    var options;
    if (typeof availableDevicesIndirect == "undefined") {
        options = {};
    } else {
        options = {onElementValidate: deviceSelectionValidationCallback};
    }
    Validation.validate(this, options);
  });
  /*jQuery('#saveNumberSelection input').on('keydown', function (e) {
    var deviceName_id = jQuery('#saveNumberSelection .device_name').attr('id');
    autocomplete(document.getElementById(deviceName_id), availableDevicesIndirect);
  });*/
  if (typeof availableDevicesIndirect != "undefined") {
    jQuery('#saveNumberSelection .device_name').autocomplete({lookup: availableDevicesIndirect});

    var section = jQuery('#saveNumberSelection');
    section.on('change', '[id^="deviceselection["][id$="][catalog_price_incl_tax]"]', processPricesNumberSelection);
    section.on('change', '[id^="deviceselection["][id$="][catalog_price]"]', processCatalogPrice);
    section.on('change', '[id^="deviceselection["][id$="][mixmatch]"]', processPriceMixmatch);
    section.on('change', '[id^="deviceselection["][id$="][mixmatch_excl]"]', processPriceMixmatchExcl);
  }

  var checkIfEmpty = function(el) {
    const isEmpty = el && !el.attr('placeholder') && el.val() == '';
    if (isEmpty) {
      el.parent().addClass('is-empty');
    } else {
      el.parent().removeClass('is-empty');
    }
  };

  jQuery('#numbers-list').find('.form-group input, .form-group textarea').each(function() {
    checkIfEmpty(jQuery(this));
  });
}

function setPhoneNumber(number) {
  if (number) {
    // check whether current number is selected
    const button = jQuery('#mobileSelect_' + number);
    const isSelected = button.hasClass('selected');

    if (isSelected) {
      resetPhoneNumber(number);
    } else {
      var target = jQuery('#search-phone-number').data('target-input');
      var packageId = jQuery('#search-phone-number').data('packageid-input');
      var oldNumber = jQuery('[id="' + target + '"]').val();

      // Setting correct value to input fields
      jQuery('[id="' + target + '"]').val(number);
      jQuery('[id="' + target + '"]').trigger('change');
      jQuery('[class="' + target + '"]').val(number);
      jQuery('[class="' + target + '"]').trigger('change');
      jQuery('.numberselection_'+ packageId +'_mobile_number-summary').html(number);
      jQuery('[class="' + target + '"]').trigger('change');

      // Setting button state
      button.addClass('selected');
      button.text(Translator.translate('Selected'));
      jQuery('#mobileSelect_'+ oldNumber).removeClass('selected');
      jQuery('#mobileSelect_'+ oldNumber).text(Translator.translate('Select'));

      // Disable refresh numbers button
      //jQuery('#refresh_fixedextra_number').addClass('disabled');
    }
  }
}

function resetPhoneNumber(number) {
  if (number) {
    const button = jQuery('#mobileSelect_'+ number);

    // Setting correct value to input fields
    var target = jQuery('#search-phone-number').data('target-input');
    var packageId = jQuery('#search-phone-number').data('packageid-input');
    jQuery('[id="' + target + '"]').val('');
    jQuery('[id="' + target + '"]').trigger('change');
    jQuery('.select_number').removeClass('disabled');

    // Setting button state
    button.removeClass('selected');
    button.text(Translator.translate('Select'));

    // Enable refresh numbers button
    //jQuery('#refresh_fixedextra_number').removeClass('disabled');
  }
}

function doCreditCheck(addresses, orderId, creditFailed) {
  checkout.showVodError(false);
  if (!addr || jQuery.isEmptyObject(addr)) {
    addr = addresses;
  }

  if (!cFailed) {
    cFailed = creditFailed;
  }

  jQuery.each(addresses, function (id, el) {
    if(el['package_type'] == 'mobile') {
      if (jQuery('#credit-check-address-' + id).length != 0 ) {
        jQuery('#credit-check-address-' + id).html(el['street']);
        if (('houseno' in el))
          jQuery('#credit-check-address-' + id).html(jQuery('#credit-check-address-' + id).html() + ' ' + el['houseno']);
        if (('addition' in el))
          jQuery('#credit-check-address-' + id).html(jQuery('#credit-check-address-' + id).html() + ' ' + el['addition']);
        jQuery('#credit-check-address-' + id).html(jQuery('#credit-check-address-' + id).html() + '<br />' + el['postcode'] + ' ' + el['city']);
      }
      creditCheck[id] = {'id': id, 'checked': false};
    }
  });
  jQuery('.bs-docs-section').addClass('hidden');
  jQuery('#creditcheck-waiting').removeClass('hidden');
  jQuery('#cart-left li').addClass('disabled');
  jQuery('#cart-left li a.pre-order').attr('href', '');
  jQuery('#credit-check-menu').removeClass('hidden').removeClass('disabled').addClass('active');
  if (orderId) {
    jQuery('#credit-check-cancel-button').attr('onclick', 'cancelOrder(' + orderId + ')');
    jQuery('#order-edit').attr('onclick', 'editOrder()');
    //jQuery('#redo-credit-check').attr('onclick', 'redoCreditCheck(' + orderId + ')');
  } else {
    jQuery('#credit-check-cancel-button').addClass('hidden');
  }
  jQuery('#cart-content').scrollspy('refresh');
  if (!creditFailed) {
    performCreditCheck(orderId);
    intervalID = setInterval(function () {
      performCreditCheck(orderId);
    }, pollingTimeGap);
  } else {
    showCreditCheckFailed();
  }
}

var searchLoadingIndicatorTimeout = false;
function searchOtherAddress(self) {
  var container = jQuery(self).closest('.search-address');
  var data = {};
  ['postcode', 'houseno', 'addition'].forEach(function(field){
    data[field] = container.find('.search-address-form input[id*="'+field+'"]').val().replace(/ /g,'').trim();
  });

  var streetField = container.find('.search-address-form input[id*="street"]');
  var cityField = container.find('.search-address-form input[id*="city"]');
  if (searchLoadingIndicatorTimeout !== false) {
    clearTimeout(searchLoadingIndicatorTimeout);
  }
  var country = jQuery("[name='address[otherAddress][country_id]']").val();
  if (data.postcode !== '' && data.houseno !== '' && country == 'NL') {
    searchLoadingIndicatorTimeout = setTimeout(function(){
      streetField.closest('.form-group').addClass('loading');
      cityField.closest('.form-group').addClass('loading');
      searchLoadingIndicatorTimeout = false;
    }, 100);
    jQuery.post(
      '/checkout/index/searchAddress',
      data,
      function (result) {
        if (searchLoadingIndicatorTimeout !== false) {
          clearTimeout(searchLoadingIndicatorTimeout);
        }
        if (result.fields && (result.fields.street || result.fields.city)) {
          streetField.closest('.form-group').removeClass('loading');
          streetField.val(result.fields.street).trigger('input');
          cityField.closest('.form-group').removeClass('loading');
          cityField.val(result.fields.city).trigger('input');
        } else {
          streetField.closest('.form-group').removeClass('loading');
          streetField.val('').trigger('input');
          cityField.closest('.form-group').removeClass('loading');
          cityField.val('').trigger('input');
        }
      }
    );
  } else if (country == 'NL') {
    streetField.closest('.form-group').removeClass('loading');
    streetField.val('').trigger('input');
    cityField.closest('.form-group').removeClass('loading');
    cityField.val('').trigger('input');
  }
}

function copyToClipboard(element) {
  var $temp = jQuery("<input>");
  jQuery(element).parent().append($temp);
  $temp.val(jQuery(element).html()).select();
  document.execCommand("copy");
  $temp.remove();
}

function showPealOrderId(elem) {
  jQuery('#order_details_modal').appendTo('body').modal();
  var orderNumber = jQuery(elem).attr('data-ordernumber');
  jQuery('#modal-order-number').text(Translator.translate('Order')+' '+orderNumber+' '+Translator.translate('details'));
  jQuery.ajax({
    async: true,
    type: 'post',
    data:{orderNo: orderNumber},
    url: MAIN_URL + 'checkout/index/showPealOrder',
     }).success(function(resp){
        if(resp.peal_order_id) {
          jQuery('#peal-order-number').text(resp.peal_order_id);
          jQuery('#peal-order-value').val(resp.peal_order_id);
          jQuery('#peal-order-copy').text(resp.peal_order_id);
        }  else {
          jQuery('#peal-order-number').text('N/A');
          jQuery('#peal-order-value').val('N/A');
          jQuery('#peal-order-copy').text('N/A');
        }
  });
}

function orderConfirmationViewOrder(event, orderId) {
  event.preventDefault();
  window.location.href = '/?orderId=' + orderId
}

function checkoutMoveWorkitem(event, orderId) {
  event.preventDefault();
  jQuery.ajax(MAIN_URL + 'checkout/cart/moveWorkitem', {
    type: 'POST',
    data: {orderId: orderId},
    success: function (response) {
      if (response.error) {
        showValidateNotificationBar("<strong>WORKITEM NOT CREATED! </strong>" + response.message, 'error');
      } else {
        showValidateNotificationBar("<strong>WORKITEM CREATED! </strong> " + response.message, 'success');
        jQuery("#checkout-move-workitem").addClass("disabled");
      }
    },
    error: function () {
      console.log('Failed to retrieve the data');
    }
  });
}

function enableContractSubmitOrder() {
  const isChecked = jQuery('.checkout-contract-signed-checkbox').is(':checked');
  if (isChecked) {
    jQuery('#contract-continue-button').removeAttr('disabled');
  } else {
    jQuery('#contract-continue-button').attr('disabled', 'disabled');
  }
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}


function getFirstDate(wishDates) {
  for (var i=0;i<wishDates.length;i++) {
    var dt = new Date(wishDates[i].slice(-4)+"-"+wishDates[i].slice(2,4)+"-"+wishDates[i].slice(0,2));
    var currentDate = new Date();
    currentDate.setHours(0,0,0,0);
    if (dt.getTime() >= currentDate.getTime()) {
      var minDate = dt.getDate() + '-' + (dt.getMonth() + 1) + '-' + dt.getFullYear();
      break;
    }
  }
  return minDate;
}
