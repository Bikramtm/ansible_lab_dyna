(function ($) {
    window.LeftSidebar = function () {
        var alignLeftColumn = function () {
                $('.col-left.sidebar').addClass('expanded');

                //Add Backdrop
                addBackdrop();
            },
            toggleLeftSideBarOnCheckout = function(stayOpen){
                var enlargeButton = $('.enlargeSearch');
                if ($(enlargeButton).parent().hasClass('smallView')) {
                    $(enlargeButton).parent().removeClass('smallView').addClass('mediumView');
                    $(enlargeButton).removeClass('right-direction').addClass('left-direction');
                    contractLeftSide();
                    return;
                }
                if ($(enlargeButton).parent().hasClass('mediumView')) {
                    $(enlargeButton).parent().removeClass('mediumView').addClass(stayOpen ? 'bigView':'smallView');
                    if(!stayOpen){
                        $(enlargeButton).parent().removeAttr("style");
                        $(enlargeButton).removeClass('left-direction').addClass('right-direction');
                    }else{
                        alignLeftColumn();
                        $(enlargeButton).removeClass('contracted').addClass('expanded');
                        toggleCustomerDetailsPage(true);
                    }
                    return;
                }

                if ($(enlargeButton).parent().hasClass('bigView') && !stayOpen) {
                    $(enlargeButton).parent().removeClass('bigView').addClass('mediumView');
                    $(enlargeButton).removeClass('expanded').addClass('contracted');
                    contractLeftSide();
                    return;
                }
            },
            toggleLeftSideBar = function (stayOpen, noCampaignCall) {
              if (!stayOpen) {
                LeftSidebar.contractLeftSide();
                return;
              }
              if(typeof stayOpen === 'undefined'){
                return;
              }
                //check if we are on the checkout page
                if(typeof $('.checkout.bigView').find('#cart')[0] !== 'undefined'){
                    toggleLeftSideBarOnCheckout(stayOpen);
                    return;
                }
                var direction = '';
                var enlargeButton = $('.enlargeSearch');
                // if a search is done and the left side bar is already expanded: do nothing
                if (stayOpen && $(enlargeButton).hasClass('expanded')) {
                    alignLeftColumn();
                    return;
                }
                if ($(enlargeButton).hasClass('left-direction')) {
                    direction = 'left';
                    var leftSide = $('.col-right').outerWidth();
                    $(enlargeButton).removeClass('left-direction');
                    $(enlargeButton).addClass('right-direction');

                    //Remove Backdrop
                    if (!stayOpen)
                    {
                        removeBackdrop();
                    }
                }
                else {
                    direction = 'right';
                    var rightSide = $('.col-right').outerWidth();
                    $(enlargeButton).removeClass('right-direction');
                    $(enlargeButton).addClass('left-direction');
                }

                // contract - show only the sticker
                if ($(enlargeButton).hasClass('expanded')) {
                    contractLeftSide();
                } else {
                    if ($(enlargeButton).parent().hasClass('smallView')) {
                        $(enlargeButton).parent().removeClass('smallView');
                        $(enlargeButton).parent().addClass('mediumView');
                        contractLeftSide();
                        return;
                    }
                    if ($(enlargeButton).parent().hasClass('mediumView')) {
                        $(enlargeButton).parent().removeClass('mediumView');
                        $(enlargeButton).parent().addClass('smallView');
                        $(enlargeButton).parent().removeAttr("style");
                        return;
                    }

                    var buttonExpanded = $('button.expanded');
                    //if the other panel is already expanded, contract it back
                    if (buttonExpanded.length > 0) {
                        buttonExpanded.first().trigger('click');
                    }

                    alignLeftColumn();
                    $(enlargeButton).removeClass('contracted').addClass('expanded');
                    toggleCustomerDetailsPage(true);
                }
            },
            contractLeftSide = function (keepBackdrop) {
              var elm = jQuery("#main_lock_btn");
              if(elm.data("state") == "unlocked" && elm.data('edited') == 1) {
                orderDetails.switchLock(elm, 'unlocked');
              } else if (elm.data("state") == "unlocked") {
                $('.col-left.sidebar').removeClass('expanded');
                $('.col-left.sidebar').removeClass('veryBigView');
                $('.enlargeSearch').removeClass('expanded').addClass('contracted');
                toggleCustomerDetailsPage(false);
                if (keepBackdrop !== true) {
                  removeBackdrop();
                }
                orderDetails.switchLock(elm, 'unlocked');
              } else {
                $('.col-left.sidebar').removeClass('expanded');
                $('.col-left.sidebar').removeClass('veryBigView');
                $('.enlargeSearch').removeClass('expanded').addClass('contracted');
                toggleCustomerDetailsPage(false);
                if (keepBackdrop !== true) {
                  removeBackdrop();
                }
              }
            },
            resizeLeftPanel = function () {
                if (new Date() - rtime < delta) {
                    setTimeout(resizeLeftPanel, delta);
                } else {
                    window.timeout = false;
                    if (!supports_calc()) {
                        CSSCalc();
                    }
                    if ($('.enlargeSearch').hasClass('expanded')) {
                        alignLeftColumn();
                    }
                }
            },
            toggleCustomerDetailsPage = function(display){
                if (display)
                {
                    return;
                }
                $('#customer-menu > li.active').removeClass('active');
            },
            setCustomerMode2ButtonsState = function( enable ) {
                var customerDetailsPage = $('.customer_ctns');
                if(!customerDetailsPage)
                    return;
                if (enable) {
                    customerDetailsPage.find('#product_info_left').removeClass('disabled');
                    customerDetailsPage.find('#cart_info_left').removeClass('disabled');
                    customerDetailsPage.find('#order_info_left').removeClass('disabled');
                }else{
                    customerDetailsPage.find('#product_info_left').addClass('disabled');
                    customerDetailsPage.find('#cart_info_left').addClass('disabled');
                    customerDetailsPage.find('#order_info_left').addClass('disabled');
                }
            },
            setCustomerHouseHoldButtonsState = function( enable ) {
                var customerDetailsPage = $('.customer_ctns');
                if(!customerDetailsPage)
                    return;
                if (enable) {
                    customerDetailsPage.find('#household_info_left').removeClass('disabled');
                }else{
                    customerDetailsPage.find('#household_info_left').addClass('disabled');
                }
            },
            addBackdrop = function() {
                $('.header-container').css('z-index', 1028);
                $('#top-panels').css('z-index', 1027);
                $('.col-right.sidebar').css('z-index', 0);
                if ($('.custom-backdrop').length === 0) {
                    $('<div class="custom-backdrop"></div>').appendTo(document.body);
                }
                $('.custom-backdrop').fadeIn(300);
            },
            removeBackdrop = function() {
                $('.header-container').css('z-index', 3002);
                $('#top-panels').css('z-index', 3001);
                $('.col-right.sidebar').css('z-index', 1002);
                if ($('.custom-backdrop').length !== 0) {
                    $('.custom-backdrop').fadeOut(300);
                }
            };
        return {
            resizeLeftPanel: resizeLeftPanel,
            toggleLeftSideBar: toggleLeftSideBar,
            contractLeftSide: contractLeftSide,
            setCustomerMode2ButtonsState: setCustomerMode2ButtonsState,
            setCustomerHouseHoldButtonsState: setCustomerHouseHoldButtonsState
        }

    }();
    window.CustomerFiltersSearch = function () {
        var keepFormFieldsForLegacy = {
            'cable': [
                'customer'
            ],
            'dsl': [
                'customer',
                'first_name',
                'last_name',
                'birthday',
                'zipcode',
                'city',
                'street',
                'no',
                'addNo',
                'telephone_number'
            ],
            'mobile': [
                'customer',
                'first_name',
                'last_name',
                'birthday',
                'zipcode',
                'city',
                'street',
                'no'
            ]
        };

        var savedFormFieldsLegacy = {
            'customer' : '',
            'first_name' : '',
            'last_name' : '',
            'birthday' : '',
            'zipcode' : '',
            'city' : '',
            'street' : '',
            'no' : '',
            'addNo' : ''
        };
        var keepFieldsValuesForLegacy = {'addNo': '', 'disabled': true};
        /**
         * when the advanced search is off:
         * - the advanced search input fields must be hidden;
         * - the bullet must in "off-state" (meaning it is now grey)
         * - the value of the radio is "0"
         */
        var offStateHandleAdvancedSearch = function () {
                addNormalSearchValidation();
                var customerSearchForm = $('#customer_search_form');
                var onAdvancedClass = ".on-advanced";
                // hide the fields that are available only when advanced search is enabled
                customerSearchForm.find(onAdvancedClass).addClass('hide');
                customerSearchForm.find(onAdvancedClass).removeClass('show');
                $('.advanced-search-label').addClass('off-state');
                $('#customer_search').find('.advanced-search-switch').find('.toggle-button').data('state', true).addClass('off-state').removeClass('on-state');
                $('#advanced-search').val(0);
                checkIfActivateNormalSearch();
            },
            /**
             * when the legacy search is off:
             * - all the initial input set must be enabled
             * - the legacy radio buttons (mobile, dsl, cable) must be hidden
             * - the legacy search input fields must be hidden;
             * - the bullet must in "off-state" (meaning it is now grey)
             * - the value of the radio is "0"
             * - bas the fields that might have been hidden using cable search should be shown
             */
            offStateHandleLegacySearch = function () {
                // set all the input fields to be enabled
                enableAllInputFields();
                // hide the radio buttons for mobile,dsl, cable
                $('#legacy-search-filters').hide();
                // show the label as disabled
                $('.legacy-search-label').addClass('off-state');
                var customerSearchForm = $('#customer_search_form');
                var onLegacyFilters = ".on-legacy-filter";
                // hide all the additional legacy filters
                customerSearchForm.find(onLegacyFilters).addClass('hide');
                customerSearchForm.find(onLegacyFilters).removeClass('show');
                $('#customer_search').find('.legacy-search-switch').find('.toggle-button').data('state', true).addClass('off-state').removeClass('on-state');
                $('#legacy-search').val(0);
                // show the normal-fields that might have been disabled by toggling cable.
                var legacyOffShow = ".legacy-off-show";
                var legacyOffHide = ".legacy-off-hide";
                customerSearchForm.find(legacyOffShow).removeClass('hide');
                customerSearchForm.find(legacyOffShow).addClass('show');
                customerSearchForm.find(legacyOffHide).addClass('hide');
                customerSearchForm.find(legacyOffHide).removeClass('show');
                //customerSearchForm.find('.on-cable-hide').removeClass('hide');
                //customerSearchForm.find('.on-cable-hide').addClass('show');
                var additionInput = $('.customer-search-form').find('input[name="addNo"]');
                if(keepFieldsValuesForLegacy['disabled']){
                    additionInput.val(keepFieldsValuesForLegacy['addNo']);
                }
                var legacyType = $('input[name=legacy_type]:checked', '#customer_search_form').val();
                if((legacyType == 'mobile')){
                    keepFieldsValuesForLegacy['disabled'] = false;
                }
                checkIfActivateNormalSearch();
            },

            /**
             * when the exact search is off:
             * - all the initial input set must be enabled
             * - the bullet must in "off-state" (meaning it is now grey)
             * - the value of the radio is "0"
             * - bas the fields that might have been hidden should be shown
             */
            offStateHandleExactSearch = function () {
                // set all the input fields to be enabled
                enableAllInputFields();

                // show the label as disabled
                $('.exact-search-label').addClass('off-state');
                var customerSearchForm = $('#customer_search_form');

                $('#customer_search').find('.exact-search-switch').find('.toggle-button').data('state', true).addClass('off-state').removeClass('on-state');
                $('#exact-search').val(0);
                checkIfActivateNormalSearch();
            },
            /**
             * when the advanced search is on:
             * - set the legacy to off (the advanced and legacy are mutually exclusive)
             * - show the specific advanced search fields
             * - the bullet must in "on-state" (meaning it is now green)
             * - the value of the radio is "1"
             */
            onStateHandleAdvancedSearch = function () {
                // set the legacy to off
                offStateHandleLegacySearch();
                offStateHandleExactSearch();
                addAdvancedSearchValidation();
                var customerSearchForm = $('#customer_search_form');
                var onAdvancedValueShowClass = ".on-advanced";
                // show the specific advanced filters inputs
                customerSearchForm.find(onAdvancedValueShowClass).addClass('show');
                customerSearchForm.find(onAdvancedValueShowClass).removeClass('hide');
                $('.advanced-search-label').removeClass('off-state');
                $('#customer_search').find('.advanced-search-switch').find('.toggle-button').data('state', false).addClass('on-state').removeClass('off-state');
                $('#advanced-search').val(1);
                checkIfActivateNormalSearch();
            },
            /**
             * when the legacy search is on:
             * - set the advanced to off (the advanced and legacy are mutually exclusive)
             * - show the radio buttons for legacy search: mobile, dsl, cable
             * - the bullet must in "on-state" (meaning it is now green)
             * - the value of the radio is "1"
             * - check if the radio button for legacy is already marked (there is already selected a mobile, dsl or cable) and show/disable specific fields
             */
            onStateHandleLegacySearch = function () {
                offStateHandleAdvancedSearch();
                offStateHandleExactSearch();
                var legacySearchFilters = $('#legacy-search-filters');
                legacySearchFilters.show();
                if (legacySearchFilters.find('input[name="legacy_type"]:checked').length == 0) {
                    legacySearchFilters.find('input[name="legacy_type"]:first').trigger('click');
                }
                $('.legacy-search-label').removeClass('off-state');
                $('#customer_search').find('.legacy-search-switch').find('.toggle-button').data('state', false).addClass('on-state').removeClass('off-state');
                $('#customer_search').find('.cable-legacy-search-switch').data('state', false).addClass('on-state').removeClass('off-state');
                $('#legacy-search').val(1);
                var customerSearchForm = $('#customer_search_form');
                var legacyOffHide = ".legacy-off-hide";
                customerSearchForm.find(legacyOffHide).removeClass('hide');
                customerSearchForm.find(legacyOffHide).addClass('show');
                showHideDisableLegacySpecificFilters();
                checkIfActivateNormalSearch();
            },
            /**
             * when the exact search is on:
             * - set the advanced/legacy to off (the advanced, exact and legacy are mutually exclusive)
             * - the bullet must in "on-state" (meaning it is now green)
             * - the value of the radio is "1"
             */
            onStateHandleExactSearch = function () {
                offStateHandleAdvancedSearch();
                offStateHandleLegacySearch();

                $('.exact-search-label').removeClass('off-state');
                $('#customer_search').find('.exact-search-switch').find('.toggle-button').data('state', false).addClass('on-state').removeClass('off-state');
                $('#exact-search').val(1);

                showHideDisableExactSpecificFilters();
                checkIfActivateNormalSearch();
            },
            /**
             * when the input fields have value
             * - the background is greenish
             * - the reset link is available for use
             * else:
             * - no background is present on the input
             */
            removeOrAddFilterInputsStyles = function () {
                var input = $(".customer-search-form input.form-control");
                if (input.map(function (idx, elem) {
                        if ($(elem).val() != "") {
                            $(elem).addClass('input-background');
                            return $(elem);
                        }
                        else {
                            $(elem).removeClass('input-background');
                        }
                    }).size() > 0) {
                }
            },
            /**
             * on refresh initialize the advanced search:
             * - must be in off state
             */
            initAdvancedSearch = function () {
                offStateHandleAdvancedSearch();
            },
            /**
             * on refresh initialize the legacy search:
             * - must be in off state
             */
            initLegacySearch = function () {
                offStateHandleLegacySearch();
            },
            /**
             * on refresh initialize the exacy search:
             * - must be in off state
             */
            initExactSearch = function () {
                offStateHandleExactSearch();
            },
            /**
             * on change advanced search toggle treat the off/on state
             * - start with a clean set of input fields: set them all on enabled
             */
            onSwitchAdvancedSearchChange = function (elem) {
                enableAllInputFields();
                resetValidations();
                ($(elem).data('state') == true) ? onStateHandleAdvancedSearch() : offStateHandleAdvancedSearch();
            },
            /**
             * on change legacy search toggle treat the off/on state
             * - start with a clean set of input fields: set them all on enabled
             */
            onSwitchLegacySearchChange = function (elem) {
                enableAllInputFields();
                resetValidations();
                ($(elem).data('state') == true) ? onStateHandleLegacySearch() : offStateHandleLegacySearch();
            },
            /**
             * on change exact search toggle treat the off/on state
             * - start with a clean set of input fields: set them all on enabled
             */
            onSwitchExactSearchChange = function (elem) {
                enableAllInputFields();
                resetValidations();
                ($(elem).data('state') == true) ? onStateHandleExactSearch() : offStateHandleExactSearch();
            },
            /**
             * on change legacy search toggle treat the off/on state
             * - start with a clean set of input fields: set them all on enabled
             */
            toggleResetFiltersEvent = function () {
                //if any input is not blank, show the link
                $(".customer-search-form input.form-control").bind('change keyup', function (el, index) {
                    removeOrAddFilterInputsStyles();
                });
            },
            /**
             * initialize the customer filters
             */
            initCustomerFilters = function () {
                enableAllInputFields();
                initAdvancedSearch();
                initLegacySearch();
                initExactSearch();
                removeOrAddFilterInputsStyles();
                toggleResetFiltersEvent();
            },
            /**
             * reset all input in the filters
             */
            resetFilters = function (onClick, keepFieldValues) {
                if(!onClick) {
                    var onClick = false;
                }
                var form = $('.customer-search-form');
                // remove all values from filters
                form.find('input[type=text], textarea').prop('disabled', false);

                if (!keepFieldValues) {
                    form.find('input[type=text], textarea').val('');
                }

                if(onClick){
                    form.find('div.toggle-button.on-state').each(function() {
                        $(this).click();
                    });
                }
                removeOrAddFilterInputsStyles();
                resetValidations();
                addValidationClassesForLegacyFilters();
                // Remove search regults content OMNVFDE-4045
                removeSearchResultContents();
                removeValidationClassesForLegacyFilters();
            },
            /**
             * reset all input validations
             */
            removeSearchResultContents = function () {
                if ($('.enlargeSearch').hasClass('expanded')) {
                    $('.enlargeSearch').trigger('click');
                }
                $('#customer-search-results').empty();
            },
            /**
             * reset all input validations
             */
            resetValidations = function () {
                new VarienForm('customer_search_form').validator.reset();
                removeValidationClassesForLegacyFilters();
                removeValidationClassesForExactFilters();
            },
            /**
             * when dls, cable or mobile is clicked from the legacy filters we should disable specific fields and show new specific fields
             * - first we enable all the input fields and after we apply the logic for the legacy option selected
             */
            showHideDisableLegacySpecificFilters = function () {
                var onClick = false;
                var keepFieldValues = true;
                resetFilters(onClick, keepFieldValues);

                // the legacy option selected: mobile,dsl or cable
                var legacyValue = $('input[name=legacy_type]:checked', '#customer_search_form').val();
                // the fields with this class should be disabled for this legacy option (these are fields from the initial search inputs that are not available for this legacy search)
                var onLegacyValueDisableClass = ".on-" + legacyValue + "-disable";
                // the fields with this class should be shown for this legacy option (these are fields available only for this legacy)
                var onLegacyValueShowClass = ".on-" + legacyValue + "-show";
                // the fields with this class should be hidden for this legacy option (these are fields that are available for other legacies, not for the one selected now)
                var onLegacyValueHideClass = ".on-" + legacyValue + "-hide";
                var customerSearchForm = $('#customer_search_form');
                customerSearchForm.find('.cable-customernumber').addClass('hide');
                customerSearchForm.find('.cable-customernumber').removeClass('show');
                customerSearchForm.find('.mobil-dsl-customernumber').addClass('show');
                customerSearchForm.find('.mobil-dsl-customernumber').removeClass('hide');

                // enable all input fields
                enableAllInputFields();
                // disable the fields from the initial set of inputs that are not available for this legacy
                customerSearchForm.find(onLegacyValueDisableClass).find('input').attr('disabled', true);
                customerSearchForm.find(onLegacyValueDisableClass).find('input').addClass('disabled');
                // show the specific legacy fields
                customerSearchForm.find(onLegacyValueShowClass).addClass('show');
                customerSearchForm.find(onLegacyValueShowClass).removeClass('hide');
                // hide the fields that were available for other legacies
                customerSearchForm.find(onLegacyValueHideClass).addClass('hide');
                customerSearchForm.find(onLegacyValueHideClass).removeClass('show');
                // add the validation classes for the legacy filters
                addValidationClassesForLegacyFilters();

                // for cable there are specific other rules that should be verified regarding the fields
                if (legacyValue == 'cable') {
                    customerSearchForm.find('.cable-customernumber').addClass('show');
                    customerSearchForm.find('.cable-customernumber').removeClass('hide');
                    customerSearchForm.find('.mobil-dsl-customernumber').addClass('hide');
                    customerSearchForm.find('.mobil-dsl-customernumber').removeClass('show');
                    disableDeviceAndOrderCableInput();
                    disableAddressCableInput();
                    customerSearchForm.find('input[name="addNo"]').removeClass('required-entry');
                    customerSearchForm.find('input[name="customer"]').removeAttr('disabled');
                }

                // Keep or remove form values based on legacy type
                copyFormDataToLegacy();
            },
            /**
             * Handle Addition when moving from simple search to legacy search
             * @param legacyType
             */
            handleAdditionFieldData = function (legacyType) {
                var additionInput = $('.customer-search-form').find('input[name="addNo"]');
                var additionInputDisabled = additionInput.attr('disabled');
                var currentValue = '';

                if(additionInputDisabled != 'disabled'){
                    if((legacyType == 'mobile')){
                        keepFieldsValuesForLegacy['disabled'] = true;
                    }
                    if((legacyType != 'mobile') && (keepFieldsValuesForLegacy['disabled']) ){
                        additionInput.val(keepFieldsValuesForLegacy['addNo']);
                        keepFieldsValuesForLegacy['disabled'] = false;
                    }
                    currentValue = additionInput.val();
                    keepFieldsValuesForLegacy['addNo'] = currentValue;
                    additionInput.val(keepFieldsValuesForLegacy['addNo']);
                } else {
                    if(!keepFieldsValuesForLegacy['disabled']){
                        currentValue = additionInput.val();
                        keepFieldsValuesForLegacy['addNo'] = currentValue;
                        additionInput.val(keepFieldsValuesForLegacy['addNo']);
                        keepFieldsValuesForLegacy['disabled'] = true;
                    }
                }
            },
            /**
             * Keep or remove specific form values when legacy search is enabled
             */
            copyFormDataToLegacy = function () {
                var legacyType = $('input[name=legacy_type]:checked', '#customer_search_form').val();
                legacyType = legacyType ? legacyType : 'mobile';
                var fieldsToKeep = keepFormFieldsForLegacy[legacyType];
                handleAdditionFieldData(legacyType);

                $('.customer-search-form').find('input[type=text], textarea').each(function() {
                    var fieldName = jQuery(this).attr('name');
                    var fieldValue = jQuery(this).val();

                    if(fieldValue){
                        savedFormFieldsLegacy[fieldName] = fieldValue;
                    }

                    if ($.inArray(fieldName, fieldsToKeep) === -1) {
                        jQuery(this).val('');
                        jQuery(this).removeClass('input-background');
                    } else {
                        jQuery(this).val(savedFormFieldsLegacy[fieldName]);
                    }
                });
            },
            /**
             * when exact search we should disable specific fields
             * - first we enable all the input fields and after we apply the logic for the exact search
             */
            showHideDisableExactSpecificFilters = function () {
                // if any address has been already filled save it
                var filledAddresses = {
                    '#customer_search_form [name="zipcode"]' : jQuery('#customer_search_form [name="zipcode"]').val(),
                    '#customer_search_form [name="city"]' : jQuery('#customer_search_form [name="city"]').val(),
                    '#customer_search_form [name="street"]' : jQuery('#customer_search_form [name="street"]').val(),
                    '#customer_search_form [name="no"]' : jQuery('#customer_search_form [name="no"]').val()
                };

            // enable all input fields
            enableAllInputFields();

            // reset filters
            resetFilters();
            resetValidations();

            var customerSearchForm = $('#customer_search_form');
            customerSearchForm.find('.on-exact-disabled').find('input').attr('disabled', true);
            customerSearchForm.find('.on-exact-disabled').find('input').addClass('disabled');

            addValidationClassesForExactFilters();

            // restore already filled addresses after resetFilters
            for(var key in filledAddresses) {
                if(filledAddresses.hasOwnProperty(key)){
                    jQuery(key).val(filledAddresses[key]);
                }
            }

        },
        /**
         * enable all input fields
         */
        enableAllInputFields = function () {
            var customerSearchInputTxt = $('#customer_search_form').find('input[type=text]');
            customerSearchInputTxt.removeAttr('disabled');
            customerSearchInputTxt.removeClass('disabled');
        },
        /**
         * removes the DSL validation classes and specific html added for validation
         */
        addDslValidation = function () {
            $('#customer_search_form').find('input[name="customer"]').addClass('validate-legacy-dsl-search');
            $('#customer_search_form').find('input[name="telephone_number"]').addClass('validate-legacy-dsl-search');
            $('#customer_search_form').find('input[name="first_name"]').addClass('validate-legacy-dsl-search');
            $('#customer_search_form').find('input[name="last_name"]').addClass('validate-legacy-dsl-search');
            $('#customer_search_form').find('input[name="birthday"]').addClass('validate-legacy-dsl-search');
            $('#customer_search_form').find('input[name="zipcode"]').addClass('validate-legacy-dsl-search');
            $('#customer_search_form').find('input[name="city"]').addClass('validate-legacy-dsl-search');
            $('#customer_search_form').find('input[name="street"]').addClass('validate-legacy-dsl-search');
            $('#customer_search_form').find('input[name="no"]').addClass('validate-legacy-dsl-search');
            $('#customer_search_form').find('input[name="addNo"]').addClass('validate-legacy-dsl-search');
        },
            /**
             * removes the DSL validation classes and specific html added for validation
             */
            removeDslValidation = function () {
                $('#customer_search_form').find('input[name="customer"]').removeClass('validate-legacy-dsl-search');
                $('#customer_search_form').find('input[name="telephone_number"]').removeClass('validate-legacy-dsl-search');
                $('#customer_search_form').find('input[name="first_name"]').removeClass('validate-legacy-dsl-search');
                $('#customer_search_form').find('input[name="last_name"]').removeClass('validate-legacy-dsl-search');
                $('#customer_search_form').find('input[name="birthday"]').removeClass('validate-legacy-dsl-search');
                $('#customer_search_form').find('input[name="zipcode"]').removeClass('validate-legacy-dsl-search');
                $('#customer_search_form').find('input[name="city"]').removeClass('validate-legacy-dsl-search');
                $('#customer_search_form').find('input[name="street"]').removeClass('validate-legacy-dsl-search');
                $('#customer_search_form').find('input[name="no"]').removeClass('validate-legacy-dsl-search');
                $('#customer_search_form').find('input[name="addNo"]').removeClass('validate-legacy-dsl-search');
            },


            addMobileValidation = function () {
                $('#customer_search_form').find('input[name="customer"]').addClass('validate-legacy-mobile-search');
                $('#customer_search_form').find('input[name="first_name"]').addClass('validate-legacy-mobile-search');
                $('#customer_search_form').find('input[name="last_name"]').addClass('validate-legacy-mobile-search');
                $('#customer_search_form').find('input[name="birthday"]').addClass('validate-legacy-mobile-search');
                $('#customer_search_form').find('input[name="zipcode"]').addClass('validate-legacy-mobile-search');
                $('#customer_search_form').find('input[name="city"]').addClass('validate-legacy-mobile-search');
                $('#customer_search_form').find('input[name="street"]').addClass('validate-legacy-mobile-search');
                $('#customer_search_form').find('input[name="no"]').addClass('validate-legacy-mobile-search');
                $('#customer_search_form').find('input[name="subscriber_number"]').addClass('validate-legacy-mobile-search');
                $('#customer_search_form').find('input[name="sim_imei"]').addClass('validate-legacy-mobile-search');
                $('#customer_search_form').find('input[name="addNo"]').attr('disabled','true');
            },
            removeMobileValidation = function () {
                $('#customer_search_form').find('input[name="customer"]').removeClass('validate-legacy-mobile-search');
                $('#customer_search_form').find('input[name="first_name"]').removeClass('validate-legacy-mobile-search');
                $('#customer_search_form').find('input[name="last_name"]').removeClass('validate-legacy-mobile-search');
                $('#customer_search_form').find('input[name="birthday"]').removeClass('validate-legacy-mobile-search');
                $('#customer_search_form').find('input[name="zipcode"]').removeClass('validate-legacy-mobile-search');
                $('#customer_search_form').find('input[name="city"]').removeClass('validate-legacy-mobile-search');
                $('#customer_search_form').find('input[name="street"]').removeClass('validate-legacy-mobile-search');
                $('#customer_search_form').find('input[name="no"]').removeClass('validate-legacy-mobile-search');
                $('#customer_search_form').find('input[name="subscriber_number"]').removeClass('validate-legacy-mobile-search');
                $('#customer_search_form').find('input[name="sim_imei"]').removeClass('validate-legacy-mobile-search');
                $('#customer_search_form').find('input[name="addNo"]').attr('disabled','false');
            },

        /**
         * add the exact validation classes and specific html added for validation
         */
        addExactValidation = function () {
            // as seen in the mockup
            $('.exact-required').each(function () {
                $(this).find('input').addClass("validate-exact-search");
                $(this).find('label').append("<span>*</span>");
            });
            // also, those 4 address fields for exact address search have to be required
            addCableValidation();
        },
        /**
         * add validation classes for cable legacy search
         */
        addCableValidation = function () {
            jQuery('#customer_search_form [name="zipcode"], ' +
                '#customer_search_form [name="city"],' +
                '#customer_search_form [name="street"], ' +
                '#customer_search_form [name="no"], ' +
                '#customer_search_form [name="addNo"]').attr('disabled','true');
            jQuery('#customer_search_form [name="customer"]').addClass('validate-legacy-cable-search');
            jQuery('#customer_search_form [name="device_id"]').addClass('validate-legacy-cable-search');
        },
            /**
             * remove validation classes for cable legacy search
             */
            removeCableValidation = function () {
                jQuery('#customer_search_form [name="zipcode"], ' +
                    '#customer_search_form [name="city"],' +
                    '#customer_search_form [name="street"], ' +
                    '#customer_search_form [name="no"], ' +
                    '#customer_search_form [name="addNo"]').removeAttr('disabled');
                jQuery('#customer_search_form [name="customer"]').removeClass('validate-legacy-cable-search');
                jQuery('#customer_search_form [name="device_id"]').removeClass('validate-legacy-cable-search');
            },


            addAdvancedSearchValidation = function () {
                $('#customer_search_form').find('input[name="customer"]').addClass('validate-advanced-search');
                $('#customer_search_form').find('input[name="telephone_number"]').addClass('validate-advanced-search');
                $('#customer_search_form').find('input[name="first_name"]').addClass('validate-advanced-search');
                $('#customer_search_form').find('input[name="last_name"]').addClass('validate-advanced-search');
                $('#customer_search_form').find('input[name="birthday"]').addClass('validate-advanced-search');
                $('#customer_search_form').find('input[name="zipcode"]').addClass('validate-advanced-search');
                $('#customer_search_form').find('input[name="city"]').addClass('validate-advanced-search');
                $('#customer_search_form').find('input[name="street"]').addClass('validate-advanced-search');
                $('#customer_search_form').find('input[name="no"]').addClass('validate-advanced-search');
                $('#customer_search_form').find('input[name="addNo"]').addClass('validate-advanced-search');
                $('#customer_search_form').find('input[name="email"]').addClass('validate-advanced-search');
                $('#customer_search_form').find('input[name="device_id"]').addClass('validate-advanced-search');
            },

            removeAdvancedSearchValidation = function () {
                $('#customer_search_form').find('input[name="customer"]').removeClass('validate-advanced-search');
                $('#customer_search_form').find('input[name="telephone_number"]').removeClass('validate-advanced-search');
                $('#customer_search_form').find('input[name="first_name"]').removeClass('validate-advanced-search');
                $('#customer_search_form').find('input[name="last_name"]').removeClass('validate-advanced-search');
                $('#customer_search_form').find('input[name="birthday"]').removeClass('validate-advanced-search');
                $('#customer_search_form').find('input[name="zipcode"]').removeClass('validate-advanced-search');
                $('#customer_search_form').find('input[name="city"]').removeClass('validate-advanced-search');
                $('#customer_search_form').find('input[name="street"]').removeClass('validate-advanced-search');
                $('#customer_search_form').find('input[name="no"]').removeClass('validate-advanced-search');
                $('#customer_search_form').find('input[name="addNo"]').removeClass('validate-advanced-search');
                $('#customer_search_form').find('input[name="email"]').removeClass('validate-advanced-search');
                $('#customer_search_form').find('input[name="device_id"]').removeClass('validate-advanced-search');
            },

            addNormalSearchValidation = function () {
                $('#customer_search_form').find('input[name="customer"]').addClass('validate-normal-search');
                $('#customer_search_form').find('input[name="telephone_number"]').addClass('validate-normal-search');
                $('#customer_search_form').find('input[name="first_name"]').addClass('validate-normal-search');
                $('#customer_search_form').find('input[name="last_name"]').addClass('validate-normal-search');
                $('#customer_search_form').find('input[name="birthday"]').addClass('validate-normal-search');
                $('#customer_search_form').find('input[name="zipcode"]').addClass('validate-normal-search');
                $('#customer_search_form').find('input[name="city"]').addClass('validate-normal-search');
                $('#customer_search_form').find('input[name="street"]').addClass('validate-normal-search');
                $('#customer_search_form').find('input[name="no"]').addClass('validate-normal-search');
                $('#customer_search_form').find('input[name="addNo"]').addClass('validate-normal-search');
            },

            removeNormalSearchValidation = function () {
                $('#customer_search_form').find('input[name="customer"]').removeClass('validate-normal-search');
                $('#customer_search_form').find('input[name="telephone_number"]').removeClass('validate-normal-search');
                $('#customer_search_form').find('input[name="first_name"]').removeClass('validate-normal-search');
                $('#customer_search_form').find('input[name="last_name"]').removeClass('validate-normal-search');
                $('#customer_search_form').find('input[name="birthday"]').removeClass('validate-normal-search');
                $('#customer_search_form').find('input[name="zipcode"]').removeClass('validate-normal-search');
                $('#customer_search_form').find('input[name="city"]').removeClass('validate-normal-search');
                $('#customer_search_form').find('input[name="street"]').removeClass('validate-normal-search');
                $('#customer_search_form').find('input[name="no"]').removeClass('validate-normal-search');
                $('#customer_search_form').find('input[name="addNo"]').removeClass('validate-normal-search');
            },

        checkIfActivateNormalSearch = function(){
            var advancedSearchSwitch = $('#customer_search').find('.advanced-search-switch').find('.toggle-button').hasClass('off-state');
            var legacySearchSwitch = $('#customer_search').find('.legacy-search-switch').find('.toggle-button').hasClass('off-state');
            if (advancedSearchSwitch && legacySearchSwitch){
                addNormalSearchValidation();
            }else{
                removeNormalSearchValidation();
            }
        }



        /**
         * add/remove the validation classes specific for legacy system marked
         */
        addValidationClassesForLegacyFilters = function () {
            var legacyValue = $('input[name=legacy_type]:checked', '#customer_search_form').val();

            // Clear all legacy validations
            removeValidationClassesForLegacyFilters();
                switch (legacyValue) {
                    case 'dsl':
                        addDslValidation();
                        break;
                    case 'mobile':
                        addMobileValidation();
                        break;
                    case 'cable':
                        addCableValidation();
                        break;
                    default:
                        break;
                }
            },
            /**
             * add/remove the validation classes specific for legacy system marked
             */
            addValidationClassesForExactFilters = function () {
                // Clear all exact validations
                removeValidationClassesForExactFilters();
                // add required validations
                addExactValidation();
            },
            /**
             * Removes all validation classes for legacy search
             */
            removeValidationClassesForLegacyFilters = function () {
                removeMobileValidation();
                removeDslValidation();
                removeCableValidation();
            },
            /**
             * Removes all validation classes for exact search
             */
            removeValidationClassesForExactFilters = function () {
                $('.exact-required').each(function () {
                    $(this).find('input').removeClass("validate-exact-search");
                    $(this).find('label span').remove();
                });
            },
            disableDeviceAndOrderInputsOnCableAddressSearch = function () {
                var customerSearchForm = $('#customer_search_form');
                customerSearchForm.find('.for-cable-address-disable').find('input').attr('disabled', true);
            },
            enableDeviceAndOrderInputsOnCableAddressSearch = function () {
                var customerSearchForm = $('#customer_search_form');
                customerSearchForm.find('.for-cable-address-disable').find('input').removeAttr('disabled');
            },
            disableAddressInputsOnCableDeviceOROrderSearch = function () {
                var customerSearchForm = $('#customer_search_form');
                customerSearchForm.find('.for-cable-device-or-order-disable').find('input').attr('disabled', true);
            },
            enableAddressInputsOnCableDeviceOROrderSearch = function () {
                var customerSearchForm = $('#customer_search_form');
                customerSearchForm.find('.for-cable-device-or-order-disable').find('input').removeAttr('disabled');
            },
            disableDeviceIdOrCustomerIdOnCableDevice = function (customerId,deviceId) {
                var customerSearchForm = $('#customer_search_form');
                if(customerId && !deviceId){
                    customerSearchForm.find('.for-cable-address-disable').find("input[name*='device_id']").attr('disabled', true);
                    customerSearchForm.find('.for-cable-address-disable').find("input[name*='customer']").removeAttr('disabled');
                    customerSearchForm.find('.for-cable-address-disable').find("input[name*='telephone_number']").attr('disabled', true);
                    customerSearchForm.find('.for-cable-address-disable').find("input[name*='zipcode']").attr('disabled', true);
                    customerSearchForm.find('.for-cable-address-disable').find("input[name*='city']").attr('disabled', true);
                    customerSearchForm.find('.for-cable-address-disable').find("input[name*='street']").attr('disabled', true);
                    customerSearchForm.find('.for-cable-address-disable').find("input[name*='not']").attr('disabled', true);
                    customerSearchForm.find('.for-cable-address-disable').find("input[name*='addNo']").attr('disabled', true);
                }
                else if (deviceId && !customerId){
                    customerSearchForm.find('.for-cable-address-disable').find("input[name*='device_id']").removeAttr('disabled');
                    customerSearchForm.find('.for-cable-address-disable').find("input[name*='customer']").attr('disabled', true);
                    customerSearchForm.find('.for-cable-address-disable').find("input[name*='telephone_number']").attr('disabled', true);
                    customerSearchForm.find('.for-cable-address-disable').find("input[name*='zipcode']").attr('disabled', true);
                    customerSearchForm.find('.for-cable-address-disable').find("input[name*='city']").attr('disabled', true);
                    customerSearchForm.find('.for-cable-address-disable').find("input[name*='street']").attr('disabled', true);
                    customerSearchForm.find('.for-cable-address-disable').find("input[name*='no']").attr('disabled', true);
                    customerSearchForm.find('.for-cable-address-disable').find("input[name*='addNo']").attr('disabled', true);
                }
                else {
                    customerSearchForm.find('.for-cable-address-disable').find("input[name*='device_id']").attr('disabled', true);
                    customerSearchForm.find('.for-cable-address-disable').find("input[name*='customer']").attr('disabled', true);
                    customerSearchForm.find('.for-cable-address-disable').find("input[name*='telephone_number']").removeAttr('disabled');
                    customerSearchForm.find('.for-cable-address-disable').find("input[name*='zipcode']").removeAttr('disabled');
                    customerSearchForm.find('.for-cable-address-disable').find("input[name*='city']").removeAttr('disabled');
                    customerSearchForm.find('.for-cable-address-disable').find("input[name*='street']").removeAttr('disabled');
                    customerSearchForm.find('.for-cable-address-disable').find("input[name*='no']").removeAttr('disabled');
                    customerSearchForm.find('.for-cable-address-disable').find("input[name*='addNo']").removeAttr('disabled');
                }
            },
            enableDeviceIdOrCustomerIdOnCableDevice = function (customerId,deviceId,telephoneNumber) {
                var customerSearchForm = $('#customer_search_form');
                var deviceIdDisabled = customerSearchForm.find('.for-cable-address-disable').find("input[name*='device_id']").attr('disabled');
                var customerDisabled = customerSearchForm.find('.for-cable-address-disable').find("input[name*='customer']").attr('disabled');
                if(!customerId && (deviceIdDisabled == 'disabled')){
                    customerSearchForm.find('.for-cable-address-disable').find("input[name*='device_id']").removeAttr('disabled');
                    customerSearchForm.find('.for-cable-address-disable').find("input[name*='telephone_number']").removeAttr('disabled');
                    customerSearchForm.find('.for-cable-address-disable').find("input[name*='zipcode']").removeAttr('disabled');
                    customerSearchForm.find('.for-cable-address-disable').find("input[name*='city']").removeAttr('disabled');
                    customerSearchForm.find('.for-cable-address-disable').find("input[name*='street']").removeAttr('disabled');
                    customerSearchForm.find('.for-cable-address-disable').find("input[name*='no']").removeAttr('disabled');
                    customerSearchForm.find('.for-cable-address-disable').find("input[name*='addNo']").removeAttr('disabled');
                }
                else if(!deviceId && (customerDisabled == 'disabled')) {
                    customerSearchForm.find('.for-cable-address-disable').find("input[name*='customer']").removeAttr('disabled');
                    customerSearchForm.find('.for-cable-address-disable').find("input[name*='zipcode']").removeAttr('disabled');
                    customerSearchForm.find('.for-cable-address-disable').find("input[name*='city']").removeAttr('disabled');
                    customerSearchForm.find('.for-cable-address-disable').find("input[name*='street']").removeAttr('disabled');
                    customerSearchForm.find('.for-cable-address-disable').find("input[name*='no']").removeAttr('disabled');
                    customerSearchForm.find('.for-cable-address-disable').find("input[name*='addNo']").removeAttr('disabled');
                }
                else {
                    customerSearchForm.find('.for-cable-address-disable').find("inp ut[name*='device_id']").removeAttr('disabled');
                    customerSearchForm.find('.for-cable-address-disable').find("input[name*='customer']").removeAttr('disabled');
                    customerSearchForm.find('.for-cable-address-disable').find("input[name*='zipcode']").removeAttr('disabled');
                    customerSearchForm.find('.for-cable-address-disable').find("input[name*='city']").removeAttr('disabled');
                    customerSearchForm.find('.for-cable-address-disable').find("input[name*='street']").removeAttr('disabled');
                    customerSearchForm.find('.for-cable-address-disable').find("input[name*='no']").removeAttr('disabled');
                    customerSearchForm.find('.for-cable-address-disable').find("input[name*='addNo']").removeAttr('disabled');
                }
            },
            /**
             * if the input for device
             */
            disableDeviceAndOrderCableInput = function () {
                var legacyValue = $('input[name=legacy_type]:checked', '#customer_search_form').val();
                if (legacyValue == 'cable') {
                    if ($('#zipcode').val() || $('#city').val() || $('#street').val() || $('#no').val() || $('#addNo').val()) {
                        disableDeviceAndOrderInputsOnCableAddressSearch();
                    }
                    else {
                        enableDeviceAndOrderInputsOnCableAddressSearch();
                    }
                }
            },
            disableAddressCableInput = function () {
                // if advanced search is active skip legacy search logic
                var advancedSearchStatus = $('#advanced-search').val();
                if(advancedSearchStatus == 1){
                    removeValidationClassesForLegacyFilters();
                    return;
                }
                // otherwise process legacy search
                var legacyValue = $('input[name=legacy_type]:checked', '#customer_search_form').val();
                var legacySearchSwitch = $('#legacy-search').val();
                if (legacyValue == 'cable') {
                    var customerId = $('#customer').val();
                    var deviceId = $('#device_id').val();
                    if (customerId || deviceId) {
                        disableDeviceIdOrCustomerIdOnCableDevice(customerId,deviceId);
                        disableAddressInputsOnCableDeviceOROrderSearch();
                        resetValidations();
                    }
                    else {
                        enableDeviceIdOrCustomerIdOnCableDevice(customerId);
                        enableAddressInputsOnCableDeviceOROrderSearch();
                    }
                    if(legacySearchSwitch == 1) {
                        addCableValidation();
                    }
                }
            };
        return {
            initCustomerFilters: initCustomerFilters,
            onSwitchLegacySearchChange: onSwitchLegacySearchChange,
            onSwitchExactSearchChange : onSwitchExactSearchChange,
            onSwitchAdvancedSearchChange: onSwitchAdvancedSearchChange,
            showHideDisableLegacySpecificFilters: showHideDisableLegacySpecificFilters,
            disableDeviceAndOrderCableInput: disableDeviceAndOrderCableInput,
            disableAddressCableInput: disableAddressCableInput,
            resetFilters: resetFilters
        }
    }();
    window.ToggleBullet = function () {
        var switchPending = false,
            switchOn = function (toggleClass) {
                $(toggleClass).attr('data-state', true);
                $(toggleClass).closest('.toggle-button').addClass('on-state').removeClass('off-state');
                if (!$(toggleClass).closest('.toggle-button').hasClass('switch')) {
                    $(toggleClass).find('.toggle-button').addClass('on-state').removeClass('off-state');
                }
            },
            switchOff = function (toggleClass) {
                $(toggleClass).attr('data-state', false);
                $(toggleClass).closest('.toggle-button').addClass('off-state').removeClass('on-state');
                if (!$(toggleClass).closest('.toggle-button').hasClass('switch')) {
                    $(toggleClass).find('.toggle-button').addClass('off-state').removeClass('on-state');
                }
            },
            onSwitchChange = function (toggleClass, toggleType) {
                var currentState = state(toggleClass);
                currentState == 1 ? switchOff(toggleClass) : switchOn(toggleClass);
                currentState = state(toggleClass);
                switch (toggleType) {
                    case 'soho':
                        $.post(
                            MAIN_URL + 'customer/details/setCustomerTypeSoho',
                            {state: currentState}
                        ).done(function(){
                            if (typeof window.campaign !== 'undefined')
                            {
                                window.campaign.getData(0);
                            }
                        });
                        ToggleBulletSoho.handleState(currentState, true);
                        break;
                    default:
                        break;
                }
            },
            state = function (toggleClass) {
                if ($(toggleClass).attr('data-state') === "true" || $(toggleClass).attr('data-state') == 1) {
                    return 1;
                }
                else {
                    return 0;
                }
            },
            disable = function (toggleClass) {
                $(toggleClass).closest('.toggle-button').addClass('disabled').attr('onclick', '');
                if (!$(toggleClass).closest('.toggle-button').hasClass('switch')) {
                    $(toggleClass).find('.toggle-button').addClass('disabled').attr('onclick', '');
                }
            },
            enable = function (toggleClass) {
                $(toggleClass).closest('.toggle-button').removeClass('disabled').attr('onclick', 'customerDe.setBusinessState()');
                if (!$(toggleClass).closest('.toggle-button').hasClass('switch')) {
                    $(toggleClass).find('.toggle-button').removeClass('disabled').attr('onclick', 'customerDe.setBusinessState()');
                }
            };
        return {
            switchPending: switchPending,
            onSwitchChange: onSwitchChange,
            state: state,
            switchOn: switchOn,
            switchOff: switchOff,
            disable: disable,
            enable: enable
        }
    }();
    window.ToggleBulletSoho = function () {
        var findStateAndHandleIt = function (toggleClass) {
                var currentState = ($(toggleClass).attr('data-state') === 'true' || $(toggleClass).attr('data-state') == 1) ? 1 : 0;
                handleState(currentState, false);
            },
            getSohoState = function (toggleClass) {
                return ($(toggleClass).attr('data-state') === 'true' || $(toggleClass).attr('data-state') == 1) ? 1 : 0;
            },
            handleState = function (currentState, updateOnPrice) {
                if (currentState == 1 || currentState === 'true') {
                    sohoStateHandle(updateOnPrice);
                }
                else {
                    privateStateHandle(updateOnPrice);
                }
            },
            sohoStateHandle = function (updateOnPrice) {
                /** OMNVFDE-301: For SOHO, nationality and DOB not displayed, **/
                $('[data-id="customer-details-dob"]').addClass('hide');
                // when the customer is Soho the tax must be set to excluded
                var state = ToggleBullet.state('.tax-toggle');
            },
            privateStateHandle = function (updateOnPrice) {
                /** OMNVFDE-301: For SOHO, nationality and DOB not displayed, **/
                $('[data-id="customer-details-dob"]').removeClass('hide');
                var state = ToggleBullet.state('.tax-toggle');
            };
        return {
            handleState: handleState,
            findStateAndHandleIt: findStateAndHandleIt,
            getSohoState: getSohoState
        }
    }();
})(jQuery);

jQuery(document).ready(function ($) {
    $('[data-toggle="tooltip"]').tooltip();
    CustomerFiltersSearch.initCustomerFilters();

    Validation.add('validate-callback-time-range', Translator.translate('Time must be greater than current time and in the following range: 07:30 - 21:30'), function(v, element) {
        if ((v != '') && (v != null) && (v.length != 0)) {
            var minTime = '07:30';
            var maxTime = '21:30';
            var min = parseInt(minTime.replace(':',''));
            var max = parseInt(maxTime.replace(':',''));
            var value = parseInt(v.replace(':',''));

            var isToday = false;
            var callbackDate = jQuery('[id="offer_data[callback][date]"]').val();
            if (callbackDate != '') {
                var date = new Date();
                var input = callbackDate.split('.');
                var test = new Date(input[2], parseInt(input[1], 10) - 1, input[0]);
                var dateNow = new Date(date.getFullYear(), date.getMonth(), date.getDate());
                var isToday = (test.valueOf() == dateNow.valueOf()) ? true : false;
                var currentMinutes = (date.getMinutes() < 10) ? '0'+date.getMinutes().toString() : date.getMinutes().toString();
                var currentTime = parseInt(date.getHours().toString() + currentMinutes);
            }

            if (isToday && (min < currentTime)) {
                min = currentTime + 1;
            }

            return min <= value && value <= max;
        } else {
            return true;
        }
    });

    Validation.add('validate-callback-time-format', Translator.translate('Please enter a time value in the following format: hh:mm'), function(v, element) {
        if ((v != '') && (v != null) && (v.length != 0)) {
            var reg = /^([0-9]|0[1-9]|1[0-9]|2[0-3])[:]([0-5][0-9])$/;
            return reg.test(v);
        } else {
            var $_element = $j(element);
            var related_element = $_element.data('related-required');

            if (related_element) {
                var rel_v = $j(related_element).val();

                if ((rel_v != '') && (rel_v != null) && (rel_v.length != 0)) {
                    return false;
                }
            }
            return true;
        }
    });

    Validation.add('validate-callback-date', Translator.translate('Date should be greater or equal to today'), function(v, element) {
        if ((v != '') && (v != null) && (v.length != 0)) {
            var date = new Date();
            var input = v.split('.');
            var test = new Date(input[2], parseInt(input[1], 10) - 1, input[0]);
            var dateNow = new Date(date.getFullYear(), date.getMonth(), date.getDate());

            return test.valueOf() >= dateNow.valueOf();
        } else {
            return true;
        }
    });
});

// resize timeout
var rtime = new Date();
var delta = 200;
window.timeout = false;

window.__resizeCallbacks = [];
window.getResizeEvents = function () {
    return window.__resizeCallbacks;
};
window.addResizeEvent = function (callback) {
    window.__resizeCallbacks.push(callback);
    window.onresize = function () {
        var len = window.__resizeCallbacks.length;
        for (var i = 0; i < len; ++i) {
            window.__resizeCallbacks[i]();
        }
    }
};
window.addResizeEvent(function () {
    rtime = new Date();
    if (window.timeout === false) {
        window.timeout = true;
        setTimeout(LeftSidebar.resizeLeftPanel, delta);
    }
});

var hideNotificationTimeout;
function showNotificationBar(message, type, button){
    var allowedTypes = ['success', 'warning', 'error'];
    if(allowedTypes.indexOf(type) == -1){
        console.log('The given notification bar type is not allowed');
        return;
    }

    var notificationBar = jQuery('#notification-bar');
    allowedTypes.forEach(function (allowedType){
        var classToCheck = 'notification-bar-' + allowedType;
        if (notificationBar.hasClass(classToCheck)) {
            notificationBar.removeClass(classToCheck);
        }
    });
    notificationBar.addClass('notification-bar-' + type);
    notificationBar.find('span.icon').addClass('notification-circle-icon');
    notificationBar.find('span.notification-bar-message').html(Translator.translate(message));
    notificationBar.removeClass('hidden').addClass('notification-bar-show');
    if (button) {
        notificationBar.find('span.notification-button').addClass('notification-bar-button');
        notificationBar.find('span.notification-bar-button').html(Translator.translate(button));
    } else {
        notificationBar.find('span.notification-button .btn').remove();
        notificationBar.find('span.notification-button').removeClass('notification-bar-button');
    }
    if (hideNotificationTimeout) {
        clearTimeout(hideNotificationTimeout);
        hideNotificationTimeout = null;
    }
    var hideNotificationTimeout = setTimeout(function() {
        hideNotificationBar();
    }, 12000);
}

function showValidateNotificationBar(message, type, button) {
    jQuery('.address-check span.loading-search').addClass('invisible');
    var allowedTypes = ['success', 'warning', 'error'];
    if(allowedTypes.indexOf(type) == -1){
        console.log('The given notification bar type is not allowed');
        return;
    }

    allowedTypes.forEach(function (allowedType){
            var classToCheck = 'notification-bar-' + allowedType;
        if(jQuery('#notification-bar').hasClass(classToCheck)){
            jQuery('#notification-bar').removeClass(classToCheck)
        }
    });

    var notificationBar = jQuery('#notification-bar');
    allowedTypes.forEach(function (allowedType){
        var classToCheck = 'notification-bar-' + allowedType;
        if (notificationBar.hasClass(classToCheck)) {
            notificationBar.removeClass(classToCheck);
        }
    });
    notificationBar.addClass('notification-bar-' + type);
    notificationBar.find('span.icon').addClass('notification-circle-icon');
    notificationBar.find('span.notification-bar-message').html(Translator.translate(message));
    if (button) {
        notificationBar.find('span.notification-button').addClass('notification-bar-button');
        notificationBar.find('span.notification-bar-button').html(Translator.translate(button));
    } else {
        notificationBar.find('span.notification-button .btn').remove();
        notificationBar.find('span.notification-button').removeClass('notification-bar-button');
    }
    notificationBar.addClass('validate' + type);
    notificationBar.removeClass('hidden').addClass('notification-bar-show');
    if (hideNotificationTimeout) {
        clearTimeout(hideNotificationTimeout);
        hideNotificationTimeout = null;
    }
    var hideNotificationTimeout = setTimeout(function() {
        hideNotificationBar();
    }, 12000);
}

function hideNotificationBar(){
    var notificationBar = jQuery('#notification-bar');
    if (!notificationBar.hasClass('notification-bar-show')) {
        return;
    }
    notificationBar.removeClass('notification-bar-show');
    var allowedTypes = ['success', 'warning', 'error'];
    allowedTypes.forEach(function (allowedType){
        var classToCheck = 'notification-bar-' + allowedType;
        if (notificationBar.hasClass(classToCheck)) {
            notificationBar.removeClass(classToCheck);
        }
    });
    jQuery('#notification-bar span.notification-bar-message').html('');
    notificationBar.addClass('hidden');
}

function removeBundleChoiceWarnings() {
    hideNotificationBar();
    jQuery('.bundle-choice-wrapper').find('div.validation-advice').remove();
}

function setEventTriggering(container) {
    jQuery(container + ' input[data-trigger]').each(function () {
        jQuery(this).trigger((jQuery(this).data('trigger')))
    });
}

Object.extend(Validation, {
    insertAdvice: function (elm, advice) {
        jQuery(elm).prop('disabled', false);
        var container = $(elm).up('.field-row');
        if (container ) {
            Element.insert(container, {before: advice});
        } else if (elm.up('td.value')) {
            elm.up('td.value').insert({bottom: advice});
        } else if (elm.advaiceContainer && $(elm.advaiceContainer)) {
            $(elm.advaiceContainer).update(advice);
        }
        else {
            switch (elm.type.toLowerCase()) {
                case 'checkbox':
                case 'radio':
                    var p = elm.parentNode;
                    if (p) {
                        Element.insert(p, {'bottom': advice});
                    } else {
                        Element.insert(elm, {'after': advice});
                    }
                    break;
                default:
                    if ((jQuery(elm).hasClass('validate-legacy-mobile-search'))
                        || ((jQuery(elm).hasClass('validate-legacy-dsl-search')))
                        || ((jQuery(elm).hasClass('validate-legacy-cable-search')))
                        || ((jQuery(elm).hasClass('validate-normal-search')))
                        || ((jQuery(elm).hasClass('validate-advanced-search')))) {
                        jQuery('.validation-advice').remove();
                        Element.insert(elm.parentNode.parentNode, {'bottom': advice});
                    } else {
                        Element.insert(elm, {'after': advice});
                    }
            }
        }
    }
});

Object.extend(Validation, {
    reset : function(elm) {
        elm = $(elm);
        var cn = $w(elm.className);
        cn.each(function(value) {
            var prop = '__advice'+value.camelize();
            if(elm[prop]) {
                var advice = Validation.getAdvice(value, elm);
                if (advice) {
                    if(jQuery(advice).is(':visible')) {
                        advice.hide();
                    }
                }
                elm[prop] = '';
            }
            elm.removeClassName('validation-failed');
            elm.removeClassName('validation-passed');
            if (Validation.defaultOptions.addClassNameToContainer && Validation.defaultOptions.containerClassName != '') {
                var container = elm.up(Validation.defaultOptions.containerClassName);
                if (container) {
                    container.removeClassName('validation-passed');
                    container.removeClassName('validation-error');
                }
            }
        });
    }
});

Object.extend(Validation, {
  hideAdvice: function (elm, advice) {
    if (typeof elm === 'undefined') {
      return;
    }
    isCheckout = !!jQuery(elm).closest('#checkout-wrapper');
    isServiceability = !!jQuery(elm).closest('#topaddressCheckForm');

    if (isCheckout && advice != null) {
      new Effect.Fade(advice, {
        duration: 1, afterFinishInternal: function () {
          if(jQuery(advice).is(':visible')) {
            advice.hide();
            // Remove validation from element (proper label look)
            if( jQuery ) {
              jQuery(elm).trigger('errorlabels:remove');
            }
          }
        }
      });
    } else if ((isCheckout || isServiceability) && advice === null) {
      // Since id's have brackets which are seen as an other type
      // of selector than ID, we wil need to escape the brackets.
      const inputId = '#' + Validation.getElmID(elm).replace(/\[/g, '\\[').replace(/]/g, '\\]').replace(/\./g, '\\.');
      const inputEl = jQuery(inputId);
      const inputWrapperEl = inputEl.closest('.form-group');
      inputWrapperEl.removeClass('error');
      inputEl.removeAttr('data-toggle');
      inputEl.removeAttr('title');
      inputEl.removeAttr('data-original-title');
      inputEl.removeAttr('aria-describedby');

      // Destroy tooltip
      const attr = inputEl.attr('data-original-title');
      // For some browsers, `attr` is undefined; for others, `attr` is false. Check for both.
      if (typeof attr !== typeof undefined && attr !== false) {
        inputEl.next('.tooltip').remove();
      }
    }
  }
});

Object.extend(Validation, {
    validate : function(elm, options){
        options = Object.extend({
            useTitle : false,
            renderError : true,
            onElementValidate : function(result, elm) {}
        }, options || {});
        elm = $(elm);

        var cn = $w(elm.className);
        return result = cn.all(function(value) {
            var test = Validation.test(value,elm,options.useTitle,options.renderError);
            options.onElementValidate(test, elm);
            return test;
        });
    },
    test: function (name, elm, useTitle, renderError) {
        var v = Validation.get(name);
        var prop = '__advice' + name.camelize();
        try {
            if (Validation.isVisible(elm) && !v.test($F(elm), elm)) {
                if (renderError) {
                    var advice = Validation.getAdvice(name, elm);
                    if (advice == null) {
                        advice = this.createAdvice(name, elm, useTitle);
                    }
                    this.showAdvice(elm, advice, name);
                    this.updateCallback(elm, 'failed');

                    elm[prop] = 1;
                    if (!elm.advaiceContainer) {
                        elm.removeClassName('validation-passed');
                        if ((jQuery(elm).hasClass('validate-legacy-mobile-search'))
                            || ((jQuery(elm).hasClass('validate-legacy-dsl-search')))
                            || ((jQuery(elm).hasClass('validate-legacy-cable-search')))
                            || ((jQuery(elm).hasClass('validate-normal-search')))
                            || ((jQuery(elm).hasClass('validate-advanced-search')))) {
                            elm.removeClassName('validation-failed');
                        }else{
                            elm.addClassName('validation-failed');
                        }
                    }

                    if (Validation.defaultOptions.addClassNameToContainer && Validation.defaultOptions.containerClassName != '') {
                        var container = elm.up(Validation.defaultOptions.containerClassName);
                        if (container && this.allowContainerClassName(elm)) {
                            container.removeClassName('validation-passed');
                            container.addClassName('validation-error');
                        }
                    }
                }
                return false;
            } else {
                if (renderError) {
                    var advice = Validation.getAdvice(name, elm);
                    this.hideAdvice(elm, advice);
                    this.updateCallback(elm, 'passed');
                    elm[prop] = '';
                    elm.removeClassName('validation-failed');
                    elm.addClassName('validation-passed');
                    if (Validation.defaultOptions.addClassNameToContainer && Validation.defaultOptions.containerClassName != '') {
                        var container = elm.up(Validation.defaultOptions.containerClassName);
                        if (container && !container.down('.validation-failed') && this.allowContainerClassName(elm)) {
                            if (!Validation.get('IsEmpty').test(elm.value) || !this.isVisible(elm)) {
                                container.addClassName('validation-passed');
                            } else {
                                container.removeClassName('validation-passed');
                            }
                            container.removeClassName('validation-error');
                        }
                    }
                }
                return true;
            }
        } catch (e) {
            throw(e)
        }
    }
});

// Override createAdvice to enable manipulation of where the advice is shown
Object.extend(Validation, {
    createAdvice: function (name, elm, useTitle, customError) {
        var advice;
        var v = Validation.get(name);
        var errorMsg = useTitle ? ((elm && elm.title) ? elm.title : v.error) : v.error;
        if (customError) {
            errorMsg = customError;
        }
        try {
            if (Translator) {
                errorMsg = Translator.translate(errorMsg);
            }
        }
        catch(e) {}

        // Special class to show the advice for the parent element, and make sure it is not repeated
        if ($(elm).hasClassName('parent-shown-advice')) {
            elm = $(elm).up().select('input').first();
            if (advice = Validation.getAdvice(name, elm)) {
                return advice;
            }
        }

        // Since id's have brackets which are seen as an other type
        // of selector than ID, we wil need to escape the brackets.
        const inputId = '#' + Validation.getElmID(elm).replace(/\[/g, '\\[').replace(/]/g, '\\]').replace(/\./g, '\\.');
        const inputEl = jQuery(inputId);

        isModal = !!(jQuery(inputId).closest('.modal-wrapper').length);
        isServiceability = !!jQuery(inputId).closest('#topaddressCheckForm') || !!jQuery(inputId).closest('#addressCheckForm') || !!jQuery(inputId).closest('#moveServiceAddressCheckForm');

        // Checkout is using a newer styling than the rest of OSE.
        if (isModal || isServiceability) {
            var inputWrapperEl = inputEl.closest('.form-group');
            inputWrapperEl.addClass('error');
            inputEl.attr('data-toggle', 'tooltip');
            inputEl.attr('title','');
            inputEl.attr("data-original-title", errorMsg);
            inputEl.tooltip();

            return false;
        } else {
            inputEl.attr("data-toggle","tooltip");
            inputEl.attr("title",'');
            inputEl.attr("data-original-title", errorMsg);
            advice = '<div class="tooltip fade top in" id="advice-' + name + '-' + Validation.getElmID(elm) + '"><div class="tooltip-arrow" style="left: 50%;"></div><div class="tooltip-inner">' + errorMsg + '</div></div>';
            Validation.insertAdvice(elm, advice);
            advice = Validation.getAdvice(name, elm);
            if ($(elm).hasClassName('absolute-advice')) {
                var dimensions = $(elm).getDimensions();
                var originalPosition = Position.cumulativeOffset(elm);

                advice._adviceTop = (originalPosition[1] + dimensions.height) + 'px';
                advice._adviceLeft = (originalPosition[0]) + 'px';
                advice._adviceWidth = (dimensions.width) + 'px';
                advice._adviceAbsolutize = true;
            }

            return advice;
        }
    },
    showAdvice : function(elm, advice, adviceName){
        isServiceability = !!jQuery(elm).closest('#topaddressCheckForm') || !!jQuery(elm).closest('#addressCheckForm');
        isModal = !!(jQuery(elm).closest('.modal-wrapper').length);
        isCheckout = !!(jQuery(elm).closest('#checkout-wrapper').length);
        isMove = false;
        if (jQuery('#move-service').val()) {
            isMove = !!jQuery(elm).closest('#moveServiceAddressCheckForm');
        }
        // Checkout is using a newer styling than the rest of OSE.
        if (isServiceability) {
          if (isMove || isModal || isCheckout) {
            jQuery(elm).tooltip({trigger: 'hover', placement: 'top', animation: false});
          } else {
            jQuery(elm).tooltip({trigger: 'hover', placement: 'bottom', animation: false});
          }
        } else {
            if(!elm.advices){
                elm.advices = new Hash();
            }
            else{
                elm.advices.each(function(pair){
                    if (!advice || pair.value.id != advice.id) {
                        // hide non-current advice after delay
                        this.hideAdvice(elm, pair.value);
                    }
                }.bind(this));
            }
            elm.advices.set(adviceName, advice);
            if(typeof Effect == 'undefined') {
                advice.style.display = 'block';
            } else {
                if(!advice._adviceAbsolutize) {
                    new Effect.Appear(advice, {duration : 1 });
                } else {
                    Position.absolutize(advice);
                    advice.show();
                    advice.setStyle({
                        'top':advice._adviceTop,
                        'left': advice._adviceLeft,
                        'width': advice._adviceWidth,
                        'z-index': 1000
                    });
                    advice.addClassName('advice-absolute');
                }
            }
        }
    }
});

/* Closing Drawer when clicked modal backdrop and Cross */
jQuery(document).ready(function() {
    jQuery('body').on('click', '.custom-backdrop', function () {
        LeftSidebar.contractLeftSide();
        if (typeof RightSidebar !== 'undefined') {
            RightSidebar.toggleRightSideBar(false);
        }
    });

    jQuery('.search-result-cross').click(function() {
      LeftSidebar.contractLeftSide();
      if (typeof RightSidebar !== 'undefined') {
        RightSidebar.toggleRightSideBar(false);
      }
    });
});


/* Modals init JS */
var searchCustomerByPinForm = null;
var saveCartModalForm = null;
jQuery(document).ready(function ($) {
  if (jQuery('#searchCustomerByPin').length > 0) {
    searchCustomerByPinForm = new VarienForm('searchCustomerByPin', true);
  }
  if (jQuery('#save-cart-form').length > 0) {
    saveCartModalForm = new VarienForm('save-cart-form', true);
  }
});

function applyEmailTooltips(container) {
    container.find('.email-display').each(function() {
        if (jQuery(this).attr('data-toggle') == 'tooltip') {
            jQuery(this).tooltip('destroy');
        }
        if (jQuery(this)[0].scrollWidth > jQuery(this).innerWidth()) {
            jQuery(this)
                .attr('data-toggle', 'tooltip')
                .attr('data-placement', 'top')
                .attr('title', jQuery(this).text())
                .attr('data-original-title', jQuery(this).text())
                .tooltip()
            ;
        }
    });
}
