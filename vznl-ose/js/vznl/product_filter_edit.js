/*
 * Copyright (c) 2018. Dynacommerce B.V.
 */

jQuery(document).ready(function($) {
  $('.section-select').click(function() {
    var selectArea = $('.' + $(this).prop('id') + '_select');
    if (selectArea.length) {
      $('.select-area').hide();
      selectArea.show();
      $('.section-select').show();
      $('.autocomplete-field').hide();
      $(this).hide();
      $(this).siblings('.autocomplete-field').show();
    } else {
      $('.select-area').hide();
      $('.section-select').show();
      $('.autocomplete-field').hide();
    }
  });

  $('.content-buttons.form-buttons .save').click(function() {
    $('.select-area select:visible').find('option').prop('selected', true);
  });

  $('[name="visibility_domain"]:checked').trigger('click');
});

function removeItem(button) {
  button = jQuery(button);
  var selections = jQuery(button.siblings('select')[0]).val();
  if (selections && selections.length) {
    jQuery.each(selections, function(i, val) {
      if (button.siblings('select').find('option[value="'+val+'"]').length) {
        button.siblings('select').find('option[value="'+val+'"]').remove();
      }
    });
  }
  return false;
}

function addSelection(selection, element) {
  var label = selection.item.value;
  var value = selection.item.data;
  if ( ! element.find('option[value="'+value+'"]').length) {
    element.append('<option value="'+value+'">'+label+'</option>');
  }
}