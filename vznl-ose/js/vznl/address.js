/*
 * Copyright (c) 2018. Dynacommerce B.V.
 */

(function ($) {
  window.Address.prototype = $.extend(VEngine.prototype, {
    showCheckModal: function (callBack, callBackParam, callBackObject) {
      var self = this;
      var addressCheckModal = this.addressCheckModal;
      /** Register callBack call */
      this.callBack = callBack;
      this.callBackParam = callBackParam;
      this.callBackObject = callBackObject;

      if (callBackParam['moveOffnetScenario']) {
        addressCheckModal = this.moveServiceAddressCheckModal;
      }
      /** Clear callBack params on window close */
      $(addressCheckModal).on('hidden.bs.modal', function () {
        self.callBack = null;
        self.callBackParam = null;
        self.callBackObject = null;
        $('#cart-spinner').removeClass('hide').addClass('hide');
      });
      $(addressCheckModal).on('show.bs.modal', function () {
        if (HAS_GOOGLE_AUTOCOMPLETE && !window.serviceabilityGoogleModal) {
          window.serviceabilityGoogleModal = $(addressCheckModal).googleSearchAddress({
            streetInputName: "street",
            cityInputName: "city",
            houseNumberInputName: "housenumber",
            houseNumberAddition: "housenumberaddition",
            zipCodeInputName: "postalcode"
          });
        }
      });

      if (HAS_GOOGLE_AUTOCOMPLETE) {
        $(addressCheckModal).modal({backdrop: 'static'});
      } else {
        $(addressCheckModal).modal("show");
      }
    },

    enableOrDisableAddressCheckBtn: function() {
      var requiredFieldsAreFilled = true;
      $('[id="topaddressCheckForm"] .required-entry').each(function() {
        var value = $(this).val();
        if (value === '' || value === null || $(this).hasClass('validation-failed')) {
          requiredFieldsAreFilled = false;
        }
      });

      return requiredFieldsAreFilled;
    },

    prefillServiceabilityHeader: function (formId){
      if (this.enableOrDisableAddressCheckBtn()) {
        var $form = jQuery(formId);
        var postcode = $form.find('[name="postalcode"]').val().replace(/\s/g, "");
        var houseno = $form.find('[name="housenumber"]').val().replace(/\s/g, "");
        if (postcode && houseno) {
          setPageLoadingState(true);
          jQuery.post(
            '/checkout/index/searchAddress',
            {postcode: postcode, houseno: houseno},
            function(result) {
              setPageLoadingState(false);
              if (result.fields) {
                $form.find('[name="street"]').val(result.fields.street);
                $form.find('[name="city"]').val(result.fields.city);
              } else {
                $form.find('[name="street"]').val('');
                $form.find('[name="city"]').val('');
              }
            }
          );
        }
      }
    },

    checkServiceabilityHeader: function (formId) {
      window.campaign.triggered=false;//to force reloading campaigns
      setPageLoadingState(true);
      var self = this;
      var formData = $(formId).serializeObject();
      if (formData.postalcode && formData.housenumber) {
        setPageLoadingState(true);
      }
      var addressStreetNode = jQuery('[id="topBar.addressCheck.street"]');
      var serviceabilityModal = $('#serviceabilityMultipleResultsModal');
      var serviceabilityAddresses = serviceabilityModal.find('.ajax-results-addresses:first');
      var addressCheckForm = new VarienForm('topaddressCheckForm');
      if (addressCheckForm.validator.validate() == false) {
        setPageLoadingState(false);
        return;
      }
      var callback = function (results) {
        // store validate address api response into localStorage for further uses
        store.set('validateApiResponse', JSON.stringify(results));
        if (results.errors) {
          if (results.message == Translator.translate('Invalid address')) {
            self.addTopAddressFailedValidation();
            showNotificationBar(results.message, 'error');
          } else {
            showPlealErrorMsg (results.errors[0].code, results.errors[0].title, results.errors[0].detail, "Validate Address");
            // remove loader and clear form
            $('.address-check span.loading-search').addClass('invisible');
            $(formId).find('input').each(function () {
              $(this).val('');
            });
          }
        } else {
          if (results.data) {
            if (results.data.length == 1) {
              var singleAddress = $('<div></div>').attr('data-id', results.data[0].id)
                .attr('data-street', results.data[0].attributes.street)
                .attr('data-building-nr', results.data[0].attributes.houseNumber)
                .attr('data-postalcode', results.data[0].attributes.postalCode)
                .attr('data-house-addition', results.data[0].attributes.houseNumberAddition)
                .attr('data-city-name', results.data[0].attributes.city);
              self.checkService(singleAddress);
            } else {
              var headerText = Translator.translate('Multiple Results Found For')+' '+results.data[0].attributes.postalCode+', '+results.data[0].attributes.houseNumber;
              $("#multipleModalHeaderTextId").text(headerText);
              $('.address-check span.loading-search').addClass('invisible');
              var template = Handlebars.compile($("#HT-serviceabilityMultipleResults").html()),
                responseHTML = template(results);
              serviceabilityAddresses.html(responseHTML);
              serviceabilityModal.modal('show');
            }
          }
        }
        setPageLoadingState(false);
      };

      if (addressStreetNode.hasClass('validation-failed')) {
        addressStreetNode.removeClass('validation-failed');
      }
      $.ajax({
        beforeSend : function(request) {
          request.complete = function() {
            customer.setLoading(false);
          };
        },
        url : validateApiUrl + '?' + encodeURIComponent($.param(formData)),
        type : 'GET',
        success: callback,
        error: function(xhr) {
          result = JSON.parse(xhr.responseText);
          if (result.errors && result.errors[0].code && result.errors[0].code == "404") {
            jQuery("#postcodeWarningContainer").removeClass("move-postcode-notification hidden");
            showNotificationBar(Translator.translate('Invalid address'), 'error');
          } else if (typeof result.errors !== 'undefined' && typeof result.errors[0].code !== 'undefined') {
            showPlealErrorMsg (result.errors[0].code, result.errors[0].title, result.errors[0].detail, "Validate Address");
          } else {
            showModalError("Request failed with status code: " + xhr.status + ' Response : ' + xhr.responseText);
          }
        }
      });
    },

    checkSameAddressValidation: function() {
      //validate move same Address
      jQuery(".move-same-address-error").addClass('hidden');
      if (jQuery("#moveServiceAddressCheckModal").hasClass('in')) {
        var formData = $('#moveServiceAddressCheckForm').serializeObject();
        var currentAddress = store.get('serviceApiAddress');
        if (currentAddress) {
          if (typeof currentAddress.housenumberaddition == "undefined") {
            currentAddress.housenumberaddition = formData.housenumberaddition;
          }
          if (currentAddress.postalcode == formData.postalcode &&
            currentAddress.housenumber == formData.housenumber &&
            currentAddress.housenumberaddition == formData.housenumberaddition
          ) {
            jQuery(".move-same-address-error").removeClass('hidden');
            return false;
          }
        }
      }
      return true;
    },

    validate: function (formId, button) {
      var self = this;
      var $_container = $(button).parents('.ajax-container:first');
      var $_message = $_container.find('.ajax-results-message:first');
      var $_addresses = $_container.find('.ajax-results-addresses:first');
      this.clearMoveServiceAddressCheckModalResults();
      var formData = $(formId).serializeObject();
      var addressCheckForm = new VarienForm(formId.replace('#', ''));
      if (addressCheckForm.validator.validate() == false) {
        return;
      }
      $(button).addClass('active');
      that = this;
      var callback = function (results) {
        $('#move-housenumberaddition').removeClass('required-entry validation-failed');
        // Make address check inputs disabled while request is executed
        if ($(button).closest('.modal').attr('id') == that.moveServiceAddressCheckModal.replace('#', '')) {
          $(that.moveServiceAddressCheckModal).find('.address-check-input').addClass('disabled').prop('disabled', true);
        }
        // store validate address api response into localStorage for further uses
        store.set('validateApiResponse', JSON.stringify(results));
        // Make address check inputs enabled when request is done
        if ($(button).closest('.modal').attr('id') == self.moveServiceAddressCheckModal.replace('#', '')) {
          $(self.moveServiceAddressCheckModal).find('.address-check-input').removeClass('disabled').prop('disabled', false);
        }
        $(button).removeClass('active');
        if (results.error || results.errors) {
          showPlealErrorMsg (results.errors[0].code, results.errors[0].title, results.errors[0].detail, "Validate Address");
        } else {
          $_message.text(results.message);
          if (results.data) {
            if (results.data.hasOwnProperty('houseNumber')) {
              results.data = [results.data];
            }
            if (results.data.length == 1) {
              elem = $('<div/>', {
                'data-id': results.data[0].id,
                'data-street': results.data[0].attributes.street,
                'data-building-nr': results.data[0].attributes.houseNumber,
                'data-postalcode': results.data[0].attributes.postalCode,
                'data-city-name': results.data[0].attributes.city,
                'data-house-addition': results.data[0].attributes.houseNumberAddition
              });
              $(addressCheckModal).append(elem);
              address.checkService(elem);
            } else {
              if(jQuery('#move-service').val()) {
                var options = '';
                var optArr = [];
                if (!$('[id^=houseNumberAddition_]:selected').val()) {
                  for (var i = 0; i < results.data.length; i++) {
                    var houseNoAddSerice = '';
                    if (optArr.indexOf(results.data[i].attributes.houseNumberAddition) == '-1') {
                      houseNoAddSerice = results.data[i].attributes.houseNumberAddition ? results.data[i].attributes.houseNumberAddition : 'None';
                      if (results.data[i].attributes.houseNumberAddition == null) {
                        results.data[i].attributes.houseNumberAddition = '';
                      }
                      results.data[i].attributes.id = results.data[i].id;
                      options += '<option data-address=\''+JSON.stringify(results.data[i].attributes)+'\'  id="houseNumberAddition_'+i+'"  value="' + results.data[i].attributes.houseNumberAddition + '">' + houseNoAddSerice + '</option>';
                    }
                  }
                  $('#move-housenumberaddition').html('<option></option>' + options);
                  $('#move-housenumberaddition').parent().parent().removeClass('disabled');
                  $('#move-housenumberaddition').parent().initSelectElement();
                  $('#move-housenumberaddition').addClass('required-entry validation-failed');
                  $("#moveServiceAddressCheckForm .move-check-button").click();
                } else {
                  elem = $('<div/>', {
                    'data-id': results.data[0].id,
                    'data-street': results.data[0].attributes.street,
                    'data-building-nr': results.data[0].attributes.houseNumber,
                    'data-postalcode': results.data[0].attributes.postalCode,
                    'data-city-name': results.data[0].attributes.city,
                    'data-house-addition': results.data[0].attributes.houseNumberAddition
                  });
                  address.checkService(elem);
                }
              } else {
                var template = Handlebars.compile($("#HT-Check-Address").html()),
                  responseHTML = template(results);
                $_addresses.html(responseHTML);
              }
            }
          }
        }
      };
      jQuery("#postcodeWarningContainerBanner").addClass('hidden');
      $.ajax({
        beforeSend : function(request) {
          request.complete = function() {
            customer.setLoading(false);
          };
        },
        url : validateApiUrl + '?' + encodeURIComponent($.param(formData)),
        type : 'GET',
        success: callback,
        error: function(xhr) {
          result = JSON.parse(xhr.responseText);
          if (result.errors && result.errors[0].code && result.errors[0].code == "404") {
            jQuery("#postcodeWarningContainer").removeClass("move-postcode-notification hidden");
            if (jQuery("#addressCheckModal").hasClass('in')) {
              jQuery("#postcodeWarningContainerBanner").removeClass('hidden');
            }
          } else {
            showModalError("Request failed with status code: " + xhr.status + ' Response : ' + xhr.responseText);
          }
        }
      });
    },

    checkService: function (elem) {
      elem = $(elem);
      var self = this;
      var $_message = $(elem).closest('.modal').find('.ajax-results-message:first');
      var $_addresses = $(elem).closest('.modal').find('.ajax-results-addresses:first');
      // Callback is required when starting a serviceability needed package before a serviceability check
      // These are set from this.showCheckModal
      var callBack = self.callBack;
      var callBackParam = self.callBackParam;
      var callBackObject = self.callBackObject;
      // Hiding modal window. Note that there is an event attached to hide that unsets callBack, callBackParam and callBackObject
      if ($(elem).closest('.modal').attr('id') == this.addressCheckModal.replace('#', '')) {
        $(elem).closest('.modal').modal('hide');
      }

      $(self.serviceAbilityMultipleResultsModal).modal("hide");

      var confirmation = Number(elem.attr('data-clear-cart-confirmed') != undefined);
      // Setting request params
      var requestData = {
        'addressId': elem.attr('data-id'),
        'street': elem.attr('data-street'),
        'housenumber': elem.attr('data-building-nr'),
        'postalcode': elem.attr('data-postalcode'),
        'city': elem.attr('data-city-name'),
        'housenumberaddition': elem.attr('data-house-addition')
      };
      if (!this.checkSameAddressValidation())
        return;
      self.waiting = true;
      $(document).trigger("serviceability-check-started", []);
      $('.address-check span.loading-search').removeClass('invisible');
      setPageLoadingState(true);
      $.ajax({
        url: serviceApiUrl + '?' + encodeURIComponent($.param(requestData)),
        type: "get",
        dataType: "json",
        loader: false,
        success: function (response) {
          if (response.errors) {
            showPlealErrorMsg (response.errors[0].code, response.errors[0].title, response.errors[0].detail, "Serviceability Check");
          } else {
            response['footprint'] = true;
            this.footprintKey = false;
            this.footprintValueSet = false;
            for (var i = 0; i < response.data.length; i++) {
              // if (response.data[i].id == 'Cable Internet & Phone') {
              var items = response.data[i].attributes.items;
              for(var k=0; k<items.length; k++) {
                var key = items[k].key;
                if(key == 'Footprint') {
                  this.footprintValue = items[k].value.toUpperCase();
                  if(this.footprintValue == 'ZIGGO' || this.footprintValue == 'UPC') {
                    this.footprintValueSet = true;
                  }
                  this.footprintKey = true;
                }
              }
              if (!this.footprintKey || !this.footprintValueSet) {
                store.set('notServiceable', true);
                showValidateNotificationBar('<strong>'+Translator.translate('Sorry!')+'</strong> '+Translator.translate('We\'re unable to deliver Ziggo services on this address'), 'warning');
                setPageLoadingState(false);
                response['footprint'] = false;
                response.data.full_address = requestData;
                if(jQuery('#move-service').val() == false) {
                  self.services = response;
                  self.showServiceBar();
                  self.services.fnAvailability = '';
                  self.services.cableAvailability = '';
                  jQuery(".service-ability").css("background-color","#eee");
                }
                if(jQuery('#move-service').val() == 'true') {
                  jQuery('#notificationContainer').removeClass('hidden');
                  jQuery('#notificationContainer').removeClass('move-service-notification');
                }
                return;
              }
              store.set('notServiceable', false);
            }
            response.data.full_address = requestData;
            self.services = response;
            self.showServiceBar();
            self.services.fnAvailability = '';
            self.services.cableAvailability = '';
          }
        },
        complete: function (response) {
          // store serviceability api response into localStorage for further uses
          if(!this.footprintKey || !this.footprintValueSet && (jQuery('#move-service').val() == "true")) {
            return;
          }
          self.storeServiceAddress(requestData);
          if (response.responseJSON.errors != undefined && response.responseJSON.errors[0].detail) {
            showModalError('Status:'+response.responseJSON.errors[0].status+' Message: '+response.responseJSON.errors[0].detail);
          } else {
            if(jQuery('#move-service').val() == 'true') {
              var customerServiceAddress = store.get('serviceApiAddress');
              var customerServiceData = store.get('serviceApiResponse');
              store.set('serviceApiAddressMove', customerServiceAddress);
              store.set('serviceApiResponseMove', customerServiceData);
            }

            store.set('serviceApiResponse', JSON.stringify(response.responseJSON));
            store.set('serviceApiAddress', requestData);
            if (response && response.hasOwnProperty('responseJSON') && response.responseJSON.hasOwnProperty('requireConfirmation') && response.responseJSON.requireConfirmation) {
              setPageLoadingState(false);
            }
          }
          if (response.responseJSON.footprint == true) {
            delete response.responseJSON["footprint"];
            // store serviceability api response into localStorage for further uses
            //self.storeServiceAddress(requestData);
            if (response.responseJSON.errors != undefined && response.responseJSON.errors[0].detail) {
              showModalError('Status:'+response.responseJSON.errors[0].status+' Message: '+response.responseJSON.errors[0].detail);
            } else {
              store.set('serviceApiResponse', JSON.stringify(response.responseJSON));
              store.set('serviceApiAddress', requestData);
              if (response && response.hasOwnProperty('responseJSON') && response.responseJSON.hasOwnProperty('requireConfirmation') && response.responseJSON.requireConfirmation) {
                setPageLoadingState(false);
              } else {
                self.notifyServiceAbilityChanged();
                if (callBack) {
                  callBack.apply(callBackObject, [callBackParam]);
                  if (response.responseJSON.footprint == true) {
                    delete response.responseJSON["footprint"];
                    // store serviceability api response into localStorage for further uses
                    self.storeServiceAddress(requestData);
                    if (response.responseJSON.errors != undefined && response.responseJSON.errors[0].detail) {
                      showModalError('Status:'+response.responseJSON.errors[0].status+' Message: '+response.responseJSON.errors[0].detail);
                    } else {
                      store.set('serviceApiResponse', JSON.stringify(response.responseJSON));
                      store.set('serviceApiAddress', requestData);
                      if (response && response.hasOwnProperty('responseJSON') && response.responseJSON.hasOwnProperty('requireConfirmation') && response.responseJSON.requireConfirmation) {
                        setPageLoadingState(false);
                      } else {
                        self.notifyServiceAbilityChanged();
                        if (callBack) {
                          callBack.apply(callBackObject, [callBackParam]);
                        }
                        self.showServiceBar();
                      }
                      $('.address-check span.loading-search').addClass('invisible');
                    }
                  }
                }
              }
            }
          }
          if (window.CustomerDe.redirectURL && window.CustomerDe.redirectURL != "undefined" && window.CustomerDe.redirectURL != "" ) {
            window.location.href = window.CustomerDe.redirectURL;
          }
        },
      }).always(function() {
        setPageLoadingState(false);
      });
    },
    showServiceBar: function(services) {
      if (typeof(services)==='undefined') {
        services = this.services;
      }
      services['customer'] = customerDe.customerData;
      var template = Handlebars.compile($("#Address-ServiceAbility-Short").html()), responseHTML = template(services);
      var templateMore = Handlebars.compile($("#Address-ServiceAbility-More-Info").html()), responseMoreHTML = templateMore(services);
      $(this.serviceAbilityContainer).find('#topaddressCheckForm').hide();
      $(this.serviceAbilityContainer).html("").hide();
      $('#topaddressCheckForm').hide();
      $(this.serviceAbilityContainer).append(responseHTML).show();
      $(this.serviceAbilityMoreInfoContainer).find('.modal-body').html(responseMoreHTML);

      // Hide address check trigger (clear address button has been added under the additional info button from serviceability bar which will re-show the trigger)
      $(this.serviceAddressModalTrigger).hide();

      // Initially hide all components and open first one to start
      $('.service-additional-info').hide();
      $('.service-additional-info:first').show();

      // Actions to additional info tabs
      $('.service-additional-title').click(function () {
        $('.service-additional-title').next('.service-additional-info').hide();
        $(this).next('.service-additional-info').show();
      });

      // Reindex document tooltips
      $('[data-toggle="tooltip"]').tooltip();

      if (document.getElementById('topBar_addressCheck_isReloadPage').value) {
        setPageLoadingState(false);
        window.location.reload(); //must refresh to reload prices
      } else if (typeof window.campaign !== 'undefined' && !window.campaign.triggered)
      {
        window.campaign.getData(0);
      }

    },
    clearServices: function (confirmation) {
      var self = this;
      confirmation = Number(confirmation != undefined);
      self.waiting = true;
      $(document).trigger("serviceability-check-started", []);
      $.ajax({
        url: '/address/index/clearServices/',
        type: "post",
        data: ({
          'address_id': '',
          'confirmation': confirmation
        }),
        dataType: "json",
        cache: false,
        success: function (response) {
          if (response.error == false) {
            self.resetServices();
            self.storeServiceAddress(null);
            setPageLoadingState(true);
            if (response['packages']) {
              // Remove packages which need new service address
              response['packages'].each(function (packageDetails) {
                self.handleRemovedPackage(packageDetails);
              });
            }
            window.location.href = MAIN_URL;
          } else if (response.requireConfirmation) {
            $(self.genericConfirmationModal).find('.confirmation-message').html(response.message);
            $(self.genericConfirmationModal).modal("show");
            $(self.genericConfirmationModal).find('.confirm-action-button').attr("onclick", "address.clearServices(true)");
          } else {
            showModalError(response.message);
          }
        },
        complete: function (response) {
          // remove responses of validate address and serviceability check services
          store.remove('validateApiResponse');
          store.remove('serviceApiResponse');
          store.remove('serviceApiAddress');

          setPageLoadingState(false);
          self.notifyServiceAbilityChanged();
          if (response.responseJSON.error == false && response.responseJSON.triggerRefresh === true) {
            setTimeout(function () {
              window.location.reload();
            }, 500);
          }
          if ($('.enlargeSearch').hasClass('left-direction')) {
            $('.enlargeSearch').trigger('click');
          }
          $('.loading-search').hide();
        }
      });
    },

    storeServiceAddress : function (requestData) {
      var self = this;
      $.ajax({
        url: '/checkout/index/saveServiceAddress/',
        type: "post",
        data: ({
          'serviceAddress': requestData,
          'addressData': requestData ? JSON.stringify({
            'serviceability': JSON.parse(store.get('serviceApiResponse')),
            'validate': JSON.parse(store.get('validateApiResponse'))
          }) : null
        }),
        dataType: "json",
        cache: false,
        success: function (response) {
        }
      }).complete(function(response) {
        var customerServiceData = jQuery.parseJSON(store.get('serviceApiResponse'));
        self.storeFootprint(customerServiceData);
      });
    },

    storeFootprint : function (response) {
      var footprintValueSet = false;
      var footprintKey = false;
      for (var i = 0; i < response.data.length; i++) {
        var items = response.data[i].attributes.items;
        for(var k=0; k<items.length; k++) {
          var key = items[k].key;
          if(key == 'Footprint') {
            var footprintValue = items[k].value.toUpperCase();
            if(footprintValue == 'ZIGGO'|| footprintValue == 'UPC') {
              footprintValueSet = true;
            }
            footprintKey = true;
          }
        }
      }

      // Save footprint to window object for checking if it was already saved
      window.footprint = footprintValue;
      $.ajax({
        async: true,
        url: '/checkout/index/saveFootprint/',
        type: "post",
        data: ({
          'footprint': footprintValue
        }),
        dataType: "json",
        cache: false,
        success: function (response) {
        }
      }).complete(function(response) {
        // Activates MOVE scenario
        if((footprintKey && footprintValueSet) && ($('#move-service').val() == 'true')) {
          customerSection.changePackage(this, 'MOVE');
        }
      });
    },
    handleMoveServiceAddressCheckButton: function() {
      var enableButton = true;
      $(this.moveServiceAddressCheckModal).find('.address-check-input.required-entry').each(function(){
        if (!$(this).val().replace(/\s/g, '').length) {
          enableButton = false;
          return false;
        }
      });
      if (enableButton) {
        $(this.moveServiceAddressCheckModal).find('.modal-footer .move-check-button').prop('disabled', false);
      } else {
        $(this.moveServiceAddressCheckModal).find('.modal-footer .move-check-button').prop('disabled', true);
      }

    },
    searchMoveAddress: function() {
      var postcode = jQuery('#move-postalcode').val().replace(/ /g,'');
      var houseno = jQuery('#move-housenumber').val().replace(/ /g,'');

      if(jQuery.trim(postcode) != '' && jQuery.trim(houseno) != '') {
        jQuery.post(
          '/checkout/index/searchAddress',
          {postcode: jQuery.trim(postcode), houseno:jQuery.trim(houseno)},
          function(result) {
            jQuery('#move-street').val(result.fields.street).trigger('input');
            jQuery('#move-city').val(result.fields.city).trigger('input');
          }
        );
      }
    }
  });
})(jQuery);

function reloadThisPage()
{
  window.location.reload();
}

function showPlealErrorMsg (code, msg, det, service) {
  setPageLoadingState(true);
  jQuery.ajax({
    url: '/address/index/getPealErrorTranslation/',
    type: "post",
    data: ({
      'code': code,
      'message': msg,
      'service': service
    }),
    dataType: "json",
    cache: false,
    success: function (response) {
      setPageLoadingState(false);
      if (response.message == '') {
        showNotificationBar(det, 'error');
      } else {
        showNotificationBar(response.message, 'error');
      }
    }
  });
}

