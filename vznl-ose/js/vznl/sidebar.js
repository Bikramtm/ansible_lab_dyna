/*
 * Copyright (c) 2018. Dynacommerce B.V.
 */

(function ($) {
  window.Sidebar.prototype = $.extend(VEngine.prototype, {
    showDetailedPrice: function() {
      $('.gesamt-min').removeClass('hide');
    },
    showSummarizedPrice: function() {
      $('#cartTotalsDetails').removeClass('hide');
      $('.cart-totals-table').removeClass('hide');
      $('.gesamt-min').addClass('hide');
    },
    handlePriceDisplayState: function() {
      ToggleBullet.onSwitchChange('.tax-toggle', 'tax');
      //By default if no state is set, hide the details
      if (store.get('btwState') == null || store.get('btwState') == true) {//show max
        ToggleBullet.switchOn('.tax-toggle');

        $('#cartTotalsDetails').removeClass('hide');
        $('.cart-totals-table').removeClass('hide');

        $('.gesamt-min').addClass('hide');

      } else {//show min
        ToggleBullet.switchOff('.tax-toggle');

        $('.gesamt-min').removeClass('hide');
      }
    },
    correctPricesVisibility: function () {
      window.setPageOverrideLoading && window.setPageOverrideLoading(true);
      /** We need the new state, which will be updated after the ajax call **/
      var state = ToggleBullet.state('.tax-toggle');
      var self = this;
      store.set('btwState', state);
    },
    initConfiguratorLocal: function (element, hasParent, skipWarningCheck) {
      $('.notification-banner error').hide();
      var self = this;
      /** Close customer panel if we want to add a new package**/
      if ($('.enlargeSearch').hasClass('left-direction')) {
        LeftSidebar.toggleLeftSideBar(false, true);
      }
      /** If no serviceability check has been made, request address check and come back here */
      if ($(element).hasClass(this.requiredServiceAbilityFlag) && !address.services) {
        address.showCheckModal(this.initConfigurator, element, window.sidebar);
      } else if($(element).hasClass(this.requiredServiceAbilityFlag) && address.services && $(element).hasClass('disabled')) {
        return false;
      } else if ($(element).hasClass('disabled')) {
        return false;
      } else {
        self.initConfiguratorPackage = element;
        if (skipWarningCheck) {
          // just continue with init
        } else {
          if ($(element).data('requires-group-confirmation') && self.showReservedMemberConfirmation) {
            self.checkReservedMemberWarning(element);
            return false;
          }
        }

        /** Init configurator */
        var target = $(element).attr('data-target');
        var packageTypeId = $(element).attr('data-package-creation-type-id');
        var panelId = $('#buttons-trigger').attr('data-target');
        var migrationOrMoveOffnet = $(element).attr('data-migration-or-moveoffnet');
        var migrationOrMoveOffnetCustomerNo = $(element).attr('data-customer-no-migration-or-move-offnet');
        var relatedProductSku = $(element).attr('data-related-product-sku');
        var bundleId = $(element).attr('data-bundle-id');
        var filters = $(element).attr('data-filter-attributes');
        var saleType = $(element).attr('data-sale-type');
        $('#' + panelId).hide();

        if (controller != 'cart') {
          $('#' + target).show();

          // minimize all
          $('#' + target + ' .content').each(function () {
            $(element).removeClass('hidden');
          });
          if (this.openPackage) {
            $('#' + this.openPackage + ' .content').each(function () {
              $(element).addClass('hidden');
            });
          }

          this.openPackage = target;

          if ($(element).hasClass('show-clear-cart-warning')) {
            // prepare call back for confirmation of deleting all packages
            self.callBack = function () {
              $('#cancel-cart-by-package-modal').modal('hide');
              this.initConfiguratorLocal(target, null, saleType, null, null, true, migrationOrMoveOffnet, migrationOrMoveOffnetCustomerNo, null, hasParent, relatedProductSku, bundleId, filters, packageTypeId, 1)
            };
            // show confirmation dialog and ensure on modal closing that callback is reset
            $('#cancel-cart-by-package-modal').modal().on('hide.bs.modal', function() {
              self.callBack = null;
            });
            $('.cancel-cart-by-package-button').unbind('click').on('click', function() {
              self.callBack();
              self.callBack = null;
            });
          } else {
            this.initConfiguratorLocal(target, null, saleType, null, null, true, migrationOrMoveOffnet, migrationOrMoveOffnetCustomerNo, null, hasParent, relatedProductSku, bundleId, filters, packageTypeId);
          }


          $('#package-types').addClass('hide');

          // OMNVFDE-3735 the buttons to add new package for cable should be available at migration
          if (!showOnlyOnePackage) {
            $('#show-packages').removeClass('hide');
            $('#cart-spinner-wrapper').find('.cart_packages').removeClassPrefix('control-buttons-');
          }
          // if the agent adds a KIP package and the migration menu is present in cart, hide the menu
          if (target == CABLE_INTERNET_PHONE_PACKAGE) {
            $('.migration-info').addClass('hide');
          }
          $('.no-package-zone').addClass('hide');
          jQuery('#btn-delete-all').removeClass('disabled');
          jQuery('#btn-save-and-send').removeClass('disabled');
        } else {
          e.preventDefault();
          window.location.href = MAIN_URL + '?type=' + target;
        }

      }
    },
    setCustomerType: function (type) {
      var customerId = jQuery('#left-customer-info').data('id');
      if (customerId) {
        return;
      }
      switch (type)
      {
        case 'individual':
          jQuery('#individualButton').addClass('selected');
          jQuery('#organizationButton').removeClass('selected');
          break;
        case 'organization':
          jQuery('#organizationButton').addClass('selected');
          jQuery('#individualButton').removeClass('selected');
          break;
      }
    },
    initConfigurator: function (element, hasParent, skipWarningCheck) {
      var self = this;
      /** Close customer panel if we want to add a new package**/
      if ($('.enlargeSearch').hasClass('left-direction')) {
        $('.enlargeSearch').trigger('click');
      }
      /** If no serviceability check has been made, request address check and come back here */
      if ($(element).hasClass(this.requiredServiceAbilityFlag) && !address.services) {
        address.showCheckModal(this.initConfigurator, element, window.sidebar);
      } else if($(element).hasClass(this.requiredServiceAbilityFlag) && address.services && $(element).hasClass('disabled')) {
        return false;
      } else if ($(element).hasClass('disabled')) {
        return false;
      } else {
        self.initConfiguratorPackage = element;
        if (skipWarningCheck) {
          // just continue with init
        } else {
          if ($(element).data('requires-group-confirmation') && self.showReservedMemberConfirmation) {
            self.checkReservedMemberWarning(element);
            return false;
          }
        }

        /** Init configurator */
        var target = $('<div/>').html($(element).attr('data-target')).text();
        var packageTypeId = $('<div/>').html($(element).attr('data-package-creation-type-id')).text();
        var panelId = $('<div/>').html($('#buttons-trigger').attr('data-target')).text();
        var migrationOrMoveOffnet = $('<div/>').html($(element).attr('data-migration-or-moveoffnet')).text();
        var migrationOrMoveOffnetCustomerNo = $('<div/>').html($(element).attr('data-customer-no-migration-or-move-offnet')).text();
        var relatedProductSku = $('<div/>').html($(element).attr('data-related-product-sku')).text();
        var bundleId = $('<div/>').html($(element).attr('data-bundle-id')).text();
        var filters =  $('<div/>').html($(element).attr('data-filter-attributes')).text();
        var saleType =  $('<div/>').html($(element).attr('data-sale-type')).text();
        $('#' + panelId).hide();

        if (controller != 'cart') {
          $('#' + target).show();

          // minimize all
          $('#' + target + ' .content').each(function () {
            $(element).removeClass('hidden');
          });
          if (this.openPackage) {
            $('#' + this.openPackage + ' .content').each(function () {
              $(element).addClass('hidden');
            });
          }

          this.openPackage = target;

          if ($(element).hasClass('show-clear-cart-warning')) {
            // prepare call back for confirmation of deleting all packages
            self.callBack = function () {
              $('#cancel-cart-by-package-modal').modal('hide');
              initConfigurator(target, null, saleType, null, null, true, migrationOrMoveOffnet, migrationOrMoveOffnetCustomerNo, null, hasParent, relatedProductSku, bundleId, filters, packageTypeId, 1);
            };
            // show confirmation dialog and ensure on modal closing that callback is reset
            $('#cancel-cart-by-package-modal').modal().on('hide.bs.modal', function() {
              self.callBack = null;
            });
            $('.cancel-cart-by-package-button').unbind('click').on('click', function() {
              self.callBack();
              self.callBack = null;
            });
          } else {
            initConfigurator(target, null, saleType, null, null, true, migrationOrMoveOffnet, migrationOrMoveOffnetCustomerNo, null, hasParent, relatedProductSku, bundleId, filters, packageTypeId);
          }


          $('#package-types').addClass('hide');

          // OMNVFDE-3735 the buttons to add new package for cable should be available at migration
          if (!showOnlyOnePackage) {
            $('#show-packages').removeClass('hide');
            $('#cart-spinner-wrapper').find('.cart_packages').removeClassPrefix('control-buttons-');
          }
          // if the agent adds a KIP package and the migration menu is present in cart, hide the menu
          if (target == CABLE_INTERNET_PHONE_PACKAGE) {
            $('.migration-info').addClass('hide');
          }
          $('.no-package-zone').addClass('hide');
          jQuery('#btn-delete-all').removeClass('disabled');
          jQuery('#btn-save-and-send').removeClass('disabled');
        } else {
          e.preventDefault();
          window.location.href = MAIN_URL + '?type=' + target;
        }

      }
    },
    updatePrices: function (element) {
      const state = {
        el: $(element),
        value: element.checked
      }
      customerDe.btwState = state;
      window.setPageOverrideLoading && window.setPageOverrideLoading(true);
      $.post('/customer/details/setBTWState', { state: state.value ? 0 : 1 })
        .done(function(data) {
          window.setPageOverrideLoading && window.setPageOverrideLoading(false);
          window.configurator && window.configurator.updateBtwState(data.btw);
        })
        .fail(function(data) {})
      ;
      if (state.value === true) {
        $('.excl-vat-tax').show();
        $('.incl-vat-tax').hide();
        $('#config-wrapper').addClass('excluding-vat').removeClass('including-vat');
        $('[name="exclInclTaxSwitch"]').bootstrapSwitch('state', false);
        $('#cart_totals .specification-show').show();
      } else {
        $('.excl-vat-tax').hide();
        $('.incl-vat-tax').show();
        $('#config-wrapper').removeClass('excluding-vat').addClass('including-vat');
        $('[name="exclInclTaxSwitch"]').bootstrapSwitch('state', true);
        $('#cart_totals').removeClass('specification');
        $('#shopping-bag .specification-show').hide();
      }
    }
  });
  $(document).ready(function () {
    jQuery('#specificationsToggle').trigger('change')

    $(document).bind('serviceability-check-completed', function () {
      var packageType = $('#package-types');
      if (address.services) {
        packageType.find('.' + sidebar.requiredServiceAbilityFlag).each(function(index, element) {
          element = $(element);
          var service = element.data('requires-green-service');
          if (typeof service === 'undefined') {
            element.removeClass('disabled');
            return true;
          }

          $.each(address.services.data, function(index, serviceData) {
            if (serviceData.attributes.availability) {
              $.each(serviceData.attributes.availability, function(key, value) {
                if (value.key.toUpperCase() == 'AVAILABLE' && value.value.toUpperCase() == 'TRUE') {
                  element.removeClass('disabled');
                }
              });
            }
          });
        });
      } else {
        if (store.get('serviceApiResponse') == null) {
          packageType.find('.' + sidebar.requiredServiceAbilityFlag).addClass('disabled');
        }
      }

      // Disable if has open order. !!! Same logic in sidebar.phtml
      $('#package-types')
        .find('.btn-block.cursor[data-has-open-order="true"][data-target!="' + MOBILE_PACKAGE_TYPES.join('"][data-target!="') + '"]')
        .removeClass('disabled')
        .addClass('disabled');
    });

    window.sidebar.handlePriceDisplayState();

    jQuery(document).on('click', '#individual, #organization', function () {
      window.sidebar.setCustomerType(jQuery(this).attr('id'));
    });
  });
})(jQuery);