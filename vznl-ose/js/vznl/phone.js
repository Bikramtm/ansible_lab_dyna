window.isOffer = 0;
var phone = {
  // Multi package Init configurator data is built in Dyna_Bundles_Helper_Data::getCampaignData()
  loadCampaignOffers: function (packages, saleType, ctn, offerId) {
    setPageLoadingState(true);
    if(typeof packages === 'string') {
      packages = (JSON).parse(decodeURIComponent(packages));
    }
    var createPackages = {
      'packages': packages,
      'saleType': saleType,
      'offerId': offerId,
      'isOffer': 1
    };
    jQuery.ajax({
      type: 'POST',
      data: createPackages,
      dataType: "json",
      url: 'configurator/cart/createPackages',
      success: function(response) {
        if (!response.error) {
          var getUrl = window.location;
          var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
          window.location = baseUrl;
        } else {
          showModalError(response.message);
        }
      }
    });
  },
  filterCustomerCampaigns: function (customerData) {
    if (customerData.hasOwnProperty('customerCampaigns')) {
      customerData.customerCampaigns.forEach(function (campaignId) {
        //Enable all campaigns allowed for current customer
        var campai4gnElement = jQuery('#homepage-wrapper #campaigns').find('[data-campaign=\'' + campaignId + '\']');
        campaignElement.find('.content-block').removeClass('disabled');
        campaignElement.find('.buy-campaign-button').removeClass('disabled');
        campaignElement.find('.buy-campaign-button').find('input[type=\'button\']').removeAttr('disabled');
      });
    } else {
      console.log('Customer has no allowed campaigns associated');
    }

    return this;
  },
};
