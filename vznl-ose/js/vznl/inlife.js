/*
 * Copyright (c) 2018. Dynacommerce B.V.
 */

(function ($) {
  Inlife = (function (original) {
    function Inlife() {
      original.apply(this, arguments);

      // Overriding methods
      this.inlifeController = '/inlife/index/';

      this.editCtn = function(lastCtn, append) {
        window.setPageLoadingState(true);
        var self = this;
        $.ajax({
          async: true,
          type: 'POST',
          data: {
            'last_ctn': lastCtn
          },
          url: self.inlifeController + 'retrieveNumberList'
        }).done(function(data) {
          window.setPageLoadingState(false);
          if (data.error) {
            showModalError(data.message);
          } else {
            var ctns = '';
            data.numbers.each(function (id) {
              ctns += '<tr><td><div class="radio-number-selection"><label><input type="radio" name="ctn-phone-number" value="1" data-phone-number="' + id + '">' + id + '</label></div></td></tr>';
            });

            var modal = $('#search-inlife-phone-number');
            var ctnTable = modal.find('table#ctn-content tbody');
            modal.find('#ctn-modal-error').html('');
            if (append !== true) {
              ctnTable.html(ctns);
              modal.appendTo('body').modal();
            } else {
              ctnTable.append(ctns);
            }
          }
        }).fail(function() {
          var modal = $('#search-inlife-phone-number');
          modal.find('#ctn-modal-error').html('Failed to get numbers list');
          console.log('Failed to get numbers list');
        }).always(function() {
          window.setPageLoadingState(false);
        });
      };

      this.loadMoreNumbers = function() {
        var modal = $('#search-inlife-phone-number');
        var lastCtn = modal.find('table#ctn-content tbody tr input').last().data('phone-number');
        this.editCtn(lastCtn, true);
      };

      /** ================ Change SIM ================ **/
      this.editSim = function(ctn) {
        window.setPageOverrideLoading(true);
        var self = this;
        var inlifeWrapper =  $('#inlife-wrapper');
        inlifeWrapper.find('.loading-extras, .home-wrapper .section, .home-wrapper .main-screen').addClass('hidden');
        $('.loading-edit-sim').removeClass('hidden').delay( 800 );
        $.ajax({
          async: true,
          type: 'POST',
          data: {'ctn': ctn},
          url: self.inlifeController + 'editSim',
          success: function (data) {
            $('#inlife-edit-block').html(data.html).removeClass('hidden');
            window.setPageOverrideLoading(false);
            $('.loading-edit-sim').addClass('hidden');
            $('.otherAddress').addClass('hidden');
          },
          error: function () {
            $('.loading-edit-sim').addClass('hidden');
            console.log('Failed to get requested block');
            window.setPageOverrideLoading(false);
          }
        });
      };

      this.cancelEditSim = function() {
        var inlifeWrapper =  $('#inlife-wrapper');
        inlifeWrapper.find('.home-wrapper .section, .home-wrapper .main-screen, .otherAddress').removeClass('hidden');
        inlifeWrapper.find('.edit-sim, #inlife-edit-block').addClass('hidden');
      };

      this.confirmSimSwap = function(obj) {
        var self = this;
        var formData = $(obj.form).serializeObject();
        self.formData = formData;

        if ($.trim(formData.sim_id) !== '') {
          $('#simcard-select').removeClass('validation-failed required-entry').addClass('validation-passed');
          $('#advice-required-entry-simcard-select').fadeOut();
        }

        if (obj.validator && obj.validator.validate()) {
          var data = {
            'block': 'inlife_confirm',
            'action': 'simswap',
            'modal': true
          };
          // Add the fields from the form
          $.extend(data, formData);
          self.getInlifeBlock(data);
        }
        return false;
      };

      this.createSimSwapOrder = function(obj) {
        var self = this;
        var form = $(obj).parents('form').first();
        var data = form.serializeObject();

        $.extend(data, self.formData);
        self.formData = data;

        var callback = function() {
          window.setPageOverrideLoading && window.setPageOverrideLoading(true);
          $.ajax({
            async: true,
            type: 'POST',
            data: data,
            url: self.inlifeController + 'createSimSwapOrder',
            success: function (data) {
              if (data.error) {
                if (data.html) {
                  $('#inlife-wrapper .inlife-loading').addClass('hidden');
                  $('#inlife-edit-block').html(data.html).removeClass('hidden');
                }
              } else if (!data.error) {
                self.doSimSwapPolling(data.polling_id, data.order_id);
                self.simSwapIntervalID = setInterval(function () {
                  self.doSimSwapPolling(data.polling_id, data.order_id);
                }, 10000);
              } else {
                console.log('Unknown response returned');
              }
            },
            error: function () {
              console.log('Failed to process requested action');
            }
          });
        };

        self.inlifeLoad('simswap', 'loading', 'SIM Swap', '', callback);
        return false;
      };

      this.doSimSwapPolling = function(id, order_id) {
        var self = this;
        --self.performSimSwapPollingCount;
        if (self.simSwapIntervalID !== false) {

          $.ajax({
            async: true,
            type: 'POST',
            data: { 'polling_id': id },
            url: self.inlifeController + 'orderPolling',
            success: function (data) {
              if (data.error == true) {
                self.simSwapIntervalID = false;
                // error
                showModalError(data.message, data.serviceError, self.cancelEditSim);
              } else if (data.error == false) {
                if (data.hasOwnProperty('performed') && data.performed == true) {
                  self.simSwapIntervalID = false;
                  if (order_id) {
                    // success
                    self.saveSimSwap(order_id, data.sim)
                  } else {
                    self.inlifeLoad('simswap', 'success', 'SIM Swap', '');
                  }
                }
              } else {
                console.log('error occured / no response received');
              }
            }
          });

        }

        if (self.performSimSwapPollingCount == 0) {
          // polling failed
          clearInterval(self.simSwapIntervalID);
          self.simSwapIntervalID = false;
        }
        return false;
      };

      this.simSwapAction = function(obj) {
        var self = this;
        self.inlifeLoad('simswap', 'loading', 'SIM Swap', '', '');

        var form = $(obj).parents('form').first();
        var data = form.serializeObject();

        $.extend(data, self.formData);
        self.formData = data;

        self.saveSimSwap(null, null);
      };

      this.saveSimSwap = function(order_id, sim) {
        var self = this;
        var polling_id = false;

        window.setPageOverrideLoading && window.setPageOverrideLoading(true);

        var callback = function(polling_id) {
          self.doSimSwapPolling(polling_id, null);
          self.simSwapIntervalID = setInterval(function () {
            self.doSimSwapPolling(polling_id, null);
          }, 10000);
        };

        $.ajax({
          async: true,
          type: 'POST',
          data: $.extend(self.formData, {'order_id': order_id, 'sim': sim}),
          url: self.inlifeController + 'simSwap',
          success: function (data) {
            if (data.error) {
              if (data.html) {
                $('#inlife-wrapper .inlife-loading').addClass('hidden');
                $('#inlife-edit-block').html(data.html).removeClass('hidden');
              }
            } else if (!data.error) {
              if (!data.polling_id) {
                self.inlifeLoad('simswap', 'success', 'SIM Swap', '');
              } else {
                self.inlifeLoad('simswap', 'loading', 'SIM Swap', '', callback(data.polling_id));
                window.setPageOverrideLoading && window.setPageOverrideLoading(true);
              }
            } else {
              console.log('Unknown response returned');
            }
          },
          error: function () {
            console.log('Failed to process requested action');
          }
        });
      };

      this.inlifeLoad = function(action, section, name, current, callback) {
        $('#inlife-confirm').modal('hide');

        var inlifeWrapper =  $('#inlife-wrapper');
        inlifeWrapper.find('.home-wrapper .section, .eligible-priceplans, .edit-sim, #inlife-edit-block').addClass('hidden');
        this.getInlifeBlock({
          'block': 'inlife_action',
          'action': action,
          'section': section,
          'name': name,
          'current': current
        }, callback);
      };

      this.selectSim  = function(obj) {
        obj = $(obj);
        var name = obj.children('span:first-child').text();
        var cost = obj.children('span:last-child').text();
        var parent = obj.parents('.btn-group');
        parent.find('.dropdown-toggle .text').text(name + ' ' + cost);
        parent.removeClass('open');

        var costContainer = $('.edit-sim .cost-container');
        costContainer.removeClass('hidden');
        costContainer.find('.name').text(name);
        costContainer.find('.sim-price').text(cost);
        costContainer.find('.total').text(cost);
        costContainer.find('.total-excl').text(obj.data('price-excl'));
        costContainer.find('[name="sim_name"]').val(name);
        costContainer.find('[name="sim_id"]').val(obj.data('sim-id'));

        if ($.trim(obj.data('sim-id')) != '') {
          $('#simcard-select').removeClass('validation-failed required-entry').addClass('validation-passed');
          $('#advice-required-entry-simcard-select').fadeOut();
        }
        $('#inlife-cash-amount').text(cost);
      };

      this.getInlifeBlock = function(data, callback) {
        var self = this;
        var is_modal = (data.modal == true) ? true : false;
        var is_edit = (data.edit == true) ? true : false;
        var section = data.section;
        $('#inlife-confirm').remove();

        window.setPageLoadingState(true);
        $.ajax({
          async: (data.simulate == true) ? true : false,
          type: 'POST',
          data: data,
          url: self.inlifeController + 'getInlifeBlock',
          success: function (data) {
            if (data.error) {
              showModalError(data.message);
            } else if (!data.error) {
              if (is_modal) {
                $('#inlife-wrapper #inlife-modal').html(data.html);
                $('#inlife-confirm').appendTo('body').modal('show');
              } else {
                if (is_edit) {
                  $('#inlife-wrapper .inlife-loading').addClass('hidden');
                  $('#inlife-edit-block').html(data.html).removeClass('hidden');
                } else {
                  if (section == 'loading') {
                    $('#inlife-edit-block').html(data.html).removeClass('hidden');
                  } else {
                    self.updateInlifeData(data.html);
                  }
                }
                if (callback && typeof callback == 'function') {
                  setTimeout(callback, 300);
                }
              }
            } else {
              console.log('Failed to get requested block');
            }
            window.setPageLoadingState(false);
          },
          error: function () {
            console.log("error");
            console.log('Failed to get requested block');
            window.setPageLoadingState(false);
          }
        });
      };

      this.updateInlifeData = function(data) {
        if (data) {
          $('#inlife-wrapper .main-screen').html(data).removeClass('hidden');
          $('#inlife-wrapper .loading-extras').addClass('hidden');
        }
      };

      this.retrieveSubsAddonsData = function(ctnCode) {
        var self = this;
        if (ctnCode) {
          window.setPageLoadingState(true);
          $('#inlife-wrapper .main-screen').empty();
          $('#inlife-wrapper .loading-extras').removeClass('hidden');
          $.ajax({
            async: true,
            type: 'POST',
            data: {'ctn_code': ctnCode},
            url: self.inlifeController + 'getInlifeCtnData',
            success: function (data) {
              window.setPageLoadingState(false);
              if (data.error) {
                showModalError(data.message);
              } else if (!data.error) {
                if (data.html) {
                  $('#inlife-wrapper .main-screen').html(data.html);
                  $('#inlife-wrapper .loading-extras').addClass('hidden');
                }
              } else {
                console.log('Failed to get ctn details');
              }
            },
            error: function () {
              console.log('Failed to get ctn details');
              window.setPageLoadingState(false);
            }
          });
        } else {
          showModalError('Invalid CTN provided');
          $(location).attr('href', '/');
        }
      };
    }

    Inlife.prototype = original.prototype;
    Inlife.prototype.constructor = Inlife;
    return Inlife;
  })(Inlife);

  /**
   * Validation from ose/app/design/frontend/vznl/default/template/inlife/ctn_details.phtml template
   */
  $(document).ready(function() {
    if (typeof validation_inlife_string !== 'undefined') {
      window.inlife = new Inlife(validation_inlife_string.current_website_code);
      inlife.websites = validation_inlife_string.website_codes;
      inlife.customer.isBusiness = validation_inlife_string.is_business;
      if (typeof loadGeneralModals === 'function') {
        var modalsContainer = $('#footer_modals');
        loadGeneralModals(modalsContainer, false);
      }
      // Make ajax call to retrieve the latest data about price plans and addons
      inlife.retrieveSubsAddonsData(validation_inlife_string.current_ctn_code);

      Validation.add('validate-sim', validation_inlife_string.invalid_sim, function (v, element) {
        var name = jQuery(element).attr('name');
        v = v.replace(/\s/g, '');
        if (name.indexOf('postfix') !== -1) {
          name = name.replace('postfix', 'prefix');
          var prefixElem = jQuery('[name="{0}"]'.format(name));
          if (prefixElem.length) {
            var prefix = prefixElem.val();
            v = prefix.replace(/\s/g, '') + v;
          }
        }
        var reg = validation_inlife_string.sim_format;
        return reg.test(v);
      });

      Validation.add('validate-name', validation_inlife_string.invalid_chars_detected, function (v, element) {
        var reg = validation_inlife_string.name_regex;
        return false == reg.test(v);
      });

      Validation.add('validate-name-length', validation_inlife_string.to_short, function (v, element) {
        return jQuery.trim(v).length > 1;
      });

      Validation.add('nl-postcode', validation_inlife_string.invalid_postcode, function (v) {
        return (v == '' || (v == null) || (v.length == 0) || /^[0-9][0-9]{3}\s?[a-zA-Z]{2}$/.test(v));
      });

      Validation.add('validate-date-nl', Translator.translate('Please enter a valid date using the following date format: dd-mm-yyyy.'), function (v, element) {
        if ((v != '') && (v != null) && (v.length != 0)) {
          var reg = /^(0[1-9]|[12][0-9]|3[01])([-])(0[1-9]|1[012])\2(\d{4})$/;
          return reg.test(v);
        } else {
          return true;
        }
      });

      Translator.add('Must select a number', validation_inlife_string.must_be_number);
    }
  });
})(jQuery);
