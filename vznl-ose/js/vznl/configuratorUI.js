/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */
(function($) {
  window.ConfiguratorUI.Base.prototype = $.extend(ConfiguratorUI.Base.prototype,
    {
      /**
       *  This function needs to be refactored because it doesn't take into consideration the btwState
       *  returns null if you try to use store.get("btwState")
       *  If this method is fixed then the excluding prices for the deviceRcs will be displayed
       **/
      updateProductPrices: function() {
        var self = this, productId, item;

        // update icons
        if (ToggleBulletSoho.getSohoState('.soho-toggle')) {
          jQuery('span.item_ilt').hide();
        }

        // icon handlers
        jQuery('.item-info').on('click', function (e) {
          // stop the event from bubbling.
          e.stopPropagation();
          // load the modal
          if(jQuery(this).data('package'))
          {
            window.configuratorUI.getConfigurator().loadProductInformationPopup(jQuery(this).data('item-id'),jQuery(this).data('package'));
          }
          else {
            window.configuratorUI.getConfigurator().loadProductInformationPopup(jQuery(this).data('item-id'));
          }
        });

        // window.prices is always updated after getPrices call
        var itemForProductId = new Map();

        // iterating through each section to see if product prices have been updated
        this.configurator.options.sections.forEach(function(section) {
          if (typeof self.configurator.products !== 'undefined' && self.configurator.products.hasOwnProperty(section)) {
            self.configurator.products[section].forEach(function(product) {
              productId = product.entity_id;
              var productPrices = window.prices[parseInt(productId)];
              // checking if product has special price
              if (typeof productPrices !== 'undefined') {
                var productSpecialPrice = window.specialPriceProducts[productId];
                var hasSpecialPrice = productSpecialPrice;

                if (!itemForProductId.has(productId)) {
                  itemForProductId.set(productId, $('#item-product-' + productId));
                }
                item = itemForProductId.get(productId);
                var itemPriceExclTaxPerMonth =  item.find('.excl-vat-tax.per-month:first');
                var itemPriceInclTaxPerMonth =  item.find('.incl-vat-tax.per-month:first');
                var itemPriceExclTaxOneTime = item.find('.excl-vat-tax.one-time:first');
                var itemPriceInclTaxOneTime = item.find('.incl-vat-tax.one-time:first');

                var mafPrice = productPrices.maf;
                if (mafPrice !== null && mafPrice !== false && mafPrice !== undefined) {
                  // checking maf
                  self.updateConfiguratorPrice(itemPriceExclTaxPerMonth, mafPrice, product['maf'], section);
                  itemPriceExclTaxPerMonth.removeClass('hide');
                } else {
                  itemPriceInclTaxPerMonth.addClass('hide');
                  itemPriceExclTaxPerMonth.addClass('hide');
                }

                // avoid updating prices if null or false come on response
                var mafTaxPrice = productPrices.maf_with_tax;
                if (mafTaxPrice !== null && mafTaxPrice !== false && mafTaxPrice !== undefined) {
                  // checking maf
                  self.updateConfiguratorPrice(itemPriceInclTaxPerMonth, mafTaxPrice, product['regular_maf_with_tax'], section);
                  itemPriceInclTaxPerMonth.removeClass('hide');
                } else {
                  itemPriceInclTaxPerMonth.addClass('hide');
                  itemPriceExclTaxPerMonth.addClass('hide');
                }

                var oneTimePrice = hasSpecialPrice ? productSpecialPrice : productPrices.price;
                // avoid updating prices if null or false come on response
                if (oneTimePrice !== null && oneTimePrice !== false && oneTimePrice !== undefined) {
                  // checking price
                  self.updateConfiguratorPrice(itemPriceExclTaxOneTime, oneTimePrice, product['price'], section);
                  itemPriceExclTaxOneTime.removeClass('hide');
                } else {
                  itemPriceInclTaxOneTime.addClass('hide');
                  itemPriceExclTaxOneTime.addClass('hide');
                }

                var oneTimeTaxPrice = hasSpecialPrice ? productSpecialPrice : productPrices.price_with_tax;
                // avoid updating prices if null or false come on response
                if (oneTimeTaxPrice !== null && oneTimeTaxPrice !== false && oneTimeTaxPrice !== undefined) {
                  // checking price
                  self.updateConfiguratorPrice(itemPriceInclTaxOneTime, oneTimeTaxPrice, product['price_with_tax'], section);
                  itemPriceInclTaxOneTime.removeClass('hide');
                } else {
                  itemPriceInclTaxOneTime.addClass('hide');
                  itemPriceExclTaxOneTime.addClass('hide');
                }
              }
            });
          }
        });
      },

      /**
       * Updates frontend prices in configurator (used specially for mixmatches)
       * @param item
       * @param whatPrice
       * @param thePrice
       * @param oldPrice
       * @param section
       */
      updateConfiguratorPrice: function(
        item, thePrice, oldPrice, section) {
        if (oldPrice && oldPrice > thePrice) {
          // setting new price including and exclusive tax
          item.find('.tax-price,.normal-price').text(Handlebars.helpers.money(thePrice, {hash: {}}));
          item.find('.top').text(Handlebars.helpers.money(oldPrice, {hash: {}}));
          // only update the top div for sections different than tariffs
          if (SUBSCRIPTION_IDENTIFIERS.indexOf(section) === -1 && DEVICERC_IDENTIFIERS.indexOf(section) === -1) {
            item.find('.top').removeClass('hide');
          }
        } else {
          // setting new price
          item.find(item.is('.excl-vat-tax.one-time') ? '.normal-price' : '.tax-price').text(Handlebars.helpers.money(thePrice, {hash: {}}));

          if (!item.find('.top').hasClass('hide')) {
            // hiding top price hint
            item.find('.top').addClass('hide');
          }
        }
      },
      indexSearchInputs: function() {
        var self = this;

        // enable tags input
        $('input[data-role="tagsinput"]').tagsinput({
          itemValue: 'id',
          itemText: 'label',
          freeInput: false
        });
        $('input[data-role="tagsinput"]').off('beforeItemRemove').on('beforeItemRemove', function(event) {
          if (
            typeof event.item !== 'undefined'
            && (
              typeof event.options === 'undefined'
              || (typeof event.options !== 'undefined' && !event.options.preventAction)
            )
          ) {
            event.cancel = true;
            var section = $(event.target).parents('.conf-block').attr('id');
            self.selectFilterOption($('#' + section + ' [data-selection="' + event.item.id + '"]')[0]);
          }
        });

        var sectionFilters = $('.section-filters');
        sectionFilters.on('show.bs.dropdown', function() {
          $(this).find('i').removeClass('vfde-unfold-more').addClass('vfde-unfold-less');
        });

        sectionFilters.on('hide.bs.dropdown', function() {
          $(this).find('i').removeClass('vfde-unfold-less').addClass('vfde-unfold-more');
        });
      },
      selectFilterOption: function(object) {
        var section = $(object).parent().parent().data('section');
        var siblings = $(object).siblings('li');

        // remove other selected filter values
        $.each(siblings, function(index, element) {
          $('.search-' + section + '-tags').tagsinput(
            'remove',
            { id: $(element).data('selection'), label: $(element).text() },
            { preventAction: true }
          );
        });

        // set tag status
        if ($(object).hasClass('active')) {
          $('.search-' + section + '-tags').
            tagsinput(
              'remove',
              { id: $(object).data('selection'), label: $(object).text() },
              { preventAction: true }
            );
        }
        else {
          $('.search-' + section + '-tags').
            tagsinput(
              'add',
              { id: $(object).data('selection'), label: $(object).text() }
            );
        }

        window.configurator.filter(object);

        // Adjusting search input after tag is inserted
        const panel = $(object).closest('.panel');
        const tags = panel.find('.bootstrap-tagsinput');
        const width = tags.width();
        const searchInput = panel.find('input.search');
        if (width > 0) {
          searchInput.css('padding-left', width + 21);
        } else {
          searchInput.css('padding-left', '');
        }
      },
      toggleFilters: function(section) {
        var $filterSection = $('.section-filters[data-section="' + section + '"]'),
          $filterContainer = $('.filters-container[data-section="' + section + '"]');

        if ($filterContainer.hasClass('filters-container-hidden')) {
          $filterSection.removeClass('hidden');
          $('.dropdown-toggle').dropdown();
        } else {
          $filterSection.addClass('hidden');
        }

        $filterContainer.toggleClass('filters-container-hidden');
      },
      /**
       * opens section in configurator and returns it's previous open state
       * @param sectionType
       * @param justUpdate
       * @returns {boolean}
       */
      openSection: function (sectionType, justUpdate) {
        var self = this;
        // if section is undefined, opening first section from configurator
        sectionType = (sectionType === undefined) ? this.getConfigurator().options.sections[0] : sectionType;
        var section = $('#'+sectionType+'-block:first');
        var selectedItem = section.find('.selected-item');

        if ($.active > 0) {
          $(document).ajaxStop(function() {
            window.configurator.showToonAlles(section);
          });
        }
        else {
          window.configurator.showToonAlles(section);
        }

        if (justUpdate) {
          this.updateSectionHints(sectionType);
          return true;
        }

        // check if it already opened
        if (this.isSectionOpened(sectionType)) {
          return true;
        }

        var sections = this.getConfigurator().options.sections;
        // Hide all sections, excepting the last one
        this.getConfigurator().options.sections.forEach(function (section) {
          if (section !== sectionType) {
            if (sectionType === null) {
              if (section !== sections[sections.length - 1]) {
                self.closeSection(section);
              }
            } else {
              self.closeSection(section);
            }
          }
        });
        selectedItem.slideUp('fast', function () {
          section.find('.selection-block').removeClass('box-closed').addClass('box-opened');
          self.updateSectionHints(sectionType);
          self.configurator.filterProducts(self.configurator.products[sectionType], sectionType);
          $('#search-'+sectionType).data('forceSearch', true).trigger('keyup');
        });
        section.find('.block-header').hide();
        section.find('.block-footer').show();
        this.indexSearchInputs();
        $('.dropdown-toggle').dropdown();
        return false;
      },
      /**
       * Determines the hint text specified when section is not expanded
       * Ex:
       * If one product, show it's name
       * If more than one product, specify how many are selected
       * If no product, specify that no product is selected
       * @param sectionType
       */
      updateSectionHints: function (sectionType) {
        var section = $('#'+sectionType+'-block');
        var textHint = '';

        // If there is a product in this section, than configurator.cart will have sectionType as property
        var sectionSelectedItems = section.find('.item-row.selected');

        if (sectionSelectedItems.length === 1) {
          // Just one product selected, get it's name
          var productId = sectionSelectedItems.first().data('id').toString();
          var name = $('#item-product-' + productId + ' .item-title').text();
          textHint ='<span class="status">' + name + '</span><span class="check"></span>';
        } else if (sectionSelectedItems.length > 1) {
          // Otherwise show a message containing the number of selected products
          textHint = '<span class="status">' + Translator.translate('Multiple options selected') + '</span><span class="check"></span>';
        } else {
          // No item selected message
          textHint = '<div class="no-item-message"><span class="status">' + this.getConfigurator().getSectionItemSelectableMessage(sectionType) + '</span><span class="please-select"></span></div>';
        }

        // Update section closed
        section.find('.selected-item .content').html(textHint);
        // Update section opened
        section.find('.panel-heading .content.text-right').html(textHint);

        return this;
      },

      parseBundleData: function () {
        var self = this;
        var eligibleBundleId = null, activeBundleExists = false;
        self.getConfigurator().eligibleBundles.forEach(function (bundle) {
          // showing hint if bundle location is draw and targetedProducts is empty (otherwise it is a simulation)
          if (bundle.targetedPackagesInCart.length && bundle.addButtonInSection.indexOf(BUNDLE_LOCATION_DRAW) !== -1) {
            bundle.targetedPackagesInCart.forEach(function (targetedPackage) {
              if (self.configurator.packageId === targetedPackage.packageId) {
                eligibleBundleId = bundle.bundleId;
                // check if bundle is active
                self.getConfigurator().activeBundles.forEach(function (activeBundle) {
                  if (activeBundle.bundleId === eligibleBundleId) {
                    activeBundleExists = true;

                    return true;
                  }
                });
              }
            });
          }
        });

        return {
          'eligibleBundleId': eligibleBundleId,
          'activeBundleExists': activeBundleExists
        }
      },
      /**
       * Load configurator
       * @param configuratorData
       * @param callback
       */
      loadConfigurator: function (configuratorData, callback) {
        var root = window;

        // set configurator object first !important
        var configuratorHtml = this.generateConfiguratorContent(configuratorData);

        // the configurator container
        var configContainer = this.configurator.options.container;
        this._configurator = $(configContainer);

        // hide home wrapper
        $('#homepage-wrapper').hide();
        this._configurator.removeClass('hidden');

        // append content to configurator container
        var configuratorContent = this._configurator.children('.content').first();
        configuratorContent.html(configuratorHtml);

        var searchFields = configuratorContent.find('[data-search-on-type]');
        var searchFieldsCount = searchFields.length;
        for (var j = 0; j < searchFieldsCount; j++) {
          var section = searchFields[j].attributes['name'].value.split('-')[1];
          if (typeof self.products !== "undefined" && section && self.products[section]) {
            searchFields[j].attributes['data-documents'].value = JSON.stringify(self.products[section]);
          }
        }

        // if some callback got here, call it
        if (root.getType(callback) === 'function') {
          callback();
        }

        // init product info popover
        this.initPopover();
        this.initConfiguratorNavigation();
        $('.select-wrapper').initSelectElement();
        $('#sim-card-select').on('change', function () {
          const val = $(this).val();
          if (val === 'Gebruikt huidige simkaart') {
            window.configurator.clearSimOption(true)
          } else {
            window.configurator.addSimOption(val, true, null);
          }
        });
      },
      /**
       * Generate configurator data for each section based on a list of products given by configurator
       * @param configuratorData
       */
      generateConfiguratorContent: function (configuratorData) {
        var fragment = '', child;
        // for each section of configurator apply data to handlebars templates
        var configuratorSection;

        var templates = this.getConfigurator().templates;
        for (configuratorSection in configuratorData) {
          if (configuratorData.hasOwnProperty(configuratorSection)) {
            fragment += Handlebars.compile(templates[configuratorSection], {noEscape: true})(configuratorData[configuratorSection]);
          }
        }

        return fragment;
      },

      /**
       * Expecting hintData as object of objects {"hintOnce" : true|false, "message": string, "ruleId" : int}
       * @param hintData
       */
      showHintNotifications: function (hintData) {
        var container = $("#sales-hints-notifications");

        // clear container
        container.find('.toast-notification').remove();
        container.addClass('hidden');

        if ( !(hintData instanceof Array) ) {
          hintData = Object.keys(hintData).map(function(hint) {
            return hintData[hint];
          });
        }
        if ( hintData.length ) {
          setTimeout(function () {
            configuratorUI.processHint(hintData, 0);
          }, 500);
        }
      },

      processHint: function(hintData, index){
        var hint = hintData[index];

        var container = $("#sales-hints-notifications");
        var activePackageId = this.getConfigurator().activePackageEntityId;
        // check if hint has already been shown
        var shownHints = store.get("shownHints");
        shownHints = shownHints || {};

        container.removeClass('hidden');

        if (hint.hintOnce) {
          if (!shownHints.hasOwnProperty(activePackageId)) {
            shownHints[activePackageId] = [];
          }
          if (shownHints[activePackageId].indexOf(hint.ruleId) === -1) {
            shownHints[activePackageId].push(hint.ruleId);
            // show hint and add it to shown hints list
            container.append(this.getHintElement(hint).render());
            this.showHint(container.find('#hint-toast-' + hint.ruleId));
          }
        } else {
          container.append(this.getHintElement(hint).render());
          this.showHint(container.find('#hint-toast-' + hint.ruleId));
        }

        store.set("shownHints", shownHints);

        index++;
        if (index < hintData.length) {
          setTimeout(function () {
            configuratorUI.processHint(hintData, index);
          }, 1000);
        }
      },

      addElipsis: function(elem){
        var innerP = $(elem).find('p');
        var textHeight = $(elem).height() + 10;
        while ($(innerP).outerHeight() > textHeight) {
          $(innerP).text(function (index, text) {
            return text.replace(/\W*\s(\S)*$/, '...');
          });
        }
      },

      showHint: function(hintElement) {
        $(hintElement).css('margin', '10px 320px');
        $(hintElement).css('margin-bottom', '');
        $(hintElement).show();
        $(hintElement).find('.toast-notification-close-icon').attr("onClick", "configuratorUI.hideHint(this)");
        $(hintElement).animate({margin: "10px 0px"}, 'slow');
        var textElem = $(hintElement).find('.toast-notification-text');
        $(textElem).each(function(index){
          configuratorUI.addElipsis(this);
        });

        setTimeout(function () {
          configuratorUI.hideHint($(hintElement));
        }, 5000);
      },

      /**
       * Mark items as installed base
       * @param installedBaseCartProducts
       */
      markConfiguratorInstalledBaseProducts: function (installedBaseCartProducts) {
        var self = this;
        self.configurator.ilsCart = [];
        $.each(installedBaseCartProducts, function (productId, productAttributes) {
          self.configurator.ilsCart.push(parseInt(productId));
          var productContract = $('#item-product-'+productId).find('.item-contract');
          productContract
            .removeClass('hidden')
            .addClass('contract');
          $.each(productAttributes, function (index, value) {
            if (index === 'contract_start_date') {
              productContract
                .find('.item-contract-start-date')
                .text(Translator.translate('Contract start date')+': '+ value);
            }
          });
        });
      },

      /**
       * Mark items as installed base
       * @param installedBaseCartProducts
       */
      hardwareDetailsforInstallBase: function (hardwareDetailsIls) {
        $.each(hardwareDetailsIls, function (productId, productAttributes) {
          $('#item-info-'+productAttributes.productId).addClass('hidden');
          var title = "<table class='hardware-main-div'>" +
            "<thead>" +
              "<tr>" +
                "<th class='hardware-tooltip-title hardware-tooltip'>" + Translator.translate('Hardware type') + "</th>" +
                "<th class='hardware-tooltip-title hardware-tooltip'>" + Translator.translate('Serial number') + "</th>" +
              "</tr>" +
            "</thead>" +
            "<tbody>" +
              "<tr>" +
                "<td class='hardware-tooltip-text hardware-tooltip'>" + productAttributes.hardwareName + "</td>" +
                "<td class='hardware-tooltip-text hardware-tooltip'>" + productAttributes.serialNumber + "</td>" +
              "</tr>" +
            "</tbody>" +
          "</table>";
          $('#item-hardware-info-'+productAttributes.productId)
            .attr({
              'class': 'item-hardware-info',
              'data-template': '<div class="tooltip hardware-info"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
              'data-toggle': 'tooltip',
              'data-html': 'true',
              'data-container': 'body',
              'data-placement': 'right',
              'title': title
            })
          ;
        });
      },

      showMoveRemovedProductsModal: function(moveRemovedProducts) {
        if((null != moveRemovedProducts.serviceRules) || (null != moveRemovedProducts.productMatchRules)) {
          showValidateNotificationBar('<strong>' + Translator.translate('Something went wrong') + '!</strong>' +Translator.translate('Please contact support or check more info for full details.'),
              'warning',
              '<div class="btn" onclick="showBasketUnsyncedModal()">'+Translator.translate('More info')+'</div>');
        }
        var unsyncedBasketItemsHtml = '';
        if(moveRemovedProducts.serviceRules) {
          unsyncedBasketItemsHtml +=
            '<div class="message-content">'
              +'<span class="unsynced-subscription-name"></span>'
              +Translator.translate('Bundle not serviceable on new address')
            +'</div>';
          jQuery.each(moveRemovedProducts.serviceRules, function(key,value) {
            unsyncedBasketItemsHtml +=
              '<div class="unsynced-basket-item">'
                  +'<span class="unsynced-basket-item-icon"></span>'
                  +'<div class="unsynced-basket-item-details">'
                      +'<div class="unsynced-basket-item-name">' + value + '</div>'
                      +'<div class="unsynced-basket-item-id">Commercial ID: ' + key+'</div>'
                  +'</div>'
              +'</div>';
          });
        }
        if(moveRemovedProducts.productMatchRules) {
          unsyncedBasketItemsHtml +=
            '<div class="message-content" style="padding: 15px 0 0 0;">'
              +'<span class="unsynced-subscription-name"></span>'
              +Translator.translate('Compatibility other than serviceability')
            +'</div>';
          jQuery.each(moveRemovedProducts.productMatchRules, function(key,value) {
            unsyncedBasketItemsHtml +=
              '<div class="unsynced-basket-item">'
                  +'<span class="unsynced-basket-item-icon"></span>'
                  +'<div class="unsynced-basket-item-details">'
                      +'<div class="unsynced-basket-item-name">' + value + '</div>'
                      +'<div class="unsynced-basket-item-id">Commercial ID: ' + key +'</div>'
                  +'</div>'
              +'</div>';
          });
        }
        jQuery("#basket-unsynced-modal .message-content").html('');
        jQuery("#basket-unsynced-modal .unsynced-basket-items").html(unsyncedBasketItemsHtml);
      },

      /**
       * Expecting object {"hintOnce" : true|false, "title": string, "message": string, "ruleId" : int}
       * @param hintData
       */
      getHintElement: function (hintData) {
        // add render function to object
        hintData.render = function () {
          var title = hintData.title ? hintData.title : '';
          var message = hintData.message ? hintData.message : '';
          // prepare hint message
          return '<div id="hint-toast-' + hintData.ruleId + '" class="toast-notification toast-notification-warning clearfix">\n' +
            '        <div class="toast-notification-icon"></div>\n' +
            '        <div class="toast-notification-content col-xs-10">\n' +
            '            <div title="' + title + '" class="toast-notification-text toast-notification-title">\n' +
            '                <p>' + title + '</p>\n' +
            '            </div>\n' +
            '\n' +
            '            <div title="' + message + '" class="toast-notification-text toast-notification-description">\n' +
            '                <p>' + message + '</p>\n' +
            '            </div>\n' +
            '        </div>\n' +
            '        <div class="toast-notification-close-icon">&nbsp;</div>\n' +
            '    </div>';
        };

        return hintData;
      },

      /**
       * Hide message by message from container
       * If message list is empty, clear all messages
       * @param closeElement
       */
      hideHint: function (closeElement) {
        var container = $("#sales-hints-notifications");
        var toastElement = $(closeElement).closest('.toast-notification');

        $(toastElement).fadeOut('slow', function () {
          $(this).remove();
        });

        if (!container.find('.toast-notification').length) {
          // container.removeClass("notification-bar-error");
          container.addClass('hidden');
        }
      },

      switchBundle: function (element, bundleId, customerNumber, productId, interStackBundle, packageId, stack, processContext) {
        var self = this;
        var containerAlternatives = $('.bundle-alternatives-packages');
        var containerHeaderAlternatives = $('.bundle-alternatives-header');

        var bundleInput = $('#bundleModal .bundle-to-cart-btn input[type=submit]');
        bundleInput.attr('data-bundle-id', bundleId).attr('data-product-id', productId);
        bundleInput.attr('data-entity-id', $(element).data('entity-id'));
        bundleInput.attr('data-contract-id', customerNumber);

        if (!interStackBundle) {
          bundleInput.attr('data-subscription-number', customerNumber);
        } else {
          bundleInput.attr('data-inter-stack-package-id', packageId);
        }

        var bundleContainer = $(".bundle-details-header").find("[data-bundle-ui-section-id='" + $(containerAlternatives).data('bundle-ui-section-id') + "']")[0];
        var selectedPackage = $(element).closest('.bundle-alternative-product');
        //put text into bundle package holder
        $(bundleContainer).find('.orderline-text').html($(selectedPackage).find('.orderline-text').html());
        $(bundleContainer).find('.orderline-text-description').html($(selectedPackage).find('.orderline-text-description').html());
        var bundleIcon = $(bundleContainer).find('.stack-identificator');
        bundleIcon.removeClass().addClass($(selectedPackage).find('.stack-identificator').attr('class'));

        if (packageId && processContext === 'ACQ') {
          bundleIcon.addClass('new_indicator');
        }
        //make active bundle package holder
        $(bundleContainer).find('.bundle-package-block-body').removeClass('bordered');

        //enable buttons
        bundleInput.removeClass('disabled').prop("disabled", false);
        $('.info-btn-bundle').removeClass('disabled');
        //hide modal
        $('#bundleModalAlternatives').modal('hide');

        $('#bundle-details-packages-list').find('.target-package').attr('data-stack', stack.toLowerCase());

        self.updateBundleInformation(bundleId, customerNumber, productId, $(element).data('entity-id'), packageId);
      },

      updateBundleInformation: function (bundleId, customerNumber, productId, entityId, interStackBundlePackageId) {
        var self = this;
        var bundleInfoContainer = $('#bundle-details-packages-list');
        self.configurator.showBundleBarInformationToaster();

        $.ajax({
          url: "bundles/index/getAdditionalBundleInfo",
          type: "POST",
          data: {
            'bundle_id': bundleId,
            'customer_number': customerNumber,
            'product_id': productId,
            'entity_id': entityId,
            'inter_stack_package_id': interStackBundlePackageId
          }
        }).done(function (response) {
          if (response.hasOwnProperty('bundleProducts') && response.bundleProducts) {
            $.each(response.bundleProducts, function (packageType, sectionData) {
              var sectionBlock = $(bundleInfoContainer).find('[data-stack="' + packageType + '"]');
              var detailsBlock = $(bundleInfoContainer).find('.bundle-details');
              var packageNameElement = $(sectionBlock).find('h4').text('');
              var productListElement = $(sectionBlock).find('ul').text('');
              var bundleNameElement = $(detailsBlock).find('h2').text('');
              var bundleDetailsElement = $(detailsBlock).find('.bundle-description > p').text('');
              var oldPriceElement = $(sectionBlock).find('p.old-price').text('');
              var newPriceElement = $(sectionBlock).find('h2.new-price').text('');

              var packageName = response.packageNames && response.packageNames[packageType] ? response.packageNames[packageType] : packageType;
              var bundleName = response.bundleName ? response.bundleName : '';
              var bundleDetails = response.bundleDescription ? response.bundleDescription : '';

              $(packageNameElement).text(packageName);

              $(bundleNameElement).text(bundleName);
              self.ellipsizeTextBox('bundle-details-title');

              $(bundleDetailsElement).text(bundleDetails);
              self.ellipsizeTextBox('bundle-description');

              var listHtml = '';
              $.each(sectionData, function (infoKey, info) {
                var liClass, iconClass, infoClass = null;
                if (info['type'] === 'add') {
                  liClass = 'bundle-added';
                  iconClass = 'vfde-verified';
                } else if (info['type'] === 'remove') {
                  liClass = 'bundle-removed';
                  iconClass = 'vfde-clear';
                }
                if (info['choose'] === 1) {
                  infoClass = 'info-indicator';
                }

                if (infoClass) {
                  listHtml += '<li class="' + liClass + '"><div class="additional-bundle-info-icon"><i class="vfde ' + iconClass + '"></i></div>' +
                      '<div class="additional-bundle-info-description"><p>' + info['name'] + (info['discountFormatted'] ? ' (' + info['discountFormatted'] + ')' : '') + '</p>' +
                      '<i class="' + infoClass + '" data-toggle="tooltip" data-placement="top" data-container="body" data-html="true" ' +
                      'data-template=\'<div class="tooltip ils-tooltip" style="width:96px"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>\' ' +
                      'data-original-title="<p>' + Translator.translate('This Promotion can be changed after the bundle has been created') + '</p>"></i></div></li>';
                } else {
                  listHtml += '<li class="' + liClass + '"><div class="additional-bundle-info-icon"><i class="vfde ' + iconClass + '"></i></div>' +
                      '<div class="additional-bundle-info-description"><p>' + info['name'] + (info['discountFormatted'] ? ' (' + info['discountFormatted'] + ')' : '') + '</p></div></li>';
                }
              });
              $(productListElement).append($(listHtml));

              if($(listHtml).find('.info-indicator').length > 0) {
                $('.info-indicator').tooltip();
              }

              if (response.hasOwnProperty('totals') && response.totals[packageType]) {
                var prices = response.totals[packageType];
                $(oldPriceElement).append('&euro; ' + prices['oldPrice']);
                $(newPriceElement).append('&euro; ' + prices['newPrice'] + ' <span class="small-text">per-month</span>');
              }
            });
          }
        });
      },

      hideBundleToaster: function () {
        if ($('.configurator-navigation').length && $('.configurator-navigation').attr('style').length) {
          $('.configurator-navigation').removeAttr('style');
        }
        var bundleDrawer = $('#bundleModal');
        bundleDrawer.modal('hide');

        this.getConfigurator().assureElementsHeight();
        $('#message-dialog-warning').removeClass('message-warning-enabled');

        // move down the notification toaster when bundle is displayed
        $('.toast-notifications-container').css('margin-bottom', '');


        return this;
      },

      ellipsizeTextBox: function(id) {
        var el = document.getElementById(id);
        var wordArray = el.innerHTML.split(' ');
        while(el.scrollHeight > el.offsetHeight) {
          wordArray.pop();
          el.innerHTML = wordArray.join(' ') + '...';
        }
      },

      initConfiguratorNavigation: function () {

        const scrollSectionIntoView = function(section) {
          const changingOrder = $('#config-wrapper').find('.change-order-notification').length;
          const position = section.offsetTop;
          const offset = changingOrder ? 130 : 20;
          $('#config-wrapper').scrollTop(position - offset);
        }

        $('.configurator-navigation .configurator-navigation-menu').html('<ul></ul>');

        $('.conf-block').each(function() {
          const block = this;
          const sectionTitle = $(block).find('.panel-title').text().trim().match(/[^*]*/i)[0];
          if (sectionTitle.length && sectionTitle !== 'Toestelbetaling') {
            $('.configurator-navigation .configurator-navigation-menu ul')
              .append(
                $('<li><span class="configurator-navigation-icon ' + sectionTitle.toLowerCase().replace(/\s/g, '') +'"></span>' + sectionTitle +'</li>').click(function() {
                  scrollSectionIntoView(block);
                })
              );
          }
        });

        $('.configurator-navigation .configurator-navigation-toggle').off('click').on('click', function() {
          $(this).parent().toggleClass('open');
        });
      },
    });

  $('#config-wrapper').on('scroll', function() {
    const notification = $('.change-order-notification');
    if (notification) {
      const scrollTop = $(this).scrollTop();
      if (scrollTop) {
        notification.css('border-bottom', '1px solid #ececec');
      } else {
        notification.css('border-bottom', '');
      }
    }
  });
})(jQuery);
