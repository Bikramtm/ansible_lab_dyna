/*
 * Copyright (c) 2018. Dynacommerce B.V.
 */
(function($) {
  var root = window;
  var automatically_refresh_selection = false;
  var isIE = /Trident|MSIE/.test(navigator.userAgent);

  window.Configurator.Base.prototype = $.extend(Configurator.Base.prototype, {
    clearFilters: function (element) {
      var el = $(element);
      var self = this;
      self.options.type = $('<div/>').html(self.options.type).text();
      self.websiteId = $('<div/>').html(self.websiteId).text();
      self.options.identifier = $('<div/>').html(self.options.identifier).text();
      self.options.packageId = $('<div/>').html(self.options.packageId).text();

      var type = $('<div/>').html(el.parents('.conf-block').data('type')).text();
      var section = $('<div/>').html(el.parent().parent().data('section')).text();
      this.filterStack[type] = {};
      self.filterValues[type] = [];
      this.filterProducts(self.products[type], type);
      var searchOnType = $('<div/>').html(el.parents('.conf-block').find('.search-on-type')).text();
      searchOnType.val('');
      el.parents('.conf-block').find('.section-filters li').removeClass('active');
      $('.search-' + section + '-tags').tagsinput('removeAll');
      searchOnType.trigger('keyup');
      el.addClass('hidden');
    },
    filterProducts: function(productsInitial, type) {
      if (typeof productsInitial === 'undefined') {
        return null;
      }

      var searchOnType = root.getType(type) === 'object' ? $(type) : false;
      type = root.getType(type) === 'object' ? $(type).parents('.conf-block').data('type') : type;
      var products = this.filterCollection(productsInitial, type);
      var self = this;
      self.options.type = $('<div/>').html(self.options.type).text();
      self.websiteId = $('<div/>').html(self.websiteId).text();
      self.options.identifier = $('<div/>').html(self.options.identifier).text();
      self.options.packageId = $('<div/>').html(self.options.packageId).text();

      var families = $('.family');
      var familiesToRemove = [];
      var displayOnlyMandatory = false;
      if (self.filterStack[type] != undefined && self.filterStack[type]['mandatory'] != undefined) {
        switch (self.filterStack[type]['mandatory']) {
          case 0:
            if (families.length > 0) {
              $.each(families, function(index, value) {
                var hidden = $(value).find('.family-header .family-status .hidden');
                if (hidden.hasClass('mandatory')) {
                  self.displayFamily(value);
                } else {
                  $(value).css('display', 'none');
                  familiesToRemove.push('' + $(value).data('family-id'));
                }
              });
            }
            break;
          case 3:
            if (families.length > 0) {
              $.each(families, function(index, valueTmp) {
                var hidden = $(valueTmp).find('.family-header .family-status .hidden');
                if (hidden.hasClass('mandatory')) {
                  $(valueTmp).css('display', 'none');
                  familiesToRemove.push('' + $(valueTmp).data('family-id'));
                } else {
                  self.displayFamily(valueTmp);
                }
              });
            }
            displayOnlyMandatory = true;
            break;
        }
      }

      var productsNew = [];
      for (var j = 0; j < products.length; j++) {
        if (!familiesToRemove.length || !products[j].hasOwnProperty('families') || !products[j]['families'].filter(function(n) {
            return familiesToRemove.indexOf(n) !== -1;
          }).length) {
          if (displayOnlyMandatory && (!products[j].hasOwnProperty('families') || !products[j]['families'].length)) {
            continue;
          }
          productsNew.push(products[j]);
        }
      }

      this.updateFilterStatus(productsNew, type);
      this.updateSelectedItemMessage(productsNew, type);

      var productsIds = $.map(productsNew, function(n, i) {
        return n ? n['entity_id'] : null;
      });

      var block = $('#'+type+'-block');
      block.find('.item-row').each(function() {
        // Avoid hiding discount items
        if (!$(this).data('disabled') && !$(this).hasClass('package-item')) {
          if (-1 === productsIds.indexOf($.trim($(this).data('id'))) && !$(this).hasClass('discount-item')) {
            $(this)[0].style.display = 'none';
          } else {
            $(this)[0].style.display = 'block';
            if (
              self.temporaryAllowedProducts &&
              self.temporaryAllowedProducts.length &&
              (-1 !== self.temporaryAllowedProducts.indexOf($.trim('' + $(this).data('id')))) &&
              ((self.allowedProducts && self.allowedProducts.length && (-1 === self.allowedProducts.indexOf($.trim('' + $(this).data('id'))))) || (!self.allowedProducts || !self.allowedProducts.length))
            ) {
              $(this).addClass('incompatible-row');
            } else {
              $(this).removeClass('incompatible-row');
            }
          }
        }
      });

      block.find('.removal-not-allowed').removeClass('mandatory-row removal-not-allowed');
      this.removalNotAllowed.forEach(function(productId) {
        $('#item-product-' + productId).addClass('mandatory-row removal-not-allowed');
      }.bind(this));
      this.selectableNotAllowedIdsFromContract.forEach(function(productId) {
        $('#item-product-' + productId).removeClass('selected multi-option').addClass('not-allowed');
        $('#item-product-' + productId).find('div').addClass('line-through');
      }.bind(this));

      // Check if the Clear Filters button should be shown
      if (searchOnType) {
        this.showClearFilters(searchOnType, type);
      }
      if (families.length > 0) {
        $.each(families, function(index, value) {
          self.displayFamily(value);
        });
      }
      self.hideNotSelectedProducts();
    },
    filter: function(element, permanent) {
      var self = this;
      var el = $(element);
      var value = el.data('selection');
      var property = el.parents('.section-filters').data('filter-for');
      var type = el.parents('.conf-block').data('type');
      var searchOnType = el.parents('.conf-block').find('.search-on-type');

      if (typeof this.filterStack[type] === 'undefined') {
        this.filterStack[type] = {};
      }

      if (typeof this.filterValues[type] === 'undefined') {
        this.filterValues[type] = [];
      }

      if (el.hasClass('active')) {
        el.removeClass('active');
        delete self.filterStack[type][property];
        self.filterValues[type].splice($.inArray(el.text(), self.filterValues[type]), 1);
      } else {
        el.parent().find('li').each(function () {
          $(this).removeClass('active');
          self.filterValues[type].splice($.inArray(el.text(), self.filterValues[type]), 1);
        });
        el.addClass('active');
        self.filterStack[type][property] = value;
        self.filterValues[type].push(el.text());

        if (permanent) {
          if (!self.permanentFilterStack.hasOwnProperty(type)) {
            self.permanentFilterStack[type] = {};
          }
          self.permanentFilterStack[type][property] = value;

          if (!self.permanentFilterValues.hasOwnProperty(type)) {
            self.permanentFilterValues[type] = [];
          }
          self.permanentFilterValues[type].push(el.text());
        }
      }

      // Check if the Clear Filters button should be shown
      this.showClearFilters(searchOnType, type);

      self.filterValues[type] = $.map(self.filterValues[type].unique(), function (n, i) {
        return n && n.length ? n : null;
      });
      if (permanent) {
        self.permanentFilterValues[type] = $.map(self.permanentFilterValues[type].unique(), function (n, i) {
          return n && n.length ? n : null;
        });
      }
      this.filterProducts(self.products[type], type);

      searchOnType.trigger('keyup');
    },
    filterCollection: function(products, type) {
      var self = this;
      var filtered = [];
      var hasFilters = false;
      var productsLength = products.length;
      for (var i = 0; i < productsLength; i++) {
        var valid = true;
        if (self.permanentFilterStack.hasOwnProperty(type)) {
          $.extend(self.filterStack[type], self.permanentFilterStack[type]);
        }
        if (self.filterStack[type]) {
          for (var property in self.filterStack[type]) {
            if (property == 'mandatory') {
              continue;
            }
            if (self.filterStack[type].hasOwnProperty(property)) {
              hasFilters = true;
              var getType = {};
              // if attribute's product values are multiple - coming from a multiselect
              if ((products[i][property] !== undefined) && (products[i][property] !== null)) {
                // convert the string to array
                products[i][property] = (products[i][property].indexOf(',') > -1) ? products[i][property].split(',') : products[i][property];
              }
              if (products[i][property] && getType.toString.call(products[i][property]) === '[object Array]') {
                valid = (products[i][property].indexOf("" + self.filterStack[type][property]) >= 0);
              } else if (products[i][property] != self.filterStack[type][property]) {
                valid = false;
              }
              if (!valid) {
                break;
              }
            }
          }
        }
        valid ? filtered.push(products[i]) : valid;
      }
      if ((self.allowedProducts && self.allowedProducts.length) || (self.temporaryAllowedProducts && self.temporaryAllowedProducts.length)) {
        var allowed = self.allowedProducts && self.allowedProducts.length;
        var temporary = self.temporaryAllowedProducts && self.temporaryAllowedProducts.length;
        var filteredProducts = hasFilters ? filtered : products;
        var filteredLength = filteredProducts.length;
        filtered = [];
        hasFilters = true;
        var j, skip;
        for (j = 0; j < filteredLength; j++) {
          skip = false;
          if (allowed) {
            if (-1 !== self.allowedProducts.indexOf("" + filteredProducts[j]['entity_id'])) {
              filtered.push(filteredProducts[j]);
              skip = true;
            }
          }
          if (temporary && !skip) {
            if (-1 !== self.temporaryAllowedProducts.indexOf("" + filteredProducts[j]['entity_id'])) {
              filtered.push(filteredProducts[j]);
            }
          }
        }
      } else {
        // no rules available for current selection => return only the products currently in cart
        var current_prods = self.getCartItems();
        products = products.filter(function (prod) {
          return $.inArray(
            parseInt(prod['entity_id']),
            current_prods
          ) != -1
        });

        //allowed products is empty and cart is empty filters must not return products
        if (current_prods.length === 0) {
            filtered = [];
        }
      }

      return hasFilters ? filtered : products;
    },
    updateFilterStatus: function (products, type) {
      var block = $('#'+type+'-block');
      var statusText = block.find('.filter-status span');
      var element = block.find('.filter-input input');
      var searchString = $.trim(element.val());
      var searchFilters = this.filterValues[type] ? this.filterValues[type] : [];
      searchFilters = $.map(searchFilters.unique(), function (n, i) {
        return n && n.length ? n : null;
      });
      //disabled for now, if required we reuse
      // if (statusText.length) {
      //   var productsLength = 0;
      //   for (var i in products) {
      //       if (products[i].product_visibility === true) {
      //           productsLength += 1;
      //       }
      //   }
      //   statusText.html(statusText.data('message').format(productsLength)).show();
      // }
    },
    increaseQueueSize: function () {
      ++this.ajaxQueueSize;
      $('#cart-spinner').removeClass('hide');
      $('.col-right.sidebar').find('> button.enlarge').hide();
      $('.cart_packages')
          .css('overflow', 'hidden')
          .scrollTop(0);
    },
    decreaseQueueSize: function () {
      --this.ajaxQueueSize;
      if (this.ajaxQueueSize <= 0) {
        this.ajaxQueueSize = 0;
        $('#cart-spinner').addClass('hide');
        $('.col-right.sidebar > button.enlarge').show();
        var cartPackages = $('.cart_packages');

        var activePackageBlock = cartPackages.find('[data-package-id={0}]'.format(this.packageId));
        if (activePackageBlock.length > 0) {
            var scrollTo = activePackageBlock.offset().top - cartPackages.offset().top + cartPackages.scrollTop();
            cartPackages.scrollTop(scrollTo);
            cartPackages.css('overflow', 'auto');
        }
      }
    },
    /**
     * Get eligible and active bundles from backend
     * @returns {Base}
     */
    getEligibleBundles: function () {
      var self = this;

      //if no customer is loaded, only trigger the call for the second package onward
      if(jQuery('.customer-info').data('id') == null){
        if(jQuery('[data-package-id]').length < 2){
          return this;
        }
      }
      else{
        //if customer is loaded and we do have a package, make sure it's not ILS/MOVE
        if(jQuery('[data-package-id]').length < 2 && this.ilsCart.length){
          return;
        }
      }


      // get bundles after rules to forward the available tarrifs instead of simulating all cart tariffs
      var tariffIdentifier;
      for (var section in self.products) {
        if (self.products.hasOwnProperty(section) && (TYPE_TARIFFS.indexOf(section) !== -1 || FIXED_SUBTYPE_SUBSCRIPTION === section)) {
            tariffIdentifier = section;
            break;
        }
      }
      var subscriptions = self.products.tariff;
      if (typeof self.products.pakket !== "undefined") {
        subscriptions = self.products.pakket;
      }

      var allowedTariffs = subscriptions.map(function (tariff) {
        return tariff.entity_id;
      }).filter(function(productId) {
        return self.allowedProducts.indexOf(productId) > -1;
      });

      if (!allowedTariffs && $('#config-wrapper .item-row.selected').not('.multi-option').length === 1) {
          allowedTariffs.push(parseInt($('#config-wrapper .item-row.selected').not('.multi-option').attr('data-id')))
      }

      this.http.ajax('POST', 'cart.getBundles', {tariffs: allowedTariffs}, false).done(function (data) {self.updateBundleData(data);});

      return this;
    },

    addMultiToCart: function (products, section, callback, async, refreshConfigurator) {
      startPiwikCall(MAIN_URL + "configurator/cart/addMulti");
      var self = this;

      if (!refreshConfigurator) {
        // show spinner in cart separate
        window.setPageLoadingState && window.setPageLoadingState(true);
        window.setPageOverrideLoading(true);
        // increase size of Queue on click
        self.increaseQueueSize();
      }

      // Disable proceed button during spinner
      var nextButton = $('#cart_totals input[type="button"]');
      // Preserving initial button state
      var proceedButtonDisabled = nextButton.attr('disabled');
      nextButton.attr('disabled', true);

      // addMulti call
      self.http.ajaxQueue('POST', 'cart.addMulti', {
        'products': products,
        'type': self.options.type,
        'section': section,
        'simType': self.simType,
        'oldSim': self.options.old_sim,
        'packageId': self.packageId,
        'clickedItemId': self.lastAddedItem.data('id'),
        'clickedItemSelected': self.lastAddedItem.hasClass('selected'),
        'addressData': JSON.stringify({
          'serviceability': JSON.parse(store.get('serviceApiResponse')),
          'validate': JSON.parse(store.get('validateApiResponse'))
        }),
        'isOffer': window.isOffer
      }, async)
      .done(function (data) {
        window.isOffer = 0;
        callback(data);

        if (section == 'device' || section == 'tariff') {
          if (data.drc_eligible) {
            $('#devicerc-block').show();
            $('.group-header-DeviceRC').show();
          } else {
            $('#devicerc-block').hide();
            $('.group-header-DeviceRC').hide();
          }
        }

        if(data.allowedSimcards) {
          var simcardElement = $('#sim-card-select');
          simcardElement
            .find('option')
            .remove();

          simcardElement.append($("<option></option>"));

          if(data.processContextCode == 'RETBLU') {
            simcardElement
              .append($("<option></option>")
              .attr("value",Translator.translate('Using current SIM'))
              .attr('selected','selected')
              .text(Translator.translate('Using current SIM')));
          }
          $.each(data.allowedSimcards,function(id,value) {
            simcardElement
              .append($("<option></option>")
              .attr("value",value)
              .attr("data-code",id)
              .text(value));
          });
          simcardElement.initSelectElement();
        }

        // no further calls are needed as the configurator gets refreshed
        if (refreshConfigurator) {
          return true;
        }

        // Update left side sections if they are complete or not
        var sections;
        if (data.hasOwnProperty('incomplete_sections')) {
          sections = data['incomplete_sections'];
        } else {
          sections = [];
        }
        // If the addMulti call fulfilled the bundle choice requirements, remove the errors
        if (data.hasOwnProperty('removeBundleErrorNotice')) {
          $('.bundle-incomplete').removeClass('bundle-incomplete');
          hideNotificationBar();

        }
        // If the addMulti call fulfilled the bundle choice requirements, remove the errors
        if (data.hasOwnProperty('removeBundleChoiceBlock')) {
          $('#bundle-options').html("");
        }
        self.showWarnings(sections);

        if (data.hasOwnProperty("hintMessages") && Object.keys(data.hintMessages).length !== 0) {
          configuratorUI.showHintNotifications(data.hintMessages);
        } else {
          $("#notification-bar").removeClass('notification-bar-error');
        }

        if (data.clearConfigurator) {
          if (window['configurator']) {
            window['configurator'].destroy();

            $(".side-cart.block[data-package-id='" + data.activePackageId + "']").removeClass('active');
            refreshConfigurator = !refreshConfigurator;

            // hide page loader
            window.setPageLoadingState && window.setPageLoadingState(false);
            window.setPageOverrideLoading(false);
            updateCartPackageLine();
            self.decreaseQueueSize();
          }
        }

        if (data.hasOwnProperty('packageEntityId')) {
          self.activePackageEntityId = data.packageEntityId;
        } else {
          self.activePackageEntityId = null;
        }

        if (typeof self.simType != 'undefined') {
          $('#sim-card-select option[value="' + self.simType + '"]').attr('selected','selected');
        }

        updateCartPackageLine();
      }).then(function () {
        // no further calls are needed as the configurator gets refreshed
        if (!refreshConfigurator) {
          // Will trigger getRules call (if not already cached at browser storage level)
          self.reapplyRules(0, function () {
            if (self.products.tariff || self.products.pakket) {
              self.getEligibleBundles();
            }
          }.bind(self));
          // getPrices call (also, if not already cached at browser storage level)
          self.getPrices();

          self.decreaseQueueSize();
        }

        endPiwikCall(MAIN_URL + "configurator/cart/addMulti");

        if (!proceedButtonDisabled) {
          nextButton.removeAttr('disabled');
        }
      });
    },
    /**
     * $_modalDialog.removeClass('middle-state');
     $_modalDialog.addClass('first-state');
     */
    showBundleBarInformationToaster: function () {
      var self = this;
      var $_modalDialog = $('#bundleModal');

      if ($('#bundleModal .bundle-to-cart-btn input[type=submit]').hasClass('disabled')) {
        $('.info-btn-bundle').addClass('disabled');
      } else {
        $('.info-btn-bundle').removeClass('disabled');
      }

      // move down the notification toaster when bundle is displayed
      jQuery('.toast-notifications-container').css('margin-bottom', '55px');

      $_modalDialog.removeClass('middle-state');
      $_modalDialog.addClass('first-state');
      $('.info-btn-bundle.colapse-drawer').hide();
      $('.info-btn-bundle.expand-drawer').show();

      if ($('.configurator-navigation').length) {
        $('.configurator-navigation').css("bottom","65px");
      }

      self.assureElementsHeight();
    },
    closeBundleDetailsModal: function() {
      $('#bundleModal').modal('hide');
      if ($('.configurator-navigation').length) {
        $('.configurator-navigation').removeAttr('style');
      }
    },
    showBundleBarMainDetailsToaster: function () {
      var $_modalDialog = $('#bundleModal');
      $('.info-btn-bundle.expand-drawer').hide();
      $('.info-btn-bundle.colapse-drawer').show();

      // move up the notification toaster when bundle is displayed
      jQuery('.toast-notifications-container').css('margin-bottom', '280px');

      $_modalDialog.removeClass('first-state');
      $_modalDialog.addClass('middle-state');
    },
    showToonAlles: function (section) {
      var self = this,
        sectionName = section.data('type'),
        elementShow = section.find('.show-incompatible'),
        elementHide = section.find('.hide-incompatible'),
        doShow = false;

      // Check if the section has the toon alles button
      if (elementShow.length) {
        // Get all the hidden elements
        var hiddenElements = section.find('.product-table .item-row[style$="display: none;"]');
        // Filter hidden elements to make sure whey were not hidden by the filters
        hiddenElements = $.grep(hiddenElements, function (el) {
          return self.allowedProducts.indexOf("" + $(el).data('id')) === -1;
        });

        //if (hiddenElements.length) {
          // When section is device only show the button if there are aikido subscriptions in the cart
          if (sectionName === 'device' && self.products.hasOwnProperty('tariff')) {
            var ids = self.getPackageProductIds();
            self.products.tariff.each(function (el) {
              if (!doShow && ids.indexOf(parseInt(el.entity_id)) !== -1 && parseInt(el.aikido) === 1) {
                doShow = true;
              }
            });
          } else {
            doShow = true;
          }
        //}
      }

      if (doShow) {
        elementShow.removeClass('hidden');
        elementHide.addClass('hidden');
      } else {
        elementShow.addClass('hidden');
      }

      self.resizeToonAlles(section);
    },
    activateDisabledItems: function (section, showPrices) {
      showPrices = showPrices != undefined ? showPrices : false;
      // show spinner in cart separate
      window.setPageLoadingState && window.setPageLoadingState(true);
      window.setPageOverrideLoading(true);

      var items = [];
      var self = this;
      self.temporaryAllowedProducts = [];
      // Display all the disabled items from the section
      section = $(section).parents('.conf-block');
      rulesCallback = function (result) {
        if (result.error) {
          showModalError(result.message);
          items = [-1];
        } else {
          items = JSON.parse(result.ids);
        }

        section.find('.item-row').each(function (id, el) {
          el = $(el);
          if (items.length === 0 || jQuery.inArray("" + el.data('id'), items) !== -1) {
            if (el.css("display") ==="none" && jQuery.inArray("" + el.data('id'), self.availableProducts) !== -1) {
              el.css("display", 'block');
              el.addClass('incompatible-row');
              self.temporaryAllowedProducts.push("" + el.data('id'));
              if (!showPrices) {
                el.find('.item-pricing').addClass('hidden');
              }
            }
          }
        });
        section.find('input.search-on-type').trigger('change');

        window.setPageLoadingState && window.setPageLoadingState(false);

      };
      var selectedItems = [];
      // Send all the selected items for cache purposes
      $('#config-wrapper .item-row.selected').each(function () {
        selectedItems.push($(this).data('id'));
      });
      selectedItems.sort(function (l, r) {
        return r < l;
      });
      self.increaseQueueSize();
      this.http.ajaxQueue('GET', 'cart.get' + section.data('type') + 'Rules', {
        "ids": selectedItems,
        "website_id": self.websiteId
      }, true)
      .done(function (data) {
        rulesCallback(data);
        self.decreaseQueueSize();
        // hide page loader
        window.setPageLoadingState && window.setPageLoadingState(false);
        window.setPageOverrideLoading(false);
      });
      section.find('.hide-incompatible').removeClass('hidden');
      section.find('.show-incompatible').addClass('hidden');
    },
    resizeToonAlles: function(section) {
      var $searchInputContainer = section.find('.search-on-type').parent();
      if (
        (!section.find('.hide-incompatible').length || section.find('.hide-incompatible').hasClass('hidden')) &&
        (!section.find('.show-incompatible').length || section.find('.show-incompatible').hasClass('hidden'))
      ) {
        $searchInputContainer.removeClass('show-all');
      } else {
        $searchInputContainer.addClass('show-all');
      }
    },
    applyMandatory: function () {
            var self = this;
            $('.conf-block .family .family-status .optional').removeClass('hidden');
            $('.conf-block .family .family-status .mandatory').addClass('hidden');
            if (self.mandatory != undefined) {
                $.each(self.mandatory, function (index, value) {
                    var status = $('.conf-block .family[data-family-id="' + value + '"] .family-status');
                    status.find('.optional').addClass('hidden');
                    status.find('.mandatory').removeClass('hidden');
                });
            }
        },
    reapplyRules: function (disableCache, callBack) {
      if (typeof disableCache === 'undefined') { disableCache = 0; }
      var self = this;
      // get ordered products ids
      var cartProducts = self.getPackageProductIds();
      var rulesCallback = function (data) {
        setPageLoadingState(false);
        if (data.error) {
          showModalError(data.message, 'Compatibility Rules error');
          return;
        }

        // todo: check if data['rules'] needs to be active to all packages
        if ($.inArray(self.options.type, window.allPackages) > -1) {
          self.allowedProducts = data['rules'];
        } else {
          self.allowedProducts = self.getPackageProductIds().length ? data['rules'] : root.allowedProducts;
        }
        if ($('#is_order_edit_mode').val() == 1) {
          self.mandatory = [];
        } else {
          self.mandatory = self.getPackageProductIds().length ? data['mandatory'] : [];
        }

        self.removalNotAllowed = data.hasOwnProperty('removalNotAllowed') ? data.removalNotAllowed : [];
        self.selectableNotAllowedIdsFromContract = data.hasOwnProperty('selectableNotAllowedIdsFromContract') ? data.selectableNotAllowedIdsFromContract : [];

        // Demo only
        if (data['disabled'] != undefined) {
          $('.product-table .item-row').removeClass('disabled-row');
          $.each(data.disabled, function (index, pid) {
            $('.product-table [data-id="{0}"]'.format(pid)).addClass('disabled-row');
          });
        }

        if (data.hasOwnProperty('invalid')) {
          self.invalidProducts = data.invalid;
        }

        self.applyMandatory();
        self.applyInvalid();
        if ($.inArray(self.options.type, window.CABLE_PACKAGES) > -1) {
          self.applyNonStandardRates();
        }
        if (typeof callBack === "function") {
          callBack();
        }

        if (typeof self.products !== 'undefined') {
          var block = $('.selection-block.box-opened');
          if (block.length === 0) {
            block = $('.selection-block');
          }
          var section = block.first().parent().data('type');
          self.filterProducts(self.products[section], section);
          $('#search-'+section).data('forceSearch', true).trigger('keyup');
        }
      };

      self.KIP = $('<div/>').html($('#serviceability-content [data-technology="KIP"]').data('downstream')).text();
      self.DSL = $('<div/>').html($('#serviceability-content [data-technology="(V)DSL"]').data('downstream')).text();

      var customerNumber = $('<div/>').html(customerDe.customerNumber).text();

      var addressData = JSON.stringify({
        'serviceability': JSON.parse(store.get('serviceApiResponse')),
        'validate': JSON.parse(store.get('validateApiResponse'))
      });

      var postData = {
        'websiteId': self.websiteId,
        'type': self.options.type,
        'kip': self.KIP,
        'dsl': self.DSL,
        'identifier': self.options.identifier,
        'packageId': self.options.packageId,
        'customer_number': customerNumber,
        'hash': root.quoteHash ? root.quoteHash : QUOTE_HASH,
        'addressData': addressData
      };
      if(disableCache) {
        postData.bypassCache = Math.random();
      }
      if (cartProducts.length > 0) {
        postData.products = cartProducts.join(',');
      }
      if (self.ilsCart.length > 0) {
        postData.ilsProducts = self.ilsCart.join(',');
      }

      this.http.post('cart.getRules', postData, rulesCallback, null, false);
    },
    selectProduct: function (item, makeCall, externalCallBack, refreshConfigurator) {
      var self = this;
      makeCall = undefined !== makeCall ? makeCall : true;
      item = $(item);
      if (item.hasClass('not-allowed')) {
        return false;
      }

      var productId = item.data('id');
      var itemDeselection = item.hasClass('selected');

      var section = this.getProductType(productId);
      var handleMethod = 'select{0}'.format(section.charAt(0).toUpperCase() + section.slice(1));

      if (self.showSusoModal === true && self.itemBreaksSusoBundle(productId)) {
        var susoWarningModal = jQuery('#break-suso-after-deselecting-modal');
        self.susoRelatedItem = item;
        susoWarningModal.modal();
        return;
      }

      if (itemDeselection && self.itemIsRedPlusRelatedProduct(productId) && self.showRedPlusModal === true) {
        var redPlusWarningModal = jQuery('#break-redplus-after-deselecting-modal');
        self.redPlusRelatedItem = item;
        redPlusWarningModal.modal();
        return;
      }

      if (self.bundleComponents.length && self.productBreaksBundle(productId) && self.showBundleBreakingModal === true && self.showSusoModal === true && self.showRedPlusModal === true) {
        var bundleComponentModal = jQuery('#cancel-bundle-modal');
        self.bundleComponentItem = item;
        bundleComponentModal.modal();
        return;
      }

      // if current clicked item is not the selected one, search for current selected item to identify if clicking another tariff will break the bundle
      var currentSelectedItem = false;
      if (!itemDeselection) {
        if(item.closest('.product-table').find('[id^=item-product] .selected').length){
          currentSelectedItem = item.closest('.product-table').find('[id^=item-product] .selected').first().data('id');
        }

        if (currentSelectedItem &&
          currentSelectedItem != productId &&
          self.productBreaksBundle(currentSelectedItem) &&
          self.showBundleBreakingModal === true &&
          self.showSusoModal === true &&
          self.showRedPlusModal === true) {
          var bundleComponentModal = jQuery('#cancel-bundle-modal');
          self.bundleComponentItem = $('#item-product-'+productId);
          bundleComponentModal.modal();
          return;
        }
      }

      /** Do nothing if item is not changeable **/
      if (item.hasClass('mandatory-row')) {
        return;
      }

      // Hide all the disabled items from the section once a product has been clicked
      item.parents('.content').find('.item-row.incompatible-row').each(function (id, el) {
        if ($(el).css("display") == "block") {
          $(el).css("display", 'none');
          $(el).removeClass('incompatible-row');
          $(el).find('.item-pricing').removeClass('hidden');
        }
      });

      // If the item has class 'exclusive-row' it means we need to deselect the other product prior to selecting it, as it is part of a mutually-excluded family
      if (item.hasClass('exclusive-row') && !itemDeselection) {
        var removeFromExclusiveFamily = [];
        item.parent().find('.item-row.selected').each(function (id, el) {
         $(el).removeClass('selected');
         $(el).removeClass('multi-option');
         removeFromExclusiveFamily.push($(el).data('id'));
        });
      }

      this.lastAddedItem = item;
      // handle subscription sim only
      if (section == self.subscriptionIdentifier && !item.hasClass('selected')) {
        var product = this.products[section].filter(function (prod) {
          return prod['entity_id'] == productId
        }).first();
        this.updateSimsList(product.identifier_simcard_allowed);
        simCardChecker.checkPackageSubscription(product, this.selectedSim || this.options.old_sim, this.packageId);
      }

      if (root.getType(this[handleMethod]) === 'function') {
        this[handleMethod](item);
      } else {
        if ((makeCall && (item.hasClass('disabled') || item.hasClass('disabled-row'))) || item.parents('.selected-item').length) {
          return;
        }
        var box = item.parents('.conf-block');

        // Already selected so we need to deselect it
        if (itemDeselection) {
          var greyOut = false;
          if (item.parents('.family').find('.family-header .family-status .mandatory:visible').length > 0) {
            if (item.siblings('.item-row:visible').length == 0) {
              return;
            }
          }

          // remove given label
          item.find('span.item-label').removeClass('given');
          if (section == 'device' || section == 'tariff') {
            if (jQuery('#devicerc-block .item-row.selected').length > 0) {
              jQuery('#devicerc-block .item-row.selected').removeClass('selected');
              this.cart['devicerc'] = [];
              var selectedItem = jQuery('#devicerc-block').find('.selected-item');
              selectedItem.find('.content')[0].innerHTML = '<div class="no-item-message"><span>{0}</span></div>'.format(selectedItem.data('no-item'));
              selectedItem.find('.block-header .left p span').first().hide();
            }
          }

          item.removeClass('selected');
          item.removeClass('multi-option');
          var remainingSelectedItems = item.parents('.content').find('.item-row.selected').filter(function (index) {
            return $(this).parents('.conf-block').data('type') == section;
          });
          if (remainingSelectedItems.length == 1) {
            remainingSelectedItems.removeClass('multi-option');
          }
          //remove hints as well
          window.configuratorUI.updateHintsOnDeselect(section);

          if (!this.cart[section]) {
            this.cart[section] = [];
          } else {
            productId = parseInt(productId);
            delete this.cart[section][$.inArray(productId, this.cart[section])];
            this.cart[section] = $.map(this.cart[section].unique(), function (n, i) {
              return n ? n : null;
            });
          }
          // We need to select the item
        } else {
          item.addClass('selected');

          var greyOut = true;

          if (!this.cart[section]) {
            this.cart[section] = [];
          }
          // Block is of type multi
          if (!box.data('multi')) {
            item.siblings().removeClass('selected').removeClass('multi-option');
            item.parents('.product-table').find('.item-row').removeClass('selected').removeClass('multi-option');
            item.addClass('selected');
            this.cart[section] = [productId];
          } else {
            this.cart[section].push(productId);
            this.cart[section] = $.map(this.cart[section].unique(), function (n, i) {
              return n ? n : null;
            });
          }

          // add multi-option class if multiple products are selected
          var previouslySelectedItems = item.parents('.content').find('.item-row.selected').filter(function (index) {
            return $(this).parents('.conf-block').data('type') == section;
          });
          if (previouslySelectedItems.length > 1) {
            previouslySelectedItems.each(function (id, el) {
              $(el).addClass('multi-option');
            });
          }
        }
      }

      if (makeCall) {
        this.cart[section] = $(this.cart[section]).not(removeFromExclusiveFamily).get();
        this.selectedSim = false;
        this.addSelections(section, makeCall, externalCallBack, refreshConfigurator);
        // Get the current section
        var currentSection = $('.conf-block.panel[data-type=' + section + ']');
        // If a product is selected in this section -> automatically go to the next section in the configurator
        if (currentSection.data('multi') != true) {
          if (!itemDeselection && currentSection.parent().find('[data-index=' + (currentSection.data('index') + 1) + ']')) {
            this.toggleSection(currentSection.parent().find('[data-index=' + (currentSection.data('index') + 1) + ']').data('type'), false);
          }
        }
      }

      if (greyOut != undefined && item.parents('.family').length > 0) {
        var familyId = item.parents('.family').data('family-id');
        var family = false;
        window.families.every(function (el) {
          if (el.entity_id == familyId) {
            family = el;
            return false;
          }
            return true;
        });
        if (family && (family.hasOwnProperty('allow_mutual_products') && family.allow_mutual_products == 1)) {
          if (greyOut == true) {
            item.siblings().addClass('exclusive-row');
          } else {
            $.each(item.siblings(), function (index, value) {
              $(value).removeClass('exclusive-row');
            });
          }
        }
      }
    },
    addSelections: function (section, makeCall, externalCallBack, refreshConfigurator) {
      window.setPageLoadingState && window.setPageLoadingState(true);
      window.setPageOverrideLoading(true);
      var self = this;
      makeCall = undefined === makeCall ? true : makeCall;
      var box = $('#'+section+'-block');
      var selectedItem = box.find('.selected-item');
      // send all the cart section ids; not only the ones selected in dom since some products might not be visible in the configurator
      var ids = self.cart[section];

      // Hide promo rules block
      $('#promo-rules').removeClass('open').find('.promo-rules-container').hide();

      var selectSections = function () {
        if (!self.cart[section]) {
          self.cart[section] = [];
        }
        self.cart[section] = ids.unique();
        // Cart needs to remain persistent even though several products have product visibility in configurator set to none
      };

      var failed = false;

      var callback;
      if (typeof externalCallBack === "function") {
        callback = externalCallBack;
      } else {
        callback = function (data) {
          var block = $(".side-cart.block[data-package-id='" + data.activePackageId + "']").find('.block-container')[0];
          // if refresh configurator is set to true, than no further addMulti callback is necessary as it is irrelevant
          if (refreshConfigurator === true && !$(block).closest('.side-cart').hasClass('active')) {
            window.setPageLoadingState && window.setPageLoadingState(true);
            window.setPageOverrideLoading(true);
            activateBlock(block);
            return;
          } else {
            refreshConfigurator = false;
          }

          if (data.hasOwnProperty('error') && data.error) {
            if (data.hasOwnProperty('reload') && data.reload) {
              window.location.reload();
            } else {
              if (data.hasOwnProperty('message')) {
                showModalError(data.message);
              }

              self.lastAddedItem.removeClass('selected').removeClass('not-allowed').removeClass('multi-option');
              self.cart[section].pop(self.lastAddedItem.data('id'));

              if (!self.cart[section].length) {
                var content = '<div class="no-item-message"><span>{0}</span></div>'.format(selectedItem.data('no-item'));

                selectedItem.find('.content')[0].innerHTML = content;
              }

              // mark current cart items as selected after error response
              if (data.hasOwnProperty('cartProducts')) {
                  $.each(data.cartProducts, function (index, el) {
                      $('#item-product-'+el).addClass('selected');
                  });
              }

              return;
            }
            failed = true;
            return;
          }

          if (data.hasOwnProperty('simError') && data.simError) {
            showModalError(data.simError);
            // remove selected sim
            $('#sim-card-select option[value="' + self.simType + '"]').attr('selected','');
          } else if (data.hasOwnProperty('sim') && data.sim) {
            self.addSimOption(data.sim);
          }

          // Save a reference to the section containing the last added/removed product
          self.lastSection = section;

          selectSections();

          self.setPackageId(data['activePackageId']);
          self.applyRules(data);

          if (configurator.ajaxQueueSize > 1 || window.checkoutEnabled === false) {
            $('#cart_totals input').attr('disabled', 'disabled');
          }

          /* keep amount of promorules synchronized when callback is done */
          if (data.promoRules) {
            $('.promo-rules-duplicate').parent().removeClass('hidden');
            $('.promo-rules-duplicate').html(data.promoRules);
          }

          if (data.hasOwnProperty('isHollandsNieuwe') && data.isHollandsNieuwe) {
            self.isHollandsNieuwe = true;
            $('.conf-block[data-type="tariff"]').parent().find('.sim-select').find('.sim-old').show();
          } else if (self.saleType != self.allowedSaleType) {
            $('#sim-select-block').find('option[value="Gebruikt huidige simkaart"]').remove();
          }

          // Add selected class to all products from quote
          if (data.hasOwnProperty('cartProducts')) {
            //refresh each product row
            $.each(self.cart, function (section, productIds) {
              //parse each section of self.cart
              $.each(productIds, function (id, productId) {
                $('#item-product-'+productId).removeClass('selected multi-option mandatory-row hide_not_selected_products not-allowed');
              });
            });

            self.cart = {};
            self.acqCart = [];
            self.ilsCart = [];
            $.each(data.cartProducts, function (id, el) {
              if (!self.cart.hasOwnProperty(el.type)) {
                self.cart[el.type] = [];
              }
              self.cart[el.type].push(el.product_id);
              var item = $('#item-product-'+el.product_id);
              item.addClass('selected');
              if (el.hasOwnProperty('isCampaign') && el['isCampaign'] == 1) {
                item.find('span.item-label').removeClass('hidden').addClass('campaign').text(Translator.translate('CAMPAIGN'));
              }
              if (el.hasOwnProperty('mandatory') && el['mandatory']) {
                item.addClass('not-allowed');
              }
              if (ILS_MOVE_PROCESS_CONTEXTS.indexOf(data.processContextCode.toLowerCase()) !== -1) {
                if (el.hasOwnProperty('mandatory') && el['mandatory']) {
                  item.addClass('mandatory-row');
                  if (!item.find('span.item-contract').hasClass('contract')) {
                    item.find('span.item-label').removeClass('hidden');
                    item.find('span.item-label').removeClass('given').addClass('mandatory').text(Translator.translate('MANDATORY'));
                  } else {
                    item.find('span.item-label').addClass('hidden');
                  }
                } else if (el.hasOwnProperty('isGiven') && el['isGiven'] && el.hasOwnProperty('isDefaulted') && el['isDefaulted']) {
                  if (!item.find('span.item-label').hasClass('contract')) {
                    item.find('span.item-label').removeClass('hidden');
                    item.find('span.item-label').removeClass('hidden').removeClass('contract').addClass('given').text(Translator.translate('GIVEN'));
                  } else {
                    item.find('span.item-label').addClass('hidden');
                  }
                }
              }
              if (el.hasOwnProperty('isContract') && el['isContract'] == 1) {
                self.ilsCart.push(parseInt(el.product_id));
              } else {
                self.acqCart.push(parseInt(el.product_id));
              }

              var familyId = item.parents('.family').data('family-id');
              var family = false;
              window.families.each(function (el) {
                if (el.entity_id == familyId) {
                  family = el;
                  return false;
                }
              });

              if (family && (family.hasOwnProperty('allow_mutual_products') && family.allow_mutual_products == 1)) {
                if (item.parents('.family').length > 0) {
                  $.each(item.siblings(), function (index, value) {
                    $(value).addClass('exclusive-row');
                  });
                }
              }

              // mark multiple selected products in cart
              self.markMultiOptions();
            });

            $.each(self.cart, function (section, productIds) {
              self.toggleSection(section, true);
            });
          }

          if (data.hasOwnProperty('quoteHash')) {
            root.quoteHash = data.quoteHash;
          }

          if (data.hasOwnProperty('salesId')) {
            root.SALES_ID = data.salesId;
          }

          if (data.hasOwnProperty('processContextCode')) {
            root.processContextCode = data.processContextCode;
          }
          // Remove the defaulted products from the configurator visually
          // Not needed anymore: logic moved to parsing productsInCart addMulti response node

          // Logic moved to parsing cartProducts response within addMulti call

          //if automatically selected an addon, refresh the addon block
          if (automatically_refresh_selection) {
            //configurator.toggleSection(automatically_refresh_selection, true);
            window.setPageLoadingState && window.setPageLoadingState(false);
            automatically_refresh_selection = false;
          }

          if (store.get('btwState') == null || store.get('btwState') == true) {
            ToggleBullet.switchOn('.tax-toggle');
            sidebar.showSummarizedPrice();
          } else {
            ToggleBullet.switchOff('.tax-toggle');
            sidebar.showDetailedPrice();
          }
          if (typeof data.cartProducts !== 'undefined' && Object.keys(data.cartProducts).length) {
            jQuery('#configurator_checkout_btn').removeAttr('disabled');
          } else {
            jQuery('#configurator_checkout_btn').attr('disabled', 'disabled');
          }

          if (data.hasOwnProperty('rightBlock')) {
            togglePackage(jQuery('.cart_packages').find('div[data-package-id="' + data.activePackageId + '"]').find('.side-block-activate'));
          }

          jQuery('[data-toggle="tooltip"]').tooltip();
        };
      }

      var simSelect = $('.conf-block[data-type="tariff"]').parent().find('.sim-select');
      var checkIfIsSimOnly = [];

      if (self.subscriptionIdentifier != '') {
        if (self.cart[self.subscriptionIdentifier] && self.cart[self.subscriptionIdentifier].length) {
          checkIfIsSimOnly = $.map(self.products[self.subscriptionIdentifier], function (i, y) {
            if (i.entity_id == self.cart[self.subscriptionIdentifier][0]) {
              return i;
            }
          });
        }
      }

      if (!this.clearSim) {
        var subscriptionIdentifier = self.subscriptionIdentifier;
        var deviceIdentifier = self.deviceIdentifier;
        var isIndirect = $('#current_store_is_indirect').val();
        if ((self.cart[subscriptionIdentifier] && self.cart[subscriptionIdentifier].length)
          && (
            (checkIfIsSimOnly.length && checkIfIsSimOnly[0]['sim_only'] == true)
            || (self.cart[deviceIdentifier] && self.cart[deviceIdentifier].length)
            || (typeof checkIfIsSimOnly[0] != undefined && (checkIfIsSimOnly.length && checkIfIsSimOnly[0]['aikido'] ==  1))
          )
        ) {
          if (simSelect.length && ((this.saleType == this.allowedSaleType) || this.isHollandsNieuwe)) {
            if (self.options.old_sim) {
              self.clearSimOption();
            } else if ((typeof self.selectedSim == 'undefined' || self.selectedSim == false) && ((this.saleType == this.allowedSaleType) || this.isHollandsNieuwe ) && !self.simType) {
              // Use old sim as default for retention packages
              self.options.old_sim = true;
            }
          } else {
            $('#sim-select-block').find('option[value="Gebruikt huidige simkaart"]').remove();
          }

          if ((self.cart[deviceIdentifier] && self.cart[deviceIdentifier].length && (typeof self.products[self.deviceIdentifier]) != 'undefined') && !self.fromSimFooter) {
            var checkIfHasSim = $.map(self.products[self.deviceIdentifier], function (i, y) {
              if (i.entity_id == self.cart[self.deviceIdentifier][0]) {
                return i;
              }
            });
            if (checkIfHasSim.length > 0 && checkIfHasSim[0].sim_type == '' && (typeof self.selectedSim == 'undefined' || self.selectedSim == false)) {
              this.simType = null;
            }
          }
          if ((typeof self.selectedSim == 'undefined' || self.selectedSim == false) && (this.saleType == this.allowedDefaultSim) && !this.isHollandsNieuwe) {
            if (section == deviceIdentifier) {
              var typeId = self.cart[deviceIdentifier][0];
              var type = deviceIdentifier;
              this.addSimOption(this.getDefaultSimOption(typeId, type));
            } else {
              this.addSimOption(self.simType);
            }
          }
          if (isIndirect == 0 || isIndirect == undefined) {
           simSelect.show();
           simSelect.parents('.conf-block').find('.block-footer').removeClass('hidden');
          }
        } else {
          if (isIndirect == 0 || isIndirect == undefined) {
            simSelect.hide();
            simSelect.parents('.conf-block').find('.block-footer').addClass('hidden');
          }
          self.clearSimOption();
        }
      }

      if (makeCall) {
        this.addMultiToCart(ids, box.data('type'), callback, !isIE, refreshConfigurator);
      } else {
        selectSections();
      }

      if (failed) {
        self.lastSection = false;
        return;
      }
    },

    addSimOption: function (simType, makeCall, item) {
      simType = $.trim(simType);
      var self = this;
      self.lastAddedItem = $(item);

      var simSelect = $(this.options.container).find('.sim-select');

      triggerConfiguratorEvent('configurator.changesim', '');

      var validSimOptions = [];
      jQuery.each(jQuery('#sim-card-select option'), function (id, el) {
        validSimOptions.push($(el).attr('value'));
      });

      if (!!(simType && (typeof self.simType != 'undefined') && (self.simType == simType) && makeCall)) {
        return false;
      } else if (!!(simType && ($.inArray(simType, validSimOptions) != -1))) {
        self.simType = simType;
      } else if (simType == '' && self.simType == '') {
        self.simType = null;
      }

      if (makeCall) {
        self.selectedSim = true;
        self.fromSimFooter = true;
        self.options.old_sim = false;
        self.addSelections(simSelect.closest('.conf-block').data('type'), true);
        self.fromSimFooter = false;
      }

      $('#sim-card-select option[value="' + simType + '"]').attr('selected','selected');
      $('#sim-card-select').initSelectElement();
    },
    clearSimOption: function (makeCall) {
      var self = this;
      var simSelect = $(this.options.container).find('.sim-select');
      self.simType = null;
      if ((this.saleType == this.allowedSaleType) || this.isHollandsNieuwe) {
        self.options.old_sim = true;
        $('#sim-card-select option[value="Gebruikt huidige simkaart"]').attr('selected','selected');
      }

      if (makeCall) {
        self.clearSim = true;
        window.simCardChecker.setUseMySim(self.packageId);
        self.addSelections(simSelect.closest('.conf-block').data('type'), true);
      }
    },
    prepareCart: function (data, callback) {
      var self = this;
      var simType = '';

      $.each(data, function (type, ids) {
        self.cart[type] = ids.unique();
      });

      if (self.cart['simcard']) {
        self.selectedSim = true;
        self.cart['sim'] = self.cart['simcard'];
      }

      if (root.simCardName) {
        var simBlock = $('#sim-card-select');
        simBlock.append($("<option></option>"));
        if (this.saleType == 'RETBLU') {
          simBlock.append($("<option></option>")
            .attr("value",Translator.translate('Using current SIM'))
            .text(Translator.translate('Using current SIM')));
        }
        $.each(root.simCardName,function(id,value) {
          if (self.cart['sim'] == id) {
            simType = value;
          }
          simBlock
            .append($("<option></option>")
            .attr("value",value)
            .attr("data-code",id)
            .text(value));
        });
        if (simType) {
          this.simType = simType;
          self.addSimOption(simType);
        }
      }

      $.each(self.cart, function (section, products) {
        var block = $('#'+section+'-block');
        if (block.length) {
          $.each(products, function (key, id) {
            var row = block.find('#item-product-'+id);
            if (row.length) {
              row.addClass('selected');
              /* Do not show empty label container on preselected products
              if (ILS_MOVE_PROCESS_CONTEXTS.indexOf(root.processContextCode.toLowerCase()) !== -1) {
                row.find('span.item-label').removeClass('hidden');
                row.find('span.item-label').addClass('contract');
              }*/
              if (row.closest('.conf-block').data('type') == 'tariff') {
                var productId = row.data('id');
                var product = self.products['tariff'].filter(function (prod) {
                  return prod['entity_id'] == productId
                }).first();
                self.updateSimsList(product.identifier_simcard_allowed);
                simCardChecker.checkPackageSubscription(product, self.selectedSim || self.options.old_sim, self.packageId);
              }
              if (ILS_MOVE_PROCESS_CONTEXTS.indexOf(root.processContextCode.toLowerCase()) !== -1) {
                if (root.defaultedItems.length && root.defaultedItems.indexOf(id.toString()) !== -1) {
                  if (!row.find('span.item-label').hasClass('contract')) {
                    row.find('span.item-label').removeClass('hidden').removeClass('contract').addClass('given').text(Translator.translate('GIVEN'));
                  }
                }
              }
              if (root.defaultedObligatedItems.length && root.defaultedObligatedItems.indexOf(id) !== -1) {
                row.addClass('mandatory-row');
                if (ILS_MOVE_PROCESS_CONTEXTS.indexOf(root.processContextCode.toLowerCase()) !== -1) {
                  if (!row.find('span.item-contract').hasClass('contract')) {
                    row.find('span.item-label').removeClass('hidden').removeClass('given').addClass('mandatory').text(Translator.translate('MANDATORY'));
                  }
                }
              }
              var familyId = row.parents('.family').data('family-id');
              var family = false;
              familyId && window.families.every(function (el) {
                familyId = familyId.toString();
                if (el.entity_id === familyId) {
                  family = el;
                  return false;
                }
                return true;
              });
              if (family && (family.hasOwnProperty('allow_mutual_products') && family.allow_mutual_products.toString() === "1")) {
                row.siblings().addClass('exclusive-row');
              }
            }
          });
        }
        self.addSelections(section, false);
      });

      // mark multiple selected products in cart
      self.markMultiOptions();

      if (typeof self.products !== "undefined") {
        $.each(self.products, function (section, products) {
          self.filterProducts(products, section);
        });
      }


      // apply updated prices, if we have any
      if (root['prices']) {
          self.updatePrices(root.prices);
      }

      if (root.getType(callback) === 'function') {
        callback();
      }
    },
    getDefaultSimOption: function (typeId, type) {
      if (typeof this.products[type] !== 'undefined') {
        var result = $.map(this.products[type], function(n, i) {
          return n && n['entity_id'] == typeId ? n : null
        })[0];
        return result && result['sim_type'] ? result['sim_type'] : null;
      }
    },
    /**
     * Gets the product popup template
     * @returns html template
     */
    getProductPopupTemplate: function () {
      var configuratorType = this.options.type;
      if (configuratorType == 'int_tv_fixtel') {
        return window.templates.partials[configuratorType]['pakket']['product_popup'];
      }
      else {
        return window.templates.partials[configuratorType]['accessory']['product_popup'];
      }
    },
    /**
     * Load product information popup
     * @param productId
     */
    loadProductInformationPopup: function(productId, package) {
      if(package === undefined || package === null) {
        package = 'device';
      }
      var self = this;
      self.http.ajaxQueue('GET', 'cart.getPopup', {
        'website_id': self.websiteId,
        'product_id': productId
      }, true).done(function(data) {
        if (data.hasOwnProperty('error')) {
          showModalError(data.message);
          return;
        }
        var product = self.products[package].filter(function(el) { return el['entity_id'] == productId;}).first();
        product = jQuery.extend(product, data);
        var template = self.getProductPopupTemplate();
        var modal = jQuery(Handlebars.compile(template, {noEscape: true})(product));
        jQuery('.productPopUpModal, .modal-backdrop.in').remove();
        modal
          .find('[data-toggle=tab]').on('click',function(e){
          e.preventDefault();
          jQuery(this).tab('show');
        })
          .end()
          .modal()
          .on('hidden.bs.modal', function () {
            // destroy on close;
            var el = jQuery(this);
            el
              .data('bs.modal', null)
              .remove();
          });
      });
      return false;
    },
    startConfigurator: function (callback) {
      var self = this;

      // set all products to corresponding btw state
      this.updateBtwState(this.options.include_btw);

      // build data to be sent to ui
      var uiData = [];

      // iterate through each configurator section
      this.options.sections.forEach(function(section, i) {
        // parse each configurator section by its given products
        if (this.templates[section]) {
          // tell each section what products template to use
          var productsTemplate = '{0}_selected_item'.format(section);
          // section products are all exiting products in each configurator section, including the ones belonging to families
          var sectionProducts = (typeof this.products != 'undefined' && this.products[section] ) ? this.products[section] : [];
          // this is the list of products with no family sent to configuratorUI
          var templateProducts = sectionProducts.reduce(function(templateProducts, cur, i) {
            templateProducts[i] = cur;
            return templateProducts;
          }, {});
          var templateFamilies = [];

          // if partial is registered and section has families, proceed to fetching products to each family
          if ((root.getType(Handlebars.partials['{0}_family'.format(section)]) === 'function') && this.families.length > 0 && sectionProducts.length > 0) {
            var familyTemplate = '{0}_family'.format(section);
            sectionProducts.forEach(function(product, k) {
              this.families.every(function(family) {
                if (product['families'] != undefined) {
                  familyEntityId = parseInt(family.entity_id);
                  if (typeof product.families[familyEntityId] !== 'undefined') {
                    // Init new family for current section
                    if (typeof templateFamilies[familyEntityId] === 'undefined') {
                      family.products = [];
                      templateFamilies[familyEntityId] = family;
                    }

                    // Add product to template family
                    templateFamilies[familyEntityId].products.push(product);

                    // If found in the first family, the product is removed from products list and added to the family product list which is rendered separately than products
                    delete templateProducts[k];
                    return false;
                  }
                }
                return true;
              }.bind(this));
            }.bind(this));
          }

          // OMNVFDE-778: Sort families inside configurator section
          if (templateFamilies.length) {
            templateFamilies.sort(function (oneFamily, anotherFamily) {
              if(oneFamily.family_position.toString() == anotherFamily.family_position.toString()){
                return oneFamily.name.localeCompare(anotherFamily.name);
              } else {
                return oneFamily.family_position - anotherFamily.family_position;
              }
            });
          }
          // EOF OMNVFDE-778: Sort families in configurator

          var permanentFilters = {};
          if (window.permanentFilters[self.options.sections[i]] !== undefined) {
            permanentFilters = window.permanentFilters[self.options.sections[i]];
          }

          uiData[section] = {
            index: i,
            productType: section,
            familyTemplate: familyTemplate ? familyTemplate : {},
            families: jQuery.extend(true, {}, templateFamilies),
            productsTemplate: productsTemplate,
            products: jQuery.extend(true, {}, templateProducts),
            permanentFilters: permanentFilters,
            filters: (typeof self.filters != 'undefined' && self.filters[section]) ? self.filters[section] : {},
            closed: !!i
          };
        }
      }.bind(this));

      // let UI handle display
      window.configuratorUI.setConfigurator(this).loadConfigurator(uiData, callback);
    },
    /**
     * Add item to bundle
     * @param element
     * @param fromConfigurator
     */
    addToCartBundle: function(element, fromConfigurator) {
      var self = this;
      var bundleId = $(element).data('bundle-id');
      var productId = $(element).data('product-id');
      var subscriptionNumber = $(element).data('subscription-number');
      var interStackPackageId = $(element).data('inter-stack-package-id');

      if (typeof fromConfigurator === undefined) {
        fromConfigurator = false;
      }

      var data = {
        bundleId: bundleId,
        subscriptionNumber: subscriptionNumber,
        productId: productId,
        interStackPackageId: interStackPackageId
      };

      // Avoid executing multiple ajax calls if button is pressed again (see OMNVFDE-3309)
      $(element).attr('onclick','');


      var callBack = function() {
        setPageLoadingState(true);
        self.http.ajax('POST', 'bundles.create', data, true).done(function (response) {
          if (response.error) {
            showModalError(response.message);
          } else {
            self.closeBundleDetailsModal();
            // move down the notification toaster when bundle is created
            $('.toast-notifications-container').css('margin-bottom', '');
            // after done updating DOM, update customer section eligible bundles and trigger initConfigurator for newly created redPlus package
            $('.cart_packages')
              .replaceWith(response.rightBlock)
              .promise()
              .done(function () {
                if (response.hasOwnProperty('eligibleBundles')) {
                  customerSection.updateMyProductsEligibleBundles(response.eligibleBundles);
                }
                if (response.hasOwnProperty('totals')){
                    $('#cart_totals').replaceWith(response.totals);
                }
                // initConfigurator for Red+ package
                if (response.hasOwnProperty('initNewPackage') && response.initNewPackage) {
                  var redPlusPackage = $('.cart_packages')
                    .find('[data-package-id="' + response.initNewPackage + '"]')
                    .first()
                    .find('.block-container');
                  activateBlock(redPlusPackage);
                } else {
                  var refreshBundles = true;

                  if (configurator.packageId !== null && configurator.packageId !== undefined) {
                    togglePackage($('.cart_packages').find('[data-package-id="' + configurator.packageId + '"]').find('.side-block-activate'));

                    initConfigurator(
                      $('.cart_packages').find('[data-package-id="' + configurator.packageId + '"]').first().find('.block-container').parents('.side-cart').attr('data-package-type'),
                      $('.cart_packages').find('[data-package-id="' + configurator.packageId + '"]').first().find('.block-container').parents('.side-cart').attr('data-package-id'),
                      $('.cart_packages').find('[data-package-id="' + configurator.packageId + '"]').first().find('.block-container').parents('.side-cart').attr('data-sale-type'),
                      null, null, null, null, null, null, null, null, null, null,
                      $('.cart_packages').find('[data-package-id="' + configurator.packageId + '"]').first().find('.block-container').parents('.side-cart').attr('data-package-creation-type-id')
                    );

                    refreshBundles = false;
                  }
                  if (refreshBundles) {
                    // call eligible bundles again (otherwise init configurator will auto call them)
                    self.getEligibleBundles();
                  }
                }
                if (response.hasOwnProperty('optionsPanel')) {
                  $('#config-wrapper').prepend(response.optionsPanel);
                }

                $(window).trigger('resize');
              });
          }
        });
      };

      if (fromConfigurator) {
        configurator
          .selectProduct($(element).closest('.item-row'), true, callBack);
      } else {
        callBack();
      }
    },
    /**
     * Initialize configurator
     * @param options
     */
    initialize: function (options) {
      this.ltIE9 = this.$$('#isLTIE9').val() == '1';
      this.websiteId = this.$$('#current_website_id').val();
      this.$$('#config-wrapper').show();
      /* hide the wrapper; */
      var self = this;
      this.cart = {};
      this.subscriptionIdentifier = "";
      this.deviceIdentifier = "";
      this.options = $.extend(this.options, options);
      this.options.sections.forEach(function (element) {
        if (SUBSCRIPTION_IDENTIFIERS.indexOf(element) !== -1) {
          self.subscriptionIdentifier = element;
        }
        if (DEVICES_IDENTIFIERS.indexOf(element) !== -1) {
          self.deviceIdentifier = element;
        }
      });
      this.packageId = this.options.packageId;
      this.saleType = this.options.sale_type;
      this.cache = this.options.cache || false;
      this.http.cache = new Cache(this, this.options.type);
      this.http.endpoints = $.extend(this.http.endpoints, this.options.endpoints);
      this.http.spinnerId = this.options.spinnerId;
      this.isHollandsNieuwe = this.options.isHollandsNieuwe;
      this.permanentFilterStack = root.permanentFilters;
      //assure that sections are array (indirect bug)
      var getType = {};
      if (self.options.sections && getType.toString.call(self.options.sections) === '[object Object]') {
        var sections = [];
        for (var i in self.options.sections) {
          if (self.options.sections.hasOwnProperty(i)) {
            sections.push(self.options.sections[i]);
          }
        }
        self.options.sections = sections;
      }

      /** Reset filters */
      this.filterStack = {};
      this.filterValues = {};

      /* Close left side bar if expanded */
      if ($(this.options.enlargeLeft).hasClass('expanded')) {
        $(this.options.enlargeLeft).trigger('click');
      }

      /*  Close customer panel if it is opened */
      if ($(this.options.customerPanel).is(':visible')) {
        $(this.options.customerPanel + ' .close:first').trigger('click');
      }

      /** Temporary prevent all clicks in document */
      $(document).click(function (e) {
        e.stopPropagation();
      });

      this.initSpinner();

      this._collectTemplates(window.templates);

      this.setFilters(root.filters, function () {
        self.setProducts(root.products, root.families, function () {
          self.startConfigurator(function () {
            self.prepareCart(root.initCart, function () {
              self.setupAcqProducts();
              $(root)
                .unbind('resize orientationchange')
                .bind('resize orientationchange', function () {
                  var el = $('{0}'.format(self.options.container));
                  var header = self.$$('#header');
                  if (el.length) {
                    var height = $(window).outerHeight() - 2;
                    if (header.length) {
                      height -= header.outerHeight();
                    }
                  }
                })
                .trigger('resize');
              var packageBlock = $('[data-package-id="{0}"]'.format(self.packageId));
              if (packageBlock.length) {
                packageBlock.addClass('active');

                var scrollTo = packageBlock.offset().top - packageBlock.parent().offset().top + packageBlock.parent().scrollTop();
                if (scrollTo != 0) {
                  packageBlock.parent().animate({
                    scrollTop: scrollTo
                  }, 1000);
                }
              }
              if (self.options.hardwareOnlySwap) {
                $.each(self.options.hiddenSections, function (index, elem) {
                  $('#'+elem+'-block').hide();
                });
              }

              //Open first selection when configurator starts
              var diff = $(self.options.sections).not(self.options.disabledSections).get();
              self.toggleSection(diff[0]);

              $("#config-wrapper").on('keyup','input[name="serial-number"]',function(){
                  var errorClassNode = $(this).closest('div.field-row');
                  if (errorClassNode.hasClass('serial-error')) {
                      errorClassNode.removeClass('serial-error');
                      $(this).popover('destroy');
                      $(this).attr('data-content', '');
                  }
              });

              self.initialized = true;
            });
          });
        });
      });

      //update prices will be executed by the toggleSection function
      self.updatePrices(root.prices);

      /** Release click lock on document */
      $(document).unbind('click');
      if (typeof self.simType != 'undefined') {
        $('#sim-card-select option[value="' + self.simType + '"]').attr('selected','selected');
      }

      self.reapplyRules(0, function() {
        if (typeof self.products !== "undefined" && (self.products.tariff || self.products.pakket)) {
          self.getEligibleBundles();
        }
      }.bind(self));
      $(document).trigger('configurator.initialized');
    },
    /**
     * Get prices for configurator products
     * @param options
     */
    getPrices: function () {
      var self = this;
      var allProducts = self.acqCart.concat(self.ilsCart);
      var cartProducts = allProducts.sort();
      var theType = self.saleType;

      var data = {
        'website_id': self.websiteId,
        'type': theType,
        'packageType' : self.options.type,
        'sales_id': root.SALES_ID
      };
      if (cartProducts.length > 0) {
        data.products = cartProducts.join(',');
      }

      this.http.ajax('GET', 'cart.getPrices', data, false).done(function (data) {
        var prices = data['prices'];
        root['prices'] = prices;

        if (Object.keys(prices).length) {
          self.lastPrices = new Date().getTime();
          self.updatePrices(prices);
        }
      });
    },
      /**
       * Determine whether or not clicking on a product in configurator will break a bundle
       * Will iterate through each tariffs and if one that is bundle component is selected will show warning
       * @param productId
       * @returns {boolean}
       */
      productBreaksBundle: function (productId)
      {
        var breaks = false;

        // Iterate through all subscription identifiers, included device in array due to cardinality 1 for mobile devices
        SUBSCRIPTION_AND_DEVICE_IDENTIFIERS.forEach(function (lowerCaseIdentifier) {
          // If cart has the subscription section
          if (this.cart.hasOwnProperty(lowerCaseIdentifier)) {
            // Clicked on a tariff
            this.productIsOfType(productId, lowerCaseIdentifier) && this.cart[lowerCaseIdentifier].every(function (cartProductId) {
              // Already a tariff in cart, whatever tariff the agent clicked will break the bundle
              if (this.productIsBundleComponent(cartProductId)) {
                breaks = true;
                // equivalent of break in a loop, will stop at the first that evals to true
                return false;
              }
            }.bind(this));
          }
        }.bind(this));

        //if subscription check passes, also check rest of products
        if(!breaks){
          breaks = this.productIsBundleComponent(productId);
        }

        //if product is still not a bundle component, check if it's part of a mutual excl category in which there is a bundle component
        if(!breaks && $("#item-product-"+productId).hasClass('exclusive-row')){
          if($("#item-product-"+productId).siblings().not('.disabled-row').each(function(index, item){
            if(this.productIsBundleComponent($(item).data('id'))){
              breaks = true;
              return false;
            }
          }.bind(this)));
        }

        return breaks;
      },

    /**
     * Shows other stores element
     * @param el
     */
    showOtherStores: function (el) {
      var modalToOpen = '#stockPopUpModal'+jQuery(el).data('id');
      jQuery(modalToOpen+' .stock-other-stores').removeClass('hidden');
      jQuery(modalToOpen+' .hide-stores-row').removeClass('hidden');
      jQuery(modalToOpen+' .display-stores-row').addClass('hidden');
    },

    /**
     * Hides other stores element
     * @param el
     */
    hideOtherStores: function (el) {
      var modalToOpen = '#stockPopUpModal'+jQuery(el).data('id');
      jQuery(modalToOpen+' .stock-other-stores').addClass('hidden');
      jQuery(modalToOpen+' .hide-stores-row').addClass('hidden');
      jQuery(modalToOpen+' .display-stores-row').removeClass('hidden');
    },

    /**
     * Generates other stores list
     * @param json
     * @param id
     */
    setOtherStoresList: function (json, id) {
      jQuery.each(json.stockStores, function(i, item){
        var dealer = this.getDealerByAxiCode(json.dealers, item.store, id);
        if (dealer) {
          var row = '<div class="row stock-data-row">'
              +'<div class="col-xs-1 text-center">' + item.stock + '</div>'
              + '<div class="col-xs-8 text-center">'
              + dealer.city + ', '
              + dealer.street + ' ' + dealer.house_nr + ' '
              + dealer.postcode
              + '</div>'
              + '<div class="col-xs-3 text-center">' + dealer.telephone + '</div>'
              + '</div>';
          jQuery('.store-list').append(row);
        }
      });
    },

    /**
     * Returns dealer data by given axi code
     * @param dealers
     * @param axiCode
     * @param id
     */
    getDealerByAxiCode: function (dealers, axiCode, id) {
      var dealer;
      var modalToOpen = '#stockPopUpModal'+id;
      jQuery.each(dealers, function(i, item){
        if (parseInt(item.axi_store_code) == parseInt(axiCode)
            && parseInt(jQuery(modalToOpen+' .dealers-list').val()) == parseInt(item.axi_store_code)) {
          dealer = item;
          return false;
        }
      });

      return dealer;
    },

    /**
     * Call refresh stock information for selected store
     * @param el
     * @param e
     */
    refreshStockStatus: function (el, e) {
      var productId = jQuery(el).data('id');
      var modalToOpen = '#stockPopUpModal'+jQuery(el).data('id');
      var storeId = parseInt(jQuery(modalToOpen+' .dealers-list').val());
      this.updateStoresStockFound(productId, storeId);
    },

    /**
     * Call refresh stock information for selected store
     * @param el
     * @param e
     */
    refreshStockStores: function (el, e) {
      var productId = jQuery(el).data('id');
      var storeId = jQuery(el).val();
      this.updateStoresStockFound(productId, storeId);
    },

    /**
     * Refresh stock information for selected store by backend request
     * @param productId
     * @param storeId
     */
    updateStoresStockFound: function (productId, storeId) {
      jQuery('.store-list').empty();
      jQuery.ajax({
        type: "POST",
        url: 'configurator/init/getStoresStockFound',
        data: {'product_id': productId,'axi_store_id': storeId},
      }).done(function (response) {
        if (response instanceof Object) {
          var json = response;
        } else {
          var json = jQuery.parseJSON(response);
        }

        if (json.error == 'false') {
          if (json.stockStores != null) {
            this.setOtherStoresList(json, productId);
          }
        }
      });
    },

    showBundleDetailsToaster: function (bundleId, activeBundleExists) {
      var self = this;
      if (bundleId) {
        var $_modalTarget = $j('#bundleModal');
        $_modalTarget.on('shown.bs.modal', function (e) {
          $('div.modal-backdrop').hide();
        });

        var data = {
          bundle_id: bundleId
        };

        this.http.ajax('POST', 'bundles.getHint', data, true).done(function (data) {
          if (data.hasOwnProperty('error') && data['error'] == true) {
            showValidateNotificationBar('<strong>ERROR!</strong> ' + data['message'], 'error', '');
          } else {
            // move up the notification toaster when bundle is displayed
            jQuery('.toast-notifications-container').css('margin-bottom', '55px');

            $_modalTarget.find('.modal-content').html(data.html);
            $_modalTarget.appendTo('body').modal();

            // when a confirmation pop-up is visible hide the bundle toaster @VFDED1W3S-5881
            if ($('#message-dialog-warning').hasClass('message-warning-enabled')) {
              configuratorUI.hideBundleToaster();
            }

            self.showBundleBarInformationToaster();
            if (activeBundleExists !== undefined && activeBundleExists === true) {
              self.markSelectedBundleProductsIDs(data.bundleProductsIds);
              $j('#bundleModal').find('input[type="submit"]').attr('disabled', true);
            }

            self.preselectSingleBundle();
          }
        });
      }
    },
  });
})(jQuery);
