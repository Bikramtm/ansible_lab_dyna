jQuery(document).ready(function ($) {
  $( document ).off( 'focus', ':input');
  $( document ).on( 'focus', ':input', function(){
    if ($( this ).attr( 'autocomplete' ) != 'disable-autofill') {
      $( this ).attr( 'autocomplete', 'off' );
    }
  });

  var footerModals    = $('#footer_modals');

  /** Trigger open styled dropdown when click on parent */
  jQuery('.input-wrapper.select-wrapper').on('click', function(e) {
    e.stopPropagation();
    if (jQuery(e.target).hasClass('select-wrapper')) {
      jQuery(this).find('.select-selected')[0].click();
    }
  });

  if (window.IS_OFFER && window.OFFER_ACTIVE_PACKAGE_ID) {
    jQuery(document).ready(function () {
      var sideBlock = $('.cart_packages [data-package-id="'+window.OFFER_ACTIVE_PACKAGE_ID+'"]');
      if (sideBlock.length && !sideBlock.data('dummy-package')) {
        activateBlock(sideBlock.find('a.side-block-activate'));
      }
    });
  }

  var theBody = $('body');
  theBody.on("input propertychange", ".date-format-nl", function () {
    var el = $(this);
    el.prop('maxlength', 10);

    var val = $.trim(el.val()).replace(/[^\d(\-|\.)]/g, '');
    if (val !== $.trim(el.val())) {
      el.val(val);
    }
    var digits = val.replace(/[^\d]/g, '');
    if (digits.length === 8) {
      var formattedDate = moment(digits, 'DDMMYYYY').format('DD-MM-YYYY');
      if (formattedDate !== 'Invalid date') {
        el.val(formattedDate);
        // trigger change event manually for the number porting cancellation date field
        if (el.hasClass('porting-cancellation-date') || el.hasClass('porting-delivery-date')) {
          el.trigger('change');
        }
        hideErrorMessages(el);
      }
    }
  });

  theBody.on("blur", ".format-postcode-nl", function () {
    var el = $(this);
    var val = $.trim(el.val()).replace(/\s+/g, '');

    if (val.length >= 4) {
      var firstPiece = val.substring(0,4);
      var secondPiece = val.substring(4, val.length);
      val = firstPiece + secondPiece.toUpperCase();
    }

    el.val(val)
  });

  theBody.on("blur", ".format-housenoaddition-nl", function () {
    var el = $(this);
    var val = $.trim(el.val());
    val = val.toUpperCase();

    el.val(val)
  });

  Validation.add('validate-date-future', Translator.translate('Date must be in the future'), function (v, element) {
    var date = new Date();
    var input = v.split('-');
    var test = new Date(input[1] + '/' + input[0] + '/' + input[2]);

    var dateNow = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    if (test.valueOf() < dateNow.valueOf()) {
      return false;
    }

    return true;
  });

  theBody.on('click', '.customer-info-section-hide', function() {
    $('.customer-info-section').toggle();
    if ($('.customer-info-section').is(':visible')) {
      $(this).children('span.glyphicon').removeClass('glyphicon-arrow-right').addClass('glyphicon-arrow-down');
    } else {
      $(this).children('span.glyphicon').removeClass('glyphicon-arrow-down').addClass('glyphicon-arrow-right');
    }
  });

  theBody.on('click', '#one-off-deal', function(e) {
    e.preventDefault();
    createOneOffDeal();
  });

  theBody.on('click', '#renewall-one-off-deal', function(e) {
    var customerNumber = $(this).data('customer-number');
    e.preventDefault();
    createRenewAll(customerNumber);
  });

  footerModals.on('click', '.cancel-cart-button', function () {
    sessionStorage.removeItem('warnings');
    store.remove('redPlusPanelState');
    store.remove('linkedAccountPanelState');
    var modal = $('#cancel-cart-modal');
    modal
      .find('.modal-body')
      .html(modal.find('.after-click-block').html());
    modal.find('.modal-footer button').hide();
    window.canShowSpinner = false;
    if (window.ToggleBullet.switchPending == true) {
      window.ToggleBullet.onSwitchChange('.soho-toggle', 'soho');
      window.ToggleBullet.switchPending = false;
    }

    $.post(MAIN_URL + 'configurator/cart/empty', function () {

      setTimeout(function () {
        modal.modal('hide');
        window.location = "/";
      }, 2000);
    });
    var isMovepackage = $('#move-cancel-package').val();
    if(isMovepackage) {
      var customerServiceaddress = store.get('serviceApiAddressMove');
      var customerServiceData = store.get('serviceApiResponseMove');
      store.set('serviceApiAddress', customerServiceaddress);
      store.set('serviceApiResponse', customerServiceData);
    }

    store.set("shownHints", null);
    window.canShowSpinner = true;
  });

  if ($('#service-stub-content').length > 0) {
    $.ajax({
      url: '/dataimport/index/getStubOptions',
      success: function(data) {
        var usageMessage = data;
        var possibleStubs = false;

        if(usageMessage){
          var messageParts = usageMessage.split('Possible stubs:<br />');
          if(messageParts.length > 1){
            usageMessage = messageParts[0];
            possibleStubs = messageParts[1];
          }
          $('#service_stub_usage').html(usageMessage);
        }

        if(possibleStubs){
          var stubs = possibleStubs.split('<br />');
          var columns = $('[id="service_stub_possible_stubs"] .column');
          if(typeof stubs != 'undefined' && stubs.length > 0
            && typeof columns != 'undefined' && columns.length > 0){
            var stubsPerColumn = Math.ceil(stubs.length / columns.length);
            // Foreach column add the stubs
            var stubIndex = 0;
            for (columnIndex = 0; columnIndex < columns.length; columnIndex++) {
              var columnContent = '';
              for (i = 0; i < stubsPerColumn; i++) {
                if(stubIndex < stubs.length){
                  columnContent += stubs[stubIndex] + '<br/>';
                }
                stubIndex++;
              }
              $(columns[columnIndex]).html(columnContent);
            }
          }else{
            $('#service_stub_possible_stubs').html(possibleStubs);
          }
        }
      }
    });
    $('#service_stub_execute').click(function () {
      var command = { 'command' : jQuery('#service_stub_command').val() };

      $.ajax({
        url: '/dataimport/index/runServiceStub',
        type: 'POST',
        data: command,
        success: function (data) {
          $('#service_stub_command').val('');
          $('#service_stub_message').html(data);
          $('#service_stub_use').mouseenter();
        }
      })
    })
  }


  // Always contract the left sidebar upon customer menu click
  $('.col-left.sidebar .sticker').on('click', '#customer-menu > li > a', function(){
    LeftSidebar.contractLeftSide($('#customer-menu > li.active > a').length > 0 && $(this).parent().hasClass('active') === false);
  });

  /*==============*** CUSTOMER DATA INIT ***==============*/
  $.each({
    'customer.load':'/customer/details/loadCustomer',
  }, function(key, url) {
    window.customer.addEndpoint(key, url);
  });
  window.customer.init();

  /*==============*** CUSTOMER-DE DATA INIT ***==============*/
  $.each({
    'customer.search': 'customerde/search/index',
    'customer.search.indirect':'customerde/search/indirect',
    'customer.load':'/customerde/details/loadCustomer',
    'customer.showExtendable': 'customerde/details/showExtendable'
  }, function (key, url) {
    window.customerDe.addEndpoint(key, url);
  });
  window.customerDe.init();


  // The code below is for the functionality of add package button
  // (hide/show)AddpackageButons functions are copied from omnius/init.js
  function addButtonClass($packageList) {
    var buttonsCount = $packageList.find('> div').length;
    $('#cart-spinner-wrapper').find('.cart_packages').addClass('control-buttons-' + buttonsCount);
  }

  function showAddPackageButtons() {
    var packageList = $('#package-types');
    addButtonClass(packageList);

    packageList.removeClass('hide');
  }

  function hideAddPackageButtons() {
    var packageList = $('#package-types');
    packageList.addClass('hide');
  }

  // Here we remove the existing eventListener on add package
  // button since we can't touch omnius/init.js
  $('#show-packages').off();

  // Here we reimplement the EventListener with the correct behaviour.
  $('#show-packages').on('click', function(event) {
    event.stopPropagation();

    function showMenu(open) {
      if (open) {
        showAddPackageButtons();
      } else {
        hideAddPackageButtons();
      }
    }

    if($('#package-types').hasClass('hide')) {
      function closeMenuOnDocumentClick(event) {
        const isDropdown = $(event.target).closest('#package-types').length;
        if (!isDropdown) {
          showMenu(false);
          $(document).off('click', closeMenuOnDocumentClick);
        }
      }
      $(document).on('click', closeMenuOnDocumentClick);
      showMenu(true);
    } else {
      showMenu(false);
    }
  });

window.removePackage = function(event, element, expanded) {
    expanded = 'undefined' !== typeof expanded ? expanded : false;
    if (!expanded && $('#expanded_checkout_btn[extendcartvalue="true"]').length) {
      expanded = true;
    }
    event.stopPropagation();
    event.preventDefault();
    var modal = $(element).parents('div.modal:first');
    modal.find('.modal-body').find('.after-click-block').removeClass('hide');
    modal.find('.modal-body').find('.before-click-block').addClass('hide');
    modal.find('.modal-title').addClass('hide');
    modal.find('.modal-footer button').addClass('hide');
    var blockPackageId = $(element).attr('data-pack-id-for-removal');
    var sideBlock = $(".side-cart[data-package-id='"+blockPackageId+"']");
    if (!blockPackageId) {
      console.log('Invalid package id: ', blockPackageId);
      return;
    }
    var checkoutButton = $('#configurator_checkout_btn');
    var classList = checkoutButton.attr('class') ? checkoutButton.attr('class') : [];
    var hasWarningClass = classList.indexOf("btn-warning");
    var oldDisabled = checkoutButton.prop('disabled');
    var aDeleteLink = $(sideBlock).find('.icons.delete a');
    aDeleteLink.removeAttr('onclick');
    var bundleId = sideBlock.data('bundle-id');
    // Reset the state of the tax toggle
    store.remove('btwState');
    try {
      customerSection.cancelMoveOffnet();
    } catch (error) {}
    var callback = function(data) {
      if (data.hasOwnProperty('showOneOfDealPopup') && data.showOneOfDealPopup) {
        $('#one-of-deal-modal').data('packageId', blockPackageId).modal();
        return;
      }
      if (window['configurator']) {
        window['configurator'].destroy();
      }
      sideBlock.remove();
      // Remove bundle hints
      if(typeof configurator != 'undefined') {
        configurator.closeBundleDetailsModal();
      }
      if (data) {
        var totals = $('.sidebar #cart_totals');
        if (data.totals && hasWarningClass && hasWarningClass != -1) {
          data.totals = data.totals.replace('btn-info ', 'btn-warning ');
        }
        if(data.hasOwnProperty('totals')) totals.replaceWith(data.totals);
        sidebar.correctPricesVisibility();
      }
      // If this is within the checkout, we need to refresh the page
      if (controller && controller == 'cart') {
        $(location).attr('href', MAIN_URL + 'checkout/cart/?step=saveOverview');
      } else {
        if (data.error) {
          showModalError(data.message);
        } else {
          window.quoteHash = data.quoteHash;
          window.salesId = data.salesId;
          window.processContextCode = data.processContextCode;
          // Reset packages indexes in right sidebar
          if (data.hasOwnProperty('packagesIndex') &&
              data['packagesIndex'] != null &&
              !$.isEmptyObject(data['packagesIndex'])
          ) {
            // Reassigning packages variables because it might be altered by dummy bundle package removal
            packages = $('.col-right .cart_packages .side-cart.block');
            $.each(packages, function(index, value){
              var initialPackageId = $(value).data('package-id');
              var newPackageId = data['packagesIndex'][initialPackageId];
              $(value).attr('data-package-id', newPackageId);
              $(value).data('package-id', newPackageId);
            });

            if (typeof window.configurator != 'undefined') {
              configurator['packageId'] = data['packagesIndex'][configurator['packageId']] ;
              if (data.rules) {
                // Refresh Cable rules
                configurator['cableRules'] = {'rules' : data.rules};
                configurator.reapplyRules();
              }
            }
            if (data.rightBlock != null) {
              if (data.packageType) {
                var rightBlock = $('div[data-package-type="' + data.packageType + '"]:visible').first();
                rightBlock.replaceWith(data.rightBlock);
                if (rightBlock.hasClass('active')) {
                  rightBlock.removeClass('active');
                }
                rightBlock.find('.block-container').trigger('click');
              } else {
                var rightBlock = jQuery('.cart_packages');
                rightBlock.replaceWith(data.rightBlock);
              }
            }
          } else {
            if (data.rightBlock != null) {
              var rightBlock = $('div[data-package-type].active:visible').first();
              rightBlock.replaceWith(data.rightBlock);
            }else{
              var remainPackages =  $('.cart_packages').find('.side-cart.block:visible');
              $.each(remainPackages, function(index, value){
                $(value).remove();
              });
              $('#package-types').removeClass('hide');
            }
          }
          $(aDeleteLink).attr('onclick', 'showCancelPackageModal(this)');
        }
      }

      // update create package buttons
      updateCartPackageLine();

      if (expanded) {
        if (!data.complete) {
          // If no packages are complete collapse the right sidebar
          $('.sidebar .enlarge').trigger('click');
        }

        if (data.expanded) {
          // Refresh the expanded cart content
          $('.col-right #right-panel-cart').remove();
          $('.col-right').append(data.expanded).find('#right-panel-cart').show();
        }
      }
    };
    if (!blockPackageId) {
      callback(data);
    } else {
      var postData = {'packageId': blockPackageId, 'btwState': jQuery('#cart_totals').data('state')};
      if (expanded) {
        postData['expanded'] = true;
      }

      // @todo on removal of bundle package remove entire bundle
      $.post(MAIN_URL + 'configurator/cart/removePackage', postData, function(data) {
        // clear registered bundle hints on window because of possible collisions for re-indexed package ids
        window.bundleHintShown = [];
        if(data.mustRemoveBundleChoice) {
          $('#bundle-options').remove()
        }

        callback(data);

        var packages = $('.col-right .cart_packages .side-cart.block');
        var packageTypes = $("#package-types");

        packages.each(function(){
          if($(this).hasClass('active')){
            $(this)
              .removeClass('active')
              .find('.title i.prs.pull-left')
              .removeClass('vfde-arrow-up')
              .addClass('vfde-arrow-down2');
          }
        });

        if(!packageTypes.hasClass('hide')){
          packageTypes.addClass('hide');
        }

        var fixedPackageExists = jQuery('.cart_packages').children('.side-cart[data-package-type*="int_tv_fixtel"]:visible').length;
        var serviceabilityCheck = jQuery('#serviceability-content:visible').length;

        // check which packages have been removed and update campaign offers buttons accordingly
        if (data.deletedPackageType === 'int_tv_fixtel' || (data.deletedPackageType === 'mobile' && data.hasFixed === false && serviceabilityCheck > 0)) {
          if ($('button.btn.btn-primary.fluid.campaigns-offers-item-footer-select[onclick*="int_tv_fixtel"]').length > 0)
            $('button.btn.btn-primary.fluid.campaigns-offers-item-footer-select[onclick*="int_tv_fixtel"]').each(function () {
              $(this).removeAttr('disabled');
            });
          if ($('.notification-banner.warning.new-ui-elements.fixed-package-in-cart').length > 0) {
            $('.notification-banner.warning.new-ui-elements.fixed-package-in-cart').hide();
          }
        } else if (data.deletedPackageType === 'mobile' && fixedPackageExists > 0) {
          if ($('button.btn.btn-primary.fluid.campaigns-offers-item-footer-select[onclick*="int_tv_fixtel"]').length > 0)
            $('button.btn.btn-primary.fluid.campaigns-offers-item-footer-select[onclick*="int_tv_fixtel"]').each(function () {
              $(this).attr("disabled", "disabled");
            });
          $('.notification-banner.warning.new-ui-elements.fixed-package-in-cart').show();
        }
        //ensure campaigns height is mantained when removing package
        window.campaign.equalizeHeight();

        setTimeout(function () {
          modal.modal('hide');
          modal.find('.modal-body').find('.after-click-block').addClass('hide');
          modal.find('.modal-body').find('.before-click-block').removeClass('hide');
          modal.find('.modal-title').removeClass('hide');
          modal.find('.modal-footer button').removeClass('hide');
        }, 1000);

        if(typeof configurator != null) {
          if (!data.clearConfigurator) {
            configurator.getEligibleBundles();
          }
        }
      });
    }
  };

  /* Set Bootstrap datepicker default language to NL */
  $.fn.datepicker.dates.nl = {
    days: ['zondag', 'maandag', 'dinsdag', 'woensdag', 'donderdag', 'vrijdag', 'zaterdag'],
    daysShort: ['zo', 'ma', 'di', 'wo', 'do', 'vr', 'za'],
    daysMin: ['zo', 'ma', 'di', 'wo', 'do', 'vr', 'za'],
    months: ['januari', 'februari', 'maart', 'april', 'mei', 'juni', 'juli', 'augustus', 'september', 'oktober', 'november', 'december'],
    monthsShort: ['jan', 'feb', 'mrt', 'apr', 'mei', 'jun', 'jul', 'aug', 'sep', 'okt', 'nov', 'dec'],
    today: 'Vandaag',
    monthsTitle: 'Maanden',
    clear: 'Wissen',
    weekStart: 1,
    format: 'dd-mm-yyyy'
  };
  $.fn.datepicker.defaults.language = 'nl';
});
function hideErrorMessages(elem) {
  // fix for identity number error message not hiding when valid input on change identity type
  if ($(elem).val() != ''
    && Validation.validate(document.getElementById(elem.attr('id'))) === true
  ) {
    $(elem).parent().find('.validation-advice').hide('slow');
  } else if ($(elem).val() == '') {
    $(elem).parent().find('.validation-advice').hide('slow');
  }
}
