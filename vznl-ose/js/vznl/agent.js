/*
 * Copyright (c) 2016. Dynacommerce B.V.
 */
(function ($) {
  window.Agent.prototype = $.extend(VEngine.prototype, {
    logout: function () {
      // If agent tries to logout while on Checkout delivery show confirmation modal
      if (checkoutDeliverPageDialog === true) {
        jQuery('#checkout_deliver_confirmation').data('function', 'agent.logout').appendTo('body').modal();
        return false;
      }

      var callback = function (data, status) {
        window.location.reload(true);
      };
      this.post('agent.logout', {}, callback, function () {
        alert('Failed to logout current agent');
      });
      store.remove('validateApiResponse');
      store.remove('serviceApiResponse');
      store.remove('serviceApiAddress');
      window.location.reload(true);// force XSRF token to refresh/recreate
    }
  });
})(jQuery);