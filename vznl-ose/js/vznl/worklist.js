(function ($) {

  OrderDetails = function (store, orderEdit) {

    var $this = this;
    this.baseURL = '/';
    this.checkoutController = '/checkout/index/';
    this.cartController = '/checkout/cart/';
    this.store = store;
    this.orderEdit = orderEdit;
    this.allowLockSteal = false;
    this.lockedByMsg = '';
    this.allowUnlock = false;

    this.cancelPendingOrder = function (orderNumber) {
      if (orderNumber) {
        $.ajax({
          async: true,
          type: 'POST',
          data: {'super_order': orderNumber},
          url: MAIN_URL + 'checkout/index/cancelPendingOrder',
          success: function (data) {
            if (data.error == false) {
              window.location.href = '/checkout/cart/exitChangeOrder';
            } else {
              showModalError(data.message);
            }
          },
          error: function () {
            console.log('Failed to cancel the order');
          }
        });
      }
    };

    this.takeOverLock = function (obj) {
      this.switchOrderLock($('.order-lock a'));
      $('#order-preview-container .takeover_lock').hide();
    };

    this.switchOrderLock = function (obj) {
      var state = $(obj).data('state');
      this.switchLock(obj, state);
    };

    this.switchLock = function (obj, state) {
      var self = this;
      var reverse = state == 'locked' ? 'unlocked' : 'locked';
      $(obj).addClass('disabled');
      if (state == 'unlocked') {
        if ($(obj).data('edited') == 1) {
          $('#order_unlock_modal').appendTo('body').modal();
          return false;
        }
      }
      var callback = function () {
        $.post(self.cartController + 'lockSuperorder', {
          state: reverse,
          orderId: $(obj).data('order-id')
        }, function (response) {
          if (response.error) {
            if (response.hasOwnProperty('locked')) {
              self.orderLockModal(response.message, response.disabled);
            } else {
              showModalError(response.message);
            }
          } else {
            if (response.hasOwnProperty('edited') && response.edited == 1) {
              window.location = self.baseURL;
            } else {
              self.toggleLockState(obj, state, false);
            }
          }
        });
      };
      if ($(obj).data('force')) {
        $.post(self.cartController + 'lockSuperorder', {
          state: 'locked',
          orderId: $(obj).data('order-id')
        }, function (response) {
          if (response.error) {
            if (response.hasOwnProperty('locked')) {
              self.orderLockModal(response.message, response.disabled);
            } else {
              showModalError(response.message);
            }
          } else {
            callback();
          }
          $('#cart-top .order-lock a').data('force', false);
        });
      } else {
        callback();
      }
    };

    this.orderLockModal = function (message, disabled) {
      var self = this;

      if (this.allowUnlock) {
        $('#order_lock_modal p.unlock-order').removeClass('hidden');
        $('#order_lock_modal .modal-body .text').text(message);
        $('#order_lock_modal .unlock-order').removeClass('hidden');
        if (self.allowLockSteal) {
          $('#order_lock_modal button.take-over-lock').removeClass('hidden');
          $('#order_lock_modal .modal-body .text-warning').removeClass('hidden');
          $('#order_lock_modal .unlock-order').addClass('hidden');
        } else {
          $('#order_lock_modal .modal-body .text-warning').addClass('hidden');
          $('#order_lock_modal button.take-over-lock').addClass('hidden');
        }
        $('#order_lock_modal .order-lock-dismiss').html(Translator.translate('Cancel'));
      } else {
        $('#order_lock_modal .unlock-order').addClass('hidden');
        $('#order_lock_modal .modal-body .text').text(message).removeClass('hidden');
      }

      if (disabled) {
        $('#order_lock_modal button.unlock-order').prop('disabled', 'disabled');
        $('#order_lock_modal button.take-over-lock').prop('disabled', 'disabled');
        $('#order_lock_modal .modal-body p.unlock-order').addClass('hidden');
        $('#order_lock_modal .modal-body .text-warning').addClass('hidden');
      }
      $('#order_lock_modal').appendTo('body').modal();
    };

    this.orderLockedByAgent = function () {
      var self = this;
      // Enable further, if an alert has to be shown for the current user about the take over.
      /*
      if (self.gotStolen) {
        $('#lock-stolen-model').appendTo('body').modal();
      } */
     if (self.allowUnlock) {
        $('#order-preview-container .takeover_lock').show();
        if (self.allowLockSteal) {
          $('#order-preview-container .take-over-lock-button').removeClass('hidden');
        } else {
          $('#order-preview-container .take-over-lock-button').addClass('hidden');
        }
      } else {
        $('#order-preview-container .takeover_lock').hide();
      }
    };

    this.setRefundMethod = function (obj) {
      var value = $(obj).find('option').filter(':selected').first().val();
      $('form#saveSuperOrder input[name=refund_method]').val(value);
    };

    this.toggleLockState = function (obj, state, disable) {
      if (!$(obj).length) {
        return;
      }
      var self = this;
      var reverse = state == 'locked' ? 'unlocked' : 'locked';
      var lockIcon = $('.order-lock .lock-icon');
      lockIcon.addClass(reverse).removeClass(state);
      $('#order-{0}-text'.format(state)).addClass('hide');
      $(obj).data('state', reverse).html($(obj).data('{0}-text'.format(reverse))+lockIcon[0].outerHTML);
      $('#order-{0}-text'.format(reverse)).removeClass('hide');
      if (disable) {
        $(obj).addClass('disabled');
      } else {
        $(obj).removeClass('disabled');
      }
      //if button onclick displayed the steal modal, make sure we always overwrite it
      if (!$(obj).data('onclick-changed')) {
        $(obj).data('onclick-changed', true);
        $(obj)[0].onclick = null;
        $(obj).click(function () {
          orderDetails.switchOrderLock(obj)
        });
        self.allowLockSteal = false;
        self.lockedByMsg = '';
      }
      $(obj).removeClass('disabled');
      var trig = $("#main_lock_btn").attr("data-trigger");
      if (trig != "")
        $(trig).trigger("click");
    };

    this.previewDisabled = function () {
      return (this.orderEdit && $('.order-lock button').length && $('.order-lock button').data('state') == 'locked' && !$('.order-lock button').hasClass('disabled')) || !(jQuery('.lock-icon.unlocked').length == 1);
    };

    this.showConfirmModal = function () {
      if ($this.previewDisabled()) {
        $this.toggleLockAction();
        return false;
      }
      jQuery('#order_confirm_modal').appendTo('body').modal();
    };

    this.showRefundModal = function () {
      if ($this.previewDisabled()) {
        $this.toggleLockAction();
        return false;
      }
      jQuery('#order_refund_modal').appendTo('body').modal();
      var method = jQuery('.refund_method_select');

      if (method.length) {
        method.trigger('change');
      }
    };

    this.toggleLockAction = function () {
      if (($('.lock-info-spinner').length == 1)) {
        // If any actions should be performed when the lock info is loading should go here
        return;
      }
      var self = this;
     if (self.allowLockSteal) {
        $('#main_lock_btn').click();
      } else {
        self.orderLockModal(self.lockedByMsg);
      }
    };

    this.registerReturn = function (packageId) {
      if (this.previewDisabled()) {
        jQuery("#main_lock_btn").attr("data-trigger", "#return_button_"+packageId);
        this.toggleLockAction();
        return false;
      } else {
        jQuery("#main_lock_btn").attr("data-trigger", "");
        var query = $('#order_register_return_' + packageId);
        query.appendTo('body').modal();

        // loading ui elements functionality
        if (window.loadDatePicker) window.loadDatePicker();
        jQuery('.select-wrapper').initMultiselectElement();
        jQuery('.input-wrapper').initInputElement();
      }
      return true;
    };

   this.loadDatePicker = function () {
         $(".return-date-js").datepicker({
           format: "dd-mm-yyyy",
           showOn: 'button',
           buttonText: 'date'
         }).datepicker('show').on('changeDate', function(){
             $(this).datepicker('hide');
         });
       return true;
   };
   
    this.changeDeliveryAddress = function () {
      if (this.previewDisabled()) {
        this.toggleLockAction();
        return false;
      } else {
        jQuery('#change_delivery_address').appendTo('body').modal();
      }
      return true;
    };

    this.saveChangeDeliveryAddress = function (obj) {
      setPageLoadingState(true);
      $.ajax({
        async: true,
        type: 'POST',
        data: obj.serialize(),
        url: MAIN_URL + 'checkout/index/saveChangeDeliveryAddress',
        success: function (data) {
          setPageLoadingState(false);
          if (data.error == true) {
            showModalError(data.message);
          } else {
            window.location.href = '/checkout/cart/';
          }
        },
        error: function () {
          console.log('Error!');
        }
      });
    };

    this.showRegisterReturnModalError = function (packageId, message) {
      var field = $('#order_register_return_error_' + packageId);
      if (message === false) {
        field.parent().addClass('hidden');
      } else {
        field.parent().removeClass('hidden');
        field.html(message);
      }
    };

    this.deliverOrder = function (orderId, packageId) {
      if (this.previewDisabled()) {
        jQuery("#main_lock_btn").attr("data-trigger", "#return_button_"+packageId);
        this.toggleLockAction();
        return false;
      }
      window.location.href = MAIN_URL + 'checkout/cart?orderId=' + orderId + '&packageId=' + packageId + '&delivery=true';
    };

    this.submitRefund = function (object, packageId, complete) {
      $.ajax({
        async: true,
        type: 'POST',
        data: object.serialize() + '&trigger_complete%5B' + packageId + '%5D=' + complete,
        url: MAIN_URL + 'checkout/index/registerReturn',
        success: function (data) {
          if (data.error) {
            orderDetails.showRegisterReturnModalError(packageId, data.message);
            orderDetails.registerReturn(packageId);
          } else {
            orderDetails.showRegisterReturnModalError(packageId, false);
            $('#return_button_' + packageId).parent().addClass('hidden');
            if (data.html) {
              $('#returned-note-' + packageId).html(data.html);
            }
            jQuery('.modal.in').modal('hide');
            showNotificationBar('Order was successfully returned', 'success');
            // Change the order confirmation message as stated in RFC-150663
            if (data.oldOrderMessage && data.oldOrderMessage.length) {
              $('#showOrderConfirmation #order-confirmation-old-order-message').html(data.oldOrderMessage);
            }
            if (data.polling_id) {
              setPageLoadingState(false);
              Polling.start('registerReturn', data.polling_id);
            }
          }
        },
        error: function () {
          console.log('Failed to retrieve the data');
        }
      });
    };

    this.cancelOneOffPackages = function () {
      if (this.previewDisabled()) {
        jQuery("#main_lock_btn").attr("data-trigger", "#cancel_package_"+packageId);
        this.toggleLockAction();
        return false;
      }
      jQuery("#main_lock_btn").attr("data-trigger", "");
      jQuery('#remove-one-of-deal-order-modal').modal();
    };

    this.cancelDeliveredPackage = function (packageId, orderId) {
      if (this.previewDisabled()) {
        jQuery("#main_lock_btn").attr("data-trigger", "#cancel_package_"+packageId);
        this.toggleLockAction();
        return false;
      }
      jQuery("#main_lock_btn").attr("data-trigger", "");
      // Because we share the same modal with Edit action, hide the edit text
      jQuery('#edit_delivered_package .modal-body #package_hardware_items_row').addClass('hidden');
      jQuery('#edit_delivered_package .modal-body #package_hardware_items').empty();
      jQuery('#edit_delivered_package .modal-body .change-text').hide();
      jQuery('#refund_reason_cancel').attr('name', 'refund_reason').removeClass('hidden');
      jQuery('#refund_reason_change').attr('name', '').addClass('hidden');
      jQuery('#edit_delivered_package').data('isCancel', 1).data('packageId', packageId).data('orderId', orderId).appendTo('body').modal();
      jQuery('#refund_reason_cancel select').trigger('change');
      var cancelSelect = '#refund_reason_cancel select[name="refund_reason"] option:selected';
      if(jQuery(cancelSelect).val()) {
        jQuery('#refund_reason_cancel .form-group .select-wrapper').removeClass('is-empty');
      }
    };

    this.cancelPackage = function (packageId, oneOff, orderId) {
      if (this.previewDisabled()) {
        jQuery("#main_lock_btn").attr("data-trigger", "#cancel_package_"+packageId);
        this.toggleLockAction();
        return false;
      }
      jQuery("#main_lock_btn").attr("data-trigger", "");
      if (customer) {
        customer.setLoading();
      }
      if (oneOff) {
        packageId = jQuery('#all-package-ids').val();
      }
      jQuery.get(MAIN_URL + 'checkout/cart/removePackage?packageId=' + packageId, {}, true)
        .done(function (data) {
          // reload package contents
          customer.setLoading(false);
          if (orderId !== undefined) {
            loadOrderDetails({number: orderId});
          }
        });
      if (oneOff) {
        jQuery('#remove-one-of-deal-order-modal').modal('hide');
      }
    };

    this.editILT = function () {
      if (this.previewDisabled()) {
        this.toggleLockAction();
        return false;
      }
      jQuery('#edit_ilt').appendTo('body').modal();
      return true;
    }

    this.openIltPopdown = function (elm) {
      var $modalDiv = jQuery('#edit_ilt');
      $modalDiv.find('.radio-collection').removeClass('checked');
      jQuery(elm).parent().parent().parent().addClass("checked");      
    };

    var elements = document.getElementsByClassName("hidden-non-password");
    var internetExplorer = false;

    for (var i = 0; i < elements.length; i++) {
      var style = window.getComputedStyle(elements[i]);
      if (!style.webkitTextSecurity) {
        internetExplorer = true;
        elements[i].setAttribute("type", "password");
      }
    }

    this.showIncome = function () {
      if (internetExplorer) {
        $('input[name="incometest[income]"]').prop("type", "text");
      }

      $('input[name="incometest[income]"]').removeClass('hidden-non-password');
      $('#hideIncome').removeClass('hidden');
      $('#showIncome').addClass('hidden');
      return false;
    };

    this.hideIncome = function () {
      if (internetExplorer) {
        $('input[name="incometest[income]"]').prop("type", "password");
      }

      $('input[name="incometest[income]"]').addClass('hidden-non-password');
      $('#hideIncome').addClass('hidden');
      $('#showIncome').removeClass('hidden');
      return false;
    };

    this.showHousingCosts = function () {
      if (internetExplorer) {
        $('input[name="incometest[housing_costs]"]').prop("type", "text");
      }

      $('input[name="incometest[housing_costs]"]').removeClass('hidden-non-password');
      $('#hideHousingCosts').removeClass('hidden');
      $('#showHousingCosts').addClass('hidden');
      return false;
    };

    this.hideHousingCosts = function () {
      if (internetExplorer) {
        $('input[name="incometest[housing_costs]"]').prop("type", "password");
      }

      $('input[name="incometest[housing_costs]"]').addClass('hidden-non-password');
      $('#hideHousingCosts').addClass('hidden');
      $('#showHousingCosts').removeClass('hidden');
    };

    this.saveChangeILT = function (obj) {
      setPageLoadingState(true);
      $.ajax({
        async: true,
        type: 'POST',
        data: obj.serialize(),
        url: MAIN_URL + 'checkout/index/saveChangeIncomeTest',
        success: function (data) {
          setPageLoadingState(false);
          if (data.error == true) {
            showModalError(data.message);
          } else {
            window.location.href = '/checkout/cart/';
          }
        },
        error: function () {
          console.log('Error!');
        }
      });
    };

    this.editPackage = function (packageId, isDelivered) {
      if (this.previewDisabled()) {
        jQuery("#main_lock_btn").attr("data-trigger", "#edit_package_"+packageId);
        this.toggleLockAction();
        return false;
      }
      jQuery("#main_lock_btn").attr("data-trigger", "");

      var id = isDelivered ? 'edit_delivered_package' : 'edit_not_delivered_package';
      var $modalDiv = jQuery('#' + id);
      $modalDiv.data('packageId', packageId);

      if (isDelivered) {
        jQuery('#edit_delivered_package .modal-body .package_hardware_items_row').addClass('hidden');
        jQuery('#edit_delivered_package .modal-body #package_hardware_items').empty();
        $modalDiv.find('#refund_reason_change select').attr('name', 'refund_reason');
        $modalDiv.find('#refund_reason_change select option:eq(1)').attr('selected', 'selected');
        jQuery('#refund_reason_change div.select-items div:nth-child(1)').trigger('click');
        $modalDiv.find('#refund_reason_change').removeClass('hidden');
        $modalDiv.find('#refund_reason_cancel select').attr('name', '');
        $modalDiv.find('#refund_reason_cancel').addClass('hidden');
        // Reset form values for delivered packages
        if ($modalDiv.children('form').length) {
          $modalDiv.children('form')[0].reset();
        }
        $modalDiv.data('isCancel', 0).find('.package_hardware_items_row').addClass('hidden');
        $modalDiv.data('isCancel', 0).find('#package_hardware_items').empty();
        $modalDiv.find('.modal-body .change-text').show();
        $modalDiv.find('#refund_reason_change select').trigger('change');
      }

      $modalDiv.appendTo('body').modal();
    };

    this.submitChange = function (obj, async) {
      async = (async == true || async == 'undefined' || async == undefined);
      var self = this;
      var thisForm = obj;
      if (self.previewDisabled()) {
        self.toggleLockAction();
        return false;
      }
      thisForm.find('.ajax-validation').fadeOut('slow', function () {
        $(this).remove();
      });
      thisForm.find('.cart-errors').remove();
      var currentForm = thisForm.attr('id');
      var cartCurrentForm = new VarienForm(currentForm);

      var previewAction = thisForm.attr('action');
      var formData = thisForm.serializeObject();

      var nextStep = 'showOrderConfirmation';
      formData['current_step'] = nextStep;
      formData['is_offer'] = self.isOffer;

      Polling.show();
      enableLeavePageDialog();

      var previewMethod = previewAction;

      var that = this;
      setPageLoadingState(true);

      $.ajax({
        type: "POST",
        url: this.checkoutController + previewMethod,
        data: formData,
        async: async
      }).done(function (response) {
        // if server validation errors are returned, display them instead of showing modal error
        if (response.fields) {
          // hide this modal until further details of its existence at this point
          if ($('.prolongation-modal').hasClass('modal-dialog')) {
            $('.prolongation-modal').removeClass('modal-dialog').addClass('modal');
          }
        }

        self.previewAction = previewAction;

        if (!(response.fields) && response.error) {
          showModalError(response.message);
          setPageLoadingState(false);
          showNotificationBar(response.message, 'warning');
          return false;
        } else {
          var polling = new Polling('saveSuperOrder');
          $('#new-order-no').val(response.new_order);
          $('#polling-superorder-no').val(response.orders);
          window.Polling.polling_superorder_no = response.orders;
          polling.updatePolling(response.pool_id['0']);
          if (previewMethod == 'saveSuperOrder') {
            var creditStub = response.credit_fail == true;
            window.creditCheckData = {addresses: response.addresses, order_id: response.order_id, creditStub: creditStub, order_no: response.orders};
          }

          if (previewMethod == 'saveSuperOrder') {
            var isFixed = $('[name="isFixed"]').val();
            if(isFixed == 0) {
              $('#order-details-btn').addClass('hidden');
            }
            $('#order-increment-id').html('Order number: <strong>'+ response.orders+'</strong>');
            $('#order-number-copy').text(response.orders);
            $('#modal-order-number').text(Translator.translate('Order')+' '+response.orders+' '+Translator.translate('details'));
            $('#order-details-btn').attr('data-ordernumber', response.orders);
            jQuery("#checkout-confirm-view-order").attr('onclick', 'orderConfirmationViewOrder(event, ' + response.orders + ')');

            $('#order-confirmation-value').val(response.orders);
            if (response.newOrderMessage && response.newOrderMessage.length)
              $('#showOrderConfirmation #order-confirmation-new-order-message').html(response.newOrderMessage);
            if (response.oldOrderMessage && response.oldOrderMessage.length)
              $('#showOrderConfirmation #order-confirmation-old-order-message').html(response.oldOrderMessage);
          }

          if (response.new_order != undefined) {
            var isFixed = $('[name="isFixed"]').val();
            if(isFixed == 0) {
              $('#order-details-btn-new').addClass('hidden');
            }
            $('#new-order-increment-id').data('id', response.new_order_id);
            $('#new-order-increment-id').html('Order number: <strong>'+response.new_order+'</strong>');
            $('#order-number-copy-new').text(response.new_order);
            $('#order-details-btn-new').attr('data-ordernumber', response.new_order);
            $('.new_superorder').removeClass('new_superorder');
            $('#normal-order-message').hide();
            $('#modal-order-number').text(Translator.translate('Order')+' '+response.orders+' '+Translator.translate('details'));
            $("#checkout-confirm-view-order").attr('onclick', 'orderConfirmationViewOrder(event, ' + response.new_order + ')');
          }

          if (response.contract_page) {
            var contract_content = $('#contract_page');
            if ($('#contract_page') && contract_content) {
              contract_content.html(response.contract_page);
            }
          } else {
            $('#saveNumberSelection').parent().addClass('skip hidden');
            $('#saveContract').parent().parent().addClass('skip hidden');
            $('[data-ref="order-confirmation"]').show();
          }
          if(response.skip_number_selection) {
            window.hideContract = true;
          }
        }

      });
      return true;

    };
    this.openRadioPopdown = function (elm) {
      var $modalDiv = jQuery('#change_delivery_address');
      $modalDiv.find('.radio-item.radio-collection').removeClass('checked');
      jQuery(elm).parent().parent().parent().addClass("checked");      
    };

    $this.submitSuperOrder = function (message) {
      jQuery('.checkout-totals').find('button').addClass('disabled');
      var form = jQuery('form#saveSuperOrder');
      form.data('success-message', message);
      form.submit();
      if (jQuery("#cancelBeforeReturn").val() == 'true')
        jQuery('#order-status').val("cancel-before-return");
      else
        jQuery('#order-status').val("cancel");

    };

    this.restartDelivery = function (button, orderNumber, deliveryId) {

      if (this.previewDisabled()) {
        jQuery("#main_lock_btn").attr("data-trigger", "#restart_delivery_"+packageId);
        this.toggleLockAction();
        return false;
      }
      jQuery("#main_lock_btn").attr("data-trigger", "");
      $(button).closest('li').hide();

      cartController = '/checkout/cart/';

      $.post(cartController + 'restartHomeDelivery', {'orderNumber': orderNumber, 'deliveryId': deliveryId},
        function (response) {
          $(button).prop('disabled', true);
          $('button[data-delivery-id="{0}"]'.format(deliveryId)).prop('disabled', true);
          if (response.hasOwnProperty('serviceError')) {
            var msg = Translator.translate('Offering again the packages for home delivery has failed.') + '<br />';
            msg += Translator.translate('Error message:');
            msg += ' ' + response.message;
            showNotificationBar(msg, 'error');
            $(button).closest('li').show();
          } else {
            showNotificationBar(response.message, 'success');
            $('#error-modal').on('hidden.bs.modal', function (e) {
              setPageLoadingState(true);
            });
          }
        }
      );
    };
  };

  $('body').on('submit', 'form.preview', function (e) {
    e.preventDefault();
    e.stopPropagation();
    e.stopImmediatePropagation();
    // Validate the manual activation form
    if(orderDetails.submitChange($(this))) {
      return true;
    }
    return false;
  });


  $('#open-orders-content').on('click', '.open_order_cluster_type', function (e) {
    var clusterType = $(e.target).data('cluster');
    var resultsPerPage = $('#order_filter_results_per_page').val();
    loadOpenOrders(1, clusterType, resultsPerPage);
  });

  $('#open-orders-content').on('click', '#order_filter_results_per_page_submit', function () {
    var clusterType = $('.active-cluster').data('cluster');
    var resultsPerPage = $('#order_filter_results_per_page').val();
    loadOpenOrders(1, clusterType, resultsPerPage);
  });

  $('#open-orders-content').on('keypress', '#order_filter_results_per_page', function (e) {
    var clusterType = $('.active-cluster').data('cluster');
    var resultsPerPage = $('#order_filter_results_per_page').val();
    if (e.which == 13) {
      loadOpenOrders(1, clusterType, resultsPerPage);
    }
  });

  $('#top-menu-dropdown-container .dropdown-toggle').on('click', function (e) {
    e.preventDefault();
    e.stopPropagation();
    return false;
  });
  
  /* Closing open orders when clicked outside of the orders */
  $('#open-orders-content').on('click', function (e) {
    if (e.target !== this)
      return;
    $('a#open-orders-menu').trigger('click');
  });

  /* Closing Auth Modal and householdAuthModal when clicking outside of the modal */
  $('body').on('click', function (e) {
    if ($('#authModal').css('display') == 'block' || $('#householdAuthModal').css('display') == 'block') {
      if (e.target.id != 'authModal' || e.target.id != 'householdAuthModal') {
        customerDe.closeModal();
      }
    }
  });

  /* Closing worklist when clicked outside of the model */
  $(".cb_slide_panel").click(function(e){
    $("#"+e.target.id+"-content").slideUp();
    $('#top-panels').hide();
  }).on("click", ".inner", function (event) {
    event.stopPropagation();
  });

})(jQuery);
