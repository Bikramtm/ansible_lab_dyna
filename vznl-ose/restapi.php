<?php

error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', false);

if (isset($_SERVER['MAGE_IS_DEVELOPER_MODE'])) {
    ini_set('display_errors', true);
}
set_time_limit(1800);

if ($_SERVER['REQUEST_METHOD'] === 'HEAD') {
    die;
}

//Magento root is required for wkhtmltopdf generation
define('MAGENTO_ROOT', getcwd());

require_once(getcwd().'/app/Mage.php');
require_once(getcwd().'/vendor/autoload.php');
$request = Mage::app()->getRequest();

$storeView = $request->getHeader("StoreView") ?: Vznl_Agent_Model_Website::WEBSITE_WEB_SHOP_CODE;
$mageStore = Mage::getModel('core/website')->load($storeView);

if ($mageStore->getId()) {
    Mage::app()->setCurrentStore($mageStore->getDefaultStore()->getId());
    Mage::getSingleton('core/translate')->setLocale('nl_NL')->init('frontend'); // Init translations (FORCE NL)

    try {
        $restApi = new RestApi($request);
        $restApi->process();
    } catch (Exception $e) {
        echo $e->getMessage();
        throw new Exception($e->getMessage());
    }
} else {

    echo "Unknown store view : " . $storeView;
    throw new Exception("Unknown store view : " . $storeView);
}

/* ============================================================================ */

class RestApi
{
    protected $allowedMethods = array('GET', 'POST');
    protected $method       = 'GET';
    protected $logFileId;
    protected $outputFormat = 'json';
    protected $request;
    protected $response;
    protected $route;
    protected $responseBody;

    /**
     * @var string
     */
    protected $log;

    const LOG_ADYEN         = 'adyen.log';
    const LOG_FAILED_ORDERS = 'api_failed_orders.log';
    const LOG_RETRIEVE_CUSTOMER_CALLED_ORDERS = 'api_failed_orders.log';
    const LOG_API_PATH      = 'api';

    /**
     * Constructor
     * @param Mage_Core_Controller_Request_Http $request
     */
    public function __construct(Mage_Core_Controller_Request_Http $request)
    {
        $this->logFileId = date('YmdHis').'.'.uniqid().'.log';
        $this->request   = $request;
        $this->response  = Mage::app()->getResponse();
        $this->validateAuthentication();
        $this->initLogging();
    }

    public function __destruct()
    {
        $this->logData();
    }

    /**
     * Get the request object
     * @return Mage_Core_Controller_Request_Http
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * Get the response object
     * @return Mage_Core_Controller_Response_Http Object
     */
    protected function getResponse()
    {
        return $this->response;
    }

    /**
     * Get the client current IP
     * @return string
     */
    protected function getCurrentIp()
    {
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) { //behind proxy
            $currentIp = $_SERVER['HTTP_X_FORWARDED_FOR'];
            $currentIp = explode(',', $currentIp);
            $currentIp = trim(reset($currentIp));
        } else {
            $currentIp = $_SERVER['REMOTE_ADDR'];
        }

        return $currentIp;
    }

    /**
     * Validate whitelisting
     */
    protected function validateAuthentication()
    {
        $currentIp = $this->getCurrentIp();

        $allowedIPs = preg_split("/[\n\r]+/",
            Mage::getStoreConfig('dynaipsecurity/ipsecurityrestapi/allow'));
        foreach ($allowedIPs as $key => $ip) {
            if (trim($ip) == "") {
                unset($allowedIPs[$key]);
            }
        }

        $ipSecurity = new Dyna_IpSecurity_Model_Observer();
        if (!$ipSecurity->isIpAllowed($currentIp, $allowedIPs, false)) {
            die('IP "'.$currentIp.'" is not allowed');
        }
    }

    /**
     * Create the log files needed
     */
    protected function initLogging()
    {
        if (!file_exists(Mage::getBaseDir('log').DIRECTORY_SEPARATOR.self::LOG_ADYEN)) {
            $urlPath = '';
            $parts   = explode('/', self::LOG_ADYEN);
            for ($i = 0; $i < (count($parts) - 1); $i++) {
                $urlPath .= DIRECTORY_SEPARATOR.$parts[$i];
                mkdir(Mage::getBaseDir('log').$urlPath);
            }
            Mage::log('Initialize log file', null, self::LOG_ADYEN);
        }
        if (!file_exists(Mage::getBaseDir('log').DIRECTORY_SEPARATOR.self::LOG_FAILED_ORDERS)) {
            $urlPath = '';
            $parts   = explode('/', self::LOG_FAILED_ORDERS);
            for ($i = 0; $i < (count($parts) - 1); $i++) {
                $urlPath .= DIRECTORY_SEPARATOR.$parts[$i];
                mkdir(Mage::getBaseDir('log').$urlPath);
            }
            Mage::log('Initialize log file', null, self::LOG_FAILED_ORDERS);
        }
    }

    protected function logEnabled()
    {
        $route = $this->getRoute();
        if ($route['log']) {
            return (bool) Mage::helper('vznl_service')->getGeneralConfig('logging');
        }

        return false;
    }

    protected function logRequest()
    {
        if ($this->logEnabled()) {
            $this->log('Request Headers:');
            $this->log('IP: '.Mage::helper('core/http')->getRemoteAddr());
            $this->log('Endpoint: '.$this->getRequest()->getRequestUri());

            $getQuery = $this->request->getQuery();
            unset($getQuery['route']);
            $this->log('Params: '.json_encode($getQuery));
            $this->log('Date: '.date('Y-m-d H:i:s'));

            $this->log('');
            $this->log('Request Body:');
            $rawInput = file_get_contents("php://input");
            if(!empty($rawInput)){
                $tempBody = json_decode($rawInput);
                $body = json_encode($tempBody);
            } else {
                $body = json_encode($this->request->getPost());
            }
            $this->log($body);
            $this->log('');
        }
    }

    protected function logResponse()
    {
        if ($this->logEnabled()) {
            $this->log('Response Headers:');
            $this->log('Format: '.$this->outputFormat);
            $this->log('Response code: '.$this->getResponse()->getHttpResponseCode());
            $this->log('Date: '.date('Y-m-d H:i:s'));
            $this->log('');
            $this->log('Response Body:');
            $this->log($this->formatBody($this->getResponse()->getBody()));
        }
    }

    public function log($message)
    {
        $this->log .= $message."\n";
        return $this->log;
    }
    /**
     * @param $message
     * @param null $level
     * @param string $file
     * @param bool $forceLog
     */
    protected function logData()
    {
        $message = $this->log;
        $file           = $this->logFileId;
        static $loggers = array();

        if(!empty($message)) {
            try {
                if (!isset($loggers[$file])) {
                    $logDir = Mage::getBaseDir('log') . DS . self::LOG_API_PATH;
                    $logFile = $logDir . DS . $file;

                    if (!is_dir($logDir)) {
                        mkdir($logDir);
                        chmod($logDir, 0777);
                    }

                    if (!file_exists($logFile)) {
                        file_put_contents($logFile, '');
                        chmod($logFile, 0777);
                    }

                    $format = '%message%' . PHP_EOL;
                    $formatter = new Zend_Log_Formatter_Simple($format);
                    $writerModel = (string)Mage::getConfig()->getNode('global/log/core/writer_model');
                    if (!$writerModel) {
                        $writer = new Zend_Log_Writer_Stream($logFile);
                    } else {
                        $writer = new $writerModel($logFile);
                    }
                    $writer->setFormatter($formatter);
                    $loggers[$file] = new Zend_Log($writer);
                }

                if (is_array($message) || is_object($message)) {
                    $message = print_r($message, true);
                }
                $loggers[$file]->log($message, Zend_Log::DEBUG);
            } catch (Exception $e) {

            }
        }
    }

    /**
     * Get the route for this request
     * @return string
     */
    protected function getRoute()
    {
        if (is_null($this->route)) {
            $fullRoute          = $this->getRequest()->getParam('route');
            $parts              = explode('/', $fullRoute);
            $rootClassNameParts = array_splice($parts, 1, 1);
            $rootClass          = 'Vznl_'.get_class($this).'_'.ucfirst($rootClassNameParts[0]);

            if (@class_exists($rootClass)) {
                foreach ($rootClass::$routes as $key => $route) {
                    if ($this->getRouteMatch($route, $fullRoute)) {
                        if (strtolower($this->getRequest()->getMethod()) === strtolower($route['method'])) {
                            $this->route = $route;
                        } else {
                            $this->getResponse()->setHttpResponseCode(406);
                        }
                        break;
                    }
                }
            } else {
                $this->getResponse()->setHttpResponseCode(500);
            }
        }
        if (is_null($this->route)) {
            $this->getResponse()->setHttpResponseCode(501);
        }

        return $this->route;
    }

    /**
     * Match the route to all possible routers
     * @param array $route
     * @param string $url
     * @return boolean
     */
    protected function getRouteMatch($route, $url)
    {
        $partsInternal = explode('/', $route['path']);
        $partsUrl      = explode('/', $url);

        if (count($partsInternal) !== count($partsUrl)) {
            return false;
        }

        foreach ($partsInternal as $key => $value) {
            if ($value[0] === ':') {
                foreach ($route['validators'] as $validatorKey => $validator) {
                    if (':'.$validatorKey === $value) {
                        switch (strtolower($validator)) {
                            case 'integer':
                                if (!ctype_digit($partsUrl[$key])) {
                                    return false;
                                }
                                break;
                            case 'email':
                                if (!filter_var($partsUrl[$key],
                                    FILTER_VALIDATE_EMAIL)) {
                                    return false;
                                }
                                break;
                        }
                        continue;
                    }
                }
            } elseif ($value !== $partsUrl[$key]) {
                return false;
            }
        }

        return true;
    }

    /**
     * Process the api request
     * @return mixed
     */
    public function process()
    {
        $this->logRequest();
        if (!is_null($this->getRoute())) {
            $route = $this->getRoute();

            if (isset($route['active']) && $route['active']) {
                try {
                    if (is_subclass_of($route['class'], "Vznl_RestApi_Processor")) {
                        $class = new $route['class']($this->getRequest(), $route);
                    } else {
                        $class = new $route['class']($this->getRequest());
                    }

                    $this->responseBody = $class->process();
                } catch (Exception $e) {
                    $this->responseBody = array('type' => 'request', 'message' => $e->getMessage());
                }
            } else {
                $this->getResponse()->setHttpResponseCode(404);
                $this->log($route['class'].' is not active');
            }
        }
        return $this->output();
    }

    /**
     * Output the actual response to client
     * @return json string object
     */
    protected function output()
    {
        // Force to XML if no array / Text is only used for plain text messages
        if ($this->outputFormat === 'json' && !is_array($this->responseBody)) {
            $this->outputFormat = 'xml';
        }

        switch ($this->outputFormat) {
            case 'xml':
                $this->getResponse()->setHeader('Content-Type',
                    'application/xml');
                $this->getResponse()->setBody($this->formatBody($this->responseBody));
                break;
            case 'json':
                $this->getResponse()->setHeader('Content-Type',
                    'application/json');
                $this->getResponse()->setBody(Zend_Json::encode($this->responseBody));
                break;
            case 'text':
                $this->getResponse()->setHeader('Content-Type', 'text/plain');
                $this->getResponse()->setBody($this->responseBody);
                break;
            default:
                $this->getResponse()->setHeader('Content-Type', 'text/plain');
                $this->getResponse()->setBody('Output format not supported');
                break;
        }
        $this->logResponse();

        return $this->getResponse()->sendResponse();
    }

    protected function formatBody($body)
    {
        if($this->outputFormat === 'xml' && !empty($body)) {
            $dom = new DOMDocument();
            $dom->preserveWhiteSpace = false;
            $dom->formatOutput = false;
            $dom->loadXML($body);
            return $dom->saveXML();
        }
        return $body;
    }
}
