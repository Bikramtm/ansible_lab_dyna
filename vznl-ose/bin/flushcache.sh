#!/usr/bin/env bash

DB_HOST=$(xmllint --nocdata --xpath '/config/global/resources/default_setup/connection/host/text()' /vagrant/ose/app/etc/local.xml)
DB_USERNAME=$(xmllint --nocdata --xpath '/config/global/resources/default_setup/connection/username/text()' /vagrant/ose/app/etc/local.xml)
DB_PASSWORD=$(xmllint --nocdata --xpath '/config/global/resources/default_setup/connection/password/text()' /vagrant/ose/app/etc/local.xml)
DB_NAME=$(xmllint --nocdata --xpath '/config/global/resources/default_setup/connection/dbname/text()' /vagrant/ose/app/etc/local.xml)

VARNISH_IP=$(mysql -h $DB_HOST -u $DB_USERNAME -p$DB_PASSWORD $DB_NAME -se "SELECT value FROM core_config_data WHERE path = 'omnius_general/cache/varnish_ip'")
curl -X PURGE ${VARNISH_IP}
echo "- Flushed Varnish"

REDIS_SESSION_SERVER=$(xmllint --nocdata --xpath '/config/global/redis_session/host/text()' /vagrant/ose/app/etc/local.xml)
REDIS_SESSION_PORT=$(xmllint --nocdata --xpath '/config/global/redis_session/port/text()' /vagrant/ose/app/etc/local.xml)
REDIS_SESSION_DB=$(xmllint --nocdata --xpath '/config/global/redis_session/db/text()' /vagrant/ose/app/etc/local.xml)
echo "- Session cache found at: ${REDIS_SESSION_SERVER}:${REDIS_SESSION_PORT} at database ${REDIS_SESSION_DB}"
echo "- flushing Redis DB: ${REDIS_SESSION_DB}"
redis-cli -h $REDIS_SESSION_SERVER -p $REDIS_SESSION_PORT -n $REDIS_SESSION_DB flushdb

REDIS_CACHE_SERVER=$(xmllint --nocdata --xpath '/config/global/cache/backend_options/server/text()' /vagrant/ose/app/etc/local.xml)
REDIS_CACHE_PORT=$(xmllint --nocdata --xpath '/config/global/cache/backend_options/port/text()' /vagrant/ose/app/etc/local.xml)
REDIS_CACHE_DB=$(xmllint --nocdata --xpath '/config/global/cache/backend_options/database/text()' /vagrant/ose/app/etc/local.xml)
echo "- App cache found at: ${REDIS_CACHE_SERVER}:${REDIS_CACHE_PORT} at database ${REDIS_CACHE_DB}"
echo "- Flushing Redis DB: ${REDIS_CACHE_DB}"
redis-cli -h $REDIS_CACHE_SERVER -p $REDIS_CACHE_PORT -n $REDIS_CACHE_DB flushdb

echo "- Done"
