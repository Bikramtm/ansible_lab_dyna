<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Catalog_Model_Resource_Product_Attribute_Backend_Media extends Mage_Catalog_Model_Resource_Product_Attribute_Backend_Media
{
    /**
     * Insert gallery value for store to db
     *
     * @param array $data
     * @return Mage_Catalog_Model_Resource_Product_Attribute_Backend_Media
     */
    public function insertGalleryValueInStore($data)
    {
        //sometime when uploading images on master, slave was ok but master throws an error
        $data = $this->_prepareDataForTable(new Varien_Object($data), $this->getTable(self::GALLERY_VALUE_TABLE));
        if(isset($data['value_id']) && !$data['value_id']){
            return $this;
        }
        $this->_getWriteAdapter()->insert($this->getTable(self::GALLERY_VALUE_TABLE), $data);

        return $this;
    }
}