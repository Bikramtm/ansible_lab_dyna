<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Catalog_Model_Type
 */
class Omnius_Catalog_Model_Type
{
    const TYPE_MOBILE = 'Mobile';
    const TYPE_INTERNET = 'Internet';
    const TYPE_HARDWARE = 'Hardware';
    const TYPE_HYBRID = 'Hybride';
    const TYPE_GENERAL = 'General';

    const SUBTYPE_ACCESSORY = 'Accessoire';
    const SUBTYPE_ADDON = 'AddOn';
    const SUBTYPE_SIMCARD = 'Simcard';
    const SUBTYPE_SUBSCRIPTION = 'Subscription';
    const SUBTYPE_DEVICE = 'Device';
    const SUBTYPE_GENERAL = 'General';
    const SUBTYPE_PROMOTION = 'Promotion';
    const SUBTYPE_NONE = 'None';

    const NUMBER_PORTING_SUBSCRIPTION_VALUE = 1;
    const NUMBER_PORTING_PORTING_VALUE = 2;

    const NUMBER_PORTING_CURRENT_OPERATOR = "LTEL";
    const NUMBER_PORTING_CURRENT_PROVIDER = "LBTP";
}