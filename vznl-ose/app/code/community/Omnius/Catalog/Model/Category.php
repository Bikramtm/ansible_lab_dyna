<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Catalog_Model_Product
 */
class Omnius_Catalog_Model_Category extends Mage_Catalog_Model_Category
{
    /** @var Dyna_Cache_Model_Cache */
    protected $_cache;

    /**
     * Fetches all the products as an associative array where the
     * key is the product id and the value is the product name.
     * @return array
     */
    public function getCategoriesForDropdown()
    {
        $productsCollection = $this->getCollection()
            ->addAttributeToSelect('name');
        $selectable_collection = array(-1 => 'Please select a category..');
        foreach ($productsCollection as $product) {
            $selectable_collection[$product->getId()] = $product->getName();
        }

        return $selectable_collection;
    }

    /**
     * Init indexing process after category save
     *
     * @return Mage_Catalog_Model_Category
     */
    protected function _beforeSave()
    {
        if (!$this->getData('unique_id')) {
            $this->setData('unique_id', hash('sha256', (time() . rand(0, 100) . __FILE__)));
        }

        return parent::_beforeSave();
    }

    /**
     * Validate attribute values
     *
     * @throws Mage_Eav_Model_Entity_Attribute_Exception
     * @return bool|array
     */
    public function validate()
    {
        if ($this->getIsFamily() && ($errors = $this->validateProductsInFamily())) {

            return $errors;
        }

        return parent::validate();
    }

    /**
     * @return bool|array
     */
    public function validateProductsInFamily()
    {
        // Find all the other categories that are set as family
        if ($families = $this->getAllFamilyCategoryIds()) {
            // Get all the products within those families
            $otherProducts = $this->getProductIds($families);
            // Compare the products from this category and if there are common products return error
            $currentProducts = $this->getProductIds();
            if (array_intersect($currentProducts, $otherProducts)) {
                return ['is_family' => Mage::helper('catalog')->__('This category can not be a family, at least one product is part of another family.')];
            }
        }

        return false;
    }

    /**
     * Get a list of product ids that are associated with this category
     *
     * @return array
     */
    public function getProductIds($category = null)
    {
        if ($category === null) {
            $condition = ['eq' => $this->getId()];
        } elseif (is_array($category)) {
            $condition = ['in' => $category];
        } else {
            $condition = ['eq' => $category];
        }
        $key = md5(serialize([strtolower(__METHOD__), serialize($condition)]));
        if (!($result = unserialize($this->getCache()->load($key)))) {
            /** @var Mage_Catalog_Model_Resource_Category_Collection $collection */
            $collection = $this->getCollection()
                ->joinField('product_id', 'catalog/category_product', 'product_id', 'category_id=entity_id', null, 'left')
                ->addFieldToFilter('entity_id', $condition);
            $collection->getSelect()
                ->reset(Zend_Db_Select::COLUMNS)
                ->columns('at_product_id.product_id');

            $result = [];
            foreach ($collection->load() as $product) {
                if (trim($product['product_id'])) {
                    $result[$product['product_id']] = $product['product_id'];

                }
            }
            $this->getCache()->save(serialize($result), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
        }

        return $result;
    }

    /**
     * Get a list of product ids and skus that are associated with this category: $product[$productId]=$productSku;
     *
     * @return array
     * @param category int
     */
    public function getProductIdsAndSkus($categoryId)
    {
        $result = [];
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $query = "SELECT
                    t1.`category_id`,
                    t2.`entity_id`,
                    t2.`sku`
                  FROM
                    catalog_category_product t1
                  RIGHT JOIN catalog_product_entity t2
                  ON t1.`product_id` = t2.`entity_id`
                  WHERE t1.`category_id`='{$categoryId}'";
        $allRows = $readConnection->fetchAll($query);
        foreach ($allRows as $row) {
            $result[$row['entity_id']] = $row['sku'];
        }

        return $result;
    }

    /**
     * Get a list of product ids for a specified website
     *
     * @param $websiteId int
     * @return array
     */
    public function getWebsitesProductsId($websiteId)
    {
        $result = [];
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $query = "select product_id from catalog_product_website where website_id='{$websiteId}'";
        $allRows = $readConnection->fetchAll($query);
        foreach ($allRows as $row) {
            $result[$row['product_id']] = $row['product_id'];
        }

        return $result;
    }

    /**
     * Get a list of all categories that are family (if the current model has an id, that id is not returned)
     *
     * @return array
     */
    public function getAllFamilyCategoryIds()
    {
        $key = md5(serialize([strtolower(__METHOD__), $this->getId()]));
        if (!($result = unserialize($this->getCache()->load($key)))) {
            $result = [];
            /** @var Mage_Catalog_Model_Resource_Category_Collection $collection */
            $collection = $this->getCollection()->addAttributeToFilter('is_family', 1)->load();
            foreach ($collection as $category) {
                if ($category->getId() != $this->getId()) {
                    $result[] = $category->getId();
                }
            }
            $this->getCache()->save(serialize($result), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
        }

        return $result;
    }

    /**
     * Get a list of all categories that are family and mutually exclusive (if the current model has an id, that id is not returned)
     *
     * @return array
     */
    public function getAllExclusiveFamilyCategoryIds()
    {
        $key = md5(serialize([strtolower(__METHOD__), $this->getId()]));
        if (!($result = unserialize($this->getCache()->load($key)))) {
            $result = [];
            /** @var Mage_Catalog_Model_Resource_Category_Collection $collection */
            $collection = $this->getCollection()->addAttributeToFilter('is_family', 1)->addAttributeToFilter('allow_mutual_products', 1)->load();
            foreach ($collection as $category) {
                if ($category->getId() != $this->getId()) {
                    $result[] = $category->getId();
                }
            }
            $this->getCache()->save(serialize($result), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
        }

        return $result;
    }

    /**
     * @return Dyna_Cache_Model_Cache
     */
    protected function getCache()
    {
        if (!$this->_cache) {
            return $this->_cache = Mage::getSingleton('dyna_cache/cache');
        }

        return $this->_cache;
    }
}