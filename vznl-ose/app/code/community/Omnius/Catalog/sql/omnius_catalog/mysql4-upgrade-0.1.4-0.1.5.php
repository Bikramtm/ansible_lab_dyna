<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Eav_Model_Entity_Setup $this */
$this->startSetup();
$this->addAttribute('catalog_product', "priority", array(
    'type' => 'int',
    'input' => 'text',
    'label' => 'Priority (0 is lowest)',
    'group' => '',
    'sort_order' => 1000,
    'required' => false,
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'backend' => '',
    'default' => '0',
    'user_defined' => true,
));

// Add the attribute to the voice and data subscriptions sets
try {
    $addonAttributeSetId = $this->getAttributeSetId('catalog_product', 'addon');
    if ($addonAttributeSetId) {
        $this->addAttributeToSet('catalog_product', $addonAttributeSetId, 'General', 'priority', 11);
    }
} catch (Exception $e) {
    //Skip attribute set
}

try {
    $deviceSubscriptionAttributeSetId = $this->getAttributeSetId('catalog_product', 'device_subscription');
    if ($deviceSubscriptionAttributeSetId) {
        $this->addAttributeToSet('catalog_product', $deviceSubscriptionAttributeSetId, 'General', 'priority', 11);
    }
} catch (Exception $e) {
    //Skip attribute set
}

$this->endSetup();
