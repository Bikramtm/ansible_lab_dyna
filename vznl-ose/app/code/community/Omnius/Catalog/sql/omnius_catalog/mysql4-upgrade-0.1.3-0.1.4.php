<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Eav_Model_Entity_Setup $this */
$this->startSetup();

$this->addAttribute('catalog_category', 'unique_id', array(
    'type' => 'static',
    'label' => 'Unique Id',
    'input' => 'text',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible' => false,
    'required' => false,
    'user_defined' => false
));

$this->endSetup();
