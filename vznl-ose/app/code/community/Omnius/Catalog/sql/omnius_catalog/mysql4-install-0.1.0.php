<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Eav_Model_Entity_Setup $installer */
$installer = $this;
$installer->startSetup();

$installer->removeAttribute('catalog_product', 'created_by');

$installer->addAttribute('catalog_product', 'agent_groups_visibility', array(
    'group' => 'General',
    'label' => 'Visible to Groups',
    'note' => '',
    'type' => 'int',       //backend_type
    'input' => 'select',    //frontend_input
    'frontend_class' => '',
    'source' => 'omnius_catalog/attribute_source_dealerGroup',
    'backend' => '',
    'frontend' => '',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_WEBSITE,
    'required' => true,
    'visible_on_front' => true,
    'apply_to' => 'grouped',
    'is_configurable' => false,
    'used_in_product_listing' => false,
    'sort_order' => 5,
));

$installer->endSetup();
