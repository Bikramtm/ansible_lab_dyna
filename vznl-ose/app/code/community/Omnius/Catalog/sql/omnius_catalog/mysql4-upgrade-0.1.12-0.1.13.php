<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer->startSetup();

$attrCode = 'is_deleted';
$attrLabel = 'Is Deleted';

/** Add attribute */
$setup = Mage::getResourceModel('catalog/eav_mysql4_setup', 'core_setup');

$attributeModel = Mage::getModel('eav/entity_attribute')->loadByCode(Mage_Catalog_Model_Product::ENTITY, $attrCode);

if (!$attributeModel->getId()) {

    $setup->addAttribute(Mage_Catalog_Model_Product::ENTITY, $attrCode, array(
        'group'                => 'General',
        'type'                 => 'int',
        'label'                => 'Is Deleted',
        'note'                 => 'Is Product Deleted',
        'input'                => 'select',
        'source'               => 'eav/entity_attribute_source_boolean',
        'global'               => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'required'             => 0,
        'visible_on_front'     => 0,
        'used_for_price_rules' => 0,
        'adminhtml_only'       => 0,
    ));

    /** Updated existing products */

    $ids = Mage::getResourceModel('catalog/product_collection')->getAllIds();

    if (count($ids)) {
        $conn = $installer->getConnection();

        $sql = 'INSERT INTO `catalog_product_entity_int` (`entity_type_id`, `attribute_id`, `store_id`, `entity_id`, `value`) VALUES ';
        $attributeModel = Mage::getModel('eav/entity_attribute')->loadByCode(Mage_Catalog_Model_Product::ENTITY, $attrCode);
        $values = array();
        foreach ($ids as $id) {
            $row = sprintf('(%s, %s, %s, %s, %s)', 4, $attributeModel->getId(), 0, $id, 0);
            array_push($values, $row);
        }

        $sql .= join(',', $values);

        $conn->query($sql);
        unset($values, $sql);
    }
}


$setup->addAttribute(Mage_Catalog_Model_Category::ENTITY, 'is_family', array(
    'group'         => 'General Information',
    'sort_order'    => 2,
    'input'         => 'select',
    'source'        => 'eav/entity_attribute_source_boolean',
    'type'          => 'int',
    'label'         => 'Is Family',
    'backend'       => '',
    'visible'       => true,
    'required'      => false,
    'visible_on_front' => true,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
));

$installer->endSetup();
