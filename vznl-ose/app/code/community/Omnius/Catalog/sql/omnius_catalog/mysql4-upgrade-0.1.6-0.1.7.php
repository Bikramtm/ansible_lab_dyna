<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Eav_Model_Entity_Setup $installer */
$installer = $this;
$installer->startSetup();

if (! $installer->getAttributeId('catalog_product', 'terms_and_conditions')) {
    /** @var Omnius_Catalog_Helper_Setup $helper */
    $helper = Mage::helper('omnius_catalog/setup');

    $helper->createAttribute(
        'terms_and_conditions',
        'Terms and conditions',
        'textarea',
        array(
            'simple',
            'configurable',
            'virtual'
        )
    );

    $helper->addAttributeToAttributeSet('catalog_product', 'terms_and_conditions');
}

$installer->endSetup();
