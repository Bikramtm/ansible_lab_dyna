<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Eav_Model_Entity_Setup $installer */
$installer = $this;
$installer->startSetup();

/**
 * Update existing categories
 */

$updates = array();
$hashes = array();
foreach (Mage::getResourceModel('catalog/category_collection')->getData() as $category) {
    $hash = hash('sha256', $category['entity_id']);
    if (in_array($hash, $hashes)) { //if there's a change we have hash collisions
        $hash = substr_replace($hash, substr($hash, 0, 10), -10); //this should assure uniqueness in our limited set of categories
    }
    $updates[] = sprintf("UPDATE `catalog_category_entity` SET `unique_id`='%s' WHERE `entity_id`='%s';", $hash, $category['entity_id']);
    $hashes[] = $hash;
}
unset($hashes);
if (count($updates)) {
    $installer->getConnection()->query(join('', $updates));
}

$installer->endSetup();
