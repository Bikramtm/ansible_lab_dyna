<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Catalog_Block_Adminhtml_Deletedproducts_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * Init class
     */
    public function __construct()
    {
        $this->_blockGroup = 'omnius_catalog';
        $this->_controller = 'adminhtml_deletedproducts';

        parent::__construct();

        $this->_updateButton('save', 'label', $this->__('Save'));
        $this->_updateButton('delete', 'label', $this->__('Delete'));
    }

    /**
     * Get Header text
     *
     * @return string
     */
    public function getHeaderText()
    {
        if (Mage::registry('foo_bar')->getId()) {
            return $this->__('Edit');
        } else {
            return $this->__('New');
        }
    }
}
