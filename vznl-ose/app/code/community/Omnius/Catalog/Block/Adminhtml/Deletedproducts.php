<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Catalog_Block_Adminhtml_Deletedproducts extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'omnius_catalog';
        $this->_controller = 'adminhtml_deletedproducts';
        $this->_headerText = $this->__('Deleted Products');

        parent::__construct();
    }
}
