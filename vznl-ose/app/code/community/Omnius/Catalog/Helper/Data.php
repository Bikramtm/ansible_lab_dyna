<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Data
 */
class Omnius_Catalog_Helper_Data extends Mage_Core_Helper_Abstract
{
    /** @var Omnius_Configurator_Helper_Attribute */
    protected $_attrHelper;

    /** @var Dyna_Cache_Model_Cache */
    protected $_cache;

    /**
     * @param $types
     * @param Mage_Catalog_Model_Product $product
     * @return bool
     *
     * Determines if a product is of a certain type by comparing it's type to a provided array
     */
    public function is($types, Mage_Catalog_Model_Product $product)
    {
        if (!is_array($types)) {
            $types = array($types);
        }

        $key = serialize(array(__METHOD__, $types, $product->getId(), Mage::app()->getStore()->getWebsiteId()));
        if ($result = $this->getCache()->load($key)) {
            return $result === 'N;' ? false : true;
        }
        $productType = $product->getType();
        foreach ($types as $type) {
            if (in_array($type, $productType)) {
                $this->getCache()->save('b:1;', $key, array(Dyna_Cache_Model_Cache::PRODUCT_TAG), $this->getCache()->getTtl());

                return true;
            }
        }
        $this->getCache()->save('N;', $key, array(Dyna_Cache_Model_Cache::PRODUCT_TAG), $this->getCache()->getTtl());

        return false;
    }

    /**
     * @return Omnius_Configurator_Helper_Attribute
     */
    protected function getAttrHelper()
    {
        if (!$this->_attrHelper) {
            $this->_attrHelper = Mage::helper('omnius_configurator/attribute');
        }

        return $this->_attrHelper;
    }

    /**
     * Gets an associative array of the stock_status attribute id as keys and slugs as values.
     * @return array
     */
    public function getStockStatusSlugs()
    {
        $key = strtolower(__METHOD__);
        if (!($result = unserialize($this->getCache()->load($key)))) {
            $attributes = Mage::getResourceModel('eav/entity_attribute_collection')
                ->addFieldToFilter('attribute_code', 'stock_status')
                ->getFirstItem();
            $_collection = Mage::getResourceModel('eav/entity_attribute_option_collection')
                ->setStoreFilter(0)
                ->setAttributeFilter($attributes->getId())
                ->load();

            $optionsArray = array();
            foreach ($_collection as $option) {
                $optionsArray[$option->getOptionId()] = Mage::getModel('catalog/product_url')->formatUrlKey($option->getValue());
            }

            $result = count($optionsArray) > 0 ? $optionsArray : false;
            $this->getCache()->save(serialize($result), $key, array('stock_status_slugs'), $this->getCache()->getTtl());
        }

        return $result;
    }

    /**
     * @param $package
     * @return bool
     */
    public function checkPackageHasCtn($package)
    {
        if (!isset($package['ctn'])) {
            return false;
        }

        return (bool) $package['ctn'];
    }
    
    /**
     * @return Dyna_Cache_Model_Cache
     */
    protected function getCache()
    {
        if ( ! $this->_cache) {
            return $this->_cache = Mage::getSingleton('dyna_cache/cache');
        }
        return $this->_cache;
    }

    /**
     * @param $attribute
     * @param $attributeValueId
     * @param null $attributeValueLabel
     * @return mixed|string
     */
    public function getAttributeAdminLabel($attribute, $attributeValueId, $attributeValueLabel = null)
    {
        $key = $attribute->getAttributeCode() . '_values';
        $result = unserialize($this->getCache()->load($key)) ?: [];
        // if not already cached, retrieve all attribute admin values
        if (!$result) {
            $_collection = Mage::getResourceModel('eav/entity_attribute_option_collection')
                ->setStoreFilter(0)//to get the admin values
                ->setAttributeFilter($attribute->getId());
            $options = Mage::getSingleton('core/resource')
                ->getConnection('core_read')
                ->fetchAll($_collection->getSelect());
            unset($_collection);
            // store as value=>label array for easy access
            foreach ($options as $option) {
                $result[$option['option_id']] = $option['value'];
            }
            $this->getCache()->save(serialize($result), $key, array(Dyna_Cache_Model_Cache::PRODUCT_TAG), $this->getCache()->getTtl());
        }
        // if an attribute label is provided, return his value(id)
        if ($attributeValueLabel) {
            return array_search($attributeValueLabel, $result);
        }
        // For multiselects, the values may come as a comma seaparate string (e.g: 1,2,3)
        $attributeValues = explode(',', $attributeValueId);
        foreach ($result as $attributeValue => $attributeLabel) {
            if (!in_array($attributeValue, $attributeValues)) {
                unset($result[$attributeValue]);
            }
        }

        return json_encode($result);
    }

    /**
     * @return int
     * Return tax rate based on default selected country
     */
    public function getDefaultCountryTaxRate()
    {
        $key = serialize(array(__METHOD__, Mage::app()->getStore()->getWebsiteId()));
        if (!($result = unserialize($this->getCache()->load($key)))) {
            $oTaxRateCollection = Mage::getResourceModel('tax/calculation_rate_collection');
            $oTaxRateCollection->addFieldToFilter('tax_country_id', Mage::getStoreConfig('tax/defaults/country'));
            $item = $oTaxRateCollection->getFirstItem();

            $result = $item->getId() ? (float) $item->getRate() : 0;
            $this->getCache()->save(serialize($result), $key, array('CONFIG'), $this->getCache()->getTtl());
        }

        return $result;
    }

    /**
     * @param $stock
     * @param $backorders
     * @param $threshold
     * @param $ss
     * @param $stockAttribute
     * @param $stock_status_array
     * @param $product
     * @return bool
     */
    public function noStock($stock, $backorders, $threshold, $ss, $stockAttribute, $stock_status_array, $product)
    {
        if ($stock <= 0) {
            if (($stock - $backorders) + $threshold > 0) {
                $product->setStockStatus('sellable-backorder');
                $product->setStockStatusText(Mage::helper('checkout')->__('Long delivery time'));
                $product->setStockStatusClass('sellable-backorder');

                return true;
            } else {
                $product->setStockStatus('sellable-uitverkocht');
                $product->setStockStatusText(Mage::helper('checkout')->__('Sold Out'));
                $product->setStockStatusClass('sellable-uitverkocht');

                return false;
            }
        } else {
            Mage::helper('omnius_catalog')->setStocksStatus($ss, $stockAttribute, $stock_status_array, $product);

            return true;
        }
    }

    /**
     * @param $stock
     * @param $ss
     * @param $stockAttribute
     * @param $stock_status_array
     * @param $product
     * @return bool
     */
    public function endOfSale($stock, $ss, $stockAttribute, $stock_status_array, $product)
    {
        if ($stock <= 0) {
            $product->setStockStatus('sellable-uitverkocht');
            $product->setStockStatusText(Mage::helper('omnius_checkout')->__('Sold Out'));
            $product->setStockStatusClass('sellable-uitverkocht');

            return false;
        } else {
            Mage::helper('omnius_catalog')->setStocksStatus($ss, $stockAttribute, $stock_status_array, $product);

            return true;
        }
    }

    /**
     * @param $backorders
     * @param $threshold
     * @param $ss
     * @param $stockAttribute
     * @param $stock_status_array
     * @param $product
     * @return bool
     */
    public function comingSoon($backorders, $threshold, $ss, $stockAttribute, $stock_status_array, $product)
    {
        if ($backorders < $threshold) {
            Mage::helper('omnius_catalog')->setStocksStatus($ss, $stockAttribute, $stock_status_array, $product);

            return true;
        } else {
            $product->setStockStatus('sellable-uitverkocht');
            $product->setStockStatusText(Mage::helper('omnius_checkout')->__('Sold Out'));
            $product->setStockStatusClass('sellable-uitverkocht');

            return false;
        }
    }

    /**
     * @param $ss
     * @param $stockAttribute
     * @param $stock_status_array
     * @param $product
     */
    public function setStocksStatus($ss, $stockAttribute, $stock_status_array, $product)
    {
        $product->setStockStatus($ss);
        $product->setStockStatusText(Mage::helper('checkout')->__($stockAttribute->getFrontend()->getValue($product)));
        $product->setStockStatusClass($stock_status_array[$ss]);
    }

    /**
     * @param $attr
     * @param $filters
     * @param $store
     * @param $localeCode
     * @param $product
     * @return mixed
     */
    public function addFilter($attr, &$filters, $store, $localeCode, $product)
    {
        $storeLabel = $attr->getStoreLabel(Mage::app()->getStore()->getId());
        $attrCode = $attr->getAttributeCode();
        $attrValue = $attr->getFrontend()->getValue($product);
        $filters[$attrCode]['label'] = $storeLabel;
        if ($attr->getFrontendInput() == 'price') {
            $filters[$attrCode]['options'][$product->getData($attrCode)] = $store->convertPrice($attrValue, true, false);
        } elseif ($attr->getAttributeCode() == 'weight') {
            $filters[$attrCode]['options'][$product->getData($attrCode)] = Zend_Locale_Format::toFloat($attrValue, array('locale' => $localeCode, 'precision' => 0));
        } elseif ($attr->getFrontendInput() == 'multiselect') {
            // get all available labels
            $options = $attr->getSource()->getAllOptions(false, true);
            $labels = array();
            foreach ($options as $option) {
                $labels[$option['value']] = $option['label'];
            }
            // get filters attr ids
            $ids = explode(',', $product->getData($attrCode));
            sort($ids);
            foreach ($ids as $attributeId) {
                $filters[$attrCode]['options'][$attributeId] = isset($labels[$attributeId]) ? $labels[$attributeId] : '';
            }
        } else {
            $filters[$attrCode]['options'][$product->getData($attrCode)] = $attrValue;
        }
        $filters[$attrCode]['position'] = $attr->getPosition();

        return $filters;
    }

    /**
     * @param $websiteId
     * @param $subtype
     * @param $cache
     * @return mixed
     */
    public function getProductForStock($websiteId, $subtype, $cache)
    {
        $attributeCacheKey = $subtype . '_attribute_stock_admin_labels_' . $websiteId;
        if ($attributeCached = $cache->load($attributeCacheKey)) {
            $product = unserialize($attributeCached);
        } else {
            $attribute = Mage::getSingleton("eav/config")
                ->getAttribute('catalog_product', Omnius_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR);
            $product = Mage::helper('omnius_catalog')->getAttributeAdminLabel(
                $attribute,
                null,
                $subtype
            );
            unset($attribute);
            $cache->save(serialize($product), $attributeCacheKey, array(), $cache->getTtl());
        }

        return $product;
    }

    /**
     * @param $websiteId
     * @param $subtype
     * @param $cache
     * @param $product
     * @return mixed
     */
    public function getProductsForStock($websiteId, $subtype, $cache, $product)
    {
        $productsCacheKey = 'catalog_products_' . $subtype . '_stock_call_' . $websiteId;
        if ($productsCached = $cache->load($productsCacheKey)) {
            $products = unserialize($productsCached);
        } else {
            $productsColl = Mage::getResourceModel('catalog/product_collection')
                ->addAttributeToSelect(array('stock_status', 'axi_threshold'), true)
                ->addWebsiteFilter($websiteId)
                ->addAttributeToFilter('is_deleted', array('neq' => 1))
                ->addAttributeToFilter(Omnius_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR, array('eq' => $product));

            $products = Mage::getSingleton('core/resource')->getConnection('core_read')->fetchAssoc($productsColl->getSelect());
            unset($productsColl);
            $cache->save(serialize($products), $productsCacheKey, array(), $cache->getTtl());
            unset($productsCacheKey);
        }

        return $products;
    }

    /**
     * @param $result
     * @param $id
     * @param $item
     * @param $stocks
     * @return mixed
     */
    public function buildStockItem($result, $id, $item, $stocks)
    {
        $result[$id] = array(
            'stock_status_text' => null,
            'stock_status_class' => null
        );

        return $result;
    }

    /**
     * Get the list of names of all products
     *
     * @return mixed
     */
    public function getProductCollection()
    {
        $products = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToFilter('name', array('neq' => ''))
            ->addAttributeToFilter('type_id', array('eq' => 'simple'))
            ->addAttributeToSelect('name')
            ->setOrder('name', 'asc');
        $conn = Mage::getSingleton('core/resource')->getConnection('core_read');

        return $conn->fetchAssoc($products->getSelect());
    }

    /**
     * @param $stock
     * @param $backorders
     * @return bool
     */
    public function checkIfSellable($stock, $backorders, $product)
    {
        $stock = (int) $stock;
        $backorders = (int) $backorders;

        $ss = $product->getStockStatus();
        $threshold = $product->getAxiThreshold();

        if (!($stockAttribute = Mage::registry('catalog_product_stock_attr'))) {
            $stockAttribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', 'stock_status');
            Mage::unregister('catalog_product_stock_attr');
            Mage::register('catalog_product_stock_attr', $stockAttribute);
        }
        if (!($stock_status_array = Mage::registry('catalog_product_stock_status_arr'))) {
            $stock_status_array = Mage::helper('omnius_catalog')->getStockStatusSlugs();
            Mage::unregister('catalog_product_stock_status_arr');
            Mage::register('catalog_product_stock_status_arr', $stock_status_array);
        }

        if (Mage::getStoreConfig('general/store_information/dev_mode') == 0) {
            if ($stock_status_array[$ss] == 'sellable-voorradig') {
                $result = Mage::helper('omnius_catalog')->noStock($stock, $backorders, $threshold, $ss, $stockAttribute, $stock_status_array, $product);
            } elseif ($stock_status_array[$ss] == 'end-of-sale-op-op') {
                $result = Mage::helper('omnius_catalog')->endOfSale($stock, $ss, $stockAttribute, $stock_status_array, $product);
            } elseif ($stock_status_array[$ss] == 'new-coming-soon') {
                $result = Mage::helper('omnius_catalog')->comingSoon($backorders, $threshold, $ss, $stockAttribute, $stock_status_array, $product);
            } else {
                Mage::helper('omnius_catalog')->setStocksStatus($ss, $stockAttribute, $stock_status_array, $product);
                $result = false;
            }
        } else {
            Mage::helper('omnius_catalog')->setStocksStatus($ss, $stockAttribute, $stock_status_array, $product);
            $result = true;
        }

        return $result;
    }

    /**
     * @return mixed
     */
    public function getCodesCollection()
    {
        $codes = Mage::getSingleton('eav/config')
            ->getEntityType(Mage_Catalog_Model_Product::ENTITY)
            ->getAttributeCollection()
            ->addFieldToFilter('is_unique', true)
            // Exclude SKU as it was handled separately
            ->addFieldToFilter('attribute_code', array('neq' => 'sku'))
            ->addFieldToSelect('attribute_code')
            ->addFieldToSelect('frontend_input');

        return $codes;
    }
} 
