<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_RuleMatch_Model_Indexer_Rulematch
 */
class Omnius_RuleMatch_Model_Indexer_Rulematch extends Mage_Index_Model_Indexer_Abstract
{
    protected $_logfile = 'rulematch_indexer.log';

    protected $_splittableAttr = array(
        'sku',
        'category_ids',
    );

    /**
     * Get Indexer name
     *
     * @return string
     */
    public function getName()
    {
        return 'Sales Rules';
    }

    /**
     * Get Indexer description
     *
     * @return string
     */
    public function getDescription()
    {
        return 'Rule matches that determine if a product has applicable sales rules';
    }

    /**
     * @throws Exception
     */
    public function reindexAll()
    {
        $data = [];
        /** @var Mage_Rule_Model_Abstract $rule */
        foreach ($this->getRuleCollection()->getItems() as $rule) {
            $conditions = $this->extractConditions($rule->getConditions()->asArray(), $rule->getConditions()->getAggregator());
            $combinations = $this->buildCombinations($conditions, $rule->getId());
            // Handle for remove products flow

            if ($rule->getSimpleAction() == "process_remove_product") {
                $combinations['all'][0]['sku'] = isset($combinations['sku']) ?  $combinations['sku'] . "," . $rule->getPromoSku() : $rule->getPromoSku();
            }

            if (isset($combinations['any']) && isset($combinations['all'])) {
               $data = array_merge($data, $this->reindexAny($combinations));
            } else {
                $data = array_merge($data, $this->reindexOther($combinations));
            }
        }

        $writeAdapter = Mage::getSingleton('core/resource')->getConnection('core_write');
        /** @var Omnius_RuleMatch_Model_Mysql4_Rulematch $resourceModel */
        $resourceModel = Mage::getResourceModel('rulematch/rulematch');
        $tableName = $resourceModel->getTable('rulematch/rulematch');
        $acceptedCollumns = $writeAdapter->fetchCol(sprintf('DESCRIBE %s;', $tableName));

        $handled = array();
        try {
            $writeAdapter->truncateTable($tableName);
            $writeAdapter->beginTransaction();
            foreach ($data as $rule) {
                ksort($rule);
                $hash = md5(serialize($rule));
                if (isset($handled[$hash])) {
                    continue;
                    //skip already inserted records
                }
                if (!empty($rule['rule_id'])) {
                    if ($columns = $this->hasInvalidColumns($rule, $acceptedCollumns)) { //assure only allowed columns
                        $this->_log(sprintf('Resulted rule row has additional columns: %s', join(',', $columns)));
                        continue;
                    }
                    $writeAdapter->insert(Mage::getResourceModel('catalog/product')->getTable('rulematch/rulematch'), $rule);
                    $handled[$hash] = true;
                }
            }
            $writeAdapter->commit();
        } catch (Exception $e) {
            $writeAdapter->rollBack();
            $this->_log("\n" . $e->__toString(), Zend_Log::CRIT);
            throw $e;
        }
    }

    /**
     * Retrieves the sales rules collection.
     * @return Mage_Rule_Model_Resource_Rule_Collection_Abstract
     */
    protected function getRuleCollection()
    {
        return Mage::getResourceModel('salesrule/rule_collection')
            ->addFieldToFilter('is_active', true);
    }

    /**
     * @param array $condition
     * @param null $aggregator
     * @param array $result
     * @return array
     */
    protected function extractConditions(array $condition, $aggregator = null, array $result = array())
    {
        $result['any'] = array();
        $result['all'] = array();
        if (isset($condition['conditions']) && is_array($condition['conditions'])) {
            foreach ($condition['conditions'] as $cond) {
                if (isset($cond['aggregator'])) {
                    if (!$cond['value']) {
                        continue;
                        //ignore negation condition
                    }
                    $subConditions = $this->extractConditions($cond, $cond['aggregator']);
                    foreach ($subConditions as $k => $sub) {
                        $result[$k] = array_merge($result[$k], $subConditions[$k]);
                    }
                } else {
                    if (false !== strpos($cond['operator'], '<') || false !== strpos($cond['operator'], '>')) {
                        $cond['operator'] = '==';
                        $cond['value'] = null;
                    }
                    $result[$aggregator][] = array(
                        'attribute' => $cond['attribute'],
                        'operator' => $cond['operator'],
                        'value' => $cond['value'],
                    );
                }
            }
            if ($aggregator == 'any') {
                $prev = array();
                foreach ($result as $res) {
                    foreach ($res as $vals) {
                        $prev[] = $vals;
                    }
                }
                $result = array();
                $result['any'] = $prev;
            }
        } else {
            if (false !== strpos($condition['operator'], '<') || false !== strpos($condition['operator'], '>')) {
                $condition['operator'] = '==';
                $condition['value'] = null;
            }
            $result[$aggregator][] = array(
                'attribute' => $condition['attribute'],
                'operator' => $condition['operator'],
                'value' => $condition['value'],
            );
        }

        return $result;
    }

    /**
     * @param array $conditions
     * @param $ruleId
     * @return array
     */
    protected function buildCombinations(array $conditions, $ruleId)
    {
        $data = [];
        foreach ($conditions as $type => $values) {
            if ($type == 'any') {
                $data = $this->handleAllConditions($ruleId, $values);
            } else {
                $data = $this->handleOtherConditions($ruleId, $values);
            }
            $merger = array();
            if (isset($data['all']) && is_array($data['all'])) {
                foreach ($data['all'] as &$item) {
                    if (isset($item['process_context'])) {
                        $item['process_context'] = implode(",", $item['process_context']);
                    }
                    $merger = array_merge($merger, $item);
                }
            }

            $data['all'] = array($merger);
        }

        return $data;
    }

    /**
     * Prepares data for insertion in the indexer table.
     * @param array $ruleRow
     * @return array
     */
    protected function prepareData(array $ruleRow)
    {
        $rows = array($ruleRow);

        foreach ($this->_splittableAttr as $attrCode) {
            $rows = $this->splitData($rows, $attrCode);
        }

        return $rows;
    }

    /**
     * @param array $rows
     * @param $attrCode
     * @param string $separator
     * @return array
     */
    protected function splitData(array $rows, $attrCode, $separator = ',')
    {
        foreach ($rows as $i => $row) {
            if (isset($row[$attrCode])) {
                $categoryIds = explode($separator, $row[$attrCode]);
                unset($rows[$i]);
                foreach ($categoryIds as $catId) {
                    $row[$attrCode] = trim($catId);
                    $rows[] = $row;
                }
            }
        }

        return $rows;
    }

    /**
     * @param array $row
     * @param array $columns
     * @return array
     */
    protected function hasInvalidColumns(array $row, array $columns)
    {
        return array_diff(array_keys($row), array_values($columns));
    }

    /**
     * @param string $message
     * @param int $level
     */
    protected function _log($message, $level = Zend_Log::ERR)
    {
        Mage::log($message, $level, $this->_logfile, true);
    }

    protected function _construct()
    {
        $this->_init('rulematch/indexer_rulematch');
    }

    /**
     * Register indexer required data inside event object
     *
     * @param   Mage_Index_Model_Event $event
     */
    protected function _registerEvent(Mage_Index_Model_Event $event)
    {
    }

    /**
     * Process event based on event state data
     *
     * @param   Mage_Index_Model_Event $event
     */
    protected function _processEvent(Mage_Index_Model_Event $event)
    {
    }

    /**
     * @param int $ruleId
     * @param array $values
     * @return array
     */
    protected function handleAllConditions($ruleId, $values)
    {
        $anySubConditions = [];
        $allSubConditions = [];
        $processed = [];
        $data = [];

        foreach ($values as $condition) {
            $row = array('rule_id' => $ruleId);
            if (isset($condition['any'])) {
                $anySubConditions = array_merge($anySubConditions, $this->buildCombinations($condition, $ruleId));
            } elseif (isset($condition['all'])) {
                $allSubConditions = array_merge($allSubConditions, $this->buildCombinations($condition, $ruleId));
            } else {
                if (false === strpos($condition['operator'], '!')) {
                    $row[$condition['attribute']] = $condition['value'];
                    $data['any'][] = $row;
                    $processed[] = $row;
                }
            }
        }
        $r = array('rule_id' => $ruleId);
        foreach ($anySubConditions as $condition) {
            foreach ($condition as $subCondition) {
                foreach ($processed as $iniRow) {
                    $r = array_merge($iniRow, $subCondition);
                    $data['any'][] = $r;
                }
            }
        }
        foreach ($allSubConditions as $aCondition) {
            foreach ($aCondition as $subACondition) {
                $r = array_merge($r, $subACondition);
                $data['all'][] = $r;
            }
        }

        return $data;
    }

    /**
     * @param int $ruleId
     * @param array $values
     * @return array
     */
    protected function handleOtherConditions($ruleId, $values)
    {
        $anySubConditions = [];
        $allSubConditions = [];
        $processed = [];
        $data = [];

        $row = array('rule_id' => $ruleId);
        foreach ($values as $condition) {
            $row = array('rule_id' => $ruleId);
            if (isset($condition['any'])) {
                $anySubConditions = array_merge($anySubConditions, $this->buildCombinations($condition, $ruleId));
            } elseif (isset($condition['all'])) {
                $allSubConditions = array_merge($allSubConditions, $this->buildCombinations($condition, $ruleId));
            } else {
                if (false === strpos($condition['operator'], '!')) {
                    $row[$condition['attribute']] = $condition['value'];
                    $data['all'][] = $row;
                    $processed[] = $row;
                }
            }
            $r = array('rule_id' => $ruleId);
            foreach ($anySubConditions as $cond) {
                foreach ($cond as $subCondition) {
                    foreach ($processed as $iniRow) {
                        $r = array_merge($iniRow, $subCondition);
                        $data['any'][] = $r;
                    }
                }
            }
            foreach ($allSubConditions as $aCondition) {
                foreach ($aCondition as $subACondition) {
                    $r = array_merge($r, $subACondition);
                    $data['all'][] = $r;
                }
            }
        }
        $data['all'][] = array_filter($row);

        return $data;
    }

    /**
     * @param array $combinations
     * @return array
     */
    protected function reindexAny($combinations)
    {
        $data = [];
        foreach ($combinations['any'] as $combination) {
            foreach ($combinations['all'] as $subCombination) {
                $row = array_merge($combination, $subCombination);
                foreach ($this->prepareData($row) as $ruleRow) {
                    // better than array_flip(array_filter(array_flip($ruleRow)));
                    $data[] = array_filter(
                        $ruleRow,
                        function($k) {
                            return $k != false && $k !== 0 && $k !== '0';
                        },
                        ARRAY_FILTER_USE_KEY
                    );
                }
            }
        }

        return $data;
    }

    /**
     * @param array $combinations
     * @return array
     */
    protected function reindexOther($combinations)
    {
        $data = [];
        foreach ($combinations as $combination) {
            foreach ($combination as $row) {
                foreach ($this->prepareData($row) as $ruleRow) {
                    // better than array_flip(array_filter(array_flip($ruleRow)));
                    $data[] = array_filter(
                        $ruleRow,
                        function($k) {
                            return $k != false && $k !== 0 && $k !== '0';
                        },
                        ARRAY_FILTER_USE_KEY
                    );
                }
            }
        }

        return $data;
    }
}
