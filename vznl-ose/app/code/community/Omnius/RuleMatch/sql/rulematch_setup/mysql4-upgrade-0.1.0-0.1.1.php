<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Core_Model_Resource_Setup $this */
$installer = $this;
$installer->startSetup();

$this->getConnection()->addColumn($this->getTable('sales_rule_product_match'), 'dealer_group', [
    'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
    'unsigned' => true,
    'length' => 11,
    'default' => null,
    'nullable' => true,
    'required' => false,
    'comment' => "Dealer group",
]);

$installer->endSetup();
