<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Service_Model_Mapper_AbstractMapper
 */
abstract class Omnius_Service_Model_Mapper_AbstractMapper extends Varien_Object
{
    const IGNORED_VALUE_FOR_FIELDS = '@@@__DO_NOT_SAVE__@@@';

    /**
     * Maps the data on the given object and returns it
     *
     * @param array $response
     * @param Varien_Object $object
     * @return mixed
     */
    abstract public function map(array $response, Varien_Object $object);

    /**
     * @param array $properties
     * @param Varien_Object $object
     * @param array $response
     * @return Varien_Object
     */
    protected function mapProperties(array $properties, Varien_Object $object, array $response = array())
    {
        foreach ($properties as $property => $value) {
            if (is_callable($value)) {
                $valueToSet = $value($response, $object);
                if ($valueToSet !== static::IGNORED_VALUE_FOR_FIELDS) {
                    $object->{$this->methodize($property)}($valueToSet);
                }
            } elseif (is_scalar($value) && !empty($value)) {
                $valueToSet = $this->getDotAccessor()->getValue($response, $value);
                if ($valueToSet !== static::IGNORED_VALUE_FOR_FIELDS) {
                    $object->{$this->methodize($property)}($valueToSet);
                }
            }
        }

        return $object;
    }

    /**
     * @param $property
     * @return string
     */
    protected function methodize($property)
    {
        return lcfirst(str_replace(" ", "", ucwords(strtr($property, "_-", "  "))));
    }

    /**
     * @return Omnius_Service_Model_DotAccessor
     */
    public function getDotAccessor()
    {
        return Mage::getSingleton('omnius_service/dotAccessor');
    }

    /**
     * Assures that the given object is supported by the mapper
     *
     * @param $object
     * @return bool
     */
    abstract protected function isSupported($object);
}
