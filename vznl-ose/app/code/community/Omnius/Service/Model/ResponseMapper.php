<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Service_Model_ResponseMapper
 */
class Omnius_Service_Model_ResponseMapper extends Varien_Object
{
    /** @var array */
    protected $_registry = array();

    public function map($method, array $response = array(), Varien_Object $object)
    {
        /** @var Omnius_Service_Model_Mapper_AbstractMapper $mapper */
        $mapper = $this->getMapper($method);

        return $mapper->map($response, $object);
    }

    protected function getMapper($method)
    {
        $className = get_class($this);
        $cacheKey = md5(serialize([$className . "::" . __FUNCTION__, $method]));
        preg_match('/^(.*)_Model_/', $className, $parts);

        $prefix = $parts[0] . 'Mapper';

        $mapperClass = sprintf('%s_%s', $prefix, str_replace(" ", "", ucwords(strtr($method, "_-", "  "))) . 'Mapper');

        if (class_exists($mapperClass)) {
            try {
                $this->_registry[$cacheKey] = @new $mapperClass();
                if (!$this->_registry[$cacheKey]) {
                    Mage::log(sprintf('Failed to response mapper. Error: %s', json_encode(error_get_last())));
                    Mage::throwException('Failed to response mapper. Please review logs.');
                }
            } catch (Exception $e) {
                Mage::log($e->getMessage() . "\n" . $e->getTraceAsString(), null, strtolower('soap_debug_' . $method . '.log'));
                Mage::throwException('Failed to create response mapper. Please review logs.');
            }
        } else {
            throw new InvalidArgumentException(sprintf('Invalid type argument provided. No client class "%s" found.', $mapperClass));
        }

        return $this->_registry[$cacheKey];
    }
} 
