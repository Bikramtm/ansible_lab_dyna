<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Service_Model_Client
 * @TODO: integrate Job module
 */
abstract class Omnius_Service_Model_Client_Client extends SoapClient
{
    const SOAP_QUEUE_ID = 'default';
    const STATIC_USERNAME_BSL = 'OMNIUS';
    const CONFIG_PATH = 'omnius_service/';

    protected $callName = '';

    /** @var string */
    protected $_schemaDir = '/../etc/schema/';

    /** @var Omnius_Service_Helper_Request */
    protected $_requestBuilder;

    /** @var string */
    protected $_namespace;

    /** @var Omnius_Service_Model_Normalizer */
    protected $_normalizer;

    /** @var array */
    protected $_options = array();

    /** @var resource */
    protected $_context;

    /** @var null|string */
    protected $_wsdl = null;

    /** @var bool */
    protected $_loaded = false;

    /** @var string */
    protected $_soapDebug = '';

    /** @var bool */
    protected $_forceUseStubs = false;

    /** @var  string */
    protected $_lastResponse;

    /** @var bool */
    protected $logSoapException = false;

    /** @var bool */
    protected $prioritizeCall = false;

    /**
     * @param string|null $wsdl
     * @param array $options
     */
    public function __construct($wsdl = null, $options = array())
    {
        if ($wsdl) {
            $this->_wsdl = $wsdl;
        }
        $this->_context = stream_context_create();
        $this->_options = array_merge($options, array('stream_context' => $this->_context));
        $this->_soapDebug = 'services' . DS . 'soap_debug_%s.log';
    }

    /**
     * @param array $options
     * @return $this
     */
    public function getClient(array $options = [])
    {
        $this->_wsdl = $this->getConfig('wsdl');
        $this->_options = $options;

        $path = Mage::getModuleDir('etc', $this->getNamespace()) . DS . 'schema';

        $requestBuilder = $this->_getRequestBuilder();
        $requestBuilder->setSchemaDir($path);

        return $this;
    }

    /**
     * @return string
     */
    public function getSchemaDir()
    {
        return $this->_schemaDir;
    }

    /**
     * @param $schemaDir
     * @return $this
     * @throws InvalidArgumentException
     */
    public function setSchemaDir($schemaDir)
    {
        if (!$schemaPath = realpath($schemaDir)) {
            throw new InvalidArgumentException('Given path does not exist');
        }

        $this->_schemaDir = $schemaPath;

        return $this;
    }

    /**
     * @param bool $bool
     */
    public function setUseStubs($bool = true)
    {
        $this->_forceUseStubs = $bool;
    }

    /**
     * @param string $name
     * @param string $arguments
     * @return array|mixed
     * @throws Zend_Soap_Client_Exception
     * @throws Exception
     */
    public function __call($name, $arguments)
    {
        $this->callName = $name;
        $args = func_get_args();
        $isJob = isset($args[2]) ? $args[2] : false;

        try {
            return $this->proxyCall($name, $arguments, $isJob);
        } catch (Exception $e) {
            if (!is_array($arguments)) {
                $arguments = array($arguments);
            }
            $this->debugCall($e, $name, $arguments);
            $e->logSoapException = $this->logSoapException;

            throw $e;
        }
    }

    /**
     * @param $name
     * @param $arguments
     * @param bool $isJob
     * @throws Exception
     * @throws SoapFault
     * @throws Zend_Soap_Client_Exception
     * @return array|mixed
     */
    protected function proxyCall($name, $arguments, $isJob = false)
    {
        if ($this->getUseStubs()) {
            $requestData = $isJob ? $arguments : $this->buildRequest($name, $arguments[0]);
            $stubClient = $this->getStubClient();
            $stubClient->setNamespace($this->getNamespace());
            $responseData = $stubClient->call($name, ($isJob ? array($arguments) : $arguments));
            $this->logStubTransfer($requestData, $responseData);

            return $responseData;
        }

        $oldTimeout = ini_get('default_socket_timeout');
        $asyncTimeout = (int) $this->getHelper()->getGeneralConfig('async_timeout');
        $newTimeout = ($isJob && $asyncTimeout)
            ? $asyncTimeout
            : ($this->getHelper()->getGeneralConfig('timeout')
                ? (int) $this->getHelper()->getGeneralConfig('timeout')
                : 60);
        ini_set('default_socket_timeout', $newTimeout);

        $this->_init(); //load WSDL

        $logDone = false;
        try {
            if ($isJob) {
                $callArgs = array(new SoapVar($arguments, XSD_ANYXML));
            } else {
                $callArgs = $this->_prepareArguments($name, $arguments[0]);
            }

            $r = parent::__call($name, $callArgs);

            if ($r === null && $name == 'SearchAvailableLogicalResources') {
                $reg = '/\<\?xml-multiple\s(.*)\s?\?>(\n?)/';
                $content = $this->__getLastResponse();
                $r = $this->getXmlError(preg_replace($reg, '', $content));
                $r = $r->Body->SearchAvailableLogicalResourcesResponse;
            } elseif ($name == 'GetEligiblePricePlans') {
                $reg = '/\<\?xml-multiple\s(.*)\s?\?>(\n?)/';
                $content = $this->__getLastResponse();
                $r = $this->getXmlError(preg_replace($reg, '', $content));
                $r = $r->Body->GetEligiblePricePlansResponse;
            } elseif ($r === null && $name == 'CreateOrder') {
                $reg = '/\<\?xml-multiple\s(.*)\s?\?>(\n?)/';
                $content = $this->__getLastResponse();
                $r = $this->getXmlError(preg_replace($reg, '', $content));
                $r = $r->Body->ProcessOrderResponse;
            } elseif ($name == 'ReceiveResponseApprovePackage') {
                $reg = '/\<\?xml-multiple\s(.*)\s?\?>(\n?)/';
                $content = $this->__getLastResponse();
                $r = $this->getXmlError(preg_replace($reg, '', $content));
                $r = $r->Body->ApprovePackagesResponse;
            } elseif ($name == 'updateProductSettings') {
                $reg = '/\<\?xml-multiple\s(.*)\s?\?>(\n?)/';
                $content = $this->__getLastResponse();
                $r = $this->getXmlError(preg_replace($reg, '', $content));
                $r = $r->Body->UpdateProductSettingsResponse;
            }

            $response = $this->getNormalizer()
                ->normalizeKeys(
                    $this->getNormalizer()
                        ->objectToArray($r)
                );
        } catch (Exception $e) {
            ini_set('default_socket_timeout', $oldTimeout);
            $this->logTransfer(); //log request and response
            $e->logSoapException = $this->logSoapException;

            throw $e;
        }

        ini_set('default_socket_timeout', $oldTimeout);
        if (!$logDone) {
            $this->logTransfer(); //log request and response if not already logged
        }

        if (0 !== (int) $this->getAccessor()->getValue($response, 'status.response_code')) {
            if ($message = Mage::getModel('field/message')->getMessageForCode($response['status']['code'])) {
                $a = new Zend_Soap_Client_Exception($message->getResponseMessage(),
                    $response['status']['response_code']);
                $a->responseCode = $response['status']['code'];
                $a->logSoapException = $this->logSoapException;
                throw $a;
            } else {
                $this->throwSoapException($response['status']['description'] . ' - ' . $response['status']['code'],
                    $response['status']['response_code']);
            }
        } elseif ($this->getAccessor()->getValue($response, 'body.fault.code')) {
            $a = new Zend_Soap_Client_Exception(
                $this->getAccessor()->getValue($response, 'body.fault.detail.error.error_code') . ' - ' . $this->getAccessor()->getValue($response, 'body.fault.detail.error.error_message'),
                $this->getAccessor()->getValue($response, 'body.fault.detail.error.error_code')
            );
            $a->response = $response;
            $a->logSoapException = $this->logSoapException;
            throw $a;
        }

        if ($this->getHelper()->containsNode($this, 'ApprovePackagesResponse')) {
            if (!isset($response['process_result']['success']) || ($response['process_result']['success'] !== true && $response['process_result']['success'] !== 'true')) {
                $description = !empty($response['process_result']['description']) ? $response['process_result']['description'] : $response['process_result']['status'];
                $this->throwSoapException($description);
            }
        }

        /**
         * Special case for ProcessOrderResponse
         */
        if ($this->getHelper()->containsNode($this, 'ProcessOrderResponse')) {

            if (!isset($response['process_result']['success']) || ($response['process_result']['success'] !== true && $response['process_result']['success'] !== 'true')) {
                $description = !empty($response['process_result']['description']) ? $response['process_result']['description'] : $response['process_result']['status'];
                $this->throwSoapException($description, 0, true);
            }
            //check format to make sure that we have sales_ordernumber and packages in the response
            if (isset($response['process_result'])) {
                if (!isset($response['process_result']['status'])) {
                    if (!empty($response['process_result']['description'])) {
                        $this->throwSoapException(sprintf('Description: "%s"',
                            $response['process_result']['description']));
                    } else {
                        $this->throwSoapException('Invalid response format: no status found');
                    }
                }

                if (!in_array((string) $response['process_result']['status'], array(
                    'completed succesfully',
                    'completed with warnings',
                    'successfully completed',
                    'ProcessOrderCallSkipped'
                ))
                ) {
                    $this->throwSoapException(sprintf('Status: "%s" , Description: "%s"',
                        $response['process_result']['status'], $response['process_result']['description']));
                }

                if (!isset($response['order_result'])) {
                    $this->throwSoapException('Invalid response format: no order_result node found');
                } else {
                    if (!isset($response['order_result']['sales_ordernumber'])) {
                        $this->throwSoapException('Invalid response format: no sales_ordernumber node found');
                    }
                    if (!isset($response['order_result']['packages'])) {
                        $this->throwSoapException('Invalid response format: no packages node found');
                    }
                }
            } else {
                $this->throwSoapException('Invalid response format: no process_result node found');
            }
        }

        /**
         * Special case for ExecuteRequestWithLogin call
         */
        $responseValues = array_values($response);
        $responseValues = reset($responseValues);
        if (strlen($this->getAccessor()->getValue($responseValues, 'string.2'))) {
            $this->throwSoapException('ExecuteRequestWithLogin: ' . $responseValues['string'][2]);
        }

        return $response;
    }

    /**
     * Do nothing unless the client implements this function
     *
     * @return $this
     */
    protected function addWSAHeader()
    {
        return $this;
    }

    /**
     * @return bool
     */
    public function getUseStubs()
    {
        return $this->_forceUseStubs || $this->getHelper()->useStubs();
    }

    /**
     * @return Omnius_Service_Helper_Data
     */
    protected function getHelper()
    {
        return Mage::helper('omnius_service');
    }

    /**
     * @param $method
     * @param array $params
     * @return mixed
     */
    public function buildRequest($method, array $params = array())
    {
        return $this->_getRequestBuilder()->create(
            $this->getNamespace(),
            $method,
            array_merge($params, $this->_getDefaultParams())
        );
    }

    /**
     * @return Omnius_Service_Helper_Request
     */
    protected function _getRequestBuilder()
    {
        if (!$this->_requestBuilder) {
            $this->_requestBuilder = Mage::helper('omnius_service/request');
        }

        return $this->_requestBuilder;
    }

    /**
     * @return string
     */
    public function getNamespace()
    {
        if (!$this->_namespace) {
            preg_match('/(.*)_Model_Client$/', get_called_class(), $parts);
            if (isset($parts[1])) {
                $this->_namespace = $parts[1];
            } else {
                Mage::throwException(
                    sprintf('Client class (%s) does not follow naming convention', get_called_class())
                );
            }
        }

        return $this->_namespace;
    }

    /**
     * Should return the system wide defaults (applicationId, channelName, etc)
     * @return array
     */
    protected function _getDefaultParams()
    {
        return [];
    }

    /**
     * @return Omnius_Service_Model_Client_StubClient
     */
    protected function getStubClient()
    {
        return Mage::getSingleton('omnius_service/client_stubClient');
    }

    protected function logStubTransfer($requestData, $responseData)
    {
        $requestBody = str_replace(["\n","\r"], '', $requestData);
        $requestData = sprintf(
            "Request Headers:\n%s\n\nRequest Body:\n%s\n\n",
            '',
            print_r($requestBody, true)
        );
        $responseBody = str_replace(["\n","\r"], '', $responseData);
        $responseData = sprintf(
            "Response Headers:\n%s\n\nResponse Body:\n%s\n\n",
            '',
            var_export($responseBody, true)
        );

        $this->getLogger()->logTransfer(
            sprintf("%s\n%s", $requestData, $responseData),
            'services' . DS . $this->getNamespace(),
            $this->callName . '.' . sha1(rand(0, 1000) . time()),
            'log'
        );
    }

    /**
     * @return Omnius_Service_Model_Logger
     */
    protected function getLogger()
    {
        return Mage::getSingleton('omnius_service/logger');
    }

    /**
     * Lazy load WSDL so that when adding a call to
     * the job queue, no exception is thrown if the
     * service is down at that moment.
     */
    protected function _init()
    {
        if (!$this->_loaded) {
            /**
             * If in stub mode, don't try to connect to the service for the WSDL
             */
            if (!$this->getUseStubs()) {
                parent::__construct($this->_wsdl, $this->_options);
            }

            $this->_loaded = true;
        }
    }

    /**
     * @param $name
     * @param $arguments
     * @return array
     */
    protected function _prepareArguments($name, $arguments)
    {
        return array(new SoapVar($this->buildRequest($name, $arguments), XSD_ANYXML));
    }

    /**
     * @param $str
     * @return SimpleXMLElement
     * @throws Exception
     */
    protected function getXmlError($str)
    {
        try {
            $xml = new SimpleXMLElement($str);
            foreach ($xml->getNamespaces(true) as $ns => $uri) {
                $str = str_replace(sprintf('%s:', $ns), '', $str);
            }
            $xml = new SimpleXMLElement($str);

            return json_decode(json_encode($xml));
        } catch (Exception $e) {
            $e->logSoapException = $this->logSoapException;

            throw $e;
        }
    }

    /**
     * @return Omnius_Service_Model_Normalizer
     */
    protected function getNormalizer()
    {
        if (!$this->_normalizer) {
            $this->_normalizer = Mage::getSingleton('omnius_service/normalizer');
        }

        return $this->_normalizer;
    }

    protected function logTransfer()
    {
        $requestBody = $this->__getLastRequest();
        $requestBody = str_replace(["\n","\r"], '', $requestBody);
        $requestData = sprintf(
            "Request Headers:\n%s\n\nRequest Body:\n%s\n\n",
            $this->__getLastRequestHeaders(),
            $requestBody
        );
        $responseBody = $this->__getLastResponse();
        $responseBody = str_replace(["\n","\r"], '', $responseBody);
        $responseData = sprintf(
            "Response Headers:\n%s\n\nResponse Body:\n%s\n\n",
            $this->__getLastResponseHeaders(),
            $responseBody
        );
        $this->_lastResponse = $this->__getLastResponse();
        $this->getLogger()->logTransfer(
            sprintf("%s\n%s", $requestData, $responseData),
            'services' . DS . $this->getNamespace(),
            $this->callName . '.' . sha1(rand(0, 1000) . time()),
            'log'
        );
    }

    /**
     * @param $message
     * @param int $level
     */
    public function log($message, $level = Zend_Log::ERR)
    {
        Mage::log($message, $level, sprintf($this->_soapDebug, strtolower($this->getNamespace())), true);
    }

    protected function throwSoapException($message = '', $code = 0, $setFail = false)
    {
        $e = new Zend_Soap_Client_Exception($message, $code);
        $e->logSoapException = $this->logSoapException;
        if ($setFail) {
            // Set when we the response returns success false
            $e->setFailStatus = true;
        }
        throw $e;
    }

    /**
     * @return Omnius_Service_Model_DotAccessor
     */
    protected function getAccessor()
    {
        return Mage::getSingleton('omnius_service/dotAccessor');
    }

    /**
     * @param Exception $e
     * @param $name
     * @param array $arguments
     */
    protected function debugCall(Exception $e, $name, array $arguments)
    {
        $soapRequestHeaders = parent::__getLastRequestHeaders();
        $soapRequest = parent::__getLastRequest();
        if ($soapRequest) {
            $soapRequest = new Omnius_Core_Model_SimpleDOM($soapRequest);
            $soapRequest = $soapRequest->asPrettyXML();
        }
        $soapResponseHeaders = parent::__getLastResponseHeaders();
        $soapResponse = parent::__getLastResponse();
        if ($soapResponse) {
            $reg = '/\<\?xml-multiple\s(.*)\s?\?>(\n?)/';
            if (preg_match($reg, $soapResponse)) {
                $soapResponse = preg_replace($reg, '', $soapResponse);
            }
            $soapResponse = new Omnius_Core_Model_SimpleDOM($soapResponse);
            $soapResponse = $soapResponse->asPrettyXML();
        }
        if ($e) {
            $eMessage = $e->getMessage();
            $eTrace = $e->getTraceAsString();
            $dump = "\n";
            $dump .= sprintf("Exception (%s): %s\n", get_class($e), $eMessage);
            $dump .= sprintf("Trace: %s\n\n", $eTrace);
        }

        //Avoid warning: json_encode(): type is unsupported, encoded as null
        $options = $this->_options;
        unset($options['stream_context']);

        // Debug for error: `Warning: json_encode(): type is unsupported, encoded as null
        if (!is_array($this->_options) || json_encode($options) === false) {
            Mage::log('Empty Service/Model/Client/Client.php options: ' . var_export($options, true), null, 'debug_service_client.log', true);
            Mage::log('Trace: ' . mageDebugBacktrace(true, false, true), null, 'debug_service_client.log', true);
        }

        $dump .= sprintf("Client:\n%s\n\n", $this->jsonPretty(json_encode($options)));
        $dump .= sprintf("Method: %s\n\n", $name);
        $dump .= sprintf("Arguments:\n%s\n\n", $this->jsonPretty(json_encode($arguments)));
        $dump .= sprintf("Request headers:\n%s", $soapRequestHeaders);
        $dump .= sprintf("Request:\n%s\n\n", $soapRequest);
        $dump .= sprintf("Response headers:\n%s\n\n", $soapResponseHeaders);
        $dump .= sprintf("Response:\n%s", $soapResponse);
        $dump .= "\n\n\n\n";
        $this->log($dump, Zend_Log::CRIT);
    }

    /**
     * Pretty print json
     *
     * @param $json
     * @param string $istr
     * @return string
     */
    protected function jsonPretty($json, $istr = '  ')
    {
        $result = '';
        for ($p = $q = $i = 0; isset($json[$p]); $p++) {
            $json[$p] == '"' && ($p > 0 ? $json[$p - 1] : '') != '\\' && $q = !$q;
            if (!$q && strchr(" \t\n", $json[$p])) {
                continue;
            }
            if (strchr('}]', $json[$p]) && !$q && $i--) {
                strchr('{[', $json[$p - 1]) || $result .= "\n" . str_repeat($istr, $i);
            }
            $result .= $json[$p];
            if (strchr(',{[', $json[$p]) && !$q) {
                $i += strchr('{[', $json[$p]) === false ? 0 : 1;
                strchr('}]', $json[$p + 1]) || $result .= "\n" . str_repeat($istr, $i);
            }
        }

        return $result;
    }

    /**
     * @param $data
     * @return Varien_Object
     */
    public function normalize($data)
    {
        return $this->getNormalizer()->normalize($data);
    }

    /**
     * @param $key
     * @param $value
     * @return $this
     */
    public function setOption($key, $value)
    {
        $this->_options[$key] = $value;

        return $this;
    }

    /**
     * @param $key
     * @return $this
     */
    public function removeOption($key)
    {
        if (isset($this->_options[$key])) {
            unset($this->_options[$key]);
        }

        return $this;
    }

    /**
     * @param string $request
     * @param string $location
     * @param string $action
     * @param int $version
     * @param int $one_way
     * @return string
     */
    public function __doRequest($request, $location, $action, $version, $one_way = 0)
    {

        // Set the HTTP headers.
        stream_context_set_option(
            $this->_context,
            array('http' => array('header' => 'X-START-DATE: ' . date('D, d M Y H:i:s e')))
        );

        return (parent::__doRequest($request, $location, $action, $version, $one_way));
    }

    /**
     * @param string $config
     * @return string|null
     */
    protected function getConfig($config)
    {
        return Mage::getStoreConfig(static::CONFIG_PATH . $config);
    }

    /**
     * @return Dyna_Cache_Model_Cache
     */
    protected function getCache()
    {
        if (!$this->_cache) {
            $this->_cache = Mage::getSingleton('dyna_cache/cache');
        }

        return $this->_cache;
    }
}
