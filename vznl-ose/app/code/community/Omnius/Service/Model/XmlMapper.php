<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Service_Model_XmlMapper
 */
class Omnius_Service_Model_XmlMapper extends Mage_Core_Model_Abstract
{
    /**
     * @param array $data
     * @param SimpleXMLElement $xml
     * @param bool $throw
     * @return SimpleXMLElement
     */
    public static function map(array $data = array(), SimpleXMLElement &$xml, $throw = false)
    {
        $callback = function ($paramName, $paramVal, &$el, $namespace = null) use ($throw) {
            $failed = true;
            if (is_callable($paramVal)) {
                $el[0] = $paramVal($el, $namespace);
                $failed = false;
            } elseif (is_array($paramVal)) {
                $el[0] = self::map($paramVal, $el[0]);
                $failed = false;
            } elseif (is_scalar($paramVal)) {
                $failed = false;
                $el[0][0] = $paramVal;
            }
            if ($failed && $throw) {
                throw new InvalidArgumentException(sprintf('Invalid value given for key "%s". Expected callable, array or scalar, %s given.', $paramName, gettype($paramVal)));
            }
        };
        foreach ($data as $paramName => $paramVal) {
            $namespaces = $xml->getNamespaces('true');
            if (count($namespaces)) {
                foreach ($namespaces as $prefix => $uri) {
                    /**
                     * When the namespace prefix is empty
                     * generate a prefix based on the uri
                     * so that xpath does not complain
                     */
                    if (!$prefix) {
                        $prefix = md5($uri);
                    }
                    $xml->registerXPathNamespace($prefix, $uri);

                    if (false !== strpos($paramName, '/') && false === strpos($paramName, ':')) {
                        $xpath = sprintf("//%s:", $prefix) . str_replace("/", sprintf("/%s:", $prefix), $paramName);
                        if ($el = @$xml->xpath($xpath)) {
                            $callback($paramName, $paramVal, $el, $uri);
                        } elseif ($el = @$xml->xpath(sprintf("//%s", $paramName))) {
                            $callback($paramName, $paramVal, $el, $uri);
                        }
                    } else {
                        if ($el = @$xml->xpath(sprintf("//%s:%s", $prefix, $paramName))) {
                            $callback($paramName, $paramVal, $el, $uri);
                        } elseif ($el = @$xml->xpath(sprintf("//%s", $paramName))) {
                            $callback($paramName, $paramVal, $el, $uri);
                        }
                    }
                }
            } else {
                if ($el = @$xml->xpath(sprintf("//%s", $paramName))) {
                    $callback($paramName, $paramVal, $el);
                }
            }
        }

        return $xml;
    }
} 
