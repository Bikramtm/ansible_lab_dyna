<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Service_Helper_Request
 */
class Omnius_Service_Helper_Request extends Mage_Core_Helper_Data
{
    /** @var string */
    protected $_schemaDir = '/../etc/schema/';

    /** @var array */
    protected $_schemaCache = array();

    /** @var array */
    protected $_registry = array();


    public function __construct()
    {
        $this->_schemaDir = realpath(__DIR__ . $this->_schemaDir);
    }

    /**
     * @param $schemaDir
     * @return $this
     * @throws InvalidArgumentException
     */
    public function setSchemaDir($schemaDir)
    {
        if (!$schemaPath = realpath($schemaDir)) {
            throw new InvalidArgumentException('Given path does not exist');
        }

        $this->_schemaDir = $schemaPath;

        return $this;
    }

    /**
     * @return string
     */
    public function getSchemaDir()
    {
        return $this->_schemaDir;
    }

    /**
     * @param $namespace
     * @param $method
     * @param array $params
     * @return mixed
     */
    public function create($namespace, $method, array $params = array())
    {
        $schema = $this->_loadSchema($namespace, $method);

        $this->_injectParams($schema, $params);

        $request = trim(str_replace('<?xml version="1.0"?>', '', $schema->asPrettyXML()));

        return $this->convertEncoding($request);
    }

    /**
     * @param SimpleXmlElement $schema
     * @param array $params
     */
    protected function _injectParams(SimpleXmlElement &$schema, array $params = array())
    {
        /** @var Omnius_Service_Model_XmlMapper $xmlMapper */
        $xmlMapper = Mage::getSingleton('omnius_service/xmlMapper');
        $xmlMapper::map($params, $schema);
    }

    /**
     * @param $namespace
     * @param $method
     * @return mixed
     * @throws RuntimeException
     * @throws Zend_Soap_Client_Exception
     */
    protected function _loadSchema($namespace, $method)
    {
        $path = realpath($this->_schemaDir . DIRECTORY_SEPARATOR . $method . '.xml');

        if (!$path) {
            throw new \RuntimeException(sprintf(
                'No schema defined for method "%s" in directory "%s"',
                $method,
                $this->_schemaDir . DIRECTORY_SEPARATOR . $namespace
            ));
        }

        try {
            $xmlContent = trim(file_get_contents($path));
            $schemaXml = $this->convertEncoding($xmlContent);
            $result = new Omnius_Core_Model_SimpleDOM($schemaXml);
            unset($xmlContent);
            unset($schemaXml);
        } catch (\Exception $e) {
            throw new Zend_Soap_Client_Exception(sprintf('Invalid schema file found at "%s"'));
        }

        return $result;
    }

    /**
     * Converts encoding of string to other encoding.
     * Default encoding is UTF-8
     * @param string $string
     * @param string $encoding
     * @return string
     */
    public function convertEncoding($string, $encoding = 'UTF-8')
    {
        if (function_exists('iconv')) {
            return iconv(mb_detect_encoding($string, mb_detect_order(), true), $encoding, $string);
        } else {
            return mb_convert_encoding($string, $encoding, mb_detect_encoding($string, mb_detect_order(), true));
        }
    }
}
