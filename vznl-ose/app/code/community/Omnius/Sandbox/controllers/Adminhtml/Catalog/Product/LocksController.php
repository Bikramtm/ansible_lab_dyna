<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Adminhtml_Catalog_Product_LocksController
 */
class Omnius_Sandbox_Adminhtml_Catalog_Product_LocksController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Render the product tab locks template
     */
    public function indexAction()
    {
        Mage::register('product', Mage::getModel('catalog/product')->load($this->getRequest()->getParam('product_id')));
        $block = Mage::getBlockSingleton('sandbox/adminhtml_catalog_product_tab_locks');
        echo $block->toHtml();
    }

    /**
     * Initialize method
     * @return $this
     */
    protected function _initAction()
    {
        return $this;
    }

    /**
     * Patch for SUPEE-6285
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return true;
    }
}
