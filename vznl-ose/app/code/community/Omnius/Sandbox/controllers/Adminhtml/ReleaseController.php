<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Adminhtml_ReleaseController
 */
class Omnius_Sandbox_Adminhtml_ReleaseController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Initialize a certain action
     * @return $this
     */
    protected function _initAction()
    {
        $this->loadLayout()->_setActiveMenu('sandbox/release')->_addBreadcrumb(Mage::helper('adminhtml')->__('Release  Manager'), Mage::helper('adminhtml')->__('Release Manager'));
        return $this;
    }

    /**
     * Render the layout on default index action
     */
    public function indexAction()
    {
        $this->_title($this->__('Sandbox'));
        $this->_title($this->__('Manager Release'));

        $this->_initAction();
        $this->renderLayout();
    }

    /**
     * Trigger edit mode for a record
     */
    public function editAction()
    {
        $this->_title($this->__('Sandbox'));
        $this->_title($this->__('Release'));
        $this->_title($this->__('Edit Item'));

        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('sandbox/release')->load($id);
        if ($model->getId()) {
            $model->setDeadline(Mage::getModel('core/date')->date('Y-m-d H:i:s', $model->getDeadline()));
            Mage::register('release_data', $model);
            $this->loadLayout();
            $this->_setActiveMenu('sandbox/release');
            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Release Manager'), Mage::helper('adminhtml')->__('Release Manager'));
            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Release Description'), Mage::helper('adminhtml')->__('Release Description'));
            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock('sandbox/adminhtml_release_edit'))->_addLeft($this->getLayout()->createBlock('sandbox/adminhtml_release_edit_tabs'));
            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('sandbox')->__('Item does not exist.'));
            $this->_redirect('*/*/');
        }
    }

    /**
     * Initialize the addition of a new record
     */
    public function newAction()
    {
        $this->_title($this->__('Sandbox'));
        $this->_title($this->__('Release'));
        $this->_title($this->__('New Item'));

        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('sandbox/release')->load($id);

        $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }

        Mage::register('release_data', $model);

        $this->loadLayout();
        $this->_setActiveMenu('sandbox/release');

        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

        $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Release Manager'), Mage::helper('adminhtml')->__('Release Manager'));
        $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Release Description'), Mage::helper('adminhtml')->__('Release Description'));

        $this->_addContent($this->getLayout()->createBlock('sandbox/adminhtml_release_edit'))->_addLeft($this->getLayout()->createBlock('sandbox/adminhtml_release_edit_tabs'));

        $this->renderLayout();

    }

    /**
     * Persist the data in the database
     */
    public function saveAction()
    {
        $post_data = $this->getRequest()->getPost();
        if ($post_data) {
            if (isset($post_data['deadline'])) {
                $a = new DateTime(Mage::getSingleton('core/date')->gmtDate());
                $b = new DateTime(Mage::getSingleton('core/date')->date());
                $diff = $a->diff($b);
                $deadlineStr = $post_data['deadline'];
                $deadline = new DateTime($deadlineStr);
                if ($diff->invert) {
                    $deadline->add($diff);
                } else {
                    $deadline->sub($diff);
                }
                $post_data['deadline'] = $deadline->format('Y-m-d H:i:s');
                if(isset($post_data['is_heavy'])){
                    $post_data['is_heavy'] = 1;
                }else {
                    $post_data['is_heavy'] = 0;
                }

                if(isset($post_data['export_media'])){
                    $post_data['export_media'] = 1;
                }else {
                    $post_data['export_media'] = 0;
                }
            }

            try {
                $model = Mage::getModel('sandbox/release')
                    ->addData($post_data)
                    ->setId($this->getRequest()->getParam('id'))
                    ->save();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Release was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setReleaseData(false);

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setReleaseData($this->getRequest()->getPost());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }

        }
        $this->_redirect('*/*/');
    }

    /**
     * Deletes a certain provided record
     */
    public function deleteAction()
    {
        if ($this->getRequest()->getParam('id') > 0) {
            try {
                $model = Mage::getModel('sandbox/release');
                $model->setId($this->getRequest()->getParam('id'))->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }

    /**
     * Trigger the mass remove to delete multiple selected records
     */
    public function massRemoveAction()
    {
        try {
            $ids = $this->getRequest()->getPost('ids', array());
            foreach ($ids as $id) {
                $model = Mage::getModel('sandbox/release');
                $model->setId($id)->delete();
            }
            Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item(s) was successfully removed'));
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }
        $this->_redirect('*/*/');
    }

    /**
     * Trigger the mass process method for multiple selected records
     */
    public function massProcessAction()
    {
        Mage::register('bypass_replication', true, true); //disable replication while publishing
        try {
            $ids = array_filter(array_map('intval', $this->getRequest()->getPost('ids', array())));
            /** @var Omnius_Sandbox_Model_Mysql4_Release_Collection $collection */
            $collection = Mage::getResourceModel('sandbox/release_collection')
                ->addFieldToFilter('id', array('in' => $ids))
                ->addFieldToFilter('tries', array('lt' => Mage::helper('sandbox')->getTriesLimit()))
                ->load();
            /** @var Omnius_Sandbox_Model_Publisher $publisher */
            $publisher = Mage::getSingleton('sandbox/publisher');
            foreach ($collection->getItems() as $release) { /** @var Omnius_Sandbox_Model_Release $release */
                switch($release->getStatus()) {
                    case Omnius_Sandbox_Model_Release::STATUS_SUCCESS:
                    case Omnius_Sandbox_Model_Release::STATUS_RUNNING:
                        //do nothing
                        break;
                    case Omnius_Sandbox_Model_Release::STATUS_PENDING:
                    case Omnius_Sandbox_Model_Release::STATUS_ERROR:
                        $publisher->publish($release)->save();
                        break;
                    default:
                        break;
                }
            }

            Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Releases were successfully processed'));
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }
        $this->_redirect('*/*/');
    }

    /**
     * Export order grid to CSV format
     */
    public function exportCsvAction()
    {
        $fileName = 'release.csv';
        $grid = $this->getLayout()->createBlock('sandbox/adminhtml_release_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }

    /**
     *  Export order grid to Excel XML format
     */
    public function exportExcelAction()
    {
        $fileName = 'release.xml';
        $grid = $this->getLayout()->createBlock('sandbox/adminhtml_release_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }

    /**
     * Patch for SUPEE-6285
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return true;
    }
}
