<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Model_Shell
 */
class Omnius_Sandbox_Model_Shell
{
    /**
     * Detects if given command is available on the current system
     *
     * @param string $cmd
     * @return bool
     */
    public function commandExists($cmd)
    {
        try {
            return $this->exec(sprintf('%s %s', (PHP_OS == 'WINNT') ? 'where' : 'which', $cmd)) != '';
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Executes given command and returns the output
     *
     * @param $cmd
     * @return string
     * @throws Exception
     */
    public function exec($cmd)
    {
        $process = proc_open(
            $this->prepare($cmd),
            array(
                0 => array("pipe", "r"), //stdin
                1 => array("pipe", "w"), //stdout
                2 => array("pipe", "w"), //stderr
            ),
            $pipes
        );

        if ($process === false) {
            throw new Exception(sprintf('Command "%s" could not be executed.', $cmd));
        }

        $stdout = trim(stream_get_contents($pipes[1]));
        $stderr = trim(stream_get_contents($pipes[2]));
        fclose($pipes[1]);
        fclose($pipes[2]);
        proc_close($process);

        return $stdout;
    }

    /**
     * @param $cmd
     * @return mixed
     */
    protected function prepare($cmd)
    {
        return $cmd;
    }
} 
