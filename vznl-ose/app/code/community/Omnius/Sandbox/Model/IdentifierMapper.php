<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Model_IdentifierMapper
 */
class Omnius_Sandbox_Model_IdentifierMapper
{
    /** @var array */
    protected $_mappers = array();

    /** @var bool */
    protected $_mappersGathered = false;

    /** @var array */
    protected $_cache = array();

    /**
     * @param string $table
     * @param string $column
     * @param string|int $value
     * @param Varien_Db_Adapter_Pdo_Mysql $masterAdapter
     * @param Varien_Db_Adapter_Pdo_Mysql $slaveAdapter
     * @param array $row
     * @throws Exception
     * @return string
     */
    public function map($table, $column, $value, $masterAdapter, $slaveAdapter, array $row = null)
    {
        /** @var Omnius_Sandbox_Model_Mapper $mapper */
        foreach ($this->getMappers() as $mapper) {
            if ($mapper->supports($table, $column)) {
                return $mapper->map($table, $column, $value, $masterAdapter, $slaveAdapter, $row);
            }
        }

        throw new Exception(sprintf('No valid mapper found for column "%s.%s"', $table, $column));
    }

    /**
     * @return array
     */
    public function getMappers()
    {
        if ( ! $this->_mappersGathered) {
            $this->gatherMappers();
        }
        return $this->_mappers;
    }

    /**
     * @return array
     */
    protected function gatherMappers()
    {
        $mappersPath = realpath(DS . trim(Mage::getModuleDir('Model', 'Omnius_Sandbox'), DS) . DS . join(DS, array('Model', 'Mapper')));

        $iterator = new RecursiveDirectoryIterator($mappersPath, FilesystemIterator::SKIP_DOTS);
        $iterator = new RecursiveIteratorIterator($iterator, RecursiveIteratorIterator::SELF_FIRST);

        $mappers = array();
        /** @var SplFileInfo $file */
        foreach ($iterator as $file) {
            if (preg_match('/\.php$/i', $file->getFilename())) {
                $mappers[] = Mage::getSingleton(sprintf('sandbox/mapper_%s', lcfirst(trim(preg_replace('/\.php$/i', '', $file->getFilename()), DS))));
            }
        }

        $mappers = array_filter($mappers);
        usort($mappers, function(Omnius_Sandbox_Model_Mapper $a, Omnius_Sandbox_Model_Mapper $b) {
            if ($a->getPriority() == $b->getPriority()) {
                return 0;
            }
            return ($a->getPriority() > $b->getPriority()) ? -1 : 1;
        });

        $this->_mappers = $mappers;
        unset($mappers);
        $this->_mappersGathered = true;
        return $this->_mappers;
    }
}
