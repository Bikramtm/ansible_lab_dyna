<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Model_ExportStrategy_ExportStrategy
 */
abstract class Omnius_Sandbox_Model_ExportStrategy_ExportStrategy
{
    /** @var int */
    protected $_batchCount = 250;

    /**
     * @return int
     */
    public function getBatchSize()
    {
        return $this->_batchCount;
    }

    /**
     * @param int $size
     * @return $this
     * @throws OutOfBoundsException
     */
    public function setBatchSize($size)
    {
        if (0 >= (int) $size) {
            throw new OutOfBoundsException('Size must be a higher value than zero');
        }
        $this->_batchCount = (int) $size;
        return $this;
    }

    /**
     * Executed when the export starts
     */
    public function start()
    {
    }

    /**
     * Executed when the export ends
     */
    public function end()
    {
    }

    /**
     * Executed after every batch
     *
     * @return mixed
     */
    public function afterBatch()
    {
    }

    /**
     * Executed for every item in the collection
     *
     * @param $item
     * @return mixed
     */
    abstract public function processItem($item);
}
