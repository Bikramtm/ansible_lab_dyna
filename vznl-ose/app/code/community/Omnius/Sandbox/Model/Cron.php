<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Model_Cron
 */
class Omnius_Sandbox_Model_Cron
{
    /** @var Omnius_Sandbox_Model_Publisher */
    protected $_publisher;

    /** @var Omnius_Sandbox_Model_Exporter */
    protected $_exporter;

    /** @var Omnius_Sandbox_Helper_Data */
    protected $_helper;

    /**
     * Retrieves all releases that follow the limits
     * set on the backend and tries to process them
     */
    public function processReleases()
    {
        foreach ($this->_getReleases() as $release) { /** @var Omnius_Sandbox_Model_Release $release */
            switch($release->getStatus()) {
                case Omnius_Sandbox_Model_Release::STATUS_SUCCESS:
                case Omnius_Sandbox_Model_Release::STATUS_RUNNING:
                    //do nothing
                    break;
                case Omnius_Sandbox_Model_Release::STATUS_PENDING:
                case Omnius_Sandbox_Model_Release::STATUS_ERROR:
                        $this->_getPublisher()
                            ->publish($release)
                            ->save();
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * Retrieves all exports that follow the limits
     * set on the backend and tries to process them
     */
    public function processExports()
    {
        foreach ($this->_getExports() as $export) { /** @var Omnius_Sandbox_Model_Export $export */
            switch($export->getStatus()) {
                case Omnius_Sandbox_Model_Export::STATUS_SUCCESS:
                case Omnius_Sandbox_Model_Export::STATUS_RUNNING:
                    //do nothing
                    break;
                case Omnius_Sandbox_Model_Export::STATUS_PENDING:
                case Omnius_Sandbox_Model_Export::STATUS_ERROR:
                    $this->_getExporter()
                        ->export($export)
                        ->save();
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * @return array
     */
    protected function _getReleases()
    {
        /** @var Omnius_Sandbox_Model_Mysql4_Release_Collection $releases */
        $releases = Mage::getResourceModel('sandbox/release_collection');
        $releases->getSelect()
            ->where(
                sprintf(
                    '(deadline <= TIMESTAMP("%s")) AND (tries < %s) AND (status NOT IN ("%s", "%s"))',
                    now(),
                    $this->_getHelper()->getTriesLimit(),
                    Omnius_Sandbox_Model_Release::STATUS_SUCCESS,
                    Omnius_Sandbox_Model_Release::STATUS_RUNNING
                )
            );
        return $releases->load();
    }

    /**
     * @return array
     */
    protected function _getExports()
    {
        /** @var Omnius_Sandbox_Model_Mysql4_Export_Collection $exports */
        $exports = Mage::getResourceModel('sandbox/export_collection');
        $exports->getSelect()
            ->where(
                sprintf(
                    '(deadline <= TIMESTAMP("%s")) AND (tries < %s) AND (status NOT IN ("%s", "%s"))',
                    now(),
                    $this->_getHelper()->getTriesLimit(),
                    Omnius_Sandbox_Model_Export::STATUS_SUCCESS,
                    Omnius_Sandbox_Model_Export::STATUS_RUNNING
                )
            );
        return $exports->load();
    }

    /**
     * @return Omnius_Sandbox_Model_Publisher
     */
    protected function _getPublisher()
    {
        if ( ! $this->_publisher) {
            return $this->_publisher = Mage::getSingleton('sandbox/publisher');
        }
        return $this->_publisher;
    }

    /**
     * @return Omnius_Sandbox_Model_Exporter
     */
    protected function _getExporter()
    {
        if ( ! $this->_exporter) {
            return $this->_exporter = Mage::getSingleton('sandbox/exporter');
        }
        return $this->_exporter;
    }

    /**
     * @return Omnius_Sandbox_Helper_Data
     */
    protected function _getHelper()
    {
        if ( ! $this->_helper) {
            return $this->_helper = Mage::helper('sandbox');
        }
        return $this->_helper;
    }
}
