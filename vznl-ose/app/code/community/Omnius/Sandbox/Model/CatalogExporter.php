<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Model_CatalogExporter
 */
class Omnius_Sandbox_Model_CatalogExporter
{
    const BATCH_SIZE = 250;

    /**
     * @param $collection
     * @param Omnius_Sandbox_Model_ExportStrategy_ExportStrategy $strategy
     * @param null $document
     * @return mixed
     */
    public function export($collection, Omnius_Sandbox_Model_ExportStrategy_ExportStrategy $strategy, $document = null)
    {
        if (func_num_args() === 3) {
            $args = func_get_args();
            $document = $args[2];
        }
        return $this->walk(
            $collection,
            array($strategy, 'processItem'),
            array($strategy, 'start'),
            array($strategy, 'end'),
            array($strategy, 'afterBatch'),
            $strategy->getBatchSize(),
            isset($document) ? $document : null
        );
    }

    /**
     * @param Varien_Data_Collection $collection
     * @param array $itemCallback
     * @param array $startCallback
     * @param array $endCallback
     * @param array $afterBatchCallback
     * @param null $batchSize
     * @param null $document
     * @return mixed
     */
    protected function walk(Varien_Data_Collection $collection,
                            array $itemCallback,
                            array $startCallback,
                            array $endCallback,
                            array $afterBatchCallback,
                            $batchSize = null,
                            $document = null
                            )
    {
        $batchSize = $batchSize ?: self::BATCH_SIZE;

        call_user_func($startCallback, $document);

        $collection->setPageSize($batchSize);

        $currentPage = 1;
        $pages = $collection->getLastPageNumber();

        do {
            $collection->setCurPage($currentPage);
            $collection->load();
            foreach ($collection as $item) {
                call_user_func($itemCallback, $item);
            }
            call_user_func($afterBatchCallback);
            $currentPage++;
            $collection->clear();
        } while ($currentPage <= $pages);

        return call_user_func($endCallback);
    }
}
