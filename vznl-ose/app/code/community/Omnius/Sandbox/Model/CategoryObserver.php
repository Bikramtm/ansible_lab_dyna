<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Model_CategoryObserver
 */
class Omnius_Sandbox_Model_CategoryObserver
{
    /**
     * Saves a reference to the deleted categories to ease replication
     *
     * @param Varien_Object $observer
     */
    public function onCategoryDeletion(Varien_Object $observer)
    {
        /** @var Dyna_Catalog_Model_Category $category */
        $category = $observer->getCategory();
        $regKey = sprintf('%s_%s', Omnius_Sandbox_Model_Sandbox::UNIQUE_ID_REFERENCE, 'catalog_category_entity');
        if ( ! ($deletedCategories = Mage::registry($regKey))) {
            $deletedCategories = array();
        }
        $deletedCategories[$category->getId()] = $category->getUniqueId();
        Mage::register($regKey, $deletedCategories, true);
    }
}
