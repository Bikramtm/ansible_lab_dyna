<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Model_Publisher
 */
class Omnius_Sandbox_Model_Publisher
{

    const UPDATE_SCRIPT_STATUS_START  = 0;
    const UPDATE_SCRIPT_STATUS_END    = 1;

    const CACHE_WARMER_SCRIPT = 'warm_cache.php';

    /** @var Varien_Db_Adapter_Interface */
    protected $_conn;

    /** @var array */
    protected $_databaseConfig = array();

    /** @var Omnius_Sandbox_Helper_Database */
    protected $_databaseHelper;

    /** @var Omnius_Sandbox_Helper_RemoteDatabase */
    protected $_remoteDatabaseHelper;

    /** @var Omnius_Sandbox_Model_Shell */
    protected $_shell;

    /** @var Omnius_Sandbox_Model_FileMapper */
    protected $_fileMapper;

    /** @var Omnius_Sandbox_Model_Sftp */
    protected $_sftpClient;

    /**
     * @param Omnius_Sandbox_Model_Release $release
     * @return Omnius_Sandbox_Model_Release
     */
    public function publish(Omnius_Sandbox_Model_Release $release)
    {
        set_time_limit(0);
        ini_set('max_execution_time', 0);

        if ($this->_lockRelease($release->getId())) {
            $release
                ->setStatus(Omnius_Sandbox_Model_Release::STATUS_RUNNING) //in order to not load the release again
                ->addMessage('Release locked')
                ->save();
            /** @var Omnius_Sandbox_Model_Replica $replica */
            $replica = Mage::getModel('sandbox/replica')->load($release->getReplica());
            if ( ! $replica || ($replica && !$replica->getId())) {
                return $release->addMessage(sprintf('Release references a non-existing replica (ID %s)', $replica->getId()), $release::MESSAGE_ERROR);
            }

            $release
                ->setExecutedAt(strftime('%Y-%m-%d %H:%M:%S', time()))
                ->save();
            try {
                /**
                 * Run indexers before dumping tables
                 * @var Dyna_Warmer_Model_Warmer_Indexing $indexersWarmer
                 */
                if ($indexersWarmer = Mage::getModel('warmer/warmer_indexing')) {
                    $release->addMessage('Starting indexers')->save();
                    Mage::dispatchEvent('warmer_cache_warmCache', ['model' => $indexersWarmer, 'verbose' => false, 'includeHeavy' => $release->getIsHeavy(), 'release' => $release]);
                    $release->addMessage('Indexers finished')->save();
                }

                /**
                 * Migrate tables to the replica
                 */
                $release
                    ->addMessage('Database tables migration started')
                    ->save();
                if ($replica->isRemote()) {
                    $release = $this->externalPublish($release, $replica);
                } else {
                    $publishCommand = $this->_getDbHelper()
                        ->createDumpCommand($this->_getDatabaseConfig(), $release->getTables(), $replica->getMysqlConfig());
                    $release
                        ->addMessage(sprintf('Executed command "%s"', $this->censorCommand($publishCommand)), $release::MESSAGE_INFO);

                    $this->executeAdditionalCommandsBeforeDBDump($release, $replica);

                    $this->_getShell()->exec($publishCommand);

                    $this->executeAdditionalCommandsAfterDBDump($release, $replica);

                    if ('success' != ($output = trim($this->_getShell()->exec($this->getCacheWarmerCommand($replica, $release->getIsHeavy()))))) {
                        throw new Exception(sprintf('Cache warmer command did not succeed. Output: %s', $output));
                    }

                    $release
                        ->addMessage('Database tables migration ended')
                        ->save();
                }

                /**
                 * Migrate media files to the replica
                 */
                if ($release->getExportMedia()) {
                    if ($replica->isRemote()) {
                        $release
                            ->addMessage('Media files migration started')
                            ->save();
                        $release = $this->externalMediaSync($release, $replica);
                        $release->addMessage('Media files migration ended');
                    } else {
                        $from = DS . trim(Mage::getBaseDir('media'), DS) . DS . 'catalog';
                        $to = DS . trim($replica->getEnvironmentPath(), DS) . DS . join(DS, array('media', 'catalog'));
                        $release
                            ->addMessage('Media catalog files migration started')
                            ->save();
                        $this->_getFileMapper()->mapMedia($from, $to, null, false);
                        $release->addMessage('Media catalog files migration ended');

                        $from = DS . trim(Mage::getBaseDir('media'), DS) . DS . 'bundles';
                        $to = DS . trim($replica->getEnvironmentPath(), DS) . DS . join(DS, array('media', 'bundles'));
                        $release
                            ->addMessage('Media bundles files migration started')
                            ->save();
                        $this->_getFileMapper()->mapMedia($from, $to, null, false);
                        $release->addMessage('Media bundles files migration ended');
                    }
                } else {
                    $release
                        ->addMessage('Media files migration disabled for this release. Skipping.')
                        ->save();
                }

                /**
                 * Update release version
                 */
                $versionUpdateCmd = sprintf(
                    "echo '<?php return \"%s\";' > %s",
                    $release->getVersion(),
                    join(DS, array(rtrim($replica->getEnvironmentPath(), DS), 'media', Dyna_Cache_Helper_Data::VERSION_FILE))
                );
                if ($replica->isRemote()) {
                    $this->_getSftp()->getConnection()->exec($versionUpdateCmd);
                } else {
                    $this->_getShell()->exec($versionUpdateCmd);
                }

                /**
                 * Clear Magento cache if is not an external publish
                 */
                $clearCacheCmd = sprintf(
                    "%s %s",
                    $replica->getPhpPath(),
                    join(DS, array(rtrim($replica->getEnvironmentPath(), DS), 'shell', 'cache.php'))
                );
                $release->addMessage('Clearing Magento cache', Omnius_Sandbox_Model_Release::MESSAGE_NOTICE);

                if ($replica->isRemote()) {
                    $output = $this->_getSftp()->getConnection()->exec($clearCacheCmd);
                } else {
                    $output = $this->_getShell()->exec($clearCacheCmd);
                }
                if (strlen($output)) {
                    $release->addMessage(sprintf('Could not clear Magento cache. Output: %s', $output), Omnius_Sandbox_Model_Release::MESSAGE_NOTICE);
                } else {
                    $release->addMessage('Successfully cleared Magento cache', Omnius_Sandbox_Model_Release::MESSAGE_NOTICE);
                }

                /**
                 * Clear Varnish
                 */
                if ($servers = $replica->getVarnishServers()) {
                    $cmd = join('; ', array_map(function($server) { return sprintf('(curl -X BAN %s > /dev/null 2>&1 &)', $server); }, $servers)) . ';';
                    $release->addMessage(sprintf('Clearing Varnish servers (%s) with command "%s"', join(',', $servers), $cmd), Omnius_Sandbox_Model_Release::MESSAGE_NOTICE);

                    if ($replica->isRemote()) {
                        $output = $this->_getSftp()->getConnection()->exec($cmd);
                    } else {
                        $output = $this->_getShell()->exec($cmd);
                    }
                    if (strlen($output)) {
                        $release->addMessage(sprintf('Could not clear Varnish cache. Output: %s', $output), Omnius_Sandbox_Model_Release::MESSAGE_NOTICE);
                    }
                }
                $release->save();

                //release update should only be executed on the master environment
                if ($replica->isRemote()) {
                    Mage::log('Starting update release at: '.date("Y-m-d H:i:s"), null, 'external_release.log', true);
                    $output = $this->_getShell()->exec($this->_getReleaseUpdateCmd($release->getId(), $replica->getId()));
                    Mage::log(var_export($output, true), null, 'external_release.log', true);
                }

                /**
                 * Assure the connection is close
                 */
                if ($replica->isRemote() && $this->_getSftp()->getConnection()) {
                    $this->_getSftp()->close();
                    $release
                        ->addMessage('SFTP connection closed')
                        ->save();
                }

                //remove releases will be finished by a shell script (see "/shell/update_release_status.php")
                if ( ! $replica->isRemote()) {
                    /**
                     * Return release after updating finish timestamp and status
                     */
                    return $release
                        ->setFinishedAt(strftime('%Y-%m-%d %H:%M:%S', time()))
                        ->setStatus(Omnius_Sandbox_Model_Release::STATUS_SUCCESS)
                        ->addMessage('Release successfully published', $release::MESSAGE_SUCCESS);
                }

                return $release;

            } catch (Exception $e) {
                return $release
                    ->addMessage($e->getMessage(), $release::MESSAGE_ERROR)
                    ->setStatus(Omnius_Sandbox_Model_Release::STATUS_ERROR)
                    ->incrementTries()
                    ->save();
            }
        }
        return $release->addMessage(
            sprintf(
                'Could not acquire lock for release (ID: %s). Already running or successfully finished.',
                $release->getId()
            ), $release::MESSAGE_ERROR
        );
    }

    /**
     * Handles database releases to external systems using Net_SSH2 and Net_SFTP
     *
     * @param Omnius_Sandbox_Model_Release $release
     * @param Omnius_Sandbox_Model_Replica $replica
     * @throws Exception
     * @return Omnius_Sandbox_Model_Release
     */
    protected function externalPublish(Omnius_Sandbox_Model_Release $release, Omnius_Sandbox_Model_Replica $replica)
    {
        //create local dump
        $localDump = sprintf('/tmp/publish_export_%s_%s.sql.gz', time(), $release->getId());
        $localMysqlConf = $this->_getDatabaseConfig();

        $differences = $replica->checkDifferences();

        if (count($differences)) {
            if (!$release->getWebsiteId()) {
                // Mark release as failed only for non-littleshops publish
                $release->setStatus(Omnius_Sandbox_Model_Release::STATUS_ERROR);
            }

            $release->addMessage(
                    'There are critical differences between the two environments.',
                    Omnius_Sandbox_Model_Release::MESSAGE_ERROR
                )
                ->save();
            $filename = 'differences_' . $release->getId() .'_'. time() . '.csv';
            $diffPath = $this->createDifferencesFile($differences, $filename, $release);

            if ($diffPath) {
                $release
                    ->addMessage(
                        sprintf('Please see the following file for a detailed overview of the differences: %s', $diffPath),
                        Omnius_Sandbox_Model_Release::MESSAGE_ERROR
                    )
                    ->save();
            }

            if (!$release->getWebsiteId()) {
                // Abort only for non-littleshops publish
                throw new Exception('Please review the catalog issues.');
            } else {
                // For littleshops only log this incident as warning, it will not stop the publish
                $message = 'There are critical differences between the two environments on release: %s.';
                if ($diffPath) {
                    $message .= sprintf(' Detailed overview of the differences saved in: %s');
                }

                Mage::log($message, Zend_Log::WARN, 'external_release.log');
            }
        }

        $command = sprintf(
            'mysqldump -u%s -p%s -h%s -P%s %s %s | gzip > %s',
            $this->_escape($localMysqlConf['username']),
            $this->_escape($localMysqlConf['password']),
            $localMysqlConf['host'],
            isset($localMysqlConf['port']) ? $localMysqlConf['port'] : 3306,
            $this->_escape($localMysqlConf['dbname']),
            $this->_escape(join(' ', $release->getTables())),
            $localDump
        );

        $release
            ->addMessage(sprintf('Executing local export command "%s"', $this->censorCommand($command)))
            ->save();
        $this->_getShell()->exec($command);
        $release
            ->addMessage(sprintf('Successfully exported tables to %s', $localDump))
            ->save();

        //move dump to remote replica
        $releasesFolder = 'releases';
        $releasesPath = DS . trim($replica->getEnvironmentPath(), DS) . DS . join(DS, array('var', $releasesFolder));

        $this->_getSftp()->open($replica->getSshConfig());
        $release
            ->addMessage('SFTP connection made')
            ->save();

        $this->_getSftp()->setAllowCreateFolders(true);
        if ( ! $this->_getSftp()->cd($releasesPath)) {
            $this->_getSftp()->cd(DS . trim($replica->getEnvironmentPath(), DS) . DS . join(DS, array('var')));
            $this->_getSftp()->mkdir($releasesFolder, 0777, false);
            if ( ! $this->_getSftp()->cd($releasesPath)) {
                throw new Exception(sprintf('Could not create releases dir at %s', $releasesPath));
            } else {
                $release
                    ->addMessage(sprintf('Remote releases dir did not exist. Successfully created at %s', $releasesPath))->save();
            }
        }

        if ( ! $this->_getSftp()->write(basename($localDump), $localDump, NET_SFTP_LOCAL_FILE)) {
            throw new Exception(sprintf('Could not push export file to %s', $releasesPath));
        } else {
            $remoteSqlPath = join(DS, array(rtrim($releasesPath, DS), ltrim(basename($localDump), DS)));
            $release
                ->addMessage(sprintf('Release export file pushed to %s', $remoteSqlPath), Omnius_Sandbox_Model_Release::MESSAGE_INFO)->save();
        }

        $fileId = md5(sprintf("output_file-%s-%s", date("u"), rand(0, 1000000)));

        $finishedText = sprintf("SCRIPT_%s_IS_COMPLETED", $fileId);

        $command = $this->getExternalCommand($release, $replica, $remoteSqlPath, $finishedText, $fileId);

        Mage::log('update script: '. $command, null, 'update_script.log');
        
        $release
            ->addMessage(sprintf('Executing import command "%s"', $this->censorCommand($command)))
            ->save();

        Mage::log('Starting external exec at: '.date("Y-m-d H:i:s"), null, 'external_release.log', true);
        $otp = $this->_getSftp()->getConnection()->exec($command);

        $finishTimestamp = (int) date("U") + 7200; // Loop for to hours
        do {
            $otp = $this->_getSftp()->getConnection()->exec(sprintf("cat /tmp/%s", $fileId));

            if (strpos($otp, $finishedText) !== false) {
                $otp = str_replace($finishedText, "", $otp);
                break;
            }
            // Wait 10 seconds before making the call again
            sleep(10);
        } while(date("U") < $finishTimestamp);

        Mage::log(var_export($otp, true), null, 'external_command_output.log', true);

        return Mage::getModel('sandbox/release')->load($release->getId());
    }

    /**
     * @param $differences
     * @param $filename
     * @param $release
     * @return bool|string
     */
    protected function createDifferencesFile($differences, $filename, $release)
    {
        $path = Mage::getBaseDir() . DS  . 'var' . DS . 'differences' . DS ;
        $fullPath = $path . $filename;

        if (! file_exists($path) && ! mkdir($path, 0777)) {
            $release
                ->setStatus(Omnius_Sandbox_Model_Release::STATUS_ERROR)
                ->addMessage(
                    sprintf('Folder %s could not be created. Please review permissions for this folder.', $path),
                    Omnius_Sandbox_Model_Release::MESSAGE_ERROR
                )
                ->save();

            return false;
        }

        $fp = fopen($fullPath, 'w');
        fputcsv($fp, ['entity_id', 'slave_sku', 'master_sku']);
        foreach ($differences as $fields) {
            fputcsv($fp, $fields);
        }

        fclose($fp);
        return $fullPath;
    }

    /**
     * @param Omnius_Sandbox_Model_Release $release
     * @param Omnius_Sandbox_Model_Replica $replica
     */
    protected function executeAdditionalCommandsAfterDBDump(Omnius_Sandbox_Model_Release $release, Omnius_Sandbox_Model_Replica $replica)
    {
        // Do nothing, this is mainly used if someone needs to extend it
    }

    /**
     * @param Omnius_Sandbox_Model_Release $release
     * @param Omnius_Sandbox_Model_Replica $replica
     */
    protected function executeAdditionalCommandsBeforeDBDump(Omnius_Sandbox_Model_Release $release, Omnius_Sandbox_Model_Replica $replica)
    {
        // Do nothing, this is mainly used if someone needs to extend it
    }

    /**
     * Ex: /usr/bin/php /var/www/PROJECT_ROOT/shell/update_release_status.php --release_id 123 --replica_id 124
     *
     * @param $releaseId int
     * @param $replicaId int
     * @return string
     */
    protected function _getReleaseUpdateCmd($releaseId, $replicaId)
    {
        return sprintf(
            '%s %supdate_release_status.php --release_id %s --replica_id "%s"',
            Mage::helper('sandbox')->getBinary(),
            rtrim(Mage::getBaseDir(), DS) . DS . 'shell' . DS,
            $releaseId,
            $replicaId
        );
    }

    /**
     * @param Omnius_Sandbox_Model_Release $release
     * @param Omnius_Sandbox_Model_Replica $replica
     * @param $status
     * @return string
     */
    public function getUpdateScript(Omnius_Sandbox_Model_Release $release, Omnius_Sandbox_Model_Replica $replica, $status)
    {
        return sprintf(
            '%s %sconserve_usage.php --release_id %s --release_version "%s" --status %s',
            $replica->getPhpPath(),
            DS . trim($replica->getEnvironmentPath(), DS) . DS . 'shell' . DS,
            $release->getId(),
            $release->getVersion(),
            $status
        );
    }

    protected function getExternalCommand(Omnius_Sandbox_Model_Release $release, Omnius_Sandbox_Model_Replica $replica, $remoteSqlPath, $finishedText, $fileId)
    {
        return sprintf(
            '(( nohup %s && (nohup gunzip < %s | sed -e \'1 i SET SESSION INTERACTIVE_TIMEOUT=7200;\' | nohup mysql -u%s %s -h%s -P%s %s) && nohup %s); nohup echo "%s") < /dev/null > /tmp/%s 2>&1 &',
            $this->getUpdateScript($release, $replica, self::UPDATE_SCRIPT_STATUS_START),
            $remoteSqlPath,
            $this->_escape($replica->getMysqlUser()),
            $this->_escape($replica->getMysqlPassword()) ? "-p" . $this->_escape($replica->getMysqlPassword()) : "",
            $replica->getMysqlHost(),
            $replica->getMysqlPort() ? $replica->getMysqlPort() : 3306,
            $this->_escape($replica->getMysqlDatabase()),
            $this->getUpdateScript($release, $replica, self::UPDATE_SCRIPT_STATUS_END),
            $finishedText,
            $fileId
        );
    }

    /**
     * @param Omnius_Sandbox_Model_Release $release
     * @param Omnius_Sandbox_Model_Replica $replica
     * @throws Exception
     * @return Omnius_Sandbox_Model_Release
     */
    public function externalMediaSync(Omnius_Sandbox_Model_Release $release, Omnius_Sandbox_Model_Replica $replica)
    {
        $localBase = rtrim(Mage::getBaseDir(), DS);
        $localCatalogPath = $localBase . DS . 'media' . DS . 'catalog';
        $localBundlesPath = $localBase . DS . 'media' . DS . 'bundles';
        $remoteBase = rtrim($replica->getEnvironmentPath(), DS);

        if(is_dir($localCatalogPath)){
            $iteratorCatalog = new RecursiveDirectoryIterator($localCatalogPath, FilesystemIterator::SKIP_DOTS);
            $iteratorCatalog = new RecursiveIteratorIterator($iteratorCatalog, RecursiveIteratorIterator::SELF_FIRST);
        }
        if(is_dir($localBundlesPath)){
            $iteratorBundles = new RecursiveDirectoryIterator($localBundlesPath, FilesystemIterator::SKIP_DOTS);
            $iteratorBundles = new RecursiveIteratorIterator($iteratorBundles, RecursiveIteratorIterator::SELF_FIRST);
        }


        $this->_getSftp()->open($replica->getSshConfig());
        $release
            ->addMessage('SFTP connection made')
            ->save();

        if ( ! $this->_getSftp()->cd($remoteBase)) {
            throw new Exception(sprintf('Remote environment path (%s) not found', $remoteBase));
        }

        $release
            ->addMessage(sprintf('Started moving media files to %s', $remoteBase))
            ->save();

        if(is_dir($localCatalogPath)) {
            $this->iterateDirectory($iteratorCatalog, $localBase, $remoteBase);
        }
        if(is_dir($localBundlesPath)) {
            $this->iterateDirectory($iteratorBundles, $localBase, $remoteBase);
        }

        return $release;
    }

    /**
     * @param $iterator
     * @param $localBase
     * @param $remoteBase
     * @throws Exception
     */
    private function iterateDirectory($iterator, $localBase, $remoteBase)
    {
        foreach ($iterator as $file) { /** @var SplFileInfo $file */
            $realPath = $file->getRealPath();
            if (false !== strpos($realPath, sprintf('%scache%s', DS, DS))
                || 'cache' === substr($realPath, strlen($realPath) - 5, 5)
            ) { //skip cache dir
                continue;
            }
            $dirPath = str_replace($localBase, $remoteBase, $file->getRealPath());
            if (!$file->isDir()) {
                $dirPath = dirname($dirPath);
            }
            if ( ! $this->_getSftp()->cd($dirPath)) {
                $this->_getSftp()->getConnection()->exec(sprintf('cd / && mkdir -p %s', DS . trim($dirPath, DS)));
                if ( ! $this->_getSftp()->cd($dirPath)) {
                    throw new Exception(sprintf('Could not create path "%s" on remote system', $dirPath));
                }
            }
            if ($file->isFile()) {
                $this->_getSftp()->write($file->getFilename(), $file->getRealPath(), NET_SFTP_LOCAL_FILE);
            }
        }
    }

    /**
     * @param Omnius_Sandbox_Model_Replica $replica
     * @param bool $isHeavy
     * @return string
     */
    public function getCacheWarmerCommand(Omnius_Sandbox_Model_Replica $replica, $isHeavy = false)
    {
        $cacheScript = rtrim($replica->getEnvironmentPath(), DS) . DS . 'shell' . DS . self::CACHE_WARMER_SCRIPT;
        $cmd = sprintf(
            '(ls %s > /dev/null 2>&1) && (nohup php %s --disable-output%s >> %s 2>&1 &) && (echo \'success\')',
            $cacheScript,
            $cacheScript,
            $isHeavy ? ' --include-heavy' : '',
            join(DS, array(rtrim($replica->getEnvironmentPath(), DS), 'var', 'log', 'warm_cache_output.log'))
        );
        return $cmd;
    }

    /**
     * @param $releaseId
     * @return bool
     */
    protected function _lockRelease($releaseId)
    {
        $temp = Mage::getModel('sandbox/release')->load($releaseId);
        $status = $temp->getStatus();
        if ($status == 'success') {
            return 1;
        }
        $tableName = Mage::getSingleton('core/resource')->getTableName('sandbox/release');
        $bind  = array('status' => Omnius_Sandbox_Model_Release::STATUS_RUNNING);
        $where = array(
            'id = (?)' => $releaseId,
            'status NOT IN (?)' => array(Omnius_Sandbox_Model_Release::STATUS_RUNNING, Omnius_Sandbox_Model_Release::STATUS_SUCCESS),
        );
        $rowsCount = $this->_getConnection()->update($tableName, $bind, $where);
        return $rowsCount > 0;
    }

    /**
     * @param $cmd
     * @return mixed
     */
    public function censorCommand($cmd)
    {
        return preg_replace('/\s\-p([^\s]*)\s/', ' -p***** ', $cmd);
    }

    /**
     * @param $str
     * @return string
     */
    protected function _escape($str)
    {
        $tr = array(
            '\'' => '\\\'',
            '"' => '\"',
            '(' => '\(',
            ')' => '\)',
            '*' => '\*',
            '?' => '\?',
            '!' => '\!',
            '[' => '\[',
            ']' => '\]',
            '<' => '\<',
            '>' => '\>',
            '&' => '\&',
            '$' => '\$',
            ';' => '\;',
            ':' => '\:',
            '|' => '\|',
        );

        return strtr($str, $tr);
    }

    /**
     * @param $releaseId
     * @return string
     */
    protected function _getExportPath($releaseId)
    {
        return sprintf(
            'release_%s_%s.sql',
            $releaseId,
            strftime('%Y%m%d%H%M%S', time())
        );
    }

    /**
     * @return Varien_Db_Adapter_Interface
     */
    protected function _getConnection()
    {
        if ( ! $this->_conn) {
            return $this->_conn = Mage::getSingleton('core/resource')->getConnection('core_read');
        }
        return $this->_conn;
    }

    /**
     * @return array
     */
    protected function _getDatabaseConfig()
    {
        if ( ! $this->_databaseConfig) {
            return $this->_databaseConfig = $this->_getConnection()->getConfig();
        }
        return $this->_databaseConfig;
    }

    /**
     * @return Omnius_Sandbox_Helper_Database
     */
    protected function _getDbHelper()
    {
        if ( ! $this->_databaseHelper) {
            return $this->_databaseHelper = Mage::helper('sandbox/database');
        }
        return $this->_databaseHelper;
    }

    /**
     * @return Omnius_Sandbox_Helper_RemoteDatabase
     */
    protected function _getRemoteDbHelper()
    {
        if ( ! $this->_remoteDatabaseHelper) {
            return $this->_remoteDatabaseHelper = Mage::helper('sandbox/remoteDatabase');
        }
        return $this->_remoteDatabaseHelper;
    }

    /**
     * @return Omnius_Sandbox_Model_Shell
     */
    protected function _getShell()
    {
        if ( ! $this->_shell) {
            return $this->_shell = Mage::getSingleton('sandbox/shell');
        }
        return $this->_shell;
    }

    /**
     * @return Omnius_Sandbox_Model_FileMapper
     */
    protected function _getFileMapper()
    {
        if ( ! $this->_fileMapper) {
            return $this->_fileMapper = Mage::getSingleton('sandbox/fileMapper');
        }
        return $this->_fileMapper;
    }

    /**
     * @return Omnius_Sandbox_Model_Sftp
     */
    protected function _getSftp()
    {
        if ( ! $this->_sftpClient) {
            return $this->_sftpClient = new Omnius_Sandbox_Model_Sftp();
        }
        return $this->_sftpClient;
    }
}
