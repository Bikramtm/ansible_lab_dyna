<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Helper_Sandbox
 */
class Omnius_Sandbox_Model_Sandbox extends Mage_Core_Model_Abstract
{
    const LOCKED_REGISTRY_KEY = 'context_product_is_locked';
    const ALLOW_DATA_DEMULTIPLEXING = 'allow_demultiplexing';
    const PRODUCT_LOCKED_ATTRIBUTE = 'product_locked';
    const UNIQUE_ID_FIELD = 'unique_id';
    const UNIQUE_ID_REFERENCE = 'unique_id_replication_reference';
    const DELETED_ATTRIBUTES_REGISTRY = 'replication_deleted_attributes_reference';
    const DELETED_ATTRIBUTES_SET_REGISTRY = 'replication_deleted_attributes_set_reference';
    const DELETED_PRODUCT_FILTER_REGISTRY = 'replication_deleted_product_filters_reference';
    const DELETED_ATTRIBUTE_GROUPS_REGISTRY = 'replication_deleted_attribute_groups_reference';
}
