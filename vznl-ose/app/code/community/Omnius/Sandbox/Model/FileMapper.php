<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Model_FileMapper
 */
class Omnius_Sandbox_Model_FileMapper
{
    /** @var Omnius_Sandbox_Model_Shell */
    protected $_shell;

    /** @var array */
    protected $_remoteShell = array();

    /**
     * @param $from
     * @param $to
     * @param array $remote
     * @return string
     */
    protected function getCopyCommand($from, $to, array $remote = null)
    {
        if ($this->_getShell()->commandExists('rsync')) {
            return $this->rsyncMap($from, $to, $remote);
        }
        return $this->cpMap($from, $to, $remote);
    }

    /**
     * The path must be to /media/catalog
     *
     * @param $from
     * @param $to
     * @param array $remote
     * @param bool $background
     */
    public function mapMedia($from, $to, array $remote = null, $background = true)
    {
        $from = DS . trim($from, DS) . DS;
        $to = DS . trim($to, DS) . DS;

        $copyCmd = $this->getCopyCommand($from, $to, $remote);
        $cacheRmCmd = sprintf(
            'rm -rf %s',
            escapeshellarg($to . join(DS, array('product', 'cache')))
        );

        if ($remote) {
            $cacheRmCmd = $this->_getRemoteShell($remote)->addRemoteParams($cacheRmCmd);
        }

        $command = sprintf('(%s && %s)%s', $copyCmd, $cacheRmCmd, $background ? ' > /dev/null 2>&1 &' : '');

        try {
            $this->_getShell()->exec($command);
        } catch (Exception $e) {
            Mage::log(var_export($e, true), null, 'cmd.log', true);
            Mage::getSingleton('core/logger')->logException($e);
        }
    }

    /**
     * @param $from
     * @param $to
     * @param array $remote
     * @return string
     */
    protected function rsyncMap($from, $to, array $remote = null)
    {
        if ( ! $remote) {
            $copyCmd = sprintf(
                'rsync -OaHAXx --numeric-ids --exclude "catalog/product/cache/" %s %s',
                escapeshellarg($from),
                escapeshellarg($to)
            );
        } else {
            $copyCmd = sprintf(
                'rsync -OaHAXx --numeric-ids --exclude "catalog/product/cache/" -e "ssh -T -c arcfour -o Compression=no -x -p %s" %s %s@%s:%s',
                $remote['port'],
                escapeshellarg($from),
                $remote['user'],
                $remote['host'],
                escapeshellarg($to)
            );
        }
        return $copyCmd;
    }

    /**
     * @param $from
     * @param $to
     * @param array $remote
     * @return string
     * @throws Exception
     */
    protected function cpMap($from, $to, array $remote = null)
    {
        if ($remote) {
            throw new Exception('Remote syncronisation is not supported by "cp"');
        }
        return sprintf(
            'cp -Rp %s %s',
            escapeshellarg($from),
            escapeshellarg($to)
        );
    }

    /**
     * @return Omnius_Sandbox_Model_Shell
     */
    protected function _getShell()
    {
        if ( ! $this->_shell) {
            return $this->_shell = new Omnius_Sandbox_Model_Shell();
        }
        return $this->_shell;
    }

    /**
     * @param array $remote
     * @return Omnius_Sandbox_Model_RemoteShell|Omnius_Sandbox_Model_Shell
     */
    protected function _getRemoteShell(array $remote)
    {
        $key = md5(serialize($remote));
        if ( ! isset($this->_remoteShell[$key])) {
            return $this->_remoteShell[$key] = new Omnius_Sandbox_Model_RemoteShell($remote);
        }
        return $this->_remoteShell[$key];
    }
} 
