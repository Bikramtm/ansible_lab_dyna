<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Model_Mapper_SalesRulesMapper
 */
class Omnius_Sandbox_Model_Mapper_SalesRulesMapper extends Omnius_Sandbox_Model_Mapper
{
    /** @var int */
    protected $_priority = 450;

    /** @var array */
    protected $_excludedColumns = array();

    /**
     * Check if current mapper supports the given column
     *
     * @param $table
     * @param $column
     * @return bool
     */
    public function supports($table, $column)
    {
        return $table === 'salesrule'
               || $table === 'salesrule_website'
               || $table === 'salesrule_customer_group'
               || $table === 'salesrule_coupon';
    }

    /**
     * Maps the master ID ($value) to the slave ID
     *
     * @param $table
     * @param $column
     * @param $value
     * @param $masterAdapter
     * @param $slaveAdapter
     * @param array $row
     * @throws Exception
     * @return mixed
     */
    public function map($table, $column, $value, $masterAdapter, $slaveAdapter, array $row = null)
    {
        if (is_null($value)) {
            return null;
        }
        $value = trim($value, '\'"`');
        $column = trim($column);

        if ($table === 'salesrule_coupon' && $column != 'rule_id') {
            $value = $this->mapCoupon($table, $column, $value, $masterAdapter, $slaveAdapter, $row);
        } elseif ($table === 'salesrule_customer_group' && $column != 'rule_id') {
            $value = $this->mapCustomerGroup($table, $column, $value, $masterAdapter, $slaveAdapter, $row);
        } else {
            /**
             * $row does not contain rule ID so we need to retrieve it here
             * and after check if its locked on the slave, stopping replication if so
             */
            if (isset($row['unique_id'])) {
                $sql = "SELECT rule_id FROM `salesrule` WHERE unique_id=:unique_id";
                $ruleId = $slaveAdapter->fetchOne($sql, array('unique_id' => $row['unique_id']));

                if ($this->_getRemote()->isItemLocked($table, 'rule_id', $ruleId, $slaveAdapter)) {
                    throw new Exception('Slave match rule is locked. No changes are allowed');
                }
            }

            if ($column == 'rule_id') {
                $slaveId = $this->_getRemote()->mapSlaveUsingUniqueId('salesrule', $column, $value, $masterAdapter, $slaveAdapter);
                if ($this->_getRemote()->isItemLocked('salesrule', $column, $slaveId, $slaveAdapter)) {
                    throw new Exception('Slave rule is locked. No changes are allowed');
                }
                $value = $slaveId;
            } elseif (in_array($column, array('from_date', 'to_date', 'uses_per_customer', 'uses_per_coupon'))) {
                if ($value === '') {
                    $value =  null;
                }
            } elseif ($column == 'locked') {
                /**
                 * For the "locked" column we return false as
                 * the value should and can not be other.
                 * If the slave item is locked, then the
                 * whole query will not be executed
                 * so returning "false" will not change anything
                 */
                $value = false;
            }
        }

        return $value;
    }

    /**
     * Maps the master ID ($value) to the slave ID
     *
     * @param $table
     * @param $column
     * @param $value
     * @param $masterAdapter
     * @param $slaveAdapter
     * @param array $row
     * @throws Exception
     * @return mixed
     */
    protected function mapCoupon($table, $column, $value, $masterAdapter, $slaveAdapter, array $row = null)
    {
        if ($column == 'coupon_id') {
            if (is_array($row) && $row && isset($row['code'])) {
                $sql = "SELECT coupon_id FROM `salesrule_coupon` WHERE code=:code";
                $value = $slaveAdapter->fetchOne($sql, array('code'=>$row['code']));
            } else {
                $sql = "SELECT rule_id FROM `salesrule_coupon` WHERE coupon_id:coupon_id";
                $ruleId = $masterAdapter->fetchOne($sql, array('coupon_id'=> $value));

                $sql = "SELECT coupon_id FROM `salesrule_coupon` WHERE rule_id=:rule_id";
                $value = $slaveAdapter->fetchOne($sql, array('rule_id' => $this->map('salesrule', 'rule_id', $ruleId, $masterAdapter, $slaveAdapter, $row)));
            }
        }
        return $value;
    }

    /**
     * Maps the master ID ($value) to the slave ID
     *
     * @param $table
     * @param $column
     * @param $value
     * @param $masterAdapter
     * @param $slaveAdapter
     * @param array $row
     * @throws Exception
     * @return mixed
     */
    protected function mapCustomerGroup($table, $column, $value, $masterAdapter, $slaveAdapter, array $row = null)
    {
        return $value;
    }
}
