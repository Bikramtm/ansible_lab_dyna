<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Model_Mapper_ProductAttributeMapper
 */
class Omnius_Sandbox_Model_Mapper_ProductAttributeMapper extends Omnius_Sandbox_Model_Mapper
{
    /** @var int */
    protected $_priority = 1500;

    /** @var array */
    protected $_disallowedAttrCodes = array(
        //'product_visibility_strategy',
        'product_locked'
    );

    /** @var null */
    protected $_disallowedAttrs = null;

    /**
     * Check if current mapper supports the given column
     *
     * @param $table
     * @param $column
     * @return bool
     */
    public function supports($table, $column)
    {
        return 0 === strpos($table, 'catalog_product_entity_') && $column == 'attribute_id';
    }

    /**
     * Maps the master ID ($value) to the slave ID
     *
     * @param $table
     * @param $column
     * @param $value
     * @param $masterAdapter
     * @param $slaveAdapter
     * @param array $row
     * @throws Exception
     * @return mixed
     */
    public function map($table, $column, $value, $masterAdapter, $slaveAdapter, array $row = null)
    {
        if ( ! $this->_isAttributeAllowed($value)) {

            /**
             * When creating a product, if we do not pass the "product_locked" value,
             * the product will not be created on the replicas as well
             */
            if (isset($this->_disallowedAttrs[$value]) && ($this->_disallowedAttrs[$value] === 'product_locked') && Mage::registry('catalog_product_creation')) {
                return $value;
            }

            throw new Exception('Replication of the certain product attributes is not allowed');
        }

        return $value;
    }

    /**
     * @param $attributeId
     * @return bool
     */
    protected function _isAttributeAllowed($attributeId)
    {
        $this->_loadAttributes();
        return in_array($attributeId, array_keys($this->_disallowedAttrs)) === false;
    }

    /**
     * Retrieves all not allowed attribute IDs
     */
    protected function _loadAttributes()
    {
        if (null === $this->_disallowedAttrs) {
             $data = Mage::getResourceModel('eav/entity_attribute_collection')
                ->setEntityTypeFilter(4)
                ->addFieldToFilter('attribute_code', array('in' => $this->_disallowedAttrCodes))
                ->toArray(array('attribute_id', 'attribute_code'));
            $this->_disallowedAttrs = array();
            foreach ($data['items'] as $attr) {
                $this->_disallowedAttrs[$attr['attribute_id']] = $attr['attribute_code'];
            }
        }
    }
}
