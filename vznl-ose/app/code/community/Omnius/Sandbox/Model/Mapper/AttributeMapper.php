<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Model_Mapper_AttributeMapper
 */
class Omnius_Sandbox_Model_Mapper_AttributeMapper extends Omnius_Sandbox_Model_Mapper
{
    /** @var int */
    protected $_priority = 750;

    /** @var array */
    protected $_specialColumns = array();

    /** @var array */
    protected $_excludedColumns = array(
        'website_id',
        'store_id',
        'type_id',
        'rule_id',
        'entity_type_id',
        'attribute_set_id',
        'default_id',
        'product_id',
    );

    /**
     * Check if current mapper supports the given column
     *
     * @param $table
     * @param $column
     * @return bool
     */
    public function supports($table, $column)
    {
        return (
                (false !== strpos($table, 'attribute'))
                ||  (false !== strpos($table, 'eav_'))
                ||  (in_array($column, array('attribute_id')))
        ) && !in_array($column, $this->_excludedColumns);
    }

    /**
     * Maps the master ID ($value) to the slave ID
     *
     * @param $table
     * @param $column
     * @param $value
     * @param $masterAdapter
     * @param $slaveAdapter
     * @param array $row
     * @return mixed
     * @throws Exception
     */
    public function map($table, $column, $value, $masterAdapter, $slaveAdapter, array $row = null)
    {
        $value = trim($value, '\'"');
        if ($this->_isMappable($column)) {
            $setId = null;
            if ($row) {
                if (isset($row['entity_type_id'])) {
                    $setId = $row['entity_type_id'];
                } elseif (isset($row['attribute_set_id'])) {
                    $setId = $row['attribute_set_id'];
                }
            }
            if ($column == 'attribute_group_id') {
                $result = $this->_mapAttributeGroupId($table, $column, $value, $masterAdapter, $slaveAdapter, $row);
            } elseif ($column == 'product_super_attribute_id') {
                $result = $this->_mapSuperAttributeProductMap($table, $column, $value, $masterAdapter, $slaveAdapter, $row);
            }
            if (isset($result)) {
                return $result;
            }

            if ($table == 'eav_attribute_option_value' || $table == 'eav_attribute_option') {
                $result = $value;
            } else{
                if ( ! ($result = $this->_getRemote()->getSlaveAttributeId($value, $column, $masterAdapter, $slaveAdapter, $setId))) {
                    $replicaConfig = $slaveAdapter->getConfig();
                    $message = sprintf('%s: Item (%s.%s=%s) not present on replica (%s:%s)', get_class($this), $table, $column, $value, $replicaConfig['host'], $replicaConfig['dbname']);
                    Mage::log($message, null, 'replication_missing.log', true);
                    throw new Exception($message);
                }
            }
            return $result;
        }
        return $value;
    }

    /**
     * @param $table
     * @param $column
     * @param $value
     * @param $masterAdapter
     * @param $slaveAdapter
     * @param array $row
     * @return mixed
     * @throws Exception
     */
    protected function _mapSuperAttributeProductMap($table, $column, $value, $masterAdapter, $slaveAdapter, array $row = null)
    {
        $sql = 'SELECT * FROM `catalog_product_super_attribute` WHERE product_super_attribute_id=:attribute_id';
        $binds = array(':attribute_id' => $value);

        $superAttrData = $masterAdapter->fetchRow($sql, $binds);

        if ($superAttrData) {
            /** @var Omnius_Sandbox_Model_Mapper_AttributeSetMapper $productMapper */
            $productMapper = Mage::getSingleton('sandbox/mapper_productMapper');
            $slaveProduct = $productMapper->map('catalog_product_entity', 'entity_id', $superAttrData['product_id'], $masterAdapter, $slaveAdapter, $row);
            $slaveAttribute = $this->map('eav_attribute', 'attribute_id', $superAttrData['attribute_id'], $masterAdapter, $slaveAdapter, $row);

            $sql = 'SELECT `product_super_attribute_id` FROM `catalog_product_super_attribute` WHERE product_id=:product_id AND attribute_id=:attribute_id';
            $binds = array(
                ':product_id' => $slaveProduct,
                ':attribute_id' => $slaveAttribute
            );

            if ( ! ($result = $slaveAdapter->fetchOne($sql,$binds))) {
                $replicaConfig = $slaveAdapter->getConfig();
                $message = sprintf('%s: Item (%s.%s=%s) not present on replica (%s:%s)', get_class($this), $table, $column, $value, $replicaConfig['host'], $replicaConfig['dbname']);
                Mage::log($message, null, 'replication_missing.log', true);
                throw new Exception($message);
            }

            return $result;
        }
        $replicaConfig = $slaveAdapter->getConfig();
        throw new Exception('Failed to map property "%s:%s" with master value of "%s" on replica (%s:%s)', $table, $column, $value, $replicaConfig['host'], $replicaConfig['dbname']);
    }

    /**
     * @param $table
     * @param $column
     * @param $value
     * @param $masterAdapter
     * @param $slaveAdapter
     * @param array $row
     * @return mixed
     * @throws Exception
     */
    protected function _mapAttributeGroupId($table, $column, $value, $masterAdapter, $slaveAdapter, array $row = null)
    {
        if (($groups = Mage::registry(Omnius_Sandbox_Model_Sandbox::DELETED_ATTRIBUTE_GROUPS_REGISTRY)) && isset($groups[$value])) {
            $groupData = $groups[$value];
            /** @var Omnius_Sandbox_Model_Mapper_AttributeSetMapper $setMapper */
            $setMapper = Mage::getSingleton('sandbox/mapper_attributeSetMapper');
            $setId = $setMapper->map('eav_attribute_set', 'attribute_set_id', $groupData['attribute_set_id'], $masterAdapter, $slaveAdapter, $row);

            $sql = "SELECT `attribute_group_id` FROM `eav_attribute_group` WHERE attribute_group_name=:attribute_group_name AND attribute_set_id=:attribute_set_id";
            $binds = array(
                'attribute_group_name' => $groupData['attribute_group_name'],
                'attribute_set_id'  => $setId
            );

            if ( ! ($result = $slaveAdapter->fetchOne($sql, $binds))) {
                $replicaConfig = $slaveAdapter->getConfig();
                $message = sprintf('%s: Item (%s.%s=%s) not present on replica (%s:%s)', get_class($this), $table, $column, $value, $replicaConfig['host'], $replicaConfig['dbname']);
                Mage::log($message, null, 'replication_missing.log', true);
                throw new Exception($message);
            }
            return $result;
        }

        if ($row && isset($row['attribute_set_id'])) {
            /** @var Omnius_Sandbox_Model_Mapper_AttributeSetMapper $setMapper */
            $setMapper = Mage::getSingleton('sandbox/mapper_attributeSetMapper');
            $setId = $setMapper->map('eav_attribute_set', 'attribute_set_id', $row['attribute_set_id'], $masterAdapter, $slaveAdapter, $row);

            $sql = "SELECT `attribute_group_name` FROM `eav_attribute_group` WHERE attribute_group_id=:attribute_group_id";
            $groupName = $masterAdapter->fetchOne($sql, array('attribute_group_id', $value));

            $sql = "SELECT `attribute_group_id` FROM `eav_attribute_group` WHERE attribute_group_name=:attribute_group_name AND attribute_set_id=:attribute_set_id";
            $binds = array(
               'attribute_group_name' =>  $groupName,
                'attribute_set_id'  => $setId
            );

            if ( ! ($result = $slaveAdapter->fetchOne($sql, $binds))) {
                $replicaConfig = $slaveAdapter->getConfig();
                $message = sprintf('%s: Item (%s.%s=%s) not present on replica (%s:%s)', get_class($this), $table, $column, $value, $replicaConfig['host'], $replicaConfig['dbname']);
                Mage::log($message, null, 'replication_missing.log', true);
                throw new Exception($message);
            }
            return $result;
        }
        return $value;
    }
}
