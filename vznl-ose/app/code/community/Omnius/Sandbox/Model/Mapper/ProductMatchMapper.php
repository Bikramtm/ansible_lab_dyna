<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Model_Mapper_ProductMatchMapper
 */
class Omnius_Sandbox_Model_Mapper_ProductMatchMapper extends Omnius_Sandbox_Model_Mapper
{
    /** @var int */
    protected $_priority = 400;

    /** @var array */
    protected $_excludedColumns = array(
        'website_id',
        'operation_type',
        'priority',
        'operation',
    );

    /**
     * Check if current mapper supports the given column
     *
     * @param $table
     * @param $column
     * @return bool
     */
    public function supports($table, $column)
    {
        return $table === 'product_match_rule';
    }

    /**
     * Maps the master ID ($value) to the slave ID
     *
     * @param $table
     * @param $column
     * @param $value
     * @param $masterAdapter
     * @param $slaveAdapter
     * @param array $row
     * @throws Exception
     * @return mixed
     */
    public function map($table, $column, $value, $masterAdapter, $slaveAdapter, array $row = null)
    {
        /**
         * $row does not contain match rule ID so we need to retrieve it here
         * and after check if its locked on the slave, stopping replication if so
         */
        if (isset($row['unique_id'])) {
            $ruleId = $slaveAdapter
                ->fetchOne('SELECT product_match_rule_id FROM product_match_rule WHERE unique_id=:id',
                    array(
                        ':id' => $row['unique_id']
                    )
                );

            if ($this->_getRemote()->isItemLocked($table, 'product_match_rule_id', $ruleId, $slaveAdapter)) {
                throw new Exception('Slave match rule is locked. No changes are allowed');
            }
        }

        $value = trim($value, '\'"`');
        $column = trim($column);

        if ($column == 'product_match_rule_id') {
            $slaveId = $this->_getRemote()->mapSlaveUsingUniqueId($table, $column, $value, $masterAdapter, $slaveAdapter);
            if ($this->_getRemote()->isItemLocked($table, $column, $slaveId, $slaveAdapter)) {
                throw new Exception('Slave match rule is locked. No changes are allowed');
            }
            $value = $slaveId;
        } elseif ($column == 'left_id' || $column == 'right_id') {
            $value = $this->mapLeftAndRight($table, $column, $value, $masterAdapter, $slaveAdapter, $row);
        } elseif ($column == 'locked') {
            /**
             * For the "locked" column we return false as
             * the value should and can not be other.
             * If the slave item is locked, then the
             * whole query will not be executed
             * so returning "false" will not change anything
             */
            $value = false;
        }

        return $value;
    }

    /**
     * @param $table
     * @param $column
     * @param $value
     * @param $masterAdapter
     * @param $slaveAdapter
     * @param array $row
     * @return mixed
     * @throws Exception
     */
    protected function mapLeftAndRight($table, $column, $value, $masterAdapter, $slaveAdapter, array $row = null)
    {
        if ($row && isset($row['operation_type']) && $row['operation_type']) {
            $mapper = false;
            switch ((int) $row['operation_type']) {
                case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_C2C:
                case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_P2P:
                    $mapper = Mage::getSingleton('sandbox/mapper_categoryMapper');
                    break;
                case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_P2C:
                    $mapper = Mage::getSingleton('sandbox/mapper_genericMapper');
                    break;
                default:
                    break;
            }
            if ($mapper && $this->_isMappable($column)) {
                return $mapper->map($table, $column, $value, $masterAdapter, $slaveAdapter, $row);
            }
        }

        return $value;
    }
}
