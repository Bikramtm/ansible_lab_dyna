<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Model_Mysql4_Replica
 */
class Omnius_Sandbox_Model_Mysql4_Replica extends Mage_Core_Model_Mysql4_Abstract
{
    /**
     * Constructor override
     */
    protected function _construct()
    {
        $this->_idFieldName = 'name';
    }

    /**
     * Load an object
     *
     * @param Mage_Core_Model_Abstract $object
     * @param mixed $value
     * @param string $field field to load by (defaults to model id)
     * @return Mage_Core_Model_Resource_Db_Abstract
     */
    public function load(Mage_Core_Model_Abstract $object, $value, $field = null)
    {
        if (is_null($field)) {
            $field = $this->getIdFieldName();
        }

        if (!is_null($value)) {
            $replicas = Mage::getSingleton('sandbox/config')->getReplicas(true);
            $data = null;
            foreach ($replicas as $replicaData) {
                if ($replicaData[$field] == $value) {
                    $data = $replicaData;
                    break;
                }
            }
            if ($data) {
                $object->setData($data);
            }
        }

        $this->unserializeFields($object);
        $this->_afterLoad($object);

        return $this;
    }
}
