<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Block_Adminhtml_Release_Edit_Tab_Form
 */
class Omnius_Sandbox_Block_Adminhtml_Release_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{

    private $checkboxes = array('is_heavy', 'export_media');
    /**
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);

        if (Mage::getSingleton('adminhtml/session')->getReleaseData()) {
            $data = Mage::getSingleton('adminhtml/session')->getReleaseData();
            Mage::getSingleton('adminhtml/session')->setReleaseData(null);
        } elseif (Mage::registry('release_data')) {
            $data = Mage::registry('release_data')->getData();
        }

        foreach ($this->getFields() as $fieldSetName => $setFields) {
            $fieldSet = $form->addFieldset(sprintf('sandbox_%s_form', strtolower($fieldSetName)), array('legend' => Mage::helper('sandbox')->__($fieldSetName)));
            foreach ($setFields as $name => $fieldDefinition) {
                if(isset($data) && isset($data[$name]) && in_array($name, $this->checkboxes)){
                    $fieldDefinition['config']['checked'] = $data[$name];
                }
                $fieldSet->addField($name, $fieldDefinition['type'], $fieldDefinition['config']);
            }
        }

        $form->setValues($data);

        return parent::_prepareForm();
    }

    /**
     * @return array
     */
    protected function getFields()
    {
        return array(
            'Replica' => array(
                'replica' => array(
                    'type' => 'select',
                    'config' => array(
                        'label' => Mage::helper('sandbox')->__('Target Replica'),
                        'name' => 'replica',
                        'values' => $this->getReplicaOptions(),
                        'class' => 'required-entry',
                        'required' => true,
                    ),
                ),
                'version' => array(
                    'type' => 'text',
                    'config' => array(
                        'label' => Mage::helper('sandbox')->__('Release Version'),
                        'name' => 'version',
                        'class' => 'required-entry',
                        'required' => true,
                    ),
                ),
                'deadline' => array(
                    'type' => 'date',
                    'config' => array(
                        'label' => Mage::helper('sandbox')->__('Execution Deadline'),
                        'name' => 'deadline',
                        'time' => true,
                        'format' => 'yyyy-MM-dd HH:mm:ss',
                        'image' => $this->getSkinUrl('images/grid-cal.gif'),
                        'class' => 'required-entry',
                        'required' => true,
                    ),
                ),
            ),
            'Media' => array(
                'export_media' => array(
                    'type' => 'checkbox',
                    'config' => array(
                        'label' => Mage::helper('sandbox')->__('Handle media also?'),
                        'name' => 'export_media',
                        'checked' => 1,
                    ),
                ),
            ),
            'Processing' => array(
                'is_heavy' => array(
                    'type' => 'checkbox',
                    'config' => array(
                        'label' => Mage::helper('sandbox')->__('Complete?'),
                        'name' => 'is_heavy',
                        'checked' => 1,
                        'after_element_html' => Mage::helper('sandbox')->__(
                            '<br/><span>If checked, the heavy indexers and cache building process will be executed also.</span>'
                        )
                    ),
                ),
            )
        );
    }

    /**
     * Returns the options for all replicas
     * @return array
     */
    protected function getReplicaOptions()
    {
        $options = array(
            null => Mage::helper('sandbox')->__('Please select...'),
        );

        foreach (Mage::getSingleton('sandbox/config')->getReplicas() as $replica) {
            $options[$replica['name']] = $replica['name'];
        }

        return $options;
    }
}
