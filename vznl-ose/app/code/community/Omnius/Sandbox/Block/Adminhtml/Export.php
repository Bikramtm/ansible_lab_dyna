<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Block_Adminhtml_Export
 */
class Omnius_Sandbox_Block_Adminhtml_Export extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * Override constructor to customize the grid
     * Omnius_Sandbox_Block_Adminhtml_Export constructor.
     */
    public function __construct()
    {
        $this->_controller = 'adminhtml_export';
        $this->_blockGroup = 'sandbox';
        $this->_headerText = Mage::helper('sandbox')->__('Export Manager');
        $this->_addButtonLabel = Mage::helper('sandbox')->__('Add New Item');

        parent::__construct();
    }
}
