<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Block_Adminhtml_Export_Edit_Tab_Form
 */
class Omnius_Sandbox_Block_Adminhtml_Export_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    const AXI_EXPORT_TYPE = 1;
    const DYNA_EXPORT_TYPE = 2;

    protected $_types = array(
        1 => 'Axi',
        2 => 'Dyna',
    );

    /**
     * Override prepare form to add custom fields
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);

        foreach ($this->getFields() as $fieldSetName => $setFields) {
            $fieldSet = $form->addFieldset(sprintf('sandbox_%s_form', strtolower($fieldSetName)), array('legend' => Mage::helper('sandbox')->__($fieldSetName)));
            foreach ($setFields as $name => $fieldDefinition) {
                $fieldSet->addField($name, $fieldDefinition['type'], $fieldDefinition['config']);
            }
        }

        if (Mage::getSingleton('adminhtml/session')->getExportData()) {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getExportData());
            Mage::getSingleton('adminhtml/session')->setExportData(null);
        } elseif (Mage::registry('export_data')) {
            $form->setValues(Mage::registry('export_data')->getData());
        }

        return parent::_prepareForm();
    }

    /**
     * Get the fields for the export form
     * @return array
     */
    protected function getFields()
    {
        return array(
            'General' => array(
                'deadline' => array(
                    'type' => 'date',
                    'config' => array(
                        'label' => Mage::helper('sandbox')->__('Execution Deadline'),
                        'name' => 'deadline',
                        'time' => true,
                        'format' => 'yyyy-MM-dd HH:mm:ss',
                        'image' => $this->getSkinUrl('images/grid-cal.gif'),
                        'class' => 'required-entry',
                        'required' => true,
                    ),
                ),
                'type' => array(
                    'type' => 'select',
                    'config' => array(
                        'label' => Mage::helper('sandbox')->__('Export Type'),
                        'name' => 'type',
                        'class' => 'required-entry',
                        'required' => true,
                        'values' => $this->getExportTypeOptions()
                    ),
                ),
                'export_filename' => array(
                    'type' => 'text',
                    'config' => array(
                        'label' => Mage::helper('sandbox')->__('Export filename (including extension)'),
                        'name' => 'export_filename',
                        'class' => 'required-entry',
                        'required' => true,
                        'after_element_html' => '<span>Ex.: export_axi.xml</span>'
                    ),
                ),
            ),
            'Destination' => array(
                'replica' => array(
                    'type' => 'select',
                    'config' => array(
                        'label' => Mage::helper('sandbox')->__('Target Replica'),
                        'name' => 'replica',
                        'values' => $this->getReplicaOptions(),
                        'class' => 'required-entry',
                        'required' => true,
                    ),
                ),
            ),
        );
    }

    /**
     * Get options for all replicates
     * @return array
     */
    protected function getReplicaOptions()
    {
        $options = array(
            null => Mage::helper('sandbox')->__('Please select...'),
        );

        foreach (Mage::getSingleton('sandbox/config')->getReplicas() as $replica) {
            $options[$replica['name']] = $replica['name'];
        }
        return $options;
    }

    /**
     * Returns the export types
     * @return array
     */
    protected function getExportTypeOptions()
    {
        return array(
            "" => Mage::helper('sandbox')->__('Please select...'),
            Omnius_Sandbox_Model_Export::AXI_EXPORT_TYPE => Omnius_Sandbox_Model_Export::$_types[self::AXI_EXPORT_TYPE],
            Omnius_Sandbox_Model_Export::DYNA_EXPORT_TYPE => Omnius_Sandbox_Model_Export::$_types[self::DYNA_EXPORT_TYPE],
        );
    }
}
