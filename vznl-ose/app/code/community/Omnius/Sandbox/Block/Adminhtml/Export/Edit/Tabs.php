<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Block_Adminhtml_Export_Edit_Tabs
 */
class Omnius_Sandbox_Block_Adminhtml_Export_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * Constructor override to customize grid
     * Omnius_Sandbox_Block_Adminhtml_Export_Edit_Tabs constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('export_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('sandbox')->__('Item Information'));
    }

    /**
     * Add custom tab to the form
     * @return Mage_Core_Block_Abstract
     * @throws Exception
     */
    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
            'label' => Mage::helper('sandbox')->__('Item Information'),
            'title' => Mage::helper('sandbox')->__('Item Information'),
            'content' => $this->getLayout()->createBlock('sandbox/adminhtml_export_edit_tab_form')->toHtml(),
        ));
        return parent::_beforeToHtml();
    }
}
