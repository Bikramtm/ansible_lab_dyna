<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$exportTableName = 'sandbox_export';

/** @var Mage_Sales_Model_Mysql4_Setup $this */
$installer = $this;
$installer->startSetup();
$connection = $installer->getConnection();

if ($connection->isTableExists($exportTableName)) {
    $connection->addColumn($exportTableName, 'replica',
        array(
            'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
            'length' => 255,
            'nullable' => false,
            'comment' => 'Replica name'
        ))
    ;
}

$installer->endSetup();