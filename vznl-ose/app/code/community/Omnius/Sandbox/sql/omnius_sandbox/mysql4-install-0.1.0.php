<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;
$installer->startSetup();

/** @var Mage_Eav_Model_Entity_Setup $model */
$model = Mage::getModel('eav/entity_setup', 'core_setup');

//set attribute data
$data = array(
    'type' => 'int',
    'input' => 'boolean',
    'label' => 'Product Locked',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_required' => false,
    'is_comparable' => false,
    'is_searchable' => false,
    'is_unique' => false,
    'is_configurable' => true,
    'use_defined' => true,
    'default' => 0,
    'is_used_for_promo_rules' => 1
);

$attributeSearch = $model->getAttribute(Mage_Catalog_Model_Product::ENTITY, 'product_locked');

if (!empty($attributeSearch)) //create the attribute
{
    $model->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'product_locked', $data);

    //assign the attribute to all sets
    $attributeId = $model->getAttribute(Mage_Catalog_Model_Product::ENTITY, 'product_locked');
    $allAttributeSetIds = $model->getAllAttributeSetIds('catalog_product');
    foreach ($allAttributeSetIds as $attributeSetId) {
        try {
            $attributeGroupId = $model->getAttributeGroup(Mage_Catalog_Model_Product::ENTITY, $attributeSetId, 'General');
        } catch (Exception $e) {
            $attributeGroupId = $model->getDefaultAttributeGroupId('catalog/product', $attributeSetId);
        }
        $model->addAttributeToSet(Mage_Catalog_Model_Product::ENTITY, $attributeSetId, $attributeGroupId, $attributeId);
    }
}

$installer->endSetup();
