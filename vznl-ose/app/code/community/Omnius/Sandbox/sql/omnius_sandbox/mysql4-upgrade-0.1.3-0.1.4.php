<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$releasesTableName = 'sandbox_replica';

$installer = $this;
$installer->startSetup();

$connection = $installer->getConnection();

if (!$connection->isTableExists($releasesTableName)) {
// Create the releases table
    $releasesTable = new Varien_Db_Ddl_Table();
    $releasesTable->setName($releasesTableName);
    $releasesTable->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
        array(
            'unsigned' => true,
            'primary' => true,
            'auto_increment' => true,
            'nullable' => false,
            'comment' => 'Replica ID',
        ))
    ->addColumn('name', Varien_Db_Ddl_Table::TYPE_TEXT, 50,
        array(
            'nullable' => false,
            'comment' => 'Replica Name',
        ))
    ->addColumn('environment_path', Varien_Db_Ddl_Table::TYPE_TEXT, 50,
        array(
            'nullable' => false,
            'comment' => 'Environment Path',
        ))
    ->addColumn('varnish_servers', Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(
            'nullable' => true,
            'comment' => 'List of IPs or Hostnames Separated by Comma',
        ))
    ->addColumn('form_key', Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(
            'nullable' => false,
            'comment' => 'Form Key',
        ))
    ->addColumn('mysql_host', Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(
            'nullable' => false,
            'comment' => 'Mysql Host',
        ))
    ->addColumn('mysql_database', Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(
            'nullable' => false,
            'comment' => 'Mysql Database',
        ))
    ->addColumn('mysql_user', Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(
            'nullable' => false,
            'comment' => 'Mysql User',
        ))
    ->addColumn('mysql_password', Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(
            'nullable' => false,
            'comment' => 'Mysql Password',
        ))
    ->addColumn('can_sync_ai', Varien_Db_Ddl_Table::TYPE_BOOLEAN, null,
        array(
            'nullable' => true,
            'default' => false,
            'comment' => 'Sync Auto Increment Flag',
        ))
    ->addColumn('ssh_host', Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(
            'nullable' => true,
            'comment' => 'Ssh Host',
        ))
    ->addColumn('ssh_port', Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(
            'nullable' => true,
            'comment' => 'Ssh Port',
        ))
    ->addColumn('ssh_user', Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(
            'nullable' => true,
            'comment' => 'Ssh User',
        ))
    ->addColumn('ssh_pass', Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(
            'nullable' => true,
            'comment' => 'Ssh Password',
        ))
    ->addColumn('php_path', Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(
            'nullable' => true,
            'comment' => 'Php Binary Path',
        ))
    ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null,
        array(
            'nullable' => false,
            'default'  => Varien_Db_Ddl_Table::TIMESTAMP_INIT,
            'comment' => 'Created at',
        ));
    $connection->createTable($releasesTable);
}

$installer->endSetup();
