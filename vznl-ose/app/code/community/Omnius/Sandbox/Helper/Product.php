<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Helper_Product
 */
class Omnius_Sandbox_Helper_Product
{
    /** @var array */
    protected $_uniqueAttr = array(
        'sku',
        'identifier_ean_or_upc_code',
        'identifier_sap_codes',
        'prodspecs_wms_codes',
    );

    /** @var null */
    protected $_replicas = null;

    /**
     * @param Mage_Catalog_Model_Product $product
     * @return bool|Varien_Object
     */
    public function isProductSaveAllowed(Mage_Catalog_Model_Product $product)
    {
        $id = Mage::app()->getRequest()->getParam('id');
        $isSkuChanged = false;
        if ($id) {
            $prod = Mage::getModel('catalog/product')->load($id);
            if ($prod->getId() && $prod->getSku() != $product->getSku()) {
                $isSkuChanged = true;
            }
        }
        $attrs = Mage::getResourceModel('eav/entity_attribute_collection')
            ->addFieldToFilter('entity_type_id', 4)
            ->addFieldToFilter('attribute_code', array('in' => $this->_uniqueAttr))
            ->getData();
        $this->_uniqueAttr = array();
        foreach ($attrs as $attribute) {
            $this->_uniqueAttr[$attribute['attribute_code']] = $attribute;
        }

        // On edit, check if sku already exists
        if ($this->_isMaster() && (int) (Mage::app()->getRequest()->getParam('id')) > 0 && !Mage::registry('bypass_replication') && $isSkuChanged) {
            $response = new Varien_Object();
            foreach ($this->_getReplicas() as $replicaName => $slaveAdapter) {
                $id = $slaveAdapter
                    ->fetchOne('SELECT entity_id FROM catalog_product_entity WHERE sku = :sku;',
                        array(':sku' => $product->getSku()));

                if ($id) {
                    $response->setError(true);
                    $response->setMessage("One or more replicas have at least one product with same SKU. Please make sure you choose a unique value.");

                    return $response;
                }
            }
        }

        if ($this->_isMaster() && (0 === (int) (Mage::app()->getRequest()->getParam('id'))) && !Mage::registry('bypass_replication')) {
            return $this->parseMaster($product);
        }

        return true;
    }

    /**
     * @return bool
     */
    protected function _isMaster()
    {
        return 0 !== count(Mage::getSingleton('sandbox/config')->getSandboxConnections());
    }

    /**
     * @return array
     */
    protected function _getReplicas()
    {
        if (null === $this->_replicas) {
            $this->_replicas = Mage::getSingleton('sandbox/demux')->getAdapters();
        }

        return $this->_replicas;
    }

    /**
     * @param array $values
     * @param array $attribute
     * @param $slaveAdapter
     * @return bool
     */
    protected function isAttrUnique(array $values, array $attribute, $slaveAdapter)
    {
        $values = join(', ', array_map(function ($value) {
            return sprintf('"%s"', trim($value, '\"'));
        }, $values));
        if ($attribute['backend_type'] === 'static') {
            return 0 === (int) $slaveAdapter
                    ->fetchOne('SELECT count(*) FROM `catalog_product_entity` WHERE `:code` IN (' . $values . ')',
                        array(
                            ':code' => $attribute['attribute_code']));
        } else {
            $query = 'SELECT count(*) FROM `catalog_product_entity_:entity` WHERE attribute_id=:attr AND entity_type_id=:type AND value IN (' . $values . ')';
            $binds = array(
                ':entity'   =>  $attribute['backend_type'],
                ':attr'     =>  $this->mapRemoteAttribute($attribute, $slaveAdapter),
                ':type'     =>  $attribute['entity_type_id']
            );

            return 0 === (int) $slaveAdapter->fetchOne($query, $binds);
        }
    }

    /**
     * @param array $attribute
     * @param $slaveAdapter
     * @return mixed
     */
    protected function mapRemoteAttribute(array $attribute, $slaveAdapter)
    {
        return $slaveAdapter
            ->fetchOne('SELECT attribute_id FROM eav_attribute WHERE attribute_code=:code AND entity_type_id=:type',
                array(
                    ':code' => $attribute['attribute_code'],
                    ':type' => $attribute['entity_type_id']
                ));
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @return bool|Varien_Object
     */
    protected function parseMaster(Mage_Catalog_Model_Product $product)
    {
        $attrValues = array();
        foreach ($this->_uniqueAttr as $code => $attr) {
            if ('' !== (string) $product->getData($code)) {
                $attrValues[$code] = array_filter(array_map('trim', explode(',', $product->getData($code))));
            }
        }

        $duplicateIssues = array();
        /**
         * @var string $replicaName
         * @var Varien_Db_Adapter_Pdo_Mysql $slaveAdapter
         */
        foreach ($this->_getReplicas() as $replicaName => $slaveAdapter) {
            foreach ($attrValues as $code => $values) {
                if (!$this->isAttrUnique($values, $this->_uniqueAttr[$code], $slaveAdapter)) {
                    $duplicateIssues[$replicaName][$code] = $values;
                }
            }
        }

        $response = new Varien_Object();
        if ($duplicateIssues) {
            $attrCodes = array();
            foreach ($duplicateIssues as $replicaName => $codes) {
                $attrCodes = array_merge($attrCodes, array_keys($codes));
            }
            $attrCodes = array_unique($attrCodes);
            foreach ($attrCodes as $attrCode) {
                $response->setError(true);
                $response->setAttribute($attrCode);

                $message = sprintf(
                    "Replica(s) %s have at least one product with same value(s) for the %s attribute(s). Please make sure you choose unique values.",
                    join(', ', array_keys($duplicateIssues)),
                    $attrCode
                );

                $response->setMessage($message);
            }

            return $response;
        }

        return true;
    }
}
