<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Helper_Database
 */
class Omnius_Sandbox_Helper_Database
{
    /**
     * @param array $config
     * @param array $tables
     * @param array $targetConfig
     * @return string
     */
    public function createDumpCommand(array $config, array $tables, array $targetConfig)
    {
        return sprintf(
            'mysqldump -u%s -p%s -h%s -P%s %s %s | mysql -u%s -p%s -h%s -P%s %s',
            $this->_escape($config['username']),
            $this->_escape($config['password']),
            $config['host'],
            isset($config['port']) ? $config['port'] : 3306,
            $this->_escape($config['dbname']),
            $this->_escape(join(' ', $tables)),
            $this->_escape($targetConfig['username']),
            $this->_escape($targetConfig['password']),
            $targetConfig['host'],
            isset($targetConfig['port']) ? $targetConfig['port'] : 3306,
            $this->_escape($targetConfig['dbname'])
        );
    }

    /**
     * @param $str
     * @return string
     */
    protected function _escape($str)
    {
        $tr = array(
            '\'' => '\\\'',
            '"' => '\"',
            '(' => '\(',
            ')' => '\)',
            '*' => '\*',
            '?' => '\?',
            '!' => '\!',
            '[' => '\[',
            ']' => '\]',
            '<' => '\<',
            '>' => '\>',
            '&' => '\&',
            '$' => '\$',
            ';' => '\;',
            ':' => '\:',
            '|' => '\|',
        );

        return strtr($str, $tr);
    }
} 
