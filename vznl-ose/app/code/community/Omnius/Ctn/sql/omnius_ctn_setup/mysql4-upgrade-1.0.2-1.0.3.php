<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();

$tableName = Mage::getSingleton('core/resource')->getTableName('ctn/ctn');

$installer->getConnection()->addColumn($tableName, 'additional', [
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'comment' => 'Additional column',
    'after' => 'end_date',
]);

$installer->endSetup();
