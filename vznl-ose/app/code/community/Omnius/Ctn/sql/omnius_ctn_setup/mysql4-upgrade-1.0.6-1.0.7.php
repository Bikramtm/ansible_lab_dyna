<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

// Synchronize table indexes with production Db
/* @var $this Mage_Core_Model_Resource_Setup */
$this->startSetup();
$idxArray = [];

$idxArray['api_session'] = [
    'IDX_API_SESSION_USERID_DATE' => ['user_id',  'logdate'],
    'IDX_API_SESSION_DATE' => ['logdate'],
];

$idxArray['customer_ctn'] = [
    'IDX_CUSTOMER_CTN_CUSTOMER_ID_STATUS' => ['customer_id',  'status'],
];

foreach ($idxArray as $table => $idx) {
    foreach ($idx as $idxName => $columns) {
        $this->getConnection()->addIndex($this->getTable($table), $idxName, $columns);
    }
}

$this->endSetup();
