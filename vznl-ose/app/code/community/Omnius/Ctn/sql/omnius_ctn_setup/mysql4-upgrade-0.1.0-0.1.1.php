<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Add unique key for the customer_id and ctn fields
 */

/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();

$customerIndexes = $installer->getConnection()->getIndexList("customer_ctn");
$indexName = strtoupper('customer_id_ctn');

if (empty($customerIndexes[$indexName])) {
    $installer->run("
    ALTER TABLE {$this->getTable('customer_ctn')} ADD CONSTRAINT {$indexName} UNIQUE (customer_id, ctn);
");
}

$installer->endSetup();
