<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Core_Model_Resource_Setup $installer */
$this->startSetup();
$connection = $this->getConnection();

$connection->addColumn($this->getTable('ctn/ctn'), 'status', [
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'length' => 255,
    'nullable' => true,
    'required' => false,
    'comment' => "Ctn status"
]);
$connection->addColumn($this->getTable('ctn/ctn'), 'import_checksum', [
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'length' => 32,
    'nullable' => true,
    'required' => false,
    'comment' => "Import checksum"
]);
$connection->addIndex($this->getTable('ctn/ctn'), 'IDX_CUSTOMER_CTN', 'ctn');
$connection->addIndex($this->getTable('ctn/ctn'), 'IDX_CUSTOMER_CTN_CUSTOMER_ID_STATUS', ['customer_id', 'status']);

$this->endSetup();
