<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Ctn_Model_Mysql4_Ctn
 */
class Omnius_Ctn_Model_Mysql4_Ctn extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init('ctn/ctn', 'entity_id');
    }
}