<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Ctn_Model_Ctn
 */
class Omnius_Ctn_Model_Ctn extends Mage_Core_Model_Abstract
{
    protected $_customer;

    const CONSUMER_ONE_OFF_LIMIT = .7; // 70%
    const BUSINESS_ONE_OFF_LIMIT = .5; // 50%

    protected function _construct()
    {
        $this->_init('ctn/ctn'); 
    }

    /**
     * Get ctn customer
     *
     * @return Mage_Core_Model_Abstract
     */
    public function getCustomer()
    {
        if ( ! $this->_customer) {
            $this->_customer = Mage::getModel('customer/customer')->load($this->getCustomerId());
        }
        return $this->_customer;
    }

    /**
     * Load by ctn and customer id
     *
     * @param $ctn
     * @param int|null $customerId
     * @return mixed
     */
    public function loadCtn($ctn, $customerId = null)
    {
        $collection = $this->getCollection()
            ->addFieldToFilter('ctn', $ctn);

        if ($customerId) {
            $collection->addFieldToFilter('customer_id', $customerId);
        }

        return $collection->getFirstItem();
    }

    /**
     * Add code to current string of codes
     *
     * @param string $code
     * @return $this
     */
    public function addCode($code)
    {
        $currCodes = explode(',', $this->getData('code'));
        array_push($currCodes, $code);
        $this->setData('code', join(',', array_filter($code)));
        return $this;
    }

    /**
     * Set start date. If string is provided, it will be converted to DateTime
     *
     * @param string|DateTime $date
     * @return $this
     */
    public function setStartDate($date)
    {
        if ( ! $date) {
            return $this;
        }

        if (is_string($date) && ! $date instanceof DateTime) {
            $date = new DateTime($date);
        } elseif (! $date instanceof DateTime) {
            Mage::throwException('Invalid date provided');
        }

        $this->setData('start_date', $date->format('Y-m-d 00:00:00'));

        return $this;
    }

    /**
     * Get the starting date of customer ctn
     *
     * @return DateTime|null
     */
    public function getStartDate()
    {
        if ($this->getData('start_date')) {
            return new DateTime($this->getData('start_date'));
        }
        return null;
    }

    /**
     * Set end date. If string is provided, it will be converted to DateTime
     *
     * @param $date
     * @return $this
     */
    public function setEndDate($date)
    {
        if ( ! $date) {
            return $this;
        }

        if (is_string($date) && ! $date instanceof DateTime) {
            $date = new DateTime($date);
        } elseif (! $date instanceof DateTime) {
            Mage::throwException('Invalid date provided');
        }

        $this->setData('end_date', $date->format('Y-m-d'));

        return $this;
    }

    /**
     * Return the ending date of customer ctn
     *
     * @return DateTime|null
     */
    public function getEndDate()
    {
        if ($this->getData('end_date')) {
            return new DateTime($this->getData('end_date'));
        }
        return null;
    }

    /**
     * This will return the decoded JSON
     *
     * @return array
     */
    public function getAdditional()
    {
        return $this->getData('additional') ? json_decode($this->getData('additional'), true) : array();
    }

    /**
     * Set additional field. This will convert array to JSON string
     *
     * @param $additional
     * @return $this
     */
    public function setAdditional($additional)
    {
        if (!is_array($additional)) {
            if (is_object($additional) && method_exists($additional, 'toArray')) {
                $additional = $additional->toArray();
            } elseif (is_string($additional)) {
                $additional = json_decode($additional);
                if (json_last_error() !== JSON_ERROR_NONE) {
                    Mage::throwException('Invalid argument provider. Expected JSON string, array or object, got invalid JSON');
                }
            } else {
                Mage::throwException(sprintf('Invalid argument provider. Expected JSON string, array or object, got %s', gettype($additional)));
            }
        }
        $this->setData('additional', json_encode($additional));

        return $this;
    }

    /**
     * Import new customer CTN
     *
     * @param int $customerId
     * @param int $ctn
     * @param string $email
     *
     * @return void
     */
    public function importCtn($customerId, $ctn, $email)
    {
        $this->setCustomerId($customerId);
        $this->setCtn($ctn);
        $this->setEmail($email);

        try {
            $this->save();
        }
        catch (Exception $ex) {
            Mage::log($ex->getMessage(), null, 'Customer_import_'.date("Y-m-d").'.log');
        }
    }

    /**
     * @param $ctn
     * @return string
     * Retrieve ctn details for a provided ctn
     */
    public function getCTNDetails($ctn)
    {
        $customer = Mage::getSingleton('customer/session')->getCustomer()->getId();
        $description = array();
        
        /** @var Omnius_Ctn_Model_Ctn $ctnRecord */
        $ctnRecord = $this->getCollection()
            ->addFieldToFilter('customer_id', $customer)
            ->addFieldToFilter('ctn', $ctn)
            ->getFirstItem()
        ;
        
        if (!$ctnRecord->getId()) {
            return '';
        }

        try {
            $additional = $ctnRecord->getAdditional();
            foreach ($additional['products'] as $product) {
                $description[] = $product['name'];
            }


        } catch (\Exception $e) {
            Mage::getSingleton('core/logger')->logException($e);
            return '';
        }
        
        return join(', ', $description);
    }

    /**
     * @param array $ctns
     * @return array
     * Return ctn details for specified ctns
     */
    public function getAllCTNDetails(array $ctns)
    {
        $customer = Mage::getSingleton('customer/session')->getCustomer()->getId();
        $description = array();
        
        if (empty($ctns) || !$ctns[0]) {
            return array();
        }

        /** @var Omnius_Ctn_Model_Ctn $ctnRecord */
        $ctnRecords = $this->getCollection()
            ->addFieldToFilter('customer_id', $customer)
            ->addFieldToFilter('ctn', array('in' => $ctns))
        ;

        foreach ($ctnRecords as $ctnRecord) {
            $additional = $ctnRecord->getAdditional();
            if ($additional) {
                if (isset($additional['products']['name'])) {
                    $additional['products'] = array($additional['products']);
                }
                $description[$ctnRecord->getCtn()] = join(
                    ', ',
                    array_map(
                        function ($product) {
                            return isset($product['name']) ? $product['name'] : '';
                        },
                        empty($additional['products']) ? [] : $additional['products']
                    )
                );
            }
        }
        
        return $description;
    }

    /**
     * @param $allCTNsCount
     * @param $retainebleCTNCount
     * @return bool
     */
    public function canActivateOneOffDeal($allCTNsCount, $retainebleCTNCount)
    {
        $customer = $this->getCustomer();
        $limit = (!$customer->getId() || !$customer->getIsBusiness()) ? self::CONSUMER_ONE_OFF_LIMIT : self::BUSINESS_ONE_OFF_LIMIT;

        return ($allCTNsCount > 1 && (($retainebleCTNCount / $allCTNsCount) >= $limit));
    }
    
    /**
     * Get the checksum
     *
     * @return mixed|string
     */
    public function getChecksum()
    {
        if(!isset($this->_checksum)) {
            $data = $this->getData();
            unset($data['entity_id']);
            unset($data['import_checksum']);
            unset($data['end_date']);
            unset($data['email']);
            unset($data['_checksum']);
            ksort($data);
            $this->_checksum = md5(json_encode($data));
        }
        return $this->_checksum;
    }
}
