<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Campaign
 */
class Omnius_Campaign_Model_Resource_Campaign extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('campaign/campaign', 'entity_id');
    }

    /**
     * @param Mage_Core_Model_Abstract $object
     * @return Mage_Core_Model_Resource_Db_Abstract|void
     */
    protected function _beforeSave(Mage_Core_Model_Abstract $object)
    {
        if ($object->hasDataChanges() && ($customers = $object->getData('customers'))) {
            $data = array();
            $customerIds = array();
            foreach ($customers as $customer) {
                array_push($data, array('customer_id' => $customer->getId(), 'campaign_id' => $object->getId()));
                array_push($customerIds, $customer->getId());
            }
            $connection = $this->_getConnection('core_write');
            $connection->beginTransaction();
            try {
                /**
                 * Find already existing connections
                 */
                $adapter = $this->_getWriteAdapter();
                /**
                 * Delete all previous keys, and add the new ones
                 */
                $adapter->delete(Mage::getSingleton('core/resource')->getTableName('customer_campaign_link'), sprintf('campaign_id = %s', $adapter->quote($object->getId())));

                foreach ($data as $row) {
                    $connection->insert(Mage::getSingleton('core/resource')->getTableName('customer_campaign_link'), $row);
                }
                $connection->commit();
            } catch (Exception $e) {
                $connection->rollBack();
                Mage::getSingleton('core/logger')->logException($e);
            }
        }

        parent::_beforeSave($object);
    }

    protected function _afterLoad(Mage_Core_Model_Abstract $object)
    {
        $adapter = $this->_getWriteAdapter();
        $bind = array('campaign_id' => $object->getId());
        $select = $adapter->select()
            ->from('customer_campaign_link', array('customer_id'))
            ->where('campaign_id = :campaign_id');
        $result = $adapter->fetchCol($select, $bind);
        $collection = new Varien_Data_Collection();
        if ($result) {
            $customers = Mage::getResourceSingleton('customer/customer_collection')
                ->addAttributeToFilter('entity_id', array('in' => $result))
                ->load();
            foreach ($customers->getItems() as $customer) {
                $collection->addItem($customer);
            }
            $object->setData('customers', $collection);
        } else {
            $object->setData('customers', $collection);
        }

        parent::_afterLoad($object);
    }
}
