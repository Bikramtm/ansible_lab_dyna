<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Sales_Model_Entity_Setup $this */
$this->startSetup();

/** Campaign Role table */
if (!$this->tableExists('customer_campaign')) {
    $campaignTable = new Varien_Db_Ddl_Table();

    $campaignTable->setName('customer_campaign');

    $campaignTable->addColumn(
        'entity_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        10,
        ['unsigned' => true, 'primary' => true, 'auto_increment' => true]
    );

    $campaignTable->addColumn(
        'name',
        Varien_Db_Ddl_Table::TYPE_VARCHAR,
        255
    );

    $campaignTable->addColumn(
        'code',
        Varien_Db_Ddl_Table::TYPE_VARCHAR,
        20
    );

    $campaignTable->addColumn(
        'type',
        Varien_Db_Ddl_Table::TYPE_VARCHAR,
        2
    );

    $campaignTable->setOption('type', 'InnoDB');
    $campaignTable->setOption('charset', 'utf8');
    $this->getConnection()->createTable($campaignTable);
}
/** Customer - Campaign Link table  */
if (!$this->tableExists('customer_campaign_link')) {

    $linkTable = new Varien_Db_Ddl_Table();

    $linkTable->setName('customer_campaign_link');

    $linkTable->addColumn(
        'link_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        10,
        ['unsigned' => true, 'primary' => true, 'auto_increment' => true]
    );

    $linkTable->addColumn(
        'customer_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        10,
        ['unsigned' => true]
    );

    $linkTable->addColumn(
        'campaign_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        10,
        ['unsigned' => true]
    );

    $linkTable->addForeignKey(
        'fk_customer_campaign_customer_id',
        'customer_id',
        Mage::getSingleton('core/resource')->getTableName('customer/entity'),
        'entity_id',
        $campaignTable::ACTION_CASCADE,
        $campaignTable::ACTION_CASCADE
    );

    $linkTable->addForeignKey(
        'fk_customer_campaign_campaign_id',
        'campaign_id',
        Mage::getSingleton('core/resource')->getTableName('campaign/campaign'),
        'entity_id',
        $campaignTable::ACTION_CASCADE,
        $campaignTable::ACTION_CASCADE
    );

    $linkTable->addIndex(
        'unique_customer_id_campaign_id',
        ['customer_id', 'campaign_id'],
        ['type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE]
    );

    $linkTable->setOption('type', 'InnoDB');
    $linkTable->setOption('charset', 'utf8');
    $this->getConnection()->createTable($linkTable);
}
$this->endSetup();
