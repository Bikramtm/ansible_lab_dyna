<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer->startSetup();

$attributes = array(
    'new_price' => array(
        'group' => 'Prices',
        'type' => 'decimal',
        'input' => 'price',
        'backend' => 'catalog/product_attribute_backend_price',
        'frontend' => '',
        'label' => 'New Price',
        'note' => 'New Price',
        'class' => '',
        'source' => '',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'visible' => true,
        'required' => false,
        'user_defined' => true,
        'default' => null,
        'visible_on_front' => false,
        'unique' => false,
        'is_configurable' => false,
        'used_for_promo_rules' => true
    ),
    'new_price_date' => array(
        'group' => 'Prices',
        'type' => 'datetime',
        'input' => 'datetime',
        'backend' => 'eav/entity_attribute_backend_datetime',
        'frontend' => '',
        'label' => 'New Price Date',
        'note' => 'New Price Date',
        'class' => '',
        'source' => '',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'visible' => true,
        'required' => false,
        'user_defined' => true,
        'default' => null,
        'visible_on_front' => false,
        'unique' => false,
        'is_configurable' => false,
        'used_for_promo_rules' => true
    ),
);

$setup = Mage::getResourceModel('catalog/eav_mysql4_setup', 'core_setup');
foreach ($attributes as $attrCode => $description) {
    $setup->addAttribute(Mage_Catalog_Model_Product::ENTITY, $attrCode, $description);
}

$installer->endSetup();