<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Add old_sim attribute to quote items and order items
 */
$salesInstaller = new Mage_Sales_Model_Resource_Setup('core_setup');
$salesInstaller->startSetup();
$salesInstaller->getConnection()
    ->addColumn($salesInstaller->getTable('sales/quote_item'), 'old_sim', array(
            'comment' => 'Use old sim card',
            'type' => Varien_Db_Ddl_Table::TYPE_SMALLINT,
            'required' => false,
            'default' => "0"
        ));
$salesInstaller->getConnection()
    ->addColumn($salesInstaller->getTable('sales/order_item'), 'old_sim', array(
            'comment' => 'Use old sim card',
            'type' => Varien_Db_Ddl_Table::TYPE_SMALLINT,
            'required' => false,
            'default' => "0"
        ));
$salesInstaller->endSetup();