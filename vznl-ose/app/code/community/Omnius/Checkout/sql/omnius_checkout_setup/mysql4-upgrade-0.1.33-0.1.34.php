<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $salesInstaller Mage_Sales_Model_Resource_Setup */
$salesInstaller = new Mage_Sales_Model_Resource_Setup('core_setup');
$salesInstaller->startSetup();

$quoteOrderItemAttr = array(
    'moved_to' => array(
        'comment'       => 'The ID of the order where it was moved',
        'type'          => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'required'      => false,
        'unsigned'      => true
    ),
    'edit_order_id' => array(
        'comment'       => 'The ID of the edit order',
        'type'          => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'required'      => false,
        'unsigned'      => true
    ),
);
foreach ($quoteOrderItemAttr as $attributeCode => $attributeProp) {
    $salesInstaller->addAttribute('quote', $attributeCode, $attributeProp);
    $salesInstaller->addAttribute('order', $attributeCode, $attributeProp);
}

$salesInstaller->getConnection()->addForeignKey(
    'fk_edit_order_id',
    'sales_flat_order',
    'edit_order_id',
    'sales_flat_order',
    'entity_id',
    Varien_Db_Ddl_Table::ACTION_SET_NULL,
    Varien_Db_Ddl_Table::ACTION_SET_NULL
);

$salesInstaller->getConnection()->addForeignKey(
    'fk_moved_to_order',
    'sales_flat_order',
    'moved_to',
    'sales_flat_order',
    'entity_id',
    Varien_Db_Ddl_Table::ACTION_SET_NULL,
    Varien_Db_Ddl_Table::ACTION_SET_NULL
);

$salesInstaller->endSetup();
