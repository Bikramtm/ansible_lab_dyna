<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $salesInstaller Mage_Sales_Model_Resource_Setup */
$salesInstaller = new Mage_Sales_Model_Resource_Setup('core_setup');
$salesInstaller->startSetup();

/* start QUOTE attribute */
$attributeProp = array(
    'comment'       => 'Package info',
    'type'          => Varien_Db_Ddl_Table::TYPE_TEXT,
    'required'      => false
);
$salesInstaller->addAttribute('quote', 'packages_info', $attributeProp);
/* end QUOTE attribute */

$salesInstaller->endSetup();
