<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $installer Mage_Sales_Model_Entity_Setup */
$installer = $this;

$installer->startSetup();

// add quote item columns
$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_item'), 'edit_order_id', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'comment' => 'Id of the initial order',
        'required' => false
    ));

$installer->endSetup();
