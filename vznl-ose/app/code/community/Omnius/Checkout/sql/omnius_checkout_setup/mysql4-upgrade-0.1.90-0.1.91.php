<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $customerInstaller Mage_Customer_Model_Resource_Setup */
$customerInstaller = new Mage_Customer_Model_Resource_Setup('core_setup');
$customerInstaller->startSetup();

$customerInstaller->getConnection()->addIndex($this->getTable('sales/quote'), 'IDX_SUPERQUOTEID_TMP', 'super_quote_id');

$customerInstaller->endSetup();