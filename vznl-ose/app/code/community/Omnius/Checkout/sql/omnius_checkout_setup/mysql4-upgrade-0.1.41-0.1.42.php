<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $installer Mage_Sales_Model_Entity_Setup */
$installer = $this;

$installer->startSetup();

// add quote item columns
$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_item'), 'applied_promo_amount', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Applied Promo Amount',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_item'), 'base_applied_promo_amount', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Base Applied Promo Amount',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_item'), 'applied_maf_promo_amount', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Applied Promo Amount',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_item'), 'base_applied_maf_promo_amount', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Base Applied Promo Amount',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));




// add quote address item columns
$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_address_item'), 'applied_promo_amount', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Applied Promo Amount',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_address_item'), 'base_applied_promo_amount', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Base Applied Promo Amount',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_address_item'), 'applied_maf_promo_amount', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Applied Promo Amount',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_address_item'), 'base_applied_maf_promo_amount', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Base Applied Promo Amount',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));





// add order item columns
$installer->getConnection()
    ->addColumn($installer->getTable('sales/order_item'), 'applied_promo_amount', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Applied Promo Amount',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/order_item'), 'base_applied_promo_amount', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Base Applied Promo Amount',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/order_item'), 'applied_maf_promo_amount', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Applied Promo Amount',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/order_item'), 'base_applied_maf_promo_amount', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Base Applied Promo Amount',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));


$installer->endSetup();