<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = new Mage_Sales_Model_Resource_Setup('core_setup');
$installer->startSetup();

$installer->addAttribute('quote_item', 'manual_pickup', array(
    'type'    => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
    'required'  => false
));

$installer->addAttribute('quote_item', 'manual_pickup_remarks', array(
    'comment'       => 'Manual pickup remarks',
    'type'          => Varien_Db_Ddl_Table::TYPE_TEXT,
    'length'          => 255,
    'required'      => false
));

$installer->addAttribute('quote_address_item', 'manual_pickup', array(
    'type'    => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
    'required'  => false
));

$installer->addAttribute('quote_address_item', 'manual_pickup_remarks', array(
    'comment'       => 'Manual pickup remarks',
    'type'          => Varien_Db_Ddl_Table::TYPE_TEXT,
    'length'          => 255,
    'required'      => false
));

$installer->addAttribute('order_item', 'manual_pickup', array(
    'type'    => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
    'required'  => false
));

$installer->addAttribute('order_item', 'manual_pickup_remarks', array(
    'comment'       => 'Manual pickup remarks',
    'type'          => Varien_Db_Ddl_Table::TYPE_TEXT,
    'length'          => 255,
    'required'      => false
));

$installer->endSetup();