<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $salesInstaller Mage_Sales_Model_Resource_Setup */
$salesInstaller = new Mage_Sales_Model_Resource_Setup('core_setup');
$salesInstaller->startSetup();

/* start QUOTE and ORDER attributes */
$quoteOrderItemAttr = array(
    'customer_account_number_long' => array(
        'comment'     => 'Account Number in long format',
        'type'      => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'required'  => false
    )
);
foreach ($quoteOrderItemAttr as $attributeCode => $attributeProp) {
    $salesInstaller->addAttribute('quote', $attributeCode, $attributeProp);
    $salesInstaller->addAttribute('order', $attributeCode, $attributeProp);
}
/* end QUOTE and ORDER attributes */

$salesInstaller->endSetup();
