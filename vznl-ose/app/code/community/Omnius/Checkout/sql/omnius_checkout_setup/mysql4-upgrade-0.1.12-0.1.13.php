<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $salesInstaller Mage_Sales_Model_Resource_Setup */
$salesInstaller = new Mage_Sales_Model_Resource_Setup('core_setup');
$salesInstaller->startSetup();

/* start QUOTE_ITEM and ORDER_ITEM attributes */
$quoteItemAttr = array(
    'i_ban' => array(
        'comment' => 'Account I Ban',
        'type' => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'size' => 50,
        'required' => false
    ),
    'acount' => array(
        'comment' => 'Account Holder',
        'type' => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'size' => 50,
        'required' => false
    ),
);
foreach ($quoteItemAttr as $attributeCode => $attributeProp) {
    $salesInstaller->addAttribute('quote_address_item', $attributeCode, $attributeProp);
}
/* end QUOTE_ITEM and ORDER_ITEM attributes */

$salesInstaller->endSetup();
