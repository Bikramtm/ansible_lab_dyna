<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * @var $helper Omnius_Catalog_Helper_Setup
 */
$helper = Mage::helper('omnius_catalog/setup');
$options = array(
    'is_visible_on_front' => 1,
    'used_in_product_listing' => 1
);

$attributes = array(
    'identifier_package_type',
    'identifier_package_subtype',
    'identifier_axi_stockable_prod',
    'identifier_commitment_months',
    'identifier_subscr_category',
    'identifier_subsc_postp_simonly',
    'identifier_subscr_brand',
    'identifier_subscr_type',
    'identifier_cons_bus',
    'prodspecs_soccode',
    'sim_only_subscr_sim_types'
);

foreach ($attributes as $attr) {
    $helper->changeAttributeOptions($attr, $options);
}