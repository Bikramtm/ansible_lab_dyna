<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $salesInstaller Mage_Sales_Model_Resource_Setup */
$salesInstaller = new Mage_Sales_Model_Resource_Setup('core_setup');
$salesInstaller->startSetup();

$salesAddressAttr = array(
    'parent_id' => array(
        'comment'       => 'Old Order from which the current one originated',
        'type'          => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'unsigned'      => true,
        'required'      => false,
    ),
    'edited' => array(
        'comment'       => 'Specifies whether this order has been altered or not',
        'type'          => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
        'required'      => false,
    ),
);

foreach ($salesAddressAttr as $attributeCode => $attributeProp) {
    $salesInstaller->addAttribute('order', $attributeCode, $attributeProp);
}


$salesInstaller->endSetup();

$installer = $this;
$installer->startSetup();
$connection = $installer->getConnection();
$connection->dropColumn('sales_flat_order','edit_order_id');
$connection->dropColumn('sales_flat_order','moved_to');
$connection->dropColumn('sales_flat_quote','moved_to');

$installer->endSetup();
