<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $installer Mage_Sales_Model_Entity_Setup */
$installer = $this;

$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote'), 'hawaii_order_exception', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'comment'   => 'Order creation exception',
        'required'  => false,
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote'), 'superorder_number', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'comment'   => 'Superorder order number',
        'required'  => false,
    ));

$installer->endSetup();