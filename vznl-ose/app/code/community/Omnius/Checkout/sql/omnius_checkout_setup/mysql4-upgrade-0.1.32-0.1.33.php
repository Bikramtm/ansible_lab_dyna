<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $salesInstaller Mage_Sales_Model_Resource_Setup */
$salesInstaller = new Mage_Sales_Model_Resource_Setup('core_setup');
$salesInstaller->startSetup();

$quoteOrderItemAttr = array(
    'superorder_id' => array(
        'comment'       => 'ID of the superorder this order is linked to',
        'type'          => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'required'      => false
    ),

);
foreach ($quoteOrderItemAttr as $attributeCode => $attributeProp) {
    $salesInstaller->addAttribute('order', $attributeCode, $attributeProp);
}

$salesInstaller->endSetup();
