<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $salesInstaller Mage_Sales_Model_Resource_Setup */
$salesInstaller = new Mage_Sales_Model_Resource_Setup('core_setup');
$salesInstaller->startSetup();

$quoteOrderItemAttr = array(
    'ecom_status' => array(
        'comment'       => 'Order status on eCom system',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'required'      => false,
        'length'    => 64
    )
);
foreach ($quoteOrderItemAttr as $attributeCode => $attributeProp) {
    $salesInstaller->addAttribute('order', $attributeCode, $attributeProp);
}

$salesInstaller->endSetup();
