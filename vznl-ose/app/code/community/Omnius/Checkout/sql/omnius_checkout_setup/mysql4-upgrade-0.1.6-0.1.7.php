<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $salesInstaller Mage_Sales_Model_Resource_Setup */
$salesInstaller = new Mage_Sales_Model_Resource_Setup('core_setup');
$salesInstaller->startSetup();

/* start QUOTE and ORDER attributes */
$quoteOrderItemAttr = array(
    'package_status' => array(
        'comment'       => 'Package status',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'size'          => 10,
        'required'      => true,
        'default'       => Omnius_Configurator_Model_AttributeSet::PACKAGE_STATUS_OPEN
    ),
);
foreach ($quoteOrderItemAttr as $attributeCode => $attributeProp) {
    $salesInstaller->addAttribute('quote_item', $attributeCode, $attributeProp);
}
/* end QUOTE and ORDER attributes */

$salesInstaller->endSetup();