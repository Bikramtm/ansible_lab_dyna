<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $salesInstaller Mage_Sales_Model_Resource_Setup */
$salesInstaller = new Mage_Sales_Model_Resource_Setup('core_setup');
$salesInstaller->startSetup();

$attributes = array(
    'ebs_reference_id' => array(
        'comment'       => 'Reference ID',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'size'          => 50,
        'required'      => false
    ),
    'ebs_reference_type' => array(
        'comment'       => 'Reference Type',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'size'          => 10,
        'required'      => false
    ),
    'ebs_delivery_date' => array(
        'comment'       => 'Delivery Data',
        'type'          => Varien_Db_Ddl_Table::TYPE_DATE,
        'required'      => false
    ),
    'ebs_discount' => array(
        'comment' => 'Discount',
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'scale'     => 4,
        'precision' => 12,
        'default'   => null,
    ),
);
foreach ($attributes as $attrCode => $definition) {
    $salesInstaller->addAttribute('order_item', $attrCode, $definition);
}

$salesInstaller->endSetup();