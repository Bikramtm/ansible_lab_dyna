<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

//mark the telephone as not required
/**
 * Get the resource model
 */
$resource = Mage::getSingleton('core/resource');

/**
 * Retrieve the read connection
 */
$readConnection = $resource->getConnection('core_read');

/**
 * Retrieve the write connection
 */
$writeConnection = $resource->getConnection('core_write');
$writeConnection->query("UPDATE eav_attribute SET is_required=0 WHERE attribute_code='telephone'");