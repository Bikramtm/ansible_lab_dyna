<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$salesInstaller = new Mage_Sales_Model_Resource_Setup('core_setup');
$salesInstaller->startSetup();
$salesInstaller->getConnection()
    ->addColumn($salesInstaller->getTable('sales/quote_item'), 'one_of_deal', array(
            'comment' => 'One of Deal',
            'type' => Varien_Db_Ddl_Table::TYPE_SMALLINT,
            'required' => false,
            'default' => "0"
        ));
$salesInstaller->getConnection()
    ->addColumn($salesInstaller->getTable('sales/order_item'), 'one_of_deal', array(
            'comment' => 'One of Deal',
            'type' => Varien_Db_Ddl_Table::TYPE_SMALLINT,
            'required' => false,
            'default' => "0"
        ));
$salesInstaller->endSetup();