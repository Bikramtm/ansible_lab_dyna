<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $installer Mage_Sales_Model_Entity_Setup */
$installer = $this;

$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_item'), 'is_promo', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
        'comment' => 'Is promotion product',
        'default'   => 0,
    ));


$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_address_item'), 'is_promo', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
        'comment' => 'Is promotion product',
        'default'   => 0,
    ));

// add quote address item columns
$installer->getConnection()
    ->addColumn($installer->getTable('sales/order_item'), 'is_promo', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
        'comment' => 'Is promotion product',
        'default'   => 0,
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_item'), 'connection_cost', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'LENGTH'  => '12,4',
        'comment' => 'Connection cost',
        'default'   => 0,
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_address_item'), 'connection_cost', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'LENGTH'  => '12,4',
        'comment' => 'Connection cost',
        'default'   => 0,
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/order_item'), 'connection_cost', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'LENGTH'  => '12,4',
        'comment' => 'Connection cost',
        'default'   => 0,
    ));

$installer->endSetup();