<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$salesInstaller = new Mage_Sales_Model_Resource_Setup('core_setup');
$salesInstaller->startSetup();
$salesInstaller->getConnection()
        ->addColumn($salesInstaller->getTable('sales/quote'), 'hawaii_version_id', array(
            'comment' => 'Hawaii version ID for the quote',
            'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
            'required' => false,
        ));
$salesInstaller->endSetup();
