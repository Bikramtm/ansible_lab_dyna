<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $customerInstaller Mage_Customer_Model_Resource_Setup */
$customerInstaller = new Mage_Customer_Model_Resource_Setup('core_setup');
$customerInstaller->startSetup();
$connection = $customerInstaller->getConnection();
$connection->dropIndex($this->getTable('sales/quote'), 'IDX_SALES_FLAT_QUOTE_CARTSTATUS');
$connection->dropIndex($this->getTable('sales/quote'), 'IDX_SALES_FLAT_QUOTE_QUOTEPARENTID');
$connection->addIndex($this->getTable('sales/quote'), 'IDX_SUPERQUOTEID_PARENT_ID_CART_STATUS', ['quote_parent_id', 'cart_status']);
$customerInstaller->endSetup();