<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $installer Mage_Sales_Model_Entity_Setup */
$installer = $this;

$installer->startSetup();

// add quote item columns
$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_item'), 'hidden_maf_tax_amount', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Hidden Monthly Annual Fee Tax Amount',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_item'), 'base_hidden_maf_tax_amount', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Base Hidden Monthly Annual Fee Tax Amount',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));




// add quote address item columns
$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_address_item'), 'hidden_maf_tax_amount', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Hidden Monthly Annual Fee Tax Amount',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_address_item'), 'base_hidden_maf_tax_amount', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Base Hidden Monthly Annual Fee Tax Amount',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));




// add order item columns
$installer->getConnection()
    ->addColumn($installer->getTable('sales/order_item'), 'hidden_maf_tax_amount', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Hidden Monthly Annual Fee Tax Amount',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/order_item'), 'base_hidden_maf_tax_amount', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Base Hidden Monthly Annual Fee Tax Amount',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));





/// quote address columns
$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_address'), 'hidden_maf_tax_amount', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Hidden Monthly Annual Fee Tax Amount',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_address'), 'base_hidden_maf_tax_amount', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Base Hidden Monthly Annual Fee Tax Amount',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));





// order columns
$installer->getConnection()
    ->addColumn($installer->getTable('sales/order'), 'hidden_maf_tax_amount', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Hidden Monthly Annual Fee Tax Amount',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/order'), 'base_hidden_maf_tax_amount', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Base Hidden Monthly Annual Fee Tax Amount',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));




$installer->endSetup();