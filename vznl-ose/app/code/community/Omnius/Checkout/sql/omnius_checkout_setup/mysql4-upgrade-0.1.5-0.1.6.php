<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $salesInstaller Mage_Sales_Model_Resource_Setup */
$salesInstaller = new Mage_Sales_Model_Resource_Setup('core_setup');
$salesInstaller->startSetup();

/* start QUOTE and ORDER attributes */
$quoteOrderItemAttr = array(
    'privacy_sms' => array(
        'comment'       => 'Privacy SMS',
        'type'          => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'required'      => false
    ),
    'privacy_email' => array(
        'comment'       => 'Privacy Email',
        'type'          => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'required'      => false
    ),
    'privacy_mailing' => array(
        'comment'       => 'Privacy Mailing',
        'type'          => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'required'      => false
    ),
    'privacy_telemarketing' => array(
        'comment'       => 'Privacy Telemarketing',
        'type'          => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'required'      => false
    ),
    'privacy_number_information' => array(
        'comment'       => 'Privacy Number Information',
        'type'          => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'required'      => false
    ),
    'privacy_phone_books' => array(
        'comment'       => 'Privacy Phone Books',
        'type'          => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'required'      => false
    ),
    'privacy_disclosure_commercial' => array(
        'comment'       => 'Privacy Disclosure Commercial',
        'type'          => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'required'      => false
    ),
    'privacy_market_research' => array(
        'comment'       => 'Privacy Market Research',
        'type'          => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'required'      => false
    ),
    'privacy_sales_activities' => array(
        'comment'       => 'Privacy Sales Activities',
        'type'          => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'required'      => false
    ),
    'customer_id_type' => array(
        'comment'     => 'ID Type',
        'type'      => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'required'  => false
    ),
    'customer_id_number' => array(
        'comment'     => 'ID Number',
        'type'      => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'required'  => false
    ),
    'customer_issuing_country' => array(
        'comment'     => 'ID Issuing Country',
        'type'      => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'length'    => 2,
        'required'  => false
    ),
    'customer_valid_until' => array(
        'comment'     => 'ID Valid Until',
        'type'      => Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        'required'  => false
    ),
    'customer_account_type' => array(
        'comment'     => 'Account Type',
        'type'      => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'required'  => false
    ),
    'customer_account_number' => array(
        'comment'     => 'Account Number',
        'type'      => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'required'  => false
    ),
    'customer_account_holder' => array(
        'comment'     => 'Account Holder',
        'type'      => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'required'  => false
    ),
    'contractant_gender' => array(
        'comment'       => 'Contractant Gender',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'required'      => false
    ),
    'contractant_prefix' => array(
        'comment'       => 'Contractant Prefix',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'size'          => 40,
        'required'      => false
    ),
    'contractant_middlename' => array(
        'comment'       => 'Contractant Middle Name',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'size'          => 40,
        'required'      => false
    ),
    'contractant_firstname' => array(
        'comment'       => 'Contractant First Name',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'size'          => 40,
        'required'      => false
    ),
    'contractant_lastname' => array(
        'comment'       => 'Contractant Last Name',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'required'      => false
    ),
    'contractant_email' => array(
        'comment'       => 'Contractant E-mail',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'required'      => false
    ),
    'contractant_phone_number' => array(
        'comment'       => 'Contractant Phone Number',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'required'      => false
    ),
    'suspect_fraud' => array(
        'comment'       => 'Suspect Fraud',
        'type'          => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'required'      => false
    ),
    'payment_method' => array(
        'comment'       => 'Payment Method',
        'type'          => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'required'      => false
    ),
    'banknumber_number' => array(
        'comment'       => 'Bank Number',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'size'          => 40,
        'required'      => false
    ),
    'banknumber_accountname' => array(
        'comment'       => 'Bank Account Name',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'required'      => false
    ),
    'credit_check_result' => array(
        'comment'       => 'Credit Check Result',
        'type'          => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'required'      => false
    ),
    'contract_accepted' => array(
        'comment'       => 'Contract Accepted',
        'type'          => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'required'      => false
    ),
    'pickup_at_store' => array(
        'comment'       => 'Pickup at Store',
        'type'          => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'required'      => false,
    ),
);
foreach ($quoteOrderItemAttr as $attributeCode => $attributeProp) {
    $salesInstaller->addAttribute('quote', $attributeCode, $attributeProp);
    $salesInstaller->addAttribute('order', $attributeCode, $attributeProp);
}
/* end QUOTE and ORDER attributes */

/* start ORDER_ADDRESS & QUOTE_ADDRESS attributes */
$salesAddressAttr = array(
    'i_ban' => array(
        'comment'       => 'Account Number(IBAN)',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'required'      => false,
    ),
    'account' => array(
        'comment'       => 'Account',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'size'          => 40,
        'required'      => false,
    ),
);

foreach ($salesAddressAttr as $attributeCode => $attributeProp) {
    $salesInstaller->addAttribute('order_address', $attributeCode, $attributeProp);
    $salesInstaller->addAttribute('quote_address', $attributeCode, $attributeProp);
}
/* end ORDER_ADDRESS & QUOTE_ADDRESS attributes */

/* start QUOTE_ITEM and ORDER_ITEM attributes */
$quoteItemAttr = array(

    'tel_number' => array(
        'comment'       => 'Telephone Number',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'required'      => false
    ),
    'sim_number' => array(
        'comment'       => 'SIM Number',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'required'      => false
    ),
    'current_number' => array(
        'comment'       => 'Current Number',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'required'      => false
    ),
    'current_simcard_number' => array(
        'comment'       => 'Current SIM Number',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'required'      => false
    ),
    'contract_nr' => array(
        'comment'       => 'Contract Number',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'required'      => false
    ),
    'network_provider' => array(
        'comment'       => 'Network Provider',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'required'      => false
    ),
    'connection_type' => array(
        'comment'       => 'Subscription Type',
        'type'          => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'required'      => false
    ),
    'contract_end_date' => array(
        'comment'       => 'Contract End Date',
        'type'          => Varien_Db_Ddl_Table::TYPE_DATETIME,
        'required'      => false
    ),
);
foreach ($quoteItemAttr as $attributeCode => $attributeProp) {
    $salesInstaller->addAttribute('quote_item', $attributeCode, $attributeProp);
    $salesInstaller->addAttribute('order_item', $attributeCode, $attributeProp);
}
/* end QUOTE_ITEM and ORDER_ITEM attributes */

$salesInstaller->endSetup();
