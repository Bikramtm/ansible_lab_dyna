<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Get the resource model
 */
$resource = Mage::getSingleton('core/resource');

/**
 * Retrieve the read connection
 */
$readConnection = $resource->getConnection('core_read');

/**
 * Retrieve the write connection
 */
$writeConnection = $resource->getConnection('core_write');

$table = $resource->getTableName('sales/quote');
$query = "ALTER TABLE `{$table}`
    CHANGE COLUMN `privacy_sms` `privacy_sms` INT(11) NULL DEFAULT '1' COMMENT 'Privacy SMS' AFTER `base_maf_subtotal`,
    CHANGE COLUMN `privacy_email` `privacy_email` INT(11) NULL DEFAULT '1' COMMENT 'Privacy Email' AFTER `privacy_sms`,
    CHANGE COLUMN `privacy_mailing` `privacy_mailing` INT(11) NULL DEFAULT '1' COMMENT 'Privacy Mailing' AFTER `privacy_email`,
    CHANGE COLUMN `privacy_telemarketing` `privacy_telemarketing` INT(11) NULL DEFAULT '1' COMMENT 'Privacy Telemarketing' AFTER `privacy_mailing`";
$writeConnection->query($query);