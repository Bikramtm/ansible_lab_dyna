<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;
/* @var $installer Mage_Catalog_Model_Resource_Eav_Mysql4_Setup */
$installer->startSetup();

//Adjust the Fixed product tax options for the System Configuration.
$installer->run("
    UPDATE `{$installer->getTable('core/config_data')}` SET `value`= NULL
        WHERE `path` = 'carriers/flatrate/type';
");

$installer->run("
    UPDATE `{$installer->getTable('core/config_data')}` SET `value`= '0'
        WHERE `path` = 'carriers/flatrate/price';
");

$installer->endSetup();