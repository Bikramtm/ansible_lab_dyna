<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * @category    Omnius
 * @package     Omnius_Checkout
 */

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();

/**
 * Create table
 */
if (!$installer->tableExists('package_porting_info')) {
    $table = $installer->getConnection()
        ->newTable('package_porting_info')
        ->addColumn('entity_id',
            Varien_Db_Ddl_Table::TYPE_INTEGER,
            null,
            array(
                'identity' => true,
                'unsigned' => true,
                'nullable' => false,
                'primary' => true,
                'auto_increment' => true,
            ),
            'ID')
        ->addColumn('catalog_package_id',
            Varien_Db_Ddl_Table::TYPE_INTEGER,
            null,
            array(
                'unsigned' => true,
            ),
            'catalog package id')
        ->addColumn('product_id',
            Varien_Db_Ddl_Table::TYPE_INTEGER,
            null,
            array(
                'unsigned' => true,
            ),
            'product id')
        ->addColumn('porting_date',
            Varien_Db_Ddl_Table::TYPE_DATETIME,
            null,
            array(),
            'porting date')
        ->addColumn('porting_status',
            Varien_Db_Ddl_Table::TYPE_VARCHAR,
            45,
            array(),
            'porting status')
        ->addColumn('current_provider',
            Varien_Db_Ddl_Table::TYPE_VARCHAR,
            45,
            array(),
            'current provider')
        ->addColumn('current_number',
            Varien_Db_Ddl_Table::TYPE_VARCHAR,
            45,
            array(),
            'current number')
        ->addColumn('current_contract_id',
            Varien_Db_Ddl_Table::TYPE_VARCHAR,
            45,
            array(),
            'current contract id')
        ->addColumn(
            'created_at',
            Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
            null,
            array(
                'default' => Varien_Db_Ddl_Table::TIMESTAMP_INIT,
            ),
            'Created At')
        ->addForeignKey(
            'fk_catalog_package_id',
            'catalog_package_id',
            'catalog_package',
            'entity_id',
            Varien_Db_Ddl_Table::ACTION_CASCADE,
            Varien_Db_Ddl_Table::ACTION_NO_ACTION
        );
    $table->setOption('type', 'InnoDB');
    $table->setOption('charset', 'utf8');
    $installer->getConnection()->createTable($table);

}

$installer->endSetup();
