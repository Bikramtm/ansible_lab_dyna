<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $this Mage_Sales_Model_Entity_Setup */
$this->startSetup();

if (!$this->tableExists('manual_activation_reasons')) {
    /** Start template_bundle table */
    $bundleTemplateTable = new Varien_Db_Ddl_Table();
    $bundleTemplateTable->setName('manual_activation_reasons');
    $bundleTemplateTable->addColumn(
        'entity_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        10,
        array('unsigned' => true, 'primary' => true, 'auto_increment' => true)
    );
    $bundleTemplateTable->addColumn(
        'name',
        Varien_Db_Ddl_Table::TYPE_VARCHAR,
        255
    );
    $bundleTemplateTable->addColumn(
        'requires_input',
        Varien_Db_Ddl_Table::TYPE_SMALLINT,
        1
    );
    $bundleTemplateTable->addColumn(
        'created_at',
        Varien_Db_Ddl_Table::TYPE_DATETIME,
        null
    );
    $bundleTemplateTable->addColumn(
        'updated_at',
        Varien_Db_Ddl_Table::TYPE_DATETIME,
        null
    );

    $bundleTemplateTable->setOption('type', 'InnoDB');
    $bundleTemplateTable->setOption('charset', 'utf8');
    $this->getConnection()->createTable($bundleTemplateTable);
    /** End template_bundle Table */
}

$this->endSetup();
