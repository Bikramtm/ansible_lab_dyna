<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $salesInstaller Mage_Sales_Model_Resource_Setup */
$salesInstaller = new Mage_Sales_Model_Resource_Setup('core_setup');
$salesInstaller->startSetup();

$quoteOrderItemAttr = array(
    'comment'       => 'Step on which the cart must resume',
    'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
    'size'          => 100,
    'required'      => false
);
$salesInstaller->addAttribute('quote', 'cart_status_step', $quoteOrderItemAttr);

$salesInstaller->endSetup();
