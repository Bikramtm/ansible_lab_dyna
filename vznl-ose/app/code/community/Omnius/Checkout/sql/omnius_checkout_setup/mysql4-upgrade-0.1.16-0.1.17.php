<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $salesInstaller Mage_Sales_Model_Resource_Setup */
$salesInstaller = new Mage_Sales_Model_Resource_Setup('core_setup');
$salesInstaller->startSetup();

$quoteOrderItemAttr = array(
    'company_name' => array(
        'comment'       => 'Company Name',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'required'      => false
    ),
    'company_coc' => array(
        'comment'       => 'Company Number (CoC)',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'required'      => false
    ),
    'company_vat_id' => array(
        'comment'       => 'Company VAT ID',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'required'      => false
    ),
    'company_legal_form' => array(
        'comment'       => 'Company Legal Form',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'required'      => false
    ),
    'company_country_id' => array(
        'comment'       => 'Company Country Id',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'required'      => false
    ),
    'company_postcode' => array(
        'comment'       => 'Company Post Code',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'required'      => false
    ),
    'company_house_nr' => array(
        'comment'       => 'Company House nr',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'required'      => false
    ),
    'company_house_nr_addition' => array(
        'comment'       => 'Company house nr addition',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'required'      => false
    ),
    'company_street' => array(
        'comment'       => 'Company Street',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'required'      => false
    ),
    'company_city' => array(
        'comment'       => 'Company City',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'required'      => false
    ),
    'company_date' => array(
        'comment'       => 'Company Creation Date',
        'type'          => Varien_Db_Ddl_Table::TYPE_DATE,
        'required'      => false
    ),
    'contractant_dob' => array(
        'comment'       => 'Contractant date of birth',
        'type'          => Varien_Db_Ddl_Table::TYPE_DATE,
        'required'      => false
    ),
    'customer_is_business' => array(
        'comment'       => 'Customer is business',
        'type'          => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'required'      => false
    ),
    
);
foreach ($quoteOrderItemAttr as $attributeCode => $attributeProp) {
    $salesInstaller->addAttribute('quote', $attributeCode, $attributeProp);
    $salesInstaller->addAttribute('order', $attributeCode, $attributeProp);
}

$salesInstaller->endSetup();
