<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;

$installer->startSetup();

$installer->getConnection()->addColumn($this->getTable('sales_flat_quote'), 'channel', 'VARCHAR(100)');
$installer->getConnection()->addColumn($this->getTable('sales_flat_quote'), 'owner', 'VARCHAR(100)');

/***************************************************************************
 * Customer attributes
 ***************************************************************************/
/* @var $customerInstaller Mage_Customer_Model_Resource_Setup */
$customerInstaller = new Mage_Customer_Model_Resource_Setup('core_setup');

$attributesInfo = array(
    'company_legal_form' => array(
        'label' => 'Company Legal Form',
        'type' => 'varchar',
        'input' => 'text',
        'visible' => true,
        'required' => false,
        'user_defined' => 1,
        'system' => 0
    )
);

foreach ($attributesInfo as $attributeCode => $attributeParams) {
    $customerInstaller->addAttribute('customer', $attributeCode, $attributeParams);
}

foreach ($attributesInfo as $attributeCode => $attributeParams) {
    $attribute = Mage::getSingleton('eav/config')->getAttribute('customer', $attributeCode);
    $attribute->setData(
        'used_in_forms',
        array(
            'adminhtml_customer',
            'checkout_register',
            'customer_account_create',
            'customer_account_edit',
        )
    );
    $attribute->save();
}


$installer->endSetup();