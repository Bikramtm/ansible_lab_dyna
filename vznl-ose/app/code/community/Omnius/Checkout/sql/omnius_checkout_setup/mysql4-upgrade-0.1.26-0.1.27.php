<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $installer Mage_Sales_Model_Entity_Setup */
$installer = $this;
$installer->startSetup();
$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote'), 'prospect_saved_quote', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'length'    => 100,
        'comment'   => 'Prospect save quote id',
        'required'  => false
    ));
$installer->endSetup();
