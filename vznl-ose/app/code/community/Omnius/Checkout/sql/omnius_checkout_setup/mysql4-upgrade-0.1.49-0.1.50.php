<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $salesInstaller Mage_Sales_Model_Resource_Setup */
$salesInstaller = new Mage_Sales_Model_Resource_Setup('core_setup');
$salesInstaller->startSetup();

$quoteOrderItemAttr = array(
    'account_validated' => array(
        'comment'       => 'Fraud suspect check',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'size'          => 50,
        'required'      => false
    ),

);
foreach ($quoteOrderItemAttr as $attributeCode => $attributeProp) {
    $salesInstaller->addAttribute('order', $attributeCode, $attributeProp);
    $salesInstaller->addAttribute('quote', $attributeCode, $attributeProp);
}

$salesInstaller->endSetup();
