<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $salesInstaller Mage_Sales_Model_Resource_Setup */
$salesInstaller = new Mage_Sales_Model_Resource_Setup('core_setup');
$salesInstaller->startSetup();

/* start ORDER attributes */
$attributeProp = array(
    'comment'       => 'Order custom status',
    'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
    'size'          => 50,
    'required'      => false
);
$salesInstaller->addAttribute('order', 'custom_status', $attributeProp);
/* end RDER attributes */

/* start ORDER_ITEM attributes */
$attributeProp = array(
    'comment'       => 'Order item custom status',
    'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
    'size'          => 50,
    'required'      => false
);
$salesInstaller->addAttribute('order_item', 'custom_status', $attributeProp);
/* end ORDER_ITEM attributes */

$salesInstaller->endSetup();
