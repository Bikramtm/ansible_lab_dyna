<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $salesInstaller Mage_Sales_Model_Resource_Setup */
$salesInstaller = new Mage_Sales_Model_Resource_Setup('core_setup');
$salesInstaller->startSetup();

$quoteOrderItemAttr = array(
    'carrier_name' => array(
        'comment'       => 'Carrier name',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'required'      => false
    ),
    'track_and_trace_url' => array(
        'comment'       => 'Track and trace url',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'required'      => false
    ),
    'planned_date' => array(
        'comment'       => 'planned date',
        'type'          => Varien_Db_Ddl_Table::TYPE_DATETIME,
        'required'      => false
    )
);
foreach ($quoteOrderItemAttr as $attributeCode => $attributeProp) {
    $salesInstaller->addAttribute('order', $attributeCode, $attributeProp);
}

$salesInstaller->endSetup();
