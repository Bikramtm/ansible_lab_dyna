<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $installer Mage_Sales_Model_Entity_Setup */
$installer = $this;

$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('sales/order'), 'contract_sign_date', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DATETIME,
        'nullable'  => true,
        'comment' => 'Contract signing date',
        'default'   => NULL,
    ));

$installer->endSetup();