<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $installer Mage_Sales_Model_Entity_Setup */
$installer = $this;

$installer->startSetup();

// add quote address item columns
$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_address_item'), 'custom_price', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Custom Price',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_address_item'), 'original_custom_price', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'comment' => 'Original Custom Price',
        'scale'     => 4,
        'precision' => 12,
        'default'   => '0.0000',
    ));

$installer->endSetup();