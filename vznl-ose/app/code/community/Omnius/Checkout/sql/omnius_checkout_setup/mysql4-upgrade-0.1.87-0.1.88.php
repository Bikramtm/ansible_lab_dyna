<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Eav_Model_Entity_Setup $installer */
$installer = $this;
$installer->startSetup();
$installer->getConnection()
    ->addColumn($installer->getTable('sales/order'), 'dealer_adapter_mode',
        array(
            'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
            'comment'   => 'Dealer adapter mode',
            'length'    => 30,
            'required'  => false
        )
    );
$installer->endSetup();