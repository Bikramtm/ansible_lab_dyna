<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $salesInstaller Mage_Sales_Model_Resource_Setup */
$salesInstaller = new Mage_Sales_Model_Resource_Setup('core_setup');
$salesInstaller->startSetup();

/* start QUOTE and ORDER item attributes */
$quoteOrderItemAttr = array(
    'shipping_data' => array(
        'comment'       => 'Shipping address details',
        'type'          => Varien_Db_Ddl_Table::TYPE_TEXT,
        'required'      => false
    ),
    'current_step' => array(
        'comment'       => 'Shipping address details',
        'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'required'      => false
    ),
);
foreach ($quoteOrderItemAttr as $attributeCode => $attributeProp) {
    $salesInstaller->addAttribute('quote', $attributeCode, $attributeProp);
}
/* end QUOTE and ORDER item attributes */

$salesInstaller->endSetup();
