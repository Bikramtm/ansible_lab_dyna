<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Checkout_Helper_Data
 */
class Omnius_Checkout_Helper_Data extends Mage_Checkout_Helper_Data
{
    const XML_PATH_EMAIL_ADMIN_QUOTE_NOTIFICATION = 'customer/quote_email/exist_user_quote_template';
    const PHONE_NUMBER_CONTRACT_PATH_BUSINESS = 'general/store_information/contract_phone';
    const PHONE_NUMBER_CONTRACT_PATH_CONSUMER = 'general/store_information/contract_phone_consumer';
    const XML_PATH_SALES_GENERAL_PATH = 'sales/general';
    const SHOW_ONLY_ONE_PACKAGE = false; // Limitation for alpha shops
    const HARDWARE_ONLY_SWAP = false;// Limitation for alpha shops

    /** @var Dyna_Cache_Model_Cache */
    protected $_cache;

    /**
     * Returns true if quote has at least one completed mobile subscription
     *
     * @return bool
     */
    public function checkOneCompletedPackage()
    {
        $conn = Mage::getSingleton('core/resource')->getConnection('core_read');
        $packages = $conn->fetchAll(
            Mage::getModel('package/package')->getCollection()
                ->addFieldToFilter('quote_id', $this->_getQuote()->getId())
                ->addFieldToFilter('current_status', Omnius_Configurator_Model_AttributeSet::PACKAGE_STATUS_COMPLETED)
                ->getSelect()
        );
        $allPackages = $conn->fetchAll(
            Mage::getModel('package/package')->getCollection()
                ->addFieldToFilter('quote_id', $this->_getQuote()->getId())
                ->getSelect()
        );
        $firstPackage = current($allPackages);

        return $firstPackage['one_of_deal'] ? count($allPackages) == count($packages) :(bool) count($packages);
    }

    /**
     * Returns maximum price for order from config
     * @return int
     */
    public function getMaxPriceForOrder()
    {
        $key = md5(__METHOD__);
        if ($result = unserialize($this->getCache()->load($key))) {
            return $result;
        } else {
            $result = (float)Mage::getStoreConfig(sprintf('%s/max_price', self::XML_PATH_SALES_GENERAL_PATH));
            $this->getCache()->save(serialize($result), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
            return $result;
        }
    }

    /**
     * Return Super Order Offset
     * @return int
     */
    public function getSuperOrderOffset()
    {
        $key = md5(__METHOD__);
        if (false !== ($result = $this->getCache()->load($key))) {
            return unserialize($result);
        } else {
            $result = (int)Mage::getStoreConfig(sprintf('%s/superorder_offset', self::XML_PATH_SALES_GENERAL_PATH));
            $this->getCache()->save(serialize($result), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
            return $result;
        }
    }

    /**
     * Converts a long iban to short iban as provided in omnitracker DF-003734
     *
     * @param $iban
     * @return string
     */
    public function longIbanToShort($iban)
    {
        return str_pad(ltrim(substr($iban, -10), '0'), 7, '0', STR_PAD_LEFT);
    }

    /**
     * @return bool
     */
    public function canCompleteOrder()
    {
        $data = $this->_getQuote()->getData();
        $maxPrice = $this->getMaxPriceForOrder();
        if( ! isset($data['grand_total'])) $data['grand_total'] = 0;
        $cartTotal = (float)$data['grand_total'];

        return ($this->checkOneCompletedPackage()) && ($cartTotal <= $maxPrice);
    }

    /**
     * @param $to
     * @param array $vars
     * @param string $templateConfigPath
     * @return $this|void
     * @throws Mage_Core_Exception
     */
    public function sendNotificationEmail($to, $vars = array(), $templateConfigPath = self::XML_PATH_EMAIL_ADMIN_QUOTE_NOTIFICATION)
    {
        if (!$to) {
            return;
        }

        $translate = Mage::getSingleton('core/translate');
        /* @var $translate Mage_Core_Model_Translate */
        $translate->setTranslateInline(false);
        $mailTemplate = Mage::getModel('core/email_template');
        /* @var $mailTemplate Mage_Core_Model_Email_Template */
        $template = Mage::getStoreConfig($templateConfigPath, Mage::app()->getStore()->getId());
        $mailTemplate->sendTransactional(
            $template,
            Mage::getStoreConfig(Mage_Sales_Model_Order::XML_PATH_EMAIL_IDENTITY, Mage::app()->getStore()->getId()),
            $to['email'],
            $to['name'],
            $vars
        );
        $translate->setTranslateInline(true);
        return $this;
    }

    /**
     * @param null $productsIds
     * @param null $websiteId
     * @param null $packageType
     * @return array
     */
    public function getUpdatedPrices($productsIds = null, $websiteId = null, $packageType = null)
    {
        $_taxHelper = Mage::helper('tax');
        $_store = Mage::app()->getStore();

        //if website is provided, get the store of the website
        if($websiteId){
            $_store = Mage::app()->getWebsite($websiteId)->getDefaultStore();
        }

        $products = Mage::getResourceModel('catalog/product_collection')
            ->addWebsiteFilter($websiteId)
            ->addAttributeToSelect('price')
            ->addTaxPercents()
            ->addAttributeToSelect('tax_class_id')
            ->load();

        $prices = array();
        /** @var Mage_Catalog_Model_Product $product */
        foreach ($products->getItems() as $product) {

            $priceStart = $_store->roundPrice($_store->convertPrice($product->getPrice()));
            $price = $_taxHelper->getPrice($product, $priceStart, false);
            $priceWithTax = $_taxHelper->getPrice($product, $priceStart, true);

            $prices[$product->getId()]['price'] = $price;
            $prices[$product->getId()]['price_with_tax'] = $priceWithTax;
        }

        return $prices;
    }

    /**
     * Check if the quote has a business or a personal contract
     *
     * @param null $quote
     * @return bool
     */
    public function checkIsBusinessCustomer($quote=null) {
        if(!$quote) {
            $quote = $this->_getQuote();
        }
        if ($quote->getCustomer()->getId()) {
            $customer = $quote->getCustomer();
            return (bool) $customer->getIsBusiness();
        } else {
            $customer = null;
            return (bool) $quote->getCustomerIsBusiness();
        }
    }

    /**
     * Check if the quote is retention only
     *
     * @param null $quote
     * @return bool
     */
    public function checkIsRetentionOnly($quote = null)
    {
        if (!$quote) {
            $quote = $this->_getQuote();
        }
        $isRetention = true;
        foreach ($quote->getPackagesInfo() as $package) {
            // Check if all packages are retention
            if ($package['sale_type'] != Omnius_Checkout_Model_Sales_Quote_Item::RETENTION) {
                $isRetention = false;
            }
        }

        return $isRetention;
    }

    /**
     * Check if the quote (or package) has hardware only items
     *
     * @param null $quote
     * @param null $packageId
     * @return array
     */
    public function checkHasHardwareOnly($quote = null, $packageId = null) {
        if (!$quote) {
            $quote = $this->_getQuote();
        }
        $hasHardware = array();
        foreach ($quote->getPackagesInfo() as $package) {
            if ($packageId) {
                if ($packageId != $package['package_id']) {
                    continue;
                }
            }

            // Check if any of the packages are hardware
            if ($package['type'] == Omnius_Configurator_Model_PackageType::PACKAGE_TYPE_HARDWARE) {
                $hasHardware[$package['package_id']] = true;
            }
        }

        return $hasHardware;
    }

    /**
     * @param $ctn
     * @return string
     */
    public function formatCtn($ctn)
    {
        $ctn = trim($ctn);
        if (substr($ctn, 0, 3) === '316' && strlen($ctn) === 11) {
            return sprintf(
                '06 %s %s %s %s',
                substr($ctn, 3, 2),
                substr($ctn, 5, 2),
                substr($ctn, 7, 2),
                substr($ctn, 9, 2)
            );
        }
        if (substr($ctn, 0, 2) === '06' && strlen($ctn) === 10) {
            return sprintf(
                '06 %s %s %s %s',
                substr($ctn, 2, 2),
                substr($ctn, 4, 2),
                substr($ctn, 6, 2),
                substr($ctn, 8, 2)
            );
        }
        return $ctn;
    }

    /**
     * Override this method to add custom logic.
     *
     * @param null $packageId
     * @return string
     */
    public function checkPackageStatus($packageId = null)
    {
        return Omnius_Configurator_Model_AttributeSet::PACKAGE_STATUS_COMPLETED;
    }

    /**
     * @return Omnius_Checkout_Model_Sales_Quote
     */
    protected function _getQuote()
    {
        return Mage::getSingleton('checkout/cart')->getQuote();
    }

    /**
     * @return Dyna_Cache_Model_Cache
     */
    protected function getCache()
    {
        if ( ! $this->_cache) {
            $this->_cache = Mage::getSingleton('dyna_cache/cache');
        }
        return $this->_cache;
    }

    /**
     * @return mixed|string
     */
    public function getQuoteIncrementPrefix()
    {
        return Mage::getStoreConfig('sales/general/quote_increment_prefix') ? Mage::getStoreConfig('sales/general/quote_increment_prefix') : '';
    }

    /**
     * @return int|mixed
     */
    public function getOfferLifetime()
    {
        return Mage::getStoreConfig('sales/general/offer_lifetime') ? Mage::getStoreConfig('sales/general/offer_lifetime') : 0;
    }

    /**
     * @param null $actualQuote
     */
    public function createNewQuote($actualQuote = null)
    {
        $cart = Mage::getSingleton('checkout/cart');
        if (! $actualQuote) {
            $actualQuote = $cart->getQuote();
        }

        if ($actualQuote->getId()) {
            $actualQuote->setIsActive(false)->save();
        }
        $quote = Mage::getModel('sales/quote');
        $this->_getCustomerSession()->unsNewEmail();
        $customer = $this->_getCustomerSession()->getCustomer();

        Mage::getSingleton('checkout/session')->clear();

        $quote->setIsActive(true)
            ->setStoreId(Mage::app()->getStore(true)->getId())
            ->setCustomer($customer)
            ->save();

        $cart->setQuote($quote);

        Mage::getSingleton('checkout/session')->replaceQuote($quote);
    }

    /**
     * Clone the given quote without keeping any customer related data (promos/ctns/addresses)
     *
     * @param Omnius_Checkout_Model_Sales_Quote $oldQuote
     * @return Omnius_Checkout_Model_Sales_Quote
     */
    public function cloneQuoteWithoutCustomerData($oldQuote)
    {
        $conn = Mage::getSingleton('core/resource')->getConnection('core_read');
        $newQuote = Mage::getModel('sales/quote');
        $newQuote->setCustomer($this->_getCustomerSession()->getCustomer());
        $newQuote->save();

        $newPackages = array();
        //Check if there are any retention packages and remove them
        $packagesModelOld = Mage::getModel('package/package')
            ->getCollection()
            ->addFieldToFilter('quote_id', $oldQuote->getId());
        $packagesModelOld = $conn->fetchAssoc($packagesModelOld->getSelect());
        foreach ($packagesModelOld as $packageModelOld) {
            if ($packageModelOld['sale_type'] != Omnius_Checkout_Model_Sales_Quote_Item::RETENTION
                && !$packageModelOld['ctn']
                && $packageModelOld['sale_type'] != Omnius_Checkout_Model_Sales_Quote_Item::INLIFE) {
                $newPackages[] = $packageModelOld['package_id'];
                $packageModelNew = Mage::getModel('package/package');
                $packageModelNew
                    ->setData($packageModelOld)
                    ->setData('entity_id', null)
                    ->setQuoteId($newQuote->getId())
                    ->setOrderId(null)
                    ->save();
            }
        }
        unset($packagesModelOld);

        foreach ($oldQuote->getAllItems() as $oldItem) {
            if (in_array($oldItem->getPackageId(), $newPackages) && $oldItem->getPackageType()) {
                $newItem = clone $oldItem;
                $newQuote = $newQuote->addItem($newItem);
            }
        }
        $oldQuote->setIsActive(false);
        $oldQuote->save();

        // Copy shipping data
        $newQuote->setShippingData($oldQuote->getShippingData());
        $cart = Mage::getSingleton('checkout/cart');
        $newQuote->setIsActive(true)
            ->setStoreId(Mage::app()->getStore(true)->getId())
            ->setCartStatusStep(Omnius_Checkout_Model_Sales_Quote::CART_STATUS_STEP_CHECKOUT);

        $cart->setQuote($newQuote);
        Mage::getSingleton('checkout/session')->setQuoteId($cart->getQuote()->getId());

        return $newQuote;
    }

    /**
     * @param $quoteId
     * @return false|Mage_Core_Model_Abstract
     * @throws Exception
     * @throws bool
     */
    public function cloneQuote($quoteId)
    {
        $conn = Mage::getSingleton('core/resource')->getConnection('core_read');
        $oldQuote = Mage::getModel('sales/quote')->load($quoteId);

        /** @var Mage_Core_Model_Resource_Transaction $transaction */
        $transaction = Mage::getResourceModel('core/transaction');

        $newQuote = Mage::getModel('sales/quote');
        $newQuote->setCustomer($oldQuote->getCustomer());

        foreach ($oldQuote->getAllItems() as $oldItem) {
            $newItem = clone $oldItem;
            $newItem->setItemId(null);
            $newQuote->addItem($newItem);
        }

        $oldQuote->setIsActive(false);
        $oldQuote->save();

        $newQuote->setIsActive(true)
            ->setDealerId($oldQuote->getDealerId())
            ->setAgentId($oldQuote->getAgentId())
            ->setStoreId($oldQuote->getStoreId())
            ->setCartStatusStep(Omnius_Checkout_Model_Sales_Quote::CART_STATUS_STEP_CHECKOUT)
            ->setQuoteParentId($oldQuote->getId())
            ->save();

        $packagesModelOld = Mage::getModel('package/package')->getCollection()
            ->addFieldToFilter('quote_id', $oldQuote->getId());
        $packagesModelOld = $conn->fetchAssoc($packagesModelOld->getSelect());
        foreach($packagesModelOld as $packageModelOld) {
            $packageModelNew = Mage::getModel('package/package');
            $packageModelNew
                ->setData($packageModelOld)
                ->setQuoteId($newQuote->getId())
                ->setOrderId(null)
                ->save();
        }
        unset($packagesModelOld);

        // clone shipping address
        /**@var Omnius_Checkout_Model_Sales_Quote_Address $address */
        foreach ($oldQuote->getAllAddresses() as $address) {
            $newAddress = Mage::getModel('sales/quote_address');
            $newAddress->setData($address->getData());
            $newAddress->unsetData('address_id');
            $newAddress->setQuoteId($newQuote->getId());
            $transaction->addObject($newAddress);
        }

        // Copy shipping data
        $newQuote->setShippingData($oldQuote->getShippingData());

        try {
            $transaction->save();
        } catch (Exception $e) {
            $newQuote->delete();
            echo $e->getMessage();
            throw new \RuntimeException('Cannot clone quote');
        }

        $newQuote->setTotalsCollectedFlag(false)
            ->save();

        return $newQuote;
    }

    /**
     * @param $request Mage_Core_Controller_Request_Http
     * @throws Exception
     */
    public function processNotesAndPickup($request)
    {
        $manualPickup = $request->get('manual_pickup');
        $manualPickupRemarks = $request->get('manual_pickup_remarks');
        $creditNotes = $request->get('credit_note');
        $manualActivations = $request->get('manual_activation_reason');
        $manualActivationSimcards = $request->get('manual_activation_simcard');
        $manualActivationRemarks = $request->get('manual_activation_remarks');
        /** @var Omnius_Package_Model_Mysql4_Package $packageCollection */
        $packageCollection = Mage::getModel('package/package')->getPackages(null, Mage::getSingleton('checkout/session')->getQuote()->getId());

        /** @var Omnius_Package_Model_Package $package */
        foreach ($packageCollection as $package) {
            $packageId = $package->getPackageId();
            if (isset($manualPickup[$packageId])) {
                $package->setManualPickup(true);
                $package->setManualPickupRemarks($manualPickupRemarks[$packageId]);
            } else {
                $package->setManualPickup(false);
                $package->setManualPickupRemarks(null);
            }

            if (!empty($manualActivations[$packageId])) {
                $package->setData('manual_activation_reason', $manualActivations[$packageId]);
                $package->setData('sim_number', $manualActivationSimcards[$packageId]);
                if (!empty($manualActivationRemarks[$packageId])) {
                    $package->setData('manual_activation_user_input', $manualActivationRemarks[$packageId]);
                } else {
                    $package->setData('manual_activation_user_input', null);
                }
            } else {
                $package->setData('manual_activation_reason', null);
                $package->setData('manual_activation_user_input', null);
                $package->setData('sim_number', null);
            }

            if (isset($creditNotes[$packageId]) && !empty($creditNotes[$packageId])) {
                $package->setCreditNote($creditNotes[$packageId]);
            } else {
                $package->setCreditNote(null);
            }
            $package->save();
        }
    }


    /**
     * Splits the quote by removing the not completed packages
     */
    public function splitQuote()
    {
        /** @var Omnius_Checkout_Model_Sales_Quote $quote */
        $quote = $this->_getQuote();
        $notCompleted = array();

        /** @var Omnius_Package_Model_Package $packageModel */
        $packageModels = Mage::getModel('package/package')->getCollection()
            ->addFieldToFilter('quote_id', $quote->getId());

        // Check for incomplete packages
        foreach($packageModels as $packageModel) {
            if($packageModel->getCurrentStatus() != Omnius_Configurator_Model_AttributeSet::PACKAGE_STATUS_COMPLETED) {
                $packageModel->delete();
                array_push($notCompleted, $packageModel->getPackageId());
            }
        }

        // Remove the incomplete packages items
        foreach ($quote->getAllItems() as $item) {
            if (in_array($item->getPackageId(), $notCompleted)) {
                $quote->removeItem($item->getId());
            }
        }

        // If items were removed recalculate totals
        if (count($notCompleted)) {
            $quote
                ->setTotalsCollectedFlag(false)
                ->collectTotals()
                ->save();

            Mage::helper('omnius_configurator/cart')->reindexPackagesIds($this->_getQuote());
        }
    }

    /**
     * @param null $superOrderEditId
     */
    public function exitSuperOrderEdit($superOrderEditId = null)
    {
        Mage::getSingleton('customer/session')->setOrderLockInfo(null);
        /** @var Mage_Customer_Model_Session $customerSession */
        $customerSession = $this->_getCustomerSession();
        if ($customerSession->getOrderEdit()) {

            /**
             * Decrement sales rules that were not applied on the order.
             * @var Omnius_Checkout_Helper_Data $checkoutHelper
             */
            $checkoutHelper = Mage::helper('omnius_checkout');
            $checkoutHelper->decrementQuotesOnUnload();
            if (!$superOrderEditId) {
                /** @var Omnius_Checkout_Model_Sales_Quote $currentQuote */
                $currentQuote = Mage::getModel('sales/quote')->load($customerSession->getOrderEdit());
            } else {
                $currentQuote = Mage::getModel('sales/quote')->getListOfModifiedQuotes($superOrderEditId, null)->getLastItem();
            }

            $modifiedQuotes = Mage::getModel('sales/quote')->getCollection()
                ->addFieldToFilter('super_quote_id', $currentQuote->getId());
            $currentQuote->setTotalsCollectedFlag(true)->setIsActive(0)->save();
            if ($modifiedQuotes) {
                foreach ($modifiedQuotes as $modifiedQuote) {
                    $modifiedQuote->setTotalsCollectedFlag(true)->setIsActive(0)->save();
                }
            }
        }
        /** @var Omnius_Checkout_Model_Cart $cart */
        $cart = Mage::getSingleton('checkout/cart');
        $customerSession->setOrderEdit(null);
        $customerSession->setOrderEditMode(false);
        $customerSession->setPackageDifferences(null);
        $customerSession->unsNewEmail();

        Mage::getSingleton('checkout/session')->unsCancelDoaItems();
        Mage::getSingleton('checkout/session')->unsDoaItems();
        Mage::getSingleton('checkout/session')->unsCancelRefundReason();

        $cart->exitEditMode();
    }

    /**
     * Locks given super order to current logged in agent
     *
     * @param $superOrderId
     * @param null $agentId
     * @return bool
     */
    public function lockOrder($superOrderId, $agentId = null, $reason = 'Test Reason')
    {
        /** @var Omnius_Superorder_Model_Superorder $superOrder */
        $superOrder = Mage::getModel('superorder/superorder')->load($superOrderId);

        $session = $this->_getCustomerSession();
        if (!$agentId) {
            $agent = $session->getAgent(true);
            $agentId = $agent->getId();
        }

        // Lock order to current agent
        $lockInfo = Mage::getSingleton('customer/session')->getOrderLockInfo();
        if(isset($lockInfo[$superOrder->getId()])){
            $lockInfo[$superOrder->getId()]['lock']['lock_state'] = "Locked";
            $lockInfo[$superOrder->getId()]['lock']['user_i_d'] = $session->getAgent(true)->getId();
            Mage::getSingleton('customer/session')->setOrderLockInfo($lockInfo);
        }

        return true;
    }

    /**
     * Releases order lock
     *
     * @param $superOrderId
     * @param null $agentId
     * @param bool $sendToAxi
     * @throws Exception
     */
    public function releaseLockOrder($superOrderId, $agentId = null, $sendToAxi = true)
    {
        /** @var Omnius_Superorder_Model_Superorder $superOrder */
        $superOrder = Mage::getModel('superorder/superorder')->load($superOrderId);

        if ($sendToAxi && !$agentId) {
            $session = $this->_getCustomerSession();
            $agent = $session->getAgent(true);
            $agentId = $agent->getId();
        }

        $lockInfo = Mage::getSingleton('customer/session')->getOrderLockInfo();
        if($lockInfo && isset($lockInfo[$superOrder->getId()])){
            $lockInfo[$superOrder->getId()]['lock']['lock_state'] = "Unlocked";
            $lockInfo[$superOrder->getId()]['lock']['user_i_d'] = '';
            Mage::getSingleton('customer/session')->setOrderLockInfo($lockInfo);
        }

    }

    /**
     * Checks if super order is locked or not
     *
     * @param $superOrderId
     * @param bool $redirect
     * @return mixed
     */
    public function checkOrder($superOrderId, $redirect = true)
    {
        /** @var Omnius_Superorder_Model_Superorder $superOrder */
        $superOrder = Mage::getModel('superorder/superorder')->load($superOrderId);
        $lockData = $orderLockInfo = Mage::getSingleton('customer/session')->getOrderLockInfo() ?: array();
        $lockData = isset($lockData[$superOrderId]) ? $lockData[$superOrderId] : null;

        $lockAgentId = isset($lockData['lock']['user_i_d']) && is_numeric($lockData['lock']['user_i_d']) ? $lockData['lock']['user_i_d'] : -1;
        $lockAgent = Mage::getModel('agent/agent')->load($lockAgentId);

        if ($lockAgentId = $lockAgent->getId()) {
            $session = $this->_getCustomerSession();
            $agent = $session->getAgent(true);
            if ($agent->getId() != $lockAgentId) {
                if ($redirect) {
                    $lockAgent = Mage::getModel('agent/agent')->load($lockAgentId);
                    $agentName = trim($lockAgent->getFirstName() . " " . $lockAgent->getLastName());
                    Mage::getSingleton('core/session')->addError(sprintf($this->__('The order is currently locked by: %s'), $agentName));
                    Mage::app()->getResponse()->setRedirect(Mage::getUrl('checkout/cart'));
                    exit;
                } else {
                    return $lockAgentId;
                }
            } else {
                return $lockAgentId;
            }
        }

        //if superorder was done on Indirect channel and we are now on a different website, don't allow edit
        if (Mage::app()->getWebsite()->getId() != $superOrder->getWebsiteId()) {
            if ($redirect) {
                Mage::getSingleton('core/session')->addError($this->__('Changing orders created in the indirect channel is not permitted from other channels'));
                Mage::app()->getResponse()->setRedirect(Mage::getUrl('checkout/cart'));
                exit;
            } else {
                return 'crossChannel';
            }
        }
        return false;
    }

    /**
     * @param $string
     * @param bool $capitalizeFirstCharacter
     * @return mixed
     */
    function underscoreToCamelCase($string, $capitalizeFirstCharacter = false)
    {
        $str = str_replace(' ', '', ucwords(str_replace('_', ' ', $string)));
        return $capitalizeFirstCharacter ? $str : lcfirst($str);
    }

    /**
     * @param null $item
     * @return string
     */
    public function getTextValue($item = null)
    {
        return trim($item);
    }

    /**
     * @param $address
     * @param bool $text
     * @return string
     */
    public function formatAddressAsHtml($address, $text=false)
    {
        $html = (isset($address['street'][0])) ? $this->getTextValue($address['street'][0]) : '';
        $html .= (isset($address['street'][1])) ? ' ' . $this->getTextValue($address['street'][1]) : '';
        $html .= (isset($address['street'][2])) ? ' ' . $this->getTextValue($address['street'][2]) : '';
        $html .= ($text) ? ', ' : '<br />';
        $html .= $this->getTextValue($address['postcode']) . ', ' . $this->getTextValue($address['city']);

        $html = trim($html);
        if (strip_tags($html) == ',') {
            return '';
        }

        return $html;
    }

    /**
     * @param $items
     * @return array
     *
     * Get a list of order items or quote item and return a list of product ids
     */
    public static function getOrderQuoteItemIds($items)
    {
        $return = [];
        foreach ($items as $item) {
            $return[$item->getProductId()] = $item->getItemDoa();
        }

        return $return;
    }

    /**
     * @param $packageId
     * @param bool $items
     * @param array $itemsGroupType
     * @param $isBusiness
     * @return int
     *
     * Calculates the difference that a client (or Vodafone) has to pay based on the products that he changed in a package
     */
    public function getEditPackagePriceDifference($packageId, $items = FALSE, $itemsGroupType = array(), $isBusiness = '')
    {
        $diffPrice = 0;

        $cart = Mage::getSingleton('checkout/cart');
        if (!$cart->isChanged()) {
            return $diffPrice;
        }

        if (!isset($isBusiness)) {
            $isBusiness = $this->_getCustomerSession()->getCustomer() && $this->_getCustomerSession()->getCustomer()->getIsBusiness();
        }

        $previousPackages = Mage::getSingleton('checkout/session')->getIsEditMode() ? Mage::getSingleton('checkout/session')->getPreviousPackages() : null;
        $productIdsInPackage = array();
        $previousIdsInPackage = array();

        //if items are not provided, get them manually
        if ($items === false) {
            $items = array();
            foreach (Mage::helper('omnius_package')->getModifiedQuotes() as $quote) {
                /** @var Omnius_Checkout_Model_Sales_Quote $quote */
                if ($quote->getPackageById($packageId)->isPayed()) { //calculate price diff only if package is already payed
                    foreach ($quote->getAllItems() as $quoteItem) {
                        if ($quoteItem->getPackageId() == $packageId) {
                            $items[] = $quoteItem;
                        }
                    }
                }
            }
        }

        foreach ($items as $item) { /** @var Omnius_Checkout_Model_Sales_Quote_Item $item */
            if ($item->getQuote()->getPackageById($packageId)->isPayed()) { //calculate price diff only if package is already payed
                $productIdsInPackage[] = $item->getProductId();
            }
        }
        foreach ($previousPackages[$packageId]['items'] as $groupType => $groupItems) {
            foreach ($groupItems as $previousItem) {
                $quote = Mage::getModel('sales/quote')->load($previousItem['quote_id']); //TODO optimise
                if ($quote->getPackageById($packageId)->isPayed() && (empty($itemsGroupType) || in_array(ucfirst($groupType),
                            $itemsGroupType))
                ) { //calculate price diff only if package is already payed
                    $previousIdsInPackage[] = $previousItem['product_id'];
                    if ($groupType == Omnius_Configurator_Model_AttributeGroup::ATTR_GROUP_PROMOTIONS) {
                        $diffPrice -= $previousItem['applied_promo_amount'];
                    } else {
                        $totalFpt = 0;
                        if ($previousItem['weee_tax_applied']) {
                            $taxesUnserialized = unserialize($previousItem['weee_tax_applied']);
                            foreach ($taxesUnserialized as $tax) {
                                $totalFpt += $isBusiness ? $tax['amount'] : $tax['amount_incl_tax'];
                            }
                        }

                        $rowTotal = $previousItem['row_total'] - ($previousItem['discount_amount'] - $previousItem['hidden_tax_amount']);
                        $rowTotalIncl = $previousItem['row_total_incl_tax'] - $previousItem['discount_amount'];
                        $itemPrice = $isBusiness ? $rowTotal : $rowTotalIncl;
                        $diffPrice -= $totalFpt + $itemPrice;
                    }
                }
            }
        }
        foreach ($items as $item) {
            if ($item->getQuote()->getPackageById($packageId)->isPayed()) { //calculate price diff only if package is already payed
                $diffPrice = round($diffPrice, 2);
                $rowTotalNew = $item->getItemFinalPriceExclTax();
                $rowTotalInclNew = $item->getItemFinalPriceInclTax();
                $finalPrice = $isBusiness ? $rowTotalNew  : $rowTotalInclNew;
                $fpt = Mage::helper('tax')->getPrice($item->getProduct(), $item->getFixedProductTaxInclVat(), !$isBusiness);
                $diffPrice += $finalPrice + $fpt;
            }
        }

        return $diffPrice;
    }

    /**
     * @return array
     *
     * Return total differences in packages
     */
    public function getTotalPackagePriceDifference()
    {
        /** @var Omnius_Package_Helper_Data $packageHelper */
        $packageHelper = Mage::helper('omnius_package');
        $modifiedQuotes = $packageHelper->getModifiedQuotes();
        $diffTotalPrice = count($modifiedQuotes) ? 0 : false;

        foreach($modifiedQuotes as $editeQuote) {
            $diffPrice = $packageHelper->getPackageDifferences($editeQuote->getActivePackageId());
            $diffTotalPrice += $diffPrice;
        }

        return $diffTotalPrice;
    }

    /**
     * @return bool
     *
     * Checks if a user is on the checkout section
     */
    public static function onCheckout()
    {
        return Mage::app()->getRequest()->getModuleName() == 'checkout' && Mage::app()->getRequest()->getControllerName() == 'cart';
    }

    /**
     * @return array
     * @param $packageDatas
     *
     * Totals for order edits need to be calculate manually as they need to be computed from the original order, temporary quotes, cancelled packages, etc
     */
    public function getEditTotals($packageDatas)
    {
        $total_maf_excl = 0;
        $total_price_excl = 0;
        $total_maf_tax = 0;
        $total_price_tax = 0;
        $total_maf = 0;
        $total_price = 0;
        $additional_subtotal_price = 0;
        $additional_total_price = 0;
        $additional_tax_price = 0;

        /**
         * Loop through each package. If package is cancelled or is currently being cancelled, ignore its prices
         */
        foreach ($packageDatas as $packageData) {
            $packageModel = $packageData[0];
            $packageItems = $packageData[1];

            if ($packageModel->getStatus() != Omnius_Package_Model_Package::ESB_PACKAGE_STATUS_CANCELLED
                && ! in_array($packageModel->getPackageId(), Mage::helper('omnius_package')->getCancelledPackagesNow())
            ) {
                $packageTotals = $packageModel->getPackageTotals($packageItems);
                $total_maf_excl += $packageTotals['subtotal_maf'];
                $total_price_excl += $packageTotals['subtotal_price'];
                $total_maf_tax += $packageTotals['tax_maf'];
                $total_price_tax += $packageTotals['tax_price'];
                $total_maf += $packageTotals['total_maf'];
                $total_price += $packageTotals['total'];
                $additional_subtotal_price += $packageTotals['additional_subtotal'];
                $additional_total_price += $packageTotals['additional_total'];
                $additional_tax_price += $packageTotals['additional_tax'];
            }
        }

        return array(
            'total_maf_excl' => $total_maf_excl,
            'total_price_excl' => $total_price_excl,
            'total_maf_tax' => $total_maf_tax,
            'total_price_tax' => $total_price_tax,
            'total_maf' => $total_maf,
            'total_price' => $total_price,
            'additional_total' => $additional_total_price,
            'additional_subtotal' => $additional_subtotal_price,
            'additional_tax' => $additional_tax_price
        );
    }

    /**
     * @param $response
     * @throws Exception
     */
    public function handleProcessOrderResponse($response)
    {
        //make sure the superorder exists and update it's status
        $superOrder = Mage::getModel('superorder/superorder')
            ->getCollection()
            ->addFieldToFilter('order_number', $response['order_result']['sales_ordernumber'])
            ->getFirstItem();
        if (!$superOrder->getId()) {
            throw new Exception('No superorder found with the provided sales order number');
        }

        // retrieve all packages and update their status based on the response
        $packageCollections = Mage::getModel('package/package')->getCollection()->addFieldToFilter('order_id',
            $superOrder->getId());
        foreach ($packageCollections as $package) {
            $packages[$package->getPackageId()] = $package;
        }
    }

    /**
     * @param null $superOrderId
     * @return array
     */
    public function getContractPackages($superOrderId = null)
    {
        $packages = array();
        if ($superOrderId) {
            $orders = Mage::getModel('sales/order')->getNonEditedOrderItems($superOrderId);
            foreach ($orders as $order) {
                foreach ($order->getAllItems() as $item) {
                    $packageId = $item->getPackageId();
                    $packages[$packageId]['items'][$item->getId()] = $item;
                }
            }
        } elseif ($this->_getCustomerSession()->getOrderEdit()) {

            $quotes = Mage::helper('omnius_package')->getModifiedQuotes();
            foreach ($quotes as $quote) {
                $packages += $quote->getPackages();
            }
        } else {
            $session = Mage::getSingleton('checkout/session');
            /** @var Omnius_Checkout_Model_Sales_Quote $quote */
            $quote = $session->getQuote();
            $packages = $quote->getPackages();
        }

        return $packages;
    }

    /**
     * Based on the type, retrieve all countries, or only a specific country
     *
     * @param $type
     */
    public function getCountriesForDropdown($type)
    {
        switch ($type) {

            case 'N' :
            case 'R' :
                $result = [['value' => 'NL', 'label' => $this->__('Netherlands')]];
                break;
            case 'P' :
            case '0' :
            case '1' :
            case '2' :
            case '3' :
            case '4' :
                $result = $this->getAllCountries();
                break;
            default:
                $result = $this->getAllCountries();
        }

        return $result;
    }

    /**
     * @return mixed
     */
    public function getEuCountries()
    {
        $cache = $this->getCache();
        if ($result = $cache->load('cached_eu_countries')) {
            return unserialize($result);
        }

        $euCountries = explode(',', Mage::getStoreConfig(Mage_Core_Helper_Data::XML_PATH_EU_COUNTRIES_LIST));

        $all = Mage::getResourceModel('directory/country_collection')
            ->loadData()
            ->toOptionArray(false);
        foreach ($all as $key => $node) {
            if (!in_array($node['value'], $euCountries)) {
                unset($all[$key]);
            }
        }

        $cache->save(serialize($all), 'cached_eu_countries');
        return $all;
    }

    /**
     * @return mixed
     */
    public function getAllCountries()
    {
        $cache = $this->getCache();
        if ($result = $cache->load('cached_all_countries')) {
            return unserialize($result);
        }

        $all = Mage::getResourceModel('directory/country_collection')->loadData()->toOptionArray(false);

        $cache->save(serialize($all), 'cached_all_countries');
        return $all;
    }

    /**
     * Check if the package with the provided id has delivery set to this store from the quote
     *
     * @param $packageId
     * @return bool
     */
    public function checkDeliveryInThisStore($quote, $packageId = null) {
        // Retrieve the previously saved data for addresses
        $data = Mage::helper('core')->jsonDecode($quote->getShippingData());
        $storeId = $this->_getCustomerSession()->getAgent(true)->getDealer()->getId();
        $allPackages = false;

        if( ! $packageId) {
            $allPackages = true;
            $packageId = 1;
        }
        $addressArray = array();
        if (isset($data['deliver'])) { // Single delivery
            $addressArray[$packageId] = $data['deliver']['address'];
        } elseif (isset($data['pakket'])) { // Split delivery
            if( ! $allPackages) {
                $addressArray[$packageId] = $data['pakket'][$packageId]['address'];
            } else {
                foreach($data['pakket'] as $address) {
                    if($address['address']['address'] == 'store' && $address['address']['store_id'] == $storeId) {
                        return true;
                    } else {
                        $addressArray[$packageId] = $address['address'];
                    }
                }
            }
        }

        if ( ! empty($addressArray[$packageId]) && $addressArray[$packageId]['address'] == 'store' && $addressArray[$packageId]['store_id'] == $storeId ) {
            return true;
        }

        return false;
    }

    /**
     * By default, shipping costs should be hidden (FogBugz 3341). However, based on FogBugz 2029 these will be displayed on phase 4
     */
    public function showShippingCosts()
    {
        return false;
    }

    /**
     * Get the edited quotes in order edit mode
     *
     * @param Omnius_Checkout_Model_Sales_Quote $quote
     * @return array
     */
    public function getModifiedPackages($quote = null)
    {
        if (!$quote) {
            $quote = Mage::getModel('sales/quote')->load($this->_getCustomerSession()->getOrderEdit());
        }

        $modifiedQuotes = $quote->getListOfModifiedQuotes($quote->getSuperOrderEditId());

        $packageIds = array();
        /** @var Omnius_Checkout_Model_Sales_Quote $modifiedQuote */
        foreach ($modifiedQuotes as $modifiedQuote) {
            $packageIds = array_merge($packageIds, array_keys($modifiedQuote->getPackages()));
        }
        return array_unique($packageIds);
    }

    /**
     * Get specific packages info
     *
     * @param array $packages
     * @return array
     */
    public function getCartPackages($packages)
    {
        $cartPackages = array();
        if ($packages) {
            foreach ($packages as $packageId => $package) {
                $packageName = '';
                foreach ($package['items'] as &$item) {
                    $packageName .= $item->getName() . ', ';
                }
                unset($item);
                $cartPackages[$packageId] = array(
                    'type' => $package['type'],
                    'name' => rtrim($packageName, ', '),
                    'current_number' => $package['current_number'],
                    'sale_type' => $package['sale_type'],
                    'tel_number' => $package['tel_number'],
                    'one_of_deal' => $package['one_of_deal']
                );
            }
        }

        return $cartPackages;
    }

    /**
     * Retrieve the initial product ids of the package
     *
     * @param Omnius_Checkout_Model_Sales_Quote $quote
     * @param $packageId
     * @return array
     */
    public function getOldPackageProducts($quote, $packageId)
    {
        $oldItems = array();
        $found = false;
        $modifiedQuotes = $quote->getListOfModifiedQuotes($quote->getSuperOrderEditId());
        foreach ($modifiedQuotes as $modifiedQuote) {
            /** @var Omnius_Checkout_Model_Sales_Quote_Item $item */
            foreach ($modifiedQuote->getAllVisibleItems() as $item) {
                if($item->getPackageId() == $packageId) {
                    if($item->getProduct()->isOfHardwareType()) {
                        $oldItems[] = $item->getProductId();
                    }
                    $found = true;
                } else {
                    // Since the quote can only have one packageId, break after first item, because it will not be found
                    break;
                }
            }
            // Since there can only be one quote for that packageId, stop after it was found
            if($found) {
                break;
            }
        }

        return array_unique($oldItems);
    }

    /**
     * Check what items have changed in the current package (ignore subscription changes)
     */
    public function getPackagesDifferences($quote)
    {
        $changes = array();
        $current = array();
        $old = array();
        $productNames = array();
        /** @var Omnius_Checkout_Model_Sales_Quote $quote */
        $modifiedQuotes = $quote->getListOfModifiedQuotes($quote->getSuperOrderEditId());
        /** @var Omnius_Checkout_Model_Sales_Quote $modifiedQuote */
        foreach ($modifiedQuotes as $modifiedQuote) {
            /** @var Omnius_Checkout_Model_Sales_Quote_Item $item */
            foreach ($modifiedQuote->getAllVisibleItems() as $item) {
                if (!isset($current[$item->getPackageId()])) {
                    $current[$item->getPackageId()] = array();
                }

                $current[$item->getPackageId()][] = $item->getProductId();
                $productNames[$item->getProductId()] = $item->getProduct()->getName();
                $currentDoa[$item->getPackageId()][$item->getProductId()] = (bool)$item->getItemDoa();
            }
        }
        foreach ($quote->getAllVisibleItems() as $item) {
            if (!isset($current[$item->getPackageId()])) {
                $old[$item->getPackageId()] = array();
            }

            $old[$item->getPackageId()][] = $item->getProductId();
            $productNames[$item->getProductId()] = $item->getProduct()->getName();
            $oldDoa[$item->getPackageId()][$item->getProductId()] = (bool)$item->getItemDoa();
        }

        foreach ($old as $packageId => $items) {
            foreach ($items as $item) {
                if (isset($current[$packageId])
                    &&
                    (!in_array($item, $current[$packageId])
                    || (isset($currentDoa[$packageId][$item]) && isset($oldDoa[$packageId][$item]) && $currentDoa[$packageId][$item] != $oldDoa[$packageId][$item]))
                ) {
                    $changes[$packageId][] = $productNames[$item];
                }
            }
        }
        return $changes;
    }

    /**
     * @param $quote
     * @param $packages
     * @param bool $editMode
     * @return array
     */
    public function getRefundAmounts($quote, $packages, $editMode = false)
    {
        $totalRefund = 0;
        $packagesRefund = array();

        if (!empty($packages)) {
            foreach($packages as $packageId) {
                if($editMode) {
                    $packageDiffPrice = Mage::helper('omnius_package')->getPackageDifferences($packageId);
                    $totalRefund += $packageDiffPrice;
                    $packagesRefund[$packageId] = $packageDiffPrice;
                } else {
                    $totals = Mage::helper('omnius_configurator')->getActivePackageTotals(
                        $packageId,
                        ! ($this->_getCustomerSession()->getCustomer() && $this->_getCustomerSession()->getCustomer()->getIsBusiness()),
                        $quote->getId()
                    );

                    $totalRefund += $totals['totalprice'];
                    $packagesRefund[$packageId] = $totals['totalprice'];
                }
            }
        }
        return array('packagesRefund' => $packagesRefund, 'totalRefund' => $totalRefund);
    }

    /**
     * @return bool
     */
    public function showSuspectFraudCheck()
    {
        return true;
    }

    /**
     * Returns the default value of a field from DB
     *
     * @param $field
     * @param null $section
     * @param null $storeId
     * @return null
     */
    public function getDefaultValueForField($field, $section = null, $storeId = null)
    {
        // If no section provided, return null
        if ( ! $section) {
            return null;
        }

        $storeId = $storeId
            ? is_object($storeId)
                ? $storeId->getId()
                : $storeId
            : Mage::app()->getStore()->getId();

        $key = sprintf('default_values_for_%s_%s_%s', $field, $section, $storeId);
        if (false !== ($result = $this->getCache()->load($key))) {
            return unserialize($result);
        } else {
            $conn = Mage::getSingleton('core/resource')->getConnection('core_read');
            if ( ! is_numeric($section)) {
                $section = Mage::getResourceModel('field/section_collection')
                    ->addFieldToFilter('name', $section);
                if (false === ($section = $conn->fetchOne($section->getSelect()))) {
                    $this->getCache()->save('N;', $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
                    return null;
                }
            }
            $sectionId = $section;

            $value = Mage::getResourceModel('field/field_collection')
                ->addFieldToSelect('default_value')
                ->addFieldToFilter('store_id', $storeId)
                ->addFieldToFilter('section_id', $sectionId)
                ->addFieldToFilter('name', $field);

            if (false === ($value = $conn->fetchOne($value->getSelect()))) {
                $this->getCache()->save('N;', $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
                return null;
            }

            $this->getCache()->save(serialize($value), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
            return $value;
        }
    }

    /**
     * Check if the delivery steps (number selection and contract) can be displayed
     *
     * @param $orderedItems
     * @return bool
     */
    public function canProcessDeliverySteps($orderedItems)
    {
        return true;
    }

    /**
     * @param $price
     * @param $item
     * @param $showPriceWithoutBtw
     * @return string
     */
    public function decorateExclIncl($price, $item, $showPriceWithoutBtw)
    {
        $product = $item->getProduct();
        $showPriceWithoutBtw = ($showPriceWithoutBtw == false) ?: !$this->_getCustomerSession()->showPriceWithBtw();
        $excl = Mage::helper('tax')->getPrice($product, $price, false);
        $incl = Mage::helper('tax')->getPrice($product, $price, true);
        // TAX Display
        $priceStyle = $showPriceWithoutBtw ? '' : 'display: none;';
        $priceTaxStyle =  !$showPriceWithoutBtw ? '' : 'display: none;';
        return
            '<div class="excl-vat-tax" style="'. $priceStyle. '">' .
            Mage::helper('core')->currency($excl) .
            '</div>
            <div class="incl-vat-tax" style="' . $priceTaxStyle. '">' .
            Mage::helper('core')->currency($incl) .
            '</div>';
    }

    /**
     * Creates the refund methods options structure
     * @param bool $asOptions Return as formatted string to be used in select
     * @param bool $firstKey Return only the first option
     * @param float $refundAmount
     * @return bool|string|array
     */
    public function getRefundMethods($asOptions = false, $firstKey = false, $refundAmount = null)
    {
        // TODO: maybe get this options from backend config
        $optionsArr = array(
            'dss'   => 'DSS refund form',
            'cash'  => 'Refund via cashier',
        );

        if ($firstKey) {
            return key($optionsArr);
        }

        if ($asOptions) {
            $html = '';
            foreach ($optionsArr as $k =>$v){
                $html .= '<option value="' . $k . '">' . $this->__($v) . '</option>';
            }
            return $html;
        }

        return $optionsArr;
    }

    /**
     * Creates the refund reasons options structure for change
     * @param bool $asOptions Return as formatted string to be used in select
     * @param bool $firstKey Return only the first option
     * @return bool|string|array
     */
    public function getRefundReasonsChange($asOptions = false, $firstKey = false)
    {
        $isError = false;
        if (!$optionsArr = Mage::helper('omnius_customer')->getRefundReasonsChangeOptions()) {
            $isError = true;
            $return = false;
        }

        if (!$isError && $firstKey) {
            $isError = true;
            $return = key($optionsArr);
        }

        if (!$isError && $asOptions) {
            $isError = true;
            $html = '';
            foreach ($optionsArr as $k => $v) {
                $html .= '<option value="' . $k . '">' . $this->__($v) . '</option>';
            }
            $return = $html;
        }

        if ($isError) {
            return $return;
        }

        return $optionsArr;
    }

    /**
     * Creates the refund reasons options structure for cancel
     * @param bool $asOptions Return as formatted string to be used in select
     * @param bool $firstKey Return only the first option
     * @return bool|string|array
     */
    public function getRefundReasonsCancel($asOptions = false, $firstKey = false)
    {
        $isError = false;
        if (!$isError && !$optionsArr = Mage::helper('omnius_customer/data')->getRefundReasonsCancelOptions()) {
            $isError = true;
            $return = false;
        }

        if (!$isError && $firstKey) {
            $isError = true;
            $return = key($optionsArr);
        }

        if (!$isError && $asOptions) {
            $isError = true;
            $html = '';
            foreach ($optionsArr as $k => $v) {
                $html .= '<option value="' . $k . '">' . $this->__($v) . '</option>';
            }

            $return = $html;
        }

        if ($isError) {
            return $return;
        }

        return $optionsArr;
    }

    /**
     * Format the date based on locale settings
     * @param $date
     * @param string $format
     * @return string
     */
    public function formatDateLocale($date, $format = 'medium')
    {
        return Mage::app()->getLocale()->date(Mage::getModel('core/date')->timestamp(strtotime($date)), null, null, false)->toString(Mage::app()->getLocale()->getDateTimeFormat($format));
    }

    /**
     * Create an array with all the packages that are disabled (cannot apply any change) in this session
     *
     * @param array $packages
     * @param int $superOrderEditId
     * @return array
     */
    public function checkForDisabledPackages($packages, $superOrderEditId)
    {
        $disabledPackages = array();
        $packageHelper = Mage::helper('omnius_package');
        $packageModel = Mage::getModel('package/package');

        $cancelledPackages = $packageHelper->getCancelledPackages(); // Edited packages of the SuperOrder
        $cancelledPackagesNow = $packageHelper->getCancelledPackagesNow(); // Edited packages in this session
        $allCancelledPackages = array_merge($cancelledPackages, $cancelledPackagesNow);

        $editedPackagesIds = Mage::registry('editedPackages'); // Edited packages in this session.
        if (empty($editedPackagesIds)) {
            $editedPackagesIds = array();
        }
        $isLocked = $this->checkOrder($superOrderEditId, false) === true; // SuperOrder is locked or not
        $childOrders = Mage::getModel('superorder/superorder')->getCollection()->addFieldToFilter('parent_id', $superOrderEditId);
        if ( count($editedPackagesIds) > 0 ) {
            //get the status of the edited package
            $editedPackageStatus = $packageModel->getPackages($superOrderEditId, null, $editedPackagesIds[0])->getFirstItem()->getStatus();
        } else {
            $editedPackageStatus = null;
        }

        foreach($packages as $packageId => $quotePackage) {
            foreach ($childOrders as $childOrder) {
                $childPackage = Mage::getModel('package/package')->getPackages($childOrder->getId(), null, $packageId)->getFirstItem();
                if ($childPackage->getId()) {
                    break;
                }
            }
            $package = $packageModel->getPackages($superOrderEditId, null, $packageId)->getFirstItem();
            $isCanceled = in_array($package->getPackageId(), $allCancelledPackages);
            $isModified = in_array($packageId, $editedPackagesIds);
            $disabledPackages[$packageId] = ( ! $package->getSuperOrder()->isEditable())
                || (count($editedPackagesIds)
                    && (($editedPackageStatus && ($package->getStatus() !== $editedPackageStatus))
                        || $isLocked
                        || ($isModified  !== false)))
                || ($isCanceled !== false)
                || (isset($childPackage) && $childPackage->getId());
        }

        return $disabledPackages;

    }

    /**
     * Strip all characters from string, and return upper case letters followed by a dot
     *
     * @param $input
     * @return string
     */
    public function processInitials($input)
    {
        $input = preg_replace('/[0987654321\s\.\~\`\!\@\#\$\%\^\&\*\(\)\_\+\=\-\[\]\{\}\;\'\:\|\"\<\>\,\/\?\\"]/', '', $input);

        if(empty($input)) {
            return '';
        }

        $text = array();
        for ($i = 0; $i < mb_strlen($input); ++$i) {
            $text[] = mb_strtoupper(mb_substr($input, $i, 1));
        }
        return implode('.', $text) . '.';
    }

    /**
     * @param $address
     * @return array
     * @throws Exception
     */
    public function _getAddressByType($address)
    {
        switch ($address['address']) {
            case 'store' :
                /** @var Dyna_Agent_Model_Mysql4_Dealer_Collection $addressData */
                $addressData = Mage::getModel('agent/dealer')->getCollection()
                    ->addFieldToFilter('dealer_id', $address['store_id']);
                $addressData->fetchItem();

                if ($addressData) {
                    $addressData = $addressData->getFirstItem();
                    $result = array(
                        'street' => array(trim($addressData['street']), trim($addressData['house_nr']), ''),
                        'postcode' => trim($addressData['postcode']),
                        'city' => trim($addressData['city']),
                    );
                    $result['store_id'] = $address['store_id'];
                }
                break;
            case 'direct' :
                $storeId = $this->_getCustomerSession()->getAgent(true)->getDealer()->getId();
                /** @var Dyna_Agent_Model_Mysql4_Dealer_Collection $addressData */
                $addressData = Mage::getModel('agent/dealer')->getCollection()
                    ->addFieldToFilter('dealer_id', $storeId)
                    ->fetchItem();
                if ($addressData) {
                    $addressData = $addressData->getData();
                    $address['address'] = 'store';
                    $result = array(
                        'street' => array(trim($addressData['street']), trim($addressData['house_nr']), ''),
                        'postcode' => trim($addressData['postcode']),
                        'city' => trim($addressData['city']),
                    );
                    $result['store_id'] = $storeId;
                }
                break;
            case 'billing_address' :
                if(!isset($address['billingAddress'])) {
                    throw new Exception($this->__('Incomplete data'));
                }

                $result = array(
                    'street' => array($address['billingAddress']['street'], $address['billingAddress']['houseno'], $address['billingAddress']['addition']),
                    'postcode' => $address['billingAddress']['postcode'],
                    'city' => $address['billingAddress']['city'],
                );
                break;
            case 'foreign_address' :
                if (!isset($address['foreignAddress'])) {
                    throw new Exception($this->__('Incomplete data'));
                }
                $temp = $address['foreignAddress'];
                $result = $temp;
                // Build street data from input data
                $result['street'] = array($temp['street']);

                $result['extra_street'] = array();
                if (isset($temp['extra']) && is_array($temp['extra'])) {
                    foreach ($temp['extra'] as $extraLine) {
                        if (!empty($extraLine)) {
                            $result['extra_street'][] = $extraLine;
                        }
                    }
                }
                break;
            case 'other_address' :
                if(!isset($address['otherAddress'])) {
                    throw new Exception($this->__('Incomplete data'));
                }
                $result = array(
                    'street' => array($address['otherAddress']['street'], $address['otherAddress']['houseno'], $address['otherAddress']['addition']),
                    'postcode' => $address['otherAddress']['postcode'],
                    'city' => $address['otherAddress']['city'],
                );
                break;
            default:
                throw new Exception($this->__('Incomplete data'));
        }
        $result['address'] = $address['address'];
        return $result;
    }

    /**
     * @param $data
     * @param string $prefix
     * @return array
     * @throws Exception
     */
    public function _validateDeliveryAddressData($data, $prefix = '')
    {
        $errors = [];
        if(is_array($data['address'])) {
            // FOR SINGLE DELIVERY OTHER_ADDRESS METHOD
            $addressValidations = $this->getStoreValidations();
            $errors += $this->_performValidate($data['address']['other_address'], $addressValidations);
        } else {
            switch($data['address']) {
                case 'store':
                    $addressValidations = $this->getStoreValidations();
                    $errors += $this->_performValidate($data, $addressValidations, strlen($prefix) ? $prefix : 'store[', ']');
                    break;
                case 'deliver':
                case 'billing_address':
                case 'other_address' :
                case 'shipping_address' :
                    $addressValidations = $this->getOtherAddressValidations();
                    $errors += $this->_performValidate($data, $addressValidations, strlen($prefix) ? $prefix : 'otherAddress[', ']');
                    break;
                case 'foreign_address' :
                    $addressValidations = $this->getForeignAddressValidations();
                    $errors += $this->_performValidate($data, $addressValidations, strlen($prefix) ? $prefix : 'foreignAddress[', ']');
                    break;
                default :
                    throw new Exception($this->__('Missing delivery address data'));
            }
        }

        return $errors;
    }

    /**
     * @param array $data
     * @param array $validations
     * @param null /string $prefix
     * @param null /string $suffix
     * @return array
     * @throws Mage_Customer_Exception
     */
    public function _performValidate($data, $validations, $prefix = null, $suffix = null)
    {
        $errors = [];
        foreach ($validations as $key => $values) {
            if ($values['required'] && ((!isset($data[$key])) || (empty($data[$key]) && strlen($data[$key]) == 0))) {
                $errors[$prefix . $key . $suffix] = $this->__('This is a required field.');
                continue;
            }

            if (!$values['required'] && (!isset($data[$key]) || empty($data[$key]))) {
                continue;
            }

            if (isset($values['validation']) && is_array($values['validation'])) {
                $this->validateRules($data, $prefix, $suffix, $values, $errors, $key);
            }
        }

        return $errors;
    }

    /**
     * Get checkout session model instance
     *
     * @return Mage_Customer_Model_Session
     */
    protected function _getCustomerSession()
    {
        return Mage::getSingleton('customer/session');
    }

    /**
     * @param $quote
     * @param $superOrderId
     * @return bool|false|Mage_Core_Model_Abstract
     */
    public function createNewCartQuote($quote, $superOrderId)
    {
        if (!$quote->getId()) {
            return false;
        }

        // Check if superorder has been edited
        $count = Mage::getModel('sales/order')->getCollection()
            ->addFieldToSelect('entity_id')
            ->addFieldToFilter('superorder_id', $superOrderId)
            ->addFieldToFilter('edited', 1)
            ->count();

        if ($count) {
            return false;
        }

        // Create a new quote and set it active
        $newQuote = $this->cloneQuote($quote->getId());

        if ($quote->getSaveAsCart()) {
            // Mark the new quote as saved shopping cart
            $newQuote->setCartStatus(Omnius_Checkout_Model_Sales_Quote::CART_STATUS_SAVED)
                ->setActivePackageId(1)
                ->save();
        }

        // Create the packages for the new quote
        $packages = Mage::getModel('package/package')->getCollection()
            ->addFieldToFilter('order_id', $superOrderId);

        foreach ($packages as $package) {
            Mage::getModel('package/package')
                ->setData($package->getData())
                ->setEntityId(null)
                ->setStatus(null)
                ->setCreditcheckStatus(null)
                ->setCreditcheckStatusUpdated(null)
                ->setVfStatusCode(null)
                ->setVfStatusDesc(null)
                ->setPortingStatus(null)
                ->setPortingStatusUpdated(null)
                ->setQuoteId($newQuote->getId())
                ->setOrderId(null)
                ->save();
        }

        if ($quote->getQuoteParentId()) {
            $parentQuote = Mage::getModel('sales/quote')->load($quote->getQuoteParentId());
            if ($parentQuote->getId()) {
                $parentQuote->setCartStatus(null)
                    ->save();
            }
        }

        return $newQuote;
    }

    /**
     * Returns a virtual delivery address used in Indirect channel
     *
     * @return array
     */
    public function getVirtualDeliveryAddress()
    {
        return array(
            'street' => array('Virtual delivery','1',''),
            'postcode' => '1111aa',
            'city' => 'Virtual city',
            'address' => 'billing_address'
        );
    }

    /**
     * Transform names to the format needed in EBS
     *
     * @param string $name
     * @param int $limit
     * @return string
     */
    public function formatNameForEbsOrchestration($name, $limit)
    {
        $name = mb_strtoupper($name);

        // Limit last name to x characters
        return substr($name, 0, $limit);
    }

    /**
     * Checks whether the current session contains any DOA changes.
     * @return bool
     */
    public function sessionHasDoa()
    {
        $cancelDoaItems = Mage::getSingleton('checkout/session')->getCancelDoaItems();
        $isDoa = false;
        if (is_array($cancelDoaItems)) {
            foreach ($cancelDoaItems as $doaItems) {
                if (count(unserialize($doaItems)) > 0) {
                    $isDoa = true;
                    break;
                }
            }
        }

        return $isDoa;
    }

    /**
     * @param int $id
     */
    public function unsetSavedCart($id)
    {
        $removeSavedCart = Mage::getModel('sales/quote')->load($id);
        if ($removeSavedCart->getId()) {
            $removeSavedCart->setCartStatus(null)->save();
        }
    }

    /**
     * Decrement sales rules on customer unload.
     */
    public function decrementQuotesOnUnload()
    {
        /** @var Omnius_Checkout_Model_Sales_Quote[] $modifiedQuotes */
        $modifiedQuotes = Mage::helper('omnius_package')->getModifiedQuotes();
        foreach ($modifiedQuotes as $modifiedQuote) {
            Mage::helper('pricerules')->decrementAll($modifiedQuote);
        }

        /** @var Omnius_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getModel('checkout/cart')->getQuote();

        if (
            !$quote->getIsOffer()
            && (
                !$quote->getSuperOrderEditId() || (
                    $quote->getSuperOrderEditId()
                    && $quote->getSuperQuoteId()
                )
            )
        ) {
            Mage::helper('pricerules')->decrementAll($quote);
        }
    }

    /**
     * @param $data
     * @param $prefix
     * @param $suffix
     * @param $values
     * @param $errors
     * @param $key
     */
    protected function validateRules($data, $prefix, $suffix, &$values, &$errors, $key)
    {
        foreach ($values['validation'] as $rule) {
            // If another error is already set, don't validate
            if (isset($errors[$prefix . $key . $suffix])) {
                continue;
            }
            if (!Mage::helper('omnius_validators/data')->{$rule}($data[$key])) {
                if (!empty($values['message'][$rule])) {
                    $errors[$prefix . $key . $suffix] = $this->__($values['message'][$rule]);
                } else {
                    $errors[$prefix . $key . $suffix] = $this->__('Field has invalid data');
                }
                continue;
            }
        }
    }

    /**
     * @return array
     */
    protected function getStoreValidations()
    {
        $addressValidations = array(
            'street' => array(
                'required' => true,
                'validation' => array()
            ),
            'postcode' => array(
                'required' => true,
                'validation' => array(
                )
            ),
            'city' => array(
                'required' => true,
                'validation' => array()
            ),
        );

        return $addressValidations;
    }

    /**
     * @return array
     */
    protected function getOtherAddressValidations()
    {
        $addressValidations = array(
            'street' => array(
                'required' => true,
                'validation' => array(
                    'validateStreet'
                ),
                'message' => ['validateStreet' => $this->__('Street name and number are mandatory')]
            ),
            'houseno' => array(
                'required' => false,
                'validation' => array()
            ),
            'addition' => array(
                'required' => false,
                'validation' => array()
            ),
            'postcode' => array(
                'required' => true,
                'validation' => array(
                )
            ),
            'city' => array(
                'required' => true,
                'validation' => array()
            ),
        );

        return $addressValidations;
    }

    /**
     * @return array
     */
    protected function getForeignAddressValidations()
    {
        $addressValidations = array(
            'street' => array(
                'required' => true,
                'validation' => array()
            ),
            'houseno' => array(
                'required' => false,
                'validation' => array()
            ),
            'addition' => array(
                'required' => false,
                'validation' => array()
            ),
            'extra' => array(
                'required' => false,
                'validation' => array()
            ),
            'city' => array(
                'required' => true,
                'validation' => array()
            ),
            'region' => array(
                'required' => false,
                'validation' => array()
            ),
            'postcode' => array(
                'required' => true,
                'validation' => array()
            ),
            'country_id' => array(
                'required' => true,
                'validation' => array()
            ),
        );

        return $addressValidations;
    }


    public function getBusinessLegalForms($code = null, $store = null)
    {
        $allOptions = $this->_prepareOptions(Mage::getStoreConfig('vodafone_customer/company_types/legal_forms', $store));
        if ($code) {
            foreach ($allOptions as $_code => $label) {
                if ($code == $_code) {
                    return $label;
                }
            }

            return null;
        }

        return $allOptions;
    }

    /**
     * Unserialize and clear options
     *
     * @param string $options
     * @return array|bool
     */
    protected function _prepareOptions($options)
    {
        $options = trim($options);
        if (empty($options)) {
            return false;
        }
        $result = array();
        $options = explode("\n", $options);
        foreach ($options as $value) {
            $values = explode(':', trim($value));
            $code = '';
            $label = '';

            if(strlen($values[0])) {
                $code = $this->escapeHtml(trim($values[0]));
                $label = $this->escapeHtml(trim($values[0]));
            }

            if(isset($values[1])) {
                $label = $this->escapeHtml(trim($values[1]));
            }

            $result[$code] = $this->__($label);
        }
        return $result;
    }
}
