<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Pdf
 */
class Omnius_Checkout_Helper_Pdf extends Mage_Core_Helper_Abstract
{
    const PDF_EMAIL_PATH  = '/var/contract_email';

    /**
     * @param $contractOutput
     * @param $identifier
     * @param string $ext
     * @param bool $superOrder
     * @return string
     */
    public function saveContract($contractOutput, $identifier, $ext = 'pdf', $superOrder = false)
    {
        if (!$superOrder) {
            $filename = $this->getPath($identifier, $ext);
            if (!@file_put_contents($filename, $contractOutput)) {
                Mage::throwException(sprintf('Failed to save contract data at path %s', $filename));
            }

            if (substr(sprintf('%o', fileperms($filename)), -4) != "0777") {
                @chmod($filename, 0777); // Set 777 to ensure the ESB SFTP user can delete and read the files
            }
            return $filename;
        }
        $emailPath = $this->getPath($identifier . '_' . date("YmdHis"), $ext, true);
        if ( ! @file_put_contents($emailPath, $contractOutput)) {
            Mage::throwException(sprintf('Failed to save contract data at path %s', $emailPath));
        }
		chmod($emailPath, 0777); // So both deamons and webserver can write

        return $emailPath;
    }

    /**
     * @param $quote
     * @return string
     * @throws Mage_Core_Exception
     */
    public function saveOffer($quote){

        $printOutput = Mage::app()->getLayout()
            ->createBlock('omnius_checkout/pdf_offer')
            ->setQuote($quote)
            ->setTemplate('checkout/cart/pdf/offer.phtml')
            ->toHtml();

        $pdfInstance = new VFDOMPDF;
        $pdfInstance->load_html($printOutput);
        $pdfInstance->render();
        $pdfOutput = $pdfInstance->output();

        $filename = $this->getOfferPath($quote->getId());
        if ( ! @file_put_contents($filename, $pdfOutput)) {
            Mage::throwException(sprintf('Failed to save offer data at path %s', $filename));
        }



        return $filename;
    }

    /**
     * @param $order
     * @param $packages
     * @return array
     * @throws Mage_Core_Exception
     */
    public function saveWarranty($order, $packages)
    {
        $filenames = array();

        foreach($packages as $pid => $packageItems) {
            /**
             * Save warranty to disk
             */
            /** @var $warrantyOutput Omnius_Checkout_Block_Pdf_Warranty */
            $warrantyOutput = Mage::getSingleton('core/layout')
                ->createBlock('omnius_checkout/pdf_warranty')
                ->setTemplate('checkout/cart/pdf/warranty.phtml')
                ->init($order, $packageItems, $pid);

            $warranties = $warrantyOutput->getWarrantyProducts();
            foreach($warranties as $warranty) {
                /**
                 * Generate the PDF version of the HTML file
                 * */
                $pdfInstance = new VFDOMPDF;

                $pdfInstance->load_html($warrantyOutput->setWarrantyProduct($warranty)->toHtml());
                $pdfInstance->render();
                $pdfOutput = $pdfInstance->output();

                $identifier = $order->getIncrementId() . '_' . $pid . '_' . $warranty->getId();
                $filename = $this->getWarrantyPath($identifier, 'pdf');
                if ( ! @file_put_contents($filename, $pdfOutput)) {
                    Mage::throwException(sprintf('Failed to save contract data at path %s', $filename));
                }
                else {
                    $filenames[] = $filename;
                }
            }
        }

        return $filenames;
    }

    /**
     * @param $identifier
     * @param string $ext
     * @param bool $isEmail
     * @return string
     */
    public function getPath($identifier, $ext = 'pdf', $isEmail = false)
    {
        return sprintf('%s/contract_%s.%s', $this->assureDirExistence($isEmail), $identifier, $ext);
    }

    /**
     * @param $identifier
     * @param string $ext
     * @return string
     */
    public function getWarrantyPath($identifier, $ext = 'pdf')
    {
        return sprintf('%s/warranty_%s.%s', $this->assureDirExistence(true), $identifier, $ext);
    }

    /**
     * @param $identifier
     * @param string $ext
     * @return string
     */
    public function getOfferPath($identifier, $ext = 'pdf')
    {
        return sprintf('%s/offer_new_%s.%s', $this->assureDirExistence(true), $identifier, $ext);
    }

    /**
     * @param bool $isEmail
     * @return string
     * @throws Mage_Core_Exception
     */
    protected function assureDirExistence($isEmail = false)
    {
        if (!$isEmail) {
            $path = Mage::getBaseDir() . '/var/contract';
        } else {
            $path = Mage::getBaseDir() . self::PDF_EMAIL_PATH;
        }

        if (!realpath($path) && !@mkdir($path, 0777, true)) {
            Mage::throwException(sprintf('Failed to create contracts directory at path %s', $path));
        }
        return $path;
    }
} 