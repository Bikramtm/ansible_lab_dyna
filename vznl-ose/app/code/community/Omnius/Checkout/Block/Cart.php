<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Checkout_Block_Cart extends Mage_Checkout_Block_Cart
{
    protected $_customerSession = null;
    protected $_checkoutHelper = null;
    protected $_packageHelper = null;
    protected $catalogHelper = null;
    protected $_cartPackages = null;
    protected $_packages = null;
    private $_changedQuotes = null;

    /**
     * Get instance of the Omnius_Customer_Model_Session
     *
     * @return Omnius_Customer_Model_Session
     */
    public function getCustomerSession()
    {
        if (! $this->_customerSession) {
            $this->_customerSession = Mage::getSingleton('customer/session');
        }

        return $this->_customerSession;
    }

    /**
     * Get instance of the Omnius_Customer_Model_Session
     *
     * @return Omnius_Customer_Model_Session
     */
    public function getCatalogHelper()
    {
        if (! $this->catalogHelper) {
            $this->catalogHelper = Mage::helper('omnius_catalog');
        }

        return $this->catalogHelper;
    }

    /**
     * Get instance of the Omnius_Checkout_Helper_Data
     *
     * @return Omnius_Checkout_Helper_Data
     */
    public function getCheckoutHelper()
    {
        if (! $this->_checkoutHelper) {
            $this->_checkoutHelper = Mage::helper('omnius_checkout');
        }

        return $this->_checkoutHelper;
    }

    /**
     * Get instance of the Omnius_Package_Helper_Data
     *
     * @return Omnius_Package_Helper_Data
     */
    public function getPackageHelper()
    {
        if (! $this->_packageHelper) {
            $this->_packageHelper = Mage::helper('omnius_package');
        }

        return $this->_packageHelper;
    }

    /**
     * Retrieve active quote
     *
     * @return Omnius_Checkout_Model_Sales_Quote
     */
    public function getQuote()
    {
        if (! isset($this->_quote)) {
            $this->_quote = Mage::registry('quote');

            if (! $this->_quote) {
                // quote not found or empty in registry
                if ($this->getCustomerSession()->getOrderEdit()) {
                    $this->_quote = Mage::getSingleton('checkout/cart')->getQuote(true);
                } else {
                    $this->_quote = Mage::getSingleton('checkout/cart')->getQuote();
                }

                Mage::register('quote', $this->_quote);
            }
        }

        return $this->_quote;
    }

    /**
     * Get quote packages with all their items
     *
     * @return array|null
     */
    public function getPackages()
    {
        if ($this->_packages == null) {
            $this->_packages = Mage::registry('packages');

            if (! $this->_packages) {
                $this->_packages = $this->preparePackages();

                // Ensure it is not already saved but empty
                Mage::unregister('packages');
                Mage::register('packages', $this->_packages);
            }
        }
        return $this->_packages;
    }

    /**
     * URL to the media directory
     *
     * @return string
     */
    public function getMediaUrl()
    {
        return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
    }

    /**
     * Format the quote packages to contain only specific information
     *
     * @return array|null
     */
    public function getCartPackages()
    {
        if ($this->_cartPackages == null) {
            $this->_cartPackages = Mage::registry('cart_packages');

            if (! $this->_cartPackages) {
                $this->_cartPackages = $this->getCheckoutHelper()->getCartPackages($this->getPackages());

                Mage::register('cart_packages', $this->_cartPackages);
            }
        }

        return $this->_cartPackages;
    }

    /**
     * Get current customer
     *
     * @return Omnius_Customer_Model_Session|null
     */
    public function getCustomer()
    {
        if (! isset($this->_customer)) {
            $this->_customer = Mage::registry('customer');

            if ($this->_customer !== false && $this->_customer === null) {
                // customer not found in registry or empty
                if ($this->getQuote()->getCustomer()->getId()) {
                    $this->_customer = $this->getQuote()->getCustomer();
                } elseif ($this->getCustomerSession()->getProspect()) {
                    $this->_customer = $this->getCustomerSession()->getCustomer();
                } else {
                    $this->_customer = false;
                }

                Mage::register('customer', $this->_customer);
            }
        }

        return $this->_customer;
    }

    /**
     * Verify if the quote has multiple packages
     *
     * @return bool
     */
    public function quoteHasMultiplePackages()
    {
        if (! isset($this->_quoteHasMultiplePackages)) {
            $this->_quoteHasMultiplePackages = Mage::registry('multiple_packages');

            if ($this->_quoteHasMultiplePackages === null) {
                $this->_quoteHasMultiplePackages = (count($this->getCartPackages()) > 1) ? true : false;

                Mage::register('multiple_packages', $this->_quoteHasMultiplePackages);
            }
        }

        return $this->_quoteHasMultiplePackages;
    }

    /**
     * @return bool|mixed
     * @throws Exception
     */
    public function skipToDeliverSteps()
    {
        if (! isset($this->_skipToDeliverSteps)) {
            $this->_skipToDeliverSteps = Mage::registry('skipToDeliverSteps');

            if ($this->_skipToDeliverSteps === null) {
                $superOrderNo = $this->getRequest()->get('orderId');
                $this->_skipToDeliverSteps = ($superOrderNo && ($superOrder = Mage::getModel('superorder/superorder')->getSuperOrderByOrderNumber($superOrderNo)) && ($superOrder->getId()));

                if ($this->_skipToDeliverSteps) {
                    Mage::register('order', $superOrder);
                }

                Mage::register('skipToDeliverSteps', $this->_skipToDeliverSteps);
            }
        }

        return $this->_skipToDeliverSteps;
    }

    /**
     * @return array|mixed
     */
    public function getAllCancelledPackages()
    {
        if (! isset($this->_allCancelledPackages)) {
            $this->_allCancelledPackages = Mage::registry('allCancelledPackages');

            if ($this->_allCancelledPackages === null) {
                $this->_allCancelledPackages = array_merge($this->getPackageHelper()->getCancelledPackagesNow(), $this->getPackageHelper()->getCancelledPackages());

                Mage::register('allCancelledPackages', $this->_allCancelledPackages);
            }
        }

        return $this->_allCancelledPackages;
    }

    /**
     * @return mixed
     */
    public function getIsOrderEdit()
    {
        return $this->getCustomerSession()->getOrderEdit();
    }

    /**
     * @return mixed
     */
    public function canAgentEdit()
    {
        $agent = Mage::getSingleton('customer/session')->getAgent(true);
        return $agent->isGranted(array('CHANGE_INPUT'));
    }

    /**
     * @return mixed|null
     */
    protected function getModifiedQuotes()
    {
        if (! isset($this->_changedQuotes)) {
            $this->_changedQuotes = Mage::registry('allChangedPackages');

            if ($this->_changedQuotes === null) {

                $this->_changedQuotes = Mage::helper('omnius_package')->getModifiedQuotes();
                Mage::register('allChangedPackages', $this->_changedQuotes);
            }
        }

        return $this->_changedQuotes;
    }

    /**
     * @return array|null
     */
    private function preparePackages()
    {
        $quote = $this->getQuote();
        $packagesResult = array();

        if (Mage::getSingleton('customer/session')->getOrderEdit()) {
            $editedQuotes = $this->getModifiedQuotes();
            foreach ($editedQuotes as $q) {
                foreach ($q->getPackages() as $packId => $pack) {
                    $packagesResult[$packId] = $pack;
                }
            }

            foreach ($quote->getPackages() as $packageId => $package) {
                if (!in_array($packageId, array_keys($packagesResult))) {
                    $packagesResult[$packageId] = $package;
                }
            }
        } else {
            $packagesResult = $quote->getPackages();
        }

        $this->_packages = $packagesResult;

        return $this->_packages;
    }

    /**
     * Check if the quote has a business or a personal contract
     * @return bool
     */
    public function checkShouldShowRambours() {
        if ($this->getCustomerSession()->getOrderEdit()) {
            $quote = Mage::getModel('sales/quote')->load($this->getCustomerSession()->getOrderEdit());
            // Order edit
            $packageDatas = array();
            $allPackages = $this->getPackages();

            foreach($allPackages as $packageId => $package) {
                $packageModel = Mage::getModel('package/package')->getPackages($quote->getSuperOrderEditId(), null, $packageId)->getFirstItem();
                $packageDatas[] = array($packageModel, $package['items']);
            }

            $editTotals = $this->getCheckoutHelper()->getEditTotals($packageDatas);
            $grandTotal = $editTotals['total_price'];
        } else {
            // Not order edit
            $quote = $this->getQuote();

            // Check if amount is within the specified margin
            $grandTotal = $quote->getGrandTotal();
        }

        if ($this->getCheckoutHelper()->checkIsBusinessCustomer($quote)) {
            $validateAmount = $grandTotal < Omnius_Checkout_Model_Sales_Quote_Item::BUSINESS_MAX_AMMOUNT;
        } else {
            $validateAmount = $grandTotal < Omnius_Checkout_Model_Sales_Quote_Item::CONSUMER_MAX_AMMOUNT;
        }

        return $validateAmount;
    }

    /**
     * Logic for which the reimbursement payment option can be displayed.
     * Extend this to add custom logic.
     * @return bool
     */
    public function checkShowProductRambours()
    {
        return true;
    }

    /**
     * Logic for which the number porting step is displayed.
     * Extend this to add custom logic.
     * @return bool
     */
    public function shouldShowNumberPorting()
    {
        return true;
    }

    /**
     * @return Omnius_Customer_Model_Session|null
     */
    public function existingCustomerIsLogged()
    {
        return $this->getCustomer();
    }

    /**
     * Customers that are assigned a user id in the services.
     * @return bool
     */
    public function existingCustomerValidated()
    {
        return $this->existingCustomerIsLogged();
    }
}