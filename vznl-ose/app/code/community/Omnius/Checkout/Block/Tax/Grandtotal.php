<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * @category    Dyna
 * @package     Omnius_Checkout
 */

/**
 * Subtotal Total Row Renderer
 */

class Omnius_Checkout_Block_Tax_Grandtotal extends Mage_Tax_Block_Checkout_Grandtotal
{
    /**
     * Get grandtotal exclude tax
     *
     * @return float
     */
    public function getMafTotalExclTax()
    {
        // this should not be a negative value, but if it is show it in front-end to check who sets it negative
        return $this->getMafTotal()->getAddress()->getInitialMixmatchMafTotal() != null
            ? $this->getMafTotal()->getAddress()->getInitialMixmatchMafTotal() - $this->getMafTotal()->getAddress()->getInitialMixmatchMafTax()
            : $this->getMafTotal()->getAddress()->getMafGrandTotal() - $this->getMafTotal()->getAddress()->getMafTaxAmount();
    }

    /**
     * Get maf grand total including taxes
     *
     * @return float
     */
    public function getMafGrandTotal()
    {
        // this should not be a negative value, but if it is show it in front-end to check who sets it negative
        return $this->getMafTotal()->getAddress()->getInitialMixmatchMafTotal() != null
            ? $this->getMafTotal()->getAddress()->getInitialMixmatchMafTotal()
            : $this->getMafTotal()->getAddress()->getMafGrandTotal();
    }
}
