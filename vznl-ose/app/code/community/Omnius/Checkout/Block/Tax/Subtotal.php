<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * @category    Dyna
 * @package     Omnius_Checkout
 */

/**
 * Subtotal Total Row Renderer
 */

class Omnius_Checkout_Block_Tax_Subtotal extends Mage_Tax_Block_Checkout_Subtotal
{
    /**
     * Get subtotal excluding tax
     *
     * @return float
     */
    public function getMafSubtotalExclTax()
    {
        // this should not be a negative value, but if it is show it in front-end to check who sets it negative
        return $this->getMafTotal()->getAddress()->getInitialMixmatchMafTotal() != null
            ? $this->getMafTotal()->getAddress()->getInitialMixmatchMafTotal() - $this->getMafTotal()->getAddress()->getInitialMixmatchMafTax()
            : $this->getMafTotal()->getAddress()->getMafGrandTotal() - $this->getMafTotal()->getAddress()->getMafTaxAmount();
    }

    /**
     * Get subtotal excluding tax
     *
     * @return float
     */
    public function getDeviceTotal()
    {
        $excl = $this->getTotal()->getAddress()->getInitialMixmatchSubtotal() != null ? $this->getTotal()->getAddress()->getInitialMixmatchSubtotal() :  $this->getTotal()->getAddress()->getGrandTotal() - $this->getTotal()->getAddress()->getTaxAmount();
        return $excl;

    }
}
