<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Checkout_Block_Adminhtml_Activationreason_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

    /**
     * Omnius_Checkout_Block_Adminhtml_Activationreason_Edit_Tabs constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('activation_reason');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('omnius_checkout')->__('Item Information'));
    }

    /**
     * @return Mage_Core_Block_Abstract
     * @throws Exception
     */
    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
            'label' => Mage::helper('omnius_checkout')->__('Item Information'),
            'title' => Mage::helper('omnius_checkout')->__('Item Information'),
            'content' => $this->getLayout()->createBlock('omnius_checkout/adminhtml_activationreason_edit_tab_form')->toHtml(),
        ));

        return parent::_beforeToHtml();
    }
}
