<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Checkout_Block_Adminhtml_Activationreason_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        /** @var Omnius_Checkout_Model_Reason $data */
        $data = Mage::registry('reason_data');
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $dataFieldset = $form->addFieldset('reason', array('legend' => Mage::helper('omnius_checkout')->__('Manual activation reason details')));

        $dataFieldset->addField('name', 'text', array(
            'label' => Mage::helper('omnius_checkout')->__('Reason'),
            'name' => 'name',
            "class" => "required-entry",
            "required" => true,
            'value' => $data->getName(),
        ));

        $dataFieldset->addField('requires_input', 'select', array(
            'label' => Mage::helper('omnius_checkout')->__('Requires input from agent'),
            'name' => 'requires_input',
            "class" => "required-entry",
            "required" => true,
            'value' => $data->getRequiresInput(),
            'values' => [
                0 => Mage::helper('omnius_checkout')->__('No'),
                1 => Mage::helper('omnius_checkout')->__('Yes'),
            ]
        ));

        return parent::_prepareForm();
    }
}
