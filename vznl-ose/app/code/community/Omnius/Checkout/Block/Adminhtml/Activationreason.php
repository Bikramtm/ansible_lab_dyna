<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Checkout_Block_Adminhtml_Activationreason extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * Omnius_Checkout_Block_Adminhtml_Activationreason constructor.
     */
    public function __construct()
    {
        $this->_controller = 'adminhtml_activationreason';
        $this->_blockGroup = 'omnius_checkout';
        $this->_headerText = Mage::helper('omnius_checkout')->__('Manage manual activation reasons');
        $this->_addButtonLabel = Mage::helper('omnius_checkout')->__('Add new manual activation reason');
        parent::__construct();
    }
}
