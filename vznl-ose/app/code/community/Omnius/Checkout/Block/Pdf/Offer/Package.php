<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Checkout_Block_Pdf_Offer_Package
 * @method Omnius_Checkout_Model_Sales_Quote getQuote()
 * @method Omnius_Package_Model_Package getPackage()
 */
class Omnius_Checkout_Block_Pdf_Offer_Package extends Mage_Page_Block_Html{

    /** @var Omnius_Customer_Model_Customer_Customer */
    private $customer = null;

    /**
     * Returns items belonging to the loaded package.
     * @return array
     */
    public function getItems()
    {
        $returnItems = array();
        foreach ($this->getQuote()->getAllItems() as $item) {
            if ($item->getPackageId() == $this->getPackage()->getPackageId()) {
                $index = rand(2, 100);
                $returnItems[$index] = $item;
            }
        }

        ksort($returnItems);
        return $returnItems;
    }

    /**
     *
     * @return Omnius_Customer_Model_Customer_Customer|Mage_Core_Model_Abstract
     */
    public function getCustomer()
    {
        if ($this->customer == null) {
            $this->customer = Mage::getModel('customer/customer')->load($this->getQuote()->getCustomerId());
        }

        return $this->customer;
    }

    /**
     * @return null
     */
    public function getCtn()
    {
        return $this->getCustomer()->getCustomerCtn($this->getPackage()->getCtn());
    }

    /**
     * Removes time from datetime
     *
     * @param DateTime|string $date
     * @return null|string
     */
    public function stripTimeFromDate($date)
    {
        if (!$date) {
            $date = null;
        } elseif (is_object($date)) {
            $date = $date->format('d/m/Y');
        } elseif (is_string($date)) {
            $date = new DateTime($date);

            $date = $date->format('d/m/Y');
        }

        return $date;
    }

    /**
     * @return null|string
     */
    public function getCtnEndDate()
    {
        return $this->stripTimeFromDate($this->getCtn()->getEndDate());
    }

    /**
     * @return null|string
     */
    public function getEffectiveDate()
    {
        return $this->stripTimeFromDate($this->getQuote()->getCreatedAt());
    }

    /**
     * @return int|string
     */
    public function getContractDuration()
    {
        $duration = '';
        /** @var Omnius_Checkout_Model_Sales_Order_Item $item */
        foreach ($this->getItems() as $item) {
            if (
                Mage::helper('omnius_catalog')->is(
                    array(Omnius_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION),
                    $item->getProduct()
                )
            ) {
                $duration = $item->getProduct()->getSubscriptionDuration();
            }
        }

        return $duration;
    }

    /**
     * @return bool
     */
    public function mafHasPromotion()
    {
        /** @var Omnius_Checkout_Model_Sales_Order_Item $item */
        foreach ($this->getItems() as $item) {
            if ($item->getMafDiscountAmount() > 0) {
                return true;
            }
        }
        return false;
    }
}
