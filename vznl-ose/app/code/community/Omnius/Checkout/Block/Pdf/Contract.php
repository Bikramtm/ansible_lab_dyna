<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Checkout_Block_Pdf_Contract extends Mage_Page_Block_Html //Mage_Checkout_Block_Onepage_Abstract
{
    const LIFETIME_PROMOTION = 100;
    /** @var  Omnius_Checkout_Model_Sales_Order */
    private $currentQuote;

    /**
     * @param $order
     * @return $this
     */
    public function init($order)
    {
        if (!($order instanceof Mage_Sales_Model_Order)) {
            $order = Mage::getModel('sales/order')->getFirstNonEditedOrder($order);
        }
        $this->currentQuote = $order;

        return $this;
    }
}
