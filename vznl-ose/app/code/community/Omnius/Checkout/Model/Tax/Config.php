<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Config
 * @package Tax
 */
class Omnius_Checkout_Model_Tax_Config extends Mage_Tax_Model_Config
{
    /**
     * Overridden to display prices including tax for business customers
     * If current customer is not business, we return the setting as it is
     *
     * Get product price display type
     *  1 - Excluding tax
     *  2 - Including tax
     *  3 - Both
     *
     * @param   mixed $store
     * @return  int
     */
    public function getPriceDisplayType($store = null)
    {
        if ($this->getCustomer() instanceof Mage_Customer_Model_Customer && $this->getCustomer()->getIsBusiness()) {
            return self::DISPLAY_TYPE_INCLUDING_TAX; //show prices including tax
        } else {
            return (int)$this->_getStoreConfig(self::CONFIG_XML_PATH_PRICE_DISPLAY_TYPE, $store);
        }
    }

    /**
     * @return Mage_Customer_Model_Customer
     */
    protected function getCustomer()
    {
        return Mage::getSingleton('customer/session')->getCustomer();
    }
} 