<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Checkout_Model_Reason extends Mage_Core_Model_Abstract
{
    const REASON_TAG = 'DYNA_REASON_CACHE';

    protected function _construct()
    {
        $this->_init("omnius_checkout/reason");
    }

    /**
     * @return Mage_Core_Model_Abstract
     */
    protected function _beforeSave()
    {
        if ($this->getData('created_at') === null) {
            $this->setData('created_at', now());
        } else {
            $this->setData('updated_at', now());
        }

        return parent::_beforeSave();
    }

    /**
     * @return Mage_Core_Model_Abstract
     */
    protected function _afterSave()
    {
        // Clear the agent cache when an agent is saved
        Mage::dispatchEvent('adminhtml_cache_flush_manual_reason_cache');
        Mage::app()->getCacheInstance()->getFrontend()->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array(self::REASON_TAG));
        return parent::_afterSave();
    }
}
