<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Order
 * @method string getCorrespondenceEmail()
 * @method Omnius_Customer_Model_Customer_Customer getCustomer()
 * @method int getSuperorderId()
 */
class Omnius_Checkout_Model_Sales_Order extends Mage_Sales_Model_Order
{
    const ESB_VALIDATION = 'telesale-to-retail-test';
    const DEFAULT_COUNTRY = 'NL';
    const INCREMENT_ID_PREFIX = 'DY1';
    const STATUS_CANCELLED = 'cancelled';
    const STATUS_NOT_DELIVERED = 'not_delivered';
    const STATUS_RDY4PICK = 'rdy4pick';
    const STATUS_PICKED = 'picked';
    const STATUS_RDY4SHIP = 'rdy4ship';
    const DA_MODE_MULTI_CTN = 'MultiCTN';
    const DA_MODE_SINGLE_CTN = 'SingleCTN';
    const REASON_CODE_APPOINTMENT = 'APPOINTMENT';
    public static $reasonCodeLabel = array(
        'APPOINTMENT' => 'Failed Appointment'
    );

    protected $_myCached = array();
    protected $statusTypes = array(
        Omnius_Superorder_Model_StatusHistory::ORDER_STATUS
    );

    /** @var Omnius_Package_Model_Package[] */
    protected $_processedPackages = [];

    /** @var Omnius_Package_Model_Package[] */
    protected $_cancelledPackages = [];

    /** @var Omnius_Package_Model_Package[] */
    protected $_untouchedPackages = [];

    /** @var  Omnius_Package_Helper_Data */
    protected $_packageHelper;

    /**
     * Override construct
     */
    public function _construct()
    {
        $this->_packageHelper = Mage::helper('omnius_package');
        parent::_construct();
    }

    /**
     * Trigger before save event to check if it's missing payment
     */
    protected function _beforeSave()
    {
        if (!$this->getPayment()) {
            Mage::log(
                'OrderId: ' . $this->getId() .
                ', current website: ' . Mage::app()->getWebsite()->getName() .
                ', url: ' . Mage::helper('core/url')->getCurrentUrl() .
                ', Agent: ' . Mage::helper('agent')->getAgentId() .
                ', Quoteid: ' . $this->getQuoteId() .
                ' Origin: ' . php_sapi_name() .
                ' Trace: ' . mageDebugBacktrace(true, false, true), null, 'Orders_no_payment.log', true
            );
        }
        if (!$this->getId()) {
            $this->setCreatedAt(now());
        }
        parent::_beforeSave();
    }

    /**
     * Save price info for all order items
     *
     * @return Mage_Sales_Model_Order
     */
    protected function _afterSave()
    {
        Mage::helper('superorder')->checkStatusesChanged($this->getOrigData(), $this->getData(), $this->statusTypes, $this);
        parent::_afterSave();

        // update the checksum of the package
        $this->processPackageChecksum();

        $oldQuote = Mage::getModel('sales/quote')->load($this->getQuoteId());
        $tempQuote = Mage::getSingleton('customer/session')->getOfferteData();
        if ($tempQuote && $oldQuote->getId() == $tempQuote->getId()) {
            foreach ($this->getAllItems() as $orderItem) {
                foreach ($tempQuote->getItemsCollection() as $item) {
                    if ($orderItem->getProductId() == $item->getProductId() && $orderItem->getPackageId() == $item->getPackageId()) {
                        $orderItem->setOrderItemPrices($item->getOrderItemPrices())->save();
                        break;
                    }
                }
            }
        }

        //Saving current product type, so we cand view this order in case this product is deleted
        foreach ($this->getAllItems() as $orderItem) {
            if ($orderItem->getProduct() && $orderItem->getProduct()->getType()) {
                $this->saveAppliesRules($orderItem);
            }
        }
    }

    /**
     * @param null $packageId
     * @return mixed
     */
    public function getPackages($packageId = null)
    {
        $collection = Mage::getResourceModel('package/package_collection')
            ->addFieldToFilter('order_id', $this->getSuperorderId());
        if ($packageId != null) {
            if (is_scalar($packageId)) {
                return $collection->addFieldToFilter('package_id', $packageId)->getFirstItem();
            } elseif (is_array($packageId)) {
                return $collection->addFieldToFilter('package_id', $packageId);
            }
        } else {
            return $collection->load();
        }
    }

    /**
     * @return mixed
     */
    protected function _getWebsiteCode()
    {
        $websiteId = Mage::getModel('core/store')->load($this->getStoreId())->getWebsiteId();

        return Mage::getModel('core/website')->load($websiteId)->getCode();
    }

    /**
     * @return bool
     */
    public function needsContractSigning()
    {
        $packages = $this->getPackageIds();
        $needsSigning = false; // Packages that need approval also have contract
        foreach ($packages as $packageId) {
            $package = $this->getPackages($packageId);
            if ($package->needsCreditCheck()) { // Seems to be same condition for credit check
                $needsSigning = true;
                break;
            }
        }
        return $needsSigning;
    }

    /**
     * @return bool
     */
    public function isStoreDelivery()
    {
        return ($this->getShippingAddress()->getDeliveryStoreId() > 0);
    }

    /**
     * @return bool
     */
    public function isHomeDelivery()
    {
        return ($this->getShippingAddress()->getDeliveryStoreId() == false);
    }

    /**
     * @return bool
     */
    public function isVirtualDelivery()
    {
        /** @var Omnius_Superorder_Model_Superorder $superOrder */
        $superOrder = Mage::getModel('superorder/superorder')->load($this->getSuperorderId());
        if ($superOrder->getParentId()) {
            $superOrder = Mage::getModel('superorder/superorder')->load($superOrder->getParentId());
            $oldOrderPackages = $superOrder->getPackages();
            $mapping = [];
            // Map the packages to the new ids
            foreach ($oldOrderPackages as $oldPackage) {
                $mapping[$oldPackage->getPackageId()] = $oldPackage->getNewPackageId();
            }
            // Compare changed items
            $parentOrderItems = $superOrder->getOrderedItems(true);
            $currentOrderItems = Mage::getResourceModel('sales/order_item_collection')
                ->setOrderFilter($this->getId());
            $currentHardwareItems = $this->getCurrentHardwareItems($currentOrderItems, $parentOrderItems, $mapping);

            // If there are still items return false;
            if (count(array_filter($currentHardwareItems))) {
                return false;
            }
        }

        return true;
    }

    /**
     * @return array
     */
    public function getPackageIds()
    {
        $packageIds = array();
        /** @var Mage_Sales_Model_Order_Item $item */
        foreach ($this->getAllVisibleItems() as $item) {
            if (in_array($item->getPackageId(), $packageIds)) {
                continue;
            }
            array_push($packageIds, $item->getPackageId());
        }
        return array_unique($packageIds);
    }

    /**
     * @return mixed
     */
    public function getDeliveryOrderPackages()
    {
        return $this->getPackages(array('in' => $this->getPackageIds()));
    }

    /**
     * @param bool $customFormat
     * @return mixed
     */
    public function getContractantIdNumber($customFormat = false)
    {
        if (!$customFormat) {
            return parent::getContractantIdNumber();
        }

        return Mage::helper('omnius_validators')->stripNLD(parent::getContractantIdNumber());
    }

    /**
     * @param bool $customFormat
     * @return mixed
     */
    public function getCustomerIdNumber($customFormat = false)
    {
        if (!$customFormat) {
            return parent::getCustomerIdNumber();
        }

        return Mage::helper('omnius_validators')->stripNLD(parent::getCustomerIdNumber());
    }

    /**
     * @param bool $sorted
     * @return array
     */
    public function getAllItems($sorted = false)
    {
        $key = __METHOD__ . ' ' . $this->getId() . ' ' . (int) $sorted;
        if (Mage::registry('freeze_models') === true && isset($this->_myCached[$key])) {
            return $this->_myCached[$key];
        }

        $orderItems = parent::getAllItems();
        if ($sorted === false) {
            $this->_myCached[$key] = $orderItems;

            return $orderItems;
        }

        $packagesAll = array();
        $result = array();

        foreach ($orderItems as $item) {
            if ($item->isPromo()) {
                $packagesAll[$item->getPackageId()][Omnius_Catalog_Model_Type::SUBTYPE_PROMOTION][$item->getId()] = $item;
            } else {
                $packagesAll[$item->getPackageId()][current($item->getProduct()->getType())][$item->getId()] = $item;
            }
        }

        foreach ($packagesAll as $tempPackage) {
            foreach($tempPackage as $type => $items) {
                $result += $tempPackage[$type];
            }
        }

        unset($packagesAll);
        $this->_myCached[$key] = $result;

        return $result;
    }

    /**
     * @throws Exception
     */
    public function processPackageChecksum()
    {
        // If the order is not yet assigned to an superorder, don't create/update packages
        if (!$this->getSuperorderId() || $this->getEdited()) {
            return;
        }
        $items = $this->getAllVisibleItems();
        $packageIds = array();
        $checksums = array();
        $devices = array();
        $sims = array();
        /** @var Omnius_Checkout_Model_Sales_Quote_Item $item */
        foreach ($items as &$item) {
            $packageIds[] = $item->getPackageId();
            if (!isset($checksums[$item->getPackageId()])) {
                $checksums[$item->getPackageId()] = array();
            }

            $checksums[$item->getPackageId()][] = $item->getProductId();

            if ($item->getProduct()->is(Omnius_Catalog_Model_Type::SUBTYPE_DEVICE)) {
                $devices[$item->getPackageId()] = $item->getProductId();
            }

            if ($item->getProduct()->is(Omnius_Catalog_Model_Type::SUBTYPE_SIMCARD)) {
                $sims[$item->getPackageId()] = $item->getProductId();
            }
        }
        unset($item);

        $packageIds = array_unique($packageIds);

        /** @var Omnius_Package_Model_Mysql4_Package_Collection $packages */
        $packages = Mage::getSingleton('package/package')->getPackages($this->getSuperorderId(), $this->getQuoteId());
        foreach ($packageIds as $packageId) {
            /** @var Omnius_Package_Model_Package $packageModel */

            $packageModelOrder = $packageModelQuote = null;
            foreach($packages->getItems() as $item) {
                if ($item->getOrderId() == $this->getSuperorderId() && $item->getPackageId() == $packageId) {
                    $packageModelOrder = $item;
                    break;
                }
            }

            foreach ($packages->getItems() as $item) {
                if ($item->getQuoteId() == $this->getQuoteId() && $item->getPackageId() == $packageId) {
                    $packageModelQuote = $item;
                    break;
                }
            }

            $packageModel = $this->getPackageModel($packageModelOrder, $packageModelQuote, $packageId);

            // Remove the IMEI if the device was changed
            if ($packageModel->getChecksum() && isset($devices[$packageId]) && !in_array($devices[$packageId], explode(',', $packageModel->getChecksum()))) {
                $packageModel->setImei(null);
            }

            // Remove the simcard if the simcard type was changed
            if ($packageModel->getChecksum() && isset($sims[$packageId]) && !in_array($sims[$packageId], explode(',', $packageModel->getChecksum()))) {
                $packageModel->setSimNumber(null);
            }

            // Remove or add the old_sim flag if a sim is selected and package is retention
            if ($packageModel->isRetention()) {
                if (isset($sims[$packageId])) {
                    $packageModel->setOldSim(false);
                } else {
                    $packageModel->setOldSim(true);
                }
            }

            $checksums[$packageId][] = $this->getId();
            $coupon = $packageModel->getCoupon();
            if (!empty($coupon)) {
                // Include coupons because they also alter the price
                $checksums[$packageId][] = $coupon;
            }

            $products = $checksums[$packageId];
            sort($products);
            $packageModel->setChecksum(join(',', $products));
            $packageModel->save();
        }
    }

    /**
     * @param $superorderId
     * @param $onlyFinal
     * @return mixed
     */
    public function getNonEditedOrderItems($superorderId, $onlyFinal = true, $returnItems = true)
    {
        if (!$superorderId) {
            return null;
        }

        $collection = $this->getCollection()
            ->addFieldToSelect('*')
            ->addFieldToFilter('superorder_id', $superorderId);
        if ($onlyFinal) {
            $collection->addFieldToFilter('edited', array('neq' => 1));
        }

        $val = $returnItems ? $collection->getItems() : $collection;

        return $val;
    }

    /**
     * @param $incrementId
     * @return mixed
     * @internal param $customIncrementId
     */
    public function getFirstNonEditedOrder($incrementId)
    {
        return $this->getCollection()
            ->addFieldToFilter('increment_id', $incrementId)
            ->addFieldToFilter('edited', array('neq' => 1))
            ->getFirstItem();
    }

    /**
     * @param null $packageId
     * @return null
     */
    public function getDeliveredParentId($packageId = null)
    {
        if (!$packageId) {
            $items = $this->getAllItems();
            $item = reset($items);
            $packageId = $item->getPackageId();
        }

        $superOrder = Mage::getModel('superorder/superorder')->load($this->getSuperorderId());
        $packageObj = $this->getPackages($packageId);
        if ($packageObj->getOldPackageId()) {
            $packageId = $packageObj->getOldPackageId();
        }
        if ($superOrder->getParentId()) {
            $orders = $this->getCollection()
                ->addAttributeToFilter('superorder_id', $superOrder->getParentId())
                ->addAttributeToFilter('edited', 0);

            foreach ($orders as $order) {
                foreach ($order->getAllVisibleItems() as $item) {
                    if ($item->getPackageId() == $packageId) {
                        return $order->getId();
                    }
                }
            }
        }

        return null;
    }

    /**
     * @return array
     */
    public function getTotals()
    {
        // Initialise array to avoid missing key warnings
        $totals = [
            'subtotal_price' => 0,
            'subtotal_maf' => 0,
            'tax_price' => 0,
            'total_maf' => 0,
            'tax_maf' => 0,
            'total' => 0,
        ];

        foreach ($this->getPackages() as $package) {
            $itemCollection = array();
            foreach ($this->getAllItems() as $item) {
                if ($item->getPackageId() == $package->getPackageId()) {
                    $itemCollection[] = $item;
                }
            }
            $packageTotals = $package->getPackageTotals($itemCollection);
            $totals['subtotal_price'] += isset($packageTotals['subtotal_price']) ? $packageTotals['subtotal_price'] : 0;
            $totals['subtotal_maf'] += isset($packageTotals['subtotal_maf']) ? $packageTotals['subtotal_maf'] : 0;
            $totals['tax_price'] += isset($packageTotals['tax_price']) ? $packageTotals['tax_price'] : 0;
            $totals['total_maf'] += isset($packageTotals['total_maf']) ? $packageTotals['total_maf'] : 0;
            $totals['tax_maf'] += isset($packageTotals['tax_maf']) ? $packageTotals['tax_maf'] : 0;
            $totals['total'] += isset($packageTotals['total']) ? $packageTotals['total'] : 0;
        }

        return $totals;
    }

    /**
     * Check if the delivery order is paid
     * @return boolean
     */
    public function isPaid()
    {
        foreach ($this->getPackages() as $package) {
            if (!$package->isPayed()) {
                return false;
            }
        }

        return true;
    }

    /**
     * Add the items of multiple orders to an order for contract purposes
     *
     * @param Mage_Sales_Model_Order $order
     */
    public function merge(Mage_Sales_Model_Order $order)
    {
        foreach ($order->getAllVisibleItems() as $item) {
            $newItem = clone $item;
            $newItem->setId(null);
            $this->addItem($newItem);
        }
        $this->setIncrementId($this->getIncrementId() . ', ' . $order->getIncrementId());
    }

    /**
     * @return bool
     */
    public function hasChildren()
    {
        return $this->getChildren()->count() > 0;
    }

    /**
     * @return bool
     */
    public function hasGrandChildren()
    {
        return $this->getChildren()->count() > 1;
    }

    /**
     * @return bool
     */
    public function hasParents()
    {
        return $this->getParents()->count() > 0;
    }

    /**
     * @return bool
     */
    public function hasGrandParents()
    {
        return $this->getParents()->count() > 1;
    }

    /**
     * @return Varien_Data_Collection
     * @throws Exception
     */
    public function getChildren()
    {
        $children = new Varien_Data_Collection();
        $sql = 'SELECT * FROM ' . $this->getResource()->getMainTable() . ' WHERE parent_id=:id;';
        $binds = array(
            ':id' => $this->getId()
        );
        $connection = $this->getResource()->getReadConnection();
        $resourceName = $this->getResourceName();
        $this->_findChildren($children, $connection, $sql, $binds, $resourceName);

        return $children;
    }

    /**
     * @param $eId
     * @param $children
     * @param $connection
     * @param $sql
     * @param $resourceName
     */
    protected function _findChildren($children, $connection, $sql, $binds, $resourceName)
    {
        $childs = $connection->fetchAll($sql, $binds);
        foreach ($childs as &$child) {
            $children->addItem(Mage::getModel($resourceName)->addData($child));

            $this->_findChildren($children, $connection, $sql,
                array(
                    ':id' => $child['entity_id']
                ), $resourceName);
        }
    }

    /**
     * @return Varien_Data_Collection
     * @throws Exception
     */
    public function getParents()
    {
        $parents = new Varien_Data_Collection();
        if (!$this->getParentId()) {
            return $parents;
        }
        $_parents = array();
        $sql = 'SELECT * FROM ' . $this->getResource()->getMainTable() . ' WHERE entity_id=:id;';
        $binds = array(
            ':id' => $this->getParentId()
        );
        $connection = $this->getResource()->getReadConnection();
        $resourceName = $this->getResourceName();

        while ($parent = $connection->fetchRow($sql, $binds)) {
            $_parents[] = Mage::getModel($resourceName)->addData($parent);
            if (!isset($parent['parent_id'])) {
                unset($parent);
                break;
            }

            $binds = array(
              ':id' => $parent['parent_id']
            );

            unset($parent);
        }
        usort($_parents, function ($a, $b) {
            return $a->getId() > $b->getId();
        });
        foreach ($_parents as $p) {
            $parents->addItem($p);
        }
        return $parents;
    }

    /**
     * Set edited order with raw queries.
     * @param $flag
     * @return $this
     */
    public function setEditedOrder($flag)
    {
        $this->setEdited($flag);
        $this->runRawQuery('edited', $flag);
        return $this;
    }

    /**
     * Set superorder id with raw queries
     * @param $superorderId
     * @return $this
     */
    public function setSuperorderQuery($superorderId)
    {
        $this->setSuperorderId($superorderId);
        $this->runRawQuery('superorder_id', $superorderId);
        return $this;
    }

    /**
     * @param $field
     * @param $data
     */
    protected function runRawQuery($field, $data)
    {
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        $query = "UPDATE sales_flat_order SET $field='{$data}' WHERE entity_id = {$this->getId()}";
        $writeConnection->query($query);
    }

    /**
     * Set the dealer adapter mode from raw query
     * @param $param
     * @return $this
     */
    public function oneOff($param)
    {
        $this->runRawQuery('dealer_adapter_mode', $param ? self::DA_MODE_MULTI_CTN : self::DA_MODE_SINGLE_CTN);
        return $this;
    }

    /**
     * @return bool
     */
    public function doesNotContainsHardware()
    {
        foreach ($this->getAllItems() as $item) {
            if ($item->getProduct()->isOfHardwareType()) {
                return false;
            }
        }

        return true;
    }

    /**
     * @return $this
     */
    public function clearFields()
    {
        /** @var Mage_Core_Model_Config_Element $fields */
        $fields = Mage::getConfig()->getFieldset('delivered_order_ignored_fields', 'global');
        foreach ($fields as $field => $data) {
            $this->setData($field, null);
        }

        return $this;
    }

    /**
     * @param $imei
     * @param $packageModel
     * @param $telNumber
     * @param $simNumber
     */
    protected function updatePackageData($imei, $packageModel, $telNumber, $simNumber)
    {
        $packageId = $packageModel->getPackageId();
        $so = Mage::getModel('superorder/superorder')->load($this->getSuperorderId());
        if ($so->getParentId()) {
            $oldPackage = Mage::getSingleton('package/package')->getOrderPackages($so->getParentId(), $packageId);

            if (empty($imei) && $oldPackage->getImei()) {
                $packageModel->setImei($oldPackage->getImei());
            }
            if (empty($telNumber) && $oldPackage->getTelNumber()) {
                $packageModel->setTelNumber($oldPackage->getTelNumber());
            }
            if (empty($simNumber) && $oldPackage->getSimNumber()) {
                $packageModel->setSimNumber($oldPackage->getSimNumber());
            }
        }
    }

    /**
     * @param $packageModelOrder
     * @param $packageModelQuote
     * @param $packageId
     * @return mixed
     */
    protected function getPackageModel($packageModelOrder, $packageModelQuote, $packageId)
    {
        if (isset($packageModelOrder) && isset($packageModelQuote)) {
            $packageModelOrder->setCoupon($packageModelQuote->getCoupon())->save();
            $packageModel = $packageModelOrder;
        } elseif (!isset($packageModelOrder) && isset($packageModelQuote)) {
            $packageModel = $packageModelQuote;
            $imei = trim($packageModel->getImei());
            $telNumber = trim($packageModel->getTelNumber());
            $simNumber = trim($packageModel->getSimNumber());

            if (empty($imei) || empty($telNumber) || empty($simNumber)) {
                $this->updatePackageData($imei, $packageModel, $telNumber, $simNumber);
            }

            $packageModel->setOrderId($this->getSuperorderId())
                ->setQuoteId(null);

            if ($packageModel->getStatus() != Omnius_Package_Model_Package::ESB_PACKAGE_STATUS_CANCELLED) {
                $packageModel->setStatus(Omnius_Package_Model_Package::ESB_PACKAGE_STATUS_PACKAGE_VALIDATED)// mark package as validated in edit after delivery
                ->setCompleteReturn(null)
                    ->setNotDamagedItems(null)
                    ->setReturnNotes(null)
                    ->setReturnedBy(null)
                    ->setReturnDate(null)
                    ->setHardwareChange(null)
                    ->setRefundMethod(null)
                    ->setApproveOrderJobId(null);
            }
        } elseif (isset($packageModelOrder) && !isset($packageModelQuote)) {
            $packageModel = $packageModelOrder;
        } elseif (!isset($packageModelQuote) && !isset($packageModelOrder)) {
            $packageModel = Mage::getModel('package/package')
                ->setPackageId($packageId)
                ->setStatus(Omnius_Package_Model_Package::ESB_PACKAGE_STATUS_INITIAL)
                ->setOrderId($this->getSuperorderId())
                ->setQuoteId(null);
        }

        return $packageModel;
    }

    /** @var Omnius_Checkout_Model_Sales_Quote[] */
    protected $_buildQuotes = [];

    /** @var Omnius_Checkout_Model_Sales_Order[] */
    protected $_childOrders = [];

    /**
     * @return $this
     */
    public function build()
    {
        return $this;
    }

    /**
     * @return Omnius_Checkout_Model_Sales_Order[]
     */
    public function getChildOrders()
    {
        return $this->_childOrders;
    }

    /**
     * @param Omnius_Package_Model_Package[] $processedPackages
     * @return $this
     */
    public function setProcessedPackages($processedPackages)
    {
        $this->_processedPackages = $processedPackages;
        return $this;
    }

    /**
     * @param Omnius_Package_Model_Package $package
     * @return $this
     */
    public function addProcessedPackage($package)
    {
        switch ($package->getBinaryState()) {
            case Omnius_Package_Helper_Builder::CANCELLED_PACKAGE:
                $this->_cancelledPackages[] = $package;
                break;
            case Omnius_Package_Helper_Builder::UNTOUCHED_PACKAGE:
                $this->_untouchedPackages[] = $package;
                break;
            default:
                $this->_processedPackages[] = $package;
                break;
        }

        return $this;
    }

    /**
     * @return int
     */
    protected function getEditSuperorderId()
    {
        return $this->getNewSuperorder() ? $this->getNewSuperorder()->getId() : $this->getSuperorderId();
    }

    /**
     * @return $this
     * @throws Exception
     */
    public function processCancelled()
    {
        /** @var Omnius_Superorder_Model_Superorder $so */
        $so = $this->getSuperorder();
        $customerSession = Mage::getSingleton('customer/session');
        $checkoutSession = Mage::getSingleton('checkout/session');
        foreach ($this->_cancelledPackages as $cancelled) {
            /** @var Omnius_Package_Model_Package $package */
            $package = Mage::getModel('package/package')->getCollection()
                ->addFieldToFilter('package_id', $cancelled->getPackageId())
                ->addFieldToFilter('order_id', $so->getId())
                ->getFirstItem();

            if (($cancelledDoaItems = $checkoutSession->getCancelDoaItems()) && isset($cancelledDoaItems[$package->getPackageId()])) {
                $sessionDoaItems = unserialize($cancelledDoaItems[$package->getPackageId()]);
                $this->updateDoaItems($package, $sessionDoaItems);
            }

            $package
                ->setData('complete_return', !$package->isDelivered())
                ->setData('hardware_change', true);

            $refundMethod = Mage::app()->getRequest()->getPost('refund_method');
            $refundReason = $checkoutSession->getCancelRefundReason();
            if ($package->isDelivered()) {
                $package->setRefundMethod($refundMethod)
                    ->setRefundReason($refundReason[$package->getPackageId()])
                    ->setReturnedBy($customerSession->getAgent(true)->getUsername())
                    ->setCancellationDate(date('d-m-Y'));
            }
            $package->save();
            $checkoutSession->setData('superorder_id', $so->getId());
        }

        return $this;
    }

    /**
     * @param Omnius_Checkout_Model_Sales_Quote[] $quotes
     * @param int $superOrderId
     * @param bool $movePackagesToSuperorder
     * @return Omnius_Checkout_Model_Sales_Order[]
     * @throws Exception
     */
    protected function buildOrders($quotes, $superOrderId, $movePackagesToSuperorder = false)
    {
        $finalOrders = [];
        $oldPackages = $this->getPackages();
        /** @var Omnius_Checkout_Model_Sales_Quote $quote */
        foreach ($quotes as $quote) {
            $newOrder = $this->generateOrder($quote);
            $newOrder->setSuperorderId($superOrderId);
            $newOrder->setEdited(0);
            if ($movePackagesToSuperorder && count($quote->getNewPackages())) {
                // Reorder to pay amount that is calculated on frontend
                $toPayAmount = Mage::getSingleton('customer/session')->getToPayAmount();
                $this->setToPayAmount($quote, $oldPackages, $toPayAmount);
            } else {
                $newOrder->setParentId($this->getId());
            }
            $newOrder->save();
            $finalOrders[] = $newOrder;
        }
        return $finalOrders;
    }

    /**
     * @param Omnius_Checkout_Model_Sales_Quote $quote
     * @return Omnius_Checkout_Model_Sales_Order
     */
    protected function generateOrder($quote)
    {
        /** @var Omnius_Checkout_Helper_Sales $salesConverter */
        $salesConverter = Mage::helper('omnius_checkout/sales');
        $quote
            ->collectTotals()
            ->save();

        /** @var Omnius_Checkout_Model_Sales_Order $newOrder */
        $newOrder = Mage::getModel('sales/order');
        $newOrder->setData($this->getData());
        $newOrder->unsetData('parent_id');
        $newOrder->unsetData('entity_id');
        $newOrder->unsetData('increment_id');
        $newOrder->unsetData('contract_sign_date');

        $newOrder = $salesConverter->quoteToOrder($quote, $newOrder);
        $newAddress = $quote->getCustomShipping();

        $address = Mage::getModel('sales/order_address');
        $address->setData($quote->getShippingAddress()->getData());
        $address->unsetData('entity_id');
        $address->unsetData('parent_id');
        $address->setStreet($address->getStreet());

        $address
            ->setData('delivery_type', $newAddress['delivery_type'])
            ->setData('payment_type', $newAddress['payment_type'])
            ->setData('delivery_store_id', $newAddress['delivery_store_id']);

        $newOrder->setShippingAddress($address);
        //Set order payment
        $orderPayment = Mage::getModel('sales/order_payment')
            ->setStoreId(Mage::app()->getStore()->getId())
            ->setCustomerPaymentId(0)
            ->setMethod($quote->getPaymentMethod());

        $newOrder->setPayment($orderPayment);

        return $newOrder;
    }

    /**
     * @param Omnius_Checkout_Model_Sales_Quote $quote
     * @param Omnius_Package_Model_Mysql4_Package_Collection $oldPackages
     * @param $toPayAmount
     */
    protected function setToPayAmount($quote, $oldPackages, $toPayAmount)
    {
        $reorderToPayAmount = [];
        /** @var Omnius_Package_Model_Package $package */
        foreach ($quote->getNewPackages() as $package) {
            //Update parent superorder packages with the new package ids from the new superorder.
            $oldPackage = $oldPackages->getItemByColumnValue('package_id', $package->getOldPackageId());

            $oldPackage->setNewPackageId($package->getPackageId())->save();
            //Set Amount to be paid for the new superorder.
            if ($toPayAmount && is_array($toPayAmount)) {
                foreach ($toPayAmount as $amount) {
                    $reorderToPayAmount[$package->getPackageId()] = $amount;
                }
            }
        }

        Mage::getSingleton('customer/session')->setToPayAmount($reorderToPayAmount);
    }

    /**
     * @param $package
     * @param $sessionDoaItems
     */
    protected function updateDoaItems($package, $sessionDoaItems)
    {
        foreach ($this->getAllItems() as $item) {
            if ($item->getPackageId() == $package->getPackageId() && in_array($item->getSku(), $sessionDoaItems)) {
                // Save DOA flag to order item
                $item->setItemDoa(true)
                    ->save();
            }
        }
    }

    /**
     * @param $currentOrderItems
     * @param $parentOrderItems
     * @param $mapping
     * @return array
     */
    protected function getCurrentHardwareItems($currentOrderItems, $parentOrderItems, $mapping)
    {
        $currentHardwareItems = [];
        // Find all the hardware products from the current magento order
        foreach ($currentOrderItems as $item) {
            if ($item->getProduct()->isOfHardwareType()) {
                $currentHardwareItems[$item->getPackageId()][$item->getProductId()] = ['product_id' => $item->getProductId(), 'doa' => $item->getItemDoa()];
            }
        }

        // Remove the hardware products that are in the current order (unchanged)
        foreach ($parentOrderItems as $items) {
            foreach ($items as $item) {
                if ($item->getProduct()->isOfHardwareType()) {
                    $packageId = $mapping[$item->getPackageId()];
                    if (isset($currentHardwareItems[$packageId][$item->getProductId()]) && $currentHardwareItems[$packageId][$item->getProductId()]['product_id'] == $item->getProductId() && !$currentHardwareItems[$packageId][$item->getProductId()]['doa']) {
                        unset($currentHardwareItems[$packageId][$item->getProductId()]);
                    }
                }
            }
        }

        return $currentHardwareItems;
    }

    /**
     * @param Omnius_Package_Model_Package $package
     * @return bool
     */
    protected function packageContainsHardware($package)
    {
        $productIds = array_map(function ($item) {
            return $item->getProductId();
        }, $package->getPackageItems());
        $productCollection = Mage::getModel('catalog/product')->getCollection()
            ->addFieldToFilter('entity_id', $productIds);

        foreach ($package->getPackageItems() as $item) {
            $product = $productCollection->getItemByColumnValue('entity_id', $item->getProductId());
            if ($product && $product->isOfHardwareType()) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param $orderItem
     */
    protected function saveAppliesRules($orderItem)
    {
        $packType = $orderItem->getProduct()->getType();
        if (!empty($packType)) {
            $productType = current($packType);
            if ($productType) {
                $orderItem->setPackProductType($productType);
            }
        }
        //Applied rules for this order Item
        $appliedRulesNames = "";
        if ($orderItem->getAppliedRuleIds()) {
            $appliedRulesIds = explode(",", $orderItem->getAppliedRuleIds());
            array_walk($appliedRulesIds, 'trim');
            if (!empty($appliedRulesIds)) {
                foreach ($appliedRulesIds as $appliedRule) {
                    $newAppliedRuleCollection = Mage::getModel('salesrule/rule')->getCollection()->addFieldToFilter('rule_id', $appliedRule)->load();
                    foreach ($newAppliedRuleCollection as $newAppliedRule) {
                        $ruleName = $newAppliedRule->getName();
                        $appliedRulesNames = $appliedRulesNames ? $appliedRulesNames . ", " . $ruleName : $ruleName;
                    }
                }
            }
        }
        if ($appliedRulesNames) {
            $orderItem->setAppliedRuleNames($appliedRulesNames);
        }
        $orderItem->save();
    }
}
