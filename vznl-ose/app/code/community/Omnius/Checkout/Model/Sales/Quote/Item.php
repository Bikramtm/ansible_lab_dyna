<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Sales Quote Item Model
 *
 * @category    Dyna
 * @package     Omnius_Checkout
 * @method      float|null getMaf()
 * @method      float|null getMafInclTax()
 * @method      float|null getMafDiscountAmount()
 * @method      float|null getMafRowTotal()
 * @method      float|null getMafRowTotalInclTax()
 * @method      float|null getHiddenMafTaxAmount()
 * @method      int getPrijsPromoDuurMaanden()
 */
class Omnius_Checkout_Model_Sales_Quote_Item extends Mage_Sales_Model_Quote_Item
{
    const ACQUISITION = 'acquisition';
    const INLIFE = 'inlife';
    const RETENTION = 'retention';
    const HARDWARE = 'hardware';
    const BUSINESS_MAX_AMMOUNT = 2500;
    const CONSUMER_MAX_AMMOUNT = 2500;
    public static $_itemData = array(
        'applied_rule_ids',
        'price',
        'base_price',
        'custom_price',
        'discount_percent',
        'discount_amount',
        'base_discount_amount',
        'tax_percent',
        'tax_amount',
        'base_tax_amount',
        'row_total',
        'base_row_total',
        'row_total_with_discount',
        'base_tax_before_discount',
        'tax_before_discount',
        'original_custom_price',
        'base_cost',
        'price_incl_tax',
        'base_price_incl_tax',
        'row_total_incl_tax',
        'base_row_total_incl_tax',
        'hidden_tax_amount',
        'base_hidden_tax_amount',
        'weee_tax_disposition',
        'weee_tax_row_disposition',
        'base_weee_tax_disposition',
        'base_weee_tax_row_disposition',
        'weee_tax_applied',
        'weee_tax_applied_amount',
        'weee_tax_applied_row_amount',
        'base_weee_tax_applied_amount',
        'base_weee_tax_applied_row_amnt',
        'maf',
        'base_maf',
        'maf_incl_tax',
        'base_maf_incl_tax',
        'maf_tax_amount',
        'base_maf_tax_amount',
        'maf_row_total',
        'base_maf_row_total',
        'maf_row_total_incl_tax',
        'base_maf_row_total_incl_tax',
        'maf_discount_amount',
        'base_maf_discount_amount',
        'maf_row_total_with_discount',
        'maf_tax_before_discount',
        'base_maf_tax_before_discount',
        'hidden_maf_tax_amount',
        'base_hidden_maf_tax_amount',
        'order_item_prices',
        'applied_promo_amount',
        'base_applied_promo_amount',
        'applied_maf_promo_amount',
        'base_applied_maf_promo_amount',
        'connection_cost',
        'mixmatch_subtotal',
        'mixmatch_tax'
    );

    /** @var Dyna_Cache_Model_Cache */
    protected $_cache = null;

    /**
     * @return Omnius_Checkout_Model_Sales_Quote
     */
    public function getQuote()
    {
        if (!$this->_quote) {
            $this->_quote = Mage::getModel('sales/quote')->load($this->getQuoteId());
        }

        return parent::getQuote();
    }

    /**
     * Calculate item maf row total price
     *
     * @return Omnius_Checkout_Model_Sales_Quote_Item
     */
    public function calcMafRowTotal()
    {
        $qty = $this->getTotalQty();
        // Round unit price before multiplying to prevent losing 1 cent on subtotal
        $total = $this->getStore()->roundPrice($this->getCalculationMafOriginal()) * $qty;
        $baseTotal = $this->getStore()->roundPrice($this->getBaseCalculationMafOriginal()) * $qty;

        $this->setMafRowTotal($this->getStore()->roundPrice($total));
        $this->setBaseMafRowTotal($this->getStore()->roundPrice($baseTotal));

        return $this;
    }

    /**
     * @return float
     */
    public function getFinalPrice()
    {

        $priceWithTax = 0;
        if ($this->getRowTotalInclTax()) {
            $priceWithTax = $this->getRowTotalInclTax();
        }

        $discount = 0;
        if ($this->getDiscountAmount()) {
            $discount = $this->getDiscountAmount();
        }

        $weeeTaxApplied = unserialize($this->getWeeeTaxApplied());
        $taxes = 0;
        if (!empty($weeeTaxApplied)) {
            foreach ($weeeTaxApplied as $taxApplied) {
                $taxes += $taxApplied['base_amount'];
            }
        }

        return (float) $priceWithTax - $discount + $taxes;
    }

    /**
     * Get item price used for quote calculation process.
     * This method get custom price (if it is defined) or original product final price
     *
     * @return float
     */
    public function getCalculationMaf()
    {
        $maf = $this->_getData('calculation_maf');
        if (is_null($maf)) {
            if ($this->hasCustomMaf()) {
                $maf = $this->getCustomMaf();
            } else {
                $maf = $this->getConvertedMaf();
            }

            $this->setData('calculation_maf', $maf);
        }

        return $maf;
    }

    /**
     * Get item price converted to quote currency
     * @return float
     */
    public function getConvertedMaf()
    {
        $price = $this->_getData('converted_maf');
        if (is_null($price)) {
            $price = $this->getStore()->convertPrice($this->getMaf());
            $this->setData('converted_maf', $price);
        }

        return $price;
    }

    /**
     * Get item price used for quote calculation process.
     * This method get original custom price applied before tax calculation
     *
     * @return float
     */
    public function getCalculationMafOriginal()
    {
        $maf = $this->_getData('calculation_maf');
        if (is_null($maf)) {
            if ($this->hasOriginalCustomMaf()) {
                $maf = $this->getOriginalCustomMaf();
            } else {
                $maf = $this->getConvertedMaf();
            }

            $this->setData('calculation_maf', $maf);
        }

        return $maf;
    }

    /**
     * Get calculation price used for quote calculation in base currency.
     *
     * @return float
     */
    public function getBaseCalculationMaf()
    {
        if (!$this->hasBaseCalculationMaf()) {
            $price = $this->getMaf();
            $this->setBaseCalculationMaf($price);
        }

        return $this->_getData('base_calculation_maf');
    }

    /**
     * Get original calculation price used for quote calculation in base currency.
     *
     * @return float
     */
    public function getBaseCalculationMafOriginal()
    {
        if (!$this->hasBaseCalculationMaf()) {
            $price = $this->getMaf();
            $this->setBaseCalculationMaf($price);
        }

        return $this->_getData('base_calculation_maf');
    }

    /**
     * Specify item price (base calculation price and converted price will be refreshed too)
     *
     * @param   float $value
     * @return  Mage_Sales_Model_Quote_Item_Abstract
     */
    public function setMaf($value)
    {
        $this->setBaseCalculationMaf(null);
        $this->setConvertedMaf(null);

        return $this->setData('maf', $value);
    }

    /**
     * @param Mage_Sales_Model_Quote_Item $item
     * @return bool
     *
     * Overwritten compare function as by default Magento compares two items and if they are the same(as in same product) it removes
     * one and increases the qty for the other. Since we can have two packages containing same product, compare should return false to avoid
     * removing the items
     */
    public function compare($item)
    {
        return false;
    }

    /**
     * @return array
     */
    public function calculatePricesForDiscount()
    {
        $totals = array();
        $quote = $this->getQuote();
        $totals['maf_after_discount'] = $this->getQty()
            * $quote->getStore()->roundPrice(
                $this->getItemFinalMafInclTax()
            );
        $totals['maf_after_discount_excl_tax'] = $this->getQty()
            * $quote->getStore()->roundPrice(
                $this->getItemFinalMafExclTax()
            );
        $totals['price_after_discount'] = $this->getQty()
            * $quote->getStore()->roundPrice(
                $this->getItemFinalPriceInclTax()
            );
        $totals['price_after_discount_excl_tax'] = $this->getQty()
            * $quote->getStore()->roundPrice(
                $this->getItemFinalPriceExclTax()
            );

        return $totals;
    }

    /**
     * @return string|void
     */
    public function getWeeeTaxAppliedUnserialized()
    {
        $weeeTaxField = $this->_getData('weee_tax_applied');
        if ($weeeTaxField) {
            return unserialize($weeeTaxField);
        }

        return array();
    }

    /**
     * Returns the fixed product tax including VAT
     * @return float|int
     */
    public function getFixedProductTaxInclVat()
    {
        $total = 0;

        foreach ($this->getWeeeTaxAppliedUnserialized() as $tax) {
            $total += $tax['amount_incl_tax'];
        }

        return $total;
    }

    /**
     * Check if the product is a promo product
     *
     * @return bool
     */
    public function isPromo()
    {
        return $this->getData('is_promo') || $this->getProduct()->isPromo();
    }

    /**
     * @return int
     */
    public function getPackageId()
    {
        return $this->getData('package_id');
    }

    /**
     * @return string
     */
    public function getDiscountRuleNames()
    {
        /** @var int $ruleIds */
        $ruleIds = Mage::helper('pricerules')->explodeTrimmed($this->getAppliedRuleIds());
        /** @var Omnius_Checkout_Model_SalesRule_Rule $rules */
        $rules = Mage::getModel('salesrule/rule')->getCollection()->addFieldToFilter('rule_id', array('in' => $ruleIds));
        $discountNames = array();
        foreach ($rules as $rule) {
            $discountNames[] = $rule->getName();
        }

        return implode(', ', $discountNames);
    }

    /**
     * Trigger beforeSave method to check if offer
     */
    protected function _beforeSave()
    {
        parent::_beforeSave();
        if ($this->getIsOffer() && Mage::getSingleton('customer/session')->getOfferteData()) {
            $tempQuote = Mage::getSingleton('customer/session')->getOfferteData();
            foreach ($tempQuote->getItemsCollection() as $item) {
                if ($item->getId() == $this->getId()) {
                    foreach ($this::$_itemData as $field) {
                        $this->setData($field, $tempQuote->getData($field));
                    }
                    break;
                }

            }
        }
    }

    /**
     * Retrieve product model object associated with item
     *
     * @return Omnius_Catalog_Model_Product
     */
    public function getProduct()
    {
        $product = $this->_getData('product');
        if ($product === null && $this->getProductId()) {
            $key = serialize(array(__METHOD__, $this->getProductId(), $this->getQuote()->getStoreId()));
            if ($product = unserialize($this->getCache()->load($key))) {
                $this->setProduct($product);
            } else {
                /** @var Omnius_Catalog_Model_Product $product */
                $product = Mage::getModel('catalog/product')
                    ->setStoreId($this->getQuote()->getStoreId())
                    ->load($this->getProductId());
                $this->setProduct($product);
                $this->getCache()->save(serialize($product), $key, array(Dyna_Cache_Model_Cache::PRODUCT_TAG), $this->getCache()->getTtl());
            }
        }

        return $product;
    }

    /**
     * @return Dyna_Cache_Model_Cache
     */
    protected function getCache()
    {
        if ($this->_cache === null) {
            $this->_cache = Mage::getSingleton('dyna_cache/cache');
        }

        return $this->_cache;
    }

    /**
     * @return float|null
     */
    public function getItemFinalMafInclTax()
    {
        return $this->getMafRowTotalInclTax() - $this->getMafDiscountAmount();
    }

    /**
     * @return float|null
     */
    public function getItemFinalMafExclTax()
    {
        return $this->getMafRowTotal() - ($this->getMafDiscountAmount() - $this->getHiddenMafTaxAmount());
    }

    /**
     * @return float
     */
    public function getItemFinalPriceInclTax()
    {
        return $this->getRowTotalInclTax() - $this->getDiscountAmount();
    }

    /**
     * @return float
     */
    public function getItemFinalPriceExclTax()
    {
        return $this->getRowTotal() - ($this->getDiscountAmount() - $this->getHiddenTaxAmount());
    }
}
