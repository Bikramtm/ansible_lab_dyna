<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Checkout_Model_Sales_UndeliveredOrder extends Omnius_Checkout_Model_Sales_Order
{
    /**
     * @return array
     * @throws Exception
     */
    public function build()
    {
        parent::build();
        $allPackages = array_merge($this->_processedPackages, $this->_untouchedPackages);
        /** @var Omnius_Package_Model_Package $package */
        foreach ($allPackages as $package) {
            $this->addPackage($package);
        }

        $this->_childOrders = $this->buildOrders($this->_buildQuotes, $this->getSuperorderId());
        $this->processCancelled();

        if (count($this->_childOrders) == 0 && count($this->_cancelledPackages) == (count($allPackages) + count($this->_cancelledPackages))) {
            $this->setStatus(self::STATUS_CANCELLED)->save();
        } else {
            $this->setEditedOrder(1);
        }

        $incrementIds = [];
        foreach($this->_childOrders as $orderIncrement) {
            $incrementIds[] = $orderIncrement->getIncrementId();
        }
        return $incrementIds;
    }

    /**
     * @param Omnius_Package_Model_Package $package
     * @return $this
     */
    protected function addPackage($package)
    {
        //if there is no quote generated for the address hash, generate a new one.
        if (!isset($this->_buildQuotes[$package->getOrderHash()])) {
            $newQuote = $this->_packageHelper->buildQuoteFromPackage($package, $this);
        } else {
            /** @var Omnius_Checkout_Model_Sales_Quote $newQuote */
            $newQuote = $this->_buildQuotes[$package->getOrderHash()];
        }

        $package->setQuoteId($newQuote->getId())->setOrderId(null)->save();

        foreach ($package->getItems() as $item) {
            /** @var Mage_Sales_Model_Quote_Item $quoteItem */
            $quoteItem = clone $item;
            $quoteItem->setPackageId($package->getPackageId());
            $quoteItem->setQuote($newQuote);
            $quoteItem->save();
        }

        $this->_buildQuotes[$package->getOrderHash()] = $newQuote;
        return $this;
    }

    /**
     * @return array
     * @throws Exception
     */
    protected function buildNewOrders()
    {
        $finalOrders = [];
        /** @var Omnius_Checkout_Model_Sales_Quote $quote */
        foreach ($this->_buildQuotes as $quote) {
            $newOrder = $this->generateOrder($quote);
            $newOrder->addStatusHistoryComment($this->getStatus(), '[Notice] - Order edited (address changed)');
            $newOrder->setParentId($this->getId());
            $newOrder->setSuperorderId($this->getSuperorderId());
            $newOrder->save();

            $newOrder->setEdited(0);
            $finalOrders[] = $newOrder;
        }
        return $finalOrders;
    }
}