<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * @category    Dyna
 * @package     Omnius_Checkout
 */
class Omnius_Checkout_Model_Sales_Quote_Address_Item extends Mage_Sales_Model_Quote_Address_Item
{
    /**
     * Import item to quote
     *
     * @param Mage_Sales_Model_Quote_Item $quoteItem
     * @return Mage_Sales_Model_Quote_Address_Item
     */
    public function importQuoteItem(Mage_Sales_Model_Quote_Item $quoteItem)
    {
        parent::importQuoteItem($quoteItem);

        // List of fields that will be saved from quote item to quote address item 
        $fields = array(
            'Ctn',
            'AppliedRuleIds',
            'StoreId',
            'Maf',
            'BaseMaf',
            'NetworkProvider',
            'NetworkOperator',
            'ContractNr',
            'ContractEndDate',
            'CurrentSimcardNumber',
            'CurrentNumber',
            'SimNumber',
            'TelNumber',
            'ConnectionType',
            'PackageId',
            'PackageType',
            'PackageStatus',
            'SaleType',
            'MafDiscountAmount',
            'BaseMafDiscountAmount',
            'MafRowTotalWithDiscount',
            'HiddenMafTaxAmount',
            'BaseHiddenMafTaxAmount',
            'AppliedPromoAmount',
            'BaseAppliedPromoAmount',
            'AppliedMafPromoAmount',
            'BaseAppliedMafPromoAmount',
            'CustomPrice',
            'OriginalCustomPrice',
            'NumberPortingType',
            'RowTotal',
            'RowTotalInclTax',
            'ConnectionCost',
            'IsPromo',
            'TargetId',
            'RegisterReturn',
            'OldSim',
            'IsDefaulted',
        );

        foreach ($fields as $field) {
            $val = call_user_func(array($quoteItem, 'get' . $field));
            call_user_func(array($this, 'set' . $field), $val);
        }
        $this->setCustomStatus(Omnius_Checkout_Model_Sales_Quote::VALIDATION);

        return $this;
    }

    /**
     * Calculate item maf row total price
     *
     * @return Omnius_Checkout_Model_Sales_Quote_Address_Item
     */
    public function calcMafRowTotal()
    {
        $qty = $this->getTotalQty();
        // Round unit price before multiplying to prevent losing 1 cent on subtotal
        $total = $this->getStore()->roundPrice($this->getCalculationMafOriginal()) * $qty;
        $baseTotal = $this->getStore()->roundPrice($this->getBaseCalculationMafOriginal()) * $qty;

        $this->setMafRowTotal($this->getStore()->roundPrice($total));
        $this->setBaseMafRowTotal($this->getStore()->roundPrice($baseTotal));

        return $this;
    }

    /**
     * Get item price used for quote calculation process.
     * This method get custom price (if it is defined) or original product final price
     *
     * @return float
     */
    public function getCalculationMaf()
    {
        $maf = $this->_getData('calculation_maf');
        if (is_null($maf)) {
            if ($this->hasCustomMaf()) {
                $maf = $this->getCustomMaf();
            } else {
                $maf = $this->getConvertedMaf();
            }

            $this->setData('calculation_maf', $maf);
        }

        return $maf;
    }

    /**
     * Get item price converted to quote currency
     * @return float
     */
    public function getConvertedMaf()
    {
        $price = $this->_getData('converted_maf');
        if (is_null($price)) {
            $price = $this->getStore()->convertPrice($this->getMaf());
            $this->setData('converted_maf', $price);
        }

        return $price;
    }

    /**
     * Get item price used for quote calculation process.
     * This method get original custom price applied before tax calculation
     *
     * @return float
     */
    public function getCalculationMafOriginal()
    {
        $maf = $this->_getData('calculation_maf');
        if (is_null($maf)) {
            if ($this->hasOriginalCustomMaf()) {
                $maf = $this->getOriginalCustomMaf();
            } else {
                $maf = $this->getConvertedMaf();
            }

            $this->setData('calculation_maf', $maf);
        }

        return $maf;
    }

    /**
     * Get calculation price used for quote calculation in base currency.
     *
     * @return float
     */
    public function getBaseCalculationMaf()
    {
        if (!$this->hasBaseCalculationMaf()) {
            $price = $this->getMaf();
            $this->setBaseCalculationMaf($price);
        }

        return $this->_getData('base_calculation_maf');
    }

    /**
     * Get original calculation price used for quote calculation in base currency.
     *
     * @return float
     */
    public function getBaseCalculationMafOriginal()
    {
        if (!$this->hasBaseCalculationMaf()) {
            $price = $this->getMaf();
            $this->setBaseCalculationMaf($price);
        }

        return $this->_getData('base_calculation_maf');
    }

    /**
     * Specify item price (base calculation price and converted price will be refreshed too)
     *
     * @param   float $value
     * @return  Mage_Sales_Model_Quote_Item_Abstract
     */
    public function setMaf($value)
    {
        $this->setBaseCalculationMaf(null);
        $this->setConvertedMaf(null);

        return $this->setData('maf', $value);
    }

    /**
     * @return bool
     */
    public function isPromo()
    {
        return $this->getData('is_promo') || $this->getProduct()->isPromo();
    }

    /**
     * @return float|null
     */
    public function getItemFinalMafInclTax()
    {
        return $this->getMafRowTotalInclTax() - $this->getMafDiscountAmount();
    }

    /**
     * @return float|null
     */
    public function getItemFinalMafExclTax()
    {
        return $this->getMafRowTotal() - ($this->getMafDiscountAmount() - $this->getHiddenMafTaxAmount());
    }

    /**
     * @return float
     */
    public function getItemFinalPriceInclTax()
    {
        return $this->getRowTotalInclTax() - $this->getDiscountAmount();
    }

    /**
     * @return float
     */
    public function getItemFinalPriceExclTax()
    {
        return $this->getRowTotal() - ($this->getDiscountAmount() - $this->getHiddenTaxAmount());
    }
}
