<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Shopping cart model
 *
 * @category    Dyna
 * @package     Omnius_Checkout
 */
class Omnius_Checkout_Model_Cart extends Mage_Checkout_Model_Cart
{
    /**
     * @param bool $orderEditMode
     * @return Omnius_Checkout_Model_Sales_Quote|Mage_Sales_Model_Quote
     */
    public function getQuote($orderEditMode = false)
    {
        $orderEditMode = Mage::getSingleton('customer/session')->getOrderEdit() && ($orderEditMode || Mage::getSingleton('customer/session')->getOrderEditMode());
        if ($orderEditMode && !Mage::getSingleton('checkout/session')->getIsEditMode()) {
            $quote = Mage::getModel('sales/quote')->load(Mage::getSingleton('customer/session')->getOrderEdit());
            if ($quote->getId()) {
                return $quote;
            }
        }

        return parent::getQuote();
    }

    /**
     * @param Mage_Sales_Model_Quote $quote
     */
    public function setEditMode(Mage_Sales_Model_Quote $quote)
    {
        $session = Mage::getSingleton('checkout/session');
        $currentCartId = $this->getQuote()->getId();
        $currentPackages = $this->extractPackageInfo($quote->getPackages());

        $session->setPreviousQuoteId($currentCartId);
        $session->setPreviousPackages($currentPackages);

        //todo optimise
        $collection = Mage::getResourceModel('sales/quote_collection')
            ->addFieldToFilter('customer_id', Mage::getSingleton('customer/session')->getCustomerId())
            ->addFieldToFilter('is_active', true);
        foreach ($collection->getItems() as $item) {
            $item->setIsActive(false);
            $item->save();
        }
        $quote->setIsActive(true);
        $quote->save();

        $session->replaceQuote($quote);
        $this->setQuote($quote);

        $session->setIsEditMode(true);
    }

	    /**
     * @param $packageItems
     * @return array
     */
    public function extractPackageInfo($packageItems)
    {
        /** @var Omnius_Service_Model_DotAccessor $accessor */
        $accessor = Mage::getSingleton('omnius_service/dotAccessor');
        $extracted = array();
        foreach ($packageItems as $packageInfo) {
            $data = array();
            if ($packageId = $accessor->getValue($packageInfo, 'package_id')) {
                $data['package_id'] = $packageId;
                $data['type'] = $accessor->getValue($packageInfo, 'type');
                $data['sale_type'] = $accessor->getValue($packageInfo, 'sale_type');
                $data['ctn'] = $accessor->getValue($packageInfo, 'ctn');
                $data['items'] = array();
                $ids = array();
                foreach (is_array($accessor->getValue($packageInfo, 'items')) ? $accessor->getValue($packageInfo, 'items') : array() as $item) {
                    $data['items'][] = $item;
                    array_push($ids, $item->getId());
                }
                $data['ids'] = $ids;
                $extracted[$packageId] = $data;
            }
        }
        return $extracted;
    }

    public function exitEditMode()
    {
        $session = Mage::getSingleton('checkout/session');
        if ($this->getPreviousQuoteId()) {
            $previousQuote = Mage::getModel('sales/quote')->load($this->getPreviousQuoteId());
            if ($previousQuote->getId() && ! $previousQuote->getSuperOrderEditId()) {
                $previousQuote
                    ->setIsActive(true)
                    ->save();
                $this->setQuote($previousQuote);
                $session->setQuoteId($previousQuote->getId());
            }
        }
        $session->setPreviousQuoteId(null);
        $session->setPreviousPackages(null);
        $session->setIsEditMode(false);
        Mage::getSingleton('customer/session')->setFailedSuperOrderId(null);
    }

    /**
     * Verifies that the current edited quote contains changes
     *
     * @param null $section
     * @return bool
     */
    public function isChanged($section = null)
    {
        $session = Mage::getSingleton('checkout/session');
        $isDirty = false;
        if ($session->getIsEditMode() && ($packages = $session->getPreviousPackages())) {
            /**
             * Gather current products and packages
             */
            $currentPackages = [];
            $currentProducts = [];
            $currentDoaItems = [];
            $this->getCurrentDataForPackages($section, $currentPackages, $currentDoaItems, $currentProducts, $isDirty);

            /**
             * Gather initial products and packages
             */
            $initialPackages = [];
            $initialProducts = [];
            foreach ($packages as $package) {
                $this->getInitialPackageData($package, $initialPackages, $initialProducts);
            }
            $initialProducts = array_unique($initialProducts);
            sort($initialProducts);

            /**
             * Compare products from certain section
             */
            if ($section) {
                $currentSectionProducts = isset($currentPackages[$section]) ? array_unique(array_values($currentPackages[$section])) : [];
                $initialSectionProducts = isset($initialPackages[$section]) ? array_unique(array_values($initialPackages[$section])) : [];
                sort($currentSectionProducts);
                sort($initialSectionProducts);
                if ($currentSectionProducts != $initialSectionProducts || isset($currentDoaItems[$section])) {
                    $isDirty = true;
                }
            } elseif ($currentProducts != $initialProducts) {
                /**
                 * Compare all products and check for missing or added products
                 */
                $isDirty = true;
            }
        }

        return $isDirty;
    }

    /**
     * Add product to shopping cart (quote)
     *
     * @param   int|Mage_Catalog_Model_Product $productInfo
     * @param   mixed $requestInfo
     * @return  Mage_Checkout_Model_Cart
     */
    public function addProduct($productInfo, $requestInfo=null)
    {
        $product = $this->_getProduct($productInfo);
        $request = $this->_getProductRequest($requestInfo);

        $productId = $product->getId();

        if ($product->getStockItem()) {
            $minimumQty = $product->getStockItem()->getMinSaleQty();
            //If product was not found in cart and there is set minimal qty for it
            if ($minimumQty && $minimumQty > 0 && $request->getQty() < $minimumQty
                && !$this->getQuote()->hasProductId($productId)
            ) {
                $request->setQty($minimumQty);
            }
        }

        if ($productId) {
            try {
                $result = $this->getQuote()->addProduct($product, $request);
                $result->setOneOfDeal($request->getOneOfDeal());
            } catch (Mage_Core_Exception $e) {
                $this->getCheckoutSession()->setUseNotice(false);
                $result = $e->getMessage();
            }
            /**
             * String we can get if prepare process has error
             */
            if (is_string($result)) {
                $redirectUrl = ($product->hasOptionsValidationFail())
                    ? $product->getUrlModel()->getUrl(
                        $product,
                        array('_query' => array('startcustomization' => 1))
                    )
                    : $product->getProductUrl();
                $this->getCheckoutSession()->setRedirectUrl($redirectUrl);
                if ($this->getCheckoutSession()->getUseNotice() === null) {
                    $this->getCheckoutSession()->setUseNotice(true);
                }
                Mage::throwException($result);
            }
        } else {
            Mage::throwException(Mage::helper('checkout')->__('The product does not exist.'));
        }

        Mage::dispatchEvent('checkout_cart_product_add_after', array('quote_item' => $result, 'product' => $product));
        $this->getCheckoutSession()->setLastAddedProductId($productId);

        return $result;
    }

    /**
     * Initialize cart quote state to be able use it on cart page
     *
     * @return Mage_Checkout_Model_Cart
     */
    public function init()
    {
        $quote = $this->getQuote()->setCheckoutMethod('');

        if ($this->getCheckoutSession()->getCheckoutState() !== Mage_Checkout_Model_Session::CHECKOUT_STATE_BEGIN) {
            $this->getCheckoutSession()->resetCheckout();
        }

        if (!$quote->hasItems()) {
            $quote->getShippingAddress()->setCollectShippingRates(false)
                ->removeAllShippingRates();
        }

        return $this;
    }

    /**
     * Get product object based on requested product information
     *
     * @param   mixed $productInfo
     * @return  Mage_Catalog_Model_Product
     */
    protected function _getProduct($productInfo)
    {
        $product = null;
        $websiteId = Mage::app()->getWebsite()->getId();
        if (($emulatedWebsiteId = Mage::helper('superorder')->shouldEmulateStore($websiteId)) != $websiteId) {
            // If a product from another shop should be added, emulate that store
            $storeId = Mage::app()->getWebsite($emulatedWebsiteId)->getDefaultStore()->getId();
        } else {
            $storeId = Mage::app()->getStore()->getId();
        }
        if ($productInfo instanceof Mage_Catalog_Model_Product) {
            $product = $productInfo;
        } elseif (is_int($productInfo) || is_string($productInfo)) {
            $product = Mage::getModel('catalog/product')
                ->setStoreId($storeId)
                ->load($productInfo);
        }
        if (!$product
            || !$product->getId()
            || !is_array($product->getWebsiteIds())
            || !in_array($emulatedWebsiteId, $product->getWebsiteIds())
        ) {
            Mage::throwException(Mage::helper('checkout')->__('The product could not be found.'));
        }

        return $product;
    }

    /**
     * @param $section
     * @param $currentPackages
     * @param $currentDoaItems
     * @param $currentProducts
     * @param $isDirty
     */
    protected function getCurrentDataForPackages($section, &$currentPackages, &$currentDoaItems, &$currentProducts, &$isDirty)
    {
        foreach ($this->getQuote()->getAllItems() as $item) {
            if ($item->isPromo()) {
                continue;
            }

            array_push($currentProducts, $item->getProductId());
            if ($section) {
                $currentPackages[current($item->getProduct()->getType())][] = $item->getProductId();
                if ($item->getItemDoa()) {
                    $currentDoaItems[current($item->getProduct()->getType())] = 1;
                }
            } elseif ($item->getItemDoa()) {
                $isDirty = true;
            }
        }
        $currentProducts = array_unique($currentProducts);
        sort($currentProducts);
    }

    /**
     * @param $package
     * @param $initialPackages
     * @param $initialProducts
     */
    protected function getInitialPackageData($package, &$initialPackages, &$initialProducts)
    {
        foreach ($package['items'] as $type => $items) {
            foreach ($items as $product) {
                if ($product['is_promo']) {
                    continue;
                }
                $initialPackages[$type][] = $product['product_id'];
                array_push($initialProducts, $product['product_id']);
            }
        }
    }
}
