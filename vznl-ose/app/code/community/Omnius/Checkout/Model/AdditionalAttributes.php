<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Checkout_Model_AdditionalAttributes
 */
class Omnius_Checkout_Model_AdditionalAttributes extends Mage_Core_Model_Abstract
{
    /**
     * Add Additional Attributes
     * @param mixed $attributes
     * @param string $id
     * @param string $level
     * @return void
     */
    public function addAdditionalAttributes(array $attributes, string $id, string $level = 'quote')
    {
        $insert = [];

        foreach ($attributes as $attribute) {
            $insert[$attribute->getKey()] = $attribute->getValue();
        }
        $serializedInsert = !empty($insert) ? json_encode($insert) : "";

        switch ($level) {
            case 'quote':
                $obj = Mage::getModel('sales/quote')->load($id);
                break;

            case 'package':
                $obj = Mage::getModel('package/package')->load($id);
                break;

            case 'product':
                $obj = Mage::getModel('sales/quote_item')->load($id);
                break;

            case 'customer':
                $obj = Mage::getModel('customer/customer')->load($id);
                break;
        }

        if ($obj->getId()) {
            $obj->setAdditionalAttributes($serializedInsert)
                ->save();
        }
    }

    /**
     * Get Additional Attributes
     * @param string $id
     * @param string $level
     * @return array
     */
    public function getAdditionalAttributes(string $id, string $level = 'quote')
    {
        $result = [];

        switch ($level) {
            case 'quote':
                $obj = Mage::getModel('sales/quote')->load($id);
                break;

            case 'package':
                $obj = Mage::getModel('package/package')->load($id);
                break;

            case 'product':
                $obj = Mage::getModel('sales/quote_item')->load($id);
                break;

            case 'customer':
                $obj = Mage::getModel('customer/customer')->load($id);
                break;
        }

        if ($obj->getId()) {
            $result = json_decode($obj->getAdditionalAttributes() ?? '[]', true);
        }

        return $result;
    }

    /**
     * Set Additional Attributes For Alternate Quote Items
     * @param Dyna_Checkout_Model_Sales_Quote $quote
     * @param Dynacommerce\Shoppingcart\V1\Model\ProductSpec $productModel
     * @return void
     */
    public function setAttributesForAlternateItems(
        Dyna_Checkout_Model_Sales_Quote $quote,
        Dynacommerce\Shoppingcart\V1\Model\ProductSpec $productModel
    ) {
        $attributes = $productModel->getAdditionalAttributes();
        $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $productModel->getSku());
        $result = Mage::getModel('sales/quote_item')->getCollection()
            ->getSelect()
            ->limit(1)
            ->reset(Zend_Db_Select::WHERE)
            ->where('quote_id IS NULL')
            ->where('alternate_quote_id = ?', $quote->getId())
            ->where('product_id = ?', $product->getId())
            ->query()
            ->fetch();

        $itemId = $result['item_id'] ?? null;
        if ($itemId) {
            $this->addAdditionalAttributes($attributes, $itemId, 'product');
        }
    }
}
