<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Checkout_Model_Mysql4_Reason extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init("omnius_checkout/reason", "entity_id");
    }
}
