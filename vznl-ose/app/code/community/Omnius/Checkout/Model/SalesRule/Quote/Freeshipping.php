<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Checkout_Model_SalesRule_Quote_Freeshipping
 */
class Omnius_Checkout_Model_SalesRule_Quote_Freeshipping extends Mage_SalesRule_Model_Quote_Freeshipping
{
    /**
     * Collect information about free shipping for all address items
     *
     * @param   Mage_Sales_Model_Quote_Address $address
     * @return  Mage_SalesRule_Model_Quote_Freeshipping
     */
    public function collect(Mage_Sales_Model_Quote_Address $address)
    {
        $this->_setAddress($address);
        /**
         * Reset amounts
         */
        $this->_setAmount(0);
        $this->_setBaseAmount(0);
        $address->setFreeShipping(0);

        return $this;
    }
}
