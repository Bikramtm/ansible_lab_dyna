<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Checkout_Model_SalesRule_Remove extends Mage_Sales_Model_Quote_Address_Total_Abstract
{

    /**
     * @var Omnius_PriceRules_Model_Validator
     */
    private $_calculator;

    /**
     * Initialize discount collector
     */
    public function __construct()
    {
        $this->setCode('maf_discount');
        $this->_calculator = Mage::getSingleton('salesrule/validator');
    }

    /**
     * Collect address discount amount
     *
     * @param   Mage_Sales_Model_Quote_Address $address
     * @return  Mage_SalesRule_Model_Quote_Discount
     */
    public function collect(Mage_Sales_Model_Quote_Address $address)
    {
        $conn = Mage::getSingleton('core/resource')->getConnection('core_write');
        $this->_setAddress($address);
        /**
         * Reset amounts
         */
//        $this->_setMafAmount(0);
//        $this->_setBaseMafAmount(0);

        $items = $this->_getAddressItems($address);
        if (!count($items)) {
            return $this;
        }

        $quote = $address->getQuote();
        $couponsPerPackage = $quote->getCouponsPerPackage();
        // Remember the previous active package id.
        $prevActivePackageId = $quote->getActivePackageId();
        $store = Mage::app()->getStore($quote->getStoreId());

        /** @var Omnius_Package_Model_Mysql4_Package_Collection $packages */
        $packages = Mage::getModel('package/package')->getPackages(null, $quote->getId());
        $quote->setPackageModels($packages);

        $this->_calculator->init($store->getWebsiteId(), $quote->getCustomerGroupId(), '', $packages, $address);

        foreach ($items as $item) {

            $packageModel = $packages->getItemByColumnValue('package_id', $item->getPackageId());

            //workaround for packages having id as ctn - to be removed
            if (!$packageModel || !$packageModel->getId()) {
                $packageModel = $packages->getItemByColumnValue('ctn', $item->getPackageId());
            }
            if (!$packageModel) {
                continue;
            }

            $quote->setActivePackageId($item->getPackageId());
            $coupon = isset($couponsPerPackage[$item->getPackageId()]) ? $couponsPerPackage[$item->getPackageId()] : $packageModel->getCoupon();
            $this->_calculator
                ->setActivePackage($packageModel)
                ->setCouponCode($coupon)
                ->setWebsiteId($store->getWebsiteId())
                ->setCustomerGroupId($quote->getCustomerGroupId())
            ;


            $eventArgs = array(
                'website_id' => $store->getWebsiteId(),
                'customer_group_id' => $quote->getCustomerGroupId(),
                'coupon_code' => $packageModel->getCoupon(),
            );

            $eventArgs['item'] = $item;
            Mage::dispatchEvent('sales_quote_address_discount_item', $eventArgs);

            if ($item->getHasChildren() && $item->isChildrenCalculated()) {
                foreach ($item->getChildren() as $child) {
                    $this->_calculator->process($child);
                }
            } else {
                $this->_calculator->process($item);
            }
        }

        $address->unsetData('cached_items_nonnominal');
        $address->unsetData('cached_items_all');
        $address->unsetData('cached_items_nominal');
        /*
                foreach ($quote->getAllItems() as $i) {

                }*/

        foreach ($this->_getAddressItems($address) as $item) {

            if ($item->getProduct()->is(Omnius_Catalog_Model_Type::SUBTYPE_DEVICE)) {
                // Mage::log("is_subtype_device: ".$item->getId(),null,"remove.log");
                Mage::dispatchEvent('sales_quote_address_mixmatch_here', ['quote_item' => $item->getQuoteItem() ?: $item, "callback_method"=>'remove']);
            }
            // Mage::dispatchEvent('sales_quote_address_mixmatch_here', ['quote_item' => $item,"callback_method"=>'remove']);
            $packageModel = $packages->getItemByColumnValue('package_id', $item->getPackageId());

            //workaround for packages having id as ctn - to be removed
            if (!$packageModel || !$packageModel->getId()) {
                $packageModel = $packages->getItemByColumnValue('ctn', $item->getPackageId());
            }
            if (!$packageModel) {
                continue;
            }

            $quote->setActivePackageId($item->getPackageId());
            $coupon = isset($couponsPerPackage[$item->getPackageId()]) ? $couponsPerPackage[$item->getPackageId()] : $packageModel->getCoupon();
            $this->_calculator
                ->setActivePackage($packageModel)
                ->setCouponCode($coupon)
                ->setWebsiteId($store->getWebsiteId())
                ->setCustomerGroupId($quote->getCustomerGroupId())
            ;


            $eventArgs = array(
                'website_id' => $store->getWebsiteId(),
                'customer_group_id' => $quote->getCustomerGroupId(),
                'coupon_code' => $packageModel->getCoupon(),
            );

            $eventArgs['item'] = $item;
            Mage::dispatchEvent('sales_quote_address_discount_item', $eventArgs);
//
            if ($item->getHasChildren() && $item->isChildrenCalculated()) {
                foreach ($item->getChildren() as $child) {
                    $this->_calculator->process($child);
                }
            } else {
                $this->_calculator->process($item);
            }
        }

        $quote->setActivePackageId($prevActivePackageId);

        $itemIds = array();
        foreach ($packages as $pack) {
            if (
                isset($this->_calculator->couponsApplied[$pack->getPackageId()])
                && $this->_calculator->couponsApplied[$pack->getPackageId()] != $pack->getCoupon()
            ) {
                Mage::helper('pricerules')->incrementCoupon($this->_calculator->couponsApplied[$pack->getPackageId()], $quote->getCustomer()->getId());
                $pack->setCoupon($this->_calculator->couponsApplied[$pack->getPackageId()])->save();
            } elseif (
                !isset($this->_calculator->couponsApplied[$pack->getPackageId()])
                && $pack->getCoupon() != null
            ) {
                Mage::helper('pricerules')->decrementCoupon($pack->getCoupon(), $quote->getCustomer()->getId());
                $itemIds[] = $pack->getId();
            }
        }


        if (count($itemIds)) {
            $conn->update('catalog_package', array('coupon' => null, 'updated_at' => now()), sprintf('entity_id in (%s)', join(',', $itemIds)));
        }
		$this->_calculator->clearInstance();
		
        return $this;
    }



}
