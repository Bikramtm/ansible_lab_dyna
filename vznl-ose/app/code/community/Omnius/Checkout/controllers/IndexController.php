<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Checkout_IndexController extends Mage_Core_Controller_Front_Action
{
    /** @var Omnius_Checkout_Helper_Data $checkoutHelper */
    protected $checkoutHelper;
    /** @var Omnius_Checkout_Helper_Validations $validationsHelper */
    protected $validationsHelper;

    protected $hasSubscription;

    protected $_classActions = array(
        'saveContract',
        'saveContractPolling',
        'saveNewNetherlands',
        'saveNumberSelection',
        'saveDeviceSelection',
        'saveCreditCheck',
        'saveOverview',
        'saveOverviewPolling',
        'saveOverviewAfter',
        'saveCustomer',
        'saveNumberPorting',
        'saveDeliveryAddress',
        'saveFixedToMobile',
        'saveVodafoneHome',
        'updateCreditNote',
        'convertBic',
        'stockStores',
        'searchAddress',
        'searchCompany',
        'saveSuperOrder',
        'saveSuperOrderPolling',
        'cancelOrder',
        'cancelPendingOrder',
        'cancelOrderPolling',
        'retrieveNumberList',
        'saveFailedCreditCheck',
        'retryPolling',
        'redoCreditCheck',
        'redoCreditCheckPolling',
        'validateDate',
        'convertFirstname',
        'registerReturn',
        'registerReturnPolling',
        'createLongIban',
        'getLockInfo',
    );


    /** @var Omnius_Core_Helper_Data|null */
    protected $coreHelper = null;
    /** @var Omnius_Service_Helper_Data|null */
    protected $omniusServiceHelper = null;

    /**
     * @return Omnius_Core_Helper_Data
     */
    protected function getCoreHelper()
    {
        if ($this->coreHelper === null) {
            $this->coreHelper = Mage::helper('omnius_core');
        }

        return $this->coreHelper;
    }

    /**
     * @return Mage_Core_Controller_Front_Action
     */
    public function preDispatch()
    {
        $this->checkoutHelper = Mage::helper('omnius_checkout');
        $this->validationsHelper = Mage::helper('omnius_checkout/validations');

        return parent::preDispatch();
    }

    /**
     * Get checkout session model instance
     *
     * @return Mage_Checkout_Model_Session
     */
    protected function _getCheckoutSession()
    {
        return Mage::getSingleton('checkout/session');
    }

    /**
     * Get checkout session model instance
     *
     * @return Mage_Customer_Model_Session
     */
    protected function _getCustomerSession()
    {
        return Mage::getSingleton('customer/session');
    }

    /**
     * Get current active quote instance
     *
     * @return Omnius_Checkout_Model_Sales_Quote
     */
    protected function getQuote()
    {
        if(Mage::getSingleton('customer/session')->getOrderEdit()) {
            $quote = Mage::getModel('sales/quote')->load(Mage::getSingleton('customer/session')->getOrderEdit());
            if($quote->getId()) {
                $this->_getCheckoutSession()->setLoadInactive()->setQuoteId($quote->getId());
            }
        }

        return $this->_getCheckoutSession()->getQuote();
    }

    /**
     * @param $action
     * @return bool
     */
    public function hasAction($action)
    {
        return in_array($action, $this->_classActions);
    }

    /**
     * Get one page checkout model
     *
     * @return Mage_Checkout_Model_Type_Multishipping
     */
    public function getCheckOut()
    {
        return Mage::getSingleton('checkout/type_multishipping');
    }

    /**
     * Get one page checkout model
     *
     * @return Mage_Checkout_Model_Type_Onepage
     */
    public function getOnePage()
    {
        return Mage::getSingleton('checkout/type_onepage');
    }

    /**
     * @param $customer
     * @return mixed
     */
    protected function _getCustomerEmail($customer)
    {
        $emailAddressCsv = $customer->getData('additional_email');
        $emailAddressArr = explode(';', $emailAddressCsv);
        return $emailAddressArr[0];
    }

    /**
     * Allows agents to register the return of hardware
     */
    public function registerReturnAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            try {
                $data = $request->getParam('returned', array());
                $triggerComplete = $request->getParam('trigger_complete', array());
                foreach ($data as $packageId => $packageData) {
                    $quote = $this->getQuote(true);
                    /** @var Omnius_Package_Model_Package $quotePackage */
                    if ((isset($triggerComplete[$packageId]) && $triggerComplete[$packageId] && isset($packageData['complete_return']) && $packageData['complete_return']) || trim($packageData['return_date'])) {
                        $this->_validateRefundData($packageData);
                    }

                    $orderPackage = Mage::getModel('package/package')->getPackages($quote->getSuperOrderEditId(), null,
                        $packageId)->getFirstItem();
                    $allowedFields = array('return_notes', 'not_damaged_items');
                    if (isset($triggerComplete[$packageId]) && $triggerComplete[$packageId]) {
                        $allowedFields[] = 'complete_return';
                    }
                    if (isset($packageData['complete_return']) && $packageData['complete_return']) {
                        $allowedFields[] = 'return_date';
                    }
                    foreach ($packageData as $fieldName => $fieldData) {
                        if (in_array($fieldName, $allowedFields)) {
                            //for return notes, append to existing notes, don't overwrite
                            if ($fieldName == 'return_notes') {
                                $orderPackage->setData($fieldName,
                                    $orderPackage->getData($fieldName) . "\n" . $fieldData);
                            } else {
                                $orderPackage->setData($fieldName, $fieldData);
                            }
                        }
                    }
                    $orderPackage->setReturnedBy($this->_getCustomerSession()->getAgent(true)->getUsername());
                    $orderPackage->save();

                    // Get returned items list
                    $packageItems = !empty($packageData['items']) ? (is_array($packageData['items']) ? $packageData['items'] : array($packageData['items'])) : [];

                    // Set order items as returned
                    $items = $orderPackage->getPackageItems();
                    foreach ($items as $item) {
                        if (in_array($item->getProductId(), $packageItems)) {
                            $item->setRegisterReturn(1)->save();
                        }
                    }
                    // Also set temporary queue items as returned
                    $items = $this->getQuote()->getAllVisibleItems();
                    $currentItems = array();
                    foreach ($items as $item) {
                        if ($item->getPackageId() == $packageId) {
                            $currentItems[] = $item;
                            if (in_array($item->getProductId(), $packageItems)) {
                                $item->setRegisterReturn(1)->save();
                            }
                        }
                    }

                    /** @var $superOrder Omnius_Superorder_Model_Superorder */
                    $superOrder = Mage::getModel('superorder/superorder')->load($quote->getSuperOrderEditId());

                    $result['html'] = $this->getLayout()
                        ->createBlock('core/template')
                        ->setPackage($orderPackage)
                        ->setItems($currentItems)
                        ->setTemplate('checkout/cart/steps/return_note.phtml')
                        ->toHtml();
                    $result['error'] = false;

                    $superOrder->setUpdatedAt(now())->save();
                    //if package is marked as completed and all other packages
                    if (isset($triggerComplete[$packageId]) && $triggerComplete[$packageId] && isset($packageData['complete_return']) && $packageData['complete_return']) {
                        if ($orderPackage->getNewPackageId()) {
                            $packageId = $orderPackage->getNewPackageId();
                        }

                        $newSuperOrder = $superOrder->getChildSuperorders($packageId);
                        if (!$superOrder->cantProcessOrder()) {
                            $newSuperOrder = $newSuperOrder ? $newSuperOrder : $superOrder;
                            if ($newSuperOrder->getXmlToBeSent()) {
                                Mage::register(
                                    'job_additional_information',
                                    array(
                                        'run_superorder_history' => true,
                                        'superorder_id' => $newSuperOrder->getId(),
                                        'superorder_parent_id' => $newSuperOrder->getId() == $superOrder->getId() ? null : $superOrder->getId(),
                                        'agent_id' => $this->_getCustomerSession()->getAgent(true)->getId(),
                                        'run_check_lock' => true,
                                        'superorder_number' => $superOrder->getOrderNumber(),
                                        'initial_state' => true,
                                    )
                                );

                                $this->_getCustomerSession()->setData(
                                    'polling',
                                    array(
                                        'superorder_id' => $newSuperOrder->getId(),
                                        'parent_superorder_id' => $superOrder->getId()
                                    )
                                );
                            }
                        }
                    }

                    // Change the order confirmation message as stated in RFC-150663
                    $result['oldOrderMessage'] = $this->__('The goods return is successfully registered under the following number.');
                    $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                        Mage::helper('core')->jsonEncode($result)
                    );

                    return;
                }
            } catch (Exception $e) {
                $result['error'] = true;
                $result['message'] = $this->__($e->getMessage());
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );

                return;
            }
        }
        $result['error'] = true;
        $result['message'] = $this->__('Invalid package data');
        $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
            Mage::helper('core')->jsonEncode($result)
        );

        return;
    }

    /**
     * Converts first name during customer save
     */
    public function convertFirstnameAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            try {
                $input = $request->getParam('name');
                $result = array(
                    'error' => 'false',
                    'name' => $this->checkoutHelper->processInitials($input),
                );
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            } catch (Exception $e) {
                $result['error'] = true;
                $result['message'] = $this->__($e->getMessage());
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            }
        } else {
            return;
        }
    }

    /**
     * @throws Exception|mixed
     */
    public function saveContractPollingAction()
    {
        $result = array(
            'error' => false,
            'performed' => true
        );
        $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
            Mage::helper('core')->jsonEncode($result)
        );
        return;
    }

    /**
     * Triggered when agents sign the contract in Retail
     */
    public function saveContractAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            try {
                /** @var Omnius_Superorder_Model_Superorder $superOrder */
                $superOrder = Mage::getModel('superorder/superorder')->getSuperOrderByOrderNumber($request->getParam('superorder_id'));
                $editSuperOrder = Mage::getSingleton('customer/session')->getLastSuperOrder();

                $params = $request->getParam('contract');
                if ( ! $superOrder->getId()) {
                    Mage::throwException('No active superorder found.');
                }

                $deliveryOrders = $superOrder->getOrders(true)->getItems();

                if (isset($params['signed'])) {
                    foreach ($deliveryOrders as $deliveryOrder) {
                        if ($deliveryOrder->getId() && !$deliveryOrder->getContractSignDate()) {
                            $deliveryOrder->setContractSignDate(date("Y-m-d H:i:s"));
                            $deliveryOrder->save();
                        }
                    }
                }

                // Regenerate contracts because phone numbers might have changed
                /** @var Omnius_Checkout_Helper_Pdf $pdfHelper */
                $pdfHelper = Mage::helper('omnius_checkout/pdf');
                /** @var Omnius_Checkout_Block_Pdf_Contract $printOutput */
                $printOutput = Mage::app()->getLayout()
                    ->createBlock('omnius_checkout/pdf_contract')
                    ->setTemplate('checkout/cart/pdf/contract.phtml')
                    ->superOrder($superOrder);

                // RFC-151034 : Regenerate contracts only if there at least one active package (ESB might change packages statuses between checkout steps)
                if (!$printOutput->getAllPackages()) {
                    //No package, don't save contract and throw exception
                    $response = array(
                        'serviceError' => $this->__("Error"),
                        'message' => $this->__("The order content has just been changed, please reopen the order"),
                    );
                    $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                        Mage::helper('core')->jsonEncode($response)
                    );
                    return;
                }

                $pdfInstance = new VFDOMPDF;
                $pdfInstance->load_html($printOutput->toHtml());
                $pdfInstance->render();
                $pdfOutput = $pdfInstance->output();

                if(isset($params['sendmail'])) {
                    $contract = $pdfHelper->saveContract($pdfOutput, $superOrder->getOrderNumber(), 'pdf', true);
                    $customer = Mage::getModel('customer/customer')->load($superOrder->getCustomerId());

                    $email = $this->_getCustomerEmail($customer); // TODO FIXME: this was always empty, using customer object is not the right way but works for now
                }

                Mage::getSingleton('customer/session')->setLastSuperOrder(null);//clear it

                // Set successful response
                $result['error'] = false;
                $result['message'] = "Step data successfully saved";
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            } catch (Exception $e) {
                // Set fail response for other errors
                $result['error'] = true;
                $result['message'] = $this->__($e->getMessage());
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );

                return;
            }
        }
    }

    /**
     * Cancel order and trigger a processOrder
     */
    public function cancelOrderAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            try {
                $superOrderNo = trim($request->getPost('super_order'));
                if($superOrderNo) {
                    /** @var Omnius_Superorder_Model_Superorder $superOrder */
                    $superOrder = Mage::getModel('superorder/superorder')->getSuperOrderByOrderNumber($superOrderNo);
                    if(!$superOrder || !$superOrder->getId()) {
                        Mage::getSingleton('core/logger')->logException(new Exception('Invalid Order id during cancelOrder'));
                        throw new Exception($this->__('Invalid Order id'));
                    }
                    $superOrder->cancel();
                    $this->checkoutHelper->exitSuperOrderEdit();
                    // Set successful response
                    $result['error'] = false;
                    $result['polling_id'] = $superOrder->getData('polling_id');
                    $result['message'] = "Step data successfully saved";
                    $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                        Mage::helper('core')->jsonEncode($result)
                    );
                    return;
                } else {
                    Mage::getSingleton('core/logger')->logException(new Exception('Invalid Order id during cancelOrder'));
                    throw new Exception($this->__('Invalid Order id'));
                }
            } catch (Exception $e) {
                $this->getServiceHelper()->returnServiceError($e);
                return;
            }
        }
    }

    /**
     * Cancels in DA an order in pending state
     */
    public function cancelPendingOrderAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            try {
                $superOrderNo = trim($request->getPost('super_order'));
                if ($superOrderNo) {
                    /** @var Omnius_Superorder_Model_Superorder $superOrder */
                    $superOrder = Mage::getModel('superorder/superorder')->getSuperOrderByOrderNumber($superOrderNo);
                    if (!$superOrder || !$superOrder->getId() || !$superOrder->hasPendingPackages()) {
                        Mage::getSingleton('core/logger')->logException(new Exception('Invalid Order id during cancelOrder'));
                        throw new Exception($this->__('Invalid Order id'));
                    }
                    $superOrder->cancelPendingOrder();
                    $this->checkoutHelper->exitSuperOrderEdit();
                    // Set successful response
                    $result['error'] = false;
                    $result['message'] = "Step data successfully saved";
                    $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                        Mage::helper('core')->jsonEncode($result)
                    );

                    return;
                } else {
                    Mage::getSingleton('core/logger')->logException(new Exception('Invalid Order id during cancelOrder'));
                    throw new Exception($this->__('Invalid Order id'));
                }
            } catch (Exception $e) {
                Mage::helper('omnius_core')->returnError($e);
                return;
            }
        }
    }

    /**
     * Persists the number selection data
     */
    public function saveNumberSelectionAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            try {
                $data = $request->getPost('numberselection');
                $superOrderNo = $request->getPost('order_edit');
                $errors = $this->_validateNumberSelectionData($data);

                if ($errors) {
                    throw new Mage_Customer_Exception(Mage::helper('core')->jsonEncode($errors));
                }

                if ($data && empty($superOrderNo)) {
                    $orders = array();
                    $orderIds = Mage::getSingleton('core/session')->getOrderIds();
                    $sessionOrderIds = !empty($orderIds) ? $orderIds : [];
                    foreach ($sessionOrderIds as $orderId) {
                        $orders[] = Mage::getModel('sales/order')->loadByAttribute('increment_id', $orderId);
                    }
                    $this->_updateNumberSelection($orders, $data);
                } elseif ($data && !empty($superOrderNo)) {
                    $superOrderId = Mage::getModel('superorder/superorder')->getSuperOrderByOrderNumber($superOrderNo)->getId();
                    $orders = Mage::getModel('sales/order')->getNonEditedOrderItems($superOrderId);
                    $this->_updateNumberSelection($orders, $data);
                }

                // Set successful response
                $result['error'] = false;
                $result['message'] = "Step data successfully saved";
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            } catch (Mage_Customer_Exception $e) {
                // Set fail response for validations
                $result['error'] = true;
                $result['message'] = $this->checkoutHelper->__("Please correct the following errors");
                $result['fields'] = Mage::helper('core')->jsonDecode($e->getMessage());
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            } catch (Exception $e) {
                // Set fail response for other errors
                $result['error'] = true;
                $result['message'] = $this->__($e->getMessage());
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            }
        }
    }

    /**
     * Parse input and sanitise any user inputted data
     *
     * @return Mage_Core_Controller_Request_Http
     */
    public function getRequest()
    {
        if(isset($this->_requestSanitised)) {
            return $this->_requestSanitised;
        }
        $request = parent::getRequest();
        $post = $request->getPost();
        array_walk_recursive($post, function (&$value) {
            $value = strip_tags($value);
        });
        $request->setPost($post);
        $this->_requestSanitised = $request;
        return $request;
    }

    /**
     * Persist customer data
     */
    public function saveCustomerAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            try {
                // Validate there is a quote
                $quote = $this->getQuote();
                if (!$quote->getId()) {
                    throw new Exception($this->__('Invalid quote'));
                }

                // Check if customer is already logged in
                $customer = $this->_getCustomerSession()->getCustomer();
                if (!$customer->getId()) {
                    $data = $this->_buildAndValidateCustomerData($request);
                    $checkCustomer = 'invalid';
                    while ($checkCustomer != null) {
                        $email = microtime(true) . uniqid("", true) . Omnius_Customer_Model_Customer_Customer::DEFAULT_EMAIL_SUFFIX;
                        $checkCustomer = Mage::getModel('customer/customer')->loadByEmail($email)->getId();
                    }

                    // Customer email address is an uniqid now
                    $data['email'] = $email;
                    // Save all customer and billing address information to the quote
                    $result = $this->getOnepage()->saveBilling($data, null);

                    if ($result) {
                        throw new Exception(Mage::helper('core')->jsonEncode($result));
                    }
                } else {
                    // When customer is logged in check if it is hardware only for this customer
                    $customerData = $request->getPost('customer', array());
                    if ($customer->getIsProspect() || isset($customerData['hardware_only']) && $customerData['hardware_only'] == $customer->getId()) {
                        // Validate input data
                        $data = $this->_buildAndValidateCustomerData($request);
                        if ($customer->getIsProspect()) {
                            // Also save all customer and billing address information to the quote
                            $result = $this->getOnepage()->saveBilling($data, null);
                        }
                        foreach ($data as $key => $value) {
                            // Map data to the customer
                            $customer->setData($key, $value);
                        }
                        $customer->save();
                    }
                    $emailOverwrite = trim($request->getParam('email_overwrite'));
                    if ($emailOverwrite) {
                        if (Mage::helper('omnius_validators')->validateEmailSyntax($emailOverwrite)) {
                            $this->_getCustomerSession()->setNewEmail($emailOverwrite);
                        } else {
                            throw new Mage_Customer_Exception(Mage::helper('core')->jsonEncode(['email_overwrite' => $this->__('Please enter a valid email address. For example johndoe@domain.com.')]));
                        }
                    } else {
                        $this->_getCustomerSession()->setNewEmail(null);
                    }
                }

                $quote->setCustomStatus(Omnius_Checkout_Model_Sales_Quote::VALIDATION);
                if ($request->getPost('current_step') && !$request->getParam('is_offer')) {
                    $quote->setCurrentStep($request->getPost('current_step'));
                }

                $quote->save();

                // Check and save offer
                if ($request->getParam('is_offer') == 1 && $this->createOffer()) {
                    $result['is_offer'] = true;
                }

                // Set successful response
                $result['error'] = false;
                $result['message'] = "Step data successfully saved";
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            } catch (Mage_Customer_Exception $e) {
                // Set fail response for validations
                $result['error'] = true;
                $result['message'] = $this->checkoutHelper->__("Please correct the following errors");
                $result['fields'] = Mage::helper('core')->jsonDecode($e->getMessage());
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            } catch (Exception $e) {
                // Set fail response for other errors
                $result['error'] = true;
                $result['message'] = $this->__($e->getMessage());
                if ($this->getServiceHelper()->isDev()) {
                    $result['trace'] = $e->getTrace();
                }
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            }
        }

        $this->_redirect('/');
    }

    /**
     * @return Omnius_Service_Helper_Data
     */
    protected function getServiceHelper(){
        if($this->omniusServiceHelper === null){
            $this->omniusServiceHelper = Mage::helper('omnius_service');
        }
        return $this->omniusServiceHelper;
    }

    /**
     * @return Omnius_Checkout_Model_Sales_Quote
     */
    protected function createOffer()
    {
        /** @var Omnius_Checkout_Model_Sales_Quote $quote */
        $quote = $this->getQuote();

        // Get SOC codes for each package
        /** @var Omnius_Package_Model_Package[] $packages */
        $packages = Mage::getModel('package/package')
            ->getPackages(null, $quote->getId());

        // Mark quote as offer
        $quote->setIsOffer(1)
            ->setChannel(Mage::app()->getWebsite()->getCode());

        //Unset the saved shopping cart.
        Mage::helper('omnius_checkout')->unsetSavedCart($quote->getQuoteParentId());

        $customer = null;
        if ($quote->getCustomer()->getId()) {
            /** @var Omnius_Customer_Model_Customer_Customer $customer */
            $customer = Mage::getModel('customer/customer')->load($quote->getCustomer()->getId());
        }

        if (!$customer || !$customer->getId()) {
            // Create the customer in database if it doesn't exist
            $customer = $this->_addCustomer();
            $quote->setCustomer($customer)->setCustomerId($customer->getId());
        }


        Mage::getSingleton('core/session')->setGeneralError(
            $this->checkoutHelper->__('The offer has been successfully saved')
        );

        // Create new quote and remove the offer quote from the shopping cart
        $this->checkoutHelper->createNewQuote();

        return $quote;
    }

    /**
     * Persist number porting data
     */
    public function saveNumberPortingAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            try {
                /** @var Omnius_Checkout_Model_Sales_Quote $quote */
                $quote = $this->getQuote();

                if (!$quote->getId()) {
                    throw new Exception($this->__('Invalid quote'));
                }
                $data = $request->getPost('portability', array());

                $errors = $this->_validatePortabilityData($data);

                if ($errors) {
                    throw new Mage_Customer_Exception(Mage::helper('core')->jsonEncode($errors));
                }

                $changedPackages = $this->_savePortabilityData($data, true);

                if ($request->getPost('current_step')) {
                    $quote->setCurrentStep($request->getPost('current_step'));
                }
                $quote->setTotalsCollectedFlag(true)->save();
                $quotes = Mage::helper('omnius_package')->getModifiedQuotes();
                foreach($quotes as $modifiedQuote) {
                    $items = $modifiedQuote->getAllItems();
                    foreach($items as $item) {
                        if(isset($changedPackages[$item->getPackageId()])) {
                            unset($changedPackages[$item->getPackageId()]);
                            break;
                        }
                    }
                }

                $session = $this->_getCustomerSession();
                if($session->getOrderEdit()) {
                    // When in order edit mode, create a quote for each package that has number porting changed
                    /**
                     * Check if current order is not locked by another agent
                     */
                    $this->checkoutHelper->checkOrder($quote->getSuperOrderEditId());
                    foreach ($changedPackages as $packageId => $changedPackage) {
                        $quoteItems = $quote->getPackageItems($packageId);
                        /** @var Mage_Customer_Model_Customer $customer */
                        $customer = $session->getCustomer();
                        /** @var Omnius_Checkout_Model_Sales_Quote $newQuote */
                        $newQuote = Mage::getModel('sales/quote')
                            ->setCustomer($customer)
                            ->setBillingAddress(Mage::getModel('sales/quote_address')->setData($customer->getDefaultBillingAddress() ? $customer->getDefaultBillingAddress()->getData() : array()))
                            ->setShippingAddress(Mage::getModel('sales/quote_address')->setData($customer->getDefaultShippingAddress() ? $customer->getDefaultShippingAddress()->getData() : array()))
                            ->setSuperQuoteId($quote->getId())
                            ->setSuperOrderEditId($quote->getSuperOrderEditId())
                            ->setStoreId($quote->getStoreId());
                        $newQuote->save();

                        /** @var Mage_Sales_Model_Quote_Item $quoteItem */
                        foreach ($quoteItems as &$quoteItem) {
                            $newQuoteItem = Mage::getModel('sales/quote_item')
                                ->setData($quoteItem->getData())
                                ->unsetData('item_id')
                                ->setQuote($newQuote);
                            $newQuoteItem->save();
                        }
                        unset($quoteItem);

                        $packageModelOld = Mage::getModel('package/package')
                            ->getPackages(null, $quote->getId(), $packageId)
                            ->getFirstItem();

                        Mage::getModel('package/package')
                            ->setData($packageModelOld->getData())
                            ->setEntityId(null)
                            ->setQuoteId($newQuote->getId())
                            ->setOrderId(null)
                            ->save();
                        $newQuote->setActivePackageId($packageId);
                        $newQuote
                            ->setTotalsCollectedFlag(false)
                            ->collectTotals()
                            ->save();
                    }
                }

                // Set successful response
                $result['error'] = false;
                $result['message'] = "Step data successfully saved";

                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            } catch (Mage_Customer_Exception $e) {
                // Set fail response for validations
                $result['error'] = true;
                $result['message'] = $this->checkoutHelper->__("Please correct the following errors");
                $result['fields'] = Mage::helper('core')->jsonDecode($e->getMessage());
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            } catch (Exception $e) {
                // Set fail response for other errors
                $result['error'] = true;
                $result['message'] = $this->__($e->getMessage());
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            }
        }
    }

    /**
     * Persist delivery address data
     */
    public function saveDeliveryAddressAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            try {
                $quote = $this->getQuote();

                if (!$quote->getId()) {
                    throw new Exception($this->__('Invalid quote'));
                }
                $delivery = $request->getPost('delivery', array());
                $data = array();
                if (!isset($delivery['method'])) {
                    throw new Exception($this->__('Missing shipment method'));
                }

                $errors = array();
                $showDeliverySteps = true;
                switch ($delivery['method']) {
                    case 'direct' :
                        $addressData = $this->_getCustomerSession()->getAgent(true)->getDealer()->getId();

                        $data['deliver']['address'] = $this->checkoutHelper->_getAddressByType(array(
                            'address' => 'store',
                            'store_id' => $addressData,
                        ));
                        if($this->checkoutHelper->_validateDeliveryAddressData($data['deliver']['address'])) {
                            $errors += array('delivery[method][text]' => $this->__('Invalid store chosen'));
                        }
                        $showDeliverySteps = true;
                        break;
                    case 'pickup' :
                        /** @var Dyna_Agent_Model_Mysql4_Dealer_Collection $addressData */
                        if(!isset($delivery['pickup']['store_id']) || empty($delivery['pickup']['store_id'])) {
                            $errors += array('delivery[pickup][store]' => $this->__('Invalid store chosen'));
                        } else {
                            $data['deliver']['address'] = $this->checkoutHelper->_getAddressByType(array(
                                'address' => 'store',
                                'store_id' => $delivery['pickup']['store_id'],
                            ));
                            if($this->checkoutHelper->_validateDeliveryAddressData($data['deliver']['address'])) {
                                $errors += array('delivery[pickup][store]' => $this->__('Invalid store chosen'));
                            }
                        }
                        break;
                    case 'deliver' :
                        if (!isset($delivery['deliver']['address'])) {
                            throw new Exception($this->__('Missing shipping address info'));
                        }

                        $data['deliver']['address'] = $this->checkoutHelper->_getAddressByType(array(
                            'address' => $delivery['deliver']['address'],
                            'billingAddress' => $request->getPost('billingAddress', array()),
                            'otherAddress' => $request->getPost('otherAddress', array()),
                            'foreignAddress' => $request->getPost('foreignAddress', array()),
                        ));
                        $errors += $this->checkoutHelper->_validateDeliveryAddressData($data['deliver']['address']);
                        break;
                    case 'split' :
                        $splitData = $request->getPost('delivery', array());
                        $splitData = isset($splitData['pakket']) ? $splitData['pakket'] : array();
                        if (!$splitData) {
                            throw new Exception($this->__('Incomplete data'));
                        }

                        $cancelledPackages = Mage::helper('omnius_package')->getCancelledPackages();
                        $cancelledPackages = !empty($cancelledPackages) ? $cancelledPackages : [];
                        $cancelledPackagesNow = Mage::getSingleton('customer/session')->getCancelledPackagesNow();
                        $cancelledPackagesNow = !empty($cancelledPackagesNow) ? $cancelledPackagesNow : [];

                        foreach ($splitData as $packageId => $address) {
                            if (!isset($address['address']) || in_array($packageId, $cancelledPackages) || (!empty($cancelledPackagesNow[$quote->getId()]) && in_array($packageId, $cancelledPackagesNow[$quote->getId()]))) {
                                continue;
                            }

                            $data['pakket'][$packageId]['address'] = $this->checkoutHelper->_getAddressByType($address);

                            if($data['pakket'][$packageId]['address']['address'] == 'store' &&
                               $data['pakket'][$packageId]['address']['store_id'] ==  Mage::getSingleton('customer/session')->getAgent(true)->getDealer()->getId()) {
                                $showDeliverySteps = true;
                            }
                            $errors += $this->_validateSplitDeliveryAddressData($data['pakket'][$packageId]['address'], $packageId);
                        }
                        break;
                    default:
                        $errors += array('delivery[method]' => $this->__('Invalid delivery method'));
                }

                $paymentData = $request->getPost('payment', array());
                if (!isset($paymentData['method'])) {
                    throw new Exception('Missing payment method');
                }

                $payment = array();
                switch ($paymentData['method']) {
                    case 'cashondelivery' :
                    case 'alreadypaid':
                    case 'checkmo':
                    case 'payinstore':
                        $payment = $paymentData['method'];
                        break;
                    case 'split-payment':
                        foreach($paymentData['pakket'] as $packageId => $packageData) {
                            if (isset($packageData['type']) && $packageData['type']) {
                                $payment[$packageId] = $packageData['type'];
                            }
                        }
                        break;
                    default:
                        $errors += array('payment[method]' => $this->__('Invalid payment method'));
                }

                $data['payment'] = $payment;

                // Validate each delivery address has same payment method
                $errors += $this->_validateSplitPaymentAndDelivery($data);

                if ($errors) {
                    throw new Mage_Customer_Exception(Mage::helper('core')->jsonEncode($errors));
                }

                $this->_saveDeliveryAddressData($data);

                if ($request->getPost('current_step')) {
                    $quote->setCurrentStep($request->getPost('current_step'));
                }
                $quote->save();

                // Set successful response
                $result['error'] = false;
                $result['message'] = "Step data successfully saved";
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                $result['delivery_steps'] = $showDeliverySteps;


                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            } catch (Mage_Customer_Exception $e) {
                // Set fail response for validations
                $result['error'] = true;
                $result['message'] = $this->checkoutHelper->__("Please correct the following errors");
                $result['fields'] = Mage::helper('core')->jsonDecode($e->getMessage());
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            } catch (Exception $e) {
                // Set fail response for other errors
                $result['error'] = true;
                $result['message'] = $this->__($e->getMessage());
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            }
        }
    }

    /**
     * @throws Exception|mixed
     */
    public function saveOverviewPollingAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()){
            return;
        }

        try {
            $id = trim($this->getRequest()->getParam('id'));
            if (!$id || 'undefined' === $id) {
                $result = array(
                    'error' => false,
                    'performed' => true
                );
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            }

            $result = array(
                'error' => false,
                'performed' => true
            );
            $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                Mage::helper('core')->jsonEncode($result)
            );
            return;
        } catch (Exception $e) {
            $superOrderId = $this->_getCustomerSession()->getData($id);
            if ($this->_getCustomerSession()->getFailedSuperOrderId() != $superOrderId['superorder_id']) {
                /** @var Omnius_Superorder_Model_Superorder $superOrder */
                $superOrder = Mage::getModel('superorder/superorder')->load($superOrderId['superorder_id']);
                $quote = $superOrder->getOriginalQuote();
                $quote->setSaveAsCart(true);
                $this->_createNewQuoteOnServiceCallFail($quote, $superOrder->getId());
            }
            Mage::helper('omnius_core')->returnError($e);
            return;
        }
    }

    /**
     * Perform various actions after the save
     */
    public function saveOverviewAfterAction()
    {
        $result = array();

        $request = $this->getRequest();
        if ($request->isPost()) {
            $quote = $this->getQuote();
            $id = trim($request->getPost('polling_id'));
            $isRefund = trim($request->getPost('refund'));
            if ($id && 'undefined' != $id) {
                try {
                    /** @var Omnius_Superorder_Model_Superorder $superOrder */
                    $sessionData = $this->_getCustomerSession()->getData($id);
                    $superOrder = Mage::getModel('superorder/superorder')->load($sessionData['superorder_id']);

                    if ($this->_getCustomerSession()->getFailedSuperOrderId() &&
                        $this->_getCustomerSession()->getFailedSuperOrderId() == $superOrder->getId()
                    ) {
                        $this->_getCheckoutSession()->setLoadInactive(false);
                    }

                    if ($superOrder->getErrorCode()) {
                        if ( ! $isRefund) {
                            $quote->setSaveAsCart(true);
                            $this->_createNewQuoteOnServiceCallFail($quote, $superOrder->getId());
                        }
                        throw new Exception($superOrder->getErrorCode() . ' ' . $superOrder->getErrorDetail());
                    }

                    Mage::getSingleton('customer/session')->setCurrentSuperOrderId($superOrder->getId());
                    $this->_getCustomerSession()->setFailedSuperOrderId(null);

                    if ( ! $isRefund) {
                        //initialize a new quote
                        $this->checkoutHelper->createNewQuote();
                    }

                    if ($isRefund) {
                        $customerSession = $this->_getCustomerSession();
                        $customerSession->setOrderEdit(null);
                        $customerSession->setOrderEditMode(false);
                        $customerSession->setPackageDifferences(null);
                        /** @var Omnius_Checkout_Model_Cart $cart */
                        $cart = Mage::getSingleton('checkout/cart');
                        $cart->exitEditMode();
                    }

                } catch (Exception $e) {
                    // Set fail response for other errors
                    $result['error'] = true;
                    $result['message'] = $this->__($e->getMessage());
                    Mage::getSingleton('core/logger')->logException($e);
                    $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                        Mage::helper('core')->jsonEncode($result)
                    );

                    return;
                }
            }
            $result['error'] = false;
            $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                Mage::helper('core')->jsonEncode($result)
            );
        }
    }

    /**
     * Performs main saving action
     */
    public function saveOverviewAction()
    {
        $result = array();

        $request = $this->getRequest();
        if ($request->isPost()) {

            try {
                $this->checkoutHelper->processNotesAndPickup($request);

                $quote = $this->getQuote();

                if (!$quote->getId()) {
                    throw new Exception($this->__('Invalid quote'));
                }

                $quote->setCustomStatus(Omnius_Checkout_Model_Sales_Quote::VALIDATION);

                // Create the order and place the ESB Order id on the response
                try {
                    /** @var Omnius_Superorder_Model_Superorder $superOrder */
                    $superOrder = $this->createOrder();
                } catch (LogicException $e) {

                    $result['error'] = false;
                    $result['message'] = "Step data successfully saved";
                    $result['polling_id'] = 'undefined';
                    $result['orders'] = $this->checkoutHelper->__('Risk incident recorded, order was not created');
                    $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                        Mage::helper('core')->jsonEncode($result)
                    );
                    return;
                }

                $result['orders'] = $superOrder->getOrderNumber();
                $result['order_id'] = $superOrder->getId();

                // Append the address to the response in order to have them in the credit check step
                $result['addresses'] = $this->_buildCreditCheckAddresses($quote);

                if ($request->getPost('current_step')) {
                    $quote->setCurrentStep($request->getPost('current_step'));
                }
                $quote->save();
                $result['error'] = false;
                $result['message'] = "Step data successfully saved";

                // Mark the quote as not a cart
                if ($quote->getQuoteParentId()) {
                    $parentQuote = Mage::getModel('sales/quote')->load($quote->getQuoteParentId());
                    if ($parentQuote->getId()) {
                        $parentQuote->setCartStatus(null)
                            ->save();
                    }
                }

                $request->setPost('superorderNumber', $superOrder->getOrderNumber());
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            } catch (Exception $e) {
                // Set fail response for other errors
                $result['error'] = true;
                $result['message'] = $this->__($e->getMessage());
                Mage::getSingleton('core/logger')->logException($e);
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            }
        }
    }

    /**
     * Convert account number to standard format
     */
    public function convertBicAction()
    {
        $accountNumber = trim($this->getRequest()->getParam('account_number'));
        $valid = null;
        try {
            $valid = Mage::helper('omnius_validators')->validateNLIBAN($accountNumber);
        } catch(Exception $ex) {
            $result = array(
                'errors' => true,
                'to_fill' => $ex->getMessage()
            );
        }
        if ($valid) {
            $result = array(
                'errors' => false,
                'to_fill' => $valid,
            );
        } else {
            $result = array(
                'errors' => true,
                'message' => $this->checkoutHelper->__('Incorrect bank account (11-test failed)')
            );
        }

        $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(Mage::helper('core')->jsonEncode($result));
        return;
    }

    /**
     * Converts IBAN into long format
     */
    public function createLongIbanAction()
    {
        try {
            $quote = $this->getQuote();
            $customer = $quote->getCustomer();
            $accountNumber = $quote->getCustomerAccountNumber();
            $accountNumberLong = Mage::helper('omnius_validators')->validateNLIBAN($accountNumber);
            if ($customer->getAccountNoLong() != $accountNumberLong) {
                $customer->setAccountNoLong($accountNumberLong)->save();
                $quote->updateFields(['customer_account_number_long' => $accountNumberLong]);
            }
        } catch (Exception $e) {
            Mage::getSingleton('core/logger')->logException($e);
        }
    }

    /**
     * Persist superorder data
     */
    public function saveSuperOrderAction()
    {
        try {
            /** @var Omnius_Superorder_Helper_Builder $superOrderBuilder */
            $superOrderBuilder = Mage::helper('superorder/builder');
            $result = $superOrderBuilder->build();

            $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                Mage::helper('core')->jsonEncode($result)
            );
            return;
        } catch (\Exception $e) {
            $result = [
                'error' => true,
                'message' => $e->getMessage()
            ];
            $this->jsonResponse($result);
            return;
        }
    }

    /**
     * @param $orderIdsArray
     * @param $superQuote
     * @param $superOrderId
     * @param null $esbSuperOrder
     * @param null $allModifiedPackages
     * @param null $esbOldSuperOrderId
     */
    protected function _finishSuperOrderSave($orderIdsArray, $superQuote, $superOrderId, $esbSuperOrder = null, $allModifiedPackages = null, $esbOldSuperOrderId = null)
    {
        // send order confirmation email if not all packages are cancelled
        $allActivePackages = Mage::getModel('package/package')
            ->getCollection()
            ->addFieldToFilter('status', array('neq' => Omnius_Package_Model_Package::ESB_PACKAGE_STATUS_CANCELLED))
            ->addFieldToFilter('order_id', $superOrderId);

                // After processorder
        Mage::getSingleton('core/session')->setOrderIds($orderIdsArray);
        Mage::getSingleton('customer/session')->setOrderEdit(null);
        Mage::getSingleton('customer/session')->setCancelledPackages(false);

        $superQuote->setIsActive(false);
        $superQuote->save();

        $this->checkoutHelper->exitSuperOrderEdit($superOrderId); //exit super order edit mode
        $this->checkoutHelper->createNewQuote(); // create a new quote on the cart

        $result['error'] = false;
        $result['performed'] = true;
        $result['message'] = "Super order successfully saved";
        if ($esbSuperOrder && $allModifiedPackages) {
            $orderedItems = $esbSuperOrder->getOrderedItems(true, false, $orderIdsArray, $allModifiedPackages);
            if ($this->checkoutHelper->canProcessDeliverySteps($orderedItems)) {
                $result['contract_page'] = $this->getLayout()
                    ->createBlock('core/template')
                    ->setShowStep(true)
                    ->setTemplate('checkout/cart/steps/save_contract.phtml')
                    ->toHtml();
            }
        }

        $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
            Mage::helper('core')->jsonEncode($result)
        );
    }

    /**
     * @return Zend_Controller_Response_Abstract
     */
    public function saveSuperOrderPollingAction()
    {
        return $this->getResponse()
            ->setHeader('Content-Type', 'application/json')
            ->setBody(
                Mage::helper('core')->jsonEncode(
                    array(
                        'error' => false,
                        'performed' => true,
                    )
                )
            );

    }

    /**
     * @param $string
     * @return bool
     */
    protected function isJSON($string)
    {
        return is_string($string) && is_object(json_decode($string)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
    }

    /**
     * @param $data
     * @return array
     */
    protected function _convertPrivacy($data)
    {
        $result = array(
            'privacy_sms' => 0,
            'privacy_email' => 0,
            'privacy_mailing' => 0,
            'privacy_telemarketing' => 0,
            'privacy_number_information' => 0,
            'privacy_phone_books' => 0,
            'privacy_disclosure_commercial' => 0,
            'privacy_market_research' => 0,
            'privacy_sales_activities' => 0,
        );

        foreach ($data as $item => $value) {
            $result['privacy_' . $item] = $value;
        }

        return $result;
    }

    /**
     * @param $data
     * @param bool $checkForChanges
     * @return array|null
     * @throws Exception
     */
    protected function _savePortabilityData($data, $checkForChanges = false)
    {
        $packages = Mage::getModel('package/package')->getCollection();

        // On order edit also update the main packages
        if($this->_getCustomerSession()->getOrderEdit()) {
            $packages->addFieldToFilter(array('quote_id', 'order_id'), array(array('eq' => $this->getQuote()->getId()), array('eq' => $this->getQuote()->getSuperOrderEditId())));
        } else {
            $packages->addFieldToFilter('quote_id', $this->getQuote()->getId());
        }

        $changedPackagesIds = array();
        /** @var Omnius_Package_Model_Package $package */
        foreach ($packages as $package) {
            $packageId = $package->getPackageId();
            if ($checkForChanges && !isset($changedPackagesIds[$packageId])) {
                $dbData = $package->getPortabilityData();
                foreach (array_keys($dbData) as $key) {
                    $data[$packageId][$key] = isset($data[$packageId][$key]) ? $data[$packageId][$key] : '';
                    $dbData[$key] = isset($dbData[$key]) ? $dbData[$key] : '';
                    $packageCheckPreCond = isset($data[$packageId][$key]) && isset($dbData[$key]);
                    $packageCheckCond = $packageCheckPreCond && ($data[$packageId][$key] != $dbData[$key] && $data[$packageId]['check'] == '1') || ($data[$packageId][$key] != null && $data[$packageId]['check'] == '0');
                    if (isset($data[$packageId][$key]) && $packageCheckCond) {
                        $changedPackagesIds[$packageId] = true;
                        break;
                    }
                }
            }

            if (isset($data[$packageId]['check'])) {
                if ($data[$packageId]['check'] === '1') {
                    $package->setContractNr($data[$packageId]['contract']);
                    $package->setNetworkOperator($data[$packageId]['current_operator']);
                    $package->setNetworkProvider($data[$packageId]['current_provider']);
                    $package->setNumberPortingType($data[$packageId]['number_porting_type']);
                    $package->setContractEndDate($data[$packageId]['end_date_contract']);
                    $package->setCurrentNumber($data[$packageId]['mobile_number']);
                    $package->setCurrentSimcardNumber($data[$packageId]['sim']);
                    $package->setConnectionType($data[$packageId]['type']);

                    if ($data[$packageId]['number_porting_type'] == 0) {
                        $package->setContractNr(null);
                    }

                    if ($data[$packageId]['number_porting_type'] == 1) {
                        $package->setCurrentSimcardNumber(null);
                    }

                    $package->save();
                } elseif ($data[$packageId]['check'] === '0') {
                    // We also need to clear previously saved data in case of an edit
                    $package->setNetworkOperator(null);
                    $package->setContractNr(null);
                    $package->setNetworkProvider(null);
                    $package->setContractEndDate(null);
                    $package->setCurrentNumber(null);
                    $package->setCurrentSimcardNumber(null);
                    $package->setConnectionType(null);
                    $package->setNumberPortingType(null);

                    $package->save();
                }
            }
        }

        return $checkForChanges ? $changedPackagesIds : null;
    }

    /**
     * @param $data
     * @return bool
     */
    protected function _saveDeliveryAddressData($data)
    {
        // Save the delivery address into session for later usage
        $quote = $this->getQuote();
        $quote->setShippingData(Mage::helper('core')->jsonEncode($data));

        $quote->save();
        return true;
    }

    /**
     * @return mixed
     * @throws Exception
     */
    protected function createOrder()
    {
        $checkout = $this->getCheckout();
        $checkout->setCollectRatesFlag(true);
        $quote = $this->getQuote();
        if ($quote->getCustomer()->getId() && !$quote->getCustomer()->getIsProspect()) {
            $customer = Mage::getModel('customer/customer')->load($quote->getCustomer()->getId());
            if ($this->_getCustomerSession()->getNewEmail()) {
               // When the email was overwritten, also set it on customer when the order is created
                $emails = explode(';', $customer->getAdditionalEmail());
                $emails[0] = $this->_getCustomerSession()->getNewEmail();
                $customer->setEmailOverwritten(1)->setAdditionalEmail(implode(';', $emails))->save();
                $this->_getCustomerSession()->setNewEmail(null);
            }
        } else {
            $customer = $this->_addCustomer();

            //if customer exists, load the entity to retrieve the campaigns, ctn records and other data
            $session = $this->_getCustomerSession();
            $session->setCustomer($customer);
            $session->unsBtwState();

            $quote->save();
        }
        Mage::getSingleton('customer/session')->setCustomerInfoSync(true);
        $quote->setIsMultiShipping(true);
        $shipToInfo = $this->_buildDeliveryAddresses($customer->getId());
        $checkout->setShippingItemsInformation($shipToInfo);
        // TODO Since we need to hard-code at this moment, we manually set them
        $addresses = $quote->getAllShippingAddresses();
        foreach ($addresses as $address) {
            $address->setShippingMethod('flatrate_flatrate');
        }
        // Check if the quote is a risk, and if it is just skip to next step
        if($quote->checkRisk()) {
            $packages = Mage::getModel('package/package')
                ->getPackages(null, $this->getQuote()->getId());

            // Mark quote as Offer and as Risk
            $quote
                ->setCartStatus(Omnius_Checkout_Model_Sales_Quote::CART_STATUS_RISK)
                ->setIsOffer(1)
                ->setChannel(Mage::app()->getWebsite()->getCode())
                ->save();

            //Unset the saved shopping cart.
            Mage::helper('omnius_checkout')->unsetSavedCart($quote->getQuoteParentId());

            throw new LogicException('Agent does not have access to submit this order');
        }

        $this->_setDefaultPayment();
        /*********************************************************/
        $checkout->createOrders();

        // From this point on cache all models for faster processing, since they won't be changed
        if (!Mage::registry('freeze_models')) {
            Mage::unregister('freeze_models');
            Mage::register('freeze_models', true);
        }

        $orderPackages = array();
        $packagesTypes = $this->_checkIfDeliveryInShop();

        $orderIds = Mage::getSingleton('core/session')->getOrderIds();
        foreach ($orderIds as $orderIncrementId) {
            $order = Mage::getModel('sales/order')->loadByAttribute('increment_id', $orderIncrementId);

            //create new package entities to set their status
            foreach ($order->getAllItems() as $item) {
                $orderPackages[] = $item->getPackageId();
            }

            // Set the order delivery type
            if (isset($item) && $order->getShippingAddress()) {
                $order->getShippingAddress()->setDeliveryType($packagesTypes[$item->getPackageId()]['type']);
                if($packagesTypes[$item->getPackageId()]['type'] == 'store') {
                    $order->getShippingAddress()->setDeliveryStoreId($packagesTypes[$item->getPackageId()]['store_id']);
                }
                $order->getShippingAddress()->save();
            }
        }

        // Create a new superOrder
        $superOrder = Mage::getModel('superorder/superorder')->createNewSuperorder();
        $quotePackages = Mage::getModel('package/package')->getPackages(null, $quote->getId());

        // If the superorder was successfully created, move the packages to the order
        $isOneOff = false;
        /** @var Omnius_Package_Model_Package $quotePackage */
        foreach($quotePackages as $quotePackage) {
            $quotePackage->setOrderId($superOrder->getId())
                ->setQuoteId(null)
                ->setStatus(Omnius_Package_Model_Package::ESB_PACKAGE_STATUS_INITIAL)
                ->setCreditcheckStatus(Omnius_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_CC_INITIAL)
                ->setCreditcheckStatusUpdated(now());

            if ($quotePackage->getOneOfDeal()) {
                $isOneOff = true;
            }

            if ($quotePackage->isNumberPorting()) {
                $quotePackage->setPortingStatus(Omnius_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_NP_INITIAL)
                    ->setPortingStatusUpdated(now());
            }

            $quotePackage->save();
        }
        unset($quotePackages);

        // Assign all the orders to the super order and custom increment id
        foreach ($orderIds as $orderIncrementId) {
            /** @var Omnius_Checkout_Model_Sales_Order $order */
            $order = Mage::getModel('sales/order')->loadByAttribute('increment_id', $orderIncrementId);
            $order
                ->setSuperorderQuery($superOrder->getId())
                ->setEditedOrder(0)
                ->oneOff($isOneOff)
                ->processPackageChecksum();
        }

        return $superOrder;
    }

    /**
     * @param $data
     * @return array
     */
    protected function _validateCustomerAdditionaldata($data) {
        $errors = array();

        // Validate additional email data
        $emails = array_filter($data['email'] ?: [], 'strlen');
        foreach ($emails as $id => $email) {
            if (!Mage::helper('omnius_validators')->validateEmailSyntax($email)
            || !Mage::helper('omnius_validators')->validateEmailDns($email)) {
                $errors += array('additional[email][' . $id . ']' => $this->__('Invalid e-mail address'));
            }
        }

        return $errors;
    }

    /**
     * @param $data
     * @return array|bool
     * @throws Exception
     */
    protected function _validateCustomerData($data)
    {
        $errors = array();

        $inputFieldsPrivate = $this->_getCustomerDataFields();
        $inputFieldsBusiness = $this->_getBusinessDataFields();
        if ($data['is_business']) {
            $validations = $inputFieldsBusiness;
            switch($data['company_address']) {
                case 'other_address' :
                    $validationsBusiness = array(
                        'company_street' => array(
                            'required' => true,
                            'validation' => array(
                                'validateName'
                            )
                        ),
                        'company_house_nr' => array(
                            'required' => true,
                            'validation' => array(
                                'validateNumber'
                            )
                        ),
                        'company_house_nr_addition' => array(
                            'required' => false,
                            'validation' => array(
                            )
                        ),
                        'company_postcode' => array(
                            'required' => true,
                            'validation' => array(
                            )
                        ),
                        'company_city' => array(
                            'required' => true,
                            'validation' => array(
                            )
                        ),
                    );
                    $errors += $this->checkoutHelper->_performValidate($data['otherAddress'], $validationsBusiness, 'customer[otherAddress][', ']');
                    break;
                case 'foreign_address' :
                    $validationsBusiness = array(
                        'company_street' => array(
                            'required' => true,
                            'validation' => array(
                                'validateName'
                            )
                        ),
                        'company_house_nr' => array(
                            'required' => true,
                            'validation' => array(
                                'validateNumber'
                            )
                        ),
                        'company_house_nr_addition' => array(
                            'required' => false,
                            'validation' => array(
                            )
                        ),
                        'extra' => array(
                            'required' => false,
                            'validation' => array(
                            )
                        ),
                        'company_city' => array(
                            'required' => true,
                            'validation' => array(
                            )
                        ),
                        'company_region' => array(
                            'required' => false,
                            'validation' => array(
                            )
                        ),
                        'company_postcode' => array(
                            'required' => true,
                            'validation' => array(
                            )
                        ),
                        'company_country_id' => array(
                            'required' => true,
                            'validation' => array(
                            )
                        ),
                    );
                    $errors += $this->checkoutHelper->_performValidate($data['foreignAddress'], $validationsBusiness, 'customer[foreignAddress][', ']');
                    break;
                default :
                    throw new Exception($this->__('Missing company address data'));
            }
        } else {
            $validations = $inputFieldsPrivate;

        }

        if ($this->hasSubscription) {
            $errors += $this->validateIdNumberByType($data);
        }
        $errors += $this->checkoutHelper->_performValidate($data, $validations, 'customer[', ']');

        return $errors;
    }

    /**
     * @param $data
     * @param bool $onlyDate
     * @param bool $type
     * @param bool $packageId
     * @param bool $current_operator
     * @param bool $current_provider
     * @param int $customerType
     * @return array
     */
    protected function _validatePortabilityData($data, $onlyDate = false, $type = false, $packageId = false, $current_operator = false, $current_provider = false, $customerType = 0)
    {
        $isBusiness = $customerType == 1 ? true : false;
        $validations = array();
        if (!$data) {
            $validations = $this->validateNpDate($isBusiness);
        }

        if ($onlyDate !== false) {
            $validations = array('end_date_contract' => $validations['end_date_contract']);
            $data = array(
                $packageId => array(
                    'check' => 1,
                    'end_date_contract' => $onlyDate,
                    'current_operator' => $current_operator,
                    'current_provider' => $current_provider,
                    'number_porting_type' => $customerType,
                    'type' => $type,
                )
            );
        }

        $errors = array();
        $usedNumbers = array();
        $usedSims = array();

        foreach ($data as $packageId => $packageData) {
            if (empty($packageData)) {
                continue;
            }
            $validations = $this->validateNpDate($packageData['number_porting_type']);
            $current_op = $packageData['current_operator'];
            $current_provider = $packageData['current_provider'];
            if ($packageData['type']) {
                if ($current_op == Omnius_Catalog_Model_Type::NUMBER_PORTING_CURRENT_OPERATOR && $current_provider == Omnius_Catalog_Model_Type::NUMBER_PORTING_CURRENT_PROVIDER) {
                        $validations['end_date_contract']['validation'][2] = 'validateMinPortingDaysPrepaid';
                        $validations['end_date_contract']['message']['validateMinPortingDaysPrepaid'] = $this->checkoutHelper->__('The date must be at least 2 working days in the future.');
                        unset($validations['end_date_contract']['message']['validateMinPortingDays']);
                } else {
                    $validations['end_date_contract']['validation'][2] = 'validateMinPortingDays';
                    $validations['end_date_contract']['message']['validateMinPortingDays'] = $this->checkoutHelper->__('The date must be at least 5 working days in the future.');
                    unset($validations['end_date_contract']['message']['validateMinPortingDaysPrepaid']);
                }
            }

            if (isset($packageData['check']) && $packageData['check']) {
                $tmpValidations = $validations;
                if (isset($packageData['number_porting_type'])) {
                    if ($packageData['number_porting_type'] == 0) {
                        unset($tmpValidations['contract']);
                    }
                    if ($packageData['number_porting_type'] == 1) {
                        unset($tmpValidations['sim']);
                    }
                }
                // Validate fields
                $errors += $this->checkoutHelper->_performValidate($packageData, $tmpValidations,
                    'portability[' . $packageId . '][', ']');

                // Validate sim data is consistent with operator data
                if ((isset($packageData['number_porting_type'], $packageData['sim']) && $packageData['number_porting_type'] == 0) && !Mage::helper('omnius_validators/data')->validateSimByProviderAndOperator($packageData['sim'],
                        $packageData['current_operator'], $packageData['current_provider'])
                ) {
                    $errors['portability[' . $packageId . '][sim]'] = $this->checkoutHelper->__('Field has invalid data');
                }
            }

            if ($packageId && (isset($packageData['mobile_number']) && !empty($packageData['mobile_number']))) {
                // Validate number is unique
                if (in_array($packageData['mobile_number'], $usedNumbers)) {
                    $errors += array('portability[' . $packageId . '][mobile_number]' => $this->__('Number is already selected'));
                } else {
                    array_push($usedNumbers, $packageData['mobile_number']);
                }
            }

            if ($packageId && (isset($packageData['sim']) && !empty($packageData['sim']))) {
                // Validate sim is unique
                if (in_array($packageData['sim'], $usedSims)) {
                    $errors += array('portability[' . $packageId . '][sim]' => $this->__('Sim is already selected'));
                } else {
                    array_push($usedSims, $packageData['sim']);
                }
            }
        }
        unset($usedNumbers);
        unset($usedSims);

        return $errors;
    }

    /**
     * @param $data
     * @param $packageId
     * @return array
     * @throws Exception
     */
    protected function _validateSplitDeliveryAddressData($data, $packageId)
    {
        $errors = array();
        switch($data['address']) {
            case 'store':
                $addressValidations = array(
                    'street' => array(
                        'required' => true,
                        'validation' => array(
                        )
                    ),
                    'postcode' => array(
                        'required' => true,
                        'validation' => array(
                        )
                    ),
                    'city' => array(
                        'required' => true,
                        'validation' => array(
                        )
                    ),
                );
                if($this->checkoutHelper->_performValidate($data, $addressValidations, 'delivery[pakket][' . $packageId . ']store[', ']')) {
                    $errors += array('delivery[pakket][' . $packageId . '][address]' => $this->__('Invalid store chosen'));
                };
                break;
            case 'deliver':
            case 'billing_address':
            case 'other_address' :
                $addressValidations = array(
                    'street' => array(
                        'required' => true,
                        'validation' => array(
                        )
                    ),
                    'houseno' => array(
                        'required' => false,
                        'validation' => array(
                            'validateNumber'
                        )
                    ),
                    'addition' => array(
                        'required' => false,
                        'validation' => array(
                        )
                    ),
                    'postcode' => array(
                        'required' => true,
                        'validation' => array(
                        )
                    ),
                    'city' => array(
                        'required' => true,
                        'validation' => array(
                        )
                    ),
                );
                $errors += $this->checkoutHelper->_performValidate($data, $addressValidations, 'delivery[pakket][' . $packageId . '][otherAddress][', ']');
                break;
            case 'foreign_address' :
                $addressValidations = array(
                    'street' => array(
                        'required' => true,
                        'validation' => array(
                        )
                    ),
                    'houseno' => array(
                        'required' => false,
                        'validation' => array(
                            'validateNumber'
                        )
                    ),
                    'addition' => array(
                        'required' => false,
                        'validation' => array(
                        )
                    ),
                    'extra' => array(
                        'required' => false,
                        'validation' => array(
                        )
                    ),
                    'city' => array(
                        'required' => true,
                        'validation' => array(
                        )
                    ),
                    'region' => array(
                        'required' => false,
                        'validation' => array(
                        )
                    ),
                    'postcode' => array(
                        'required' => true,
                        'validation' => array(
                        )
                    ),
                    'country_id' => array(
                        'required' => true,
                        'validation' => array(
                        )
                    ),
                );
                $errors += $this->checkoutHelper->_performValidate($data, $addressValidations, 'delivery[pakket][' . $packageId . '][foreignAddress][', ']');
                break;
            default :
                throw new Exception($this->__('Missing delivery address data'));
        }

        return $errors;
    }

    /**
     * @param $customerId
     * @return array
     * @throws Exception
     */
    protected function _buildDeliveryAddresses($customerId)
    {
        $quote = $this->getQuote();
        // Retrieve the previously saved data for addresses
        $data = Mage::helper('core')->jsonDecode($quote->getShippingData());

        $addressArray = array();
        if (isset($data['deliver'])) { // Single delivery
            foreach ($quote->getPackages() as $packageId => $package) {
                $addressArray[$packageId] = $data['deliver']['address'];
            }
        } elseif (isset($data['pakket'])) { // Split delivery
            foreach ($quote->getPackages() as $packageId => $package) {
                $addressArray[$packageId] = $data['pakket'][$packageId]['address'];
            }
        }

        $addresses = array();
        foreach ($addressArray as $packageId => $address) {
            if (is_numeric($address)) {
                $address = Mage::getModel('customer/address')->load($address);
                if (!$address) {
                    throw new Exception($this->__('Invalid delivery address id'));
                }
                $addresses[$packageId] = $address->getId();
            } elseif (is_array($address)) {
                $addresses[$packageId] = $this->_addCustomerShippingAddress($customerId, $address);
            }
        }

        $result = array();
        $items = $quote->getAllItems();

        foreach ($items as $item) {
            $result[] = array(
                $item->getId() => array(
                    'address' => $addresses[$item->getPackageId()],
                    'qty' => $item->getQty()
                )
            );
        }

        return $result;
    }

    /**
     * @return array
     */
    protected function _checkIfDeliveryInShop()
    {
        $quote = $this->getQuote();
        // Retrieve the previously saved data for addresses
        $data = Mage::helper('core')->jsonDecode($quote->getShippingData());

        $addressArray = array();
        if (isset($data['deliver'])) { // Single delivery
            foreach ($quote->getPackages() as $packageId => $package) {
                $addressArray[$packageId]['type'] = $data['deliver']['address']['address'];
                if ($addressArray[$packageId]['type'] == 'store') {
                    $addressArray[$packageId]['store_id'] = $data['deliver']['address']['store_id'];
                }
            }
        } elseif (isset($data['pakket'])) { // Split delivery
            foreach ($quote->getPackages() as $packageId => $package) {
                $addressArray[$packageId]['type'] = $data['pakket'][$packageId]['address']['address'];
                if ($addressArray[$packageId]['type'] == 'store') {
                    $addressArray[$packageId]['store_id'] = $data['pakket'][$packageId]['address']['store_id'];
                }
            }
        }

        return $addressArray;
    }

    /**
     * @param $customerId
     * @param $addressData
     * @return bool|mixed
     * @throws Exception
     */
    protected function _addCustomerShippingAddress($customerId, $addressData)
    {
        $customer = Mage::getModel('customer/customer')->load($customerId);
        $shipAddress = $this->getQuote()->getShippingAddress();
        $addressData['firstname'] = (trim($shipAddress->getFirstname()) != '') ? $shipAddress->getFirstname() : $customer->getFirstname();
        $addressData['company'] = $shipAddress->getCompany();
        // Delivery can only be in Netherlands!
        $addressData['country_id'] = Omnius_Checkout_Model_Sales_Order::DEFAULT_COUNTRY;
        $addressData['fax'] = $shipAddress->getFax();
        $addressData['lastname'] = (trim($shipAddress->getLastname()) != '') ? $shipAddress->getLastname() : $customer->getLastname();
        $addressData['middlename'] = (trim($shipAddress->getMiddlename()) != '') ? $shipAddress->getMiddlename() : $customer->getMiddlename();
        $addressData['suffix'] = (trim($shipAddress->getSuffix()) != '') ? $shipAddress->getSuffix() : $customer->getSuffix();
        $addressData['prefix'] = (trim($shipAddress->getPrefix()) != '') ? $shipAddress->getPrefix() : $customer->getPrefix();
        $addressData['telephone'] = $shipAddress->getTelephone();
        // Check if the address was already added, in that case just return the id.
        $addressId = false;

        foreach ($customer->getAddresses() as $tempAddress) {
            if ($addressData['street'] == array(
                    $tempAddress->getStreetName(),
                    $tempAddress->getHouseNo(),
                    $tempAddress->getHouseAdd(),
                ) &&
                $addressData['postcode'] == $tempAddress->getPostcode() &&
                $addressData['city'] == $tempAddress->getCity()
            ) {
                $addressId = $tempAddress->getId();
                break;
            }
        }
        if (!$addressId) {
            // address does not exist in the db , create it
            $address = Mage::getModel('customer/address');

            $addressForm = Mage::getModel('customer/form');
            $addressForm->setFormCode('customer_address_edit')
                ->setEntity($address);

            $addressErrors = $addressForm->validateData($addressData);
            if (true === $addressErrors) {
                $addressForm->compactData($addressData);
                $address->setCustomerId($customer->getId())
                    ->setIsDefaultBilling(false)
                    ->setIsDefaultShipping(false)
                    ->save();

                return $address->getId();
            } else {
                throw new Exception(Mage::helper('core')->jsonEncode($addressErrors));
            }

        } else {
            // just return the id
            return $addressId;
        }
    }

    /**
     * @return Mage_Customer_Model_Customer
     * @throws Exception
     */
    protected function _addCustomer()
    {
        $quote = $this->getQuote();
        $billing = $quote->getBillingAddress();
        $shipping = $quote->isVirtual() ? null : $quote->getShippingAddress();

        /* @var $customer Mage_Customer_Model_Customer */
        $customer = $quote->getCustomer();
        $customerBilling = $billing->exportCustomerAddress();
        $customer->addAddress($customerBilling);
        $billing->setCustomerAddress($customerBilling);
        $customerBilling->setIsDefaultBilling(true);
        if ($shipping && !$shipping->getSameAsBilling()) {
            $customerShipping = $shipping->exportCustomerAddress();
            $customer->addAddress($customerShipping);
            $shipping->setCustomerAddress($customerShipping);
            $customerShipping->setIsDefaultShipping(true);
        } else {
            $customerBilling->setIsDefaultShipping(true);
        }

        Mage::helper('core')->copyFieldset('checkout_onepage_quote', 'to_customer', $quote, $customer);
        $customer->setPassword(md5(mt_rand()));
        $customer->setPasswordHash($customer->hashPassword($customer->getPassword()));
        $customer->setIsProspect(false);
        $quote->setCustomer($customer)
            ->setCustomerId($customer->getId());
        // RFC 160064 - When a certain email address is used, generate an unique dummy one for safety
        $allEmails = explode(";", $customer->getData('additional_email'));
        if ($quote->getDummyEmail()) {
            $allEmails[0] = Mage::helper('omnius_customer')->generateDummyEmail();
            $customer->setData('additional_email', implode(";", $allEmails));
            $quote->setData('additional_email', implode(";", $allEmails));
        }
        $customer->save();

        return $customer;
    }

    /**
     * @throws Exception
     */
    protected function _setDefaultPayment()
    {
        $quote = $this->getQuote();

        // Retrieve the previously saved data for addresses
        $data = Mage::helper('core')->jsonDecode($quote->getShippingData());
        $paymentData = (isset($data['payment']) && $data['payment']) ? $data['payment'] : null;
        if (!$paymentData) {
            throw new Exception($this->__('No shipping method was set.'));
        }

        if(is_array($paymentData)) {
            $payment['method'] = reset($paymentData);
        } else {
            $payment['method'] = $paymentData;
        }
        $checkout = $this->getCheckout();
        $payment['checks'] = Mage_Payment_Model_Method_Abstract::CHECK_USE_FOR_MULTISHIPPING
            | Mage_Payment_Model_Method_Abstract::CHECK_USE_FOR_COUNTRY
            | Mage_Payment_Model_Method_Abstract::CHECK_USE_FOR_CURRENCY
            | Mage_Payment_Model_Method_Abstract::CHECK_ORDER_TOTAL_MIN_MAX;
        $checkout->setPaymentMethod($payment);
    }

    /**
     * @param $data
     * @return array
     */
    protected function _validateSplitPaymentAndDelivery($data)
    {
        $errors = array();

        if (!isset($data['pakket']) || !is_array($data['payment'])) {
            return $errors;
        }

        foreach ($data['pakket'] as $packetId => $packetData) {
            if (empty($data['payment'][$packetId])) {
                $errors += array('payment[pakket][' . $packetId . '][type]' => $this->__('Please select a payment method'));
                continue;
            }
            if (Mage::getSingleton('customer/session')->getOrderEdit()) {
                $quote = Mage::getModel('sales/quote')->load(Mage::getSingleton('customer/session')->getOrderEdit());
                foreach ($quote->getAllItems() as $item) {
                    if ($item->getPackageId() == $packetId) {
                        $packetData['order_id'] = $item->getEditOrderId();
                        break;
                    }
                    continue;
                }
                $temp[$packetId] = Mage::helper('core')->jsonEncode($packetData);
            } else {
                $temp[$packetId] = Mage::helper('core')->jsonEncode($packetData);
            }

            foreach ($temp as $id => $newtemp) {
                if ($id !== $packetId && $newtemp == $temp[$packetId] && $data['payment'][$id] !== $data['payment'][$packetId]) {
                    $errors += array('payment[pakket][' . $packetId . '][type]' => $this->__('Payment methods should be the same for same address'));
                }
            }
        }

        return $errors;
    }

    /**
     * @param $data
     * @return array
     */
    protected function _validateNumberSelectionData($data)
    {
        $result = [];
        if (empty($data) || !is_array($data)) {
            return $result;
        }

        $usedNumbers = array();
        foreach ($data as $id => $number) {
            if ($id && !empty($number['mobile_number'])) {
                // Validate number is unique
                if (in_array($number['mobile_number'], $usedNumbers)) {
                    $result += array('numberselection[' . $id . '][mobile_number]' => $this->__('Number is already selected'));
                } else {
                    array_push($usedNumbers, $number['mobile_number']);
                }
                // Validate sim is correct format
                if (!empty($number['new_sim_number']) && !Mage::helper('omnius_validators/data')->validateSim($number['new_sim_number']) ) {
                    $result += array('numberselection[' . $id . '][new_sim_number]' => $this->__('Invalid simcard number'));
                }
            }
        }
        unset($usedNumbers);

        return $result;
    }

    /**
     * @param $quote
     * @return array
     */
    protected function _buildCreditCheckAddresses($quote)
    {
        // Retrieve the previously saved data for addresses
        $data = Mage::helper('core')->jsonDecode($quote->getShippingData());

        $addressArray = array();
        if (isset($data['deliver'])) { // Single delivery
            foreach ($quote->getPackages() as $packageId => $package) {
                $addressArray[$packageId] = $data['deliver']['address'];
            }
        } elseif (isset($data['pakket'])) { // Split delivery
            foreach ($quote->getPackages() as $packageId => $package) {
                $addressArray[$packageId] = $data['pakket'][$packageId]['address'];
            }
        }

        return $addressArray;
    }

    /**
     * Validate the id_number depending on the id_type
     *
     * @param $data
     * @return bool
     */
    protected function validateIdNumberByType($data)
    {
        $errors = array();
        if($data['is_business']) {
                if (!Mage::helper('omnius_validators/data')->validateIdType($data['contractant_id_type'], $data['contractant_id_number'], $data['contractant_issuing_country'])) {
                $errors['customer[contractant_id_number]'] = $this->checkoutHelper->__('Field has invalid data');
            }
        } else {
            if (!Mage::helper('omnius_validators/data')->validateIdType($data['id_type'], $data['id_number'], $data['issuing_country'])) {
                $errors['customer[id_number]'] = $this->checkoutHelper->__('Field has invalid data');
            }
        }

        return $errors;
    }

    /**
     * @param $orders
     * @param $data
     */
    protected function _updateNumberSelection($orders, $data)
    {
        foreach ($orders as $order) {
            $packages = $order->getPackages();
            foreach ($packages as $packageModel) {
                $packageId = $packageModel->getPackageId();
                if (!isset($data[$packageId])) {
                    continue;
                }
                if (isset($data[$packageId]['mobile_number'])) {
                    $defaultedMsisdn = $packageModel->getTelNumber();
                    $customerId = $order->getCustomerId();
                    $packageModel->setTelNumber($data[$packageId]['mobile_number']);
                    if ($defaultedMsisdn && $customerId) { // Only update the ctn when not in override mode
                        $write = Mage::getSingleton('core/resource')->getConnection('core_write');
                        $write->exec("UPDATE customer_ctn SET ctn = '" . $data[$packageId]['mobile_number'] . "' WHERE ctn = '" . $defaultedMsisdn . "' AND customer_id = '" . $customerId . "'");
                    }
                }
                if (isset($data[$packageId]['new_sim_number'])) {
                    $packageModel->setSimNumber($data[$packageId]['new_sim_number']);
                }
                if (isset($data[$packageId]['imei_number'])) {
                    $packageModel->setImei($data[$packageId]['imei_number']);
                }
                $packageModel->save();

                unset($data[$packageId]);
            }
        }
    }

    /**
     * @return array
     */
    protected function _getCustomerDataFields()
    {
        return array(
            'firstname' => array(
                'required' => true,
                'validation' => array(
                    'validateName'
                )
            ),
            'middlename' => array(
                'required' => false,
                'validation' => array(
                    'validateName'
                )
            ),
            'lastname' => array(
                'required' => true,
                'validation' => array(
                    'validateName'
                )
            ),
            'gender' => array(
                'required' => true,
                'validation' => array(
                    'validateIsNumber'
                )
            ),
            'dob' => array(
                'required' => true,
                'validation' => array(
                    'validateMaxAge',
                    'validateMinAge',
                    'validateIsDate'
                )
            ),
            'id_type' => array(
                'required' => true,
                'validation' => array()
            ),
            'id_number' => array(
                'required' => true,
                'validation' => array()
            ),
            'valid_until' => array(
                'required' => true,
                'validation' => array(
                    'validateFutureDate',
                    'validateIsDate'
                )
            ),
            'issuing_country' => array(
                'required' => true,
                'validation' => array(
                    'validateCountryCode',
                )
            ),
        );
    }

    /**
     * @return array
     */
    protected function _getBusinessDataFields()
    {
        return array(
            'company_vat_id' => array(
                'required' => false,
                'validation' => array(
                    'validateName'
                )
            ),
            'company_coc' => array(
                'required' => true,
                'validation' => array(
                    'validateName',
                    'validateCoc',
                )
            ),
            'company_name' => array(
                'required' => true,
                'validation' => array(
                    'validateName'
                )
            ),
            'company_date' => array(
                'required' => true,
                'validation' => array(
                    'validatePastDate',
                    'validateIsDate'
                )
            ),
            'company_legal_form' => array(
                'required' => true,
                'validation' => array(
                    'validateName'
                )
            ),

            'contractant_gender' => array(
                'required' => true,
                'validation' => array(
                    'validateIsNumber'
                )
            ),
            'contractant_firstname' => array(
                'required' => true,
                'validation' => array(
                    'validateName'
                )
            ),
            'contractant_middlename' => array(
                'required' => false,
                'validation' => array(
                    'validateName'
                )
            ),
            'contractant_lastname' => array(
                'required' => true,
                'validation' => array(
                    'validateName'
                )
            ),
            'contractant_dob' => array(
                'required' => true,
                'validation' => array(
                    'validateMaxAge',
                    'validateMinAge',
                    'validateIsDate'
                )
            ),
            'contractant_id_type' => array(
                'required' => true,
                'validation' => array(
                    'validateName',
                )
            ),
            'contractant_id_number' => array(
                'required' => true,
                'validation' => array()
            ),
            'contractant_valid_until' => array(
                'required' => true,
                'validation' => array(
                    'validateFutureDate',
                    'validateIsDate'
                )
            ),
            'contractant_issuing_country' => array(
                'required' => true,
                'validation' => array(
                    'validateCountryCode',
                )
            ),
        );
    }

    /**
     * @param $request
     * @return array
     * @throws Exception
     * @throws Mage_Customer_Exception
     */
    protected function _buildAndValidateCustomerData($request)
    {
        $customerData =
            $request->getPost('customer', array()) +
            $this->_convertPrivacy($request->getPost('privacy', array()));
        $addressData = $request->getPost('address', array());

        $genderArr = array('Mr.', 'Mrs.');

        //set prefix based on gender
        if (isset($customerData['gender']) && array_key_exists($customerData['gender']-1, $genderArr)) {
            $customerData['prefix'] = $genderArr[$customerData['gender'] - 1];
        }else{
            $customerData['prefix'] = '';
        }

        $this->hasSubscription = $request->getPost('hasSubscription');
        // Convert from frontend data to magento format
        $customerData['is_business'] = (int) isset($customerData['type']) && $customerData['type'] == Omnius_Customer_Model_Customer_Customer::TYPE_BUSINESS;

        // Check if the credit failed button was clicked based on type of customer
        if ($customerData['is_business']) {
            if (isset($customerData['business_suspect_fraud'])) {
                $customerData['suspect_fraud'] = $customerData['business_suspect_fraud'];
                unset($customerData['business_suspect_fraud']);
            }

            if (isset($customerData['contractant_gender']) && array_key_exists($customerData['contractant_gender']-1, $genderArr)) {
                $customerData['contractant_prefix'] = $genderArr[$customerData['contractant_gender'] - 1];
            }else{
                $customerData['contractant_prefix'] = '';
            }
        }

        $checkoutCart = Mage::getSingleton('checkout/cart');
        if (isset($customerData['suspect_fraud']) && $customerData['suspect_fraud']) {
            $checkoutCart->getQuote()->updateFields(array('suspect_fraud' => 1));
        } else {
            $checkoutCart->getQuote()->updateFields(array('suspect_fraud' => 0));
        }
        if (isset($customerData['dummy_email']) && $customerData['dummy_email']) {
            $checkoutCart->getQuote()->updateFields(array('dummy_email' => 1));
        } else {
            $checkoutCart->getQuote()->updateFields(array('dummy_email' => 0));
        }

        // Default country code is netherlands, unless a foreign address is chosen
        $customerData['country_id'] = Omnius_Checkout_Model_Sales_Order::DEFAULT_COUNTRY;
        if ($customerData['is_business']) {
            // Clear the private fields if business
            foreach ($this->_getCustomerDataFields() as $field => $data) {
                $customerData[$field] = null;
            }
            $customerData['company'] = $customerData['company_name'];
            $customerData['vat_id'] = $customerData['company_vat_id'];
            $customerData['firstname'] = $customerData['contractant_firstname'];
            $customerData['lastname'] = $customerData['contractant_lastname'];
            $customerData['middlename'] = $customerData['contractant_middlename'];
            $customerData['gender'] = isset($customerData['contractant_gender']) ? $customerData['contractant_gender'] : null;
            $customerData['dob'] = $customerData['contractant_dob'];
        } else {
            // Clear the business fields if private
            foreach ($this->_getBusinessDataFields() as $field => $data) {
                $customerData[$field] = null;
            }

            $customerData['company_address'] = null;
            $customerData['foreignAddress'] = null;
            $customerData['otherAddress'] = null;
        }

        $additionalData = $request->getPost('additional');
        $customerData['additional_email'] = join(';', array_filter($additionalData['email'] ?: [], 'strlen'));
        $customerData['additional_fax'] = isset($additionalData['fax']) ? join(';', array_filter($additionalData['fax'], 'strlen')) : '';
        $customerData['additional_telephone'] = isset($additionalData['telephone']) ? join(';', array_filter($additionalData['telephone'], 'strlen')) : '';

        // By default set the shipping address same as billing
        $customerData['use_for_shipping'] = 1;

        $errors = $this->_validateCustomerAdditionaldata($additionalData);
        $errors += $this->_validateCustomerData($customerData);
        $errors += $this->validationsHelper->_validateBillingAddressData($addressData + $customerData);
        if ($errors) {
            throw new Mage_Customer_Exception(Mage::helper('core')->jsonEncode($errors));
        }
        if ($customerData['is_business']) {
            $customerData += $this->validationsHelper->_getBusinessAddressByType($customerData);
        }
        $addressData += $this->checkoutHelper->_getAddressByType($addressData);

        $addressData['street'] = is_array($addressData['street']) ? $addressData['street'] : array($addressData['street']);

        if (isset($addressData['houseno'])){
            array_push($addressData['street'], $addressData['houseno']);
        }
        if (isset($addressData['addition'])){
            array_push($addressData['street'], $addressData['addition']);
        }

        if (isset($addressData['extra_street'])) {
            foreach ($addressData['extra_street'] as $streetLine) {
                array_push($addressData['street'], $streetLine);
            }
        }

        return $addressData + $customerData;
    }

    /**
     * @throws Mage_Customer_Exception
     *
     * Validate porting contract end date
     */
    public function validateDateAction()
    {
        $result = array();
        try{
            $date = $this->getRequest()->getParam('date');
            $type = $this->getRequest()->getParam('type');
            $customerType = $this->getRequest()->getParam('customerType');
            $currentOperator = $this->getRequest()->getParam('current_operator');
            $currentProvider = $this->getRequest()->getParam('current_provider');
            $packageId = $this->getRequest()->getParam('packageId');
            $errors = $this->_validatePortabilityData(array(), $date, $type, $packageId, $currentOperator, $currentProvider, $customerType);

            //only return the date error
            foreach($errors as $key => $error){
                if(strpos($key, 'end_date_contract') === FALSE) {
                    unset($errors[$key]);
                } else {
                    $result['field'] = $this->getRequest()->getParam('field');
                    $result['field_error'] = $error;
                }
            }

            if ($errors) {
                throw new Mage_Customer_Exception(reset($errors));
            }
        } catch (Exception $e) {
            // Set fail response for validations
            $result['error'] = true;
            $result['message'] = $this->checkoutHelper->__("Please correct the following errors");
            $result['field_error'] = $e->getMessage();
            $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                Mage::helper('core')->jsonEncode($result)
            );
            return;
        }
    }

    /**
     * @param $quote
     * @param int $superOrderId
     */
    protected function _createNewQuoteOnServiceCallFail($quote, $superOrderId)
    {
        $newQuote = Mage::helper('omnius_checkout')->createNewCartQuote($quote, $superOrderId);
        if(!$newQuote) {
            return false;
        }
        Mage::getSingleton('checkout/cart')
            ->setQuote($newQuote)
            ->save();
        $this->_getCheckoutSession()->setQuoteId($newQuote->getId());
        $this->_getCustomerSession()->setFailedSuperOrderId($superOrderId);
    }

    /**
     * @param $packageData
     * @throws Exception
     */
    protected function _validateRefundData($packageData)
    {
        if(!isset($packageData['return_date']) || !Mage::helper('omnius_validators/data')->validateIsDate($packageData['return_date'])) {
            throw new Exception($this->__('The value is not a valid date format.'));
        }
    }

    /**
     * @param $isBusiness
     * @return array
     */
    protected function validateNpDate($isBusiness)
    {
        return array(
            'mobile_number' => array(
                'required' => true,
                'validation' => array(
                    'validateIsNumber',
                )
            ),
            'sim' => array(
                'required' => true,
            ),
            'contract' => array(
                'required' => true,
                'validation' => array(
                    'validateIsNumber'
                )
            ),
            'current_provider' => array(
                'required' => true,
                'validation' => array(
                    'validateNetworkProvider'
                )
            ),
            'current_operator' => array(
                'required' => true,
                'validation' => array(
                    'validateNetworkOperator'
                )
            ),
            'type' => array(
                'required' => true,
                'validation' => array(
                    'validateName',
                )
            ),
            'end_date_contract' => array(
                'required' => true,
                'validation' => array(
                    'validateIsDate',
                    $isBusiness ? 'validateMaxPortingDaysB' : 'validateMaxPortingDaysC',
                    'validateMinPortingDays',
                    'validateHolidayDate',
                    'validateWorkingDay',
                ),
                'message' => array(
                    'validateIsDate' => $this->checkoutHelper->__('The value is not a valid date format.'),
                    'validateWorkingDay' => $this->checkoutHelper->__('The date is in a weekend day.'),
                    $isBusiness ? 'validateMaxPortingDaysB' : 'validateMaxPortingDaysC' => $this->checkoutHelper->__(sprintf('The date can be up to %s days in the future.',
                        $isBusiness ? '120' : '60')),
                    'validateMinPortingDays' => $this->checkoutHelper->__('The date must be at least 5 working days in the future.'),
                    'validateHolidayDate' => $this->checkoutHelper->__('The date is in a holiday.')
                )
            ),
        );
    }

    /**
     * @param $data
     * @param int $statusCode
     * @throws InvalidArgumentException
     */
    protected function jsonResponse($data, $statusCode = 200)
    {
        $response = $this->getCoreHelper()
            ->jsonResponse($data, $statusCode);

        $this->getResponse()
            ->setBody($response['body'])
            ->setHeader('Content-Type', 'application/json')
            ->setHttpResponseCode($response['status']);
    }
}
