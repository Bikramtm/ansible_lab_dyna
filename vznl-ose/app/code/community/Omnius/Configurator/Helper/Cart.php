<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Cart
 */
class Omnius_Configurator_Helper_Cart extends Mage_Core_Helper_Abstract
{

    protected $_collectionsCache = array();

    /** @var Dyna_Cache_Model_Cache */
    protected $_cache;

    /**
     * Retrieves existing cart package items based on type and package id.
     * @param $type
     * @param null $packageId
     * @return array
     */
    public function getCartItems($type, $packageId = null)
    {
        $ids = array();
        if ( ! $packageId) {
            return $ids;
        }
        /** @var Mage_Sales_Model_Quote_Item $item */
        foreach (Mage::getSingleton('checkout/cart')->getQuote()->getItemsCollection() as $item) {
            if ($item->getPackageId() != $packageId || $item->isDeleted()) {
                continue;
            }

            if ($type == $item->getPackageType()) {
                $ids[strtolower(current($item->getProduct()->getType()))][] = (int)$item->getProductId();
                $ids[strtolower(current($item->getProduct()->getType()))] = array_values(array_unique($ids[strtolower(current($item->getProduct()->getType()))]));
            }
        }

        return $ids;
    }

    /**
     * @param $type
     * @param $saleType
     * @param null $ctn
     * @param null $currentProducts
     * @param array $oneOffDealOptions
     * @return mixed
     */
    public function initNewPackage($type, $saleType, $ctn = null, $currentProducts = null, $oneOffDealOptions = array())
    {
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $quoteId = $quote->getId();
        $newPackageId = Omnius_Configurator_Model_CartObserver::getNewPackageId($quote);

        if (!$quoteId) {
            $quote->setCustomerId(Mage::getSingleton('customer/session')->getId());
        }

        $quote->setActivePackageId($newPackageId);
        $quote->save();

        if (!$quoteId) {
            Mage::getSingleton('checkout/cart')->getCheckoutSession()->setQuoteId($quote->getId());
        }
        // Create the new package for the quote or update if already created
        /** @var Omnius_Package_Model_Package $packageModel */
        $packageModel = Mage::getModel('package/package')->getCollection()
            ->addFieldToFilter('quote_id', $quote->getId())
            ->addFieldToFilter('package_id', $newPackageId)
            ->getFirstItem();

        if (!$packageModel->getId()) {
            $packageModel->setQuoteId($this->_getQuote()->getId())
                ->setPackageId($newPackageId)
                ->setType($type)
                ->setSaleType($saleType)
                ->setCtn($ctn)
                ->setCurrentProducts($currentProducts);
            if ($ctn && strtolower($saleType) == strtolower(Omnius_Checkout_Model_Sales_Quote_Item::RETENTION)) {
                // RFC-160003: When a retention package is created, also save the ctn data for the contract
                $ctnModel = Mage::getResourceModel('ctn/ctn_collection')->addFieldToFilter('customer_id', Mage::getSingleton('customer/session')->getId())->addFieldToFilter('ctn', $ctn)->getFirstItem();
                if ($ctnModel->getId()) {
                    $packageModel
                        ->setCtnOriginalStartDate($ctnModel->getStartDate())
                        ->setCtnOriginalEndDate($ctnModel->getEndDate());
                }
            }
        }

        if (!empty($oneOffDealOptions)) {
            $packageModel->setOneOfDeal(true);
            $quote->setItemsQty(count(explode(',', $ctn)));
            $packageModel->setRetainable($oneOffDealOptions['retainable']);
        }
        $packageModel->save();

        return $newPackageId;
    }

    /**
     * @param $productIds
     * @param array $allProducts
     * @param null $websiteId
     * @param bool $cacheCollections
     * @param null $packageType
     * @param bool $skipServiceRules
     * @return array|mixed
     */
    public function getCartRules($productIds, $allProducts = array(), $websiteId = null, $cacheCollections = false, $packageType = null, $skipServiceRules = false)
    {
        $websiteId = $websiteId ?: Mage::app()->getStore()->getWebsiteId();
        $key = sprintf('get_cart_new_rules_%s_%s_%s', md5(serialize($productIds)), md5(serialize($allProducts)), $websiteId);
        if (!($result = unserialize($this->getCache()->load($key)))) {
            if (count($productIds)) {
                $adapter = Mage::getSingleton('core/resource')->getConnection('core_read');
                $available = [];

                $storeId = $websiteId ? Mage::app()->getWebsite($websiteId)->getDefaultStore()->getId() : Mage::app()->getStore()->getId();
                $catalogResource = Mage::getResourceModel('catalog/product');
                $attribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', Omnius_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR);
                $typeDevice = Mage::helper('omnius_catalog')->getAttributeAdminLabel($attribute, null, Omnius_Catalog_Model_Type::SUBTYPE_DEVICE);
                $typeSubscription = Mage::helper('omnius_catalog')->getAttributeAdminLabel($attribute, null, Omnius_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION);
                $rawAttributesValues = [];

                if ($cacheCollections) {
                    if (!isset($this->_collectionsCache['device'])) {
                        $this->_collectionsCache['device'] = $adapter->fetchCol(Mage::getModel('catalog/product')->getCollection()
                            ->addAttributeToFilter(Omnius_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR, $typeDevice)->getSelectSql(true));
                    }
                    if (!isset($this->_collectionsCache['sub'])) {
                        $this->_collectionsCache['sub']  = $adapter->fetchCol(Mage::getModel('catalog/product')->getCollection()
                            ->addAttributeToFilter(Omnius_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR, $typeSubscription)->getSelectSql(true));
                    }

                    $deviceCollection = $this->_collectionsCache['device'];
                    $subsCollection = $this->_collectionsCache['sub'];
                } else {
                    $deviceCollection = $adapter->fetchCol(Mage::getModel('catalog/product')->getCollection()
                        ->addAttributeToFilter(Omnius_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR, $typeDevice)->getSelectSql(true));
                    $subsCollection = $adapter->fetchCol(Mage::getModel('catalog/product')->getCollection()
                        ->addAttributeToFilter(Omnius_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR, $typeSubscription)->getSelectSql(true));
                }

                foreach ($productIds as $id) {
                    $available[] = $this->getRulesForId($websiteId, $id, $catalogResource, $storeId, $rawAttributesValues, $attribute, $deviceCollection, $subsCollection);
                }
                $result_new = array_pop($available);
                foreach ($available as $items) {
                    $result_new = array_intersect($result_new, $items);
                }

                $result = array_values($result_new);
            } else {
                $result = $this->getAllRulesForProducts($allProducts, $websiteId);
            }

            $this->getCache()->save(serialize($result), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
        }

        return $result;
    }

    /**
     * Retrieves all available product ids based on the website id.
     * @param null $websiteId
     * @return mixed
     */
    public function getAllRules($websiteId = null)
    {
        $readConnection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $websiteId = $websiteId ? $websiteId : Mage::app()->getWebsite()->getId();
        $key = sprintf('product_all_rules_%s', $websiteId);

        if (!($result = unserialize($this->getCache()->load($key)))) {
            $result = $readConnection
                ->fetchCol('SELECT product_id FROM catalog_product_website WHERE website_id = :id',
                    array(
                        ':id' => $websiteId
                    )
                );
            $this->getCache()->save(serialize($result), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
        }

        return $result;
    }

    /**
     * @return Omnius_Checkout_Model_Sales_Quote
     */
    protected function _getQuote()
    {
        return Mage::getSingleton('checkout/cart')->getQuote();
    }

    /**
     * @return Dyna_Cache_Model_Cache
     */
    protected function getCache()
    {
        if (!$this->_cache) {
            $this->_cache = Mage::getSingleton('dyna_cache/cache');
        }

        return $this->_cache;
    }

    /**
     * Regenerates the package ids in case of a package removal.
     * @param $quote
     * @param null $packageId
     * @return array
     */
    public function reindexPackagesIds($quote, $packageId = null)
    {
        $quoteItems = Mage::getModel('sales/quote_item')->getCollection()
            ->setQuote($quote)
            ->addOrder('package_id', 'asc');
        $packageModels = Mage::getModel('package/package')->getCollection()
            ->addFieldToFilter('quote_id', $quote->getId())
            ->addOrder('package_id', 'asc');

        $counter = 0;
        $newPackages = array();

        foreach ($packageModels as $packageInfo) {
            $counter++;
            $p_id = $packageInfo['package_id'];

            if ($p_id != $counter) {
                $packageInfo->setPackageId($counter)->save();
            }
            $newPackages[$p_id] = $counter;
        }

        $newActivePackage = $packageId ? (isset($newPackages[$packageId]) ? $newPackages[$packageId] : 1) : 1;

        foreach ($quoteItems as $quoteItem) {
            $itemPackageID = $quoteItem->getPackageId();
            if ($itemPackageID != $newPackages[$itemPackageID]) {
                $quoteItem->setPackageId($newPackages[$itemPackageID])->save();
            }
        }

        $quote->saveActivePackageId($newActivePackage);
        $quote->refreshItemsCollection();

        return $newPackages;
    }

    /**
     * Checks if an one-of-deal package exists in the cart
     *
     * @return bool
     */
    public function isOneOfDealActive()
    {
        $packages = Mage::getModel('package/package')->getCollection()
            ->addFieldToFilter('quote_id', $this->_getQuote()->getId())
            ->addFieldToFilter('one_of_deal', array('neq' => null));
        foreach ($packages as $package) {
            if ($package->getOneOfDeal()) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param $quote
     * @return array
     */
    public function checkSimForPackages($quote)
    {
        $packagesWithoutSim = array();
        $packages = Mage::getModel('package/package')->getPackages(null, $quote->getId());
        /** @var Omnius_Package_Model_Package $package */
        foreach ($packages as $package) {
            if ($package->isCompleted() && ($package->hasOldSim() || $package->hasSim($quote))) {
                continue;
            } else {
                if (!$package->hasSubscription($quote)) {
                    continue;
                }

                $packagesWithoutSim[] = $package->getPackageId();
            }
        }

        return $packagesWithoutSim;
    }

    /**
     * Retrieves all product match rules in which the product is available.
     * @param $id
     * @return mixed
     */
    protected function getRuleById($id, $websiteId = null)
    {
        $websiteId = $websiteId ?: Mage::app()->getStore()->getWebsiteId();
        $adapter = Mage::getSingleton('core/resource')->getConnection('core_read');
        $key = sprintf('%s_product_mix_match_white_rules_%s', $id, $websiteId);
        if ($result = $this->getCache()->load($key)) {
            return unserialize($result);
        } else {
            $result = $adapter->fetchAll(Mage::getResourceModel('productmatchrule/matchRule_collection')
                ->addFieldToSelect(array('source_product_id', 'target_product_id'))
                ->addFieldToFilter('website_id', $websiteId)
                ->addFieldToFilter(
                    array('source_product_id', 'target_product_id'),
                    array(
                        array('eq' => $id),
                        array('eq' => $id)
                    )
                )->getSelectSql(true));

            $this->getCache()->save(serialize($result), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());

            return $result;
        }
    }

    /**
     * @param $productIds
     * @return array
     */
    public function getMandatory($productIds, $websiteId = null)
    {
        $websiteId = $websiteId ?: Mage::app()->getStore()->getWebsiteId();
        $key = sprintf('%s_product_mandatory_addons__%s', md5(serialize($productIds)), $websiteId);
        if ($result = $this->getCache()->load($key)) {
            return unserialize($result);
        } else {
            $rules = $this->getCartRules($productIds);
            $mandatory = [];
            foreach ($productIds as $prod) {
                // Retrieve all the mandatory families for the product
                $mandatoryFamilies = Mage::getModel('productmatchrule/mandatory')->getMandatoryFamilies($prod, $websiteId);
                foreach ($mandatoryFamilies as $family) {
                    // If the family was already parsed, skip it
                    if (isset($mandatory[$family])) {
                        continue;
                    }

                    // Ensure there are products in this family compatible with the rest of the products
                    $collectionIds = Mage::getModel('catalog/category')->getProductIds($family);
                    $collectionIds = $collectionIds ? $collectionIds : [];
                    if (count(array_intersect($rules, $collectionIds)) > 0) {
                        $mandatory[$family] = $family;
                    }
                }
            }

            $result = array_keys($mandatory);

            return $result;
        }
    }

    /**
     * @param $allProducts
     * @param $websiteId
     * @return array|mixed
     */
    protected function getAllRulesForProducts($allProducts, $websiteId)
    {
        // If no products exists, return all product ids
        $prodIds = [];
        if (is_array($allProducts) && count($allProducts)) {
            foreach ($allProducts as $typeProdIds) {
                foreach ($typeProdIds as $prod) {
                    if (isset($prod['entity_id'])) {
                        $prodIds[] = $prod['entity_id'];
                    }
                }
            }
        } else {
            $prodIds = $this->getAllRules($websiteId);
        }

        return $prodIds;
    }

    /**
     * @param $websiteId
     * @param $id
     * @param $catalogResource
     * @param $storeId
     * @param $rawAttributesValues
     * @param $attribute
     * @param $deviceCollection
     * @param $subsCollection
     * @return array
     */
    protected function getRulesForId($websiteId, $id, $catalogResource, $storeId, &$rawAttributesValues, $attribute, $deviceCollection, $subsCollection)
    {
        $rulesSource = $this->getRuleById($id, $websiteId);

        /**
         * When choosing a device or subscription, normally the application would hide the rest of the devices or subscriptions
         * if there is no allowed rule between them. However, for devices and subscriptions, we must allow users to switch
         * between the allowed ones without hiding the other options.
         */
        $valueId = $catalogResource->getAttributeRawValue($id, Omnius_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR, $storeId);
        if (isset($rawAttributesValues[$valueId])) {
            $productType = $rawAttributesValues[$valueId];
        } else {
            $productType = json_decode(Mage::helper('omnius_catalog')->getAttributeAdminLabel($attribute, $valueId), true);
            $rawAttributesValues[$valueId] = $productType;
        }

        $similarProductIds = array();

        //if device, also retrieve all other devices to simulate that subscriptions are allowed between themselves
        if (in_array(Omnius_Catalog_Model_Type::SUBTYPE_DEVICE, $productType)) {
            $similarProductIds = $deviceCollection;
        }

        //if subscription, also retrieve all other subscriptions to simulate that subscriptions are allowed between themselves
        if (in_array(Omnius_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION, $productType)) {
            $similarProductIds = array_merge($similarProductIds, $subsCollection);
        }

        $targetProductIds = [];
        $sourceProductIds = [];
        foreach ($rulesSource as $row) {
            $targetProductIds[$row['target_product_id']] = $row['target_product_id'];
            $sourceProductIds[$row['source_product_id']] = $row['source_product_id'];
        }

        $available = array_unique(
            array_merge(
                $similarProductIds,
                $targetProductIds,
                $sourceProductIds
            )
        );

        return $available;
    }
}
