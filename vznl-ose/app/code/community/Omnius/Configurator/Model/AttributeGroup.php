<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Configurator_Model_AttributeGroup
 */
class Omnius_Configurator_Model_AttributeGroup
{
    const ATTR_GROUP_PROMOTIONS = 'promotion';
    const ATTR_GROUP_DATA_SUBSCRIPTIONS = 'data_subscription';
    const ATTR_GROUP_VOICE_SUBSCRIPTIONS = 'voice_subscription';
    const ATTR_SERVICE_ITEM = 'service_item';
}