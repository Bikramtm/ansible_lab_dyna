<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Configurator_Model_Catalog
 */
class Omnius_Configurator_Model_Catalog extends Mage_Catalog_Model_Resource_Product
{
    /** @var Omnius_Configurator_Helper_Attribute */
    protected $_attrHelper;

    /** @var Varien_Object_Cache */
    protected $_cache;

    protected $disabledKeys = array(
        'sku',
        'thumbnail',
        'image',
        'entity_id',
        'name',
        'rel_promotional_maf',
        'rel_promotional_maf_with_tax',
        'rel_regular_maf',
        'rel_regular_maf_with_tax',
        'regular_maf',
        'regular_maf_with_tax',
        'price',
        'price_with_tax',
        'identifier_simcard_formfactor',
        'identifier_simcard_type',
        'identifier_simcard_allowed',
        'sim_type',
        'maf',
        'group_type',
        'is_discontinued',
        'sim_only',
        'prodspecs_afmetingen_unit_value',
        'prodspecs_besturingssysteem_value',
        'business_product',
        'include_btw'
    );

    /**
     * @param $packageSubType
     * @param string $type
     * @param $websiteId
     * @param $packageType
     * @return Mage_Catalog_Model_Resource_Product_Collection
     */
    public function getProductCollection($packageSubType, $type, $websiteId, $packageType, $consumerType, $filters = [])
    {
        $attribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', Omnius_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR);
        $subTypeValue = Mage::helper('omnius_catalog')->getAttributeAdminLabel($attribute, null, $packageSubType);

        //if website_id is provided in request param, overwrite it
        $websiteId = Mage::app()->getRequest()->getParam('website_id', $websiteId);
        /** @var Omnius_Catalog_Model_Resource_Product_Collection $collection */
        $collection = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToSelect('*')
            ->setStoreId(Mage::app()->getStore())
            ->addWebsiteFilter($websiteId)
            ->addTaxPercents()
            ->addPriceData(null, $websiteId)
            ->addAttributeToFilter('type_id', $type)
            ->addAttributeToFilter('is_deleted', array('neq' => 1))
            ->addAttributeToFilter(Omnius_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR, $subTypeValue)
        ;

        if (count($filters)) {
            $collection = $this->applyFilters($collection, $filters);
        }

        // Don't get the price for mixmatch as we also need disabled products
        if (!Mage::registry('mix_match_with_disabled_products')) {
            $collection
                ->addTaxPercents()
                ->addPriceData(null, $websiteId);
        }
        $collection->addAttributeToSort('priority', Mage_Eav_Model_Entity_Collection_Abstract::SORT_ORDER_DESC);
        if ( $consumerType !== 0 ) {
            $consValue = Mage::helper('omnius_configurator/attribute')->getSubscriptionIdentifier($consumerType);
            $collection->addAttributeToFilter(Omnius_Catalog_Model_Product::CATALOG_CONSUMER_TYPE, array('finset' => $consValue));
        }

        if ($packageType) {
            $attribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', Omnius_Catalog_Model_Product::CATALOG_PACKAGE_TYPE_ATTR);
            $typeValue = Mage::helper('omnius_catalog')->getAttributeAdminLabel($attribute, null, $packageType);
            $collection->addAttributeToFilter(Omnius_Catalog_Model_Product::CATALOG_PACKAGE_TYPE_ATTR, array("finset" => $typeValue));
        }

        /**
         * Apply agent groups visibility filters
         */

        // transform "customertype" attribute values to boolean
        $customerTypeAttribute = Mage::getResourceModel('catalog/product')->getAttribute('customertype');
        $customerTypeOptions = $customerTypeAttribute->getSource()->getAllOptions(true, true);
        $customerTypes = array();
        foreach ($customerTypeOptions as $key => $ct) {
            if ($ct['value']) {
                $customerTypes[$ct['value']] = ($ct['label'] == 'Business') ? true : false;
            }
        }

        $showPriceWithBtw = Mage::getSingleton('customer/session')->showPriceWithBtw();
        $isBusiness = $this->getCustomer() instanceof Mage_Customer_Model_Customer && $this->getCustomer()->getIsBusiness();

        /** @var Omnius_Catalog_Model_Product $item */
        foreach ($collection as $item) {
            /**
             * Save if customer is business or not
             */
            $item->setData('is_business', $isBusiness);

            $item->setData('maf', $item->getMaf());

            $item->setData('group_type', $item->getType());

            $item->updateSimType();

            $item->setData('sim_only', $item->getSimOnly());
            $item->setData('prodspecs_afmetingen_unit_value', $item->getUnitValue());
            $item->setData('prodspecs_besturingssysteem_value', $item->getOperatingSystem());
            $customerTypeAttributeId = $item->getData('customertype');

            $item->setData('business_product', isset($customerTypes[$customerTypeAttributeId]) ? $customerTypes[$customerTypeAttributeId] : false);
            $item->setData('include_btw', $showPriceWithBtw);

            $item->setData(
                Omnius_Catalog_Model_Product::CATALOG_CONSUMER_TYPE,
                $item->getData(Omnius_Catalog_Model_Product::CATALOG_CONSUMER_TYPE) ? explode(',', $item->getData(Omnius_Catalog_Model_Product::CATALOG_CONSUMER_TYPE)) : array()
            );

            $item->escapeData();
            /**
             * Prepare prices
             */
            $this->preparePrice($item);
        }

        return $collection;
    }

    /**
     * @param $packageType
     * @return mixed|Varien_Data_Collection
     *
     * Get all items for a certain package type (mobile, prepaid, etc)
     * This relies on the "identifier_package_type" attribute meaning that only products that have this attribute set will be retrieved
     */
    public function getAllItems($packageType)
    {
        $dealerGroups = Mage::helper('agent')->getCurrentDealerGroups();
        sort($dealerGroups);
        $key = serialize(array(strtolower(__METHOD__), $packageType, implode(',',$dealerGroups)));
        if ($result = unserialize($this->getCache()->load($key))) {
            return $result;
        } else {
            $attribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', Omnius_Catalog_Model_Product::CATALOG_PACKAGE_TYPE_ATTR);
            $type = Mage::helper('omnius_catalog')->getAttributeAdminLabel($attribute, null, $packageType);
            /** @var Mage_Catalog_Model_Resource_Product_Collection $collection */
            $collection = Mage::getResourceModel('catalog/product_collection')
                ->setStoreId(Mage::app()->getStore())
                ->addWebsiteFilter()
                ->addAttributeToSelect('*')
                ->addMinimalPrice()
                ->addFinalPrice()
                ->addTaxPercents()
                ->addAttributeToFilter(Omnius_Catalog_Model_Product::CATALOG_PACKAGE_TYPE_ATTR, $type)
                ->load();

            $result = new Varien_Data_Collection();
            foreach ($collection->getItems() as $item) {
                $result->addItem($item);
            }
            unset($collection);

            $this->getCache()->save(serialize($result), $key, array(Dyna_Cache_Model_Cache::PRODUCT_TAG), $this->getCache()->getTtl());

            return $result;
        }
    }

    /**
     * @param Mage_Catalog_Model_Resource_Product_Collection $collection
     * @param array $filters
     * @return Omnius_Catalog_Model_Resource_Product_Collection
     */
    public function applyFilters(Mage_Catalog_Model_Resource_Product_Collection $collection, array $filters = array())
    {
        foreach ($filters as $attribute => $rule) {
            if (is_numeric($attribute)) {
                $collection->addAttributeToFilter($rule);
            } else {
                $collection->addAttributeToFilter($attribute, $rule);
            }
        }
        return $collection;
    }

    /**
     * Applies tax on the prices if the current customer is business
     *
     * @param Mage_Catalog_Model_Product $product
     */
    protected function preparePrice(Mage_Catalog_Model_Product $product)
    {
        $_taxHelper = Mage::helper('tax');
        $_store = $product->getStore();

        if (Mage::helper('omnius_catalog')->is(array(
            Omnius_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION,
            Omnius_Catalog_Model_Type::SUBTYPE_ADDON
        ), $product)
        ) {
            $priceValues = array(
                'promotional_maf',
                'regular_maf',
            );
            foreach ($priceValues as $priceValue) {
                if ($product->hasData($priceValue)) {
                    $_convertedPrice = $_store->roundPrice($_store->convertPrice($product->getData($priceValue)));
                    $priceWithTax = $_taxHelper->getPrice($product, $_convertedPrice, true);
                    $price = $_taxHelper->getPrice($product, $_convertedPrice, false);
                    /**
                     * Save both prices (with and without tax)
                     */
                    $product->setData(sprintf('%s_with_tax', $priceValue), $priceWithTax);
                    $product->setData($priceValue, $price);
                }
            }
        }

        /**
         * Save both prices (with and without tax)
         */
        $_convertedFinalPrice = $_store->roundPrice($_store->convertPrice($product->getPrice()));
        $priceWithTax = $_taxHelper->getPrice($product, $_convertedFinalPrice, true);
        $price = $_taxHelper->getPrice($product, $_convertedFinalPrice, false);
        $product->setPriceWithTax($priceWithTax);
        $product->setPrice($price);
    }

    /**
     * @return Omnius_Configurator_Helper_Attribute
     */
    protected function getAttrHelper()
    {
        if ( ! $this->_attrHelper) {
            $this->_attrHelper = Mage::helper('omnius_configurator/attribute');
        }

        return $this->_attrHelper;
    }

    /**
     * @return Mage_Customer_Model_Customer
     */
    protected function getCustomer()
    {
        return Mage::getSingleton('customer/session')->getCustomer();
    }

    /**
     * @return Dyna_Cache_Model_Cache
     */
    protected function getCache()
    {
        if ( ! $this->_cache) {
            $this->_cache = Mage::getSingleton('dyna_cache/cache');
        }
        return $this->_cache;
    }

    /**
     * @param $packageType
     *
     * Get available configuration options based on package type
     * @return array
     */
    public static function getOptionsForPackageType($packageType)
    {
        switch ($packageType) {
            case strtolower(Omnius_Catalog_Model_Type::TYPE_MOBILE):
                $result = array(
                    Omnius_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION,
                    Omnius_Catalog_Model_Type::SUBTYPE_DEVICE,
                    Omnius_Catalog_Model_Type::SUBTYPE_ADDON,
                    Omnius_Catalog_Model_Type::SUBTYPE_ACCESSORY
                );
                break;
            default:
                // do nothing
                $result = [];
                break;
        }

        return $result;
    }

    /**
     * @param $websiteCode
     * @return array
     *
     * Define possible device type restrictions for certain website
     */
    public static function getWebsiteRestrictions($websiteCode)
    {
        $restrictions = array();
        return $restrictions;
    }

    /**
     * @param $productType
     * @param null $websiteId
     * @param array $filters
     * @return Mage_Eav_Model_Entity_Collection_Abstract|mixed|Varien_Data_Collection
     *
     * Get list of products of type device, subscription, addon or accessory
     */
    public function getProductsOfType($productType, $websiteId = null, array $filters = array(), $packageType = null, $consumerType = 0)
    {
        $dealerGroups = Mage::helper('agent')->getCurrentDealerGroups();
        sort($dealerGroups);
        $key = serialize(array($packageType, $filters, $productType, $type = Mage_Catalog_Model_Product_Type::TYPE_SIMPLE, $websiteId, implode(',',$dealerGroups), $consumerType));
        if (!($products = unserialize($this->getCache()->load($key)))) {
            $attribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', Omnius_Catalog_Model_Product::CATALOG_PACKAGE_TYPE_ATTR);
            $type = Mage::helper('omnius_catalog')->getAttributeAdminLabel($attribute, null, $packageType);

            $collection = $this->getProductCollection($productType, Mage_Catalog_Model_Product_Type::TYPE_SIMPLE, $websiteId, $packageType, $consumerType, $filters)->load();

            $products = new Varien_Data_Collection();
            //only return the following properties to avoid large json objects with properties that are not used
            $keys = array('sku', 'thumbnail','image','entity_id', 'name','stock_status_class','stock_status_text','stock_status','rel_promotional_maf','rel_promotional_maf_with_tax',
            'rel_regular_maf','rel_regular_maf_with_tax','regular_maf','regular_maf_with_tax','price','price_with_tax', 'identifier_simcard_formfactor',
            'identifier_simcard_type','identifier_simcard_allowed','sim_type','maf','group_type','is_discontinued','sim_only',
            'prodspecs_afmetingen_unit_value','prodspecs_besturingssysteem_value','business_product', 'include_btw');
            $keys = array_unique(array_merge($keys, $this->getFilterableAttributeCodes()));
            $attrHelper = Mage::helper('omnius_configurator/attribute');
            foreach ($collection->getItems() as $item) {
                $data = $this->parseProductDataForType($item, $attrHelper, $keys);
                if ($item->is(Omnius_Catalog_Model_Type::SUBTYPE_ADDON)) {
                    $mandatoryCategories = Mage::helper('omnius_configurator')->getMandatoryCategories();
                    $mandatory = array_intersect($item->getCategoryIds(), $mandatoryCategories);
                    $data['families'] = array_values($mandatory);
                }
                $item->setData($data);
                try {
                    $item->setData('thumbnail', (string)Mage::helper('catalog/image')->init($item, 'thumbnail')->resize(30));
                } catch (Exception $e) {
                    Mage::log(sprintf('Assigned image for product %s not found on disk.', $item->getSku()));
                }
                $products->addItem($item);
            }

            $this->getCache()->save(serialize($products), $key, array(Dyna_Cache_Model_Cache::PRODUCT_TAG), $this->getCache()->getTtl());
        }

        return $products;
    }

    /**
     * Return section restrictions for order
     * @param string $websiteCode
     * @return array
     */
    public static function getOrderRestrictions($websiteCode)
    {
        // normal restrictions otherwise
        return static::getWebsiteRestrictions($websiteCode);
    }

    /**
     * @return array|mixed
     *
     * Get a list of filterable attribute codes
     */
    public function getFilterableAttributeCodes()
    {
        $key = __METHOD__;
        if ($filterable = unserialize($this->getCache()->load($key))) {
            return $filterable;
        } else {
            $collection = Mage::getResourceModel('catalog/product_attribute_collection')
                ->addFieldToSelect('attribute_code')
                ->addFieldToFilter('is_filterable', true);

            $filterable = Mage::getSingleton('core/resource')
                ->getConnection('core_read')
                ->fetchCol($collection->getSelect()->reset('COLUMNS'));

            $this->getCache()->save(serialize($filterable), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());

            return $filterable;
        }
    }

    /**
     * Parse attributes for disabled products.
     * @param $productIds
     * @return array
     */
    public function parseAttributesDisabled($productIds)
    {
        $collection = array();

        $keys = $this->disabledKeys;
        $keys = array_unique(array_merge($keys, Mage::getModel('omnius_configurator/catalog')->getFilterableAttributeCodes()));

        $products = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToSelect('*')
            ->setStoreId(Mage::app()->getWebsite()->getDefaultStore()->getId())
            ->addTaxPercents()
            ->addAttributeToSelect('price')
            ->addAttributeToFilter('status', array('in' => [Mage_Catalog_Model_Product_Status::STATUS_DISABLED, Mage_Catalog_Model_Product_Status::STATUS_ENABLED]))
            ->addAttributeToFilter('entity_id', array('in' => $productIds));

        // transform "customertype" attribute values to boolean
        $customerTypeAttribute = Mage::getResourceModel('catalog/product')->getAttribute('customertype');
        $customerTypeOptions = $customerTypeAttribute->getSource()->getAllOptions(true, true);
        $customerTypes = array();
        foreach ($customerTypeOptions as $key => $ct) {
            if ($ct['value']) {
                $customerTypes[$ct['value']] = ($ct['label'] == 'Business') ? true : false;
            }
        }

        /** @var Omnius_Catalog_Model_Product $item */
        foreach ($products as $item) {

            $item->setData('maf', $item->getMaf());

            $item->setData('group_type', $item->getType());
            $item->updateSimType();

            $item->setData('sim_only', $item->getSimOnly());
            $item->setData('prodspecs_afmetingen_unit_value', $item->getUnitValue());
            $item->setData('prodspecs_besturingssysteem_value', $item->getOperatingSystem());
            $customerTypeAttributeId = $item->getData('customertype');
            $item->setData('business_product', isset($customerTypes[$customerTypeAttributeId]) ? $customerTypes[$customerTypeAttributeId] : false);

            $item->setData('include_btw', Mage::getSingleton('customer/session')->showPriceWithBtw());

            $item->setData(
                Omnius_Catalog_Model_Product::CATALOG_CONSUMER_TYPE,
                $item->getData(Omnius_Catalog_Model_Product::CATALOG_CONSUMER_TYPE) ? explode(',', $item->getData(Omnius_Catalog_Model_Product::CATALOG_CONSUMER_TYPE)) : array()
            );

            $item->escapeData();

            /**
             * Prepare prices
             */
            $this->preparePrice($item);
        }

        foreach ($products as $product) {
            $data = $this->parseProductData($product, $keys);
            try {
                $data['thumbnail'] = (string) Mage::helper('catalog/image')->init($product, 'thumbnail')->resize(30);
            } catch (Exception $e) {
                Mage::log(sprintf('Assigned image for product %s not found on disk.', $product->getSku()));
            }
            $productTypes = $product->getType();
            $collection[strtolower(current($productTypes))][] = $data;
        }

        return $collection;
    }

    /**
     * @return array|mixed
     *
     * Get an array of all categories
     */
    protected function getAllCategories()
    {
        $key = serialize(array(strtolower(__METHOD__)));
        if ($result = unserialize($this->getCache()->load($key))) {
            return $result;
        } else {
            $result = array();
            foreach (Mage::getModel('catalog/category')->getCollection()->addAttributeToSelect('name') as $category) {
                $result[$category->getId()] = strtolower(str_replace(' ','__', $category->getName()));
            };

            $this->getCache()->save(serialize($result), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
        }

        return $result;
    }

    /**
     * Checks what addon families are available based on the package type, current products in cart and website.
     * @param $type
     * @param array $allProducts
     * @param null $websiteId
     * @return mixed
     */
    public function processFamilies($type, &$allProducts = array(), $websiteId = null)
    {
        $websiteId = $websiteId ?: Mage::app()->getWebsite()->getId();
        $productsKey = md5(serialize($allProducts));
        $key = serialize(array(__METHOD__, $websiteId, $type, $productsKey));
        if ($result = unserialize($this->getCache()->load($key))) {
            return $result;
        } else {
            $familyCategories = Mage::getModel('catalog/category')
                ->getCollection()
                ->addAttributeToSelect('*')
                ->addAttributeToFilter('is_family', 1)
                ->addAttributeToFilter('is_active', 1)
                ->setOrder('family_sort_order', Varien_Data_Collection::SORT_ORDER_ASC);

            $categories = new Varien_Data_Collection();
            foreach($familyCategories as $cat) {
                $categories->addItem($cat);
            }

            $result['families'] = $categories->toArray();

            $this->getCache()->save(serialize($result), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
            return $result;
        }
    }

    /**
     * @param $product
     * @param $keys
     * @return mixed
     */
    protected function parseProductData($product, $keys)
    {
        $data = $product->getData();
        foreach ($data as $key => &$value) {
            if ($key == Omnius_Catalog_Model_Product::AVAILABLE_SIM_TYPES_ATTRIBUTE) {
                $sims = array();
                // Array of possible simcards
                $allSims = explode(',', $data[$key]);
                $attrHelper = Mage::helper('omnius_configurator/attribute');
                foreach ($allSims as $sim) {
                    $sims[$attrHelper->getSimCardTypeById($sim)] = $attrHelper->getSimCardTypeById($sim);
                }
                $data[$key] = $sims;
            } elseif (!in_array($key, $keys)) {
                unset($data[$key]);
            } elseif ($key == 'name') {
                $data[$key] = html_entity_decode($data[$key]);
            }
        }
        unset($value);

        return $data;
    }

    /**
     * @param $item
     * @param $attrHelper
     * @param $keys
     * @return mixed
     */
    protected function parseProductDataForType($item, $attrHelper, $keys)
    {
        $data = $item->getData();
        foreach ($data as $key => &$value) {
            if ($key == Omnius_Catalog_Model_Product::AVAILABLE_SIM_TYPES_ATTRIBUTE) {
                $sims = [];
                // Array of possible simcards
                $allSims = explode(',', $data[$key]);
                foreach ($allSims as $sim) {
                    $sims[$attrHelper->getSimCardTypeById($sim)] = $attrHelper->getSimCardTypeById($sim);
                }
                $data[$key] = $sims;
            } elseif (!in_array($key, $keys)) {
                unset($data[$key]);
            } elseif ($key == 'name') {
                $data[$key] = html_entity_decode($data[$key]);
            }
        }
        unset($value);

        return $data;
    }

    /**
     * @param $item
     * @param $stockStatuses
     */
    protected function checkIfDiscontinued($item, $stockStatuses)
    {
        $stockStatusAttributeId = $item->getData('stock_status');
        if (isset($stockStatuses[$stockStatusAttributeId])) {
            $item->setData('is_discontinued', $stockStatuses[$stockStatusAttributeId]);
        } else {
            $item->setData('is_discontinued', false);
        }
    }
}
