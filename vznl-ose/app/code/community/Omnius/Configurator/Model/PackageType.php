<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Configurator_Model_PackageType
 */
class Omnius_Configurator_Model_PackageType
{
    const PACKAGE_TYPE_MOBILE = 'mobile';
    const PACKAGE_TYPE_INTERNET = 'internet';
    const PACKAGE_TYPE_PREPAID = 'prepaid';
    const PACKAGE_TYPE_HARDWARE = 'hardware';
}