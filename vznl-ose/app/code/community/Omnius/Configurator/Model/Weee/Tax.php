<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Configurator_Model_Weee_Tax extends Mage_Weee_Model_Tax
{
    /**
     * @param Mage_Catalog_Model_Product $product
     * @param null $shipping
     * @param null $billing
     * @param null $website
     * @param null $calculateTax
     * @param bool $ignoreDiscount
     * @return array|mixed|Varien_Object
     *
     * Overwrite method to gain some improvements when same product(device) is added in multiple packages.
     * The product attribute will only be calculed the first time for the first device afterwhich the registry will be used
     */
    public function getProductWeeeAttributes($product, $shipping = null, $billing = null, $website = null, $calculateTax = null, $ignoreDiscount = false)
    {
        $website = $website ?: Mage::app()->getWebsite()->getId();
        $key = $product->getId().'_'.$website.'_'.(int)$calculateTax.'_'.(int)$ignoreDiscount;
        $weee = Mage::registry($key);
        if($weee === null){
            $weee = parent::getProductWeeeAttributes($product, $shipping, $billing, $website, $calculateTax, $ignoreDiscount);
            Mage::unregister($key);
            Mage::register($key, $weee);
        }

        return $weee;
    }
}