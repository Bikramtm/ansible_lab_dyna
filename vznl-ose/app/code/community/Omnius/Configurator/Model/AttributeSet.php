<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Configurator_Model_AttributeSet
 */
class Omnius_Configurator_Model_AttributeSet
{
    const PROMOTION = 'promotion';
    const PACKAGE_STATUS_OPEN = 'open';
    const PACKAGE_STATUS_COMPLETED = 'completed';
}