<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Wrapper
 */
class Omnius_Configurator_Block_Wrapper extends Mage_Core_Block_Template
{
    protected $_template = 'configurator/wrapper.phtml';

    /**
     * Sets the format template for the configurator display. 
     * @param $format
     * @return $this
     */
    public function setFormat($format)
    {
        if ($format === 'json') {
            $this->_template = 'configurator/wrapper_json.phtml';
        }
        
        return $this;
    }
} 