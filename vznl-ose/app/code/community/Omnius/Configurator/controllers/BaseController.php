<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Configurator_BaseController
 */
abstract class Omnius_Configurator_BaseController extends Mage_Core_Controller_Front_Action
{
    /** @var Omnius_Core_Helper_Data|null */
    private $_dynaCoreHelper = null;

    /**
     * @return Omnius_Core_Helper_Data
     */
    protected function getDynaCoreHelper()
    {
        if ($this->_dynaCoreHelper === null) {
            $this->_dynaCoreHelper = Mage::helper('omnius_core');
        }

        return $this->_dynaCoreHelper;
    }

    /**
     * Gets subscriptions for the mentioned package type in the URL.
     */
    public function subscriptionsAction()
    {
        $this->jsonResponse($this->getModel()
            ->getProductsOfType(Omnius_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION, Mage::app()->getWebsite()->getId(), $this->getRequest()->getParams()));
    }

    /**
     * Gets devices for the mentioned package type in the URL.
     */
    public function devicesAction()
    {
        $this->jsonResponse($this->getModel()
            ->getProductsOfType(Omnius_Catalog_Model_Type::SUBTYPE_DEVICE, Mage::app()->getWebsite()->getId(), $this->getRequest()->getParams()));
    }

    /**
     * Gets addons for the mentioned package type in the URL.
     */
    public function addonsAction()
    {
        $this->jsonResponse($this->getModel()
            ->getProductsOfType(Omnius_Catalog_Model_Type::SUBTYPE_ADDON, Mage::app()->getWebsite()->getId(), $this->getRequest()->getParams()));
    }

    /**
     * Gets accessories for the mentioned package type in the URL.
     */
    public function accessoriesAction()
    {
        $this->jsonResponse($this->getModel()
            ->getProductsOfType(Omnius_Catalog_Model_Type::SUBTYPE_ACCESSORY, Mage::app()->getWebsite()->getId(), $this->getRequest()->getParams()));
    }

    /**
     * Retrieves all products for the specific package type.
     */
    public function allAction()
    {
        $this->jsonResponse($this->getModel()
            ->getAllConfiguratorItems(Mage::app()->getWebsite()->getId(), $this->getRequest()->getParams()));
    }

    /**
     * @param $data
     * @param int $statusCode
     */
    protected function jsonResponse($data, $statusCode = 200)
    {
        /** @var array $response */
        $response = $this->getDynaCoreHelper()
            ->jsonResponse($data, $statusCode);

        $this->getResponse()
            ->setBody($response['body'])
            ->setHeader('Content-Type', 'application/json')
            ->setHttpResponseCode($response['status']);
    }

    protected function getModel()
    {
        preg_match('/^(.*)_(.*)Controller$/', get_called_class(), $matches);
        return Mage::getSingleton(sprintf('omnius_configurator/%s', lcfirst($matches[2])));
    }
}