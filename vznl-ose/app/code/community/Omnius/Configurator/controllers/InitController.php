<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class InitController
 */
class Omnius_Configurator_InitController extends Mage_Core_Controller_Front_Action
{
    public function mobileAction()
    {
        $this->getActionData(__FUNCTION__);
    }

    /** @var Omnius_Core_Helper_Data|null */
    private $coreHelper = null;

    /**
     * @return Omnius_Core_Helper_Data
     */
    protected function getCoreHelper()
    {
        if ($this->coreHelper === null) {
            $this->coreHelper = Mage::helper('omnius_core');
        }

        return $this->coreHelper;
    }

    /**
     * @param $section
     *
     * Get configuration for specific action
     */
    protected function getActionData($section)
    {
        $section = str_replace('Action', '', $section);
        $config = array(
            'endpoints' => array(
                'products' => sprintf('configurator/%s/all', $section),
                'cart.addMulti' => 'configurator/cart/addMulti',
                'cart.delete' => 'configurator/cart/remove',
                'cart.list' => 'configurator/cart/list',
                'cart.empty' => 'configurator/cart/empty',
                'cart.getRules' => 'configurator/cart/getRules',
                'cart.getPrices' => 'configurator/init/getPrices',
                'cart.getPopup' => 'configurator/init/getProductpopupdata',
                'cart.getdeviceRules' => 'configurator/cart/getDeviceRules',
                'cart.getsubscriptionRules' => 'configurator/cart/getSubscriptionRules',
            ),
            'container' => '#config-wrapper',
            'sections' => array_map('strtolower', array_diff(
                Mage::getSingleton('omnius_configurator/catalog')->getOptionsForPackageType($section),
                Mage::getSingleton('omnius_configurator/catalog')->getOrderRestrictions(Mage::app()->getWebsite()->getCode())
            )),
            'type' => $section,
            'spinner' => 'skin/frontend/omnius/default/images/spinner.gif',
            'spinnerId' => '#spinner',
            'cache' => false,
            'include_btw' => Mage::getSingleton('customer/session')->showPriceWithBtw(),
            'hardwareOnlySwap' => Mage::getSingleton('customer/session')->getOrderEditMode()
        );

        $this->buildConfigurator($section, $config);
    }

    /**
     * Initializes the configurator based on the package type and sale type.
     * @param $type
     * @param array $config
     * @param string $saleType
     */
    protected function buildConfigurator($type, array $config = array(), $saleType = Omnius_Checkout_Model_Sales_Quote_Item::ACQUISITION)
    {
        $packageId = $this->getRequest()->getParam('packageId');
        $currentProducts = $this->getRequest()->getParam('current_products');

        $ctn = $this->getRequest()->getParam('ctn');
        $saleType = $this->getRequest()->getParam('saleType') ? : $saleType;
        $extraCTNPackages = array();
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        try {
            if (!$packageId) {
                $ctns = explode(',', $ctn);
                $descriptions = Mage::getModel('ctn/ctn')->getAllCTNDetails($ctns);
                $getKey = function (array &$array, $key) {
                    return isset($array[$key]) ? $array[$key] : '';
                };

                $packageCTN = array_pop($ctns);
                // check if one-of-deal is active
                $isOneOfDeal = Mage::helper('omnius_configurator/cart')->isOneOfDealActive();
                $oneOfDealOptions = $isOneOfDeal && $packageCTN ? array('retainable' => false) : array();

                if ($packageCTN) {
                    $len = count($ctns);
                    for ($i = 0; $i < $len; ++$i) {
                        $extraCTNPackages[$ctns[$i]] = array(
                            'id' => Mage::helper('omnius_configurator/cart')->initNewPackage(
                                $type,
                                $saleType,
                                $ctns[$i],
                                $getKey($descriptions, $ctns[$i]),
                                $oneOfDealOptions
                            ),
                            'description' => $getKey($descriptions, $ctns[$i]),
                        );
                    }

                    // only store description for last package
                    $extraCTNPackages[$packageCTN] = array(
                        'description' => $getKey($descriptions, $packageCTN),
                    );
                }
                $packageId = Mage::helper('omnius_configurator/cart')->initNewPackage(
                    $type,
                    $saleType,
                    $packageCTN,
                    $currentProducts,
                    $oneOfDealOptions
                );
            } else {
                $quote->saveActivePackageId($packageId);
            }

            $initCart = Mage::helper('omnius_configurator/cart')->getCartItems($type, $packageId);

            $config['packageId'] = $packageId;
            $config['sale_type'] = $saleType;
            $config['current_products'] = $currentProducts;
            Mage::register('freeze_models', true, true);
            /** @var Omnius_Package_Model_Package $packageModel */
            $packageModel = Mage::getModel('package/package')->getPackages(null, $quote->getId(), $packageId)->getFirstItem();
            $config['old_sim'] = $packageModel->getId() && $packageModel->getOldSim();
            $responseArray = array(
                'error' => false,
                'initCart' => count($initCart) ? $initCart : new stdClass(),
                'config' => $config,
                'packageId' => $packageId,
                'promoRules' => Mage::helper('pricerules')->findApplicablePromoRules(true),
            );

            if (($incompleteSections = Mage::helper('omnius_checkout')->checkPackageStatus($packageId)) && is_array($incompleteSections)) {
                $responseArray['incomplete_sections'] = $incompleteSections;
            }

            if (!empty($extraCTNPackages)) {
                $responseArray['ctnPackages'] = $extraCTNPackages;
            }

            $this->jsonResponse($responseArray);
        } catch (Exception $e) {
            Mage::getSingleton('core/logger')->logException($e);
            $this->jsonResponse(array(
                'error' => true,
                'message' => 'Could not load mobile configurator',
            ));
        }
    }

    /**
     * Returns product information
     */
    protected function getProductsAction()
    {
        try {
            $response = array();
            $helper = Mage::helper('omnius_configurator');
            $request = Mage::app()->getRequest();
            $websiteId = $request->get('website_id');
            $packageTypes = array('mobile');
            foreach($packageTypes as $type) {
                $productModel = Mage::getModel(sprintf('omnius_configurator/%s', $type));
                $products = $productModel->getAllConfiguratorItems($websiteId);
                $productFilters = $products['filters'];
                $productFilters = isset($productFilters) && is_array($productFilters) && count($productFilters) ? $helper->toArray($productFilters) : new stdClass();
                unset($products['filters']);
                $allProducts = count($products) ? $helper->toArray($products) : new stdClass();
                $response[sprintf('prod_%s', $type)] = $allProducts;
                $response[sprintf('filter_%s', $type)] = $productFilters;

                $processedFamilies = $productModel->processFamilies($type, $allProducts);
                $response[sprintf('fam_%s', $type)] = $processedFamilies['families'];
                $response[sprintf('prod_%s', $type)] = $processedFamilies['products'];

                if (isset($products['consumer_filters'])) {
                    $consumerProductFilters = $products['consumer_filters'];
                    $consumerProductFilters = isset($consumerProductFilters) && is_array($consumerProductFilters) && count($consumerProductFilters) ? $helper->toArray($consumerProductFilters) : new stdClass();
                    unset($products['consumer_filters']);
                    $response[sprintf('filter_consumer_%s', $type)] = $consumerProductFilters;
                }
            }

            $date = new DateTime('+1 year');
            $this->jsonResponse(
                $response,
                200,
                [
                    'Pragma' => 'cache',
                    'Cache-Control' => 'max-age=31536000, must-revalidate',
                    'Expires' => $date->format('r'),
                    'X-Cache-Override' => true,
                ]);
        } catch (Exception $e) {
            Mage::getSingleton('core/logger')->logException($e);
            $this->jsonResponse(array(
                'error' => true,
                'message' => 'Could not load products',
            ));
        }
    }

    /**
     * Get the updated products prices based on current selection
     */
    public function getPricesAction()
    {
        /** @var Dyna_Cache_Model_Cache $cache */
        $cache = Mage::getSingleton('dyna_cache/cache');
        $websiteId = $this->getRequest()->getParam('website_id');
        $packageType = $this->getRequest()->getParam('type');
        $productIds = array();
        $cacheKey = 'configurator_get_prices_action_' . $websiteId . '_' . $packageType;
        if ($products = $this->getRequest()->getParam('products', false)) {
            $productIds = array_filter(explode(',', $products), function($id) {
                return (int) $id;
            });
            sort($productIds);
            $cacheKey .= join('_', $productIds);
        }

        if ($cachedPrices = $cache->load($cacheKey)) {
            $prices = unserialize($cachedPrices);
        } else {
            $prices = Mage::helper('omnius_checkout')->getUpdatedPrices($productIds, $websiteId, $packageType);
            $cache->save(serialize($prices), $cacheKey, array(Dyna_Cache_Model_Cache::PRODUCT_TAG), $cache->getTtl());
        }

        $date = new DateTime('+1 year');
        $this->jsonResponse(
            array(
                'prices' => $prices
            ), 200,
            [
                'Pragma' => 'cache',
                'Cache-Control' => 'max-age=31536000, must-revalidate',
                'Expires' => $date->format('r'),
                'X-Cache-Override' => true,
            ]
        );
    }

    /**
     *
     * Get configurator options that can be cached by Varnish
     */
    public function getConfiguratorOptionsAction()
    {
        $type = $this->getRequest()->getParam('type');
        $helper = Mage::helper('omnius_configurator');
        $productModel = Mage::getModel(sprintf('omnius_configurator/%s', $type));
        $products = $productModel->getAllConfiguratorItems();

        if (isset($products['filters'])) {
            unset($products['filters']);
        }

        $attribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', Omnius_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR);
        $simcard = Mage::helper('omnius_catalog')->getAttributeAdminLabel($attribute, null, Omnius_Catalog_Model_Type::SUBTYPE_SIMCARD);

        $sims = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToSelect('*')
            ->addWebsiteFilter()
            ->addAttributeToFilter(Omnius_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR, $simcard)
            ->load()
            ->toArray();
        $allProducts = count($products) ? $helper->toArray($products) : new stdClass();
        $rules = Mage::helper('omnius_configurator/cart')->getCartRules(array(), $allProducts);
        /** @var Omnius_Configurator_Block_Wrapper $wrapper */
        try {
            $responseArray = array(
                'sim' => count($sims) ? $helper->toArray($sims) : new stdClass(),
                'allowedProducts' => array_values($rules)
            );
            Mage::getSingleton('core/session')->setCacheData($responseArray);
            $this->jsonResponse($responseArray);
        } catch (Exception $e) {
            Mage::getSingleton('core/logger')->logException($e);
            $this->jsonResponse(array(
                'error' => true,
                'message' => 'Could not load configurator options',
            ));
        }
    }

    /**
     * @param $data
     * @param int $statusCode
     * @throws InvalidArgumentException
     */
    protected function jsonResponse($data, $statusCode = 200, $headers = [])
    {
        $response = $this->getCoreHelper()
            ->jsonResponse($data, $statusCode);

        $this->getResponse()
            ->setBody($response['body'])
            ->setHeader('Content-Type', 'application/json');
            foreach($headers as $key => $value) {
                $this->getResponse()->setHeader($key, $value, true);
            }
            $this->getResponse()->setHttpResponseCode($response['status']);
    }

    /**
     * Identify referer url via all accepted methods (HTTP_REFERER, regular or base64-encoded request param)
     *
     * @return string
     */
    protected function _getRefererUrl()
    {
        $refererUrl = $this->getRequest()->getServer('HTTP_REFERER');
        if ($url = $this->getRequest()->getParam(self::PARAM_NAME_REFERER_URL)) {
            $refererUrl = $url;
        }
        if ($url = $this->getRequest()->getParam(self::PARAM_NAME_BASE64_URL)) {
            $refererUrl = Mage::helper('core')->urlDecode($url);
        }
        if ($url = $this->getRequest()->getParam(self::PARAM_NAME_URL_ENCODED)) {
            $refererUrl = Mage::helper('core')->urlDecode($url);
        }

        if (!$this->_isUrlInternal($refererUrl)) {
            $refererUrl = Mage::app()->getStore()->getBaseUrl();
        }
        return $refererUrl;
    }

    /**
     * Action that parses the shopping cart and renders the extended shopping cart.
     */
    public function expandCartAction()
    {
        $html = Mage::getSingleton('core/layout')->createBlock('core/template')
            ->setTemplate('customer/right_sidebar.phtml')->toHtml();
        $this->getResponse()->setBody($html);
    }

    public function showDetailsCartAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }
        if ($this->getRequest()->isXmlHttpRequest()) {
            $params = new Varien_Object($this->getRequest()->getParams());
            /** @var Mage_Sales_Model_Resource_Quote_Collection $collection */
            try {
                if ($params->hasData('cart_id')) {
                    /** @var Mage_Sales_Model_Resource_Quote_Collection $collection */
                    $collection = Mage::getModel('sales/quote')->getCollection()
                        ->addFieldToFilter('entity_id', $params['cart_id'])
                        ->load();

                    /** @var Mage_Sales_Model_Quote $quote */
                    if (!$collection->getItemById($params->getData('cart_id'))) {
                        $this->getResponse()
                            ->setHeader('Content-Type', 'application/json')
                            ->setBody(
                                Mage::helper('core')->jsonEncode(
                                    array('error' => true, 'message' => 'No quote with the provided quote_id found')
                                )
                            );
                        return;
                    }
                    $quote = $collection->getItemById($params->getData('cart_id'));

                    $html = Mage::getSingleton('core/layout')->createBlock('core/template')
                        ->assign('quote', $quote)
                        ->setTemplate('customer/popup_cart_details.phtml')->toHtml();
                    $this->getResponse()->setBody($html);
                    $this->getResponse()
                        ->setHeader('Content-Type', 'application/json')
                        ->setBody(
                            Mage::helper('core')->jsonEncode(
                                array(
                                    'html'  => $html,
                                    'header'=> $this->getCartEmailButton($quote)
                                )
                            )
                        );
                    return;
                }
            } catch (Exception $e) {
                Mage::getSingleton('core/logger')->logException($e);
                $this->getResponse()
                    ->setHeader('Content-Type', 'application/json')
                    ->setBody(Mage::helper('core')->jsonEncode(array('error' => true, 'message' => $e->getMessage())));
            }
        }
    }

    /**
     * @param $quote
     * @return string
     */
    public function getCartEmailButton($quote)
    {
        return $quote->getCartStatus() != Omnius_Checkout_Model_Sales_Quote::CART_STATUS_RISK ? '<div class="btn btn-info" onclick="jQuery.post(\'' . Mage::getUrl('checkout/cart/emailQuote') . '?cartId='  . $quote->getId() . '\', function(response){ jQuery(\'#cart-show-details .message\').html(response.message);})">' . $this->__('Email '. $quote->getTypeOfCartLiteral()) . '</div>'  : '&nbsp;';
    }

    /**
     * Fetches the stocks for the devices.
     */
    public function getDeviceStockAction()
    {
        /** @var Omnius_Catalog_Model_Product $productModel */
        $productModel = Mage::getModel('catalog/product');
        $websiteId = $this->getRequest()->getParam('website_id');
        $axiStoreId = $this->getRequest()->getParam('axi_store_id', 0);
        $result = $productModel->getStockCall($websiteId, Omnius_Catalog_Model_Type::SUBTYPE_DEVICE, $axiStoreId);
        $this->jsonResponse($result);
    }

    /**
     * Fetches the stocks for the accessories.
     */
    public function getAccessoriesStockAction()
    {
        /** @var Omnius_Catalog_Model_Product $productModel */
        $productModel = Mage::getModel('catalog/product');
        $websiteId = $this->getRequest()->getParam('website_id');
        $axiStoreId = $this->getRequest()->getParam('axi_store_id', 0);
        $result = $productModel->getStockCall($websiteId, Omnius_Catalog_Model_Type::SUBTYPE_ACCESSORY, $axiStoreId);
        $this->jsonResponse($result);
    }

    /**
     * Retrieves the stock status for a product via AJAX.
     */
    public function getStockAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }

        try {
            $html = Mage::getSingleton('core/layout')->createBlock('core/template')
                ->setTemplate('configurator/widget/stock.phtml')->toHtml();
            $this->getResponse()->setBody($html);
        } catch (Exception $e) {
            $this->getResponse()
                ->setHeader('Content-Type', 'application/json')
                ->setBody(
                    Mage::helper('core')->jsonEncode(
                        array(
                            'error' => true,
                            'message' => $e->getMessage()
                        )
                    )
                );
        }
    }

    public function emptyAction()
    {
        $saleType = $this->getRequest()->getParam('saleType');
        $ctn = $this->getRequest()->getParam('ctn');
        $type = $this->getRequest()->getParam('type');

        $packageId = Mage::helper('omnius_configurator/cart')->initNewPackage($type, $saleType, $ctn);
        Mage::getSingleton('checkout/cart')->getQuote()->save();

        $this->getResponse()
            ->setHttpResponseCode(200)
            ->setHeader('Content-Type', 'application/json')
            ->setBody(Mage::helper('core')->jsonEncode(array('packageId' => $packageId)));
    }

    /**
     * Retrieve product additional information for info popup
     */
    public function getProductpopupdataAction()
    {
        $request = $this->getRequest();

        if(!$request->getParam('product_id') || !$request->getParam('website_id'))
        {
            $this->jsonResponse(array('error' => true, 'message' => Mage::helper('omnius_checkout')->__('Incorrect parameters provided')));
        }
        else{
            $fields = array('short_description',
                'prodspecs_besturingssysteem_value', 'prodspecs_afmetingen_lengte', 'prodspecs_afmetingen_unit_value', 'prodspecs_afmetingen_dikte',
                'prodspecs_afmetingen_breedte', 'prodspecs_weight_gram', 'prodspecs_batterij', 'prodspecs_schermdiagonaal_inch', 'prodspecs_schermresolutie',
                'prodspecs_touchscreen', 'prodspecs_camera', 'prodspecs_megapixels_1e_cam', 'prodspecs_processor', 'prodspecs_werkgeheugen_mb',
                'prodspecs_standby_tijd', 'prodspecs_spreektijd', 'prodspecs_filmen_in_hd', 'prodspecs_audio_out', 'prodspecs_hdmi', 'prodspecs_2e_camera',
                'prodspecs_4g_lte', 'prodspecs_3g', 'prodspecs_gprs_edge', 'prodspecs_wifi', 'prodspecs_wifi_frequenties', 'prodspecs_bluetooth',
                'prodspecs_bluetooth_version', 'prodspecs_nfc', 'prodspecs_gps', 'prodspecs_lichtsensor', 'prodspecs_opslag_uitbreidbaar', 'prodspecs_flitser',
                'prodspecs_voice_dialing', 'prodspecs_spraakbesturing', 'prodspecs_accu_verwisselb');

            $product = Mage::getModel('catalog/product')
                    ->getCollection()
                    ->addFieldToFilter('entity_id', $request->getParam('product_id'))
                    ->addStoreFilter(Mage::app()->getWebsite($request->getParam('website_id'))->getDefaultStore()->getId())
                    ->addAttributeToSelect($fields)
                    ->getFirstItem();
            if(!$product) {
                $this->jsonResponse(array('error' => true, 'message' => Mage::helper('omnius_checkout')->__('Product does not exist')));
            }
            $this->jsonResponse($product->getData());
        }
    }

    /**
     * Get html block containing all modals
     */
    public function getGeneralModalsAction()
    {
        $html = Mage::getSingleton('core/layout')->createBlock('core/template')
            ->setTemplate('page/html/global_modals.phtml')->toHtml();
        $this->getResponse()->setBody($html);
    }
}
