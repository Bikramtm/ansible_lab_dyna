<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Configurator_TemplateController
 */
class Omnius_Configurator_TemplateController extends Mage_Core_Controller_Front_Action
{
    /** @var Omnius_Configurator_Model_Template */
    protected $_templateModel;

    /** @var Dyna_Cache_Model_Cache */
    protected $_cache;

    /**
     * Retrieves all template models for the configurator display.
     */
    public function allAction()
    {
        $type = $this->getRequest()->getParam('type');
        $templates = $this->getTemplateModel()->getAllTemplates($type);
        $this->jsonResponse($templates);
    }

    public function getAction()
    {
        $type = $this->getRequest()->getParam('type');
        $section = $this->getRequest()->getParam('section');
        $key = sprintf('%s_section_template', $section);
        if ($result = unserialize($this->getCache()->load($key))) {
            $this->jsonResponse($result);
        } else {
            $template = $this->getTemplateModel()->getTemplate($type, $section);

            $this->getCache()->save(serialize($template), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());

            $this->jsonResponse($template);
        }
    }

    /**
     * Renders the partials needed for the display of the configurator. 
     */
    public function partialsAction()
    {
        $type = $this->getRequest()->getParam('type');
        $key = sprintf('template_partials');
        if ($result = unserialize($this->getCache()->load($key))) {
            $this->jsonResponse($result);
        } else {
            $partials = $this->getTemplateModel()->getPartials($type);

            $this->getCache()->save(serialize($partials), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());

            $this->jsonResponse($partials);
        }
    }

    /**
     * @return Omnius_Configurator_Model_Template|Mage_Core_Model_Abstract
     */
    protected function getTemplateModel()
    {
        if ( ! $this->_templateModel) {
            $this->_templateModel = Mage::getSingleton('omnius_configurator/template');
        }
        return $this->_templateModel;
    }

    /**
     * @param $data
     * @param int $statusCode
     * @throws InvalidArgumentException
     */
    protected function jsonResponse($data, $statusCode = 200)
    {
        if ($data instanceof Varien_Data_Collection) {
            $items = array();
            foreach ($data->getItems() as $item) {
                array_push($items, $item->toArray());
            }
        } elseif ($data instanceof Varien_Object) {
            $items = $data->toArray();
        } elseif (is_array($data)) {
            $el = array_shift(array_values($data));
            if ($el instanceof Varien_Data_Collection) {
                $items = array();
                foreach ($data as $key => $coll) {
                    $items[$key] = array();
                    foreach ($coll->getItems() as $item) {
                        $items[$key][] = $item->toArray();
                    }
                }
            } elseif ($el instanceof Varien_Object) {
                $items = array();
                foreach ($data as $key => $coll) {
                    $items[$key][] = $coll->toArray();
                }
            } else {
                $items = $data;
            }
        } else {
            $items = array(
                'error' => true,
                'message' => 'An error has been encountered',
            );
            $status = 400;
        }

        $this->getResponse()
            ->setHttpResponseCode(isset($status) ? $status : $statusCode)
            ->setHeader('Content-Type', 'application/json')
            ->setBody(Mage::helper('core')->jsonEncode($items));
    }

    /**
     * @return Dyna_Cache_Model_Cache
     */
    protected function getCache()
    {
        if ( ! $this->_cache) {
            $this->_cache = Mage::getSingleton('dyna_cache/cache');
        }
        return $this->_cache;
    }
}