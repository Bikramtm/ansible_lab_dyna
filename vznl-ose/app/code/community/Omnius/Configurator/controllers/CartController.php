<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Configurator_CartController
 */
class Omnius_Configurator_CartController extends Mage_Core_Controller_Front_Action
{
    /** @var Dyna_ProductMatchRule_Model_Rule */
    protected $_ruler;

    /** @var Omnius_Configurator_Helper_Attribute */
    protected $_attrHelper;

    /** @var Omnius_Configurator_Helper_Cart */
    protected $_cartHelper;

    /** @var Omnius_Checkout_Helper_Data */
    protected $_checkoutHelper;

    /** @var  Omnius_PriceRules_Helper_Data */
    protected $_priceRulesHelper;

    /** @var Omnius_Core_Helper_Data|null */
    private $_dynaCoreHelper = null;

    /**
     * @return Omnius_Core_Helper_Data
     */
    protected function getDynaCoreHelper()
    {
        if ($this->_dynaCoreHelper === null) {
            $this->_dynaCoreHelper = Mage::helper('omnius_core');
        }

        return $this->_dynaCoreHelper;
    }

    /**
     * @return $this
     */
    public function preDispatch(){
        parent::preDispatch();
        $this->_priceRulesHelper = Mage::helper('pricerules');
        return $this;
    }

    /**
     * Retrieves the cart items for a specific package.
     */
    public function listAction()
    {
        $type = $this->getRequest()->getParam('type');
        $packageId = $this->getRequest()->getParam('packageId');

        $this->jsonResponse(array(
            'error' => false,
            'ids' => $this->getCartHelper()->getCartItems($type, $packageId),
        ));
    }

    /**
     * Creates a new package in the configurator.
     */
    public function newPackageAction()
    {
        $this->_getQuote()->saveActivePackageId(null);
    }

    /**
     * Method used for adding products to cart.
     */
    public function addMultiAction()
    {
        if (Mage::getSingleton('customer/session')->getOrderEditMode() && ! Mage::getSingleton('checkout/session')->getIsEditMode()) {
            $this->jsonResponse(array(
                'error' => true,
                'reload' => true,
                'message' => $this->__('Cannot add items to an already edited and saved quote'),
            ));
            return;
        }

        Mage::register('product_add_origin', 'addMulti');
        $priceRulesHelper = $this->_priceRulesHelper;
        $type = $this->getRequest()->getParam('type');
        $section = $this->getRequest()->getParam('section');
        $sim = $this->getRequest()->getParam('simType');
        $oldSim = $this->getRequest()->getParam('oldSim');
        $packageId = $this->getRequest()->getParam('packageId');
        $this->_getQuote()->saveActivePackageId($packageId);
        $this->_getQuote()->setCurrentStep(null);
        $removedDefaultFlag = false;

        // Retrieve all the exclusive families
        $allExclusiveFamilyCategoryIds = Mage::getModel('catalog/category')->getAllExclusiveFamilyCategoryIds();
        // List of all the exclusive families that are contained in the package
        $exclusiveFamilyCategoryIds = [];
        $products = array_filter(
            array_filter(
                is_array($this->getRequest()->getParam('products')) ? $this->getRequest()->getParam('products') : array(), 'trim'
            )
        );

        // Create the new package for the quote or update if already created
        /** @var Omnius_Package_Model_Package $packageModel */
        $packageModel = Mage::getModel('package/package')->getCollection()
            ->addFieldToFilter('quote_id', $this->_getQuote()->getId())
            ->addFieldToFilter('package_id', $packageId)
            ->getFirstItem();
        if (!$packageModel->getId()) {
            $packageModel->setQuoteId($this->_getQuote()->getId())->setPackageId($packageId)->setType($type);
        }

        $added = [];
        if (!count($products)) {
            // Clear all
            return $this->removeAllItems($section, $packageId, $type, $packageModel);
        }

        $persistedProducts = [];
        $allItems = $this->_getQuote()->getAllItems();
        $productsInCart = $appliedRulesArray = [];
        $deletedItems = [];
        try {
            /** @var Mage_Sales_Model_Quote_Item $item */
            foreach ($allItems as $itemKey => $item) {
                if (isset($deletedItems[$item->getId()])) {
                    continue;
                }
                if (
                    $item->getPackageId() == $packageId &&
                    (
                        $item->getProduct()->is(Omnius_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION)
                        || $item->getProduct()->is(Omnius_Catalog_Model_Type::SUBTYPE_DEVICE)
                        || $item->getProduct()->is(Omnius_Catalog_Model_Type::SUBTYPE_ACCESSORY)
                        || $item->getProduct()->is(Omnius_Catalog_Model_Type::SUBTYPE_ADDON))
                ) {
                    $productsInCart[] = $item->getProductId();
                }

                if (($categoriesExclusive = array_intersect($item->getProduct()->getCategoryIds(), $allExclusiveFamilyCategoryIds))
                    && ($item->getPackageId() == $packageId)
                ) {
                    foreach ($categoriesExclusive as $categoryExclusive) {
                        $exclusiveFamilyCategoryIds[$item->getProductId()] = $categoryExclusive;
                    }
                }

                if ($item->getPackageId() == $packageId
                    && $item->getProduct()->isInSection($section)
                ) {
                    if (!in_array($item->getProductId(), $products)) {
                        $this->removeItemAndParseDefault($item, $productsInCart, $exclusiveFamilyCategoryIds, $packageModel, $allItems, $deletedItems, $itemKey, $removedDefaultFlag, $removedDefault);
                    } else {
                        $persistedProducts[] = $item->getProductId();
                    }
                }
            }
        } catch (Exception $e) {
            Mage::getSingleton('core/logger')->logException($e);
            $this->jsonResponse(array(
                'error' => true,
                'message' => $this->__('Cannot add the item to shopping cart'),
            ));
            return;
        }

        try {
            $this->validateProducts($productsInCart, $allItems, $packageId, $validProducts, $item);
        } catch (Exception $e) {
            $this->jsonResponse(array(
                'error' => true,
                'message' => $e->getMessage(),
            ));

            return;
        }
        
        // Preload the attributes needed for all the products to prevent loading each product
        $invalidProducts = array_diff($products, $validProducts);
        if ($invalidProducts) {
            $allProducts = Mage::getResourceModel('catalog/product_collection')
                ->addIdFilter($invalidProducts);
        }

        foreach ($products as $productId) {
            try {
                if (in_array($productId, $persistedProducts)) {
                    continue;
                }
                $this->parseProduct($packageId, $productId, $type, $appliedRulesArray, $added);
            } catch (RuntimeException $e) {
                continue;
            } catch (Mage_Core_Exception $e) {
                $messages = array_map(function ($el) {
                    return Mage::helper('core')->escapeHtml($el);
                }, array_unique(explode("\n", $e->getMessage())));
                $this->jsonResponse(array(
                    'error' => true,
                    'message' => join(', ', $messages),
                ));
                $this->removeProductFromCart($added);
                $this->_getCart()->save();

                return;
            } catch (Exception $e) {
                Mage::getSingleton('core/logger')->logException($e);
                $this->jsonResponse(array(
                    'error' => true,
                    'message' => $this->__('Cannot add the item to shopping cart'),
                ));
                $this->removeProductFromCart($added);
                $this->_getCart()->save();

                return;
            }
        }
        unset($allProducts, $invalidProducts);

        // Add sim if needed
        $simError = false;
        $simName = null;
        $package = $this->_getQuote()->getPackageById($packageId);

        $this->addSimProductToCart($sim, $package, $type, $oldSim, $simName, $simError);
        $finalProducts = array_merge($productsInCart, $added);
        $addedDefaultItems = $this->parseDefaultedProducts($added, $packageModel, $finalProducts, $allExclusiveFamilyCategoryIds, $exclusiveFamilyCategoryIds, $type);

        $this->_getCart()->save();
        /** @var Omnius_Package_Model_Package $packageModel */
        $packageModel = Mage::getModel('package/package')->getCollection()
            ->addFieldToFilter('quote_id', $this->_getQuote()->getId())
            ->addFieldToFilter('package_id', $packageId)
            ->getFirstItem();
        // Update old_sim attribute to the current package if it is a retention package or if it is HN subscription

        if ($packageModel->getSaleType() == Omnius_Checkout_Model_Sales_Quote_Item::RETENTION
        ) {
            $oldSimValue = ($oldSim == 'true') ? 1 : 0;
            $packageModel->setOldSim($oldSimValue)->save();
        }

        $packageStatus = Mage::helper('omnius_checkout')->checkPackageStatus($packageId);

        // Update status
        $packageModel->setCurrentStatus(Omnius_Configurator_Model_AttributeSet::PACKAGE_STATUS_COMPLETED === $packageStatus
            ? Omnius_Configurator_Model_AttributeSet::PACKAGE_STATUS_COMPLETED
            : Omnius_Configurator_Model_AttributeSet::PACKAGE_STATUS_OPEN);
        $packageModel->updateDefaultedRemoved($removedDefaultFlag);
        $packageModel->checkRemoveCurrentSim();
        $packageModel->save();

        $this->returnCombinations($simError, $simName, isset($addedDefaultItems) ? $addedDefaultItems : [], isset($removedDefault) ? $removedDefault : [], $packageStatus);
    }

    /**
     * Retrieves the list of possible combinations for the current package without the deviceRc
     */
    public function getDeviceRulesAction()
    {
        if (!$this->_getQuote()->getId()) {
            return $this->jsonResponse(
                array(
                    'error' => true,
                    'message' => $this->__('Invalid quote'),
                )
            );
        }
        $items = [];
        $packageId = $this->_getQuote()->getActivePackageId();
        foreach ($this->_getQuote()->getAllItems() as $item) {
            // Only compute items that are in the current package, and are not device subscription
            $product = $item->getProduct();
            if ($item->getPackageId() == $packageId
                && Mage::helper('omnius_catalog')->is(
                    Mage::getSingleton('omnius_configurator/catalog')->getOptionsForPackageType(Omnius_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE),
                    $item->getProduct()
                )
            ){
                $items[] = $product->getId();
            }
        }
        $result = Mage::helper('omnius_configurator/cart')->getCartRules($items, [], Mage::helper('superorder')->shouldEmulateStore(Mage::app()->getWebsite()->getId()));

        return $this->jsonResponse(
            array(
                'error' => false,
                'ids' => Mage::helper('core')->jsonEncode($result),
            )
        );
    }

    /**
     * Retrieves the list of possible combinations for the current package when subscription is changed
     */
    public function getSubscriptionRulesAction()
    {
        if (!$this->_getQuote()->getId()) {
            return $this->jsonResponse(
                array(
                    'error' => true,
                    'message' => $this->__('Invalid quote'),
                )
            );
        }
        // Return an empty array as there are no restrictions at this point for this    
        return $this->jsonResponse(
            array(
                'error' => false,
                'ids' => Mage::helper('core')->jsonEncode([]),
            )
        );
    }

    /**
     * Get list of rules that will be cached
     */
    public function getRulesAction()
    {
        $productIds = array();
        if ($products = Mage::app()->getRequest()->getParam('products', false)) {
            $productIds = explode(',', $products);
        }
        $type = $this->getRequest()->get('type');
        $websiteId = $this->getRequest()->get('websiteId', null);
        if ($type) {
            $productModel = Mage::getModel(sprintf('omnius_configurator/%s', $type));
            $allProducts = $productModel->getAllConfiguratorItems();
            $productsArray = count($allProducts) ? Mage::helper('omnius_configurator')->toArray($allProducts) : new stdClass();
        }
        /** @var Omnius_Configurator_Helper_Cart $cartHelper */
        $cartHelper = Mage::helper('omnius_configurator/cart');
        $rules = $cartHelper->getCartRules($productIds, isset($productsArray) ? $productsArray : [], $websiteId);
        $mandatory = $cartHelper->getMandatory($productIds, $websiteId);
        $this->jsonResponse(array(
            'rules' => $rules,
            'mandatory' => $mandatory
        ));
    }

    public function addPackageAction() 
    {
    
        if (!$this->getRequest()->isPost()) {
            return;
        }

        $package_id = $this->getRequest()->getParam('product');

        $package = Mage::getModel('catalog/product')->load($package_id);
        if($package->getId()) {
            $products = $package->getTypeInstance(true)->getAssociatedProductIds($package);
            // TODO: check if products are still allowed

            $packageTypeAttr = $package->getResource()->getAttribute('package_type');
            if ($packageTypeAttr->usesSource()) {
                $packageType = $packageTypeAttr->getFrontend()->getValue($package);
            } else {
                $this->jsonResponse(array(
                    'error' => true,
                    'message' => $this->__('Cannot add the item to shopping cart. Package type is missing.'),
                ));
                return;
            }

            Mage::helper('omnius_configurator/cart')->initNewPackage($packageType, Omnius_Checkout_Model_Sales_Quote_Item::ACQUISITION, null);
            foreach($products as $productId) {
                try {
                    $this->addProductToCart($productId, $packageType);
                } catch (Exception $e) {
                    Mage::getSingleton('core/logger')->logException($e);
                    $this->jsonResponse(array(
                        'error' => true,
                        'message' => $this->__($e->getMessage())
                    ));
                    return;
                }
            }
            $this->_getCart()->save();
        }

        $this->jsonResponse(array('error' => false));
    }

    /**
     * Add the sim to the cart
     * @param $simType
     * @param $type
     * @return bool|Varien_Object
     * @throws Exception
     */
    public function addSim($subscription, $type, $simType, $simProd)
    {
        $sim = Mage::getModel('catalog/product')->getSimProduct($subscription, $simType, $simProd);
        try {
            /** @var Mage_Sales_Model_Quote_Item $item */
            foreach ($this->_getQuote()->getItemsCollection() as $item) {
                if (!$item->isDeleted()
                    && $item->getPackageId() == $this->_getQuote()->getActivePackageId()
                    && $item->getProduct()->isSim()
                ) {
                    $item->delete();
                }
            }

            $this->addProductToCart($sim->getId(), $type);

            return $sim;
        } catch (Exception $e) {
            Mage::getSingleton('core/logger')->logException($e);
            throw $e;
        }
    }

    /**
     * Removes products from cart.
     */
    public function removeAction()
    {
    
       if (!$this->getRequest()->isPost()) {
            return;
        }

        $id = $this->getRequest()->getParam('product');
        try {
            if ($this->removeProductFromCart($id)) {
                $this->_getCart()->save();
                $this->returnCombinations();
            } else {
                $this->jsonResponse(array(
                    'error' => true,
                    'message' => $this->__('Please provided product id'),
                ));
            }
        } catch (Exception $e) {
            Mage::getSingleton('core/logger')->logException($e);
            $this->jsonResponse(array(
                'error' => true,
                'message' => $this->__('Cannot remove the item'),
            ));
        }
    }

    /**
     * The action used for the removal of packages from the configurator.
     */
    public function removePackageAction()
    {
        $accepted = $this->getRequest()->getParam('accepted', '0');
        try {
            $packageId = $this->getRequest()->getParam('packageId');
            $expanded = $this->getRequest()->getParam('expanded');

            // check if one of deal
            $isOneOfDeal = Mage::helper('omnius_configurator/cart')->isOneOfDealActive();

            $quote = $this->_getQuote();

            // Get all the packages
            $packagesModels = Mage::getModel('package/package')->getCollection()
                ->addFieldToFilter('quote_id', $quote->getId());

            // Find the current package
            foreach($packagesModels as $packagesModel) {
                if($packagesModel->getPackageId() == $packageId) {
                    $packageModel = $packagesModel;
                }
            }

            if(!isset($packageModel) || !$packageModel->getId()) {
                throw new Exception(sprintf($this->__('Could not find package %d for quote: %d'), $packageId, $quote->getId()));
            }

            $packageIds = array((int) $packageId);
            $removeRetainables = false;
            $couponsToDecrement = array();
            if ($isOneOfDeal && $packageModel->getRetainable()) {
                    if ($accepted != '1') {
                        $this->jsonResponse(array(
                            'error'              => false,
                            'showOneOfDealPopup' => true
                        ));
                        return;
                    }
                foreach($packagesModels as $packagesModel) {
                    if($packagesModel->getOneOfDeal()) {
                        $packageIds[] = $packagesModel->getPackageId();
                        $couponsToDecrement[] = $packageModel->getCoupon();
                        $packagesModel->delete();

                    }
                }

                $removeRetainables = true;
            }

            /** @var Mage_Sales_Model_Quote_Item $item */
            foreach ($quote->getAllItems() as $item) {
                if (in_array((int)$item->getPackageId(), $packageIds)) {
                    $item->isDeleted(true);
                }
            }

            $couponsToDecrement[] = $packageModel->getCoupon();
            // Remove the package from the packages table
            $packageModel->delete();
            $customerId = $quote->getCustomerId();

            foreach($couponsToDecrement as $coupon){
                Mage::helper('pricerules')->decrementCoupon($coupon, $customerId);
            }

            if (in_array((int) $quote->getActivePackageId(), $packageIds)) {
                $quote->setActivePackageId(false);
            }

            $quote->save();

            $this->_getCart()->save();

            $packagesIndex = Mage::helper('omnius_configurator/cart')->reindexPackagesIds($quote);

            $this->jsonResponse(array(
                'error'             => false,
                'totals'            => Mage::getSingleton('core/layout')->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/totals.phtml')->toHtml(),
                'packagesIndex'     => $packagesIndex,
                'complete'          => Mage::helper('omnius_checkout')->canCompleteOrder(),
                'expanded'          => $expanded ? Mage::getSingleton('core/layout')->createBlock('core/template')->setTemplate('customer/right_sidebar.phtml')->toHtml() : '',
                'removeRetainables' => $removeRetainables ? array_values($packageIds) : false,
            ));

        } catch (Exception $e) {
            Mage::getSingleton('core/logger')->logException($e);
            $this->jsonResponse(array(
                'error' => true,
                'message' => $this->__($e->getMessage())
            ));
        }
    }

    /**
     * Method used for the duplication of packages in the configurator.
     */
    public function duplicatePackageAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }

        $packageId = $this->getRequest()->getParam('packageId');
        $type = $this->getRequest()->getParam('type');
        if ($packageId) {
            try {
                $items = array();
                $quote = $this->_getQuote();

                /** @var Omnius_Checkout_Model_Sales_Quote_Item $item */
                foreach ($quote->getAllItems() as $item) {
                    if ($item->getPackageId() == $packageId && !$item->isPromo() && !$item->getProduct()->isServiceItem()) {
                        array_push($items, array($item->getProductId() => $item));
                    }
                }
                $quote->saveActivePackageId(0);

                foreach ($items as $value) {
                    $item = current($value);
                    $newitem = $this->addProductToCart(key($value), $type);
                    $newitem->setRemoveRuleId(',');
                    $newitem->setAppliedRuleIds($item->getAppliedRuleIds());
                }

                /** @var Omnius_Package_Model_Package $oldPackageModel */
                $oldPackageModel = Mage::getModel('package/package')->getCollection()
                    ->addFieldToFilter('quote_id', $quote->getId())
                    ->addFieldToFilter('package_id', $packageId)
                    ->getFirstItem();

                // Create the new package for the quote
                /** @var Omnius_Package_Model_Package $packageModel */
                $newPackage = Mage::getModel('package/package')
                    ->setData($oldPackageModel->getData())
                    ->setCoupon(null)
                    ->setId(null)
                    ->setPackageId($quote->getActivePackageId());
                $newPackage->save();

                $quote->setCouponsPerPackage(array($newPackage->getPackageId() => $oldPackageModel->getCoupon()));

                /** Initially used by FF modules */
                Mage::dispatchEvent('configurator_cart_duplicate_package', [
                    'quote' => $quote,
                    'old_package' => $oldPackageModel,
                    'new_package' => $newPackage
                ]);

                $this->_getCart()->save();
                
                $this->jsonResponse(array(
                    'error'         => false,
                    'packageId'     => $this->_getQuote()->getActivePackageId(),
                    'type'          => $type,
                    'rightBlock'    => Mage::getSingleton('core/layout')->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/partials/package_block.phtml')->setCurrentPackageId($this->_getQuote()->getActivePackageId())->toHtml(),
                    'totals'        => Mage::getSingleton('core/layout')->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/totals.phtml')->toHtml(),
                ));
            } catch (Exception $e) {
                $this->jsonResponse(array(
                    'error' => true,
                    'message' => $e->getMessage(),
                ));
            }

        } else {
            $this->jsonResponse(array(
                'error' => true,
                'message' => 'No package id provided',
            ));
        }
    }

    /**
     * Removes the quote from session and creates another one.
     */
    public function emptyAction()
    {
        try {
            Mage::helper('pricerules')->decrementAll($this->_getCart()->getQuote());

            Mage::dispatchEvent('configurator_cart_empty', [
                'quote' => $this->_getCart()->getQuote()
            ]);

            Mage::helper('omnius_checkout')->createNewQuote();

            $this->jsonResponse(array(
                'error' => false,
                'message' => $this->__('Cart cleared'),
            ));
        } catch (Mage_Core_Exception $exception) {
            $this->jsonResponse(array(
                'error' => true,
                'message' => $this->__($exception->getMessage()),
            ));
        } catch (Exception $exception) {
            $this->jsonResponse(array(
                'error' => true,
                'message' => $this->__('Cannot update shopping cart.'),
            ));
        }

    }

    /**
     * @param $productId
     * @param $packageType
     * @param bool $isDefaulted
     * @return bool
     */
    protected function addProductToCart($productId, $packageType, $isDefaulted = false)
    {
        $productId = (int) $productId;
        $websiteId = Mage::app()->getWebsite()->getId();
        if (($emulatedWebsiteId = Mage::helper('superorder')->shouldEmulateStore($websiteId)) != $websiteId) {
            // If a product from another shop should be added, emulate that store
            $storeId = Mage::app()->getWebsite($emulatedWebsiteId)->getDefaultStore()->getId();
        } else {
            $storeId = Mage::app()->getStore()->getId();
        }
        $cart = $this->_getCart();
        if ($productId) {
            $product = Mage::getModel('catalog/product')
                ->setStoreId($storeId)
                ->load($productId);
            if (!$product->getId()) {
                Mage::throwException('Invalid product ID given');
            }
        } else {
            Mage::throwException('Invalid product ID given');
        }

        $product->setPackageType($packageType);
        $numberCtns = count($this->_getCart()->getQuote()->getPackageCtns());
        $isOneOfDeal = Mage::helper('omnius_configurator/cart')->isOneOfDealActive();

        if (!$this->exists($product)) {
            $item = $cart->addProduct($product, array(
                    'qty' => $numberCtns ?: 1,
                    'one_of_deal' => ($isOneOfDeal && $numberCtns) ? 1 : 0
                )
            );
            Mage::dispatchEvent('checkout_cart_add_product_complete',
                array('product' => $product, 'request' => $this->getRequest(), 'response' => $this->getResponse())
            );
            
            // Make sure the item has the flag so we know when to delete it
            if ($isDefaulted) {
                $item->setIsDefaulted(true);
            }

            return $item;
        }

        return false;
    }

    /**
     * @param $productId
     * @return bool
     */
    protected function removeProductFromCart($productId)
    {
        if (is_array($productId)) {
            $ids = array_filter(array_filter($productId, 'trim'));
            foreach ($ids as $id) {
                $item = $this->_getQuote()->getItemsCollection()->getItemsByColumnValue('product_id', $id);
                $item = reset($item);
                if ($item) {
                    $this->_getCart()
                        ->removeItem($item->getId());
                }
            }
        } else {
            $id = (int) $productId;
            $item = $this->_getQuote()->getItemsCollection()->getItemsByColumnValue('product_id', $id);
            $item = reset($item);
            if ($item) {
                $this->_getCart()->removeItem($item->getId());
            }
        }
        return true;
    }

    /**
     * Checks whether a product already exists in the current package of the shopping cart.
     * @param Mage_Catalog_Model_Product $product
     * @return bool
     */
    protected function exists(Mage_Catalog_Model_Product $product)
    {
        /** @var Mage_Sales_Model_Quote_Item $quoteItem */
        foreach ($this->_getQuote()->getItemsCollection() as $quoteItem) {
            if (!$quoteItem->isDeleted() && $quoteItem->getPackageId() == $this->_getQuote()->getActivePackageId() &&
                $quoteItem->getProductId() == $product->getId()) {
                    return true;
            }
        }
        return false;
    }

    /**
     * Initialize product instance from request data
     *
     * @return Mage_Catalog_Model_Product || false
     */
    protected function _initProduct()
    {
        $productId = (int) $this->getRequest()->getParam('product');
        if ($productId) {
            $product = Mage::getModel('catalog/product')
                ->setStoreId(Mage::app()->getStore()->getId())
                ->load($productId);
            if ($product->getId()) {
                return $product;
            }
        }
        return false;
    }

    /**
     * Retrieve shopping cart model object
     *
     * @return Mage_Checkout_Model_Cart
     */
    protected function _getCart()
    {
        return Mage::getSingleton('checkout/cart');
    }

    /**
     * Get checkout session model instance
     *
     * @return Mage_Checkout_Model_Session
     */
    protected function _getSession()
    {
        return Mage::getSingleton('checkout/session');
    }

    /**
     * @return Mage_Customer_Model_Session
     */
    protected function _getCustomerSession()
    {
        return Mage::getSingleton('customer/session');
    }

    /**
     * Get current active quote instance
     *
     * @return Omnius_Checkout_Model_Sales_Quote
     */
    protected function _getQuote()
    {
        return $this->_getCart()->getQuote();
    }

    /**
     * @return Dyna_ProductMatchRule_Model_Rule
     */
    protected function getRuler()
    {
        if ( ! $this->_ruler) {
            $this->_ruler = Mage::getSingleton('productmatchrule/rule');
        }
        return $this->_ruler;
    }

    /**
     * @return Omnius_Configurator_Helper_Attribute
     */
    protected function getAttrHelper()
    {
        if ( ! $this->_attrHelper) {
            $this->_attrHelper = Mage::helper('omnius_configurator/attribute');
        }
        return $this->_attrHelper;
    }

    /**
     * @return Omnius_Checkout_Helper_Data
     */
    protected function getCheckoutHelper()
    {
        if ( ! $this->_checkoutHelper) {
            $this->_checkoutHelper = Mage::helper('omnius_checkout');
        }
        return $this->_checkoutHelper;
    }

    /**
     * @return Omnius_Configurator_Helper_Cart
     */
    protected function getCartHelper()
    {
        if ( ! $this->_cartHelper) {
            $this->_cartHelper = Mage::helper('omnius_configurator/cart');
        }
        return $this->_cartHelper;
    }

    /**
     * @param $data
     * @param int $statusCode
     * @throws InvalidArgumentException
     */
    protected function jsonResponse($data, $statusCode = 200)
    {
        $response = $this->getDynaCoreHelper()
            ->jsonResponse($data, $statusCode);

        $this->getResponse()
            ->setBody($response['body'])
            ->setHeader('Content-Type', 'application/json')
            ->setHttpResponseCode($response['status']);
    }
    
    /**
     * Calculate shopping cart totals
     *
     * @return array
     */
    public function getCartTotals()
    {
        $quote = $this->_getQuote();
        $total = array('pm' => 0, 'pm_tax' => 0, 'eenmalig' => 0, 'eenmalig_tax' => 0);
        /** @var Omnius_Checkout_Model_Sales_Quote_Item $item */
        foreach($quote->getItemsCollection() as $item) {
            if ( ! $item->isDeleted()) {
                $total["pm_tax"] += $item->getTaxAmount();
                $total["pm"] += $item->getRowTotal() - $item->getTaxAmount();
                $total["eenmalig_tax"] += $item->getTaxAmount();
                $total["eenmalig"] += $item->getRowTotal() - $item->getTaxAmount();
            }
        }
        $total["subtotal"] = ($quote->getSubtotal() ? number_format($quote->getSubtotal(), 2, '.', '') : 0);
        $total["grandtotal"] = ($quote->getGrandTotal() ? number_format($quote->getGrandTotal(), 2, '.', '') : 0);
        return $total;
    }

    /**
     * Returns list of products that are not allowed based on current selection together with package totals
     * @param bool|false $simError
     * @param null $simName
     * @param array $addedDefault
     * @param array $removedDefault
     * @param null $packageStatus
     */
    private function returnCombinations($simError = false, $simName = null, $addedDefault = [], $removedDefault = [], $packageStatus = null)
    {
        // From this point models can be cached
        Mage::register('freeze_models', true, true);
        $totalsUpdated = Mage::helper('omnius_configurator')->getActivePackageTotals();
        $totals = Mage::getSingleton('core/layout')->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/totals.phtml')->toHtml();
        $packageId = $this->_getQuote()->getActivePackageId();
        $rightBlock = Mage::getSingleton('core/layout')->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/partials/package_block.phtml')->setCurrentPackageId($packageId)->toHtml();
        $promoRules = $this->_priceRulesHelper->findApplicablePromoRules(true);

        $response = array(
            'error' => false,
            'activePackageId' => $packageId,
            'totalMaf' => $totalsUpdated['totalmaf'],
            'totalPrice' => $totalsUpdated['totalprice'],
            'totals' => $totals,
            'rightBlock' => $rightBlock,
            'promoRules' => $promoRules,
            'complete' => Mage::helper('omnius_checkout')->canCompleteOrder(),
        );

        if ($packageStatus === null) {
            $packageStatus = Mage::helper('omnius_checkout')->checkPackageStatus($packageId);
        }
        if (($incompleteSections = $packageStatus) && is_array($incompleteSections)) {
            $response['incomplete_sections'] = $incompleteSections;
        }
        $closure = function ($el) {
            return ['product_id' => $el->getProductId(), 'type' => strtolower($el->getProduct()->getTypeText())];
        };
        if ($addedDefault) {
            $response['addedDefaultedItems'] = array_map($closure, $addedDefault);
        }
        if ($removedDefault) {
            $response['removedDefaultedItems'] = array_map($closure, $removedDefault);
        }
        if ($simError) {
            $response['simError'] = $simError;
        } else {
            $response['sim'] = $simName;
        }

        $this->jsonResponse($response);
    }

    /**
     * Deletes a quote specified in a post call.
     */
    public function deleteSavedQuoteAction()
    {
        if ($this->getRequest()->isPost()){
            $param = $this->getRequest()->get('quote_id');
            if($param){
                try{
                    $quote = Mage::getModel('sales/quote')->getCollection()
                        ->addFieldToFilter('entity_id', $param)
                        ->getFirstItem();

                    if ($quote->getIsOffer()) {
                        Mage::helper('pricerules')->decrementAll($quote);
                    }

                    $quote->setIsActive(false);
                    $quote->delete();
                    $this->jsonResponse(array(
                        'error' => false
                    ));
                } catch(Exception $e) {
                    $this->jsonResponse(array(
                        'error'     => true,
                        'message'   => $e->getMessage()
                    ));
                }
            }else {
                $this->jsonResponse(array(
                    'error'     => true,
                    'message'   => Mage::helper('omnius_checkout')->__('The quote id was not specified.')
                ));
            }
        }
    }

    /**
     * Compute if the combination has a defaulted item that is not in the package
     * 
     * @param $products
     * @return array
     */
    protected function getDefaultedProducts($products)
    {
        $current = [];
        foreach ($products as $product) {
            $current[$product] = $product;
        }
        $rules = Mage::helper('productmatchrule')->getDefaultRules($current);
        $found = array_intersect_key($rules, $current);
        foreach ($found as $productId => $defaulted) {
            foreach ($defaulted as $item) {
                if (isset($current[$item])) {
                    unset($found[$productId]);
                }
            }
        }

        return $found;
    }

    /**
     * Compute if the item being removed has a defaulted item that should also be removed
     * 
     * @param $removedItem
     * @param $allItems
     * @return array
     */
    protected function getDefaultedProductsForRemoval($removedItem, $allItems)
    {
        $current = [];
        foreach ($allItems as $item) {
            $current[$item->getProductId()] = $item->getProductId();
        }
        $rules = Mage::helper('productmatchrule')->getDefaultRules($current);
        $found = [];
        $productId = $removedItem->getProductId();
        $packageId = $removedItem->getPackageId();
        if (isset($rules[$productId])) {
            foreach ($allItems as $foundItem) {
                if ($packageId == $foundItem->getPackageId() && in_array($foundItem->getProductId(), $rules[$productId]) && $foundItem->getIsDefaulted()) {
                    $found[$foundItem->getProductId()] = $foundItem;
                }
            }
        }

        return $found;
    }

    /**
    * Action for to be audited when multiple tabs are opened
    */
    public function newtabAction()
    {
        $this->jsonResponse(array(
            'error'     => false,
            'message'   => "new tab request"
        ));
    }

    /**
     * Empties a product section from the configurator.
     * @param $section
     * @param $packageId
     * @param $type
     * @param $packageModel
     */
    protected function removeAllItems($section, $packageId, $type, $packageModel)
    {
        $removeSim = false;
        $removedDefaultFlag = false;
        $hasSimOnly = false;
        /** @var Omnius_Checkout_Model_Sales_Quote_Item $item */
        foreach ($this->_getQuote()->getItemsCollection() as $item) {
            if ($item->getPackageId() != $packageId || $item->isDeleted()) {
                continue;
            }
            if (strtolower($item->getPackageType()) == strtolower($type)
                && $item->getProduct()->isInSection($section)
            ) {
                $this->doRemoveItemOfAll($packageModel, $item, $removedDefaultFlag, $removeSim, $removedDefault);
            }
        }

        /**
         * Needs second iteration to remove the sim products
         */
        if ($removeSim) {
            foreach ($this->_getQuote()->getItemsCollection() as $item) {
                if ($item->getPackageId() == $packageId && !$item->isDeleted() && $item->getProduct()->isSim()) {
                    $this->_getCart()->removeItem($item->getId());
                }
            }
        }
        $this->_getCart()->save();
        /** @var Omnius_Package_Model_Package $packageModel */
        $packageModel = Mage::getModel('package/package')->getCollection()
            ->addFieldToFilter('quote_id', $this->_getQuote()->getId())
            ->addFieldToFilter('package_id', $packageId)
            ->getFirstItem();
        $packageStatus = Mage::helper('omnius_checkout')->checkPackageStatus($packageId);

        $packageModel->setCurrentStatus(Omnius_Configurator_Model_AttributeSet::PACKAGE_STATUS_COMPLETED === $packageStatus
            ? Omnius_Configurator_Model_AttributeSet::PACKAGE_STATUS_COMPLETED
            : Omnius_Configurator_Model_AttributeSet::PACKAGE_STATUS_OPEN);
        $packageModel->updateDefaultedRemoved($removedDefaultFlag);
        $packageModel->save();

        $this->returnCombinations(false, null, false, [], isset($removedDefault) ? $removedDefault : [], $packageStatus);
    }

    /**
     * @param $sim
     * @param $package
     * @param $type
     * @param $oldSim
     * @param $simName
     * @param $simError
     */
    protected function addSimProductToCart($sim, $package, $type, $oldSim, &$simName, &$simError)
    {
        if ($sim && ($sub = $package->hasSubscription($this->_getQuote()))) {
            $simProd = Mage::getModel('catalog/product')->loadByAttribute('name', $sim);
            if ($simProd) {
                $simT = $simProd->getAttributeText(Omnius_Catalog_Model_Product::DEFAULT_SIM_ATTRIBUTE);
                if (Mage::helper('omnius_configurator/attribute')->checkSimTypeAvailable($sub->getProduct(), $simT)) {
                    try {
                        $sim = Mage::helper('omnius_configurator/attribute')->checkSubscriptionAllowsSim($sub->getProduct(), $sim);
                        $addedSim = $this->addSim($sub, $type, $simT, $sim);
                        $simName = $addedSim->getName();
                    } catch (Exception $e) {
                        // Exception already logged in addSim method
                        $simError = sprintf('%s %s: ', Mage::helper('omnius_configurator')->__('Simcard'), $sim);
                        $simError .= $e->getMessage();
                    }
                }
            }
        } elseif (($sub = $package->hasSubscription($this->_getQuote())) && $oldSim != 'true' && ($sim = $package->hasHardwareWithoutSim($this->_getQuote()))) {
            try {
                $addedSim = $this->addSim($sub, $type, $sim, null);
                $simName = $addedSim->getName();
            } catch (Exception $e) {
                // Exception already logged in addSim method
                $simError = sprintf('%s %s: ', Mage::helper('omnius_configurator')->__('Simcard'), $sim);
                $simError .= $e->getMessage();
            }
        }
    }

    /**
     * @param $added
     * @param $packageModel
     * @param $finalProducts
     * @param $allExclusiveFamilyCategoryIds
     * @param $exclusiveFamilyCategoryIds
     * @param $packageType
     * @return array
     */
    protected function parseDefaultedProducts($added, $packageModel, $finalProducts, &$allExclusiveFamilyCategoryIds, &$exclusiveFamilyCategoryIds, $packageType)
    {
        // Check if there are "defaulted" products, and if there are, add them to the cart
        $addedDefault = $this->getDefaultedProducts($added);
        $addedDefaultItems = [];

        if ($addedDefault && count($addedDefault) && $packageModel->allowDefaulted()) {
            // Recalculate valid combinations
            foreach ($addedDefault as $toAdd) {
                // Check if the products are still compatible
                $validProducts = Mage::helper('omnius_configurator/cart')->getCartRules($finalProducts);
                $arrayToAdd = array_intersect($toAdd, $validProducts);
                if (count($arrayToAdd)) {
                    $this->addDefaultedProducts($finalProducts, $allExclusiveFamilyCategoryIds, $exclusiveFamilyCategoryIds, $packageType, $arrayToAdd, $addedDefaultItems);
                }
            }
        }

        return $addedDefaultItems;
    }

    /**
     * Checks whether the product is eligible with the rest of the cart.
     * @param $productsInCart
     * @param $allItems
     * @param $packageId
     * @param $validProducts
     * @param $item
     * @throws Exception
     */
    protected function validateProducts($productsInCart, $allItems, $packageId, &$validProducts, &$item)
    {
        $validProducts = Mage::helper('omnius_configurator/cart')->getCartRules($productsInCart);
        /** @var Mage_Sales_Model_Quote_Item $item */
        foreach ($allItems as $item) {
            if ($item->getPackageId() == $packageId) {
                if ($item->getProduct()->isSim()) {
                    $item->isDeleted(true);
                } elseif ($item->getProduct()->getType() && !$item->getProduct()->is(Omnius_Catalog_Model_Type::SUBTYPE_NONE)) {
                    if (
                        in_array($item->getProduct()->getId(), $validProducts)
                        || $item->getProduct()->isServiceItem()
                    ) {
                        continue;
                    }
                    throw new Exception($this->__('Cannot add the item to shopping cart. Invalid match rule.'));
                }
            }
        }
    }

    /**
     * @param $finalProducts
     * @param $allExclusiveFamilyCategoryIds
     * @param $exclusiveFamilyCategoryIds
     * @param $packageType
     * @param $arrayToAdd
     * @param $addedDefaultItems
     */
    protected function addDefaultedProducts(&$finalProducts, &$allExclusiveFamilyCategoryIds, &$exclusiveFamilyCategoryIds, $packageType, $arrayToAdd, &$addedDefaultItems, $productsWithFlags = [])
    {
        $currentWebsite = Mage::app()->getWebsite()->getId();
        /** @var Omnius_Catalog_Model_Resource_Product_Collection $defaultedCollection */
        $defaultedCollection = Mage::getModel('catalog/product')->getCollection()
            ->addWebsiteFilter($currentWebsite)
            ->addIdFilter($arrayToAdd);
        foreach ($arrayToAdd as $itemToAdd) {
            // If the item is Addon, make sure there is no other addon in an mutually exclusive family
            $product = $defaultedCollection->getItemById($itemToAdd);
            if (!$product) {
                // Skip products that don't exist
                continue;
            }
            // Product is part of an mutually exclusive category
            if ($categoriesExclusive = array_intersect($product->getCategoryIds(), $allExclusiveFamilyCategoryIds)) {
                if (array_intersect($product->getCategoryIds(), $exclusiveFamilyCategoryIds)) {
                    // There is already a product in this mutually exclusive category
                    continue;
                } else {
                    // Add the categories to the list so we make sure the next product is not in them
                    foreach ($categoriesExclusive as $categoryExclusive) {
                        $exclusiveFamilyCategoryIds[$product->getId()] = $categoryExclusive;
                    }
                }
            }
            $newItem = $this->addProductToCart($itemToAdd, $packageType, $isDefaulted = true);
            if ($newItem) {
                $addedDefaultItems[] = $newItem;
                // Add the product to the list to make sure the next ones are also compatible with it
                array_push($finalProducts, $itemToAdd);
            }
        }
    }

    /**
     * @param $packageId
     * @param $productId
     * @param $type
     * @param $appliedRulesArray
     * @param $added
     */
    protected function parseProduct($packageId, $productId, $type, $appliedRulesArray, &$added)
    {
        Mage::unregister('add_multi_package_id');
        Mage::register('add_multi_package_id', $packageId);
        $item = $this->addProductToCart($productId, $type);

        if ($item && !empty($appliedRulesArray) && isset($appliedRulesArray[$item->getPackageId() . '-' . $item->getProductId()])) {
            $item->setAppliedRuleIds(implode(',', $appliedRulesArray[$item->getPackageId() . '-' . $item->getProductId()]));
        }

        array_push($added, $productId);
    }

    /**
     * @param $productsInCart
     * @param $item
     * @param $exclusiveFamilyCategoryIds
     * @param $productsInCartKey
     * @param $productInCartId
     */
    protected function parseProductsInCart($productsInCart, $item, $exclusiveFamilyCategoryIds, &$productsInCartKey, &$productInCartId)
    {
        foreach ($productsInCart as $productsInCartKey => $productInCartId) {
            if ($productInCartId == $item->getProductId()) {
                unset($productsInCart[$productsInCartKey]);
                unset($exclusiveFamilyCategoryIds[$item->getProductId()]);
            }
        }
    }

    /**
     * @param $item
     * @param $productsInCart
     * @param $exclusiveFamilyCategoryIds
     * @param $packageModel
     * @param $allItems
     * @param $deletedItems
     * @param $itemKey
     * @param $removedDefaultFlag
     * @param $removedDefault
     */
    protected function removeItemAndParseDefault($item, $productsInCart, &$exclusiveFamilyCategoryIds, $packageModel, $allItems, &$deletedItems, $itemKey, &$removedDefaultFlag, &$removedDefault, $type = "")
    {
        $this->_getCart()->removeItem($item->getId());
        if ($item->getIsDefaulted()) {
            $removedDefaultFlag = true;
        }
        if (in_array($item->getProductId(), $productsInCart)) {
            $this->parseProductsInCart($productsInCart, $item, $exclusiveFamilyCategoryIds, $productsInCartKey, $productInCartId);
        }
        if ($packageModel->allowDefaulted()) {
            // Check if the removed item had defaulted items, and remove them
            $removedDefault = $this->getDefaultedProductsForRemoval($item, $allItems);
            foreach ($removedDefault as $pI) {
                $this->doRemoveItem($item, $productsInCart, $exclusiveFamilyCategoryIds, $deletedItems, $pI);
            }
        }
        unset($allItems[$itemKey]);
    }

    /**
     * @param $item
     * @param $productsInCart
     * @param $exclusiveFamilyCategoryIds
     * @param $deletedItems
     * @param $pI
     */
    protected function doRemoveItem($item, $productsInCart, &$exclusiveFamilyCategoryIds, &$deletedItems, $pI)
    {
        $this->_getCart()->removeItem($pI->getId());
        $deletedItems[$pI->getId()] = true;
        if (in_array($pI->getProductId(), $productsInCart)) {
            foreach ($productsInCart as $productsInCartKey => $productInCartId) {
                if ($productInCartId == $pI->getProductId()) {
                    unset($productsInCart[$productsInCartKey]);
                    unset($exclusiveFamilyCategoryIds[$item->getProductId()]);
                }
            }
        }
    }

    /**
     * @param $packageModel
     * @param $item
     * @param $removedDefaultFlag
     * @param $removeSim
     * @param $removedDefault
     */
    protected function doRemoveItemOfAll($packageModel, $item, &$removedDefaultFlag, &$removeSim, &$removedDefault)
    {
        $this->_getCart()->removeItem($item->getId());
        if ($item->getIsDefaulted()) {
            $removedDefaultFlag = true;
        }

        if ($packageModel->allowDefaulted()) {
            // Check if the removed item had defaulted items, and remove them
            $removedDefault = $this->getDefaultedProductsForRemoval($item, $this->_getQuote()->getItemsCollection());
            foreach ($removedDefault as $pI) {
                $this->_getCart()->removeItem($pI->getId());
            }
        }
    }
} 
