<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Package Helper
 */
class Omnius_Package_Helper_Data extends Mage_Core_Helper_Abstract
{
    const XML_PATH_SALES_GENERAL_PATH = 'sales/general';

    /**
     * @return array
     *
     * Get list of packages that have been cancelled
     */
    public function getCancelledPackages()
    {
        $sessionQuoteId = Mage::getSingleton('customer/session')->getOrderEdit();
        $cancelOrders = Mage::getSingleton('customer/session')->getCancelledPackages() ?: array($sessionQuoteId => array());

        return !empty($cancelOrders[$sessionQuoteId]) ? $cancelOrders[$sessionQuoteId] : [];
    }

    /**
     * @param int $packageId
     *
     * Update list of cancelled packages
     */
    public function updateCancelledPackages($packageId)
    {
        $sessionQuoteId = Mage::getSingleton('customer/session')->getOrderEdit();
        $cancelOrders = Mage::getSingleton('customer/session')->getCancelledPackages() ?: array($sessionQuoteId => array());
        $cancelOrders[$sessionQuoteId] = isset($cancelOrders[$sessionQuoteId]) ? $cancelOrders[$sessionQuoteId] : array();
        if(in_array($packageId, $cancelOrders[$sessionQuoteId]) === false){
            $cancelOrders[$sessionQuoteId][] = $packageId;
        }

        Mage::getSingleton('customer/session')->setCancelledPackages($cancelOrders);
    }

    /**
     * @return array
     *
     * Get list of packages that have been cancelled during the current session
     */
    public function getCancelledPackagesNow()
    {
        $sessionQuoteId = Mage::getSingleton('customer/session')->getOrderEdit();
        $cancelOrders = Mage::getSingleton('customer/session')->getCancelledPackagesNow() ?: array($sessionQuoteId => array());

        return isset($cancelOrders[$sessionQuoteId]) ? $cancelOrders[$sessionQuoteId] : array();
    }

    /**
     * @param int|mixed $params
     * Update list of cancelled packages during the current session
     */
    public function updateCancelledPackagesNow($params)
    {
        $sessionQuoteId = Mage::getSingleton('customer/session')->getOrderEdit();
        $packagesIds = array_map('trim', explode(',', $params));
        $cancelPackages = Mage::getSingleton('customer/session')->getCancelledPackagesNow() ?: array($sessionQuoteId => array());
        $cancelPackages[$sessionQuoteId] = isset($cancelPackages[$sessionQuoteId]) ? $cancelPackages[$sessionQuoteId] : array();

        //@todo event needed to be caught for custom implementation

        Mage::dispatchEvent("omnius_package_update_cancelled_packages_now", [
            'packageIds' => $packagesIds,
            'cancelPackages' => $cancelPackages,
            'sessionQuoteId' => $sessionQuoteId,
        ]);
    }

    /**
     * Remove the cancelled tag from all items
     */
    public function resetCancelledPackagesNow()
    {
        $sessionQuoteId = Mage::getSingleton('customer/session')->getOrderEdit();
        $cancelOrders = Mage::getSingleton('customer/session')->getCancelledPackagesNow() ? : array($sessionQuoteId => array());
        $cancelOrders[$sessionQuoteId] = isset($cancelOrders[$sessionQuoteId]) ? $cancelOrders[$sessionQuoteId] : array();
        /** @var Omnius_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getModel('sales/quote')->load($sessionQuoteId);
        $quote->setTotalsCollectedFlag(true);
        foreach ($quote->getAllItems() as $item) {
            if (in_array($item->getPackageId(), $cancelOrders[$sessionQuoteId])) {
                $item->setIsCancelled(0)->save();
            }
        }
        $quote->setTotalsCollectedFlag(false);
        $quote->collectTotals();
        $quote->save();
    }
    
    /**
     * @param int $packageId
     *
     * Remember package differences on session to avoid persisting them and to avoid re-calculating them
     */
    public function savePackageDifferences($packageId)
    {
        $sessionQuoteId = Mage::getSingleton('customer/session')->getOrderEdit();
        $packageDifferences = Mage::getSingleton('customer/session')->getPackageDifferences() ?: array($sessionQuoteId => array());
        $packageDifferences[$sessionQuoteId][$packageId] = Mage::helper('omnius_checkout')->getEditPackagePriceDifference($packageId);

        Mage::getSingleton('customer/session')->setPackageDifferences($packageDifferences);
    }

    /**
     * @param $packageId
     * @return array
     *
     * Return array with package differences
     */
    public function getPackageDifferences($packageId)
    {
        $sessionQuoteId = Mage::getSingleton('customer/session')->getOrderEdit();
        $packageDifferences = Mage::getSingleton('customer/session')->getPackageDifferences();

        return isset($packageDifferences[$sessionQuoteId][$packageId]) ? $packageDifferences[$sessionQuoteId][$packageId] : array();
    }

    /**
     * Checks if at least one delivered package has been modified
     * @return bool
     */
    public function isDeliveredPackageModified()
    {
        $quotes = $this->getModifiedQuotes();
        if (count($quotes)) {
            /** @var Omnius_Checkout_Model_Sales_Quote $quote */
            foreach ($quotes as $quote) {
                /** @var Omnius_Package_Model_Mysql4_Package_Collection $package */
                $package = Mage::getModel('package/package')->getCollection()
                    ->addFieldToFilter('order_id', $quote->getSuperOrderEditId())
                    ->addFieldToFilter('package_id', $quote->getActivePackageId());
                /** @var Omnius_Package_Model_Package $obj */
                $obj = $package->getFirstItem();

                if ($obj->getId() && $obj->isDelivered()) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @return Omnius_Checkout_Model_Sales_Quote[]
     *
     * Retrieves a list of quotes that were saved during this session of order edit
     */
    public function getModifiedQuotes()
    {
        $superQuoteId = Mage::getSingleton('customer/session')->getOrderEdit();
        /** @var Omnius_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getModel('sales/quote')->load($superQuoteId);

        return $quote->getListOfModifiedQuotes($quote->getSuperOrderEditId());
    }
	
    /**
     * @param Omnius_Checkout_Model_Sales_Quote_Item[] $packageItems
     * @return bool
     */
    public function subscriptionHasPromo($packageItems){
        foreach ($packageItems as $item) {
            if ($item->isPromo()) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return int
     */
    public function getPackageDateLimit()
    {
        $limit = (int) Mage::getStoreConfig(sprintf('%s/package_date_limit', self::XML_PATH_SALES_GENERAL_PATH));

        return $limit ?: 10;
    }

    /**
     * Generate the pagination footer
     * 
     * @param $pagesCount
     * @param $currentPage
     * @param $size
     * @return array
     * @internal param $max
     * @internal param $current
     */
    public function getPagesIds($pagesCount, $currentPage, $size)
    {
        $res = [];
        $size = min($pagesCount, $size);
        $bottom = $currentPage - (int) ($size / 2);
        $top = $currentPage + (int) ($size / 2) - ($size % 2 === 0 ? 1 : 0);
        $shift = 0;
        if ($bottom < 1) {
            $shift = 1 - $bottom;
        } elseif ($top > $pagesCount) {
            $shift = $pagesCount - $top;
        }
        $top += $shift;
        $bottom += $shift;

        if ($bottom > 1) {
            $res[] = 1;
            if ($bottom > 2) {
                $res[] = '...';
            }
        }
        for ($i = $bottom; $i <= $top; ++$i) {
            $res[] = $i;
        }

        if ($top < $pagesCount) {
            if ($top < $pagesCount - 1) {
                $res[] = '...';
            }
            $res[] = $pagesCount;
        }

        return $res;
    }

    /**
     * @param $price
     * @return mixed
     */
    protected function formatPrice($price)
    {
        return str_replace(',', '.', $price);
    }

    /**
     * @param Omnius_Package_Model_Package $package
     * @param Omnius_Checkout_Model_Sales_Order
     * @param bool $keepAddress
     * @return Omnius_Checkout_Model_Sales_Quote
     */
    public function buildQuoteFromPackage($package, $baseOrder, $keepAddress = false)
    {
        $shippingAddressData = $keepAddress ? $baseOrder->getShippingAddress()->getData() : $package->getAddress();
        /** @var Omnius_Checkout_Model_Sales_Quote_Address $shippingAddress */
        $shippingAddress = Mage::getModel('sales/quote_address')
            ->setData($shippingAddressData);

        $shippingAddress->setCollectShippingRates(true);
        $billingAddress = Mage::getModel('sales/quote_address');
        $billingAddressData = $baseOrder->getBillingAddress()->getData();
        $billingAddress->setData($billingAddressData)
            ->setEntityId(null)
            ->setParentId(null);

        $packagePayment = $package->getPayment();

        /** @var Omnius_Checkout_Model_Sales_Quote $newQuote */
        $newQuote = Mage::getModel('sales/quote');

        $newQuote->setShippingData(Mage::helper('core')->jsonEncode(['deliver' => $shippingAddressData]));

        $newQuote->setData($baseOrder->getBaseQuote()->getData())
            ->setIsSuperMode(true)
            ->setData('entity_id', null)
            ->setBillingAddress($billingAddress)
            ->setShippingAddress($shippingAddress)
            ->save();

        $newQuote->setPaymentMethod($packagePayment['method']);
        $newQuote->setCustomShipping($shippingAddressData);
        return $newQuote;
    }
}
