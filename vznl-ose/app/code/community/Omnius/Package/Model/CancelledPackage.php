<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Package_Model_CancelledPackage
 */
class Omnius_Package_Model_CancelledPackage extends Omnius_Package_Model_Package
{
    public function _construct()
    {
        $this->_binaryState = Omnius_Package_Helper_Builder::CANCELLED_PACKAGE;
        parent::_construct();
    }

    /**
     * @return $this
     */
    public function build()
    {
        // Set old address as the package address.
        $address = $this->getProcessedOrder()->getShippingAddress()->getData();
        //Unset indexes as these addresses are used to generate hashes based on which we generate the final orders.
        unset($address['entity_id'], $address['parent_id'], $address['quote_address_id']);
        $this->setItems($this->getOldItems())->setAddress($address);
        $this->setCancelled(true);
        return parent::build();
    }
}