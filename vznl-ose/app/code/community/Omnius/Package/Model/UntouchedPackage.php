<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Package_Model_UntouchedPackage extends Omnius_Package_Model_Package
{
    /**
     * @return $this
     */
    public function build()
    {
        $this->setItems($this->getOldItems());
        // Set old address as the package address.
        $address = $this->getProcessedOrder()->getShippingAddress()->getData();
        //Unset indexes as these addresses are used to generate hashes based on which we generate the final orders.
        unset($address['entity_id'], $address['parent_id'], $address['quote_address_id']);
        $this->setAddress($address);
        return parent::build();
    }
}