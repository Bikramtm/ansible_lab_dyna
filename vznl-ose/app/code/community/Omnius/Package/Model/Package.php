<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Package_Model_Package
 * @method Omnius_Package_Model_Mysql4_Package_Collection getCollection()
 * @method int getQuoteId()
 * @method int getOrderId()
 * @method array getPayment()
 * @method string getManualActivationReason()
 * @method string getEsbManualActivation()
 * @method Omnius_Package_Model_Package setPayment(array $array)
 */
class Omnius_Package_Model_Package extends Mage_Core_Model_Abstract
{
    const ESB_PACKAGE_STATUS_WAITING_FOR_VALIDATION = 'Waiting for Validation';
    const ESB_PACKAGE_STATUS_PACKAGE_VALIDATED = 'Package Validated';
    const ESB_PACKAGE_STATUS_BACK_ORDER = 'Back Order';
    const ESB_PACKAGE_STATUS_ALL_RESOURCES_RESERVED = 'All Resources Reserved';
    const ESB_PACKAGE_STATUS_PORTING_PENDING = 'Porting Pending';
    const ESB_PACKAGE_STATUS_ADDITIONAL_INFO_REQUIRED = 'Additional info required';
    const ESB_PACKAGE_STATUS_PORTING_APPROVED = 'Porting Approved';
    const ESB_PACKAGE_STATUS_PORTING_REJECTED = 'Porting Rejected';
    const ESB_PACKAGE_STATUS_READY_FOR_FULFILLMENT = 'Ready for fulfillment';
    const ESB_PACKAGE_STATUS_PACKED = 'Picked';
    const ESB_PACKAGE_STATUS_ON_ITS_WAY = 'On its Way';
    const ESB_PACKAGE_STATUS_DELIVERED = 'Delivered';
    const ESB_PACKAGE_STATUS_NOT_DELIVERED = 'Not delivered';
    const ESB_PACKAGE_STATUS_RETURNED = 'Returned';
    const ESB_PACKAGE_STATUS_CANCELLED = 'Cancelled';
    const ESB_PACKAGE_STATUS_RESERVED = 'Reserved';
    const ESB_PACKAGE_STOCK_STATUS_RESERVED = 'RESERVED';
    const ESB_PACKAGE_STATUS_FAILED = 'Validation Failed';
    const ESB_PACKAGE_STATUS_INITIAL = 'Initial';

    const ESB_PACKAGE_NETWORK_STATUS_CC_PENDING = 'PENDING';
    const ESB_PACKAGE_NETWORK_STATUS_CC_REFERRED = 'REFERRED';
    const ESB_PACKAGE_NETWORK_STATUS_CC_ADDITIONNAL_INFO_REQUIRED = 'ADDITIONNAL_INFO_REQUIRED';
    const ESB_PACKAGE_NETWORK_STATUS_CC_REJECTED = 'REJECTED';
    const ESB_PACKAGE_NETWORK_STATUS_CC_APPROVED = 'APPROVED';
    const ESB_PACKAGE_NETWORK_STATUS_CC_VALIDATION_FAILED = "VALIDATION_FAILED";
    const ESB_PACKAGE_NETWORK_STATUS_CC_INITIAL = "INITIAL";

    const ESB_PACKAGE_NETWORK_STATUS_NP_PENDING = 'PENDING';
    const ESB_PACKAGE_NETWORK_STATUS_NP_NOT_SOLVABLE = 'NOT_SOLVABLE';
    const ESB_PACKAGE_NETWORK_STATUS_NP_AUTHORIZED = 'APPROVED';
    const ESB_PACKAGE_NETWORK_STATUS_NP_REJECTED = 'REJECTED';
    const ESB_PACKAGE_NETWORK_STATUS_NP_INITIAL = 'INITIAL';

    const PACKAGE_SUCCESS = 'success';
    const PACKAGE_WAITING = 'waiting';
    const PACKAGE_FAILED = 'failed';
    const PACKAGE_REFERRED = 'referred';
    const PACKAGE_REJECTED = 'rejected';

    const PACKAGE_STATE_AVAILABLE = 'Available';
    const NEVER = 'not allowed';

    const MAX_LINES_TOP_MENU = 10;
    const MAX_PAGES_TOP_MENU = 9;
    /** @var Dyna_Cache_Model_Cache */
    protected $_cache;

    /** @var Omnius_Checkout_Model_Sales_Quote_Item[] */
    protected $_oldItems = [];

    /** @var Omnius_Checkout_Model_Sales_Quote|null */
    protected $_modifiedQuote = null;

    /** @var Omnius_Checkout_Model_Sales_Quote_Address|null */
    protected $_newAddress = null;

    /** @var Omnius_Checkout_Model_Sales_Order */
    protected $_processedOrder = null;

    /**
     * Init as untouched package.
     * For more in-depth description of the package states, please @see Omnius_Package_Helper_Builder.
     * @var int
     */
    protected $_binaryState = 0b0;

    protected $statusTypes = array(
        Omnius_Superorder_Model_StatusHistory::PACKAGE_STATUS,
        Omnius_Superorder_Model_StatusHistory::PACKAGE_CC_STATUS,
        Omnius_Superorder_Model_StatusHistory::PACKAGE_ESB_STATUS_CODE,
        Omnius_Superorder_Model_StatusHistory::PACKAGE_PORTING_STATUS,
        Omnius_Superorder_Model_StatusHistory::PACKAGE_VF_STATUS_CODE,
        Omnius_Superorder_Model_StatusHistory::PACKAGE_STOCK_STATUS,
        Omnius_Superorder_Model_StatusHistory::PACKAGE_CREDIT_CODE
    );
    protected $hasChanged;
    protected $removedProducts;
    protected $quoteItems = array();
    protected $orderItems = array();
    protected $_needsApprove = null;
    /** @var Omnius_Checkout_Model_Sales_Order */
    protected $_deliveryOrder = null;
    /** @var Omnius_Superorder_Model_Superorder */
    protected $_superOrder = null;
    /** @var string */
    protected $_contractSignDate = null;
    protected $acceptedSections = array(
        'creditcheck',
        'porting',
        'validation',
        'openorders',
        'customer-screen-orders',
    );

    /** @var  Omnius_Checkout_Model_Sales_Quote */
    protected $quote = null;

    /**
     * @return array
     *
     * Get default product order
     */
    public static function getDefaultProductOrder($simcard = false)
    {
        $subtypes = array(
            Omnius_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION,
            Omnius_Catalog_Model_Type::SUBTYPE_DEVICE,
            Omnius_Catalog_Model_Type::SUBTYPE_ADDON,
            Omnius_Catalog_Model_Type::SUBTYPE_ACCESSORY
        );
        if ($simcard) {
            return array_merge(
                array_slice($subtypes, 0, 1, true),
                array(Omnius_Catalog_Model_Type::SUBTYPE_SIMCARD),
                array_slice($subtypes, 1, count($subtypes), true)
            );
        }

        return $subtypes;
    }

    /**
     * @return array
     *
     * Get product order used in saved shopping cart and order confirmation emails
     */
    public static function getEmailProductOrder($simcard = false)
    {
        $subtypes = array(
            Omnius_Catalog_Model_Type::SUBTYPE_DEVICE,
            Omnius_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION,
            Omnius_Catalog_Model_Type::SUBTYPE_ADDON,
            Omnius_Catalog_Model_Type::SUBTYPE_ACCESSORY
        );
        if ($simcard) {
            return array_merge(
                array_slice($subtypes, 0, 1, true),
                array(Omnius_Catalog_Model_Type::SUBTYPE_SIMCARD),
                array_slice($subtypes, 1, count($subtypes), true)
            );
        }

        return $subtypes;
    }

    /**
     * @return Omnius_Superorder_Model_Superorder
     */
    public function getSuperOrder()
    {
        if ($this->_superOrder === null && $this->getOrderId()) {
            $this->_superOrder = Mage::getModel('superorder/superorder')->load($this->getOrderId());
        }

        return $this->_superOrder;
    }

    /**
     * @return bool
     */
    public function isNumberPorting()
    {

        return (bool) $this->getCurrentNumber();
    }

    /**
     * @return bool
     */
    public function isSimOnly()
    {
        return false;
    }

    /**
     * Get a list of order items from the current package
     *
     * @return array
     */
    public function getPackageItems()
    {
        $packageItems = array();
        $orders = Mage::getModel('sales/order')->getNonEditedOrderItems($this->getOrderId());

        foreach ($orders as $order) {
            foreach ($order->getAllItems() as $item) {
                if ($item->getPackageId() == $this->getPackageId()) {
                    $packageItems[] = $item;
                }
            }
        }

        return $packageItems;
    }

    /**
     * @return int
     */
    public function getPackageId()
    {
        return $this->getData('package_id');
    }

    /**
     * @return bool
     */
    public function isSimOnlyRetentionWithoutSim()
    {
        if (!$this->isRetention()) {
            return false;
        }

        $orderItems = $this->getPackageItems();
        $isSimOnly = true;
        foreach ($orderItems as $orderItem) {
            if (!$orderItem->getProduct()->getSimOnly() || $orderItem->getProduct()->isSim()) {
                $isSimOnly = false;
            }
        }

        return $isSimOnly;
    }

    /**
     * @return bool
     */
    public function isRetention()
    {
        return (bool) ($this->getCtn() && $this->getSaleType() == Omnius_Checkout_Model_Sales_Quote_Item::RETENTION);
    }

    /***
     * @param bool $useQuote
     * @return bool
     */
    public function needsCreditCheck($useQuote = false)
    {
        $key = __METHOD__ . ' ' . $this->getId();
        if (Mage::registry('freeze_models') === true) {
            $data = Mage::registry($key);
            if ($data !== null) {
                return $data;
            }
        }

        if ($useQuote) {
            $items = Mage::getSingleton('checkout/session')->getQuote()->getPackageItems($this->getPackageId());
        } else {
            $items = $this->getPackageItems();
        }
        $hasRetention = false;
        if ($this->getCtn()) {
            $hasRetention = true;
        }

        // Packages with Vodafone network products need a CreditCheck
        // Packages that are retention packages do not need CreditCheck
        if (Mage::registry('freeze_models') === true) {
            Mage::unregister($key);
            Mage::register($key, !$hasRetention);
        } else {
            Mage::unregister($key);
        }

        return !$hasRetention;
    }

    /**
     * Check if the package should show the warning to Deliver it first
     *
     * @return bool
     */
    public function showDeliveryWarning()
    {
        //Get canceled packages
        $cancelOrders = array_merge(
            Mage::helper('omnius_package')->getCancelledPackages(),
            Mage::helper('omnius_package')->getCancelledPackagesNow()
        );
        $editedPackagesIds = Mage::registry('editedPackages');

        $wasDeleted = in_array($this->getPackageId(), $cancelOrders);
        $wasEdited = in_array($this->getPackageId(), $editedPackagesIds);

        $editCond = $wasEdited || $wasDeleted || !$this->getOrderId();
        if ($editCond || $this->getContractSignDate() || $this->getApproveOrder()) {
            return false;
        }

        return $this->needsApprovePackage();
    }

    /**
     * Retrieve the sign date of the order to which this package belongs
     *
     * @return null|string
     */
    public function getContractSignDate()
    {
        if ($this->_contractSignDate === null && ($deliveryOrder = $this->getDeliveryOrder())) {
            $this->_contractSignDate = $deliveryOrder->getContractSignDate();
        }

        return $this->_contractSignDate;
    }

    /**
     * Retrieve the delivery order to which this package belongs
     *
     * @return Omnius_Checkout_Model_Sales_Order|null
     */
    public function getDeliveryOrder()
    {
        if ($this->_deliveryOrder === null && $this->getOrderId()) {
            $orders = Mage::getModel('sales/order')->getNonEditedOrderItems($this->getOrderId());
            foreach ($orders as $order) {
                foreach ($order->getAllItems() as $item) {
                    if ($item->getPackageId() == $this->getPackageId()) {
                        return ($this->_deliveryOrder = $order);
                    }
                }
            }
        }

        return $this->_deliveryOrder;
    }

    /**
     * Check if approveOrder is true or if there is a job for approveOrder running/waiting in the queue
     * @return bool
     */
    public function getApproveOrder()
    {
        if ($this->getData('approve_order')) {
            return true;
        } else {
            if ($jobId = $this->getData('approve_order_job_id')) {
                return Mage::helper('job')->getQueue()->jobIsInQueue($jobId);
            }
        }

        return false;
    }

    /**
     * @return bool|null
     */
    public function needsApprovePackage()
    {
        return $this->_needsApprove = true;
    }

    /**
     * Get a collection of packages for a certain super order
     *
     * @param $superOrderId
     * @param int $packageId To retrieve only a specific package
     * @return mixed
     */
    public function getOrderPackages($superOrderId, $packageId = null)
    {
        $collection = $this->getPackages($superOrderId, null, $packageId);
        if ($packageId) {
            return $collection->getFirstItem();
        }

        return $collection;
    }

    /**
     * @param null $superOrderId
     * @param null $quoteId
     * @param null $packageId
     * @return mixed
     */
    public function getPackages($superOrderId = null, $quoteId = null, $packageId = null)
    {
        $fields = $constraints = array();
        $superOrderId = (int) $superOrderId;
        $quoteId = (int) $quoteId;
        $packageId = (int) $packageId;

        if ((!$superOrderId) && (!$quoteId)) {
            Mage::throwException('Must provide at least a super order ID or a quote ID');
        }

        $key = __METHOD__ . ' ' . $superOrderId . ' ' . $quoteId . ' ' . $packageId;
        if ((Mage::registry('freeze_models') === true) && ($data = Mage::registry($key))) {
            return $data;
        }

        $collection = Mage::getResourceModel('package/package_collection');

        if ($superOrderId && $quoteId) {
            $fields = array('order_id', 'quote_id');
            $constraints = array(
                array('eq' => $superOrderId),
                array('eq' => $quoteId),
            );
        } elseif (!$superOrderId) {
            $fields = 'quote_id';
            $constraints = array('eq' => $quoteId);
        } elseif (!$quoteId) {
            $fields = 'order_id';
            $constraints = array('eq' => $superOrderId);
        }

        $collection->addFieldToFilter($fields, $constraints);

        if ($packageId) {
            $collection->addFieldToFilter('package_id', $packageId);
        }

        if (Mage::registry('freeze_models') === true) {
            Mage::unregister($key);
            Mage::register($key, $collection);
        } else {
            Mage::unregister($key);
        }

        return $collection;
    }

    /**
     * Check if a package status allows number porting step editing on order edit
     *
     * @param bool $numberPorting
     * @return bool
     */
    public function allowNumberPortingEdit($numberPorting = false)
    {
        if (Omnius_Checkout_Helper_Data::HARDWARE_ONLY_SWAP) {
            return false;
        }

        $notAllowedStatusesWithNumberPorting = array(
            self::ESB_PACKAGE_STATUS_ON_ITS_WAY,
            self::ESB_PACKAGE_STATUS_CANCELLED,
            self::ESB_PACKAGE_STATUS_PORTING_REJECTED,
        );

        $allowedStatusesWithoutNumberPorting = array(
            self::NEVER,
        );

        return $numberPorting
            ? !in_array($this->getStatus(), $notAllowedStatusesWithNumberPorting)
            : in_array($this->getStatus(), $allowedStatusesWithoutNumberPorting);
    }

    /**
     * Check if a package status allows billing step editing on order edit
     *
     * @param bool $numberPorting
     * @return bool
     */
    public function allowBillingAddressEdit($numberPorting = false)
    {
        $allowedStatusesWithNumberPorting = array(
            self::ESB_PACKAGE_STATUS_WAITING_FOR_VALIDATION,
        );

        $allowedStatusesWithoutNumberPorting = array(
            self::ESB_PACKAGE_STATUS_WAITING_FOR_VALIDATION,
        );

        return $numberPorting
            ? in_array($this->getStatus(), $allowedStatusesWithNumberPorting)
            : in_array($this->getStatus(), $allowedStatusesWithoutNumberPorting);
    }

    /**
     * Check if a package status allows ctn step editing on order edit
     *
     * @param bool $numberPorting
     * @return bool
     */
    public function allowCtnSelectionEdit($numberPorting = false)
    {
        $allowedStatusesWithNumberPorting = array(
            self::NEVER,
        );

        $allowedStatusesWithoutNumberPorting = array(
            self::NEVER,
        );

        return $numberPorting
            ? in_array($this->getStatus(), $allowedStatusesWithNumberPorting)
            : in_array($this->getStatus(), $allowedStatusesWithoutNumberPorting);
    }

    /**
     * Check if a package status allows package content editing on order edit
     *
     * @param bool $numberPorting
     * @return bool
     */
    public function allowPackageContentEdit($numberPorting = false)
    {
        $allowedStatusesWithNumberPorting = array(
            self::ESB_PACKAGE_STATUS_WAITING_FOR_VALIDATION,
            self::ESB_PACKAGE_STATUS_PACKAGE_VALIDATED,
            self::ESB_PACKAGE_STATUS_BACK_ORDER,
            self::ESB_PACKAGE_STATUS_ALL_RESOURCES_RESERVED,
            self::ESB_PACKAGE_STATUS_PORTING_PENDING,
            self::ESB_PACKAGE_STATUS_ADDITIONAL_INFO_REQUIRED,
            self::ESB_PACKAGE_STATUS_PORTING_APPROVED,
            self::ESB_PACKAGE_STATUS_PORTING_REJECTED,
            self::ESB_PACKAGE_STATUS_READY_FOR_FULFILLMENT,
            self::ESB_PACKAGE_STATUS_NOT_DELIVERED,
            self::ESB_PACKAGE_STATUS_RETURNED,
        );

        $allowedStatusesWithoutNumberPorting = array(
            self::ESB_PACKAGE_STATUS_WAITING_FOR_VALIDATION,
            self::ESB_PACKAGE_STATUS_PACKAGE_VALIDATED,
            self::ESB_PACKAGE_STATUS_BACK_ORDER,
            self::ESB_PACKAGE_STATUS_ALL_RESOURCES_RESERVED,
            self::ESB_PACKAGE_STATUS_READY_FOR_FULFILLMENT,
            self::ESB_PACKAGE_STATUS_NOT_DELIVERED,
            self::ESB_PACKAGE_STATUS_RETURNED,
        );

        return $numberPorting
            ? in_array($this->getStatus(), $allowedStatusesWithNumberPorting)
            : in_array($this->getStatus(), $allowedStatusesWithoutNumberPorting);
    }

    /**
     * Check if a package status allows package content editing on order edit
     *
     * @param bool $numberPorting
     * @return bool
     */
    public function allowDeliveryChannelEdit($numberPorting = false)
    {
        $allowedStatusesWithNumberPorting = array(
            self::ESB_PACKAGE_STATUS_WAITING_FOR_VALIDATION,
            self::ESB_PACKAGE_STATUS_PACKAGE_VALIDATED,
            self::ESB_PACKAGE_STATUS_BACK_ORDER,
            self::ESB_PACKAGE_STATUS_ALL_RESOURCES_RESERVED,
            self::ESB_PACKAGE_STATUS_PORTING_PENDING,
            self::ESB_PACKAGE_STATUS_ADDITIONAL_INFO_REQUIRED,
            self::ESB_PACKAGE_STATUS_PORTING_APPROVED,
            self::ESB_PACKAGE_STATUS_PORTING_REJECTED,
            self::ESB_PACKAGE_STATUS_READY_FOR_FULFILLMENT,
            self::ESB_PACKAGE_STATUS_NOT_DELIVERED,
            self::ESB_PACKAGE_STATUS_RETURNED,
        );

        $allowedStatusesWithoutNumberPorting = array(
            self::ESB_PACKAGE_STATUS_WAITING_FOR_VALIDATION,
            self::ESB_PACKAGE_STATUS_PACKAGE_VALIDATED,
            self::ESB_PACKAGE_STATUS_BACK_ORDER,
            self::ESB_PACKAGE_STATUS_ALL_RESOURCES_RESERVED,
            self::ESB_PACKAGE_STATUS_READY_FOR_FULFILLMENT,
            self::ESB_PACKAGE_STATUS_NOT_DELIVERED,
            self::ESB_PACKAGE_STATUS_RETURNED,
        );

        return $numberPorting
            ? in_array($this->getStatus(), $allowedStatusesWithNumberPorting)
            : in_array($this->getStatus(), $allowedStatusesWithoutNumberPorting);
    }

    /**
     * Check if a package status allows package content editing on order edit
     *
     * @param bool $numberPorting
     * @return bool
     */
    public function allowDeliveryAddressEdit($numberPorting = false)
    {
        $allowedStatusesWithNumberPorting = array(
            self::ESB_PACKAGE_STATUS_WAITING_FOR_VALIDATION,
            self::ESB_PACKAGE_STATUS_PACKAGE_VALIDATED,
            self::ESB_PACKAGE_STATUS_BACK_ORDER,
            self::ESB_PACKAGE_STATUS_ALL_RESOURCES_RESERVED,
            self::ESB_PACKAGE_STATUS_PORTING_PENDING,
            self::ESB_PACKAGE_STATUS_ADDITIONAL_INFO_REQUIRED,
            self::ESB_PACKAGE_STATUS_PORTING_APPROVED,
            self::ESB_PACKAGE_STATUS_PORTING_REJECTED,
            self::ESB_PACKAGE_STATUS_READY_FOR_FULFILLMENT,
            self::ESB_PACKAGE_STATUS_NOT_DELIVERED,
            self::ESB_PACKAGE_STATUS_RETURNED,
        );

        $allowedStatusesWithoutNumberPorting = array(
            self::ESB_PACKAGE_STATUS_WAITING_FOR_VALIDATION,
            self::ESB_PACKAGE_STATUS_PACKAGE_VALIDATED,
            self::ESB_PACKAGE_STATUS_BACK_ORDER,
            self::ESB_PACKAGE_STATUS_ALL_RESOURCES_RESERVED,
            self::ESB_PACKAGE_STATUS_READY_FOR_FULFILLMENT,
            self::ESB_PACKAGE_STATUS_NOT_DELIVERED,
            self::ESB_PACKAGE_STATUS_RETURNED,
        );

        return $numberPorting
            ? in_array($this->getStatus(), $allowedStatusesWithNumberPorting)
            : in_array($this->getStatus(), $allowedStatusesWithoutNumberPorting);
    }

    /**
     * Check if a package status allows package content editing on order edit
     *
     * @param bool $numberPorting
     * @return bool
     */
    public function allowDeliveryContactsEdit($numberPorting = false)
    {
        $allowedStatusesWithNumberPorting = array(
            self::ESB_PACKAGE_STATUS_WAITING_FOR_VALIDATION,
            self::ESB_PACKAGE_STATUS_PACKAGE_VALIDATED,
            self::ESB_PACKAGE_STATUS_BACK_ORDER,
            self::ESB_PACKAGE_STATUS_ALL_RESOURCES_RESERVED,
            self::ESB_PACKAGE_STATUS_PORTING_PENDING,
            self::ESB_PACKAGE_STATUS_ADDITIONAL_INFO_REQUIRED,
            self::ESB_PACKAGE_STATUS_PORTING_APPROVED,
            self::ESB_PACKAGE_STATUS_PORTING_REJECTED,
            self::ESB_PACKAGE_STATUS_READY_FOR_FULFILLMENT,
            self::ESB_PACKAGE_STATUS_NOT_DELIVERED,
            self::ESB_PACKAGE_STATUS_RETURNED,
        );

        $allowedStatusesWithoutNumberPorting = array(
            self::ESB_PACKAGE_STATUS_WAITING_FOR_VALIDATION,
            self::ESB_PACKAGE_STATUS_PACKAGE_VALIDATED,
            self::ESB_PACKAGE_STATUS_BACK_ORDER,
            self::ESB_PACKAGE_STATUS_ALL_RESOURCES_RESERVED,
            self::ESB_PACKAGE_STATUS_READY_FOR_FULFILLMENT,
            self::ESB_PACKAGE_STATUS_NOT_DELIVERED,
            self::ESB_PACKAGE_STATUS_RETURNED,
        );

        return $numberPorting
            ? in_array($this->getStatus(), $allowedStatusesWithNumberPorting)
            : in_array($this->getStatus(), $allowedStatusesWithoutNumberPorting);
    }

    /**
     * @return bool
     *
     * Check if a packages is validated based on the status
     */
    public function isValidated()
    {
        return in_array($this->getStatus(), array(
            self::ESB_PACKAGE_STATUS_PACKAGE_VALIDATED,
            self::ESB_PACKAGE_STATUS_BACK_ORDER,
            self::ESB_PACKAGE_STATUS_ALL_RESOURCES_RESERVED,
            self::ESB_PACKAGE_STATUS_PORTING_PENDING,
            self::ESB_PACKAGE_STATUS_ADDITIONAL_INFO_REQUIRED,
            self::ESB_PACKAGE_STATUS_PORTING_APPROVED,
            self::ESB_PACKAGE_STATUS_PORTING_REJECTED,
            self::ESB_PACKAGE_STATUS_READY_FOR_FULFILLMENT,
            self::ESB_PACKAGE_STATUS_PACKED,
            self::ESB_PACKAGE_STATUS_ON_ITS_WAY,
            self::ESB_PACKAGE_STATUS_DELIVERED,
            self::ESB_PACKAGE_STATUS_NOT_DELIVERED,
            self::ESB_PACKAGE_STATUS_RETURNED,
            self::ESB_PACKAGE_STATUS_CANCELLED
        ));
    }

    /**
     * @return bool
     *
     * Check if a package is payed
     */
    public function isPayed()
    {
        if ($this->isDelivered() || $this->wasDelivered()) {
            // @todo is this still needed or is the base_total_due set to 0 on cashondelivery
            $bool = true;
        } else {
            if ($this->getOrderId()) {
                $checkEditedOrders = $this->checkEditedOrders();
                if ($checkEditedOrders !== null) {
                    return $checkEditedOrders;
                }

                $so = Mage::getModel('superorder/superorder')->load($this->getOrderId());
                if ($so->hasParents()) {
                    return $this->wasPayed($so);
                }
            }

            $bool = false;
        }

        return $bool;
    }

    /**
     * @return bool
     *
     * Check if a package is delivered
     */
    public function isDelivered()
    {
        return in_array(strtolower($this->getStatus()), array(
            strtolower(self::ESB_PACKAGE_STATUS_DELIVERED),
            strtolower(self::ESB_PACKAGE_STATUS_NOT_DELIVERED)
        ));
    }

    /**
     * Tells whether the package was delivered or not.
     * @return bool
     */
    public function wasDelivered()
    {
        $history = Mage::getModel('superorder/statusHistory')->getCollection()->addFieldToFilter('package_id', $this->getId());
        foreach ($history as $instance) {
            if ($instance->getType() == Omnius_Superorder_Model_StatusHistory::PACKAGE_STATUS && ($instance->getStatus() == self::ESB_PACKAGE_STATUS_DELIVERED || $instance->getStatus() == self::ESB_PACKAGE_STATUS_NOT_DELIVERED)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param $superOrder Omnius_Superorder_Model_Superorder
     * @return bool
     */
    public function wasPayed($superOrder)
    {
        $parents = $superOrder->getParents();
        /** @var Omnius_Superorder_Model_Superorder $parent */
        foreach ($parents as $parent) {
            /** @var Omnius_Package_Model_Package $package */
            foreach ($parent->getPackages() as $package) {
                if (($package->getPackageId() == $this->getOldPackageId()) && $package->wasDelivered()) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param $productType
     * @param $allItems
     * @return array
     *
     * Check for current changes or history orders and display the corresponding striked information
     */
    public function getDisplayFor($productType, $allItems)
    {
        $quoteItems = array();
        $this->removedProducts = array();
        $changedIds = array();
        $cancelOrders = array();
        $changedPackagesIds = array();
        $oldOrdersPackages = array();

        if (!$this->getIsExtendedCart()) {
            //check existence of order history, first identifying which Magento/delivery order corresponds to that package
            $packages = Mage::getSingleton('core/session')->getData('packageOrderCorrespondence' . Mage::getSingleton('customer/session')->getOrderEdit()) ?: array();
            $this->handleEditedPackages($packages);
        }

        //Checking if packages exist, otherwise this might be just a quote
        if (isset($packages[$this->getPackageId()])) {
            $orderObj = Mage::getModel('sales/order')->load($packages[$this->getPackageId()]);
            $allOrderItems = Mage::getModel('sales/order_item')
                ->getCollection()
                ->addAttributeToFilter('order_id', $orderObj->getId())
                ->addAttributeToFilter('package_id', $this->getPackageId())
                ->load();

            foreach ($allOrderItems as $oneOrderItem) {
                $product = $oneOrderItem->getProduct();
                if (($product->getId() === null || $product->getId() === '')
                    && ($oneOrderItem->getPackProductType() == $productType)
                ) {
                    $allItems[] = $oneOrderItem;
                }
            }
        }


        //check if user changed something in the current session
        if (($changedItems = $this->getChanges()) && !$this->getIsExtendedCart()) {
            // Get new package items in the format: [product_id => item_doa]
            $changedIds = Omnius_Checkout_Helper_Data::getOrderQuoteItemIds($changedItems);
            foreach ($changedItems as $changedItem) {
                $changedItem->setTelNumber('');//tel number should only be displayed under device
                if (in_array($productType, $changedItem->getProduct()->getType())) {
                    list($newImg, $newName, $newMaf, $newPrice) = $this->getContentFor($changedItem, $changedItems);
                    $quoteId = $changedItem->getData('quote_id');
                    if (!isset($quoteItems[$changedItem->getData('quote_id')])) {
                        $quoteItems[$changedItem->getData('quote_id')] = array(
                            'image' => $newImg,
                            'name' => $newName,
                            'maf' => $newMaf,
                            'price' => $newPrice,
                        );
                    } else {
                        $quoteItems[$quoteId]['image'] = array_merge($newImg, $quoteItems[$quoteId]['image']);
                        $quoteItems[$quoteId]['name'] = array_merge($newName, $quoteItems[$quoteId]['name']);
                        $quoteItems[$quoteId]['maf'] = array_merge($newMaf, $quoteItems[$quoteId]['maf']);
                        $quoteItems[$quoteId]['price'] = array_merge($newPrice, $quoteItems[$quoteId]['price']);
                    }
                    $changedPackagesIds[$changedItem->getPackageId()] = $changedItem->getPackageId();
                }
            }
        }

        $currentSimcard = array();
        //display current package details
        foreach ($allItems as $item) {
            if (in_array(Omnius_Catalog_Model_Type::SUBTYPE_SIMCARD, $item->getProduct()->getType()) && $item->getPackageId() == $this->getPackageId()) {
                $currentSimcard[$this->getPackageId()] = $item;
            }
            if (in_array(Omnius_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION, $item->getProduct()->getType()) && $item->getPackageId() == $this->getPackageId()) {
                $currentSubscription[$this->getPackageId()] = $item;
            }

            $tempCond = isset($changedIds[$item->getProductId()]) && ($item->getItemDoa() != $changedIds[$item->getProductId()]);
            $editedCond = (count($changedIds) == 0)
                || (!array_key_exists($item->getProductId(), $changedIds))
                || $tempCond;

            if (in_array($productType, $item->getProduct()->getType()) && $editedCond) {
                $cancelOrders = array_merge(Mage::helper('omnius_package')->getCancelledPackages(), Mage::helper('omnius_package')->getCancelledPackagesNow());

                $c1Cond = isset($changedIds[$item->getProductId()]) && $item->getItemDoa() != $changedIds[$item->getProductId()];
                $c2Cond = count($changedIds) > 0 && (!array_key_exists($item->getProductId(), $changedIds) || $c1Cond);
                $wasDeleted = in_array($item->getPackageId(), $cancelOrders) || $c2Cond;
                $quoteId = $item->getData('quote_id');
                list($currentImg, $currentName, $currentMaf, $currentPrice) = $this->getContentFor($item, $allItems, $wasDeleted);
                if (!isset($quoteItems[$quoteId])) {
                    $quoteItems[$quoteId] = array(
                        'image' => $currentImg,
                        'name' => $currentName,
                        'maf' => $currentMaf,
                        'price' => $currentPrice,
                    );
                } else {
                    $quoteItems[$quoteId]['image'] = array_merge($currentImg, $quoteItems[$quoteId]['image']);
                    $quoteItems[$quoteId]['name'] = array_merge($currentName, $quoteItems[$quoteId]['name']);
                    $quoteItems[$quoteId]['maf'] = array_merge($currentMaf, $quoteItems[$quoteId]['maf']);
                    $quoteItems[$quoteId]['price'] = array_merge($currentPrice, $quoteItems[$quoteId]['price']);
                }
            }
        }

        $subscriptionsChanged = array();
        $simcardChanged = array();
        $oldOrders = array();

        if (!$this->getIsExtendedCart() && isset($packages[$this->getPackageId()]) && $packages[$this->getPackageId()]) {
            $orderObj = Mage::getModel('sales/order')->load($packages[$this->getPackageId()]);
            if ($orderObj->getParentId()) {
                $oldOrder = Mage::getModel('sales/order')->load($orderObj->getParentId());
                $parentOrderLevel = 0;
                while ($oldOrder) {
                    $parentOrderLevel++;
                    $hasSubscriptionChanged = false;
                    $oldOrderItems = $oldOrder->getAllItems(true);
                    foreach ($oldOrderItems as $oldOrderItem) {
                        if (in_array(Omnius_Catalog_Model_Type::SUBTYPE_SIMCARD, $oldOrderItem->getProduct()->getType())
                            && $oldOrderItem->getPackageId() == $this->getPackageId()
                        ) {
                            if (!isset($subscriptionsChanged[$this->getPackageId()])) {
                                if (isset($currentSimcard[$oldOrderItem->getPackageId()])
                                    && ($currentSimcard[$oldOrderItem->getPackageId()]->getProductId() != $oldOrderItem->getProductId())
                                ) {
                                    if ($parentOrderLevel > 1) {
                                        $oldOrders[$orderObj->getId()] = $orderObj->getAllItems(true);
                                    } else {
                                        $oldOrders[$oldOrder->getId()] = $oldOrderItems;
                                    }
                                    $oldOrderItem->setParentOrderLevel($parentOrderLevel);
                                    $simcardChanged[$this->getPackageId()][$oldOrder->getId()] = $oldOrderItem;
                                }
                            } else {
                                if (isset($currentSimcard[$oldOrderItem->getPackageId()])
                                    && ($currentSimcard[$oldOrderItem->getPackageId()]->getProductId() != $oldOrderItem->getProductId())
                                ) {
                                    $simcardChanged[$this->getPackageId()][$oldOrder->getId()] = $oldOrderItem;
                                }
                            }
                            $currentSimcard[$oldOrderItem->getPackageId()] = $oldOrderItem;
                        }

                        if (in_array(Omnius_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION, $oldOrderItem->getProduct()->getType())
                            && ($oldOrderItem->getPackageId() == $this->getPackageId())
                            && ($currentSubscription[$oldOrderItem->getPackageId()]->getProductId() != $oldOrderItem->getProductId())
                        ) {
                            $currentSubscription[$oldOrderItem->getPackageId()] = $oldOrderItem;
                            $hasSubscriptionChanged = true;
                        }

                        $order_id = $oldOrderItem->getData('order_id');
                        if (in_array($productType, $oldOrderItem->getProduct()->getType()) && $oldOrderItem->getPackageId() == $this->getPackageId()) {
                            list($oldImg, $oldName, $oldMaf, $oldPrice) = $this->getContentFor($oldOrderItem, $oldOrderItems, true, $orderObj->getAllItems(true));
                            if (!empty($oldName)) {
                                if ($productType == Omnius_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION) {
                                    if (!isset($subscriptionsChanged[$this->getPackageId()])) {
                                        $subscriptionsChanged[$this->getPackageId()] = 1;
                                    }
                                    unset($simcardChanged[$this->getPackageId()][$oldOrder->getId()]);
                                }
                                if (!isset($orderItems[$order_id])) {
                                    $orderItems[$order_id] = array(
                                        'image' => $oldImg,
                                        'name' => $oldName,
                                        'maf' => $oldMaf,
                                        'price' => $oldPrice,
                                    );
                                } else {
                                    $orderItems[$order_id]['image'] = array_merge($oldImg, $orderItems[$order_id]['image']);
                                    $orderItems[$order_id]['name'] = array_merge($oldName, $orderItems[$order_id]['name']);
                                    $orderItems[$order_id]['maf'] = array_merge($oldMaf, $orderItems[$order_id]['maf']);
                                    $orderItems[$order_id]['price'] = array_merge($oldPrice, $orderItems[$order_id]['price']);
                                }
                                $oldOrdersPackages[$oldOrderItem->getPackageId()] = $oldOrderItem->getPackageId();
                            }
                        }
                    }
                    if ($hasSubscriptionChanged) {
                        unset($simcardChanged[$this->getPackageId()][$oldOrder->getId()]);
                    }
                    $orderObj = $oldOrder;
                    $oldOrder = $oldOrder->getParentId() ? Mage::getModel('sales/order')->load($oldOrder->getParentId()) : null;
                    unset($subscriptionsChanged[$this->getPackageId()]);
                }
            }
        }

        $this->quoteItems = $quoteItems;
        if (isset($orderItems)) {
            $this->orderItems = $orderItems;
        }

        if ($productType == Omnius_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION) {
            reset($allItems);
            $firstQuoteItem = current($allItems);
            $appendToQuoteId = null;
            if ($changedItems) {
                reset($changedItems);
                $firstChangedItem = current($changedItems);
                if (!isset($quoteItems[$firstQuoteItem->getQuoteId()]) && isset($quoteItems[$firstChangedItem->getQuoteId()])) {
                    $appendToQuoteId = $firstChangedItem->getQuoteId();
                }
            }

            $doExtraLines = false;
            foreach ($allItems as $currentItem) {
                $quoteId = $currentItem->getQuoteId();
                if (!array_key_exists($currentItem->getProductId(), $changedIds)
                    && (Mage::helper('omnius_catalog')->is([Omnius_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION], $currentItem->getProduct())
                        || $currentItem->getProduct()->isSim()
                    )
                ) {
                    $doExtraLines = true;
                }
            }

            if ($changedItems) {
                // Set extra lines (sim) for the edited packages quote items
                $this->setExtraLines($changedItems, 'quote', array());
            }

            // Set extra lines (promo, sim) for the current super quote items
            if ($doExtraLines) {
                $this->setExtraLines($allItems, 'quote', array_merge($changedPackagesIds, $cancelOrders), $appendToQuoteId);
            }

            if (isset($oldOrders)) {
                foreach ($oldOrders as $key => $oldOrderItems) {
                    // Set extra lines (sim) for the old orders items
                    if (!isset($simcardChanged[$this->getPackageId()][$key])) {
                        $this->setExtraLines($oldOrderItems, 'order', $oldOrdersPackages);
                    }
                }
            }

            $quoteId = isset($quoteId) ? $quoteId : null;
            $parentOrderLevel = isset($parentOrderLevel) ? $parentOrderLevel : 0;
            $this->handleChangedSimcard($simcardChanged, $oldOrders, $quoteId, $parentOrderLevel);
        }

        if (isset($orderItems)) {
            $historyItems = array_merge($this->quoteItems, $this->orderItems);
        } else {
            $historyItems = $this->quoteItems;
        }

        return $historyItems;
    }

    /**
     * @return bool
     *
     * Checks if there is any quote for this
     */
    public function getChanges()
    {
        /** @var Omnius_Package_Helper_Data $packageHelper */
        $packageHelper = Mage::helper('omnius_package');
        $changedQuotes = $packageHelper->getModifiedQuotes();
        foreach ($changedQuotes as $changedQuote) {
            if ($changedQuote->getId()) {
                $quoteItems = $changedQuote->getAllItems();
                if (count($quoteItems) && $quoteItems[0]->getPackageId() == $this->getPackageId()) {
                    $this->hasChanged = true;

                    return $quoteItems;
                }
            }
        }

        return false;
    }

    /**
     * @param Omnius_Checkout_Model_Sales_Quote_Item $item
     * @param Omnius_Checkout_Model_Sales_Quote_Item[] $allItems
     * @param bool|false $wasDeleted
     * @param Omnius_Checkout_Model_Sales_Order_Item[] $parentOrderItems
     * @return array
     */
    public function getContentFor($item, $allItems, $wasDeleted = false, $parentOrderItems = array())
    {
        if ($wasDeleted && (end($this->removedProducts) == $item->getProductId()) && $item->getItemDoa()) {
            //don't display the same product as removed twice
            return array(array(), array(), array(), array());
        }
        if ($parentOrderItems && $wasDeleted && $this->skipNotEdited($item, $parentOrderItems)) {
            // Product was not changed so we skip it
            return array(array(), array(), array(), array());
        }

        $image = array();
        $name = array();
        $maf = array();
        $price = array();
        $prodPrice = $mixMatch = $imgWH = $itemProps = null;
        $this->removedProducts[] = $item->getProductId();
        if (!$this->getIsExtendedCart()) {
            $itemProps = json_decode($item->getData('order_item_prices'), true);
            $this->getContentForCheckout($item, $itemProps, $prodPrice, $mixMatch, $imgWH);
        } else {
            $this->getContentForRightSidebar($item, $mixMatch, $prodPrice, $imgWH);
        }

        $productType = $item->getProduct()->getType();
        switch (current($productType)) {
            case Omnius_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION:
            case Omnius_Catalog_Model_Type::SUBTYPE_ADDON:
                $this->parseItemWithMaf($item, $allItems, $wasDeleted, $itemProps, $imgWH, $image, $name, $maf, $price, $productType);
                break;
            case Omnius_Catalog_Model_Type::SUBTYPE_DEVICE:
            case Omnius_Catalog_Model_Type::SUBTYPE_ACCESSORY:
                $this->parseItemWithOneTimePrice($item, $allItems, $mixMatch, $productType, $imgWH, $image, $name, $prodPrice, $price, $maf);
                break;
            default:
                break;
        }

        //strike all prices for history or old changes
        if ($wasDeleted) {
            foreach ($name as &$label) {
                $label = str_replace('<label>', '<label class="old_package">', $label);
                $label = str_replace('list-style', 'list-style old_package', $label);
            }
            unset($label);
            foreach ($price as &$label) {
                $label = str_replace('striked', 'striked old_package', $label);
                $label = str_replace('<label>', '<label class="striked old_package">', $label);
            }
            unset($label);
            foreach ($maf as &$label) {
                $label = str_replace('striked', 'striked old_package', $label);
                $label = str_replace('<label>', '<label class="striked old_package">', $label);
            }
            unset($label);
        }

        return array($image, $name, $maf, $price);
    }

    /**
     * Return the phone number(s) for current package
     *
     * @return array|null
     * @todo: Check if it still needed
     */
    protected function getPhoneNumber()
    {
        $return = null;

        if ($this->getCtn()) {
            $return = $this->getCtn();
        } elseif ($this->getCurrentNumber()) {
            $return = $this->getCurrentNumber();
        } elseif ($this->getTelNumber()) {
            $return = $this->getTelNumber();
        }

        return $return;
    }

    /**
     * Set the extra lines (promo, sim) info to the items
     *
     * @param array $items
     * @param string $affected
     * @param array $changedPackagesIds
     * @param int $affected_id
     */
    protected function setExtraLines($items, $affected, $changedPackagesIds, $affected_id = null)
    {
        $old_class = $old_price_class = '';
        $affectedItems = $affected . 'Items';

        foreach ($items as $item) {
            if (in_array($item->getPackageId(), $changedPackagesIds)) {
                $old_class = ' old_package';
                $old_price_class = ' class="striked old_package"';
            }
            if ($item->getProduct()->isSim() && $item->getPackageId() == $this->getPackageId()) {
                if (!$this->getIsExtendedCart()) {
                    $itemProps = json_decode($item->getData('order_item_prices'), true);
                    $prodPrice = $itemProps['product']['price'];
                } else {
                    $prodPrice = $item->getProduct()->getPrice();
                }

                if (!$affected_id) {
                    $affected_id = $item->getData($affected . '_id');
                }
                $this->{$affectedItems}[$affected_id]['image'][] = '';
                $this->{$affectedItems}[$affected_id]['name'][] = '<label class="list-style' . $old_class . '">' . $item->getName() . '</label>';
                $this->{$affectedItems}[$affected_id]['maf'][] = '<label></label>';
                $this->{$affectedItems}[$affected_id]['price'][] = '<label' . $old_price_class . '>' . Mage::helper('core')->currency($prodPrice, true, false) . '</label>';
            }
        }
    }

    /**
     * @param Omnius_Checkout_Model_Sales_Quote_Item[] $items
     * @param bool $checkChanges
     * @param bool $isOffer
     * Calculate package totals
     * @return array
     */
    public function getPackageTotals($items, $checkChanges = true, $isOffer = false)
    {
        if ($checkChanges) {
            $items = $this->getChanges() ?: $items;
        }
        //Init totals with 0
        $subtotalPrice = 0;
        $subtotalMaf = 0;
        $taxPrice = 0;
        $taxMaf = 0;
        $total = 0;
        $totalMaf = 0;
        $noPromoMaf = 0;
        $noPromoTax = 0;
        $additionalSubtotal = 0;
        $additionalTotal = 0;
        $additionalTax = 0;
        if (is_array($items) || ($items instanceof Traversable)) {
            /** @var Omnius_Checkout_Model_Sales_Quote_Item $item */
            foreach ($items as $item) {
                if ($item->getPackageId() == $this->getPackageId()
                    && (!$item->isPromo())
                ) {
                    $this->parsePricesForItem($item, $total, $subtotalPrice, $noPromoMaf, $noPromoTax, $subtotalMaf, $taxMaf, $taxPrice, $totalMaf, $additionalSubtotal, $additionalTax, $additionalTotal);
                }
            }
            unset($item);
        }
        if ($this->getOrderId()) {
            // Checking for deleted products in package
            $order = Mage::getModel('sales/order')->load($this->getOrderId(), 'superorder_id');
            /** @var Omnius_Checkout_Model_Sales_Order_Item[] $allOrderItems */
            $allOrderItems = Mage::getModel('sales/order_item')
                ->getCollection()
                ->addAttributeToFilter('order_id', $order->getId())
                ->addAttributeToFilter('package_id', $this->getPackageId())
                ->load();

            $productCollection = Mage::getModel('catalog/product')->getCollection()
                ->addFieldToFilter('entity_id', array(
                    'in' => $allOrderItems->getColumnValues('product_id')
                ));

            foreach ($allOrderItems as $item) {
                if ($productCollection->getItemByColumnValue('entity_id', $item->getProductId()) === null) {
                    //There's a deleted product
                    $this->parsePricesForItem($item, $total, $subtotalPrice, $noPromoMaf, $noPromoTax, $subtotalMaf, $taxMaf, $taxPrice, $totalMaf, $additionalSubtotal, $additionalTax, $additionalTotal);
                }
            }
        }

        return [
            'subtotal_price' => $this->roundFloat($subtotalPrice),
            'subtotal_maf' => $this->roundFloat($subtotalMaf),
            'no_promo_maf' => $this->roundFloat($noPromoMaf),
            'no_promo_maf_tax' => $this->roundFloat($noPromoTax),
            'no_promo_tax_amount' => $this->roundFloat($noPromoTax - $noPromoMaf),
            'tax_price' => $this->roundFloat($taxPrice),
            'tax_maf' => $this->roundFloat($taxMaf),
            'total' => $this->roundFloat($total),
            'total_maf' => $this->roundFloat($totalMaf),
            'additional_subtotal' => $this->roundFloat($additionalSubtotal),
            'additional_total' => $this->roundFloat($additionalTotal),
            'additional_tax' => $this->roundFloat($additionalTax),
        ];
    }

    /**
     * Fix the floating point errors
     *
     * @param $number float
     * @return string
     */
    public function roundFloat($number)
    {
        return number_format(round($number, 10), 10, '.', "");
    }

    /**
     * @return bool
     *
     * Check if this packages has been changed in previous sessions
     */
    public function getHistory()
    {
        return false;
    }

    /**
     * @return mixed
     *
     * Getter for hasChanged
     */
    public function hasChanged()
    {
        return $this->hasChanged;
    }

    /*
     * Returns results for Open Orders Section - RFC150641 - based on
     * @param $agentId
     * @param $customer_id
     * @param $dealer_id
     * @param $section = openorders
     * @param $websiteId
     * @param $cluster (error|ready|check|other)
     * @param $clusterOrderField (id|mobile_nr|status|package_nr)
     * @param $clusterOrderType (asc|desc)
     * @param $clusterResultsOnPage (default 50)
     */

    /**
     * @param $isModified
     * @return bool
     * Determines if a package can be modified
     */
    public function canModify($isModified)
    {
        //check also if it was cancelled
        $isCancelled = in_array($this->getPackageId(), Mage::helper('omnius_package')->getCancelledPackages());
        //if package is delivered, check if a non-delivered package was already modified and vice versa
        $deliveredModified = Mage::helper('omnius_package')->isDeliveredPackageModified();
        $othersChanged = (!$this->isDelivered() && $deliveredModified) || ($this->isDelivered() && Mage::helper('omnius_package')->getModifiedQuotes()->count());

        //basically, can't be modified if it was already edited, cancelled, or delivered/un-delivered packages were modified

        return !($isModified || $isCancelled || $othersChanged);
    }

    /**
     * Checks if delivered or not delivered packages were modified to lock the change of other types
     */
    public function lockAddressChange()
    {
        // In alpha shops we disable package address change
        if (Omnius_Checkout_Helper_Data::HARDWARE_ONLY_SWAP) {
            return true;
        }

        //if package is delivered, check if a non-delivered package was already modified and vice versa
        /** @var Omnius_Package_Helper_Data $package_helper */
        $package_helper = Mage::helper('omnius_package');
        $deliveredModified = $package_helper->isDeliveredPackageModified();
        $cancelledPackagesNow = Mage::helper('omnius_package')->getCancelledPackagesNow();
        $editedPackagesNow = Mage::registry('editedPackages');
        $cancelledDelivered = false;
        $cancelledNotDelivered = false;
        foreach ($cancelledPackagesNow as $packageId) {
            $package = Mage::getModel('package/package')
                ->getCollection()
                ->addFieldToFilter('order_id', $this->getOrderId())
                ->addFieldToFilter('package_id', $packageId);

            $obj = $package->getFirstItem();

            if ($obj->getId() && $obj->isDelivered()) {
                $cancelledDelivered = true;
            } elseif ($obj->getId() && !$obj->isDelivered()) {
                $cancelledNotDelivered = true;
            }
            break;
        }
        $c1Cond = $this->isDelivered() && ($deliveredModified || !$cancelledNotDelivered) && in_array($this->getPackageId(), $editedPackagesNow);
        $c2Cond = !$this->isDelivered() && !($deliveredModified || $cancelledDelivered);
        $allowAddressChange = $c2Cond || $c1Cond;

        //basically, can't be modified if it was already edited, cancelled, or delivered/un-delivered packages were modified
        return !$allowAddressChange;
    }

    /**
     * @return bool
     *
     * Check if package is cancelled
     */
    public function isCancelled()
    {
        return in_array($this->getStatus(), array(
            self::ESB_PACKAGE_STATUS_CANCELLED
        ));
    }

    /**
     * @param $agentId
     * @param $customerId
     * @param $dealerId
     * @param $section
     * @param $websiteId
     * @param $customerScreen
     * @param $clusterParams
     * @return Omnius_Superorder_Model_Mysql4_Superorder_Collection
     */
    public function getOpenedOrdersPackages(
        $agentId,
        $customerId,
        $dealerId,
        $section,
        $websiteId,
        $customerScreen,
        $clusterParams
    ) {
        $cluster = $clusterParams['cluster'];
        $clusterOrderField = $clusterParams['orderField'];
        $clusterOrderType = $clusterParams['orderType'];
        $clusterPage = $clusterParams['page'];
        $clusterResultsPerPage = $clusterParams['resultsPerPage'];

        /** @var Omnius_Superorder_Model_Mysql4_Superorder_Collection $collection */
        $collection = $this->getPackagesByStatus($agentId, $customerId, $dealerId, $section, $websiteId);

        $tbl_superorder = Mage::getResourceModel('superorder/superorder')->getTable('superorder');

        $collection->getSelect()
            ->joinRight(
                array('superorder' => $tbl_superorder),
                'main_table.order_id = superorder.entity_id',
                [
                    'superorder.entity_id AS super_entity_id',
                    'superorder.order_status AS super_order_status',
                    'superorder.website_id AS super_website_id',
                    'superorder.order_number AS super_order_number',
                    'superorder.created_at AS super_order_created',
                    'superorder.customer_id AS super_customer_id',
                    'superorder.created_agent_id AS super_created_agent_id',
                    'superorder.error_code AS super_error_code',
                    'superorder.error_detail AS super_error_detail',
                ]
            )
            ->joinLeft(
                array('t' => 'status_history'),
                'superorder.entity_id = t.superorder_id',
                [
                    't.entity_id AS historys_id',
                    't.superorder_id AS hsuperorder_id',
                    't.status AS hsstatus',
                    't.type AS hstype',
                    'MAX(t.created_at) AS hscreated_at',
                ]
            )
            ->joinLeft(
                array('t_2' => 'status_history'),
                'main_table.entity_id = t_2.package_id',
                [
                    't_2.entity_id AS historyc_id',
                    't_2.package_id AS hpackage_id',
                    't_2.status AS hcstatus',
                    't_2.type AS hctype',
                    'MAX(t_2.created_at) AS hccreated_at',
                ]
            )
            ->joinLeft(
                array('t_4' => 'sales_flat_order'),
                'superorder.entity_id = t_4.superorder_id',
                [
                    't_4.customer_prefix AS customer_prefix',
                    't_4.customer_middlename AS customer_middle_name',
                    't_4.customer_lastname AS customer_lastname',
                    't_4.customer_firstname AS customer_firstname',
                    't_4.company_name AS company_name',
                ]
            )
            ->joinLeft(
                array('t_5' => 'customer_entity'),
                'superorder.customer_id = t_5.entity_id',
                [
                    't_5.ban AS customer_ban',
                ]
            )
            ->joinRight(
                array('t_9' => 'catalog_package'),
                'main_table.order_id = t_9.order_id',
                [
                    'COUNT(distinct(t_9.entity_id)) AS total_packages',
                    'MAX(t_9.tel_number) AS phone_number',
                ]
            );

        //Setting up cluster filters
        $finalOrders = $this->getFinalOrders();
        $clusterTwo = $this->getClusterTwo();
        $clusterThree = $this->getClusterThree();
        $clusterOne = $this->getClusterOne();

        switch ($cluster) {
            case 'error' :
                $collection->getSelect()->where("not (" . $finalOrders . ")");
                $collection->getSelect()->where($clusterOne);
                break;
            case 'ready' :
                $collection->getSelect()->where("not (" . $finalOrders . ")");
                $collection->getSelect()->where($clusterTwo);
                break;
            case 'check' :
                $collection->getSelect()->where("not (" . $finalOrders . ")");
                $collection->getSelect()->where($clusterThree);
                break;
            case 'other' :
                if ($customerScreen) {
                    $collection->getSelect()->where($finalOrders);
                } else {
                    $collection->getSelect()->where("1=0");
                }
                break;
            default:
                break;
        }

        $collection->getSelect()->group('superorder.entity_id');

        switch ($clusterOrderField) {
            case 'id':
                $collection->getSelect()->order(['superorder.entity_id ' . $clusterOrderType]);
                break;
            case 'status':
                $collection->getSelect()->order(['superorder.order_status ' . $clusterOrderType]);
                break;
            case 'package_nr':
                $collection->getSelect()->order(['total_packages ' . $clusterOrderType]);
                break;
            case 'cn':
                $collection->getSelect()->order(['customer_lastname ' . $clusterOrderType]);
                break;
            default:
                break;
        }

        $collection->getSelect()->limit($clusterResultsPerPage, ($clusterPage - 1) * $clusterResultsPerPage);

        return $collection;
    }

    /**
     * @return array
     */
    protected function getDefaultStatusesSections()
    {
        $statuses['porting'] = [
            self::ESB_PACKAGE_NETWORK_STATUS_NP_PENDING,
            self::ESB_PACKAGE_NETWORK_STATUS_NP_INITIAL,
        ];
        $statuses['openorders'] = [];
        $statuses['customer-screen-orders'] = [];
        $statuses['porting_limited'] = [
            self::ESB_PACKAGE_NETWORK_STATUS_NP_NOT_SOLVABLE,
            self::ESB_PACKAGE_NETWORK_STATUS_NP_REJECTED,
            self::ESB_PACKAGE_NETWORK_STATUS_NP_AUTHORIZED,
        ];
        $statuses['creditcheck'] = [
            self::ESB_PACKAGE_NETWORK_STATUS_CC_PENDING,
            self::ESB_PACKAGE_NETWORK_STATUS_CC_INITIAL,
            self::ESB_PACKAGE_NETWORK_STATUS_CC_REFERRED,
            self::ESB_PACKAGE_NETWORK_STATUS_CC_ADDITIONNAL_INFO_REQUIRED,
        ];
        $statuses['creditcheck_limited'] = [
            self::ESB_PACKAGE_NETWORK_STATUS_CC_REJECTED,
            self::ESB_PACKAGE_NETWORK_STATUS_CC_APPROVED,
        ];

        return $statuses;
    }

    /**
     * Retrieve superorder packages based on the porting or credit-check statuses
     *
     * @param int $agentId
     * @param int $customerId
     * @param int $dealerId
     * @param string $section
     * @param int $websiteId
     * @throws InvalidArgumentException
     * @return object
     */
    public function getPackagesByStatus($agentId = null, $customerId = null, $dealerId = null, $section = 'porting', $websiteId = null)
    {
        if (!in_array($section, $this->acceptedSections)) {
            throw new InvalidArgumentException(sprintf('Invalid section provided. Expected one of %s, got %s', join(', ', $this->acceptedSections), $section));
        }

        $statuses = $this->getDefaultStatusesSections();

        $search = ['fields' => [], 'rules' => ''];
        /** @var Omnius_Package_Model_Mysql4_Package_Collection $collection */
        $collection = $this->getCollection()
            // Ignore packages where there is no checksum, because they contain no items and are most likely created using multiple tabs
            ->addFieldToFilter('main_table.checksum', ['neq' => 'null']);
        if ($agentId) {
            if (is_array($agentId)) {
                $agentId = array_unique($agentId);
                if (count($agentId) > 0) {
                    $search['fields'][] = 'superorder.created_agent_id';
                    $search['rules'][] = ['in' => $agentId];
                }
            } elseif (is_numeric($agentId)) {
                $search['fields'][] = 'superorder.created_agent_id';
                $search['rules'][] = ['eq' => $agentId];
            }
        }

        if ($customerId) {
            $collection->addFieldToFilter('superorder.customer_id', $customerId);
        }
        if ($dealerId) {
            if (is_array($dealerId)) {
                $dealerId = array_unique($dealerId);
                if (count($dealerId) > 0) {
                    $search['fields'][] = 'superorder.created_dealer_id';
                    $search['rules'][] = ['in' => $dealerId];
                }
            } elseif (is_numeric($dealerId)) {
                $search['fields'][] = 'superorder.created_dealer_id';
                $search['rules'][] = ['eq' => $dealerId];
            }
        }
        if ($websiteId) {
            if (is_array($websiteId)) {
                $search['fields'][] = 'superorder.created_website_id';
                $search['rules'][] = ['in' => $websiteId];
            } else {
                $search['fields'][] = 'superorder.created_website_id';
                $search['rules'][] = ['eq' => $websiteId];
            }
        }

        if (count($search['fields'])) {
            if ($websiteId !== true) {
                $collection->addFieldToFilter($search['fields'], $search['rules']);
            }
        } else {
            // In case there are no permissions return empty collection
            $collection->getSelect()->where('1=0');
        }

        $tbl_superorder = Mage::getSingleton('core/resource')->getTableName('superorder');
        $tbl_sales_order = Mage::getSingleton('core/resource')->getTableName('sales_flat_order');

        if ($section == 'validation') {
            $collection->getSelect()
                ->join(
                    array('superorder' => $tbl_superorder),
                    'main_table.order_id = superorder.entity_id',
                    array('superorder.order_number', 'superorder.created_at AS validation_status_updated', 'superorder.error_code', 'superorder.error_detail')
                )
                ->join(
                    array('sales_order' => $tbl_sales_order),
                    'sales_order.superorder_id = superorder.entity_id',
                    array('sales_order.dealer_id')
                )
                ->where('sales_order.edited = ?', '0')
                ->where('superorder.error_detail IS NOT NULL')
                ->order(array(sprintf('main_table.creditcheck_status_updated DESC'), 'superorder.created_at DESC', sprintf('main_table.creditcheck_status ASC')))
                ->group('superorder.entity_id');
        } elseif ($section != 'openorders' && $section != 'customer-screen-orders') {
            $collection->getSelect()
                ->join(
                    array('superorder' => $tbl_superorder),
                    'main_table.order_id = superorder.entity_id',
                    array('superorder.order_number', 'superorder.created_at')
                )
                ->join(
                    array('sales_order' => $tbl_sales_order),
                    'sales_order.superorder_id = superorder.entity_id',
                    array('sales_order.dealer_id')
                )
                ->where('sales_order.edited = ?', '0')
                ->where('main_table.' . $section . '_status IN (' . $this->_getSqlStatusCollection($statuses[$section]) . ') OR ' . 'main_table.' . $section . '_status IN (' . $this->_getSqlStatusCollection($statuses[sprintf('%s_limited',
                        $section)]) . ') AND main_table.' . $section . "_status_updated >= " . sprintf('\'%s\'', $this->getDateLimit()))
                ->order(array(sprintf('main_table.%s_status_updated DESC', $section), 'superorder.created_at DESC', sprintf('main_table.%s_status ASC', $section)))
                ->group('main_table.entity_id');
        }

        return $collection;
    }

    /**
     * @param $statusses
     * @return string
     */
    protected function _getSqlStatusCollection($statusses)
    {
        return "'" . implode("','", $statusses) . "'";
    }

    /**
     * @param string $format
     * @param bool $asString
     * @return DateTime|string
     */
    public function getDateLimit($format = 'Y-m-d H:i:s', $asString = true)
    {
        $date = new DateTime(sprintf('%s days ago', Mage::helper('omnius_package')->getPackageDateLimit()));

        return $asString ? $date->format($format) : $date;
    }

    /**
     * @return mixed
     */
    protected function getDefaultFilterStatusesSections()
    {
        $statuses['porting'] = [
            'rejected' => [
                self::ESB_PACKAGE_NETWORK_STATUS_NP_NOT_SOLVABLE,
                self::ESB_PACKAGE_NETWORK_STATUS_NP_REJECTED,
            ],
            'ongoing' => [
                self::ESB_PACKAGE_NETWORK_STATUS_NP_PENDING,
                self::ESB_PACKAGE_NETWORK_STATUS_NP_INITIAL,
            ],
            'completed' => [self::ESB_PACKAGE_NETWORK_STATUS_NP_AUTHORIZED],
        ];

        $statuses['creditcheck'] = [
            'ongoing' => [
                self::ESB_PACKAGE_NETWORK_STATUS_CC_PENDING,
                self::ESB_PACKAGE_NETWORK_STATUS_CC_INITIAL,
                self::ESB_PACKAGE_NETWORK_STATUS_CC_REFERRED,
            ],
            'additional' => [self::ESB_PACKAGE_NETWORK_STATUS_CC_ADDITIONNAL_INFO_REQUIRED],
            'rejected' => [self::ESB_PACKAGE_NETWORK_STATUS_CC_REJECTED],
            'completed' => [self::ESB_PACKAGE_NETWORK_STATUS_CC_APPROVED],
        ];

        $statuses['validation'] = [
            'failed' => ['=', self::ESB_PACKAGE_NETWORK_STATUS_CC_VALIDATION_FAILED],
            'rejected' => ['!=', self::ESB_PACKAGE_NETWORK_STATUS_CC_VALIDATION_FAILED],
        ];

        return $statuses;
    }

    /**
     * @param $agentId
     * @param $dealerId
     * @param $websiteId
     * @return array
     */
    protected function getSearchFieldsAndRules($agentId, $dealerId, $websiteId)
    {
        $search = ['fields' => [], 'rules' => ''];
        if ($agentId) {
            if (is_array($agentId)) {
                $agentId = array_unique($agentId);
                if (count($agentId) > 0) {
                    $search['fields'][] = 'superorder.created_agent_id';
                    $search['rules'][] = ['in' => $agentId];
                }
            } elseif (is_numeric($agentId)) {
                $search['fields'][] = 'superorder.created_agent_id';
                $search['rules'][] = ['eq' => $agentId];
            }
        }
        if ($dealerId) {
            if (is_array($dealerId)) {
                $dealerId = array_unique($dealerId);
                if (count($dealerId) > 0) {
                    $search['fields'][] = 'superorder.created_dealer_id';
                    $search['rules'][] = ['in' => $dealerId];
                }
            } elseif (is_numeric($dealerId)) {
                $search['fields'][] = 'superorder.created_dealer_id';
                $search['rules'][] = ['eq' => $dealerId];
            }
        }
        if ($websiteId) {
            if (is_array($websiteId)) {
                $search['fields'][] = 'superorder.created_website_id';
                $search['rules'][] = ['in' => $websiteId];
            } else {
                $search['fields'][] = 'superorder.created_website_id';
                $search['rules'][] = ['eq' => $websiteId];
            }
        }

        return $search;
    }
    /**
     * @param $type
     * @param null $agentId
     * @param null $customerId
     * @param null $dealerId
     * @param string $section
     * @param null $websiteId
     * @param array $pageOptions
     * @return int|Varien_Data_Collection_Db
     */
    public function getPackagesByStatusFilters($type, $agentId = null, $customerId = null, $dealerId = null, $section = 'porting', $websiteId = null, $pageOptions = [])
    {
        if (!in_array($section, $this->acceptedSections)) {
            throw new InvalidArgumentException(sprintf('Invalid section provided. Expected one of %s, got %s', join(', ', $this->acceptedSections), $section));
        }

        $pageNumber = isset($pageOptions['pageNumber']) ? $pageOptions['pageNumber'] : 1;
        $showLimit = isset($pageOptions['showLimit']) ? $pageOptions['showLimit'] : self::MAX_LINES_TOP_MENU;
        $doCount = isset($pageOptions['doCount']) ? $pageOptions['doCount'] : false;

        $statuses = $this->getDefaultFilterStatusesSections();
        $search = $this->getSearchFieldsAndRules($agentId, $dealerId, $customerId);
        /** @var Omnius_Package_Model_Mysql4_Package_Collection $collection */
        $collection = $this->getCollection()
            // Ignore packages where there is no checksum, because they contain no items and are most likely created using multiple tabs
            ->addFieldToFilter('main_table.checksum', ['neq' => 'null']);

        if ($customerId) {
            $collection->addFieldToFilter('superorder.customer_id', $customerId);
        }

        $currentWebsite = Mage::app()->getWebsite();

        $tbl_superorder = Mage::getSingleton('core/resource')->getTableName('superorder');
        $tbl_sales_order = Mage::getSingleton('core/resource')->getTableName('sales_flat_order');
        if (count($search['fields'])) {
            if ($websiteId !== true) {
                $collection->addFieldToFilter($search['fields'], $search['rules']);
            }
        } else {
            // In case there are no permissions return empty collection
            $collection->getSelect()->where('1=0');
        }


        if ($section == 'validation') {
            $collection->getSelect()
                ->join(
                    array('superorder' => $tbl_superorder),
                    'main_table.order_id = superorder.entity_id',
                    array('superorder.order_number', 'superorder.created_at AS validation_status_updated', 'superorder.error_code', 'superorder.error_detail')
                )
                ->join(
                    array('sales_order' => $tbl_sales_order),
                    'sales_order.superorder_id = superorder.entity_id',
                    array('sales_order.dealer_id')
                )
                ->where('sales_order.edited = ?', '0')
                ->where('superorder.error_detail IS NOT NULL')
                ->where(sprintf('main_table.creditcheck_status %s "%s"', $statuses[$section][$type][0], $statuses[$section][$type][1]))
                ->order(array(sprintf('main_table.creditcheck_status_updated DESC'), 'superorder.created_at DESC', 'main_table.creditcheck_status ASC'))
                ->group('superorder.entity_id');
            if ($doCount) {
                $sql = $collection->getSelectCountSql();
                $resource = Mage::getSingleton('core/resource');

                return count($resource->getConnection('core_read')->fetchAll($sql));
            } else {
                $collection->getSelect()
                    ->limitPage($pageNumber, $showLimit);
            }
        } elseif ($section == 'openorders') {
            // Do nothing
        } else {
            $collection->getSelect()
                ->join(
                    array('superorder' => $tbl_superorder),
                    'main_table.order_id = superorder.entity_id',
                    array('superorder.order_number', 'superorder.created_at')
                )
                ->join(
                    array('sales_order' => $tbl_sales_order),
                    'sales_order.superorder_id = superorder.entity_id',
                    array('sales_order.dealer_id')
                )
                ->where('sales_order.edited = ?', '0')
                ->where('main_table.' . $section . '_status IN (' . $this->_getSqlStatusCollection($statuses[$section][$type]) . ') AND main_table.' . $section . "_status_updated >= " . sprintf('\'%s\'',
                        $this->getDateLimit()))
                ->order(array(sprintf('main_table.%s_status_updated DESC', $section), 'superorder.created_at DESC', sprintf('main_table.%s_status ASC', $section)))
                ->group('main_table.entity_id');
            if ($doCount) {
                $sql = $collection->getSelectCountSql();
                $resource = Mage::getSingleton('core/resource');

                return count($resource->getConnection('core_read')->fetchAll($sql));
            } else {
                $collection->getSelect()
                    ->limitPage($pageNumber, $showLimit);
            }
        }

        return $collection->load();
    }

    /**
     * Retrieve portability data for fast compare on order edit
     *
     * @return array
     */
    public function getPortabilityData()
    {
        return array(
            'mobile_number' => $this->getCurrentNumber(),
            'sim' => $this->getCurrentSimcardNumber(),
            'current_provider' => $this->getNetworkProvider(),
            'type' => $this->getConnectionType(),
            'end_date_contract' => $this->getContractEndDate() ? date('d-m-Y', strtotime($this->getContractEndDate())) : null,
            'number_porting_type' => $this->getNumberPortingType(),
            'current_operator' => $this->getNetworkOperator(),
            'contract_nr' => $this->getContractNr()
        );
    }

    /**
     * @param Omnius_Package_Model_Package|string $package
     * @return bool
     */
    public function sameChecksum($package)
    {
        if ($package instanceof Omnius_Package_Model_Package) {
            return $this->getChecksum() === $package->getChecksum();
        }

        return $this->getChecksum() === $package;
    }

    /**
     * @param null $quote
     * @return bool
     */
    public function hasHardwareWithoutSim($quote = null)
    {
        /** @var Omnius_Checkout_Model_Sales_Quote $quote */
        if (!$quote) {
            $quote = Mage::getModel('sales/quote')->load($this->getQuoteId());
        }

        $hasHardware = false;
        $subscription = false;
        foreach ($quote->getAllVisibleItems() as $item) {
            $itemProduct = $item->getProduct();
            if ($itemProduct->is(Omnius_Catalog_Model_Type::SUBTYPE_SIMCARD)
                && $item->getPackageId() == $this->getPackageId()
            ) {
                return false;
            }

            if ($itemProduct->is(Omnius_Catalog_Model_Type::SUBTYPE_DEVICE)
                && $item->getPackageId() == $this->getPackageId()
            ) {
                $hasHardware = true;
                $product = Mage::getModel('catalog/product')->load($itemProduct->getId());
                $simType = $product->getAttributeText(Omnius_Catalog_Model_Product::DEFAULT_SIM_ATTRIBUTE);
            }

            if ($itemProduct->is(Omnius_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION)
                && $item->getPackageId() == $this->getPackageId()
            ) {
                $subscription = $item;
            }
        }

        if ($hasHardware && $subscription) {
            return isset($simType) && Mage::helper('omnius_configurator/attribute')->checkSimTypeAvailable($subscription->getProduct(), $simType) ? $simType : false;
        }

        return false;
    }

    /**
     * Package removal function based on the quote/superorderId
     * @param $quoteId int
     * @param $packageId int
     * @param $isSuperorder bool
     * @return $this
     */
    public function deletePackage($quoteId, $packageId, $isSuperorder = false)
    {
        $coll = $this->getCollection();
        if ($isSuperorder) {
            $coll->addFieldToFilter('order_id', $quoteId)
                ->addFieldToFilter('package_id', $packageId)
                ->getFirstItem()->delete();
        } else {
            $coll->addFieldToFilter('quote_id', $quoteId)
                ->addFieldToFilter('package_id', $packageId)
                ->getFirstItem()->delete();
        }

        return $this;
    }

    /**
     * @param $quoteId int
     * @param $packageIds array
     * @param bool $isSuperorder bool
     * @return $this
     */
    public function deletePackages($quoteId, $packageIds, $isSuperorder = false)
    {
        $packageIds = array_unique($packageIds);

        $coll = $this->getCollection();
        if ($isSuperorder) {
            $deletePackages = $coll->addFieldToFilter('order_id', $quoteId)
                ->addFieldToFilter('package_id', array('in' => $packageIds));
        } else {
            $deletePackages = $coll->addFieldToFilter('quote_id', $quoteId)
                ->addFieldToFilter('package_id', array('in' => $packageIds));
        }

        foreach ($deletePackages as $pack) {
            $pack->delete();
        }

        return $this;
    }

    /**
     * Check if a package is completed
     *
     * @return bool
     */
    public function isCompleted()
    {
        return $this->getCurrentStatus() === Omnius_Configurator_Model_AttributeSet::PACKAGE_STATUS_COMPLETED;
    }

    /**
     * Check if the package has a sim
     */
    public function hasSim($quote)
    {
        foreach ($quote->getAllVisibleItems() as $item) {
            if ($item->getProduct()->isSim() && $item->getPackageId() == $this->getPackageId()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check if a package has old sim
     */
    public function hasOldSim()
    {
        return $this->getOldSim();
    }

    /**
     * @return $this
     */
    public function checkRemoveCurrentSim()
    {
        foreach (Mage::getSingleton('checkout/cart')->getQuote()->getAllItems() as $item) {
            if ($item->getPackageId() == $this->getPackageId()) {
                if (!$this->isRetention()) {
                    $this->setOldSim(0);
                }
                break;
            }
        }

        return $this;
    }

    /**
     * @return array
     *
     * Return a list of products of the new superorder resulted from editing the current delivered one
     */
    public function getItemsChanged()
    {
        $conn = Mage::getSingleton("core/resource")->getConnection("core_read");

        $binds = array(
            ':orderId' => $this->getOrderId(),
            ':package' => $this->getPackageId()
        );

        return $conn
            ->fetchPairs("SELECT oi.item_id, oi.product_id FROM sales_flat_order_item oi 
            INNER JOIN sales_flat_order o ON o.entity_id=oi.order_id
            INNER JOIN superorder s ON s.entity_id=o.superorder_id 
            WHERE (oi.item_doa != 1 or oi.item_doa IS NULL) AND o.edited=0 
            AND s.parent_id=:orderId AND oi.package_id=:package", $binds
        );
    }

    /**
     * Determines if a packages contains at least one hardware item
     */
    public function containsHardware()
    {
        foreach ($this->getPackageItems() as $item) {
            if ($item->getProduct()->isOfHardwareType()) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return array
     */
    public function getHardwareItems()
    {
        $items = [];
        /** @var Omnius_Checkout_Model_Sales_Quote_Item $item */
        foreach ($this->getPackageItems() as $item) {
            if ($item->getProduct()->canBeDOA()) {
                $items[$item->getSku()] = $item->getName();
            }
        }

        return $items;
    }

    /**
     * @return array
     */
    public function hasDOADeletedItems()
    {
        $items = [];
        /** @var Omnius_Checkout_Model_Sales_Quote_Item $item */
        foreach ($this->getPackageItems() as $item) {
            if ($item->getProduct()->isEndOfLife()) {
                $items[] = $item->getSku();
            }
        }

        return $items;
    }

    /**
     * @return string
     */
    public function getContractChecksum()
    {
        return $this->getChecksum() .
        (
        ($this->getDeviceName() != null && trim($this->getDeviceName()) != '')
            ? md5($this->getDeviceName())
            : ''
        );
    }

    /**
     * Check if the package had one of it's defaulted addons removed or not
     */
    public function allowDefaulted()
    {
        return $this->getData('defaulted_removed') != 1;
    }

    /**
     * Check if the flag should be updated
     */
    public function updateDefaultedRemoved($removed)
    {
        $this->setData('defaulted_removed', $removed || $this->getData('defaulted_removed'));

        return $this;
    }

    /**
     * @param $status
     * @return $this
     */
    public function setCurrentStatus($status)
    {
        $this->setData('current_status', $status);

        // SOC codes checks
        if ($status == Omnius_Configurator_Model_AttributeSet::PACKAGE_STATUS_COMPLETED) {
            try {
                $this->setData('soc_error', null);
            } catch (LogicException $e) {
                $this->setData('soc_codes', null);
                $this->setData('soc_error', $e->getMessage());
                $this->setData('current_status', Omnius_Configurator_Model_AttributeSet::PACKAGE_STATUS_OPEN);
            }
        } else {
            $this->setData('soc_codes', null);
            $this->setData('soc_error', null);
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function hasSubscription($quote = null)
    {
        /** @var Omnius_Checkout_Model_Sales_Quote $quote */
        if (!$quote) {
            $quote = Mage::getModel('sales/quote')->load($this->getQuoteId());
        }

        foreach ($quote->getAllVisibleItems() as $item) {
            if (!$item->isDeleted()
                && $item->getProduct()->is(Omnius_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION)
                && $item->getPackageId() == $this->getPackageId()
            ) {
                return $item;
            }
        }

        return false;
    }

    /**
     * @param $date
     * @return $this
     */
    public function setCtnOriginalStartDate($date)
    {
        if (!$date) {
            return $this;
        }

        if (is_string($date) && !$date instanceof DateTime) {
            $date = new DateTime($date);
        } elseif (!$date instanceof DateTime) {
            Mage::throwException('Invalid date provided');
        }

        $this->setData('ctn_original_start_date', $date->format('Y-m-d'));

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getCtnOriginalStartDate()
    {
        if ($this->getData('ctn_original_start_date')) {
            return new DateTime($this->getData('ctn_original_start_date'));
        }

        return null;
    }

    /**
     * @param $date
     * @return $this
     */
    public function setCtnOriginalEndDate($date)
    {
        if (!$date) {
            return $this;
        }

        if (is_string($date) && !$date instanceof DateTime) {
            $date = new DateTime($date);
        } elseif (!$date instanceof DateTime) {
            Mage::throwException('Invalid date provided');
        }

        $this->setData('ctn_original_end_date', $date->format('Y-m-d'));

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getCtnOriginalEndDate()
    {
        if ($this->getData('ctn_original_end_date')) {
            return new DateTime($this->getData('ctn_original_end_date'));
        }

        return null;
    }

    /**
     * Determine if the package is in manual activation override mode
     *
     * @return bool
     */
    public function isManualActivation()
    {
        return (bool) $this->getManualActivationReason();
    }


    /**
     * Determine if the ESB confirmed the package as manual activation
     */
    public function esbManualActivation()
    {
        return (bool) $this->getEsbManualActivation();
    }

    /**
     * Determines if the package is a packages which arrived directly cancelled from a delivered order
     */
    public function arrivedCancelled()
    {
        if (!$this->getOldPackageId()) {
            return false;
        }
        $statuses = 0;
        $history = Mage::getModel('superorder/statusHistory')->getCollection()->addFieldToFilter('package_id', $this->getId());
        foreach ($history as $instance) {
            if ($instance->getType() == Omnius_Superorder_Model_StatusHistory::PACKAGE_STATUS) {
                $statuses++;
            }
            if ($statuses == 2 && $instance->getStatus() == self::ESB_PACKAGE_STATUS_CANCELLED) {
                return true;
            }
        }
        return false;
    }

    protected function _construct()
    {
        $this->_init("package/package");
        $this->hasChanged = false;
        $this->removedProducts = array();
    }

    /**
     * @return Mage_Core_Model_Abstract
     */
    protected function _beforeSave()
    {
        if ($this->getData('created_at') === null) {
            $this->setData('created_at', now());
        } else {
            $this->setData('updated_at', now());
        }

        return parent::_beforeSave();
    }

    /**
     * @return Mage_Core_Model_Abstract|void
     */
    protected function _afterSave()
    {
        Mage::helper('superorder')->checkStatusesChanged($this->getOrigData(), $this->getData(), $this->statusTypes, $this);

        return parent::_afterSave();
    }

    /**
     * Return the database Id based on code
     *
     * @param $code
     * @return int
     */
    protected function getWebsiteIdByCode($code)
    {
        $key = md5(sprintf("%s|%s", __METHOD__, $code));
        if ($result = unserialize($this->getCache()->load($key))) {
            return $result;
        } else {
            $result = Mage::getResourceModel('core/website_collection')->addFieldToFilter('code', $code)->getFirstItem()->getId();
            $this->getCache()->save(serialize($result), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());

            return $result;
        }
    }

    /**
     * @return Dyna_Cache_Model_Cache
     */
    protected function getCache()
    {
        if (!$this->_cache) {
            $this->_cache = Mage::getSingleton('dyna_cache/cache');
        }

        return $this->_cache;
    }

    /**
     * @return Omnius_Checkout_Model_Sales_Quote_Item[]
     */
    public function getOldItems()
    {
        return $this->_oldItems;
    }

    /**
     * @param Omnius_Checkout_Model_Sales_Quote_Item[] $oldItems
     * @return $this
     */
    public function setOldItems($oldItems)
    {
        $this->_oldItems = $oldItems;
        return $this;
    }

    /**
     * @return Omnius_Checkout_Model_Sales_Quote|null
     */
    public function getModifiedQuote()
    {
        return $this->_modifiedQuote;
    }

    /**
     * @param Omnius_Checkout_Model_Sales_Quote|null $modifiedQuote
     * @return $this
     */
    public function setModifiedQuote($modifiedQuote)
    {
        $this->_modifiedQuote = $modifiedQuote;
        return $this;
    }

    /**
     * @return Omnius_Checkout_Model_Sales_Quote_Address|null
     */
    public function getNewAddress()
    {
        return $this->_newAddress;
    }

    /**
     * @param Omnius_Checkout_Model_Sales_Quote_Address|null $newAddress
     * @return $this
     */
    public function setNewAddress($newAddress)
    {
        $this->_newAddress = $newAddress;
        return $this;
    }

    /**
     * @return Omnius_Checkout_Model_Sales_Order
     */
    public function getProcessedOrder()
    {
        return $this->_processedOrder;
    }

    /**
     * @param Omnius_Checkout_Model_Sales_Order $processedOrder
     * @return $this
     */
    public function setProcessedOrder($processedOrder)
    {
        $this->_processedOrder = $processedOrder;
        return $this;
    }

    /** @var Omnius_Checkout_Model_Sales_Quote_Item[] */
    protected $_items = [];
    /** @var Omnius_Checkout_Model_Sales_Quote_Address|null */
    protected $_address = null;

    /** @var bool */
    protected $_hasEdited = false;

    /** @var null|string  */
    protected $_orderHash = null;

    /** @var bool */
    protected $_isCancelled = false;

    /**
     * @return $this
     */
    public function build()
    {
        $this->_hasEdited = (bool) $this->getModifiedQuote();
        $this->_orderHash = md5(serialize($this->getAddress()) . serialize($this->getPayment()));
        return $this;
    }

    /**
     *
     * @param null $ofType String or array of strings representing the type(s) to retrieve
     * @param null $notOfType String or array of strings representing type(s) to skip
     * @return Omnius_Checkout_Model_Sales_Quote_Item[]
     */
    public function getItems($ofType = null, $notOffType = null)
    {
        /** @var Omnius_Checkout_Model_Sales_Quote_Item[] $allItems */
        if($this->getQuoteId()) {
            $allItems = $this->getQuote()->getPackageItems($this->getPackageId());
        }
        else{
            $allSuperorderItems = Mage::getModel('superorder/superorder')->load($this->getOrderId())->getOrderedItems(true);
            $allItems = isset($allSuperorderItems[$this->getPackageId()]) ? $allSuperorderItems[$this->getPackageId()] : array();
        }

        if ($ofType || $notOffType) {
            if($ofType){
                $ofType = is_array($ofType) ? $ofType : array($ofType);
                $ofType = array_map('strtolower', $ofType);
            }
            if($notOffType){
                $notOffType = is_array($notOffType) ? $notOffType : array($notOffType);
                $notOffType = array_map('strtolower', $notOffType);
            }

            /** @var Omnius_Checkout_Model_Sales_Quote_Item[] $filteredItems */
            $filteredItems = [];
            foreach ($allItems as $item) {
                $productSubTypePackage = strtolower(Mage::getResourceModel('eav/entity_attribute_option_collection')
                    ->setStoreFilter(0, false)
                    ->setIdFilter($item->getProduct()->getData(Omnius_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR))
                    ->getFirstItem()
                    ->getValue());

                if (($ofType && in_array(strtolower($productSubTypePackage), $ofType)) ||
                    ($notOffType && !in_array(strtolower($productSubTypePackage), $notOffType))){
                    $filteredItems[] = $item;
                }
            }

            return $filteredItems;
        } else {
            return $allItems;
        }
    }

    /**
     * @param Omnius_Checkout_Model_Sales_Quote_Item[] $items
     * @return $this
     */
    protected function setItems($items)
    {
        $this->_items = $items;
        return $this;
    }

    /**
     * @param Omnius_Checkout_Model_Sales_Quote_Item $item
     * @return $this
     */
    protected function addItem($item)
    {
        $this->_items[] = $item;
        return $this;
    }

    /**
     * @return Omnius_Checkout_Model_Sales_Quote_Address|null
     */
    public function getAddress()
    {
        return $this->_address;
    }

    /**
     * @param Omnius_Checkout_Model_Sales_Quote_Address $address
     * @return $this
     */
    protected function setAddress($address)
    {
        $this->_address = $address;
        return $this;
    }

    /**
     * @return bool
     */
    public function getIsEdited()
    {
        return $this->_hasEdited;
    }

    /**
     * @return null|string
     */
    public function getOrderHash()
    {
        return $this->_orderHash;
    }

    /**
     * @return boolean
     */
    public function getIsCancelled()
    {
        return $this->_isCancelled;
    }

    /**
     * @param boolean $isCancelled
     * @return $this
     */
    public function setCancelled($isCancelled)
    {
        $this->_isCancelled = $isCancelled;
        return $this;
    }

    /**
     * Returns the binary code of the package state during the order edit flow.
     * @return int
     */
    public function getBinaryState()
    {
        return $this->_binaryState;
    }

    /**
     * @return string
     */
    protected function getFinalOrders()
    {
        return "
        superorder.order_status = '" . Omnius_Superorder_Model_Superorder::SO_CANCELLED
        . "' or superorder.order_status = '" . Omnius_Superorder_Model_Superorder::SO_CLOSED
        . "' or superorder.order_status = '" . Omnius_Superorder_Model_Superorder::SO_FULFILLED
        . "' or superorder.order_status = '" . Omnius_Superorder_Model_Superorder::SO_STATUS_CANCELLED
        . "' or superorder.order_status = '" . Omnius_Superorder_Model_Superorder::SO_STATUS_REJECTED
        . "' or superorder.order_status = '" . Omnius_Superorder_Model_Superorder::SO_VALIDATION_FAILED
        . "'";
    }

    /**
     * @return string
     */
    protected function getClusterTwo()
    {
        return "
        superorder.error_detail is NULL
        and (
            main_table.vf_status_code is NULL
            OR TRIM(main_table.vf_status_code) = ''
        )
        and main_table.status <> '" . self::ESB_PACKAGE_STATUS_DELIVERED
        . "' and (
            (
                main_table.creditcheck_status <> '" . self::ESB_PACKAGE_NETWORK_STATUS_CC_PENDING
        . "' and main_table.creditcheck_status <> '" . self::ESB_PACKAGE_NETWORK_STATUS_CC_INITIAL
        . "' and main_table.creditcheck_status <> '" . self::ESB_PACKAGE_NETWORK_STATUS_CC_REFERRED
        . "' and main_table.creditcheck_status <> '" . self::ESB_PACKAGE_NETWORK_STATUS_CC_ADDITIONNAL_INFO_REQUIRED
        . "' )
            or main_table.creditcheck_status is NULL
        )
        and (
                (
                    main_table.porting_status <> '" . self::ESB_PACKAGE_NETWORK_STATUS_NP_PENDING
        . "' and main_table.porting_status <> '" . self::ESB_PACKAGE_NETWORK_STATUS_NP_INITIAL
        . "' )
                or main_table.porting_status is NULL
            )
            and not exists (
                            select 1 from catalog_package where
                            order_id=superorder.entity_id
                            and
                            (
                                creditcheck_status='" . self::ESB_PACKAGE_NETWORK_STATUS_CC_PENDING
        . "' OR creditcheck_status='" . self::ESB_PACKAGE_NETWORK_STATUS_CC_INITIAL
        . "' OR creditcheck_status='" . self::ESB_PACKAGE_NETWORK_STATUS_CC_REFERRED
        . "' OR creditcheck_status='" . self::ESB_PACKAGE_NETWORK_STATUS_CC_ADDITIONNAL_INFO_REQUIRED
        . "' OR porting_status='" . self::ESB_PACKAGE_NETWORK_STATUS_NP_PENDING
        . "' OR porting_status='" . self::ESB_PACKAGE_NETWORK_STATUS_NP_INITIAL
        . "' )
                        )
                        ";
    }

    /**
     * @return string
     */
    protected function getClusterOne()
    {
        return "
        (
            superorder.error_detail IS NOT NULL
            OR (
                main_table.vf_status_code IS NOT NULL
                AND TRIM(main_table.vf_status_code) <> ''
            )
        )
        OR (
            not exists (
            (
                SELECT 1 FROM catalog_package
                WHERE order_id = superorder.entity_id
                AND (
                        (
                            status <> '" . self::ESB_PACKAGE_STATUS_DELIVERED
        . "' AND (
                                    (
                                        creditcheck_status NOT IN ( '"
        . self::ESB_PACKAGE_NETWORK_STATUS_CC_INITIAL
        . "', '" . self::ESB_PACKAGE_NETWORK_STATUS_CC_PENDING
        . "', '" . self::ESB_PACKAGE_NETWORK_STATUS_CC_REFERRED
        . "', '" . self::ESB_PACKAGE_NETWORK_STATUS_CC_ADDITIONNAL_INFO_REQUIRED
        . "')
                                            or creditcheck_status is null
                                    ) AND (
                                        porting_status NOT IN ( '"
        . self::ESB_PACKAGE_NETWORK_STATUS_NP_INITIAL
        . "', '" . self::ESB_PACKAGE_NETWORK_STATUS_NP_PENDING
        . "')
                                                or porting_status is null
                                            )
                                        )
                                    )
                                    OR (
                                        creditcheck_status IN ('"
        . self::ESB_PACKAGE_NETWORK_STATUS_CC_INITIAL
        . "', '" . self::ESB_PACKAGE_NETWORK_STATUS_CC_PENDING
        . "', '" . self::ESB_PACKAGE_NETWORK_STATUS_CC_REFERRED
        . "', '" . self::ESB_PACKAGE_NETWORK_STATUS_CC_ADDITIONNAL_INFO_REQUIRED
        . "') OR porting_status IN ('"
        . self::ESB_PACKAGE_NETWORK_STATUS_NP_INITIAL
        . "','" . self::ESB_PACKAGE_NETWORK_STATUS_NP_PENDING
        . "')
                                    )
                                )
                            )
                        )
                    )
                ";
    }

    /**
     * @return string
     */
    protected function getClusterThree()
    {
        return "
        superorder.error_detail is NULL
        and (
                main_table.vf_status_code is NULL
                OR TRIM(main_table.vf_status_code) = ''
            )
        and (
                main_table.creditcheck_status = '" . self::ESB_PACKAGE_NETWORK_STATUS_CC_PENDING
        . "' or main_table.creditcheck_status = '" . self::ESB_PACKAGE_NETWORK_STATUS_CC_INITIAL
        . "' or main_table.creditcheck_status = '" . self::ESB_PACKAGE_NETWORK_STATUS_CC_REFERRED
        . "' or main_table.creditcheck_status = '" . self::ESB_PACKAGE_NETWORK_STATUS_CC_ADDITIONNAL_INFO_REQUIRED
        . "' or main_table.porting_status = '" . self::ESB_PACKAGE_NETWORK_STATUS_NP_PENDING
        . "' or main_table.porting_status = '" . self::ESB_PACKAGE_NETWORK_STATUS_NP_INITIAL
        . "')
            ";
    }

    /**
     * @param $item
     * @param $cloneItem
     * @param $image
     * @param $name
     * @param $maf
     * @param $tempKey
     * @param $price
     */
    protected function getItemPromoPrice($item, $cloneItem, &$image, &$name, &$maf, $tempKey, &$price)
    {
        $priceAfterDiscount = $this->getWithBtw()
            ? $item->getItemFinalPriceInclTax()
            : $item->getItemFinalPriceExclTax();

        $mafAfterDiscount = $this->getWithBtw()
            ? $item->getItemFinalMafInclTax()
            : $item->getItemFinalMafExclTax();

        $image[] = '';
        $name[] = '<label class="list-style">' . $cloneItem->getName() . '</label>';

        if ($cloneItem->getProduct()->getPrijsMafNewAmount() || $cloneItem->getProduct()->getPrijsMafDiscountPercent()) {
            $maf[$tempKey] = str_replace('<label>', '<label class="striked">', $maf[$tempKey]);
            $maf[] = '<label>' . Mage::helper('core')->currency($mafAfterDiscount, true, false) . '</label>';
            $price[count($price) - 1] = '<label></label>';
            $price[] = (isset($prodPrice) && (int) $prodPrice) ? ('<label>' . Mage::helper('core')->currency($priceAfterDiscount, true, false) . '</label>') : '<label></label>';
        } else {
            $maf[] = '<label></label>';
            $price[] = '<label></label>';
        }
    }

    /**
     * @param Omnius_Checkout_Model_Sales_Quote_Item $item
     * @param $allItems
     * @param $wasDeleted
     * @param $itemProps
     * @param $imgWH
     * @param $image
     * @param $name
     * @param $maf
     * @param $price
     * @param $productType
     */
    protected function parseItemWithMaf($item, $allItems, $wasDeleted, $itemProps, $imgWH, &$image, &$name, &$maf, &$price, $productType)
    {
        $regularMaf = $this->getItemRegularMaf($item, $itemProps);

        //add name and regular maf
        $image[] = '<img src="' . Mage::helper('catalog/image')->init($item->getProduct(), 'small_image')->resize($imgWH, $imgWH) . '"/>';
        $name[] = '<label><strong>' . $item->getName() . '</strong></label>';
        $maf[] = '<label>' . Mage::helper('core')->currency($regularMaf, true, false) . '</label>';
        $price[] = (isset($prodPrice) && (int) $prodPrice) ? ('<label>' . Mage::helper('core')->currency($prodPrice, true, false) . '</label>') : '<label></label>';
        // Move the internal pointer to the end of the array
        end($maf);
        $tempKey = key($maf);

        foreach ($allItems as $cloneItem) {
            $hasDiscount = $item->getMafDiscountAmount() >= 0 || $cloneItem->getProduct()->getPrijsAansluitPromoBedrag() || $cloneItem->getProduct()->getPrijsAansluitPromoProcent();
            $isPromoForPackage = $cloneItem->isPromo() && $cloneItem->getTargetId() == $item->getProductId() && $cloneItem->getPackageId() == $item->getPackageId();
            if ($isPromoForPackage && $hasDiscount) {
                $this->getItemPromoPrice($item, $cloneItem, $image, $name, $maf, $tempKey, $price);
            }
        }
        unset($tempKey);

        if ($item->getData('connection_cost') && $item->getData('connection_cost') > 0) {
            $connCost = Mage::helper('core')->currency(
                Mage::helper('tax')->getPrice(
                    $item->getProduct(),
                    $item->getData('connection_cost'),
                    $this->getWithBtw()
                ),
                true,
                false
            );
            $image[] = '';
            $name[] = '<label class="list-style">' . Mage::helper('omnius_checkout')->__('Connection fees') . ': ' . $connCost . '</label>';
            $maf[] = '<label></label>';
            $price[] = '<label></label>';
        }
        if (in_array(Omnius_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION, $productType)) {
            if (!$wasDeleted && $this->getPhoneNumber() && current($productType) !== Omnius_Catalog_Model_Type::SUBTYPE_ADDON) {
                $image[] = '';
                $name[] = '<label class="list-style">' . Mage::helper('omnius_checkout')->__('Mobile number') . ': ' . Mage::helper('omnius_checkout')->formatCtn($this->getPhoneNumber()) . '</label>';
                $maf[] = '<label></label>';
                $price[] = '<label></label>';
            }
            if (!$wasDeleted && $this->getSimNumber() && current($productType) !== Omnius_Catalog_Model_Type::SUBTYPE_ADDON) {
                $image[] = '';
                $name[] = '<label class="list-style">' . Mage::helper('omnius_checkout')->__('SIMNR') . ': ' . $this->getSimNumber() . '</label>';
                $maf[] = '<label></label>';
                $price[] = '<label></label>';
            }
        }
    }

    /**
     * @param $item
     * @param $allItems
     * @param $mixMatch
     * @param $productType
     * @param $imgWH
     * @param $image
     * @param $name
     * @param $prodPrice
     * @param $price
     * @param $maf
     */
    protected function parseItemWithOneTimePrice($item, $allItems, $mixMatch, $productType, $imgWH, &$image, &$name, $prodPrice, &$price, &$maf)
    {
        if (isset($mixMatch) && $mixMatch !== null && in_array(Omnius_Catalog_Model_Type::SUBTYPE_DEVICE, $productType)) {
            $image[] = '<img src="' . Mage::helper('catalog/image')->init($item->getProduct(), 'small_image')->resize($imgWH, $imgWH) . '"/>';
            $name[] = '<label><strong>' . $item->getName() . '</strong></label>';
            $price[] = '<label class="striked">' . Mage::helper('core')->currency($prodPrice, true, false) . '</label>';
            $maf[] = '<label></label>';

            $image[] = '';
            $name[] = '';
            $price[] = '<label>' . Mage::helper('core')->currency($mixMatch, true, false) . '</label>';
            $maf[] = '<label></label>';
        } else {
            $image[] = '<img src="' . Mage::helper('catalog/image')->init($item->getProduct(), 'small_image')->resize($imgWH, $imgWH) . '"/>';
            $name[] = '<label><strong>' . $item->getName() . '</strong></label>';
            $price[] = '<label>' . Mage::helper('core')->currency($prodPrice, true, false) . '</label>';
            $maf[] = '<label></label>';
        }

        if ($item->getDiscountAmount() != null && $item->getDiscountAmount() > 0) {
            $price[count($price) - 1] = str_replace('<label>', '<label class="striked">', $price[count($price) - 1]);
            $appliedRuleIds = Mage::helper('pricerules')->explodeTrimmed($item->getAppliedRuleIds());
            $appliedRules = Mage::getModel('salesrule/rule')->getCollection()
                ->addFieldToFilter('simple_action', array('neq' => 'ampromo_items'))
                ->addFieldToFilter('rule_id', array('in' => $appliedRuleIds));
            $discountNames = array();

            foreach ($appliedRules as $rule) {
                $discountNames[] = $rule->getName();
            }

            $deviceItem = isset($mixMatch) && $mixMatch !== null ? $mixMatch : $prodPrice;
            $discount = (float) $item->getDiscountAmount();
            $deviceItem -= $this->getWithBtw() ? $discount : ($discount - $item->getHiddenTaxAmount());

            $image[] = '';
            $name[] = '<label class="list-style">' . Mage::helper('omnius_checkout')->__(implode(', ', $discountNames)) . '</label>';
            $price[] = '<label>' . Mage::helper('core')->currency($deviceItem, true, false) . '</label>';
            $maf[] = '<label></label>';
        }

        // Check if has promotional product with maf discount
        if (in_array(Omnius_Catalog_Model_Type::SUBTYPE_DEVICE, $productType)) {
            foreach ($allItems as $cloneItem) {
                $this->parseCloneItem($item, $image, $name, $price, $maf, $cloneItem);
            }
        }
        //display sim and imei
        $c1Cond = !$item->getItemDoa() && $item->getEditOrderId() && $this->getImei();
        if (in_array(Omnius_Catalog_Model_Type::SUBTYPE_DEVICE, $productType) && $c1Cond) {
            $image[] = '';
            $name[] = '<label class="list-style">IMEI: ' . $this->getImei() . '</label>';
            $maf[] = '<label></label>';
            $price[] = '<label></label>';
        }
    }

    /**
     * @param $item
     * @param $image
     * @param $name
     * @param $price
     * @param $maf
     * @param $cloneItem
     */
    protected function parseCloneWeee($item, &$image, &$name, &$price, &$maf, $cloneItem)
    {
        $weeeTaxApplied = $cloneItem->getWeeeTaxAppliedUnserialized();

        if (!empty($weeeTaxApplied)) {
            foreach ($weeeTaxApplied as $taxApplied) {
                $connCost = Mage::helper('core')->currency(
                    Mage::helper('tax')->getPrice(
                        $item->getProduct(),
                        $taxApplied['base_row_amount_incl_tax'],
                        $this->getWithBtw()
                    ),
                    true,
                    false
                );
                $image[] = '';
                $name[] = '<label class="list-style">' . Mage::helper('omnius_checkout')->__($taxApplied['title']) . '</label>';
                $price[] = '<label>' . $connCost . '</label>';
                $maf[] = '<label></label>';
            }
        }
    }

    /**
     * @param $image
     * @param $name
     * @param $price
     * @param $maf
     * @param $cloneItem
     */
    protected function parseCloneLevy(&$image, &$name, &$price, &$maf, $cloneItem)
    {
        $copyLevy = $this->getWithBtw() ? $cloneItem->getRowTotalInclTax() : $cloneItem->getRowTotal();
        if ($copyLevy > 0) {
            $image[] = '';
            $name[] = '<label class="list-style">' . Mage::helper('omnius_checkout')->__($cloneItem->getName()) . '</label>';
            $price[] = '<label>' . Mage::helper('core')->currency($copyLevy, true, false) . '</label>';
            $maf[] = '<label></label>';
        }
    }

    /**
     * @param $image
     * @param $name
     * @param $price
     * @param $maf
     * @param $cloneItem
     */
    protected function parseClonePromo(&$image, &$name, &$price, &$maf, $cloneItem)
    {
        $image[] = '';
        $name[] = '<label class="list-style">' . $cloneItem->getName() . '</label>';
        $price[count($price) - 1] = str_replace('<label>', '<label class="striked">', $price[count($price) - 1]);
        $price[] = '<label>' . Mage::helper('core')->currency($cloneItem->getRowTotalInclTax(), true, false) . '</label>';
        $maf[] = '<label></label>';
    }

    /**
     * @param $item
     * @param $itemProps
     * @return float
     */
    protected function getItemRegularMaf($item, $itemProps)
    {
        if (!$this->getIsExtendedCart()) {
            $regularMaf = Mage::helper('tax')->getPrice(
                $item->getProduct(),
                $itemProps['product']['regular_maf'],
                $this->getWithBtw()
            );
        } else {
            $regularMaf = $this->getWithBtw()
                ? $item->getMafRowTotalInclTax()
                : $item->getMaf();

        }

        return $regularMaf;
    }

    /**
     * @param Omnius_Checkout_Model_Sales_Quote_Item $item
     * @param $total
     * @param $subtotalPrice
     * @param $noPromoMaf
     * @param $noPromoTax
     * @param $subtotalMaf
     * @param $taxMaf
     * @param $taxPrice
     * @param $totalMaf
     * @param $additionalSubtotal
     * @param $additionalTax
     * @param $additionalTotal
     */
    protected function parsePricesForItem($item, &$total, &$subtotalPrice, &$noPromoMaf, &$noPromoTax, &$subtotalMaf, &$taxMaf, &$taxPrice, &$totalMaf, &$additionalSubtotal, &$additionalTax, &$additionalTotal)
    {
        $discountAmount = $item->getDiscountAmount();
        $hiddenTaxAmount = $item->getHiddenTaxAmount();
        $taxes = $item->getWeeeTaxAppliedAmount();

        $weeeTaxes = ($taxes && $taxes > 0) ? $taxes : 0;
        $total += $weeeTaxes;
        $subtotalPrice += $weeeTaxes;

        $rowTotal = ceil($item->getRowTotal()) ? $item->getItemFinalPriceExclTax() : 0;
        $subtotalPrice += $rowTotal;

        $noPromoMaf += $item->getMaf();
        $noPromoTax += $item->getMafInclTax();

        $rowTotalMaf = ceil($item->getMafRowTotal()) ? $item->getItemFinalMafExclTax() : 0;
        $subtotalMaf += $rowTotalMaf;
        $mafTaxAmount = ceil($item->getMafTaxAmount()) ? $item->getMafTaxAmount() : 0;
        $taxMaf += $mafTaxAmount;
        $taxAmount = ceil($item->getTaxAmount()) ? $item->getTaxAmount() : 0;
        $taxPrice += $taxAmount;
        $total += ($rowTotal + $taxAmount);
        $totalMaf += ($rowTotalMaf + $mafTaxAmount);

        $mixMatchSubtotal = $item->getMixmatchSubtotal() == null ? $rowTotal : $item->getMixmatchSubtotal() - ($discountAmount - $hiddenTaxAmount);
        $additionalTaxAmount = $item->getMixmatchTax() == null ? $taxAmount : $item->getMixmatchTax() - $item->getHiddenTaxAmount();

        $additionalSubtotal += $mixMatchSubtotal;
        $additionalTax += $additionalTaxAmount;
        $additionalTotal += $mixMatchSubtotal + $additionalTaxAmount;
    }

    /**
     * @param Omnius_Checkout_Model_Sales_Quote_Item $item
     * @param $image
     * @param $name
     * @param $price
     * @param $maf
     * @param Omnius_Checkout_Model_Sales_Quote_Item $cloneItem
     */
    protected function parseCloneItem($item, &$image, &$name, &$price, &$maf, $cloneItem)
    {
        if ($cloneItem->isPromo() && (int) $cloneItem->getRowTotalInclTax()) {
            $this->parseClonePromo($image, $name, $price, $maf, $cloneItem);
        }

        if ($cloneItem->getTargetId() == $item->getProductId() && $cloneItem->getProduct()->isServiceItem() && $item->getPackageId() == $cloneItem->getPackageId()) {
            $this->parseCloneLevy($image, $name, $price, $maf, $cloneItem);
        }

        if ($cloneItem->getId() == $item->getId()) {
            $this->parseCloneWeee($item, $image, $name, $price, $maf, $cloneItem);
        }
    }

    /**
     * @param Omnius_Checkout_Model_Sales_Quote_Item $item
     * @param $itemProps
     * @param $prodPrice
     * @param $mixMatch
     * @param $imgWH
     */
    protected function getContentForCheckout($item, &$itemProps, &$prodPrice, &$mixMatch, &$imgWH)
    {
        $this->setWithBtw(true);
        $prodPrice = $itemProps['product']['special_price'] ?: $itemProps['product']['price'];
        $mixMatch = isset($itemProps['mixmatch']) ? $itemProps['mixmatch'] : null;
        if (isset($itemProps['mixmatch_subtotal']) && $itemProps['mixmatch_subtotal'] != null) {
            $mixMatch = $itemProps['mixmatch_subtotal'] + $itemProps['mixmatch_tax'];
        }
        $customer = Mage::getModel('customer/session')->getCustomer();
        if ($customer->getIsBusiness()) {
            $this->setWithBtw(false);
            if ($mixMatch) {
                $mixMatch = Mage::helper('tax')->getPrice($item->getProduct(), $mixMatch, false);
            }
            $prodPrice = Mage::helper('tax')->getPrice($item->getProduct(), $prodPrice, false);
        }

        $imgWH = 25;
    }

    /**
     * @param Omnius_Checkout_Model_Sales_Quote_Item $item
     * @param $mixMatch
     * @param $prodPrice
     * @param $imgWH
     */
    protected function getContentForRightSidebar($item, &$mixMatch, &$prodPrice, &$imgWH)
    {
        $jsonData = json_decode($item->getOrderItemPrices(), true);
        if (Mage::getSingleton('checkout/cart')->getQuote()->getIsOffer() && isset($jsonData['mixmatch_excl_tax'])) {
            $mixMatch =
                isset($jsonData['mixmatch_subtotal'])
                && $jsonData['mixmatch_subtotal'] != null
                    ? $jsonData['mixmatch_subtotal']
                    : $jsonData['mixmatch_excl_tax'];

        } elseif (Mage::helper('tax')->getPrice($item->getProduct(), $item->getProduct()->getFinalPrice(), false) - $item->getRowTotal() != 0) {
            $item->setHasMixMatch(true);
            $mixMatch = $this->getWithBtw()
                ? ($item->getMixmatchSubtotal() != null ? $item->getMixmatchSubtotal() + $item->getMixmatchTax() : $item->getRowTotalInclTax())
                : ($item->getMixmatchSubtotal() != null ? $item->getMixmatchSubtotal() : $item->getRowTotal());
        }
        $productPrices = Mage::helper('omnius_configurator')->getBlockRowData($item);
        $prodPrice = $this->getWithBtw() ? (isset($productPrices['priceTax']) ? $productPrices['priceTax'] : 0) : $productPrices['price'];
        if (Mage::getSingleton('checkout/cart')->getQuote()->getIsOffer()) {
            $tmpPriceData = json_decode($item->getData('order_item_prices'), true);
            $prodPrice = isset($tmpPriceData['product']['price_excl']) ? $tmpPriceData['product']['price_excl'] : $prodPrice;
        }

        $imgWH = 50;
    }

    /**
     * @param $packages
     */
    protected function handleEditedPackages(&$packages)
    {
        if (!$packages) {
            $quote = Mage::getModel('sales/quote')->load(Mage::getSingleton('customer/session')->getOrderEdit());
            $deliveryOrders = Mage::getModel('sales/order')->getNonEditedOrderItems($quote->getSuperOrderEditId());
            foreach ($deliveryOrders as $deliveryOrder) {
                foreach ($deliveryOrder->getAllItems() as $deliveryOrderItem) {
                    if (isset($packages[$deliveryOrderItem->getPackageId()])) {
                        continue;
                    }
                    $packages[$deliveryOrderItem->getPackageId()] = $deliveryOrder->getId();
                }
            }
            Mage::getSingleton('core/session')->setData('packageOrderCorrespondence' . Mage::getSingleton('customer/session')->getOrderEdit(), $packages);
        }
    }

    /**
     * @param $simcardChanged
     * @param $oldOrders
     * @param $quoteId
     * @param $parentOrderLevel
     */
    protected function handleChangedSimcard($simcardChanged, $oldOrders, $quoteId, $parentOrderLevel)
    {
        if (isset($simcardChanged[$this->getPackageId()]) && $quoteId) {
            $old_class = ' old_package';
            $old_class .= ($parentOrderLevel > 1) ? ' striked' : '';
            $old_price_class = ' class="striked old_package"';
            $affected_id = $quoteId;
            $affected = 'quoteItems';

            foreach ($simcardChanged[$this->getPackageId()] as $orderId => $item) {
                if (!isset($oldOrders) || !in_array($orderId, $oldOrders)) {
                    if ($item->getParentOrderLevel() > 1) {
                        $affected_id = $orderId;
                        $affected = 'orderItems';
                    }
                    $this->{$affected}[$affected_id]['image'][] = '';
                    $this->{$affected}[$affected_id]['name'][] = '<label class="list-style' . $old_class . '">' . $item->getName() . '</label>';
                    $this->{$affected}[$affected_id]['maf'][] = '<label></label>';
                    $this->{$affected}[$affected_id]['price'][] = '<label' . $old_price_class . '>' . Mage::helper('core')->currency($item->getRowTotal(), true, false) . '</label>';
                }
            }
        }
    }

    /**
     * If the product is still present in the new order, it was not changed and we skip it
     *
     * @param $item
     * @param $parentOrderItems
     * @return bool
     */
    protected function skipNotEdited($item, $parentOrderItems)
    {
        foreach ($parentOrderItems as $parentItem) {
            if ($item->getPackageId() == $parentItem->getPackageId() && $item->getProductId() == $parentItem->getProductId()) {
                // Product was not changed so we skip it
                return true;
            }
        }

        return false;
    }

    /**
     * Check if edited order package is paid
     *
     * @return bool|null
     */
    protected function checkEditedOrders()
    {
        $deliveryOrders = Mage::getModel('sales/order')->getNonEditedOrderItems($this->getOrderId(), true, true);
        /** @var Omnius_Checkout_Model_Sales_Order $deliveryOrder */
        foreach ($deliveryOrders as $deliveryOrder) {
            foreach ($deliveryOrder->getAllItems() as $item) {
                if ($item->getPackageId() == $this->getPackageId()) {
                    return ($deliveryOrder->getBaseTotalDue() > 0);
                }
            }
        }

        return null;
    }

    /**
     * Returns the current package status (OPEN or COMPLETED), determined by the status of package items
     * Logic is moved to Configuration Model, preventing model from beying to heavy
     * @return Omnius_Package_Model_Configuration
     */
    public function getPackageConfiguration()
    {
        /** @var Omnius_Package_Model_Configuration $packageConfiguration */
        return $packageConfiguration = Mage::getModel('package/configuration')
            ->setPackage($this);
    }

    /**
     * Return the quote instance associated to this package
     * @return Omnius_Checkout_Model_Sales_Quote
     */
    public function getQuote()
    {
        if (!$this->quote) {
            $this->quote = Mage::getModel('sales/quote')->load($this->getQuoteId());
        }

        return $this->quote;
    }

    /**
     * Set quote on current instance. Useful when loading package from quote instance to skip loading from db
     * @param Omnius_Checkout_Model_Sales_Quote $quote
     * @return $this
     */
    public function setQuote(Omnius_Checkout_Model_Sales_Quote $quote)
    {
        $this->quote = $quote;

        return $this;
    }
}
