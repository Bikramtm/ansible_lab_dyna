<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Package_Model_Mysql4_Package extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init("package/package", "entity_id");
    }
}