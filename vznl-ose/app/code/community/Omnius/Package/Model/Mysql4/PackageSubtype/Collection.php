<?php

/**
 * Class Omnius_Package_Model_Mysql4_PackageSubtype_Collection
 */
class Omnius_Package_Model_Mysql4_PackageSubtype_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        $this->_init("package/packageSubtype");
    }

    /**
     * Load all package subtypes by an instance of related package type
     * @param Omnius_Package_Model_PackageType $packageType
     * @return $this
     */
    public function loadByPackageType(Omnius_Package_Model_PackageType $packageType)
    {
        $this->addFieldToFilter("type_package_id", ['eq' => $packageType->getId()]);
        return $this;
    }

    /**
     * @param Omnius_Package_Model_PackageType $packageType
     * @param $subtypeCodes
     * @return $this
     */
    public function loadByPackageTypeAndCode( Omnius_Package_Model_PackageType $packageType, $subtypeCodes )
    {
        if ( !is_array($subtypeCodes) ) {
            $subtypeCodes = array($subtypeCodes);
        }

        $this->loadByPackageType($packageType)
            ->addFieldToFilter("package_code", ['in' => $subtypeCodes]);
        return $this;
    }
}
