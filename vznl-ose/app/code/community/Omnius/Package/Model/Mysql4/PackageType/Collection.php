<?php

/**
 * Class Omnius_Package_Model_Mysql4_PackageType_Collection
 */
class Omnius_Package_Model_Mysql4_PackageType_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        $this->_init("package/packageType");
    }

    /**
     * Return a package subtype model by package code
     * @param string $code
     * @return Omnius_Package_Model_PackageSubtype
     */
    public function loadByCode($code)
    {
        return $this->addFieldToFilter('package_code', ['eq' => strtoupper($code)])
            ->getFirstItem();
    }
}
