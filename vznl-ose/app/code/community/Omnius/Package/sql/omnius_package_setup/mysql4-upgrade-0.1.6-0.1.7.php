<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;
$installer->startSetup();
$connection = $installer->getConnection();
$connection->addColumn($this->getTable('catalog_package'), 'coupon', 'varchar(100) null');
$installer->endSetup();