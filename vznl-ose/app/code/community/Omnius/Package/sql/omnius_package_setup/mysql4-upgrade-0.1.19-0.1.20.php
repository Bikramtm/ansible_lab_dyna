<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;
$installer->startSetup();
$connection = $this->getConnection();
$connection->addColumn($this->getTable('catalog_package'), 'complete_return', Varien_Db_Ddl_Table::TYPE_BOOLEAN);
$connection->addColumn($this->getTable('catalog_package'), 'not_damaged_items', Varien_Db_Ddl_Table::TYPE_BOOLEAN);
$connection->addColumn($this->getTable('catalog_package'), 'return_notes', Varien_Db_Ddl_Table::TYPE_TEXT);
$connection->addColumn($this->getTable('catalog_package'), 'returned_by', 'varchar(50) null');
$connection->addColumn($this->getTable('catalog_package'), 'return_date', Varien_Db_Ddl_Table::TYPE_DATETIME);
$connection->addColumn($this->getTable('catalog_package'), 'cancellation_date', Varien_Db_Ddl_Table::TYPE_DATETIME);
$installer->endSetup();
