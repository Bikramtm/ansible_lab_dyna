<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Eav_Model_Entity_Setup $this */
$this->startSetup();
$connection = $this->getConnection();

$connection->addColumn($this->getTable('package/package'), 'ctn_original_end_date',
    array(
        'type' => Varien_Db_Ddl_Table::TYPE_DATETIME,
        'comment' => 'Date at which the initial CTN ends',
        'required' => false,
        'after' => 'ctn',
        'nullable' => true
    )
);

$connection->addColumn($this->getTable('package/package'), 'ctn_original_start_date',
    array(
        'type' => Varien_Db_Ddl_Table::TYPE_DATETIME,
        'comment' => 'Date at which the initial CTN starts',
        'required' => false,
        'after' => 'ctn',
        'nullable' => true
    )
);

$this->endSetup();
