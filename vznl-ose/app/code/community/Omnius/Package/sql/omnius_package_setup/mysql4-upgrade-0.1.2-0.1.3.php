<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;

$installer->startSetup();
$connection = $installer->getConnection();
$connection->addColumn($this->getTable('catalog_package'), 'vf_status_code', 'varchar(45) null');
$connection->addColumn($this->getTable('catalog_package'), 'vf_status_desc', 'text null');

$installer->endSetup();
