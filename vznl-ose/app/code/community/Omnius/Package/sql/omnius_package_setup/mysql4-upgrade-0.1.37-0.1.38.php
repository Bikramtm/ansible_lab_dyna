<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Eav_Model_Entity_Setup $this */
$this->startSetup();
$connection = $this->getConnection();
$items = [
    'esb_manual_activation' => [
        'comment' => 'ESB confirmation for the manual activation',
        'type' => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
        'required' => false,
    ]
];
foreach ($items as $code => $data) {
    $connection->addColumn($this->getTable('package/package'), $code, $data);
}
$this->endSetup();
