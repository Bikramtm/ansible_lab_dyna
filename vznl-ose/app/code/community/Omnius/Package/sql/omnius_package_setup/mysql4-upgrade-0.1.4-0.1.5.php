<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;
$installer->startSetup();
$connection = $installer->getConnection();
$connection->addColumn($this->getTable('catalog_package'), 'esb_status_code', 'varchar(100) null');
$connection->addColumn($this->getTable('catalog_package'), 'esb_status_desc', 'varchar(500) null');
$installer->endSetup();