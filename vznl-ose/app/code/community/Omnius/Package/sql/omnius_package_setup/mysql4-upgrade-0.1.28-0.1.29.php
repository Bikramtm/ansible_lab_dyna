<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $customerInstaller Mage_Customer_Model_Resource_Setup */
$customerInstaller = new Mage_Customer_Model_Resource_Setup('core_setup');
$customerInstaller->startSetup();
$customerInstaller->getConnection()->addIndex($customerInstaller->getTable('catalog_package'), 'IDX_CATALOG_PACKAGE_CC_UPDATE', 'creditcheck_status_updated');
$customerInstaller->endSetup();