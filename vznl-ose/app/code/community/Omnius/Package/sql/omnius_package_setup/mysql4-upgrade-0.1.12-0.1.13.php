<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * @category    Dyna
 * @package     Omnius_Package
 */

$installer = $this;

$installer->startSetup();
$connection = $installer->getConnection();
$connection->addColumn($this->getTable('catalog_package'), 'quote_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array('nullable' => true), 'Quote id');
$connection->addForeignKey(
    'fk_catalog_package_quote_id',
    'catalog_package',
    'quote_id',
    'sales_flat_quote',
    'entity_id',
    Varien_Db_Ddl_Table::ACTION_CASCADE,
    Varien_Db_Ddl_Table::ACTION_CASCADE
);

$installer->endSetup();
