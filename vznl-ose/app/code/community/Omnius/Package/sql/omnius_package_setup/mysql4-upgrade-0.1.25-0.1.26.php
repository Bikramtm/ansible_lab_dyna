<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

// Add old and new package id columns
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();
$installer->getConnection()->addColumn($this->getTable('catalog_package'), 'new_package_id', array(
    'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
    'after' => 'package_id',
    'nullable' => true,
    'comment' => 'New package ID after creating a new SuperOrder is created.'
));
$installer->getConnection()->addColumn($this->getTable('catalog_package'), 'old_package_id', array(
    'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
    'after' => 'new_package_id',
    'nullable' => true,
    'comment' => 'Previous package ID before re-ordering in the new SuperOrder.'
));
$installer->endSetup();
