<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Add package SOC codes field used for Offerte functionality
 */
$installer = $this;
$installer->startSetup();
$connection = $this->getConnection();
$connection->addColumn($this->getTable('catalog_package'), 'soc_codes', Varien_Db_Ddl_Table::TYPE_TEXT);
$installer->endSetup();