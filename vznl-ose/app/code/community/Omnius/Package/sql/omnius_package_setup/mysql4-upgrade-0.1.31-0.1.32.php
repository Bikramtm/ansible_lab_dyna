<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $this Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('sales/order_item'), 'applied_rule_names', array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'comment' => 'Applied rules names / used for deleted products'
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/order_item'), 'pack_product_type', array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'length' => 20,
        'comment' => 'Product type / used for deleted products'
    ));


$installer->getConnection()->addColumn($this->getTable('package/package'), 'actual_porting_date',
    array(
        'type' => Varien_Db_Ddl_Table::TYPE_DATETIME,
        'comment' => 'Actual porting date',
        'required' => false,
        'after' => 'contract_end_date',
        'nullable' => true
    )
);

$installer->endSetup();