<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Add package SOC codes field used for Offerte functionality
 */
$installer = $this;
$installer->startSetup();
$connection = $this->getConnection();
$connection->addColumn($this->getTable('catalog_package'), 'stock_status', 'varchar(50) null');
$connection->addColumn($this->getTable('catalog_package'), 'ciboodle_cases', Varien_Db_Ddl_Table::TYPE_TEXT);
$installer->endSetup();
