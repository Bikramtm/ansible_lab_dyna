<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Eav_Model_Entity_Setup $this */
$this->startSetup();
$connection = $this->getConnection();

$connection->addColumn($this->getTable('package/package'), 'soc_error',
    array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'comment' => 'SOC mapping errors',
        'required' => false,
        'after' => 'soc_codes',
        'nullable' => true
    )
);

$this->endSetup();