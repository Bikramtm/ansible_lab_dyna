<?php
/** @var Mage_Core_Model_Resource_Setup $this */

$this->startSetup();
$connection = $this->getConnection();

if ( !$connection->isTableExists('catalog_package_types') ) {
    // Create the catalog_package_types table
    $packageTypeTable = new Varien_Db_Ddl_Table();
    $packageTypeTable->setName($this->getTable("package/type"));

    $packageTypeTable
        ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, [
            "unsigned" => true,
            "primary" => true,
            'nullable' => false,
            "auto_increment" => true,
        ], "Primary key for types table")
        ->addColumn('package_code', Varien_Db_Ddl_Table::TYPE_TEXT, null, [
        ], "The package type code  - must be aligned with package_type product attribute")
        ->addColumn('front_end_name', Varien_Db_Ddl_Table::TYPE_TEXT, null, [
        ], "The frontend package display name")
        ->addColumn("is_visible", Varien_Db_Ddl_Table::TYPE_BOOLEAN, null, [
            'default' => 1
        ], "Fields that makes package type visible")
    ;

    $connection->createTable($packageTypeTable);
}

if ( !$connection->isTableExists('catalog_package_subtypes') ) {
    // Create the catalog_package_configurations table
    $configurationTable = new Varien_Db_Ddl_Table();
    $configurationTable->setName($this->getTable("package/subtype"));

    $configurationTable
        ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, [
            "unsigned" => true,
            "primary" => true,
            'nullable' => false,
            "auto_increment" => true,
        ], "Primary key for packages configuration")
        ->addColumn("type_package_id", Varien_Db_Ddl_Table::TYPE_INTEGER, null, [
        ], "Reference to package type resource id")
        ->addColumn('package_code', Varien_Db_Ddl_Table::TYPE_TEXT, null, [
        ], "The package type code - must be aligned with package_subtype product attribute")
        ->addColumn('front_end_name', Varien_Db_Ddl_Table::TYPE_TEXT, null, [
        ], "The frontend package display name")
        ->addColumn('front_end_position', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, [
        ], "The frontend display position of package subtype")
        ->addColumn('cardinality', Varien_Db_Ddl_Table::TYPE_TEXT, null, [
        ], "The frontend package display name")
        ->addColumn("is_visible", Varien_Db_Ddl_Table::TYPE_BOOLEAN, null, [
            'default' => 1
        ], "Fields that makes package type visible")
        ->addColumn("visible_configurator", Varien_Db_Ddl_Table::TYPE_BOOLEAN, null, [
            'default' => 1
        ], "Fields that makes package type visible in configurator")
        ->addColumn("visible_cart", Varien_Db_Ddl_Table::TYPE_BOOLEAN, null, [
            'default' => 1
        ], "Fields that makes package type visible in shopping cart")
        ->addForeignKey(
            $this->getFkName($configurationTable->getName(), 'type_package_id', $packageTypeTable->getName(), 'entity_id'),
            'type_package_id', $this->getTable('package/type'), 'entity_id',
            Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
        )
    ;

    $connection->createTable($configurationTable);
}

$this->endSetup();
