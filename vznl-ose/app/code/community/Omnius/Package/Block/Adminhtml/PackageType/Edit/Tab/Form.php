<?php

class Omnius_Package_Block_Adminhtml_PackageType_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form(array(
                "id" => "edit_form",
                "action" => $this->getUrl("*/*/save", array("id" => $this->getRequest()->getParam("id"))),
                "method" => "post",
                "enctype" => "multipart/form-data",
            )
        );
        $form->setUseContainer(true);
        $this->setForm($form);

        $fieldSet = $form->addFieldset("general",
            array(
                "legend" => "Package type configuration"
            )
        );

        $fieldSet->addField("package_code", "text",
            array(
                "name" => "package_code",
                "label" => "Package code",
                "input" => "text",
                "required" => true,
                'note' => "Mapped to <b>" . Omnius_Catalog_Model_Product::CATALOG_PACKAGE_TYPE_ATTR . "</b> product attribute (uppercase)",
            )
        );

        $fieldSet->addField("front_end_name", "text",
            array(
                "name" => "front_end_name",
                "label" => "Frontend name",
                "input" => "text",
                "required" => true,
                'note' => "This is the name displayed in cart packages",
            )
        );

        $fieldSet->addField("is_visible", "select",
            array(
                "name" => "is_visible",
                "label" => "Visible in frontend",
                "input" => "text",
                "required" => true,
                "values" => [0 => "No", 1 => "Yes"],
            )
        );

        $packageTypeModel = Mage::registry("package_model_type");
        if ($packageTypeModel) {
            $form->addValues($packageTypeModel->getData());
        }

        return parent::_prepareForm();
    }
}
