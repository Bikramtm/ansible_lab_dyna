<?php

class Omnius_Package_Block_Adminhtml_Package_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        // Building package types
        $packageTypes = [];
        $packageTypesCollection = Mage::getModel('package/packageType')
        ->getCollection()
        ->addFieldToFilter('is_visible', ['eq' => 1]);

        foreach ($packageTypesCollection as $packageType) {
            $packageTypes[$packageType->getId()] = $packageType->getPackageCode();
        }


        $form = new Varien_Data_Form(array(
                "id" => "edit_form",
                "action" => $this->getUrl("*/*/save", array("id" => $this->getRequest()->getParam("id"))),
                "method" => "post",
                "enctype" => "multipart/form-data",
            )
        );
        $form->setUseContainer(true);
        $this->setForm($form);

        $fieldSet = $form->addFieldset("general",
            array(
                "legend" => "Package configuration"
            )
        );

        $fieldSet->addField("package_code", "text",
            array(
                "name" => "package_code",
                "label" => "Package code",
                "input" => "text",
                "required" => true,
                'note' => "Mapped to <b>" . Omnius_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR . "</b> product attribute",
            )
        );

        $fieldSet->addField("type_package_id", "select",
            array(
                "name" => "type_package_id",
                "label" => "Package type",
                "input" => "text",
                "required" => true,
                "values" => $packageTypes,
                'note' => "The package type to which this item is related",
            )
        );

        $fieldSet->addField("front_end_name", "text",
            array(
                "name" => "front_end_name",
                "label" => "Frontend name",
                "input" => "text",
                "required" => true,
                'note' => "This is the name displayed in configurator and cart",
            )
        );

        $fieldSet->addField("front_end_position", "select",
            array(
                "name" => "front_end_position",
                "label" => "Frontend position",
                "input" => "text",
                "required" => true,
                "values" => [1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9],
                'note' => "Ordered in fronted by selected value",
            )
        );

        $fieldSet->addField("cardinality", "text",
            array(
                "name" => "cardinality",
                "label" => "Package cardinality",
                "input" => "text",
                "required" => true,
                'note' => "0...0 = none, 0...1 = none or just one, 1...1 = only one, 1...n = at least one, 0...n = doesn't matter",
            )
        );

        $fieldSet->addField("visible_configurator", "select",
            array(
                "name" => "visible_configurator",
                "label" => "Visible in configurator",
                "input" => "text",
                "required" => true,
                "values" => [0 => "No", 1 => "Yes"],
            )
        );

        $fieldSet->addField("visible_cart", "select",
            array(
                "name" => "visible_cart",
                "label" => "Visible in cart",
                "input" => "text",
                "required" => true,
                "values" => [0 => "No", 1 => "Yes"],
            )
        );

        $fieldSet->addField("is_visible", "select",
            array(
                "name" => "is_visible",
                "label" => "Visible in frontend",
                "input" => "text",
                "required" => true,
                "values" => [0 => "No", 1 => "Yes"],
            )
        );

        $configurationModel = Mage::registry("package_model_configuration");
        if ($configurationModel) {
            $form->addValues($configurationModel->getData());
        }

        return parent::_prepareForm();
    }
}
