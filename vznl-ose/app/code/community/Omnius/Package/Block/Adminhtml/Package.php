<?php

class Omnius_Package_Block_Adminhtml_Package extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = "adminhtml_package";
        $this->_blockGroup = "omnius_package";
        $this->_headerText = Mage::helper("omnius_package")->__("Packages Subtypes");
        $this->_addButtonLabel = Mage::helper("omnius_package")->__("Add New Subtype");
        parent::__construct();
    }
}
