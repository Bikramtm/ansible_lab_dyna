<?php

class Omnius_Package_Block_Adminhtml_PackageType extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = "adminhtml_packageType";
        $this->_blockGroup = "omnius_package";
        $this->_headerText = Mage::helper("omnius_package")->__("Packages Types");
        $this->_addButtonLabel = Mage::helper("omnius_package")->__("Add New Package type");
        parent::__construct();
    }
}
