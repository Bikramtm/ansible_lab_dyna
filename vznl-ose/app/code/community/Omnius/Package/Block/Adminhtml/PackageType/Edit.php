<?php

class Omnius_Package_Block_Adminhtml_PackageType_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        $this->_blockGroup = "omnius_package";
        $this->_controller = "adminhtml_packageType";
        $this->_headerText = Mage::helper("omnius_package")->__("Add package type");
        $this->_mode = "edit_tab";
        $this->_updateButton("save", "label", Mage::helper("catalog")->__("Save package type"));

        parent::__construct();
    }
}
