<?php

class Omnius_Package_Block_Adminhtml_Package_Type extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    /**
     * Renders grid column for package type visibility
     * @param   Varien_Object $row
     * @return  string
     */
    public function render(Varien_Object $row)
    {
        $packageTypeId = $row->getTypePackageId();

        $packageModel = Mage::getModel('package/packageType')
            ->load($packageTypeId);

        return $packageModel->getPackageCode() ?: "-";
    }
}
