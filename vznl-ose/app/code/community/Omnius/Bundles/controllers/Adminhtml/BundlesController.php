<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * class Omnius_Bundles_Adminhtml_BundlesController
 *
 */
class Omnius_Bundles_Adminhtml_BundlesController extends Mage_Adminhtml_Controller_Action
{
    /**
     * @return $this
     */
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('bundles/bundles')
            ->_addBreadcrumb(Mage::helper('bundles')->__('Manage Bundle Templates'),
                Mage::helper('bundles')->__('Manage Bundle Templates'));

        return $this;
    }

    /**
     * Display the admin grid (table) with all the announcements
     */
    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    /**
     * Gather data and generate edit form
     *
     */
    public function editAction()
    {
        $id = $this->getRequest()->getParam('entity_id');
        /** @var Omnius_Bundles_Model_Package $model */
        $model = Mage::getModel('bundles/bundle')->load($id);

        if ($model->getEntityId() || $id == 0) {
            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
            if (!empty($data)) {
                $model->setData($data);
            }
            Mage::register('bundles_data', $model);

            $this->loadLayout();
            $this->_setActiveMenu('bundles/bundles');

            $this->_addContent($this->getLayout()->createBlock('bundles/adminhtml_bundles_edit'))
                ->_addLeft($this->getLayout()->createBlock('bundles/adminhtml_bundles_edit_tabs'));

            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('bundles')->__('Item does not exist'));
            $this->_redirect('*/*/');
        }
    }

    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            try {
                list($model, $data, $deleteImage) = $this->_updateBundleData($data);
                if ($model->getId()) {
                    // When an existing item is changed, only update image if changes have been performed
                    if ($deleteImage) {
                        $model->deleteImage(null);
                    } elseif (isset($_FILES['image']['name']) && file_exists($_FILES['image']['tmp_name']) && $_FILES['image']['error'] == 0) {
                        // When a new image request is received
                        $model->uploadImage();
                    }
                } else {
                    // When a new item is provided, it always needs an image
                    if (!isset($_FILES['image']['name']) || !file_exists($_FILES['image']['tmp_name']) || $_FILES['image']['error'] != 0) {
                        throw new Exception(Mage::helper('bundles')->__('You must select an image for the template'));
                    }
                    $model->uploadImage();
                }
                $model->save();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('bundles')->__('The bundle template was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', ['entity_id' => $model->getEntityId()]);

                    return;
                }
            } catch (Exception $e) {
                $this->_throwErrror($e->getMessage(), $data);

                return;
            }
            $this->_redirect('*/*/');

            return;
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('bundles')->__('Unable to find item to save'));
        $this->_redirect('*/*/');
    }

    public function validateBundleAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            try {
                $this->_updateBundleData($data);
                $result['error'] = false;
            } catch (Exception $e) {
                $result['error'] = true;
                if (!empty($e->AllErrors)) {
                    $result['messages'] = $e->AllErrors;
                } else {
                    $result['messages'] = $e->getMessage();
                }
            }
            $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                Mage::helper('core')->jsonEncode($result)
            );
        }
    }

    /**
     * Patch for SUPEE-6285
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return true;
    }

    /**
     * @param $data
     * @return array
     * @throws Exception
     */
    protected function _updateBundleData($data)
    {
        $entityId = $this->getRequest()->getParam('entity_id');
        /** @var $model Omnius_Bundles_Model_Bundle */
        if ($entityId) {
            $model = Mage::getModel('bundles/bundle')->load($entityId);
        } else {
            $model = Mage::getModel('bundles/bundle');
        }
        $data['website_id'] = "|" . implode('|', $data['website_id']) . "|";
        $data['package_ids'] = !empty($data['package_ids']) ? $data['package_ids'] : [];
        $deleteImage = isset($data['image']['delete']) && $data['image']['delete'];
        unset($data['image']);
        $model->addData($data);
        if (($errors = $model->validateData()) !== true) {
            $ex = new Exception(implode(', ', $errors));
            $ex->AllErrors = $errors;
            throw $ex;
        }

        return array($model, $data, $deleteImage);
    }

    /**
     * Method that throws the error based on a message
     *
     * @param mixed $message
     */
    private function _throwErrror($message, $data)
    {
        Mage::getSingleton('adminhtml/session')->addError($message);
        Mage::getSingleton('adminhtml/session')->setFormData($data);
        $this->_redirect('*/*/edit', ['entity_id' => $this->getRequest()->getParam('entity_id')]);
    }
}
