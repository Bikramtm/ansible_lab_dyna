<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Bundles_Model_Bundle extends Mage_Core_Model_Abstract
{
    /** @var  Omnius_Bundles_Model_Resource_Package_Collection */
    protected $_packages;

    protected function _construct()
    {
        $this->_init('bundles/bundle');
    }

    /**
     * Return the list of website ids for the current bundle
     *
     * @return array
     */
    public function getWebsiteId()
    {
        return array_filter(explode('|', $this->getData('website_id')), function ($el) {
            return $el != "";
        });
    }

    /**
     * Set the list of websites for the current bundle
     * If string is used as parameter, values must be separated by "|", E.g. '|1|2|3|'
     *
     * @param array|string $websites
     */
    public function setWebsiteId($websites)
    {
        if (is_array($websites)) {
            $this->setData('website_id', "|" . implode('|', $websites) . "|");
        } else {
            $this->setData('website_id', $websites);
        }
    }

    /**
     * Perform cleanup before deleting
     *
     * @return Mage_Core_Model_Abstract
     */
    protected function _beforeDelete()
    {
        // Physically remove the image
        if (file_exists($this->getImageRealPath())) {
            unlink($this->getImageRealPath());
        }

        return parent::_beforeDelete();
    }

    /**
     * Return the image url path
     *
     * @return null|string
     */
    public function getImageUrlPath()
    {
        if (!$this->getImage()) {
            return null;
        }

        return Omnius_Bundles_Helper_Data::MEDIA_FOLDER_NAME . $this->getImage();
    }

    /**
     * Return the image file path
     *
     * @return string
     */
    public function getImageRealPath()
    {
        return Mage::helper('bundles')->getMediaFolder() . $this->getImage();
    }

    /**
     * Get Bundle packages collection based on package ids on this model
     *
     * @return Dyna_Bundles_Model_Resource_Package_Collection
     */
    public function getPackages()
    {
        if ($this->_packages === null) {
            $this->_packages = Mage::getResourceModel('bundles/package_collection')->addFieldToFilter('entity_id', ['in' => $this->getPackageIds()]);
        }

        return $this->_packages;
    }

    /**
     * Get the all package ids for the current bundle
     *
     * @return array
     */
    public function getPackageIds()
    {
        if ($this->getData('package_ids') === null) {
            $this->setData('package_ids', Mage::helper('bundles')->getBundlesPackagesLink($this->getId(), null));
        }

        return $this->getData('package_ids');
    }

    /**
     * When package ids are changed, also reset the package collection
     * 
     * @return mixed
     */
    public function setPackageIds()
    {
        $this->_packages = null;

        return parent::setPackageIds();
    }

    /**
     * Delete the old packages that are not in the current packages list and add the current packages that are not in old packages
     *
     * @return Mage_Core_Model_Abstract
     */
    public function _afterSave()
    {
        if ($this->hasDataChanges()) {
            // Parse the raw data of the packages and see if they changed
            $currentPackages = $this->getPackageIds();
            $oldPackages = Mage::helper('bundles')->getBundlesPackagesLink($this->getId(), null);
            $toDelete = [];
            foreach ($oldPackages as $oldPackage) {
                if (!in_array($oldPackage, $currentPackages)) {
                    $toDelete[] = $oldPackage;
                } else {
                    unset($currentPackages[array_search($oldPackage, $currentPackages)]);
                }
            }
            /** @var Varien_Db_Adapter_Interface $connection */
            $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
            $connection->beginTransaction();
            try {
                if ($toDelete) {
                    $connection->delete(Mage::getSingleton('core/resource')->getTableName('template_bundle_package_link'), sprintf('bundle_id = %s AND package_id IN (%s)', $this->getId(), implode(',', $toDelete)));
                }
                foreach ($currentPackages as $added) {
                    $connection->insert(Mage::getSingleton('core/resource')->getTableName('template_bundle_package_link'), ['bundle_id' => $this->getId(), 'package_id' => $added]);
                }
                $connection->commit();
            } catch (Exception $e) {
                $connection->rollBack();
                Mage::getSingleton('core/logger')->logException($e);
            }
        }

        return parent::_afterSave();
    }

    /**
     * Delete bundle image
     *
     * @return $this
     */
    public function deleteImage()
    {
        // When a delete request is received also remove the old unused file
        if (file_exists($this->getImageRealPath())) {
            unlink($this->getImageRealPath());
        }
        $this->setImage(null);

        return $this;
    }

    /**
     * Upload bundle image
     *
     * @throws Exception
     */
    public function uploadImage()
    {
        try {
            $uploader = new Varien_File_Uploader('image');
            $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png'])
                ->setAllowRenameFiles(true)
                ->setFilesDispersion(true)
                ->save(Mage::helper('bundles')->getMediaFolder());
            // Also delete the old image since it's not used
            if (file_exists($this->getImageRealPath())) {
                unlink($this->getImageRealPath());
            }
            $this->setImage($uploader->getUploadedFileName());
        } catch (Exception $e) {
            throw new Exception(Mage::helper('bundles')->__('Unable to save the image'));
        }
    }

    /**
     * Validates the data for the model
     *
     * Returns true when no errors, or an array with the errors found
     *
     * @return bool|array
     */
    public function validateData()
    {
        $errors = [];
        $websitesIds = $this->getWebsiteId();
        /** @var Omnius_Bundles_Model_Package $package */
        foreach ($this->getPackages() as $package) {
            if ($errorWebsites = array_diff($websitesIds, $package->getWebsiteId())) {
                $websiteCodes = [];
                $websitesIds = $this->getWebsiteId();
                foreach ($errorWebsites as $errorWebsite) {
                    $websiteCodes[] = Mage::app()->getWebsite($errorWebsite)->getName();
                }
                $errors[] = sprintf(Mage::helper('bundles')->__('Bundle contains package %s that is not assigned to website(s): %s. Package must also part of these websites, or remove the package from the bundle before editing.'), $package->getName(), implode(',', $websiteCodes));
            }
        }

        if (Mage::helper('bundles')->hasDuplicates($this->getPackageIds())) {
            $errors[] = Mage::helper('bundles')->__('A package can only be present once within a bundle');
        }
        if (!count($this->getPackageIds())) {
            $errors[] = Mage::helper('bundles')->__('A bundle must have atleast one package');
        }

        return $errors ?: true;
    }

    /**
     * @return Omnius_Bundles_Model_Bundle
     */
    protected function _beforeSave()
    {
        if ($this->getData('created_at') === null) {
            $this->setData('created_at', now());
        } else {
            $this->setData('updated_at', now());
        }

        return parent::_beforeSave();
    }
}
