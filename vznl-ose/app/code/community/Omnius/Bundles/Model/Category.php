<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Bundles_Model_Category extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('bundles/category');
    }

    /**
     * @return Omnius_Bundles_Model_Package
     */
    protected function _beforeSave()
    {
        if ($this->getData('created_at') === null) {
            $this->setData('created_at', now());
        } else {
            $this->setData('updated_at', now());
        }

        return parent::_beforeSave();
    }
}
