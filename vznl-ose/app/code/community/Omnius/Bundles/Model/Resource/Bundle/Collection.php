<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Bundles_Model_Resource_Bundle_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{

    protected function _construct()
    {
        $this->_init('bundles/bundle');
    }
}
