<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $this Mage_Core_Model_Resource_Setup */

$this->startSetup();
$setup = Mage::getSingleton('core/resource')->getConnection('core_read');

$oldIdxFound = false;
foreach ($setup->fetchAll('SHOW INDEX FROM template_package') as $indexes) {
    if ($indexes['Key_name'] == 'idx_website_id') {
        $oldIdxFound = true;
    }
}
if (!$oldIdxFound) {
    $this->run("ALTER TABLE `template_package` ADD INDEX `idx_website_id` (`website_id`)");
}

$oldIdxFound = false;
foreach ($setup->fetchAll('SHOW INDEX FROM template_bundle') as $indexes) {
    if ($indexes['Key_name'] == 'idx_website_id') {
        $oldIdxFound = true;
    }
}
if (!$oldIdxFound) {
    $this->run("ALTER TABLE `template_bundle` ADD INDEX `idx_website_id` (`website_id`)");
}
$this->endSetup();
