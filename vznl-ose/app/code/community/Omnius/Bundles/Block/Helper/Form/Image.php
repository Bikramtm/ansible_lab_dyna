<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Bundles_Block_Helper_Form_Image extends Varien_Data_Form_Element_Image
{
    /**
     * Overwrite method so there is no delete button
     *
     * @return string
     */
    protected function _getDeleteCheckbox()
    {
        return '';
    }
}
