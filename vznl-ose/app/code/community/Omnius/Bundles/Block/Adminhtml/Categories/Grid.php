<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Admin list Customer images grid
 */
class Omnius_Bundles_Block_Adminhtml_Categories_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('categoriesGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    /**
     * Prepare grid collection object
     *
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('bundles/category')->getCollection();
        $this->setCollection($collection);
        parent::_prepareCollection();

        return $this;
    }

    /**
     * Create the admin grid
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */

    protected function _prepareColumns()
    {
        parent::_prepareColumns();

        if (!$this->_isExport) {
            $this->addColumn('entity_id', array(
                'header' => $this->_getColumnTitle('ID'),
                'align' => 'left',
                'width' => '30px',
                'index' => 'entity_id',
            ));
        }
        $this->addColumn('name', array(
            'header' => $this->_getColumnTitle('Name'),
            'align' => 'left',
            'index' => 'name',
        ));
        $this->addColumn('identifier', array(
            'header' => $this->_getColumnTitle('Identifier'),
            'align' => 'left',
            'index' => 'identifier',
        ));

        return $this;
    }

    /**
     * @param $text
     * @return mixed|string
     */
    protected function _getColumnTitle($text)
    {
        if ($this->_isExport) {
            return str_replace(' ', '_', strtolower($text));
        }

        return Mage::helper('bundles')->__($text);
    }

    /**
     * @param $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('entity_id' => $row->getEntityId()));
    }
}
