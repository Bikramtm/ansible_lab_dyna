<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Bundles_Block_Adminhtml_Packages_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('packages_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('bundles')->__('Item Information'));
    }

    /**
     * Before rendering html, but after trying to load cache
     *
     * @return Mage_Core_Block_Abstract
     * @throws Exception
     */
    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
            'label' => Mage::helper('bundles')->__('Item Information'),
            'title' => Mage::helper('bundles')->__('Item Information'),
            'content' => $this->getLayout()->createBlock('bundles/adminhtml_packages_edit_tab_form')->toHtml(),
        ));

        return parent::_beforeToHtml();
    }
}
