<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Bundles_Block_Adminhtml_Bundles_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();

        $this->_objectId = 'entity_id';
        $this->_blockGroup = 'bundles';
        $this->_controller = 'adminhtml_bundles';

        $this->_updateButton('save', 'label', Mage::helper('bundles')->__('Save bundle'));
        $this->_updateButton('save', 'onclick', 'submitBundle()');

        $this->_addButton('saveandcontinue', array(
            'label' => Mage::helper('bundles')->__('Save And Continue Edit'),
            'onclick' => 'submitBundle(true)',
            'class' => 'save',
        ), -100);

        $objId = $this->getRequest()->getParam($this->_objectId);

        if (!empty($objId)) {
            $this->_addButton('delete', array(
                'label' => Mage::helper('bundles')->__('Delete'),
                'class' => 'delete',
                'onclick' => 'deleteConfirm(\''
                    . Mage::helper('core')->jsQuoteEscape(
                        Mage::helper('bundles')->__('Are you sure you want to do this?')
                    )
                    . '\', \''
                    . $this->getDeleteUrl()
                    . '\')',
            ));
        }

        $this->_formScripts[] = "
            function saveAndContinueEdit() {
                editForm.submit($('edit_form').action + 'back/edit/');
            }
        ";
    }

    /**
     * @return string
     */
    public function getHeaderText()
    {
        if (Mage::registry('bundles_data') && Mage::registry('bundles_data')->getEntityId()) {
            return Mage::helper('bundles')->__("Edit bundle template with id '%s'", $this->escapeHtml(Mage::registry('bundles_data')->getEntityId()));
        } else {
            return Mage::helper('bundles')->__('Add new bundle template');
        }
    }
}
