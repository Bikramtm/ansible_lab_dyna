<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Bundles_Block_Adminhtml_CategoryRenderer extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    private $_isExport = false;

    /**
     * @param Varien_Object $row
     * @return string
     */
    public function render(Varien_Object $row)
    {
        $categoryNames = "";

        $categories = $row->getCategories();
        foreach ($categories as $item) {
            // When exporting, export the website id, not website name
            if ($this->_isExport) {
                $categoryNames = $categoryNames ? ($categoryNames . "," . $item->getName()) : $item->getName();
            } else {
                $categoryNames = $categoryNames ? ($categoryNames . ",<br>" . $item->getName()) : $item->getName();
            }
        }

        return $categoryNames;
    }

    /**
     * Render column for export
     *
     * @param Varien_Object $row
     * @return string
     */
    public function renderExport(Varien_Object $row)
    {
        $this->_isExport = true;

        return $this->render($row);
    }
}
