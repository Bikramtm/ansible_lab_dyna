<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Admin list Customer images grid
 */
class Omnius_Bundles_Block_Adminhtml_Packages_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    protected $multiselectFields = ['website_id'];

    public function __construct()
    {
        parent::__construct();
        $this->setId('packagesGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    /**
     * Prepare grid collection object
     *
     * @return $this
     */
    protected function _prepareCollection()
    {
        /** @var Omnius_Bundles_Model_Resource_Category_Collection $collection */
        $collection = Mage::getModel('bundles/package')->getCollection();
        $collection->getSelect()->joinLeft(["t1" => 'template_package_category_link'], "main_table.entity_id = t1.package_id", ['category' => 't1.category_id']);
        $collection->getSelect()->group('main_table.entity_id');
        $collection->addFilterToMap('category', 't1.category_id');
        $this->setCollection($collection);
        parent::_prepareCollection();

        return $this;
    }

    /**
     * Create the admin grid
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */

    protected function _prepareColumns()
    {
        parent::_prepareColumns();

        if (!$this->_isExport) {
            $this->addColumn('entity_id', array(
                'header' => $this->_getColumnTitle('ID'),
                'align' => 'left',
                'width' => '30px',
                'index' => 'entity_id',
            ));
        }

        $this->addColumn('name', array(
            'header' => $this->_getColumnTitle('Name'),
            'align' => 'left',
            'index' => 'name',
        ));

        $this->addColumn('type', array(
            'header' => $this->_getColumnTitle('Package type'),
            'align' => 'left',
            'index' => 'type',
            'type' => 'options',
            'options' => Mage::helper('bundles/packages')->getTypesForDropdown(),
        ));

        $this->addColumn('category', array(
            'header' => $this->_getColumnTitle('Package category'),
            'align' => 'left',
            'type' => 'options',
            'index' => 'category',
            'options' => Mage::helper('bundles/packages')->getCategoriesForDropdown(),
            'renderer' => 'Omnius_Bundles_Block_Adminhtml_CategoryRenderer',
            'sortable' => false,
        ));

        $this->addColumn('website_id', array(
            'header' => $this->_getColumnTitle('Website'),
            'index' => 'website_id',
            'type' => 'options',
            'options' => Mage::getSingleton('adminhtml/system_store')->getWebsiteOptionHash(false),
            'renderer' => 'Omnius_Bundles_Block_Adminhtml_WebsiteRenderer',
            'sortable' => false,
        ));

        $this->addColumn('locked', array(
            'header' => $this->_getColumnTitle('Locked'),
            'align' => 'left',
            'index' => 'locked',
            'type' => 'options',
            'options' => Mage::helper('bundles')->getLockedForDropdown(),
        ));

        return $this;
    }

    /**
     * @param $text
     * @return mixed|string
     */
    protected function _getColumnTitle($text)
    {
        if ($this->_isExport) {
            return str_replace(' ', '_', strtolower($text));
        }

        return Mage::helper('bundles')->__($text);
    }

    /**
     * @param $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('entity_id' => $row->getEntityId()));
    }

    /**
     * @param $column
     * @return $this
     */
    protected function _addColumnFilterToCollection($column)
    {
        if ($this->getCollection()) {
            $field = ($column->getFilterIndex()) ? $column->getFilterIndex() : $column->getIndex();
            if ($column->getFilterConditionCallback()) {
                call_user_func($column->getFilterConditionCallback(), $this->getCollection(), $column);
            } else {
                $cond = $column->getFilter()->getCondition();
                if ($field && isset($cond)) {
                    if (in_array($field, $this->multiselectFields)) {
                        $cond = ['like' => "%|" . $cond['eq'] . "|%"];
                    }
                    $this->getCollection()->addFieldToFilter($field, $cond);
                }
            }
        }

        return $this;
    }
}
