<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Bundles_Block_Adminhtml_Categories_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        /** @var Omnius_Bundles_Model_Category $data */
        $data = Mage::registry('categories_data');
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $dataFieldset = $form->addFieldset('bundles', array('legend' => Mage::helper('bundles')->__('Category details')));

        $dataFieldset->addField('name', 'text', array(
            'label' => Mage::helper('bundles')->__('Name'),
            'name' => 'name',
            "class" => "required-entry",
            "required" => true,
            'value' => $data->getName(),
        ));
        $dataFieldset->addField('identifier', 'text', array(
            'label' => Mage::helper('bundles')->__('Identifier'),
            'name' => 'identifier',
            "class" => "required-entry",
            "required" => true,
            'value' => $data->getIdentifier(),
        ));

        return parent::_prepareForm();
    }

}
