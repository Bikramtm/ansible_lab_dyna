<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Bundles_Block_Adminhtml_Packages_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();

        $this->_objectId = 'entity_id';
        $this->_blockGroup = 'bundles';
        $this->_controller = 'adminhtml_packages';

        $this->_updateButton('save', 'label', Mage::helper('bundles')->__('Save package'));
        $this->_updateButton('save', 'onclick', 'submitPackage()');

        $this->_addButton('saveandcontinue', array(
            'label' => Mage::helper('bundles')->__('Save And Continue Edit'),
            'onclick' => 'submitPackage(true)',
            'class' => 'save',
        ), -100);

        $objId = $this->getRequest()->getParam($this->_objectId);
        if (!empty($objId)) {
            $this->_addButton('delete', array(
                'label' => Mage::helper('bundles')->__('Delete'),
                'class' => 'delete',
                'onclick' => 'deletePackage(\''
                    . $this->getDeleteUrl()
                    . '\')',
            ));
        }

        $this->_formScripts[] = "
            function saveAndContinueEdit() {
                editForm.submit($('edit_form').action + 'back/edit/');
            }
        ";
    }

    /**
     * @return string
     */
    public function getHeaderText()
    {
        if (Mage::registry('packages_data') && Mage::registry('packages_data')->getEntityId()) {
            return Mage::helper('bundles')->__("Edit package template with id '%s'", $this->escapeHtml(Mage::registry('packages_data')->getEntityId()));
        } else {
            return Mage::helper('bundles')->__('Add new package template');
        }
    }
}
