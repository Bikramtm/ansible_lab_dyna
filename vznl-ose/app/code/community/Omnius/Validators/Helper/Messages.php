<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Validators_Helper_Messages
 */
class Omnius_Validators_Helper_Messages extends Mage_Core_Helper_Data
{
    const INVALID_POSTCODE = 'Invalid postcode';
    const INVALID_FUTURE_DATE = 'The date entered is a date in the past.';
    const INVALID_DATE_FORMAT = 'Please enter a valid date using the following date format: dd-mm-yyyy.';
    const INVALID_LEGITIMATION_NUMBER = 'Invalid document number.';

    /**
     * @param $key
     * @return string
     */
    public function getMessage($key)
    {
        switch($key) {
            case 'nl-postcode':
                $message = self::INVALID_POSTCODE;
                break;
            case 'validate-date-age':
                case 'validate-date-nl':
                    $message = self::INVALID_DATE_FORMAT;
                break;
            case 'validate-date-future':
                $message = self::INVALID_FUTURE_DATE;
                break;
            case 'validate-legitimation-number-0':
            case 'validate-legitimation-number-1-4':
            case 'validate-legitimation-number-p':
            case 'validate-legitimation-number-p-nl':
            case 'validate-legitimation-number-r':
            case 'validate-legitimation-number-n':
                $message = self::INVALID_LEGITIMATION_NUMBER;
                break;
            case 'validate-np-dot-contract':
            case 'validate-np-contract':
                $message = 'Please enter a valid contract number.';
                break;
            default:
                $message = '';
                break;
        }

        return Mage::helper('omnius_checkout')->__($message);
    }
}
