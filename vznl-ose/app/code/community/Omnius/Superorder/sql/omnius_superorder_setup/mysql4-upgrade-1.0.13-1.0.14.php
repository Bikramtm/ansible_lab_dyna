<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

// Add a column that shows if the approveOrder (packages) call has been executed for the superOrder
$installer = $this;
$installer->startSetup();
$this->getConnection()->addColumn($this->getTable('superorder'), 'approve_order', Varien_Db_Ddl_Table::TYPE_BOOLEAN);
$installer->endSetup();
