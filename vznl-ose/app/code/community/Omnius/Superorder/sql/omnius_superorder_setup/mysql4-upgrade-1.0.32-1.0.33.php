<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$this->startSetup();
$connection = $this->getConnection();
$connection->addColumn($this->getTable('superorder/statusHistory'), 'agent_id',
    array(
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'comment'   => 'Agent id that cancelled order',
        'required'  => false,
        'after'     => 'type',
        'length'    => 100,
        'nullable'  => true
    )
);
$this->endSetup();