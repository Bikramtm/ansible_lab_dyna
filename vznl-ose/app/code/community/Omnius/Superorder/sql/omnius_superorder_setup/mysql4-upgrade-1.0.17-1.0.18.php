<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

// Drop approveOrder (packages) column from the superOrder and move it to packages
$installer = $this;
$installer->startSetup();
$this->getConnection()->dropColumn($this->getTable('superorder'), 'approve_order');
$installer->endSetup();
