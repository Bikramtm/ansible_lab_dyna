<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;
$installer->startSetup();
$this->getConnection()->addColumn($this->getTable('superorder'), 'order_number', 'varchar(100)');
$this->getConnection()->addColumn($this->getTable('superorder'), 'created_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP);
$this->getConnection()->addColumn($this->getTable('superorder'), 'customer_id', Varien_Db_Ddl_Table::TYPE_INTEGER);
$this->getConnection()->addForeignKey(
    'fk_superorder_customer_id',
    'superorder',
    'customer_id',
    'customer_entity',
    'entity_id',
    Varien_Db_Ddl_Table::ACTION_CASCADE,
    Varien_Db_Ddl_Table::ACTION_CASCADE
);
$installer->endSetup();