<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Add dealer_code column to the superorder_change_history table
 */
/** @var Mage_Core_Model_Resource_Setup $this */
$installer = $this;

$installer->startSetup();
$installer->getConnection()->addColumn($this->getTable('superorder_change_history'), 'dealer_code', 'VARCHAR(50)');
$installer->endSetup();