<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * @category    Omnius
 * @package     Omnius_Superorder
 */

/* @var $salesInstaller Mage_Sales_Model_Resource_Setup */
$installer = new Mage_Sales_Model_Resource_Setup('core_setup');
$installer->startSetup();
if (!$installer->tableExists('superorder')) {
    $table = new Varien_Db_Ddl_Table();
    $table->setName('superorder');

    /**
     * Create table
     */
    $table->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity' => true,
        'unsigned' => true,
        'nullable' => false,
        'primary' => true,
    ), 'ID')
        ->addColumn('order_status', Varien_Db_Ddl_Table::TYPE_TEXT, 64, array(), 'Super Order Status')
        ->addColumn('website_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, 5, array(
            'unsigned' => true,
            'nullable' => false,
        ), 'Store')
        ->setComment('Super Orders Table');

    $installer->getConnection()->createTable($table);
}
$installer->endSetup();
