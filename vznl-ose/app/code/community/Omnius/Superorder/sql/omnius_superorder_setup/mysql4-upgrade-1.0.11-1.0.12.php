<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Core_Model_Resource_Setup $this */
$installer = $this;
$installer->startSetup();
if (!$installer->getConnection()->isTableExists($installer->getTable('superorder_history'))) {
    /** Superorder History table */
    $superorderHistoryTable = new Varien_Db_Ddl_Table();
    $superorderHistoryTable->setName('superorder_history');

    $superorderHistoryTable->addColumn(
        'entity_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        10,
        array('unsigned' => true, 'primary' => true, 'auto_increment' => true)
    );

    $superorderHistoryTable->addColumn(
        'superorder_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        10,
        array('unsigned' => true)
    );

    $superorderHistoryTable->addColumn(
        'package_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        10
    );

    $superorderHistoryTable->addColumn(
        'status',
        Varien_Db_Ddl_Table::TYPE_VARCHAR,
        30
    );

    $superorderHistoryTable->addColumn(
        'data',
        Varien_Db_Ddl_Table::TYPE_TEXT
    );

    $superorderHistoryTable->addForeignKey(
        'fk_superorder_id',
        'superorder_id',
        Mage::getSingleton('core/resource')->getTableName('superorder'),
        'entity_id',
        $superorderHistoryTable::ACTION_CASCADE,
        $superorderHistoryTable::ACTION_CASCADE
    );

    $superorderHistoryTable->setOption('type', 'InnoDB');
    $superorderHistoryTable->setOption('charset', 'utf8');
    $this->getConnection()->createTable($superorderHistoryTable);
}

if (!$installer->getConnection()->isTableExists($installer->getTable('superorder_history_item'))) {
    /** Superorder History Item table */
    $superorderHistoryItemTable = new Varien_Db_Ddl_Table();
    $superorderHistoryItemTable->setName('superorder_history_item');

    $superorderHistoryItemTable->addColumn(
        'entity_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        10,
        array('unsigned' => true, 'primary' => true, 'auto_increment' => true)
    );

    $superorderHistoryItemTable->addColumn(
        'superorder_history_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        10,
        array('unsigned' => true)
    );

    $superorderHistoryItemTable->addColumn(
        'line_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        10,
        array('unsigned' => true)
    );

    $superorderHistoryItemTable->addColumn(
        'product_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        10,
        array('unsigned' => true)
    );

    $superorderHistoryItemTable->addColumn(
        'status',
        Varien_Db_Ddl_Table::TYPE_VARCHAR,
        30
    );

    $superorderHistoryItemTable->addColumn(
        'data',
        Varien_Db_Ddl_Table::TYPE_TEXT
    );

    $superorderHistoryItemTable->addColumn(
        'removed',
        Varien_Db_Ddl_Table::TYPE_BOOLEAN,
        array('default' => 0, 'nullable' => false)
    );

    $superorderHistoryItemTable->addForeignKey(
        'fk_superorder_history_id',
        'superorder_history_id',
        Mage::getSingleton('core/resource')->getTableName('superorder_history'),
        'entity_id',
        $superorderHistoryItemTable::ACTION_CASCADE,
        $superorderHistoryItemTable::ACTION_CASCADE
    );

    $superorderHistoryItemTable->setOption('type', 'InnoDB');
    $superorderHistoryItemTable->setOption('charset', 'utf8');
    $this->getConnection()->createTable($superorderHistoryItemTable);
}
$installer->endSetup();