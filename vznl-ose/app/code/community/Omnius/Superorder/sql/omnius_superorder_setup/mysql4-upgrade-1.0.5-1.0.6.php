<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

//Increase password field length
$installer->getConnection()->changeColumn($installer->getTable('superorder'), 'created_at', 'created_at', array(
    'type'      => Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
    'nullable'  => true,
    'default'   => null
));

$installer->endSetup();