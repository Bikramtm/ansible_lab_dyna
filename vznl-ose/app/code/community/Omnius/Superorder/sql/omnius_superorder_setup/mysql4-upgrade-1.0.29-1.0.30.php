<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Core_Model_Resource_Setup $this */
$this->startSetup();
$this->getConnection()->addIndex($this->getTable('status_history'), 'IDX_SUPERORDER_ID', 'superorder_id');
$this->getConnection()->addIndex($this->getTable('status_history'), 'IDX_PACKAGE_ID', 'package_id');
$this->endSetup();