<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $this Mage_Core_Model_Resource_Setup */
$this->startSetup();
$this->getConnection()->modifyColumn($this->getTable('superorder_history_item'), 'product_id', [
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'length' => 64,
    'nullable' => true,
    'required' => false
]);
$this->endSetup();
