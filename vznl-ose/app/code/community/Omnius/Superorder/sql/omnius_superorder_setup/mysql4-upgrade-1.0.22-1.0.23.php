<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Add history updates fields
 */
/** @var Mage_Core_Model_Resource_Setup $this */
$installer = $this;
$installer->startSetup();
$this->getConnection()->addColumn($this->getTable('superorder'), 'created_website_id', array(
    'type' => Varien_Db_Ddl_Table::TYPE_SMALLINT,
    'size' => 5,
    'after' => 'order_status',
    'nullable' => false,
    'unsigned' => true,
    'comment' => 'Creation store'
));
$this->getConnection()->addColumn($this->getTable('superorder'), 'created_agent_id', array(
    'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
    'after' => 'parent_id',
    'nullable' => false,
    'unsigned' => true,
    'comment' => 'Agent that created the order'
));
$this->getConnection()->addColumn($this->getTable('superorder'), 'created_dealer_id', array(
    'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
    'after' => 'created_agent_id',
    'nullable' => false,
    'unsigned' => true,
    'comment' => 'Dealer that created the order'
));
$this->getConnection()->addColumn($this->getTable('superorder'), 'updated_at', array(
    'type' => Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
    'after' => 'created_at',
    'nullable'  => true,
    'default'   => null,
    'comment'   => 'Update time'
));
$installer->endSetup();
