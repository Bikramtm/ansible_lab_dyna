<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $this Mage_Customer_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();
if (!$installer->getConnection()->isTableExists($installer->getTable('superorder_errors'))) {
    /** Superorder History table */
    $superorderErrorsTable = new Varien_Db_Ddl_Table();
    $superorderErrorsTable->setName('superorder_errors');

    $superorderErrorsTable->addColumn(
        'entity_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        10,
        array('unsigned' => true, 'primary' => true, 'auto_increment' => true)
    );

    $superorderErrorsTable->addColumn(
        'superorder_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        10,
        array('unsigned' => true)
    );

    $superorderErrorsTable->addColumn(
        'error_code',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        11
    );

    $superorderErrorsTable->addColumn(
        'error_detail',
        Varien_Db_Ddl_Table::TYPE_TEXT
    );

    $superorderErrorsTable->addForeignKey(
        'fk_errors_superorder_id',
        'superorder_id',
        Mage::getSingleton('core/resource')->getTableName('superorder'),
        'entity_id',
        $superorderErrorsTable::ACTION_CASCADE,
        $superorderErrorsTable::ACTION_CASCADE
    );

    $superorderErrorsTable->setOption('type', 'InnoDB');
    $superorderErrorsTable->setOption('charset', 'utf8');

    $this->getConnection()->createTable($superorderErrorsTable);
}
$installer->endSetup();