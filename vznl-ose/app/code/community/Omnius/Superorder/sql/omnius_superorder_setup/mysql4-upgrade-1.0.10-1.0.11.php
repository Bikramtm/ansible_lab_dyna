<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $customerInstaller Mage_Customer_Model_Resource_Setup */
$installer = new Mage_Customer_Model_Resource_Setup('core_setup');
$installer->startSetup();
$installer->getConnection()->addColumn($installer->getTable('superorder'), 'xml_to_be_sent', [
    'type' => Varien_Db_Ddl_Table::TYPE_BLOB,
    'nullable' => true,
    'required' => false,
    'comment' => "XML to be sent",
]);

$installer->endSetup();
