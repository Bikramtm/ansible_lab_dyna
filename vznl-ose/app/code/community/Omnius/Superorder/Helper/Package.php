<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Superorder_Helper_Package
 */
class Omnius_Superorder_Helper_Package extends Mage_Core_Helper_Data
{
    // Duration for which the data is cached 
    const CACHE_DURATION = 120;
    /** @var Dyna_Cache_Model_Cache */
    protected $_cache;

    /**
     * If a package of a given superorder is available it returns the content to render it for the order edit
     * @param $superOrder
     * @param $packageModel
     * @return mixed
     */
    public function getPackageAvailability($superOrder, $packageModel)
    {
        $orders = $superOrder->getOrders();
        $deliveryInThisStore = array();
        $session = Mage::getSingleton('customer/session');
        $agent = $session->getAgent(true);
        $packageItems = [];
        /** @var Omnius_Checkout_Model_Sales_Order $deliveryOrder */
        foreach ($orders as $deliveryOrder) {
            $packageIds = $deliveryOrder->getPackageIds();
            // Check if the order has delivery in this retail store
            if ($deliveryOrder->isStoreDelivery()
                && $deliveryOrder->getShippingAddress()
                && $deliveryOrder->getShippingAddress()->getDeliveryStoreId() == $agent->getDealer()->getId()
            ) {
                $deliveryInThisStore = array_merge($deliveryInThisStore, $packageIds);
            }
            // If the package belongs to this delivery order, also get the items 
            if (in_array($packageModel->getPackageId(), $packageIds)) {
                foreach ($deliveryOrder->getAllItems() as $item) {
                    if ($item->getPackageId() == $packageModel->getPackageId()) {
                        $packageItems[] = $item;
                    }
                }
            }
        }
        $html = Mage::getSingleton('core/layout')
            ->createBlock('core/template')
            ->setPackage($packageModel)
            ->setItems($packageItems)
            ->setTemplate('checkout/cart/steps/edit_order_package.phtml')
            ->setModified(false)
            ->setModifiedPackageIds([])
            ->setEditedPackageStatus(null)
            ->setCancelledPackageIds([])
            ->setSuperOrder($superOrder)
            ->setIsDeliveredEdited($packageModel->getNewPackageId())
            ->setDeliveryInThisStore(in_array($packageModel->getPackageId(), $deliveryInThisStore))
            ->setOnlyShowButtons(true)
            ->toHtml();

        return $html;
    }

    /**
     * Returns the cache singleton instance
     * @return Dyna_Cache_Model_Cache
     */
    protected function getCache()
    {
        if (!$this->_cache) {
            $this->_cache = Mage::getSingleton('dyna_cache/cache');
        }

        return $this->_cache;
    }
}
