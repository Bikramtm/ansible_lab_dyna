<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Superorder_Helper_History
 */
class Omnius_Superorder_Helper_History extends Mage_Core_Helper_Data
{
    /**
     * Saves superorder history based to determine the package/items status at the moment the changes were made
     * @param $id
     * @param $superOrderId
     * @param null $superOrderParentId
     * @return void
     */
    public function saveSuperorderHistory($id, $superOrderId, $superOrderParentId = null, $newOrder = false)
    {
        /** @var Dyna_Job_Model_Queue $queue */
        $queue = Mage::helper('job')->getQueue();
        $job = $queue->get($id);

        if ($job) {
            $write = Mage::getSingleton('core/resource')->getConnection('core_write');
            $table = Mage::getSingleton('core/resource')->getTableName('superorder_history');

            if ($newOrder) {
                $query = "SELECT entity_id FROM `$table` WHERE superorder_id = :superorder_id LIMIT 1;";
                $found = $write->fetchAssoc($query, array('superorder_id' => $superOrderId));
                if (count($found) > 0 ) {
                    // Order history already saved for this superOrder
                    return;
                }
            }

            $request = $job->getData('request');
            $request = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$3", $request);
            $xml = new SimpleXMLElement($request);
            $normalizer = Mage::getSingleton('omnius_service/normalizer');
            $data = $normalizer->normalizeKeys(
                $normalizer->objectToArray($xml)
            );

            $accessor = Mage::getModel('omnius_service/dotAccessor');
            if ($accessor->getValue($data, 'packages.package.state')) {
                $data['packages']['package'] = array($data['packages']['package']);
            }

            $itemTable = Mage::getSingleton('core/resource')->getTableName('superorder_history_item');

            foreach ($accessor->getValue($data, 'packages.package', array()) as $package) {
                $packageLines = $accessor->getValue($package, 'package_lines.package_line', array());
                if ($accessor->getValue($packageLines, 'state')) {
                    $packageLines = array($packageLines);
                }
                unset($package['package_lines']);
                unset($package['telecom_lines']);
                // save packages to superorder history
                $packageId = $accessor->getValue($package, 'package_i_d');
                $returnPackageId = $accessor->getValue($package, 'return_package_i_d');
                $status = $accessor->getValue($package, 'state');
                $data = serialize($package);
                $row = array(
                    'superorder_id' => $superOrderId,
                    'package_id' => $packageId,
                    'status' => $status,
                    'data' => $data
                );

                $write->insert($table,$row);

                // save package lines to superorder history item
                $superOrderHistoryId = $write->lastInsertId();
                $lines = array();
                foreach ($packageLines as $packageLine) {
                    $productId = $accessor->getValue($packageLine, 'product_i_d');
                    $status = $accessor->getValue($packageLine, 'state');
                    $lineId = $accessor->getValue($packageLine, 'order_line_i_d');

                    // Save to history only new items
                    if ($status == 'Reserve' || ($status == 'Return' && $superOrderParentId)) {

                        $data = serialize($packageLine);

                        $lines[] = array(
                            'superorder_history_id' => $superOrderHistoryId,
                            'line_id' => $lineId,
                            'product_id' => $productId,
                            'status' => $status,
                            'data' => $data,
                            'removed' => ($status == 'Return') ? 1 : 0
                        );

                    } elseif ($status == 'Cancel') {
                        $this->handleCancel($superOrderId, $write, $packageId, $productId, $lineId);
                    } elseif ($status == 'Nothing' || $status == 'Change') {
                        $this->handleNothing($superOrderId, $write, $packageId, $productId, $lineId, $status);
                    } elseif ($status == 'Return') {
                        $this->handleReturn($superOrderId, $superOrderParentId, $write, $returnPackageId, $productId);
                    }
                }

                if (!empty($lines)) {
                    $write->insertMultiple($itemTable, $lines);
                }
            }
        }
    }

    /**
     * Returns a list of history items for a superorder to determine what changes were made on that superorder
     * @param $superOrderId
     * @param $historyPackageId
     * @return array
     */
    public function getHistoryItems($superOrderId, $historyPackageId)
    {
        $read = Mage::getSingleton('core/resource')->getConnection('core_read');

        return $read->fetchAll("SELECT sh.package_id, shi.data, shi.line_id, shi.removed, shi.product_id FROM superorder_history sh INNER JOIN superorder_history_item shi ON sh.entity_id = shi.superorder_history_id WHERE sh.superorder_id = :id AND sh.package_id = :package_id ORDER BY shi.line_id ASC",
            array(
                'id' => $superOrderId,
                'package_id' => $historyPackageId,
            )
        );
    }

    /**
     * Returns all history items for a give superorder id
     * @param $superOrderId
     * @return array
     */
    public function getHistoryItemsCollection($superOrderId)
    {
        return Mage::getResourceModel('superorder/history_collection')->addFieldToFilter('superorder_id', (int) $superOrderId)->getFirstItem();
    }

    /**
     * Returns a list of deleted packages of a given superorder
     * @param $superOrderId
     * @return array
     */
    public function getDeletedPackages($superOrderId)
    {
        $read = Mage::getSingleton('core/resource')->getConnection('core_read');

        $dbResults = $read->fetchAll("SELECT sh.* FROM superorder_history sh WHERE sh.superorder_id = :id ORDER BY  sh.entity_id DESC, sh.package_id ASC",
            array(
                'id' => $superOrderId,
            )
        );

        $results = array();
        foreach ($dbResults as $item) {
            if ( ! isset($results[$item['package_id']]) ) {
                $results[$item['package_id']] = $item;
            }
        }
        return $results;

    }

    /**
     * Updates history items for Cancel status
     * @param $superOrderId
     * @param $write
     * @param $packageId
     * @param $productId
     * @param $lineId
     */
    protected function handleCancel($superOrderId, $write, $packageId, $productId, $lineId)
    {
        $historyId = $write->fetchOne(
            "SELECT shi.entity_id FROM superorder_history sh INNER JOIN superorder_history_item shi ON sh.entity_id = shi.superorder_history_id WHERE sh.superorder_id = :superorder_id AND sh.package_id = :package_id AND shi.product_id = :product_id AND shi.line_id = :line_id LIMIT 1",
            array(
                'superorder_id' => $superOrderId,
                'package_id' => $packageId,
                'product_id' => $productId,
                'line_id' => $lineId,
            )
        );
        if ($historyId) {
            $write->query(
                "UPDATE superorder_history_item shi SET shi.removed = 1, shi.status = 'Cancel' WHERE shi.entity_id = :entity_id LIMIT 1",
                array(
                    'entity_id' => $historyId
                )
            );
        }
    }

    /**
     * Updates history items for Nothing status
     * @param $superOrderId
     * @param $write
     * @param $packageId
     * @param $productId
     * @param $lineId
     * @param $status
     */
    protected function handleNothing($superOrderId, $write, $packageId, $productId, $lineId, $status)
    {
        $historyId = $write->fetchOne(
            "SELECT shi.entity_id FROM superorder_history sh INNER JOIN superorder_history_item shi ON sh.entity_id = shi.superorder_history_id WHERE sh.superorder_id = :superorder_id AND sh.package_id = :package_id AND shi.product_id = :product_id AND shi.line_id = :line_id LIMIT 1",
            array(
                'superorder_id' => $superOrderId,
                'package_id' => $packageId,
                'product_id' => $productId,
                'line_id' => $lineId,
            )
        );
        if ($historyId) {
            $write->query(
                "UPDATE superorder_history_item shi SET shi.status = :status WHERE shi.entity_id = :entity_id LIMIT 1",
                array(
                    'entity_id' => $historyId,
                    'status' => $status,
                )
            );
        }
    }

    /**
     * Updates history items for the Return status
     * @param $superOrderId
     * @param $superOrderParentId
     * @param $write
     * @param $returnPackageId
     * @param $productId
     */
    protected function handleReturn($superOrderId, $superOrderParentId, $write, $returnPackageId, $productId)
    {
        $hId = $superOrderId;
        if ($superOrderParentId) {
            $hId = $superOrderParentId;
        }
        $history = $write->fetchRow(
            "SELECT shi.* FROM superorder_history sh INNER JOIN superorder_history_item shi ON sh.entity_id = shi.superorder_history_id WHERE sh.superorder_id = :superorder_id AND sh.package_id = :package_id AND shi.product_id = :product_id AND shi.removed = :removed LIMIT 1",
            array(
                'superorder_id' => $hId,
                'package_id' => $returnPackageId,
                'product_id' => $productId,
                'removed' => 0,
            )
        );
        $historyId = $history['entity_id'];

        if ($historyId) {
            $write->query(
                "UPDATE superorder_history_item shi SET shi.removed = :removed, shi.status = :status WHERE shi.entity_id = :entity_id LIMIT 1",
                array(
                    'removed' => 1,
                    'status' => 'Return',
                    'entity_id' => $historyId,
                )
            );
        }
    }
}
