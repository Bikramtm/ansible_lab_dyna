<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Superorder_Model_History
 */
class Omnius_Superorder_Model_History extends Mage_Core_Model_Abstract
{
    /**
     * Constructor initialization
     */
    protected function _construct()
    {
        $this->_init('superorder/history');
    }

    /**
     * Returns a list of items for a given superorder history id
     * @return Omnius_Superorder_Model_Mysql4_Historyitem_Collection
     */
    public function getItems()
    {
        return Mage::getResourceModel('superorder/historyitem_collection')
            ->addFieldToFilter('superorder_history_id', $this->getId());
    }
}
