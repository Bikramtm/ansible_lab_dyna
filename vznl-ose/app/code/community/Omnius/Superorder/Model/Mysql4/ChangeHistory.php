<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Superorder_Model_Mysql4_ChangeHistory
 */

class Omnius_Superorder_Model_Mysql4_ChangeHistory extends Mage_Core_Model_Mysql4_Abstract
{
    /**
     * Constructor initialization
     */
    protected function _construct()
    {
        $this->_init('superorder/changeHistory', 'entity_id');
    }
}