<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Superorder_Model_Mysql4_Superorder
 */
class Omnius_Superorder_Model_Mysql4_Superorder extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init('superorder/superorder', 'entity_id');
    }
}
