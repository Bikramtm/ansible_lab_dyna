<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Superorder_Model_Mysql4_Historyitem_Collection
 */
class Omnius_Superorder_Model_Mysql4_Historyitem_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    /**
     * Constructor initialization
     */
    public function _construct()
    {
        $this->_init('superorder/historyitem');
    }
}
