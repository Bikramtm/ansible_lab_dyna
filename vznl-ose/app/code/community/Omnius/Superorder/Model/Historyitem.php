<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Superorder_Model_Historyitem
 */
class Omnius_Superorder_Model_Historyitem extends Mage_Core_Model_Abstract
{
    /**
     * Constructor initialization
     */
    protected function _construct()
    {
        $this->_init('superorder/historyitem');
    }
}
