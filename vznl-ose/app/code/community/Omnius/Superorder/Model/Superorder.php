<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Superorder_Model_Superorder
 */
class Omnius_Superorder_Model_Superorder extends Mage_Core_Model_Abstract
{
    const SO_VALIDATED = 'Validation approved';
    const SO_VALIDATION_FAILED = 'Validation declined';
    const SO_WAITING_FOR_VALIDATION = 'Waiting for Validation';
    const SO_WAITING_VALIDATION_ACCEPTED_AXI = 'Waiting for validation: accepted by AXI';
    const SO_CANCELLED = 'Cancelled';
    const SO_CLOSED = 'Closed';
    const SO_STATUS_FAILED = 'Validation failed';
    const SO_STATUS_CANCELLED = 'Validation Cancelled';
    const SO_STATUS_REJECTED = 'Validation Rejected';
    const SO_STATUS_REFERRED = 'Validation Referred';
    const SO_DELIVERED = 'Delivered';
    const SO_PRE_INITIAL = 'In initial queue';
    const SO_INITIAL = 'Waiting for validation: processing started';
    const SO_FAILED = 'Waiting for validation: technical failure';
    const SO_ADYEN_PAYMENT = 'Order not placed';
    const SO_FULFILLED = 'Fulfilled';

    const SO_CHANGE_AFTER = 'ChangeOrderAfterDelivery';
    const SO_CHANGE_BEFORE = 'ChangeOrderBeforeDelivery';
    const SO_CREATE = 'CreateSaleOrder';

    protected $statusTypes = array(
        Omnius_Superorder_Model_StatusHistory::SUPERORDER_STATUS
    );

    /** @var Dyna_Agent_Model_Dealer */
    protected $_dealer = null;
    /** @var Dyna_Agent_Model_Agent */
    protected $_agent = null;

    /**
     * Constructor initialization
     */
    protected function _construct()
    {
        $this->_init('superorder/superorder');
    }

    /**
     * Return a list of delivery orders (final or not)
     * @param bool $onlyFinal
     * @return Mage_Sales_Model_Resource_Collection_Abstract
     */
    public function getOrders($onlyFinal = false)
    {
        return Mage::getModel('sales/order')->getNonEditedOrderItems($this->getId(), $onlyFinal, false);
    }

    /**
     * Check if the superorder has delivery orders
     *
     * @return mixed
     */
    public function getDeliveryOrdersCount()
    {
        $collection = Mage::getModel('sales/order')->getCollection()
            ->addFieldToSelect('*')
            ->addFieldToFilter('superorder_id', $this->getId())
            ->addFieldToFilter('edited', array('neq' => 1))
            ->count();

        return $collection;
    }

    /**
     * Check whether or not an super order has deleted products
     *
     * @return bool
     */
    public function hasDeletedProducts()
    {
        /** @var Omnius_Checkout_Model_Sales_Order $order */
        foreach ($this->getOrders(true) as $order) {
            /** @var Omnius_Checkout_Model_Sales_Order_Item $orderItem */
            foreach ($order->getAllItems() as $orderItem) {
                if ($orderItem->getProduct()->getId() === null) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Returns all packages of the current superorder or only one package if an id is provided
     * @param int $id
     * @return mixed
     */
    public function getPackages($id = null)
    {
        $orderId = $id ? $id : $this->getId();

        return Mage::getModel('package/package')->getOrderPackages($orderId)->load();
    }

    /**
     * Returns a list of delivery order items (final or not)
     * @param bool $onlyFinal
     * @param bool $asArray
     * @param array $orderIds
     * @param array $allModifiedPackages
     * @return array
     */
    public function getOrderedItems($onlyFinal = false, $asArray = false, $orderIds = [], $allModifiedPackages = null)
    {
        $items = [];
        /** @var Omnius_Checkout_Model_Sales_Order $order */
        foreach ($this->getOrders($onlyFinal) as $order) {

            // skip cancelled orders
            if (strtolower($order->getStatus()) == strtolower(Omnius_Checkout_Model_Sales_Order::STATUS_CANCELLED)) {
                continue;
            }

            /** @var Omnius_Checkout_Model_Sales_Order_Item $orderItem */
            foreach ($order->getAllItems() as $orderItem) {

                if ($orderIds && !in_array($order->getIncrementId(), $orderIds)) {
                    continue;
                }

                // Skip items that were not in modified packages
                if ($allModifiedPackages && !in_array($orderItem->getPackageId(), $allModifiedPackages)) {
                    continue;
                }
                $items[$orderItem->getPackageId()][] = $asArray ? $orderItem->toArray() : $orderItem;
            }
        }

        return $items;
    }

    /**
     * Returns the history of the current superorder
     * @return Omnius_Superorder_Model_Mysql4_History_Collection
     */
    public function getHistory()
    {
        return Mage::getResourceModel('superorder/history_collection')
            ->addFieldToFilter('superorder_id', $this->getId());
    }

    /**
     * Returns the collection of orders with completed Credit Checks
     *
     * @param null $id
     * @return array
     */
    public function getCollectionForCustomer($id = null)
    {
        if ($id == null) {
            $customerId = $this->_getCustomerSession()->getCustomerId();
        } else {
            $customerId = $id;
        }

        return $this->_getSuperordersCollection($customerId);
    }

    /**
     * Verify if a specific superorder belongs to the current session customer
     *
     * @param $orderId
     * @return array
     */
    public function belongsToCustomer($orderId)
    {
        $customerId = $this->_getCustomerSession()->getCustomerId();

        return $this->_getSuperordersCollection($customerId, $orderId);
    }

    /**
     * Filters a given collection of superorder based on multiple criterias (agent, website)
     * @param Omnius_Superorder_Model_Mysql4_Superorder_Collection $superOrders
     */
    public function applyFiltersByAgent($superOrders)
    {
        /** @var Dyna_Agent_Model_Agent $agent */
        $agent = $this->_getCustomerSession()->getSuperAgent() ?: $this->_getCustomerSession()->getAgent(true);
        if ($agent && $agent->getId()) {
            if ($agent->isGranted(array('SEARCH_ORDER_OF_DEALER')) && $agent->isGranted(array('SEARCH_ORDER_OF_GROUP')) && !$agent->isGranted('SEARCH_ORDER_OF_ALL')) {
                $superOrders->addFieldToFilter(
                    ['main_table.created_agent_id', 'main_table.created_dealer_id'],
                    [['in' => array_unique(array_merge([$agent->getAgentId()], $agent->getAgentsInSameGroup()))], ['eq' => $agent->getDealerId()]]
                );
            } elseif ($agent->isGranted(array('SEARCH_ORDER_OF_GROUP')) && !$agent->isGranted('SEARCH_ORDER_OF_ALL')) {
                $superOrders->addFieldToFilter('main_table.created_agent_id', array('in' => array_unique(array_merge([$agent->getId()], $agent->getAgentsInSameGroup()))));
            } elseif ($agent->isGranted(array('SEARCH_ORDER_OF_DEALER')) && !$agent->isGranted('SEARCH_ORDER_OF_ALL')) {
                $superOrders->addFieldToFilter(
                    ['main_table.created_dealer_id', 'main_table.created_agent_id'],
                    [['eq' => $agent->getDealerId()], ['eq' => $agent->getId()]]
                );
            } elseif (!$agent->isGranted('SEARCH_ORDER_OF_ALL')) {
                // If he has no search permissions return empty
                $superOrders->addFieldToFilter(
                    ['main_table.created_agent_id'],
                    [['eq' => $agent->getId()]]
                );
            }
        }

        return $superOrders;
    }

    /**
     * Returns a list of superorders for a given customer
     * @param $customerId
     * @param null $orderId
     * @return array
     */
    protected function _getSuperordersCollection($customerId, $orderId = null)
    {
        $agent = $this->_getCustomerSession()->getSuperAgent() ?: $this->_getCustomerSession()->getAgent(true);

        $collection = Mage::getModel('package/package')->getCollection()
            // Ignore packages where there is no checksum, because they contain no items and are most likely created using multiple tabs
            ->addFieldToFilter('main_table.checksum', ['neq' => 'null']);
        $collection->getSelect()->join(
            array('superorder'),
            'main_table.order_id = superorder.entity_id',
            array('superorder.order_status', 'superorder.order_number', 'order_created_at' => 'superorder.created_at', 'customer_id' => 'superorder.customer_id')
        );
        $collection->addFieldToFilter('customer_id', $customerId);
        $collection->load();

        /** @var Omnius_Superorder_Model_Mysql4_Superorder_Collection $superOrders */
        $superOrders = Mage::getModel('superorder/superorder')->getCollection();
        $superOrders->addFieldToFilter('main_table.customer_id', $customerId);

        if ($orderId) {
            $superOrders->addFieldToFilter('main_table.entity_id', $orderId);
        }

        $superOrders->getSelect()->joinRight(array('sales_flat_order'), 'main_table.entity_id = sales_flat_order.superorder_id', array())->group('main_table.entity_id');

        if ($agent && $agent->getId()) {
            $superOrders = $this->applyFiltersByAgent($superOrders);
        }

        $superOrders->load();
        $response = [];
        if (count($collection) && count($superOrders)) {
            $found = [];
            foreach ($collection as $order) {
                $found[$order->getOrderId()] = $order->getOrderNumber();
            }
            foreach ($superOrders as $superOrder) {
                if (isset($found[$superOrder->getId()])) {
                    $response[$superOrder->getId()] = $superOrder;
                }
            }
        }

        return $response;
    }

    /**
     * Should return a true if the superorder is editable
     */
    public function isEditable($deliveryInThisStore = false)
    {
        $agent = $this->_getCustomerSession()->getAgent(true);

        $permissionsCond = $agent->canDoActionOnSuperorderPackage(Dyna_Agent_Model_Agent::AGENT_CANCEL_PACKAGE,
                $this) || $agent->canDoActionOnSuperorderPackage(Dyna_Agent_Model_Agent::AGENT_CHANGE_PACKAGE, $this) || $deliveryInThisStore;
        $processedCond = $this->getAxiProcessed() || $this->isVirtualDelivery();

        $deliveredCond = (!$this->hasPackagesOnItsWay())
            && (!$this->hasAllDeliveredPackagesEdited())
            && (!$this->hasDeletedProducts());

        $editedCond = $deliveredCond && $processedCond && (!$this->hasNotEditableDeliveryOrders()); // Added from DF-005890

        $statusCond = !in_array(strtolower($this->getOrderStatus()), array(
            strtolower(self::SO_CANCELLED),
            strtolower(self::SO_CLOSED),
            strtolower(self::SO_WAITING_FOR_VALIDATION),
            strtolower(self::SO_INITIAL),
            strtolower(self::SO_WAITING_VALIDATION_ACCEPTED_AXI),
            strtolower(self::SO_PRE_INITIAL)
        ));

        return ($permissionsCond && $statusCond && $editedCond);
    }

    /**
     * Check if the superorder has delivery orders at the Point of No Return (DF-005890)
     */
    public function hasNotEditableDeliveryOrders()
    {
        foreach ($this->getOrders(true) as $order) {
            if (in_array($order->getEcomStatus(), [Omnius_Checkout_Model_Sales_Order::STATUS_RDY4PICK, Omnius_Checkout_Model_Sales_Order::STATUS_RDY4SHIP, Omnius_Checkout_Model_Sales_Order::STATUS_PICKED])) {
                return true;
            }
        }

        return false;
    }

    /**
     * Checks whether the superorder contains only virtual deliveries.
     * @return bool
     */
    public function isVirtualDelivery()
    {
        /** @var Omnius_Checkout_Model_Sales_Order $order */
        foreach ($this->getOrders(true) as $order) {
            if (!$order->isVirtualDelivery()) {
                return false;
            }
        }

        return true;
    }

    /**
     * Checks whether or not the current superorders has children (resulted from editing an older delivered order)
     * @return bool
     */
    public function hasChildren()
    {
        return $this->getChildren()->count() > 0;
    }

    /**
     * Checks whether or not the current superorders has grand children (resulted from editing an older edited delivered order)
     * @return bool
     */
    public function hasGrandChildren()
    {
        return $this->getChildren()->count() > 1;
    }

    /**
     * Checks if it has parents (meaning it was edited and resulted in another superorder
     * @return bool
     */
    public function hasParents()
    {
        return $this->getParents()->count() > 0;
    }

    /**
     * Checks if it has grand parents (meaning it was edited and resulted in another superorder which in turn has parent)
     * @return bool
     */
    public function hasGrandParents()
    {
        return $this->getParents()->count() > 1;
    }

    /**
     * Returns all children of the current superorder
     * @return Varien_Data_Collection
     * @throws Exception
     */
    public function getChildren()
    {
        $children = new Varien_Data_Collection();
        $sql = 'SELECT * FROM ' . $this->getResource()->getMainTable() . ' WHERE parent_id=:eId;';

        $binds = array(
            ':eId' => $this->getId()

        );

        $connection = $this->getResource()->getReadConnection();
        $resourceName = $this->getResourceName();
        $this->_findChildren($children, $connection, $sql, $binds, $resourceName);

        return $children;
    }

    /**
     * Finds a specific children of the current superorder
     * @param $eId
     * @param $children
     * @param $connection
     * @param $sql
     * @param $resourceName
     */
    protected function _findChildren($children, $connection, $sql, $binds, $resourceName)
    {
        $childs = $connection->fetchAll($sql, $binds);
        foreach ($childs as &$child) {
            $children->addItem(Mage::getModel($resourceName)->addData($child));
            $this->_findChildren($children, $connection, $sql,
                array(
                    ':eId' => $child['entity_id']
                ), $resourceName);
        }
    }

    /**
     * Returns a list of parents of the current superorder
     * @return Varien_Data_Collection
     * @throws Exception
     */
    public function getParents()
    {
        $parents = new Varien_Data_Collection();
        if (!$pId = $this->getParentId()) {
            return $parents;
        }
        $_parents = [];
        $sql = 'SELECT * FROM ' . $this->getResource()->getMainTable() . ' WHERE entity_id=:id;';
        $binds = array(
            ':id' => $pId
        );

        $connection = $this->getResource()->getReadConnection();
        $resourceName = $this->getResourceName();

        while ($parent = $connection->fetchRow($sql, $binds)) {
            $_parents[] = Mage::getModel($resourceName)->addData($parent);
            if (!isset($parent['parent_id'])) {
                unset($parent);
                break;
            }

            $binds = array(
                ':id' => $parent['parent_id']
            );

            unset($parent);
        }
        usort($_parents, function ($a, $b) {
            return $a->getId() > $b->getId();
        });
        foreach ($_parents as $p) {
            $parents->addItem($p);
        }

        return $parents;
    }

    /**
     * Get list of package ids that are delivered
     */
    public function getDeliveredPackageIds()
    {
        $deliveredPackageIds = [];
        $packages = $this->getPackages();
        foreach ($packages as $package) {
            if ($package->isDelivered()) {
                $deliveredPackageIds[] = $package->getPackageId();
            }
        }

        return $deliveredPackageIds;
    }

    /**
     * @param Omnius_Superorder_Model_Superorder $parentSuperorder
     * @return $this
     *
     * Create a new superorder
     */
    public function createNewSuperorder($parentSuperorder = null)
    {
        $customerSession = $this->_getCustomerSession();
        // The new superorder will have the created at website of the parent superorder
        $this->setCreatedWebsiteId($parentSuperorder ? $parentSuperorder->getCreatedWebsiteId() : Mage::app()->getWebsite()->getId());
        $this->setOrderStatus(self::SO_PRE_INITIAL);
        $this->setCustomerId($customerSession->getCustomerId());
        $this->setCreatedAt(now());
        $this->setParentId($parentSuperorder ? $parentSuperorder->getId() : null);
        $agent = $customerSession->getAgent();
        $dealer = $agent ? $agent->getDealer() : null;
        if ($agent) {
            $this->setCreatedAgentId($agent->getId());
        }
        if ($dealer) {
            $this->setCreatedDealerId($dealer->getId());
        }
        $this->save();
        $this->setOrderNumber(Mage::helper('omnius_checkout')->getSuperOrderOffset() + (int) $this->getId())->save();

        return $this;
    }

    /**
     * Get the name of the agent and dealer who updated the superorder
     */
    public function getCreatedByAgentAndWebsite()
    {
        //get the first order associated with the superorder
        $agent = Mage::getModel('agent/agent')->load($this->getCreatedAgentId());
        $agentName = $agent->getId() ? ($agent->getFirstName() . ' ' . $agent->getLastName()) : '';

        $website = Mage::app()->getWebsite($this->getCreatedWebsiteId());
        $websiteName = $website->getId() ? $website->getName() : '';

        return array($agentName, $websiteName);
    }

    /**
     * Get the name of the agent and dealer who updated the superorder
     */
    public function getAgentAndWebsite()
    {
        //get the first order associated with the superorder
        $agent = Mage::getModel('agent/agent')->load($this->getAgentId());
        $agentName = $agent->getId() ? ($agent->getFirstName() . ' ' . $agent->getLastName()) : '';

        $website = Mage::app()->getWebsite($this->getWebsiteId());
        $websiteName = $website->getId() ? $website->getName() : '';

        return array($agentName, $websiteName);
    }

    /**
     * Retrieves the customer model of this superorder
     * @return Mage_Core_Model_Abstract
     */
    public function getCustomer()
    {
        return Mage::getModel('customer/customer')->load($this->getCustomerId());
    }

    /**
     * Cancel the SuperOrder and all the linked orders
     */
    public function cancel()
    {
        $packages = $this->getPackages()->getItems();
        $cancelPackages = [];
        $cancelledCtns = [];
        $refundAmounts = [];
        $refundMethod = null;

        foreach ($packages as $package) {
            $cancelPackages[] = $package->getPackageId();
            $packageItems = $package->getPackageItems();
            $packageTotals = $package->getPackageTotals($packageItems);
            // Get order refund amount
            $refundAmounts['refundAmount'][$package->getPackageId()] = $packageTotals['total'];
            $refundAmounts['payments'][$package->getPackageId()] = $packageTotals['additional_total'];
            $cancelledCtns[] = $package->getTelNumber();
        }

        // Lock the order
        Mage::helper('omnius_checkout')->lockOrder($this->getId(), Mage::helper('agent')->getAgentId(), 'Order cancel');

        // Process order
        $client = Mage::helper('omnius_service')->getClient('ebs_orchestration');
        $pollingId = $client->processCustomerOrder($this, null, null, null, null, $cancelPackages, $refundAmounts, $refundMethod); //call esb

        // Save data for later usage, after the call is performed
        $this->setData('polling_id', $pollingId);
        $this->_getCustomerSession()->setData(
            $pollingId,
            array(
                'cancelled_ctns' => $cancelledCtns,
                'packages' => $packages,
                'superorder' => $this
            )
        );

        return $this;
    }

    /**
     * Release lock and set order as cancelled.
     * @param $pollingId
     * @return $this
     * @throws Exception
     */
    public function postProcessOrderCancel($pollingId)
    {
        $sessionData = $this->_getCustomerSession()->getData($pollingId);
        $cancelledCtns = $sessionData['cancelled_ctns'];
        $packages = $sessionData['packages'];
        // Release lock
        Mage::helper('omnius_checkout')->releaseLockOrder($this->getId(), Mage::helper('agent')->getAgentId(), false);

        $customerId = $this->getCustomerId();
        // Delete CTN from customer_ctn table
        foreach ($cancelledCtns as $ctn) {
            $write = Mage::getSingleton('core/resource')->getConnection('core_write');
            $write->exec("DELETE FROM customer_ctn WHERE ctn = '" . $ctn . "' AND customer_id = '" . $customerId . "'");
        }

        $orders = $this->getOrders();
        /** @var Omnius_Checkout_Model_Sales_Order $order */
        foreach ($orders->getItems() as $order) {
            $order->cancel();
            $order->save();
        }

        foreach ($packages as $package) {
            $package->setStatus(Omnius_Package_Model_Package::ESB_PACKAGE_STATUS_CANCELLED);
            $package->save();
        }

        $this->setOrderStatus(self::SO_CANCELLED);
        $this->save();

        return $this;
    }

    /**
     * Sort the packages of the current superorder
     */
    public function sortPackages()
    {
        // Get all the packages
        $packages = $this->getPackages();

        $oldPackages = $this->getPackages($this->getParentId());

        $packageIds = [];
        foreach ($packages as $package) {
            $packageIds[$package->getPackageId()] = $package->getPackageId();
        }

        ksort($packageIds);
        $count = 1;
        foreach ($packageIds as $id => $value) {
            $packageIds[$id] = $count;
            ++$count;
        }

        $reorderToPayAmount = [];
        // Reorder to pay amount that is calculated on frontend
        $toPayAmount = Mage::getSingleton('customer/session')->getToPayAmount();
        // Renumber packages
        foreach ($packages as $package) {
            $package
                ->setOldPackageId($package->getPackageId())
                ->setPackageId($packageIds[$package->getPackageId()])
                ->save();

            if ($toPayAmount && is_array($toPayAmount)) {
                foreach ($toPayAmount as $packageId => $amount) {
                    $reorderToPayAmount[$packageIds[$packageId]] = $amount;
                }
            }
        }
        Mage::getSingleton('customer/session')->setToPayAmount($reorderToPayAmount);
        foreach ($oldPackages as $oldPackage) {
            if (isset($packageIds[$oldPackage->getPackageId()])) {
                $oldPackage->setNewPackageId($packageIds[$oldPackage->getPackageId()])->save();
            }
        }
        $orders = $this->getOrders(true);
        // Renumber items
        foreach ($orders as &$order) {
            $items = $order->getAllItems();
            foreach ($items as &$item) {
                if ($item->getPackageId() !== $packageIds[$item->getPackageId()]) {
                    $item->setPackageId($packageIds[$item->getPackageId()])->save();
                }
            }
            unset($item);
        }
        unset($order);
    }

    /**
     * Retrieves a superder model based on the provided order number
     * @param $orderNumber
     * @return mixed
     *
     * Returns a superorder based on order number (which is unique)
     */
    public function getSuperOrderByOrderNumber($orderNumber)
    {
        return $this->getCollection()->addFieldToFilter('order_number', $orderNumber)->getFirstItem();
    }

    /**
     * Calculate delivery costs for the entire superorder
     * @return float
     * @todo Has to be implemented
     */
    public function getDeliveryCosts()
    {
        return 0;
    }

    /**
     * Check if a superorder has any delivered packages
     */
    public function hasDeliveredPackages()
    {
        $key = __METHOD__ . ' ' . $this->getId();
        if (Mage::registry('freeze_models') === true) {
            $data = Mage::registry($key);
            if ($data !== null) {
                return $data;
            }
        }

        $hasDeliveredPackages = Mage::getModel('package/package')->getCollection()
            ->addFieldToSelect('entity_id')
            ->addFieldToFilter('order_id', $this->getId())
            ->addFieldToFilter('status', Omnius_Package_Model_Package::ESB_PACKAGE_STATUS_DELIVERED)
            ->getSize();

        if (Mage::registry('freeze_models') === true) {
            Mage::unregister($key);
            Mage::register($key, $hasDeliveredPackages);
        } else {
            Mage::unregister($key);
        }

        return $hasDeliveredPackages;
    }

    /**
     * Mark the superorder as failed and packages as cancelled and cc/porting status rejected
     * @throws Exception
     */
    public function setFailedStockReservation()
    {
        $this->setOrderStatus(self::SO_STATUS_FAILED);
        $this->setParentId(null);
        $this->save();
        $packages = $this->getPackages();
        foreach ($packages as $package) {
            $package->setStatus(Omnius_Package_Model_Package::ESB_PACKAGE_STATUS_CANCELLED);
            $package->setPortingStatus(Omnius_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_NP_REJECTED);
            $package->save();
        }
    }

    /**
     * Checks if a superorder has any products of type Garant
     */
    public function getGarantProductOrders()
    {
        $orderIds = [];
        $packagesWithGarant = [];
        //if superorder does not exist anymore, don't execute queries
        if (!$this->getId()) {
            return false;
        }

        //get attribute id for the addon_type
        $eavAttribute = new Mage_Eav_Model_Mysql4_Entity_Attribute();
        $attrId = $eavAttribute->getIdByCode('catalog_product', Omnius_Catalog_Model_Product::ADDON_TYPE_ATTR);

        //get id for the "Garant" option
        $_product = Mage::getModel('catalog/product');
        $attr = $_product->getResource()->getAttribute(Omnius_Catalog_Model_Product::ADDON_TYPE_ATTR);

        if ($attr && $attr->usesSource()) {
            $valueId = $attr->getSource()->getOptionId("Garant");
        }

        if (isset($valueId) && $valueId) {
            $read = Mage::getSingleton('core/resource')->getConnection('core_read');
            $query = "
            SELECT o.entity_id, oi.package_id 
            FROM catalog_product_entity_varchar cpe 
            INNER JOIN sales_flat_order_item AS oi ON cpe.entity_id=oi.product_id
            INNER JOIN sales_flat_order AS o ON o.entity_id=oi.order_id 
            WHERE cpe.attribute_id=:attr 
            AND cpe.VALUE=:value 
            AND o.edited != 1 and o.superorder_id=:orderId";

            $binds = array(
                ':attr' => $attrId,
                ':value' => $valueId,
                ':orderId' => $this->getId()
            );

            $queryResult = $read->query($query,$binds);
            $results = $queryResult->fetchAll();
            foreach ($results as $result) {
                $orderIds[] = $result['entity_id'];
                $packagesWithGarant[$result['entity_id']] = isset($packagesWithGarant[$result['entity_id']]) ? $packagesWithGarant[$result['entity_id']] : [];
                $packagesWithGarant[$result['entity_id']][] = $result['package_id'];
            }

        }

        return array($packagesWithGarant, array_unique($orderIds));
    }

    /**
     * Checks if there is at least one retention package in the superorder
     * @param bool $onlyFinal
     * @return bool
     */
    public function hasRetention($onlyFinal = true)
    {
        return $this->checkForPackageType(Omnius_Checkout_Model_Sales_Quote_Item::RETENTION, $onlyFinal);
    }

    /**
     * Checks if there is at least one acquisition package in the superorder
     * @param bool $onlyFinal
     * @return bool
     */
    public function hasAcquisition($onlyFinal = true)
    {
        return $this->checkForPackageType(Omnius_Checkout_Model_Sales_Quote_Item::ACQUISITION, $onlyFinal);
    }

    /**
     * Checks if the superorder contains only packages of a certain type
     * @param $type
     * @param bool $onlyFinal
     * @return bool
     */
    public function hasOnlyOfType($type, $onlyFinal = true)
    {
        $result = true;
        foreach ($this->getOrders($onlyFinal) as $order) {
            /** @var Omnius_Checkout_Model_Sales_Order $order */
            foreach ($order->getPackages() as $package) {
                if ($package->getSaleType() != $type) {
                    return false;
                }
            }
        }

        return $result;
    }

    /**
     * Checks if the superorder has number porting
     * @param bool $onlyFinal
     * @return bool
     */
    public function hasNumberPorting($onlyFinal = true)
    {
        foreach ($this->getOrders($onlyFinal) as $order) {
            /** @var Omnius_Checkout_Model_Sales_Order $order */
            foreach ($order->getPackages() as $package) {
                if ($package->getCurrentNumber()) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Checks if the superorder contains at least one package of a certain type
     * @param $type
     * @param bool $onlyFinal
     * @return bool
     */
    protected function checkForPackageType($type, $onlyFinal = true)
    {
        foreach ($this->getOrders($onlyFinal) as $order) {
            /** @var Omnius_Checkout_Model_Sales_Order $order */
            foreach ($order->getPackages() as $package) {
                if ($package->getSaleType() == $type) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Check if the superorder has children after it was delivered
     */
    public function isDeliveredAndEdited()
    {
        return (bool) $this->getCollection()->addFieldToFilter('parent_id', $this->getId())
            ->addFieldToSelect('entity_id')
            ->count();
    }

    /**
     * Check if the superorder was created in this channel
     */
    public function isInThisChannel()
    {
        return $this->getWebsiteId() == Mage::app()->getWebsite()->getId();
    }

    /**
     * Return the collection for all the orders derived from this one
     */
    public function getChildSuperorders($packageId = null)
    {
        if ($packageId) {
            $conn = Mage::getSingleton('core/resource')->getConnection('core_read');
            $superOrderId = $conn->fetchPairs("select s.entity_id, s.entity_id from superorder s inner join catalog_package cp on s.entity_id=cp.order_id where cp.package_id=" . $packageId . " and s.parent_id=" . $this->getId());
            if ($superOrderId) {
                return $this->getCollection()->addFieldToFilter('entity_id', current($superOrderId))->getFirstItem();
            } else {
                return false;
            }
        }

        return $this->getCollection()->addFieldToFilter('parent_id', $this->getId());
    }

    /**
     * Checks if the current superorder has an agent associated
     * @return bool
     */
    public function hasAgentRetrieved()
    {
        return $this->_agent !== null;
    }

    /**
     * Check if the order has the proper status and all packages have been approved
     */
    public function isValidated()
    {
        if (in_array(strtolower($this->getOrderStatus()),
            array(strtolower(self::SO_WAITING_FOR_VALIDATION), strtolower(self::SO_WAITING_VALIDATION_ACCEPTED_AXI), strtolower(self::SO_PRE_INITIAL), strtolower(self::SO_INITIAL)))) {
            return false;
        }

        return true;
    }

    /**
     * Checks if the superorder is cancelled
     * @return bool
     */
    public function isCancelled()
    {
        return strtolower($this->getOrderStatus()) == strtolower(self::SO_CANCELLED);
    }

    /**
     * Retrieve the agent that created the superorder
     *
     * @return Dyna_Agent_Model_Agent
     */
    public function getAgent()
    {
        if (!$this->hasAgentRetrieved()) {
            $this->_agent = Mage::getModel('agent/agent')->load($this->getCreatedAgentId());

            return $this->_agent;
        } else {
            return $this->_agent;
        }
    }

    /**
     * Retrieve the dealer that created the superorder
     *
     * @return Dyna_Agent_Model_Dealer
     */
    public function getDealer()
    {
        if ($this->_dealer === null) {
            $this->_dealer = Mage::getModel('agent/dealer')->load($this->getCreatedDealerId());
        }

        return $this->_dealer;
    }

    /**
     * Make default for is_vf_only 1
     *
     * @return Mage_Core_Model_Abstract|void
     */
    protected function _beforeSave()
    {
        //only update agent/dealer/website on frontend
        $website = Mage::app()->getWebsite();
        if ($website->getId() && !$this->getSkipUpdatedAt()) {
            $agent = $this->_getCustomerSession()->getAgent();
            $dealer = $agent ? $agent->getDealer() : null;

            $this->setWebsiteId($website->getId());
            if ($dealer) {
                $this->setDealerId($dealer->getId());
            } else {
                $this->setDealerId(null);
            }
            if ($agent) {
                $this->setAgentId($agent->getId());
            } else {
                $this->setAgentId(null);
            }
        }

        //for new superorder, if customer does not have ban, set new_customer to true
        if (!$this->getId() && !$this->getCustomer()->getBan()) {
            $this->setNewCustomer(1);
        }

        return parent::_beforeSave();
    }

    /**
     * Get checkout session model instance
     *
     * @return Omnius_Customer_Model_Session
     */
    protected function _getCustomerSession()
    {
        return Mage::getSingleton('customer/session');
    }

    /**
     * Only consider this value if there are delivered packages
     *
     * @return bool|mixed
     */
    public function getAxiProcessed()
    {
        if (!$this->hasDeliveredPackages()) {
            return true;
        } else {
            return (bool) $this->getData('axi_processed');
        }
    }

    /**
     * Checks if a given package of a delivered superorder was already changed
     * @param $packageId
     * @return int
     */
    public function hasPackageChanged($packageId)
    {
        $conn = Mage::getSingleton("core/resource")->getConnection("core_read");

        return $conn
            ->fetchOne("
            SELECT entity_id 
            FROM catalog_package 
            WHERE package_id = ':package' 
            AND order_id = ':id' 
            AND new_package_id IS NOT NULL;" ,
                array(
                    ':package' => $packageId,
                    ':id' => $this->getId()
                )
            );
    }

    /**
     * Check if a superorder which expect return delivery has all hardware returned
     * @return bool
     */
    public function hasAllPackagesReturned()
    {
        $currentPackageIds = [];
        foreach ($this->getPackages() as $packageObj) {
            $currentPackageIds[] = $packageObj->getOldPackageId();
        }

        /** @var Omnius_Superorder_Model_Superorder $oldSuperOrder */
        $oldSuperOrder = Mage::getModel('superorder/superorder')->getCollection()->addFieldToFilter('entity_id', $this->getParentId())->getFirstItem();
        /** @var Omnius_Package_Model_Package $package */
        foreach ($oldSuperOrder->getPackages() as $package) {
            $packageCond = $package->getHardwareChange() && !$package->getCompleteReturn() && $package->wasDelivered();
            if (in_array($package->getPackageId(), $currentPackageIds) && $packageCond) {
                return false;
            }
        }

        return true;
    }

    /**
     * Tells whether the current superorder had any changes without return.
     * @return bool
     */
    public function hasAllHardwareChangesReturnedForDelivered()
    {
        /** @var Omnius_Package_Model_Package $package */
        foreach ($this->getPackages() as $package) {
            if ($package->getHardwareChange() && !$package->getCompleteReturn() && $package->wasDelivered()) {
                return false;
            }
        }

        return true;
    }

    /**
     * Returns whether the processOrder call be made.
     */
    public function cantProcessOrder()
    {
        return (($this->getId() && $this->getParentId() && !$this->hasAllPackagesReturned()) || ($this->hasAllHardwareChangesReturnedForDelivered() == false));
    }

    /**
     * Return the quote from which the superorder originated
     *
     * @return Mage_Sales_Model_Quote
     */
    public function getOriginalQuote()
    {
        return Mage::getModel('sales/quote')->load($this->getOrders()->getFirstItem()->getQuoteId());
    }

    /**
     * After save event that checks if statuses changed in the meanwhile
     * @return Mage_Core_Model_Abstract|void
     */
    protected function _afterSave()
    {
        Mage::helper('superorder')->checkStatusesChanged($this->getOrigData(), $this->getData(), $this->statusTypes, $this);
        Mage::helper('superorder')->checkIfChanged($this->getOrigData(), $this->getData());
        parent::_afterSave();

        return $this;
    }

    /**
     * When loading a superorder, if the id does not exist, throw exception
     *
     * @param int $id
     * @param null $field
     * @return Mage_Core_Model_Abstract|void
     * @throws Exception
     */
    public function load($id, $field = null)
    {
        parent::load($id, $field);
        if ($id && !$this->getId()) {
            throw new Exception(Mage::helper('omnius_checkout')->__('Invalid Order id'));
        }

        return $this;
    }

    /**
     * Return if superorder has packages on it`s way
     */
    protected function hasPackagesOnItsWay()
    {
        $packages = $this->getPackages();
        foreach ($packages as $package) {
            if (mb_strtolower($package->getStatus()) === mb_strtolower(Omnius_Package_Model_Package::ESB_PACKAGE_STATUS_ON_ITS_WAY)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Return if superorder has all delivered packages edited
     */
    protected function hasAllDeliveredPackagesEdited()
    {
        $packages = $this->getPackages();
        foreach ($packages as $package) {
            if (!$package->getNewPackageId()) {
                return false;
            }
        }

        return true;
    }

    /**
     * RFC-150742 Returns the name of the agent who cancelled the super order
     * @return string
     */
    public function getCancelledBy()
    {
        $statusHistory = Mage::getModel('superorder/statusHistory')->getCollection()
            ->addFieldToFilter('superorder_id', $this->getId())
            ->addFieldToFilter('agent_id', array('neq' => 'NULL'))
            ->setOrder('created_at', 'DESC')
            ->getFirstItem();
        if ($statusHistory && $statusHistory->getId()) {
            if (!is_numeric($statusHistory->getAgentId())) {
                return $statusHistory->getAgentId();
            }

            $agent = Mage::getModel('agent/agent')->load($statusHistory->getAgentId());
            $agentName = $agent->getId() ? ($agent->getFirstName() . ' ' . $agent->getLastName()) : $statusHistory->getAgentId();

            return $agentName;
        } else {
            return "";
        }
    }

    /**
     * Check at superorder level if any of the packages are missing the sim while having number porting
     */
    public function getMissingSim()
    {
        if ($this->_missingSim === null) {
            $this->_missingSim = false;
            foreach ($this->getPackages() as $package) {
                if ($package->getCurrentNumber() && !$package->getSimNumber()) {
                    return ($this->_missingSim = true);
                }
            }
        }

        return $this->_missingSim;
    }

    /**
     * Return last type of change performed on this superorder (changeBefore/changeAfter/create)
     */
    public function getLastChangeType()
    {
        //if it was delivered and also edited, it is a change after
        if ($this->isDeliveredAndEdited() || $this->getParentId()) {
            return self::SO_CHANGE_AFTER;
        }

        //if it doesn't have children but has a delivered package that was cancelled, it is also change after
        foreach ($this->getPackages() as $package) {
            if ($package->wasDelivered() && $package->isCancelled()) {
                return self::SO_CHANGE_AFTER;
            }
        }

        $editedDeliveryOrders = Mage::getModel('sales/order')
            ->getCollection()
            ->addFieldToFilter('superorder_id', $this->getId())
            ->addFieldToFilter('edited', array('eq' => 1))
            ->getItems();
        if (count($editedDeliveryOrders) || $this->isCancelled()) {
            return self::SO_CHANGE_BEFORE;
        }

        return self::SO_CREATE;
    }

    /**
     * Get a list of payment methods of a superorder
     * return Comma separated string with payment methods
     *
     * @return string
     */
    public function getAllPaymentMethods()
    {
        $payments = array();
        foreach ($this->getOrders() as $order) {
            if ($order->getPayment() && $order->getPayment()->getMethodInstance() && $order->getPayment()->getMethodInstance()->getCode()) {
                $payments[] = $order->getPayment()->getMethodInstance()->getCode();
            }
        }

        return implode(', ', array_unique($payments));
    }

    /**
     * Get a list of delivery methods of a superorder
     * return Comma separated string with delivery methods
     *
     * @return string
     */
    public function getAllDeliveryMethods()
    {
        $deliveries = array();
        foreach ($this->getOrders(true) as $order) {
            if ($order->getShippingAddress() && ($type = $order->getShippingAddress()->getDeliveryType())) {
                if ($type == 'billing_address' || $type == 'other_address') {
                    $deliveries[] = Mage::helper('dyna_checkout')->__('home delivery');
                } else {
                    $lastDealer = Mage::getModel('agent/dealer')->load($this->getDealerId());
                    $deliveryDealer = Mage::getModel('agent/dealer')->load($this->getDealerId());
                    if ($lastDealer && $deliveryDealer && $lastDealer->getAxiStoreCode() === $deliveryDealer->getAxiStoreCode()) {
                        $deliveries[] = Mage::helper('dyna_checkout')->__('order store');
                    } else {
                        $deliveries[] = Mage::helper('dyna_checkout')->__('other store');
                    }
                }
            }
        }

        return implode(', ', array_unique($deliveries));
    }

    /**
     * Get current one time charge for an entire
     */
    public function getCurrentOnTimeCharge()
    {
        $packageModel = Mage::getModel('package/package');
        $orderItems = array();
        $oneTimeCharge = 0;
        foreach ($this->getOrders(true) as $order) {
            foreach ($order->getAllItems() as $orderItem) {
                $orderItems[$orderItem->getPackageId()][] = $orderItem;
            }
        }

        $oldPackages = array();
        foreach ($this->getPackages() as $package) {
            if ($package->getOldPackageId()) {
                if (!$package->wasDelivered()) {
                    $oldPackages[] = $package->getOldPackageId();
                }
                if ($package->isCancelled() && ($package->arrivedCancelled() || $package->wasDelivered())) {
                    foreach ($orderItems[$package->getPackageId()] as $item) {
                        $oneTimeCharge = $packageModel->roundFloat($oneTimeCharge - $item->getItemFinalPriceInclTax());
                    }
                }
            } elseif ($package->isCancelled() && isset($orderItems[$package->getPackageId()])) {
                foreach ($orderItems[$package->getPackageId()] as $item) {
                    $oneTimeCharge = $packageModel->roundFloat($oneTimeCharge - $item->getItemFinalPriceInclTax());                    
                }
            }
            if (!$package->wasDelivered() && !$package->isCancelled()) {
                if (isset($orderItems[$package->getPackageId()])) {
                    foreach ($orderItems[$package->getPackageId()] as $item) {
                        $oneTimeCharge = $packageModel->roundFloat($oneTimeCharge + $item->getItemFinalPriceInclTax());
                    }
                }
            }
        }
        if (count($oldPackages) && $this->hasParents()) {
            $oldItems = Mage::getModel('sales/order_item')->getCollection()
                ->join(array('order' => 'sales/order'), 'main_table.order_id = order.entity_id', array('edited', 'superorder_id'))
                ->addAttributeToFilter('package_id', array('in' => $oldPackages))
                ->addAttributeToFilter('edited', 0)
                ->addAttributeToFilter('superorder_id', $this->getParentId());
            foreach ($oldItems as $oldItem) {
                $oneTimeCharge = $packageModel->roundFloat($oneTimeCharge - $oldItem->getItemFinalPriceInclTax());
            }
        }

        return round($oneTimeCharge, 2);
    }
}
