<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Proxy_Helper_Data
 */
class Omnius_Proxy_Helper_Data
{
    /**
     * All modules found in this array will
     * be ignored by the proxy generating logic
     * @var array
     */
    protected $_excluded = array(
        'Omnius_Proxy',
    );

    /** @var array */
    protected $_annotations;

    /**
     * @return array
     */
    public function getModules()
    {
        $modules = array_keys((array) Mage::getConfig()->getNode('modules'));

        /**
         * Handle only Dyna_* modules
         */
        $modules = array_filter($modules, function($module) {
            return substr($module, 0, 5) === 'Dyna_';
        });

        return array_values(array_diff($modules, $this->_excluded));
    }

    /**
     * @return array
     */
    public function getAnnotations()
    {
        if (null === $this->_annotations) {

            $annotationsDir = Mage::getModuleDir(null, 'Omnius_Proxy') . DS . 'Model' . DS . 'Annotation';

            $iterator = new RecursiveDirectoryIterator($annotationsDir, FilesystemIterator::SKIP_DOTS);
            $iterator = new RecursiveIteratorIterator($iterator, RecursiveIteratorIterator::SELF_FIRST);

            $this->_annotations = array();
            /** @var SplFileInfo $file */
            foreach ($iterator as $file) {
                $annotation = preg_replace('/\.php$/', '', $file->getFilename());
                if ('AbstractAnnotation' !== $annotation) {
                    $this->_annotations[] = $annotation;
                }
            }
        }
        return $this->_annotations;
    }
}