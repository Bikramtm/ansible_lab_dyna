<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Proxy_Model_Annotation_IsGranted
 */
class Omnius_Proxy_Model_Annotation_IsGranted extends Omnius_Proxy_Model_Annotation_AbstractAnnotation
{
    /**
     * Executed before the method that has
     * assigned this annotation. If the
     * method returns any result that passes
     * the truth check (==), that result will be returned.
     * To allow the normal method to be executed,
     * return a value that validates as false.
     *
     * @throws DomainException
     * @return mixed
     */
    public function canProceed()
    {
        if ($this->getPermissions() && ($agent = $this->getAgent())) {
            if ( ! $agent->isGranted($this->getPermissions())) {
                if ($this->getIsController()) {
                    $request = Mage::app()->getRequest();
                    $response = Mage::app()->getResponse();
                    if ($request->isAjax()) {
                        $response->clearAllHeaders();
                        $response->clearBody();
                        $response
                            ->setHttpResponseCode(200)
                            ->setHeader('Content-Type', 'application/json')
                            ->setBody(Mage::helper('core')->jsonEncode(array(
                                'error' => true,
                                'message' => 'Access denied',
                            )));
                    } else {
                        if ($referer = $this->getReferer()) {
                            Mage::getSingleton('core/session')->addError(Mage::helper('core')->__('Not allowed'));
                            $response->setRedirect($referer);
                        }
                    }
                } else {
                    throw new DomainException('Not allowed');
                }

                return false;
            }
        }

        return true;
    }

    /**
     * @return Dyna_Agent_Model_Agent
     */
    protected function getAgent()
    {
        if (($agent = Mage::getSingleton('customer/session')->getAgent(true)) instanceof Dyna_Agent_Model_Agent) {
            return $agent;
        }
        return null;
    }

    /**
     * @return array
     */
    protected function getPermissions()
    {
        return is_array($this->getData('permissions')) ? $this->getData('permissions') : array();
    }
}