<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Proxy_Model_Writer
 */
class Omnius_Proxy_Model_Writer
{
    /**
     * @param $method
     * @param array $annotations
     * @param $proxy
     * @return string
     */
    public function getRewriteMethod($method, array $annotations, $proxy)
    {
        $methodDefinition = $this->getMethodDefinition($method);
        $methodDeclaration = join(PHP_EOL, array(PHP_EOL, "    " . $this->stripDocComment($method->getDocComment()), "    " . $methodDefinition));
        $annotationCalls = array();
        foreach ($annotations as $annotation) { /** @var Omnius_Proxy_Model_Annotation_AbstractAnnotation $annotation */
            $annotationCalls[] = $annotation->callString();
        }
        $annotationInit = trim(join(PHP_EOL, array_map(function($anno) { return $anno->initString(); }, $annotations)));
        $annotationCalls = join(' && ', array_map(function($anno) { return $anno->callString(); }, $annotations));
        $parentCall = sprintf('return parent::' . $method->getName() . '(%s);', join(', ', $method->getParameters()));
        $methodDeclaration .= <<<LOGIC

    {
        {$annotationInit}
        if ({$annotationCalls}) {
            {$parentCall}
        }
    }
LOGIC;
        return $methodDeclaration;
    }

    /**
     * @param Omnius_Proxy_Model_Proxy $proxy
     * @return string | null
     */
    public function rewriteClass(Omnius_Proxy_Model_Proxy $proxy)
    {
        if ($proxy->isController()) {
            return $this->rewriteController($proxy);
        }

        if ($proxy->hasMethods()) {
            $message = <<<GEN_MESSAGE
/**
 * @IMPORTANT!
 * This class was generated automatically.
 * Any manual change will be lost.
 * @IMPORTANT!
 *
 * Class {$proxy->getClass()}
 */

GEN_MESSAGE;
;
            return sprintf(
                "<?php\n%s%sclass %s%s\n{%s\n}\n",
                $message,
                ($proxy->getIsAbstract() ? 'abstract ' : ''),
                $proxy->getClass(),
                $proxy->isController() ? '' : sprintf(' extends %s', $proxy->getParent()),
                join(PHP_EOL, $proxy->getMethods())
            );
        }
        return null;
    }

    public function rewriteController(Omnius_Proxy_Model_Proxy $proxy)
    {
        if ($proxy->hasMethods()) {
            $body = join(PHP_EOL, $proxy->getMethods());
            $class = $proxy->getClass();
            $parent = $proxy->getParent();
            $parentPath = sprintf("Mage::getBaseDir() . '%s'", str_replace(Mage::getBaseDir(), '', $proxy->getParentPath()));
            $code = <<<CONTROLLER
<?php

/**
 * @IMPORTANT!
 * This class was generated automatically.
 * Any manual change will be lost.
 * @IMPORTANT!
 *
 * Class {$proxy->getClass()}
 */
require_once {$parentPath};
class {$class} extends {$parent}
{{$body}
}
CONTROLLER;
            ;
            return $code;
        }
        return null;
    }

    /**
     * @param ReflectionMethod $method
     * @return string
     */
    protected function getMethodDefinition($method)
    {
        $filename = $method->getFileName();
        $startLine = $method->getStartLine() - 1;
        $source = file($filename);
        $definition = array_slice($source, $startLine, 1);
        if (strpos($definition[0], '{')) {
            $parts = explode('{', $definition[0]);
            return trim($parts[0]);
        }
        return trim($definition[0]);
    }

    /**
     * @param ReflectionMethod $method
     * @return string
     */
    protected function getMethodBody($method)
    {
        $filename = $method->getFileName();
        $startLine = $method->getStartLine() - 1;
        $endLine = $method->getEndLine();
        $length = $endLine - $startLine;
        $source = file($filename);
        preg_match('/\{(.*)\}/', join(PHP_EOL, array_slice($source, $startLine, $length)), $body);
        return trim($body[1]);
    }

    /**
     * @param $docComment
     * @return string
     */
    protected function stripDocComment($docComment)
    {
        if ($annotations = Mage::helper('proxy')->getAnnotations()) {
            $pattern = sprintf('/(%s)/', join('|', array_map(function($ann) {
                return sprintf('\\@%s', $ann);
            }, $annotations)));
            $docLines = array();
            foreach (explode(PHP_EOL, $docComment) as $line) {
                if ( ! preg_match($pattern, $line)) {
                    $docLines[] = $line;
                }
            }
            return join(PHP_EOL, $docLines);
        }
        return $docComment;
    }
}