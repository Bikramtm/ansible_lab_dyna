<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Proxy_Model_Parser
 */
class Omnius_Proxy_Model_Parser
{
    const SCAN = 1;
    const SKIP = 2;
    const NAME = 3;
    const COPY_LINE = 4;
    const COPY_ARRAY = 5;

    /** @var */
    protected $supported = array();

    /**
     * @param array $annotations
     */
    public function addSupportedAnnotations(array $annotations = array())
    {
        $this->supported = $annotations;
    }

    /**
     * @param $docBlock
     * @return array
     * @throws Exception
     */
    public function parseAnnotation($docBlock)
    {
        $str = trim(preg_replace('/^[\/\*\# \t]+/m', '', $docBlock)) . "\n";
        $str = str_replace("\r\n", "\n", $str);

        $state = self::SCAN;
        $nesting = 0;
        $name = '';
        $value = '';

        $matches = array();

        for ($i = 0; $i < strlen($str); $i++) {
            $char = substr($str, $i, 1);

            switch ($state) {
                case self::SCAN:
                    if ($char == '@') {
                        $name = '';
                        $value = '';
                        $state = self::NAME;
                    } elseif ($char != "\n" && $char != " " && $char != "\t") {
                        $state = self::SKIP;
                    }
                    break;

                case self::SKIP:
                    if ($char == "\n") {
                        $state = self::SCAN;
                    }
                    break;

                case self::NAME:
                    if (preg_match('/[a-zA-Z\-\\\\]/', $char)) {
                        $name .= $char;
                    } elseif ($char == ' ') {
                        $state = self::COPY_LINE;
                    } elseif ($char == '(') {
                        $nesting++;
                        $value = $char;
                        $state = self::COPY_ARRAY;
                    } elseif ($char == "\n") {
                        $matches[] = array($name, null);
                        $state = self::SCAN;
                    } else {
                        $state = self::SKIP;
                    }
                    break;

                case self::COPY_LINE:
                    if ($char == "\n") {
                        $matches[] = array($name, $value);
                        $state = self::SCAN;
                    } else {
                        $value .= $char;
                    }
                    break;

                case self::COPY_ARRAY:
                    if ($char == '(') {
                        $nesting++;
                    }
                    if ($char == ')') {
                        $nesting--;
                    }

                    $value .= $char;

                    if ($nesting == 0) {
                        $matches[] = array($name, $value);
                        $state = self::SCAN;
                    }
            }
        }

        $annotations = array();

        foreach ($matches as $match) {
            $name = $match[0];

            if ( ! $this->isSupported($name)) {
                continue;
            }

            $type = sprintf('Omnius_Proxy_Model_Annotation_%s', $name);

            if (!class_exists($type, true)) {
                Mage::log(sprintf('No custom class for annotation "%s" was not found.', $type), null, 'annotations.log', true);
                continue; //simply ignore not found annotations
            }

            preg_match('/\((.*)\)/', $match[1], $jsonStr);

            if (isset($jsonStr[1])) {
                $value = $jsonStr[1];
            } else {
                $value = '{}';
            }

            if (!array_key_exists('Omnius_Proxy_Model_Annotation_AbstractAnnotation', class_parents($type, true))) {
                throw new Exception("The {$type} Annotation does not extend the Omnius_Proxy_Model_Annotation_AbstractAnnotation abstract class.");
            }

            try {
                $parameters = json_decode($value, true);
                $annotations[] = new $type($parameters ? $parameters : array());
            } catch (Exception $e) {
                Mage::getSingleton('core/logger')->logException($e);
            }
        }

        return $annotations;
    }

    /**
     * @param $annotation
     * @return bool
     */
    protected function isSupported($annotation)
    {
        return in_array($annotation, $this->supported);
    }
}