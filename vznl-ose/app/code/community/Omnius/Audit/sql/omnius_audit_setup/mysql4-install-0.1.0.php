<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;

/**
 * Create table 'default_field_sections'
 */
$tableName = $installer->getTable('audit/snapshot');
if ( ! $installer->getConnection()->isTableExists($tableName)) {
    $table = $installer->getConnection()
        ->newTable($tableName)
        ->addColumn('item_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array('identity'  => true,'unsigned'  => true,'nullable'  => false,'primary'   => true), 'ID')
        ->addColumn('hash', Varien_Db_Ddl_Table::TYPE_VARCHAR, 50)
        ->addColumn('object_resource', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255)
        ->addColumn('supported_fields', Varien_Db_Ddl_Table::TYPE_BLOB)
        ->addColumn('class', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255)
        ->addColumn('object_id', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255)
        ->addColumn('state', Varien_Db_Ddl_Table::TYPE_BLOB)
        ->addColumn('timestamp', Varien_Db_Ddl_Table::TYPE_DATETIME)
        ->setOption('charset', 'utf8');

    $installer->getConnection()->createTable($table);
    $installer->run(sprintf('ALTER TABLE `%s` CHANGE `state` `state` LONGBLOB NOT NULL;', $tableName));
    $installer->run(sprintf('ALTER TABLE `%s` CHANGE `supported_fields` `supported_fields` LONGBLOB NOT NULL;', $tableName));
}

$installer->endSetup();