<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Interface Omnius_Audit_Helper_DiffInterface
 */
interface Omnius_Audit_Helper_DiffInterface
{
    /**
     * @param $origData
     * @param $currentData
     * @return mixed
     */
    public function diff($origData, $currentData);
} 