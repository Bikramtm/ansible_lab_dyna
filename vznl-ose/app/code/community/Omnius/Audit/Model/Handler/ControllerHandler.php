<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Audit_Model_Handler_ControllerHandler
 */
class Omnius_Audit_Model_Handler_ControllerHandler implements Omnius_Audit_Model_Handler_InterfaceHandler
{
    use Omnius_Audit_Model_Handler_TraitHandler;
    //list of actions for which the response should be logged in the audit
    protected $addResponse = [
    ];

    /**
     * @param Varien_Object $payload
     * @param string $actionType
     * @return mixed
     */
    public function handle(Varien_Object $payload, $actionType = null)
    {
        /** @var Mage_Core_Controller_Varien_Action $controller */
        $controller = $payload->getData('controller_action');
        $config = $this->getConfig();

        if ((int) $config->getValue('active')
            && ( ($config->isWhitelistMode() && in_array($controller->getFullActionName(), $config->getControllersList()))
                || ($config->isBlacklistMode() && !in_array($controller->getFullActionName(), $config->getControllersList()))
            )
        ) {

            /** @var Omnius_Audit_Model_Event $event */
            $event = Mage::getModel('audit/event');
            $admin = Mage::getSingleton('admin/session', ['name' => 'adminhtml'])->getUser();
            $event->setAdminId($admin ? $admin->getId() : 'null');
            $event->setAdminUsername($admin ? $admin->getUsername() : 'null');
            $event->setAction($controller->getFullActionName());
            $event->setActionType($actionType);

            // check if we should log the execution time
            $boolLogTime = (in_array($controller->getFullActionName(), $config->getLogTimeActions()));

            if ($actionType == 'predispath') {
                $this->preDispatchActions($boolLogTime);
            }
            $params = $actionType == 'predispath' ? Mage::helper('audit')->censorData($controller->getRequest()->getParams()) : [];

            //for certain actions, directly add the response in the postdispatch as it is more useful
            if ($actionType == 'postdispath' && in_array($event->getAction(), $this->addResponse)) {
                $params['body'] = $controller->getResponse()->getBody();
            }

            $reqUniqueId = Mage::registry('req_unique_id');
            $params['req_unique_id'] = $reqUniqueId;

            if ($actionType == 'postdispath') {
                $params = $this->postDispatchActions($boolLogTime, $reqUniqueId, $params);
            }
            $event->setParams($params ? json_encode($params) : 'null');
            $event->setIsController(true);
            $event->setDumper(lcfirst($config->getValue('dumper')));

            $this->getControllerLogger()->log($event);
        }
    }

    /**
     * @param $boolLogTime
     * @param $reqUniqueId
     * @param $params
     * @return mixed
     */
    protected function postDispatchActions($boolLogTime, $reqUniqueId, $params)
    {
        Mage::unregister('req_unique_id');

        // calculate the execution time and add it to the post-dispatch params
        if ($boolLogTime && ($timeEnd = Mage::registry('start_' . $reqUniqueId))) {
            $timeDiff = round((microtime(true) - $timeEnd), 6);
            $params['req_time_diff'] = $timeDiff;
            Mage::unregister('start_' . $reqUniqueId);
        }

        return $params;
    }

    /**
     * @param $boolLogTime
     */
    protected function preDispatchActions($boolLogTime)
    {
        Mage::unregister('req_unique_id');//make sure it's not yet set
        $unqId = uniqid(mt_rand(), true);
        Mage::register('req_unique_id', $unqId);
        if ($boolLogTime) {
            Mage::register('start_' . $unqId, microtime(true));
        }
    }

    /**
     * @param Mage_Core_Controller_Response_Http $response
     * @return bool|mixed
     */
    protected function extractJsonBody(Mage_Core_Controller_Response_Http $response)
    {
        $header = array_filter($response->getHeaders(), function ($el) {
            if ($el['name'] == 'Content-Type' && (false !== strpos($el['value'], 'application/json'))) {
                return $el;
            }

            return false;
        });
        if (count($header)) { //response is JSON
            return json_decode($response->getBody(), true);
        }

        return false;
    }
}