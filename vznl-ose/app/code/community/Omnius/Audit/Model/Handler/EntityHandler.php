<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Audit_Model_Handler_EntityHandler
 */
class Omnius_Audit_Model_Handler_EntityHandler implements Omnius_Audit_Model_Handler_InterfaceHandler
{
    use Omnius_Audit_Model_Handler_TraitHandler;

    /**
     * @param Varien_Object $payload
     * @param string $action
     * @return mixed
     */
    public function handle(Varien_Object $payload, $action = 'save')
    {
        $config = $this->getConfig();
        $object = $payload->getData('object');
        if ((int) $config->getValue('active')
            && ($object instanceof Varien_Object)
            && $this->supportsEntity($object)
        ) {
            /** @var Omnius_Audit_Model_Event $event */
            $event = Mage::getModel('audit/event');
            $object = $payload->getData('object');
            $admin = Mage::getSingleton('admin/session', ['name' => 'adminhtml'])->getUser();
            $event->setAdminId($admin ? $admin->getId() : 'null');
            $event->setAdminUsername($admin ? $admin->getUsername() : 'null');
            $event->setObjectAction($action);
            $event->setObject($object);
            $event->setDumper(lcfirst($config->getValue('dumper')));
            $event->setIsController(false);
            $event->setChanges(Mage::helper('audit')->censorData($event->getChanges()));

            $this->getEntityLogger()->log($event);
        }
    }

    /**
     * @param $object
     * @return bool
     */
    protected function supportsEntity($object)
    {
        $allowedClasses = $this->getConfig()->getAllowedEntities();

        return 0 < count(array_filter(array_map(function ($cl) use ($object) {
            if ($object instanceof $cl || in_array($cl, class_parents($object))) {
                return $cl;
            }

            return false;
        }, $allowedClasses)));
    }
}
