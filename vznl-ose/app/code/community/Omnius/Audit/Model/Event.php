<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Audit_Model_Event
 *
 * @method DateTime getTime
 * @method Varien_Object getObject
 * @method Omnius_Audit_Model_Event setObject
 * @method array getAdditional
 * @method string getDumper
 * @method Omnius_Audit_Model_Event setAgent
 * @method Omnius_Audit_Model_Event setObjectAction
 * @method Omnius_Audit_Model_Event setAdminId
 * @method Omnius_Audit_Model_Event setAdminUsername
 * @method Omnius_Audit_Model_Event setDumper
 * @method Omnius_Audit_Model_Event setIsController
 * @method string getAdminId
 * @method string getAdminUsername
 */
class Omnius_Audit_Model_Event extends Mage_Core_Model_Abstract
{
    const CONTROLLER_EVENT = 'controller';
    const ENTITY_EVENT = 'entity';

    /** @var bool  */
    protected $_convertedFlag = false;

    /** @var array|null */
    protected $_diff = null;

    /**
     * Return changes made to event
     * 
     * @return array
     */
    public function getChanges()
    {
        if (is_null($this->_diff)) {
            /**
             * Because certain actions on the backend apply changes
             * by creating a new object and setting the data on it
             * and saving it, we do not have access to the data from
             * before the change without reloading the object.
             */
            return $this->_diff = Mage::helper('audit')->getDiff($this->getObject());
        }
        return $this->_diff;
    }

    /**
     * @param $changes
     * @return $this
     */
    public function setChanges($changes)
    {
        $this->_diff = $changes;
        return $this;
    }

    /**
     * @param $data
     * @return $this
     */
    public function setFormatted($data)
    {
        $this->setData('formatted', $data);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFormatted()
    {
        return $this->getData('formatted');
    }

    /**
     * @param $flag
     * @return $this
     */
    public function setIsConverted($flag)
    {
        $this->_convertedFlag = (bool) $flag;
        return $this;
    }

    /**
     * @return bool
     */
    public function isConverted()
    {
        return true === $this->_convertedFlag;
    }

    /**
     * @return bool
     */
    public function isControllerEvent()
    {
        return $this->getType() == self::CONTROLLER_EVENT;
    }

    /**
     * @return bool
     */
    public function isEntityEvent()
    {
        return $this->getType() == self::ENTITY_EVENT;
    }

    /**
     * Before event save
     */
    public function beforeSave()
    {
        if (!$this->hasTime()) {
            $this->setTime(new DateTime('now', new DateTimeZone('UTC')));
        }
    }
}