<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Audit_Model_Extractor
 */
class Omnius_Audit_Model_Extractor
{
    /** @var Omnius_Audit_Model_Preparer */
    protected $_preparer;

    /**
     * Extract items
     *
     * @param $data
     * @param array $fields
     * @return array
     */
    public function extract($data, $fields = array())
    {
        if ($this->_getPreparer()->supports($data)) {
            $data = $this->_getPreparer()->prepare($data);
        }

        $items = array();
        if (is_null($data) || is_scalar($data)) {
            return $data;
        } elseif (is_array($data)) {
            foreach ($data as $key => &$value) {
                if ($this->_isAllowed($key, $fields)) {
                    $items[$key] = $this->extract($value);
                }
            }
        } elseif ($data instanceof Varien_Data_Collection) {
            $items['_definition'] = array(
                '_class' => get_class($data),
            );
            foreach ($data->getItems() as $key => $value) {
                if ($this->_isAllowed($key, $fields)) {
                    $items[$key] = $this->extract($value);
                }
            }
        } elseif ($data instanceof Varien_Object) {
            $items['_definition'] = array(
                '_class' => get_class($data),
            );
            foreach ($data->getData() as $key => $value) {
                if ($this->_isAllowed($key, $fields)) {
                    $items[$key] = $this->extract($value);
                }
            }
        } elseif ($data instanceof SimpleXMLElement) {
            $items['_definition'] = array(
                '_class' => 'SimpleXMLElement',
                '_xml' => $data->asXml(),
            );
        } elseif (is_object($data)) {
            foreach (get_object_vars($data) as $key => $value) {
                if ($this->_isAllowed($key, $fields)) {
                    $items[$key] = $this->extract($value);
                }
            }
        }
        return $items;
    }

    /**
     * Check if field is allowed to be extracted
     *
     * @param $key
     * @param $fields
     * @return bool
     */
    protected function _isAllowed($key, $fields)
    {
        return ($fields && is_array($fields)) ? in_array($key, $fields) : true;
    }

    /**
     * @return Omnius_Audit_Model_Preparer
     */
    protected function _getPreparer()
    {
        if ( ! $this->_preparer) {
            return $this->_preparer = Mage::getSingleton('audit/preparer');
        }
        return $this->_preparer;
    }
} 