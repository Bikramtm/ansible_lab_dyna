<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Audit_Model_Dumper_File
 */
class Omnius_Audit_Model_Dumper_File extends Omnius_Audit_Model_Dumper_AbstractDumper
{
    /**
     * @param Omnius_Audit_Model_Event $event
     * @return mixed|void
     * @throws Exception
     */
    public function dump(Omnius_Audit_Model_Event $event)
    {
        $section = (Mage::app()->getStore()->isAdmin() || Mage::getDesign()->getArea() == 'adminhtml') ? 'backend' : 'frontend';

        // Do not log GET requests to the admin login page
        if ($section == 'backend' && $event->getAction() == 'adminhtml_index_index' && ! Mage::app()->getRequest()->isPost()) {
            return;
        }

        $event->setData('section', $section);
        $data = trim($this->extractData($event), PHP_EOL);

        $filename = $this->createFilename(sprintf('audit_%s_%s', $section, $event->getIsController() ? 'controller' : 'entity'));
        if ($filename && $data && ! @file_put_contents($filename, $data . PHP_EOL, FILE_APPEND)) {
            throw new Exception(sprintf('Could not write data to log file at "%s"', $filename));
        }
    }

    /**
     * @param Omnius_Audit_Model_Event $event
     * @return string
     */
    public function extractData(Omnius_Audit_Model_Event $event)
    {
        if ( ! $event->getTime()) {
            $event->setTime(new DateTime('now', new DateTimeZone('UTC')));
        }
        if ($event->getIsController()) {
            //Timestamp(neutral)|Website_code|Agent_id|Dealer_id|Dealer_code|Axi_store_code|Action|Data(if applicable)
            $data = array(
                $event->getTime()->format('Y-m-d H:i:s'),
                $event->getSection() == 'backend' ? $event->getAdminUsername() : Mage::app()->getWebsite()->getCode(),
                $event->getSection() == 'backend' ? $event->getAdminId() : $event->getAgentId(),
                $event->getDealerId(),
                $event->getDealerCode(),
                $event->getAxiStoreCode(),
                $event->getAction(),
                $event->getParams(),
                $event->getActionType(),
            );
        } else {
            //Timestamp(neutral)|User_id|Action|Data(if applicable)
            $changes = $event->getChanges();
            $data = array(
                $event->getTime()->format('Y-m-d H:i:s'),
                $event->getAdminUsername(),
                $event->getAdminId(),
                $event->getObjectAction(),
                strtolower(get_class($event->getObject())),
                $event->getObject()->getId() ?: 'null',
                ($changes && isset($changes['has_changes'])) ? json_encode($changes) : 'null',
            );
        }

        // Do not log fields that do not have any changes
        if (isset($changes) && ! isset($changes['has_changes'])) {
            return false;
        }

        return join('|', $data);
    }

    /**
     * @param $baseName
     * @return string
     */
    public function generateFilename($baseName)
    {
        return join(DS, array(Mage::getBaseDir('var'), 'log', 'security' . DS . 'audit', $baseName . '_' . date("dmY") . '.log'));
    }

    /**
     * @param $baseName
     * @return string
     * @throws Exception
     */
    public function createFilename($baseName)
    {
        $filename = $this->generateFilename($baseName);
        if (!(is_file($filename) || is_writeable($filename))) {
            if (!realpath($filename)) {
                $file = new Varien_Io_File();
                $file->setAllowCreateFolders(true);
                $file->createDestinationDir(dirname($filename));
                unset($file);
                if (false == @touch($filename)) {
                    Mage::log(sprintf("Could not create log file at path '%s'. Please check file permission.", $filename), null, 'audit_file_exceptions.log', true);
                    $filename = false;
                }
            } elseif (realpath($filename) && is_dir($filename)) {
                throw new Exception(sprintf("Expected '%s' to be a file, directory found.", $filename));
            } else {
                //log but prevent throwing exception as it will often occur due to the cron log merging script
                Mage::log(sprintf("Write permissions for '%s' file not given.", $filename), null, 'audit_file_exceptions.log', true);
                $filename = false;
            }
        }

        return $filename;
    }
}