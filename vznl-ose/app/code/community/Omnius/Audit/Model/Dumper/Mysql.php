<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Audit_Model_Dumper_File
 */
class Omnius_Audit_Model_Dumper_Mysql extends Omnius_Audit_Model_Dumper_AbstractDumper
{
    /**
     * @param Omnius_Audit_Model_Event $event
     * @return mixed|void
     * @throws Exception
     */
    public function dump(Omnius_Audit_Model_Event $event)
    {
        throw new Exception('MySQL strategy not implemented');
    }
}