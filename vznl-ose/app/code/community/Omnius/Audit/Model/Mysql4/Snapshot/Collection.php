<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Audit_Model_Mysql4_Snapshot_Collection
 */
class Omnius_Audit_Model_Mysql4_Snapshot_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        $this->_init('audit/snapshot');
    }

    /**
     * Delete all elements in collection
     *
     * @return $this
     */
    public function delete()
    {
        foreach ($this->getItems() as $item) {
            $item->delete();
            unset($this->_items[$item->getId()]);
        }
        return $this;
    }
}