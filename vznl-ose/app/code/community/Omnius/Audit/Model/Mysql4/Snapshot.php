<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Audit_Model_Mysql4_Snapshot
 */
class Omnius_Audit_Model_Mysql4_Snapshot extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init('audit/snapshot', 'item_id');
    }
}