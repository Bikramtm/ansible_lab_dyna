<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Audit_Model_Reverter
 */
class Omnius_Audit_Model_Reverter
{
    /** @var Omnius_Audit_Model_Extractor */
    protected $_extractor;

    /** @var Varien_Db_Adapter_Interface */
    protected $_connection;

    /** @var Omnius_Audit_Helper_Data */
    protected $_helper;

    protected $_skippedKeys = array(
        'created_at',
    );

    /**
     * @param $object
     * @param Omnius_Audit_Model_Snapshot|string|null $snapshot
     * @return mixed
     */
    public function revert(Varien_Object $object, $snapshot = null)
    {
        $snapshot = ($snapshot instanceof Omnius_Audit_Model_Snapshot)
            ? $snapshot
            : $this->retrieveSnapshot($object, $snapshot);
        if ( ! $snapshot->getId()) {
            throw new RuntimeException('No snapshot found for the given object');
        }

        return $this->_build($snapshot->getState(), $object);
    }

    /**
     * @param $state
     * @param $object
     * @return Mage_Core_Model_Abstract
     * @throws Exception
     */
    protected function _build($state, $object)
    {
        if ( ! is_array($state)) {
            return $state;
        }

        if (isset($state['_definition'])) {
            $class = $state['_definition']['_class'];
            $xml = isset($state['_definition']['_xml']) ? $state['_definition']['_xml'] : null;
            unset($state['_definition']);

            if ($class === 'SimpleXMLElement') {
                $root = new $class($xml);
                unset($xml);
            } else {
                $root = new $class;
                /**
                 * If $root is a collection, we must make sure
                 * that the _isCollectionLoaded flag is set to
                 * true so that it will not try to load all
                 * existing items within the db when calling getItems
                 */
                if ($root instanceof Varien_Data_Collection) {
                    $refObject = new ReflectionObject($root);
                    try {
                        $refProperty = $refObject->getProperty('_isCollectionLoaded');
                        $refProperty->setAccessible(true);
                        $refProperty->setValue($root, true);
                        unset($refObject, $refProperty);
                    } catch (Exception $e) {
                        //no-op as the collection does not have a _isCollectionLoaded property/flag
                    }
                }
            }

        } else {
            $root = $object;
        }

        /**
         * If $root is a model and we have its ID, try to refresh the data (item may not be in the DB)
         */
        if ($root instanceof Mage_Core_Model_Abstract && isset($state[$root->getIdFieldname()])) {
            $root->load($state[$root->getIdFieldname()]);
        }

        $hasChanges = false;
        foreach ($state as $key => &$value) {
            if ($root instanceof Varien_Object) {
                $_oldValue = (get_class($root) === get_class($object)) ? $object->getData($key) : $root->getData($key);
                $_newValue = $this->_build($value, $root);
                $hasChanges = $this->_buildVarienObject($hasChanges, $key, $_oldValue, $_newValue, $root);
                $root->setData($key, $_newValue);
            } elseif ($root instanceof Varien_Data_Collection) {
                $_item = $this->_build($value, $root);
                $this->_buildCollection($_item, $root);
            }
        }

        /**
         * Only trigger save if the item has changes to reduce execution time and load
         */
        if ($root instanceof Varien_Object && method_exists($root, 'save') && $hasChanges) {
            $root->save();
        }

        return $root;
    }

    /**
     * Verifies if the old value is different from the new value
     *
     * @param $key
     * @param $oldValue
     * @param $newValue
     * @return bool
     */
    protected function _hasChanges($key, $oldValue, $newValue)
    {
        /**
         * Skip custom properties (__getSomePropertyThatOtherwiseIsAbsentFomTheObject),
         * special keys (self::_skippedKeys) and numeric keys
         */
        if (0 === strpos($key, '__') || in_array($key, $this->_skippedKeys) || is_numeric($key)) {
            return false;
        }

       
        return $this->_compareValues($oldValue, $newValue);
    }

    /**
     * @param $entity
     * @param $snapshot
     * @return mixed
     */
    public function retrieveSnapshot($entity, $snapshot)
    {
        if ( ! $entity->getId()) {
            throw new RuntimeException('Given entity does not have an ID');
        }

        /** @var Omnius_Audit_Model_Mysql4_Snapshot_Collection $coll */
        $coll = Mage::getResourceModel('audit/snapshot_collection');
        $coll
            ->addFieldToFilter('class', get_class($entity))
            ->addOrder('timestamp', Varien_Data_Collection::SORT_ORDER_ASC);

        if ($snapshot) {
            $coll->addFieldToFilter('hash', $snapshot);
        } else {
            $coll->addFieldToFilter('object_id', $entity->getId());
        }

        $item = $coll->getFirstItem(); //get the first ever created snapshot of the entity
        unset($coll);

        return $item;
    }

    /**
     * @return Omnius_Audit_Model_Extractor
     */
    protected function _getExtractor()
    {
        if ( ! $this->_extractor) {
            return $this->_extractor = Mage::getSingleton('audit/extractor');
        }
        return $this->_extractor;
    }

    /**
     * @return Varien_Db_Adapter_Interface
     */
    protected function _getConn()
    {
        if ( ! $this->_connection) {
            return $this->_connection = Mage::getSingleton('core/resource')->getConnection('core_write');
        }
        return $this->_connection;
    }

    /**
     * @return Omnius_Audit_Helper_Data
     */
    protected function _getHelper()
    {
        if ( ! $this->_helper) {
            return $this->_helper = Mage::helper('audit');
        }
        return $this->_helper;
    }

    /**
     * Check if objects have the same values
     *
     * @param $oldValue
     * @param $newValue
     * @return bool
     */
    protected function _compareObjects($oldValue, $newValue)
    {
        if (get_class($oldValue) !== get_class($newValue)) {
            $result = true;

        } elseif ($oldValue instanceof Varien_Data_Collection || $oldValue instanceof Varien_Object) {
            $result = $oldValue->toArray() !== $newValue->toArray();
        } else {
            $result = $oldValue !== $newValue;
        }

        return $result;
    }

    /**
     * Check if values are the same
     *
     * @param $oldValue
     * @param $newValue
     * @return bool
     */
    protected function _compareValues($oldValue, $newValue)
    {
        if (is_numeric($oldValue) && is_numeric($newValue)) {
            /**
             * Special case when value is returned as float
             * instead of string (ex: "234.3000" vs 234.3)
             */
            $result = floatval($oldValue) !== floatval($newValue);
        } elseif (gettype($oldValue) !== gettype($newValue)) {
            $result = true;
        } elseif (is_object($oldValue) && is_object($newValue)) {
            $result = $this->_compareObjects($oldValue, $newValue);
        } else {
            $result = $oldValue !== $newValue;
        }

        return $result;
    }

    /**
     * @param $hasChanges
     * @param $key
     * @param $_oldValue
     * @param $_newValue
     * @param $root
     * @return array
     */
    protected function _buildVarienObject($hasChanges, $key, $_oldValue, $_newValue, $root)
    {
        /**
         * We need to determine if a save is needed. In this way, we reduce the save calls
         */
        if (false === $hasChanges && $this->_hasChanges($key, $_oldValue, $_newValue)) {
            $hasChanges = true;
        }

        /**
         * If we can retrieve the initial collection items, delete
         * the items that were added in the meantime to bring the
         * collection the the initial set of items
         */
        $_getterKey = str_replace('__', '', $key);
        if (0 !== strpos(strtolower($_getterKey), 'get')) {
            $_getterKey = 'get' . ucfirst($_getterKey);
        }
        if ($_newValue instanceof Varien_Data_Collection && method_exists($root, $_getterKey)) {
            $_initialIds = array_map(function ($item) {
                return $item->getId();
            }, $root->{$_getterKey}()->getItems());
            $_newIds = array_map(function ($item) {
                return $item->getId();
            }, $_newValue->getItems());
            if ($_toRemove = array_map('intval', array_diff($_initialIds, $_newIds))) {
                $_firstItem = $_newValue->getFirstItem();
                if ($_firstItem->getResource()) {
                    $_sql = sprintf(
                        'DELETE FROM `%s` WHERE %s IN (%s)',
                        $_firstItem->getResource()->getMainTable(),
                        $_firstItem->getIdFieldName(),
                        join(',', $_toRemove)
                    );
                    $this->_getConn()->query($_sql);
                    unset($_sql);
                }
            }
            unset($_getterKey, $_initialIds, $_newIds, $_toRemove);
        }

        return $hasChanges;
    }

    /**
     * @param $_item
     * @param $root
     */
    protected function _buildCollection($_item, $root)
    {
        if (method_exists($_item, 'save') && $_item->getResource()) {
            /**
             * We need to check if the item was deleted from the
             * database in the meanwhile. If yes, we need to reset
             * its ID to null so that the save can work
             */
            $_sql = 'SELECT * FROM ' . $_item->getResource()->getMainTable() . ' WHERE :column=:value;';
            $binds = array(
                ':column' => $_item->getIdFieldName(),
                ':value' => $_item->getId()
            );

            if ($_item->getId() && false == $this->_getConn()->fetchOne($_sql, $binds)) { //does not exist anymore
                $_item->unsetData($_item->getIdFieldName());
            } else { //already present in the db
                //no-op
            }
            $root->addItem($_item->save());
        } elseif (method_exists($_item, 'save')) { //item has save method but no resource
            $root->addItem($_item->save());
        } else { //item is a plain Varien_Object
            $root->addItem($_item);
        }
    }
}
