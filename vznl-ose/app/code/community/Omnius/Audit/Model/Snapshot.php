<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Audit_Model_Snapshot
 *
 * @method string getObjectResource()
 * @method Omnius_Audit_Model_Snapshot setObjectResource()
 * @method string getClass()
 * @method Omnius_Audit_Model_Snapshot setClass()
 * @method string getObjectId()
 * @method Omnius_Audit_Model_Snapshot setObjectId()
 * @method string getHash()
 */
class Omnius_Audit_Model_Snapshot extends Mage_Core_Model_Abstract
{

    protected function _construct()
    {
        $this->_init('audit/snapshot');
    }

    /**
     * @param $hash
     * @return null
     */
    public function loadByHash($hash)
    {
        return Mage::getResourceModel('audit/snapshot_collection')
            ->addFieldToFilter('hash', $hash)
            ->getLastItem();
    }

    /**
     * @param array $fields
     * @return $this
     */
    public function setSupportedFields($fields = array())
    {
        $this->setData('supported_fields', implode(',', $fields));
        return $this;
    }

    /**
     * @return array
     */
    public function getSupportedFields()
    {
        return array_map('trim', array_filter(explode(',', $this->getData('supported_fields'))));
    }

    /**
     * @param $state
     * @return $this
     */
    public function setState($state)
    {
        if (is_array($state)) {
            $this->setData('state', serialize($state));
        } elseif (is_string($state)) {
            $this->setData('state', $state);
        } else {
            throw new InvalidArgumentException(sprintf(
                'Expected array or string, got instead %s',
                (is_object($state)
                    ? sprintf('object of type %s', get_class($state))
                    : gettype($state))
            ));
        }
        return $this;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        $result = @unserialize($this->getData('state'));
        if ($result) {
            return $result;
        } else {
            Mage::log(sprintf('Invalid state found for snapshot #%s', $this->getId()));
            return array();
        }
    }

    /**
     * @return $this|Mage_Core_Model_Abstract
     * @throws Exception
     */
    public function save()
    {
        if ( ! $this->getId()) {
            $this->setData('timestamp', now());
        }

        if ( ! $this->getObjectId()) {
            $this->setData('object_id', sha1(time() . now() . rand(0, 1000)));
        }

        if ( ! $this->getHash()) {
            $this->setData('hash', sha1(sprintf('%s::%s', $this->getClass(), $this->getObjectId())));
        }

        return parent::save();
    }
}