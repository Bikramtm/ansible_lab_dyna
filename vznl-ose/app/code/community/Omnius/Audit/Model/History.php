<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Audit_ModelHistory
 */
class Omnius_Audit_Model_History
{
    /** @var Omnius_Audit_Model_Config */
    protected $_config;

    /** @var Omnius_Audit_Model_Extractor */
    protected $_extractor;

    /** @var Omnius_Audit_Model_Reverter */
    protected $_reverter;

    /**
     * @param $new
     * @param $old
     * @return array
     */
    public function compare($new, $old)
    {
        return Mage::helper('audit')->getDiff($new, $old, false);
    }

    /**
     * Create and return snapshot
     *
     * @param Varien_Object $entity
     * @param null $snapshotId
     * @return Omnius_Audit_Model_Snapshot
     */
    public function snapshot(Varien_Object $entity, $snapshotId = null)
    {
        /** @var Omnius_Audit_Model_Snapshot $snapshot */
        $snapshot = Mage::getModel('audit/snapshot');
        if (method_exists($entity, 'getResourceName')) {
            $snapshot->setObjectResource($entity->getResourceName());
            $snapshot->setSupportedFields($this->_getSupportedFields($snapshot));
        }

        $snapshot->setClass(get_class($entity));
        $snapshot->setState($this->_getExtractor()->extract($entity, $snapshot->getSupportedFields()));
        $snapshot->setObjectId($snapshotId ? $snapshotId : $entity->getId());

        $snapshot->save(); //persist

        return $snapshot;
    }

    /**
     * If no snapshot ID or instance is given, it will try
     * to revert the entity to the last known preserve state
     *
     * @param $entity
     * @param Omnius_Audit_Model_Snapshot|string|null $snapshot
     * @return Omnius_Audit_Model_Snapshot
     */
    public function revert($entity, $snapshot = null)
    {
        return $this->_getReverter()->revert($entity, $snapshot);
    }

    /**
     * Get snapshot
     *
     * @param $entity
     * @return mixed
     */
    public function getSnapshot($entity)
    {
        return $this->_getReverter()->retrieveSnapshot($entity, null);
    }

    /**
     * Get supported fields
     *
     * @param Omnius_Audit_Model_Snapshot $snapshot
     * @return array
     */
    protected function _getSupportedFields(Omnius_Audit_Model_Snapshot $snapshot)
    {
        if ($resourceName = $snapshot->getObjectResource()) {
            list($module, $class) = explode('/', $resourceName);
            if (($node = $this->_getConfig()->getNode(sprintf('entities/%s/%s', $module, $class))) && is_array($nodeArray = $node->asArray())) {
                return array_keys($nodeArray);
            }
            unset($resourceName, $module, $class);
        }

        return array();
    }

    /**
     * Get extractor object
     *
     * @return Omnius_Audit_Model_Extractor
     */
    protected function _getExtractor()
    {
        if ( ! $this->_extractor) {
            return $this->_extractor = Mage::getSingleton('audit/extractor');
        }
        return $this->_extractor;
    }

    /**
     * Get reverter object
     *
     * @return Omnius_Audit_Model_Reverter
     */
    protected function _getReverter()
    {
        if ( ! $this->_reverter) {
            return $this->_reverter = Mage::getSingleton('audit/reverter');
        }
        return $this->_reverter;
    }

    /**
     * Returns the snapshot config
     *
     * @return Omnius_Audit_Model_Config
     */
    protected function _getConfig()
    {
        /** @var Omnius_Audit_Model_Config $config */
        if (!$this->_config) {
            if (Mage::app()->useCache('config')) {
                $cacheConfig = Mage::app()->loadCache(Omnius_Audit_Model_SnapshotConfig::CONFIG_CACHE_KEY);
            } else {
                $cacheConfig = false;
            }

            if ($cacheConfig) {
                $config = Mage::getSingleton('audit/snapshotConfig');
                $config->loadString($cacheConfig);
                $this->_config = $config;
            } else {
                $config = Mage::getSingleton('audit/snapshotConfig');
                $config->loadString(Mage::getConfig()->loadModulesConfiguration('snapshot.xml')->getXmlString());
                if (Mage::app()->useCache('config')) {
                    Mage::app()->saveCache($config->getXmlString(), Omnius_Audit_Model_SnapshotConfig::CONFIG_CACHE_KEY, array(Mage_Core_Model_Config::CACHE_TAG));
                }
                $this->_config = $config;
            }
        }
        return $this->_config;
    }
} 