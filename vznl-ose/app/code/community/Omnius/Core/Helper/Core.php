<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Core_Helper_Core
 */
class Omnius_Core_Helper_Core extends Mage_Core_Helper_Data
{
    // PHP 7 backward compatibility fix
    public function jsonDecode($encodedValue, $objectDecodeType = Zend_Json::TYPE_ARRAY)
    {
        if (empty($encodedValue)) {
            return null;
        }

        return parent::jsonDecode($encodedValue, $objectDecodeType);
    }
}
