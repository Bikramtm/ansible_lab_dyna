<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();

$attribute = array(
    'comment' => 'Device subscription SKU',
    'type' => Varien_Db_Ddl_Table::TYPE_VARCHAR,
    'size' => 100,
    'required' => false
);
$connection = $installer->getConnection();
$connection->addColumn($this->getTable('catalog_mixmatch'), 'device_subscription_sku', 'varchar(64) null');


$idxFound = false;
$oldIdx1Found = false;
$oldIdx2Found = false;
$setup = Mage::getSingleton('core/resource')->getConnection('core_read');
foreach ($setup->fetchAll('SHOW INDEX FROM catalog_mixmatch') as $indexes) {
    if ($indexes['Key_name'] == '895CDBBAF6EB81BD061AF8691DE06061') {
        $idxFound = true;
    }

    if ($indexes['Key_name'] == 'UNQ_CATALOG_MIXMATCH_DEVICE_SKU_SUBSCRIPTION_SKU_WEBSITE_ID') {
        $oldIdx1Found = true;
    }

    if ($indexes['Key_name'] == 'IDX_CATALOG_MIXMATCH_WEBSITE_ID_DEVICE_SKU_SUBSCRIPTION_SKU') {
        $oldIdx2Found = true;
    }
}

if ($oldIdx1Found) {
    $installer->run("ALTER TABLE `catalog_mixmatch` DROP INDEX `UNQ_CATALOG_MIXMATCH_DEVICE_SKU_SUBSCRIPTION_SKU_WEBSITE_ID`");
}

if ($oldIdx2Found) {
    $installer->run("ALTER TABLE `catalog_mixmatch` DROP INDEX `IDX_CATALOG_MIXMATCH_WEBSITE_ID_DEVICE_SKU_SUBSCRIPTION_SKU`");
}

if (!$idxFound) {
    $table = $this->getTable('catalog_mixmatch');
    $fields1 = array('device_sku', 'subscription_sku', 'device_subscription_sku', 'website_id');
    $fields2 = array('website_id', 'device_sku', 'subscription_sku', 'device_subscription_sku');
    $isValid1 = $isValid2 = true;

    foreach ($fields1 as $field) {
        if (!$connection->tableColumnExists($table, $field)) {
            $isValid1 = false;
        }
    }

    foreach ($fields2 as $field) {
        if (!$connection->tableColumnExists($table, $field)) {
            $isValid2 = false;
        }
    }

    if ($isValid1) {
        $connection->addIndex(
            $table,
            $installer->getIdxName('omnius_mixmatch/mixmatch', $fields1, Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE),
            $fields1,
            Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
        );
    }

    if ($isValid2) {
        $connection->addIndex(
            $table,
            $installer->getIdxName('omnius_mixmatch/mixmatch', $fields2, Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE),
            $fields2,
            Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
        );
    }


}
$installer->endSetup();

