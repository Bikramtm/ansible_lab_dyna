<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();

$connection = $installer->getConnection();
$connection->addColumn($this->getTable('catalog_mixmatch'), 'uploaded_at', Varien_Db_Ddl_Table::TYPE_DATETIME);
$connection->addColumn($this->getTable('catalog_mixmatch'), 'uploaded_by', 'varchar(64) null');

$installer->endSetup();
