<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * MixMatch attributes grid
 *
 * @category   Omnius
 * @package    Omnius_MixMatch
 */
class Omnius_MixMatch_Block_Adminhtml_Rules_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    protected $_attributeSets = array();
    protected $_rulesArray = array();

    public function __construct()
    {
        $this->_attributeSets = array('mobile'); //TODO these options must be dynamic

        parent::__construct();
        $this->setId('mixmatchRulesGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setVarNameFilter('mixmatch_rules_filter');

    }

    /**
     * Prepare product attributes grid collection object
     *
     * @return Mage_Adminhtml_Block_Catalog_Product_Attribute_Grid
     */
    protected function _prepareCollection()
    {
        /** @var Omnius_MixMatch_Model_Resource_Price_Collection $collection */
        $collection = Mage::getResourceModel('omnius_mixmatch/price_collection');
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * Prepare product attributes grid columns
     *
     * @return Mage_Adminhtml_Block_Catalog_Product_Attribute_Grid
     */
    protected function _prepareColumns()
    {
        parent::_prepareColumns();

        $this->addColumn('entity_id',
            array(
                'header' => Mage::helper('omnius_mixmatch')->__('ID'),
                'width' => '10px',
                'type' => 'number',
                'index' => 'entity_id',
            ));
        $this->addColumn('device_sku',
            array(
                'header' => Mage::helper('omnius_mixmatch')->__('Device SKU'),
                'index' => 'device_sku',
            ));
        $this->addColumn('subscription_sku',
            array(
                'header' => Mage::helper('omnius_mixmatch')->__('Subscription SKU'),
                'index' => 'subscription_sku',
            ));
        $this->addColumn('device_subscription_sku',
            array(
                'header' => Mage::helper('omnius_mixmatch')->__('Device Subscription SKU'),
                'index' => 'device_subscription_sku',
            ));
        $this->addColumn('uploaded_by',
            array(
                'header' => Mage::helper('omnius_mixmatch')->__('Uploaded by'),
                'index' => 'uploaded_by',
            ));
        $this->addColumn('uploaded_at',
            array(
                'header' => Mage::helper('omnius_mixmatch')->__('Uploaded at'),
                'type' => 'datetime',
                'index' => 'uploaded_at',
            ));

        $store = $this->_getStore();
        $this->addColumn('price',
            array(
                'header' => Mage::helper('omnius_mixmatch')->__('Purchase Price'),
                'type' => 'price',
                'currency_code' => $store->getBaseCurrency()->getCode(),
                'index' => 'price',
            ));
        $this->addColumn('website_id',
            array(
                'header' => Mage::helper('catalog')->__('Website'),
                'index' => 'website_id',
                'type' => 'options',
                'options' => Mage::getModel('core/website')->getCollection()->toOptionHash(),
            ));

        return $this;
    }

    /**
     * Get current store
     *
     * @return Mage_Core_Model_Store
     * @throws Exception
     */
    protected function _getStore()
    {
        $storeId = (int) $this->getRequest()->getParam('store', 0);

        return Mage::app()->getStore($storeId);
    }
}
