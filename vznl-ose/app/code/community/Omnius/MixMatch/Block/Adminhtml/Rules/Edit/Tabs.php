<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_MixMatch_Block_Adminhtml_Rules_Edit_Tabs
 */
class Omnius_MixMatch_Block_Adminhtml_Rules_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();

        $this->setId("omnius_mixmatch_tabs");
        $this->setDestElementId("edit_form");
        $this->setTitle(Mage::helper("omnius_mixmatch")->__("Item Information"));
    }

    /**
     * Before rendering html, but after trying to load cache
     *
     * @return Mage_Core_Block_Abstract
     * @throws Exception
     */
    protected function _beforeToHtml()
    {
        $this->addTab("form_section", array(
            "label" => Mage::helper("omnius_mixmatch")->__("Item Information"),
            "title" => Mage::helper("omnius_mixmatch")->__("Item Information"),
            "content" => $this->getLayout()->createBlock("omnius_mixmatch/adminhtml_rules_edit_tab_form")->toHtml(),
        ));

        return parent::_beforeToHtml();
    }

}
