<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * @category   Omnius
 * @package    Omnius_MixMatch
 */
class Omnius_MixMatch_Block_Adminhtml_Rules extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    public function __construct()
    {
        $this->_controller = 'adminhtml_rules';
        $this->_blockGroup = 'omnius_mixmatch';

        $this->_headerText = Mage::helper('omnius_mixmatch')->__('Manage MixMatch Rules');
        $this->_addButtonLabel = Mage::helper('omnius_mixmatch')->__('Add New Item');
        parent::__construct();

        $this->_addButton('export', array(
            'label' => Mage::helper('omnius_mixmatch')->__('Download Mixmatch'),
            'onclick' => 'setLocation(\'' . $this->getExportUrl() . '\')',
            'class' => 'go',
        ));

        $this->_addButton('import', array(
            'label' => Mage::helper('omnius_mixmatch')->__('Upload Mixmatch'),
            'onclick' => 'javascript:new Popup(\'' . $this->getImportUrl() . '\', {title:\'' . Mage::helper('omnius_mixmatch')->__('Upload new MixMatch file') . '\', width: 600, height:400})',
            'class' => 'go',
        ));

        Mage::dispatchEvent('omnius_mixmatch_adminhtml_rules_buttons_added', array('buttons' => $this));
    }

    /**
     * Get the URL for export button
     *
     * @return string
     * @throws Exception
     */
    public function getExportUrl()
    {
        $params = array();
        $filters_name = 'mixmatch_rules_filter';
        if ($filters_value = $this->getRequest()->getParam($filters_name)) {
            $params = array($filters_name => $filters_value);
        }

        return $this->getUrl('*/*/export', $params);
    }

    /**
     * Get the URL for last export button
     *
     * @return string
     * @throws Exception
     */
    public function getLastExportUrl()
    {
        $params = array();
        $filters_name = 'mixmatch_rules_filter';
        if ($filters_value = $this->getRequest()->getParam($filters_name)) {
            $params = array($filters_name => $filters_value);
        }

        return $this->getUrl('*/*/lastexport', $params);
    }

    /**
     * Get URL for import button
     *
     * @return string
     */
    public function getImportUrl()
    {
        return $this->getUrl('*/*/import');
    }
}
