<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_MixMatch_Adminhtml_RulesController
 */
class Omnius_MixMatch_Adminhtml_RulesController extends Mage_Adminhtml_Controller_Action
{
    /**
     * @return $this
     */
    protected function _initAction()
    {
        $this->loadLayout()->_setActiveMenu("catalog")->_addBreadcrumb(Mage::helper("adminhtml")->__("Manage MixMatch"), Mage::helper("adminhtml")->__("Manage MixMatch"));

        return $this;
    }

    public function indexAction()
    {
        $this->_title($this->__("MixMatch"));
        $this->_title($this->__("Manage MixMatch"));

        $this->_initAction()
            ->_addContent($this->getLayout()->createBlock('omnius_mixmatch/adminhtml_rules'))
            ->renderLayout();
    }


    public function editAction()
    {
        $this->_title($this->__("MixMatch"));
        $this->_title($this->__("MixMatch"));
        $this->_title($this->__("Edit Item"));

        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("omnius_mixmatch/price")->load($id);

        if ($model->getId()) {
            Mage::register("omnius_mixmatch_data", $model);
            $this->loadLayout();
            $this->_setActiveMenu("omnius_mixmatch/price");
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("MixMatch Manager"), Mage::helper("adminhtml")->__("MixMatch Manager"));
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("MixMatch Description"), Mage::helper("adminhtml")->__("MixMatch Description"));
            $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock("omnius_mixmatch/adminhtml_rules_edit"))->_addLeft($this->getLayout()->createBlock("omnius_mixmatch/adminhtml_rules_edit_tabs"));
            $this->renderLayout();
        } else {
            Mage::getSingleton("adminhtml/session")->addError(Mage::helper("omnius_mixmatch")->__("Item does not exist."));
            $this->_redirect("*/*/");
        }
    }

    public function newAction()
    {

        $this->_title($this->__("MixMatch"));
        $this->_title($this->__("MixMatch"));
        $this->_title($this->__("New Item"));

        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("omnius_mixmatch/price")->load($id);

        $data = Mage::getSingleton("adminhtml/session")->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }

        Mage::register("omnius_mixmatch_data", $model);

        $this->loadLayout();
        $this->_setActiveMenu("omnius_mixmatch/price");

        $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);

        $this->_addBreadcrumb(Mage::helper("adminhtml")->__("MixMatch Manager"), Mage::helper("adminhtml")->__("MixMatch Manager"));
        $this->_addBreadcrumb(Mage::helper("adminhtml")->__("MixMatch Description"), Mage::helper("adminhtml")->__("MixMatch Description"));


        $this->_addContent($this->getLayout()->createBlock("omnius_mixmatch/adminhtml_rules_edit"))->_addLeft($this->getLayout()->createBlock("omnius_mixmatch/adminhtml_rules_edit_tabs"));

        $this->renderLayout();

    }

    public function saveAction()
    {
        $postData = new Varien_Object($this->getRequest()->getPost());

        $websiteIds = $postData->getData('website_id');
        $deviceSku = $postData->getData('device_sku');
        $subscriptionSku = $postData->getData('subscription_sku');
        $deviceSubscriptionSku = $postData->getData('device_subscription_sku');
        if ($deviceSku && $subscriptionSku && $websiteIds) {
            try {
                if ($deviceSubscriptionSku) {
                    if (3 !== count($this->getProductCollection(array($deviceSku, $subscriptionSku, $deviceSubscriptionSku)))) {
                        Mage::throwException('Invalid SKUs given. Please make sure that the SKUs belong to existing products');
                    }
                } else {
                    if (2 !== count($this->getProductCollection(array($deviceSku, $subscriptionSku)))) {
                        Mage::throwException('Invalid SKUs given. Please make sure that the SKUs belong to existing products');
                    }
                }
                foreach ($websiteIds as $websiteId) {
                    $model = Mage::getModel('omnius_mixmatch/price')
                        ->setDeviceSku($deviceSku)
                        ->setSubscriptionSku($subscriptionSku)
                        ->setDeviceSubscriptionSku($deviceSubscriptionSku)
                        ->setPrice($postData->getData('price'))
                        ->setWebsiteId($websiteId)
                        ->save();
                }

                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Rule successfully saved"));
                Mage::getSingleton("adminhtml/session")->setOmniusMixMatchData(false);

                if ($this->getRequest()->getParam("back")) {
                    $this->_redirect("*/*/edit", array("id" => $model->getId()));

                    return;
                }
                $this->_redirect("*/*/");

                return;
            } catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                Mage::getSingleton("adminhtml/session")->setOmniusMixMatchData($this->getRequest()->getPost());
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));

                return;
            }
        }
        Mage::getSingleton("adminhtml/session")->addError('Please fill all fields');
        Mage::getSingleton("adminhtml/session")->setOmniusMixMatchData($this->getRequest()->getPost());
        $this->_redirect("*/*/new");
    }


    public function deleteAction()
    {
        if ($this->getRequest()->getParam("id") > 0) {
            try {
                $model = Mage::getModel("omnius_mixmatch/price");
                $model->setId($this->getRequest()->getParam("id"))->delete();
                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("omnius_mixmatch")->__("Item was successfully deleted"));
                $this->_redirect("*/*/");
            } catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
            }
        }
        $this->_redirect("*/*/");
    }


    /**
     *  Export order grid to Excel XML format
     */
    public function exportAction()
    {
        /** @var Omnius_MixMatch_Block_Adminhtml_Rules_Grid $grid */
        $grid = $this->getLayout()->createBlock('omnius_mixmatch/adminhtml_rules_grid');

        // check if website filter has been set
        $filters_name = $grid->getVarNameFilter();
        $filter = $this->getRequest()->getParam($filters_name);
        if ($filter && is_string($filter)) {
                $filter = Mage::helper('adminhtml')->prepareFilterString($filter);
        }

        if (!isset($filter) || !is_array($filter) || !isset($filter['website_id']) || !$filter['website_id']) {
            Mage::getSingleton("adminhtml/session")->addError(Mage::helper("omnius_mixmatch")->__("Filter by Website first."));

            return $this->_redirect('*/*');
        }

        $fileName = 'MixMatch-W' . $filter['website_id'] . '-' . date("Ymdhis") . '.xlsx';
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName, $filter['website_id']));
    }

    public function lastexportAction()
    {
        $grid = $this->getLayout()->createBlock('omnius_mixmatch/adminhtml_rules_grid');

        // check if website filter has been set
        $filters_name = $grid->getVarNameFilter();
        $filter = $this->getRequest()->getParam($filters_name);
        if ($filter && is_string($filter)) {
            $filter = Mage::helper('adminhtml')->prepareFilterString($filter);
        }

        if (!isset($filter) || !is_array($filter) || !isset($filter['website_id']) || !$filter['website_id']) {
            Mage::getSingleton("adminhtml/session")->addError(Mage::helper("omnius_mixmatch")->__("Filter by Website first."));

            return $this->_redirect('*/*');
        }

        $fileName = Mage::helper('omnius_mixmatch')->getLastImportedFileName($filter['website_id']);
        if (!$fileName || !realpath($fileName)) {
            Mage::getSingleton("adminhtml/session")->addError(Mage::helper("omnius_mixmatch")->__("File not found or empty."));

            return $this->_redirect('*/*');
        }
        $content = file_get_contents($fileName);
        $this->_prepareDownloadResponse($fileName, $content);
    }

    public function importAction()
    {
        $this->loadLayout();
        $this->getLayout()->getBlock('catalog.product.edit.tab.related');

        $this->renderLayout();
    }

    public function uploadAction()
    {
        if ($postData = $this->getRequest()->getPost()) {
            Mage::getModel('omnius_mixmatch/price')->importFile($postData, $_FILES);
        } else {
            Mage::getSingleton('adminhtml/session')->addError(
                $this->__('Invalid POST data (please check post_max_size and upload_max_filesize settings in your php.ini file).')
            );
        }
        $this->_redirect('*/*');
    }

    /**
     * @param array $skus
     * @return array
     */
    protected function getProductCollection(array $skus = array())
    {
        $collection = Mage::getResourceSingleton('catalog/product_collection')
            ->addAttributeToFilter('sku', array('in' => array_unique($skus)))
            ->load();
        $ids = array();
        foreach ($collection->getItems() as $item) {
            $ids[$item->getSku()] = $item->getId();
        }

        return $ids;
    }

    /**
     * Patch for SUPEE-6285
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return true;
    }
}
