<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * @category    Omnius
 * @package     Omnius_MixMatch
 * @method      float getPrice()
 */
class Omnius_MixMatch_Model_Price extends Mage_Core_Model_Abstract
{
    /**
     * Override this method to implement the import functionality for each project.
     * @param array $postData
     * @param array $files
     * @return bool
     */
    public function importFile($postData, $files)
    {
        Mage::getSingleton('adminhtml/session')->addWarning(
            Mage::helper('omnius_mixmatch')->__('To implement the import functionality, please override the Omnius_MixMatch_Model_Price::importFile method.')
        );
        return true;
    }

    /**
     * Initialize resources
     */
    protected function _construct()
    {
        $this->_init('omnius_mixmatch/price');
    }

    /**
     * Processing object after save data
     *
     * @return Mage_Core_Model_Abstract
     */
    protected function _afterSave()
    {
        /**
         * Do not clean cache (heavy I/O) after each save
         */
        return $this;
    }
}
