<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_MixMatch_Model_Pricing
 */
class Omnius_MixMatch_Model_Pricing extends Varien_Object
{
    /** @var Dyna_Cache_Model_Cache */
    protected $_cache;

    /**
     * @param string $deviceSku
     * @param string $subscriptionSku
     * @param string $loanSku
     * @param int $websiteId
     * @return Omnius_MixMatch_Model_Resource_Price_Collection
     */
    public function getMixMatchCollection($deviceSku, $subscriptionSku, $loanSku = null, $websiteId = null)
    {
        if ($websiteId == null) {
            $websiteId = Mage::app()->getStore()->getWebsiteId();
        }

        $key = serialize(array(
            __METHOD__,
            $deviceSku,
            $subscriptionSku,
            $loanSku,
            $websiteId
        ));
        if ($result = unserialize($this->getCache()->load($key))) {
            return $result;
        } else {
            /** @var Omnius_MixMatch_Model_Resource_Price_Collection $collection */
            $collection = Mage::getModel('omnius_mixmatch/price')->getCollection()
                ->addFieldToFilter('device_sku', $deviceSku)
                ->addFieldToFilter('subscription_sku', $subscriptionSku)
                ->addFieldToFilter('website_id', $websiteId);

            if ($loanSku != null) {
                $collection->addFieldToFilter('device_subscription_sku', $loanSku);
            }

            $result = new Varien_Data_Collection();
            foreach ($collection->getItems() as $item) {
                $result->addItem($item);
            }

            $this->getCache()->save(serialize($result), $key, array(Dyna_Cache_Model_Cache::PRODUCT_TAG),
                $this->getCache()->getTtl());

            return $result;
        }
    }

    /**
     * Get cache object
     *
     * @return Dyna_Cache_Model_Cache
     */
    protected function getCache()
    {
        if ( ! $this->_cache) {
            $this->_cache = Mage::getSingleton('dyna_cache/cache');
        }
        return $this->_cache;
    }
}
