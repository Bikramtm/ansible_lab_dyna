<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * MixMatch prices collection
 *
 * @category    Omnius
 * @package     Omnius_MixMatch
 */
class Omnius_MixMatch_Model_Resource_Price_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{

    /**
     * Model initialization
     *
     */
    protected function _construct()
    {
        $this->_init('omnius_mixmatch/price');
    }
}
