<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * class Omnius_ProductMatchRule_Adminhtml_RulesController
 *
 * Default Rules Administration Controller
 */
class Omnius_ProductMatchRule_Adminhtml_RulesController extends Mage_Adminhtml_Controller_Action
{
    /**
     * @return $this
     */
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('catalog/rules')
            ->_addBreadcrumb(
                Mage::helper('adminhtml')->__('Product Match Rules'),
                Mage::helper('adminhtml')->__('Product Match Rules')
            );

        return $this;
    }

    /**
     * Display the admin grid (table) with all the announcements
     */
    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    /**
     * Display the categories tree in the popup to select a category
     */
    public function gettreeAction()
    {
        $product = Mage::getModel('catalog/product');
        $category = Mage::getModel('catalog/category')->load(8);
        $product->setCategory($category);
        Mage::register('current_product', $product);
        $position = $this->getRequest()->getParam('position');
        $blockHtml = $this->getLayout()
            ->createBlock('productmatchrule/adminhtml_catalog_product_tree_category')
            ->setPosition($position)
            ->toHtml();
        $this->getResponse()->setBody($blockHtml);

        return;
    }

    /**
     * Display the selected category products
     */
    public function getproductsAction()
    {
        $catId = $this->getRequest()->getParam('category', 0);
        $category = Mage::getModel('catalog/category')->load($catId);
        $category->setProductsReadonly(true);
        $category->setRemoveColumns(true);
        Mage::register('category', $category);

        $this->_initAction()
            ->renderLayout();
    }

    /**
     * Category products grid ajax pagination
     */
    public function gridAction()
    {
        $catId = $this->getRequest()->getParam('category', 0);
        $category = Mage::getModel('catalog/category')->load($catId);
        $category->setProductsReadonly(true);
        $category->setRemoveColumns(true);
        Mage::register('category', $category);

        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('adminhtml/catalog_category_tab_product', 'category.product.grid')
                ->toHtml()
        );
    }

    /**
     * Gather data and generate edit form
     *
     */
    public function editAction()
    {
        $id = $this->getRequest()->getParam('product_match_rule_id');
        $model = Mage::getModel('productmatchrule/rule')->load($id);

        if ($model->getProductMatchRuleId() || $id == 0) {
            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);

            if (!empty($data)) {
                $model->setData($data);
            }
            $websites = [];
            // Read the rule for all websites
            $allRules = $model->loadAll();
            foreach ($allRules as $rule) {
                $websites[] = $rule->getWebsiteId();
            }
            $model->setWebsiteId($websites);

            Mage::register('rules_data', $model);

            $this->loadLayout();
            $this->_setActiveMenu('catalog/rule');

            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Rule manager'),
                Mage::helper('adminhtml')->__('Rule manager'));
            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'),
                Mage::helper('adminhtml')->__('Item News'));

            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

            $this->_addContent($this->getLayout()->createBlock('productmatchrule/adminhtml_rules_edit'))
                ->_addLeft($this->getLayout()->createBlock('productmatchrule/adminhtml_rules_edit_tabs'));

            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')
                ->addError(Mage::helper('productmatchrule')->__('Item does not exist'));
            $this->_redirect('*/*/');
        }
    }

    /**
     * Admin add a new Announcement, forwards to edit action
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    /**
     * Perform the validation and saving of the announcements code
     *
     */
    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            $websiteIds = $data['website_id'];

            if (isset($data['left_id']['product']) && $data['left_id']['product'] == -1) {
                unset($data['left_id']['product']);
            }
            if (isset($data['right_id']['product']) && $data['right_id']['product'] == -1) {
                unset($data['right_id']['product']);
            }

            if (isset($data['left_id']['category'])
                && (!isset($data['left_id']['category']) || $data['left_id']['category'] == -1)
            ) {
                unset($data['left_id']['category']);
            }
            if (isset($data['right_id']['product'])
                && (!isset($data['right_id']['category']) || $data['right_id']['category'] == -1)
            ) {
                unset($data['right_id']['category']);
            }

            $leftId = empty($data['left_id'])
                ? 0
                : ((empty($data['left_id']['product']) && empty($data['left_id']['category']))
                    ? 0
                    : ((empty($data['left_id']['product']) || $data['left_id']['product'] == -1)
                        ? ((empty($data['left_id']['category']) || $data['left_id']['category'] == -1)
                            ? 0
                            : (int) $data['left_id']['category']
                        )
                        : (int) $data['left_id']['product'])
                );

            $rightId = empty($data['right_id'])
                ? 0
                : ((empty($data['right_id']['product']) && empty($data['right_id']['category']))
                    ? 0
                    : ((empty($data['right_id']['product']) || $data['right_id']['product'] == -1)
                        ? ((empty($data['right_id']['category']) || $data['right_id']['category'] == -1)
                            ? 0
                            : (int) $data['right_id']['category']
                        )
                        : (int) $data['right_id']['product']
                    )
                );

            if (!$leftId || !$rightId) {
                $this->throwErrror('You need to select both Source and Target', $data);

                return;
            }

            try {
                $this->handleOperation($data);

                $oldWebsiteIds = [];
                $removed = [];
                if ($baseId = $this->getRequest()->getParam('product_match_rule_id')) {
                    $oldModels = Mage::getModel('productmatchrule/rule')->loadAll($baseId);
                    foreach ($oldModels as $oldModel) {
                        $oldWebsiteId = $oldModel->getWebsiteId();
                        $oldWebsiteIds[$oldWebsiteId] = $oldModel;
                        if (!in_array($oldWebsiteId, $websiteIds)) {
                            $removed[] = $oldModel;
                        }
                    }
                }

                // Parse POST data to Omnius_ProductMatchRule_Model_MatchRule model
                $models = Mage::helper('productmatchrule')->setRuleData($data, $oldWebsiteIds);
                if (!is_array($models)) {
                    // Error occured
                    $this->throwErrror($models, $data);

                    return;
                }

                foreach ($models as $model) {
                    $model->save();
                }
                foreach ($removed as $model) {
                    $model->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('productmatchrule')->__('The rule was successfully saved')
                );
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('product_match_rule_id' => $model->getProductMatchRuleId()));

                    return;
                }
            } catch (Exception $e) {
                $this->throwErrror($e->getMessage(), $data);

                return;
            }
            $this->_redirect('*/*/');

            return;
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('productmatchrule')->__('Unable to find item to save')
        );
        $this->_redirect('*/*/');
    }

    /**
     * Method that throws the error based on a message
     *
     * @param mixed $message
     * @param $data
     */
    protected function throwErrror($message, $data)
    {
        Mage::getSingleton('adminhtml/session')->addError($message);
        Mage::getSingleton('adminhtml/session')->setFormData($data);
        $this->_redirect('*/*/edit',
            array('product_match_rule_id' => $this->getRequest()->getParam('product_match_rule_id')));
    }

    /**
     * Method that deleted a rules code
     *
     */
    public function deleteAction()
    {
        if ($this->getRequest()->getParam('product_match_rule_id') > 0) {
            try {
                $model = Mage::getModel('productmatchrule/rule')->load($this->getRequest()->getParam('product_match_rule_id'));
                if (!$model->getId()) {
                    throw new Exception('Rule with the provided ID not found');
                }
                $regKey = sprintf('%s_%s', Omnius_ProductMatchRule_Model_Rule::UNIQUE_ID_REFERENCE, 'product_match_rule');
                if (!($deletedRules = Mage::registry($regKey))) {
                    $deletedRules = array();
                }
                $allRules = $model->loadAll();
                foreach ($allRules as $rule) {
                    $deletedRules[$rule->getId()] = $rule->getUniqueId();
                    Mage::unregister($regKey);
                    Mage::register($regKey, $deletedRules);
                    $rule->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit',
                    array('product_match_rule_id' => $this->getRequest()->getParam('product_match_rule_id')));
            }
        }
        $this->_redirect('*/*/');
    }

    /**
     * Method to allow bulk deletion of rules codes
     *
     */
    public function massDeleteAction()
    {
        $rulesIds = $this->getRequest()->getParam('rules');
        if (!is_array($rulesIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                $regKey = sprintf('%s_%s', Omnius_ProductMatchRule_Model_Rule::UNIQUE_ID_REFERENCE, 'product_match_rule');
                $deletedRules = [];
                foreach ($rulesIds as $ruleId) {
                    $rule = Mage::getModel('productmatchrule/rule')->load($ruleId);
                    Mage::unregister($regKey);
                    Mage::register($regKey, array($rule->getId() => $rule->getUniqueId()));
                    $allRules = $rule->loadAll();
                    foreach ($allRules as $ruleNew) {
                        $deletedRules[$ruleNew->getId()] = $ruleNew->getUniqueId();
                        Mage::unregister($regKey);
                        Mage::register($regKey, $deletedRules);
                        $ruleNew->delete();
                    }
                    $rule->delete();
                }

                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully deleted', count($rulesIds)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * Method to allow bulk locking of announcements codes
     */
    public function massLockAction()
    {
        $rulesIds = $this->getRequest()->getParam('rules');

        if (!is_array($rulesIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($rulesIds as $ruleId) {
                    $matchRules = Mage::getModel('productmatchrule/rule')->loadAll($ruleId);
                    foreach ($matchRules as $matchRule) {
                        $matchRule->setLocked(1)->save();
                    }
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully locked', count($rulesIds)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * Method to allow bulk unlocking of announcements codes
     */
    public function massUnLockAction()
    {
        $rulesIds = $this->getRequest()->getParam('rules');
        if (!is_array($rulesIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($rulesIds as $ruleId) {
                    $matchRules = Mage::getModel('productmatchrule/rule')->loadAll($ruleId);
                    foreach ($matchRules as $matchRule) {
                        $matchRule->setLocked(0)->save();
                    }
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully unlocked', count($rulesIds)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * Import MatchRules
     */
    public function importAction()
    {
        $this->loadLayout()
            ->renderLayout();
    }

    /**
     * Upload and run import file
     */
    public function uploadAction()
    {
        if ($this->getRequest()->getPost()) {
            if (isset($_FILES['file']['tmp_name'])) {
                try {
                    $this->parseFile();
                } catch (Exception $e) {
                    Mage::getSingleton('adminhtml/session')->addError(
                        $this->__($e->getMessage())
                    );
                }
            }
        } else {
            Mage::getSingleton('adminhtml/session')->addError(
                $this->__('Invalid POST data (please check post_max_size and upload_max_filesize settings in your php.ini file).')
            );
        }
        $this->_redirect('*/*');
    }

    /**
     * Export order grid to CSV format
     */
    public function exportCsvAction()
    {
        $fileName = sprintf('export_match_rules_%s.csv', now());
        /** @var Dyna_MultiMapper_Block_Adminhtml_Mapper_Grid $grid */
        $grid = $this->getLayout()->createBlock('productmatchrule/adminhtml_rules_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }

    /**
     * Declare headers and content file in response for file download
     *
     * @param string $fileName
     * @param string|array $content set to null to avoid starting output, $contentLength should be set explicitly in
     *                              that case
     * @param string $contentType
     * @param int $contentLength explicit content length, if strlen($content) isn't applicable
     * @return Mage_Core_Controller_Varien_Action
     */
    protected function _prepareDownloadResponse(
        $fileName,
        $content,
        $contentType = 'application/octet-stream',
        $contentLength = null
    ) {
        $session = Mage::getSingleton('admin/session');
        if ($session->isFirstPageAfterLogin()) {
            $this->_redirect($session->getUser()->getStartupPageUrl());

            return $this;
        }

        $isFile = false;
        $file = null;
        if (is_array($content)) {
            if (!isset($content['type']) || !isset($content['value'])) {
                return $this;
            }
            if ($content['type'] == 'filename') {
                $isFile = true;
                $file = $content['value'];
                $contentLength = filesize($file);
            }
        }

        $this->getResponse()
            ->setHttpResponseCode(200)
            ->setHeader('Pragma', 'public', true)
            ->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true)
            ->setHeader('Content-type', $contentType, true)
            ->setHeader('Content-Length', is_null($contentLength) ? strlen($content) : $contentLength, true)
            ->setHeader('Content-Disposition', 'attachment; filename="' . $fileName . '"', true)
            ->setHeader('Last-Modified', date('r'), true);

        if (!is_null($content)) {
            if ($isFile) {
                $this->getResponse()->clearBody();
                $this->getResponse()->sendHeaders();

                $ioAdapter = new Varien_Io_File();
                $ioAdapter->open(array('path' => $ioAdapter->dirname($file)));
                $ioAdapter->streamOpen($file, 'r');
                while ($buffer = $ioAdapter->streamRead()) {
                    print $buffer;
                }
                $ioAdapter->streamClose();
                if (!empty($content['rm'])) {
                    $ioAdapter->rm($file);
                }

                exit(0);
            } else {
                $this->getResponse()->setBody($content);
            }
        }

        return $this;
    }

    /**
     * Patch for SUPEE-6285
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return true;
    }

    /**
     * Parse file
     */
    protected function parseFile()
    {
        if ($file = $_FILES['file']['tmp_name']) {
            $uploader = new Mage_Core_Model_File_Uploader('file');
            $uploader->setAllowedExtensions(array('csv'));
            $path = Mage::app()->getConfig()->getTempVarDir() . '/import/';
            $uploader->save($path);
            if ($uploadFile = $uploader->getUploadedFileName()) {
                $newFilename = 'import-' . date('YmdHis') . '_' . $uploadFile;
                rename($path . $uploadFile, $path . $newFilename);
            }
        }

        if (isset($newFilename) && $newFilename) {
            /** @var Omnius_ProductMatchRule_Model_Importer $importer */
            $importer = Mage::getModel('productmatchrule/importer');
            $success = $importer->import($path . $newFilename);

            if ($success) {
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    $this->__('Import file successfully parsed and imported into database')
                );
            } else {
                Mage::getSingleton('adminhtml/session')->addError(
                    $this->__('Something went wrong with this file')
                );
            }
        }
    }

    /**
     * @param array $data
     * @throws Exception
     */
    protected function handleOperation(&$data)
    {
        $left = key($data['left_id']);
        $right = key($data['right_id']);

        if ($left == 'category' && $right == 'category') {
            $data['operation_type'] = Omnius_ProductMatchRule_Model_Rule::OP_TYPE_C2C;
        } elseif ($left == 'product' && $right == 'category') {
            $data['operation_type'] = Omnius_ProductMatchRule_Model_Rule::OP_TYPE_P2C;
        } elseif ($left == 'product' && $right == 'product') {
            $data['operation_type'] = Omnius_ProductMatchRule_Model_Rule::OP_TYPE_P2P;
        } elseif ($left == 'category' && $right == 'product') {
            $data['operation_type'] = Omnius_ProductMatchRule_Model_Rule::OP_TYPE_C2P;
        } else {
            throw new Exception('Operation type impossible.');
        }

        $data['left_id'] = $data['left_id'][$left];
        $data['right_id'] = $data['right_id'][$right];
    }
}
