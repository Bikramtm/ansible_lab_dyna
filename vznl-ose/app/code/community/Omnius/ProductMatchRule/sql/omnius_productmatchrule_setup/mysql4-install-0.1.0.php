<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Eav_Model_Entity_Setup $this */
$installer = $this;
$installer->startSetup();

if (!$this->getConnection()->isTableExists($this->getTable('product_match_rule'))) {
    /** Match rule table */
    $matchRuleTable = new Varien_Db_Ddl_Table();
    $matchRuleTable->setName('product_match_rule');
    $matchRuleTable->addColumn(
        'product_match_rule_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        10,
        array('unsigned' => true, 'primary' => true, 'auto_increment' => true)
    );
    $matchRuleTable->addColumn(
        'left_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        11
    );
    $matchRuleTable->addColumn(
        'operation',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        2
    );
    $matchRuleTable->addColumn(
        'right_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        11
    );
    $matchRuleTable->addColumn(
        'operation_type',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        1
    );
    $matchRuleTable->addColumn(
        'priority',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        2
    );

    $matchRuleTable->setOption('type', 'InnoDB');
    $matchRuleTable->setOption('charset', 'utf8');
    $this->getConnection()->createTable($matchRuleTable);
    /** End Match Rule Table */
}
$installer->endSetup();
