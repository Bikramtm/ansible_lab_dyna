<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $this Mage_Catalog_Model_Resource_Eav_Mysql4_Setup */
$installer = $this;

$installer->startSetup();
$connection = $this->getConnection();
$connection->addColumn($this->getTable('product_match_rule'), 'unique_id', [
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'length' => 64,
    'nullable' => true,
    'required' => false,
    'comment' => "Unique id",
]);
$connection->addIndex($this->getTable('product_match_rule'), 'PRODUCT_MATH_RULE_UNIQUE_ID_INDEX', 'unique_id');

/**
 * Update existing categories
 */
$updates = array();
$hashes = array();
foreach (Mage::getResourceModel('productmatchrule/rule_collection')->getData() as $match) {
    $hash = hash('sha256', $match['product_match_rule_id']);

    // If there's a change we have hash collisions
    if (in_array($hash, $hashes)) {

        // This should assure uniqueness in our limited set of categories
        $hash = substr_replace($hash, substr($hash, 0, 10), -10);
    }
    $updates[] = sprintf("UPDATE `product_match_rule` SET `unique_id`='%s' WHERE `product_match_rule_id`='%s';", $hash, $match['product_match_rule_id']);
    $hashes[] = $hash;
}
unset($hashes);
if (count($updates)) {
    foreach (array_chunk($updates, 250) as $chunk) {
        $connection->query(join('', $chunk));
    }
}

$installer->endSetup();
