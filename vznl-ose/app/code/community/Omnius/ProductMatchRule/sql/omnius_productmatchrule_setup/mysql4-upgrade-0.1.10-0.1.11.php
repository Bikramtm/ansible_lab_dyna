<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Core_Model_Resource_Setup $this */
$this->startSetup();

$this->getConnection()->addColumn($this->getTable('product_match_rule'), 'rule_title', 'varchar(255)');
$this->getConnection()->addColumn($this->getTable('product_match_rule'), 'rule_description', 'varchar(255)');
$this->endSetup();
