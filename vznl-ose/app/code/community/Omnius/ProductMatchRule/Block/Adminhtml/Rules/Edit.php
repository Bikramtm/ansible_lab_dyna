<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_ProductMatchRule_Block_Adminhtml_Rules_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * Omnius_ProductMatchRule_Block_Adminhtml_Rules_Edit constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_objectId = 'product_match_rule_id';
        $this->_blockGroup = 'productmatchrule';
        $this->_controller = 'adminhtml_rules';

        $this->_updateButton('save', 'label', Mage::helper('productmatchrule')->__('Save rule'));

        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);
        $objId = $this->getRequest()->getParam($this->_objectId);
        if (! empty($objId)) {
            $this->_addButton('delete', array(
                'label'     => Mage::helper('adminhtml')->__('Delete'),
                'class'     => 'delete',
                'onclick'   => 'deleteConfirm(\''. Mage::helper('adminhtml')->__('Are you sure you want to do this?')
                .'\', \'' . $this->getDeleteUrl() . '\')',
            ));
        }

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('rules_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'rules_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'rules_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    /**
     * @return mixed
     */
    public function getHeaderText()
    {
        if( Mage::registry('rules_data') && Mage::registry('rules_data')->getProductMatchRuleId() ) {
            return Mage::helper('productmatchrule')->__("Edit rule '%s'", $this->htmlEscape(Mage::registry('rules_data')->getProductMatchRuleId()));
        } else {
            return Mage::helper('productmatchrule')->__('Add rule');
        }
    }
}