<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_ProductMatchRule_Block_Adminhtml_Rules
 */
class Omnius_ProductMatchRule_Block_Adminhtml_Rules extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * Omnius_ProductMatchRule_Block_Adminhtml_Rules constructor.
     */
    public function __construct()
    {
        $this->_controller = 'adminhtml_rules';
        $this->_blockGroup = 'productmatchrule';
        $this->_headerText = Mage::helper('productmatchrule')->__('Rules manager');
        $this->_addButtonLabel = Mage::helper('productmatchrule')->__('Add rule');
        parent::__construct();

        $importUrl = $this->getImportUrl();
        $title = Mage::helper('productmatchrule')->__('Upload new MatchRules file');
        $onClick = <<<JS
javascript:new Popup('{$importUrl}', {title:'{$title}', width: 600, height:220});
JS;

        $this->_addButton('import', array(
            'label'     => Mage::helper('productmatchrule')->__('Upload Match Rules'),
            'onclick'   => $onClick,
            'class'     => 'go',
        ));
    }

    /**
     * @return mixed
     */
    public function getImportUrl()
    {
        return $this->getUrl('*/*/import');
    }
}