<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_ProductMatchRule_Model_MatchRule
 * @package MatchRule
 */
class Omnius_ProductMatchRule_Model_MatchRule extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('productmatchrule/matchRule');
    }
}