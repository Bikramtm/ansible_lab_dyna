<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_ProductMatchRule_Model_Importer
 */
class Omnius_ProductMatchRule_Model_Importer extends Mage_Core_Model_Abstract
{

    /**
     * @param $path
     * @return bool
     */
    public function import($path)
    {
        /** @var Omnius_ProductMatchRule_Helper_Data $helper */
        $helper = Mage::helper('productmatchrule');
        $file = fopen($path, 'r');
        $lc = 0;
        $success = true;
        $operationsTypes = $helper->getOperationTypes();
        $operations = $helper->getOperations();
        $mandatoryColumns = ['left_id','operation','right_id','operation_type','priority'];
        $headers = [
            'Left Entity'       => 'left_id',
            'Operation'         => 'operation',
            'Right Entity'      => 'right_id',
            'Operation type'    => 'operation_type',
            'Prioriteit'        => 'priority',
            'Website'           => 'website_id',
            'Rule title'        => 'rule_title',
            'Rule description'  => 'rule_description',
            'Locked'            => 'locked',
            'Unique Id'         => 'unique_id'
        ];
        while (($line = fgetcsv($file)) !== false) {
            if ($lc == 0) {
                foreach ($headers as $key => $value) {
                    if(in_array($key, $line)) {
                        $position[$value] = array_search($key, $line);
                    }
                }
            }

            $lc++;
            if ($lc == 1) {
                continue;
            }
            $opType = [];

            if(isset($position['rule_title'])){
                $data['rule_title'] = trim($line[$position['rule_title']]);
            }
            if(isset($position['operation_type'])){
                $data['operation_type'] =  array_search(trim($line[$position['operation_type']]), $operationsTypes);
                $opType = explode('-', trim($line[$position['operation_type']]));
            }
            if(isset($position['operation'])){
                $data['operation'] = array_search(trim($line[$position['operation']]), $operations);
            }
            if(isset($position['left_id']) && !empty($opType)){
                $data['left_id'] = $this->_getEntity($opType[0], trim($line[$position['left_id']]));
            }
            if(isset($position['right_id']) && !empty($opType)){
                $data['right_id'] = $this->_getEntity($opType[1], trim($line[$position['right_id']]));
            }
            if(isset($position['rule_description'])){
                $data['rule_description'] = trim($line[$position['rule_description']]);
            }
            if(isset($position['priority'])){
                $data['priority'] = (int)$line[$position['priority']];
            }
            if(isset($position['locked'])){
                $data['locked'] = (strtolower(trim($line[$position['locked']])) == 'locked') ? 1 : 0;
            }
            if(isset($position['website_id'])){
                $data['website_id'] = explode(',', trim($line[$position['website_id']]));
            }

            $data['rule_origin'] = 0;

            $messageCol = [];
            $messageVal = [];

            foreach ($mandatoryColumns as $column) {
                if (!isset($data[$column])) {
                    $messageCol[] = array_search($column, $headers);
                }elseif (empty($data[$column])) {
                    $messageVal[] = array_search($column, $headers);
                }
            }

            if (!empty($messageCol)) {
                $msg = "The '".implode(', ',$messageCol). (count($messageCol) == 1 ? "' column is " : "' columns are "). "missing from the imported csv.";
                Mage::getSingleton('adminhtml/session')->addWarning($msg);
                return false;
            } elseif (!empty($messageVal)){
                $msg = "The line No.".($lc - 1)." has failed to import because the '".implode(', ',$messageVal). (count($messageVal) == 1 ? "' contains an empty value  " : "' fileds contains empty values "). " or invalid characters.";
                Mage::getSingleton('adminhtml/session')->addWarning($msg);
            } else{
                $oldWebsiteIds = [];
                $removed = [];

                $existingRule = Mage::getModel('productmatchrule/rule')->getCollection()
                    ->addFieldToFilter('operation_type', $data['operation_type'])
                    ->addFieldToFilter('left_id', $data['left_id'])
                    ->addFieldToFilter('right_id', $data['right_id'])
                    ->addFieldToFilter('operation', $data['operation'])
                    ->addFieldToFilter('priority', $data['priority'])
                    ->getFirstItem();

                if ($baseId = $existingRule->getId()) {

                    $oldModels = Mage::getModel('productmatchrule/rule')->loadAll($baseId);
                    foreach ($oldModels as $oldModel) {
                        $oldWebsiteId = $oldModel->getWebsiteId();
                        $oldWebsiteIds[$oldWebsiteId] = $oldModel;
                        if (!in_array($oldWebsiteId, $data['website_id'])) {
                            $removed[] = $oldModel;
                        }
                    }
                }

                // Parse POST data to Omnius_ProductMatchRule_Model_MatchRule model
                $models = $helper->setRuleData($data, $oldWebsiteIds);

                if (! is_array($models)) {
                    // Error occurred
                    return false;
                }

                try {
                    foreach ($models as $model) {
                        $model->save();
                    }

                    foreach ($removed as $model) {
                        $model->delete();
                    }

                    $success = true;
                } catch (Exception $e) {

                    return false;
                }

                $this->_log('Imported row ' . print_r($line, true));
            }

        }
        fclose($file);

        return $success;
    }

    /**
     * @param $type
     * @param $value
     * @return Omnius_Catalog_Model_Product|Omnius_Catalog_Model_Category|null
     */
    protected function _getEntity($type, $value)
    {
        switch ($type) {
            case 'PRODUCT':
                return Mage::getModel("catalog/product")->getIdBySku($value);
            case 'CATEGORY':
                return Mage::getModel('catalog/category')
                    ->getCollection()
                    // load the "$value" category
                    ->addAttributeToFilter('url_key', $value)
                    ->getFirstItem()
                    ->getId();
            default:
                // Unknown entity
                break;
        }

        return null;
    }

    /**
     * Log message
     *
     * @param $msg
     */
    protected function _log($msg)
    {
        Mage::log($msg, null, 'matchrules_import_' . date('Y-m-d') . '.log');
    }
}
