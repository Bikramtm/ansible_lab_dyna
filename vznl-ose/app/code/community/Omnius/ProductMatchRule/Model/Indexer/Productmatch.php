<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_ProductMatchRule_Model_Indexer_Productmatch
 */
class Omnius_ProductMatchRule_Model_Indexer_Productmatch extends Omnius_ProductMatchRule_Model_Indexer_Abstract
{
    const DELETE_ACTION = 0;
    const ADD_ACTION = 1;

    /** @var string */
    protected $_table;

    /** @var array */
    protected $_matches = array();

    /** @var array */
    protected $_prev = array();

    /** @var array */
    protected $_allProductIds = array();

    /** @var array */
    protected $_allCategoryIds = array();

    /** @var array */
    protected $_productCategories = array();

    /** @var array */
    protected $_websites = array();

    /** @var array */
    protected $_websiteIds = array();

    /**
     * Initiate indexer
     */
    public function __construct()
    {
        parent::__construct();
        $this->_table = (string)Mage::getResourceSingleton('productmatchrule/matchRule')->getMainTable();

        $this->_init('omnius_productmatchrule/indexer_productmatch');
    }

    /**
     * Get Indexer name
     *
     * @return string
     */
    public function getName()
    {
        return 'Product combination rules';
    }

    /**
     * Get Indexer description
     *
     * @return string
     */
    public function getDescription()
    {
        return 'Product match rules that determine which products can be combined together';
    }

    /**
     * Empty index and generate whitelist based on rules
     */
    public function reindexAll()
    {
        $this->getConnection()->query(sprintf('TRUNCATE TABLE `%s`;', $this->_table));
        $this->closeConnection();
        $this->_reindexAll();
    }

    /**
     * Executed before processing the collection
     */
    public function start()
    {
        $this->_websites = array();
        $this->_websiteIds = array_keys(Mage::app()->getWebsites());

        foreach ($this->_websiteIds as $websiteId) {
            $this->_websites[$websiteId] = array();
            $this->_productCategories[$websiteId] = Mage::getModel('productmatchrule/rule')->getAllCategoryProducts($websiteId);
        }

        //get id of all products to check if these are still available
        $this->_allProductIds = Mage::getResourceModel('catalog/product_collection')->getAllIds();
        $this->_allCategoryIds = Mage::getResourceModel('catalog/category_collection')->getAllIds();
    }

    /**
     * Executed after processing the collection
     */
    public function end()
    {
        $this->_applyChanges();

        Mage::dispatchEvent('index_mix_matches', ['product_match' => $this]);

        $this->_applyChanges();
        parent::end();
    }

    /**
     * @param $productId
     * @param $websiteId
     * @return array
     *
     * Gets possible products that can be combined with a certain product id
     */
    public function getAllowedProductsFor($productId, $websiteId)
    {
        $allowedLeft = Mage::getResourceModel('productmatchrule/matchRule_collection')
            ->addFieldToFilter('source_product_id', $productId)
            ->addFieldToFilter('website_id', $websiteId);
        $allowedLeft->getSelect()
            ->reset(Varien_Db_Select::COLUMNS)
            ->columns('target_product_id')
            ->distinct(true);

        $allowedRight = Mage::getResourceModel('productmatchrule/matchRule_collection')
            ->addFieldToFilter('target_product_id', $productId)
            ->addFieldToFilter('website_id', $websiteId);
        $allowedRight->getSelect()
            ->reset(Varien_Db_Select::COLUMNS)
            ->columns('source_product_id')
            ->distinct(true);

        $allowed = array_unique(array_merge($this->getConnection()->fetchCol($allowedLeft->getSelect()), $this->getConnection()->fetchCol($allowedRight->getSelect())));
        $this->closeConnection();
        unset($allowedLeft, $allowedRight);
        return $allowed;
    }

    /**
     * @param $rule Omnius_ProductMatchRule_Model_Rule
     */
    protected function processItem($rule)
    {
        $websiteId = $rule['website_id'];

        switch ($rule['operation_type']) {
            case Omnius_ProductMatchRule_Model_Rule::OP_TYPE_P2P:
                if (!in_array($rule['left_id'], $this->_allProductIds) || !in_array($rule['right_id'], $this->_allProductIds)
                ) {
                    break;
                }
                switch ($rule['operation']) {
                    case Omnius_ProductMatchRule_Model_Rule::OP_ALLOWED:
                    case Omnius_ProductMatchRule_Model_Rule::OP_DEFAULTED:
                    case Omnius_ProductMatchRule_Model_Rule::OP_OBLIGATED:
                        $this->_addConditional($rule['left_id'], $rule['right_id'], $websiteId);
                        break;
                    case Omnius_ProductMatchRule_Model_Rule::OP_NOTALLOWED:
                        $this->_removeConditional($rule['left_id'], $rule['right_id'], $websiteId);
                        break;
                }
                break;
            case Omnius_ProductMatchRule_Model_Rule::OP_TYPE_P2C:
                if (!in_array($rule['left_id'], $this->_allProductIds) || !in_array($rule['right_id'], $this->_allCategoryIds)) {
                    break;
                }
                switch ($rule['operation']) {
                    case Omnius_ProductMatchRule_Model_Rule::OP_ALLOWED:
                    case Omnius_ProductMatchRule_Model_Rule::OP_OBLIGATED:
                    case Omnius_ProductMatchRule_Model_Rule::OP_DEFAULTED:
                        if (isset($this->_productCategories[$websiteId][$rule['right_id']])) {
                            foreach ($this->_productCategories[$websiteId][$rule['right_id']] as &$catProdId) {
                                $this->_addConditional($rule['left_id'], $catProdId, $websiteId);
                            }
                            unset($catProdId);
                        }
                        break;
                    case Omnius_ProductMatchRule_Model_Rule::OP_NOTALLOWED:
                        if (isset($this->_productCategories[$websiteId][$rule['right_id']])) {
                            foreach ($this->_productCategories[$websiteId][$rule['right_id']] as &$catProdId) {
                                $this->_removeConditional($rule['left_id'], $catProdId, $websiteId);
                            }
                            unset($catProdId);
                        }
                        break;
                }
                break;
            case Omnius_ProductMatchRule_Model_Rule::OP_TYPE_C2P:
                if (!in_array($rule['right_id'], $this->_allProductIds) || !in_array($rule['left_id'], $this->_allCategoryIds)) {
                    break;
                }
                switch ($rule['operation']) {
                    case Omnius_ProductMatchRule_Model_Rule::OP_ALLOWED:
                    case Omnius_ProductMatchRule_Model_Rule::OP_DEFAULTED:
                    case Omnius_ProductMatchRule_Model_Rule::OP_OBLIGATED:
                        if (isset($this->_productCategories[$websiteId][$rule['left_id']])) {
                            foreach ($this->_productCategories[$websiteId][$rule['left_id']] as &$catProdId) {
                                $this->_addConditional($rule['right_id'], $catProdId, $websiteId);
                            }
                            unset($catProdId);
                        }
                        break;
                    case Omnius_ProductMatchRule_Model_Rule::OP_NOTALLOWED:
                        if (isset($this->_productCategories[$websiteId][$rule['left_id']])) {
                            foreach ($this->_productCategories[$websiteId][$rule['left_id']] as &$catProdId) {
                                $this->_removeConditional($rule['right_id'], $catProdId, $websiteId);
                            }
                            unset($catProdId);
                        }
                        break;
                }
                break;
            case Omnius_ProductMatchRule_Model_Rule::OP_TYPE_C2C:
                if (!in_array($rule['left_id'], $this->_allCategoryIds) || !in_array($rule['right_id'], $this->_allCategoryIds)) {
                    break;
                }
                $this->handleConditionals($rule, $websiteId, $leftProdId, $rightProdId);
                break;
        }
    }

    /**
     * Groups gathered items together by statement (INSERT/DELETE)
     * to decrease the number of statements executed on the database
     */
    protected function _applyChanges()
    {
        $this->_matches = array_reverse($this->_matches);
        while (($_row = array_pop($this->_matches)) !== null) {
            if (!count($this->_prev) || $_row[0] === $this->_prev[0][0]) {
                $this->_prev[] = $_row;
            } elseif ($this->_prev[0][0] === self::DELETE_ACTION) {
                $this->_removeMultiple();
                $this->_prev = array($_row);
            } elseif ($this->_prev[0][0] === self::ADD_ACTION) {
                $this->_insertMultiple();
                $this->_prev = array($_row);
            } else {
                //should not happen
                throw new LogicException('Invalid action type for row');
            }
        }
        unset($_row);

        /**
         * If something remains unprocessed in the _prev array
         */
        if (count($this->_prev)) {
            if ($this->_prev[0][0] === self::DELETE_ACTION) {
                $this->_removeMultiple();
            } elseif ($this->_prev[0][0] === self::ADD_ACTION) {
                $this->_insertMultiple();
            } else {
                //should not happen
                throw new LogicException('Invalid action type for row');
            }
        }
        $this->_matches = [];
        $this->_prev = [];
    }

    /**
     * Builds DELETE statements and executes them
     * Iterates over the items withing the $_prev array
     * and builds the DELETE statements, always checking
     * if we approach the MySQL max_allowed_packet limit.
     * If we approach the limit too much, we execute the current
     * SQL statement and start building the statements for the remaining items
     */
    protected function _removeMultiple()
    {
        $sql = null;

        $remove = array();
        $this->_prev = array_reverse($this->_prev);
        while (($_row = array_pop($this->_prev)) !== null) {
            $remove[$_row[1][0]][] = array($_row[1][1], $_row[1][2]);
        }
        unset($_row);

        foreach ($remove as $websiteId => &$combinations) {
            $count = count($combinations);
            if ($count) {
                $sql = sprintf('DELETE FROM `%s` WHERE (website_id="%s" AND (', $this->_table, $websiteId);
                foreach ($combinations as $key => &$combination) {
                    $sql .= sprintf(
                        '((source_product_id="%s" AND target_product_id="%s") OR (source_product_id="%s" AND target_product_id="%s")) OR ',
                        $combination[0],
                        $combination[1],
                        $combination[1],
                        $combination[0]
                    );
                    unset($combinations[$key]);

                    if (strlen($sql) >= $this->_maxPacketsLength) {
                        $sql = trim($sql, ' OR ') . '))';
                        $this->getConnection()->query($sql);
                        $this->closeConnection();
                        $count--;
                        $sql = (0 === ($count)) ? null : sprintf('DELETE FROM `%s` WHERE (website_id="%s" AND (', $this->_table, $websiteId);
                    }
                }
            }

            if ($sql) {
                $sql = trim($sql, ' OR ') . '))';
                $this->getConnection()->query($sql);
                $this->closeConnection();
                $sql = null;
            }
            unset($remove[$websiteId]);
        }
        unset($combinations);
        unset($sql);
        unset($remove);
    }

    /**
     * Builds INSERT statements and executes them
     * Iterates over the items withing the $_prev array
     * and builds the INSERT statements, always checking
     * if we approach the MySQL max_allowed_packet limit.
     * If we approach the limit too much, we execute the current
     * SQL statement and start building the statements for the remaining items
     */
    protected function _insertMultiple()
    {
        $values = '';

        $add = array();
        $this->_prev = array_reverse($this->_prev);
        while (($_row = array_pop($this->_prev)) !== null) {
            $add[$_row[1][0]][] = array($_row[1][1], $_row[1][2]);
        }
        unset($_row);
        foreach ($add as $websiteId => &$combinations) {
            foreach ($combinations as $key => &$combination) {
                $values .= sprintf('("%s","%s","%s"),', $websiteId, $combination[0], $combination[1]);

                if (strlen($values) >= $this->_maxPacketsLength) {
                    $sql = sprintf(
                        'INSERT INTO `%s` (`website_id`,`source_product_id`,`target_product_id`) VALUES %s ON DUPLICATE KEY UPDATE `website_id`=VALUES(`website_id`);' . PHP_EOL,
                        $this->_table,
                        trim($values, ',')
                    );
                    $this->getConnection()->query($sql);
                    $this->closeConnection();
                    unset($sql);
                    $values = '';
                }
                unset($combinations[$key]);
            }
            unset($combination);
            unset($add[$websiteId]);
        }
        unset($combinations);
        unset($add);

        if ($values) {
            $sql = sprintf(
                'INSERT INTO `%s` (`website_id`,`source_product_id`,`target_product_id`) VALUES %s ON DUPLICATE KEY UPDATE `website_id`=VALUES(`website_id`);' . PHP_EOL,
                $this->_table,
                trim($values, ',')
            );
            unset($values);
            $this->getConnection()->query($sql);
            $this->closeConnection();
            unset($sql);
        }
    }

    /**
     * @param $left
     * @param $right
     * @param $websiteId
     */
    public function _addConditional($left, $right, $websiteId)
    {
        if ($left != $right) {
            $this->_matches[] = array(1, array($websiteId, min($left, $right), max($left, $right)));
            $this->_assertMemory();
        }
    }

    /**
     * @param $left
     * @param $right
     * @param $websiteId
     */
    protected function _removeConditional($left, $right, $websiteId)
    {
        $this->_matches[] = array(0, array($websiteId, min($left, $right), max($left, $right)));
        $this->_assertMemory();
    }

    /**
     * Reindex all
     */
    protected function _reindexAll()
    {
        /** @var Omnius_ProductMatchRule_Model_Resource_Rule_Collection $collection */
        $collection = Mage::getResourceModel('productmatchrule/rule_collection');
        $collection->setPageSize(self::BATCH_SIZE);

        $currentPage = 1;
        $pages = $collection->getLastPageNumber();
        $this->start();

        /**
         * Process the rules within batches to remove
         * the risk using all the allocated memory
         * only to load the rule collections
         * This way, we never load the whole
         * collection into the current memory
         */
        do {
            $collection->setCurPage($currentPage)->getSelect()
                ->reset(Zend_Db_Select::ORDER)
                ->order(new Zend_Db_Expr(
                    "CASE WHEN
                    `operation` <> " . Omnius_ProductMatchRule_Model_Rule::OP_DEFAULTED . "
                    AND `operation` <> " . Omnius_ProductMatchRule_Model_Rule::OP_OBLIGATED . "
                    THEN 1 ELSE 2 END,
                    priority ASC"));
            foreach ($collection as $rule) {
                $this->processItem($rule);
            }
            $currentPage++;
            $collection->clear();
        } while ($currentPage <= $pages);

        $this->end();
    }

    /**
     * Register indexer required data inside event object
     *
     * @param   Mage_Index_Model_Event $event
     */
    protected function _registerEvent(Mage_Index_Model_Event $event)
    {
        // no-op
    }

    /**
     * Process event based on event state data
     *
     * @param   Mage_Index_Model_Event $event
     */
    protected function _processEvent(Mage_Index_Model_Event $event)
    {
        // no-op
    }

    /**
     * Before importing mix match rules, remove all combination rules initially generated for it
     * @param int|null $website_id
     */
    public static function removeAllMixMatchRules($website_id = null)
    {
        $tableName = Mage::getResourceSingleton('productmatchrule/rule')->getMainTable();
        $connection = Mage::getSingleton('core/resource')->getConnection('core_write');

        $where = array(
            'rule_origin = ?' => Omnius_ProductMatchRule_Model_Rule::ORIGIN_IMPORT
        );
        if ($website_id) {
            $where['website_id = ?'] = $website_id;
        }
        $connection->delete($tableName, $where);
    }

    /**
     * @param $rule
     * @param $websiteId
     * @param $leftProdId
     * @param $rightProdId
     */
    protected function handleConditionals($rule, $websiteId, &$leftProdId, &$rightProdId)
    {
        switch ($rule['operation']) {
            case Omnius_ProductMatchRule_Model_Rule::OP_ALLOWED:
            case Omnius_ProductMatchRule_Model_Rule::OP_DEFAULTED:
            case Omnius_ProductMatchRule_Model_Rule::OP_OBLIGATED:
                if (isset($this->_productCategories[$websiteId][$rule['right_id']]) && isset($this->_productCategories[$websiteId][$rule['left_id']])) {
                    foreach ($this->_productCategories[$websiteId][$rule['right_id']] as &$leftProdId) {
                        foreach ($this->_productCategories[$websiteId][$rule['left_id']] as &$rightProdId) {
                            $this->_addConditional($leftProdId, $rightProdId, $websiteId);
                        }
                        unset($rightProdId);
                    }
                    unset($leftProdId);
                }

                return;
            case Omnius_ProductMatchRule_Model_Rule::OP_NOTALLOWED:
                if (isset($this->_productCategories[$websiteId][$rule['right_id']]) && isset($this->_productCategories[$websiteId][$rule['left_id']])) {
                    foreach ($this->_productCategories[$websiteId][$rule['right_id']] as &$rightProdId) {
                        foreach ($this->_productCategories[$websiteId][$rule['left_id']] as &$leftProdId) {
                            $this->_removeConditional($leftProdId, $rightProdId, $websiteId);
                        }
                        unset($leftProdId);
                    }
                    unset($rightProdId);
                }

                return;
            default:
                return;
        }
    }
}
