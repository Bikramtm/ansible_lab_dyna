<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_ProductMatchRule_Model_Indexer_Defaulted
 */
class Omnius_ProductMatchRule_Model_Indexer_Defaulted extends Omnius_ProductMatchRule_Model_Indexer_Abstract
{
    const ADD_MANDATORY = 0;
    const ADD_ACTION = 1;

    /** @var string */
    protected $_table;

    /** @var string */
    protected $_combinationTable;

    /** @var string */
    protected $_mandatoryTable;

    /** @var array */
    protected $_matches = array();

    /** @var array */
    protected $_mandatory = array();

    /** @var array */
    protected $_prev = array();

    /** @var array */
    protected $_allProductIds = array();

    /** @var array */
    protected $_allCategoryIds = array();

    /** @var array */
    protected $_productCategories = array();

    /** @var array */
    protected $_websites = array();

    /** @var array */
    protected $_websiteIds = array();

    /**
     * Initiate indexer
     */
    public function __construct()
    {
        parent::__construct();

        $this->_table = (string) Mage::getResourceSingleton('productmatchrule/defaulted')->getMainTable();
        $this->_mandatoryTable = (string) Mage::getResourceSingleton('productmatchrule/mandatory')->getMainTable();
        $this->_combinationTable = (string) Mage::getResourceSingleton('productmatchrule/matchRule')->getMainTable();

        $this->_init('omnius_productmatchrule/indexer_defaulted');
    }

    /**
     * Get Indexer name
     *
     * @return string
     */
    public function getName()
    {
        return 'Product defaulted rules';
    }

    /**
     * Get Indexer description
     *
     * @return string
     */
    public function getDescription()
    {
        return 'Product match rules that determine which products are selected by default';
    }

    /**
     * Empty index and generate whitelist based on rules
     */
    public function reindexAll()
    {
        $this->getConnection()->query(sprintf('TRUNCATE TABLE `%s`;', $this->_table));
        $this->getConnection()->query(sprintf('TRUNCATE TABLE `%s`;', $this->_mandatoryTable));
        $this->closeConnection();
        $this->_reindexAll();
    }

    /**
     * Reindex all
     */
    protected function _reindexAll()
    {
        /** @var Omnius_ProductMatchRule_Model_Resource_Rule_Collection $collection */
        $collection = Mage::getResourceModel('productmatchrule/rule_collection')
            ->addFieldToFilter('operation',
                array(
                    array('eq' => Omnius_ProductMatchRule_Model_Rule::OP_DEFAULTED),
                    array('eq' => Omnius_ProductMatchRule_Model_Rule::OP_OBLIGATED)
                )
            );
        $collection->setPageSize(self::BATCH_SIZE);

        $currentPage = 1;
        $pages = $collection->getLastPageNumber();
        $this->start();

        /**
         * Process the rules within batches to remove
         * the risk using all the allocated memory
         * only to load the rule collections
         * This way, we never load the whole
         * collection into the current memory
         */
        do {
            $collection->setCurPage($currentPage);
            $collection->load();
            foreach ($collection as $rule) {
                $this->processItem($rule);
            }
            $currentPage++;
            $collection->clear();
        } while ($currentPage <= $pages);

        $this->end();
    }

    /**
     * Executed before processing the collection
     */
    public function start()
    {
        $this->_websites = array();
        $this->_websiteIds = array_keys(Mage::app()->getWebsites());

        foreach ($this->_websiteIds as $websiteId) {
            $this->_websites[$websiteId] = array();
            $this->_productCategories[$websiteId] = Mage::getModel('productmatchrule/rule')->getAllCategoryProducts($websiteId);
        }

        //get id of all products to check if these are still available
        $this->_allProductIds = Mage::getResourceModel('catalog/product_collection')->getAllIds();
        $this->_allCategoryIds = Mage::getResourceModel('catalog/category_collection')->getAllIds();
    }

    /**
     * @param $rule Omnius_ProductMatchRule_Model_Rule
     */
    protected function processItem($rule)
    {
        $websiteId = $rule['website_id'];

        switch ($rule['operation_type']) {
            case Omnius_ProductMatchRule_Model_Rule::OP_TYPE_P2P:
                if (!in_array($rule['left_id'], $this->_allProductIds)
                    || !in_array($rule['right_id'], $this->_allProductIds)
                ) { break; }
                $this->addConditionalCategoryOpTypeP2P($rule, $websiteId);
                break;

            case Omnius_ProductMatchRule_Model_Rule::OP_TYPE_C2P:
                if (!in_array($rule['right_id'], $this->_allProductIds)
                    || !in_array($rule['left_id'], $this->_allCategoryIds)
                ) { break; }
                $this->addConditionalCategoryOpTypeC2P($rule, $websiteId);
                break;

            case Omnius_ProductMatchRule_Model_Rule::OP_TYPE_P2C:
                if (!in_array($rule['left_id'], $this->_allProductIds)
                    || !in_array($rule['right_id'], $this->_allCategoryIds)
                ) { break; }
                $this->addConditionalCategoryOpTypeP2C($rule, $websiteId);
                break;

            case Omnius_ProductMatchRule_Model_Rule::OP_TYPE_C2C:
                if (!in_array($rule['left_id'], $this->_allCategoryIds)
                    || !in_array($rule['right_id'], $this->_allCategoryIds)
                ) { break; }
                $this->addConditionalCategoryOpTypeC2C($rule, $websiteId);
                break;

            default:
                break;
        }
    }

    /**
     * @param $left
     * @param $right
     * @param $websiteId
     */
    protected function _addConditional($left, $right, $websiteId)
    {
        if ($left != $right) {
            $this->_matches[] = array(self::ADD_ACTION, array($websiteId, $left, $right));
            $this->_assertMemory();
        }
    }

    /**
     * Groups gathered items together by statement (INSERT/DELETE)
     * to decrease the number of statements executed on the database
     */
    protected function _applyChanges()
    {
        foreach ($this->_matches as $_key => &$_row) {
            if (!count($this->_prev)) {
                $this->_prev[] = $_row;
            } else {
                if ($_row[0] === $this->_prev[0][0]) {
                    $this->_prev[] = $_row;
                } else {
                    if ($this->_prev[0][0] === self::ADD_MANDATORY) {
                        $this->_insertMultipleCategories();
                    } elseif ($this->_prev[0][0] === self::ADD_ACTION) {
                        $this->_insertMultiple();
                    } else {
                        //should not happen
                        throw new LogicException('Invalid action type for row');
                    }
                    $this->_prev = array($_row);
                }
            }
            unset($this->_matches[$_key]);
        }
        unset($_row);

        /**
         * If something remains unprocessed in the _prev array
         */
        if (count($this->_prev)) {
            if ($this->_prev[0][0] === self::ADD_MANDATORY) {
                $this->_insertMultipleCategories();
            } elseif ($this->_prev[0][0] === self::ADD_ACTION) {
                $this->_insertMultiple();
            } else {
                //should not happen
                throw new LogicException('Invalid action type for row');
            }
        }
        $this->_matches = array();
        $this->_prev = array();
    }

    /**
     * Insert multiple categories
     */
    protected function _insertMultipleCategories()
    {
        $values = '';
        $valuesAllowed = '';

        $add = array();

        foreach ($this->_prev as $_key => &$_row) {
            $add[$_row[1][0]][] = array($_row[1][1], $_row[1][2]);
            unset($this->_prev[$_key]);
        }
        unset($_row);

        foreach ($add as $websiteId => &$combinations) {

            foreach ($combinations as $key => &$combination) {
                $values .= sprintf('("%s","%s","%s"),', $websiteId, $combination[1], $combination[0]);


                foreach ($this->_productCategories[$websiteId][$combination[1]] as &$leftProdId) {
                    $valuesAllowed .= sprintf('("%s","%s","%s"),', $websiteId, min($leftProdId, $combination[0]), max($leftProdId, $combination[0]));
                }

                unset($leftProdId);

                if (strlen($values) + strlen($valuesAllowed) >= $this->_maxPacketsLength) {
                    $sql = sprintf(
                        'INSERT INTO `%s` (`website_id`,`category_id`,`product_id`) VALUES %s ON DUPLICATE KEY UPDATE `website_id`=VALUES(`website_id`);' . PHP_EOL,
                        $this->_mandatoryTable,
                        trim($values, ',')
                    );


                    $sqlAllowed = sprintf(
                        'INSERT INTO `%s` (`website_id`,`source_product_id`,`target_product_id`) VALUES %s ON DUPLICATE KEY UPDATE `website_id`=VALUES(`website_id`);' . PHP_EOL,
                        $this->_combinationTable,
                        trim($valuesAllowed, ',')
                    );

                    $this->getConnection()->query($sql);
                    $this->getConnection()->query($sqlAllowed);
                    $this->closeConnection();
                    unset($sql);
                    unset($sqlAllowed);
                    $values = '';
                    $valuesAllowed = '';
                }
                unset($combinations[$key]);
            }
            unset($combination);
            unset($add[$websiteId]);
        }
        unset($combinations);
        unset($add);

        if ($values) {
            $sql = sprintf(
                'INSERT INTO `%s` (`website_id`,`category_id`,`product_id`) VALUES %s ON DUPLICATE KEY UPDATE `website_id`=VALUES(`website_id`);' . PHP_EOL,
                $this->_mandatoryTable,
                trim($values, ',')
            );

            unset($values);
            $this->getConnection()->query($sql);
            $this->closeConnection();
            unset($sql);
        }

        if ($valuesAllowed) {
            $sql = sprintf(
                'INSERT INTO `%s` (`website_id`,`source_product_id`,`target_product_id`) VALUES %s ON DUPLICATE KEY UPDATE `website_id`=VALUES(`website_id`);' . PHP_EOL,
                $this->_combinationTable,
                trim($valuesAllowed, ',')
            );

            unset($valuesAllowed);
            $this->getConnection()->query($sql);
            $this->closeConnection();
            unset($sql);
        }
    }

    /**
     * Builds INSERT statements and executes them
     * Iterates over the items withing the $_prev array
     * and builds the INSERT statements, always checking
     * if we approach the MySQL max_allowed_packet limit.
     * If we approach the limit too much, we execute the current
     * SQL statement and start building the statements for the remaining items
     */
    protected function _insertMultiple()
    {
        $values = '';
        $valuesAllowed = '';

        $add = array();
        foreach ($this->_prev as $_key => &$_row) {
            $add[$_row[1][0]][] = array($_row[1][1], $_row[1][2]);
            unset($this->_prev[$_key]);
        }
        unset($_row);

        foreach ($add as $websiteId => &$combinations) {
            foreach ($combinations as $key => &$combination) {
                $values .= sprintf('("%s","%s","%s"),', $websiteId, $combination[0], $combination[1]);
                $valuesAllowed .= sprintf('("%s","%s","%s"),', $websiteId, min($combination[0], $combination[1]), max($combination[0], $combination[1]));

                if (strlen($values) + strlen($valuesAllowed) >= $this->_maxPacketsLength) {
                    $sql = sprintf(
                        'INSERT INTO `%s` (`website_id`,`source_product_id`,`target_product_id`) VALUES %s ON DUPLICATE KEY UPDATE `website_id`=VALUES(`website_id`);' . PHP_EOL,
                        $this->_table,
                        trim($values, ',')
                    );

                    $sqlAllowed = sprintf(
                        'INSERT INTO `%s` (`website_id`,`source_product_id`,`target_product_id`) VALUES %s ON DUPLICATE KEY UPDATE `website_id`=VALUES(`website_id`);' . PHP_EOL,
                        $this->_combinationTable,
                        trim($valuesAllowed, ',')
                    );
                    $this->getConnection()->query($sql);
                    $this->getConnection()->query($sqlAllowed);
                    $this->closeConnection();
                    unset($sql);
                    unset($sqlAllowed);
                    $values = '';
                    $valuesAllowed = '';
                }
                unset($combinations[$key]);
            }
            unset($combination);
            unset($add[$websiteId]);
        }
        unset($combinations);
        unset($add);

        if ($values) {
            $sql = sprintf(
                'INSERT INTO `%s` (`website_id`,`source_product_id`,`target_product_id`) VALUES %s ON DUPLICATE KEY UPDATE `website_id`=VALUES(`website_id`);' . PHP_EOL,
                $this->_table,
                trim($values, ',')
            );

            unset($values);
            $this->getConnection()->query($sql);
            $this->closeConnection();
            unset($sql);
        }

        if ($valuesAllowed) {
            $sql = sprintf(
                'INSERT INTO `%s` (`website_id`,`source_product_id`,`target_product_id`) VALUES %s ON DUPLICATE KEY UPDATE `website_id`=VALUES(`website_id`);' . PHP_EOL,
                $this->_combinationTable,
                trim($valuesAllowed, ',')
            );

            unset($valuesAllowed);
            $this->getConnection()->query($sql);
            $this->closeConnection();
            unset($sql);
        }
    }

    /**
     * @param $left
     * @param $right
     * @param $websiteId
     */
    protected function _addConditionalCategory($left, $right, $websiteId)
    {
        $this->_matches[] = array(self::ADD_MANDATORY, array($websiteId, $left, $right));
        $this->_assertMemory();
    }

    /**
     * Executed after processing the collection
     */
    public function end()
    {
        $this->_applyChanges();
        parent::end();
    }

    /**
     * Register indexer required data inside event object
     *
     * @param   Mage_Index_Model_Event $event
     */
    protected function _registerEvent(Mage_Index_Model_Event $event)
    {
        // no-op
    }

    /**
     * Process event based on event state data
     *
     * @param   Mage_Index_Model_Event $event
     */
    protected function _processEvent(Mage_Index_Model_Event $event)
    {
        // no-op
    }

    /**
     * @param $rule
     * @param $websiteId
     */
    protected function addConditionalCategoryOpTypeP2P($rule, $websiteId)
    {
        if ($rule['operation'] == Omnius_ProductMatchRule_Model_Rule::OP_DEFAULTED) {
            $this->_addConditional($rule['left_id'], $rule['right_id'], $websiteId);
        }
    }

    /**
     * @param $rule
     * @param $websiteId
     */
    protected function addConditionalCategoryOpTypeC2P($rule, $websiteId)
    {
        if (($rule['operation'] == Omnius_ProductMatchRule_Model_Rule::OP_DEFAULTED)
            && isset($this->_productCategories[$websiteId][$rule['left_id']])
        ) {
            foreach ($this->_productCategories[$websiteId][$rule['left_id']] as &$catProdId) {
                $this->_addConditional($catProdId, $rule['right_id'], $websiteId);
            }
            unset($catProdId);
        }
    }

    /**
     * @param $rule
     * @param $websiteId
     */
    protected function addConditionalCategoryOpTypeP2C($rule, $websiteId)
    {
        if (($rule['operation'] == Omnius_ProductMatchRule_Model_Rule::OP_OBLIGATED)
            && isset($this->_productCategories[$websiteId][$rule['right_id']])
        ) {
            $this->_addConditionalCategory($rule['left_id'], $rule['right_id'], $websiteId);
        }
    }

    /**
     * @param $rule
     * @param $websiteId
     */
    protected function addConditionalCategoryOpTypeC2C($rule, $websiteId)
    {
        if (($rule['operation'] == Omnius_ProductMatchRule_Model_Rule::OP_OBLIGATED)
            && isset($this->_productCategories[$websiteId][$rule['left_id']])
            && isset($this->_productCategories[$websiteId][$rule['right_id']])
        ) {
            foreach ($this->_productCategories[$websiteId][$rule['left_id']] as &$rightProdId) {
                $this->_addConditionalCategory($rightProdId, $rule['right_id'], $websiteId);
            }
            unset($rightProdId);
        }
    }
}
