<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_ProductMatchRule_Model_Mandatory
 * @package MatchRule
 */
class Omnius_ProductMatchRule_Model_Mandatory extends Mage_Core_Model_Abstract
{

    public $_cache;

    protected function _construct()
    {
        $this->_init('productmatchrule/mandatory');
    }

    /**
     * @param $productId
     * @param $websiteId
     * @return array|mixed
     */
    public function getMandatoryFamilies($productId, $websiteId)
    {
        $cacheKey = sprintf('mandatory_categories_cache_%s_%s', $productId, $websiteId);
        if ($result = $this->getCache()->load($cacheKey)) {
            return unserialize($result);
        } else {
            $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
            $allowedLeft = Mage::getResourceModel('productmatchrule/mandatory_collection')
                ->addFieldToFilter('product_id', $productId)
                ->addFieldToFilter('website_id', $websiteId);
            $allowedLeft->getSelect()
                ->reset(Varien_Db_Select::COLUMNS)
                ->columns('category_id')
                ->distinct(true);

            $result = array_unique($connection->fetchCol($allowedLeft->getSelect()));
            unset($allowedLeft);
        }

        $this->getCache()->save(
            serialize($result),
            $cacheKey,
            [Dyna_Cache_Model_Cache::CACHE_TAG],
            $this->getCache()->getTtl()
        );

        return $result;
    }

    /**
     * @return Dyna_Cache_Model_Cache
     */
    protected function getCache()
    {
        if (!$this->_cache) {
            $this->_cache = Mage::getSingleton('dyna_cache/cache');
        }

        return $this->_cache;
    }
}