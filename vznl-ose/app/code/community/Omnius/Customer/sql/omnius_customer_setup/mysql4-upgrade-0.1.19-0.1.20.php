<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$resource = Mage::getSingleton('core/resource');
$writeConnection = $resource->getConnection('core_write');
$query = "UPDATE core_config_data SET value=REPLACE(value,'''','') WHERE path='form_validations/form_fields/name_fields'";
$writeConnection->query($query);