<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = new Mage_Customer_Model_Entity_Setup('core_setup');
$installer->startSetup();

$attributes = array(
    array(
        'entity' => 'customer',
        'attr_code' => 'account_no_long',
        'data' => array(
            'group'                => 'Default',
            'type'                 => 'varchar',
            'label'                => 'account_no_long',
            'note'                 => 'Account number long format',
            'input'                => 'text',
            'global'               => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
            'required'             => 0,
            'visible_on_front'     => 1,
            'used_for_price_rules' => 0,
            'adminhtml_only'       => 0,
            'sort_order'           => 350,
        )
    ),
);


foreach ($attributes as $attribute) {
    $installer->addAttribute($attribute['entity'], $attribute['attr_code'], $attribute['data']);

    $oAttribute = Mage::getSingleton('eav/config')->getAttribute($attribute['entity'], $attribute['attr_code']);
    $oAttribute->setData('used_in_forms', array(
            'adminhtml_customer',
            'checkout_register',
            'customer_account_create',
            'customer_account_edit',
        ));
    $oAttribute->save();
}

$installer->endSetup();
