<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;
/* @var $installer Mage_Catalog_Model_Resource_Eav_Mysql4_Setup */
$installer->startSetup();

$installer->run("
    INSERT INTO `{$installer->getTable('core/config_data')}` (scope, scope_id, path,`value`) VALUES ('default',0,'customer/address/street_lines','3') ON DUPLICATE KEY UPDATE `value`=VALUES(`value`);
");

$installer->endSetup();