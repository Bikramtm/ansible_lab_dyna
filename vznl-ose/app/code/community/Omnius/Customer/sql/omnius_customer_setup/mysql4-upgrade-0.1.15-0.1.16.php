<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$this->startSetup();
$setup = Mage::getResourceModel('catalog/setup','catalog_setup');
$setup->removeAttribute('customer','customer_ban');
$this->endSetup();