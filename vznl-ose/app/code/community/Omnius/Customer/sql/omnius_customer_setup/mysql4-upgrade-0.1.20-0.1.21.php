<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $customerInstaller Mage_Customer_Model_Resource_Setup */
$customerInstaller = new Mage_Customer_Model_Resource_Setup('core_setup');
$customerInstaller->startSetup();

$customerInstaller->getConnection()->addColumn($customerInstaller->getTable('customer/entity'), 'email_overwritten', [
    'type' => Varien_Db_Ddl_Table::TYPE_SMALLINT,
    'length' => 6,
    'nullable' => true,
    'required' => false,
    'default' => 0,
    'comment' => "Email overwritten",
]);

$customerInstaller->endSetup();