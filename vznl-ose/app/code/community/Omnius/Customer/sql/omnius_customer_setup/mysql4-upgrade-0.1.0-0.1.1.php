<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = new Mage_Customer_Model_Entity_Setup('core_setup');
$installer->startSetup();

$attributes = array(
    array(
        'entity' => 'customer',
        'attr_code' => 'id_number',
        'data' => array(
            'group'                => 'Default',
            'type'                 => 'varchar',
            'label'                => 'id_number',
            'note'                 => 'Customer ID Number',
            'input'                => 'text',
            'global'               => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
            'required'             => 0,
            'visible_on_front'     => 1,
            'used_for_price_rules' => 0,
            'adminhtml_only'       => 0,
            'sort_order'           => 290,
        )
    ),
    array(
        'entity' => 'customer',
        'attr_code' => 'id_type',
        'data' => array(
            'group'                => 'Default',
            'type'                 => 'varchar',
            'label'                => 'id_type',
            'note'                 => 'Customer ID Type',
            'input'                => 'text',
            'global'               => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
            'required'             => 0,
            'visible_on_front'     => 1,
            'used_for_price_rules' => 0,
            'adminhtml_only'       => 0,
            'sort_order'           => 300,
        )
    ),
    array(
        'entity' => 'customer',
        'attr_code' => 'issuing_country',
        'data' => array(
            'group'                => 'Default',
            'type'                 => 'varchar',
            'label'                => 'issuing_country',
            'note'                 => 'Customer ID Issuing country',
            'input'                => 'text',
            'global'               => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
            'required'             => 0,
            'visible_on_front'     => 1,
            'used_for_price_rules' => 0,
            'adminhtml_only'       => 0,
            'sort_order'           => 310,
        ),
    ),
    array(
        'entity' => 'customer',
        'attr_code' => 'valid_until',
        'data' => array(
            'group'                => 'Default',
            'type'                 => 'datetime',
            'label'                => 'valid_until',
            'note'                 => 'Customer ID valid until',
            'input'                => 'date',
            'global'               => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
            'required'             => 0,
            'visible_on_front'     => 1,
            'used_for_price_rules' => 0,
            'adminhtml_only'       => 0,
            'sort_order'           => 320,
        )
    ),
    array(
        'entity' => 'customer',
        'attr_code' => 'account_holder',
        'data' => array(
            'group'                => 'Default',
            'type'                 => 'varchar',
            'label'                => 'account_holder',
            'note'                 => 'Account holder',
            'input'                => 'text',
            'global'               => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
            'required'             => 0,
            'visible_on_front'     => 1,
            'used_for_price_rules' => 0,
            'adminhtml_only'       => 0,
            'sort_order'           => 340,
        )
    ),
    array(
        'entity' => 'customer',
        'attr_code' => 'account_no',
        'data' => array(
            'group'                => 'Default',
            'type'                 => 'varchar',
            'label'                => 'account_no',
            'note'                 => 'Account number',
            'input'                => 'text',
            'global'               => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
            'required'             => 0,
            'visible_on_front'     => 1,
            'used_for_price_rules' => 0,
            'adminhtml_only'       => 0,
            'sort_order'           => 350,
        )
    ),
);


foreach ($attributes as $attribute) {
    $installer->addAttribute($attribute['entity'], $attribute['attr_code'], $attribute['data']);

    $oAttribute = Mage::getSingleton('eav/config')->getAttribute($attribute['entity'], $attribute['attr_code']);
    $oAttribute->setData('used_in_forms', array(
            'adminhtml_customer',
            'checkout_register',
            'customer_account_create',
            'customer_account_edit',
        ));
    $oAttribute->save();
}

$installer->endSetup();
