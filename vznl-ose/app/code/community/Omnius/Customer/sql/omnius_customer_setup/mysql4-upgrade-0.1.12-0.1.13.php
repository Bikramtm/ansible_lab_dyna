<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/***************************************************************************
 * Customer attributes
 ***************************************************************************/
/* @var $customerInstaller Mage_Customer_Model_Resource_Setup */
$customerInstaller = new Mage_Customer_Model_Resource_Setup('core_setup');
$customerInstaller->startSetup();

$attributesInfo = array(
    'contact_id' => array(
        'label' => 'Contact ID',
        'type' => 'int',
        'input' => 'text',
        'visible' => true,
        'required' => false,
        'user_defined' => 1,
        'system' => 0,
        'note' => 'Customer contact created in Amdocs CRM'
    ),
);

foreach ($attributesInfo as $attributeCode => $attributeParams) {
    $customerInstaller->addAttribute('customer', $attributeCode, $attributeParams);
    $attribute = Mage::getSingleton('eav/config')->getAttribute('customer', $attributeCode);
    $attribute->setData(
        'used_in_forms',
        array(
            'adminhtml_customer',
            'customer_account_create',
            'customer_account_edit',
        )
    );
    $attribute->save();
}

$customerInstaller->endSetup();
