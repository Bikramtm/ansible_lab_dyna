<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/***************************************************************************
 * Customer attributes
 ***************************************************************************/
/* @var $customerInstaller Mage_Customer_Model_Resource_Setup */

//make sure attribute is associated to the customer prior to removing it
Mage::getSingleton('core/resource')->getConnection('core_write')->exec(
    'INSERT INTO customer_eav_attribute (attribute_id,is_visible) SELECT attribute_id,1 FROM eav_attribute WHERE attribute_code LIKE \'contact_id\' ON DUPLICATE KEY UPDATE is_visible=is_visible'
);

$customerInstaller = new Mage_Customer_Model_Resource_Setup('core_setup');
$customerInstaller->startSetup();

$customerInstaller->removeAttribute('customer','contact_id');

$attributesInfo = array(
    'contact_id' => array(
        'label' => 'Contact ID',
        'type' => 'varchar',
        'input' => 'text',
        'visible' => true,
        'required' => false,
        'user_defined' => 1,
        'system' => 0,
        'note' => 'Customer contact created in Amdocs CRM'
    ),
);

foreach ($attributesInfo as $attributeCode => $attributeParams) {
    $customerInstaller->addAttribute('customer', $attributeCode, $attributeParams);
    $attribute = Mage::getSingleton('eav/config')->getAttribute('customer', $attributeCode);
    $attribute->setData(
        'used_in_forms',
        array(
            'adminhtml_customer',
            'customer_account_create',
            'customer_account_edit',
        )
    );
    $attribute->save();
}

$customerInstaller->endSetup();

