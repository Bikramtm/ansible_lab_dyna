<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/***************************************************************************
 * Customer attributes
 ***************************************************************************/
/* @var $customerInstaller Mage_Customer_Model_Resource_Setup */
$customerInstaller = new Mage_Customer_Model_Resource_Setup('core_setup');
$customerInstaller->startSetup();

$attributesInfo = array(
    
    'company_region' => array(
        'label' => 'Company foreign address region',
        'type' => 'varchar',
        'input' => 'text',
        'visible' => true,
        'required' => false,
        'user_defined' => 1,
        'system' => 0
    ),
    'company_extra_lines' => array(
        'label' => 'Company foreign address extra lines followed by ;',
        'type' => 'varchar',
        'input' => 'text',
        'visible' => true,
        'required' => false,
        'user_defined' => 1,
        'system' => 0
    ),
);

foreach ($attributesInfo as $attributeCode => $attributeParams) {
    $customerInstaller->addAttribute('customer', $attributeCode, $attributeParams);
}

foreach ($attributesInfo as $attributeCode => $attributeParams) {
    $attribute = Mage::getSingleton('eav/config')->getAttribute('customer', $attributeCode);
    $attribute->setData(
        'used_in_forms',
        array(
            'adminhtml_customer',
            'checkout_register',
            'customer_account_create',
            'customer_account_edit',
        )
    );
    $attribute->save();
}

$customerInstaller->endSetup();
