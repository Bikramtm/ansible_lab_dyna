<?php
interface Omnius_Customer_Adapter_FactoryInterface
{
    public static function create();
}
