<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */


/**
 * Class Omnius_Customer_Model_System_General_Backend
 */
class Omnius_Customer_Model_System_Config_Backend
{
    /**
     * Options for the customer backend type
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array(
                'value' => 'Unify',
                'label' => 'Unify',
            ),
            array(
                'value' => 'Gemini',
                'label' => 'Gemini',
            ),
        );
    }

    /**
     * Get the currently selected customer backend type
     * @return string
     */
    public function getSelected()
    {
        return trim(Mage::getStoreConfig('vodafone_customer/default_backend_type/selected_type'));
    }
}
