<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */


/**
 * Class Omnius_Customer_Mode_Customer_Resource_Customer
 */
class Omnius_Customer_Model_Customer_Resource_Customer extends Mage_Customer_Model_Resource_Customer
{
    /**
     * Add ban to the default fields
     * 
     * @return array
     */
    protected function _getDefaultAttributes()
    {
        return array_unique(array_merge(array('ban','email_overwritten'), parent::_getDefaultAttributes()), SORT_REGULAR);
    }
    
    /**
     * Delete the old customer campaigns and add the new ones
     *
     * @param Varien_Object $object
     * @return Mage_Core_Model_Resource_Db_Abstract|void
     */
    protected function _beforeSave(Varien_Object $object)
    {
        if ($object->hasDataChanges() && !Mage::getSingleton('customer/session')->getIsImport()) {

            $adapter = $this->_getWriteAdapter();
            $connection = $this->getWriteConnection();

            if (Mage::app()->getStore()->isAdmin() || Mage::getDesign()->getArea() == 'adminhtml') {
                $object = $this->saveBackendSetCampaigns($object);
            }

            if ($campaigns = $object->getData('campaigns')) {
                $data = array();
                $campaignIds = array();
                foreach ($campaigns as $campaign) {
                    array_push($data, array('campaign_id' => $campaign->getId(), 'customer_id' => $object->getId()));
                    array_push($campaignIds, $campaign->getId());
                }

                $connection->beginTransaction();
                try {
                    /**
                     * Find already existing connections
                     * Delete all previous keys, and add the new ones
                     */
                    $adapter->delete(Mage::getSingleton('core/resource')->getTableName('customer_campaign_link'), sprintf('customer_id = %s', $adapter->quote($object->getId())));

                    foreach ($data as $row) {
                        $connection->insert(Mage::getSingleton('core/resource')->getTableName('customer_campaign_link'), $row);
                    }
                    $connection->commit();
                } catch (Exception $e) {
                    $connection->rollBack();
                    Mage::getSingleton('core/logger')->logException($e);
                }
            }

            $object = $this->saveCampaignsIdsForBackend($object);

        }

        //make sure email is unqiue
        $object->setEmail(Mage::helper('omnius_customer')->generateDummyEmail());
        parent::_beforeSave($object);
    }

    /**
     * Load campaings and ctns
     *
     * @param Varien_Object $object
     * @return Mage_Eav_Model_Entity_Abstract|void
     */
    protected function _afterLoad(Varien_Object $object)
    {
        $object = $this->loadCampaigns($object);
        $object = $this->loadCtns($object);
        
        parent::_afterLoad($object);
    }

    /**
     * Load the collection of campaigns for this customer
     *
     * @param Mage_Core_Model_Abstract $object
     * @return Mage_Core_Model_Abstract
     */
    protected function loadCampaigns(Mage_Core_Model_Abstract $object)
    {
        $adapter = $this->_getWriteAdapter();
        $bind = array('customer_id' => $object->getId());
        $select = $adapter->select()
            ->from('customer_campaign_link', array('campaign_id'))
            ->where('customer_id = :customer_id');
        $result = $adapter->fetchCol($select, $bind);
        $collection = new Varien_Data_Collection();
        if ($result) {
            $campaigns = Mage::getResourceSingleton('campaign/campaign_collection')
                ->addFieldToFilter('entity_id', array('in' => $result))
                ->load();
            foreach ($campaigns->getItems() as $campaign) {
                $collection->addItem($campaign);
            }
            $object->setData('campaigns', $collection);
        } else {
            $object->setData('campaigns', $collection);
        }

        return $object;
    }

    /**
     * Load the collection of ctns for this customer
     *
     * @param Mage_Core_Model_Abstract $object
     * @return Varien_Object
     */
    protected function loadCtns(Mage_Core_Model_Abstract $object)
    {
        /** @var Omnius_Ctn_Model_Mysql4_Ctn_Collection $ctns */
        $ctns = Mage::getResourceModel('ctn/ctn_collection')
            ->addFieldToFilter('customer_id', $object->getId())
            ->load();

        $collection = new Varien_Data_Collection();
        foreach ($ctns->getItems() as $ctn) {
            $collection->addItem($ctn);
        }

        return $object->setData('ctn', $collection);
    }

    /**
     * Set the backend campaigns
     *
     * @param Varien_Object $object
     * @return Varien_Object
     */
    protected function saveBackendSetCampaigns(Varien_Object $object)
    {
        $attributeOptions = array();
        foreach ($object->getAttribute('campaign')->getSource()->getAllOptions() as $option) {
            $attributeOptions[$option['value']] = $option['label'];
        }
        $customerCampaignsIds = array_map('trim', explode(',', $object->getData('campaign')));
        $customerCampaignsNames = array();
        foreach ($customerCampaignsIds as $campaignId) {
            if (isset($attributeOptions[$campaignId])) {
                array_push($customerCampaignsNames, $campaignId);
            }
        }

        $customerCampaigns = Mage::getResourceModel('campaign/campaign_collection')
            ->addFieldToFilter('entity_id', array('in' => $customerCampaignsNames));

        $object->setData('campaigns', new Varien_Data_Collection());

        foreach ($customerCampaigns as $campaign) {
            if ( ! $object->getCampaigns()->getItemByColumnValue('entity_id', $campaign->getId())) {
                $object->getCampaigns()->addItem($campaign);
            }
        }

        return $object;
    }

    /**
     * Set the backend campaign ids
     *
     * @param Varien_Object $object
     * @return Varien_Object
     */
    protected function saveCampaignsIdsForBackend(Varien_Object $object)
    {
        $attributeOptions = array();
        foreach ($object->getAttribute('campaign')->getSource()->getAllOptions() as $option) {
            $attributeOptions[$option['value']] = $option['label'];
        }

        if ( ! $object->getCampaigns()) {
            $object->setData('campaigns', new Varien_Data_Collection());
        }
        $campaignsIds = array();
        foreach ($object->getCampaigns()->getItems() as $campaign) {
            if (isset($attributeOptions[$campaign->getId()])) {
                array_push($campaignsIds, $campaign->getId());
            }
        }

        $object->setData('campaign', join(',', $campaignsIds));

        return $object;
    }
}
