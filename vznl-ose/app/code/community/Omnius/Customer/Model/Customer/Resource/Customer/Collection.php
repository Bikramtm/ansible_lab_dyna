<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */


/**
 * Class Collection
 */
class Omnius_Customer_Model_Customer_Resource_Customer_Collection extends Mage_Customer_Model_Resource_Customer_Collection
{
    /**
     * Filter collection by ctn
     *
     * @param $ctn
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     */
    public function filterByCtn($ctn)
    {
        $adapter = $this->getResource()->getReadConnection();
        if (strpos($ctn, '06') === 0) {
            $ctn = '31' . substr($ctn, 1);
        } elseif (strpos($ctn, '0031') === 0) {
            $ctn = '31' . substr($ctn, 4);
        } elseif (strpos($ctn, '+31') === 0) {
            $ctn = '31' . substr($ctn, 3);
        }

        $bind = array('ctn' => $ctn);
        $select = $adapter->select()
            ->from(Mage::getSingleton('core/resource')->getTableName('ctn/ctn'), array('customer_id'))
            ->where('ctn = :ctn');
        $ids = $adapter->fetchCol($select, $bind);

        return $this->addAttributeToFilter(
            0 === count($ids)
                ? array(array('attribute' => 'customer_ctn', array('eq' => $ctn)))
                : array(
                array('attribute' => 'customer_ctn', array('eq' => $ctn)),
                array('attribute' => 'entity_id', 'in' => $ids)
            )
        );
    }

    /**
     * Filter collection based on a specified field and value
     *
     * @param $field
     * @param $value
     * @param bool $strict
     * @return Omnius_Customer_Model_Customer_Resource_Customer_Collection|Mage_Eav_Model_Entity_Collection_Abstract
     */
    public function filterBy($field, $value, $strict = false)
    {
        if ($field == 'billing_postcode') {
            $prefix1 = str_replace(' ', '', $value);
            $prefix2 = substr($value, 0, 4) . ' ' . substr($value, 4);

            $this->addAttributeToFilter(array(
                array('attribute' => $field, 'like' => '%' . $prefix1 . '%'),
                array('attribute' => $field, 'like' => '%' . $prefix2 . '%'),
            ));
        } elseif ($field == 'billing_street') {
            $street1 = str_replace(' ', '', $value);
            $street2 = str_replace(' ', "\r\n", $value);

            $this->addAttributeToFilter(array(
                array('attribute' => $field, 'like' => '%' . $street1 . '%'),
                array('attribute' => $field, 'like' => '%' . $street2 . '%'),
                array('attribute' => $field, 'like' => '%' . $value . '%')
            ));
        } elseif ($field == 'customer_ctn') {
            $this->filterByCtn($value);
        } else {
            $strict
                ? $this->addAttributeToFilter($field, array('eq' => $value))
                : $this->addAttributeToFilter($field, array('like' => '%' . $value . '%'));
        }

        return $this;
    }
}
