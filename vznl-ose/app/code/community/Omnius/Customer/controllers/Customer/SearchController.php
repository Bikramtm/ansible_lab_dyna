<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */


/**
 * Class SearchController
 */
 
// TODO: Refactor search logic to model
class Omnius_Customer_Customer_SearchController extends Mage_Core_Controller_Front_Action
{
    protected $acceptedFields = array('customer_ctn', 'company_coc', 'pin', 'customer_type', 'billing_postcode', 'billing_house_number', 'billing_street', 'company_name', 'ban', 'firstname', 'middlename', 'lastname', 'email', 'dob', 'entity_id', 'order_number');
    protected $_limit = null;

    protected $_attributeIds = array();
    protected $_readConnection = null;
    /** @var Dyna_Cache_Model_Cache */
    protected $_cache = null;
    const LIMIT_PATH = 'vodafone_customer/search/limit';

    /**
     * @param $handle
     * @param $typeId
     * @return bool|false|Mage_Core_Model_Abstract|mixed
     */
    private function _getAttributeId($handle, $typeId)
    {
        if (isset($this->_attributeIds[$typeId . '_' . $handle])) {
            return $this->_attributeIds[$typeId . '_' . $handle];
        }

        $key = serialize([__METHOD__, $handle, $typeId]);
        if (!$attributeId = $this->getCache()->load($key)) {
            $attributeId = $this->_readConnection
                ->fetchOne("SELECT attribute_id FROM eav_attribute WHERE entity_type_id = :type AND attribute_code = ':code'", array(
                    ':type' => $typeId,
                    ':code' => $handle
                ));
            $this->getCache()->save($attributeId, $key, [Dyna_Cache_Model_Cache::CACHE_TAG], $this->getCache()->getTtl());
        }
        $this->_attributeIds[$typeId . '_' . $handle] = $attributeId;

        return $attributeId;
    }

    /**
     * Get the backend limit for the customer search
     * 
     * @return mixed
     */
    private function _getLimit()
    {
        if ($this->_limit === null) {
            $storeConfig = (int) trim(Mage::getStoreConfig(self::LIMIT_PATH));
            $this->_limit = $storeConfig ?: 10;
        }

        return $this->_limit;
    }
    
    /**
     * @param $collection Omnius_Customer_Model_Customer_Resource_Customer_Collection
     * @param $fields
     */
    private function _getHits(&$collection, $fields) {
        $select = $collection->getSelect();
        $select->joinLeft(['cae'=>'customer_address_entity'], 'cae.parent_id = e.entity_id','');
        foreach ($fields as $field => $value) {
            switch ($field) {
                case 'customer_ctn':
                    $select->joinLeft(['cc' => 'customer_ctn'], 'cc.customer_id = e.entity_id', "");
                    $select->where('cc.ctn = ?', $value);
                    break;
                case 'billing_postcode':
                    $select->joinLeft(['caev'=>'customer_address_entity_varchar'], 'caev.entity_id = cae.entity_id AND caev.attribute_id = '.$this->_getAttributeId('postcode', 2),'');
                    $select->where('caev.value = "' . $value[0] . '" OR caev.value = "' . $value[1] . '"');
                    break;
                case 'billing_street':
                    $select->joinLeft(['caev2'=>'customer_address_entity_text'], 'caev2.entity_id = cae.entity_id AND caev2.attribute_id = '.$this->_getAttributeId('street', 2),'');
                    $select->where('caev2.value LIKE ?', $value);
                    break;
                case 'billing_house_number':
                    $select->joinLeft(['caev3'=>'customer_address_entity_varchar'], 'caev3.entity_id = cae.entity_id AND caev3.attribute_id = '.$this->_getAttributeId('house_number', 2),'');
                    $select->where('caev3.value = ?', $value);
                    break;
                case 'company_name':
                    $select->joinLeft(['cev1' => 'customer_entity_varchar'], '(cev1.entity_id = e.entity_id) AND (cev1.attribute_id=' . $this->_getAttributeId('company_name', 1) . ')', "");
                    $select->where('cev1.value LIKE ?', $value . '%');
                    break;
                case 'ban':
                    $select->where('e.ban = ?', $value);
                    break;
                case 'firstname':
                    $select->joinLeft(['cev2' => 'customer_entity_varchar'], '(cev2.entity_id = e.entity_id) AND (cev2.attribute_id=' . $this->_getAttributeId('firstname', 1) . ')', "");
                    $select->where('cev2.value LIKE ?', $value . '%');
                    break;
                case 'lastname':
                    $select->joinLeft(['cev3' => 'customer_entity_varchar'], '(cev3.entity_id = e.entity_id) AND (cev3.attribute_id=' . $this->_getAttributeId('lastname', 1) . ')', "");
                    $select->where('cev3.value LIKE ?', $value . '%');
                    break;
                case 'email':
                    $select->joinLeft(['cev4' => 'customer_entity_varchar'], '(cev4.entity_id = e.entity_id) AND (cev4.attribute_id=' . $this->_getAttributeId('additional_email', 1) . ')', "");
                    $select->where('cev4.value LIKE ?', $value . '%');
                    break;
                case 'dob':
                    $select->joinLeft(['ced' => 'customer_entity_datetime'], '(ced.entity_id = e.entity_id) AND (ced.attribute_id=' . $this->_getAttributeId('dob', 1) . ')', "");
                    $select->where('ced.value = ?', $value);
                    break;
                case 'company_coc':
                    $select->joinLeft(['cev5' => 'customer_entity_varchar'], '(cev5.entity_id = e.entity_id) AND (cev5.attribute_id=' . $this->_getAttributeId('company_coc', 1) . ')', "");
                    $select->where('cev5.value LIKE ?', $value . '%');
                    break;
                case 'order_number':
                    $select->joinLeft(['so' => 'superorder'], 'so.customer_id = e.entity_id', "");
                    $select->where('so.order_number = ?', $value);
                    break;
                default:
                    //do nothing
                    break;
            }
        }
        $select->group('e.entity_id');
        if ($this->_getLimit()) {
            $select->limit($this->_getLimit() + 1);
        }
    }

    /**
     * Customer search
     * @IsGranted({"permissions":["SEARCH_CUSTOMER"]})
     */
    public function indexAction()
    {
        $this->_readConnection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $requestParameters = $this->getRequest()->getParams();

        $dataToCollect = array('entity_id', 'is_prospect', 'customer_type', 'additional_email', 'firstname', 'lastname', 'middlename', 'customer_ctn', 'ban', 'company_coc', 'billing_address', 'billing_street', 'billing_city', 'billing_postcode', 'billing_house_number', 'company_name', 'is_business', 'gender', 'customer_label', 'prefix');

        //remove empty parameters
        foreach ($requestParameters as $key => $value) {
            if (!in_array($key, $this->acceptedFields) || !trim($value)) {
                unset($requestParameters[$key]);
            }
        }
        $errorFields = $this->validateSearchParameters($requestParameters);

        $parameters = new Varien_Object($requestParameters);
        if (empty($errorFields) && (count($parameters->getData()) || $this->getRequest()->getPost('quote_number'))) {
            $result = $this->getSearchResults($parameters, $dataToCollect);
        } else {
            //return No search filter provided and the array with error inputted fields
            $result = array(
                'message' => $this->__('No search filter provided'),
                'error_fields' => $errorFields,
            );
        }

        $this->getResponse()
            ->setHeader('Content-Type', 'application/json')
            ->setBody(Mage::helper('core')->jsonEncode($result));

        return true;
    }

    /**
     * @param array $requestParameters
     * @return Mage_Core_Model_Abstract|Mage_Customer_Model_Customer|mixed
     * @throws Exception
     */
    protected function fetchCustomer(array $requestParameters)
    {
        // Validate date format
        if (!empty($requestParameters['dob']) && !Mage::helper('omnius_validators')->validateIsDate($requestParameters['dob'])) {
            throw new Exception($this->__('The date is of an invalid format.'));
        }

        if (isset($requestParameters['company_coc']) && (trim($requestParameters['company_coc']) || $requestParameters['company_coc'] === '0')) {
            $requestParameters['is_business'] = true;
        }

        return Mage::helper('omnius_customer')->fetchCustomer($requestParameters);
    }

    /**
     * Sets up the current customer session
     * @param $customer
     * @param $customerId
     * @param $currentQuote
     * @return array
     */
    private function logCustomer($customer)
    {
        $customerId = $customer->getId();
        $currentQuote = Mage::getSingleton('checkout/cart')->getQuote();
        //if customer exists, load the entity to retrieve the campaigns, ctn records and other data
        $session = Mage::getSingleton('customer/session');
        $session->setCustomer($customer);
        $session->setCustomerInfoSync(true);

        /*
         * and if customer was already logged out when loading a new customer, this quote will be lost,
         * otherwise, if this quote was not associated to any customer, it will be associated to this customer that is about to be loaded
         */
        if ($currentQuote->getCustomerId()) {
            Mage::helper('omnius_checkout')->createNewQuote();
        } else {
            $currentQuote->setCustomerId($customerId)->save();
            Mage::getSingleton('checkout/cart')->setQuote($currentQuote);
            Mage::getSingleton('checkout/session')->setQuoteId($currentQuote->getId());
        }

        $defaultBillingAddress = Mage::getModel('customer/address')->load($customer->getData('default_billing'));

        return array(
            'sticker' => $this->loadLayout('page_three_columns')->getLayout()->getBlock('customer.details')->toHtml(),
            'saveCartModal' => $this->getLayout()->createBlock('core/template')->setTemplate('page/html/save_cart_modal.phtml')->toHtml(),
            'customerData' => $customer->getCustomData(),
            'addressData' => $defaultBillingAddress->getData()
        );
    }

    /**
     * Checks CTN validity
     * @param $value
     * @return bool
     */
    private function checkCtn($value)
    {
        $valid = false;
        if (strpos($value, '06') === 0 || strpos($value, '0031') === 0 || substr($value, 0, 2) == '31' || strpos($value, '+31') === 0) {
            $valid = true;
        }

        return strpos($value, ' ') === false && $valid;
    }

    /**
     * @return Dyna_Cache_Model_Cache
     */
    protected function getCache()
    {
        if ( ! $this->_cache) {
            $this->_cache = Mage::getSingleton('dyna_cache/cache');
        }

        return $this->_cache;
    }

    /**
     * @param $requestParameters
     * @return array
     */
    protected function validateSearchParameters($requestParameters)
    {
        $errorFields = [];
        /** @var Omnius_Validators_Helper_Data $validator */
        $validator = Mage::helper('omnius_validators');
        foreach ($requestParameters as $key => $value) {
            if (!empty($value) && trim($value)) {
                switch ($key) {
                    case 'dob':
                        if (!$validator->validateIsDate($value)) {
                            unset($requestParameters['dob']);
                            $errorFields[] = $this->__("Invalid date of birth");
                        }
                        break;
                    case 'company_coc' :
                        if (!$validator->validateCoc($value)) {
                            unset($requestParameters['company_coc']);
                            $errorFields[] = $this->__("Invalid COC number");
                        }
                        break;
                    case 'email' :
                        if (!$validator->validateEmailSyntax($value)) {
                            unset($requestParameters['email']);
                            $errorFields[] = $this->__("Invalid e-mail address");
                        }
                        break;
                    case 'customer_ctn' :
                        if (!$validator->validateIsNumber($value)) {
                            unset($requestParameters['customer_ctn']);
                            $errorFields[] = $this->__("Invalid mobile number");
                        }
                        break;
                    case 'ban' :
                        if (!$validator->validateIsNumber($value)) {
                            unset($requestParameters['ban']);
                            $errorFields[] = $this->__("Invalid ban number");
                        }
                        break;
                    case 'order_number' :
                        if (!$validator->validateIsNumber($value)) {
                            unset($requestParameters['order_number']);
                            $errorFields[] = $this->__("Invalid order number");
                        }
                        break;
                    default:
                        //do nothing
                        break;
                }
            }
        }

        return $errorFields;
    }

    /**
     * @param $parameters
     * @return array
     */
    protected function getSearchFieldsData($parameters)
    {
        /** @var Omnius_Customer_Helper_Data $customerHelper */
        $customerHelper = Mage::helper('omnius_customer');
        //filter through each provided parameters
        $searchFields = [];
        foreach ($this->acceptedFields as $field) {
            if ($value = $parameters->getData($field)) {
                switch ($field) {
                    case 'customer_ctn':
                        $value = $customerHelper->parseCtn($value);
                        break;
                    case 'billing_postcode':
                        $value = $customerHelper->formatPostCode($value);
                        break;
                    case 'billing_street':
                        $value = $customerHelper->formatHouseNumber($value);
                        break;
                    default:
                        //do nothing
                        break;
                }
                $searchFields[$field] = $value;
            }
        }

        return $searchFields;
    }

    /**
     * @param $parameters
     * @param $dataToCollect
     * @return array
     */
    protected function getSearchResults($parameters, $dataToCollect)
    {
        $searchFields = $this->getSearchFieldsData($parameters);
        $result = [];
        if (count($parameters->getData())) {
            /** @var Omnius_Customer_Model_Customer_Resource_Customer_Collection $customerCollection */
            $customerCollection = Mage::getResourceSingleton('customer/customer_collection')
                ->addAttributeToSelect($dataToCollect)
                ->joinAttribute('is_prospect_3', 'customer/is_prospect', 'is_prospect', null, 'left')
                ->joinAttribute('billing_street', 'customer_address/street', 'default_billing', null, 'left')
                ->joinAttribute('billing_postcode', 'customer_address/postcode', 'default_billing', null, 'left')
                ->joinAttribute('billing_city', 'customer_address/city', 'default_billing', null, 'left');
            $this->_getHits($customerCollection, $searchFields);

            $result = $customerCollection
                ->load()
                ->toArray($dataToCollect);
        }

        //serialize result or show No results found
        $result = $result ? array_values($result) : array('message' => $this->__('No results found'));

        foreach ($result as &$record) {
            if (is_array($record)) {
                $record['prefix'] = (isset($record['prefix']) && $record['prefix']) ? $this->__($record['prefix']) : '';
                $record['customer_label'] = (isset($record['customer_label']) && $record['customer_label']) ? $record['customer_label'] : '';
                $record['is_prospect'] = (isset($record['is_prospect']) && $record['is_prospect']) ? $record['additional_email'] : '0';
                if (isset($record['additional_email'])) {
                    unset($record['additional_email']);
                }
            }
        }
        unset($record);

        // If the limit was breached, show warning
        if (count($result) === $this->_getLimit() + 1) {
            array_pop($result);
            $result[] = ['limit_reached' => sprintf($this->__('Warning: only the first %s results are displayed. Use additional search parameters when the correct customer is not in the selection.'), $this->_getLimit())];
        }

        return $result;
    }

    /**
     * @param $requestParameters
     * @param $orderNumber
     * @param $orderFound
     * @param $superOrder
     */
    protected function doLoginCustomer($requestParameters, $orderNumber, $orderFound, $superOrder)
    {
        try {
            if (count($requestParameters)) {
                $requestParameters['advanced_search'] = true;
                try {
                    $customer = $this->fetchCustomer($requestParameters);
                } catch (Exception $e) {
                    // When logging the customer on indirect if the search was done by order number, let the customer login
                    if (isset($orderNumber) && $orderNumber && $orderFound) {
                        $customer = Mage::getModel('customer/customer')->load($superOrder[0]['customer_id']);
                    } else {
                        throw $e;
                    }
                }
                $returnArray = $this->logCustomer($customer);
            } else {
                $returnArray = array(
                    'error' => true,
                    'message' => Mage::helper('omnius_customer')->__(!$orderFound ? 'Order not found' : 'Please fill the form fields.'),
                );
            }
            $this->getResponse()
                ->setHeader('Content-Type', 'application/json')
                ->setBody(Mage::helper('core')->jsonEncode($returnArray));
        } catch (Exception $e) {
            Mage::helper('omnius_core')->returnError($e);
        }
    }

    /**
     * @param $requestParameters
     * @param $orderNumber
     * @param $orderFound
     * @param $superOrder
     */
    protected function validateSearchData(&$requestParameters, &$orderNumber, &$orderFound, &$superOrder)
    {
        $akeCond1 = array_key_exists('customer_ctn', $requestParameters) && array_key_exists('dob', $requestParameters);
        $akeCond2 = array_key_exists('customer_ctn', $requestParameters) && array_key_exists('company_coc', $requestParameters);
        $akeCond3 = array_key_exists('company_coc', $requestParameters) && array_key_exists('ban', $requestParameters);
        if (!($akeCond1 || $akeCond2 || $akeCond3 || array_key_exists('order_number', $requestParameters))) {
            throw new RuntimeException($this->__('Customers can search sole basis of CTN + Date of birth, CTN + CoC number, BAN + CoC number'));
        }

        /** Validate customer CTN */
        if (isset($requestParameters['customer_ctn'])) {
            $ctnToValidate = trim($requestParameters['customer_ctn']);
            if (!$this->checkCtn($ctnToValidate)) {
                throw new RuntimeException($this->__('The CTN format is invalid.'));
            }
        }

        if (isset($requestParameters['order_number'])) {
            $orderNumber = trim($requestParameters['order_number']);
            unset($requestParameters['order_number']);
        }

        if (isset($orderNumber) && $orderNumber) {
            $orderFound = false;
            $conn = Mage::getSingleton('core/resource')->getConnection('core_read');
            $dealerId = Mage::getSingleton('customer/session')->getAgent(true)->getDealer()->getId();
            $superOrder = $conn
            ->fetchAll('SELECT * FROM `superorder` WHERE `order_number`=:order', array(':order' => $conn->quote((string) $orderNumber)));

            if ($superOrder) {
                if ($superOrder[0]['created_dealer_id'] != $dealerId) {
                    throw new RuntimeException($this->__('Order created by different dealer, no authorisation to view order.'));
                }
                $orderFound = true;
                $requestParameters['entity_id'] = $superOrder[0]['customer_id'];
            } else {
                throw new RuntimeException($this->__('Order not found'));
            }
        }
    }
}
