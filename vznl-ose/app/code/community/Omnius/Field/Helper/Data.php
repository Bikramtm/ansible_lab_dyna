<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Field_Helper_Data
 */
class Omnius_Field_Helper_Data extends Mage_Core_Helper_Data
{
    /** @var Dyna_Cache_Model_Cache */
    protected $_cache;

    /**
     * Get all fields default values from specific section
     * @param int|string $section
     * @return mixed
     */
    public function getDefaultsForSection($section)
    {
        $key = serialize(array(strtolower(__METHOD__), func_get_args()));
        if ($result = unserialize($this->getCache()->load($key))) {
            return $result;
        } else {
            $result = Mage::getResourceModel('field/field_collection')
                ->addFieldToFilter('section_id', is_numeric($section) ? $section : $this->getSectionId($section));

            $this->getCache()->save(serialize($result), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());

            return $result;
        }
    }

    /**
     * @param $fieldName
     * @return mixed
     */
    public function getDefault($fieldName)
    {
        $key = serialize(array(strtolower(__METHOD__), func_get_args()));
        if ($result = unserialize($this->getCache()->load($key))) {
            return $result;
        } else {
            $result = Mage::getResourceModel('field/field_collection')
                ->addFieldToFilter('name', $fieldName)
                ->getFirstItem();

            $this->getCache()->save(serialize($result), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());

            return $result;
        }
    }

    /**
     * @param $fieldName
     * @param $section
     * @return mixed
     */
    public function getDefaultValue($fieldName, $section)
    {
        $key = serialize(array(strtolower(__METHOD__), func_get_args()));
        if ($result = unserialize($this->getCache()->load($key))) {
            return $result;
        } else {
            $result = Mage::getResourceModel('field/field_collection')
                ->addFieldToFilter('section_id', is_numeric($section) ? $section : $this->getSectionId($section))
                ->addFieldToFilter('name', $fieldName)
                ->getFirstItem()
                ->getDefaultValue();

            $this->getCache()->save(serialize($result), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());

            return $result;
        }
    }

    /**
     * @param $name
     * @return mixed
     */
    protected function getSectionId($name)
    {
        $key = serialize(array(strtolower(__METHOD__), func_get_args()));
        if ($result = unserialize($this->getCache()->load($key))) {
            return $result->getId();
        } else {
            $section = Mage::getResourceModel('field/section_collection')
                ->addFieldToFilter('name', $name)
                ->load()
                ->getFirstItem();

            $this->getCache()->save(serialize($section), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());

            return $section->getId();
        }
    }

    /**
     * Get cache instance
     * @return Dyna_Cache_Model_Cache
     */

    protected function getCache()
    {
        if ( ! $this->_cache) {
            $this->_cache = Mage::getSingleton('dyna_cache/cache');
        }
        return $this->_cache;
    }
}