<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;

/**
 * Create table 'dyna_service_error_message'
 */
if ( ! $installer->getConnection()->isTableExists($installer->getTable('field/message'))) {
    $table = $installer->getConnection()
        ->newTable($installer->getTable('field/message'))
        ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array('identity'  => true,'unsigned'  => true,'nullable'  => false,'primary'   => true), 'ID')
        ->addColumn('service_code', Varien_Db_Ddl_Table::TYPE_VARCHAR, 64, array(), 'Service Code')
        ->addColumn('description', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(), 'Description')
        ->addColumn('response_code', Varien_Db_Ddl_Table::TYPE_VARCHAR, 64, array(), 'Response Code')
        ->addColumn('response_message', Varien_Db_Ddl_Table::TYPE_TEXT, 65535, array(), 'Response message')
        ->addIndex(
            'response_code_index',
            array('response_code'),
            array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE)
        )
        ->setComment('Service Error Messages')
        ->setOption('charset', 'utf8');

    $installer->getConnection()->createTable($table);
}

$installer->endSetup();