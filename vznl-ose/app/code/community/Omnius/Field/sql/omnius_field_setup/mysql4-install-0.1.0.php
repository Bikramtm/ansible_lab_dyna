<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Core_Model_Resource_Setup $this */
$this->startSetup();
/**
 * Create table 'default_field_sections'
 */
if ( ! $this->getConnection()->isTableExists($this->getTable('field/section'))) {
    $table = $this->getConnection()
        ->newTable($this->getTable('field/section'))
        ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array('identity'  => true,'unsigned'  => true,'nullable'  => false,'primary'   => true), 'ID')
        ->addColumn('name', Varien_Db_Ddl_Table::TYPE_VARCHAR, 64, array('unique' => true, 'primary' => true), 'Section Name')
        ->addIndex(
            'field_section_name_index',
            array('name'),
            array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE)
        )
        ->setComment('Default Field Sections')
        ->setOption('charset', 'utf8');

    $this->getConnection()->createTable($table);
}

$this->endSetup();