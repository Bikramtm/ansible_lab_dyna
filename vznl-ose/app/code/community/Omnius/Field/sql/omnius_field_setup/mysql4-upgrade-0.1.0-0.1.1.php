<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;

/**
 * Create table 'default_field_values'
 */
if ( ! $installer->getConnection()->isTableExists($installer->getTable('field/field'))) {
    $table = $installer->getConnection()
        ->newTable($installer->getTable('field/field'))
        ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array('identity'  => true,'unsigned'  => true,'nullable'  => false,'primary'   => true), 'ID')
        ->addColumn('name', Varien_Db_Ddl_Table::TYPE_VARCHAR, 64, array('unique' => true, 'primary' => true), 'Field Identifier')
        ->addColumn('default_value', Varien_Db_Ddl_Table::TYPE_TEXT, 65535, array(), 'Field Default Value')
        ->addColumn('type', Varien_Db_Ddl_Table::TYPE_VARCHAR, 64, array(), 'Field Type')
        ->addColumn('comment', Varien_Db_Ddl_Table::TYPE_TEXT, 65535, array(), 'Field Comment')
        ->addColumn('error_message', Varien_Db_Ddl_Table::TYPE_TEXT, 65535, array(), 'Field Error Message')
        ->addColumn('section_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array('unsigned' => true,'nullable' => false,'primary' => true), 'Section Identifier')
        ->addColumn('store_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array('unsigned' => true,'nullable' => false,'primary' => true), 'Store Identifier')
        ->addForeignKey(
            'fk_field_section',
            'section_id',
            $this->getTable('field/section'),
            'entity_id',
            Varien_Db_Ddl_Table::ACTION_CASCADE,
            Varien_Db_Ddl_Table::ACTION_CASCADE
        )
        ->addForeignKey(
            'fk_field_core_store',
            'store_id',
            'core_store',
            'store_id',
            Varien_Db_Ddl_Table::ACTION_CASCADE,
            Varien_Db_Ddl_Table::ACTION_CASCADE
        )
        ->addIndex(
            'default_field_value_section_name_index',
            array('name', 'section_id', 'store_id'),
            array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE)
        )
        ->setComment('Default Field Values')
        ->setOption('charset', 'utf8');

    $installer->getConnection()->createTable($table);
}

$installer->endSetup();