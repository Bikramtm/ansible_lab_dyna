<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Field_Block_Adminhtml_Field
 */
class Omnius_Field_Block_Adminhtml_Field extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * Omnius_Field_Block_Adminhtml_Field constructor.
     */
    public function __construct()
    {
        $this->_controller = "adminhtml_field";
        $this->_blockGroup = "field";
        $this->_headerText = Mage::helper("field")->__("Default Field Values");
        $this->_addButtonLabel = Mage::helper("field")->__("Add New Default Value");

        parent::__construct();
    }
}
