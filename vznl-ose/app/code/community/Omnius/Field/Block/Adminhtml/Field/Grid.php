<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Field_Block_Adminhtml_Field_Grid
 */
class Omnius_Field_Block_Adminhtml_Field_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Omnius_Field_Block_Adminhtml_Field_Grid constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId("fieldGrid");
        $this->setDefaultSort("entity_id");
        $this->setDefaultDir("ASC");
        $this->setSaveParametersInSession(true);
    }

    /**
     * Get collection to be used in Grid
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel("field/field")->getCollection();
        
        $this->setCollection($collection);
        parent::_prepareCollection();
        
        return $this;
    }

    /**
     * Define grids' columns
     * @return mixed
     */
    protected function _prepareColumns()
    {
        $this->addColumn("entity_id", array(
            "header" => Mage::helper("field")->__("ID"),
            "align" => "right",
            "width" => "50px",
            "type" => "number",
            "index" => "entity_id",
        ));

        $this->addColumn("name", array(
            "header" => Mage::helper("field")->__("Name"),
            "index" => "name",
        ));

        $this->addColumn("default_value", array(
            "header" => Mage::helper("field")->__("Default Value"),
            "index" => "default_value",
        ));
        
        $this->addColumn("type", array(
            "header" => Mage::helper("field")->__("Type"),
            "index" => "type",
        ));
        
        $this->addColumn("comment", array(
            "header" => Mage::helper("field")->__("Comment"),
            "index" => "comment",
            "type" => "text"
        ));

        $this->addColumn('section_id', array(
            'header' => Mage::helper('field')->__('Section'),
            'index' => 'section_id',
            'type' => 'options',
            'options' => Omnius_Field_Block_Adminhtml_Field_Grid::getOptionSections(),
        ));

        $this->addColumn('store_id', array(
            'header' => Mage::helper('field')->__('Store'),
            'index' => 'store_id',
            'type' => 'options',
            'options' => Mage::getSingleton('adminhtml/system_store')->getStoreOptionHash(false),
        ));
             
        $this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl("*/*/edit", array("id" => $row->getId()));
    }

    /**
     * Filters handler
     * @param $collection
     * @param $column
     * @return mixed
     */
    protected function _filterCategoriesCondition($collection, $column)
    {
        $value = $column->getFilter()->getValue();
        foreach ($collection as $key => $item) {
            if ( ! in_array($value,$item->getGroupId())) {
                $collection->removeItemByKey($key);
            }
        }
        return $collection;        
    }

    /**
     * Mass actions definition.
     * Available actions: remove
     * @return $this
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('entity_ids');
        $this->getMassactionBlock()->setUseSelectAll(true);
        $this->getMassactionBlock()->addItem('remove_field', array(
            'label' => Mage::helper('field')->__('Remove Field'),
            'url' => $this->getUrl('*/adminhtml_field/massRemove'),
            'confirm' => Mage::helper('field')->__('Are you sure?')
        ));
        return $this;
    }

    /**
     * Create array with all available fields sections
     * @return array
     */
    static public function getOptionSections()
    {
        $collection = Mage::getModel("field/section")->getCollection()->load();
        $data_array = array();
        $data_array[''] = '';
        foreach ($collection as $section) {
            $data_array[$section->getId()] = $section->getName();
        }
        return $data_array;
    }

    static public function getOptionFieldTypes()
    {
        return array(
            '' => '',
            'int' => Mage::helper('core')->__('Integer'),
            'string' => Mage::helper('core')->__('String'),
            'email' => Mage::helper('core')->__('Email'),
            'checkbox' => Mage::helper('core')->__('Checkbox'),
            'dropdown' => Mage::helper('core')->__('Dropdown'),
        );
    }
}
