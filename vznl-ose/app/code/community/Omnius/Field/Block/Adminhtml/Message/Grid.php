<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Field_Block_Adminhtml_Message_Grid
 */
class Omnius_Field_Block_Adminhtml_Message_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Omnius_Field_Block_Adminhtml_Message_Grid constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId("messageGrid");
        $this->setDefaultSort("entity_id");
        $this->setDefaultDir("ASC");
        $this->setSaveParametersInSession(true);
    }

    /**
     * Prepare messages collection to be used in Grid
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel("field/message")->getCollection();
        
        $this->setCollection($collection);
        parent::_prepareCollection();
        
        return $this;
    }

    /**
     * Define fields massages grids' columns
     * @return mixed
     */
    protected function _prepareColumns()
    {
        $this->addColumn("entity_id", array(
            "header" => Mage::helper("field")->__("ID"),
            "align" => "right",
            "width" => "50px",
            "type" => "number",
            "index" => "entity_id",
        ));

        $this->addColumn("service_code", array(
            "header" => Mage::helper("field")->__("Service Code"),
            "index" => "service_code",
        ));

        $this->addColumn("description", array(
            "header" => Mage::helper("field")->__("Description"),
            "index" => "description",
        ));

        $this->addColumn("response_code", array(
            "header" => Mage::helper("field")->__("Response Code"),
            "index" => "response_code",
        ));

        $this->addColumn("response_message", array(
            "header" => Mage::helper("field")->__("Response Message"),
            "index" => "response_message",
        ));

        $this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl("*/*/edit", array("id" => $row->getId()));
    }
    /**
     * Messages filters handler
     * @param $collection
     * @param $column
     * @return mixed
     */
    protected function _filterCategoriesCondition($collection, $column)
    {
        $value = $column->getFilter()->getValue();
        foreach ($collection as $key => $item) {
            if ( ! in_array($value,$item->getGroupId())) {
                $collection->removeItemByKey($key);
            }
        }
        return $collection;        
    }

    /**
     * Mass actions definition.
     * Available actions: remove
     * @return $this
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('entity_ids');
        $this->getMassactionBlock()->setUseSelectAll(true);
        $this->getMassactionBlock()->addItem('remove_message', array(
            'label' => Mage::helper('field')->__('Remove Message'),
            'url' => $this->getUrl('*/adminhtml_message/massRemove'),
            'confirm' => Mage::helper('field')->__('Are you sure?')
        ));
        return $this;
    }
}
