<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Field_Block_Adminhtml_Field_Edit
 */
class Omnius_Field_Block_Adminhtml_Field_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * Omnius_Field_Block_Adminhtml_Field_Edit constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->_objectId = "entity_id";
        $this->_blockGroup = "field";
        $this->_controller = "adminhtml_field";
        $this->_updateButton("save", "label", Mage::helper("field")->__("Save Item"));
        $this->_updateButton("delete", "label", Mage::helper("field")->__("Delete Item"));

        $this->_addButton("saveandcontinue", array(
            "label" => Mage::helper("field")->__("Save And Continue Edit"),
            "onclick" => "saveAndContinueEdit()",
            "class" => "save",
        ), -100);

        $this->_formScripts[] = "
							function saveAndContinueEdit(){
								editForm.submit($('edit_form').action+'back/edit/');
							}
						";
    }

    /**
     * Set header text according to current action: add/edit
     * @return mixed
     */
    public function getHeaderText()
    {
        if (Mage::registry("field_data") && Mage::registry("field_data")->getId()) {
            return Mage::helper("field")->__("Edit Item '%s'", $this->htmlEscape(Mage::registry("field_data")->getId()));
        } else {
            return Mage::helper("field")->__("Add Item");
        }
    }
}