<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Field_Block_Adminhtml_Message
 */
class Omnius_Field_Block_Adminhtml_Message extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * Omnius_Field_Block_Adminhtml_Message constructor.
     */
    public function __construct()
    {
        $this->_controller = "adminhtml_message";
        $this->_blockGroup = "field";
        $this->_headerText = Mage::helper("field")->__("Service Error Messages");
        $this->_addButtonLabel = Mage::helper("field")->__("Add New Message");

        parent::__construct();
    }
}
