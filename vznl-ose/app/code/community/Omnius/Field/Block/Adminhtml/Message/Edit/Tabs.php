<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Field_Block_Adminhtml_Message_Edit_Tabs
 */
class Omnius_Field_Block_Adminhtml_Message_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * Omnius_Field_Block_Adminhtml_Message_Edit_Tabs constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->setId("message_tabs");
        $this->setDestElementId("edit_form");
        $this->setTitle(Mage::helper("field")->__("Message Information"));
    }

    /**
     * Define left sidebar messages tab text
     * @return mixed
     */
    protected function _beforeToHtml()
    {
        $this->addTab("form_message", array(
            "label" => Mage::helper("field")->__("Message Information"),
            "title" => Mage::helper("field")->__("Message Information"),
            "content" => $this->getLayout()->createBlock("field/adminhtml_message_edit_tab_form")->toHtml(),
        ));

        return parent::_beforeToHtml();
    }
}
