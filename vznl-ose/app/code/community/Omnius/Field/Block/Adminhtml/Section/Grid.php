<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Field_Block_Adminhtml_Section_Grid
 */
class Omnius_Field_Block_Adminhtml_Section_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Omnius_Field_Block_Adminhtml_Section_Grid constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId("sectionGrid");
        $this->setDefaultSort("entity_id");
        $this->setDefaultDir("ASC");
        $this->setSaveParametersInSession(true);
    }

    /**
     * Prepare fields sections collection to be used in admin Grid
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel("field/section")->getCollection();
        
        $this->setCollection($collection);
        parent::_prepareCollection();
        
        return $this;
    }

    /**
     * Define fields sections grid columns
     * @return mixed
     */
    protected function _prepareColumns()
    {
        $this->addColumn("entity_id", array(
            "header" => Mage::helper("field")->__("ID"),
            "align" => "right",
            "width" => "50px",
            "type" => "number",
            "index" => "entity_id",
        ));

        $this->addColumn("name", array(
            "header" => Mage::helper("field")->__("Name"),
            "index" => "name",
        ));
             
        $this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl("*/*/edit", array("id" => $row->getId()));
    }
    /**
     * Fields sections filters handler
     * @param $collection
     * @param $column
     * @return mixed
     */
    protected function _filterCategoriesCondition($collection, $column)
    {
        $value = $column->getFilter()->getValue();
        foreach ($collection as $key => $item) {
            if ( ! in_array($value,$item->getGroupId())) {
                $collection->removeItemByKey($key);
            }
        }
        return $collection;        
    }

    /**
     * Mass actions definition.
     * Available actions: remove
     * @return $this
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('entity_ids');
        $this->getMassactionBlock()->setUseSelectAll(true);
        $this->getMassactionBlock()->addItem('remove_section', array(
            'label' => Mage::helper('field')->__('Remove Section'),
            'url' => $this->getUrl('*/adminhtml_section/massRemove'),
            'confirm' => Mage::helper('field')->__('Are you sure?')
        ));
        return $this;
    }
}
