<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Field_Block_Adminhtml_Message_Edit_Tab_Form
 */
class Omnius_Field_Block_Adminhtml_Message_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Define add/edit form fields in backend for massages
     * @return mixed
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset("message_form", array("legend" => Mage::helper("field")->__("Message information")));

        $fieldset->addField("service_code", "text", array(
            "label" => Mage::helper("field")->__("Service Code"),
            "class" => "required-entry",
            "required" => true,
            "name" => "service_code",
        ));

        $fieldset->addField("description", "textarea", array(
            "label" => Mage::helper("field")->__("Description"),
            "class" => "required-entry",
            "required" => true,
            "name" => "description",
        ));

        $fieldset->addField("response_code", "text", array(
            "label" => Mage::helper("field")->__("Response Code"),
            "class" => "required-entry",
            "required" => true,
            "name" => "response_code",
        ));

        $fieldset->addField("response_message", "text", array(
            "label" => Mage::helper("field")->__("Response Message"),
            "class" => "required-entry",
            "required" => true,
            "name" => "response_message",
        ));

        if (Mage::getSingleton("adminhtml/session")->getMessageData()) {
            $form->setValues(Mage::getSingleton("adminhtml/session")->getMessageData());
            Mage::getSingleton("adminhtml/session")->setMessageData(null);
        } elseif (Mage::registry("message_data")) {
            $form->setValues(Mage::registry("message_data")->getData());
        }

        return parent::_prepareForm();
    }
}
