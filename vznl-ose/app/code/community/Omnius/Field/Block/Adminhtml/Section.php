<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Field_Block_Adminhtml_Section
 */
class Omnius_Field_Block_Adminhtml_Section extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * Dyna_Field_Block_Adminhtml_Section constructor.
     */
    public function __construct()
    {
        $this->_controller = "adminhtml_section";
        $this->_blockGroup = "field";
        $this->_headerText = Mage::helper("field")->__("Field Sections");
        $this->_addButtonLabel = Mage::helper("field")->__("Add New Section");

        parent::__construct();
    }
}
