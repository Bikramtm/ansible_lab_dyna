<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Field_Adminhtml_SectionController
 */
class Omnius_Field_Adminhtml_SectionController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()->_setActiveMenu("field/section")->_addBreadcrumb(Mage::helper("adminhtml")->__("Field Sections"), Mage::helper("adminhtml")->__("Field Sections"));
        return $this;
    }

    /**
     * Admin fields sections grid display action
     */
    public function indexAction()
    {
        $this->_title($this->__("Field Sections"));

        $this->_initAction();
        $this->renderLayout();
    }

    /**
     * Edit field section admin action
     */
    public function editAction()
    {
        $this->_title($this->__("Edit Field Sections"));

        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("field/section")->load($id);

        if ($model->getId()) {
            Mage::register("section_data", $model);
            $this->loadLayout();
            $this->_setActiveMenu("field/section");
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Field Sections"), Mage::helper("adminhtml")->__("Field Sections"));
            $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock("field/adminhtml_section_edit"))->_addLeft($this->getLayout()->createBlock("field/adminhtml_section_edit_tabs"));
            $this->renderLayout();
        } else {
            Mage::getSingleton("adminhtml/session")->addError(Mage::helper("field")->__("Item does not exist."));
            $this->_redirect("*/*/");
        }
    }

    /**
     * Add new field section admin action
     */
    public function newAction()
    {
        $this->_title($this->__("New Section"));

        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("field/section")->load($id);

        $data = Mage::getSingleton("adminhtml/session")->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }

        Mage::register("section_data", $model);

        $this->loadLayout();
        $this->_setActiveMenu("field/section");

        $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);

        $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Field Sections"), Mage::helper("adminhtml")->__("Field Sections"));


        $this->_addContent($this->getLayout()->createBlock("field/adminhtml_section_edit"))->_addLeft($this->getLayout()->createBlock("field/adminhtml_section_edit_tabs"));

        $this->renderLayout();
    }

    /**
     * Save field section admin action
     */
    public function saveAction()
    {
        if ($postData = $this->getRequest()->getPost()) {
            try {
                $currentDefaultValue = Mage::getModel("field/section")->load($this->getRequest()->getParam("id"));

                $model = Mage::getModel("field/section")
                    ->addData($postData)
                    ->setId($this->getRequest()->getParam("id"))
                    ->save();

                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Section was successfully saved"));
                Mage::getSingleton("adminhtml/session")->setAgentData(false);

                if ($this->getRequest()->getParam("back")) {
                    $this->_redirect("*/*/edit", array("id" => $model->getId()));

                    return;
                }
            } catch (Zend_Db_Statement_Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError('A section with the same name already exists');
                $this->getRedirectUrl($currentDefaultValue);

                return;
            } catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                $this->getRedirectUrl($currentDefaultValue);

                return;
            }
        }
        $this->_redirect("*/*/");
    }


    /**
     * Delete field section admin action
     */
    public function deleteAction()
    {
        if ($this->getRequest()->getParam("id") > 0) {
            try {
                $model = Mage::getModel("field/section");
                $model->setId($this->getRequest()->getParam("id"))->delete();
                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Section was successfully deleted"));
                $this->_redirect("*/*/");
            } catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
            }
        }
        $this->_redirect("*/*/");
    }

    /**
     * Remove multiple fields.
     * Called from the grid mass action.
     */
    public function massRemoveAction()
    {
        try {
            $ids = $this->getRequest()->getPost('entity_ids', array());
            foreach ($ids as $id) {
                $model = Mage::getModel("field/section");
                $model->setId($id)->delete();
            }
            Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item(s) was successfully removed"));
        } catch (Exception $e) {
            Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
        }
        $this->_redirect('*/*/');
    }

    /**
     * Export field sections grid to CSV format
     */
    public function exportCsvAction()
    {
        $fileName = 'sections.csv';
        $grid = $this->getLayout()->createBlock('field/adminhtml_section_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }

    /**
     *  Export field sections grid to Excel XML format
     */
    public function exportExcelAction()
    {
        $fileName = 'sections.xml';
        $grid = $this->getLayout()->createBlock('field/adminhtml_section_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }

    /**
     * Patch for SUPEE-6285
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return true;
    }

    /**
     * @param $currentDefaultValue
     */
    protected function getRedirectUrl($currentDefaultValue)
    {
        Mage::getSingleton("adminhtml/session")->setSectionData($this->getRequest()->getPost());
        if ($currentDefaultValue->getId()) {
            $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
        } else {
            $this->_redirect("*/*/new");
        }
    }
}
