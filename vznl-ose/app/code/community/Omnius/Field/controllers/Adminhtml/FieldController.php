<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Field_Adminhtml_FieldController
 */
class Omnius_Field_Adminhtml_FieldController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()->_setActiveMenu("field/field")->_addBreadcrumb(Mage::helper("adminhtml")->__("Default Field Values"), Mage::helper("adminhtml")->__("Default Field Values"));
        return $this;
    }

    /**
     * Admin fields grid display action
     */
    public function indexAction()
    {
        $this->_title($this->__("Default Field Values"));

        $this->_initAction();
        $this->renderLayout();
    }

    /**
     * Edit field admin action
     */
    public function editAction()
    {
        $this->_title($this->__("Edit Default Value"));

        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("field/field")->load($id);

        if ($model->getId()) {
            Mage::register("field_data", $model);
            $this->loadLayout();
            $this->_setActiveMenu("field/field");
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Default Field Values"), Mage::helper("adminhtml")->__("Default Field Values"));
            $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock("field/adminhtml_field_edit"))->_addLeft($this->getLayout()->createBlock("field/adminhtml_field_edit_tabs"));
            $this->renderLayout();
        } else {
            Mage::getSingleton("adminhtml/session")->addError(Mage::helper("field")->__("Item does not exist."));
            $this->_redirect("*/*/");
        }
    }

    /**
     * Add new field admin action
     */
    public function newAction()
    {
        $this->_title($this->__("New Default Value"));

        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("field/field")->load($id);

        $data = Mage::getSingleton("adminhtml/session")->getAgentData(true);
        if (!empty($data)) {
            $model->setData($data);
        }

        Mage::register("field_data", $model);

        $this->loadLayout();
        $this->_setActiveMenu("field/field");

        $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);

        $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Default Field Values"), Mage::helper("adminhtml")->__("Default Field Values"));
        $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Default Field Values"), Mage::helper("adminhtml")->__("Default Field Values"));


        $this->_addContent($this->getLayout()->createBlock("field/adminhtml_field_edit"))->_addLeft($this->getLayout()->createBlock("field/adminhtml_field_edit_tabs"));

        $this->renderLayout();
    }

    /**
     * Save field admin action
     */
    public function saveAction()
    {
        $postData = $this->getRequest()->getPost();

        if ($postData) {
            try {
                $currentDefaultValue = Mage::getModel("field/field")->load($this->getRequest()->getParam("id"));

                $model = Mage::getModel("field/field")
                    ->addData($postData)
                    ->setId($this->getRequest()->getParam("id"))
                    ->save();

                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Default value was successfully saved"));
                Mage::getSingleton("adminhtml/session")->setAgentData(false);

                if ($this->getRequest()->getParam("back")) {
                    $this->_redirect("*/*/edit", array("id" => $model->getId()));
                    return;
                }
                $this->_redirect("*/*/");
                return;
            } catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                Mage::getSingleton("adminhtml/session")->setAgentData($this->getRequest()->getPost());
                if ($currentDefaultValue->getId()) {
                    $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
                } else {
                    $this->_redirect("*/*/new");
                }
                return;
            }
        }
        $this->_redirect("*/*/");
    }

    /**
     * Delete field admin action
     */
    public function deleteAction()
    {
        if ($this->getRequest()->getParam("id") > 0) {
            try {
                $model = Mage::getModel("field/field");
                $model->setId($this->getRequest()->getParam("id"))->delete();
                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item was successfully deleted"));
                $this->_redirect("*/*/");
            } catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
            }
        }
        $this->_redirect("*/*/");
    }

    /**
     * Remove multiple fields.
     * Called from the grid mass action.
     */
    public function massRemoveAction()
    {
        try {
            $ids = $this->getRequest()->getPost('entity_ids', array());
            foreach ($ids as $id) {
                $model = Mage::getModel("field/field");
                $model->setId($id)->delete();
            }
            Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item(s) was successfully removed"));
        } catch (Exception $e) {
            Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
        }
        $this->_redirect('*/*/');
    }

    /**
     * Export fields grid to CSV format
     */
    public function exportCsvAction()
    {
        $fileName = 'fields.csv';
        $grid = $this->getLayout()->createBlock('field/adminhtml_field_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }

    /**
     *  Export fields grid to Excel XML format
     */
    public function exportExcelAction()
    {
        $fileName = 'field.xml';
        $grid = $this->getLayout()->createBlock('field/adminhtml_field_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }

    /**
     * Patch for SUPEE-6285
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return true;
    }
}