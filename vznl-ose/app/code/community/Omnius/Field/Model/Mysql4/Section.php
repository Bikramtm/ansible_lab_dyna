<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Field_Model_Mysql4_Section
 */
class Omnius_Field_Model_Mysql4_Section extends Mage_Core_Model_Mysql4_Abstract
{
    /**
     * Omnius_Field_Model_Mysql4_Section constructor
     */
    protected function _construct()
    {
        $this->_init('field/section', 'entity_id');
    }
}