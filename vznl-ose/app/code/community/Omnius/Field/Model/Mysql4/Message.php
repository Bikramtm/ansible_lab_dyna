<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Field_Model_Mysql4_Message
 */
class Omnius_Field_Model_Mysql4_Message extends Mage_Core_Model_Mysql4_Abstract
{
    /**
     * Omnius_Field_Model_Mysql4_Message constructor
     */
    protected function _construct()
    {
        $this->_init('field/message', 'entity_id');
    }
}