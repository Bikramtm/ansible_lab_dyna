<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Field_Model_Message
 */
class Omnius_Field_Model_Message extends Mage_Core_Model_Abstract
{
    /** @var Dyna_Cache_Model_Cache */
    protected $_cache;

    /**
     * Omnius_Field_Model_Message constructor
     */
    protected function _construct()
    {
        $this->_init('field/message');
    }

    /**
     * Get service error messages by the service code
     * @param $code
     * @return mixed|null
     */
    public function getMessageForCode($code)
    {
        $key = sprintf('service_code_%s', $code);
        if ($result = $this->getCache()->load($code)) {
            return unserialize($result);
        } else {
            $collection = Mage::getResourceModel('field/message_collection')
                ->addFieldToFilter('response_code', $code)
                ->load();
            if (count($collection) !== 1) {
                return null;
            }
            $message = $collection->getFirstItem();
            $this->getCache()->save(serialize($message), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
            return $message;
        }
    }

    /**
     * Get cache instance
     * @return Dyna_Cache_Model_Cache
     */
    protected function getCache()
    {
        if ( ! $this->_cache) {
            $this->_cache = Mage::getSingleton('dyna_cache/cache');
        }
        return $this->_cache;
    }
}
