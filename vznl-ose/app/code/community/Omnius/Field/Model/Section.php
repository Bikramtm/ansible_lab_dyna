<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Field_Model_Section
 */
class Omnius_Field_Model_Section extends Mage_Core_Model_Abstract
{
    /**
     * Omnius_Field_Model_Section constructor
     */
    protected function _construct()
    {
        $this->_init('field/section');
    }

    /**
     * Special load method that first checks for object instance in Mage::objects() cache
     * @param int $id
     * @param null $field
     * @return $this|Mage_Core_Model_Abstract
     */
    public function load($id, $field=null)
    {
        $key = 'field_default_section_' . md5(serialize(array($id, $field)));
        if ($product = Mage::objects()->load($key)) {
            $this->setData($product->getData());
            return $this;
        }

        parent::load($id, $field);
        Mage::objects()->save($this, $key);
        return $this;
    }
}
