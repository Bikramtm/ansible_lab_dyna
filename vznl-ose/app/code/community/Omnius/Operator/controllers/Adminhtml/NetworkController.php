<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Operator_Adminhtml_NetworkController extends Mage_Adminhtml_Controller_Action
{
    /**
     * @return $this
     */
    protected function _initAction() {
        $this->loadLayout()
            ->_setActiveMenu('system/operator')
            ->_addBreadcrumb(Mage::helper('adminhtml')->__('Network Operators'), Mage::helper('adminhtml')->__('Network Operators'));
        return $this;
    }

    /**
     * Display the admin grid (table) with all the welcome messages
     */
    public function indexAction() {
        $this->_initAction()->renderLayout();
    }

    /**
     * Gather data and generate edit form
     *
     */
    public function editAction() {
        $id     = $this->getRequest()->getParam('entity_id');
        $model  = Mage::getModel('operator/networkOperator')->load($id);

        if ($model->getEntityId() || $id == 0) {
            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
            if (!empty($data)) {
                $model->setData($data);
            }

            Mage::register('operator_network_data', $model);

            $this->loadLayout();
            $this->_setActiveMenu('system/operator');

            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Network operator'), Mage::helper('adminhtml')->__('Network operator'));
            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Update operator'), Mage::helper('adminhtml')->__('Update operator'));

            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

            $this->_addContent($this->getLayout()->createBlock('operator/adminhtml_network_edit'))
                ->_addLeft($this->getLayout()->createBlock('operator/adminhtml_network_edit_tabs'));

            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('operator')->__('Network operator does not exist'));
            $this->_redirect('*/*/');
        }
    }

    /**
     * Admin add a new Announcement, forwards to edit action
     */
    public function newAction() {
        $this->_forward('edit');
    }

    /**
     * Perform the validation and saving of the announcements code
     *
     */
    public function saveAction() {
        if ($data = $this->getRequest()->getPost()) {
            $model = Mage::getModel('operator/networkOperator');
            $model->setData($data)
                ->setEntityId($this->getRequest()->getParam('entity_id'));
            try {
                $model->save();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('operator')->__('The network operator was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('entity_id' => $model->getEntityId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                $this->throwErrror($e->getMessage(), $data);
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('operator')->__('Unable to find item to save'));
        $this->_redirect('*/*/');
    }

    /**
     * Method that throws the error based on a message
     *
     * @param mixed $message
     */
    private function throwErrror($message, $data)
    {
        Mage::getSingleton('adminhtml/session')->addError($message);
        Mage::getSingleton('adminhtml/session')->setFormData($data);
        $this->_redirect('*/*/edit', array('entity_id' => $this->getRequest()->getParam('entity_id')));
    }

    /**
     * Method that deleted a operator code
     *
     */
    public function deleteAction() {
        if( $this->getRequest()->getParam('entity_id') > 0 ) {
            try {
                $model = Mage::getModel('operator/networkOperator');

                $model->setEntityId($this->getRequest()->getParam('entity_id'))
                    ->delete();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('entity_id' => $this->getRequest()->getParam('entity_id')));
            }
        }
        $this->_redirect('*/*/');
    }

    /**
     * Method to allow bulk deletion of announcements codes
     *
     */
    public function massDeleteAction() {
        $announcementsIds = $this->getRequest()->getParam('operator');
        if(!is_array($announcementsIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($announcementsIds as $announcementsId) {
                    $announcements = Mage::getModel('operator/networkOperator')->load($announcementsId);
                    $announcements->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully deleted', count($announcementsIds)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * Patch for SUPEE-6285
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return true;
    }
}
