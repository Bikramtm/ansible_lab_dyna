<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Operator_Model_Operator extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('operator/operator');
    }

    /**
     * @return array
     */
    public function getOperatorsWithProviders()
    {
        /** @var Omnius_Operator_Model_Resource_Operator_Collection $collection */
        $collection = $this->getCollection();
        $collection->getSelect()
            ->joinInner(
                array('t' => 'operator_service_providers'),
                'main_table.service_provider_id = t.entity_id');
        $collection->getSelect()
            ->order(['t.name ASC']);
        $array = array();
        foreach ($collection as $item) {
            $operatorModel = Mage::getModel('operator/networkOperator')->load($item->getNetworkOperatorId());
            $operator = [
                0 => $operatorModel->getCode(),
                1 => $operatorModel->getName(),
            ];
            $providerModel = Mage::getModel('operator/serviceProvider')->load($item->getServiceProviderId());
            $provider = [
                0 => $providerModel->getCode(),
                1 => $providerModel->getName(),
            ];

            if (empty($array[$operator[0]])) {
                $array[$operator[0]] = array(
                    'name' => isset($operator[1]) ? $operator[1] : '',
                );
            }

            $array[$operator[0]]['providers'][$item->getOperatorId()] = array(
                'code' => $provider[0],
                'name' => isset($provider[1]) ? $provider[1] : ''
            );
        }
        return $array;
    }

    /**
     * @param $operator
     * @param $provider
     * @return mixed
     */
    public function getRangeByCodes($operator, $provider)
    {
        /** @var Omnius_Operator_Model_Resource_Operator_Collection $collection */
        $collection = $this->getCollection();
        $collection->getSelect()
            ->join('operator_network_operators as no', 'no.entity_id = main_table.network_operator_id')
            ->join('operator_service_providers as sp', 'sp.entity_id = main_table.service_provider_id')
            ->where(sprintf('no.code = "%s"', $operator))
            ->where(sprintf('sp.code = "%s"', $provider));

        return $collection->getFirstItem()->getSimRange();
    }
}
