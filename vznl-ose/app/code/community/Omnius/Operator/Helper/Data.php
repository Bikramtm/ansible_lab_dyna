<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Operator_Helper_Data
 */
class Omnius_Operator_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Returns the service providers list
     * @param bool|false $split, $byId
     * @return array
     */
    public function getServiceProviders($split = false, $byId = false)
    {
        $return_array = [];
        $serviceProviders = Mage::getModel('operator/serviceProvider')->getCollection()->addFieldToFilter('fixed_line', 0);
        foreach ($serviceProviders as $serviceProvider) {
            if ($split) {
                $return_array[strtolower($serviceProvider->getCode())] = $serviceProvider->getName();
            } elseif ($byId) {
                $return_array[$serviceProvider->getEntityId()] = $serviceProvider->getCode() . ":" . $serviceProvider->getName();
            } else {
                $value = $serviceProvider->getCode() . ":" . $serviceProvider->getName();
                $return_array[$value] = $value;
            }
        }
        return $return_array;
    }

    /**
     * Returns the service providers list
     * @param bool|false $split, $byId
     * @return array
     */
    public function getNetworkOperators($split = false, $byId = false)
    {
        $return_array = [];
        $networkOperators = Mage::getModel('operator/networkOperator')->getCollection();
        foreach ($networkOperators as $networkOperator) {
            if ($split) {
                $return_array[strtolower($networkOperator->getCode())] = $networkOperator->getName();
            } elseif ($byId) {
                $return_array[$networkOperator->getEntityId()] = $networkOperator->getCode() . ":" . $networkOperator->getName();
            } else {
                $value = $networkOperator->getCode() . ":" . $networkOperator->getName();
                $return_array[$value] = $value;
            }
        }
        return $return_array;
    }

    /**
     * @param string $string
     * @return array
     */
    public function splitByColon($string)
    {
        if ($string) {
            return explode(':', $string);
        } else {
            return [];
        }
    }

    /**
     * Returns the list of fixed-line service providers
     * @return array|bool
     */
    public function getVomOperatorOptions()
    {
        $result = [];
        $vomOperatorsCollection = Mage::getModel('operator/ServiceProvider')
            ->getCollection()
            ->addFieldToFilter('fixed_line', 1)
            ->load();
        foreach ($vomOperatorsCollection as $vomOperator) {
            $code = $this->escapeHtml(trim($vomOperator->getCode()));
            $label = $this->escapeHtml(trim($vomOperator->getCode()));
            if(trim($vomOperator->getName())) {
                $label = $this->escapeHtml(trim($vomOperator->getName()));
            }

            $provider['code'] = $code;
            $provider['name'] = $this->__($label);
            $result[] = $provider;
        }

        return empty($result) ? false : $result;
    }

    /**
     * Return the dot allowed in contract option
     * @return array
     */
    public function getDotAllowedOptions()
    {
        return [
            0 => $this->__("No"),
            1 => $this->__("Yes")
        ];
    }

    /**
     * Returns an array of operators with code based key
     * @param null $store
     * @return mixed
     */
    public function getOperatorOptions()
    {
        $result = [];
        $networkOperatorsCollection = Mage::getModel('operator/networkOperator')
            ->getCollection()
            ->load();
        foreach ($networkOperatorsCollection as $networkOperator) {
            $result[$this->escapeHtml(trim($networkOperator->getCode()))] = $this->escapeHtml(trim($networkOperator->getName()));
        }

        return $result;
    }

    /**
     * Returns an array of service providers with code based key
     * @param null $store
     * @return array
     */
    public function getProviderOptions()
    {
        $result = [];
        $serviceProviderCollection = Mage::getModel('operator/serviceProvider')
            ->getCollection()
            ->addFieldToFilter('fixed_line', 0)
            ->load();
        foreach ($serviceProviderCollection as $serviceProvider) {
            $result[$this->escapeHtml(trim($serviceProvider->getCode()))] = $this->escapeHtml(trim($serviceProvider->getName()));
        }

        return $result;
    }
}
