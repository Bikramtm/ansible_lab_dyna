<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Core_Model_Resource_Setup $this */
$this->startSetup();
$connection = $this->getConnection();
if (!$this->getConnection()->isTableExists($this->getTable('operator_network_operators'))) {
    $networkProvidersTable = $this->getConnection()->newTable($this->getTable('operator_network_operators'))
        ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
            'unsigned' => true,
            'nullable' => false,
            'primary' => true,
            'identity' => true,
        ), 'Network Provider Id')
        ->addColumn('code', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
            'nullable' => false,
            'default' => '',
            'length' => '10',
        ), 'Network Provider Code')
        ->addColumn('name', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
            'nullable' => false,
            'default' => '',
        ), 'Network Provider Name')
        ->setComment('Network Providers Table');
    $this->getConnection()->createTable($networkProvidersTable);
}

if (!$this->getConnection()->isTableExists($this->getTable('operator_service_providers'))) {
    $networkOperatorsTable = $this->getConnection()->newTable($this->getTable('operator_service_providers'))
        ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
            'unsigned' => true,
            'nullable' => false,
            'primary' => true,
            'identity' => true,
        ), 'Network Operator Id')
        ->addColumn('code', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
            'nullable' => false,
            'default' => '',
            'length' => '10',
        ), 'Network Operator Code')
        ->addColumn('name', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
            'nullable' => false,
            'default' => '',
        ), 'Network Operator Name')
        ->addColumn('fixed_line', Varien_Db_Ddl_Table::TYPE_BOOLEAN, null, array(
            'nullable' => false,
            'default' => false,
        ), 'Fixed line network')
        ->setComment('Network Operators Table');
    $this->getConnection()->createTable($networkOperatorsTable);
}

$connection->addColumn($this->getTable('operator/operator'), 'network_operator_id',
    [
        'type'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'comment'   => 'Network Operator Id',
        'required'  => false,
        'after'     => 'operator',
        'nullable'  => true,
        'unsigned' => true,
    ]
);


$connection->addForeignKey('fk_network_operator_id', $this->getTable('operator/operator'), 'network_operator_id', $this->getTable('operator_network_operators'), 'entity_id');

$connection->addColumn($this->getTable('operator/operator'), 'service_provider_id',
    [
        'type'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'comment'   => 'Service Provider Id',
        'required'  => false,
        'after'     => 'service_provider',
        'nullable'  => true,
        'unsigned' => true,
    ]
);

$connection->addForeignKey('fk_service_provider_id', $this->getTable('operator/operator'), 'service_provider_id', $this->getTable('operator_service_providers'), 'entity_id');
$connection->addColumn($this->getTable('operator/operator'), 'dot_allowed',
    [
        'type'      => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
        'comment'   => 'Dot allowed in the contract',
        'required'  => false,
        'nullable'  => false,
        'default'  => false,
    ]
);

$this->endSetup();
