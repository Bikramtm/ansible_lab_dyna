<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Operator_Block_Adminhtml_Network extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_network';
        $this->_blockGroup = 'operator';
        $this->_headerText = Mage::helper('operator')->__('Network Operators');
        $this->_addButtonLabel = Mage::helper('operator')->__('Add network operator');
        parent::__construct();
    }
}
