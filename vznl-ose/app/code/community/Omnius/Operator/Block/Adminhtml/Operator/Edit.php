<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Operator_Block_Adminhtml_Operator_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();

        $this->_objectId = 'operator_id';
        $this->_blockGroup = 'operator';
        $this->_controller = 'adminhtml_operator';

        $this->_updateButton('save', 'label', Mage::helper('operator')->__('Save combination'));

        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);
        $objId = $this->getRequest()->getParam($this->_objectId);
        if (! empty($objId)) {
            $this->_addButton('delete', array(
                'label'     => Mage::helper('adminhtml')->__('Delete'),
                'class'     => 'delete',
                'onclick'   => 'deleteConfirm(\''. Mage::helper('adminhtml')->__('Are you sure you want to do this?')
                .'\', \'' . $this->getDeleteUrl() . '\')',
            ));
        }

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('messages_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'messages_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'messages_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    /**
     * @return string
     */
    public function getHeaderText()
    {
        if( Mage::registry('operator_data') && Mage::registry('operator_data')->getOperatorId() ) {
            return Mage::helper('operator')->__("Edit operator combination '%s'", $this->htmlEscape(Mage::registry('operator_data')->getOperatorId()));
        } else {
            return Mage::helper('operator')->__('Add operator combination');
        }
    }
}