<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Operator_Block_Adminhtml_Operator_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('operator_form', array('legend'=>Mage::helper('operator')->__('Operator combination')));
        $helper = Mage::helper('operator');
        $operator_list = $helper->getNetworkOperators(false, true);
        $provider_list = $helper->getServiceProviders(false, true);

        $fieldset->addField('network_operator_id', 'select', array(
            'label'     => Mage::helper('operator')->__('Network Operator'),
            'name'      => 'network_operator_id',
            'values'    => $operator_list,
        ));

        $fieldset->addField('service_provider_id', 'select', array(
            'label'     => Mage::helper('operator')->__('Service provider'),
            'name'      => 'service_provider_id',
            'values'    => $provider_list,
        ));

        $fieldset->addField('sim_range', 'text', array(
            'label'     => Mage::helper('operator')->__('Sim range'),
            'name'      => 'sim_range',
        ));

        $fieldset->addField('dot_allowed', 'select', array(
            'label'     => Mage::helper('operator')->__('Dot allowed in contract'),
            'name'      => 'dot_allowed',
            'values'    => Mage::helper('operator')->getDotAllowedOptions(),
        ));

        if (Mage::getSingleton("adminhtml/session")->getOperatorData())
        {
            $form->setValues(Mage::getSingleton("adminhtml/session")->getOperatorData());
            Mage::getSingleton("adminhtml/session")->setOperatorData(null);
        }
        elseif(Mage::registry("operator_data")) {
            $form->setValues(Mage::registry("operator_data")->getData());
        }

        return parent::_prepareForm();
    }

}
