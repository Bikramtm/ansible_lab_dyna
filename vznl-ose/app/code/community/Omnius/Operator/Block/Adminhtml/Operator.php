<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_Operator_Block_Adminhtml_Operator extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_operator';
        $this->_blockGroup = 'operator';
        $this->_headerText = Mage::helper('operator')->__('Sim ranges combination');
        $this->_addButtonLabel = Mage::helper('operator')->__('Add sim range combination');
        parent::__construct();

    }
}
