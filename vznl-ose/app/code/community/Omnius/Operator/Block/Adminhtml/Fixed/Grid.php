<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Admin list operator combinations
 */

class Omnius_Operator_Block_Adminhtml_Fixed_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('fixedGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('operator/serviceProvider')->getCollection()->addFieldToFilter('fixed_line', 1);
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * Create the admin images list
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareColumns()
    {
        $this->addColumn('entity_id', array(
            'header'    => Mage::helper('operator')->__('ID'),
            'align'     =>'right',
            'width'     => '30px',
            'index'     => 'entity_id',
        ));

        $this->addColumn('provider_code', array(
            'header'        => Mage::helper('operator')->__('Fixed-line provider code'),
            'align'         => 'left',
            'index'         => 'code'
        ));

        $this->addColumn('provider_name', array(
            'header'    => Mage::helper('operator')->__('Fixed-line provider name'),
            'align'     => 'left',
            'index'     => 'name'
        ));

        $this->addColumn('action', array(
            'header'    =>  Mage::helper('operator')->__('Action'),
            'width'     => '100',
            'type'      => 'action',
            'getter'    => 'getEntityId',
            'actions'   => array(
                array(
                    'caption'   => Mage::helper('operator')->__('Edit'),
                    'url'       => array('base'=> '*/*/edit'),
                    'field'     => 'entity_id'
                )
            ),
            'filter'    => false,
            'sortable'  => false,
            'is_system' => true,
        ));


        return parent::_prepareColumns();
    }

    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('operator');

        $this->getMassactionBlock()->addItem('delete', array(
            'label'    => Mage::helper('operator')->__('Delete'),
            'url'      => $this->getUrl('*/*/massDelete'),
            'confirm'  => Mage::helper('operator')->__('Are you sure?')
        ));

        return $this;
    }

    /**
     * @param $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('entity_id' => $row->getEntityId()));
    }
}
