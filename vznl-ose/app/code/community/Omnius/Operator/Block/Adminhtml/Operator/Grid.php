<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Admin list operator combinations
 */

class Omnius_Operator_Block_Adminhtml_Operator_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('operatorGrid');
        $this->setDefaultSort('operator_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);

    }

    /**
     * Prepare grid collection object
     * 
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('operator/operator')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * Create the admin images list
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareColumns()
    {
        $this->addColumn('operator_id', array(
            'header'    => Mage::helper('operator')->__('ID'),
            'align'     =>'right',
            'width'     => '30px',
            'index'     => 'operator_id',
        ));

        $this->addColumn('operator', array(
            'header'    => Mage::helper('operator')->__('Operator'),
            'index'     => 'network_operator_id',
            'align'     => 'left',
            'type'      => 'options',
            'sortable'  => false,
            'options'   => Mage::helper('operator')->getNetworkOperators(false, true),
        ));

        $this->addColumn('service_provider', array(
            'header'    => Mage::helper('operator')->__('Service provider'),
            'index'     => 'service_provider_id',
            'align'     => 'left',
            'sortable'  => false,
            'type'      => 'options',
            'options' => Mage::helper('operator')->getServiceProviders(false, true),
        ));

        $this->addColumn('sim_range', array(
            'header'    => Mage::helper('operator')->__('Sim range'),
            'align'     => 'left',
            'index'     => 'sim_range'
        ));

        $this->addColumn('dot_allowed', array(
            'header'    => Mage::helper('operator')->__('Dot allowed in contract'),
            'index'     => 'dot_allowed',
            'align'     => 'left',
            'type'      => 'options',
            'options'   => Mage::helper('operator')->getDotAllowedOptions(),
        ));

        $this->addColumn('action', array(
            'header'    =>  Mage::helper('operator')->__('Action'),
            'width'     => '100',
            'type'      => 'action',
            'getter'    => 'getOperatorId',
            'actions'   => array(
                array(
                    'caption'   => Mage::helper('operator')->__('Edit'),
                    'url'       => array('base'=> '*/*/edit'),
                    'field'     => 'operator_id'
                )
            ),
            'filter'    => false,
            'sortable'  => false,
            'is_system' => true,
        ));

        return parent::_prepareColumns();
    }

    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('operator_id');
        $this->getMassactionBlock()->setFormFieldName('operator');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('operator')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('operator')->__('Are you sure?')
        ));

        return $this;
    }

    /**
     * @param $row
     * @return string
     */
    public function getRowUrl($row)
    {
      return $this->getUrl('*/*/edit', array('operator_id' => $row->getOperatorId()));
    }
}
