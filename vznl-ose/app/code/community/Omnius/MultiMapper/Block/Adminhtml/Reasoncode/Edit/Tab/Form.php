<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_MultiMapper_Block_Adminhtml_Reasoncode_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {

        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset("multimapper_form", array("legend" => Mage::helper("multimapper")->__("Item information")));


        $fieldset->addField('customer_value_option_id', 'select', array(
            'label' => Mage::helper('multimapper')->__('Customer value'),
            'values' => Omnius_MultiMapper_Block_Adminhtml_Reasoncode_Grid::getValueArray0(),
            'name' => 'customer_value_option_id',
            'class' => 'required-entry',
            'required' => true,
        ));
        $fieldset->addField('catalog_product_reasoncode_option_id', 'select', array(
            'label' => Mage::helper('multimapper')->__('Subscription type'),
            'values' => Omnius_MultiMapper_Block_Adminhtml_Reasoncode_Grid::getValueArray1(),
            'name' => 'catalog_product_reasoncode_option_id',
            'class' => 'required-entry',
            'required' => true,
        ));
        $fieldset->addField("reason_code", "text", array(
            "label" => Mage::helper("multimapper")->__("Reason code"),
            "name" => "reason_code",
            'class' => 'required-entry',
            'required' => true,
        ));


        if (Mage::getSingleton("adminhtml/session")->getReasoncodeData()) {
            $form->setValues(Mage::getSingleton("adminhtml/session")->getReasoncodeData());
            Mage::getSingleton("adminhtml/session")->setReasoncodeData(null);
        } elseif (Mage::registry("reasoncode_data")) {
            $form->setValues(Mage::registry("reasoncode_data")->getData());
        }
        return parent::_prepareForm();
    }
}
