<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_MultiMapper_Block_Adminhtml_Mapper_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * Omnius_MultiMapper_Block_Adminhtml_Mapper_Edit_Tabs constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId("mapper_tabs");
        $this->setDestElementId("edit_form");
        $this->setTitle(Mage::helper("multimapper")->__("Item Information"));
    }

    /**
     * Before rendering html, but after trying to load cache
     *
     * @return Mage_Core_Block_Abstract
     * @throws Exception
     */
    protected function _beforeToHtml()
    {
        $this->addTab("form_section", array(
            "label" => Mage::helper("multimapper")->__("Item Information"),
            "title" => Mage::helper("multimapper")->__("Item Information"),
            "content" => $this->getLayout()->createBlock("multimapper/adminhtml_mapper_edit_tab_form")->toHtml(),
        ));
        return parent::_beforeToHtml();
    }

}
