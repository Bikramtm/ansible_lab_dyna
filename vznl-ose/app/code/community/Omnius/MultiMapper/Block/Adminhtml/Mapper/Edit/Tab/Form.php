<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_MultiMapper_Block_Adminhtml_Mapper_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {

        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset("multimapper_form", array("legend" => Mage::helper("multimapper")->__("Item information")));


        $fieldset->addField("priceplan_sku", "text", array(
            "label" => Mage::helper("multimapper")->__("Price plan SKU"),
            "class" => "required-entry",
            "required" => true,
            "name" => "priceplan_sku",
        ));

        $fieldset->addField("promo_sku", "text", array(
            "label" => Mage::helper("multimapper")->__("Promo SKU"),
            "name" => "promo_sku",
        ));

        $fieldset->addField("comment", "textarea", array(
            "label" => Mage::helper("multimapper")->__("Comment"),
            "name" => "comment",
        ));

        for ($i = 1; $i <= 10; $i++) {
            $fieldset->addField("addon" . $i . "_name", "textarea", array(
                "label" => Mage::helper("multimapper")->__("Addon " . $i . " Name"),
                "name" => "addon" . $i . "_name",
            ));
            $fieldset->addField("addon" . $i . "_desc", "textarea", array(
                "label" => Mage::helper("multimapper")->__("Addon " . $i . " Description"),
                "name" => "addon" . $i . "_desc",
            ));
            $fieldset->addField("addon" . $i . "_nature", "textarea", array(
                "label" => Mage::helper("multimapper")->__("Addon " . $i . " Nature"),
                "name" => "addon" . $i . "_nature",
            ));
            $fieldset->addField("addon" . $i . "_soc", "textarea", array(
                "label" => Mage::helper("multimapper")->__("Addon " . $i . " SOC"),
                "name" => "addon" . $i . "_soc",
            ));
        }

        if (Mage::getSingleton("adminhtml/session")->getMapperData()) {
            $form->setValues(Mage::getSingleton("adminhtml/session")->getMapperData());
            Mage::getSingleton("adminhtml/session")->setMapperData(null);
        } elseif (Mage::registry("mapper_data")) {
            $form->setValues(Mage::registry("mapper_data")->getData());
        }
        return parent::_prepareForm();
    }
}
