<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_MultiMapper_Block_Adminhtml_Mapper_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId("mapperGrid");
        $this->setDefaultSort("entity_id");
        $this->setDefaultDir("DESC");
        $this->setSaveParametersInSession(true);
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel("multimapper/mapper")->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function _prepareColumns()
    {
        if (!$this->_isExport) {
            $this->addColumn("entity_id", array(
                "header" => Mage::helper("multimapper")->__("ID"),
                "align" => "right",
                "width" => "50px",
                "type" => "number",
                "index" => "entity_id",
            ));
        }
        $this->addColumn("priceplan_sku", array(
            "header" => Mage::helper("multimapper")->__("Price plan SKU"),
            "index" => "priceplan_sku",
        ));
        $this->addColumn("promo_sku", array(
            "header" => Mage::helper("multimapper")->__("Promo SKU"),
            "index" => "promo_sku",
        ));
        $this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

        return parent::_prepareColumns();
    }

    /**
     * @param $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl("*/*/edit", array("id" => $row->getId()));
    }

    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('entity_ids');
        $this->getMassactionBlock()->setUseSelectAll(true);
        $this->getMassactionBlock()->addItem('remove_mapper', array(
            'label' => Mage::helper('multimapper')->__('Remove Mapper'),
            'url' => $this->getUrl('*/adminhtml_mapper/massRemove'),
            'confirm' => Mage::helper('multimapper')->__('Are you sure?')
        ));
        return $this;
    }
}
