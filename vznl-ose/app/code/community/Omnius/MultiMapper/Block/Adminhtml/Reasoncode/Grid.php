<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_MultiMapper_Block_Adminhtml_Reasoncode_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /** @var null|array */
    protected $_customerValueOpts = null;
    protected $_catalogProductReasonCodeOpts = null;

    public function __construct()
    {
        parent::__construct();
        $this->setId("reasoncodeGrid");
        $this->setDefaultSort("entity_id");
        $this->setDefaultDir("DESC");
        $this->setSaveParametersInSession(true);
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel("multimapper/reasoncode")->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function _prepareColumns()
    {
        $this->addColumn("entity_id", array(
            "header" => Mage::helper("multimapper")->__("ID"),
            "align" => "right",
            "width" => "50px",
            "type" => "number",
            "index" => "entity_id",
        ));

        $this->addColumn("reason_code", array(
            "header" => Mage::helper("multimapper")->__("Reason code"),
            "index" => "reason_code",
        ));

        $this->addColumn("customer_value_option_id", array(
            "header" => Mage::helper("multimapper")->__("Customer value"),
            "index" => "customer_value_option_id",
            'frame_callback' => array($this, 'getCustomerValueOption'),
        ));

        $this->addColumn("catalog_product_reasoncode_option_id", array(
            "header" => Mage::helper("multimapper")->__("Catalog product reason code"),
            "index" => "catalog_product_reasoncode_option_id",
            'frame_callback' => array($this, 'getCatalogProductReasonCode'),
        ));

        $this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

        return parent::_prepareColumns();
    }

    /**
     * @param $value
     * @return mixed
     * @throws Mage_Core_Exception
     */
    public function getCustomerValueOption($value)
    {
        if (null === $this->_customerValueOpts) {
            $customerValueAttr = Mage::getSingleton("eav/config")->getAttribute('customer', 'customer_value');
            $customerValueOptions = $customerValueAttr->getSource()->getAllOptions(true, true);
            $this->_customerValueOpts = array();
            foreach ($customerValueOptions as $opt) {
                if ($opt['value']) {
                    $this->_customerValueOpts[$opt['value']] = ($opt['label']);
                }
            }
        }

        return isset($this->_customerValueOpts[$value]) ? $this->_customerValueOpts[$value] : $value;
    }

    /**
     * @param $value
     * @return mixed
     * @throws Mage_Core_Exception
     */
    public function getCatalogProductReasonCode($value)
    {
        if (null === $this->_catalogProductReasonCodeOpts) {
            $reasonCodeAttr = Mage::getSingleton("eav/config")->getAttribute('catalog_product', 'identifier_reasoncode');
            $reasonCodeOptions = $reasonCodeAttr->getSource()->getAllOptions(true, true);
            $this->_catalogProductReasonCodeOpts = array();
            foreach ($reasonCodeOptions as $opt) {
                if ($opt['value']) {
                    $this->_catalogProductReasonCodeOpts[$opt['value']] = ($opt['label']);
                }
            }
        }

        return isset($this->_catalogProductReasonCodeOpts[$value]) ? $this->_catalogProductReasonCodeOpts[$value] : $value;
    }

    /**
     * @param $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl("*/*/edit", array("id" => $row->getId()));
    }

    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('entity_ids');
        $this->getMassactionBlock()->setUseSelectAll(true);
        $this->getMassactionBlock()->addItem('remove_reasoncode', array(
            'label' => Mage::helper('multimapper')->__('Remove Reasoncode'),
            'url' => $this->getUrl('*/adminhtml_reasoncode/massRemove'),
            'confirm' => Mage::helper('multimapper')->__('Are you sure?')
        ));
        return $this;
    }

    /**
     * @return array
     */
    static public function getOptionArray0()
    {
        $data_array = array();
        $attribute = Mage::getModel('eav/config')->getAttribute('customer', 'customer_value');
        foreach ($attribute->getSource()->getAllOptions(false) as $option) {
            $data_array[$option['value']] = $option['label'];
        }
        return ($data_array);
    }

    /**
     * @return array
     */
    static public function getValueArray0()
    {
        $data_array = array();
        foreach (Omnius_MultiMapper_Block_Adminhtml_Reasoncode_Grid::getOptionArray0() as $k => $v) {
            $data_array[] = array('value' => $k, 'label' => $v);
        }
        return ($data_array);

    }

    /**
     * @return array
     */
    static public function getOptionArray1()
    {
        $data_array = array();
        $attribute = Mage::getModel('eav/config')->getAttribute('catalog_product', 'identifier_reasoncode');
        foreach ($attribute->getSource()->getAllOptions(false) as $option) {
            $data_array[$option['value']] = $option['label'];
        }
        return ($data_array);
    }

    /**
     * @return array
     */
    static public function getValueArray1()
    {
        $data_array = array();
        foreach (Omnius_MultiMapper_Block_Adminhtml_Reasoncode_Grid::getOptionArray1() as $k => $v) {
            $data_array[] = array('value' => $k, 'label' => $v);
        }
        return ($data_array);

    }
}
