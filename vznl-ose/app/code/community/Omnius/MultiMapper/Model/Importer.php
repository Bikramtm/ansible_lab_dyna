<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_MultiMapper_Model_Importer extends Mage_Core_Model_Abstract
{

    /**
     * @param $msg
     */
    protected function _log($msg)
    {
        Mage::log($msg, null, 'multimapper_import_' . date('Y-m-d') . '.log');
    }

    /**
     * @param $path
     * @return bool
     */
    public function import($path)
    {
        /** @var Omnius_MultiMapper_Helper_Data $helper */
        $helper = Mage::helper('multimapper');
        $file = fopen($path, 'r');
        $lc = 0;
        $success = true;
        $combinations = [];
        while (($line = fgetcsv($file)) !== false) {
            $lc++;
            if ($lc == 1) {
                continue;
            }

            $priceplanSku = trim($line[0]);
            $promoSku = trim($line[1]);

            $pricePlanId = Mage::getModel("catalog/product")->getIdBySku($priceplanSku);
            if (!$pricePlanId) {
                $success = false;
                $this->_log('Skipping row ' . print_r($line, true) . ' because the priceplan could not be found using the SKU specified');

                Mage::getSingleton('adminhtml/session')->addError(
                    sprintf('Skipping row with subscription `%s` and promo `%s` because the priceplan could not be found using the SKU specified', $priceplanSku, $promoSku)
                );
                continue;
            }

            $promoId = null;
            if ($promoSku) {
                $promoId = Mage::getModel("catalog/product")->getIdBySku($promoSku);
                if (!$promoId) {
                    $success = false;
                    $this->_log('Skipping row ' . print_r($line, true) . ' because the promotion could not be found using the SKU specified');

                    Mage::getSingleton('adminhtml/session')->addError(
                        sprintf('Skipping row with subscription `%s` and promo `%s` because the promotion could not be found using the SKU specified', $priceplanSku, $promoSku)
                    );
                    continue;
                }
            }
            if (isset($combinations[$priceplanSku.'-'.$promoSku])) {
                $success = false;
                $combinations[$priceplanSku.'-'.$promoSku]++;

                Mage::getSingleton('adminhtml/session')->addError(
                    sprintf('The row with subscription `%s` and promo `%s` was found more than once in the import file. The database was updated with the last entry', $priceplanSku, $promoSku)
                );
            } else {
                $combinations[$priceplanSku.'-'.$promoSku] = 1;
            }

            $mapping = $helper->getMappingInstanceBySku($priceplanSku, $promoSku);

            $mapping->setPriceplanId($pricePlanId);
            if ($promoId) {
                $mapping->setPromoId($promoId);
            }
            $mapping->setPriceplanSku($priceplanSku);
            $mapping->setPromoSku($promoSku);

            $addonCnt = 1;
            $addons = array_chunk(array_slice($line, 2), 4);
            foreach ($addons as $addon) {
                $mapping->setData('addon' . $addonCnt . '_name', $addon[0]);
                $mapping->setData('addon' . $addonCnt . '_desc', $addon[1]);
                $mapping->setData('addon' . $addonCnt . '_nature', $addon[2]);
                $mapping->setData('addon' . $addonCnt . '_soc', $addon[3]);
                $addonCnt++;
            }

            $mapping->save();
            $this->_log('Imported row ' . print_r($line, true));
        }
        fclose($file);
        unset($combinations);

        return $success;
    }
}
