<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_MultiMapper_Model_Mysql4_Reasoncode extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init("multimapper/reasoncode", "entity_id");
    }
}