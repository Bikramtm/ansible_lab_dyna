<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_MultiMapper_Adminhtml_ReasoncodeController extends Mage_Adminhtml_Controller_Action
{
    /**
     * @return $this
     */
    protected function _initAction()
    {
        $this->loadLayout()->_setActiveMenu("multimapper/reasoncode")->_addBreadcrumb(Mage::helper("adminhtml")->__("Reasoncode  Manager"), Mage::helper("adminhtml")->__("Reasoncode Manager"));
        return $this;
    }

    public function indexAction()
    {
        $this->_title($this->__("MultiMapper"));
        $this->_title($this->__("Manager Reasoncode"));

        $this->_initAction();
        $this->renderLayout();
    }

    public function editAction()
    {
        $this->_title($this->__("MultiMapper"));
        $this->_title($this->__("Reasoncode"));
        $this->_title($this->__("Edit Item"));

        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("multimapper/reasoncode")->load($id);
        if ($model->getId()) {
            Mage::register("reasoncode_data", $model);
            $this->loadLayout();
            $this->_setActiveMenu("multimapper/reasoncode");
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Reasoncode Manager"), Mage::helper("adminhtml")->__("Reasoncode Manager"));
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Reasoncode Description"), Mage::helper("adminhtml")->__("Reasoncode Description"));
            $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock("multimapper/adminhtml_reasoncode_edit"))->_addLeft($this->getLayout()->createBlock("multimapper/adminhtml_reasoncode_edit_tabs"));
            $this->renderLayout();
        } else {
            Mage::getSingleton("adminhtml/session")->addError(Mage::helper("multimapper")->__("Item does not exist."));
            $this->_redirect("*/*/");
        }
    }

    public function newAction()
    {

        $this->_title($this->__("MultiMapper"));
        $this->_title($this->__("Reasoncode"));
        $this->_title($this->__("New Item"));

        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("multimapper/reasoncode")->load($id);

        $data = Mage::getSingleton("adminhtml/session")->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }

        Mage::register("reasoncode_data", $model);

        $this->loadLayout();
        $this->_setActiveMenu("multimapper/reasoncode");

        $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);

        $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Reasoncode Manager"), Mage::helper("adminhtml")->__("Reasoncode Manager"));
        $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Reasoncode Description"), Mage::helper("adminhtml")->__("Reasoncode Description"));


        $this->_addContent($this->getLayout()->createBlock("multimapper/adminhtml_reasoncode_edit"))->_addLeft($this->getLayout()->createBlock("multimapper/adminhtml_reasoncode_edit_tabs"));

        $this->renderLayout();

    }

    public function saveAction()
    {

        $post_data = $this->getRequest()->getPost();


        if ($post_data) {

            try {


                $model = Mage::getModel("multimapper/reasoncode")
                    ->addData($post_data)
                    ->setId($this->getRequest()->getParam("id"))
                    ->save();

                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Reasoncode was successfully saved"));
                Mage::getSingleton("adminhtml/session")->setReasoncodeData(false);

                if ($this->getRequest()->getParam("back")) {
                    $this->_redirect("*/*/edit", array("id" => $model->getId()));
                    return;
                }
                $this->_redirect("*/*/");
                return;
            } catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                Mage::getSingleton("adminhtml/session")->setReasoncodeData($this->getRequest()->getPost());
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
                return;
            }

        }
        $this->_redirect("*/*/");
    }


    public function deleteAction()
    {
        if ($this->getRequest()->getParam("id") > 0) {
            try {
                $model = Mage::getModel("multimapper/reasoncode");
                $model->setId($this->getRequest()->getParam("id"))->delete();
                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item was successfully deleted"));
                $this->_redirect("*/*/");
            } catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
            }
        }
        $this->_redirect("*/*/");
    }


    public function massRemoveAction()
    {
        try {
            $ids = $this->getRequest()->getPost('entity_ids', array());
            foreach ($ids as $id) {
                $model = Mage::getModel("multimapper/reasoncode");
                $model->setId($id)->delete();
            }
            Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item(s) was successfully removed"));
        } catch (Exception $e) {
            Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
        }
        $this->_redirect('*/*/');
    }

    /**
     * Export order grid to CSV format
     */
    public function exportCsvAction()
    {
        $fileName = 'reasoncode.csv';
        $grid = $this->getLayout()->createBlock('multimapper/adminhtml_reasoncode_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }

    /**
     *  Export order grid to Excel XML format
     */
    public function exportExcelAction()
    {
        $fileName = 'reasoncode.xml';
        $grid = $this->getLayout()->createBlock('multimapper/adminhtml_reasoncode_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }

    /**
     * Patch for SUPEE-6285
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return true;
    }
}
