<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Import_Helper_Data
 */
class Omnius_Import_Helper_Data extends Mage_Core_Helper_Abstract
{
    /** @var string $_import_log_file */
    private $_import_log_file;

    /**
     * Log exception messages
     *
     * @param Exception $ex
     */
    public function log($ex)
    {
        Mage::log($ex->getMessage(), null, $this->getImportLogFile());
    }

    /**
     * Log simple messages
     *
     * @param string $msg
     */
    public function logMsg($msg)
    {
        Mage::log($msg, null, $this->getImportLogFile());
    }

    /**
     * Set the import file name
     *
     * @param string $name
     */
    public function setImportLogFile($name)
    {
        $this->_import_log_file = $name;
    }

    /**
     * Get the import file name
     *
     * @return string
     */
    public function getImportLogFile()
    {
        return $this->_import_log_file;
    }
}
