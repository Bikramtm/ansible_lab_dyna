<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class AdapterInterface
 */
interface Omnius_Import_Model_Adapter_AdapterInterface
{
    /**
     * @param array $fields List of fields that will be read from the file
     * @param string $class The class of the returned object
     * @param int $position Offset position in file
     * @param int $limit Limit the number of retrieved items
     * @return Mage_Core_Model_Abstract[]
     */
    public function import($fields, $class, $position, $limit);
}
