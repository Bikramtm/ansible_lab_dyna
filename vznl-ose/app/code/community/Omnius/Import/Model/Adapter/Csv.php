<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Import_Model_Adapter_Csv
 */
class Omnius_Import_Model_Adapter_Csv
    extends Mage_ImportExport_Model_Import_Adapter_Csv
    implements Omnius_Import_Model_Adapter_AdapterInterface
{

    /**
     * Set custom delimiter for the CSV file
     *
     * @param string $delimiter
     */
    public function setDelimiter($delimiter = ",")
    {
        $this->_delimiter = $delimiter;
    }

    /**
     * Get specific fields data from the CSV
     *
     * @param array $fields
     * @param string $class
     * @param int $position
     * @param int $limit
     * @throws Exception
     * @return array
     */
    public function import($fields, $class, $position, $limit)
    {
        $csvFile = $this->_init();

        try {
            $csvFile->seek($position);
        } catch (Exception $ex) {
            Mage::helper("omnius_import/data")->log($ex);
        }

        $result = array();

        try {
            if (!$csvFile->valid()) {
                throw new Exception('Invalid CSV File');
            }
            $fields = array_map('mb_strtolower', $fields);
            $count = 0;

            while (!is_null($csvFile->key()) && $count < $limit) {
                /** @var Mage_Core_Model_Abstract $temp */
                $temp = new $class();
                $v = $this->current();
                foreach ($v as $key => $val) {
                    if (in_array(mb_strtolower($key), $fields)) {
                        $temp->setData($key, $val);
                    }
                }
                $result[] = $temp;
                $csvFile->next();
                $count++;
            }
        } catch (Exception $ex) {
            Mage::helper("omnius_import/data")->log($ex);
        }

        return $result;
    }
}
