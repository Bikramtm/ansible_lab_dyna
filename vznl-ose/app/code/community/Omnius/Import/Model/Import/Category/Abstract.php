<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Import_Model_Import_Category_Abstract
 */
abstract class Omnius_Import_Model_Import_Category_Abstract extends Omnius_Import_Model_Import_ImportAbstract
{
    /**
     * @param array $data
     * @return mixed
     */
    abstract public function importCategory($data);

    /**
     * Get simple yes/no data from value
     *
     * @param string $data
     * @return int
     */
    protected function _getYesNoData($data)
    {
        $this->_log('Getting yes/no attribute for the value ' . $data, true);
        switch (strtolower($data)) {
            case 'ja':
            case 'y':
            case 'yes':
            case '1':
            case 'ok':
                $this->_log('Mapped value for ' . $data . ' is 1', true);
                return 1;
            case 'nee':
            case 'n':
            case 'no':
            case '0':
            case 'x':
                $this->_log('Mapped value for ' . $data . ' is 0', true);
                return 0;
            default:
                $this->_log('Mapped value for ' . $data . ' is 0 (default)', true);
                return 0;
        }
    }
}
