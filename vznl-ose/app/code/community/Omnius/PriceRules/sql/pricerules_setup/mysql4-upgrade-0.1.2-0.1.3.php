<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Eav_Model_Entity_Setup $this */
$installer = $this;

$installer->startSetup();
$conn = $installer->getConnection();
$conn->addColumn($this->getTable('salesrule'), 'is_promo', 'tinyint(1) unsigned not null');

$installer->endSetup();
