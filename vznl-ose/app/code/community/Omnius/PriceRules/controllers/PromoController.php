<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_PriceRules_PromoController extends Mage_Core_Controller_Front_Action
{
    /** @var Omnius_Core_Helper_Data|null */
    protected $coreHelper = null;

    /**
     * @return Omnius_Core_Helper_Data
     */
    protected function getCoreHelper()
    {
        if ($this->coreHelper === null) {
            $this->coreHelper = Mage::helper('omnius_core');
        }

        return $this->coreHelper;
    }

    /**
     * Tries to apply a promo rule
     */
    public function addRuleAction()
    {
       if (!$this->getRequest()->isPost()) {
            return;
        }
    
        $ruleId = $this->getRequest()->getParam('id');
        if ($ruleId) {
            $returnData = Mage::helper('pricerules')->applyPromoRule($ruleId, true);
            $this->jsonResponse($returnData);
        } else {
            $this->jsonResponse(array(
                'error' => true,
                'message' => 'Empty promo rule id',
            ));
        }
    }

    /**
     * @throws Exception
     */
    public function couponAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }

        $remove = $this->getRequest()->getParam('remove');
        $couponCode = $remove ? '' : $this->getRequest()->getParam('coupon'); // Empty coupon code param when removing
        $packageId = $this->getRequest()->getParam('packageId');
        $parentClass = $this->getRequest()->getParam('parentClass');
        $isCheckout = $this->getRequest()->getParam('isCheckout') == 'true' ? true : false;

        /** @var Omnius_PriceRules_Helper_External_Coupon_Validator $externalValidator */
        $externalValidator = Mage::helper('pricerules/external_coupon_validator');
        
        // Throw error when required parameters are not provided
        if (!$packageId || (!$couponCode && !$remove)) {
            return $this->getResponse()
                ->setHeader('Content-Type', 'application/json')
                ->setBody(Mage::helper('core')->jsonEncode(array(
                    'error' => true,
                    'message' => $this->__('Please provide the Coupon code and package ID')
                )));
        }

        $quote = Mage::getSingleton('checkout/cart')->getQuote();

        /** @var Omnius_Package_Model_Package $packageModel */
        $packageModel = Mage::getModel('package/package')->getCollection()
            ->addFieldToFilter('quote_id', $quote->getId())
            ->addFieldToFilter('package_id', $packageId)
            ->getFirstItem();

        if (Mage::getSingleton('customer/session')->getOrderEdit()) {
            $orderEditQuote = Mage::getModel('sales/quote')->load(Mage::getSingleton('customer/session')->getOrderEdit());

            /** @var Dyna_Package_Model_Package $packageModel */
            $oldPackage = Mage::getModel('package/package')->getCollection()
                ->addFieldToFilter('package_id', $packageId)
                ->addFieldToFilter('order_id', $orderEditQuote->getSuperOrderEditId())
                ->getFirstItem();

            $originalPackage = $oldPackage->getId();
        } else {
            $originalPackage = clone $packageModel;
        }

        if (!$remove) {
            $coupon = Mage::getModel('salesrule/coupon')->load($couponCode, 'code');
            if (!$coupon->getId() && !$externalValidator->isValid($couponCode, $quote, $originalPackage)) {
                $this->getResponse()
                    ->setHeader('Content-Type', 'application/json')
                    ->setBody(Mage::helper('core')->jsonEncode(array(
                        'error' => true,
                        'message' => $this->__('Invalid coupon code')
                    )));

                return;
            }
        }

        $quote->setCurrentStep(null);
        $customerSession = Mage::getSingleton('customer/session');

        $quote->getShippingAddress()
            ->setCollectShippingRates(true);

        $prevActivePackageId = $quote->getActivePackageId();
        $quote->setCouponsPerPackage(array($packageId => $couponCode));

        if ($externalValidator->isValid($couponCode, $quote, $originalPackage)) { //Coupon is used by other modules
            $packageModel->setCoupon($couponCode)->save();

            if (!Mage::getSingleton('customer/session')->getOrderEdit()) {
                Mage::dispatchEvent('pricerules_promo_coupon_update_external_usage', [
                    'quote' => $quote,
                    'package' => $packageModel
                ]);
            }

            $packageModel->save();
        }

        if ($remove) {
            Mage::dispatchEvent('pricerules_promo_removed_coupon', [
                'package' => $packageModel
            ]);
        }

        $quote->setActivePackageId($prevActivePackageId != $packageId ? $prevActivePackageId : $packageId)
            ->collectTotals()
            ->save();

        /** @var Omnius_Package_Model_Package $checkPackage */
        $checkPackage = Mage::getModel('package/package')->getCollection()
            ->addFieldToFilter('quote_id', $quote->getId())
            ->addFieldToFilter('package_id', $packageId)
            ->getFirstItem();

        $packageTotals = Mage::helper('omnius_configurator')->getActivePackageTotals($packageId, !($customerSession->getCustomer() && $customerSession->getCustomer()->getIsBusiness()),
            $quote->getId());

        $quoteTotals = array(
            'quoteGrandTotal' => array(
                'rawValue' => $quote->getGrandTotal(),
                'formatted' => Mage::helper('core')->currency($quote->getGrandTotal(), true, false)
            ),
            'packageTotal' => Mage::helper('core')->currency($packageTotals['totalprice'], true, false)
        );

        $quoteCouponCode = $checkPackage->getCoupon();//get the applied coupon code from the quote
        $rightBlockCollapsed = $this->getLayout()->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/partials/package_block.phtml');
        switch ($parentClass) {
            case '.package-expand':
                $rightBlock = $this->getLayout()->createBlock('core/template')->setCheckout($isCheckout)->setTemplate('customer/right_sidebar.phtml');
                $bodyResponse = [
                    'error' => false,
                    'message' => $remove ? $this->__('Coupon removed') : $this->__('Coupon applied'),
                    'totals' => $this->getLayout()->createBlock('checkout/cart_totals')->setCheckoutButton($isCheckout)->setTemplate('checkout/cart/totals.phtml')->toHtml(),
                    'rightBlock' => $rightBlock->setCurrentPackageId($packageId)->toHtml(),
                    'rightBlockCollapsed' => $rightBlockCollapsed->setCurrentPackageId($packageId)->toHtml(),
                    'quoteTotals' => $quoteTotals ];
                break;
            case '.side-cart':
                $rightBlock = $this->getLayout()->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/partials/package_block.phtml');
                $bodyResponse = array(
                    'error' => false,
                    'message' => $remove ? $this->__('Coupon removed') : $this->__('Coupon applied'),
                    'totals' => $this->getLayout()->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/totals.phtml')->toHtml(),
                    'rightBlock' => $rightBlock->setCurrentPackageId($packageId)->toHtml(),
                    'rightBlockCollapsed' => $rightBlockCollapsed->setCurrentPackageId($packageId)->toHtml(),
                );
                break;
            default:
                break;
        }

        if ($quoteCouponCode) {
            $this->getResponse()
                ->setHeader('Content-Type', 'application/json')
                ->setBody(Mage::helper('core')->jsonEncode($bodyResponse));
        } else {
            if (!$remove) {
                $this->getResponse()
                    ->setHeader('Content-Type', 'application/json')
                    ->setBody(Mage::helper('core')->jsonEncode(array(
                        'error' => true,
                        'message' => $this->__('Invalid coupon code')
                    )));
            } else {
                $this->getResponse()
                    ->setHeader('Content-Type', 'application/json')
                    ->setBody(Mage::helper('core')->jsonEncode($bodyResponse));
            }
        }
    }

    /**
     * @param $data
     * @param int $statusCode
     * @throws InvalidArgumentException
     */
    protected function jsonResponse($data, $statusCode = 200)
    {
        $response = $this->getCoreHelper()
            ->jsonResponse($data, $statusCode);

        $this->getResponse()
            ->setBody($response['body'])
            ->setHeader('Content-Type', 'application/json')
            ->setHttpResponseCode($response['status']);
    }

    /**
     * Render a template with the search results
     */
    public function searchAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }
        $this->jsonResponse(array(
            'error' => false,
            'rules' => $this->getLayout()->createBlock('core/template')->setTemplate('configurator/mobile/sections/partials/general/promo_rules.phtml')->toHtml()
        ));
    }
}
