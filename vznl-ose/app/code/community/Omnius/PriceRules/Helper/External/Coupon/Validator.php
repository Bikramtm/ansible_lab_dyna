<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Omnius_PriceRules_Helper_External_Coupon_Validator
{
    const CHECK_EXTERNAL_COUPON_EVENT_INDEX = 'pricerules_helper_external_coupon_validator';

    public function isValid($coupon, $quote, $package)
    {
        $validated = [];

        Mage::dispatchEvent(self::CHECK_EXTERNAL_COUPON_EVENT_INDEX, [
            'coupon' => $coupon,
            'quote' => $quote,
            'package' => $package,
            'validate' => function ($moduleName) use (&$validated) {
                $validated[] = $moduleName;
            }
        ]);

        return (boolean) count($validated);
    }
}
