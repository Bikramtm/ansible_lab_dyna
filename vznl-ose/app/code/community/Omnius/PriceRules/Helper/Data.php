<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_PriceRules_Helper_Data
 */
class Omnius_PriceRules_Helper_Data extends Mage_Core_Helper_Abstract
{
    // Max iterations for automatic sale rules
    public static $maxIterations = 10;

    /**
     * Cached promo rules.
     *
     * @var array
     */
    protected $promoRules = array();

    /**
     * @param bool $returnCount
     * @param int $activePackageId
     * @return array|int
     *
     * Finds what promo rules can be applied for a certain package
     */
    public function findApplicablePromoRules($returnCount = false, $activePackageId = null)
    {
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        if (!$activePackageId) {
            $activePackageId = $quote->getActivePackageId();
        }
        $quoteItems = $quote->getItemsCollection();
        $cacheKey = $activePackageId . '_' . implode('_', $quoteItems->getColumnValues('item_id'));

        if (!isset($this->promoRules[$cacheKey])) {
            $applicableRules = [];
            $appliedPromoRules = [];

            /** @var Omnius_PriceRules_Model_Validator $validator */
            $validator = Mage::getModel('pricerules/validator');
            $shippingAddress = $quote->getShippingAddress();

            $rules = Mage::helper('rulematch')
                ->setActivePackage(Mage::getSingleton('checkout/cart')->getQuote()->getCartPackage($activePackageId))
                ->getApplicableRules(true);

            foreach ($quoteItems as $quoteItem) {
                if ($quoteItem->getPackageId() == $activePackageId && !$quoteItem->isDeleted()) {
                    $this->parseApplicableRule($activePackageId, $quoteItem, $appliedPromoRules, $rules, $validator, $shippingAddress, $applicableRules);
                }
            }

            $this->promoRules[$cacheKey] = $applicableRules;
        }

        return $returnCount ? count($this->promoRules[$cacheKey]) : $this->promoRules[$cacheKey];
    }

    /**
     * Manually apply promo rules. Set them to all products in a package and let Magento filter them out if not applicable
     *
     * @param int $ruleId
     * @param bool $returnJsonData
     * @param bool $collect
     * @return array
     *
     * Try to apply a promo rule
     */
    public function applyPromoRule($ruleId, $returnJsonData = false, $collect = true)
    {

        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $activePackageId = $quote->getActivePackageId();
        $ruleId = ($ruleId > 0) ? $ruleId : '';


        $updated = false;
        /** @var Omnius_Checkout_Model_Sales_Quote_Item $item */
        foreach ($quote->getItemsCollection() as $item) {
            $item->setRemoveRuleId($item->getAppliedRuleIds());
            if (!$item->isDeleted() && !$item->getParentItemId() && !$item->getIsPromo()) { //only visible items
                if ($item->getPackageId() != $activePackageId) {
                    continue;
                }

                $item->setManualRuleId($ruleId);
                $item->setPreviousRules($item->getAppliedRuleIds());
                $item->setAppliedRuleIds($ruleId);
                $item->save();
                $updated = true;
            }
        }

        if (!$updated) {
            return array(
                'error' => true,
                'message' => 'Could not apply promo rule',
            );
        }

        if ($collect) {
            $quote->setTotalsCollectedFlag(false)
                ->collectTotals()
                ->save();
        }

        Mage::getSingleton('checkout/cart')->getQuote()->setData('collect_totals_cache_items', false);

        return $returnJsonData ? array(
            'error' => false,
            'totals' => Mage::getSingleton('core/layout')->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/totals.phtml')->toHtml(),
            'rightBlock' => Mage::getSingleton('core/layout')->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/partials/package_block.phtml')->setCurrentPackageId($activePackageId)->toHtml(),
        ) : '';
    }

    /**
     * Returns only not empty values
     * @param $var
     * @return string
     */
    public function checkEmptyValue($var)
    {
        $trimmed = trim($var);
        if (!empty($trimmed)) {
            return $trimmed;
        }
    }

    /**
     * Returns array of trimmed values if the values after trim are not empty.
     * @param $string string
     * @param $separator string
     * @return array
     */
    public function explodeTrimmed($string, $separator = ',')
    {
        return array_filter(
            explode($separator, $string),
            array($this, 'checkEmptyValue')
        );
    }

    /**
     * @param $couponCode
     * @param null $customerId
     * @throws Exception
     */
    public function decrementCoupon($couponCode, $customerId = null)
    {
        /** @var Mage_SalesRule_Model_Coupon */
        $coupon = Mage::getModel('salesrule/coupon');
        $coupon->load($couponCode, 'code');
        if ($coupon->getId() && $this->couponCanBeDecremented($couponCode)) {
            $coupon->setTimesUsed($coupon->getTimesUsed() - 1)->save();

            $oRule = Mage::getModel('salesrule/rule')->load($coupon->getRuleId());
            $oRule->setTimesUsed($oRule->getTimesUsed() - 1)->save();

            if ($customerId) {
                $ruleCustomer = Mage::getModel('salesrule/rule_customer');
                $ruleCustomer->loadByCustomerRule($customerId, $oRule->getId());

                if ($ruleCustomer->getId() && $ruleCustomer->getTimesUsed() > 0) {
                    $ruleCustomer->setTimesUsed($ruleCustomer->getTimesUsed() - 1);
                    $ruleCustomer->save();
                }

                $this->reduceCustomerCouponTimesUsed($customerId, $coupon->getId());
            }
        }
    }

    /**
     * @param $couponCode
     * @param null $customerId
     * @throws Exception
     * @return $this
     */
    public function incrementCoupon($couponCode, $customerId = null)
    {
        if (!$this->couponCanBeDecremented($couponCode)) {
            return $this;
        }
        /** @var Mage_SalesRule_Model_Coupon */
        $coupon = Mage::getModel('salesrule/coupon');
        $coupon->load($couponCode, 'code');
        if ($coupon->getId()) {
            $coupon->setTimesUsed($coupon->getTimesUsed() + 1);
            $oRule = Mage::getModel('salesrule/rule')->load($coupon->getRuleId());
            $oRule->setTimesUsed($oRule->getTimesUsed() + 1)->save();
            $coupon->save();
            if ($customerId) {
                $this->incrementCouponCustomer($coupon, $customerId);
            }
        }
    }

    /**
     * @param Mage_SalesRule_Model_Coupon $coupon
     * @param int|null $customerId
     * @throws Exception
     */
    public function incrementCouponCustomer($coupon, $customerId = null)
    {
        $ruleCustomer = Mage::getModel('salesrule/rule_customer');
        $ruleCustomer->loadByCustomerRule($customerId, $coupon->getRuleId());

        if ($ruleCustomer->getId() && $ruleCustomer->getTimesUsed() > 0) {
            $ruleCustomer->setTimesUsed($ruleCustomer->getTimesUsed() + 1);
        } else {
            $ruleCustomer
                ->setCustomerId($customerId)
                ->setRuleId($coupon->getRuleId())
                ->setTimesUsed(1);
        }
        $ruleCustomer->save();

        Mage::getResourceModel('salesrule/coupon_usage')->updateCustomerCouponTimesUsed($customerId, $coupon->getId());
    }

    /**
     * @param $customerId int
     * @param $couponId int
     * @param $decrement int
     */
    public function reduceCustomerCouponTimesUsed($customerId, $couponId, $decrement = 1)
    {
        $couponUsage = Mage::getResourceModel('salesrule/coupon_usage');
        $write = Mage::getSingleton('core/resource')->getConnection('core_write');
        $select = $write->select();
        $select->from($couponUsage->getMainTable(), array('times_used'))
            ->where('coupon_id = :coupon_id')
            ->where('customer_id = :customer_id');

        $timesUsed = $write->fetchOne($select, array(':coupon_id' => $couponId, ':customer_id' => $customerId));

        if ($timesUsed > $decrement) {
            $write->update(
                $couponUsage->getMainTable(),
                array(
                    'times_used' => $timesUsed - $decrement
                ),
                array(
                    'coupon_id = ?' => $couponId,
                    'customer_id = ?' => $customerId,
                )
            );
        } else {
            $write->delete(
                $couponUsage->getMainTable(),
                array(
                    'coupon_id = ?' => $couponId,
                    'customer_id = ?' => $customerId,
                )
            );
        }
    }

    /**
     * Decrements all rules and coupons times used for the quote
     * @param $quote Mage_Sales_Model_Quote
     */
    public function decrementAll($quote)
    {
        /** @var Varien_Db_Adapter_Interface $connection */
        $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
        $sql = 'SELECT coupon FROM catalog_package WHERE quote_id=:id';
        foreach ($connection->fetchCol($sql, array(':id' => $quote->getId())) as $couponCode) {
            $this->decrementCoupon($couponCode, $quote->getCustomerId());
        }

        foreach ($quote->getAllItems() as $item) {
            $this->decrementTimesUsedOfRule($item);
        }
    }

    /**
     * @param $item Mage_Sales_Model_Quote_Item
     * @throws Exception
     */
    public function decrementTimesUsedOfRule($item)
    {
        $appliedRuleIds = Mage::helper('pricerules')->explodeTrimmed($item->getAppliedRuleIds());
        $appliedRuleCollection = Mage::getResourceModel('salesrule/rule_collection')
            ->addFieldToFilter('rule_id', array('in' => $appliedRuleIds));
        $customerId = $item->getQuote()->getCustomer()->getId();
        /** @var Omnius_Checkout_Model_SalesRule_Rule $rule */
        foreach ($appliedRuleCollection as $rule) {
            if (
                $rule->getCouponType() == Mage_SalesRule_Model_Rule::COUPON_TYPE_NO_COUPON
                && $this->canBeDecremented($item, $rule->getId())
            ) {
                $rule->updateFields(['times_used' => $rule->getTimesUsed() - 1]);
                if ($customerId) {
                    $ruleCustomer = Mage::getModel('salesrule/rule_customer');
                    $ruleCustomer->loadByCustomerRule($customerId, $rule->getId());
                    if ($ruleCustomer->getId() && $ruleCustomer->getTimesUsed() > 0) {
                        $ruleCustomer->setTimesUsed($ruleCustomer->getTimesUsed() - 1)->save();
                    }
                }
            }
        }
    }

    /**
     * Checks whether the rule times used can be decremented or not.
     * @param $item Omnius_Checkout_Model_Sales_Quote_Item
     * @param $ruleId
     * @return bool
     */
    public function canBeDecremented($item, $ruleId)
    {
        $orderEdit = Mage::getSingleton('customer/session')->getOrderEdit();
        if ($orderEdit) {
            /** @var Varien_Db_Adapter_Interface $connection */
            $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
            $sql = 'SELECT applied_rule_ids FROM sales_flat_quote_item WHERE quote_id=:quote AND package_id=:package';
            $binds = array(
                ':quote' => (string) $orderEdit,
                ':package' => (string) $item->getPackageId()
            );

            $rules = array();
            foreach ($connection->fetchAll($sql, $binds) as $i) {
                $rules = array_merge($rules, $this->explodeTrimmed($i['applied_rule_ids']));
            }
            if (in_array($ruleId, $rules)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Checks whether the coupon times used can be decremented or not.
     * @param $coupon
     * @return bool
     */
    public function couponCanBeDecremented($coupon)
    {
        $orderEdit = Mage::getSingleton('customer/session')->getOrderEdit();
        if ($orderEdit) {
            /** @var Varien_Db_Adapter_Interface $connection */
            $connection = Mage::getSingleton('core/resource')->getConnection('core_write');

            return 0 === (int) $connection
                    ->fetchOne('SELECT count(entity_id) FROM catalog_package WHERE quote_id=:quote AND coupon=:coupon',
                        array(
                            ':quote' => (string) $orderEdit,
                            ':coupon' => (string) $coupon
                        )
                    );
        }

        return true;
    }

    /**
     * Formats price with
     * @param float|string $value
     * @param bool|true $showDot
     * @return string
     * @throws Zend_Locale_Exception
     */
    public function customPriceFormat($value, $showDot = true)
    {
        $currencyModel = Mage::getModel('directory/currency');
        return $currencyModel->format(
            $value,
            [
                'format'    => $showDot ? $currencyModel->getNumberFormat() : $currencyModel->getNumberFormatNoDot(),
                'display'   => Zend_Currency::NO_SYMBOL
            ],
            false
        );
    }

    /**
     * Increments times used for all the rules from the provided quote.
     * @param $quote
     */
    public function reinitSalesRules($quote)
    {
        $packageCollection = Mage::getResourceModel('package/package_collection')
            ->addFieldToFilter('quote_id', $quote->getId());
        $couponsPerPackage = array();
        $retainableCtns = Mage::helper('omnius_customer')->getRetainableCtns($quote);

        foreach ($packageCollection as $package) {
            $couponsPerPackage[$package->getPackageId()] = $package->getCoupon();
            $package->setCoupon(null);
            // Check if the ctn from retention packages is still retainable
            if ($package->getSaleType() == Omnius_Checkout_Model_Sales_Quote_Item::RETENTION) {
                if (!isset($ctnsList)) {
                    $extractCtns = function ($arr) {
                        return $arr['ctn'];
                    };
                    $retainableCtns['ctns'] = !empty($retainableCtns['ctns']) ? $retainableCtns['ctns'] : [];
                    // Create an array with all the retainable ctns for this customer
                    $ctnsList = array_map($extractCtns, $retainableCtns['ctns']);
                }

                if (in_array($package->getCtn(), $ctnsList) === false) {
                    // Set package type to aquisition if the ctn is not retainable anymore
                    $package->setSaleType(Omnius_Checkout_Model_Sales_Quote_Item::ACQUISITION)
                        ->setCtn(null);
                }
            }
            $package->save();
        }

        $quote->setCouponsPerPackage($couponsPerPackage);

        foreach ($quote->getAllItems() as $item) {
            $item->setManualRuleId($item->getAppliedRuleIds());
            $ruleIds = Mage::helper('pricerules')->explodeTrimmed($item->getAppliedRuleIds());
            $keep = [];
            foreach ($ruleIds as $ruleIdentifier) {
                $rule = Mage::getModel('salesrule/rule')->load($ruleIdentifier);
                if ($rule->getId() && $rule->getIsPromo()) {
                    $keep[] = $rule->getId();
                }
            }
            $item->setAppliedRuleIds(implode(',', $keep));
        }

        $quote->getBillingAddress();
        $quote->getShippingAddress()->setCollectShippingRates(true);
        $quote->collectTotals();
        $quote->save();

        return $quote;
    }


    /**
     * @param Omnius_Checkout_Model_Sales_Quote_Item $item
     * @param Omnius_Checkout_Model_Sales_Quote $quote
     * @return bool
     */
    public function promotionsOverride($item, $quote)
    {
        /** @var Omnius_Package_Model_Package $package */
        $package = $quote->getPackageModels()->getItemByColumnValue('package_id', $item->getPackageId());
        $isDelivered = $package && $package->isDelivered();
        return $this->shouldOverride($quote)
            || (
                $isDelivered
                && Mage::getSingleton('customer/session')->getOrderEditMode()
                && $this->productIsInInitialOrder($item, $quote)
            );
    }

    /**
     * @param Omnius_Checkout_Model_Sales_Quote_Item $item
     * @param Omnius_Checkout_Model_Sales_Quote $quote
     * @return bool
     */
    public function productIsInInitialOrder($item, $quote)
    {
        $initialItems = Mage::getSingleton('customer/session')->getOrderEditMode() ? Mage::getSingleton('checkout/session')->getInitialItems() : [];
        return
            (
                !$item->isPromo()
                && isset($initialItems[$item->getProductId()])
                && $quote->hasProductId($item->getProductId())
            )
            || (
                $item->isPromo()
                && isset($initialItems[$item->getTargetId()])
                && $quote->hasProductId($item->getTargetId())
            );
    }

    /**
     * @param Omnius_Checkout_Model_Sales_Quote $quote
     * @return bool
     */
    public function shouldOverride($quote)
    {
        return Mage::registry('is_override_promos')
        || $quote->getData('collect_totals_cache_items')
        || $quote->getIsOverrulePromoMode();
    }

    /**
     * @param $entry
     * @param $path
     * @param $values
     */
    public function getValues($entry, $path, &$values)
    {
        $r = pathinfo($entry);
        if ($r['extension'] == 'csv') {
            $values[] = array(
                'label' => $entry,
                'value' => $path . DS . $entry
            );
        }
    }

    /**
     * Checks for applicable promo rules on the current package.
     * @param $activePackageId
     * @param $quoteItem
     * @param $appliedPromoRules
     * @param $rules
     * @param $validator
     * @param $shippingAddress
     * @param $applicableRules
     */
    protected function parseApplicableRule($activePackageId, $quoteItem, $appliedPromoRules, $rules, $validator, $shippingAddress, &$applicableRules)
    {
        if (!$quoteItem->isPromo() && $quoteItem->getAppliedRuleIds()) {
            $appliedPromoRules = array_merge(
                $appliedPromoRules,
                explode(",", $quoteItem->getAppliedRuleIds())
            );
        }
        foreach ($rules as $rule) {
            if ($rule->getIsPromo() == 1
                && $validator->canProcessRules($rule, $shippingAddress, $activePackageId)
                && $validator->validateItemByRule($rule, $quoteItem)
            ) {
                if (in_array($rule->getRuleId(), $appliedPromoRules)) {
                    $rule->setIsCurrentlyApplied(true);
                }
                $applicableRules[$rule->getRuleId()] = $rule;
            }
        }
    }
}
