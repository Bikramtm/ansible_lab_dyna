<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_PriceRules_Model_Eav_Entity_Attribute_Source_Dealergroup
 */
class Omnius_PriceRules_Model_Eav_Entity_Attribute_Source_Dealergroup extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
    /**
     * Retrieve all options array
     *
     * @return array
     */
    public function getAllOptions()
    {
        $conn = Mage::getSingleton('core/resource')->getConnection('core_read');
        $campaigns = $conn->fetchPairs('SELECT group_id, name from dealer_group');
        $this->_options = array();
        foreach ($campaigns as $id => $name) {
            $this->_options[] = array('label'=>Mage::helper("eav")->__($name), 'value' => $id);
        }
        return $this->_options;
    }

    /**
     * Retrieve option array
     *
     * @return array
     */
    public function getOptionArray()
    {
        $_options = array();
        foreach ($this->getAllOptions() as $option) {
            $_options[$option["value"]] = $option["label"];
        }
        return $_options;
    }

    /**
     * Get a text for option value
     *
     * @param string|integer $value
     * @return string
     */
    public function getOptionText($value)
    {
        $options = $this->getAllOptions();
        foreach ($options as $option) {
            if ($option["value"] == $value) {
                return $option["label"];
            }
        }
        return false;
    }
}
