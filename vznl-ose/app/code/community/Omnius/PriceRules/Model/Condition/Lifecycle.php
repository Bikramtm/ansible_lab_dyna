<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_PriceRules_Model_Condition_Lifecycle
 * Adds lifecycle condition to sales rules.
 */
class Omnius_PriceRules_Model_Condition_Lifecycle extends Omnius_PriceRules_Model_Condition_Abstract
{
    const ACQUISITION = 'acquisition'; //aquisition
    const RETENTION = 'retention';

    /**
     * @return $this
     */
    public function loadAttributeOptions()
    {
        $this->setupSelectCondition('Lifecycle', 'lifecycle');
        return $this;
    }

    /**
     * Returns available condition options.
     * @return mixed
     */
    public function getValueSelectOptions()
    {
        $segmentOptionsModel = Mage::getModel('pricerules/eav_entity_attribute_source_lifecycle');
        $options = $segmentOptionsModel->getAllOptions();
        return $this->setupOptions($options);
    }

    /**
     * Validates condition. 
     * @param Varien_Object $object
     * @return bool
     */
    public function validate(Varien_Object $object)
    {
        $quote = $object->getQuote();
        $package = Mage::getModel('package/package')->getCollection()
            ->addFieldToFilter('quote_id', $quote->getId())
            ->addFieldToFilter('package_id', $quote->getActivePackageId())
            ->getFirstItem();
        if ($package->getId()) {
            return $this->validateAttribute($package->getSaleType());
        }
        return false;
    }
}
