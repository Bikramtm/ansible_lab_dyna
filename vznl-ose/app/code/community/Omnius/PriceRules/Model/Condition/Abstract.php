<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_PriceRules_Model_Condition_Abstract
 */
class Omnius_PriceRules_Model_Condition_Abstract extends Mage_Rule_Model_Condition_Abstract
{
    /** @var Dyna_Cache_Model_Cache */
    protected $_cache;

    /**
     * Validate product attribute value for condition
     *
     * @param   mixed $validatedValue product attribute value
     * @return  bool
     */
    public function validateAttribute($validatedValue)
    {
        if (is_object($validatedValue)) {
            return false;
        }

        $key = md5(serialize(array(array($validatedValue), $this->getValue(), $this->getId())));
        if (!is_array($result = (unserialize($this->getCache()->load($key))))) {
            /**
             * Condition attribute value
             */
            $value = $this->getValueParsed();

            /**
             * Comparison operator
             */
            $op = $this->getOperatorForValidate();

            // if operator requires array and it is not, or on opposite, return false
            if ($this->isArrayOperatorType() xor is_array($value)) {
                return false;
            }

            try {
                $result = $this->handleOperator($validatedValue, $op, $value);

                $notCond = ('!=' == $op) || ('!{}' == $op) || ('!()' == $op);
                if ('>' == $op || '<' == $op || $notCond) {
                    $result = !$result;
                }

                $result = array('valid' => $result);
                $this->getCache()->save(serialize($result), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
            } catch (Varien_Exception $e) {
                $result = array('valid' => false);
            }
        }

        return $result['valid'];
    }

    /**
     * @return Dyna_Cache_Model_Cache
     */
    protected function getCache()
    {
        if (!$this->_cache) {
            $this->_cache = Mage::getSingleton('dyna_cache/cache');
        }

        return $this->_cache;
    }

    /**
     * Retrieves the type of input for the attribute.
     * @return mixed
     */
    public function getInputType()
    {
        $attributesMeta = $this->getAttributesMeta();

        return $attributesMeta[$this->getAttribute()]['input_type'];
    }

    /**
     * @return mixed
     */
    public function getAttributeElement()
    {
        $element = parent::getAttributeElement();
        $element->setShowAsText(true);

        return $element;
    }

    /**
     * @return mixed
     */
    public function getValueElementType()
    {
        $attributesMeta = $this->getAttributesMeta();

        return $attributesMeta[$this->getAttribute()]['value_element_type'];
    }

    /**
     * @param $label
     * @param $handle
     */
    protected function setupSelectCondition($label, $handle)
    {
        $this->setAttributesMeta(
            array(
                $handle => array(
                    'label' => Mage::helper('pricerules')->__($label),
                    'input_type' => 'select',
                    'value_element_type' => 'select'
                )
            )
        );
        foreach ($this->getAttributesMeta() as $attribute => $attributeMeta) {
            $attributes[$attribute] = $attributeMeta['label'];
        }
        $this->setAttributeOption($attributes);
    }

    /**
     * @param $label
     * @param $handle
     */
    protected function setupTextCondition($label, $handle)
    {
        $this->setAttributesMeta(
            array(
                $handle => array(
                    'label' => Mage::helper('pricerules')->__($label),
                    'input_type' => 'select',
                    'value_element_type' => 'text'
                )
            )
        );
        foreach ($this->getAttributesMeta() as $attribute => $attributeMeta) {
            $attributes[$attribute] = $attributeMeta['label'];
        }
        $this->setAttributeOption($attributes);
    }

    /**
     * @param $options
     * @return mixed
     */
    protected function setupOptions($options)
    {
        if (!$this->hasData('value_select_options')) {
            $this->setData('value_select_options', $options);
        }

        return $this->getData('value_select_options');
    }

    /**
     * @param $validatedValue
     * @param $op
     * @param $value
     * @return array|bool
     * @throws Varien_Exception
     */
    protected function handleOperator($validatedValue, $op, $value)
    {
        switch ($op) {
            case '==':
            case '!=':
                $result = $this->parseEqual($validatedValue, $value);
                break;
            case '<=':
            case '>':
                $result = $this->parseLower($validatedValue, $value);
                break;
            case '>=':
            case '<':
                $result = $this->parseHigher($validatedValue, $value);
                break;
            case '{}':
            case '!{}':
                $result = $this->parseBraces($validatedValue, $value);
                break;
            case '()':
            case '!()':
                $result = $this->parseParentheses($validatedValue, $value);
                break;
            default:
                $result = false;
                break;
        }

        return $result;
    }

    /**
     * @param $validatedValue
     * @param $value
     * @return bool
     */
    protected function parseParentheses($validatedValue, $value)
    {
        $result = false;
        if (is_array($validatedValue)) {
            $result = count(array_intersect($validatedValue, (array) $value)) > 0;
        } else {
            $value = (array) $value;
            foreach ($value as $item) {
                if ($this->_compareValues($validatedValue, $item)) {
                    $result = true;
                    break;
                }
            }
        }

        return $result;
    }

    /**
     * @param $validatedValue
     * @param $value
     * @return array|bool
     * @throws Varien_Exception
     */
    protected function parseBraces($validatedValue, $value)
    {
        $result = false;
        if (is_scalar($validatedValue) && is_array($value)) {
            foreach ($value as $item) {
                if (stripos($validatedValue, $item) !== false) {
                    $result = true;
                    break;
                }
            }
        } elseif (is_array($value)) {
            if (is_array($validatedValue)) {
                $result = array_intersect($value, $validatedValue);
                $result = !empty($result);
            } else {
                throw new Varien_Exception("return false");
            }
        } else {
            if (is_array($validatedValue)) {
                $result = in_array($value, $validatedValue);
            } else {
                $result = $this->_compareValues($value, $validatedValue, false);
            }
        }

        return $result;
    }

    /**
     * @param $validatedValue
     * @param $value
     * @return array|bool
     * @throws Varien_Exception
     */
    protected function parseEqual($validatedValue, $value)
    {
        if (is_array($value)) {
            if (is_array($validatedValue)) {
                $result = array_intersect($value, $validatedValue);
                $result = !empty($result);
            } else {
                throw new Varien_Exception("return false");
            }
        } else {
            if (is_array($validatedValue)) {
                $result = count($validatedValue) == 1 && array_shift($validatedValue) == $value;
            } else {
                $result = $this->_compareValues($validatedValue, $value);
            }
        }

        return $result;
    }

    /**
     * @param $validatedValue
     * @param $value
     * @return bool
     * @throws Varien_Exception
     */
    protected function parseLower($validatedValue, $value)
    {
        if (!is_scalar($validatedValue)) {
            throw new Varien_Exception("return false");
        } else {
            $result = $validatedValue <= $value;
        }

        return $result;
    }

    /**
     * @param $validatedValue
     * @param $value
     * @return bool
     * @throws Varien_Exception
     */
    protected function parseHigher($validatedValue, $value)
    {
        if (!is_scalar($validatedValue)) {
            throw new Varien_Exception("return false");
        } else {
            $result = $validatedValue >= $value;

        }

        return $result;
    }
}
