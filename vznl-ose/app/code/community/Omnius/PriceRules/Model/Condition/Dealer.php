<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_PriceRules_Model_Condition_Dealer
 * Adds dealer condition to the sales rules.
 */
class Omnius_PriceRules_Model_Condition_Dealer extends Omnius_PriceRules_Model_Condition_Abstract
{

    /**
     * @return $this
     */
    public function loadAttributeOptions()
    {
        $this->setupTextCondition('Dealer code', 'dealer');
        return $this;
    }

    /**
     * Returns the customer session.
     * @return mixed
     */
    protected function _getSession()
    {
        return Mage::getSingleton('customer/session');
    }

    /**
     * Validates the condition.
     * @param Varien_Object $object
     * @return bool
     */
    public function validate(Varien_Object $object)
    {
        $dealerId = $this->_getSession()->getAgent()->getDealer()->getVfDealerCode();
        return $this->validateAttribute($dealerId);
    }

    /**
     * Returns types of condition operators.
     * @return array
     */
    public function getOperatorSelectOptions()
    {
        $options = array(
            '==' => Mage::helper('rule')->__('is'),
            '!=' => Mage::helper('rule')->__('is not'),
            '()' => Mage::helper('rule')->__('is one of'),
            '!()' => Mage::helper('rule')->__('is not one of')
        );

        $operatorOptions = array();
        foreach ($options as $value => $label) {
            $operatorOptions[] = array(
                'value' => $value,
                'label' => $label,
            );
        }

        return $operatorOptions;
    }
}
