<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_PriceRules_Block_Adminhtml_Promo_Quote_Grid
 */
class Omnius_PriceRules_Block_Adminhtml_Promo_Quote_Grid extends Mage_Adminhtml_Block_Promo_Quote_Grid
{
    /**
     * Add grid columns
     *
     * @return Mage_Adminhtml_Block_Promo_Quote_Grid
     */
    protected function _prepareColumns()
    {
        parent::_prepareColumns();

        $this->addColumn('locked', array(
            'header'    => Mage::helper('productmatchrule')->__('Locked'),
            'align'     =>'left',
            'after'     =>'status',
            'width'     => '30px',
            'index'     => 'locked',
            'type'      => 'options',
            'options'   => array(
                1 => 'Locked',
                0 => 'Unlocked',
            ),
        ));

        if ($this->_isExport) {
            $this->_prepareExportColumns();
        }

        $this->addExportType('*/*/exportCsv', Mage::helper('pricerules')->__('CSV'));

        return $this;
    }

    /**
     * Prepare export columns
     *
     * @throws Exception
     */
    protected function _prepareExportColumns()
    {
        $this->removeColumn('rule_id');
        $this->removeColumn('name');
        $this->removeColumn('coupon_code');
        $this->removeColumn('from_date');
        $this->removeColumn('to_date');
        $this->removeColumn('is_active');
        $this->removeColumn('rule_website');
        $this->removeColumn('sort_order');
        $this->removeColumn('locked');

        $this->addColumn('name', array(
            'header'    => 'rule_name',
            'index'     => 'name',
        ));
        $this->addColumn('description', array(
            'header'    => 'description',
            'index'     => 'description',
        ));
        $this->addColumn('from_date', array(
            'header'    => 'from_date',
            'type'      => 'date',
            'index'     => 'from_date',
        ));
        $this->addColumn('to_date', array(
            'header'    => 'to_date',
            'type'      => 'date',
            'index'     => 'to_date',
        ));
        $this->addColumn('uses_per_customer', array(
            'header'    => 'uses_per_customer',
            'index'     => 'uses_per_customer',
        ));
        $this->addColumn('is_active', array(
            'header'    => 'status',
            'index'     => 'is_active',
        ));
        $this->addColumn('conditions_serialized', array(
            'header'    => 'conditions',
            'index'     => 'conditions_serialized',
            'type'      => 'text',
            'truncate'  => 999999
        ));
        $this->addColumn('actions_serialized', array(
            'header'    => 'actions',
            'index'     => 'actions_serialized',
            'type'      => 'text',
            'truncate'  => 999999
        ));
        $this->addColumn('stop_rules_processing', array(
            'header'    => 'stop_rules_processing',
            'index'     => 'stop_rules_processing',
        ));
        $this->addColumn('is_advanced', array(
            'header'    => 'is_advanced',
            'index'     => 'is_advanced',
        ));
        $this->addColumn('product_ids', array(
            'header'    => 'product_ids',
            'index'     => 'product_ids',
        ));
        $this->addColumn('sort_order', array(
            'header'    => 'sort_order',
            'index'     => 'sort_order',
        ));
        $this->addColumn('simple_action', array(
            'header'    => 'discount_type',
            'index'     => 'simple_action',
        ));
        $this->addColumn('discount_amount', array(
            'header'    => 'discount_amount',
            'index'     => 'discount_amount',
        ));
        $this->addColumn('discount_qty', array(
            'header'    => 'discount_qty',
            'index'     => 'discount_qty',
        ));
        $this->addColumn('discount_step', array(
            'header'    => 'discount_step',
            'index'     => 'discount_step',
        ));
        $this->addColumn('simple_free_shipping', array(
            'header'    => 'simple_free_shipping',
            'index'     => 'simple_free_shipping',
        ));
        $this->addColumn('apply_to_shipping', array(
            'header'    => 'apply_to_shipping',
            'index'     => 'apply_to_shipping',
        ));
        $this->addColumn('times_used', array(
            'header'    => 'times_used',
            'index'     => 'times_used',
        ));
        $this->addColumn('is_rss', array(
            'header'    => 'is_rss',
            'index'     => 'is_rss',
        ));
        $this->addColumn('coupon_type', array(
            'header'    => 'coupon_type',
            'index'     => 'coupon_type',
        ));
        $this->addColumn('use_auto_generation', array(
            'header'    => 'use_auto_generation',
            'index'     => 'use_auto_generation',
        ));
        $this->addColumn('user_per_coupon', array(
            'header'    => 'user_per_coupon',
            'index'     => 'user_per_coupon',
        ));
        $this->addColumn('is_promo', array(
            'header'    => 'is_promo',
            'index'     => 'is_promo',
            'type'      => 'text',
        ));
        $this->addColumn('promo_sku', array(
            'header'    => 'promo_sku',
            'index'     => 'promo_sku',
            'type'      => 'text',
        ));
        $this->addColumn('locked', array(
            'header'    => 'locked',
            'index'     => 'locked',
        ));
        $this->addColumn('customer_group_ids', array(
            'header'    => 'customer_group',
            'index'     => 'customer_group_ids',
            'type'      => 'options',
            'options'   => $this->_customerGroupOptions(),
        ));
        $this->addColumn('coupon_code', array(
            'header'    => 'coupon_code',
            'index'     => 'code',
        ));
        if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn('rule_website', array(
                'header'    => 'website',
                'index'     => 'website_ids',
                'type'      => 'options',
                'options'   => Mage::getSingleton('adminhtml/system_store')->getWebsiteOptionHash(false, 'id'),

            ));
        }
    }

    /**
     * Get customer group options
     *
     * @return array|mixed
     */
    protected function _customerGroupOptions()
    {
        if ($this->_customerGroups == null) {
            //get all customer groups
            $customerGroups = Mage::getModel('customer/group')->getCollection();
            $groups = array();
            foreach ($customerGroups as $group){
                $groups[$group->getId()] = $group->getId();
            }
            $this->_customerGroups = $groups;
        }

        return $this->_customerGroups;
    }
}
