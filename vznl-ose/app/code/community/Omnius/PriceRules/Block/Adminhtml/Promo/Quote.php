<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Catalog price rules reqrite
 *
 */
class Omnius_PriceRules_Block_Adminhtml_Promo_Quote extends Mage_Adminhtml_Block_Promo_Quote
{
    /**
     * Omnius_PriceRules_Block_Adminhtml_Promo_Quote constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_addButton('import', array(
            'label' => Mage::helper('pricerules')->__('Import Rules'),
            'onclick' => 'setLocation(\'' . $this->getUrl('*/*/import') . '\')',
            'class' => 'import go',
        ));
    }
}
