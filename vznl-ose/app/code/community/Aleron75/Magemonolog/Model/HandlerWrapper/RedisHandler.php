<?php
use Monolog\Logger;
use Monolog\Handler\RedisHandler;

class Aleron75_Magemonolog_Model_HandlerWrapper_RedisHandler
  extends Aleron75_Magemonolog_Model_HandlerWrapper_AbstractHandler
{
    /**
     * @var Aleron75_Magemonolog_Helper_Data
     */
    protected $dataHelper;

    public function __construct(array $args)
    {
        $this->dataHelper = Mage::helper('aleron75_magemonolog');
        try {
            $this->_validateArgs($args);
            $this->_handler = new RedisHandler(
              new \Predis\Client(array(
                'host'      => $args['host'],
                'port'      => $args['port'],
                'database'  => $args['database'],
                'timeout'   => $args['timeout']
              )),
              $args['stream'],
              $args['level'],
              $args['bubble'],
              $args['capSize']
            );
        }
        catch(\Exception $e) {
            $this->dataHelper->logExceptionToDisk($e);
        }
    }

    protected function _validateArgs(array &$args)
    {
        parent::_validateArgs($args);

        // Stream
        $file = Mage::getStoreConfig('dev/log/file');
        if (isset($args['stream']))
        {
            $file = $args['stream'];
        }

        $args['stream'] = $this->_createKey($file, !empty($args['keyPrefix']) ? $args['keyPrefix'] : '');

        // Redis host
        if (empty($args['host']))
        {
            $args['host'] = '127.0.0.1';
        }

        // Redis port
        $port = 6379;
        if (isset($args['port']) && is_numeric($args['port']))
        {
            $port = filter_var($args['port'], FILTER_VALIDATE_INT);
        }
        $args['port'] = $port;

        // Redis database
        $database = 0;
        if (isset($args['database']) && is_numeric($args['database']))
        {
            $database = filter_var($args['database'], FILTER_VALIDATE_INT);
        }
        $args['database'] = $database;

        if (!isset($args['level']))
        {
            $args['level'] = 'DEBUG';
        }
        if (!isset($args['bubble']))
        {
            $args['bubble'] = true;
        }
        if (!isset($args['capSize']))
        {
            $args['capSize'] = false;
        }
        if (!isset($args['timeout']))
        {
            $args['timeout'] = 5;
        }

    }

    private function _createKey($path, $prefix = '')
    {
        $keyParts = array($prefix);
        $logDir  = Mage::getBaseDir('var') . DS . 'log';
        $logFilePath = preg_replace('/^(' . preg_quote($logDir, '/') . '(\/)?)+/', '', $path);
        $parts = explode(DS, $logFilePath);
        array_push($keyParts, count($parts) < 2 ? strtolower($logFilePath) : implode('_', array_map('strtolower', array_slice($parts, 0, -1))));
        $filename = end($parts);

        // if filename is of format foo_bar_01012015.log, keep the foo_bar part in the key as well
        if (preg_match('/^[a-zA-Z_]+?_?(?:\d+)?\.log$/', $filename) && count($parts) > 1)
        {
            array_push($keyParts, preg_replace('/^([a-zA-Z_]+?)_?(?:\d+)?\.log$/', '\\1', $filename));
        }

        $this->dataHelper = Mage::helper('aleron75_magemonolog');
        return sprintf('%s.%s', implode('_', array_filter($keyParts)), $this->dataHelper->getHostName());
    }
}
