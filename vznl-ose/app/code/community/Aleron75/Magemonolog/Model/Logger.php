<?php

/** AUTOLOADER PATCH **/
if (file_exists($autoloaderPath = __DIR__ . DS . '../lib/autoload.php')
) {
    require_once $autoloaderPath;
}
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class Aleron75_Magemonolog_Model_Logger extends Logger
{
    /**
     * Adds a log record.
     *
     * @param  integer $level The logging level
     * @param  string $message The log message
     * @param  array $context The log context
     * @return Boolean Whether the record has been processed
     */
    public function addRecord($level, $message, array $context = array())
    {
        if (!$this->handlers) {
            $this->pushHandler(new StreamHandler('php://stderr', static::DEBUG));
        }

        $levelName = static::getLevelName($level);

        // check if any handler will handle this message so we can return early and save cycles
        $handlerKey = null;
        foreach ($this->handlers as $key => $handler) {
            if ($handler->isHandling(array('level' => $level))) {
                $handlerKey = $key;
                break;
            }
        }

        if (null === $handlerKey) {
            return false;
        }

        if (!static::$timezone) {
            static::$timezone = new DateTimeZone(date_default_timezone_get() ?: 'UTC');
        }

        $record = array(
            'message' => (string) $message,
            'context' => $context,
            'level' => $level,
            'level_name' => $levelName,
            'channel' => $this->name,
            'datetime' => \DateTime::createFromFormat('U.u', sprintf('%.6F', microtime(true)), static::$timezone)->setTimezone(static::$timezone),
            'extra' => array(),
        );

        foreach ($this->processors as $processor) {
            $record = call_user_func($processor, $record);
        }

        while (isset($this->handlers[$handlerKey])) {
            try {
                $this->handlers[$handlerKey]->handle($record);
            } catch (\Exception $e) {
                Mage::helper('aleron75_magemonolog')->logExceptionToDisk($e);
            }
            $handlerKey++;
        }

        return true;
    }
}
