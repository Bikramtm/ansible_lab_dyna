<?php
/**
 * Created by PhpStorm.
 * User: alessandro
 * Date: 10/6/14
 * Time: 6:25 AM
 */
class Aleron75_Magemonolog_Helper_Data extends Mage_Core_Helper_Abstract {

    /** Get machine hostname
     * @return string
     */
    public function getHostName()
    {
        return gethostname();
    }

    /**
     * Log exception messages to disk directly when logging handlers fail
     * @param $exception
     */
    public function logExceptionToDisk(\Exception $exception)
    {
        $file = Mage::getStoreConfig('dev/log/exception_file');
        $logDir  = Mage::getBaseDir('var') . DS . 'log';
        $logFile = $logDir . DS . $file;
        $monologFailureLogFile = $logDir . DS . 'monologException.log';

        if (!is_dir($logDir)) {
            mkdir($logDir);
            chmod($logDir, 0777);
        }

        if (!file_exists($logFile)) {
            file_put_contents($logFile, '');
            chmod($logFile, 0777);
        }

        if(file_exists($monologFailureLogFile)){
            return;
        }
        else{
            file_put_contents($monologFailureLogFile, '');
            chmod($monologFailureLogFile, 0777);
        }

        $format = '%timestamp% %priorityName% (%priority%): %message%' . PHP_EOL;
        $formatter = new Zend_Log_Formatter_Simple($format);
        $writer = new Zend_Log_Writer_Stream($logFile);
        $writer->setFormatter($formatter);
        $logger = new Zend_Log($writer);
        $logger->log($exception->__toString() . PHP_EOL . $exception->getTraceAsString(), Zend_Log::ERR);
    }   
}
