<?php
/**
 * ISAAC Profiler
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    ISAAC
 * @package     ISAAC_Profiler
 * @copyright   Copyright (c) 2011 ISAAC Software Solutions B.V. (http://www.isaac.nl)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author      Noud Speijcken
 * @version     0.0.1
 */

class ISAAC_Profiler_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function profiler( $nl = "\n", $sep = ', ', $table = false){
        if( !Mage::getStoreConfig('dev/debug/profiler') || !Mage::helper('core')->isDevAllowed()) {
            return 'Profiler is not enabled';
        }

        $timers = Varien_Profiler::getTimers();

        $out  = 'Memory usage: real: '.memory_get_usage(true).', emalloc: '.memory_get_usage().$nl;
        $out .= ($table?'<table border="1" cellspacing="0" cellpadding="2" style="width:auto"><tr><td>':'')
            .implode($sep, array('Time', 'Cnt', 'Emalloc', 'RealMem', 'Code Profiler')).$nl;
        foreach ($timers as $name=>$timer) {
            $sum = Varien_Profiler::fetch($name,'sum');
            $count = Varien_Profiler::fetch($name,'count');
            $realmem = Varien_Profiler::fetch($name,'realmem');
            $emalloc = Varien_Profiler::fetch($name,'emalloc');
            if ($sum<.0010 && $count<10 && $emalloc<10000) {
                continue;
            }
            $out .= implode($sep, array(number_format($sum,4), $count, number_format($emalloc), number_format($realmem), $name )).$nl;
        }
        $out .= print_r(Varien_Profiler::getSqlProfiler(Mage::getSingleton('core/resource')->getConnection('core_write')), 1)
            .($table?'</td></tr></table>':'');

        return $out;
    }

    public function shellProfiler(){
        return $this->profiler("\n",", ",false);
    }

    public function webProfiler(){
        return $this->profiler('</td></tr><tr><td>', '</td><td>', true);
    }
}