<?php
class Dyna_CatalogLog_Adminhtml_CataloglogbackendController extends Mage_Adminhtml_Controller_Action
{

	protected function _isAllowed()
	{
		//return Mage::getSingleton('admin/session')->isAllowed('cataloglog/cataloglogbackend');
		return true;
	}

	public function indexAction()
    {
       $this->loadLayout();
	   $this->_title($this->__("CatalogLog"));
	   $this->renderLayout();
    }
}