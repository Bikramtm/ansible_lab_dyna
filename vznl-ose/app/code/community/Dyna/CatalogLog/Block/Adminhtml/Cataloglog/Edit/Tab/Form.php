<?php
class Dyna_CatalogLog_Block_Adminhtml_Cataloglog_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset("cataloglog_form", array("legend" => Mage::helper("cataloglog")->__("Item information")));

        $dateFormatIso = Mage::app()->getLocale()->getDateTimeFormat(
            Mage_Core_Model_Locale::FORMAT_TYPE_SHORT
        );

        $fieldset->addField('date', 'date', array(
            'label' => Mage::helper('cataloglog')->__('Import Date'),
            'name' => 'date',
            "class" => "required-entry",
            "required" => true,
            'time' => true,
            'image' => $this->getSkinUrl('images/grid-cal.gif'),
            'format' => $dateFormatIso
        ));

        $fieldset->addField("log", "text", array(
            "label" => Mage::helper("cataloglog")->__("Import Comment"),
            "name" => "log",
        ));

        $fieldset->addField("author", "text", array(
            "label" => Mage::helper("cataloglog")->__("Import Author"),
            "name" => "author",
        ));

        $fieldset->addField("file_name", "text", array(
            "label" => Mage::helper("cataloglog")->__("Filename"),
            "class" => "required-entry",
            "required" => true,
            "name" => "file_name",
        ));

        $fieldset->addField("stack", "text", array(
            "label" => Mage::helper("cataloglog")->__("Stack"),
            "class" => "required-entry",
            "required" => true,
            "name" => "stack",
        ));

        $fieldset->addField("interface_version", "text", array(
            "label" => Mage::helper("cataloglog")->__("Interface version"),
            "class" => "required-entry",
            "required" => true,
            "name" => "interface_version",
        ));

        $fieldset->addField("reference_data_build", "text", array(
            "label" => Mage::helper("cataloglog")->__("Reference Data Build"),
            "class" => "required-entry",
            "required" => true,
            "name" => "reference_data_build",
        ));

        if (Mage::getSingleton("adminhtml/session")->getCataloglogData()) {
            $form->setValues(Mage::getSingleton("adminhtml/session")->getCataloglogData());
            Mage::getSingleton("adminhtml/session")->setCataloglogData(null);
        } elseif (Mage::registry("cataloglog_data")) {
            $form->setValues(Mage::registry("cataloglog_data")->getData());
        }
        return parent::_prepareForm();
    }
}