<?php


class Dyna_CatalogLog_Block_Adminhtml_Cataloglog extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

	$this->_controller = "adminhtml_cataloglog";
	$this->_blockGroup = "cataloglog";
	$this->_headerText = Mage::helper("cataloglog")->__("Catalog Log Manager");
	$this->_addButtonLabel = Mage::helper("cataloglog")->__("Add New Item");
	parent::__construct();
	
	}

}