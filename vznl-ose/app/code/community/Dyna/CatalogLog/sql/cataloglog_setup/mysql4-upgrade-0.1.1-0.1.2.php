<?php
/** @var Mage_Sales_Model_Entity_Setup $installer */
$installer = $this;
$installer->startSetup();


$this->run("DROP TABLE IF EXISTS `catalog_import_log`;");

if (!$installer->getConnection()->isTableExists($installer->getTable('catalog_import_log'))) {
    $catalogTable = new Varien_Db_Ddl_Table();
    $catalogTable->setName($installer->getTable('catalog_import_log'));

    $catalogTable
        ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_BIGINT, 10, array(
            'unsigned' => true,
            'primary' => true,
            'auto_increment' => true,
            'nullable' => false,
        ))

        ->addColumn('date', Varien_Db_Ddl_Table::TYPE_DATETIME, 5)

        ->addColumn('log', Varien_Db_Ddl_Table::TYPE_TEXT)

        ->addColumn('author', Varien_Db_Ddl_Table::TYPE_TEXT)

        ->addColumn('file_name', Varien_Db_Ddl_Table::TYPE_TEXT)

        ->addColumn('stack', Varien_Db_Ddl_Table::TYPE_TEXT)

        ->addColumn('interface_version', Varien_Db_Ddl_Table::TYPE_TEXT)

        ->addColumn('reference_data_build', Varien_Db_Ddl_Table::TYPE_TEXT);

    $installer->getConnection()->createTable($catalogTable);
}

$installer->endSetup();