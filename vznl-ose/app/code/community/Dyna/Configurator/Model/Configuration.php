<?php

class Dyna_Configurator_Model_Configuration extends Mage_Core_Model_Abstract
{
    const CACHE_KEY = "services_configuration_data";

    public static $codesMapping = [
        "telesales" => "OFT",
        "D2D" => "OFD",
        "retail" => "OFR",
        "online" => "OFO",
        "callcenter" => "OFC",
    ];

    protected $cache = null;

    /**
     * Returns the Comments node from the service response
     * @return mixed
     */
    public function getComments()
    {
        return $this->getDataValue('Comments');
    }

    /**
     * Returns the value of a specific node in the response using xpath like parameter (parent/child/subchild)
     *
     * @param string $path Path to a specific node
     * @param mixed $default Value to use if the node not found
     * @return mixed
     */
    public function getDataValue($path, $default = '')
    {
        // fail if the path is empty
        if (empty($path)) {
            return $default;
        }

        // remove all leading and trailing slashes
        $path = trim($path, '/');

        // use current array as the initial value
        $value = $this->getConfigurationData()->getData();

        // extract parts of the path
        $parts = array_map('strtolower', explode('/', $path));

        // loop through each part and extract its value
        foreach ($parts as $part) {
            if (isset($value[$part])) {
                // replace current value with the child
                $value = $value[$part];
            } else {
                // key doesn't exist, fail
                return $default;
            }
        }

        return $value;
    }

    /**
     * Return this model after adding configuration data from cache/services
     * Use this parameter for development purposes. Forces configuration data update
     * @param $forceLoad
     * @return $this|Varien_Object
     */
    public function getConfigurationData($forceLoad = false)
    {
        // If configuration data is not set on current instance, load it from cache
        if (empty($this->getData()) || $forceLoad) {
            $key = $this->getCacheKey();
            // Cache data exists
            if (!$forceLoad && !empty($configurationData = unserialize($this->getCache()->load($key)))) {
                $this->addData(self::array_change_key_case_recursive($configurationData));
            } else {
                // No cached data, load it from services - will be saved to cache
                $configurationData = $this->updateConfigurationData();
                return $this->addData(self::array_change_key_case_recursive($configurationData));
            }
        }

        return $this;
    }

    /**
     * Return a string cache key for saving / retrieving cached configuration data
     * @return string
     */
    public function getCacheKey()
    {
        return md5(static::CACHE_KEY . Mage::app()->getWebsite()->getCode());
    }

    /**
     * Get the Dyna Cache model
     * @return Dyna_Cache_Model_Cache
     */
    protected function getCache()
    {
        if (!$this->cache) {
            $this->cache = Mage::getSingleton('dyna_cache/cache');
        }

        return $this->cache;
    }

    /**
     * Converts all keys of a multidimensional array to lower case
     * @param $arr
     * @return array
     */
    public static function array_change_key_case_recursive($arr)
    {
        return array_map(function($item){
            if(is_array($item)){
                $item = self::array_change_key_case_recursive($item);
            }
            return $item;
        }, array_change_key_case($arr));
    }

    /**
     * This method updates configuration data retrieved from services
     * called once a day through magento cron job
     */
    public function updateConfigurationData()
    {
        /** @var Dyna_Configurator_Model_Client_ConfigurationDataClient $serviceClient */
        $serviceClient = Mage::getModel('dyna_configurator/client_configurationDataClient');
        $params = [
            'Portal' => $this->getConfigurationCode()
        ];
        $configurationData = $serviceClient->executeGetConfigurationData($params);

        $this->getCache()->save(
            serialize($configurationData),
            $this->getCacheKey(),
            [Dyna_Cache_Model_Cache::PRODUCT_TAG],
            $this->getCache()->getTtl()
        );

        return $configurationData;
    }

    /**
     * Get the portal code for getConfigurationData service call
     * @return string
     */
    public function getConfigurationCode()
    {
        $storeCode = Mage::app()->getWebsite()->getCode();
        if (!empty(self::$codesMapping[strtolower($storeCode)])) {
            return self::$codesMapping[strtolower($storeCode)];
        } else {
            return "";
        }
    }
}
