<?php

/**
 * Class Dyna_Configurator_Model_Expression_Availability
 */
class Dyna_Configurator_Model_Expression_Availability extends Varien_Object
{
    /**
     * @var Varien_Object containing stream information
     */
    protected $streams;

    /**
     * @var string
     */
    protected $areaCode;

    /**
     * @var string
     */
    protected $postCode;

    /**
     * Dyna_Configurator_Model_Expression_Availability constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->buildAvailabilityContexts();
    }

    /**
     * @return Varien_Object[]
     */
    public function buildAvailabilityContexts()
    {
        /**
         * The data is gathered in \Dyna_Address_Model_Client_CheckServiceAbilityClient::updateAvailability
         */
        $storage = Mage::getSingleton('dyna_address/storage');
        $this->streams = $storage->getAvailability();
        $this->areaCode = $storage->getAreaCode();
        $this->postCode = $storage->getPostCode();

        return $this->streams;
    }

    /**
     * @param string $downstream
     * @param string $priority
     * @param string|null $technology
     * @param string|null $availabilityIndicator
     * @return bool
     */
    public function hasTechnology($downstream = null, $priority = null, $technology = null, $availabilityIndicator = null)
    {
        /**
         * Try and avoid false negatives by uppercasing/casting all values
         */
        $downstream = $downstream !== null ? intval($downstream) : null;
        $priority = $priority !== null ? intval($priority) : null;
        $technology = $technology !== null ? strtoupper(trim($technology)) : null;
        $availabilityIndicator = $availabilityIndicator !== null ? strtoupper(trim($availabilityIndicator)) : null;

        /**
         * As we need to check for optional requirements, we set the flags for technology and availability indicator
         * to true if there were not passed, simplifying the final condition that we need to check
         */
        $hasDownstreamAndPriority = false;
        $hasTechnology = $technology === null;
        $hasAvailabilityIndicator = $availabilityIndicator === null;

        /**
         * Check downstream and priority as one condition
         *
         * So say the stream is 16000 with prio 1. in that case, 6000 will also halve prio 1, 2000 will have prio 1 and 1000 will have prio 1. All higher then 16000 will NOT have prio 1.
         * so the logic of the function
         *      availability.HasTechnology('100000','1',null,null) should be FALSE
         *      availability.HasTechnology('50000','1',null,null) should be FALSE
         *      availability.HasTechnology('25000','1',null,null) should be FALSE
         *      availability.HasTechnology('16000','1',null,null) should be TRUE
         *      availability.HasTechnology('6000','1',null,null) should be TRUE
         *      availability.HasTechnology('2000','1',null,null) should be TRUE
         *      availability.HasTechnology('1000','1',null,null) should be TRUE
         */
        if ($this->streams) {
            foreach ($this->streams as $stream) {

                $hasDownstreamAndPriority = false;
                $hasTechnology = $technology === null;
                $hasAvailabilityIndicator = $availabilityIndicator === null;
                // OMNVFDE-3355 Priority is not relevant if == null
                if ((intval($stream->getDownstream()) === $downstream || $downstream === null) && (intval($stream->getPriority()) === $priority || $priority === null)) {
                    $hasDownstreamAndPriority = true;
                }

                /**
                 * We check for $technology ONLY if it was passed as an argument
                 */
                if ($technology !== null && strtoupper(trim($stream->getTechnology())) === $technology) {
                    $hasTechnology = true;
                }

                /**
                 * We check for $availabilityIndicator ONLY if it was passed as an argument
                 */
                if ($availabilityIndicator !== null && strtoupper(trim($stream->getAvailabilityIndicator())) === $availabilityIndicator) {
                    $hasAvailabilityIndicator = true;
                }

                /**
                 * Try to bail quickly without iterating over all streams if not needed
                 */
                if ($hasDownstreamAndPriority && $hasTechnology && $hasAvailabilityIndicator) {
                    return true;
                }
            }
            unset($stream);
        }

        return false;
    }

    /**
     * @param string $optionName
     * @param string $lifeCycleStatusCode
     * @param string $bandwidth
     * @param string|null $technology
     * @param string $priority
     *
     * @return bool
     */
    public function hasOption($optionName, $lifeCycleStatusCode, $bandwidth, $technology, $priority)
    {
        /**
         * Try and avoid false negatives by uppercasing all values
         */
        $optionName = strtoupper(trim($optionName));
        $lifeCycleStatusCode = strtoupper(trim($lifeCycleStatusCode));

        $bandwidth = $bandwidth !== null ? intval($bandwidth) : null;
        $priority = $priority !== null ? intval($priority) : null;
        $technology = $technology !== null ? strtoupper(trim($technology)) : null;

        if ($this->streams) {
            foreach ($this->streams as $stream) {

                if (intval($stream->getDownstream()) !== $bandwidth || intval($stream->getPriority()) !== $priority) {
                    continue;
                }
                if (!is_null($technology) && strtoupper(trim($stream->getTechnology())) !== $technology) {
                    continue;
                }
                foreach ($stream->getAvailabilityOptions() as $option) {
                    if (strtoupper(trim($option->getOptionName())) === $optionName
                        && strtoupper(trim($option->getLifeCycleStatusCode())) === $lifeCycleStatusCode
                    ) {
                        return true;
                    }
                }
            }
            unset($stream);
        }

        return false;
    }

    /**
     * @param string $additionalInformationKey
     * @param string $additionalInformationValue
     * @param string $bandwidth
     * @param string|null $technology
     * @param string $priority
     *
     * @return bool
     */
    public function hasAdditionalOption($additionalInformationKey, $additionalInformationValue, $bandwidth, $technology, $priority)
    {
        /**
         * Try and avoid false negatives by uppercasing all values
         */
        $additionalInformationKey = strtoupper(trim($additionalInformationKey));
        $additionalInformationValue = strtoupper(trim($additionalInformationValue));

        $bandwidth = $bandwidth !== null ? intval($bandwidth) : null;
        $priority = $priority !== null ? intval($priority) : null;
        $technology = $technology !== null ? strtoupper(trim($technology)) : null;

        if ($this->streams) {
            foreach ($this->streams as $stream) {
                if (intval($stream->getDownstream()) !== $bandwidth || intval($stream->getPriority()) !== $priority) {
                    continue;
                }
                if (!is_null($technology) && strtoupper(trim($stream->getTechnology())) !== $technology) {
                    continue;
                }
                /** @var \Varien_Object $property */
                foreach ($stream->getAdditionalInformation() as $property) {
                    if ($property->getInformationKey() === $additionalInformationKey
                        && $property->getInformationValue() === $additionalInformationValue
                    ) {
                        return true;
                    }
                }
            }
            unset($stream);
        }

        return false;
    }

    /**
     * @param string $areaCode
     *
     * @return bool
     */
    public function hasAreaCode($areaCode) : bool
    {
        if (empty($areaCode)) {
            return false;
        }

        // if areaCodePattern will be given as "030*,031,032" we must verify if is one of these values OMNVFDE-2776
        if (strpos($areaCode, ',') !== false) {
            $areaCodeArray = explode(',', $areaCode);
            foreach ($areaCodeArray as $areaCode) {
                $areaCodePattern = str_replace('\\*', '.*', preg_quote($areaCode, '/'));
                if(preg_match(sprintf('/%s/', $areaCodePattern), $this->areaCode)) {
                    return true;
                }
            }
            return false;
        }
        $areaCodePattern = str_replace('\\*', '.', preg_quote($areaCode, '/'));
        $match = preg_match(sprintf('/%s/', $areaCodePattern), $this->areaCode);
        return ($match == 1 ? true : false);
    }

    /**
     * Validation on post code
     * @param $postCode
     * @return bool
     */
    public function postcodeMatches($postCode) : bool {

        $postCode = explode(",", $postCode);

        if(empty($postCode)) {
            return false;
        }

        if(!$this->postCode) {
            return false;
        }

        $status = $default = false;

        foreach($postCode as $code) {
            list($operator, $truncatedValue) = $this->getComparators(trim($code));

            switch ($operator) {
                case '>':
                    if($this->postCode > $truncatedValue) {
                        $status = true;
                    } else {
                        $status = false;
                    }
                    break;
                case '>=':
                    if($this->postCode >= $truncatedValue) {
                        $status = true;
                    } else {
                        $status = false;
                    }
                    break;
                case '<':
                    if($this->postCode < $truncatedValue) {
                        $status = true;
                    } else {
                        $status = false;
                    }
                    break;
                case '<=':
                    if($this->postCode <= $truncatedValue) {
                        $status = true;
                    } else {
                        $status = false;
                    }
                    break;
                case '=':
                    if($this->postCode == $truncatedValue) {
                        $status = true;
                    } else {
                        $status = false;
                    }
                    break;
                case '==':
                    if($this->postCode == $truncatedValue) {
                        $status = true;
                    } else {
                        $status = false;
                    }
                    break;
                case '===':
                    if($this->postCode === $truncatedValue) {
                        $status = true;
                    } else {
                        $status = false;
                    }
                    break;
                case '!=':
                    if($this->postCode != $truncatedValue) {
                        $status = true;
                    } else {
                        $status = false;
                    }
                    break;
                case '*':
                    $postCodePattern = str_replace('\\*', '.*', preg_quote($code, '/'));
                    if(preg_match(sprintf('/%s/', $postCodePattern), $this->postCode)) {
                        $status = true;
                        $default = true;
                    }
                    break;
                default :
                    if($this->postCode == $code) {
                        $status = true;
                        $default = true;
                    } else {
                        $status = false;
                    }
                    break;
            }

            if($status && $default) {
                break;
            }
        }

        return $status;

    }

    /**
     * Validation on area code
     * @param $areaCode
     * @return bool
     */
    public function areacodeMatches($areaCode) : bool {
        
        $areaCode = explode(",", $areaCode);

        if(empty($areaCode)) {
            return false;
        }

        if(!$this->areaCode) {
            return false;
        }

        $status = $default = false;

        foreach($areaCode as $code) {
            list($operator, $truncatedValue) = $this->getComparators(trim($code));

            switch ($operator) {
                case '>':
                    if($this->areaCode > $truncatedValue) {
                        $status = true;
                    } else {
                        $status = false;
                    }
                    break;
                case '>=':
                    if($this->areaCode >= $truncatedValue) {
                        $status = true;
                    } else {
                        $status = false;
                    }
                    break;
                case '<':
                    if($this->areaCode < $truncatedValue) {
                        $status = true;
                    } else {
                        $status = false;
                    }
                    break;
                case '<=':
                    if($this->areaCode <= $truncatedValue) {
                        $status = true;
                    } else {
                        $status = false;
                    }
                    break;
                case '=':
                    if($this->areaCode == $truncatedValue) {
                        $status = true;
                    } else {
                        $status = false;
                    }
                    break;
                case '==':
                    if($this->areaCode == $truncatedValue) {
                        $status = true;
                    } else {
                        $status = false;
                    }
                    break;
                case '===':
                    if($this->areaCode === $truncatedValue) {
                        $status = true;
                    } else {
                        $status = false;
                    }
                    break;
                case '!=':
                    if($this->areaCode != $truncatedValue) {
                        $status = true;
                    } else {
                        $status = false;
                    }
                    break;
                case '*':
                    $areaCodePattern = str_replace('\\*', '.*', preg_quote($code, '/'));
                    if(preg_match(sprintf('/%s/', $areaCodePattern), $this->areaCode)) {
                        $status = true;
                        $default = true;
                    }
                    break;
                default :
                    if($this->areaCode == $code) {
                        $status = true;
                        $default = true;
                    } else {
                        $status = false;
                    }
                    break;
            }

            if($status && $default) {
                break;
            }
        }

        return $status;

    }

    /**
     * Retruns operator added in the string and returns truncated string after removing operator
     * @param $string
     * @return array
     */
    private function getComparators($string) {

        $comparators = array('>=', '>', '<=', '<', '!=','===', '==', '=', '*');

        foreach($comparators as $operator){
            if(strpos($string, $operator) !== false) {
                return array($operator, str_replace($operator, "", $string));
            }
        }
    }

    /**
     * Get any info from retrieve customer info response
     * @param $xpath
     * @return mixed
     */
    public function getByDotPath($xpath)
    {
        if ($xpath) {
            /** @var Dyna_Checkout_Model_Sales_Quote $quote */
            $quote = Mage::getSingleton('checkout/cart')->getQuote();
            if ($activePackage = $quote->getCartPackage()) {
                return $activePackage->getServiceValue("CheckServiceAbility", $xpath);
            }
        }

        return null;
    }
}
