<?php

/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */
class Dyna_Configurator_Model_Expression_Catalog
{
    CONST ITERATION_PRODUCTS = "DYNA_CONFIGURATOR_EXPRESSION_CATALOG_ITERATION_PRODUCTS";
    CONST TARIFFS = "DYNA_CONFIGURATOR_EXPRESSION_CATALOG_TARIFFS";
    CONST OPTIONS = "DYNA_CONFIGURATOR_EXPRESSION_CATALOG_OPTIONS";
    CONST DEVICES = "DYNA_CONFIGURATOR_EXPRESSION_CATALOG_DEVICES";

    // list of products that needs to be altered
    // this list should initially contain all products returned by the getProductsCall
    public $iterationProducts = array();

    /** @var Dyna_Configurator_Helper_ExpressionEvaluator */
    protected $expressionEvaluator = null;
    protected $tariffs = array();
    protected $options = array();
    protected $devices = array();
    protected $selectedItems = array();

    /** @var bool Dyna_Catalog_Model_Product */
    protected $ibTariffProduct = false;
    // holds the install base tariff version code
    protected $ibTariffSku = false;
    // holds the install base tariff contract value
    protected $ibTariffContractValue = false;
    // holds the install base tariff hierarchy value
    protected $ibTariffHierarchyValue = false;
    // holds all categories from database
    protected $allCategories;

    /**
     * Dyna_Configurator_Model_Expression_Catalog constructor.
     * @param $packageType
     */
    public function __construct($packageType)
    {
        /** @var Dyna_Cache_Model_Cache $cache */
        $cache = Mage::getSingleton('dyna_cache/cache');
        if ($this->iterationProducts = unserialize($cache->load(self::ITERATION_PRODUCTS . $packageType))) {
            $this->tariffs = unserialize($cache->load(self::TARIFFS . $packageType));
            $this->options = unserialize($cache->load(self::OPTIONS . $packageType));
            $this->devices = unserialize($cache->load(self::DEVICES . $packageType));
        } else {
            $this->allCategories = Mage::helper('dyna_catalog')->getAllCategories();

            // prepare products
            /** @var Dyna_Configurator_Model_Catalog $configuratorTypeModel */
            $configuratorTypeModel = Mage::getModel('dyna_configurator/catalog')->setPackageType($packageType);

            $productAttributesList = [
                'entity_id',
                'type',
                'sku',
                'hierarchy_value',
                'lifecycle_status',
                'product_version',
                'product_family',
                'contract_value',
                'category_id',
                'product_type',
                Dyna_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR,

            ];
            $packageProducts = $configuratorTypeModel->getAllProducts($productAttributesList);
            /** @var Mage_Catalog_Model_Resource_Product $productResource */
            $productResource = Mage::getResourceModel("catalog/product");
            /** @var Dyna_Catalog_Model_Product $product */
            foreach ($packageProducts as $product) {
                $productId = $product->getId();

                if (!$productId) {
                    continue;
                }

                $type = $product->getType();
                $categoriesIds = $productResource->getCategoryIdsWithAnchors($product);

                $this->iterationProducts[(int)$productId] = array(
                    // set section type on product
                    'type' => $type,
                    'entity_id' => $productId,
                    'sku' => $product->getSku(),
                    // preserve hierarchy_value attribute as it is needed in rules processing
                    'hierarchy_value' => $product->getHierarchyValue(),
                    // only one lifecycle status per product
                    'lifecycle_status' => $product->getLifecycleStatus(),
                    // multiple product versions can be found on product
                    'product_version' => $product->getProductVersionCode(),
                    // multiple product families can be set on product
                    'product_family' => $product->getProductFamily() ? array_flip(explode(",", $product->getProductFamily())) : null,
                    'contract_value' => $product->getContractValue() !== null ? (int)$product->getContractValue() : null,
                    'product_type' => $product->getProductType(),
                    'category_data' => $this->getCategoryData($categoriesIds),
                );
                if ($product->isSubscription()) {
                    $this->tariffs[(int)$productId] = 1;
                } elseif ($product->isOption()) {
                    $this->options[(int)$productId] = 1;
                } elseif ($product->isDevice()) {
                    $this->devices[(int)$productId] = 1;
                }
            }

            $cache->save(serialize($this->iterationProducts), self::ITERATION_PRODUCTS . $packageType, array(Dyna_Cache_Model_Cache::PRODUCT_TAG), $cache->getTtl());
            $cache->save(serialize($this->tariffs), self::TARIFFS . $packageType, array(Dyna_Cache_Model_Cache::PRODUCT_TAG), $cache->getTtl());
            $cache->save(serialize($this->options), self::OPTIONS . $packageType, array(Dyna_Cache_Model_Cache::PRODUCT_TAG), $cache->getTtl());
            $cache->save(serialize($this->devices), self::DEVICES . $packageType, array(Dyna_Cache_Model_Cache::PRODUCT_TAG), $cache->getTtl());
        }
        // prepare selected items
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $cartPackage = $quote->getCartPackage();

        $this->selectedItems = array_merge(
            $cartPackage ? $cartPackage->getAllItems() : [],
            $quote->getAlternateItems(($cartPackage) ? $cartPackage->getPackageId() : 0)
        );

        $this->expressionEvaluator = Mage::helper('dyna_configurator/expressionEvaluator');
    }

    /**
     * Return category data based on a list of category ids
     * @param $categoryIds
     * @return array
     */
    protected function getCategoryData($categoryIds)
    {
        $categoryData = array();
        if (!$categoryIds) {
            return $categoryData;
        }

        foreach ($categoryIds as $id) {
            // gather unique ids
            if ($categoryUniqueId = $this->allCategories[$id]['unique_id'] ?? null) {
                $categoryData['unique_id'][$categoryUniqueId] = 1;
            }
            // gather category paths as lower case
            $categoryPathArray = explode("/", $this->allCategories[$id]['path']);
            // remove root and default category
            unset($categoryPathArray[0]);
            unset($categoryPathArray[1]);
            $categoryPath = implode(Mage::helper('dyna_configurator/expression')->getCategoryPathSeparator(), array_map(function ($categoryId) {
                return strtolower($this->allCategories[$categoryId]['name']);
            }, $categoryPathArray));

            $categoryData['path'][$categoryPath] = 1;
        }

        return $categoryData;
    }

    /**
     * Return all tariffs that have a certain hierarchy value related to another one
     * @return array
     */
    public function getTariffsWithHierarchy($operator)
    {
        if ($this->ibTariffHierarchyValue === false) {
            $this->ibTariffHierarchyValue = null;
            if ($ibTariffProduct = $this->getInstalledBaseTariffProduct()) {
                $this->ibTariffHierarchyValue = (int)$ibTariffProduct->getHierarchyValue();
            }
        }

        // filter those that are not
        $validProducts = array_filter($this->iterationProducts, function ($product) use ($operator) {
            // check if these products are tariffs or if they match condition
            if (!isset($this->tariffs[$product['entity_id']]) || $product['hierarchy_value'] === null || !$this->expressionEvaluator->evaluate($product['hierarchy_value'], $this->ibTariffHierarchyValue, $operator)) {
                return false;
            }
            return true;
        });

        return array_keys($validProducts);
    }

    /**
     * Return all available tariffs for current package type filtering them by install base tariff's product version
     * @param $operator
     * @return int[]
     */
    public function getTariffsWithVersion($operator)
    {
        if ($this->ibTariffSku === false) {
            $this->ibTariffSku = null;
            if ($ibTariffProduct = $this->getInstalledBaseTariffProduct()) {
                $this->ibTariffSku = $ibTariffProduct->getSku();
            }
        }

        return $this->ibTariffSku !== false ? $this->getProductsWithVersion($this->ibTariffSku, $operator, true) : array();
    }

    /**
     * Return all options that have a certain hierarchy value related to another one
     * @return array
     */
    public function getOptionsWithHierarchy($productSku, $operator)
    {
        $value = $this->getProductHierarchyValue($productSku);

        // filter those that are not
        $validProducts = array_filter($this->iterationProducts, function ($product) use ($value, $operator) {
            // check if these products are options or if they match condition
            if (!isset($this->options[$product['entity_id']]) || !$this->expressionEvaluator->evaluate($product['hierarchy_value'], $value, $operator)) {
                return false;
            }
            return true;
        });

        return array_keys($validProducts);
    }

    /*
     * Return hierarchy value for a certain product
     * @return int
     */
    protected function getProductHierarchyValue($productSku)
    {
        foreach ($this->iterationProducts as $productData) {
            if ($productData['sku'] == $productSku) {
                return $productData['hierarchy_value'];
            }
        }

        return 0;
    }

    /**
     * Return first product version a certain sku has
     * @param string $productSku
     * @return int
     */
    public function getProductVersionCode($productSku)
    {
        foreach ($this->iterationProducts as $productData) {
            if ($productData['sku'] == $productSku) {
                return $productData['product_version'];
            }
        }

        return 0;
    }

    /*
     * Return contract value for a certain product
     * @return int
     */
    public function getProductContractValue($productSku)
    {
        foreach ($this->iterationProducts as $productData) {
            if ($productData['sku'] == $productSku) {
                return $productData['contract_value'];
            }
        }

        return null;
    }

    /**
     * Get all products that have a certain hierarchy value related to another value
     * @param $value
     * @param $operator
     * @return array
     */
    public function getProductsWithHierarchyValue($value, $operator)
    {
        // filter those that are not
        $validProducts = array_filter($this->iterationProducts, function ($product) use ($value, $operator) {
            // check if these products are options or if they match condition
            if (!$this->expressionEvaluator->evaluate($product['hierarchy_value'], $value, $operator)) {
                return false;
            }
            return true;
        });

        return array_keys($validProducts);
    }

    /**
     * Return all tariffs which have a certain lifecycle status
     * @param $lifeCycleStatus
     * @return int[]
     */
    public function getTariffsWithLifecycleStatus($lifeCycleStatus)
    {
        $lifeCycleStatusValue = Dyna_Catalog_Model_Lifecycle::getLifecycleStatusIdByCode($lifeCycleStatus);
        $result = array();

        if ($lifeCycleStatusValue === false) {
            return $result;
        }

        foreach ($this->iterationProducts as $productData) {
            if ($productData['lifecycle_status'] === null || !isset($this->tariffs[$productData['entity_id']]) || $productData['lifecycle_status'] != $lifeCycleStatusValue) {
                continue;
            }
            $result[] = $productData['entity_id'];
        }

        return $result;
    }

    /**
     * Return all products for current package type that have a certain lifecycle status
     * @param $lifeCycleStatus
     * @return int[]
     */
    public function getProductsWithLifecycleStatus($lifeCycleStatus)
    {
        $lifeCycleStatusValue = Dyna_Catalog_Model_Lifecycle::getLifecycleStatusIdByCode($lifeCycleStatus);
        $result = array();

        if ($lifeCycleStatusValue === false) {
            return $result;
        }

        foreach ($this->iterationProducts as &$productData) {
            if ($productData['lifecycle_status'] !== $lifeCycleStatusValue) {
                continue;
            }
            $result[] = $productData['entity_id'];
        }

        return $result;
    }

    /**
     * Return all products that belong to the same family
     * @param $familyIdentifier
     * @return int[]
     */
    public function getProductsInSameFamily($familyIdentifier)
    {
        $familyId = Dyna_Catalog_Model_ProductFamily::getProductFamilyIdByCode($familyIdentifier);
        // remove products that are not part of this family
        $validProducts = array_filter($this->iterationProducts, function ($product) use ($familyId) {
            // check if these products are options or if they match condition
            if ($product['product_family'] && isset($product['product_family'][$familyId])) {
                return true;
            }
            return false;
        });

        return array_keys($validProducts);
    }

    /**
     * Return all tariffs that belong to the same product family as the install base tariff
     * @return int[]
     */
    public function getTariffsWithSameProductFamily()
    {
        $familyIds = array();

        if ($ibTariffProduct = $this->getInstalledBaseTariffProduct()) {
            $familyIds = ($familyIds = $ibTariffProduct->getProductFamily()) ? explode(",", $familyIds) : array();
        }

        // remove products that are not part of this family
        $validProducts = array_filter($this->iterationProducts, function ($product) use ($familyIds) {
            // check if these products are options or if they match condition
            if ($product['product_family'] && array_intersect(array_flip($product['product_family']), $familyIds)) {
                return true;
            }
            return false;
        });

        return array_keys($validProducts);
    }

    /**
     * Get options and devices which have a certain product version in relation with another version checked by operator
     * @param $productSku
     * @param $operator
     * @param bool $onlyTariffs
     * @return array
     */
    public function getProductsWithVersion($productSku, $operator, $onlyTariffs = false)
    {
        $versionCode = $this->getProductVersionCode($productSku);

        // filter those that are not
        $validProducts = array_filter($this->iterationProducts, function ($product) use ($versionCode, $operator, $onlyTariffs) {
            if (($onlyTariffs && !isset($this->tariffs[$product['entity_id']]))
                || (!$onlyTariffs && !isset($this->options[$product['entity_id']]) && !isset($this->devices[$product['entity_id']]))
            ) {
                return false;
            }

            // check if these products are options or if they match condition
            if ($this->expressionEvaluator->evaluate($product['product_version'], $versionCode, $operator)) {
                return true;
            }

            // if this line has been reached, than current iteration product doesn't have the same version
            return false;
        });

        return array_keys($validProducts);
    }

    /**
     * Return all tariffs that have a certain contract value in comparison with another value using the given operator
     * @return array
     */
    public function getTariffsWithContractValue($operator)
    {
        if ($this->ibTariffContractValue === false) {
            $this->ibTariffContractValue = null;
            if ($ibTariffProduct = $this->getInstalledBaseTariffProduct()) {
                $this->ibTariffContractValue = (int)$ibTariffProduct->getContractValue();
            }
        }

        // filter those that are not
        $value = $this->ibTariffContractValue;
        $validProducts = array_filter($this->iterationProducts, function ($product) use ($value, $operator) {
            // check if these products are tariffs or if they match condition
            if (!isset($this->tariffs[$product['entity_id']]) || $product['contract_value'] === null || !$this->expressionEvaluator->evaluate($product['contract_value'], $value, $operator)) {
                return false;
            }
            return true;
        });

        return array_keys($validProducts);
    }

    /**
     * Return all options that have a certain contract value related to another one
     * @return array
     */
    public function getOptionsWithContractValue($productSku, $operator)
    {
        $value = $this->getProductContractValue($productSku);
        // filter those that are not
        $validProducts = array_filter($this->iterationProducts, function ($product) use ($value, $operator) {
            // check if these products are options or if they match condition
            if (!isset($this->options[$product['entity_id']]) || $product['contract_value'] === null || !$this->expressionEvaluator->evaluate($product['contract_value'], $value, $operator)) {
                return false;
            }
            return true;
        });

        return array_keys($validProducts);
    }

    /**
     * Return all options and hardware that have a certain contract value related to another one
     * @return array
     */
    public function getProductsWithContractValue($productSku, $operator)
    {
        $value = $this->getProductContractValue($productSku);

        // filter those that are not
        $validProducts = array_filter($this->iterationProducts, function ($product) use ($value, $operator) {
            // check if these products are options or if they match condition
            if ((!isset($this->options[$product['entity_id']]) && !isset($this->devices[$product['entity_id']]))
                || $product['contract_value'] === null
                || !$this->expressionEvaluator->evaluate($product['contract_value'], $value, $operator)
            ) {
                return false;
            }
            return true;
        });

        return array_keys($validProducts);
    }

    /**
     * Return catalog products from a certain category
     * @param $categoryPath
     * @param $categoryUniqueId
     * @return array
     */
    public function getProductsInCategory($categoryPath, $categoryUniqueId = null)
    {
        if ($categoryUniqueId) {
            $validProducts = array_filter($this->iterationProducts, function ($product) use ($categoryUniqueId) {
                return isset($product['category_data']['unique_id'][$categoryUniqueId]);
            });
        } else {
            $categoryPath = strtolower($categoryPath);
            $validProducts = array_filter($this->iterationProducts, function ($product) use ($categoryPath) {
                return isset($product['category_data']['path'][$categoryPath]);
            });
        }

        return array_keys($validProducts);
    }

    /**
     * Return catalog products from a certain category
     * @param $categoryUniqueId
     * @return array
     */
    public function getProductsInCategoryId($categoryUniqueId)
    {
        return $this->getProductsInCategory("", $categoryUniqueId);
    }

    /**
     * Return the product with the given sku
     * @param string $productSku
     * @return array
     */
    public function getProduct($productSku)
    {
        foreach ($this->iterationProducts as $product) {
            if ($product['sku'] == $productSku) {
                return array($product['entity_id']);
            }
        }

        return array();
    }

    /**
     * Get all the contract items, even if they were removed from cart
     * @return Dyna_Catalog_Model_Product|false
     */
    protected function getInstalledBaseTariffProduct()
    {
        if ($this->ibTariffProduct === false) {
            /** @var Dyna_Checkout_Model_Sales_Quote_Item $item */
            foreach ($this->selectedItems as $item) {
                if ($item->getProduct()->isSubscription() && $item->getIsContract()) {
                    $this->ibTariffProduct = $item->getProduct();
                    break;
                }
            }
        }

        return $this->ibTariffProduct;
    }

    /**
     * Return all options that have a certain contract value related to a set of values
     * @return array
     */
    public function getOptionsWithContractValueForProducts($productIds, string $operator)
    {
        $result = array();
        $expressionHelper = Mage::helper('dyna_configurator/expression');

        // $productIds argument can also be a string of skus separated by coma
        $productIds = is_string($productIds) ? explode(',', $productIds) : $productIds;
        foreach ($productIds as $productId) {
            $sku = is_string($productIds) ? trim($productId) : $this->iterationProducts[$productId]['sku'];
            $result = array_merge($result, array_values($this->getOptionsWithContractValue($sku, $operator)));
        }

        $installedBaseProducts = array_merge(array_values($expressionHelper->getPackageIBItems(null, true)), array_values($expressionHelper->getPackageIBItems()));
        $result = array_diff($result, array_values($installedBaseProducts));

        return array_unique($result);
    }

    /**
     * Returns a list of matching product ids what have the provided product_type
     * @param $productType
     * @return array
     */
    public function getProductsWithProductType($productType)
    {
        // Get the id of the product_type option value
        $attribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', 'product_type');
        if ($attribute) {
            $value = Mage::helper('dyna_catalog')->getAttributeValueIds($attribute, $productType);
        }
        $productTypeId = array_pop($value);

        // Compare all the current products with the id of the provided option value
        $validProducts = array_filter($this->iterationProducts, function ($product) use ($productTypeId) {
            foreach (explode(',', $product['product_type']) as $currentProductType) {
                // check if these products are options or if they match condition
                if ($this->expressionEvaluator->evaluate($currentProductType, $productTypeId, '=')) {
                    return true;
                }
            }
        });

        return array_keys($validProducts);
    }
}
