<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Configurator_Model_Expression_Package
 */
class Dyna_Configurator_Model_Expression_Package
{
    /** @var Dyna_Package_Model_Package */
    protected $package = null;

    /** @var array $packageItems */
    protected $packageItems = null;

    /** @var array $injectedItems */
    protected $injectedItems = array();

    /**
     * Set package model on current instance
     * @return $this
     */
    public function setPackage(Dyna_Package_Model_Package $package)
    {
        $this->package = $package;
        $this->packageItems = $this->package->getAllItems();
        
        if ($package->getEditingDisabled()) {
            $dummyItems = $this->package->getInstalledBaseProducts();
            if (!is_array($dummyItems)) {
                $dummyItems = array($dummyItems);
            }
            $this->packageItems = array_merge($this->packageItems, $dummyItems);
        }
        $this->getInjectedItems();

        return $this;
    }

    /**
     * Returns injected items
     * @return $this
     */
    public function getInjectedItems() 
    {
        $packageInjectedProducts = Mage::helper('dyna_configurator/expression')->getInjectedProducts();
        $skusPresentInPackage = !empty($this->packageItems)
            ? array_map(
                function ($e) {
                    return $e->getProduct()->getSku();
                },
                $this->packageItems)
            : array();

        /** @var Dyna_Checkout_Model_Sales_Quote_Item $item */
        foreach ($this->injectedItems as $item) {
            if (isset($packageInjectedProducts[$item->getProduct()->getSku()])) {
                unset($packageInjectedProducts[$item->getProduct()->getSku()]);
            }
        }

        /** @var Vfde_Catalog_Model_Product $product */
        foreach ($packageInjectedProducts as $sku => $product) {
            if (!empty($this->package->getData()) && !in_array($product->getSku(), $skusPresentInPackage)) {
                $this->injectedItems[] = Mage::getModel('sales/quote_item')
                    ->setProduct($product);
            }
        }

        return $this->injectedItems;
    }

    /**
     * Determine whether or not current package contains a certain product
     * @param string $productSku
     * @return bool
     */
    public function containsProduct($productSku)
    {
        /** @var Dyna_Checkout_Model_Sales_Order_Item $packageItem */
        foreach ($this->packageItems as $packageItem) {
            if ($packageItem->getSku() === $productSku) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param string $prolongType
     * @return bool
     */
    public function hasSubsidizedHandsetValidation($prolongType)
    {
        return strtoupper($this->package->getProlongationType()) == strtoupper($prolongType);
    }

    /**
     * VFDED1W3S-936, VFDED1W3S-901
     * @param string $prolongInfoEligibility
     * @param string $prolongType
     * @return bool
     */
    public function hasProlongationEligibility($prolongInfoEligibility , $prolongType = null)
    {
        $validProlongationInfoEligibility = strtoupper($this->package->getProlongationInfoEligibility()) == strtoupper($prolongInfoEligibility);
        if(!$prolongType) {
            return $validProlongationInfoEligibility;
        }

        $validProlongType = strtoupper($this->package->getProlongationType()) == strtoupper($prolongType);
        return $validProlongationInfoEligibility && $validProlongType;
    }

    /**
     * Determine whether or not package tariff
     * @param $operator
     */
    public function hasTariffInContract()
    {
        $allItems = $this->package->getAllItems(true, true, false, true);
        /** @var Dyna_Checkout_Model_Sales_Quote_Item $item */
        foreach ($allItems as $item) {
            if (!$item->getProduct()->isSubscription()) {
                continue;
            }
            if ($item->getIsContract()) {
                return true;
            }
        }

        return false;
    }

    /*
     * Iterate through quote items and get first found tariff's hierarchy value
     * This implementation is required for FN where only one tariff can be present in cart
     * @return int
     */
    public function getTariffHierarchyValue()
    {
        // if is in contract, get tariff with is_contract = 1
        $isInContract = false;
        if ($this->hasTariffInContract()) {
            $isInContract = true;
            $allItems = array_merge($this->package->getAllItems(), $this->package->getQuote()->getAlternateItems($this->package->getPackageId()));
        } else {
            $allItems = $this->package->getAllItems();
        }

        /** @var Dyna_Checkout_Model_Sales_Quote_Item $item */
        foreach ($allItems as $item) {
            // skip items that are not subscriptions
            if (!$item->getProduct()->isSubscription()) {
                continue;
            }
            // if in contract, skip items that not in contract
            if ($isInContract && !$item->getIsContract()) {
                continue;
            }

            return (int)$item->getProduct()->getHierarchyValue();
        }

        return 0;
    }

    /**
     * TODO: After is removed from the imported conditions and will not be used anymore, remove this fallback.
     * @param $operator
     */
    public function hasTariffWithHierarchyValue($operator)
    {
        $this->hasTariffWithHierarchy($operator);
    }

    /**
     * Determine whether or not this package has a new tariff from the one previously owned install base
     * It reads $cartTariffHierarchyValue $operator $installBaseHierarchy value
     * return bool
     */
    public function hasTariffWithHierarchy($operator)
    {
        // skip if this package doesn't have a subscription in contract
        if (!$this->hasTariffInContract()) {
            return false;
        }

        $currentHierarchyValue = $ibHierarchyValue = null;
        $allItems = array_merge($this->package->getAllItems(), $this->package->getQuote()->getAlternateItems($this->package->getPackageId()));
        foreach ($allItems as $item) {
            if (!$item->getProduct()->isSubscription()) {
                continue;
            }
            if ($item->getIsContract()) {
                $ibHierarchyValue = (int)$item->getProduct()->getHierarchyValue();
            } else {
                $currentHierarchyValue = (int)$item->getProduct()->getHierarchyValue();
            }
        }
        // false if no subscription updated in cart
        if ($currentHierarchyValue === null) {
            return false;
        }

        /** @var Dyna_Configurator_Helper_ExpressionEvaluator $evaluatorHelper */
        $evaluatorHelper = Mage::helper('dyna_configurator/expressionEvaluator');
        return $evaluatorHelper->evaluate($currentHierarchyValue, $ibHierarchyValue, $operator);
    }

    /*
     * Evaluation of cart package tariff for in minimum duration flag
     * @return bool
     */
    public function tariffInMinimumDuration()
    {
        /** @var Dyna_Checkout_Model_Sales_Quote_Item $item */
        foreach ($this->package->getAllItems(true, true, true) as $item) {
            if ($item->getInMinimumDuration() && $item->getProduct()->isSubscription()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Determine whether or not a certain cart product is in minimum duration
     * @param $productSku
     * @return bool
     */
    public function productInMinimumDuration($productSku)
    {
        foreach ($this->package->getAllItems() as $item) {
            if ($item->getSku() === $productSku && $item->getInMinimumDuration()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Return product ids of option type items that are in minimum duration
     * @return int[]
     */
    public function getOptionsInMinimumDuration($includeDeleted = false)
    {
        return Mage::helper('dyna_configurator/expression')->getOptionsInMinimumDuration($includeDeleted);
    }


    /**
     * Determine whether or not current cart contains a product in a certain family
     * @param $familyIdentifier
     * @return bool
     */
    public function containsProductInProductFamily($familyIdentifier)
    {
        $familyId = Dyna_Catalog_Model_ProductFamily::getProductFamilyIdByCode($familyIdentifier);
        foreach ($this->package->getAllItems() as $item) {
            if ($productFamilies = $item->getProduct()->getProductFamily()) {
                $productFamilies = explode(",", $productFamilies);
                if (in_array($familyId, $productFamilies)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Check if tariff in current quote is in the same family as the install base tariff
     * @return bool
     */
    public function tariffHasSameProductFamily()
    {
        $allItems = array_merge($this->package->getAllItems(), $this->package->getQuote()->getAlternateItems($this->package->getPackageId()));
        $oldTariff = null;
        $newTariff = null;
        /** @var Dyna_Checkout_Model_Sales_Quote_Item $item */
        foreach ($allItems as $item) {
            if (!$item->getProduct()->isSubscription()) {
                continue;
            }
            if ($item->getIsContract()) {
                $oldTariff = $item;
            } else {
                $newTariff = $item;
            }
        }

        // If not removed from cart, then this is true if the tariff is in at least one product family
        if (!$oldTariff->getIsContractDrop()) {
            return (bool)$oldTariff->getProduct()->getProductFamily();
        }

        if ($oldTariff && $newTariff) {
            $oldTariffFamilies = ($oldTariffFamilies = $oldTariff->getProduct()->getProductFamily()) ? explode(",", $oldTariffFamilies) : array();
            $newTariffFamilies = ($newTariffFamilies = $newTariff->getProduct()->getProductFamily()) ? explode(",", $newTariffFamilies) : array();

            return (bool)count(array_intersect($oldTariffFamilies, $newTariffFamilies));
        }

        return false;
    }

    /*
    * Iterate through quote items and get first found tariff's product version
     * Products can have multiple versions, so the first found is served
    * @return int
    */
    public function getTariffVersion()
    {
        // if is in contract, get tariff with is_contract = 1
        $isInContract = false;
        if ($this->hasTariffInContract()) {
            $isInContract = true;
            $allItems = array_merge($this->package->getAllItems(), $this->package->getQuote()->getAlternateItems($this->package->getPackageId()));
        } else {
            $allItems = $this->package->getAllItems();
        }

        /** @var Dyna_Checkout_Model_Sales_Quote_Item $item */
        foreach ($allItems as $item) {
            // skip items that are not subscriptions
            if (!$item->getProduct()->isSubscription()) {
                continue;
            }
            // if in contract, skip items that not in contract
            if ($isInContract && !$item->getIsContract()) {
                continue;
            }

            return $item->getProduct()->getProductVersionCode();
        }

        return null;
    }

    /**
     * Determine whether or not this package has a new tariff from the one previously owned install base
     * It reads $cartTariffVersion $operator $installBaseVersion value
     * return bool
     */
    public function hasTariffWithVersion($operator)
    {
        // skip if this package doesn't have a subscription in contract
        if (!$this->hasTariffInContract()) {
            return false;
        }

        $currentVersion = $ibVersion = null;
        $allItems = array_merge($this->package->getAllItems(true, true, false, true), $this->getInjectedItems());
        /** @var Dyna_Checkout_Model_Sales_Quote_Item $item */
        foreach ($allItems as $item) {
            if (!$item->getProduct()->isSubscription()) {
                continue;
            }
            if ($item->getIsContract()) {
                $ibVersion = $item->getProduct()->getProductVersionCode();
            } else {
                $currentVersion = $item->getProduct()->getProductVersionCode();
            }
        }

        // Evaluate expression only if we identify 2 tariffs to compare
        if ($currentVersion !== null && $ibVersion !== null) {
            /** @var Dyna_Configurator_Helper_ExpressionEvaluator $evaluatorHelper */
            $evaluatorHelper = Mage::helper('dyna_configurator/expressionEvaluator');
            if ($evaluatorHelper->evaluate($currentVersion, $ibVersion, $operator)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Determine whether or not current package has a product with a certain version in relation to another product version using a certain operator
     * @param $productSku
     * @param $operator
     * @return bool
     * @throws Mage_Core_Exception
     */
    public function hasProductWithVersion($productSku, $operator)
    {
        /** @var Dyna_Configurator_Helper_ExpressionEvaluator $evaluatorHelper */
        $evaluatorHelper = Mage::helper('dyna_configurator/expressionEvaluator');
        /** @var Dyna_Catalog_Model_Product $productInstance */
        $productInstance = Mage::getModel('catalog/product');
        $comparisionProductId = $productInstance->getIdBySku($productSku);
        if (($comparisionProduct = $productInstance->load($comparisionProductId)) && $comparisionVersion = $comparisionProduct->getProductVersionCode()) {
            $allItems = $this->package->getAllItems();
            foreach ($allItems as $item) {
                $currentProductVersion = $item->getProduct()->getProductVersionCode();
                if ($currentProductVersion && $evaluatorHelper->evaluate($currentProductVersion, $comparisionVersion, $operator)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Check if current package has a tariff with a certain contract value in relation to the install base tariff using the given operator
     * It reads $cartTariffContractValue $operator $installBaseContractValue value
     * return bool
     */
    public function hasTariffWithContractValue($operator)
    {
        // skip if this package doesn't have a subscription in contract
        if (!$this->hasTariffInContract()) {
            return false;
        }

        $currentContractValue = $ibContractValue = null;
        $allItems = array_merge($this->package->getAllItems(true, true, false, true), $this->getInjectedItems());
        foreach ($allItems as $item) {
            if (!$item->getProduct()->isSubscription()) {
                continue;
            }
            if ($item->getIsContract()) {
                $ibContractValue = (int)$item->getProduct()->getContractValue();
            } else {
                $currentContractValue = (int)$item->getProduct()->getContractValue();
            }
        }
        // false if no subscription updated in cart
        if ($currentContractValue === null) {
            return false;
        }

        /** @var Dyna_Configurator_Helper_ExpressionEvaluator $evaluatorHelper */
        $evaluatorHelper = Mage::helper('dyna_configurator/expressionEvaluator');
        return $evaluatorHelper->evaluate($currentContractValue, $ibContractValue, $operator);
    }

    /*
     * Iterate through quote items and get first found tariff's contract value
     * @return int
     */
    public function getTariffContractValue()
    {
        // if is in contract, get tariff with is_contract = 1
        $isInContract = false;
        if ($this->hasTariffInContract()) {
            $isInContract = true;
            $allItems = array_merge($this->package->getAllItems(), $this->package->getQuote()->getAlternateItems($this->package->getPackageId()));
        } else {
            $allItems = $this->package->getAllItems();
        }

        /** @var Dyna_Checkout_Model_Sales_Quote_Item $item */
        foreach ($allItems as $item) {
            // skip items that are not subscriptions
            if (!$item->getProduct()->isSubscription()) {
                continue;
            }
            // if in contract, skip items that not in contract
            if ($isInContract && !$item->getIsContract()) {
                continue;
            }

            return (int)$item->getProduct()->getContractValue();
        }

        return 0;
    }

    /*
     * Check if a package is in a Convert to Member flow
     * @return bool
     */
    public function isConvertedToMember() {
        $isConvertedToMember = false;
        if ($this->package->getConvertedToMember()) {
            $isConvertedToMember = true;
        }
        return $isConvertedToMember;
    }

    /*
     * Apply package filters for products in cart
     * @return int
     */
    public function filterOutOwnerSubscription()
    {
        $response = false;
        /** @var Dyna_Package_Model_Package $package */
        foreach ($this->package->getQuote()->getCartPackages() as $package) {
            if (!$package->getConvertedToMember()) {
                continue;
            }

            foreach ($package->getAllItems() as $item) {
                $item->getProduct()->isSubscription() && $item->getProduct()->isRedPlusOwner() && $item->isDeleted(true) && $response = true;
            }
        }

        return $response;
    }

    /*
     * Apply package filters for products in cart
     * @return int
     */
    public function removeProductsByPackageFilters()
    {
        $response = false;
        $cart = Mage::getSingleton('checkout/cart');

        /** @var Dyna_Package_Model_Package $package */
        foreach ($cart->getQuote()->getCartPackages() as $package) {
            // package type filters
            $conditionType = Mage::helper('dyna_configurator')->getFilterCondition($package->getPermanentFilters());
            $typeFilterAttribute = $conditionType['attributeName'];
            foreach ($package->getAllItems() as $item) {
                $typeFilterAttributeValue = $item->getProduct()->getData($typeFilterAttribute);
                if (!is_array($typeFilterAttributeValue)) {
                    $typeFilterAttributeValue = array($typeFilterAttributeValue);
                }
                $passedTypeFilter = ((in_array($conditionType['condition'], $typeFilterAttributeValue) && $conditionType['operator'] == 'finset') || (!in_array($conditionType['condition'], $typeFilterAttributeValue) && $conditionType['operator'] == 'nfinset'));

                // package subtype filters
                $conditionSubtype = $package->getSubtypeFilter($item->getProduct()->getPackageSubtypeName());
                $passedSubtypeFilter = true;
                if ($conditionSubtype && $conditionSubtype = Mage::helper('dyna_configurator')->getFilterCondition($conditionSubtype)) {
                    $subtypeFilterAttribute = $conditionSubtype['attributeName'];
                    $subtypeFilterAttributeValue = $item->getProduct()->getData($subtypeFilterAttribute);
                    // make sure product attribute values are array
                    !is_array($subtypeFilterAttributeValue) && $subtypeFilterAttributeValue = array($subtypeFilterAttributeValue);
                    $passedSubtypeFilter = ((in_array($conditionSubtype['condition'], $subtypeFilterAttributeValue) && $conditionSubtype['operator'] == 'finset') || (!in_array($conditionSubtype['condition'], $subtypeFilterAttributeValue) && $conditionSubtype['operator'] == 'nfinset'));
                }

                // if one of these filters failed and product is subscription, remove it from cart
                if ((!$passedTypeFilter || !$passedSubtypeFilter) && $item->getProduct()->isSubscription()) {
                    $item->isDeleted(true);
                    $response = true;
                }
            }
        }

        return $response;
    }

    /**
     * Returns removed installed base product ids of products belonging to a specific category
     * @param string $categoryPath
     * @param $returnDeleted
     * @return int[]
     */
    public function getInstalledBaseItemsInCategory(string $categoryPath, $returnDeleted = false)
    {
        return Mage::helper('dyna_configurator/expression')->getPackageIBItems($categoryPath, $returnDeleted);
    }

    /**
     * Returns removed installed base product ids of option type products which are in minimum duration and belong to a specific category
     * @param string $categoryPath
     * @param bool $returnDeleted
     * @return int[]
     */
    public function getOptionsInMinimumDurationByCategory(string $categoryPath, $returnDeleted = false)
    {
        return array_intersect($this->getInstalledBaseItemsInCategory($categoryPath, $returnDeleted), $this->getOptionsInMinimumDuration($returnDeleted));
    }

    /**
     * Returns all installed base product ids of option type products which are in minimum duration and belong to a specific category
     * @param string $categoryPath
     * @param bool $returnDeleted
     * @return int[]
     */
    public function getAllOptionsInMinimumDurationByCategory(string $categoryPath)
    {
        $result = array_merge(
            array_intersect($this->getInstalledBaseItemsInCategory($categoryPath, true), $this->getOptionsInMinimumDuration(true)),
            array_intersect($this->getInstalledBaseItemsInCategory($categoryPath), $this->getOptionsInMinimumDuration(true))
        );
        return array_combine($result, $result);
    }

    /**
     * Returns TRUE if the package contains products of the specified category which belong to the specified category
     * @param string $categoryPath
     * @return bool
     */
    public function containsOptionsInMinimumDurationOfCategory(string $categoryPath)
    {
        return !empty($this->getOptionsInMinimumDurationByCategory($categoryPath));
    }

    /**
     * Returns TRUE if installed base options in minimum duration were removed, FALSE otherwise
     * @param string $categoryPath
     * @return bool
     */
    public function containedOptionsInMinimumDurationOfCategory(string $categoryPath)
    {
        return !empty($this->getOptionsInMinimumDurationByCategory($categoryPath, true));
    }

    /**
     * @return bool
     */
    public function isTariffChanged()
    {
        return $this->package->ilsTariffChanged();
    }

    /**
     * Returns true if the current active package has one of the process contexts received as input param
     * @param $process_contexts
     * @return bool
     */
    public function hasProcessContext($process_contexts)
    {
        $contexts_array = array_map("trim", explode(',', trim($process_contexts)));
        if (in_array($this->package->getSaleType(), $contexts_array)) {
            return true;
        } else {
            return false;
        }
    }

}
