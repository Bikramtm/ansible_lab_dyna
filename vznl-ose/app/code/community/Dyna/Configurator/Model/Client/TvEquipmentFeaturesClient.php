<?php

/**
 * Class Dyna_Configurator_Model_Client_TvEquipmentFeaturesClient
 */
class Dyna_Configurator_Model_Client_TvEquipmentFeaturesClient extends Dyna_Service_Model_Client
{
    const WSDL_CONFIG_KEY = "tv_equipment_features/wsdl_tv_equipment_features";
    const ENDPOINT_CONFIG_KEY = "tv_equipment_features/endpoint_tv_equipment_features";
    const CONFIG_STUB_PATH = 'tv_equipment_features/use_stubs';

    /**
     * GetSmartCardTypes
     * @param $params
     * @return mixed
     */
    public function executeGetTvEquipmentFeatures($params = array())
    {
        $cacheId = $this->buildCacheKey(__METHOD__, $params);
        $root = [];

        if ($cachedValues = $this->getCache()->load($cacheId)) {
            $values = unserialize($cachedValues);
        } else {
            $this->setOgwHeaderInfo($root, 'GetTvEquipmentFeatures');
            $root['ROOT']['DATA'] = $params;
            $values = $this->GetTvEquipmentFeatures($root);

            $this->getCache()->save(serialize($values), $cacheId);
        }

        return $values;
    }
}
