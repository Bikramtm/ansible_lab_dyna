<?php
class Dyna_Configurator_Model_Client_SmartCardTypeClient extends Dyna_Service_Model_Client
{
    const WSDL_CONFIG_KEY = "smart_card_type/wsdl_smart_card_type";
    const ENDPOINT_CONFIG_KEY = "smart_card_type/endpoint_smart_card_type";
    const CONFIG_STUB_PATH = 'smart_card_type/use_stubs';

    /**
     * GetSmartCardTypes
     * @param $params
     * @return mixed
     */
    public function executeGetSmartCardType($params = array())
    {
        $root = [];
        $this->setOgwHeaderInfo($root, 'GetSmartCardType');
        $root['ROOT']['DATA'] = $params;

        return $this->GetSmartCardType($root);
    }
}
