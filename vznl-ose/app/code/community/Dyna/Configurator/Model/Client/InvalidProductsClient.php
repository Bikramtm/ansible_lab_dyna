<?php

/**
 * Class Dyna_Configurator_Model_Client_InvalidProductsClient
 */
class Dyna_Configurator_Model_Client_InvalidProductsClient extends Dyna_Service_Model_Client
{
    const WSDL_CONFIG_KEY = "invalid_products/wsdl_invalid_products";
    const ENDPOINT_CONFIG_KEY = "invalid_products/endpoint_invalid_products";
    const CONFIG_STUB_PATH = 'invalid_products/use_stubs';

    /**
     * VFDEGetConfigurationDataRequest
     * @param $params
     * @return mixed
     */
    public function executeGetInvalidProducts($params = array())
    {
        $cacheId = $this->buildCacheKey(__METHOD__, $params);
        $root = array();

        if ($cachedValues = $this->getCache()->load($cacheId)) {
            $values = unserialize($cachedValues);
        } else {
            $this->setOgwHeaderInfo($root, 'GetInvalidProducts');
            $root['ROOT']['DATA'] = $params;

            $values = $this->GetInvalidProducts($root);

            $this->getCache()->save(serialize($values), $cacheId);
        }

        return $values;
    }
}
