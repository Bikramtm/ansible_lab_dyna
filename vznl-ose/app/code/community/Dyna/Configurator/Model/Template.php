<?php

class Dyna_Configurator_Model_Template extends Omnius_Configurator_Model_Template
{
    /** @var Dyna_Package_Model_PackageType $currentPackageType */
    protected $currentPackageType = null;
    protected $configuratorTemplateDir = null;
    protected $configuratorParentTemplateDir = null;
    protected $genericType = "generic";
    protected $genericSubType = "generic";

    /**
     * @param $type
     * @return array|mixed
     */
    public function getAllTemplates($type)
    {
        $key = sprintf('_cache_templates_%s_%s', $type, Mage::app()->getWebsite()->getCode());
        if ($templates = unserialize($this->getCache()->load($key))) {
            return $templates;
        } else {
            $this->clearPackageType();
            $templates = array();
            $allowedSections = array_diff(
                Mage::getSingleton('dyna_configurator/catalog')->getOptionsForPackageType($type),
                Mage::getSingleton('dyna_configurator/catalog')->getOrderRestrictions(Mage::app()->getWebsite()->getCode())
            );

            // No need to filter them against an allowed sections array any more
            // It would be wise to to this in getOptionsForPackageType
            foreach ($allowedSections as $section) {
                $block = new Mage_Page_Block_Html();
                $templateFile = $this->prepareBlock($block, $type, $section);

                // Logic moved to prepareBlock method
                $block->setScriptPath(Mage::getBaseDir('design'));
                $templates[strtolower($section)] = $block->fetchView($templateFile);
            }
            $templates['partials'] = $this->getPartials($this->getPackageType($type)->getPackageCode(true));

            $this->getCache()->save(serialize($templates), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());

            return $templates;
        }
    }

    /**
     * Method that customizes current configurator block, adding naming, cardinality ...etc
     * @param Mage_Page_Block_Html $block
     * @param string $forPackageType
     * @param string $forPackageSubType
     */
    protected function prepareBlock (Mage_Page_Block_Html &$block, string $forPackageType, string $forPackageSubType)
    {
        $packageType = $this->getPackageType($forPackageType);
        $packageSubtype = $packageType->getPackageSubType($forPackageSubType);

        $templateType = sprintf($this->_templatePath, $packageType->getPackageCode(true), $packageSubtype->getPackageCode(true));
        $genericTemplateType = sprintf($this->_templatePath, $this->genericType, $this->genericSubType);

        $this->getTemplateSourceDir();
        // Check for existence of a customized template file and fall back to generic if not found
        $customFileExistsInTheme = is_file(Mage::getBaseDir('design') . DS . $this->configuratorTemplateDir . DS . $templateType);
        $customFileExistsInParentTheme = is_file(Mage::getBaseDir('design') . DS . $this->configuratorParentTemplateDir . DS . $templateType);
        $genericFileExistsInTheme = is_file(Mage::getBaseDir('design') . DS . $this->configuratorTemplateDir . DS . $genericTemplateType);
        if (!$customFileExistsInTheme && !$customFileExistsInParentTheme) {
            $templateType = $genericTemplateType;
        }

        $templateDir = $this->configuratorTemplateDir;
        if (!$customFileExistsInTheme && ($customFileExistsInParentTheme || !$genericFileExistsInTheme)) {
            $templateDir = $this->configuratorParentTemplateDir;
        }
        $templateFile = $templateDir . DS . $templateType;

        $block
            // Set template on current instance
            ->setTemplate($templateType)
            // Set package type name
            ->setPackageTypeName($packageSubtype->getFrontEndName())
            ->setType($packageType->getPackageCode(true))
            ->setSection($packageSubtype->getPackageCode(true));
        return $templateFile;
    }

    /**
     * Get package type instance
     * @param string $packageTypeCode - should arrive upper cased
     * @return Dyna_Package_Model_PackageType
     */
    protected function getPackageType(string $packageTypeCode)
    {
        if (!$this->currentPackageType) {
            $packageTypeCode = strtoupper($packageTypeCode);
            $this->currentPackageType = Mage::getModel('dyna_package/packageType')
                ->loadByCode($packageTypeCode);
        }

        return $this->currentPackageType;
    }

    /**
     * Clear package type from current instance
     * @return $this
     */
    protected function clearPackageType()
    {
        $this->currentPackageType = null;

        return $this;
    }

    /**
     * @param $type
     * @return array|mixed
     */
    public function getPartials($type)
    {
        $key = sprintf('_cache_partials_%s_%s', $type, Mage::app()->getWebsite()->getCode());
        if ($partials = unserialize($this->getCache()->load($key))) {
            return $partials;
        } else {
            $partials = array();
            $this->getTemplateSourceDir();

            foreach ($this->getPackageType($type)->getPackageSubTypes() as $packageSubType) {
                if (!$packageSubType->getVisibleConfigurator()) {
                    continue;
                }
                $partials[$packageSubType->getPackageCode(true)] = $this->getPartialsForSubType($type, $packageSubType);
            }

            $this->getCache()->save(serialize($partials), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
            return $partials;
        }
    }

    /**
     * Get partials for the given type and subtype.
     * @param $type The type of the package.
     * @param $packageSubType The subtype.
     * @return array returns the partials.
     */
    protected function getPartialsForSubType($type, $packageSubType)
    {
        $partials = [];
        $genericPartialPath = sprintf('configurator/%s/sections/partials', $this->genericSubType);
        $partialPath = sprintf('configurator/%s/sections/partials', $this->getPackageType($type)->getPackageCode(true))  . DS . $packageSubType->getPackageCode(true);

        $prepath = Mage::getBaseDir('design') . DS;

        // All paths to check
        $customPartialDir = $this->configuratorTemplateDir . DS . $partialPath;
        $genericPartialDir = $this->configuratorTemplateDir . DS . $genericPartialPath;
        $customPartialParentDir = $this->configuratorParentTemplateDir . DS . $partialPath;
        $genericPartialParentDir = $this->configuratorParentTemplateDir . DS . $genericPartialPath;

        // Check if the paths exists and add them to the list to check
        $dirsToCheck = [];
        if (is_dir($prepath . $customPartialDir)) {
            $dirsToCheck[] = $customPartialDir;
        }
        if (is_dir($prepath . $genericPartialDir)) {
            $dirsToCheck[] = $genericPartialDir;
        }
        if (is_dir($prepath . $customPartialParentDir)) {
            $dirsToCheck[] = $customPartialParentDir;
        }
        if (is_dir($prepath . $genericPartialParentDir)) {
            $dirsToCheck[] = $genericPartialParentDir;
        }

        $filesAlreadyUsed = [];
        foreach ($dirsToCheck as $dir) {
            // Scan for templates in this dir
            foreach (scandir($prepath . $dir) as $fileName) {
                // Check if filename not `.` nor `..` and also that the file is not already added.
                if (in_array($fileName, array('.', '..')) || in_array($fileName, $filesAlreadyUsed)) {
                    continue;
                }

                // Register each filename as a partial key and a partial path
                $templateName = str_replace(".phtml", "", $fileName);
                $block = new Mage_Page_Block_Html();
                $templateFile = $dir . DS . $fileName;
                $block->setScriptPath(Mage::getBaseDir('design'))
                    ->setTemplate($partialPath . DS . $fileName)
                    ->setPackageSubTypeName($packageSubType->getFrontEndName())
                    ->setSection($packageSubType->getPackageCode(true));
                $partials[$templateName] = $block->fetchView($templateFile);

                // set as already used
                $filesAlreadyUsed[] = $fileName;
            }
        }

        return $partials;
    }

    /**
     * Get the full path to configurator templates (where the generic and customized sections, selected items and family templates can be found)
     * @notice has no trailing slash at the end of the returned string
     * @return string
     */
    public function getTemplateSourceDir()
    {
        $design_package = Mage::getSingleton('core/design_package')->getPackageName();
        if (!$this->configuratorTemplateDir) {
            $this->configuratorTemplateDir =
                // Start with frontend
                'frontend'
                // Append current design package
                . DS . $design_package
                // Append current theme (usually is default)
                . DS . Mage::getSingleton('core/design_package')->getTheme('frontend')
                // Forward to template directory
                . DS . 'template';
        }
        if (!$this->configuratorParentTemplateDir) {
            $fallback = Mage::getSingleton('core/design_fallback', array(
                'config' => Mage::getModel('core/design_config'),
            ));
            $fallbackScheme = $fallback->getFallbackScheme('frontend',
                $design_package,
                'default'
            );
            $parent_design_package = $design_package;
            foreach ($fallbackScheme as $scheme) {
                if (!empty($scheme) && array_key_exists('_package', $scheme)) {
                    $parent_design_package = $scheme['_package'];
                    break;
                }
            }
            $this->configuratorParentTemplateDir =
                // Start with frontend
                'frontend'
                // Append current design package
                . DS . $parent_design_package
                // Append current theme (usually is default)
                . DS . Mage::getSingleton('core/design_package')->getTheme('frontend')
                // Forward to template directory
                . DS . 'template';
        }

        return $this->configuratorTemplateDir;
    }
}
