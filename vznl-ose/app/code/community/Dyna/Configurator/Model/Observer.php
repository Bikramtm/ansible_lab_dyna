<?php

class Dyna_Configurator_Model_Observer
{
    /**
     * Special collector for Cable Rules
     * @param $observer
     * @return $this
     */
    public function collectTotalsAfter($observer)
    {
        // Skip if product is not added from configurator
        if (Mage::registry('product_add_origin') != 'addMulti') {
            return $this;
        }

        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = $observer->getEvent()->getQuote();

        // Set this flag to true if max iterations has been reached or no need for triggering cable post condition rules
        // see OMNVFDE-2119: checkout added products should not trigger cable post condition rules
        if ($quote->getSkipCablePostConditions()) {
            return $this;
        }

        /** @var Dyna_Package_Model_Package $activePackage */
        $activePackage = $quote->getCartPackage();

        // Checking for active package id because getFirstItem (called by getPackageById) returns an empty model instance if collection is empty
        if ($activePackage && $activePackage->isCable()) {
            try {
                /** @var Dyna_Configurator_Helper_Rules $helper */
                $helper = Mage::helper('dyna_configurator/rules');
                $helper->executeCablePostRules($quote, $activePackage->getType());
            } catch (Exception $e) {
                Mage::logException($e);
            }

            if ($quote->getNeedsRecollectPostTotals()) {
                /** @var Dyna_Checkout_Model_Sales_Quote @quote */
                foreach ($quote->getAllAddresses() as $address) {
                    $address->unsetData('cached_items_nonnominal');
                }

                Mage::log('=== Re-execute OMNIUS SALES RULES ===', Zend_Log::DEBUG, 'CableOmniusRules.log', true);

                $quote->setTotalsCollectedFlag(false)
                    ->collectTotals()
                    ->setNeedsRecollectPostTotals(false);
            }
        }

        return $this;
    }

    /**
     * Method that catches magento session event of initiating a new quote
     * Needed for aligning new initiated quote with session so that session should always have the quote id previously initiated
     * If not caught, the only way of having a quote id on current session is to save the quote/cart (usually after adding a product) which will
     * brake certain code logic as it counting on this alignment: active quote id = session quote id
     * @param $observer
     * @return $this
     */
    public function initQuote($observer)
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = $observer->getEvent()->getQuote();
        if (!Mage::getSingleton('checkout/session')->getQuoteId()) {
            if (!$quote->getId()) {
                $quote->save();
            }

            Mage::getSingleton('checkout/session')->setQuoteId($quote->getId());
        }

        return $this;
    }

    /**
     * Checks whether we are on index page and removes the
     * shipping fee product from cart
     *
     * @param $observer
     */
    public function updateShippingFeeProduct($observer)
    {
        /** @var Mage_Core_Controller_Front_Action $controller */
        $controller = $observer->getEvent()->getControllerAction();
        /** @var Mage_Core_Controller_Request_Http $request */
        $request = $controller->getRequest();

        // remove shipping fee product if the agent returns to configurator
        if ($request->getRouteName() === 'cms' && $request->getActionName() === 'index') {
            /** @var Dyna_Checkout_Helper_Delivery $deliveryHelper */
            $deliveryHelper = Mage::helper('dyna_checkout/delivery');
            $deliveryHelper->removeShippingFeeProduct();
        }
    }
}
