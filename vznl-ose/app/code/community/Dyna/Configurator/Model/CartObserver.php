<?php

class Dyna_Configurator_Model_CartObserver extends Omnius_Configurator_Model_CartObserver
{
    /**
     * Executed when an item is added to the quote with
     * the responsibility to link the newly added item to
     * an existing quote item, if this is possible
     *
     * @param Varien_Event_Observer $observer
     */
    public function linkProducts(Varien_Event_Observer $observer)
    {
        /** @var Mage_Sales_Model_Quote_Item $addedQuoteItem */
        $addedQuoteItem = $observer->getQuoteItem();
        /** @var Mage_Catalog_Model_Product $addedProduct */
        $addedProduct = $addedQuoteItem->getProduct();
        $quote = Mage::getModel('sales/quote')->load($addedQuoteItem->getQuoteId());

        /**
         * If the quote does not have an active package id
         * we get the biggest package id as active
         */
        if (!$quote->getActivePackageId()) {
            $quote->saveActivePackageId(self::getBiggestPackageId($quote));
        }
        $package = $quote->getCartPackage();
        $saleType = $quote->getPackageSaleType($quote->getActivePackageId()) ?: $addedProduct->getSaleType();
        if (!$quote->getIsOrderEdit()) {
            $addedQuoteItem
                // set package id only if item doesn't have one
                ->setPackageId($addedQuoteItem->getPackageId() ?: $quote->getActivePackageId())
                // set ctn only if it doesn't exist
                ->setCtn($addedQuoteItem->getCtn() ?: $package->getCtn())
                // set new package type only if it doesn't exist
                ->setPackageType($addedQuoteItem->getPackageType() ?: $package->getType())
                ->setSaleType($addedQuoteItem->getSaleType() ?: $saleType);
        }
        // no price and maf for added product if it is a promo product (discount amount is set on totals collectors)
        if ($addedQuoteItem->isPromo() || $addedQuoteItem->isBundlePromo()) {
            $addedProduct
                ->setMaf(0)
                ->setPrice(0);
        }
    }

    /**
     * Executed when an item is added to the quote from another saved quote
     *
     * @param Varien_Event_Observer $observer
     * @return bool
     */
    public function linkClonedProducts(Varien_Event_Observer $observer)
    {
        /** @var Mage_Sales_Model_Quote_Item $addedQuoteItem */
        $addedQuoteItem = $observer->getQuoteItem();
        $quote = Mage::getModel('sales/quote')->load($addedQuoteItem->getQuoteId());
        /**
         * If the quote does not have an active package id
         * we get the biggest package id as active
         */
        if (!$quote->getActivePackageId()) {
            $quote->saveActivePackageId(self::getBiggestPackageId($quote));
        }
        if(!$addedQuoteItem->getPackageId()){
            $package = $quote->getCartPackage();
        }else{
            $package = $quote->getCartPackage($addedQuoteItem->getPackageId());
        }

        if (!$quote->getIsOrderEdit()) {
            if(!$addedQuoteItem->getPackageId()){
                $addedQuoteItem->setPackageId($quote->getActivePackageId());
            }
            if(!$addedQuoteItem->getCtn()){
                $addedQuoteItem->setCtn($package->getCtn());
            }
            if(!$addedQuoteItem->getPackageType()){
                $addedQuoteItem->setPackageType($package->getType());
            }
            if(!$addedQuoteItem->getSaleType()){
                $saleType = $quote->getPackageSaleType($quote->getActivePackageId());
                $addedQuoteItem->setSaleType($saleType);
            }
            $addedQuoteItem->save();
        }

        // no price and maf for added product if it is a promo product (discount amount is set on totals collectors)
        if ($addedQuoteItem->isPromo() || $addedQuoteItem->isBundlePromo()) {
            /** @var Mage_Catalog_Model_Product $addedProduct */
            $addedProduct = $addedQuoteItem->getProduct();
            $addedProduct
                ->setMaf(0)
                ->setPrice(0);
        }
    }
}
