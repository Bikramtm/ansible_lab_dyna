<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Configurator_IndexController
 */
class Dyna_Configurator_IndexController extends Mage_Core_Controller_Front_Action
{
    /** @var Omnius_Core_Helper_Data|null */
    private $_dynaCoreHelper = null;

    /**
     * @return Omnius_Core_Helper_Data
     */
    protected function getDynaCoreHelper()
    {
        if ($this->_dynaCoreHelper === null) {
            $this->_dynaCoreHelper = Mage::helper('omnius_core');
        }

        return $this->_dynaCoreHelper;
    }

    /**
     * Edit ILS mobile action
     */
    public function inlifeAction()
    {
        if ($this->getRequest()->isPost()) {
            $productSkus = $this->getRequest()->getPost('products');
            $tariffSku = $this->getRequest()->getPost('tariff');
            $allSocs = $this->getRequest()->getPost('allSocs');
            $context = $this->getRequest()->getPost('context');
            $contractEndDate = $this->getRequest()->getPost('contract_end_date');
            $contractPossibleCancellationDate = $this->getRequest()->getPost('contract_possible_cancellation_date');
            $convertToMember = $this->getRequest()->getPost('convert_to_member');
            $bundleProductIds = $this->getRequest()->getPost('bundle_product_ids');
            $packageServiceLineId = $this->getRequest()->getPost('service_line_id');
            $customerId = $this->getRequest()->getPost('customer_id');
            $stack = $this->getRequest()->getPost('stack');

            Mage::register('ils_process_initiated', $context);

            /** @var Dyna_Checkout_Model_Sales_Quote $quote */
            $quote = Mage::getSingleton('checkout/cart')->getQuote();

            $valid = $this->validateInlifeRequest($quote);

            if ($valid['error']) {
                return $this->jsonResponse($valid);
            }

            //tariff sku is required for all ILS flows, except MIGCOC
            if (!$tariffSku && $context != Dyna_Catalog_Model_ProcessContext::MIGCOC) {
                $this->jsonResponse(array(
                    'error' => true,
                    'message' => $this->__("No installed base tariff provided")
                ));
            }

            try {
                /** @var Dyna_Checkout_Model_Cart $cart */
                $cart = Mage::getSingleton('checkout/cart');
                /** @var Dyna_Bundles_Helper_Redplus $redplusHelper */
                $redplusHelper = Mage::helper('dyna_bundles/redplus');
                /** @var Dyna_Bundles_Helper_Data $bundlesHelper */
                $bundlesHelper = Mage::helper('dyna_bundles');
                /** @var Dyna_Configurator_Helper_Cart $cartHelper */
                $cartHelper = Mage::helper('dyna_configurator/cart');
                /** @var Dyna_Checkout_Helper_Cart $checkoutHelper */
                $checkoutHelper = Mage::helper('dyna_checkout/cart');
                $packageType = strtolower($this->getRequest()->getPost('package_type'));
                $packageCreationType = strtolower($this->getRequest()->getPost('package_creation_type'));
                $packageCTN = $this->getRequest()->getPost('ctn');
                $productId = $this->getRequest()->getPost('product_id');
                $prolongationType = $this->getRequest()->getPost('prolongation_type', null);
                $prolongationInfoEligibility = $this->getRequest()->getPost('prolongation_info_eligibility', null);
                $installBaseSubscriptionNumber = $this->getRequest()->getPost('customer_number');
                $isInMinimumDuration = $this->isInMinimumDuration($customerId);
                $redPlusRole = $this->getRequest()->getPost('redplus_role');
                $customerSession = Mage::getSingleton('customer/session');
                $productIds = [];
                $cartHelper->setNewPackageIlsInitialSocs($allSocs);
                $bundleProductIds = explode(",", $bundleProductIds);

                $currentProducts = '';
                $products = $productsBySku = [];
                $packageId = null;
                $foundedRedPlusOwnerInCart = null;
                $customMessages = array();
                $productModel = Mage::getModel('dyna_catalog/product');
                /** @var Dyna_Catalog_Model_Resource_Product_Collection $productsCollection */
                $productsCollection = $productModel->getCollection();
                $productsCollection->addAttributeToSelect('preselect');
                //if we are on MIGCOC flow we don't add the products in the cart because are not compatible
                //installed base products in this flow are part of prepaid and the created package is postpaid
                if (count($productSkus) && $context != Dyna_Catalog_Model_ProcessContext::MIGCOC) {
                    $productSkus = explode(',', $productSkus);
                    $productSkus = $this->filterSkus($productSkus);
                    $productSkus = array_combine($productSkus, $productSkus);
                    $productsCollection = $productsCollection->addFieldToFilter('sku', ['in' => $productSkus]);
                    $products = $productsCollection->getItems();
                    foreach ($products as $product) {
                        $sku = $product->getSku();
                        if ($productSkus[$sku]) {
                            $productIds[$sku] = $product->getId();
                            $productsBySku[$sku] = $product;
                        }
                    }
                    $currentProducts = implode(',', $productIds);
                }

                // Sale type is now replaced by process_context
                $saleType = $context;
                $packageCreationTypeId = $cartHelper->getCreationTypeId($packageCreationType);

                if ($convertToMember) {
                    $packageCreationTypeId = $cartHelper->getCreationTypeId(Dyna_Catalog_Model_Type::CREATION_TYPE_REDPLUS);
                    // if not sent in request, try loading it from internal mapping
                } elseif (!$packageCreationTypeId) {
                    $packageCreationTypeId = $cartHelper->getCreationTypeId(Dyna_Catalog_Model_Type::getCreationTypesMapping()[strtolower($packageType)]);
                }

                $packages = $quote->getCartPackages();
                if ($packages && $stack == Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS) {
                    foreach ($packages as $package) {
                        if ($package->isPartOfRedPlus() && $package->getParentId() == null && $package->getParentAccountNumber() == $customerId && $package->getServiceLineId() == $packageServiceLineId) {
                            $package
                                ->setEditingDisabled(null)
                                ->setCurrentProducts($currentProducts)
                                ->setInitialInstalledBaseProducts(null)
                                ->setInstalledBaseProducts(null)
                                ->setSaleType($saleType)
                                ->setPackageCreationTypeId($packageCreationTypeId)
                                ->setCurrentStatus(null);
                            //delete items from package and added the ones from installed base in order to not have more then one product in cart with the same sku
                            $dummyPackageItems = $package->getAllItems();
                            foreach ($dummyPackageItems as $dummyItem) {
                                $dummyItem->delete();
                            }
                            $package->save();
                            $packageId = $package->getPackageId();
                            $quote->saveActivePackageId($packageId);
                            break;
                        }

                        if ($package->isPartOfRedPlus()
                            && $package->getParentId() == null &&
                            $redPlusRole == Dyna_Customer_Helper_Customer::SUBSCRIPTION_TYPE_MEMBER
                        ) {
                            $foundedRedPlusOwnerInCart = $package;
                            break;
                        }
                    }
                }
                unset($package);
                if (!$packageId) {
                    // Create new package with the products and add it to the quote
                    $packageId = $cartHelper->initNewPackage(
                        $packageType,
                        $saleType,
                        $packageCTN,
                        $currentProducts,
                        null,
                        null,
                        null,
                        null,
                        $packageCreationTypeId
                    );
                }

                /** @var Dyna_Package_Model_Package $packageModel */
                $packageModel = $quote->getCartPackage($packageId);
                $packageModel->setServiceLineId($packageServiceLineId)
                    ->setProlongationType($prolongationType)
                    ->setProlongationInfoEligibility($prolongationInfoEligibility);

                $typeModel = Mage::getModel('dyna_package/packageCreationTypes');
                $packageCreationType = $typeModel->getById($packageCreationTypeId);
                $filters = $packageCreationType->getFilterAttributes();
                $packageModel->setPermanentFilters($filters);

                //convert to member flow
                if ($convertToMember) {
                    $packageModel = $redplusHelper->convertToMember($packageModel, $installBaseSubscriptionNumber, $saleType, $productId);
                    $packageModel->updateBundleTypes();
                }

                if ($convertToMember || $context == Dyna_Catalog_Model_ProcessContext::MIGCOC && $packageType == strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE)) {
                    $customerId = $customerSession->getKiasLinkedAccountNumber();
                }

                // modify install-base subscription that is already part of an red-plus group (as member) and the red-plus group is available in cart
                if ($foundedRedPlusOwnerInCart) {
                    $packageModel
                        ->setParentId($foundedRedPlusOwnerInCart->getId())
                        ->setBundleTypes(Dyna_Bundles_Model_BundleRule::TYPE_RED_PLUS)
                        ->setPackageCreationTypeId($cartHelper->getCreationTypeId(Dyna_Catalog_Model_Type::CREATION_TYPE_REDPLUS))
                        ->setSharingGroupId($foundedRedPlusOwnerInCart->getSharingGroupId())
                        ->setAddedFromMyProducts(1);
                    /*  ->setRedplusRelatedProduct();*///todo get bundles' subscription and related product!
                    $bundlesHelper->addBundlePackageEntry($foundedRedPlusOwnerInCart->getBundleOfType(Dyna_Bundles_Model_BundleRule::TYPE_RED_PLUS)->getId(), $packageModel->getId());
                    // let quote know that the active package is the child
                    $quote->setActivePackageId($packageModel->getPackageId());
                }

                !$convertToMember && $packageModel->setParentAccountNumber($customerId);
                $convertToMember && $packageModel->setConvertedToMember(1);
                $packageModel->save();

                // VFDED1W3S-1380 changes
                /** @var Dyna_Catalog_Model_Product|null $tariff */
                $tariff = null;
                foreach ($products as $product) {
                    if ($product->getSku() == $tariffSku) {
                        $tariff = $product;
                        break;
                    }
                }
                $tariffId = null;
                $tariffDefaultProductsIds = $tariffDefaultProductsIdsBySku = $tariffDefaultProductsModelsBySku = [];

                // todo: determine if the defaulted should be searched for all the products or only for the tariff
                $serviceProducts = array();
                $tariffDefaultProducts = array();
                if (($tariff && $tariffId = $tariff->getId()) || ($currentProducts == "" && $context == Dyna_Catalog_Model_ProcessContext::MIGCOC)) {

                    if ($currentProducts == "" && $context == Dyna_Catalog_Model_ProcessContext::MIGCOC) {
                        $tariffId = null;
                    }

                    $tariffDefaultProducts = $checkoutHelper->getDefaultedProducts([$tariffId], $packageModel, Dyna_ProductMatchRule_Helper_Data::SOURCE_COLLECTION_IB);

                    if (isset($tariffDefaultProducts['service'])) {
                        $serviceProducts = $tariffDefaultProducts['service'];
                        unset($tariffDefaultProducts['service']);
                    }

                    $tariffDefaultProducts = array_filter(array_merge(array_shift($tariffDefaultProducts), $serviceProducts));

                    if (count($tariffDefaultProducts)) {
                        foreach ($tariffDefaultProducts as $defaultProduct) {
                            $tariffDefaultProductsIds[] = $defaultProduct['target_product_id'];
                        }
                    }

                    if(!empty($tariffDefaultProductsIds)){
                        /** @var Dyna_Catalog_Model_Resource_Product_Collection $productsCollection */
                        $productsCollection = $productModel->getCollection();
                        /*$productsCollection->addAttributeToSelect('preselect');*/
                        $productsCollection = $productsCollection->addFieldToFilter('entity_id', ['in' => $tariffDefaultProductsIds]);
                        foreach ($productsCollection as $productDefault){

                            $tariffDefaultProductsIdsBySku[$productDefault->getSku()] = $productDefault->getId();
                            $tariffDefaultProductsModelsBySku[$productDefault->getSku()] = $productDefault;
                        }
                    }
                }

                // define function
                $arraySearchMultidim = function ($array, $column, $key){
                    return (array_search($key, array_column($array, $column)));
                };

                // inject the tariff defaulted products
                // @todo clarify if omnius defaulted, not service, should be appended to these ids
                $allProductIds = array_unique(array_merge($productIds, $tariffDefaultProductsIdsBySku));
                $productsBySkuMerged = array_merge($productsBySku, $tariffDefaultProductsModelsBySku);

                // needed to execute postcondition rules for cable
                Mage::register('product_add_origin', 'addMulti');
                $stringErrors = false;

                foreach ($allProductIds as $productSku => $productId) {
                    /** @var Dyna_Catalog_Model_Product $product */
                    $product = $productsBySkuMerged[$productSku] ?? null;
                    if ($product) {
                        $isMobileProduct = count(array_intersect(explode(',', $product->getPackageType()), Dyna_Catalog_Model_Type::getMobilePackages()));
                        $isRetentionOrIls = in_array($saleType, array_merge(Dyna_Catalog_Model_ProcessContext::getProlongationProcessContexts(), Dyna_Catalog_Model_ProcessContext::ILSProcessContexts()));
                        $preselect = true;
                        // [VFDED1W3S-1108] For Mobile ILS/Retention check the if the product has the Preselect attribute value set to 'Yes', otherwise it will be skipped
                        if ($isMobileProduct && $isRetentionOrIls && !$product->getPreselect() && array_key_exists($product->getSku(), $productIds)) {
                            if (!isset($customMessages['preselect'])) {
                                $customMessages['preselect']['message'] = $this->__("Please be aware that the following products will not be added in the cart due the preselect option attribute: ");
                            }
                            $customMessages['preselect']['items'][] = $product->getSku();
                            $preselect = false;
                        }
                        // check if product is a defaulted product of the tariff and it is not already part of installed base
                        $foundInDefaulted = $arraySearchMultidim($tariffDefaultProducts, 'target_product_id', $productId);
                        if ($foundInDefaulted !== false && in_array($productId, $productIds)) {
                            $foundInDefaulted = false;
                        }
                        $productData = [
                            'qty' => 1,
                            'one_of_deal' => 0,
                            'preselect_product' => $preselect,
                            'is_contract' => $foundInDefaulted !== false ? 0 : 1,
                            'system_action' => $foundInDefaulted !== false ? Dyna_Checkout_Model_Sales_Quote_Item::SYSTEM_ACTION_ADD : Dyna_Checkout_Model_Sales_Quote_Item::SYSTEM_ACTION_EXISTING,
                            'contract_end_date' => $productId == $tariffId ? $contractEndDate : null,
                            'contract_possible_cancellation_date' => $productId == $tariffId ? $contractPossibleCancellationDate : null,
                            'is_defaulted' => $foundInDefaulted !== false ? 1 : null,
                            'is_obligated' => $foundInDefaulted !== false ? $tariffDefaultProducts[$foundInDefaulted]['obligated'] : null
                        ];

                        /** @var Dyna_Checkout_Model_Sales_Quote_Item $item */
                        $item = $cart->addProduct($productId, $productData);

                        if (!is_string($item)) {
                            if (($item->getProduct()->isSubscription()
                                    || in_array($item->getProductId(), $bundleProductIds)
                                    || in_array($item->getSku(), Dyna_Customer_Model_Client_RetrieveCustomerInfo::getPartOfBundleSocs())
                                )
                                && Mage::getSingleton('customer/session')->getCustomer()->isIBProductPartOfBundle($customerId, $packageServiceLineId)
                            ) {
                                $item->setBundleComponent(-1);
                            }

                            $item
                                ->setInMinimumDuration((int)$isInMinimumDuration)
                                ->setPackageType(strtolower($packageType));

                            if ($convertToMember) {
                                $item->setPackageId($packageModel->getPackageId());
                            }

                            //move item to alternate quote items if product has preselect attribute
                            if (!$preselect) {
                                $item->setSystemAction(Dyna_Checkout_Model_Sales_Quote_Item::SYSTEM_ACTION_NOT_APPLICABLE);
                                $item->isDeleted(true);
                            }
                        } else {
                            $stringErrors[] = $item;
                        }
                    }
                }

                //VFDED1W3S-3023
                if ($tariff && in_array($tariff->getSku(), (!empty($customMessages['preselect']['items']) ? $customMessages['preselect']['items'] : array())) && in_array($tariff->getPackageType(), Dyna_Catalog_Model_Type::getMobilePackages())) {
                    $addedProducts = $packageModel->getAllItems();
                    foreach ($addedProducts as $addedProduct){
                        $addedProduct->setPreselectProduct(0);
                        $addedProduct->setSystemAction(Dyna_Checkout_Model_Sales_Quote_Item::SYSTEM_ACTION_NOT_APPLICABLE);
                        $addedProduct->isDeleted(true);
                    }
                }

                if ($stringErrors) {
                    throw new Exception(implode("<br />", $stringErrors));
                }

                switch ($stack) {
                    case Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS:
                        $customerSession->setKiasLinkedAccountNumber($customerId);
                        break;
                    case Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN:
                        $customerSession->setFnLinkedAccountNumber($customerId);
                        break;
                    case Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD:
                        $customerSession->setKdLinkedAccountNumber($customerId);
                        break;
                }

                $cart->save();

                $response = array(
                    'error' => false,
                    'message' => $cartHelper->__('Package created successfully'),
                    'rightBlock' => Mage::getSingleton('core/layout')->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/partials/package_block.phtml')->toHtml(),
                    'totals' => Mage::getSingleton('core/layout')->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/totals.phtml')->toHtml(),
                    'package_id' => $packageId
                );

                //set inlife response
                if(!empty($customMessages['preselect']['items'])){
                    $response['customMessage'] = $customMessages['preselect']['message'] . " " . implode(", ", $customMessages['preselect']['items']);
                }
                $this->jsonResponse($response);

            } catch (Exception $e) {
                $this->jsonResponse(array(
                    'error' => true,
                    'message' => $e->getMessage()
                ));
            }
        }
    }

    /**
     * Delete skus which are set in admin in general configuration page at ignoredSku field.
     */
    protected function filterSkus($productSkus){
        $configIgnoredSkus = Mage::getStoreConfig('omnius_general/cable_general_settings/ignored_sku');
        $configIgnoredSkus= explode("\n", $configIgnoredSkus);
        foreach ($configIgnoredSkus as $ignoredSku){
            if (in_array($ignoredSku,$productSkus)){
                $productSkus = array_diff($productSkus,array($ignoredSku));
            }
        }
        return $productSkus;
    }

    /**
     * Validate the inlife action
     * @param Dyna_Checkout_Model_Sales_Quote $quote
     * @return mixed
     */
    protected function validateInlifeRequest($quote)
    {
        /** @var Dyna_Customer_Model_Session $customerStorage */
        $customerStorage = Mage::getSingleton('dyna_customer/session');
        $installBaseData = $customerStorage->getCustomerProducts();
        $buttons = array_keys(Dyna_Customer_Helper_Validation::$myProductsActionButtons);
        $fail = false;

        $customerId = $this->getRequest()->getPost('customer_id');
        $serviceLineId = $this->getRequest()->getPost('service_line_id');
        $button = $this->getRequest()->getPost('button');
        $stack = $this->getRequest()->getPost('stack');
        $packageType = $this->getRequest()->getPost('package_type');

        // check if there is already an inlife started for this install base product
        /** @var Dyna_Package_Model_Package $package */
        foreach ($quote->getCartPackages() as $package) {
            if ($package->getParentAccountNumber() == $customerId && $package->getServiceLineId() == $serviceLineId) {
                return array(
                    'error' => true,
                    'message' => Mage::helper('dyna_core')->__("This product is already modified in cart. Please clear cart before proceeding.")
                );
            }
        }

        //responses array
        $response = [
            "success" => [
                'error' => false
            ],
            "fail" => [
                'error' => true,
                'message' => $this->__("This action can not be performed as it is not allowed.")
            ]
        ];

        // Check if we get stack from request and if exists
        if(!isset($stack) || isset($stack) && !in_array($stack, Dyna_Customer_Helper_Services::getAllStacks())){
            $fail = true;
        }
        // Check if we get the pressed button from request and if exists
        if(!isset($button) || isset($button) && !in_array($button, $buttons)){
            $fail = true;
        }
        // check if we get the customer id from request
        if(!isset($customerId) || isset($customerId) && trim($customerId) == ""){
            $fail = true;
        }
        // check if we get the service line id from request (serviceLineId should be the productId from the install base data
        if(!isset($serviceLineId) || isset($serviceLineId) && trim($serviceLineId) == ""){
            $fail = true;
        }

        //if one of above fails, return
        if ($fail) {
            return $response['fail'];
        }
        //check the initial state of the pressed button
        foreach ($installBaseData[$stack][$customerId]['contracts'] as $contract) {
            foreach ($contract['subscriptions'] as $subscription) {
                if (in_array($stack, Dyna_Customer_Helper_Services::getMobileStack()) || in_array($stack, Dyna_Customer_Helper_Services::getFixedStack())) {
                    if ($subscription['product_id'] == $serviceLineId) {
                        $disabledButtons = $subscription['disableButtons'];
                        if (isset($disabledButtons[$button]) && $disabledButtons[$button]['class'] === true) {
                            return $response['fail'];
                        } elseif (!isset($disabledButtons[$button]) || isset($disabledButtons[$button]) && $disabledButtons[$button]['class'] === false){
                            return $response['success'];
                        }
                    }
                } elseif (in_array($stack, Dyna_Customer_Helper_Services::getCableStack())) {
                    $stack = strtolower(Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_CABLE);
                    foreach ($subscription['products'] as $KDsubscriptions) {
                        foreach ($KDsubscriptions as $KDsubscription) {
                            $disabledButtons = $KDsubscription['disableButtons'];

                            if ($KDsubscription['package_type'] != $packageType) {
                                continue;
                            }

                            if (isset($disabledButtons[$button]) && $disabledButtons[$button]['class'] === true) {
                                return $response['fail'];
                            } elseif (!isset($disabledButtons[$button]) || isset($disabledButtons[$button]) && $disabledButtons[$button]['class'] === false){
                                return $response['success'];
                            }
                        }
                    }
                }
            }
        }

        return $response['fail'];
    }

    /**
     * Determine whether or not contract is in minimum duration (see VFDED1W3S-895)
     * @param $contractId
     * @return bool
     */
    protected function isInMinimumDuration($contractId)
    {
        /** @var Dyna_Customer_Model_Customer $customer */
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        return $customer->getIsInMinimumDuration($contractId);
    }


    public function setSourceMigrationAction(){
        if (!$this->getRequest()->isPost()) {
            return;
        }
        $postData = $this->getRequest()->getParams();

        try{
            Mage::getSingleton('customer/session')->setSourcePackageCreationType($postData['package_creation_type']);
            $response = [
                'valid' => true,
                'error' => false,
            ];
        } catch(Exception $e){
            $response = [
                'valid' => false,
                'error' => true,
            ];
        }
        $this->jsonResponse($response);
    }

    /**
     * @param $data
     * @param int $statusCode
     * @throws InvalidArgumentException
     */
    protected function jsonResponse($data, $statusCode = 200)
    {
        $response = $this->getDynaCoreHelper()
            ->jsonResponse($data, $statusCode);

        $this->getResponse()
            ->setBody($response['body'])
            ->setHeader('Content-Type', 'application/json')
            ->setHttpResponseCode($response['status']);

        if (!$this->getRequest()->isAjax()) {
            $body = $this->getResponse()->getBody();
            $this->getResponse()->setHeader('Content-Type', 'text/html; charset=utf-8');
            $this->getResponse()
                ->setBody($body);
        }
    }
}

