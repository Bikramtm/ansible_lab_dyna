<?php

class Dyna_Configurator_Helper_Data extends Omnius_Configurator_Helper_Data
{
    /**
     * @param Omnius_Checkout_Model_Sales_Quote_Item $item
     * @return array
     */
    public function getBlockRowData($item)
    {
        /** @var Dyna_Catalog_Model_Product $product */
        $product = $item->getProduct();
        $originalItem = $item;
        $result = array(
            'name' => $product->getDisplayNameCart() ?: $product->getName(),
            'hashed' => false,
        );

        if ($product->isPromo()) {
            $result = $this->getPromoProductBlock($item, $product, $originalItem, $result);
        } elseif (($item->isPromo() || $item->isBundlePromo()) && $item->getTargetId()) {
            // to actually get the correct discount amount, we need parent quote item
            $result['price'] = $result['priceTax'] = -1 * abs($item->getAppliedPromoAmount());
            $result['maf'] = $result['mafTax'] = -1 * abs($item->getAppliedMafPromoAmount());
            $result['itTargetsProduct'] = 1;
        } elseif ($item->isPromo()) {
            $result['price'] = Mage::helper('tax')->getPrice($product, $item->getRowTotal(), false);
            $result['priceTax'] = Mage::helper('tax')->getPrice($product, $item->getRowTotalInclTax(), false);
            $result['maf'] = Mage::helper('tax')->getPrice($product, $item->getMaf(), false);
            $result['mafTax'] = Mage::helper('tax')->getPrice($product, $item->getMafInclTax(), true);
        }

        if ($product->isPromo() || $item->isPromo() || $item->isBundlePromo()) {
            return $result;
        }

        $result['maf'] = (float)$item->getMaf();
        $result['mafTax'] = (float)$item->getMafInclTax();
        $result['price'] = (float)$item->getPrice();
        $result['priceTax'] = (float)$item->getPriceInclTax();

        if (!$product->isPromo()) {
            $result['price'] = Mage::helper('tax')->getPrice(
                $product,
                $item->getHasMixMatch() ? ($product->getSpecialPrice() ?: $product->getPrice()) : $item->getPrice(),
                false
            );
            $result['priceTax'] = Mage::helper('tax')->getPrice(
                $product,
                $item->getHasMixMatch() ? ($product->getSpecialPrice() ?: $product->getPrice()) : $item->getPriceInclTax(),
                true
            );
            $result['maf'] = Mage::helper('tax')->getPrice(
                $product,
                $item->getHasMixMatch() ? ($product->getMaf()) : $item->getMaf(),
                true
            );
            $result['mafTax'] = Mage::helper('tax')->getPrice(
                $product,
                $item->getHasMixMatch() ? ($product->getMaf()) : $item->getMafInclTax(),
                true
            );
        }

        if ($product->isAccessory()) {
            $result['discount'] = true;
        }

        $result['initial_period'] = ($product->getInitialPeriod()) ? $product->getInitialPeriod() : null;
        $result['minimum_contract_duration'] = ($product->getMinimumContractDuration()) ? $product->getMinimumContractDuration() : null;
        $result['contract_period'] = ($product->getContractPeriod()) ? $product->getContractPeriod() : null;
        $result['is_contract'] = $item->isContract();

        return $result;
    }

    /**
     * Get customer SOHO status
     * return string
     */
    public function isCustomerSoho()
    {
        return Mage::getSingleton('customer/session')
            ->getCustomer()
            ->getIsSoho();
    }

    /**
     * Returns the open orders collection from the database.
     *
     * @param $clusterType
     * @param $resultsPerPage
     * @param $currentPage
     * @return object
     */
    public function getOpenOrders($clusterType, $resultsPerPage, $currentPage)
    {
        $superOrders = Mage::getModel('superorder/superorder')->getCollection();


        $superOrders->getSelect()
            ->joinLeft(
                array('orders' => 'sales_flat_order'),
                'main_table.entity_id = orders.superorder_id',
                [
                    'orders.customer_firstname AS customer_firstname',
                    'orders.customer_lastname AS customer_lastname',
                    'orders.company_name AS company_name'
                ]
            )
            ->joinRight(
                array('packages' => 'catalog_package'),
                'main_table.entity_id = packages.order_id',
                [
                    'packages.status AS status',
                    'COUNT(distinct(packages.entity_id)) AS total_packages',
                    'MAX(packages.tel_number) AS phone_number',
                ]
            );

        $filterCompletedOrders = "main_table.order_status = '" . Dyna_Superorder_Model_Superorder::SO_COMPLETED_SUCCESS
            . "' or main_table.order_status = '" . Dyna_Superorder_Model_Superorder::SO_COMPLETED_FAILED
            . "'";

        $filterErrorOrders = "main_table.order_status = '" . Dyna_Superorder_Model_Superorder::SO_IN_PROGRESS_WITH_FAILURE . "'";

        /** @var Dyna_Customer_Model_Session $session */
        $session = Mage::getSingleton('customer/session');
        $agent = $session->getSuperAgent() ?: $session->getAgent(true);

        if ($agent->isGranted(Dyna_AgentDE_Model_Dealer::ORDER_PERMISSION_ALL)) {
            // For all permissions granted, retrieve all orders.
        } else if ($agent->isGranted(Dyna_AgentDE_Model_Dealer::ORDER_PERMISSION_TELESALES)) {
            $superOrders->getSelect()->where('main_table.created_website_id = ?', Mage::app()->getWebsite(Dyna_AgentDE_Model_Website::WEBSITE_TELESALES_CODE)->getId());
        } else if ($agent->isGranted(Dyna_AgentDE_Model_Dealer::ORDER_PERMISSION_DEALER_GROUP)) {
            $dealerGroupId = $agent->getDealer()->getDealerGroups()->getAllIds()[0];

            /** @var Dyna_AgentDE_Model_Dealer $dealerModel */
            $dealerModel = Mage::getModel('agent/dealer');
            $dealers = $dealerModel->getDealersInGroup($dealerGroupId);

            if (sizeof($dealers) > 0) {
                $superOrders->getSelect()->where('main_table.created_dealer_id IN (' . implode(",", $dealers) . ') OR main_table.created_dealer_id = ?', $agent->getDealer()->getDealerId());
            } else {
                // If the dealer is not part of a group, fetch only its own orders
                $superOrders->getSelect()->where('main_table.created_dealer_id = ?', $agent->getDealer()->getDealerId());
            }
        } else if ($agent->isGranted(Dyna_AgentDE_Model_Dealer::ORDER_PERMISSION_DEALER)) {
            $superOrders->getSelect()->where('main_table.created_dealer_id = ?', $agent->getDealer()->getDealerId());
        } else {
            // If no permissions are granted return an empty collection
            $superOrders->getSelect()->where('1=0');
        }

        switch ($clusterType) {
            case 'error':
                $superOrders->getSelect()->where($filterErrorOrders);
                break;
            case 'no-error':
                $superOrders->getSelect()->where("not (" . $filterErrorOrders . ")");
                break;
        }

        $superOrders->getSelect()->where("not (" . $filterCompletedOrders . ")");

        $superOrders->getSelect()->group('main_table.entity_id');

        $superOrders->getSelect()->order('main_table.created_at DESC');

        $superOrders->getSelect()->limit($resultsPerPage + 1, ($currentPage - 1) * $resultsPerPage);

        return $superOrders;
    }

    /**
     * Maps the orders collection data to the data needed in front end.
     *
     * @param $ordersCollection
     * @return array
     */
    public function getOrdersInfo($ordersCollection)
    {
        $finalOrders = [];
        $today = new DateTime("now");

        foreach ($ordersCollection as $order) {
            $newOrder = [
                'superOrderId' => $order->getEntityId(),
                'status' => $order->getOrderStatusTranslate(),
                'creationDate' => date("d.m.Y", strtotime($order->getCreatedAt())),
                'customerNumber' => $order->getCustomerNumber(),
                'customerFirstName' => $order->getCustomerFirstname(),
                'customerLastName' => $order->getCustomerLastname(),
                'customerCompany' => $order->getCompanyName(),
                'customerPhoneNumber' => $order->getPhoneNumber(),
                'customerCompany' => $order->getCompanyName(),
                'orderNumber' => $order->getOrderNumber(),
                'numberOfPackages' => $order->getTotalPackages(),
                'daysInStatus' => $today->diff(new DateTime(date("Y-m-d", strtotime($order->getCreatedAt()))))->days,
                'details' => [
                    'error_code' => $order->getErrorCode(),
                    'error_details' => $order->getErrorDetail()
                ],
                'packages' => [

                ]
            ];

            $newOrder['has_cable'] = $newOrder['has_fixed'] = $newOrder['has_mobile'] = false;

            $packages = Mage::getModel('package/package')->getCollection()
                ->addFieldToFilter('order_id', $order->getEntityId());

            foreach ($packages as $package) {
                $singlePackage = [];
                $singlePackage['id'] = $package->getPackageId();

                if ($package->isCable()) {
                    $newOrder['has_cable'] = true;
                    $singlePackage['type'] = 'cable';
                } else if ($package->isMobile() || $package->isPrepaid()) {
                    $newOrder['has_mobile'] = true;
                    $singlePackage['type'] = 'mobile';
                } else if ($package->isFixed()) {
                    $newOrder['has_fixed'] = true;
                    $singlePackage['type'] = 'fixed';
                }

                $singlePackage['orderType'] = ucfirst($package->getSaleType());
                $singlePackage['packageStatus'] = $this->__($package->getStatus());

                $newOrder['packages'][] = $singlePackage;
            }

            $finalOrders[] = $newOrder;
        }

        return $finalOrders;
    }

    public function getPackageSubtypes($packageType, $forConfigurator = true, $forCart = false)
    {
        // Now called directly from package content so we need to strip the dummy part from packageType
        $packageType = str_replace("template-", "", $packageType);
        // get cache key
        $key = sprintf('package_subtypes_%s_%s_%s_%s', $packageType, date('Y-m'), $forConfigurator ? "yes" : "no", $forCart ? "yes" : "no");

        if ($result = unserialize($this->getCache()->load($key))) {
            return $result;
        } else {
            $data = array();
            /** @var Dyna_Package_Model_PackageType $packageModel */
            $packageModel = Mage::getModel("dyna_package/packageType")
                ->loadByCode($packageType);

            // Get package subtypes for current package
            $subtypesCollection = $packageModel
                ->getPackageSubTypes()
                ->addOrder('front_end_position', Mage_Eav_Model_Entity_Collection_Abstract::SORT_ORDER_ASC);
            // Check if these packages are needed for configurator
            if ($forConfigurator) {
                $subtypesCollection->addFieldToFilter('package_subtype_visibility',
                    array(
                        array('finset'=> array('configurator')),
                    )
                );
            }
            // Check if these packages are needed for displaying in cart section
            if ($forCart) {
                $subtypesCollection->addFieldToFilter('package_subtype_visibility',
                    array(
                        array('finset'=> array('shopping_cart')),
                    )
                );
            }

            /** @var Dyna_Package_Model_PackageSubtype $subtype */
            // Create an array with all subtypes
            foreach ($subtypesCollection as $subtype) {
                $data[] = $subtype->getPackageCode();
            }

            // save cache
            $this->getCache()
                ->save(
                    serialize($data),
                    $key,
                    array(Dyna_Cache_Model_Cache::PRODUCT_TAG),
                    $this->getCache()->getTtl()
                );

            // return results
            return $data;
        }
    }

    /**
     * Retrieve the quote packages ordered by open/completed
     *
     * @return array
     */
    public function getPackages()
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quoteModel */
        $quoteModel = Mage::getSingleton('checkout/cart')->getQuote();
        $tempPackages = $quoteModel->getPackages();
        $packages = [];
        if ($tempPackages) {
            unset($openPackages);
            $openPackages = [];
            foreach ($tempPackages as $i => $package) {
                $packageId = $package['package_id'];
                if (!count($package['items']) || $package['current_status'] != Omnius_Configurator_Model_AttributeSet::PACKAGE_STATUS_COMPLETED) {
                    $openPackages[$packageId] = $package;
                    unset($tempPackages[$i]);
                    continue;
                }
            }
            if (count($openPackages) > 0) {
                foreach ($openPackages as $pack) {
                    array_push($tempPackages, $pack);
                }
            }
            unset($openPackages);
        }
        foreach ($tempPackages as $package) {
            /** @var Dyna_Package_Model_Package $packageModel */
            $packageModel = $quoteModel->getCartPackage($package['package_id']);
            $packageModel->setData('items', $package['items']);
            $packages[] = $packageModel;
        }

        return $packages;
    }

    /**
     * Check if the KIP from the installed base is on the same address as the address currently loaded
     * OVG-85
     * @return bool
     */
    public function customerHasServiceInstalled($serviceType)
    {
        $serviceCustomers = Mage::getSingleton('dyna_customer/session')->getServiceCustomers();
        $currentAddressId = Mage::getSingleton('dyna_address/storage')->getAddressId();

        if (!is_array($serviceCustomers) || !array_key_exists(Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD, $serviceCustomers)) {
            return false;
        }
        foreach ($serviceCustomers[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD] as $customer) {
            if (!isset($customer['contracts']) || count($customer['contracts']) == 0) {
                return false;
            }
        }


        foreach ($serviceCustomers[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD] as $customer) {
            foreach ($customer['contracts'] as $contract) {
                if (isset($contract['subscriptions']) && count($contract['subscriptions'])) {
                    foreach ($contract['subscriptions'] as $subscription) {
                        $installedServiceId = null;
                        foreach ($subscription['addresses'] as $address) {
                            if($address['address_type'] == 'service') {
                                $installedServiceId = $address['address_id'];
                            }
                        }

                        if($installedServiceId != $currentAddressId) {
                            continue;
                        }
                        if(isset($subscription['products']) && count($subscription['products'])) {
                            foreach ($subscription['products'] as $product) {
                                if(isset($product['product_category']) &&
                                    strtolower($product['product_category']) == strtolower($serviceType)) {
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    /**
     * OMNVFDE-1832
     * There should be only 1 kip at a specific address
     * @return boolean <true> if allowed, <false> if not.
     */
    public function hasKIPWhileNotAllowed()
    {
        // verify if the Agent wants to order a kip
        $hasNewKipAddedToCart = false;
        $packagesAddedInCart = $this->getPackages();
        foreach ($packagesAddedInCart as $package) {
            if(strtolower($package->getType()) == strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_INTERNET_PHONE) &&
                // allow if in CHANGE PACKAGE mode
                !$package->isInEditMode() &&
                // ignore the bundle dummy kip product that is already in the installed base
                !$package->getEditingDisabled()) {
                $hasNewKipAddedToCart = true;
            }
        }
        // if the Agent does not want to order a KIP -> the restriction does not apply
        if(!$hasNewKipAddedToCart) {
            return false;
        }
        return $this->customerHasServiceInstalled(Dyna_Customer_Model_Client_RetrieveCustomerInfo::CABLE_PRODUCT_CATEGORY_KIP);
    }

    /**
     * Overriding Omnius package totals to prevent showing negative prices to frontend
     * @see OMNVFDE-1376 Products with negative prices scenarios
     * @param null $packageId
     * @param bool $withTax
     * @param bool $quoteId
     * @return array
     */
    public function getActivePackageTotals($packageId = null, $withTax = true, $quoteId = false)
    {
        $totals = parent::getActivePackageTotals($packageId, $withTax, $quoteId);
        $totals['totalmaf'] = max($totals['totalmaf'], 0);
        return $totals;
    }

    /**
     * Returns the cardinality of the specified subtype & type
     * @param $packageTypeName
     * @param $packageSubtypeName
     * @return mixed
     */
    public function getSubtypeCardinality($packageTypeName, $packageSubtypeName)
    {
        $packageId = Mage::getModel('dyna_package/packageType')->getCollection()
            ->addFieldToFilter('package_code', $packageTypeName)
            ->getFirstItem()
            ->getEntityId();

        $subtype = Mage::getModel('dyna_package/packageSubtype')->getCollection()
            ->addFieldToFilter('type_package_id', $packageId)
            ->addFieldToFilter('package_code', $packageSubtypeName)
            ->getFirstItem();

        return $subtype->getCardinality();
    }
    /**
     * Returns the cardinality of the specified subtype & type by process context
     * @param $packageTypeName
     * @param $packageSubtypeName
     * @return mixed
     */
    public function getSubtypeCardinalityByProcessContext($packageTypeName, $packageSubtypeName, $processContextId)
    {
        $packageId = Mage::getModel('dyna_package/packageType')->getCollection()
            ->addFieldToFilter('package_code', $packageTypeName)
            ->getFirstItem()
            ->getEntityId();

        $subtypeId = Mage::getModel('dyna_package/packageSubtype')->getCollection()
            ->addFieldToFilter('type_package_id', $packageId)
            ->addFieldToFilter('package_code', $packageSubtypeName)
            ->getFirstItem()
            ->getEntityId();

        /** @var Dyna_Package_Model_PackageSubtypeCardinality $packageSubtypeCardinality */
        $packageSubtypeCardinality = Mage::getModel('dyna_package/packageSubtypeCardinality');

        return $packageSubtypeCardinality->loadBySubtypeAndContext($subtypeId, $processContextId)->getCardinality();
    }

    /**
     * Returns whether the user is able to select multiple items
     * in the same subtype & type based on its cardinality
     * @param $packageTypeName
     * @param $packageSubtypeName
     * @return bool
     */
    public function allowMultipleProductsSelected($packageTypeName, $packageSubtypeName)
    {
        $cardinality = $this->getSubtypeCardinality($packageTypeName, $packageSubtypeName);

        if (preg_match('/^(0|1)\.+(([^0-1\D]|\d{2,})|n)$/', $cardinality)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Returns whether the user is able to select multiple items
     * in the same subtype & type based on its cardinality
     * @param $packageTypeName
     * @param $packageSubtypeName
     * @return bool
     */
    public function isSectionDisabled($packageTypeName, $packageSubtypeName, $processContextId)
    {
        $cardinality = $this->getSubtypeCardinalityByProcessContext($packageTypeName, $packageSubtypeName, $processContextId);
        if (!$cardinality) {
            $cardinality = $this->getSubtypeCardinality($packageTypeName, $packageSubtypeName);
        }

        if (preg_match('/^(0|1)\.+0$/', $cardinality)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Returns the entity id of a ghost package for an installed base
     * subscription. If a ghost package does not exist for the current bundle
     * a new ghost is created and the id of this package is returned
     *
     * @param $bundleId
     * @param $subscriptionNumber
     * @param $productId
     * @param string $packageType
     * @return int|null
     */
    public function getGhostPackageId($bundleId, $subscriptionNumber, $productId, $packageType, $relatedProductSku, $sharingGroupId, $packageCreationId)
    {
        $parentPackageId = null;
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();

        $existingGhostPackage = $quote->getGhostPackageForIBSubscription($subscriptionNumber, $productId);

        if ($existingGhostPackage) {
            $parentPackageId = $existingGhostPackage->getEntityId();
        } else {
            $relatedProducts = Mage::getSingleton('customer/session')->getCustomer()
                ->getInstalledBaseProducts($subscriptionNumber, $productId, true);
            $relatedProductSku = implode(",", $relatedProducts);
            $ghostPackageId = Mage::helper('omnius_configurator/cart')
                ->initNewPackage(
                    strtolower($packageType),
                    Dyna_Checkout_Model_Sales_Quote_Item::ACQUISITION,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    $packageCreationId
                );
            $ghostPackage = $quote->getPackageById($ghostPackageId);

            // Save install base package
            /** @var Dyna_Package_Model_Package $packageModel */
            $ghostPackage
                ->setEditingDisabled(true)
                ->setCurrentStatus(Omnius_Configurator_Model_AttributeSet::PACKAGE_STATUS_COMPLETED)
                ->setCreatedAt(now())
                ->setBundleId($bundleId)
                ->setRedplusRelatedProduct($relatedProductSku)
                ->setInitialInstalledBaseProducts($relatedProductSku)
                ->setInstalledBaseProducts($relatedProductSku)
                ->setSharingGroupId($sharingGroupId)
                ->setQuoteId($quote->getId())
                ->setParentAccountNumber($subscriptionNumber)
                ->setServiceLineId($productId)
                ->save();

            $parentPackageId = $ghostPackage->getEntityId();
        }

        return $parentPackageId;
    }

    public function getFiltersPerProductType($productType, $initialFilters)
    {
        // Apply 'product_visibility' filter
        $productVisibilityAttribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', 'product_visibility');
        $sectionId = Mage::helper('omnius_catalog')->getAttributeAdminLabel($productVisibilityAttribute, null, Dyna_Catalog_Model_Product::PRODUCT_VISIBILITY_CONFIGURATOR);

        $filters = [];
        foreach ($initialFilters as $sectionFilters) {
            foreach ($sectionFilters as $filter) {
                if (isset($filter['specific_filter'])) {
                    if ($filter['specific_filter'] == $productType) {
                        unset($filter['specific_filter']);
                        $attributeValues = $filter['attributeValue'][0];
                        // operator is finset with multiple values to compare
                        if ($filter['operator']=='finset' && count($attributeValues) > 1) {
                            foreach ($attributeValues as $attributeValue) {
                                $conditions[] = [$filter['operator'] => $attributeValue];
                            }
                            $filters[$filter['attributeName']] = [$conditions];
                        } else {
                            $filters[$filter['attributeName']] = [$filter['operator'] => $filter['attributeValue']];
                        }
                    }
                }
            }
        }

        return array_merge($filters, ['product_visibility' => ["finset" => $sectionId]]);
    }

    /**
     * @param string $filters
     * eq: $filters = "redplus_role=member,owner;red;"
     * if is a pair "attribute=value1,value2" the filtering will be done on that product attribute with the given values
     * if is a simple text like "red" we will use it as a search string inn name sku and description
     * @return array
     */
    public function getSearchTxtPermanentFilters($filters)
    {
        $parsedFilters = [];
        if (!$filters) {
            return $parsedFilters;
        }

        $filtersArr = explode(";", $filters);
        foreach ($filtersArr as $filter) {
            if (!strpos($filter, "=")) {
                if($filter) {
                    if (strpos($filter, ",") !== false) {
                        $filters = explode(",", $filter);
                        foreach ($filters as $singleFilter) {
                            if($singleFilter) {
                                $parsedFilters[] = $singleFilter;
                            }
                        }
                    }
                    else {
                        $parsedFilters[] = $filter;
                    }
                }
            }
        }

        return $parsedFilters;
    }

    /**
     * Parses the filter string and returns an array of filter attributes
     * with the attribute name, operator and attribute value
     *
     * @param $filter
     * @return array
     */
    public function getFilterCondition($filter)
    {
        $operatorMapping = [
            '!=' => 'nfinset',
            '=' => 'finset'
        ];

        $filterCondition = [];

        foreach ($operatorMapping as $operator => $operatorValue) {
            if (strpos($filter, $operator) !== false) {
                /** @var Dyna_Catalog_Model_Product $productModel */
                $productModel = Mage::getModel('catalog/product');
                $attributePair = explode($operator, $filter);
                $attributeName = $attributePair[0];
                $attributeValue = $attributePair[1];
                $attribute = $productModel->getResource()->getAttribute($attributeName);

                if ($attribute) {
                    if (strpos($attributeValue, "|") !== false) {
                        // filter for multiple values (redplus_role=member,owner)
                        $attributeTxtValues = explode("|", $attributeValue);
                        if ($attribute->usesSource()) {
                            $attributeValues = [];
                            foreach ($attributeTxtValues as $attributeTxtValue) {
                                $attributeValues[] = $attribute->getSource()->getOptionId($attributeTxtValue);
                            }
                            if ($attributeValues) {
                                $filterCondition['attributeName'] = $attributeName;
                                // attr column is text and has multiple values to compare, then use finset
                                if ($attribute->getBackendType() == 'text') {
                                    $filterCondition['operator'] = $operatorValue;
                                } else {
                                    $filterCondition['operator'] = ($operatorValue == 'finset' ? 'in' : 'nin');
                                }
                                $filterCondition['condition'] = [$attributeValues];
                            }
                        }
                    } else {
                        // filter for simple values (redplus_role=member)
                        $filterCondition['attributeName'] = $attributeName;
                        $filterCondition['operator'] = $operatorValue;
                        if ($attribute->usesSource()) {
                            $filterCondition['condition'] = $attribute->getSource()->getOptionId(trim(strtolower($attributeValue)));
                        } else {
                            $filterCondition['condition'] = [$operatorValue => $attributeValue];
                        }
                    }
                }

                // exit after finding the first matching operator
                break;
            }
        }

        return $filterCondition;
    }

    public function getSubtypeFilterCondition($tmpFilters)
    {
        $additionalFilters = array();
        $filtersArr = explode("&", $tmpFilters);
        foreach ($filtersArr as $filter) {
            $currentFilter = explode('[', $filter);

            $additionalFilters[$currentFilter[0]] = trim($currentFilter[1], ']');
        }

        return $additionalFilters;
    }

    public function getAttributePairsPermanentFilters($section, $filters)
    {
        $parsedFilters = [];
        if (!$filters) {
            return $parsedFilters;
        }

        $filtersArr = explode(",", $filters);
        foreach ($filtersArr as $filter) {
            $condition = $this->getFilterCondition($filter);
            if (!empty($condition)) {
                $parsedFilters[] = [
                    'specific_filter' => $section,
                    'attributeName' => $condition['attributeName'],
                    'attributeValue' => $condition['condition'],
                    'operator' => $condition['operator'],
                    'string_filter' => $filter
                ];
            }
        }
        return $parsedFilters;
    }

    public function getArtifactPath() : string
    {
        return sprintf('%s/%s', Mage::getBaseDir(), Dyna_Configurator_Helper_Rules_Cable::ARTIFACT_PATH);
    }
}
