<?php

class Dyna_Configurator_Helper_UCT extends Mage_Core_Helper_Abstract
{
    public function isUctFlow()
    {
        $session = Mage::getSingleton('dyna_customer/session');
        $uctParams = $session->getUctParams();

        return !empty($uctParams);
    }

    /**
     * Once a campaign is selected, we are no longer in an UCT flow
     */
    public function exitUctFlow()
    {
        $session = Mage::getSingleton('dyna_customer/session');
        $session->setUctParams(null);
    }
}