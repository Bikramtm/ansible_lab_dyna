<?php

/**
 * Class Dyna_Configurator_Helper_Cart
 */
class Dyna_Configurator_Helper_Cart extends Omnius_Configurator_Helper_Cart
{
    const MIN_MAX_EQL_OPERATIONS = array(
        Dyna_ProductMatchRule_Model_Rule::OP_MIN_N,
        Dyna_ProductMatchRule_Model_Rule::OP_MAX_N,
        Dyna_ProductMatchRule_Model_Rule::OP_EQL_N
    );

    const DUPLICABLE_PACKAGES = array(
        Omnius_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE,
        Omnius_Configurator_Model_PackageType::PACKAGE_TYPE_PREPAID,
    );

    // stores as a string (ex: "soc1,soc2,soc3") all initial socs that are found on the contract that is now in the flow for ILS
    protected $ilsInitialSocs = "";
    // stores the permanent filters for this package (ex: redplus_role=member,role;red,young;)
    protected $permanentFilters = "";
    private $sourceCollection = null;

    protected $rulesDebug = false;
    protected $fauxProcessContextId = null;

    /**
     * @param $productIds
     * @param array $allProducts
     * @param null $websiteId
     * @param bool $cacheCollections
     * @param null $packageType
     * @param bool $skipServiceRules
     * @return array
     */
    public function getCartRules($productIds, $allProducts = array(), $websiteId = null, $cacheCollections = false, $packageType = null, $skipServiceRules = false)
    {
        sort($productIds);
        sort($allProducts);
        $debugRules = Mage::helper('dyna_productmatchrule')->isDebugMode();

        if ($debugRules) {
            Mage::log("getCartRules called on action: " . Mage::app()->getFrontController()->getAction()->getFullActionName(), null, 'ServiceExpressionRules.log');
            Mage::log("Current source collection found in registry: " . Mage::registry('rules_source_collection'), null, 'ServiceExpressionRules.log');
        }

        $websiteId = $websiteId ?: Mage::app()->getStore()->getWebsiteId();
        /** @var Dyna_Catalog_Helper_ProcessContext $processContextHelper */
        $processContextHelper = Mage::helper("dyna_catalog/processContext");
        $processContextId = $processContextHelper->getProcessContextId();

        $sourceCollectionId = $this->getCurrentSourceCollection();

        $cache_key = sprintf('get_cart_new_rules_%s_%s_%s_%s_%s_%s',
            md5(serialize($productIds)),
            md5(serialize($allProducts)),
            $websiteId,
            $packageType,
            $processContextId,
            $sourceCollectionId);
        $notInitiallySelectable = $this->getNotInitiallySelectableProducts($packageType);

        if ($debugRules || !$cacheCollections) {
            $result = false;
        } else {
            $result = unserialize($this->getCache()->load($cache_key));
            if ($debugRules) {
                Mage::log("[CACHED] Normal allowed rules: ", null, 'ServiceExpressionRules.log');
                Mage::log(json_encode($result), null, 'ServiceExpressionRules.log');
            }
        }

        if (!Mage::registry("no_updated_for_expression_helper_needed")) {
            /** @var Dyna_Configurator_Helper_Expression $serviceHelper */
            $serviceHelper = Mage::helper('dyna_configurator/expression');
            $serviceHelper->updateInstance(Mage::getSingleton('checkout/cart')->getQuote());
        }

        if (!$result) {
            $skippedProducts = [];
            if ($packageType) {
                $productIds = $this->handleSkippedSubtypes($packageType, $productIds, $skippedProducts);
            }

            if (count($productIds)) {
                if ($debugRules) {
                    Mage::log("Getting Omnius allowed rules for the following input ids (after removing the ones with skipped package subtype): ", null, 'ServiceExpressionRules.log');
                    Mage::log(json_encode($productIds), null, 'ServiceExpressionRules.log');
                }

                $available = [];
                $storeId = $websiteId ? Mage::app()->getWebsite($websiteId)->getDefaultStore()->getId() : Mage::app()->getStore()->getId();
                $catalogResource = Mage::getResourceModel('catalog/product');
                $subtypeAttribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', Dyna_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR);
                $rawAttributesValues = [];
                foreach ($productIds as $id) {
                    $available[$id] = [];
                    $rule = $this->getRulesForId($websiteId, $id, $catalogResource, $storeId, $rawAttributesValues, $subtypeAttribute, null, null, $packageType);
                    if ($debugRules) {
                        Mage::log("Got the following compatible products for [" . $id . "]: ", null, 'ServiceExpressionRules.log');
                        Mage::log(json_encode($rule), null, 'ServiceExpressionRules.log');
                    }
                    if (!empty($rule['whitelist'])) {
                        $available[$id] = $rule['whitelist'];
                    }

                    if (!empty($rule['options'])) {
                        $notInitiallySelectable = array_diff($notInitiallySelectable, $rule['options']);
                    }
                }

                $result_new = [];
                if (!empty($available)) {
                    $result_new = array_pop($available);
                    foreach ($available as $items) {
                        $result_new = array_intersect($result_new, $items);
                    }
                }

                if (!$result_new) {
                    $result = [];
                } else {
                    $result = array_values($result_new);
                    $result = array_diff($result, $notInitiallySelectable);
                }
                $result = array_merge($result, $skippedProducts);

                if ($debugRules) {
                    Mage::log("[NOT CACHED] Normal allowed rules: ", null, 'ServiceExpressionRules.log');
                    Mage::log(json_encode($result), null, 'ServiceExpressionRules.log');
                    Mage::log("No initial selectable: ", null, 'ServiceExpressionRules.log');
                    Mage::log(json_encode($notInitiallySelectable), null, 'ServiceExpressionRules.log');
                }
            } else {
                if (in_array(strtolower($packageType), Dyna_Catalog_Model_Type::getMobilePackages())) {
                    // not forwarding all products because this is Omnius legacy logic for calling rules without session (in order to be cached in varsnish) which is not suitable for DE
                    $result = $this->getAllRulesForProducts(array(), $websiteId, $packageType);
                    $result = array_diff($result, $notInitiallySelectable);
                } else {
                    $result = [];
                }
            }

            $this->getCache()->save(serialize($result), $cache_key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
        }

        // Appending service expression rules to result (not cached because these depend on customer and serviceability conditions)

        if (!$skipServiceRules) {
            /** @var Dyna_ProductMatchRule_Model_Rule $ruleModel */
            $ruleModel = Mage::getSingleton('dyna_productmatchrule/rule');
            $allowedServiceRules = $ruleModel->loadAllowedServiceRules($productIds, $websiteId, $packageType);
            $notAllowedServiceRules = $ruleModel->loadAllowedServiceRules($productIds, $websiteId, $packageType, true);

            if (count($notAllowedServiceRules)) {
                foreach ($result as $key => &$id) {
                    if (isset($notAllowedServiceRules[$id])) {
                        // remove products from static rules if found in service not allowed
                        unset($result[$key]);
                        continue;
                    }
                }

                if ($debugRules) {
                    Mage::log("Not allowed service rules: ", null, 'ServiceExpressionRules.log');
                    Mage::log(json_encode($notAllowedServiceRules), null, 'ServiceExpressionRules.log');

                    Mage::log("Normal allowed rules after service not allowed: ", null, 'ServiceExpressionRules.log');
                    Mage::log(json_encode($result), null, 'ServiceExpressionRules.log');
                }
            }

            if ($debugRules) {
                Mage::log("Service allowed rules: ", null, 'ServiceExpressionRules.log');
                Mage::log(json_encode(array_keys($allowedServiceRules)), null, 'ServiceExpressionRules.log');
            }

            $result = array_merge($result, array_keys($allowedServiceRules));
        }

        return array_map('strval', $result);
    }

    /**
     * Retrieves all available product ids based on the website id.
     * @param null $websiteId
     * @param null $packageType
     * @return mixed
     * @throws Mage_Core_Exception
     * @throws Zend_Db_Statement_Exception
     */
    public function getAllRules($websiteId = null, $packageType = null)
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $activePackage = $quote->getActivePackage();
        $processContextSetIds = null;

        if ($this->fauxProcessContextId) {
            $connection = Mage::getSingleton('core/resource')->getConnection('core/read');
            $processContextSetIds = $connection->query("select entity_id from product_match_whitelist_index_process_context_set where find_in_set({$this->fauxProcessContextId}, process_context_ids)")->fetchAll(\PDO::FETCH_COLUMN);
        }
        $processContextSetIds = (isset($processContextSetIds) && $processContextSetIds) ? implode(',', $processContextSetIds) : null;

        $sourceCollectionId = $this->getCurrentSourceCollection();

        $websiteId = $websiteId ? $websiteId : Mage::app()->getWebsite()->getId();
        $key = sprintf('product_all_rules_%s_%s_%s_%s',
            $websiteId,
            $packageType,
            $sourceCollectionId,
            $processContextSetIds);

        if (!($result = unserialize($this->getCache()->load($key)))) {
            if ($activePackage && strtolower($activePackage->getSaleType()) != strtolower(Dyna_Catalog_Model_ProcessContext::ACQ)
                && strtolower($packageType) == strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE)) {

                /** @var Dyna_Package_Model_PackageType $packageTypeModel */
                $packageTypeModel = Mage::getModel('dyna_package/packageType');
                $packageTypeId = $packageTypeModel->getPackageTypeIdByCode($packageType);

                $sql1 = 'SELECT DISTINCT(source_product_id) as product_id
                        FROM product_match_whitelist_index
                        WHERE `website_id` = :website_id AND `package_type` = :package_type 
                        ' . ($processContextSetIds ? 'AND `product_match_whitelist_index_process_context_set_id` in (' . $processContextSetIds . ')' : '') . '
                        AND `source_collection` = :source_collection_id';
                $sql2 = 'SELECT DISTINCT(target_product_id) as product_id
                        FROM product_match_whitelist_index
                        WHERE `website_id` = :website_id AND `package_type` = :package_type 
                        ' . ($processContextSetIds ? 'AND `product_match_whitelist_index_process_context_set_id` in (' . $processContextSetIds . ')' : '') . '
                        AND `source_collection` = :source_collection_id';

                $binds = array(
                    ':website_id' => $websiteId,
                    ':package_type' => $packageTypeId,
                    ':source_collection_id' => $sourceCollectionId,
                );

                /** @var Varien_Db_Adapter_Interface $readConnection */
                $readConnection = Mage::getSingleton('core/resource')->getConnection('core_read');
                $result = $readConnection->query($sql1, $binds)->fetchAll(Zend_Db::FETCH_COLUMN);
                $result = array_merge($result, $readConnection->query($sql2, $binds)->fetchAll(Zend_Db::FETCH_COLUMN));
                $result = array_unique($result);
            } else {
                $result = Mage::getModel('catalog/product')->getCollection()->getPackageTypeProductIds($packageType, $websiteId);
            }
            $this->getCache()->save(serialize($result), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
        }

        return $result;
    }

    /**
     * Check if the product packageSubtype is defined to be skipped for checking the compatibility rules
     * @param string $packageType
     * @param array $productIds
     * @param array $skippedProducts
     * @return bool|mixed
     */
    public function handleSkippedSubtypes($packageType, $productIds, &$skippedProducts)
    {
        //remove skipped products from array
        $skippedIdsKey = "SKIPPED_PRODUCT_IDS_BY_PACKAGE_TYPE" . md5(implode(",", $productIds)) . $packageType;
        if (!$serializedResult = $this->getCache()->load($skippedIdsKey)) {
            $packageSubTypesSkipped = Mage::getModel("dyna_package/packageSubtype")->getSkippedSubTypes($packageType);
            if (count($packageSubTypesSkipped) && count($productIds)) {
                $skippedByCompatibilityRules = Mage::getResourceModel('catalog/product_collection')
                    ->addAttributeToFilter('entity_id', $productIds);

                foreach ($skippedByCompatibilityRules as $skippedProduct) {
                    if (in_array($skippedProduct->getPackageSubtype(), $packageSubTypesSkipped)) {
                        $skippedProducts[] = $skippedProduct->getId();
                        $skipKey = array_search($skippedProduct->getId(), $productIds);
                        unset($productIds[$skipKey]);
                    }
                }
            }

            $cacheResult = array(
                'productIds' => $productIds,
                'skippedProducts' => $skippedProducts,
            );

            $this->getCache()->save(serialize($cacheResult), $skippedIdsKey, array($this->getCache()::PRODUCT_TAG), $this->getCache()->getTtl());

        } else {
            $cache = unserialize($serializedResult);
            $productIds = $cache['productIds'];
            $skippedProducts = $cache['skippedProducts'];
        }

        return $productIds;
    }

    /**
     * @return array
     */
    public function emptyCart()
    {
        Mage::helper('dyna_customer')->clearMigrationData();
        Mage::getSingleton('dyna_customer/session')->unsActiveBundleId();

        try {
            Mage::helper('pricerules')->decrementAll($this->_getCart()->getQuote());

            Mage::dispatchEvent('configurator_cart_empty', [
                'quote' => $this->_getCart()->getQuote()
            ]);

            Mage::helper('omnius_checkout')->createNewQuote();

            $response = array(
                'error' => false,
                'message' => $this->__('Cart cleared'),
            );
        } catch (Mage_Core_Exception $exception) {
            $response = array(
                'error' => true,
                'message' => $this->__($exception->getMessage()),
            );
        } catch (Exception $exception) {
            $response = array(
                'error' => true,
                'message' => $this->__('Cannot update shopping cart.'),
            );
        }
        return $response;
    }

    /**
     * @return array
     */
    public function removePackagesWithServiceability()
    {
        /** @var Dyna_Package_Helper_Data $packageHelper */
        $packageHelper = Mage::helper('dyna_package');

        // Confirmation needed for renewing service address because it will delete all packages
        if ($packageHelper->hasServiceAbilityRequiredPackages()) {

            /** @var Dyna_Checkout_Model_Sales_Quote $quote */
            $quote = Mage::getSingleton('checkout/cart')->getQuote();

            foreach ($quote->getCartPackages() as $package) {
                // if current package has a service address and is not a dummy package, then remove it
                if (($package->getServiceAddressId() || $package->getServiceAddressData()) && (!$package->getEditingDisabled())) {
                    $removedPackageIds[] = (int) $package->getPackageId();

                    $packageHelper->removePackage(null, null, $package->getId());
                }
            }

            // collect totals
            Mage::getSingleton('checkout/cart')->save();

            $response = [
                'error' => false,
                'message' => '',
                'removedPackageIds' => $removedPackageIds
            ];

            return $response;
        }
    }

    /**
     * Retrieve shopping cart model object
     *
     * @return Mage_Checkout_Model_Cart
     */
    protected function _getCart()
    {
        return Mage::getSingleton('checkout/cart');
    }

    /**
     * @param $websiteId
     * @param $id
     * @param $catalogResource
     * @param $storeId
     * @param $rawAttributesValues
     * @param $attribute
     * @param $deviceCollection
     * @param $subsCollection
     * @param $packageType
     * @return array
     */
    protected function getRulesForId($websiteId, $id, $catalogResource=null, $storeId = null, &$rawAttributesValues =null, $attribute =null, $deviceCollection = null, $subsCollection = null, $packageType = "")
    {
        $rulesSource = $this->getRuleById($id, $websiteId, $packageType);

        $targetProductIds = [];
        $sourceProductIds = [];
        $options = [];

        foreach ($rulesSource as &$row) {
            $targetProductIds[$row['target_product_id']] = $row['target_product_id'];
            $sourceProductIds[$row['source_product_id']] = $row['source_product_id'];

            if (!empty($row['override_initial_selectable']) && $row['source_product_id'] == $id) {
                $options[$row['target_product_id']] = $row['target_product_id'];
                $options[$row['source_product_id']] = $row['source_product_id'];
            }
        }

        $rule = array_unique(
            array_merge(
                $targetProductIds,
                $sourceProductIds
            )
        );

        return ['whitelist' => $rule, 'options' => $options];
    }

    /**
     * Retrieves all product match rules in which the product is available.
     * @param $id
     * @param $websiteId
     * @param string $packageType
     * @return mixed
     * @throws Mage_Core_Exception
     * @throws Mage_Core_Model_Store_Exception
     * @throws Zend_Db_Adapter_Exception
     * @throws Zend_Db_Statement_Exception
     */
    protected function getRuleById($id, $websiteId = null, $packageType = "")
    {
        $websiteId = $websiteId ?: Mage::app()->getStore()->getWebsiteId();
        /** @var Dyna_Catalog_Helper_ProcessContext $processContextHelper */
        $processContextHelper = Mage::helper("dyna_catalog/processContext");
        $processContextSetIds = $processContextHelper->getProcessContextSetIds();
        $processContextSetIds = $processContextSetIds ? implode(',', $processContextSetIds) : null;

        $sourceCollectionId = $this->getCurrentSourceCollection();
        $debug = Mage::helper('dyna_productmatchrule')->isDebugMode();

        $key = sprintf('%s_product_mix_match_white_rules_%s_%s_%s_%s',
            $id,
            $websiteId,
            $packageType,
            $processContextSetIds,
            $sourceCollectionId);

        if (!$debug && $result = $this->getCache()->load($key)) {
            return unserialize($result);
        } else {
            // Load package type
            /** @var Dyna_Package_Model_PackageType $packageTypeModel */
            $packageTypeModel = Mage::getModel('dyna_package/packageType')
                ->loadByCode($packageType);

            if (!$packageTypeModel->getId()) {
                Mage::throwException("Cannot determine the package for which combination rules need to be retrieved.");
            }
            $packageType = $packageTypeModel->getId();

            $adapter = Mage::getSingleton('core/resource');
            /** @var Magento_Db_Adapter_Pdo_Mysql $connection */
            $connection = $adapter->getConnection('core_read');

            $sql1 = "SELECT product_match_whitelist_index.source_product_id, product_match_whitelist_index.target_product_id, product_match_whitelist_index.override_initial_selectable FROM product_match_whitelist_index 
                        WHERE `website_id` = :website_id AND `package_type` = :package_type AND `source_product_id` = :id 
                        " . ($processContextSetIds ? 'AND `product_match_whitelist_index_process_context_set_id` in (' . $processContextSetIds . ')' : '') . "
                        AND `source_collection` = :source_collection_id";
            $sql2 = "SELECT product_match_whitelist_index.source_product_id, product_match_whitelist_index.target_product_id, product_match_whitelist_index.override_initial_selectable FROM product_match_whitelist_index 
                        WHERE 
                        `website_id` = :website_id AND `package_type` = :package_type AND `target_product_id` = :id 
                        " . ($processContextSetIds ? 'AND `product_match_whitelist_index_process_context_set_id` in (' . $processContextSetIds . ')' : '') . "
                       AND `source_collection` = :source_collection_id";

            $binds = array(
                ':website_id' => $websiteId,
                ':package_type' => $packageType,
                ':id' => $id,
                ':source_collection_id' => $sourceCollectionId
            );

            if ($debug) {
                Mage::log("INSIDE THE HEART OF GET RULES [" . $id . "]: ", null, 'ServiceExpressionRules.log');
                Mage::log($sql1, null, 'ServiceExpressionRules.log');
                Mage::log($sql2, null, 'ServiceExpressionRules.log');
                Mage::log($binds, null, 'ServiceExpressionRules.log');
            }

            $result = $connection->query($sql1, $binds)->fetchAll(\PDO::FETCH_ASSOC);
            $result = array_merge_recursive($result, $connection->query($sql2, $binds)->fetchAll(\PDO::FETCH_ASSOC));

            $this->getCache()->save(serialize($result), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());

            return $result;
        }
    }

    /**
     * @param $allProducts
     * @param $websiteId
     * @return array|mixed
     */
    protected function getAllRulesForProducts($allProducts, $websiteId, $packageType = null)
    {
        // If no products exists, return all product ids
        $prodIds = [];
        if (is_array($allProducts) && count($allProducts)) {
            foreach ($allProducts as &$typeProdIds) {
                foreach ($typeProdIds as &$prod) {
                    if (isset($prod['entity_id'])) {
                        $prodIds[] = $prod['entity_id'];
                    }
                }
            }
        } else {
            $prodIds = $this->getAllRules($websiteId, $packageType);
        }

        return $prodIds;
    }

    /**
     * Returns the defaulted obligated (auto added to cart) products for current cart content
     * @param $quote
     * @param $packageId
     * @return array with product ids
     * @internal param array $selectedProducts with product ids
     * @internal param null $websiteId
     */
    public function getDefaultedObligatedProductsFromCart($quote, $packageId)
    {
        $selectedProducts = $quote->getItemsCollection();
        /** Defaulted products are appended to the current list of products */
        $defaultedObligatedProducts = [];
        foreach ($selectedProducts as $item) {
            // Retrieve all defaulted products for current product
            /** @var $defaultedProducts Dyna_ProductMatchRule_Model_Defaulted */
            if ($item->getPackageId() == $packageId && (bool)$item->getIsObligated()) {
                $defaultedObligatedProducts[] = $item->getData('product_id');
            }
        }

        return $defaultedObligatedProducts;
    }
    /**
     * Returns the defaulted (auto added to cart) products for current cart content
     * @param $quote
     * @param $packageId
     * @return array with product ids
     * @internal param array $selectedProducts with product ids
     * @internal param null $websiteId
     */
    public function getDefaultedProductsFromCart($quote, $packageId)
    {
        $selectedProducts = $quote->getItemsCollection();
        /** Defaulted products are appended to the current list of products */
        $defaultedProducts = [];
        foreach ($selectedProducts as $item) {
            // Retrieve all defaulted products for current product
            /** @var $defaultedProducts Dyna_ProductMatchRule_Model_Defaulted */
            if ($item->getPackageId() == $packageId && (bool)$item->getIsDefaulted()) {
                $defaultedProducts[] = $item->getData('product_id');
            }
        }

        return $defaultedProducts;
    }

    /**
     * @param string $ilsInitialSocs
     * stores as a string (ex: "soc1,soc2,soc3") all initial soc that are found on the contract that is now in the flow for ILS
     */
    public function setNewPackageIlsInitialSocs($ilsInitialSocs)
    {
        $this->ilsInitialSocs = $ilsInitialSocs;
    }

    /**
     * @param string $permanentFilters
     * stores the permanent filters for this package (ex: redplus_role=member,role;red,young;
     */
    public function setNewPackagePermanentFilters($permanentFilters)
    {
        $this->permanentFilters = $permanentFilters;
    }

    /**
     * Creates a mew package and sets it as active package in the current quote
     * @param string $type
     * @param string $saleType
     * @param null $ctn
     * @param null $currentProducts
     * @param array $oneOffDealOptions
     * @param null $parentPackageId
     * @param null $relatedProductSku
     * @param null $bundleId
     * @return mixed
     */
    public function initNewPackage($type, $saleType, $ctn = null, $currentProducts = null, $oneOffDealOptions = array(), $parentPackageId = null, $relatedProductSku = null, $bundleId = null, $packageCreationTypeId = null, $offerId = null)
    {
        /** @var Dyna_Configurator_Helper_Rules $rulesHelper */
        $rulesHelper = Mage::helper('dyna_configurator/rules');

        // always clear cache for new package
        $rulesHelper->cacheSave(null, $rulesHelper->getCacheKey($type));

        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $quoteId = $quote->getId();
        $newPackageId = Omnius_Configurator_Model_CartObserver::getNewPackageId($quote);
        /** @var Dyna_Customer_Model_Session $customerSession */
        $customerSession = Mage::getSingleton('customer/session');

        if (!$quoteId) {
            $quote->setCustomerId($customerSession->getId());
        }

        $quote->setActivePackageId($newPackageId);
        $quote->save();

        if (!$quoteId) {
            Mage::getSingleton('checkout/cart')->getCheckoutSession()->setQuoteId($quote->getId());
        }

        // Create the new package for the quote or update if already created
        /** @var Dyna_Package_Model_Package $packageModel */
        $packageModel = Mage::getModel('package/package')->getCollection()
            ->addFieldToFilter('quote_id', $quote->getId())
            ->addFieldToFilter('package_id', $newPackageId)
            ->getFirstItem();

        // If package creation type is not provided, try to select it by the package type
        if ($packageCreationTypeId === null) {
            $packageCreationTypeId = $this->getCreationTypeId(Mage::getModel('catalog/type')->getCreationTypesMapping()[strtolower($type)]);

            if (!$packageCreationTypeId) {
                Mage::throwException("Cannot determine the package creation type.");
            }
        }

        if (!$packageModel->getId()) {
            // Cannot create new serviceability dependent package without a service address id
            $serviceAddressId = Mage::getSingleton('dyna_address/storage')->getAddressId();

            if (!in_array($type, Dyna_Catalog_Model_Type::getMobilePackages())) {
                $packageModel->setServiceAddressId($serviceAddressId);
                $packageModel->setServiceAddressData(json_encode(
                        array_merge(
                            array('area_code' => Mage::getSingleton('dyna_address/storage')->getAreaCode()),
                            array('asb' => Mage::getSingleton('dyna_address/storage')->getAsb()),
                            Mage::getSingleton('dyna_address/storage')->getServiceAddressParts(),
                            array('id' => $serviceAddressId))
                    )
                );
            }

            $packageModel->setQuoteId($this->_getQuote()->getId())
                ->setPackageId($newPackageId)
                ->setType($type)
                ->setSaleType($saleType)
                ->setCtn($ctn)
                ->setCurrentProducts($currentProducts)
                ->setParentId($parentPackageId)
                ->setRedplusRelatedProduct($relatedProductSku)
                ->setPackageCreationTypeId($packageCreationTypeId)
                ->setBundleId($bundleId)
                ->takeServicesSnapshot();
        }

        $redplusCreationTypeId = $this->getCreationTypeId(Dyna_Catalog_Model_Type::CREATION_TYPE_REDPLUS);
        if ($packageCreationTypeId == $redplusCreationTypeId) {
            // get package creation type
            $typeModel = Mage::getModel('dyna_package/packageCreationTypes');
            $packageCreationType = $typeModel->getById($redplusCreationTypeId);
            //get permanent filters for the red+ member package
            $filters = $packageCreationType->getFilterAttributes();
            $packageModel->setPermanentFilters($filters);
        }

        if($this->ilsInitialSocs) {
            $packageModel->setIlsInitialSocs($this->ilsInitialSocs);
        }
        if($this->permanentFilters) {
            $packageModel->setPermanentFilters($this->permanentFilters);
        }

        if (!empty($oneOffDealOptions)) {
            $packageModel->setOneOfDeal(true);
            $quote->setItemsQty(count(explode(',', $ctn)));
            $packageModel->setRetainable($oneOffDealOptions['retainable']);
        }
        $uctParams = $customerSession->getUctParams();

        if ($uctParams && !empty($uctParams['transactionId']) && !empty($uctParams['campaignId']) && $offerId) {
            if ($offerId) {
                $uctParams['offerId'] = $offerId;
                $customerSession->setUctParams($uctParams);
                $packageModel->setOfferId($offerId);
            }
        }
        $packageModel->save();

        return $newPackageId;
    }

    /**
     * Produces Min/Max/Eql warning messages for current cart
     * @param $cartProductsHash
     * @return array
     * @internal param int $packageId
     * @internal param string $section
     * @internal param array $productsInSection
     * @internal param array $updatedSectionProducts
     */
    public function getMinMaxEqlWarnings($cartProductsHash)
    {
        $cache_key = sprintf('min_max_eql_rules_for_quote_items_%s', $cartProductsHash);
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getModel('checkout/session')->getQuote();
        $packages = $quote->getCartPackages(true);
        $warningMessages = [];
        if ($response = $this->getCache()->load($cache_key)) {
            $warningMessages = unserialize($response);
        } else {
            $warnings
                = $cartProducts
                = $cartCategories
                = $eachProductCategories
                = $ruleRightProductIds
                = [];

            foreach ( $packages as $packageId => $package ) {
                $packageItems = $package->getItems();
                $cartProducts[$packageId] = [];
                $cartCategories[$packageId] = [];
                $cartCategoriesProducts[$packageId] = [];

                foreach ($packageItems as $item) {
                    $productId = intval($item->getProductId());
                    if ( !isset($eachProductCategories[$productId]) ) {
                        $eachProductCategories[$productId] = Mage::getResourceModel("catalog/product")->getCategoryIdsWithAnchors($item->getProduct());
                    }

                    if (!($item->getProduct()->isPromo() || $item->isBundlePromo()) && !in_array($productId, $cartProducts[$packageId])) {
                        $cartProducts[$packageId][$item->getSku()] = $productId;
                        $productCategoriesQty = array_fill_keys($eachProductCategories[$productId], $item->getQty());
                        foreach (array_unique(array_keys($cartCategories[$packageId] + $productCategoriesQty)) as $key) {
                            $cartCategories[$packageId][$key] = (isset($cartCategories[$packageId][$key]) ? $cartCategories[$packageId][$key] : 0) + (isset($productCategoriesQty[$key]) ? $productCategoriesQty[$key] : 0);
                            if (in_array($key, $item->getProduct()->getCategoryIds())) {
                                $cartCategoriesProducts[$packageId][$key][$item->getProductId()] = $item->getSku() . '_' . $item->getName();
                            }
                        }
                    }
                }
            }

            $ruleCollection = $this->getRulesForProductsInCart($cartProducts, $eachProductCategories, $packages ?? null);

            foreach ($ruleCollection as $packageId => $packageRules) {
                $warnings[$packageId] = [];
                foreach ($packageRules as $rule) {
                    $ruleId = $rule['product_match_rule_id'];
                    $ruleRightId = $rule['right_id'];
                    $ruleOperation = $rule['operation'];
                    $ruleOperationValue = intval($rule['operation_value']);
                    $ruleOperationType = $rule['operation_type'];

                    $targetType = 'category';
                    if (array_key_exists($ruleRightId, $cartCategories[$packageId])) {
                        $cartCategories[$packageId][$ruleRightId] = $cartCategories[$packageId][$ruleRightId] ?: 0;
                    } else {
                        $cartCategories[$packageId][$ruleRightId] = 0;
                    }

                    switch ($ruleOperationType) {
                        case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_P2S:
                        case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_C2S:
                        case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_S2S:
                            $ruleRightProductIds = $rule['right_id'];
                            /** @var Dyna_Catalog_Model_Product $productModel */
                            $productModel = Mage::getModel('catalog/product');
                            // convert all product ids to skus
                            $targetProductsSkusArray = $productModel->getSkusByIds($ruleRightProductIds);
                            $productsCount = 0;
                            $rightProductIdsInCartToSkus = [];
                            $cartProductIdsToSkus = array_flip($cartProducts[$packageId]);
                            foreach ($ruleRightProductIds as $ruleRightProductId) {
                                if (in_array($ruleRightProductId, $cartProducts[$packageId])) {
                                    unset($targetProductsSkusArray[$ruleRightProductId]);
                                    $rightProductIdsInCartToSkus[$ruleRightProductId] = $cartProductIdsToSkus[$ruleRightProductId];
                                    $productsCount++;
                                }
                            }
                            $targetType = 'service';
                        break;
                        case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_S2P:
                        case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_P2P:
                        case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_C2P:
                            /** @var Varien_Db_Adapter_Interface $adapter */
                            $adapter = Mage::getSingleton('core/resource')->getConnection('core_read');
                            $select = $adapter->select()->from('catalog_category_product', array('category_id'))->where('product_id = :product_id');
                            $bind = array('product_id' => $rule['right_id']);
                            // @todo in case of target=product rules we should get the family category they are part of
                            $ruleRightId = array_shift($adapter->fetchCol($select, $bind));
                            $ruleRightProductId = $rule['right_id'];
                            $targetType = 'product';
                            break;
                    }

                    switch ($ruleOperation) {
                        case Dyna_ProductMatchRule_Model_Rule::OP_MIN_N:
                            if ($targetType == 'category' && array_key_exists($ruleRightId, $cartCategories[$packageId])) {
                                $qty = $ruleOperationValue - $cartCategories[$packageId][$ruleRightId];
                                $warnings[$packageId][$ruleId][$targetType][$ruleRightId] = $qty > 0 ? $qty : 0;
                            } elseif ($targetType == 'product') {
                                if (in_array($ruleRightProductId, $cartProducts[$packageId])) {
                                    $warnings[$packageId][$ruleId][$targetType][$ruleRightProductId] = 0;
                                } else {
                                    $warnings[$packageId][$ruleId][$targetType][$ruleRightProductId] = 1;
                                }
                            } elseif ($targetType == 'service') {
                                $qty = $ruleOperationValue - $productsCount;
                                $warnings[$packageId][$ruleId][$targetType][$ruleRightId] = $qty > 0 ? $qty : 0;
                            }
                            break;
                        case Dyna_ProductMatchRule_Model_Rule::OP_MAX_N:
                            if ($targetType == 'category' && array_key_exists($ruleRightId, $cartCategories[$packageId])) {
                                $qty = $ruleOperationValue - $cartCategories[$packageId][$ruleRightId];
                                $warnings[$packageId][$ruleId][$targetType][$ruleRightId] = $qty < 0 ? $qty : 0;
                            } elseif ($targetType == 'product') {
                                $warnings[$packageId][$ruleId][$targetType][$ruleRightProductId] = in_array($ruleRightProductId, $cartProducts[$packageId]) ? 0 : 1;
                            } elseif ($targetType == 'service') {
                                $qty = $ruleOperationValue - $productsCount;
                                $warnings[$packageId][$ruleId][$targetType][$ruleRightId] = $qty < 0 ? $qty : 0;
                            }
                            break;
                        case Dyna_ProductMatchRule_Model_Rule::OP_EQL_N:
                            if ($targetType == 'category' && array_key_exists($ruleRightId, $cartCategories[$packageId])) {
                                $warnings[$packageId][$ruleId][$targetType][$ruleRightId] = $ruleOperationValue - $cartCategories[$packageId][$ruleRightId];
                            } elseif ($targetType == 'product') {
                                $warnings[$packageId][$ruleId][$targetType][$ruleRightProductId] = in_array($ruleRightProductId, $cartProducts[$packageId]) ? 0 : 1;
                            } elseif ($targetType == 'service') {
                                $warnings[$packageId][$ruleId][$targetType][$ruleRightId] = $ruleOperationValue - $productsCount;
                            }
                            break;
                    }
                }
            }

            foreach ($packages as $packageModel) {
                $packageId = $packageModel->getPackageId();
                $packageName = Mage::getModel('dyna_package/packageType')->loadByCode($packageModel->getType())->getFrontEndName();
                if (!empty($packageId) && !empty($warnings[$packageId])) {
                    foreach ($warnings[$packageId] as $warningRule => $w) {
                        $warningType = array_keys($w)[0];
                        $wid = array_keys($w[$warningType])[0];
                        $wQty = $w[$warningType][$wid];
                        if (isset($cartCategoriesProducts[$packageId][$wid])) {
                            $ruleOperationValue = count($cartCategoriesProducts[$packageId][$wid]) - abs($wQty);
                        }
                        if ($wQty != 0) {
                            if ($warningType == 'category') {
                                if ($wQty > 0) {
                                    $message = 'Add %d item' . (abs($wQty) == 1 ? '' : 's') . ' from %s category in Package %d: %s.';
                                    $message = Mage::helper('dyna_configurator')->__($message,
                                        abs($wQty),
                                        Mage::getModel('catalog/category')->load($wid)->getName(),
                                        $packageId,
                                        $packageName
                                    );
                                } else {
                                    $message = 'It is only possible to select %d from the following list of selected products: %s';
                                    $message = Mage::helper('dyna_configurator')->__($message,
                                        $ruleOperationValue,
                                        implode(', ', $cartCategoriesProducts[$packageId][$wid])
                                    );
                                }
                            } elseif ($warningType == 'product') {
                                $message = ($wQty < 0 ? 'Remove' : 'Add') . ' item %s from Package %d: %s.';
                                $message = Mage::helper('dyna_configurator')->__($message,
                                    Mage::getModel('catalog/product')->load($wid)->getName(),
                                    $packageId,
                                    $packageName
                                );
                            } elseif ($warningType == 'service') {
                                if ($wQty > 0) {
                                    $message = 'Add %d item' . (abs($wQty) == 1 ? '' : 's') . ' from %s in Package %d: %s.';
                                    $message = Mage::helper('dyna_configurator')->__($message,
                                        abs($wQty),
                                        implode(', ', $targetProductsSkusArray),
                                        $packageId,
                                        $packageName
                                    );
                                } else {
                                    $message = 'It is only possible to select %d from the following list of selected products: %s';
                                    $message = Mage::helper('dyna_configurator')->__($message,
                                        $ruleOperationValue,
                                        implode(', ', $rightProductIdsInCartToSkus)
                                    );
                                }
                            }
                            $warningMessages[$packageId][$warningRule] = htmlspecialchars($message);
                        }
                    }
                }
            }

            $this->getCache()->save(serialize($warningMessages), $cache_key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
        }
        return $warningMessages;
    }

    /**
     * Retrieves all Min/Max/Eql rule IDs applicable to products in cart
     * @param $cartProducts
     * @param $eachProductCategories
     * @param null $packages
     * @return array
     * @internal param $cartCategories
     * @internal param $packageId
     */
    protected function getRulesForProductsInCart($cartProducts, $eachProductCategories, $packages = null)
    {
        $rules = [];
        if (empty($cartProducts)) {
            return $rules;
        }
        /** @var Dyna_Package_Model_Package $packageModel */
        foreach ($packages as $packageModel) {
            $packageType = null;
            if ($packageModel) {
                $packageType = $packageModel->getType();
            }

            /** @var Dyna_Catalog_Helper_ProcessContext $processContextHelper */
            $processContextHelper = Mage::helper("dyna_catalog/processContext");
            $processContextId = $processContextHelper->getProcessContextId($packageModel);

            $cache_key = sprintf('all_min_max_eql_rules_for_package_type_%s_%s', $packageType, $processContextId);
            $cachedValues = $this->getCache()->load($cache_key);
            if ($cachedValues === false) {
                $ruleModel = Mage::getSingleton('dyna_productmatchrule/rule');
                $ruleCollection = $ruleModel->getCollection();
                if($processContextId) {
                    $ruleCollection->getSelect()->joinLeft(
                        array('product_match_rule_process_context' => 'product_match_rule_process_context'),
                        'product_match_rule_process_context.rule_id = main_table.product_match_rule_id',
                        array('process_context_id' => 'product_match_rule_process_context.process_context_id')
                    );
                    $ruleCollection->addFieldToFilter('process_context_id', array('eq' => $processContextId));
                }
                $ruleCollection->addFieldToFilter('operation', array('in' => self::MIN_MAX_EQL_OPERATIONS))
                    ->setOrder('priority', 'ASC');
                // Appending package type filter by package model
                if ($packageType) {
                    $packageTypeModel = Mage::getModel('dyna_package/packageType')
                        ->loadByCode($packageType);
                    $ruleCollection->addFieldToFilter('package_type', array('eq' => $packageTypeModel->getId()));
                }
                $allMinMaxEqlRules = $ruleCollection->toArray();
                $this->getCache()->save(serialize($allMinMaxEqlRules), $cache_key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
            } else {
                $allMinMaxEqlRules = unserialize($cachedValues);
            }

            $rulesForProduct = [];
            $packageId = $packageModel->getPackageId();
            $packageProducts = $cartProducts[$packageId];
                foreach ($packageProducts as $product) {
                    foreach ($allMinMaxEqlRules['items'] as $rule) {
                        $ruleId = $rule['product_match_rule_id'];
                        switch ($rule['operation_type']) {
                            case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_S2P:
                            case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_S2C:
                                if ($this->evaluateServiceSourceExpression($rule['service_source_expression'])) {
                                    $rulesForProduct[$ruleId] = $rule;
                                }
                                break;
                            case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_S2S:
                                if ($this->evaluateServiceSourceExpression($rule['service_source_expression'])) {
                                    $rulesForProduct[$ruleId] = $rule;
                                } else {
                                    continue 2;
                                }
                                $rulesForProduct[$ruleId]['right_id'] = $this->evaluateServiceTargetExpression($rule['service_target_expression'], $packageType);
                                break;
                            case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_P2S:
                                if ($rule['left_id'] == $product) {
                                    $rulesForProduct[$ruleId] = $rule;
                                }
                                $rulesForProduct[$ruleId]['right_id'] = $this->evaluateServiceTargetExpression($rule['service_target_expression'], $packageType);
                                break;
                            case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_C2S:
                                foreach ($eachProductCategories[$product] as $category) {
                                    if ($rule['left_id'] == $category) {
                                        $rulesForProduct[$ruleId] = $rule;
                                    }
                                }
                                $rulesForProduct[$ruleId]['right_id'] = $this->evaluateServiceTargetExpression($rule['service_target_expression'], $packageType);
                                break;
                            case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_P2C:
                            case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_P2P:
                                if ($rule['left_id'] == $product) {
                                    $rulesForProduct[$ruleId] = $rule;
                                }
                                break;
                            case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_C2C:
                            case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_C2P:
                                foreach ($eachProductCategories[$product] as $category) {
                                    if ($rule['left_id'] == $category) {
                                        $rulesForProduct[$ruleId] = $rule;
                                    }
                                }
                                break;
                        }
                    }
                }

                $rules[$packageId] = $rulesForProduct;
            }

        return $rules;
    }

    /**
     * Get a list of mandatory product ids from mandatory list indexer
     * @param $productIds
     * @return array
     */
    public function getMandatory($productIds, $websiteId = null, $packageType = "")
    {
        if (count($productIds) === 0) {
            return [];
        }

        //@todo evaluate addMulti performance impact of this method
        $websiteId = $websiteId ?: Mage::app()->getStore()->getWebsiteId();
        /** @var Dyna_Catalog_Helper_ProcessContext $processContextHelper */
        $processContextHelper = Mage::helper("dyna_catalog/processContext");
        $processContextId = $processContextHelper->getProcessContextId();
        $productIds = array_unique($productIds);
        sort($productIds, SORT_NUMERIC);
        $key = sprintf('%s_product_mandatory_addons_%s_%s', md5(serialize($productIds)), $websiteId, $processContextId);
        if ($result = $this->getCache()->load($key)) {
            return unserialize($result);
        } else {
            $rules = $this->getCartRules($productIds, [], $websiteId, false, $packageType);
            $mandatory = [];
            foreach ($productIds as $prod) {
                // Retrieve all the mandatory families for the product
                $mandatoryFamilies = Mage::getModel('productmatchrule/mandatory')->getMandatoryFamilies($prod, $websiteId);
                foreach ($mandatoryFamilies as $family) {
                    // If the family was already parsed, skip it
                    if (isset($mandatory[$family])) {
                        continue;
                    }

                    // Ensure there are products in this family compatible with the rest of the products
                    $collectionIds = Mage::getModel('catalog/category')->getProductIds($family);
                    $collectionIds = $collectionIds ? $collectionIds : [];
                    if (count(array_intersect($rules, $collectionIds)) > 0) {
                        $mandatory[$family] = $family;
                    }
                }
            }

            $result = array_keys($mandatory);
            $this->getCache()->save(serialize($result), $key, array($this->getCache()::PRODUCT_TAG), $this->getCache()->getTtl());

            return $result;
        }
    }

    /**
     * Get a list of mandatory product ids from mandatory list indexer
     * @param $productIds
     * @return array
     */
    public function getMandatoryWithRules($productIds, $rules, $websiteId = null)
    {
        if (!$productIds) {
            return array();
        }

        $websiteId = $websiteId ?: Mage::app()->getStore()->getWebsiteId();
        /** @var Dyna_Catalog_Helper_ProcessContext $processContextHelper */
        $processContextHelper = Mage::helper("dyna_catalog/processContext");
        $processContextId = $processContextHelper->getProcessContextId();

        $key = sprintf('%s_product_mandatory_addons_with_rules_%s_%s', md5(serialize($productIds)), $websiteId, $processContextId);
        if ($result = $this->getCache()->load($key)) {
            return unserialize($result);
        } else {
            $mandatory = [];
            foreach ($productIds as $prod) {
                // Retrieve all the mandatory families for the product
                $mandatoryFamilies = Mage::getModel('productmatchrule/mandatory')->getMandatoryFamilies($prod, $websiteId);
                foreach ($mandatoryFamilies as $family) {
                    // If the family was already parsed, skip it
                    if (isset($mandatory[$family])) {
                        continue;
                    }

                    // Ensure there are products in this family compatible with the rest of the products
                    $collectionIds = Mage::getModel('catalog/category')->getProductIds($family);
                    $collectionIds = $collectionIds ? $collectionIds : [];
                    if (count(array_intersect($rules, $collectionIds)) > 0) {
                        $mandatory[$family] = $family;
                    }
                }
            }
            $result = array_keys($mandatory);
            $this->getCache()->save(serialize($result), $key, array($this->getCache()::PRODUCT_TAG), $this->getCache()->getTtl());
        }

        return $result;
    }

    /**
     * Retrieves existing cart package items based on type and package id.
     * @param $type
     * @param null $packageId
     * @return array
     */
    public function getCartItems($type, $packageId = null)
    {
        $ids = $installedBaseProductsIds = array();
        if (!$packageId) {
            return $ids;
        }
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        /** @var Dyna_Checkout_Model_Sales_Quote_Item $item */
        foreach ($quote->getPackageItems($packageId) as $item) {

            if (in_array($type, explode(",", $item->getPackageType()))) {
                $packageSubtype = strtolower(current($item->getProduct()->getType()));

                $ids[$packageSubtype][] = (int)$item->getProductId();
                $ids[$packageSubtype] = array_values(array_unique($ids[$packageSubtype]));

                //Get is_contract flag of each package to check if it belongs to installed base (Inlife Sales)
                if ($item->getIsContract()) {
                    $installedBaseProductsIds[(int)$item->getProductId()] = [];
                    if ($item->getContractEndDate()) {
                        $installedBaseProductsIds[(int)$item->getProductId()]['contract_end_date'] = date('d-m-Y', strtotime($item->getContractEndDate()));
                    }
                    if($item->getContractPossibleCancellationDate()){
                        $installedBaseProductsIds[(int)$item->getProductId()]['contract_possible_cancellation_date'] = date('d-m-Y', strtotime($item->getContractPossibleCancellationDate()));
                    }
                }
            }
        }
        //merge alternate items that are preselected or not
        $installedBase = array_merge($quote->getAlternateItems(), $quote->getAlternateItems(null, false, false));
        // Append the Installed Base items that were removed from current quote
        foreach ($installedBase as $alternateItem) {
            $installedBaseProductsIds[(int)$alternateItem->getProductId()] = [];
        }

        $ids['initCart'] = $ids;
        $ids['installedBaseCart'] = $installedBaseProductsIds;

        return $ids;
    }

    /**
     * Gets bundle id for current packages
     * @param null $quote
     * @return array
     */
    public function getBundleId($quote = null, $packageId = null)
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = $quote ?? Mage::getSingleton('checkout/cart')->getQuote();

        foreach ($quote->getPackages() as $item) {
            if (!isset($packageId) || $item['package_id'] == $packageId) {
                return $item['bundle_id'];
            }
        }

        return false;
    }

    /**
     * @return array
     */
    public function getCardinalityWarnings()
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getModel('checkout/session')->getQuote();
        $packages = $quote->getCartPackages(true);
        $warnings = [];
        foreach ($packages as $packageModel) {
            $packageId = $packageModel->getPackageId();
            $warnings[$packageId] = [];
            if (!empty($packageId)) {
                $packageName = Mage::getModel('dyna_package/packageType')->loadByCode($packageModel->getType())->getFrontEndName();
                $packageConfiguration = Mage::getModel('package/configuration')->setPackage($packageModel);
                $packageSubtypes = $packageConfiguration->getConfiguration();
                foreach ($packageSubtypes as $packageSubtype) {
                    // Define package_subtype product attribute
                    $subtypeIdentifier = $packageSubtype->getPackageCode();
                    // Load all package items
                    $packageItems = $packageModel->getItems(strtolower($subtypeIdentifier));

                    // Set count limit to which a product of certain type can reach
                    $packageSubtypeCardinality = $packageSubtype->getCardinality();

                    /** @var Dyna_Catalog_Helper_ProcessContext $processContext */
                    $processContext = Mage::helper('dyna_catalog/processContext');
                    $processContextId = $processContext->getProcessContextId($packageModel);

                    if ($processContextId > 0) {
                        $subtypeCardinality = Mage::getModel('dyna_package/packageSubtypeCardinality')
                            ->loadBySubtypeAndContext($packageSubtype->getId(), $processContextId)
                            ->getData();

                        if (isset($subtypeCardinality['cardinality']) && $subtypeCardinality['cardinality'] != '') {
                            $packageSubtypeCardinality = $subtypeCardinality['cardinality'];
                        }
                    }

                    $minItems = $packageConfiguration->getMinProducts($packageSubtypeCardinality);
                    $maxItems = $packageConfiguration->getMaxProducts($packageSubtypeCardinality);
                    $packageItemsCount = count($packageItems);

                    if (!$minItems && !$maxItems && $packageItemsCount > $maxItems) {
                        $diff = $packageItemsCount - $maxItems;
                        $message = abs($diff) == 1 ? 'Remove %d product from package subtype %s of Package %d: %s (this includes hidden products).'
                            : 'Remove %d products from package subtype %s of Package %d: %s (this includes hidden products)';
                        $warnings[$packageId][$subtypeIdentifier] = htmlspecialchars(Mage::helper('dyna_configurator')->__($message,
                            $diff,
                            $subtypeIdentifier,
                            $packageId,
                            $packageName
                        ));
                    }

                    if ($minItems && $minItems != -1 && $packageItemsCount < $minItems) {
                        $diff = $minItems - $packageItemsCount;
                        $message = abs($diff) == 1 ? 'Add %d product in the package subtype %s of Package %d: %s.'
                            : 'Add %d products in the package subtype %s of Package %d: %s.';
                        $warnings[$packageId][$subtypeIdentifier] = htmlspecialchars(Mage::helper('dyna_configurator')->__($message,
                            $diff,
                            $subtypeIdentifier,
                            $packageId,
                            $packageName
                        ));
                    }
                    // If max is not infinite
                    if ($maxItems && $maxItems != -1 && $packageItemsCount > $maxItems) {
                        $diff = $packageItemsCount - $maxItems;
                        $message = abs($diff) == 1 ? 'Remove %d product from package subtype %s of Package %d: %s (this includes hidden products).'
                            : 'Remove %d products from package subtype %s of Package %d: %s (this includes hidden products)';
                        $warnings[$packageId][$subtypeIdentifier] = htmlspecialchars(Mage::helper('dyna_configurator')->__($message,
                            $diff,
                            $subtypeIdentifier,
                            $packageId,
                            $packageName
                        ));
                    }
                }
            }
            if (empty($warnings[$packageId])) {
                unset($warnings[$packageId]);
            }
        }

        return $warnings ?? [];
    }

    /**
     * @return array
     */
    public function getMandatoryWarnings()
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getModel('checkout/session')->getQuote();
        $packages = $quote->getCartPackages(true);
        $websiteId = Mage::app()->getStore()->getWebsiteId();
        $warnings = [];
        foreach ($packages as $packageModel) {
            $packageId = $packageModel->getPackageId();
            $packageType = $packageModel->getType();
            $packageName = Mage::getModel('dyna_package/packageType')->loadByCode($packageModel->getType())->getFrontEndName();
            $productIds = [];
            if (!empty($packageId)) {
                $packageItems = $quote->getPackageItems($packageId);
                foreach ($packageItems as $item) {
                    $productIds[] = $item->getProductId();
                }
                $mandatoryFamilies = $this->getMandatory($productIds, $websiteId, $packageType);
                foreach ($mandatoryFamilies as $family) {
                    $warnings[$packageId]['mandatory-' . $family] = Mage::helper('dyna_configurator')->__('You must select at least one product from %s category in Package %d: %s.',
                        Mage::getModel('catalog/category')->load($family)->getName(),
                        $packageId,
                        $packageName
                    );
                }
            }
        }
        return $warnings ?? [];
    }

    /**
     * Checks if there is any mandatory tariff change when renewing the package
     * @return array
     */
    public function getProlongationWarnings()
    {
        $warnings = [];

        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $packages = $quote->getPackages();
        foreach ($packages as $package) {
            $packageId = $package['package_id'];

            if ($package['prolongation_info_eligibility'] == 'T') {
                $packageItems = $quote->getPackageItems($packageId);
                foreach ($packageItems as $item) {
                    if ($item->getProduct()->isSubscription() && $item->getSystemAction() == 'existing') {
                        $warnings[$packageId]['prolongation'] = Mage::helper('dyna_configurator')->__('Tariff change of package %d is mandatory (%s)',
                            $packageId,
                            $item->getName()
                        );
                    }
                }
            }
        }

        return $warnings ?? [];
    }

    /**
     * Returns whether the package should be allowed to be duplicated based on its state inside a bundle
     * @see OVG-1756
     * @param Dyna_Package_Model_Package $package
     * @return bool
     */
    public function allowBundlePackageDuplication($package)
    {
        $allowed = true;

        // only mobile packages can be duplicated
        if (!in_array($package->getType(), static::DUPLICABLE_PACKAGES)) {
            $allowed = false;
        }

        // if still allowed, proceed to evaluating package bundles
        if ($allowed) {
            $packageBundles = $package->getBundles();
            // only Red+ owner packages can be duplicated when the package is part of a bundle
            if (count($packageBundles) && !$package->isPartOfRedPlus() || ($package->isPartOfRedPlus() && $package->getParentId())) {
                $allowed = false;
            }
        }

        return $allowed;
    }

    /**
     * Returns whether the package should be allowed to be duplicated based on its process Context
     * not ILS
     * not Prolongation
     * @param Dyna_Package_Model_Package $package
     * @return bool
     */
    public function allowProcessContextPackageDuplication($package)
    {
        if ($package->getSaleType() != Dyna_Catalog_Model_ProcessContext::ACQ) {
            return false;
        }

        return true;
    }

    /**
     * @param $packageType
     * @return mixed
     */
    public function getCreationTypeId($packageType)
    {
        return Mage::getModel('dyna_package/packageCreationTypes')->getTypeByPackageCode($packageType)->getId();
    }

    /**
     * Regenerates the package ids in case of a package removal.
     * @param Dyna_Checkout_Model_Sales_Quote $quote
     * @param null $packageId
     * @return array
     */
    public function reindexPackagesIds($quote, $packageId = null)
    {
        $quoteItems = $quote->getAllItems();
        $packageModels = $quote->getCartPackages();

        $counter = 0;
        $newPackages = array();

        foreach ($packageModels as $packageInfo) {
            $counter++;
            $p_id = $packageInfo['package_id'];

            if ($p_id != $counter) {
                $packageInfo->setPackageId($counter)->save();
            }
            $newPackages[$p_id] = $counter;
        }

        $newActivePackage = $packageId ? (isset($newPackages[$packageId]) ? $newPackages[$packageId] : 1) : 1;

        foreach ($quoteItems as $quoteItem) {
            $itemPackageID = $quoteItem->getPackageId();
            if (isset($newPackages[$itemPackageID]) && $itemPackageID != $newPackages[$itemPackageID]) {
                $quoteItem->setPackageId($newPackages[$itemPackageID])->save();
            }
        }

        $quote->saveActivePackageId($newActivePackage);

        return $newPackages;
    }

    /**
     * Method to get the current source collection of the package
     * @return int|mixed|null
     */
    public function getCurrentSourceCollection()
    {
        // don't cache source collection on this instance as flows will intersect during addMulti process
        $sourceCollectionId = Mage::registry('rules_source_collection');

        if (!$sourceCollectionId) {
            // Default is Current Quote
            $sourceCollectionId = Dyna_ProductMatchRule_Helper_Data::SOURCE_COLLECTION_CQ;
            /** @var Dyna_Checkout_Model_Sales_Quote $quote */
            $quote = Mage::getSingleton('checkout/cart')->getQuote();
            $activePackage = $quote->getActivePackage();
            if ($activePackage && $activePackage->getSaleType() != Dyna_Catalog_Model_ProcessContext::ACQ) {
                // if process context is different from acquisition we need to check if the contract tarif has changed
                if ($activePackage->hasSubscriptionFromContract()) {
                    $sourceCollectionId = Dyna_ProductMatchRule_Helper_Data::SOURCE_COLLECTION_IB;
                }
            }
        }

        $this->sourceCollection = $sourceCollectionId;

        return $this->sourceCollection;
    }

    public function getNotInitiallySelectableProducts($packageType) {
        $cache_key = sprintf('not_initially_selectable_from_%s', $packageType);
        if (in_array(strtolower($packageType), Dyna_Catalog_Model_Type::getCablePackages())) {
            // cable initial selectable will be handled through option of option and Artifact
            return array();
        }
        //$initialSelectableAttribute is 'selectable' for Cable Stack but we don't need to check that here as it is the artifact's job
        $initialSelectableAttribute = 'initial_selectable';
        if (!($notInitiallySelectable = unserialize($this->getCache()->load($cache_key)))) {
            $packageTypeAttribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', Dyna_Catalog_Model_Product::CATALOG_PACKAGE_TYPE_ATTR);
            $typeValue = Mage::helper('omnius_catalog')->getAttributeAdminLabel($packageTypeAttribute, null, $packageType);
            $notInitiallySelectable = Mage::getResourceModel('catalog/product_collection')
                ->addAttributeToFilter($initialSelectableAttribute, array('neq' => 1))
                ->addAttributeToFilter(Dyna_Catalog_Model_Product::CATALOG_PACKAGE_TYPE_ATTR, array('eq' => $typeValue))
                ->getAllIds();

            $this->getCache()->save(serialize($notInitiallySelectable), $cache_key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
        }
        return $notInitiallySelectable;
    }

    protected function evaluateServiceSourceExpression($ssExpression) {
        /** @var $dynaCoreHelper Dyna_Core_Helper_Data */
        $dynaCoreHelper = Mage::helper('dyna_core');
        try {
            $passed = $dynaCoreHelper->evaluateExpressionLanguage(
                $ssExpression,
                array(
                    'customer' => Mage::getModel('dyna_configurator/expression_customer'),
                    'cart' => Mage::getModel('dyna_configurator/expression_cart'),
                    'availability' => Mage::getModel('dyna_configurator/expression_availability'),
                    'installBase' => Mage::getModel('dyna_configurator/expression_installBase'),
                    'activeInstallBase' => Mage::getModel('dyna_configurator/expression_activeInstallBase'),
                    'agent' => Mage::getModel('dyna_configurator/expression_agent')
                )
            );
        } catch (\Exception $e) {
            $passed = false;
        }
        return $passed;
    }

    protected function evaluateServiceTargetExpression($ssExpression, $packageType) {
        /** @var $dynaCoreHelper Dyna_Core_Helper_Data */
        $dynaCoreHelper = Mage::helper('dyna_core');
        /** @var Dyna_Configurator_Model_Expression_Catalog $catalogObject */
        $catalogObject = Mage::getModel('dyna_configurator/expression_catalog', $packageType);
        try {
            $result = $dynaCoreHelper->evaluateExpressionLanguage(
                trim($ssExpression),
                array(
                    'catalog' => $catalogObject,
                    'cart' => Mage::getModel('dyna_configurator/expression_cart'),
                )
            );
        } catch (\Exception $e) {
            $result = false;
        }
        return $result === false ? [] : $result;
    }


    /**
     * Get a list of products of the specified package type, with the non standard attributes
     *
     * @param $packageType
     * @return array|mixed|Varien_Data_Collection
     */
    public function getProductsWithBounds($packageType)
    {
        if (!$packageType || !in_array(strtolower($packageType), Dyna_Catalog_Model_Type::getCablePackages())) {
            return [];
        }

        /** @var Dyna_Configurator_Model_Catalog $catalogModel */
        $catalogModel = Mage::getModel('dyna_configurator/catalog');
        $attributesList = array(
            'req_rate_prod_for_contracts',
            'req_rate_min_bound',
            'req_rate_max_bound',
            'product_code',
            'contract_code'
        );

        return $catalogModel->getAllItems(strtolower($packageType), $attributesList);
    }

    /**
     * Parses the list of products and builds an array of price min/max bounds
     *
     * @param $products
     * @return array
     */
    public function parseProductsWithBounds($products)
    {
        $parsedProducts = [];
        $parsedProducts['products'] = [];
        $parsedProducts['bounds'] = [];

        /** @var Dyna_Catalog_Model_Product $product */
        foreach ($products as $product) {
            if (!empty($contractRates = $product->getData('req_rate_prod_for_contracts'))) {
                $sku = $product->getData('sku');
                $parsedProducts['products'][$sku] = json_decode($contractRates, true);
                $parsedProducts['bounds'][$sku] = array(
                    'min' => $product->getData('req_rate_min_bound'),
                    'max' => $product->getData('req_rate_max_bound')
                );
            }
        }

        return $parsedProducts;
    }

    /**
     * Parses the service response and saves the prices within bounds into cache
     *
     * @param $products
     * @param $parsedProductBounds
     * @param $nonStandardRates
     * @param $addressId
     * @return array
     */
    public function saveNonStandardRates($products, $parsedProductBounds, $nonStandardRates, $addressId)
    {
        /** @var Mage_Tax_Helper_Data $_taxHelper */
        $_taxHelper = Mage::helper('tax');
        $result = [];

        foreach ($nonStandardRates as $rate) {
            $rateContractCode = $rate['ContractCode'];
            $rateProductCode = $rate['ProductCode'];

            foreach ($products as $product) {
                $productAttributes = $product->getAttributes();

                $contractCode = isset($productAttributes['contract_code']) ? $productAttributes['contract_code']->getFrontend()->getValue($product) : null;
                $productCode = $product->getData('product_code');

                if ($rateContractCode == $contractCode && $rateProductCode == $productCode) {
                    // check whether the returned prices are between the bounds
                    $sku = $product->getData('sku');
                    $prices = [];
                    // The non standard rate is saved and applied only if the product has min/max bounds defined in the catalog and the rate is between the bounds
                    $saveNonStandardRate = false;

                    // Load product model
                    /** @var Dyna_Catalog_Model_Product $product */
                    $product = Mage::getModel('catalog/product')->load($product->getData('entity_id'));

                    if (isset($parsedProductBounds['bounds'][$sku]) && $rate['NonStandardInstallRate'] >= $parsedProductBounds['bounds'][$sku]['min'] && $rate['NonStandardInstallRate'] <= $parsedProductBounds['bounds'][$sku]['max']) {
                        $saveNonStandardRate = true;
                        $prices['price'] = $_taxHelper->getPrice($product, $rate['NonStandardInstallRate'], false, null, null, null, null, false);
                        $prices['price_with_tax'] = $_taxHelper->getPrice($product, $rate['NonStandardInstallRate'], true, null, null, null, null, false);
                    } else {
                        $prices['price'] = null;
                        $prices['price_with_tax'] = null;
                    }

                    if (isset($parsedProductBounds['bounds'][$sku]) && $rate['NonStandardRate'] >= $parsedProductBounds['bounds'][$sku]['min'] && $rate['NonStandardRate'] <= $parsedProductBounds['bounds'][$sku]['max']) {
                        $saveNonStandardRate = true;
                        $prices['maf'] = $_taxHelper->getPrice($product, $rate['NonStandardRate'], false, null, null, null, null, false);
                        $prices['maf_with_tax'] = $_taxHelper->getPrice($product, $rate['NonStandardRate'], true, null, null, null, null, false);
                    } else {
                        $prices['maf'] = null;
                        $prices['maf_with_tax'] = null;
                    }

                    if ($saveNonStandardRate) {
                        $cacheKey = sprintf(
                            "non_standard_rates_%s_%s",
                            $addressId,
                            $product->getData('entity_id')
                        );
                        $this->getCache()->save(serialize($prices), $cacheKey);
                        $result[$product->getData('entity_id')] = $prices;
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Calls the GetNonStandardRates service, parses its response and stores the updated prices into cache
     *
     * @param $packageType
     * @param $addressId
     * @param $availableProducts
     * @return array|mixed
     */
    public function getNonStandardRates($packageType, $addressId, $availableProducts=[])
    {
        $cacheKey = sprintf(
            "non_standard_rates_for_%s_%s_%s",
            $packageType,
            $addressId,
            md5(implode(',', (empty($availableProducts) ? [] : $availableProducts) ))
        );

        if ($nonStandardRates = $this->getCache()->load($cacheKey)) {
            return unserialize($nonStandardRates);
        } else {
            // Get all products with non standard rates available in the catalog
            $products = $this->getProductsWithBounds($packageType);
            // Parse the products and build the service parameters
            $parsedProducts = $this->parseProductsWithBounds($products);

            $requestParams = [
                'addressId' => $addressId,
                'products' => $parsedProducts['products']
            ];

            /** @var Dyna_Configurator_Model_Client_NonStandardRatesClient $client */
            $client = Mage::getModel('dyna_configurator/client_nonStandardRatesClient');
            $serviceResponse = $client->executeGetNonStandardRates($requestParams);
            $nonStandardRates = $serviceResponse['NonStandardRates'];

            // parses the service response and saves the non standard rates in cache
            $result = $this->saveNonStandardRates($products, $parsedProducts, $nonStandardRates, $addressId);

            $this->getCache()->save(serialize($result), $cacheKey);
            return $result;
        }
    }

    /**
     * @param string $type
     * @param array $productIds
     * @return mixed
     */
    public function getCampaignOfferCompatibility($type, $productIds)
    {
        $websiteId = Mage::app()->getStore()->getWebsiteId();
        /** @var Dyna_Configurator_Helper_Rules $rulesHelper */
        $rulesHelper = Mage::helper('dyna_configurator/rules');
        $rules = null;

        if (in_array($type, Dyna_Catalog_Model_Type::getCablePackages())) {
            $rulesHelper->setNeedsFauxQuote(true);
        } else {
            $rulesHelper->setNeedsFauxQuote(false);
        }
        $this->setFauxProcessContextId(Dyna_Catalog_Model_ProcessContext::ACQ);
        $rules = $rulesHelper->getPreconditionRules($productIds, $type, $websiteId);
        $rulesHelper->cacheSave(null, $rulesHelper->getCacheKey($type));
        if (in_array($type, Dyna_Catalog_Model_Type::getMobilePackages()) && count($productIds) === 1) {
            $rules['allowed'] = $productIds;
        }

        return count(array_intersect($productIds, $rules['allowed'])) == count($productIds) ? true : false;
    }

    /**
     * Sets FauxProcessContext id by code
     * @param string $processContextCode
     */
    public function setFauxProcessContextId($processContextCode)
    {
        /** @var $processContextModel Dyna_Catalog_Model_ProcessContext */
        $processContextModel = Mage::getModel('dyna_catalog/processContext');
        $this->fauxProcessContextId = $processContextModel->getProcessContextIdByCode($processContextCode);
    }
}
