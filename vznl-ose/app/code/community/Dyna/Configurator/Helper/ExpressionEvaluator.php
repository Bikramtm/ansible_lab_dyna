<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Dyna_Configurator_Helper_ExpressionEvaluator
{
    protected $allowedOperators = array(
        '>' => "strictHigher",
        '>=' => "higher",
        '<' => "strictLower",
        '<=' => "lower",
        '=' => "equal",
        '==' => "equal",
        '===' => "strictEqual",
        '!=' => "different",
    );

    /**
     * Make sure all allowed operators have their evaluation methods implemented
     * ExpressionEvaluator constructor.
     */
    public function __construct()
    {
        $missingImplementations = array();
        foreach ($this->allowedOperators as $method) {
            if (!method_exists($this, $method)) {
                $missingImplementations[] = $method;
            }
        }

        if (!empty($missingImplementations)) {
            Mage::throwException("More allowed operators than implemented. Please implement: " . implode(", ", array_unique($missingImplementations)));
        }
    }

    /**
     * Evaluate two values using a configurable operator
     * It reads as following: $left $operator $right (Ex: $left > $right : bool)
     * @return bool
     * @throws Mage_Core_Exception
     */
    public function evaluate($left, $right, $operator)
    {
        // make sure
        if (!array_key_exists($operator, $this->allowedOperators)) {
            //throw new Mage_Core_Exception("Trying to evaluate condition using an unsupported operator.");
            Mage::throwException("Trying to evaluate condition using an unsupported operator.");
        }


        $method = $this->allowedOperators[$operator];

        return $this->$method($left, $right);
    }

    /**
     * Return whether or not left is higher than right
     * @param $left
     * @param $right
     * @return bool
     */
    protected function strictHigher($left, $right)
    {
        return $left > $right;
    }

    /**
     * Return whether or not left is higher or equal than right
     * @param $left
     * @param $right
     * @return bool
     */
    protected function higher($left, $right)
    {
        return ($left >= $right);
    }

    /**
     * Return whether or not left is lower than right
     * @param $left
     * @param $right
     * @return bool
     */
    protected function strictLower($left, $right)
    {
        return $left < $right;
    }

    /**
     * Return whether or not left is equal or lower than right
     * @param $left
     * @param $right
     * @return bool
     */
    protected function lower($left, $right)
    {
        return $left <= $right;
    }

    /**
     * Return whether or not left equals right
     * @param $left
     * @param $right
     * @return bool
     */
    protected function equal($left, $right)
    {
        return $left == $right;
    }

    /**
     * Return whether or not left equals right in strict mode
     * @param $left
     * @param $right
     * @return bool
     */
    protected function strictEqual($left, $right)
    {
        return $left === $right;
    }

    protected function different($left, $right)
    {
        return $left != $right;
    }
}
