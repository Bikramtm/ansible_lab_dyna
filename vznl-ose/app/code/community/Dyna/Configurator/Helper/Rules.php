<?php

class Dyna_Configurator_Helper_Rules extends Mage_Core_Helper_Abstract
{
    // 2 hours
    const CACHE_TTL = 7200;
    const CACHE_EXPIRED_PRODUCTS_KEY = "EXPIRED_PRODUCT_IDS";
    const MAX_ITERATIONS = 3;

    /** @var $productsMapping array */
    protected $productsMapping = [];
    // Max iterations for cable rules
    /** @var $cache Dyna_Cache_Model_Cache */
    protected $cache;
    // Current iterations for post-condition rules
    protected $postIterations = 0;
    /** @var $cartHelper Dyna_Configurator_Helper_Cart */
    protected $cartHelper;

    protected $packageCacheProcessed = [];

    // This is the gateway (helper) that implements getProductState, executePreconditionRules and executePostConditionRules methods
    /** @var  $gateWay Dyna_Configurator_Helper_Rules_Cable */
    protected $gateWay;
    protected $cartProducts = [];
    protected $packageType = null;
    protected $mandatoryProducts = [];
    protected $selectableAttributeData = [];
    protected $cableProducts = array();
    // holds expired product ids for checking against when adding products to cart
    protected $expiredProductIds = false;
    protected $logData = array();

    protected $stickToCurrentPackage = true;

    protected $needsFauxQuote = false;

    /**
     * @param $value
     * @return $this
     */
    public function setPostIterations($value)
    {
        $this->postIterations = $value;

        return $this;
    }

    /**
     * @param $type string
     * @return $this
     */
    public function setPackageType($type)
    {
        $this->packageType = $type;

        return $this;
    }

    /**
     * Return an array of orderable products based on current rules
     * @param array $selectedProducts Product ids
     * @param string $type Package type
     * @param int $websiteId
     * @return array
     * @throws Mage_Core_Model_Store_Exception
     */
    public function getPreconditionRules($selectedProducts = [], $type = "cable_tv", $websiteId = 2)
    {
        $this->packageType = $type;
        $websiteId = $websiteId ?: Mage::app()->getStore()->getWebsiteId();
        $ownHardwareRules = [];

        /**
         * For DSL and LTE packages, if no product is selected we need to only show
         * the serviceProducts for the first time in the configurator and not all products
         */

        $productsArray = [];

        // Cable package flow
        if ($this->checkGateWay($type)) {
            // This will contain of list of compatible product ids retrieved from services and will be appended later to the allowed products list
            $ownHardwareRules = $this->getAvailableOwnHardware($selectedProducts);
            $cachedCableProducts = $this->getAllCableProducts($type);
            $productsArray = &$cachedCableProducts;

            // Check if any cable products are in the database
            if (empty($productsArray)) {
                $this->log("No " . $type . " products found in your database. Returning empty array.");

                return [];
            }

            $nowSelected = $selectedProducts;
            unset($selectedProducts);

            // No products in cart, parse rules for all products
            $this->log('=== Executing precondition rules for all products of type: ' . $type . ' ===');

            // Precondition rules are sending product list as parameter after a certain iteration
            $foundInCache = $this->cacheLoad();

            $unserializedData = unserialize($foundInCache);
            if (!empty($unserializedData)) {
                $currentIterationProducts = $unserializedData;
            } else {
                $currentIterationProducts = $this->getDbProductsStates($productsArray, $nowSelected);
            }

            if (empty($currentIterationProducts)) {
                $this->dumpLog();
                return [
                    "error" => true,
                    "message" => $this->__("No products mapping available for current store."),
                ];
            }

            $iterations = 1;
            do {
                $previousIterationProducts = $currentIterationProducts;

                // dump current selected products to log
                $this->log("****************************");
                $this->log("* STARTING NEW PRECONDITION ITERATION: " . $iterations);
                $this->log("****************************");

                $this->log("****************************");
                $this->log("* INPUT FOR CablePreconditionRules ITERATION: " . $iterations);
                $this->log("****************************");

                // Execute option-of-option rules between packages (KIP-KUD)
                $quote = Mage::getSingleton('checkout/cart')->getQuote();
                if ($this->needsFauxQuote) {
                    $items = $quote->getAllItems();
                    foreach ($nowSelected as $productId) {
                        $packageCreationTypeId = $this->getCartHelper()->getCreationTypeId($type) ?? null;
                        $quoteItem = Mage::getModel('dyna_checkout/sales_quote_item');
                        $sku = null;
                        foreach ($productsArray as $product) {
                            if ($product['entity_id'] == $productId) {
                                $sku = $product['sku'];
                                break;
                            }
                        }
                        $quoteItem->setData([
                            'package_type' => $type,
                            'product_id' => $productId,
                            'sku' => $sku,
                            'is_contract' => 0,
                            'package_creation_type_id' => $packageCreationTypeId,
                        ]);
                        $items[] = $quoteItem;
                    }
                } else {
                    $quote = Mage::getSingleton('checkout/cart')->getQuote();
                    // Send refreshed quote items_collection
                    $items = $quote->getAllItems();
                }
                $currentIterationProducts = $this->processOptionOfOption($items, $currentIterationProducts);

                $this->dumpToLog($currentIterationProducts);

                // 1. Executing Cable Precondition Rules
                $currentIterationProducts = $this->executeCablePreconditionRules($currentIterationProducts);
                if (empty($currentIterationProducts)) {
                    $this->log('=== No remaining products available in current conditions ===');
                }

                $this->log("****************************");
                $this->log("* RESULT FOR CablePreconditionRules ITERATION: " . $iterations);
                $this->log("****************************");
                // dump current selected products to log
                $this->dumpToLog($currentIterationProducts);

                $nowSelectedCount = $this->getArrayCount($nowSelected) ;
                if ($nowSelectedCount) {
                    // Execute Omnius Compatibility Rules only when there are selected products

                    // 2. Execute Omnius Compatibility Rules
                    $omniusAllowedProducts = $this->executeOmniusCompatibilityRules($type, $nowSelected, $productsArray, $websiteId);
                    $omniusAllowedProducts = array_unique(array_merge($omniusAllowedProducts, $ownHardwareRules));
                    $omniusAllowedMappedSkus = $this->convertFromIdsToSkus($omniusAllowedProducts);

                    /**
                     * @var  $key
                     * @var Omnius\RulesEnginePlugin\IProductState $cProduct
                     */
                    foreach ($currentIterationProducts as $key => $cProduct) {
                        if (!isset($omniusAllowedMappedSkus[$cProduct->getProductId()])) {
                            // Mark products as not changeable
                            $currentIterationProducts[$key] = $this->gateWay->getProductStateInstance($cProduct->getProductId(), $cProduct->isVisible(), $cProduct->isSelected(), false);
                        }
                    }
                    $this->log("****************************");
                    $this->log("* RESULT FOR OmniusCompatibilityRules ITERATION: " . $iterations);
                    $this->log("****************************");

                    // dump current selected products to log
                    $this->dumpToLog($currentIterationProducts);
                } else {
                    // OMNVFDE-2495
                    $this->log("****************************");
                    $this->log("* Execute Omnius Service Expression Rules - as no products are selected");
                    $this->log("****************************");

                    // Checking the service expression rules for NOT ALLOWED rules (not cached because these depend on customer and serviceability conditions)
                    /** @var Dyna_ProductMatchRule_Model_Rule $ruleModel */
                    $ruleModel = Mage::getSingleton('dyna_productmatchrule/rule');
                    // Allowed and Not Allowed by service rules
                    $omniusAllowedProducts = array_map('strval', array_keys($ruleModel->loadAllowedServiceRules([], $websiteId, $type)));
                    $omniusNotAllowedProducts = array_map('strval', array_keys($ruleModel->loadAllowedServiceRules([], $websiteId, $type, true)));

                    $omniusAllowedProductsCount = $this->getArrayCount($omniusAllowedProducts) ;
                    $omniusNotAllowedProductsCount = $this->getArrayCount($omniusNotAllowedProducts);

                    if ($omniusAllowedProductsCount || $omniusNotAllowedProductsCount) {
                        $omniusAllowedIterationProducts = $this->convertFromIdsToSkus($omniusAllowedProducts);
                        $omniusNotAllowedIterationProducts = $this->convertFromIdsToSkus($omniusNotAllowedProducts);

                        $this->log("*********** Omnius not allowed by service rules *****************");
                        $this->log(var_export($omniusNotAllowedIterationProducts, true));
                        /**
                         * @var  $key
                         * @var Omnius\RulesEnginePlugin\IProductState $cProduct
                         */
                        foreach ($currentIterationProducts as $key => $cProduct) {
                            if (isset($omniusAllowedIterationProducts[$cProduct->getProductId()])) {
                                // Mark products as changeable as it is allowed by a service rule
                                $currentIterationProducts[$key] = $this->gateWay->getProductStateInstance($cProduct->getProductId(), $cProduct->isVisible(), $cProduct->isSelected(), true);
                            } elseif (isset($omniusNotAllowedIterationProducts[$cProduct->getProductId()]) ) {
                                // Mark products as not changeable as it is not allowed by a service rule
                                $currentIterationProducts[$key] = $this->gateWay->getProductStateInstance($cProduct->getProductId(), $cProduct->isVisible(), $cProduct->isSelected(), false);
                            }
                        }

                        $this->log("****************************");
                        $this->log("* RESULT FOR Omnius Service Expression Rules - ITERATION: " . $iterations);

                        // dump current selected products to log
                        $this->dumpToLog($currentIterationProducts);
                    }
                }

                // 3. If states changed, execute (1) again
                if ($statesChanges = $this->areDifferent($previousIterationProducts, $currentIterationProducts)) {
                    $this->log('=== Found differences between input and output products. [Iteration: ' . $iterations . '] ===');
                } else {
                    $this->log('=== [RULES EXECUTION FINISHED] Got same result after applying Cable Precondition and Omnius Rules [Iteration: ' . $iterations . '] ===');
                }

                $iterations++;
            } while ($statesChanges && $iterations <= self::MAX_ITERATIONS);

            $available = [];

            foreach ($currentIterationProducts as $product) {
                if (($product->isVisible() && $product->isChangeable()) || $product->isSelected()) {
                    $available[] = $this->getProductIdBySku($product->getProductId());
                }
            }

            unset($product);

            // Keep in cache all the products returned as available
            $allAvailable = $available;
            foreach ($this->gateWay->allPreconditionStates as $returnedPackage) {
                if ($returnedPackage->getPackageId() != $this->getQuote()->getActivePackageId()) {
                    $packageProductStates = $returnedPackage->getProductSelection()->getCurrentProductStates();
                    foreach ($packageProductStates as $product) {
                        if (($product->isVisible() && $product->isChangeable()) || $product->isSelected()) {
                            $allAvailable[] = $this->getProductIdBySku($product->getProductId());
                        }
                    }
                }
            }
            unset($returnedPackage, $product);

            $this->cacheSave($currentIterationProducts);
            // Free memory @see OVG-2154
            $this->clearCableProducts();
            // EOF Free memory @see OVG-2154
        } else {
            // Not a cable package; Get products compatibility rules
            $available = $this->executeOmniusCompatibilityRules($type, $selectedProducts, $productsArray, $websiteId);
        }

        // Append products of type own to current rules and compatible smartcards and receiver if any found
        $allRules = array_values(array_unique(array_merge($available, $ownHardwareRules)));

        if ($this->checkGateWay($type)) {
            // keep these here, bundle
            $lastIdsKey = $this->getCacheKey('all_precondition_allowed_products');
            $this->cacheSave($allAvailable, $lastIdsKey);
            $this->dumpLog();
        }

        $mandatory = $this->getCartHelper()->getMandatoryWithRules($selectedProducts ?? $nowSelected ?? array(), $allRules, $websiteId);

        return array(
            "rules" => $allRules,
            "mandatory" => $mandatory,
            "removalNotAllowed" => $this->getRemovalNotAllowedIds(),
            "selectableNotAllowedIdsFromContract" => $this->getSelectableNotAllowedIdsFromContract()
        );
    }

    /**
     * Get configurator cart helper
     * @return Dyna_Configurator_Helper_Cart
     */
    protected function getCartHelper()
    {
        if (!$this->cartHelper) {
            $this->cartHelper = Mage::helper('dyna_configurator/cart');
        }

        return $this->cartHelper;
    }

    /**
     * @param $type
     * @return bool
     */
    public function checkGateWay($type)
    {
        if (in_array($type, Dyna_Catalog_Model_Type::getCablePackages())) {
            /** @var Dyna_Configurator_Helper_Rules_Cable $this ->gateWay */
            $this->gateWay = Mage::helper('dyna_configurator/rules_cable');

            return $this->gateWay->isArtifactEnabled();
        }

        return false;
    }

    /**
     * Return a list of allowed products based on cart
     * @return array
     */
    public function getAvailableOwnHardware($selectedProducts)
    {
        $customAllowed = [];

        /** @var Dyna_Customer_Model_Customer $customer */
        $customer = Mage::getModel('customer/session')->getCustomer();

        // Build allowed smart cards list if getTvEquipment service call has been done
        if ($receiver = $customer->getOwnReceiver()) {
            /** @var Dyna_Catalog_Model_Product $blankProduct */
            $blankProduct = Mage::getModel('catalog/product');
            if (!empty($smartSkus = $receiver->getData("SupportedSmartcardList"))) {
                foreach ($smartSkus as $sku) {
                    if ($productId = $blankProduct->getResource()->getIdBySku(trim($sku))) {
                        $customAllowed[] = $productId;
                    }
                }
            }
        }

        foreach ($selectedProducts as $productId) {
            /** @var Dyna_Catalog_Model_Product $product */
            $product = Mage::getModel('catalog/product')
                ->load($productId);

            // If customer has chosen own receiver, add own smartcard product as allowed
            if ($product->isOwnReceiver()) {
                $customAllowed[] = $product->getResource()->getIdBySku(Dyna_Catalog_Model_Type::OWN_SMARTCARD_SKU);
            }

            // If customer has chosen own smartcard, add own receivers to allowed list
            if ($product->isOwnSmartcard()) {
                foreach (Dyna_Catalog_Model_Type::getOwnReceiverSkus() as $ownReceiverSku) {
                    $customAllowed[] = $product->getResource()->getIdBySku($ownReceiverSku);
                }
            }
        }

        return $customAllowed;
    }

    protected function clearCableProducts()
    {
        unset($this->cableProducts);
        return $this;
    }

    /**
     * Get all cable products based on the attribute set name: KD_Cable_Products
     * @return array
     */
    protected function getAllCableProducts($type = null)
    {
        $allCableProducts = array();
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/session')->getQuote();
        $this->log('=== Quote: ' . $quote->getId() . ' ===');

        // Not yet cached at helpers instance level
        if ($type && empty($this->cableProducts[$type])) {
            $key = "all_cable_products_for_" . $type . "_" . Mage::app()->getStore()->getWebsiteId();
            $cachedCableProducts = $this->cacheLoad($key);
            // No cached cable products in redis, try loading them from db
            if ($cachedCableProducts === false) {
                $this->log('=== NO CACHED PRODUCTS FOUND FOR: ' . $type . ', LOADING FROM DB ===');
                /** @var Dyna_Configurator_Model_Catalog $cableProductModel */
                $cableProductModel = Mage::getModel('dyna_configurator/catalog')->setPackageType($type);

                //Load product model collection filtered by attribute set id
                $cableProducts = $cableProductModel->getAllProducts(array(
                    'entity_id',
                    'sku',
                    'marketable',
                    'selectable',
                    'droppable',
                    'relation_components'
                ));
                $this->cableProducts[$type] = count($cableProducts) ? Mage::helper('omnius_configurator')->toArray($cableProducts) : [];
                $this->cacheSave($this->cableProducts[$type], $key);
            } else {
                $this->log('=== USING CACHED PRODUCTS FOR ' . $type . ' ===');
                $this->cableProducts[$type] = unserialize($cachedCableProducts);
            }
        } elseif (!$type) {
            // no package type specified, so cache all products recursively
            foreach (Dyna_Catalog_Model_Type::getCablePackages() as $cablePackageType) {
                // if not set on current instance by type, load them either form cache or from db
                if (empty($this->cableProducts[$cablePackageType])) {
                    // no type provided, load all cable products
                    $this->log('=== BUILD CACHED PRODUCTS FOR ' . $cablePackageType . ' ===');
                    $this->cableProducts[$cablePackageType] = $this->getAllCableProducts($cablePackageType);
                }
                // type not specified, so all products requested, merge them into allCableProducts variable
                $allCableProducts = array_merge($allCableProducts, $this->cableProducts[$cablePackageType]);
            }
        }

        return $type ? $this->cableProducts[$type] : $allCableProducts;
    }

    /**
     * @return Dyna_Cache_Model_Cache
     */
    public function getCache()
    {
        if (!$this->cache) {
            $this->cache = Mage::getSingleton('dyna_cache/cache');
        }
        return $this->cache;
    }

    /**
     * Log messages to custom file: var/log/CableOmniusRules.log
     * @param $message
     */
    public function log($message, $level = Zend_Log::DEBUG)
    {
        if ($this->gateWay->isLoggingForArtifactEnabled()) {
            $this->logData[] = $message;
        }
    }

    public function dumpLog($level = Zend_Log::DEBUG)
    {
        if (!empty($this->logData)) {
            Mage::log(implode(PHP_EOL, $this->logData), $level, 'CableOmniusRules.log', true);
        }
        $this->logData = array();
    }

    /**
     * Return product sku for a given id
     * @param $productId int
     * @return $string
     */
    public function getProductSkuById($productId)
    {
        $mapping = $this->getProductMapping();

        return array_search($productId, $mapping) ?: Mage::getResourceModel('catalog/product')->getProductsSku([$productId])[0]['sku'];
    }

    /**
     * Returns an array of $sku => $id for identifying products
     * @return array
     */
    protected function getProductMapping()
    {
        if (empty($this->productsMapping)) {
            // Check whether or not current package type is a cable one
            $key = 'sku2id_product_mapping_for_ALL';

            // Try loading mapping list from cache
            if (!$this->productsMapping = unserialize($this->cacheLoad($key))) {
                $this->productsMapping = [];
                /** @var array $allCableProducts */
                $allCableProducts = $this->getAllCableProducts();
                foreach ($allCableProducts as &$product) {
                    $this->productsMapping[$product['sku']] = $product['entity_id'];
                }
                /** cache it */
                $this->cacheSave($this->productsMapping, $key);
            }
        }

        return $this->productsMapping;
    }

    /**
     * Returns a caching key formed from all cable packages separated by _
     * @return string
     */
    public function getCablePackagesKey()
    {
        return implode("_", Dyna_Catalog_Model_Type::getCablePackages());
    }

    /**
     * @param $productsArray
     * @param $nowSelected
     * @param $droppableProducts
     * @return array
     */
    protected function getDbProductsStates($productsArray, $nowSelected, $droppableProducts = [])
    {
        $currentIterationProducts = [];

        // Convert array of products to array of product states
        foreach ($productsArray as &$product) {
            if (in_array($product['entity_id'], $nowSelected)) {
                $selected = true;
            } else {
                $selected = false;
            }

            $sku = $product['sku'];
            // Setting default states from catalog for each product
            $visible = (bool)!empty($product['marketable']);

            if (!Mage::registry('ils_process_initiated')) {
                $droppableProducts = [];
            }

            // VFDED1W3S-867: evaluate the droppable flag only when ILS is initiated to alter the changeabe flag
            if (count($droppableProducts) && isset($droppableProducts[$sku])) {
                $changeable = $droppableProducts[$sku];
            } else {
                $changeable = (bool)$product['selectable'];
            }
            $currentIterationProducts[] = $this->gateWay->getProductStateInstance($sku, $visible, $selected, $changeable);
        }

        return $currentIterationProducts;
    }

    /**
     * Custom option of option logic that makes a product available only if another product is selected
     * @param $items array
     * @param array $productList
     * @return array
     * @throws Mage_Core_Model_Store_Exception
     */
    protected function processOptionOfOption($items, $productList = [], $stickToCurrentPackage = true)
    {
        $this->stickToCurrentPackage = $stickToCurrentPackage;

        $selectedIds = $notChangeableProducts = $selectedPerPackage = $selectedItemsPerPackage = $selectedSkus = [];
        if (!$stickToCurrentPackage) {
            $allCableProducts = array();
            foreach ($this->getQuote()->getCartPackages(true) as $package) {
                if (!$package->isCable()) {
                    continue;
                }
                $allCableProducts = array_merge($allCableProducts, $this->getAllCableProducts($package->getType()));
            }
        } else {
            $allCableProducts = $this->getAllCableProducts($this->packageType);
        }

        // Installed base products that can be removed from the current quote
        $droppable = [];
        /** @var Dyna_Checkout_Model_Sales_Quote_Item $selectedItem */
        foreach ($items as $selectedItem) {
            $selectedPackageCreationTypeIds[$selectedItem->getProductId()] = $selectedItem->getPackageCreationTypeId();
            $selectedIds[$selectedItem->getProductId()] = $selectedItem->getPackageType();
            $selectedSkus[$selectedItem->getProductId()] = $selectedItem->getSku();

            foreach ($allCableProducts as &$cableProduct) {
                if ($cableProduct['sku'] == $selectedItem->getSku()
                    && !$cableProduct['selectable']
                ) {
                    $notChangeableProducts[$cableProduct['entity_id']] = $cableProduct['sku'];
                }
            }

            if ($selectedItem->isContract()) {
                $droppable[$selectedItem->getSku()] = (bool)$selectedItem->getProduct()->getDroppable();
            }

            $selectedPerPackage[$selectedItem->getPackageType()][$selectedItem->getSku()] = $selectedItem->getProductId();
            // hold reference to item by package type and product id
            $selectedItemsPerPackage[$selectedItem->getPackageType()][$selectedItem->getProductId()] = $selectedItem;
        }

        // Get option of option products: OMNVFDE-1421
        $productOptions = $this->getProductOptions($selectedIds);
        $notChangeable = $changeableOptions = [];
        foreach ($selectedIds as $selectedProductId => $packageType) {
            if (isset($notChangeableProducts[$selectedProductId]) && !isset($productOptions[$selectedSkus[$selectedProductId]])
                && in_array($selectedPackageCreationTypeIds[$selectedProductId], Dyna_Catalog_Model_Type::getOptionOfOptionRemovalPackages())
            ) {
                $notChangeable[] = $selectedProductId;
                $this->log("==> PRODUCT " . $notChangeableProducts[$selectedProductId] . " IS REMOVED FROM SELECTED LIST AS IT HAS SELECTABLE = FALSE");
                unset($selectedIds[$selectedProductId]);

                foreach ($selectedPerPackage as $packageType => $dataItem) {
                    if (in_array($selectedProductId, $dataItem)) {
                        unset($selectedPerPackage[$packageType][array_search($selectedProductId, $dataItem)]);
                        $selectedItemsPerPackage[$packageType][$selectedProductId]->isDeleted(true);
                    }
                }
            }
        }

        foreach ($productOptions as &$sku) {
            if (!empty($sku)) {
                $changeableOptions[] = $sku;
            }
        }

        // Precondition rules are sending product list as parameter after a certain iteration
        $foundInCache = $this->cacheLoad();
        if (!empty($foundInCache)) {
            $cachedProductList = unserialize($foundInCache);
        } else {
            $cachedProductList = $this->getDbProductsStates($allCableProducts, [], $droppable);
        }

        $productList = !empty($productList) ? $productList : ($cachedProductList ?: []);

        $this->log("****************************");
        $this->log("* BEFORE OPTION-OF-OPTION ITERATION: ");
        $this->log("****************************");

        // dump current selected products to log
        $this->dumpToLog($productList);

        foreach ($productList as &$productState) {
            $sku = $productState->getProductId();
            $visible = $productState->isVisible();
            $id = $this->getProductIdBySku($sku);
            $selected = isset($selectedIds[$id]);

            if (in_array($sku, $changeableOptions)) {
                $changeable = true;
            } else {
                $changeable = in_array($sku, $notChangeable) ? false : $productState->isChangeable();
            }

            $productState = $this->gateWay->getProductStateInstance($sku, $visible, $selected, $changeable);
        }

        unset($productState);

        $this->cartProducts = $selectedIds;

        $this->log("****************************");
        $this->log("* AFTER OPTION-OF-OPTION ITERATION: ");
        $this->log("****************************");
        // dump current selected products to log
        $this->dumpToLog($productList);

        return $productList;
    }

    /**
     * Create cache key
     * @param $packageType string
     * @return string
     */
    public function getCacheKey($packageType = null)
    {
        if ($this->stickToCurrentPackage == true && !$packageType) {
            $packageType = $this->packageType;
        }
        $quoteId = Mage::getSingleton('checkout/session')->getQuoteId();
        $addressId = Mage::getSingleton('dyna_address/storage')->getAddressId();
        $sessionId = Mage::getSingleton("core/session")->getEncryptedSessionId();

        return sprintf('precondition_rules_products_%s_%s_%s_%s', (string)$packageType, $quoteId, $addressId, $sessionId);
    }

    /**
     * @param array $productIds
     * @param int $storeId
     * @return array
     * @internal param $quote
     */
    public function getProductOptions(array $productIds, int $storeId = null): array
    {
        ksort($productIds);
        $optionsKey = $storeId . "_OPTION_OF_OPTION_FOR_" . implode("_", array_keys($productIds));
        if (!$result = unserialize($this->cacheLoad($optionsKey))) {
            if (!$storeId) {
                $storeId = Mage::app()->getStore()->getStoreId();
            }
            $relatedOptionIds = [];
            $productCollection = Mage::getModel('catalog/product')
                ->getCollection()
                ->addWebsiteFilter($storeId)
                ->addAttributeToFilter('entity_id', array('in' => array_keys($productIds)))
                ->addAttributeToSelect(array(
                    'relation_options',
                    'relation_preselected_options',
                    'relation_mandatory_options',
                    'relation_components',
                    'relation_devices',
                    'relation_fees',
                ));
            foreach ($productCollection as $product) {
                $relationOptions = explode(',', trim($product->getRelationOptions()));
                $relationPreselectedOptions = explode(',', trim($product->getRelationPreselectedOptions()));
                $relationMandatoryOptions = explode(',', trim($product->getRelationMandatoryOptions()));
                $relationComponents = explode(',', trim($product->getRelationComponents()));
                $relationDevices = explode(',', trim($product->getRelationDevices()));
                $relationFees = explode(',', trim($product->getRelationFees()));

                $relatedOptionIds = array_merge(
                    $relatedOptionIds,
                    $relationOptions,
                    $relationPreselectedOptions,
                    $relationMandatoryOptions,
                    $relationComponents,
                    $relationDevices,
                    $relationFees
                );
            }

            $result = count($relatedOptionIds) ? array_unique(array_filter($relatedOptionIds)) : array();
            $this->cacheSave($result, $optionsKey);
        }

        return $result;
    }

    /**
     * Execute Omnius Compatibility Rules
     * @param $productIds
     * @return array
     */
    protected function executeOmniusCompatibilityRules(string $packageType, $productIds, $allProducts, $websiteId)
    {
        $cartHelper = $this->getCartHelper();
        if (count($allProducts)) {
            $allProducts = [$this->packageType => $allProducts];
        } else {
            $allProducts = [];
        }
        $availableProducts = $cartHelper->getCartRules($productIds, $allProducts, $websiteId, false, $packageType);
        if (empty($availableProducts) && in_array($packageType, Dyna_Catalog_Model_Type::getCablePackages())) {
            $this->log('=== Omnius Compatibility Rules returned that there are no products visible/available ===');
        }

        return $availableProducts;
    }

    /**
     * Helper method to convert an array of product ids (returned by OmniusCompatibilityRules) in an array of [SKU => ProductID]
     * @param array $omniusAllowedProducts
     * @return array
     */
    protected function convertFromIdsToSkus($omniusAllowedProducts)
    {
        $omniusIterationProducts = [];
        if (empty($omniusAllowedProducts)) {
            $this->log('=== No remaining products available in current conditions after executeOmniusCompatibilityRules ===');
        } else {
            foreach ($omniusAllowedProducts as &$productId) {
                $sku = $this->getProductSkuById($productId);
                $omniusIterationProducts[$sku] = $productId;
            }
        }

        return $omniusIterationProducts;
    }

    /**
     * @param $productStateList
     */
    protected function dumpToLog($productStateList, $onlyCount = true)
    {
        if (!$this->gateWay->isLoggingForArtifactEnabled()) {
            return;
        }
        if ($productStateList && is_array($productStateList)) {
            $this->log("Number of all database products: " . count($productStateList));
        } else {
            $this->log("[ERROR] No database products.");
        }

        $visible = $selected = $changeable = 0;
        $selectedProducts = [];

        if ($productStateList) {
            foreach ($productStateList as &$productState) {
                if ($productState->isVisible()) {
                    $visible++;
                }
                if ($productState->isSelected()) {
                    $sku = $productState->getProductId();

                    if (!isset($selectedProducts[$sku])) {
                        $selectedProducts[$sku] = $sku;
                    }
                    $selected++;
                }
                if ($productState->isChangeable()) {
                    $changeable++;
                }

                if (!$onlyCount) {
                    $text = '';
                    $text .= "\t" . $productState->getProductId();
                    $text .= " [" . (int)$productState->isVisible();
                    $text .= " : " . (int)$productState->isSelected();
                    $text .= " : " . (int)$productState->isChangeable();
                    $text .= "]";
                    $this->log($text);
                }
            }
        }

        $this->log(">>> Visible: " . $visible);
        $this->log(">>> Selected: " . $selected . ' [' . implode(', ', $selectedProducts) . ']');
        $this->log(">>> Changeable: " . $changeable);
    }

    /**
     * This method will call executePreconditionRules() set on gateWay
     * @todo implement needed input (IRulesInput) to be parsed to IOmniusRules->executePreConditionRules
     * @param $productsList Omnius\RulesEnginePlugin\IProductState []
     * @return array of product ids
     */
    protected function executeCablePreconditionRules($productsList)
    {
        $this->gateWay->setCurrentProductStates($productsList);
        $this->gateWay->setPackageType($this->packageType);

        return $this->gateWay->executePreconditionRules();
    }

    /**
     * @param $initialProducts Omnius\RulesEnginePlugin\IProductState[]
     * @param $processedProducts Omnius\RulesEnginePlugin\IProductState[]
     * @return boolean;
     */
    protected function areDifferent($initialProducts, $processedProducts)
    {
        if (count($initialProducts) != count($processedProducts)) {
            return true;
        }

        /** Building arrays for both input and output */
        $initialArray = [];
        foreach ($initialProducts as &$productState) {
            $initialArray[$productState->getProductId()]['visible'] = $productState->isVisible();
            $initialArray[$productState->getProductId()]['changeable'] = $productState->isChangeable();
            $initialArray[$productState->getProductId()]['selected'] = $productState->isSelected();
        }

        /** Building arrays for both input and output */
        $processedArray = [];
        if ($processedProducts) {
            foreach ($processedProducts as &$productState) {
                $processedArray[$productState->getProductId()]['visible'] = $productState->isVisible();
                $processedArray[$productState->getProductId()]['changeable'] = $productState->isChangeable();
                $processedArray[$productState->getProductId()]['selected'] = $productState->isSelected();
            }
        }

        ksort($initialArray);
        ksort($processedArray);

        return !($initialArray == $processedArray);
    }

    /**
     * Return product id for a given sku
     * @param $productSku string
     * @return $int
     */
    public function getProductIdBySku($productSku)
    {
        $mapping = $this->getProductMapping();
        return isset($mapping[$productSku]) ? $mapping[$productSku] : $productSku;
    }

    /**
     * Method is called in addMulti after postcondition rules
     * @return array
     */
    public function processCableArtifactPreconditionRules()
    {
        $available = [];

        if ($inputProducts = unserialize($this->cacheLoad())) {

            // 1. Executing Cable Precondition Rules
            $currentIterationProducts = $this->executeCablePreconditionRules($inputProducts);
            if (empty($currentIterationProducts)) {
                $this->log('=== No products returned by cable precondition rules ===');
            } else {
                foreach ($currentIterationProducts as $product) {
                    if (($product->isVisible() && $product->isChangeable()) || $product->isSelected()) {
                        $available[] = $this->getProductIdBySku($product->getProductId());
                    }
                }
            }

            $this->cacheSave($currentIterationProducts);
        }

        return $available;
    }

    /*
     * Convert a list of product states to a list of ids
     * Method will ignore products not visible
     */

    /**
     * Check whether or not a product is mandatory after executing Omnius compatibility rules
     */
    public function isProductMandatory($productId)
    {
        return true;
    }

    /**
     * Returns an array of cable package ids found in current quote
     * @param null $forPackageType
     * @return array
     */
    public function getQuoteCablePackageId($forPackageType = null)
    {
        $cablePackageIds = array();
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = $this->getQuote();
        foreach ($quote->getCartPackages() as $package) {
            // checking whether or not this package's type is in the cable list
            if ($package->isCable() && (!$forPackageType || $forPackageType && strtolower($forPackageType) === strtolower($package->getType()))) {
                // checking if there is a restriction for a certain package type
                $cablePackageIds[] = $package->getPackageId();
            }
        }

        return $cablePackageIds;
    }

    /**
     * Returns the quote cable package id of a certain type
     * @assumption never two cable packages of type Cable Internet or Phone or Cable TV in same order (will server the first found one otherwise)
     * @assumption if Artifact will alter interstack KUD package (which in wave2 will be 99 packages allowed) only the first found one will be altered
     * @return int|null
     */
    public function getQuoteCablePackageIdByType($packageType)
    {
        foreach ($this->getQuote()->getCartPackages() as $package) {
            // checking whether or not this package's type is in the cable list
            if ($package->isCable() && strtolower($package->getType()) === strtolower($packageType)) {
                return $package->getPackageId();
            }
        }

        return null;
    }

    /**
     * Return current session quote
     * @return Dyna_Checkout_Model_Sales_Quote
     */
    public function getQuote()
    {
        return Mage::getSingleton('checkout/cart')->getQuote();
    }

    /*
     * Convert a list of product ids to a list of productStates
     * Method will ignore products not visible
     */

    /**
     * @param Dyna_Checkout_Model_Sales_Quote $quote
     */
    public function executeCablePostRules($quote, $packageType = null, $stickToCurrentPackage = true)
    {
        $this->packageType = $packageType;
        if (!$this->checkGateWay($packageType)) {
            return;
        }
        // Send refreshed quote items_collection
        $items = $quote->getAllItems();

        $optionsOfOptions = $this->processOptionOfOption($items, [], $stickToCurrentPackage);
        $productList = &$optionsOfOptions;

        $this->log('=== CABLE POST CONDITION INPUT FOR: ' . strtoupper($packageType) . ' (selected products) ===');
        $this->dumpToLog($productList);

        try {
            /** @var array $results */
            $results = $this->executeCablePostconditionRules($productList);
        } catch (Exception $e) {
            $this->log('Artifact executeCablePostconditionRules failed with message: ' . $e->getMessage());
            Mage::logException($e);
            $results = [];
        }

        $selectedProducts = [];

        $this->log('=== CABLE POST CONDITION RESULT ===');
        foreach ($results as $returnedPackageType => $result) {
            $this->log('=== RESULT FOR PACKAGE TYPE ' . strtoupper($returnedPackageType) . ' ===');
            $this->dumpToLog($result);
        }

        foreach ($results as $returnedPackageType => &$packageStates) {
            foreach ($packageStates as &$result) {
                if ($result->isSelected()) {
                    $selectedProducts[$returnedPackageType][] = $this->getProductIdBySku($result->getProductId());
                }
            }
        }

        $this->postIterations++;

        // Set all the new products as selected
        // Response includes products other packages
        $this->cartProducts = array();
        foreach ($selectedProducts as $packType => $productIds) {
            foreach ($productIds as $productId) {
                $this->cartProducts[$productId] = $packType;
            }
        }

        // Proceed with results for active package
        $results = &$results[$quote->getCartPackage()->getType()];
        if ($this->areDifferent($productList, $results) && $this->postIterations <= self::MAX_ITERATIONS) {
            unset ($productList);
            $this->processPostCart();
            $quote->setNeedsRecollectPostTotals(true);

            $this->log('=== Processing continues ===');
        } else {
            if (empty($this->cartProducts)) {
                $this->log('==== No products remain in cart ===');
                $items = $quote->getAllItems();
                foreach ($items as $item) {
                    if (in_array(strtolower($item->getPackageType()), Dyna_Catalog_Model_Type::getCablePackages()) && !isset($this->cartProducts[$item->getProductId()])) {
                        // Removing product because it is not in the selected items result
                        $item->isDeleted(1);
                        $this->log('=== REMOVED FROM CART PROD WITH SKU: ' . $item->getSku() . ' ===');
                    }
                }
            } else {
                unset ($productList);
                $this->processPostCart();
            }

            // Collect the final totals but skip cable post conditions rules
            $quote->setNeedsRecollectPostTotals(true)
                ->setSkipCablePostConditions(true);

            $this->log('=== Processing stops ===');

            if ($this->postIterations >= self::MAX_ITERATIONS) {
                $this->log(sprintf('=== Max iterations (%d) reached ===', self::MAX_ITERATIONS));
            }
        }

        $this->cacheSave($results);
        unset($results);
        $this->clearCableProducts();
        $this->dumpLog();
    }

    /**
     * Method that adds/removes items to cart based on the result of PostCondition response
     * @param Dyna_Checkout_Model_Sales_Quote $quote
     */
    protected function processPostCart()
    {
        /** @var Dyna_Checkout_Model_Cart $cart */
        $cart = Mage::getSingleton('checkout/cart');
        $quote = $this->getQuote();

        if ($this->cartProducts) {
            $cartSkus = [];
            foreach ($this->cartProducts as $productId => $packageType) {
                $cartSkus[] = $this->getProductSkuById($productId);
            }
            $this->log("=== THE FOLLOWING PRODUCTS SHOULD BE PRESENT IN CART AFTER CABLE POST CONDITION: " . implode(', ', $cartSkus));
            unset($cartSkus);
        }

        // Aligning cart items with post condition rules result
        $previousProductIds = [];
        $items = $quote->getAllItems();
        foreach ($items as $item) {
            // Check only cable products
            if (in_array(strtolower($item->getPackageType()), Dyna_Catalog_Model_Type::getCablePackages())) {
                // Removing product because it is not in the selected items result
                if (!isset($this->cartProducts[$item->getProductId()])) {
                    $item->isDeleted(1);
                    $item->setSystemAction(Dyna_Checkout_Model_Sales_Quote_Item::SYSTEM_ACTION_REMOVE);
                    $this->log('=== REMOVED FROM CART PROD WITH SKU: ' . $item->getSku() . ' ===');
                } else {
                    // Remains in cart
                    $previousProductIds[] = $item->getProductId();
                    $this->log('=== REMAINS IN CART PROD WITH SKU: ' . $item->getSku() . ' ===');
                }
            }
        }

        foreach ($this->cartProducts as $addId => $packType) {
            $addSku = $this->getProductSkuById($addId);
            if (!in_array($addId, $previousProductIds)) {
                $prod = Mage::getModel('catalog/product')->load($addId);
                // adding product to cart based on it's package type, assuming that no cable product will ever be assigned to two package types
                $prodPackageTypeId = $this->getQuoteCablePackageIdByType($packType);
                if (!$prodPackageTypeId) {
                    $this->log('=== SKIPPING PROD WITH SKU: ' . $addSku . ' === Unable to determine the correct package id in which to add ===');
                    continue;
                }
                $this->log('=== ADD PROD WITH SKU: ' . $addSku . ' ===');
                /** @var Dyna_Checkout_Model_Sales_Quote_Item $item */
                // quote will set the active package id so we need to override it in this case with the corresponding cable package package id with this quote's item product type
                // no need for item save, quote will do the job
                $item = $cart->addProduct($addId, 1);

                if (is_string($item)) {
                    $this->log('=== EXCEPTION WHEN ADDING PROD WITH SKU: ' . $prod->getSku() . ' === ' . $item);
                } else {
                    $item
                        ->setSystemAction('add')
                        ->setPackageId($prodPackageTypeId)
                        ->setQuote($quote)
                        ->setPackageType(strtolower($packType));
                }
            }
        }
    }

    /**
     * @param $productList
     * @return array|\Omnius\RulesEnginePlugin\IProductState[]
     */
    protected function executeCablePostconditionRules(&$productList)
    {
        /** @todo Implement setCurrentProductStates on IRules input */
        $this->gateWay->setCurrentProductStates($productList);
        $this->gateWay->setPackageType($this->packageType);

        return $this->gateWay->executePostconditionRules();
    }

    /**
     * Check if given type is one of DSL or LTE
     *
     * @param string $type
     * @return bool
     */
    public function checkFixedPackages(string $type)
    {
        return in_array($type, Dyna_Catalog_Model_Type::getFixedPackages());
    }

    /**
     * Needed for precondition and post condition rules processing
     * @param $productId
     * @return bool
     */
    public function isProductInCart($productId)
    {
        return isset($this->cartProducts[$productId]);
    }

    /**
     * Save cached data based on key / package type / or none = active package
     * These caching methods are public because the gateway needs to access them for saving the Artifact response
     * @param $value
     * @param null $key A specific cache key
     */
    public function cacheSave($value, $key = null)
    {
        $this->getCache()->save(
            serialize($value),
            ($key !== null ? $key : $this->getCacheKey()),
            [Dyna_Cache_Model_Cache::PRODUCT_TAG],
            self::CACHE_TTL
        );
    }

    /**
     * Load cached data based on key / package type / or none = active package
     * These caching methods are public because the gateway needs to access them for saving the Artifact response
     * @param null $key
     * @return bool|false|Mage_Core_Model_Abstract|mixed
     */
    public function cacheLoad($key = null)
    {
        $key = ($key !== null ? $key : $this->getCacheKey());
        return $this->getCache()->load($key);
    }

    /**
     * Get product ids that cannot be removed from cart
     * @return array
     */
    public function getRemovalNotAllowedIds()
    {
        $mandatoryIds = array();

        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        /** @var Dyna_Catalog_Model_ProcessContext $processContext */
        $processContext = Mage::getModel('dyna_catalog/processContext');
        /** @var Dyna_ProductMatchRule_Helper_Data $productMatchHelper */
        $productMatchHelper = Mage::helper('dyna_productmatchrule');
        if ($activePackage = $quote->getCartPackage()) {
            $noManualOverrideAllowed = array();
            // Getting all quote items
            $cartProductIds = array();
            // In order to apply removal not allowed rules on installed base products - VFDED1W3S-51
            $includeAlternateItems = strtolower($activePackage->getType()) == strtolower(Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_DSL) &&
                Mage::getSingleton('customer/session')->getCustomer()->getIsInMinimumDuration($activePackage->getParentAccountNumber());

            foreach ($activePackage->getAllItems($includeAlternateItems) as $item) {
                $cartProductIds[] = $item->getProductId();

                // VFDED1W3S-376: For all process contexts, except ACQ and MIGCOC, it is NOT allowed to manually remove a product that was automatically added
                if ($item->getIsDefaulted()
                    && !in_array($activePackage->getSaleType(), [Dyna_Catalog_Model_ProcessContext::ACQ, Dyna_Catalog_Model_ProcessContext::MIGCOC])
                ) {
                    if ($item->getProduct()->getPackageSubtypeModel()->getManualOverrideAllowed() == 0) {
                        $noManualOverrideAllowed[] = $item->getProductId();
                    }
                }
            }
            // If cart is empty return an empty array
            if (empty($cartProductIds)) {
                return $mandatoryIds;
            }

            $processContextId = $processContext->getProcessContextIdByCode($activePackage->getSaleType());
            // Apply legacy compatibility rules
            $omniusIds = $productMatchHelper->getOmniusNotRemovableIds($cartProductIds, $processContextId);

            // apply service expression rules
            $serviceIds = $productMatchHelper->getServiceNotRemovableIds(
                $cartProductIds,
                $processContextId,
                $activePackage->getType()
            );

            // Javascript expects array, this response will reach frontend, using array values to reset keys
            $mandatoryIds = array_values(
                array_intersect($cartProductIds,
                    array_unique(array_merge($omniusIds, $serviceIds, $noManualOverrideAllowed))
                )
            );
            unset ($omniusIds, $serviceIds);
        }

        return $mandatoryIds;
    }

    /**
     * Get product ids removed by the system and that cannot be added by user in
     * ILS, PROLONGATION and PRE2POST scenarios
     * @return array
     */
    public function getSelectableNotAllowedIdsFromContract()
    {
        $resultIds = array();

        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();

        if ($activePackage = $quote->getCartPackage()) {
            if (!in_array($activePackage->getSaleType(),
                [Dyna_Catalog_Model_ProcessContext::ACQ, Dyna_Catalog_Model_ProcessContext::MIGCOC])) {
                /** @var Dyna_Checkout_Model_Sales_Quote_Item $item */
                foreach ($activePackage->getAlternateItems() as $item) {
                    // Option automatically removed by the system
                    if ($item->getIsContractDrop() && $item->getSystemAction() == 'existing') {
                        // VFDED1W3S-376: If manual override is NOT allowed, than add it to NOT allowed list
                        if ($item->getProduct()->getPackageSubtypeModel()->getManualOverrideAllowed() == 0) {
                            $resultIds[] = $item->getProductId();
                        }
                    }
                }
            }
        }

        return $resultIds;
    }


    /**
     * When executing precodition rules without quote packages we need to create faux quote items from the product ids we check for compatibility
     * @param bool $value
     */
    public function setNeedsFauxQuote($value = true)
    {
        $this->needsFauxQuote = $value;
    }

    private function getArrayCount($dataArray)
    {
        return count($dataArray);

    }
}
