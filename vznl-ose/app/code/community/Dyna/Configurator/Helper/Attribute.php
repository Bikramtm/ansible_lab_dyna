<?php


class Dyna_Configurator_Helper_Attribute extends Omnius_Configurator_Helper_Attribute {

    /**
     * @param null $consumerType
     * @return string
     */
    public function getSubscriptionIdentifier($consumerType = null)
    {
        if ( ! $consumerType ) {
            $consumerType = Dyna_Catalog_Model_Product::IDENTIFIER_CONSUMER;
        }

        $key = sprintf('identifier_consumer_%s', $consumerType);

        if ($result = $this->getCache()->load($key)) {
            return $result;
        } else {
            $attribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', Dyna_Catalog_Model_Product::CATALOG_CONSUMER_TYPE);
            $consValue = (string)Mage::helper('dyna_catalog')->getAttributeAdminLabel($attribute, null, $consumerType);
            $this->getCache()->save($consValue, $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
            return $consValue;
        }
    }
}
