<?php

use Omnius\RulesEngineCode\Input\RulesInput;
use Omnius\RulesEngineCode\Input\InstalledBaseServiceLine;
use Omnius\RulesEngineCode\Input\Hardware\Hardware;
use Omnius\RulesEngineCode\Input\Hardware\Modem;
use Omnius\RulesEngineCode\Input\Hardware\Receiver;
use Omnius\RulesEngineCode\Input\Hardware\Smartcard;
use Omnius\RulesEngineCode\Input\Hardware\SmartDvr;
use Omnius\RulesEnginePlugin\Input\ICustomer;
use Omnius\RulesEnginePlugin\Input\IServiceAddress;

/**
 * Class Dyna_Configurator_Helper_Rules_Input
 */
class Dyna_Configurator_Helper_Rules_Input extends Mage_Core_Helper_Abstract
{
    const DEVICE_TYPE_MODEM = 'CABLEMODEM';
    const DEVICE_TYPE_RECEIVER = 'RECEIVER';
    const DEVICE_TYPE_SMARTCARD = 'SMARTCARD';
    const DEVICE_TYPE_SMARTDVR = 'SMARTDVR';
    const FEATURE_VOD = 'VOD';
    const FEATURE_IP_VOD = 'IP_VOD';
    const FEATURE_HD = 'HD';
    const FEATURE_HD_FREE = 'HDFree';
    const FEATURE_FSK16 = 'FSK16';
    const FEATURE_HARDDRIVE = 'harddrive';
    const FEATURE_MAX_BANDWIDTH = 'maxBandwidth';
    const FEATURE_VOICE = 'voice';
    const FEATURE_WLAN = 'wlan';
    const FEATURE_HOTSPOT = 'hotspot';
    const FEATURE_HOMESPOT = 'homespot';
    const FEATURE_HOMEBOX_TO_LIGHT = 'homebox2light';
    const OWNERSHIP_OWN_DEVICE_VALUE = 'customer';

    public function build($packages)
    {
        /** @var Dyna_Address_Model_Storage $sessionAddress */
        $sessionAddress = Mage::getSingleton('dyna_address/storage');
        // VFDED1W3S-384
        $customerProducts = Mage::getSingleton('customer/session')->getCustomerProducts();

        $serviceAbility = $sessionAddress->getServiceAbility()['services'];

        $inputMarketabilityMap = [
            IServiceAddress::SERVICE_CATEGORY_KAD => $serviceAbility[IServiceAddress::SERVICE_CATEGORY_KAD]['status'] ?? null,
            IServiceAddress::SERVICE_CATEGORY_KAA => $serviceAbility[IServiceAddress::SERVICE_CATEGORY_KAA]['status'] ?? null,
            IServiceAddress::SERVICE_CATEGORY_KAI => $serviceAbility[IServiceAddress::SERVICE_CATEGORY_KAI]['status'] ?? null,
            IServiceAddress::SERVICE_CATEGORY_KUD => $serviceAbility[IServiceAddress::SERVICE_CATEGORY_KUD]['status'] ?? null,
        ];

        $bandwidth = (int)$serviceAbility['KIP']['bandwidth'];

        $MMDAvailable = (bool)$sessionAddress->getMMDAvailable();
        $VODAvailable = (bool)$sessionAddress->getVODAvailable();
        $selfInstallAvailable = (bool)$sessionAddress->getSelfInstallAvailable();
        $satAvailable = (bool)$sessionAddress->getSatAvailable();
        $serviceObjectClass = (string)$sessionAddress->getServiceObjectClass();
        $networkOperators = $sessionAddress->getNetworkOperators();
        $objectTenantBonus = []; // OMNVFDE-2862

        // It is set in the CartController::getRulesAction
        $customerType = Mage::registry('getRules__customer_type');
        if ($customerType == 1) {
            $customerType = ICustomer::TYPE_LEGAL_PERSON;
        } elseif ($customerType == 0) {
            $customerType = ICustomer::TYPE_PRIVATE_PERSON;
        }

        /** @var Dyna_Address_Model_Storage $addressStorage */
        $addressStorage = Mage::getModel('dyna_address/storage');
        $serviceAddressId = $addressStorage->getAddressId();
        $firstCustomerId = null;

        // VFDED1W3S-434
        $installedBaseServiceLines = [];
        if (isset($customerProducts) && isset($customerProducts[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD])) {
            foreach ($customerProducts[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD] as $customerId => $customerDetails) {
                foreach ($customerDetails['contracts'] as $contract) {
                    // VFDED1W3S-3138: If same address as serviceability address take serviceLineId from first customer if there are linked customers
                    if ($contract['service_address']['address_id']==$serviceAddressId) {
                        if (is_null($firstCustomerId)) {
                            $firstCustomerId = $customerId;

                            /** @var Dyna_Checkout_Model_Sales_Quote $quote */
                            $quote = Mage::getSingleton('checkout/cart')->getQuote();
                            foreach ($quote->getCartPackages() as $package) {
                                if (in_array($package->getType(), Dyna_Catalog_Model_Type::getCablePackages())) {
                                    if (empty($package->getParentAccountNumber())) { // set Parent Account Number only if it is NOT set.
                                        $package->setParentAccountNumber($firstCustomerId);
                                        $package->save();
                                    }
                                }
                            }
                        } elseif ($firstCustomerId != $customerId) {
                            break;
                        }

                        foreach ($contract['subscriptions'] as $subscription) {
                            foreach ($subscription['products'] as $category => $products) {
                                foreach ($products as $product) {
                                    $installedBaseServiceLine = (new InstalledBaseServiceLine())
                                        ->withServiceLineId($product['product_id'])
                                        ->withServiceLineCategory($category)
                                        ->withInstalledBaseProducts(explode(',', $product['mapped_skus']));

                                    // VFDED1W3S-601
                                    $hardware = (new Hardware());
                                    foreach ($product['devices'] as $devices) {
                                        foreach ($devices as $device) {
                                            if (isset($device['feature_list_type'])) {
                                                $featureTypeList = array_column($device['feature_list_type'], 'FeatureList', 'FeatureType');
                                                $enabledFeatures = array_column($featureTypeList[Dyna_Customer_Model_Client_RetrieveCustomerInfo::KD_DEVICE_ENABLED_FEATURES], 'FeatureValue', 'FeatureName');
                                                $ownDevice = $device['ownership'] == self::OWNERSHIP_OWN_DEVICE_VALUE;

                                                if (strtolower($device['device_type']) == strtolower(self::DEVICE_TYPE_MODEM)) {
                                                    $modem = (new Modem())
                                                        ->setEquipmentType($device['equipment_type'] ?? '')
                                                        ->setSupportsWlan($enabledFeatures[self::FEATURE_WLAN] ?? false)
                                                        ->setSupportsHomespot($enabledFeatures[self::FEATURE_HOMESPOT] ?? false)
                                                        ->setIsHomebox2Light($enabledFeatures[self::FEATURE_HOMEBOX_TO_LIGHT] ?? false)
                                                        ->setIsOwnModem($ownDevice)
                                                        ->setMaxBandwidth((int)$enabledFeatures[self::FEATURE_MAX_BANDWIDTH]);

                                                    $hardware->setModem($modem);
                                                } elseif (strtolower($device['device_type']) == strtolower(self::DEVICE_TYPE_RECEIVER)) {
                                                    $receiver = (new Receiver())
                                                        ->setEquipmentType($device['equipment_type'] ?? '')
                                                        ->setSupportsVod($enabledFeatures[self::FEATURE_VOD] ?? false)
                                                        ->setSupportsHd($enabledFeatures[self::FEATURE_HD] ?? false)
                                                        ->setSupportsHdFree($enabledFeatures[self::FEATURE_HD_FREE] ?? false)
                                                        ->setOwnReceiver($ownDevice);

                                                    $hardware->setReceiver($receiver);
                                                } elseif (strtolower($device['device_type']) == strtolower(self::DEVICE_TYPE_SMARTCARD)) {
                                                    $smartcard = (new Smartcard())
                                                        ->setSupportsHd($enabledFeatures[self::FEATURE_HD] ?? false)
                                                        ->setIsOwnSmartcard($ownDevice);

                                                    $hardware->setSmartcard($smartcard);
                                                } elseif (strtolower($device['device_type']) == strtolower(self::DEVICE_TYPE_SMARTDVR)) {
                                                    $smartDvr = (new SmartDvr())
                                                        ->setEquipmentType($device['equipment_type'] ?? '')
                                                        ->setSupportsVod($enabledFeatures[self::FEATURE_VOD] ?? false)
                                                        ->setSupportsHd($enabledFeatures[self::FEATURE_HD] ?? false)
                                                        ->setSupportsHdFree($enabledFeatures[self::FEATURE_HD_FREE] ?? false);

                                                    $hardware->setSmartDvr($smartDvr);
                                                }
                                            }
                                        }
                                    }
                                    // VFDED1W3S-601
                                    $installedBaseServiceLine->withHardware($hardware);

                                    $installedBaseServiceLines[] = $installedBaseServiceLine;
                                }
                            }
                        }


                    }
                }
            }
        }

        $rulesInput = (new RulesInput())
                ->withPackages($packages)
                ->withCustomerType($customerType)
                ->withBandwidth($bandwidth)
                ->withMMDAvailable($MMDAvailable)
                ->withVODAvailable($VODAvailable)
                ->withSelfInstallAvailable($selfInstallAvailable)
                ->withSatAvailable($satAvailable)
                ->withMarketability($inputMarketabilityMap)
                ->withNetworkOperators($networkOperators)
                ->withObjectTenantBonus($objectTenantBonus);

        // VFDED1W3S-434
        $rulesInput->withInstalledBaseServiceLines($installedBaseServiceLines);

        if ($serviceObjectClass) {
            $rulesInput->withObjectCategory($serviceObjectClass);
        }

        $rulesInput->build();

        return $rulesInput;
    }
}
