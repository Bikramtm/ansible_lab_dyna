<?php

use CableRulesPlugin\CableRulesPluginFactory;
use CableRulesPlugin\OmniusTypes\CableRulesProductState;
use Omnius\RulesEngineCode\Package\Package;
use Omnius\RulesEngineCode\Package\ProductStateSelection;
use Omnius\RulesEngineCode\Input\Hardware\Hardware;
use Omnius\RulesEngineCode\Input\Hardware\Smartcard;
use Omnius\RulesEngineCode\Input\Hardware\Receiver;

class Dyna_Configurator_Helper_Rules_Cable extends Mage_Core_Helper_Abstract
{
    const ARTIFACT_FILE = 'cable-rules-plugin.phar';
    const ARTIFACT_PATH = 'lib' . DS . self::ARTIFACT_FILE;

    /**
     * @var mixed
     */
    public static $autoloader;
    public $allPreconditionStates = [];

    public function __construct()
    {
        if ($this->isArtifactEnabled() && !self::$autoloader && !self::$autoloader = include_once(sprintf('phar://%s/vendor/autoload.php', Mage::helper('dyna_configurator')->getArtifactPath()))) {
            throw new Exception('Could not load cable artifact');
        }
    }

    public function __destruct()
    {
        if (self::$autoloader) {
            self::$autoloader->unregister();
        }
    }

    /** @var $currentProductStates array|ProductStateSelection */
    protected $currentProductStates = [];
    /** @var $packageType string */
    protected $packageType = '';
    /** @var $hardware Hardware */
    protected $hardware;
    /** @var $ruleHelper Dyna_Configurator_Helper_Rules */
    protected $ruleHelper;
    /** @var $logger Aleron75_Magemonolog_Model_Logger */
    protected $logger = false;
    protected static $cableRulesPlugin = false;

    /**
     * Set current product state selection
     * @param $currentProductStates array|ProductStateSelection
     * @return Dyna_Configurator_Helper_Rules_Cable
     */
    public function setCurrentProductStates(&$currentProductStates)
    {
        $this->currentProductStates = $currentProductStates;

        return $this;
    }

    /**
     * @param $cachePackageType string
     * @return array|ProductStateSelection
     */
    public function getCurrentProductStates($cachePackageType = null)
    {
        // If package type param is provided
        if ($cachePackageType) {
            $helper = $this->getRulesHelper();
            $cachedData = $helper->getCache()->load($helper->getCacheKey($cachePackageType));
            return unserialize($cachedData) ?: [];
        } else {
            return $this->currentProductStates;
        }
    }

    /**
     * No need to keep product states on this instance after executing cable rules
     * Product states are always set on this instance before executing cable rules
     * @see OVG-2154
     * @return $this
     */
    public function clearProductStates()
    {
        unset($this->currentProductStates);
        return $this;
    }

    /**
     * Set the package type
     * @param $type
     * @return $this
     */
    public function setPackageType($type)
    {
        $this->packageType = strtoupper($type);

        return $this;
    }

    /**
     * Return mock up of IOmniusRules
     * @param $productId string
     * @param $visible bool
     * @param $selected bool
     * @param $changeable bool
     * @return Omnius\RulesEnginePlugin\IProductState
     */
    public function getProductStateInstance($productId, $visible = false, $selected = false, $changeable = false)
    {
        return  new CableRulesProductState($productId, $visible, $selected, $changeable);
    }

    /**
     * @return bool|\Omnius\RulesEnginePlugin\IOmniusRules
     */
    public function getCableRulesPlugin()
    {
        if (static::$cableRulesPlugin === false) {
            if ($this->isLoggingForArtifactEnabled()) {
                static::$cableRulesPlugin = CableRulesPluginFactory::createPlugin($this->getLogger());
            } else {
                static::$cableRulesPlugin = CableRulesPluginFactory::createPlugin();
            }
            static::$cableRulesPlugin->activateExtendedRulesLog($this->isExtraLoggingForArtifactEnabled());
        }

        return static::$cableRulesPlugin;
    }

    /**
     * This method calls for CableRulesPlugin\executePreConditionRules() parsing the current state products
     * @return array|\Omnius\RulesEnginePlugin\IProductState[]
     * @throws Exception

     */
    public function executePreconditionRules() : array
    {
        $this->allPreconditionStates = [];
        if (! $productStates = $this->getCurrentProductStates()) {
            throw new \Exception("No product state selection defined.");
        }

        if ($this->isArtifactEnabled()) {
            $this->getLogger()->debug("\n########## Quote " . $this->getQuote()->getId() . ": #########\n");
            $this->getLogger()->debug("\n########## Executing preconditionRules: #########\n");
            $artifactResult = $this->getCableRulesPlugin()->executePreConditionRules($this->getRulesInput());

            // Keep all the artifact response as a reference
            $this->allPreconditionStates = $artifactResult;

            foreach ($artifactResult as &$returnedPackage) {
                if ($returnedPackage->getPackageId() == $this->getQuote()->getActivePackageId()) {
                    $productStates = $returnedPackage->getProductSelection()->getCurrentProductStates();
                    break;
                }
                unset($returnedPackage);
            }
            unset($artifactResult);

            $this->getLogger()->debug("\n########## PreconditionRules executed: #########\n");
        }

        return $productStates;
    }

    /**
     * This method calls for CableRulesPlugin\executePostConditionRules() parsing the current state products
     * @return array|\Omnius\RulesEnginePlugin\IProductState[]
     * @throws Exception
     */
    public function executePostconditionRules()
    {
        if (! $productStates = $this->getCurrentProductStates()) {
            throw new \Exception("No product state selection defined.");
        }

        $responseStates = array();

        if ($this->isArtifactEnabled()) {
            $this->getLogger()->debug("\n########## Quote " . $this->getQuote()->getId() . ": #########\n");
            $this->getLogger()->debug("\n########## Executing postconditionRules: #########\n");
            $artifactResult = $this->getCableRulesPlugin()->executePostConditionRules($this->getRulesInput());
            $cartPackages = $this->getQuote()->getCartPackages();

            foreach ($artifactResult as &$returnedPackage) {
                // Get product states for this returned package
                $productStates = $returnedPackage->getProductSelection()->getCurrentProductStates();

                // Override cached artifact response for all packages (products from packages other than active one might arrive changed in response)
                $package = $cartPackages[$returnedPackage->getPackageId()];
                $cacheKey = $this->getRulesHelper()->getCacheKey($package->getType());
                $this->getRulesHelper()->cacheSave($productStates, $cacheKey);

                $responseStates[$package->getType()] = $productStates;
                unset($productStates);
                unset($returnedPackage);
            }
            unset($artifactResult);

            $this->getLogger()->debug("\n########## PostconditionRules executed: #########\n");
        }

        return $responseStates;
    }

    /**
     * Check if the Cable Rules Artifact is enabled in System-Configuration-Omnius Settings-Default settings-Cable rules artifact settings
     * @return boolean
     */
    public function isArtifactEnabled()
    {
        return Mage::getStoreConfig('omnius_general/cable_rules/artfifact_enabled');
    }

    /**
     * Check if the logging for Cable Rules Artifact is enabled in System-Configuration-Omnius Settings-Default settings-Cable rules artifact settings
     * @return boolean
     */
    public function isLoggingForArtifactEnabled()
    {
        return (bool) Mage::getStoreConfig('omnius_general/cable_rules/artifact_logging');
    }

    /**
     * Check if the extra logging for Cable Rules Artifact is enabled in System-Configuration-Omnius Settings-Default settings-Cable rules artifact settings
     * @return boolean
     */
    public function isExtraLoggingForArtifactEnabled()
    {
        return (bool) Mage::getStoreConfig('omnius_general/cable_rules/extra_logging_enabled');
    }

    /**
     * @return Dyna_Checkout_Model_Sales_Quote
     */
    protected function getQuote()
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        return Mage::getSingleton('checkout/cart')->getQuote();
    }

    /**
     * Setup IRulesInput object
     * The only real data are the catalog products ($this->currentProductStates)) returned by getCurrentProductStates method
     * @return Omnius\RulesEnginePlugin\Input\IRulesInput|Dyna_Configurator_Helper_Rules_Input
     */
    protected function getRulesInput()
    {
        $inputPackages = [];

        $quote = $this->getQuote();
        $packages = $quote->getCartPackages();
        
        if ($packages) {
            foreach ($packages as $packageId => $package) {
                if (! in_array($package->getType(), Dyna_Catalog_Model_Type::getCablePackages())) {
                    continue;
                }
                // Call hardware builder before setting products state for cable rules
                $packageHardware = $this->buildHardware($package);
                $packageType = strtoupper($package->getType());

                if ((int)$packageId == (int)$quote->getActivePackageId()) {
                    $currentProductStates = $this->getCurrentProductStates();
                } else {
                    $currentProductStates = $this->getCurrentProductStates($package->getType());
                }

                $packageInstance = (new Package())
                    ->setPackageId($packageId)
                    ->setPackageType($packageType)
                    ->withCurrentProductStateSelection($currentProductStates)
                    ->setInstalledBaseServiceLineId($package->getServiceLineId())
                    // Set hardware after setting product states
                    ->withHardware($packageHardware)
                    ->withTechnicianNeeded((bool)Mage::getSingleton('customer/session')->getCustomer()->getTechnicianNeeded($package->getType()));

                if ($packageInstance) {
                    $inputPackages[] = $packageInstance;
                }
            }
        }

        /** @var Dyna_Configurator_Helper_Rules_Input $inputHelper */
        $inputHelper = Mage::helper('dyna_configurator/rules_input');

        return $inputHelper->build($inputPackages);
    }

    /**
     * Get active quote and active package and iterate through each package item and set receiver / smartcard and build Hardware instance
     * @param $package Dyna_Package_Model_Package
     * @return Hardware
     */
    public function buildHardware($package)
    {
        // Init new Hardware instance
        $hardware = new Hardware();

        /** @var Dyna_Customer_Model_Customer $customer */
        $customer = \Mage::getSingleton('customer/session')->getCustomer();

        /** @var Dyna_Checkout_Model_Sales_Quote_Item $foundReceiver */
        $foundReceiver = null;
        /** @var Dyna_Checkout_Model_Sales_Quote_Item $foundSmartcard */
        $foundSmartcard = null;

        /** @var Dyna_Checkout_Model_Sales_Quote_Item $item */
        foreach ($package->getItems() as $item) {
            // Deleted items are marked for removal from cart
            if ($item->isDeleted()) {
                continue;
            }
            // Found receiver
            if (!$foundReceiver && (strtoupper($item->getProduct()->getSubType()) == strtoupper(Dyna_Catalog_Model_Type::getCableReceiverSubtype()))) {
                $foundReceiver = $item;
            }
            if (!$foundSmartcard && (strtoupper($item->getProduct()->getSubType()) == strtoupper(Dyna_Catalog_Model_Type::getCableSmartcardSubtype()))) {
                $foundSmartcard = $item;
            }
        }

        // Parse found smartcard if any
        if ($foundSmartcard) {
            $this->getRulesHelper()->log("[WITH SMARTCARD][" . $foundSmartcard->getSku() . "]");

            // Init new smartcard, otherwise Hardware will send a dummy Smartcard instance to cable rules
            $smartcard = new Smartcard();
            // Special case for owned smartcard
            if ($customer->getOwnSmartCard()) {
                $smartcard->setIsOwnSmartcard(true);
                /** @var Dyna_Catalog_Model_Smartcard $customerSmartcard */
                $customerSmartcard = $customer->getOwnSmartCard();
                $smartcard->setSupportsHd($customerSmartcard->supportsHd());
            } else {
                // Let's check into tags attribute for isHD value
                $tags = explode(",", $foundSmartcard->getProduct()->getTags());
                $smartcard->setSupportsHd(in_array("isHD", $tags));
            }

            // Identify smartcard
            $smartcard->setSku($foundSmartcard->getSku());
            $hardware->setSmartcard($smartcard);
        }

        // Parse found receiver
        if ($foundReceiver) {
            $this->getRulesHelper()->log("[WITH RECEIVER][" . $foundReceiver->getSku() . "]");

            $receiver = new Receiver();
            if ($customerReceiver = $customer->getOwnReceiver()) {
                // Check if it owned by customer
                $receiver->setOwnReceiver(true);
                /** @var Dyna_Catalog_Model_Receiver $customerReceiver */
                $receiver->setSupportsHd($customerReceiver->supportsHd());
                $receiver->setSupportsVod($customerReceiver->supportsVod());
                $receiver->setSupportsHdFree($customerReceiver->supportsHDFree());
                $receiver->setEquipmentType($customerReceiver->getType());

            } else {
                $tags = explode(",", $foundReceiver->getProduct()->getTags());
                $receiver->setSupportsHd(in_array("isHD", $tags));
                //@todo Determine if it supports HDFree
                //@todo Determine if it supports Vod
                $receiverTypes = $foundReceiver->getProduct()->getType();
                $receiver->setEquipmentType(!empty($receiverTypes) ? current($receiverTypes) : "");
            }

            // Identify receiver
            $receiver->setSku($foundReceiver->getSku());
            $hardware->setReceiver($receiver);
        }

        return $hardware;
    }

    /**
     * Create Logger instance
     * @return Psr\Log\LoggerInterface
     */
    protected function getLogger()
    {
        if ($this->logger === false) {
            //by default, try to log for each quote separately, fallback using date
            $quote = Mage::getSingleton('checkout/cart')->getQuote();
            $args = [
                'stream' => 'cable_rules_artifact' . ((!$quote || !$quote->getId()) ? "Ymd" : $quote->getId()) . '.log'
            ];
            $this->logger = Mage::getModel('aleron75_magemonolog/logger');
            $handlerModel = 'streamHandler';
            $handlerWrapper = Mage::getModel('aleron75_magemonolog/handlerWrapper_'.$handlerModel, $args);
            $this->logger->pushHandler($handlerWrapper->getHandler());
            $handlers = Mage::getStoreConfig('magemonolog/handlers');
            $redisArgs = $handlers['RedisHandler']['params'];
            $redisArgs['stream'] = 'cablerulesartifact.log';
            /** @var Aleron75_Magemonolog_Model_HandlerWrapper_RedisHandler $redisHandlerWrapper */
            $redisHandlerWrapper = Mage::getModel('aleron75_magemonolog/handlerWrapper_redisHandler', $redisArgs);
            $this->logger->pushHandler($redisHandlerWrapper->getHandler());
        }

        return $this->logger;
    }

    /**
     * @return Dyna_Configurator_Helper_Rules
     */
    protected function getRulesHelper()
    {
        if (!$this->ruleHelper) {
            $this->ruleHelper = Mage::helper("dyna_configurator/rules");
        }
        return $this->ruleHelper;
    }
}
