<?php

class Dyna_Configurator_Helper_ExtraLogCart extends Mage_Log_Helper_Data
{
    public function extraLog($message)
    {
        if (!Mage::getStoreConfig('omnius_general/log_settings/cart_log_enabled')) {
            return;
        }

        Mage::log($message, Zend_Log::DEBUG, "extra-cart-log-".date('Ymd').".log");
    }
}
