<?php
/**
 * Installer for adding FN_Unknown attribute set (see OMNVFDE-72) for imported products that have empty values for HW Type columns
 */

/** @var Dyna_Fixed_Model_Resource_Setup $this */

$newAttributeSetName = "FN_Unknown";

/** Create attribute set if not exists */
if (! $attrSetId = $this->getAttributeSet(Mage_Catalog_Model_Product::ENTITY, $newAttributeSetName, 'attribute_set_id')) {

    $originalSetId = $this->getAttributeSetId(Mage_Catalog_Model_Product::ENTITY, "Default_Set");

    /** @var $attributeSetApi Mage_Catalog_Model_Product_Attribute_Set_Api */
    $attributeSetApi = Mage::getModel('catalog/product_attribute_set_api');

    $attrSetId = $attributeSetApi->create($newAttributeSetName, $originalSetId);
}

