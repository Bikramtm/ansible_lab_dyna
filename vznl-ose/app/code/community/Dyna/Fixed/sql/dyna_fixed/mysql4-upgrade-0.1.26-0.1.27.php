<?php
/**
 * Installer that adds "sorting" attribute to all "General" groups of all attribute sets
 */

/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;
foreach ($installer->getAllAttributeSetIds($entityTypeId) as $attributeSetId) {
    $installer->addAttributeToSet($entityTypeId, $attributeSetId, "General", 'sorting');

}

$installer->endSetup();
