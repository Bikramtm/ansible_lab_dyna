<?php
/**
 * Installer that adds new attributes to FN_Promotions attribute set @see OMNVFDE-2900
 */

/* @var $installer Dyna_Fixed_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

// add attributes to corresponding attribute set
$attributesToSets = [
    'FN_Accessory' => [
        'product_type',
        'product_family_id',
        'hierarchy_value',
        'product_version_id',
        'contract_value',
        'lifecycle_status',
        'display_name_inventory',
        'display_name_configurator',
        'display_name_cart',
        'display_name_communication',
    ],
    'FN_Hardware' => [
        'product_type',
        'product_family_id',
        'hierarchy_value',
        'contract_value',
        'product_version_id',
        'display_name_inventory',
        'display_name_configurator',
        'display_name_cart',
        'display_name_communication',
        'lifecycle_status',
    ],
    'FN_Options' => [
        'sorting',
        'product_family_id',
        'hierarchy_value',
        'contract_value',
        'product_version_id',
        'display_name_inventory',
        'display_name_configurator',
        'display_name_cart',
        'display_name_communication',
        'lifecycle_status',
        'related_slave_sku',
    ],
    'FN_Promotion' => [
        'price_discount',
    ],
    'FN_SalesPackage' => [
        'sorting',
        'product_version_id',
        'hierarchy_value',
        'contract_value',
        'display_name_inventory',
        'display_name_configurator',
        'display_name_cart',
        'display_name_communication',
        'lifecycle_status',
    ]
];

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;
foreach ($attributesToSets as $attributeSetCode => $attributes) {
    foreach ($attributes as $attributeCode) {
        $attributeSetId = $installer->getAttributeSet($entityTypeId, $attributeSetCode, "attribute_set_id");
        $installer->addAttributeToSet($entityTypeId, $attributeSetId, "Fixed", $attributeCode);
    }
}

$installer->endSetup();
