<?php
/**
 * Installer for adding new attributes (details in OMNVFDE-107)
 */

/* @var $this Dyna_Mobile_Model_Resource_Setup */

$this->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

/** Add missing attributes */

$attributes = [
    'internal_name' => [
        'label' => 'Internal name',
        'input' => 'text',
        'type' => 'varchar'
    ],
    'display_name' => [
        'label' => 'Displayname (GUI)',
        'input' => 'text',
        'type' => 'varchar'
    ],
    'print_name' => [
        'label' => 'Printname (documents)',
        'input' => 'text',
        'type' => 'varchar'
    ],
    'special_legal_text' => [
        'label' => 'Special legal text',
        'input' => 'text',
        'type' => 'varchar'
    ],
    'special_legal_text_nr_porting' => [
        'label' => 'Special legal text for number porting',
        'input' => 'text',
        'type' => 'varchar'
    ],
    'special_legal_text_for_redone' => [
        'label' => 'Special legal text for Red One',
        'input' => 'text',
        'type' => 'varchar'
    ],
    'product_hint' => [
        'label' => 'User books product hint',
        'input' => 'text',
        'type' => 'varchar'
    ],
    'not_compatible_hint' => [
        'label' => 'User books incompatible product hint',
        'input' => 'text',
        'type' => 'varchar'
    ],
    'isdn_network_capable_nt_spi' => [
        'label' => 'ISDN network capable (NT-Split integrated)',
        'input' => 'boolean',
        'type' => 'int',
        'source' => 'eav/entity_attribute_source_boolean',
    ],
];

$attributeSetNames = [
    1 => 'FN_SalesPackage',
    2 => 'Prepaid_Package',
    3 => 'Mobile_Voice_Data_Tariff',
    4 => 'Mobile_Data_Tariff',
    5 => 'Mobile_Prepaid_Tariff',
    6 => 'FN_Settings',
    7 => 'FN_Options',
    8 => 'FN_Hardware',
];

$attributeGroupNames['General'] = [];
$attributeGroupNames['Hardware'] = [];
$createAttributeSets = [];
$sortOrder = 10;

foreach ($attributes as $code => $options) {
    $attributeId = $this->getAttribute($entityTypeId, $code, 'attribute_id');
    if (!$attributeId) {
        $options['user_defined'] = 1;
        $this->addAttribute($entityTypeId, $code, $options);

        if ($code == 'isdn_network_capable_nt_spi') {
            $attributeGroupNames['Hardware'][$code] = '8';
        } else {
            $attributeGroupNames['General'][$code] = '1,2,3,4,5,6,7';
        }
    }
}

foreach ($attributeGroupNames as $group => $attributes) {
    foreach ($attributes as $code => $sets) {
        $setIds = explode(',', $sets);
        foreach ($setIds as $setId) {
            $attributeSet = $attributeSetNames[$setId];
            $createAttributeSets[$attributeSet]['groups'][$group][] = $code;
        }
    }
}

$this->assignAttributesToSet($createAttributeSets, $sortOrder);
unset($attributeSetNames, $attributeGroupNames, $createAttributeSets);

unset($attributes);

/** Removing terms_and_conditions attributed added by Dyna_Catalog installer */

if ($attributeId = $this->getAttributeId($entityTypeId, 'terms_and_conditions')) {
    $this->removeAttribute($entityTypeId, $attributeId);
}

/** Update attribute service_description to allow html code */

if ($attributeId = $this->getAttributeId($entityTypeId, 'service_description')) {
    $attribute = Mage::getModel('catalog/resource_eav_attribute')->load($attributeId);
    $attribute->setData('is_html_allowed_on_front', 1)->save();
}

$this->endSetup();