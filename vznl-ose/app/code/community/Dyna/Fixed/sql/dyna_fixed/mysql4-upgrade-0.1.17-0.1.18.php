<?php
/* @var $this Dyna_Fixed_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

/**
 * OMNVFDE-355
 * Remove some mandatory attributes from FN_SalesPackage attribute set
 */

$attributeCodes = [
    "main_access_technology",
    "main_access_service_code",
    "upstream",
    "downstream",
    "bandwidth_service_code",
    "related_pricing_structure_code",
];

$attributeSetCode = 'FN_SalesPackage';
$attributeSetId = $installer->getAttributeSet($entityTypeId, $attributeSetCode, "attribute_set_id");


if ($attributeSetId) {
    foreach ($attributeCodes as $attributeCode) {
        $attId = $installer->getAttribute($entityTypeId, $attributeCode, 'attribute_id');
        Mage::getModel('catalog/product_attribute_set_api')->attributeRemove($attId, $attributeSetId);
    }
}


/**
 * OMNVFDE-355
 * Add maf attribute to FN_SalesPackage
 */
$attributeId = $installer->getAttributeId($entityTypeId, 'maf');
$installer->addAttributeToSet($entityTypeId, $attributeSetId, 'Fixed', $attributeId, 35);


/**
 * OMNVFDE-369
 * assignment column can be removed as it is not in the final import files anymore.
 */
$attributeCode = 'assignment';
$attributeSetCode = 'FN_Promotion';
$attId = $installer->getAttribute($entityTypeId, $attributeCode, 'attribute_id');
$attributeSetId = $installer->getAttributeSet($entityTypeId, $attributeSetCode, "attribute_set_id");
if ($attributeSetId) {
    Mage::getModel('catalog/product_attribute_set_api')->attributeRemove($attId, $attributeSetId);
}

$installer->endSetup();
