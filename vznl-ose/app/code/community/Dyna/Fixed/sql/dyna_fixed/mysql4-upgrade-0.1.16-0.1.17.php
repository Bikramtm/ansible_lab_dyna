<?php
/**
 * OMNVFDE-354
 */

/* @var $this Dyna_Fixed_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

// Assign missing attributes to FN_Options
$assignAttributes = [
    "portfolio",
    "minimum_contract_duration",
    "minimum_contract_duration_unit",
];

$attributeSetCode = 'FN_Options';
$attributeSetId = $installer->getAttributeSet($entityTypeId, $attributeSetCode, "attribute_set_id");

foreach ($assignAttributes as $attributeCode) {
    if ($attributeSetId) {
        $installer->addAttributeToSet($entityTypeId, $attributeSetId, "Fixed", $attributeCode);
    }
}

$installer->endSetup();
