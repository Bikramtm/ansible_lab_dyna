<?php
/**
 * OMNVFDE-368
 * Remove tariff_name and tariff_code from all FN attribute sets
 * Delete FN_Settings attribute set
 */

/* @var $this Dyna_Fixed_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

$attributeCodes = [
    "tariff_name",
    "tariff_code",
];

$attributeSets = [
    1 => 'FN_SalesPackage',
    2 => 'FN_Options',
    3 => 'FN_Service',
    4 => 'FN_Accessory',
    5 => 'FN_Hardware',
    6 => 'FN_Settings',
];

foreach ($attributeCodes as $attributeCode) {
    $attId = $installer->getAttribute($entityTypeId, $attributeCode, 'attribute_id');
    foreach ($attributeSets as $attributeSetCode) {
        $attributeSetId = $installer->getAttributeSet($entityTypeId, $attributeSetCode, "attribute_set_id");
        if ($attributeSetId) {
            Mage::getModel('catalog/product_attribute_set_api')->attributeRemove($attId, $attributeSetId);
        }
    }
}

// Remove FN_Settings attribute set
$installer->removeAttributeSet($entityTypeId, 'FN_Settings');

$installer->endSetup();
