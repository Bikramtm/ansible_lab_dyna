<?php

/**
 * Installer that adds the related_slave_sku attribute in the FN_Options attribute set.
 * Also marks the attribute as NOT required
 */

/** @var Dyna_Fixed_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();

$attributeCode = 'related_slave_sku';

// Update related_slave_sku to NOT required
$installer->updateAttribute(
    Mage_Catalog_Model_Product::ENTITY,
    $attributeCode,
    'is_required',
    false
);

// Add related_slave_sku to the FN_Options attribute set
$installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
    'FN_Options',
    'Fixed',
    $attributeCode
);

$installer->endSetup();