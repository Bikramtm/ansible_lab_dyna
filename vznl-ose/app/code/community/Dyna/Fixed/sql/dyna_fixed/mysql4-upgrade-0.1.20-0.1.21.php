<?php
/**
 * Installer that adds new attributes to FN_Promotions attribute set @see OMNVFDE-2900
 */

/* @var $installer Dyna_Fixed_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

$attributesList = [
    'maf_discount' => [
        'label' => 'Maf discount',
        'input' => 'price',
        'type' => 'decimal'
    ],
    'maf_discount_percent' => [
        'label' => 'Maf discount percent',
        'input' => 'text',
        'type' => 'int',
    ],
    'discount_period_amount' => [
        'label' => 'Discount period amount',
        'input' => 'text',
        'type' => 'int',
    ],
    'discount_period' => [
        'label' => 'Discount period',
        'input' => 'text',
        'type' => 'text',
    ],
];


foreach ($attributesList as $attributeCode => $attributeData) {
    $attributeData['user_defined'] = 1;
    $installer->addAttribute($entityTypeId, $attributeCode, $attributeData);
}

$createAttributeSets = [
    'FN_Promotion' => [
        'groups' => [
            'Fixed' => [
                'maf_discount',
                'maf_discount_percent',
                'discount_period_amount',
                'discount_period',
            ]
        ]
    ],
];

$sortOrder = 170;
$installer->assignAttributesToSet($createAttributeSets, $sortOrder);
unset($createAttributeSets);

$installer->endSetup();
