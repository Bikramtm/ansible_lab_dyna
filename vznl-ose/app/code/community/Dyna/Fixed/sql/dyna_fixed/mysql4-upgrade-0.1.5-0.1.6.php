<?php
/**
 * New attributes to FN_Sales_Package as requested in OMNVFDE-98
 */

$installer = $this;
/* @var $installer Dyna_Mobile_Model_Resource_Setup */

$installer->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

$attributes = [
    'portfolio' => [
        'label' => 'Portfolio',
        'input' => 'date',
        'type' => 'datetime',
        'backend' => 'eav/entity_attribute_backend_datetime',
    ],
    'sales_package_id' => [
        'label' => 'Sales Package ID',
        'input' => 'text',
        'type' => 'varchar',
    ],
    'ccb_package_code' => [
        'label' => 'CCB Package Code (Backend Code)',
        'input' => 'text',
        'type' => 'varchar',
    ],
    'voice_access' => [
        'label' => 'Voice Access Level',
        'input' => 'text',
        'type' => 'varchar',
    ],
    'technology_product' => [
        'label' => 'Technology Product',
        'input' => 'text',
        'type' => 'varchar',
    ],
    'technology_category' => [
        'label' => 'Technology category',
        'input' => 'text',
        'type' => 'varchar',
    ],
    'bandwidth' => [
        'label' => 'Bandwidth',
        'input' => 'text',
        'type' => 'varchar',
    ],
    'tv_product_name' => [
        'label' => 'TV Product Name',
        'input' => 'text',
        'type' => 'varchar',
    ],
    'iptv_service_name' => [
        'label' => 'IPTV Service Name (Monthly price)',
        'input' => 'text',
        'type' => 'varchar',
    ],
    'connection_type' => [
        'label' => 'Connection Type',
        'input' => 'text',
        'type' => 'varchar',
    ],
    'service_code' => [
        'label' => 'Service Code',
        'input' => 'text',
        'type' => 'varchar',
    ],
    'service_name' => [
        'label' => 'Service Name',
        'input' => 'text',
        'type' => 'varchar',
    ],
];

$attributeSetNames = [
    1 => 'FN_SalesPackage',
    2 => 'FN_Settings',
];
$attributeGroupNames['Fixed'] = [];
$createAttributeSets = [];
$sortOrder = 10;

foreach ($attributes as $code => $options) {
    $attributeId = $installer->getAttribute($entityTypeId, $code, 'attribute_id');
    if (!$attributeId) {
        $options['user_defined'] = 1;
        $installer->addAttribute($entityTypeId, $code, $options);

        /** OMNVFDE-100: portfolio will belong to both FN_Settings and FN_SalesPackage and service_code and service_name only for FN_Settings */
        /** the rest of added attribute will go to FN_SalesPackage */
        switch ($code) {
            case "portfolio" :
                $attributeGroupNames['Fixed'][$code] = '1,2';
                break;
            case "service_code" :
            case "service_name" :
                $attributeGroupNames['Fixed'][$code] = '2';
                break;
            default :
                $attributeGroupNames['Fixed'][$code] = '1';
                break;
        }
    }
}

foreach ($attributeGroupNames as $group => $attributes) {
    foreach ($attributes as $code => $sets) {
        $setIds = explode(',', $sets);
        foreach ($setIds as $setId) {
            $attributeSet = $attributeSetNames[$setId];
            $createAttributeSets[$attributeSet]['groups'][$group][] = $code;
        }
    }
}

$installer->assignAttributesToSet($createAttributeSets, $sortOrder);
unset($attributeSetNames, $attributeGroupNames, $createAttributeSets);

unset($attributes);

$installer->endSetup();