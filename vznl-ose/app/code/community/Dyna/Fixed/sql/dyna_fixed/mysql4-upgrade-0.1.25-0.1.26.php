<?php
// Delete gui_ranking and ranking attributes

/* @var $installer Dyna_Fixed_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;
$installer->removeAttribute($entityTypeId,'gui_ranking');
$installer->removeAttribute($entityTypeId,'ranking');

$installer->endSetup();