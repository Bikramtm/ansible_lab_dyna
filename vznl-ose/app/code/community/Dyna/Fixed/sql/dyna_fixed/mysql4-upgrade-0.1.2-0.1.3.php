<?php

/* @var $installer Dyna_Fixed_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

// Fixed(DSL) attribute sets
$attributeSetNames = [
    1 => 'FN_SalesPackage',
    2 => 'FN_Options',
    3 => 'FN_Service',
    4 => 'FN_Accessory',
    5 => 'FN_Hardware',
    6 => 'FN_Settings',
    7 => 'FN_Promotion',
];

// Map each attribute group to the attribute set
$attributeGroupNames = [
    'Fixed' => [
        "code" => '1,2,3,4,5,6,7',
        "sales_start_date" => '1,2,3,4,5,6,7',
        "sales_end_date" => '1,2,3,4,5,6,7',
        "service_item_type" => '1,2,3,4,5,6,7',
        "version" => '1',
        "main_access_technology" => '1',
        "main_access_service_code" => '1',
        "upstream" => '1',
        "downstream" => '1',
        "bandwidth_service_code" => '1',
        "tariff_name" => '1,2,3,4,5,6',
        "tariff_code" => '1,2,3,4,5,6',
        "gui_name" => '1,2,3,4,5,6,7',
        "gui_hint_text" => '1,2,3,4,5,6,7',
        "print_text" => '1,2,3,4,5,6,7',
        "print_additional_text" => '1,2,3,4,5,6,7',
        "no_of_new_telephone_numbers" => '1',
        "port_all_numbers" => '1',
        "tv_access_type" => '2',
        "tv_service_code" => '2',
        "directory_entry_type" => '3',
        "directory_entry_amount" => '3',
        "inverse_search_indicator" => '3',
        "directory_usage_type" => '3',
        "addon_type" => '2',
        "related_pricing_structure_code" => '1',
        "assignment" => '7',
        "one_time_condition_code" => '7',
        "recurring_condition_code" => '7',
        "bookable_in_sales_channel" => '7',
        "bookable_sales_force_id" => '7',
        "customer_class" => '7',
        "direct_debit" => '7',
        "paper_bill" => '7',
    ],
    'Promotion' => [
        "contract_duration" => '7',
        "product_allows_inventory" => '7',
        "package_allowed" => '7',
        "tariff_allowed" => '7',
        "tariff_option_allowed" => '7',
        "product_combination" => '7',
        "online_tariff_allowed" => '7',
        "dsl_resale_tariff_allowed" => '7',
        "ngn_dsl_tariff_allowed" => '7',
        "voip_tariff_allowed" => '7',
        "tv" => '7',
        "tv_tariff_allowed" => '7',
        "dsl" => '7',
        "dsl_realized" => '7',
        "existing_connection" => '7',
        "ordered_connection" => '7',
        "number_s0_lines" => '7',
        "current_telephone_company" => '7',
        "acquisition_numbers" => '7',
        "hardware_subsidized" => '7',
        "onkz_allowed" => '7',
        "instant_access" => '7',
        "tv_subscriptions_allowed" => '7',
        "free_tv_package" => '7',
    ],
    'Hardware' => [
        "hw_materianumer" => '5',
        "internal_hardware_name" => '5',
        "hardware_gui_name" => '5',
        "hardware_print_name" => '5',
        "hw_type" => '5',
        "hw_sub_type" => '5',
        "payment_mode" => '5',
        "iad_hardware_class" => '5',
        "price_subsidized" => '5',
        "price_unsubsidized" => '5',
        "monthly_price" => '5',
        "print_kassenzettel" => '5',
        "restricted_to" => '5',
        "allowed_unsubsidized_sales" => '5',
        "fn_new_start_date_nc" => '5',
        "fn_new_end_date_nc" => '5',
        "fn_new_restricted_tsp" => '5',
        "fn_new_device_allowed_tat" => '5',
        "fn_new_device_restricted_vat" => '5',
        "fn_change_start_date_bc" => '5',
        "fn_change_end_date_bc" => '5',
        "fn_change_new_restricted_tsp" => '5',
        "fn_change_new_allowed_tat" => '5',
        "fn_change_new_restricted_vat" => '5',
        "fn_change_ex_restricted_tsp" => '5',
        "fn_change_ex_compat_target_tat" => '5',
        "fn_change_ex_restricted_vat" => '5',
        "vdsl2_vectoring" => '5',
        "vdsl2_legacy" => '5',
        "iptv" => '5',
        "tv_center_inkl_qos" => '5',
        "fttx" => '5',
        "vdsl_capable_add_vsdl_modem" => '5',
        "isdn_capable_add_hw_device" => '5',
        "lte_capable_add_hw_device" => '5',
        "isdn_enabled" => '5',
        "ipv6_enabled" => '5',
        "adsl2_plus" => '5',
        "acs_enabled" => '5',
        "annex_j_enabled" => '5',
    ],
];

$createAttributeSets = [];
$sortOrder = 10;
foreach ($attributeGroupNames as $group => $attributes) {
    foreach ($attributes as $code => $sets) {
        $setIds = explode(',', $sets);
        foreach ($setIds as $setId) {
            $attributeSet = $attributeSetNames[$setId];
            $createAttributeSets[$attributeSet]['groups'][$group][] = $code;
        }
    }
}

$installer->assignAttributesToSet($createAttributeSets, $sortOrder);
unset($attributeSetNames, $attributeGroupNames, $createAttributeSets);

$installer->endSetup();
