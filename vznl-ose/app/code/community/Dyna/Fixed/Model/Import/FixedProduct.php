<?php

/**
 * Class Dyna_Fixed_Model_Import_CableProduct
 */
class Dyna_Fixed_Model_Import_FixedProduct extends Dyna_Import_Model_Import_Product_Abstract
{
    /** @var string $_csvDelimiter */
    protected $_csvDelimiter = ';';
    /** @var string $_logFileName */
    protected $_logFileName = "fixed_product_import";
    protected $_debug = false;
    /** @var Dyna_Catalog_Model_ProductVersion */
    protected $productVersionModel;
    /** @var Dyna_Catalog_Model_ProductFamily */
    protected $productFamilyModel;

    /**
     * Dyna_Import_Model_Import_CableProduct constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_helper->setImportLogFile($this->_logFileName . '.' . $this->_logFileExtension);
        $this->_qties = 0;

        $this->productFamilyModel = Mage::getSingleton('dyna_catalog/productFamily');
        $this->productVersionModel = Mage::getSingleton('dyna_catalog/productVersion');
    }

    /**
     * Import cable product
     *
     * @param array $data
     * @return void
     */
    public function importProduct($data)
    {
        $skip = false;
        $this->_totalFileRows++;
        $data = $this->_setDataMapping($data);

        if (!$this->_helper->hasValidPackageDefined($data)) {
            $this->_skippedFileRows++;
            return;
        }

        if (!array_key_exists('sku', $data) || empty($data['sku'])) {
            $this->_logError('Skipping line without sku');
            $skip = true;
        } elseif (!array_key_exists('name', $data) || empty($data['name'])) {
            $this->_logError('Skipping line without name');
            $skip = true;
        }

        if ($skip) {
            $this->_skippedFileRows++;
            return;
        }

        $data['stack'] = $this->stack;
        $data['sku'] = trim($data['sku']);

        $this->_currentProductSku = $data['sku'];
        $this->_log('Start importing ' . $data['name'], true);
        $data = $this->checkPrice($data);

        try {
            /** @var Mage_Catalog_Model_Product $product */
            $product = Mage::getModel('catalog/product');
            $id = $product->getIdBySku($data['sku']);
            $new = ($id == 0);
            if (!$new) {
                $this->_log('Loading existing product"' . $data['sku'] . '" with ID ' . $id, true);
                $product->load($id);
            }
            // check vat value and tax
            $vatValue = $data['vat'];
            $vatClass = Mage::helper('dyna_catalog')->getVatTaxClass($vatValue);

            $skip = false;
            if (!$this->_setAttributeSetByCode($product, $data)) {
                $this->_logError('Unknown attribute set "' . $data['attribute_set'] . '" for product ' . $data['sku']);
                $skip = true;
            } elseif (!$this->_setWebsitesByCsvString($product, $data)) {
                $this->_logError('Unknown website "' . $data['_product_websites'] . '" for product ' . $data['sku']);
                $skip = true;
            } elseif (!$this->setTaxClassIdByName($product, $vatClass)) {
                $this->_logError((empty($vatClass) ? 'Empty vat value' : 'Unknown vat value "' . $vatClass . '"') . ' for product ' . $data['sku']);
                $skip = true;
            }

            if ($skip) {
                return;
            }

            $this->_setDefaults($product, $data);
            $this->_setStatus($product, $data);

            $this->_setProductData($product, $data);

            // set product categories
            if (isset($data['category']) && is_array($data['category']) && count($data['category'])) {
                $product->setCategoryIds($data['category']);
            }

            if ($new) {
                $this->_createStockItem($product);
            }

            // Adding product versions to product
            if (!empty($data['product_version_id'])) {
                $versionIds = array();
                foreach (array_filter(explode(",", $data['product_version_id'])) as $versionCode) {
                    if ($productVersionId = $this->productVersionModel::getProductVersionIdByCode($versionCode)) {
                        $versionIds[] = $productVersionId;
                    } else {
                        $this->_logError('Unknown version id "' . $data['product_version'] . '" for product ' . $data['sku']);
                    }
                }
                $product->setData("product_version", implode(",", $versionIds));
            }
            // Adding product family to product
            if (!empty($data['product_family_id'])) {
                $familyIds = array();
                foreach (array_filter(explode(",", $data['product_family_id'])) as $familyCode) {
                    if ($familyId = $this->productFamilyModel::getProductFamilyIdByCode($familyCode)) {
                        $familyIds[] = $familyId;
                    } else {
                        $this->_logError('Unknown family id "' . $data['product_family_id'] . '" for product ' . $data['sku']);
                    }
                }
                $product->setData("product_family", implode(",", $familyIds));
            }

            if (!$this->getDebug()) {
                $product->save();
            }

            if (!$this->getDebug()) {
                $productVisibilityInfo = @json_decode($product->getProductVisibilityStrategy(), true);
                if ($productVisibilityInfo) {
                    Mage::getModel('productfilter/filter')->createRule($product->getId(), $productVisibilityInfo['strategy'], $productVisibilityInfo['groups']);
                } else {
                    //uncomment the following line if products that have no defined filters must be treated as available for all groups
                    //Mage::getModel('productfilter/filter')->createRule($product->getId(), Dyna_ProductFilter_Model_Filter::STRATEGY_INCLUDE_ALL, null); //available for all dealer groups
                }
            }

            unset($product);
            $this->_qties++;

            $this->_log('Finished importing ' . $data['name'], true);
        } catch (Exception $ex) {
            echo $ex->getMessage();
            $this->_logEx($ex);
        }
    }

    /**
     * Process custom data mapping for cable products
     *
     * @param array $data
     * @return array
     */
    protected function _setDataMapping($data)
    {
        // Hardcoded values
        // todo: remove hardcoded 'telesales' website code
        $data['_product_websites'] = 'telesales';

        $header_mappings = array_flip($this->_helper->getMapping('product_template'));

        // new name => old name
        $renamedFields = [
            'gui_name' => 'hardware_gui_name',
            'name' => 'internal_hardware_name',
            'fn_new_start_date_nc' => 'fn_new_start_sales_date_new_customer',
            'fn_new_end_date_nc' => 'fn_new_end_sales_date_new_customer',
            'fn_change_start_date_bc' => 'fn_change_start_sales_date_base_customer',
            'fn_change_end_date_bc' => 'fn_change_end_sales_date_base_customer',
            'isdn_network_capable_nt_spi' => 'isdn_network_capable(nt-split_integrated)',
            'hw_sub_type' => 'hw_sub-type',
            'monthly_price' => 'monthly_price(rental_or_surcharge)',
            'allowed_unsubsidized_sales' => 'allow_for_unsubsidized_sales',
            'vdsl2_vectoring' => 'vdsl2-vectoring',
            'vdsl2_legacy' => 'vdsl2-vectoring',
            'main_access_technology' => 'access_technology',
            'isdn_enabled' => 'isdn-enabled(s0)',
            'ipv6_enabled' => 'i_pv6enabled',
            'adsl2_plus' => 'adsl2+',
            'acs_enabled' => 'acs-enabled',
            'tax_class_id' => 'tax_class',
            'maf' => 'monthly_price',
        ];

        $renamedFields = array_merge($renamedFields, $header_mappings);

        foreach ($renamedFields as $new => $old) {
            if (empty($data[$new]) && !empty($data[$old])) {
                $data[$new] = $data[$old];
                unset($data[$old]);
            }
        }

        if (empty($data['name']) && !empty($data['gui_name'])) {
            $data['name'] = $data['gui_name']; // None
        }

        if (empty($data['gui_name'])) {
            //If set, it will be preserved
            $data['gui_name'] = $data['name'];
        }

        if (!empty($data['fn_new_start_date_nc'])) {
            $data['fn_new_start_date_nc'] = $this->determineFnStartDate($data['fn_new_start_date_nc']);
        }

        if (!empty($data['fn_change_start_date_bc'])) {
            $data['fn_change_start_date_bc'] = $this->determineFnStartDate($data['fn_change_start_date_bc']);
        }

        /** Converting the remaining date fields to mysql friendly format, assuming that these do contain valid values */
        $fieldsKeys = array(
            "fn_new_start_date_nc",
            "fn_new_end_date_nc",
            "fn_change_start_date_bc",
            "fn_change_end_date_bc",
            "portfolio",
        );
        foreach ($fieldsKeys as $key) {
            if (!empty($data[$key])) {
                $data[$key] = date("Y-m-d", strtotime($data[$key]));
            }
        }

        if (!empty($data['monthly_price'])) {
            $data['monthly_price'] = str_replace(",", ".", $data['monthly_price']);
        }

        if (empty($data['attribute_set'])) {
            $data['attribute_set'] = $this->_getAttributeSet($data);
        }

        if (empty($data['price']) && !empty($data['ot_value_euro_brutto'])) {
            $data['price'] = str_replace("€", "", $data['ot_value_euro_brutto']);
        }

        if (!empty($data['maf_discount'])) {
            $data['maf_discount'] = $this->getFormattedPrice($data['maf_discount']);
        }

        if (!empty($data['price_discount'])) {
            $data['price_discount'] = $this->getFormattedPrice($data['price_discount']);
        }

        if (isset($data['product_visibility'])) {
            $data['product_visibility'] = $this->parseProductVisibility($data['product_visibility']);
        }

        /** Get value for checkout product attribute */
        $data['checkout_product'] = $this->getCheckoutProductValue($data);

        return $data;
    }

    /**
     * Replace non date strings and strings values that have a date like string inside it's content
     *
     * @access private
     * @param string $startDate
     * @return string
     */
    private function determineFnStartDate($startDate)
    {
        /** fn_change_start_date_bc: Replacement for non date strings */
        $startDate = in_array(
            $startDate,
            array(
                "keine Vermarktung (GK Artikel)",
                "keine Vermarktung",
            )
        ) ? "" : $startDate;

        /** Replacement for string values having a date like string inside it's content */
        if (preg_match("/[0-9]{2}.[0-9]{2}.[0-9]{4}/", $startDate, $matches)) {
            $startDate = current($matches);
        }

        return $startDate;
    }

    /**
     * Return the appropriate attribute set to which the current product is assigned
     * The entire imported product info is passed for future improvements:
     * ex: determining the attribute_set from combination of product properties
     *
     * @readme to determine the Attribute Set to which a product is assigned, either use a column named hw_type with values from bellow switch
     * @readme of use a column named attribute_set which contains the attribute set name from magento
     *
     * @param $product array
     * @return string
     */
    protected function _getAttributeSet($product)
    {
        /** @done check with BA if the default attribute set for fixed line remains hardware for those products that do not have a defined hw_type attribute */
        /** new attribute set for empty HW Type column import (see OMNVFDE-72): FN_Unknown */
        $attributeSet = "FN_Unknown";

        /** custom checks are made because CSV columns differ from one imported file to another */
        if (isset($product['hw_type'])) {
            switch ($product['hw_type']) {
                case "IAD" :
                case "STB" :
                case "SIM" :
                    $attributeSet = "FN_Hardware";
                    break;
                case "ACC" :
                    $attributeSet = "FN_Accessory";
                    break;
                /** @done check with BA what attribute set should the products of hw_type bellow be related to */
                /** No attribute set should be used in this case (see OMNVFDE-72), but the import will fail so the Default_Set will be used */
                case "NTD" :
                case "VMO" :
                case "LMO" :
                    $attributeSet = "Default_Set";
                    break;
                default:
                    break;
            }
        }

        /** if attribute_set column exists, but has an empty values, we'll preserve the previous attributeSet value */
        if (isset($product['attribute_set'])) {
            $attributeSet = trim($product['attribute_set']) ?: $attributeSet;
        }

        return $attributeSet;
    }

    /**
     * Method to parse the value of the product visibility attributes
     * @param $values
     * @return string
     * @internal param $value
     */
    protected function parseProductVisibility($values){
        $markedOptions = [];
        $markedOptionIds = [];
        $options = [];

        $attribute = Mage::getSingleton('eav/config')
            ->getAttribute(Mage_Catalog_Model_Product::ENTITY, Dyna_Catalog_Model_Product::CATALOG_PACKAGE_PRODUCT_VISIBILITY_ATTR);
        if ($attribute->usesSource()) {
            $options = $attribute->getSource()->getAllOptions(false);
        }

        if ('*' == $values) {
            $markedOptionIds = array_diff(
                array_column($options, 'label'),
                [Dyna_Catalog_Model_Product::CATALOG_PRODUCT_VISIBILITY_OPTION_NOT_SHOWN]
            );
        } else {
            $values = explode(',', $values);
            foreach($values as $value){
                $markedOptions[] = strtolower(str_replace(' ', '', $value));
            }
            foreach ($options as $option) {
                if(in_array(strtolower(str_replace(' ', '', $option['label'])), $markedOptions)){
                    $markedOptionIds[] = $option['label'];
                }
            }
        }

        return implode(',', $markedOptionIds);
    }

    /**
     * Determine checkout product attribute value
     *
     * @param array $data
     * @return null|string
     */
    private function getCheckoutProductValue($data)
    {
        if(isset($data['checkout_product']) && $data['checkout_product']) {
            return ('yes' == strtolower($data['checkout_product'])) ? 'Yes' : 'No';
        }
        return 'No';
    }

    /**
     * Checks whether or not price is listed in import file (can be 0, but needs to exists)
     */
    protected function checkPrice($data)
    {
        if (!isset($data['price']) || $data['price'] === "") {
            $data['price'] = 0;
            $this->_log("[NOTICE] Did not receive any value for price column. Setting price to 0");
        }

        return $data;
    }

    /**
     * Set product class id based on the provided tax_class_id in the CSV file
     *
     * @param $product
     * @param string $className
     * @return bool
     * @todo: move in abstract class
     */
    public function setTaxClassIdByName($product, $className)
    {
        $productTaxClass = Mage::getModel('tax/class')
            ->getCollection()
            ->addFieldToFilter('class_name', $className)
            ->load()
            ->getFirstItem();
        if (!$productTaxClass || !$productTaxClass->getId()) {
            return false;
        }

        $product->setTaxClassId($productTaxClass->getId());

        return true;
    }

    /**
     * Set product model default values
     *
     * @param Mage_Catalog_Model_Product $product
     * @param array $data
     */
    protected function _setDefaults($product, $data)
    {
        $product->setTypeId(Mage_Catalog_Model_Product_Type::TYPE_SIMPLE);
        // This will format itself trough observer
        $product->setUrlKey($data['name']);
        $product->setProductVisibilityStrategy('all');
        $product->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH);
        $product->setIsDeleted('0');
    }

    /**
     * Set product status
     *
     * @param Mage_Catalog_Model_Product $product
     * @param array $data
     */
    protected function _setStatus($product, $data)
    {
        if (array_key_exists('status', $data) && $data['status']) {
            switch ($data['status']) {
                case 'Enabled':
                case 'enabled':
                    $product->setStatus(1);
                    break;
                case 'Disabled':
                case 'disabled':
                default:
                    $product->setStatus(2);
                    break;
            }
        } else {
            $product->setStatus(2);
        }
    }

    /**
     * @param $price
     * @return string
     */
    private function getFormattedPrice($price)
    {
        $price = str_replace(',', '.', $price);

        return number_format($price, 4, '.', null);
    }

    public function setImportHelper($helper) {
        $this->_helper = $helper;
    }

    /**
     * Log function
     *
     * @param string $msg
     * @param bool $verbose
     */
    public function _log($msg, $verbose = false)
    {
        if($verbose && !$this->_debug) {
            return;
        }
        $this->_helper->logMsg('[' . $this->_currentProductSku . '] ' . $msg, false);
    }

    /**
     * Log function
     *
     * @param string $msg
     */
    public function _logError($msg)
    {
        $this->_helper->logMsg('[' . $this->_currentProductSku . '] ' . $msg);
    }
}
