<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Superorder_Helper_Client
 */
class Dyna_Superorder_Helper_Client extends Mage_Core_Helper_Abstract
{
    protected $checkoutData;
    protected $customer;
    protected $inputParams;
    protected $dynaCoreHelper;
    protected $shippingFee;
    protected $isSapSimOnlyPackage = array();
    protected $initialILSSocs = [];
    protected $processContext = null;
    /** @var Dyna_Checkout_Model_Sales_Order_Item[] $packageItems */
    protected $packageItems = array();
    // this will be an array of the for "soc" => "action" that will be used to check if the action for a soc was already evalued; to avoid different actions for the same soc

    // array containing the skus of master => slave(s) products
    protected $slaveProducts = array();

    // stores Master-Slave overwrite actions used in the FN multimapper
    protected $fnOverwrites = array();

    CONST PHONE_BOOK_STANDARD = 3;
    CONST PHONE_BOOK_NONE = 1;
    CONST PHONE_BOOK_EXTENDED = 2;
    CONST INFORMATION_EXCHANGE_EVERYTHING = 0;
    CONST INFORMATION_EXCHANGE_PHONE_INFO = 1;
    CONST INFORMATION_EXCHANGE_NO_INFO = 2;
    CONST CONNECTION_NONE = 1;
    CONST CONNECTION_FULL = 2;
    CONST CONNECTION_CONDENSED = 3;
    CONST GENDER_HERR = 1;
    CONST GENDER_FRAU = 2;
    CONST GENDER_FIRMA = 3;
    CONST NP_CHANGE_PROVIDER_YES = 1;
    CONST NP_CHANGE_PROVIDER_NO = 0;
    CONST NP_CHANGE_PROVIDER_DATA_LATER = 2;
    CONST TERMINATION_REASON_FILL_LATER = 1;
    CONST TERMINATION_REASON_NO_MIN_CONTRACT = 2;
    CONST TERMINATION_REASON_AGREEMENT_TERMINATED = 3;
    CONST PORTING_ALL_NUMBERS = 1;
    CONST PORTING_ONE_NUMBER = 2;
    CONST PORTING_MULTIPLE_NUMBERS = 3;
    CONST PORTING_NO_NUMBER = 4;
    CONST BACKEND_SAP = 'SAP';
    CONST BACKEND_KIAS = 'KIAS';
    CONST BILLING_LETTER = 'LETTER';
    CONST BILLING_PAPER = 'PAPER';
    CONST BILLING_WEBBILL = 'WEBBILL';
    CONST BILLING_ONLINE = 'ONLINE';
    CONST ORDER_TYPE_DSL_NEW = 'DSLnew';
    CONST ORDER_TYPE_DSL_OLD = 'DSLold';
    CONST ORDER_TYPE_MF_NEW = 'MFnew';
    CONST ORDER_TYPE_MF_OLD = 'MFold';
    CONST ORDER_TYPE_DSL_FROM_CABLE = 'DSLfromKABEL';
    CONST ORDER_TYPE_CABLE_NEW = 'KABELnew';
    CONST ORDER_TYPE_CABLE_OLD = 'KABELold';
    CONST ORDER_TYPE_CABLE_FROM_DSL = 'KABELfromDSL';
    CONST EXTRA_INDICATOR_HIS = 'Insurance';
    CONST COMPONENT_ACTION_ADD = 'ADD';
    CONST COMPONENT_ACTION_REMOVE = 'DROP';
    CONST COMPONENT_ACTION_REFUSE = 'REFUSE';
    CONST COMPONENT_ACTION_EXISTING = 'EXISTING';
    CONST COMPONENT_ACTION_CHANGE = 'CHANGE';
    CONST COMPONENT_ACTION_IGNORE = 'IGNORE';
    CONST COMPONENT_ACTIVITY_TYPE_INDICATION_MANUAL = 'MANUAL';
    CONST COMPONENT_ACTIVITY_TYPE_INDICATION_AUTOMATIC = 'AUTOMATIC';
    const DUMMY_PARTY_IDENTIFICATION = "000000000";

    public static function getOldPackageMappings()
    {
        return [
            Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN => self::ORDER_TYPE_DSL_OLD,
            Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD => self::ORDER_TYPE_CABLE_OLD,
            Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS => self::ORDER_TYPE_MF_OLD,
            Dyna_Customer_Model_Customer::ACCOUNT_CATEGORY_MOBILE_POSTPAID => self::ORDER_TYPE_MF_OLD,
            Dyna_Customer_Model_Customer::ACCOUNT_CATEGORY_MOBILE_PREPAID => self::ORDER_TYPE_MF_OLD,
            Dyna_Customer_Model_Customer::ACCOUNT_CATEGORY_CABLE => self::ORDER_TYPE_CABLE_OLD,
            Dyna_Customer_Model_Customer::ACCOUNT_CATEGORY_FIXED => self::ORDER_TYPE_DSL_OLD
        ];
    }

    public static function getNewPackageMappings()
    {
        return [
            Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN => self::ORDER_TYPE_DSL_NEW,
            Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD => self::ORDER_TYPE_CABLE_NEW,
            Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS => self::ORDER_TYPE_MF_NEW,
            Dyna_Customer_Model_Customer::ACCOUNT_CATEGORY_MOBILE_POSTPAID => self::ORDER_TYPE_MF_NEW,
            Dyna_Customer_Model_Customer::ACCOUNT_CATEGORY_MOBILE_PREPAID => self::ORDER_TYPE_MF_NEW,
            Dyna_Customer_Model_Customer::ACCOUNT_CATEGORY_CABLE => self::ORDER_TYPE_CABLE_NEW,
            Dyna_Customer_Model_Customer::ACCOUNT_CATEGORY_FIXED => self::ORDER_TYPE_DSL_NEW
        ];
    }

    public static function getMigrationMappings()
    {
        return [
            Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN => self::ORDER_TYPE_DSL_FROM_CABLE,
            Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD => self::ORDER_TYPE_DSL_FROM_CABLE
        ];
    }

    /**
     * Identify the appropriate value/label for the selected information exchange
     * @param $selected
     * @return string
     */
    public static function getInformationExchange($selected)
    {
        $value = '';
        switch ($selected) {
            case self::INFORMATION_EXCHANGE_EVERYTHING:
                $value = 'A';
                break;
            case self::INFORMATION_EXCHANGE_PHONE_INFO:
                $value = 'M';
                break;
            case self::INFORMATION_EXCHANGE_NO_INFO:
                $value = 'N';
                break;
        }

        return $value;
    }

    /**
     * Returns all customer entities
     * @param Dyna_Package_Model_Package[] $packages
     * @return array
     */
    public static function getCustomerEntities($packages)
    {
        $customerEntities = array();
        $installedBaseCustomerEntities = self::getInstalledBaseCustomerEntities();

        foreach ($packages as $package) {
            $parentAccountNumber = $package->getParentAccountNumber();
            $accountType = self::getPackageType($package->getType());
            
            if (isset($installedBaseCustomerEntities[$parentAccountNumber]) 
                && $installedBaseCustomerEntities[$parentAccountNumber]
                && isset($installedBaseCustomerEntities[$parentAccountNumber]['installed_base_entity'])
            ) {
                $installedBaseCustomerEntities[$parentAccountNumber]['installed_base_entity'] = false;
            } else if (!isset($customerEntities[$accountType])) {
                $customerEntities[$accountType] = [
                    'customer_number' => $parentAccountNumber,
                    'account_type' => $accountType,
                    'package_type' => strtolower($package->getType()),
                    'installed_base_entity' => false
                ];
            }
        }

        return ($installedBaseCustomerEntities + $customerEntities);
    }

    /**
     * Returns all installed base customer entities
     * @param Dyna_Package_Model_Package[] $packages
     * @return array
     */
    public static function getInstalledBaseCustomerEntities()
    {
        $customerEntities = array();
        $customerProducts = Mage::getSingleton('dyna_customer/session')->getCustomerProducts();

        foreach ($customerProducts as $legacySystem => $customers) {
            foreach ($customers as $customerNumber => $customer) {
                $packageType = '';

                $subscriptionDetails = [];
                foreach($customer['contracts'] as $cnt) {
                    if(isset($cnt['subscriptions'])) {
                        foreach($cnt['subscriptions'] as $subscription) {
                            if(isset($subscription['package_creation_type'])) {
                                $subscriptionDetails = $subscription;
                                break;
                            }
                        }
                    }
                }
                if (strtolower($legacySystem) == strtolower(Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD)) {
                    foreach ($subscriptionDetails['products'] as $products) {
                        foreach ($products as $product) {
                            if ($product['package_type']) {
                                $packageType = $product['package_type'];
                                break 2;
                            }
                        }
                    }
                } else {
                    $packageType = $subscriptionDetails['package_creation_type'];
                }

                $accountType = self::getPackageType($packageType);
                $customerEntities[$customerNumber] = [
                    'customer_number' => $customerNumber,
                    'account_type' => $accountType,
                    'package_type' => $packageType,
                    'installed_base_entity' => true
                ];
            }
        }

        return $customerEntities;
    }

    /**
     * Identify the appropriate value for the selected phonebook entry
     *
     * @param $phoneBookChoice
     * @return null|string
     */
    public static function getPhoneBookTypeValue($phoneBookChoice)
    {
        $value = null;
        switch ($phoneBookChoice) {
            case self::PHONE_BOOK_STANDARD:
                $value = 'Einzeleintrag';
                break;
            case self::PHONE_BOOK_EXTENDED:
                $value = 'Anlage';
                break;
            default:
                break;
        }

        return $value;
    }

    /**
     * Send the correct OIL package type based on OSE package type
     * @param $type
     * @return string
     */
    public static function getPackageType($type)
    {
        $category = "Unknown";
        switch (strtolower($type)) {
            case strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_INDEPENDENT):
            case strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_INTERNET_PHONE):
            case strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_TV):
            case strtolower(Dyna_Customer_Model_Customer::ACCOUNT_CATEGORY_CABLE):
                $category = Dyna_Customer_Model_Customer::ACCOUNT_CATEGORY_CABLE;
                break;
            case strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE):
                // Assuming Redplus is always a postpaid
            case strtolower(Dyna_Catalog_Model_Type::TYPE_REDPLUS):
                $category = Dyna_Customer_Model_Customer::ACCOUNT_CATEGORY_MOBILE_POSTPAID;
                break;
            case strtolower(Dyna_Catalog_Model_Type::TYPE_PREPAID):
                $category = Dyna_Customer_Model_Customer::ACCOUNT_CATEGORY_MOBILE_PREPAID;
                break;
            case strtolower(Dyna_Catalog_Model_Type::TYPE_FIXED_DSL):
            case strtolower(Dyna_Catalog_Model_Type::TYPE_LTE):
            case strtolower('Fixed'):
                $category = Dyna_Customer_Model_Customer::ACCOUNT_CATEGORY_FIXED;
                break;
            default:
                break;
        }

        return $category;
    }

    /**
     * Set checkout data
     * @param $data
     */
    public function setClientData($data, $customer)
    {
        $this->checkoutData = $data;
        $this->customer = $customer;
    }

    /**
     * Get PhoneBook personal data
     */
    public function getPhoneBookPersonalFields($accountType,$packageType,$packageId)
    {
        $selected = $this->getCheckout('other[phonebook_choice]['.$packageId.']['.$packageType.']');
        $profession = $this->getCheckout('other[extended]['.$packageId.']['.$packageType.'][profession]') ?? null;
        $keyword = $this->getCheckout('other[extended]['.$packageId.']['.$packageType.'][keyword]') ?? null;
        $data = '';
        switch ($selected) {
            case self::PHONE_BOOK_STANDARD:
                $isSoho = $this->getCheckout('customer[type]') == 'Soho';
                $contactPhoneNumbers = $accountType ? $this->getContactPhoneNumbers($isSoho,$accountType) : '';
                $data = array(
                    'PersonalDetails' => array(
                        'FirstName' => $this->getCheckout('customer[firstname]'),
                        'FamilyName' => $this->getCheckout('customer[lastname]'),
                        'Title' => $this->getCheckout('customer[prefix]'),
                        'ContactPhoneNumber' => $contactPhoneNumbers
                    ),
                    'Address' => null,
                    'Type' => Dyna_Superorder_Helper_Client::getPhoneBookTypeValue(Dyna_Superorder_Helper_Client::PHONE_BOOK_STANDARD),
                    'InverseSearchIndicator' => null,
                    'TelephoneInfoUse' => null,
                    'NamePrefix' => null,
                    'FaxDirectoryIndicator' => null,
                    'VoiceDirectoryIndicator' => null,
                    'NameSuffix' => null,
                    'InverseSearch' => null,
                    'InformationUse' => null,
                    'ElecListIndicator' => null,
                    'PrintListIndicator' => null,
                    'PublishAddress' => null,
                    'AltUserFirstName' => null,
                    'AltUserLastName' => null,
                    'Profession' => $profession,
                    'KeyWord' => $keyword,
                );
                break;
            case self::PHONE_BOOK_NONE:
                $data = null;
                break;
            case self::PHONE_BOOK_EXTENDED:
                break;
        }

        return $data;
    }

    /**
     * Method to either search and retrieve the value of a checkout field or true/false if a compare value is provided
     * @param $field
     * @param null $compareValue
     * @return null|string
     * @internal param $value
     */
    public function getCheckout($field, $compareValue = null)
    {
        $fieldValue = trim(Mage::helper('omnius_checkout')->getCheckoutFieldData($this->checkoutData, $field));

        if ($compareValue) {
            return $fieldValue == $compareValue ? 'true' : 'false';
        }
        return isset($fieldValue) ? $fieldValue : null;
    }

    /**
     * Get the submitOrder
     * @param int $valueOrField The value or the field from which the value must be retrieved
     * @return string
     */
    public function getSalutation($valueOrField)
    {
        if (!is_numeric($valueOrField)) {
            $valueOrField = $this->getCheckout($valueOrField);
        }

        $salutation = null;

        switch ($valueOrField) {
            case self::GENDER_HERR:
                $salutation = 'R';
                break;
            case self::GENDER_FRAU:
                $salutation = 'S';
                break;
            case self::GENDER_FIRMA:
                $salutation = 'F';
                break;
            default :
                $salutation = 'N';
        }

        return $salutation;
    }

    /**
     * Get the payment method code associated to the user selection
     * @param $$package The package model
     * @return string
     */
    public function getPaymentMethod( $package, $chargeType = null )
    {
        if (is_null($chargeType)) {
            if ($package->isFixed() || $package->isCable()) {
                $chargeType = 'RC';
            } else {
                $chargeType = 'OTC';
            }
        }

        switch ($chargeType) {
            case 'RC':
                $value = $this->getCheckout('payment[monthly][type]');
                if ($value == Dyna_Checkout_Helper_Fields::PAYMENT_SPLIT_PAYMENT) {
                    $value = $this->getCheckout('payment[pakket][' . $package->getPackageId() . '][type]');
                }
                break;
            case 'OTC':
                $value = $this->getCheckout('payment[one_time][type]');
                break;
        }

        if (strpos($value, "|") !== FALSE) {
            $value = substr($value, 0, strpos($value, "|"));
        }


        $payment = null;

        foreach ($package->getItems() as $item) {
            if (Dyna_Catalog_Model_Type::getCheckoutSelfReferrerSkus() == $item->getData('sku')) {
                switch ($value) {
                    case Dyna_Checkout_Helper_Fields::PAYMENT_DIRECT_DEBIT:
                    case Dyna_Checkout_Helper_Fields::PAYMENT_DIRECT_DEBIT_REUSE:
                    case Dyna_Checkout_Helper_Fields::PAYMENT_DIRECT_DEBIT_DIFF:
                        $payment = 'GI';
                        break;
                }
                break;
            }
        }

        if( is_null( $payment )) {
            switch ($value) {
                case Dyna_Checkout_Helper_Fields::PAYMENT_CASH_ON_DELIVERY:
                    $payment = 'CA';
                    break;
                case Dyna_Checkout_Helper_Fields::PAYMENT_DIRECT_DEBIT:
                case Dyna_Checkout_Helper_Fields::PAYMENT_DIRECT_DEBIT_REUSE:
                case Dyna_Checkout_Helper_Fields::PAYMENT_DIRECT_DEBIT_DIFF:
                case Dyna_Checkout_Helper_Fields::PAYMENT_CHECKMO:
                    $payment = 'DD';
                    break;
            }
        }

        return $payment;
    }

    /**
     * Get all the rufnumber form field values
     */
    public function getContactPhoneNumbers($isSoho, $accountType)
    {
        /** @var Omnius_Service_Helper_Data $serviceHelper */
        $serviceHelper = Mage::helper('omnius_service');
        $numberData = array();

        $i = 0;
        while ($this->getCheckout("customer[contact_details][phone_list][$i][telephone]")) {
            $localAreaCode = $this->getCheckout("customer[contact_details][phone_list][$i][telephone_prefix]");
            $phoneNumber = $this->getCheckout("customer[contact_details][phone_list][$i][telephone]");
            $numberData[] = array(
                'CountryCode' => ($serviceHelper->getGeneralConfig('country_code') ? $serviceHelper->getGeneralConfig('country_code') : Dyna_Customer_Model_Customer::COUNTRY_CODE),
                'LocalAreaCode' => $localAreaCode,
                'PhoneNumber' => $phoneNumber,
                'Type' => $this->getContactPhoneNumberType("customer[contact_details][phone_list][$i][telephone_type]")
            );
            $i++;
        }
        $localAreaCode = $this->getCheckout("customer[contact_person_details][0][telephone_prefix]");
        $phoneNumber = $this->getCheckout("customer[contact_person_details][0][telephone]");
        if ($localAreaCode && $phoneNumber) {
            $numberData[] = array(
                'CountryCode' => ($serviceHelper->getGeneralConfig('country_code') ? $serviceHelper->getGeneralConfig('country_code') : Dyna_Customer_Model_Customer::COUNTRY_CODE),
                'LocalAreaCode' => $localAreaCode,
                'PhoneNumber' => $phoneNumber,
                'Type' => $this->getContactPhoneNumberType("customer[contact_person_details][0][telephone_type]")
            );
        }


        return !empty($numberData) && (!empty($numberData[0]) || !empty($numberData['PhoneNumber'])) ? $numberData : null;
    }

    /**
     * Get the type of contact phone number
     * @param $field
     * @return string
     */
    public function getContactPhoneNumberType($field)
    {
        switch ($this->getCheckout($field)) {
            case 'landline':
                return 'FIXED_PHONE';
            case 'mobile':
                return 'MOBILE_PHONE';
        }

        return null;
    }

    /**
     * Get the customer type
     * @param $field
     * @return null|string
     */
    public function getCustomerType($field)
    {
        $customerType = null;

        switch (strtolower($this->getCheckout($field))) {
            case 'privat':
                $customerType = Dyna_Customer_Model_Client_RetrieveCustomerInfo::PARTY_TYPE_PRIVATE;
                break;
            case 'soho':
                $customerType = Dyna_Customer_Model_Client_RetrieveCustomerInfo::PARTY_TYPE_SOHO;
                break;
            default :
                $customerType = '';
        }

        return $customerType;
    }

    /**
     * Generate list of components
     * @param $package Dyna_Package_Model_Package
     * @return mixed
     */
    public function getComponents($package)
    {
        /**
         * @var $dynaCoreHelper Dyna_Core_Helper_Data
         */
        $this->dynaCoreHelper = Mage::helper('dyna_core');
        $this->inputParams = [
            'customer' => Mage::getModel('dyna_configurator/expression_customer'),
            'order' => Mage::getModel('dyna_configurator/expression_order')->setSuperOrder($package->getSuperOrder())->setPackage($package),
            'availability' => Mage::getModel('dyna_configurator/expression_availability'),
            'orderPackage' => Mage::getModel('dyna_configurator/expression_package')->setPackage($package),
            'agent' => Mage::getModel('dyna_configurator/expression_agent'),
        ];

        if (empty($this->shippingFee)) {
            $allSuperorderItems = Mage::getModel('superorder/superorder')->load($package->getOrderId())->getOrderedItems(true);
            if (!empty($allSuperorderItems)) {
                $shippingFee = false;

                foreach ($allSuperorderItems as $items) {
                    foreach ($items as $item) {
                        if ($item->getSku() == Dyna_Catalog_Model_Type::MOBILE_SHIPPING_SKU) {
                            $shippingFee = $item->getPriceInclTax();
                            break;
                        }
                    }
                }

                if ($shippingFee !== false) {
                    $packageSkus = array_keys($package->getAllProductIs());
                    $hasSAPAddons = Mage::getModel("dyna_multimapper/addon")->quoteSkuMappersHaveSAPAddons(
                        $packageSkus,
                        $package->getSaleType()
                    );

                    if ($hasSAPAddons || $package->hasSimOnly()) {
                        $feeDecimal = number_format((float)$shippingFee, 2, '.', '');
                        $zeroDecimal = number_format((float)0, 2, '.', '');

                        $this->shippingFee = array(
                            'ShippingFeeNetPrice' => $shippingFee ? $feeDecimal : null,
                            'ShippingFeeText' => $feeDecimal > $zeroDecimal
                                ? Mage::helper('dyna_checkout')->__('Shipping')
                                : null
                        );
                    }
                }
            }
        }
  
        $this->log('');
        $this->log('Evaluating multimappers for package: ' . $package->getPackageId());

        if ($package->getInstalledBaseProducts() && $package->getInitialInstalledBaseProducts()) {
            if ($package->isFixed()) {
                return $this->getComponentsForFixedInstalledBase($package);
            }
            return $this->getComponentsForInstalledBase($package);
        }
        if ($package->isFixed()) {
            return $this->getComponentsForFixedPackage($package);
        } elseif ($package->isMobile()) {
            return $this->getComponentsForMobilePackage($package);
        } elseif ($package->isCable()) {
            return $this->getComponentsForCablePackage($package);
        }

        return null;
    }

    public function getComponentsForFixedInstalledBase($package)
    {
        $skuAction = array();
        $initialSkus = explode(',', $package->getInitialInstalledBaseProducts());
        $skusAfterBundleRules = explode(',', $package->getInstalledBaseProducts());
        $componentModel = Mage::getModel('dyna_superorder/component');
        $products = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToFilter('sku', array('in' => $skusAfterBundleRules));

        $result = [];

        /** @var Dyna_Catalog_Model_Product $product */
        foreach ($products as $product) {
            $this->log('');
            $this->log('Evaluating SKU: ' . $product->getSku());
            //first, find if there is a mapper for that sku
            $itemMapper = Mage::getModel('dyna_multimapper/mapper')->getSingleMappersBySku($product->getSku());
            if (!$itemMapper || !$itemMapper->getId()) {
                continue;
            }
            $this->log("Multimapper entries fetched:");
            $this->log("----------------------------");
            foreach ($itemMapper->getData() as $key => $value) {
                if (!empty($value)) {
                    $this->log("... $key: $value");
                }
            }

            $itemMapperId = $itemMapper->getData('entity_id');

            //get all available addons for that mapper
            $itemAddons = Mage::getModel('dyna_multimapper/addon')
                ->getAddonByMapperId($itemMapperId, $package->getSaleType());
            if (!$itemAddons->count()) {
                continue;
            }
            $this->log("... ... Multimapper details entries:");
            $this->log("... ... ----------------------------");

            $extraIndicator = $product->getExtraIndicator('HIS');
            $shippingFee = $this->getShippingFeeNetPrice();
            $componentType = $itemMapper->getComponentType();
            $tarrifOptionsItems = ['serviceCode', 'serviceBillingName'];
            $componentModel->setAddons($itemAddons);

            /**
             *  The Access, Internet and Hardware components should be copied to the SubmitOrder;
             * ComponentActivityType should be set to "EXISTING".
             */

            if (in_array($product->getSku(), $initialSkus)) {
                /**
                 * The Voice function should be split into 2 components:
                 * (a) The original Voice component without the tarifOption with ComponentActivityType = "CHANGE"
                 * (b) a new component of type "tariffOption" with ComponentActivityType = "ADD"
                 */
                if ($componentModel->isVoice()) {

                    $currentResult = array(
                        'Action' => 'CHANGE',
                        'ComponentType' => null,
                        'ComponentActivityTypeIndication' => self::COMPONENT_ACTIVITY_TYPE_INDICATION_AUTOMATIC,
                        'SkuID' => $product->getSku(),
                        'ComponentAdditionalData' => [],
                        'TariffOption' => null,
                        'TariffOptionDescription' => null,
                        'TariffOptionName' => null,
                        'Agreement' => null,
                        'SAPArticleID' => null,
                        'Country' => null,
                        'DeviceID' => null,
                        'ProductID' => null,
                        'SAPArticleNetPrice' => null,
                        'SAPArticleGrossPrice' => null,
                        'SAPDiscount1' => null,
                        'SAPDiscount2' => null,
                        'ShippingFeeNetPrice' => $shippingFee['ShippingFeeNetPrice'],
                        'ShippingFeeText' => $shippingFee['ShippingFeeText']
                    );
                    foreach ($itemAddons as $itemAddon) {
                        /** ComponentAdditionalData blocks for the existing Access,Internet,Hardware and Voice components
                         * with <lean::Code> = processingType must be removed. */
                        if ($itemAddon->getData('addon_name') != 'processingType' && !in_array($itemAddon->getAddonName(), $tarrifOptionsItems)) {
                            $currentResult['ComponentAdditionalData'][] = [
                                'Code' => $itemAddon->getData('addon_name'),
                                'Value' => $itemAddon->getData('addon_value'),
                                'AdditionalValue' => $itemAddon->getData('addon_additional_value')
                            ];
                        }
                        foreach ($itemAddon->getData() as $key => $value) {
                            if (!empty($value)) {
                                $this->log("... ... $key: $value");
                            }
                        }
                    }
                    $skuAction[$product->getSku()] = $currentResult['Action'];
                    $result[] = $currentResult;


                    /** * (b) a new component of type "tariffOption" with ComponentActivityType = "ADD" */
                    $currentResult = array(
                        'Action' => self::COMPONENT_ACTION_ADD,
                        'ComponentType' => 'tariffOption',
                        'ComponentActivityTypeIndication' => self::COMPONENT_ACTIVITY_TYPE_INDICATION_AUTOMATIC,
                        'SkuID' => $product->getSku(),
                        'ComponentAdditionalData' => [],
                        'TariffOption' => null,
                        'TariffOptionDescription' => null,
                        'TariffOptionName' => null,
                        'Agreement' => null,
                        'SAPArticleID' => null,
                        'Country' => null,
                        'DeviceID' => null,
                        'ProductID' => null,
                        'SAPArticleNetPrice' => null,
                        'SAPArticleGrossPrice' => null,
                        'SAPDiscount1' => null,
                        'SAPDiscount2' => null,
                        'ShippingFeeNetPrice' => $shippingFee['ShippingFeeNetPrice'],
                        'ShippingFeeText' => $shippingFee['ShippingFeeText']
                    );

                    foreach ($itemAddons as $itemAddon) {
                        /** ComponentAdditionalData blocks for the existing Access,Internet,Hardware and Voice components
                         * with <lean::Code> = processingType must be removed. */
                        if (in_array($itemAddon->getAddonName(), $tarrifOptionsItems) || $itemAddon->getAddonName() == 'function@ID') {
                            $currentResult['ComponentAdditionalData'][] = [
                                'Code' => $itemAddon->getData('addon_name'),
                                'Value' => $itemAddon->getData('addon_value'),
                                'AdditionalValue' => $itemAddon->getData('addon_additional_value')
                            ];
                        }

                    }
                    $skuAction[$product->getSku()] = $currentResult['Action'];
                    $result[] = $currentResult;


                } else {
                    $currentResult = array(
                        'Action' => 'EXISTING',
                        'ComponentType' => $extraIndicator == null ? $componentType : $extraIndicator,
                        'ComponentActivityTypeIndication' => self::COMPONENT_ACTIVITY_TYPE_INDICATION_AUTOMATIC,
                        'SkuID' => $product->getSku(),
                        'ComponentAdditionalData' => [],
                        'TariffOption' => null,
                        'TariffOptionDescription' => null,
                        'TariffOptionName' => null,
                        'Agreement' => null,
                        'SAPArticleID' => null,
                        'Country' => null,
                        'DeviceID' => null,
                        'ProductID' => null,
                        'SAPArticleNetPrice' => null,
                        'SAPArticleGrossPrice' => null,
                        'SAPDiscount1' => null,
                        'SAPDiscount2' => null,
                        'ShippingFeeNetPrice' => $shippingFee['ShippingFeeNetPrice'],
                        'ShippingFeeText' => $shippingFee['ShippingFeeText']
                    );

                    foreach ($itemAddons as $itemAddon) {
                        /** ComponentAdditionalData blocks for the existing Access,Internet,Hardware and Voice components
                         * with <lean::Code> = processingType must be removed. */
                        if ($itemAddon->getData('addon_name') != 'processingType') {
                            $currentResult['ComponentAdditionalData'][] = [
                                'Code' => $itemAddon->getData('addon_name'),
                                'Value' => $itemAddon->getData('addon_value'),
                                'AdditionalValue' => $itemAddon->getData('addon_additional_value')
                            ];
                        }

                    }
                    $skuAction[$product->getSku()] = $currentResult['Action'];
                    $result[] = $currentResult;
                }
            } else {
                /** * a new component of type "tariffOption" with ComponentActivityType = "ADD" */
                $currentResult = array(
                    'Action' => self::COMPONENT_ACTION_ADD,
                    'ComponentType' => 'tariffOption',
                    'ComponentActivityTypeIndication' => self::COMPONENT_ACTIVITY_TYPE_INDICATION_AUTOMATIC,
                    'SkuID' => $product->getSku(),
                    'ComponentAdditionalData' => [],
                    'TariffOption' => null,
                    'TariffOptionDescription' => null,
                    'TariffOptionName' => null,
                    'Agreement' => null,
                    'SAPArticleID' => null,
                    'Country' => null,
                    'DeviceID' => null,
                    'ProductID' => null,
                    'SAPArticleNetPrice' => null,
                    'SAPArticleGrossPrice' => null,
                    'SAPDiscount1' => null,
                    'SAPDiscount2' => null,
                    'ShippingFeeNetPrice' => $shippingFee['ShippingFeeNetPrice'],
                    'ShippingFeeText' => $shippingFee['ShippingFeeText']
                );

                foreach ($itemAddons as $itemAddon) {

                    if ((in_array($itemAddon->getAddonName(), $tarrifOptionsItems) || $itemAddon->getAddonName() == 'function@ID')) {
                        $currentResult['ComponentAdditionalData'][] = [
                            'Code' => $itemAddon->getData('addon_name'),
                            'Value' => $itemAddon->getData('addon_value'),
                            'AdditionalValue' => $itemAddon->getData('addon_additional_value')
                        ];
                    }
                }
                $skuAction[$product->getSku()] = $currentResult['Action'];
                $result[] = $currentResult;

                /** a new component of type "conditionService" with ComponentActivityType = "ADD" */
                $currentResult = array(
                    'Action' => self::COMPONENT_ACTION_ADD,
                    'ComponentType' => 'conditionService',
                    'ComponentActivityTypeIndication' => self::COMPONENT_ACTIVITY_TYPE_INDICATION_AUTOMATIC,
                    'SkuID' => $product->getSku(),
                    'ComponentAdditionalData' => [],
                    'TariffOption' => null,
                    'TariffOptionDescription' => null,
                    'TariffOptionName' => null,
                    'Agreement' => null,
                    'SAPArticleID' => null,
                    'Country' => null,
                    'DeviceID' => null,
                    'ProductID' => null,
                    'SAPArticleNetPrice' => null,
                    'SAPArticleGrossPrice' => null,
                    'SAPDiscount1' => null,
                    'SAPDiscount2' => null,
                    'ShippingFeeNetPrice' => $shippingFee['ShippingFeeNetPrice'],
                    'ShippingFeeText' => $shippingFee['ShippingFeeText']
                );

                foreach ($itemAddons as $itemAddon) {
                    if (strpos($itemAddon->getData('addon_name'), 'condition') !== false) {
                        $currentResult['ComponentAdditionalData'][] = [
                            'Code' => $itemAddon->getData('addon_name'),
                            'Value' => $itemAddon->getData('addon_value'),
                            'AdditionalValue' => $itemAddon->getData('addon_additional_value')
                        ];
                    }
                }
                $skuAction[$product->getSku()] = $currentResult['Action'];
                $result[] = $currentResult;

            }
        }

        foreach ($result as $res){
            if (isset($skuAction[$res['SkuID']])){
                $skuAction[$res['SkuID']]=$res['Action'];
            }
        }

        foreach ($package->getItems() as $item) {
            if (isset($skuAction[$item->getSku()])) {
                $item->setProductActivityType($skuAction[$item->getSku()])->save();
            }
        }
        return empty($result) ? null : $result;
    }

    /**
     * Special handling for installed base products
     * @param $package
     * @return array|null
     */
    public function getComponentsForInstalledBase($package)
    {
        $skuAction = array();
        $result = array();
        $newSkus = explode(',', $package->getInstalledBaseProducts());
        $oldSkus = explode(',', $package->getInitialInstalledBaseProducts());
        $addedSkus = array_diff($newSkus, $oldSkus);
        $removedSkus = array_diff($oldSkus, $newSkus);
        $filterSkus = array_merge($addedSkus, $removedSkus);
        $packageData = $package->getData();
        if (empty($addedSkus) && empty($removedSkus) && $packageData['bundle_types'] == Dyna_Bundles_Model_BundleRule::TYPE_RED_PLUS) {
            $filterSkus = $newSkus;
        }
        $products = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToFilter('sku', array('in' => $filterSkus));

        if (!$products->count()) {
            return null;
        }

        //loop through all products and try to build the components
        /** @var Dyna_Catalog_Model_Product $product */
        foreach ($products as $product) {
            $productSku = $product->getSku();
            $this->log('');
            $this->log('Evaluating SKU: ' . $productSku);

            //first, find if there is a mapper for that sku
            $itemMapper = Mage::getModel('dyna_multimapper/mapper')->getMappersBySku($productSku)->getFirstItem();
            $itemMapperId = $itemMapper->getId();
            if (!$itemMapperId) {
                continue;
            }
            $this->log("Multimapper entries fetched:");
            $this->log("----------------------------");
            foreach ($itemMapper->getData() as $key => $value) {
                if (!empty($value)) {
                    $this->log("... $key: $value");
                }
            }

            //get all available addons for that mapper
            $itemAddons = Mage::getModel('dyna_multimapper/addon')
                ->getAddonByMapperId($itemMapperId);
            if ($package->isMobile() || $package->isCable()) {
                $itemAddons->addFieldToFilter('technical_id', array('neq' => 'NULL'));
            }

            if (!$itemAddons->count()) {
                continue;
            }
            $this->log("... ... Multimapper details entries:");
            $this->log("... ... ----------------------------");

            $extraIndicator = $product->getExtraIndicator('HIS');

            $shippingFee = $this->getShippingFeeNetPrice();
            foreach ($itemAddons as $itemAddon) {
                $componentType = null;
                if ($itemAddon->getData('addon_name') == 'ComponentType') {
                    $componentType = $itemAddon->getData('addon_value');
                }

                $componentAction = in_array($productSku, $removedSkus) ? self::COMPONENT_ACTION_REMOVE : self::COMPONENT_ACTION_ADD;

                // VFDED1W3S-3007: Submit bundle in migrate or move scenario
                if ($package->isMobile()) {
                    $existingBundleMarker = $this->packageContainsBundleMarker($package);
                    $newBundleMarker = ($itemAddon->getTechnicalId() == Dyna_Customer_Model_Client_RetrieveCustomerInfo::KIAS_ELIGIBLE_BUNDLE_SOC);

                    if ($existingBundleMarker && $newBundleMarker) {
                        $componentAction = self::COMPONENT_ACTION_IGNORE;
                    }
                }

                $result[] = array(
                    'Action' => $componentAction,
                    'ComponentType' => ($package->isMobile() && $itemMapper->getComponentType())
                        ?  $itemMapper->getComponentType()
                        : ($extraIndicator == null ? $componentType : $extraIndicator),
                    'ComponentActivityTypeIndication' => self::COMPONENT_ACTIVITY_TYPE_INDICATION_AUTOMATIC,
                    'SkuID' => $productSku,
                    'ComponentAdditionalData' => $package->isFixed() && $componentType != 'ComponentType' ? array(
                        'Code' => $itemAddon->getData('addon_name'),
                        'Value' => $itemAddon->getData('addon_value'),
                        'AdditionalValue' => $itemAddon->getData('addon_additional_value')
                    ) : null,
                    'TariffOption' => ($product->isSubscription() && $itemAddon->getBackend() != self::BACKEND_SAP) ? trim($itemAddon->getTechnicalId()) : null,
                    'TariffOptionDescription' => null,
                    'TariffOptionName' => null,
                    'Agreement' => ($product->isSubscription() || $itemAddon->getBackend() != self::BACKEND_KIAS) ? null : array(
                        'AgreementSOC' => $itemAddon->getTechnicalId(),
                        'AgreementSOCDescription' => null,
                        'AgreementServiceType' => null,
                        'AgreementEffectiveDate' => null,
                        'AgreementExpirationDate' => null,
                        'AgreementTariffOption' => null
                    ),
                    'SAPArticleID' => $itemAddon->getBackend() == self::BACKEND_SAP ? $itemAddon->getTechnicalId() : null,
                    'Country' => null,
                    'DeviceID' => null,
                    'ProductID' => $package->isCable() ? $itemAddon->getTechnicalId() : null,
                    'SAPArticleNetPrice' => null,
                    'SAPArticleGrossPrice' => null,
                    'SAPDiscount1' => null,
                    'SAPDiscount2' => null,
                    'ShippingFeeNetPrice' => $shippingFee['ShippingFeeNetPrice'],
                    'ShippingFeeText' => $shippingFee['ShippingFeeText']
                );
                $skuAction[$productSku] = $result['Action'];

                foreach ($result as $keyRes => $res) {
                    if ($result[$keyRes]['SAPArticleID']) {
                        $result[$keyRes]['SAPArticleNetPrice'] = 0;
                        $result[$keyRes]['SAPArticleGrossPrice'] = 0;
                        $result[$keyRes]['SAPDiscount1'] = 0;
                        $result[$keyRes]['SAPDiscount2'] = 0;
                    }
                }
                foreach ($itemAddon->getData() as $key => $value) {
                    if (!empty($value)) {
                        $this->log("... ... $key: $value");
                    }
                }
            }
        }

        foreach ($result as $res){
            if (isset($skuAction[$res['SkuID']])){
                $skuAction[$res['SkuID']]=$res['Action'];
            }
        }

        foreach ($package->getItems() as $item) {
            if (isset($skuAction[$item->getSku()])) {
                $item->setProductActivityType($skuAction[$item->getSku()])->save();
            }
        }
        return empty($result) ? null : $result;
    }

    /**
     * Returns the order item with the specified SKU
     *
     * @param $sku
     * @return Dyna_Checkout_Model_Sales_Order_Item|null
     */
    protected function getItemBySku($sku)
    {
        foreach ($this->packageItems as $item) {
            if ($item->getSku() == $sku) {
                return $item;
            }
        }

        return null;
    }

    /**
     * Builds an array of available master products
     */
    protected function buildMasterSlaveProducts()
    {
        foreach ($this->packageItems as $item) {
            $this->slaveProducts[$item->getSku()] = $item->getProduct()->getSlaveSkus();
        }
    }

    /**
     * Checks whether the system action of the order item matches the basket action
     * of the addon
     *
     * @param Dyna_Checkout_Model_Sales_Order_Item $item
     * @param Dyna_MultiMapper_Model_Addon $addon
     * @return bool
     */
    protected function hasValidBasketAction($item, $addon)
    {
        $result = false;

        $this->log(sprintf("... Evaluating basket action for item %s and addon %s", $item->getSku(), $addon->getId()));
        $this->log(sprintf("... Item system action: %s, addon basket action: %s", $item->getSystemAction(), $addon->getBasketAction()));

        // if the addon does not have a basket_action don't filter it out
        if (!$addon->getBasketAction()) {
            $this->log("... Addon is VALID - no basket action set");
            return true;
        }

        switch ($addon->getBasketAction()) {
            case Dyna_MultiMapper_Model_Addon::BASKET_ACTION_ADD:
                $systemAction = $item->getSystemAction();
                $addActions = array(
                    Dyna_Checkout_Model_Sales_Quote_Item::SYSTEM_ACTION_ADD,
                    Dyna_Checkout_Model_Sales_Quote_Item::SYSTEM_ACTION_NOT_APPLICABLE
                );
                $result = (!$systemAction || in_array($systemAction, $addActions)) && !$this->isProductChanged($item);
                break;
            case Dyna_MultiMapper_Model_Addon::BASKET_ACTION_KEEP:
                $result = $item->getSystemAction() == Dyna_Checkout_Model_Sales_Quote_Item::SYSTEM_ACTION_EXISTING;
                break;
            case Dyna_MultiMapper_Model_Addon::BASKET_ACTION_REMOVE:
                $result = $item->getIsContractDrop();
                break;
            case Dyna_MultiMapper_Model_Addon::BASKET_ACTION_CHANGE:
                $result = ($this->processContext != Dyna_Catalog_Model_ProcessContext::ACQ) && $this->isProductChanged($item);
                break;
            default:
                break;
        }

        if ($result) {
            $this->log("... Addon is VALID");
        } else {
            $this->log("... Addon is INVALID");
        }
        return $result;
    }

    /**
     * Generate component node for a fixed package
     * @param $package
     * @return mixed
     */
    public function getComponentsForFixedPackage(Dyna_Package_Model_Package $package)
    {
        // Very important to set the package Process Context
        $this->processContext = $package->getSaleType();
        $skuAction = array();
        $result = array();
        $items = $package->getItems();
        $this->packageItems = $items;
        $componentType = null;

        $this->buildMasterSlaveProducts();

        /**
         * For all items in the order, retrieve their mappers and group them
         * by ogw_component_id_reference. A group ends up as a Component in process order.
         * ComponentAdditionalData nodes are built based on the addons of ALL mappers in the group.
         *
         * @ VFDED1W3S-3261
         * SkuId => send all skus for all mappers in the group
         * For ComponentType, Action, ComponentActivityTypeIndication get the value from the
         * first product in the group.
         */

        $componentGroups = [];
        foreach ($items as $item) {
            $mappers = Mage::getModel('dyna_multimapper/mapper')->getMappersBySku($item->getSku());
            $skuAction[$item->getSku()] = $this->getComponentAction($item);

            /** @var Dyna_MultiMapper_Model_Mapper $mapper */
            foreach ($mappers as $mapper) {
                $ogwComponentId = $mapper->getData('ogw_component_id_reference');

                $componentGroups[$ogwComponentId]['mappers'][] = $mapper;
                $componentGroups[$ogwComponentId]['skus'][] = $mapper->getSku();

                if (!isset($componentGroups[$ogwComponentId]['ComponentType'])) {
                    $componentGroups[$ogwComponentId]['ComponentType'] = $mapper->getComponentType() ?: null;
                }
                if (!isset($componentGroups[$ogwComponentId]['ComponentActivityTypeIndication'])) {
                    $componentGroups[$ogwComponentId]['ComponentActivityTypeIndication'] = $this->getComponentActivityTypeIndication($item);
                }
                if (!isset($componentGroups[$ogwComponentId]['Action'])) {
                    $componentGroups[$ogwComponentId]['Action'] = $this->getFnComponentAction($item);
                }
            }
        }

        // 1 group = 1 component
        foreach ($componentGroups as $groupData) {
            $additionalData = [];
            $groupSkus = [];
            /** @var Dyna_MultiMapper_Model_Mapper $mapper */
            $entriedHeaderPrinted = false;
            foreach ($groupData['mappers'] as $mapper) {
                if (!$entriedHeaderPrinted) {
                    $this->log("Multimapper entries fetched:");
                    $this->log("----------------------------");
                    $entriedHeaderPrinted = true;
                } else {
                    $this->log('...');
                }
                foreach ($mapper->getData() as $key => $value) {
                    if (!empty($value)) {
                        $this->log("... $key: $value");
                    }
                }
                if ($this->serviceExpressionIsFalse($mapper)) {
                    continue;
                }
                $this->log('... Service expression was evaluated to: true');

                $itemMapperId = $mapper->getId();
                $itemAddons = Mage::getModel('dyna_multimapper/addon')
                    ->getAddonsWithDirection($itemMapperId, $this->processContext, Dyna_MultiMapper_Model_Mapper::DIRECTION_OUT);

                $detailsHeaderPrinted = false;
                $this->log('');
                foreach ($itemAddons as $itemAddon) {
                    if ($this->serviceExpressionIsFalse($itemAddon)) {
                        continue;
                    }
                    if (!$this->hasValidBasketAction($this->getItemBySku($mapper->getSku()), $itemAddon)) {
                        continue;
                    }
                    $componentType = null;
                    if ($itemAddon->getData('addon_name') == 'ComponentType') {
                        $componentType = $itemAddon->getData('addon_value');
                    }
                    if ($componentType != 'ComponentType') {
                        if ($addonData = $this->getFnComponentAdditionalData($this->getItemBySku($mapper->getSku()), $mapper, $itemAddon, $package->getParentAccountNumber(), $package->getServiceLineId())) {
                            // store SKUs of the valid addons
                            $groupSkus[] = $mapper->getSku();
                            $additionalData[] = $addonData;
                        }

                        if (!$detailsHeaderPrinted) {
                            $this->log("... ... Multimapper details entries:");
                            $this->log("... ... ----------------------------");
                            $detailsHeaderPrinted = true;
                        } else {
                            $this->log('... ...');
                        }
                        foreach ($itemAddon->getData() as $key => $value) {
                            if (!empty($value)) {
                                $this->log("... ... $key: $value");
                            }
                        }
                    }
                }
            }

                $shippingFee = $this->getShippingFeeNetPrice();
            if (!empty($additionalData)) {
                // The SkuID field should contain only the SKUs of the products for which a valid addon was found and a component will be sent
                $uniqueSkus = array_intersect(array_unique($groupData['skus']), array_unique($groupSkus));

                $component = array(
                    'ComponentType' => $groupData['ComponentType'],
                    'SkuID' => implode(',', $uniqueSkus),
                    'ComponentAdditionalData' => $additionalData,
                    'TariffOption' => null,
                    'TariffOptionDescription' => null,
                    'TariffOptionName' => null,
                    'Agreement' => null,
                    'SAPArticleID' => null,
                    'Country' => null,
                    'DeviceID' => null,
                    'ProductID' => null,
                    'SAPArticleNetPrice' => null,
                    'SAPArticleGrossPrice' => null,
                    'SAPDiscount1' => null,
                    'SAPDiscount2' => null,
                    'ShippingFeeNetPrice' => $shippingFee['ShippingFeeNetPrice'],
                    'ShippingFeeText' => $shippingFee['ShippingFeeText'],
                    'ComponentActivityTypeIndication' => $groupData['ComponentActivityTypeIndication'],
                    'Action' => $groupData['Action']
                );

                foreach ($uniqueSkus as $itemSku) {
                    $skuAction[$itemSku] = $component['Action'];
                }
                $result[] = $component;
            }
        }


        foreach ($result as $res){
            if (isset($skuAction[$res['SkuID']])){
                $skuAction[$res['SkuID']]=$res['Action'];
            }
        }

        foreach ($package->getItems() as $item) {
            if (isset($skuAction[$item->getSku()])) {
                $item->setProductActivityType($skuAction[$item->getSku()])->save();
            }
        }

        foreach ($result as &$component) {
            $componentSkus = explode(',', $component['SkuID']);

            foreach ($componentSkus as $componentSku) {
                if (isset($this->fnOverwrites[$componentSku])) {
                    $overwriteData = $this->fnOverwrites[$componentSku];

                    // loop through all Component Additional data and replace the addon value of the master with the one in the overwrites array
                    foreach ($component['ComponentAdditionalData'] as &$additionalData) {
                        if ($additionalData['Code'] == $overwriteData['addon_name']
                            && $additionalData['AdditionalValue'] == $overwriteData['addon_additional_value']) {
                            $additionalData['Value'] = $overwriteData['addon_value'];
                        }
                    }
                }
            }
        }

        return empty($result) ? null : $result;
    }

    /**
     * @param Dyna_Checkout_Model_Sales_Order_Item $slaveProduct
     * @return int|null|string
     */
    protected function getMasterProductSku($slaveProduct)
    {
        foreach ($this->slaveProducts as $masterSku => $slaves) {
            if (in_array($slaveProduct->getSku(), $slaves)) {
                return $masterSku;
            }
        }

        return null;
    }

    /**
     * @param Dyna_Checkout_Model_Sales_Order_Item $slaveItem
     * @param Dyna_MultiMapper_Model_Addon $slaveAddon
     */
    protected function overwriteMaster($slaveItem, $slaveAddon)
    {
        $masterSku = $this->getMasterProductSku($slaveItem);

        if ($masterSku) {
            $this->fnOverwrites[$masterSku] = array(
                'slaveSKU' => $slaveItem->getSku(),
                'addon_name' => $slaveAddon->getData('addon_name'),
                'addon_value' => $slaveAddon->getData('addon_value'),
                'addon_additional_value'=> $slaveAddon->getData('addon_additional_value')
            );
        }
    }

    /**
     * @param Dyna_MultiMapper_Model_Addon $addon
     * @param $customerNumber
     * @param $serviceLineId
     * @return array|null
     */
    protected function getFnComponentAdditionalData($item, $mapper, $addon, $customerNumber, $serviceLineId)
    {
        if (empty($addon->getData('addon_name')) || empty($addon->getData('addon_additional_value'))) {
            // multimapper data not complete, returning null so the component will not be sent in SubmitOrder
            return null;
        }

        if ($addon->getData('attribute_action') == Dyna_MultiMapper_Model_Addon::ATTRIBUTE_ACTION_TAKEOVER
            && empty($addon->getData('addon_value'))) {
            // for 'takeover' attribute action, take the value from install base instead of using the multimapper data
            return $this->getInstallBaseAdditionalData($addon, $customerNumber, $serviceLineId);
        } elseif ($addon->getData('attribute_action') == Dyna_MultiMapper_Model_Addon::ATTRIBUTE_ACTION_OVERWRITE) {
            // for 'overwrite' attribute action, take the current value of the addon and set it to the master
            $this->overwriteMaster($item, $addon);
            return null;
        } elseif (!empty($addon->getData('addon_value'))) {
            // for other actions just return the data from the multimapper
            return array(
                'Code' => $addon->getData('addon_name'),
                'Value' => $addon->getData('addon_value'),
                'AdditionalValue' => $addon->getData('addon_additional_value')
            );
        } else {
            // multimapper data not complete, returning null so the component will not be sent in SubmitOrder
            return null;
        }
    }

    /**
     * @param $item
     * @return string
     */
    protected function getComponentActivityTypeIndication($item)
    {
        return ($item->getSystemAction() == null)
        || ($item->getSystemAction() == Dyna_Checkout_Model_Sales_Quote_Item::SYSTEM_ACTION_NOT_APPLICABLE)
            ? self::COMPONENT_ACTIVITY_TYPE_INDICATION_MANUAL
            : self::COMPONENT_ACTIVITY_TYPE_INDICATION_AUTOMATIC;
    }
    /**
     * @param $item
     * @return string
     */
    protected function getComponentAction($item)
    {
        if ($this->processContext == Dyna_Catalog_Model_ProcessContext::ACQ) {
            return self::COMPONENT_ACTION_ADD;
        }

        $componentAction = null;

        switch ($item->getSystemAction()) {
            case Dyna_Checkout_Model_Sales_Quote_Item::SYSTEM_ACTION_EXISTING:
                if ($item->getIsContractDrop()) {
                    $componentAction = self::COMPONENT_ACTION_REMOVE;
                } else {
                    $componentAction = self::COMPONENT_ACTION_EXISTING;
                }
                break;
            case Dyna_Checkout_Model_Sales_Quote_Item::SYSTEM_ACTION_NOT_APPLICABLE:
                $componentAction = self::COMPONENT_ACTION_REMOVE;
                break;
            default:
                $componentAction = self::COMPONENT_ACTION_ADD;
                break;
        }

        return $componentAction;
    }

    /**
     * @param Dyna_Checkout_Model_Sales_Order_Item $item
     * @return string
     */
    protected function getFnComponentAction($item)
    {
        if ($this->processContext == Dyna_Catalog_Model_ProcessContext::ACQ) {
            return self::COMPONENT_ACTION_ADD;
        }

        if ($item->isContract()) {
            if ($item->getIsContractDrop()) {
                return self::COMPONENT_ACTION_REMOVE;
            } else {
                return self::COMPONENT_ACTION_EXISTING;
            }
        } else {
            if ($this->isProductChanged($item)) {
                return self::COMPONENT_ACTION_CHANGE;
            } else {
                return self::COMPONENT_ACTION_ADD;
            }
        }
    }

    /**
     * Checks whether the current order item is considered changed
     *
     * @param Dyna_Checkout_Model_Sales_Order_Item $item
     * @return bool
     */
    protected function isProductChanged($item)
    {
        if ($this->processContext == Dyna_Catalog_Model_ProcessContext::ACQ) {
            return false;
        }

        $product = $item->getProduct();
        $isMutualExclusive = $hasCardinalityOne = false;

        $packageSubtypeCardinality = $product
            ->getPackageSubtypeModel()
            ->getCardinalityForProcessContext($this->processContext)
            ->getCardinality();
        $hasCardinalityOne = $packageSubtypeCardinality == Dyna_Package_Model_PackageSubtypeCardinality::PACKAGE_SUBTYPE_CARDINALITY_ONE;

        /** @var Dyna_Catalog_Model_Category $categoryModel */
        $categoryModel = Mage::getModel('dyna_catalog/category');
        $mutualExclusiveCategories = $categoryModel->getAllExclusiveFamilyCategoryIds();
        $productCategoriesIds = [];

        foreach ($product->getCategoryCollection() as $category) {
            if (in_array($category->getId(), $mutualExclusiveCategories)) {
                $isMutualExclusive = true;
                $productCategoriesIds[] = $category->getId();
            }
        }

        if (!($hasCardinalityOne || $isMutualExclusive)) {
            return false;
        }

        if ($hasCardinalityOne) {
            $subtypeItems = $this->getItemsInSubtype($item->getProduct()->getPackageSubtypeModel());

            foreach ($subtypeItems as $subtypeItem) {
                if ($subtypeItem->getId() !== $item->getId() && $subtypeItem->getIsContractDrop()) {
                    // found one contract product that belongs to the same subtype
                    return true;
                }
            }
        }

        if ($isMutualExclusive) {
            $categoryItems = $this->getItemsInCategories($productCategoriesIds);

            foreach ($categoryItems as $categoryItem) {
                if ($categoryItem->getId() !== $item->getId() && $categoryItem->getIsContractDrop()) {
                    // found one contract product that belongs to the same mutual exclusive category
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param Dyna_Checkout_Model_Sales_Order_Item $packageSubtype
     * @return array|Dyna_Checkout_Model_Sales_Order_Item
     */
    protected function getItemsInSubtype($packageSubtype)
    {
        $items = [];
        foreach ($this->packageItems as $item) {
            if ($item->getProduct()->getPackageSubtypeModel()->getId() == $packageSubtype->getId()) {
                $items[] = $item;
            }
        }

        return $items;
    }

    /**
     * @param $categoryIds
     * @return array
     */
    protected function getItemsInCategories($categoryIds)
    {
        $items = [];

        foreach ($this->packageItems as $item) {
            foreach ($item->getProduct()->getCategoryCollection() as $category) {
                if (in_array($category->getId(), $categoryIds)) {
                    $items[] = $item;
                }
            }
        }

        return $items;
    }

    /**
     * Generate component node for a mobile or cable package
     * @param $package Dyna_Package_Model_Package
     * @return mixed
     */
    public function getComponentsForMobilePackage(Dyna_Package_Model_Package $package)
    {
        $skuAction = array();
        // Very important to set the package Process Context
        $this->processContext = $package->getSaleType();

        $result = array();
        $handsetSoc = null;

        $order = $package->getDeliveryOrder();
        $tariffs = $package->getItems(Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_SUBSCRIPTION);

        if (isset($tariffs[0]) && $tariffs[0]) {
            $allItems = $package->getItems(null, array(Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_SUBSCRIPTION, Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_DEVICE));
            // merge tariffs in all items
            $allItems = array_merge($tariffs, $allItems);

            $socs = array();
            $attributeSets = array();

            foreach ($allItems as $item) {
                $this->log('');
                $this->log('Evaluating SKU: ' . $item->getSku());

                $skuAction[$item->getSku()] =  $this->getComponentAction($item);
                /** @var Dyna_Catalog_Model_Product $itemProduct */
                $itemProduct = $item->getProduct();
                $mappers = Mage::getModel('dyna_multimapper/mapper')->getMappersBySku($item->getSku());
                $extraIndicator = $itemProduct->getExtraIndicator('HIS');

                $entriedHeaderPrinted = false;
                /** @var Dyna_MultiMapper_Model_Mapper $mapper */
                foreach ($mappers as $mapper) {
                    if (!$entriedHeaderPrinted) {
                        $this->log("Multimapper entries fetched:");
                        $this->log("----------------------------");
                        $entriedHeaderPrinted = true;
                    } else {
                        $this->log('...');
                    }
                    foreach ($mapper->getData() as $key => $value) {
                        if (!empty($value)) {
                            $this->log("... $key: $value");
                        }
                    }
                    $itemSocs = Mage::getModel('dyna_multimapper/addon')->getAddonByMapperId($mapper->getId());
                    $isFalseExpression = $this->serviceExpressionIsFalse($mapper);
                    if ($isFalseExpression === false) {
                        $this->log('... Service expression was evaluated to: true');
                    }

                    $detailsHeaderPrinted = false;
                    foreach ($itemSocs as $itemSoc) {
                        $technicalId = $itemSoc->getData('technical_id');
                        if ($itemSoc && in_array($itemSoc->getData('backend'), array(self::BACKEND_KIAS, self::BACKEND_SAP))) {
                            if ($isFalseExpression) {
                                if ($item->isContract()) {
                                    $this->initialILSSocs[$technicalId] = $mapper;
                                }
                                continue;
                            }
                            $isFalseAddonExpression = $this->serviceExpressionIsFalse($itemSoc);
                            if ($isFalseAddonExpression) {
                                continue;
                            }

                            $componentType = ($extraIndicator == null ? $mapper->getComponentType() : $extraIndicator);

                            $itemSoc->setData('sku', $mapper->getSku());
                            $itemSoc->setData('name', $item->getName());
                            $itemSoc->setData('activityTypeIndication', $this->getComponentActivityTypeIndication($item));
                            $itemSoc->setData('socAction', $this->getComponentAction($item));
                            $itemSoc->setData('isTariffSoc', $itemProduct->isSubscription());
                            $itemSoc->setData('isSimOnly', $itemProduct->getSimOnly());
                            $itemSoc->setData('product_name', $itemProduct->getName());
                            $itemSoc->setData('additional_text', strip_tags($itemProduct->getServiceDescription()));
                            $itemSoc->setData('mapperComponentType', $componentType);
                            // Found soc
                            $socs[] = $itemSoc;

                            if (!$detailsHeaderPrinted) {
                                $this->log("... ... Multimapper details entries:");
                                $this->log("... ... ----------------------------");
                                $detailsHeaderPrinted = true;
                            }
                            foreach ($itemSoc->getData() as $key => $value) {
                                if (!empty($value)) {
                                    $this->log("... ... $key: $value");
                                }
                            }

                            $attributeSets[$mapper->getSku()] = $itemProduct->getAttributeSetId();
                        }
                    }
                }
            }

            $handset = $package->getItems(Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_DEVICE);
            if (count($handset) != 0) {
                // We assume that only one device is allowed per package for Mobile
                $handsetSocMapper = Mage::getModel('dyna_multimapper/mapper')
                    ->getMappersBySku($handset[0]->getSku())->getFirstItem();
                if ($handsetSocMapper->getId()) {
                    $handsetSoc = Mage::getModel('dyna_multimapper/addon')->getSingleAddonByMapperId($handsetSocMapper->getId());
                    $this->log("Multimapper entries fetched:");
                    $this->log("----------------------------");
                    foreach ($handsetSocMapper->getData() as $key => $value) {
                        if (!empty($value)) {
                            $this->log("... $key: $value");
                        }
                    }

                    $this->log("... ... Multimapper details entries:");
                    $this->log("... ... ----------------------------");
                    foreach ($handsetSoc->getData() as $key => $value) {
                        if (!empty($value)) {
                            $this->log("... ... $key: $value");
                        }
                    }
                }

                $handsetProduct = $handset[0]->getProduct();

                $handsetName = $handsetProduct->getName();
                $handsetDescription = strip_tags($handsetProduct->getServiceDescription());
                $handsetSoc = (isset($handsetSoc) && $handsetSoc) ? $handsetSoc->getData('technical_id') : null;
            }
            //if no socs found, don't generate components
            if (empty($socs)) {
                return null;
            }

            $shippingFee = $this->getShippingFeeNetPrice();

            // loop through found soc and remove them from the list of socs that do not validate service expressions
            foreach ($socs as $soc) {
                unset($this->initialILSSocs[$soc->getData('technical_id')]);
            }

            // if remain initial socs -> these should be sent with REMOVE
            if ($this->initialILSSocs) {
                foreach ($this->initialILSSocs as $addonMapper) {
                    // set the action as remove
                    $addonMapper->setData('socAction', self::COMPONENT_ACTION_REMOVE);
                    $addonMapper->setData('sku', $addonMapper->getSku());
                    $addonMapper->setData('activityTypeIndication', self::COMPONENT_ACTIVITY_TYPE_INDICATION_AUTOMATIC);
                    $socs[] = $addonMapper;
                    $this->log($addonMapper);
                }
            }

            // Add Component/TN/PhoneNumber node on Debit to Credit flow (VFDED1W3S-1136)
            if ($package->getPackageAction() == Dyna_Package_Model_Package::PACKAGE_ACTION_DEBIT_TO_CREDIT) {
                $componentTn = array(
                    'PhoneNumber' => $this->getComponentMsisdn($package) ?: null
                );
                $package->setComponentTn($componentTn);
            }

            $this->getMobileComponentData($socs, $attributeSets, $shippingFee, $package, $order, $result);

            if ($handsetSoc) {
                $result[] = array(
                    'Action' => $this->getComponentAction($handset[0]),
                    'ComponentType' => isset($handsetSocMapper) ? $handsetSocMapper->getComponentType() : null,
                    'ComponentActivityTypeIndication' => $this->getComponentActivityTypeIndication($handset[0]),
                    'SkuID' => isset($handset[0]) ? $handset[0]->getSku() : null,
                    'ComponentAdditionalData' => null,
                    'Agreement' => null,
                    'TariffOption' => null,
                    'TariffOptionDescription' => $handsetDescription ?? null,
                    'TariffOptionName' => $handsetName ?? null,
                    'TN' => $package->getComponentTn() ?: null,
                    'SAPArticleID' => trim($handsetSoc),
                    'Country' => null,
                    'DeviceID' => null,
                    'ProductID' => null,
                    'SAPArticleNetPrice' => null,
                    'SAPArticleGrossPrice' => null,
                    'SAPDiscount1' => null,
                    'SAPDiscount2' => null,
                    'ShippingFeeNetPrice' => $shippingFee['ShippingFeeNetPrice'],
                    'ShippingFeeText' => $shippingFee['ShippingFeeText']
                );
                $skuAction[$handset[0]->getSku()] = $this->getComponentAction($handset[0]);

                // Calculate SAP Devices prices per package
                $devicesNetPrice = 0;
                $devicesGrossPrice = 0;
                $devicesDiscount1 = 0;
                foreach ($handset as $device) {
                    $devicesNetPrice += $device->getOriginalPrice() * 100 / ($device->getTaxPercent() + 100);
                    $devicesGrossPrice += $device->getOriginalPrice();
                    $devicesDiscount1 += (float)$device->getAppliedPromoAmount()
                        + ((float)$device->getMixmatchSubtotal() > 0 ? (float)$device->getOriginalPrice() - ((float)$device->getMixmatchSubtotal() + (float)$device->getMixmatchTax()) : 0) ;
                }

                foreach ($result as $keyRes => $res) {
                    if ($result[$keyRes]['SAPArticleID']) {
                        $result[$keyRes]['SAPArticleNetPrice'] = number_format($devicesNetPrice, 2, '.', '');
                        $result[$keyRes]['SAPArticleGrossPrice'] = number_format($devicesGrossPrice, 2, '.', '');
                        $result[$keyRes]['SAPDiscount1'] = number_format($devicesDiscount1, 2, '.', '');
                        $result[$keyRes]['SAPDiscount2'] = number_format('0.0000', 2, '.', '');;
                    }
                }
            }
        }
        foreach ($result as $res){
            if (isset($skuAction[$res['SkuID']])){
                $skuAction[$res['SkuID']]=$res['Action'];
            }
        }

        foreach ($package->getItems() as $item) {
            if (isset($skuAction[$item->getSku()])) {
                $item->setProductActivityType($skuAction[$item->getSku()])->save();
            }
        }
        return empty($result) ? null : $result;
    }

    /**
     * @param Dyna_Package_Model_Package $package
     * @return array|null
     */
    public function getComponentsForCablePackage(Dyna_Package_Model_Package $package)
    {
        // Very important to set the package Process Context
        $this->processContext = $package->getSaleType();
        $skuAction = array();
        $skus = array();
        $result = array();
        $componentTypes = array('items' => array(), 'dropped' => array());
        $refusedDefaultedItemSkus = $package->getRefusedDefaultedItems() ? explode(',', $package->getRefusedDefaultedItems()) : [];

        /** @var Dyna_Checkout_Model_Sales_Quote_Item $item */
        foreach ($package->getItems() as $item) {
            $skuAction[$item->getSku()] = $this->getComponentAction($item);
            $sku = $item->getSku();
            if ($item->getIsContractDrop()) {
                $componentTypes['dropped'][$sku] = true;
            }
            $componentTypes['items'][$sku] = $item;
            $skus[] = $sku;
        }

        if (!empty($refusedDefaultedItemSkus)) {
            $skus = array_merge($skus, $refusedDefaultedItemSkus);
        }

        $mapperRecords = Mage::getModel('dyna_multimapper/mapper')->getMappersBySkus($skus);
        $shippingFee = $this->getShippingFeeNetPrice();
        // all valid mappers
        $addonRecords = [];
        if ($mapperRecords->count()) {
            /** @var Dyna_MultiMapper_Model_Mapper $mapperRecord */
            foreach ($mapperRecords as $mapperRecord) {
                $entriedHeaderPrinted = false;
                $this->log('');
                $this->log('Evaluating SKU: ' . $mapperRecord->getData('sku'));
                // fallback for mapped contract socs that cannot be sent because of service expression
                $isFalseExpression = $this->serviceExpressionIsFalse($mapperRecord);

                if (!$entriedHeaderPrinted) {
                    $this->log("Multimapper entries fetched:");
                    $this->log("----------------------------");
                    $entriedHeaderPrinted = true;
                } else {
                    $this->log('...');
                }
                foreach ($mapperRecord->getData() as $key => $value) {
                    if (!empty($value)) {
                        $this->log("... $key: $value");
                    }
                }
                if ($isFalseExpression === false) {
                    $this->log('... Service expression was evaluated to: true');
                }

                $addons = Mage::getModel('dyna_multimapper/addon')->getAddonByMapperId($mapperRecord->getId());

                $detailsHeaderPrinted = false;
                foreach ($addons as $addon) {
                    $technicalId = $addon->getTechnicalId();
                    $addon->setData('sku', $mapperRecord->getSku());
                    $addon->setData('componentType', $mapperRecord->getComponentType());
                    $isFalseAddonExpression = $this->serviceExpressionIsFalse($addon);
                    if ($isFalseAddonExpression) {
                        continue;
                    }
                    if ($isFalseExpression) {
                        if ($item->isContract()) {
                            $this->initialILSSocs[$technicalId] = $addon;
                        }
                        continue;
                    }
                    $addonRecords[] = $addon;

                    if (!$detailsHeaderPrinted) {
                        $this->log("... ... Multimapper details entries:");
                        $this->log("... ... ----------------------------");
                        $detailsHeaderPrinted = true;
                    }
                    foreach ($addon->getData() as $key => $value) {
                        if (!empty($value)) {
                            $this->log("... ... $key: $value");
                        }
                    }
                }
            }
        }

        // loop through found soc and remove them from the list of socs that do not validate service expressions
        foreach ($addonRecords as $addonRecord) {
            unset($this->initialILSSocs[$addonRecord->getData('technical_id')]);
        }

        // if remain initial skus -> these should be sent with REMOVE
        if ($this->initialILSSocs) {
            foreach ($this->initialILSSocs as $remainingAddon) {
                $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $remainingAddon->getSku());
                if ($product->getPreselect() === false) {
                    $remainingAddon->setComponentActivityTypeIndication(self::COMPONENT_ACTIVITY_TYPE_INDICATION_AUTOMATIC);
                }
                $componentTypes['dropped'][$remainingAddon->getSku()] = true;
            }
        }

        foreach ($addonRecords as $addonRecord) {
            if (!isset($componentTypes['items'][$addonRecord->getSku()]) && !in_array($addonRecord->getSku(), $refusedDefaultedItemSkus)) {
                continue;
            }
            $result[] = $this->getCableComponentsDetails($addonRecord, $package, $componentTypes, $refusedDefaultedItemSkus, $shippingFee);
        }

        foreach ($result as $res){
            if (isset($skuAction[$res['SkuID']])){
                $skuAction[$res['SkuID']]=$res['Action'];
            }
        }

        foreach ($package->getItems() as $item) {
            if (isset($skuAction[$item->getSku()])) {
                $item->setProductActivityType($skuAction[$item->getSku()])->save();
            }
        }
        return !empty($result) ? $result : null;
    }

    /**
     * Get shipping fee net price and text
     * @return array
     */
    public function getShippingFeeNetPrice()
    {
        if (!$this->shippingFee) {
            $this->shippingFee = array(
                'ShippingFeeNetPrice' => null,
                'ShippingFeeText' => null
            );
        }

        return $this->shippingFee;
    }

    /**
     * Get IBAN value for package / chage type
     * @return string
     */
    public function getIBANFieldValue( $package, $chargeType ) {
        $iban = null;
        switch( $chargeType ) {
            case "RC":
                // Recurring payment (monthly)
                $paymentMethod = $this->getCheckout( 'payment[monthly][type]' );
                if( strpos( $paymentMethod, '|' ) !== FALSE ) {
                    list( $paymentMethod , $iban ) = explode( '|', $paymentMethod );
                }
                switch( $paymentMethod ) {
                    case "direct_debit":
                        $iban = $this->getCheckout('payment[monthly][direct_debit][iban][new]');
                        break;
                    case "direct_debit_diff":
                        $iban = $this->getCheckout('payment[monthly][direct_debit][iban][diff]');
                        break;
                    case "direct_debit_reuse":
                        // IBAN is already known
                        break;
                    case "split":
                        $packageId = $package->getPackageId();
                        $splitPaymentMethod = $this->getCheckout( 'payment[pakket][' . $packageId . '][type]' );
                        if( strpos( $splitPaymentMethod, '|' ) !== FALSE ) {
                            list( $splitPaymentMethod , $iban ) = explode( '|', $splitPaymentMethod );
                        }
                        switch( $splitPaymentMethod ) {
                            case "direct_debit":
                                $iban = $this->getCheckout('payment[pakket][' . $packageId . '][direct_debit][iban][new]');
                                break;
                            case "direct_debit_diff":
                                $iban = $this->getCheckout('payment[pakket][' . $packageId . '][direct_debit][iban][diff]');
                                break;
                            case "direct_debit_reuse":
                                // IBAN is already known
                                break;
                        }
                        break;
                }
                break;
            case "OTC":
                // One-time payment option
                $paymentMethod = $this->getCheckout( 'payment[one_time][type]' );
                switch( $paymentMethod ) {
                    case "direct_debit":
                        $iban = $this->getCheckout('payment[one_time][direct_debit][iban]');
                        if( empty( $iban ) ) {
                            // If IBAN is empty for onetime payments, it means we are reusing monthly one
                            $iban = $this->getIBANFieldValue( $package, 'RC' );
                        }
                        break;
                }
                break;
        }
        return $iban;
    }

    /**
     * Get account holder for package / charge type
     * @return string
     */
    public function getAccountHolderieldValue() {
        return $this->getCheckout('customer[firstname]') . ' ' . $this->getCheckout('customer[lastname]');
    }

    /**
     * Get the type of SEPA mandate
     * @return string
     */
    public function getSepaMandate( $package )
    {
        $type = null;

        $paymentType = $this->getCheckout('payment[monthly][type]');
        switch( $paymentType ) {
            case Dyna_Checkout_Helper_Fields::PAYMENT_SPLIT_PAYMENT:
                $splitPaymentType = $this->getCheckout('payment[pakket][' . $package->getPackageId() . '][type]');
                switch( $splitPaymentType ) {
                    case Dyna_Checkout_Helper_Fields::PAYMENT_DIRECT_DEBIT:
                        $type = $this->getCheckout('payment[pakket][' . $package->getPackageId() . '][sepa_mandate][type][new]');
                        break;
                    case Dyna_Checkout_Helper_Fields::PAYMENT_DIRECT_DEBIT_DIFF:
                        $type = $this->getCheckout('payment[pakket][' . $package->getPackageId() . '][sepa_mandate][type][diff]');
                        break;
                }
                break;
            case Dyna_Checkout_Helper_Fields::PAYMENT_DIRECT_DEBIT:
                $type = $this->getCheckout('payment[monthly][sepa_mandate][type][new]');
                break;
            case Dyna_Checkout_Helper_Fields::PAYMENT_DIRECT_DEBIT_DIFF:
                $type = $this->getCheckout('payment[monthly][sepa_mandate][type][diff]');
                break;
        }

        $sepaMandate = null;

        switch ($type) {
            case '0':
                $sepaMandate = 'ONLINE';
                break;
            case '1':
                $sepaMandate = 'PAPER';
                break;
            default :
                $sepaMandate = 'EXISTING';
                break;
        }

        return $sepaMandate;
    }

    /**
     * Check if a value exists in the chceckout data and returns it. Otherwise returns null or empty string
     * @param $data
     * @param $key
     * @param $returnEmptyString
     * @return null|string
     */
    public function getValue($data, $key, $returnEmptyString = false)
    {
        if (isset($data[$key]) && !empty($this->getCheckout($data[$key]))) {
            return $this->getCheckout($data[$key]);
        }

        return $returnEmptyString ? '' : null;
    }

    /**
     * Add LOC or PD/PL contacts
     * @param $contacts
     * @param $package
     */
    public function addConnectionOwnerContacts(&$contacts, $package)
    {
        $value = $this->getCheckout('provider['.$package->getType().'][owner_of_terminal]');
        $contactData = array(
            'Salutation' => 'provider['.$package->getType().'][other_owner][gender]',
            'Title' => 'provider['.$package->getType().'][other_owner][title]',
            'FirstName' => 'provider['.$package->getType().'][other_owner][firstname]',
            'LastName' => 'provider['.$package->getType().'][other_owner][lastname]',
        );
        switch ($value) {
            case Dyna_Checkout_Block_Cart_Steps_SaveChangeProvider::YES_ONE_OF_THE_TERMINAL_OWNERS:
                $contacts[$package->isFixed() ? 'LOC' : 'PD'] = $contactData;
                break;
            case Dyna_Checkout_Block_Cart_Steps_SaveChangeProvider::NO_OWNER_OF_TERMINAL:
                $contacts[$package->isFixed() ? 'LOC' : 'PL'] = $contactData;
                break;
        }

    }

    /**
     * Gets billing type based on checkout options
     *
     * @param $package Dyna_Package_Model_Package
     * @return string
     */
    public function getBillingType($package = false)
    {
        if ($package !== false) {
            foreach ($package->getItems() as $item) {
                if (Dyna_Catalog_Model_Type::getSendInvoiceByPostSkus() == $item->getData('sku')) {
                    return self::BILLING_LETTER;
                }
            }
        }

        // see latest update on OMNVFDE-2896
        if ($this->getCheckout('billing[billing_type]') == 'mail' ) {
            if ($package->isCable()) {
                return self::BILLING_ONLINE;
            } else {
                return self::BILLING_WEBBILL;
            }
        } else {
            $sameAsCustomerOption = Dyna_Checkout_Block_Cart_Steps_SaveCustomer::SAME_AS_CUSTOMER_ADDRESS;
            $otherBillingAddressOption = Dyna_Checkout_Block_Cart_Steps_SaveCustomer::BILLING_ANOTHER_ADDRESS;
            if (($this->getCheckout('billing[other_address][selected]') &&
                    in_array($this->getCheckout('billing[other_address][selected]'), [$sameAsCustomerOption, $otherBillingAddressOption])) ||
                $this->getCheckout('billing[billing_type]') == 'invoice'
            ) {
                if ($package->isMobile() || $package->isCable()) {
                    return self::BILLING_PAPER;
                } else if ($package->isFixed()) {
                    return self::BILLING_LETTER;
                }
            }
        }

        return self::BILLING_PAPER; // We never return this empty?
    }

    /**
     * Get the details of the residentail building
     * @param $includeComments
     */
    public function getResidentialBuildingInfo($includeComments = true)
    {
        $buildingInfo = array();
        $buildingInfo[] = $this->getProviderHomeType(true);

        $eingangKey = $this->getCheckout('provider[home_entrance]');
        $eingangValues = Dyna_Checkout_Block_Cart_Steps_SaveChangeProvider::getDslHomeEntranceOptions();
        $buildingInfo[] = isset($eingangValues[$eingangKey]) ? $eingangValues[$eingangKey] : null;

        $stockwerkKey = $this->getCheckout('provider[home_floor]');
        $stockwerkValues = Dyna_Checkout_Block_Cart_Steps_SaveChangeProvider::getDslHomeFloorsOptions();
        $buildingInfo[] = isset($stockwerkValues[$stockwerkKey]) ? $stockwerkValues[$stockwerkKey] : null;

        $lageDerWohnungKey = $this->getCheckout('provider[home_location]');
        $lageDerWohnungValues = Dyna_Checkout_Block_Cart_Steps_SaveChangeProvider::getDslApartmentLocationOptions();
        $buildingInfo[] = isset($lageDerWohnungValues[$lageDerWohnungKey]) ? $lageDerWohnungValues[$lageDerWohnungKey] : null;

        $buildingInfo[] = $this->getCheckout('provider[phone_socket_no]');
        if ($includeComments) {
            $buildingInfo[] = $this->getCheckout('provider[comment_installer]');
        }
        $buildingInfo = array_filter($buildingInfo);
        return $buildingInfo ? implode(', ', $buildingInfo) : null;
    }

    /**
     * Get the provided home type
     * @param $shortVersion bool specify if long value or short value should be returned (Mehrfamilienhaus vs MFH)
     * @return null|string
     */
    public function getProviderHomeType($shortVersion = false)
    {
        $keyValue = $this->getCheckout('provider[home_type]');

        $providerHomeType = null;

        if ($shortVersion) {
            switch ($keyValue) {
                case Dyna_Checkout_Block_Cart_Steps_SaveChangeProvider::HOME_WITH_1_FAMILY:
                    $providerHomeType = 'EFH';
                    break;
                case Dyna_Checkout_Block_Cart_Steps_SaveChangeProvider::HOME_WITH_MULTIPLE_FAMILIES:
                    $providerHomeType = 'MFH';
                    break;
                default:
                    $possibleValues = Dyna_Checkout_Block_Cart_Steps_SaveChangeProvider::getDslResidenceTypesOptions();
                    $providerHomeType = isset($possibleValues[$keyValue]) ? $possibleValues[$keyValue] : null;
            }
        }

        return $providerHomeType;
    }

    /**
     * Get a list of porting phone numbers
     * @param $type - type of phone number
     * @param $onlyFirst - if true, value is needed even if no number is selected
     * @return mixed
     */
    public function getPortingPhoneNumbers(Dyna_Package_Model_Package $package, $type = null, $onlyFirst = false)
    {
        /** @var Omnius_Service_Helper_Data $serviceHelper */
        $serviceHelper = Mage::helper('omnius_service');

        $result = array();
        $i = 0;
        $packageType = strtolower($package->getType());
        if ($onlyFirst || $this->getCheckout('provider[' . $packageType . '][no_phones_transfer]') != Dyna_Superorder_Helper_Client::PORTING_NO_NUMBER) {
            while ($this->getCheckout("provider[" . $packageType . "][phone_transfer_list][telephone][$i]") &&
                $this->getCheckout("provider[" . $packageType . "][phone_transfer_list][telephone_prefix][$i]")) {
                $result[] = array(
                    'CountryCode' => ($serviceHelper->getGeneralConfig('country_code') ? $serviceHelper->getGeneralConfig('country_code') : Dyna_Customer_Model_Customer::COUNTRY_CODE),
                    'PhoneNumber' => $this->getCheckout("provider[" . $packageType . "][phone_transfer_list][telephone][$i]"),
                    'LocalAreaCode' => $this->getCheckout("provider[" . $packageType . "][phone_transfer_list][telephone_prefix][$i]"),
                    'Type' => $onlyFirst ? null : $type
                );
                if ($onlyFirst) {
                    break;
                }
                $i++;
            }
        }

        return $result ?: null;
    }

    /**
     * Return the package type
     * @param $package
     * @return null|string
     */
    public function getPackageCategoryType($package)
    {
        $packageCategory = null;

        switch (strtolower($package->getType())) {
            case strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_INTERNET_PHONE):
                $packageCategory = Dyna_Customer_Model_Client_RetrieveCustomerInfo::CABLE_PRODUCT_CATEGORY_KIP;
                break;
            case strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_TV):
                $packageCategory = Dyna_Customer_Model_Client_RetrieveCustomerInfo::CABLE_PRODUCT_CATEGORY_TV;
                break;
            case strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_INDEPENDENT):
                $packageCategory = Dyna_Customer_Model_Client_RetrieveCustomerInfo::CABLE_PRODUCT_CATEGORY_CLS;
                break;
            default :
                $packageCategory = null;
        }

        return $packageCategory;
    }

    /**
     * Return the value for the same day delivery
     * @return null|string
     */
    public function getSapDeliveryType()
    {
        $deliveryType = $this->getCheckout('delivery[same_day_schedule]');

        $sapDeliveryType = null;

        switch ($deliveryType) {
            case Dyna_Checkout_Block_Cart_Steps_SaveDeliveryAddress::DELIVERY_SAME_DAY_18_20:
                $sapDeliveryType = '12';
                break;
            case Dyna_Checkout_Block_Cart_Steps_SaveDeliveryAddress::DELIVERY_SAME_DAY_19_21:
                $sapDeliveryType = '13';
                break;
            default :
                $sapDeliveryType = null;
        }

        return $sapDeliveryType;
    }

    /**
     * Identify the value for carrier contract cancel
     * @param $packageType Type of package
     * @return null|string
     */
    public function getCarrierContractCancel($packageType)
    {
        if ($this->getCheckout("provider_notice_date_radio[$packageType]") == Dyna_Checkout_Block_Cart_Steps_SaveChangeProvider::TERMINATION_DATE_NOT_RELEVANT) {
            return $this->getCheckout("provider[$packageType][termination_date_reason]") ==
            Dyna_Checkout_Block_Cart_Steps_SaveChangeProvider::TERMINATION_DATE_NOT_FILLED_CONTRACT_END ? 'false' : 'true';
        }

        return null;
    }

    /**
     * Identify value for end of term mode
     * @return null|string
     */
    public function getEndOfTermMode()
    {
        if ($this->getCheckout('provider_notice_date_radio[cable_internet_phone]') == Dyna_Checkout_Block_Cart_Steps_SaveChangeProvider::TERMINATION_DATE_NOT_RELEVANT) {
            return $this->getCheckout('provider[cable_internet_phone][termination_date_reason]')
                ?: Dyna_Checkout_Block_Cart_Steps_SaveChangeProvider::TERMINATION_MODE_END_DATE;
        } elseif ($this->getCheckout('provider_notice_date_radio[cable_internet_phone]') == Dyna_Checkout_Block_Cart_Steps_SaveChangeProvider::LAST_POSSIBLE_DATE_NOTICE) {
            return Dyna_Checkout_Block_Cart_Steps_SaveChangeProvider::TERMINATION_MODE_END_DATE;
        }

        return null;
    }

    /**
     * Identify the correct value for the customer contact salutation
     * @return null|string
     */
    public function getCustomerContactSalutation()
    {
        return $this->getSalutation($this->getCheckout('customer[gender]') ?: null);
    }

    /**
     * Identify the correct value for the customer contact first name
     * @return null|string
     */
    public function getCustomerContactFirstName()
    {
        return $this->getCheckout('customer[firstname]') ?: null;
    }

    /**
     * Identify the correct value for the customer contact last name
     * @return null|string
     */
    public function getCustomerContactLastName()
    {
        return $this->getCheckout('customer[lastname]') ?: null;
    }

    /**
     * Getting Additional Customer Name
     * @return null|string
     */
    public function getCustomerAdditionalName() {

        $contactName = $this->getCheckout('customer[contact_person_details][0][firstname]');

        $lastName = $this->getCheckout('customer[contact_person_details][0][lastname]');

        if($lastName) {
            $contactName .= " ".$lastName;
        }

        return $contactName;
    }

    /**
     * Get the package type of the order
     * @param $order_id
     * @return string
     */
    public function getOrderPackageType($order_id)
    {
        /** @var Dyna_Package_Model_Package $package */
        $package = Mage::getModel('package/package')
            ->getCollection()
            ->addFieldToFilter('order_id', $order_id)
            ->getFirstItem();

        return $package->getType();
    }

    /**
     * Calculate the charge type
     * @return null
     */
    public function getChargeType( $package )
    {
        $checkoutLayout = Mage::helper("dyna_checkout/layout");
        $paymentTypes = [];
        if( $checkoutLayout->isAvailableSection( 'payments_onetime', $package ) ) {
            $paymentTypes[] = 'OTC';
        }
        if( $checkoutLayout->isAvailableSection( 'payments_monthly', $package ) ) {
            $paymentTypes[] = 'RC';
        }
        return $paymentTypes;
    }

    /**
     * Fill in paymentMethodList node
     * @param $package
     * @return array
     */
    public function getPaymentMethodList($package)
    {
        $data = array(
            'PaymentChargeType' => array(
                array(
                    'PaymentMethod' => $this->getPaymentMethod($package),
                    'PaymentMethodType' => 'ServiceCharges'
                )
            )
        );

        if (isset($this->isSapSimOnlyPackage[$package->getId()])) {
            $data['PaymentChargeType'][] = array(
                'PaymentMethod' => 'CR',
                'PaymentMethodType' => 'HardwareCharges'
            );
        }

        return $data;
    }

    /**
     * @param $socs
     * @param $attributeSets
     * @param $shippingFee
     * @param $package
     * @param $order
     * @param $result
     */
    protected function getMobileComponentData($socs, $attributeSets, $shippingFee, $package, $order, &$result)
    {
        $attributeSetModel = Mage::getModel('eav/entity_attribute_set');
        foreach ($socs as $soc) {
            $technicalId = $soc->getData('technical_id') ? $soc->getData('technical_id') : null;
            $backend = $soc->getData('backend') ? $soc->getData('backend') : null;
            $componentData = array(
                'Action' => $soc->getData('socAction'),
                'ComponentType' => $soc->getData('mapperComponentType'),
                'ComponentActivityTypeIndication' => $soc->getData('activityTypeIndication'),
                'SkuID' => $soc->getData('sku'),
                'name' => $soc->getData('name'),
                'ComponentAdditionalData' => null,
                'TariffOption' => ($soc->getData('isTariffSoc') && $backend != self::BACKEND_SAP) ? trim($technicalId) : null,
                'TariffOptionCategory' => isset($attributeSets[$soc->getData('sku')]) && $attributeSets[$soc->getData('sku')] ? $attributeSetModel->load($attributeSets[$soc->getData('sku')])->getAttributeSetName() : null,
                'TariffOptionDescription' => $soc->getData('additional_text'),
                'TariffOptionName' => $soc->getData('product_name'),
                'Agreement' => ($soc->getData('isTariffSoc') || $backend != self::BACKEND_KIAS) ? null : array(
                    'AgreementSOC' => $technicalId,
                    'AgreementSOCDescription' => null,
                    'AgreementServiceType' => null,
                    'AgreementEffectiveDate' => null,
                    'AgreementExpirationDate' => null,
                    'AgreementTariffOption' => null
                ),
                'TN' => $package->getComponentTn(),
                'SAPArticleID' => $backend == self::BACKEND_SAP ? $technicalId : null,
                'Country' => null,
                'DeviceID' => null,
                'ProductID' => null,
                'SAPArticleNetPrice' => null,
                'SAPArticleGrossPrice' => null,
                'SAPDiscount1' => null,
                'SAPDiscount2' => null,
                'ShippingFeeNetPrice' => $shippingFee['ShippingFeeNetPrice'],
                'ShippingFeeText' => $shippingFee['ShippingFeeText']
            );

            //add simonly component additional data based on OMNVFDE-2826
            if (strtolower($package->getType()) == strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE) && isset($componentData['SAPArticleID'])) {
                $componentData['ComponentAdditionalData'] = array(
                    'Code' => 'SimOnly',
                    'Value' => $soc->getData('isSimOnly') == 1 ? 'Yes' : 'No',
                );
                if ($soc->getData('isSimOnly')) {
                    $this->isSapSimOnlyPackage[$package->getId()] = true;
                }
            }
            $result[] = $componentData;
            if (!isset($this->isSapSimOnlyPackage[$package->getId()])
                || !$this->isSapSimOnlyPackage[$package->getId()]
            ) {
                $handset = $package->getItems(Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_DEVICE);

                // Calculate SAP Devices prices per package
                $devicesNetPrice = 0;
                $devicesGrossPrice = 0;
                $devicesDiscount1 = 0;

                foreach ($handset as $device) {
                    $devicesNetPrice += $device->getOriginalPrice() * 100 / ($device->getTaxPercent() + 100);
                    $devicesGrossPrice += $device->getOriginalPrice();
                    $devicesDiscount1 += (float)$device->getAppliedPromoAmount()
                        + ((float)$device->getMixmatchSubtotal() > 0 ? (float)$device->getOriginalPrice() - ((float)$device->getMixmatchSubtotal() + (float)$device->getMixmatchTax()) : 0) ;
                }

                foreach ($result as $keyRes => $res) {
                    if ($result[$keyRes]['SAPArticleID']) {
                        $result[$keyRes]['SAPArticleNetPrice'] = number_format($devicesNetPrice, 2, '.', '');
                        $result[$keyRes]['SAPArticleGrossPrice'] = number_format($devicesGrossPrice, 2, '.', '');
                        $result[$keyRes]['SAPDiscount1'] = number_format($devicesDiscount1, 2, '.', '');
                        $result[$keyRes]['SAPDiscount2'] = number_format('0.0000', 2, '.', '');
                    }
                }
            }
        }
    }

    /**
     * Process service expressions if one is found on the mapper or mapper addon
     * @param $mapper
     * @return bool
     */
    protected function serviceExpressionIsFalse($object)
    {
        //skip if service expression did not match
        if ($ssExpression = $object->getServiceExpression()) {
            if (!$this->dynaCoreHelper->evaluateExpressionLanguage($ssExpression, $this->inputParams)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param $addonRecord
     * @param $package
     * @param $componentTypes
     * @param $refusedDefaultedItemSkus
     * @param $shippingFee
     * @return array
     */
    protected function getCableComponentsDetails($addonRecord, $package, $componentTypes, $refusedDefaultedItemSkus, $shippingFee)
    {
        if (!$addonRecord->getTechnicalId()) {
            return null;
        }

        $deviceDetails = null;

        // For Cable, technical id is equal with sku, so we can determine if customer has own hardware based on multimapper
        if (in_array($addonRecord->getTechnicalId(), Dyna_Catalog_Model_Type::getOwnReceiverSkus())) {
            $deviceDetails = $package->getReceiverSerialNumber();
        } elseif ($addonRecord->getTechnicalId() == Dyna_Catalog_Model_Type::OWN_SMARTCARD_SKU) {
            $deviceDetails = $package->getSmartcardSerialNumber();
        }

        //if above check fails (due to improper catalog) but we do have receiver or smartcard send it anyway
        if (!$deviceDetails && ($package->getReceiverSerialNumber() || $package->getSmartcardSerialNumber())) {
            $deviceDetails = $package->getReceiverSerialNumber() ?: $package->getSmartcardSerialNumber();
        }

        if (!is_null($deviceDetails)) {
            $deviceDetails = Mage::helper('core')->jsonDecode($deviceDetails);
        }

        $sku = $addonRecord->getSku();
        $item = $componentTypes['items'][$sku];
        $removeSoc = isset($componentTypes['dropped'][$sku]) && $componentTypes['dropped'][$sku];
        if (in_array($addonRecord->getSku(), $refusedDefaultedItemSkus)) {
            return array(
                'Action' => self::COMPONENT_ACTION_REFUSE,
                'ComponentType' => $addonRecord->getComponentType(),
                'ComponentActivityTypeIndication' => self::COMPONENT_ACTIVITY_TYPE_INDICATION_MANUAL,
                'SkuID' => $sku,
                'ComponentAdditionalData' => null,
                'TariffOption' => null,
                'TariffOptionDescription' => null,
                'TariffOptionName' => null,
                'Agreement' => null,
                'SAPArticleID' => null,
                'Country' => null,
                'DeviceID' => ($addonRecord->getTechnicalId() == $deviceDetails['productSku']) ? $deviceDetails['sn'] : null,
                'ProductID' => $addonRecord->getTechnicalId(),
                'SAPArticleNetPrice' => null,
                'SAPArticleGrossPrice' => null,
                'SAPDiscount1' => null,
                'SAPDiscount2' => null,
                'ShippingFeeNetPrice' => $shippingFee['ShippingFeeNetPrice'],
                'ShippingFeeText' => $shippingFee['ShippingFeeText']
            );
        } else {
            return array(
                'Action' => $this->getCableILSComponentAction($item, $refusedDefaultedItemSkus, $removeSoc),
                'ComponentType' => $addonRecord->getComponentType(),
                'ComponentActivityTypeIndication' => $addonRecord->getComponentActivityTypeIndication() ?: $this->getComponentActivityTypeIndication($componentTypes['items'][$sku]),
                'SkuID' => $sku,
                'ComponentAdditionalData' => null,
                'TariffOption' => null,
                'TariffOptionDescription' => null,
                'TariffOptionName' => null,
                'Agreement' => null,
                'SAPArticleID' => null,
                'Country' => null,
                'DeviceID' => ($addonRecord->getTechnicalId() == $deviceDetails['productSku']) ? $deviceDetails['sn'] : null,
                'ProductID' => $addonRecord->getTechnicalId(),
                'SAPArticleNetPrice' => null,
                'SAPArticleGrossPrice' => null,
                'SAPDiscount1' => null,
                'SAPDiscount2' => null,
                'ShippingFeeNetPrice' => $shippingFee['ShippingFeeNetPrice'],
                'ShippingFeeText' => $shippingFee['ShippingFeeText']
            );
        }
    }

    /**
     * @param $item Dyna_Checkout_Model_Sales_Quote_Item
     * @param $refusedDefaultedItemSkus
     * @param bool $removeSoc
     * @return mixed|string
     */
    protected function getCableILSComponentAction($item, $refusedDefaultedItemSkus, $removeSoc = false)
    {

        if ($this->processContext == Dyna_Catalog_Model_ProcessContext::ACQ) {
            $quoteSkuToCheck = $item->getSku();
            return in_array($quoteSkuToCheck, $refusedDefaultedItemSkus) ? self::COMPONENT_ACTION_REFUSE : self::COMPONENT_ACTION_ADD;
        }

        if ($removeSoc) {
            return self::COMPONENT_ACTION_REMOVE;
        }

        return $this->getComponentAction($item);
    }

    /**
     * @param $package
     * @return mixed
     */
    protected function getComponentMsisdn($package)
    {
        /** @var Dyna_Superorder_Model_Client_SubmitOrderClient_PackageSubscriber $packageSubscriber */
        $packageSubscriber = Mage::getModel('dyna_superorder/client_submitOrderClient_packageSubscriber');
        $ctnData = $packageSubscriber->getCtnData($package);
        return $ctnData['PhoneNumber'];
    }

    /**
     * Check if a KIAS installed base package contains the bundle marker
     * @param $package
     * @return int
     */
    protected function packageContainsBundleMarker($package)
    {
        $newSkus = [];
        $mappedSkus = [];

        if ($package->getInstalledBaseProducts() && $package->getInitialInstalledBaseProducts()) {
            $newSkus = explode(',', $package->getInitialInstalledBaseProducts());
        }

        if (count($newSkus)) {
            $mappedSkus = Mage::helper('dyna_multimapper')->getSkusBySocs(Dyna_Customer_Model_Client_RetrieveCustomerInfo::KIAS_PART_OF_BUNDLE_SOC);
        }

        $found = array_intersect($mappedSkus, $newSkus);

        return count($found);
    }

    /**
     * Log messages to custom file: var/log/SubmitOrder_Multimapping.log
     * @param $message
     */
    public function log($message, $level = null)
    {
        if (!Mage::getStoreConfig('omnius_general/log_settings/multimapper_log')) {
            return;
        }
        Mage::log($message, $level, 'SubmitOrder_Multimapping.log', true);
    }

    /**
     * Returns all installed base components for the given contract.
     * The method is used by the FN mapper to replace 'takeover' components
     * with installed base components
     *
     * @param $contract
     * @return array
     */
    protected function getAllFnInstalledBaseComponents($contract)
    {
        $components = [];
        foreach ($contract['Subscription'] as $subscription) {
            $components[] = $subscription['Product']['Component'];
        }

        return $components;
    }

    /**
     * Returns the component additional data from installed base
     *
     * @param $customerNumber
     * @param $serviceLineId
     * @param Dyna_MultiMapper_Model_Addon $addon
     * @return array|null
     */
    protected function getInstallBaseAdditionalData($addon, $customerNumber, $serviceLineId)
    {
        /** @var Dyna_Customer_Model_Session $customerSession */
        $customerSession = Mage::getSingleton('customer/session');

        foreach ($customerSession->getCustomersForStack(Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN) as $fnCustomer) {
            if ($fnCustomer['ParentAccountNumber']['ID'] == $customerNumber) {
                if (!isset($fnCustomer['Contract'][0])) {
                    $fnCustomer['Contract'] = [$fnCustomer['Contract']];
                }
                $ibContract = null;
                foreach ($fnCustomer['Contract'] as $contract) {
                    if (!isset($contract['Subscription'][0])) {
                        $contract['Subscription'] = [$contract['Subscription']];
                    }

                    foreach ($contract['Subscription'] as $subscription) {
                        if ($subscription['Product']['StandardItemIdentification']['ID'] == $serviceLineId) {
                            $ibContract = $contract;
                            break;
                        }
                    }

                    if ($ibContract) {
                        $installBaseComponents = $this->getAllFnInstalledBaseComponents($ibContract);
                        foreach ($installBaseComponents as $component) {
                            foreach ($component['ComponentAdditionalData'] as $componentAdditionalData) {
                                if ($componentAdditionalData['Code'] == $addon->getData('addon_name')
                                    && $componentAdditionalData['AdditionalValue'] == $addon->getData('addon_additional_value')) {
                                    return array(
                                        'Code' => $addon->getData('addon_name'),
                                        'Value' => $componentAdditionalData['Value'],
                                        'AdditionalValue' => $addon->getData('addon_additional_value')
                                    );
                                }
                            }
                        }
                    }
                }
            }
        }

        return null;
    }

    /**
     * Get customer number for request (@todo - change for wave2 as order can have multiple packages)
     * @param $quoteOrSuperorder
     * @return bool
     */
    public function getCustomerNumber($quoteOrSuperorder)
    {
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        $customerNumber = null;

        if (true === empty($customer->isCustomerLoggedIn())) {
            return self::DUMMY_PARTY_IDENTIFICATION;
        } else {
            $packages = $quoteOrSuperorder->getOrderNumber() ?
                $quoteOrSuperorder->getPackages() :
                $quoteOrSuperorder->getCartPackages();

            foreach ($packages as $package) {
                if ($package->getData('parent_account_number') != '') {
                    $customerNumber = $package->getData('parent_account_number');
                    break;
                }
            }

            if ($customerNumber == null) {
                $customerNumber = self::DUMMY_PARTY_IDENTIFICATION;
            }
        }

        return $customerNumber;
    }
}
