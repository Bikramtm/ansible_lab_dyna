<?php
class Dyna_Superorder_Model_Component
{
    /**
     * @var Dyna_MultiMapper_Model_Resource_Addon_Collection
     */
    protected $addons;

    public function setAddons($addons)
    {
        $this->addons = $addons;
    }

    /**
     * Based on a components addons, we can determine its type
     */
    public function isVoice()
    {
        foreach($this->addons as $addon) {
            if($addon->getAddonName() == 'function@ID' && $addon->getAddonAdditionalValue() == '/functions/voice') {
                return true;
            }
        }

        return false;
    }
}