<?php

class Dyna_Superorder_Model_Superorder extends Omnius_Superorder_Model_Superorder
{
    const SO_VALIDATED = 'Validation approved';
    const SO_VALIDATION_FAILED = 'Validation declined';
    const SO_WAITING_FOR_VALIDATION = 'Waiting for Validation';
    const SO_WAITING_VALIDATION_ACCEPTED_AXI = 'Waiting for validation: accepted by AXI';
    const SO_CANCELLED = 'Cancelled';
    const SO_CLOSED = 'Closed';
    const SO_STATUS_FAILED = 'Validation failed';
    const SO_STATUS_CANCELLED = 'Validation Cancelled';
    const SO_STATUS_REJECTED = 'Validation Rejected';
    const SO_STATUS_REFERRED = 'Validation Referred';
    const SO_DELIVERED = 'Delivered';
    const SO_PRE_INITIAL = 'In initial queue';
    const SO_INITIAL = 'Waiting for validation: processing started';
    const SO_FAILED = 'Waiting for validation: technical failure';
    const SO_ADYEN_PAYMENT = 'Order not placed';
    const SO_FULFILLED = 'Fulfilled';
    const SO_COMPLETED_SUCCESS = 'order-completed-successfuly';
    const SO_COMPLETED_FAILED = 'order-completed-failed';
    const SO_IN_PROGRESS_NO_FAILURE = 'order-in-progress-no-failure';
    const SO_IN_PROGRESS_WITH_FAILURE = 'order-in-progress-with-failure';

    const SO_TYPE_NEW = 'New';
    const SO_TYPE_CHANGE = 'Change';
    const SO_TYPE_CANCEL = 'Cancel';

    protected $packages = false;

    /**
     * Tries to determine if the superorder is a result of a create or change(before/after):
     * If it's updatedAt == createdAt , means is "New"
     * If it has children, it's a "changeAfter"
     * If none of the above, it's a "changeBefore"
     *
     * @return string Type of action
     */
    public function getActionType()
    {
        //if it was never updated, it's new, if it has cancelled status is cancelled and otherwise, changed
        if(!$this->getUpdatedAt() || $this->getUpdatedAt() == $this->getCreatedAt()) {
            return self::SO_TYPE_NEW;
        }
        elseif($this->getOrderStatus == self::SO_CANCELLED){
            return self::SO_TYPE_CANCEL;
        }
        else{
            return self::SO_TYPE_CHANGE;
        }
    }

    protected function _beforeSave()
    {
        //only update agent/dealer/website on frontend
        $website = Mage::app()->getWebsite();
        if ($website->getId() && !$this->getSkipUpdatedAt()) {
            $agent = $this->_getCustomerSession()->getAgent();
            $dealer = $agent ? $agent->getDealer() : null;

            $this->setWebsiteId($website->getId());
            if ($dealer) {
                $this->setDealerId($dealer->getId());
            } else {
                $this->setDealerId(null);
            }
            if ($agent) {
                $this->setAgentId($agent->getId());
            } else {
                $this->setAgentId(null);
            }
        }

        // Save customer number on superorder
        $this->setCustomerNumber(
            Mage::getSingleton("checkout/session")
                ->getQuote()
                ->getCustomer()
                ->getCustomerNumber()
        );

        // For new superorder, if customer does not have customer number, set new_customer to true
        if (!$this->getId() && !$this->getCustomer()->getCustomerBan()) {
            $this->setNewCustomer(1);
        }

        return parent::_beforeSave();
    }

    /**
     * Check if there is at least one package of type DSL in the cart
     *
     * @return bool
     */
    public function hasDsl()
    {
        return Mage::helper('dyna_checkout/cart')->hasPackageOfType(
            $this->getPackages(),
            Dyna_Catalog_Model_Type::TYPE_FIXED_DSL
        );

//        foreach ($this->getPackages() as $package) {
//            if (strtolower($package->getType()) == strtolower(Dyna_Catalog_Model_Type::TYPE_FIXED_DSL)) {
//                return true;
//            }
//        }
//        return false;
    }

    /**
     * Check if the superorder contains packages with installed base
     *
     * @return bool
     */
    public function hasInstalledBasedPackage()
    {
        foreach ($this->getPackages() as $package) {
            if($package->getInstalledBaseProducts() && $package->getInitialInstalledBaseProducts()){
                return true;
            }
        }

        return false;

    }

    /**
     * Returns all packages of the current superorder based on package and bundle rule combination
     * The key is set to be the unique bundle id identifier on package level
     * @return Dyna_Bundles_Model_BundleRule[]
     */
    public function getBundles()
    {
        $chosenBundles = array();
        $orderPackages = $this->getPackages();

        /** @var Dyna_Package_Model_Package $package */
        foreach ($orderPackages as $package) {
            if (count($package->getBundles())) {
                /** @var Dyna_Bundles_Model_BundleRule $bundle */
                foreach ($package->getBundles() as $bundle) {
                    // Currently, LegacyBundleId is used only for mapping Red+ install base group to SubmitOrder, for all others it will be filled with 0
                    $legacyBundleId = 0;
                    // If it belongs to Red+ group and has parent get sharing group id from parent
                    if ($package->isPartOfRedPlus() && $package->getParentId() && ($parentPackage = $this->getParentPackage($package, $orderPackages))) {
                        // If created from install base, set it, otherwise preserve it's original zero value
                        $legacyBundleId = $parentPackage->getSharingGroupId() ?: $legacyBundleId;
                    } else if ($package->isPartOfRedPlus()) {
                        // Else if it belongs to Red+ and doesn't have parent, get sharing group id from it
                        $legacyBundleId = $package->getSharingGroupId() ?: $legacyBundleId;
                    }
                    // If it doesn't belong to a Red+ group, legacy bundle id falls back to zero
                    $bundle->setLegacyBundleId($legacyBundleId);
                    $chosenBundles[$bundle->getId()] = $bundle;
                }
            }
        }

        return $chosenBundles;
    }

    /**
     * Return the parent package of a child package
     * @param $childPackage
     * @param $packagesList
     */
    protected function getParentPackage($childPackage, $packagesList)
    {
        /** @var Dyna_Package_Model_Package $tempPackage */
        foreach ($packagesList as $tempPackage) {
            if ($tempPackage->getId() === $childPackage->getParentId()) {
                return $tempPackage;
            }
        }

        return null;
    }

    /**
     * Returns all packages of the current superorder or only one package if an id is provided
     * @param int $id SuperOrder id
     * @return Dyna_Package_Model_Package[]
     */
    public function getPackages($id = null)
    {
        if ($id) {
            return parent::getPackages($id);
        }

        // caching these packages at super order instance level
        if ($this->packages === false) {
            $this->packages = Mage::getModel('package/package')->getOrderPackages($this->getId())->load();
        }

        return $this->packages;
    }

    /**
     * Checks if superorder contains new hardware
     * @return bool
     */
    public function hasNewHardware() {
        /** @var Dyna_Package_Model_Package $package */
        foreach ($this->getPackages() as $package) {
            /** @var Mage_Sales_Model_Quote_Item $item */
            foreach ($package->getItems(Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_DEVICE) as $item) {
                if (!$item->getIsContract()) {
                    return true;
                }
            }
        }

        return false;
    }

    //extended the function to save transaction id
    public function createNewSuperorder($parentSuperorder = null)
    {
        parent::createNewSuperorder($parentSuperorder);
        $this->setOrderStatusTranslate(Mage::helper('customer')->__($this->getOrderStatus()))->save();
        $session = Mage::getSingleton('customer/session');
        $uctParams = $session->getUctParams();
        if (isset($uctParams["transactionId"])) {
               $this->setTransactionId($uctParams["transactionId"])
                   ->save();
        }
        return $this;
    }
}
