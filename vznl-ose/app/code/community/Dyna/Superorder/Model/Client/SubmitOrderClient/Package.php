<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Dyna_Superorder_Model_Client_SubmitOrderClient_Package extends Mage_Core_Model_Abstract
{
    /** @var Dyna_Customer_Model_Customer $customer */
    protected $customer;
    /** @var  $clientHelper Dyna_Superorder_Helper_Client */
    protected $clientHelper;

    /**
     * @param Dyna_Package_Model_Package $package
     * @param Dyna_Customer_Model_Customer $customer
     * @return array
     */
    public function getXMLData($package, $customer, $packageIndex, $packages)
    {
        $this->customer = $customer;
        /** @var Dyna_Superorder_Helper_Client clientHelper */
        $this->clientHelper = Mage::helper('dyna_superorder/client');
        $subscriberData = Mage::getModel('dyna_superorder/client_submitOrderClient_packageSubscriber');
        $serviceAddress = $package->getServiceAddressData();

        $phonebookChoice = $this->clientHelper->getCheckout('other[phonebook_choice][' . $package->getPackageId() . '][' . $package->getType() . ']');
        if (!empty($phonebookChoice) && $phonebookChoice != Dyna_Checkout_Block_Cart_Steps_SaveOther::PHONEBOOK_CHOICE_NO_ENTRIES) {
            $phoneBookEntry = $this->getPhoneBookEntryData(
                $package->getType(),
                strtolower($package->getType()),
                $package->getPackageId()
            );
        }

        return array(
            'LogicalPackageID' => array(
                'ID' => $packageIndex
            ),
            'PackageType' => Dyna_Superorder_Helper_Client::getPackageType($package->getType()),
            'Action' => $package->getPackageAction(),
            'UseCaseIndication' => $package->getUseCaseIndication() ? $package->getUseCaseIndication() : $this->getUseCaseIndication($package),
            'ProcessContext' => strtoupper($package->getSaleType()),
            'DeliveryDate' => $this->determineDeliveryDate($package),
            'BundleID' => count($package->getBundles()) ? array(
                'ID' => $package->getBundles()[0]->getId(),
                'MemberType' => $package->isPartOfRedPlus()
                    ? ($package->getParentId() ? Dyna_Customer_Helper_Customer::SUBSCRIPTION_TYPE_MEMBER : Dyna_Customer_Helper_Customer::SUBSCRIPTION_TYPE_OWNER)
                    : null,
            ) : null,
            'CustomerSequenceID' => array(
                'ID' => $package->getCustomerSequenceId(),
            ),
            'DeliveryOrderID' => array(
                'ID' => $package->getDeliveryOrder()->getIncrementId(),
            ),
            'OGWOrderLineID' => $package->getOGWOrderLineIDNode(),
            'CallDataEvaluationIndicator' => 'N', //static value, as indicated in OMNVFDE-852
            'ProlongationType' => $package->getProlongationType(),
            'Category' => $this->clientHelper->getPackageCategoryType($package),
            'Asb' => isset($serviceAddress['asb']) ? $serviceAddress['asb'] : null,
            'Subscriber' => $subscriberData->getSubscriberData($package, $customer, null, $packages),
            'ServiceAddress' => array(
                'ID' => $serviceAddress['id'],
                'AddressTypeCode' => 'S',
                'Postbox' => $serviceAddress['postbox'],
                'StreetName' => $serviceAddress['street'],
                'BuildingNumber' => $serviceAddress['houseno'],
                'CityName' => $serviceAddress['city'],
                'PostalZone' => $serviceAddress['postcode'],
                'Location' => null,
                'CountrySubentity' => null,
                'District' => $serviceAddress['district'],
                'Country' => array(
                    'Name' => 'DEU'
                ),
                'HouseNumberAddition' => $serviceAddress['addition'],
                'Validated' => 'true'
            ),
            'Component' => $this->clientHelper->getComponents($package),
            'BillingAccount' => $this->getBillingData($package),
            'LocalAreaCode' => $package->isFixed() && isset($serviceAddress['area_code']) ? $serviceAddress['area_code'] : null,
            'PhonebookEntry' => isset($phoneBookEntry) ? $phoneBookEntry : null,
        );
    }

    /**
     * Generate PhoneBookEntry fields
     * @param int $packageId
     * @return array
     */
    private function getPhoneBookEntryData($accountType,$packageType,$packageId = 1)
    {
        $personalDetails = $this->clientHelper->getPhoneBookPersonalFields($accountType,$packageType,$packageId);
        $phoneBook = ($personalDetails && $personalDetails != '') ? $personalDetails : [];

        if ($personalDetails == '') {

            // Company name will be concatenated with family name
            $familyName = array();
            if($this->clientHelper->getCheckout('other[extended]['.$packageId.']['.$packageType.'][lastname]'))
                $familyName[] = $this->clientHelper->getCheckout('other[extended]['.$packageId.']['.$packageType.'][lastname]');
            if($this->clientHelper->getCheckout('other[extended]['.$packageId.']['.$packageType.'][company_name]'))
                $familyName[] = $this->clientHelper->getCheckout('other[extended]['.$packageId.']['.$packageType.'][company_name]');

            $familyName = implode(", ", $familyName);

            $phoneBook = array(
                'Address' => null,
                'Type' => Dyna_Superorder_Helper_Client::getPhoneBookTypeValue($this->clientHelper->getCheckout('other[phonebook_choice][' . $packageId . '][' . strtolower($packageType) . ']')),
                'InverseSearchIndicator' => null,
                'TelephoneInfoUse' => null,
                'NamePrefix' => $this->clientHelper->getCheckout('other[extended]['.$packageId.']['.$packageType.'][co_users][firstname]') ?: null,
                'FaxDirectoryIndicator' => null,
                'VoiceDirectoryIndicator' => null,
                'NameSuffix' => $this->clientHelper->getCheckout('other[extended]['.$packageId.']['.$packageType.'][co_users][lastname]') ?: null,
                'InverseSearch' => $this->clientHelper->getCheckout('other[extended]['.$packageId.'][not_inverse_search]') ? 'Y' : 'N',
                'InformationUse' => Dyna_Superorder_Helper_Client::getInformationExchange($this->clientHelper->getCheckout('other[extended]['.$packageId.']['.$packageType.'][information_exchange]')),
                'ElecListIndicator' => $this->clientHelper->getCheckout('other[extended]['.$packageId.']['.$packageType.'][directories_entry][electronic]') ? 'Y' : 'N',
                'PrintListIndicator' => $this->clientHelper->getCheckout('other[extended]['.$packageId.']['.$packageType.'][directories_entry][printed]') ? 'Y' : 'N',
                'PublishAddress' => $this->clientHelper->getCheckout('other[extended]['.$packageId.']['.$packageType.'][publish_address]') ? 'Y' : 'N',
                'AltUserFirstName' => null,
                'AltUserLastName' => null,
                'Profession' => $this->clientHelper->getCheckout('other[extended]['.$packageId.']['.$packageType.'][profession]'),
                'KeyWord' => $this->clientHelper->getCheckout('other[extended]['.$packageId.']['.$packageType.'][keyword]'),
                'PersonalDetails' => array(
                    'FirstName' => $this->clientHelper->getCheckout('other[extended]['.$packageId.']['.$packageType.'][firstname]') ?: null,
                    'FamilyName' => $familyName,
                    'Title' => $this->clientHelper->getCheckout('other[extended]['.$packageId.']['.$packageType.'][prefix]') ?: null,
                    'Salutation' => $this->clientHelper->getSalutation($this->clientHelper->getCheckout('other[extended]['.$packageId.']['.$packageType.'][gender]')) ?: null,
                    'ContactPhoneNumber' => null
                )
            );

            if (!empty($portingData = $this->hasPortingPhoneNumbers($packageId,$packageType))) {
                $phoneBook['PersonalDetails']['ContactPhoneNumber'] = $portingData;
            }
        }

        return $phoneBook;
    }

    /**
     * Determine whether or not porting is required and add numbers that need to be ported to submit order request
     * @see OMNVFDE-2099
     * @return array
     */
    private function hasPortingPhoneNumbers($packageId,$packageType)
    {
        $portingData = array();

        // There are maximum three porting options
        for ($i = 0; $i < 3; $i++) {
            // Selected phone number is a reference incremented by one to "provider[phone_transfer_list][telephone][index-1]"
            $phoneNumber = $this->clientHelper->getCheckout('other[extended]['.$packageId.']['.$packageType.'][phone_entries][' . $i . '][telephone_no]');
            $phonePrefix = $this->clientHelper->getCheckout('other[extended]['.$packageId.']['.$packageType.'][phone_entries][' . $i . '][prefix]');
            $phoneType = $this->clientHelper->getCheckout('other[extended]['.$packageId.']['.$packageType.'][phone_entries][' . $i . '][type]');
            // Hardcoded as requested on item
            $phoneCountryCode = 49;
            // If no phone entry selected, do not send this info to submit order
            if (!$phoneNumber || ($phoneNumber == Mage::helper('omnius_customer')->__('New')) || ($phoneType == Dyna_Checkout_Block_Cart_Steps_SaveOther::PHONE_ENTRY_TYPE_NO_ENTRY)) {
                continue;
            }
            // There is a valid phone number so adding this to returned array
            // This is the value saved in checkout fields when New entry is selected
            $portingData[] = array(
                'CountryCode' => $phoneCountryCode,
                'LocalAreaCode' => $phonePrefix,
                'PhoneNumber' => $phoneNumber,
                'Type' => $phoneType,
            );
        }

        return $portingData;
    }

    /**
     * Build the package billing account data
     * @param Dyna_Package_Model_Package $package
     * @return array
     */
    protected function getBillingData($package)
    {
        $callDetailType = $callDigitsMask = null;
        switch($this->clientHelper->getCheckout('other[single_connection][' . $package->getType() . ']'))
        {
            case Dyna_Superorder_Helper_Client::CONNECTION_NONE:
                $callDetailType = 'N';
                $callDigitsMask = '0';
                break;
            case Dyna_Superorder_Helper_Client::CONNECTION_FULL:
                $callDetailType = 'S';
                $callDigitsMask = '3';
                break;
            case Dyna_Superorder_Helper_Client::CONNECTION_CONDENSED:
                $callDetailType = 'M';
                $callDigitsMask = '3';
                break;
        }

        $totalPrice = 0.00;
        $items = $package->getItems(Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_DEVICE);
        /** @var Omnius_Checkout_Model_Sales_Quote_Item $item */
        foreach($items as $item) {
            $totalPrice += $item->getItemFinalPriceInclTax();
        }

        $dunningStatus = null;
        if ($package->isMobile()) {
            if (is_null($this->customer->getEntityId())) { // new customer
                $dunningStatus = 'NO_DUNNING';
            } else {
                $status = $this->customer->getDunningStatusForPackage($package);
                $dunningStatus = is_null($status) ?: strtoupper($status);
            }
        }

        $returnData = [];

        foreach( $this->clientHelper->getChargeType( $package ) as $chargeType ){

            $bic = null;
            $directIban = $this->clientHelper->getIBANFieldValue( $package, $chargeType );
            if($directIban){
                $bic = $this->clientHelper->getCheckout("[$directIban][bank_code]");
            }

            $paymentType = null;
            if( $package->isMobile() ) {
                $paymentMethod = $this->clientHelper->getPaymentMethod( $package, $chargeType );
                if ($paymentMethod == 'DD') {
                    $paymentType =  'directDebit';
                } else {
                    $paymentType = 'invoice';
                }
            }

            $returnData[] =         array(
                'ID' => $directIban,//@excel
                'FinancialInstitutionBranch' => array(
                    // send last 10 digits from account number as submit order fails validation (see OMNVFDE-2913)
                    'ID' => substr($this->clientHelper->getCheckout("iban[$directIban][account_number]"), -10),//@excel
                    'Name' => $this->clientHelper->getCheckout("iban[$directIban][credit_provider]"),//@excel
                    'FinancialInstitution' => array(
                        'ID' => $this->clientHelper->getCheckout("iban[$directIban][bank_code]"),//@excel,
                        'Name' => $this->clientHelper->getCheckout("iban[$directIban][usage]"),//@excel,
                    ),
                ),
                'PartyIdentyfication' => $this->customer->getGlobalId() ? array('ID' => $this->customer->getCustomerNumber()) : null,
                // @TODO Needs update with user as account owner or other person as account owner
                'AccountOwnerName' => $this->clientHelper->getAccountHolderieldValue(),//@excel
                'PaymentMethodList' => $this->clientHelper->getPaymentMethodList($package),//@excel
                'BillingType' => $this->clientHelper->getBillingType($package),
                'CallDetailType' => $callDetailType,//@excel
                'DigitsMaskED' => null,
                'DigitsMaskCD' => $callDigitsMask,//@excel
                'BooIndicator' => null,
                'Purpose' => $this->clientHelper->getCheckout("iban[$directIban][usage]"), //@excel
                'IBANValidated' => !empty( $directIban ) ? 'true' : '',
                'MandateMethod' => $this->clientHelper->getSepaMandate( $package ),
                'ChargesType' => $chargeType,
                'PaymentType' => $paymentType,
                'TotalPrice' => $totalPrice > 0.00 ? $totalPrice : null,
                'DunningStatus' => $dunningStatus,
                'BIC' => $bic
            );
        }
        return $returnData;
    }

    /**
     * Returns the Use Case Indication
     * @param Dyna_Package_Model_Package $package
     * @return string
     */

    public function getUseCaseIndication($package)
    {
        $packageType = strtolower($package->getType());
        $processContext = $package->getSaleType();
        $customer = Mage::getSingleton('dyna_customer/session')->getCustomer();
        $account_category = $customer->getAccountCategory();

        $useCase = '';

        if(!$account_category){
            switch ($packageType) {
                case strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE):
                case strtolower(Dyna_Catalog_Model_Type::TYPE_PREPAID):
                    $useCase = 'NACQMOB';
                    break;
                case strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_INTERNET_PHONE):
                case strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_INDEPENDENT):
                case strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_TV):
                    $useCase = 'NACQCAB';
                    break;
                case strtolower(Dyna_Catalog_Model_Type::TYPE_FIXED_DSL):
                    $useCase = 'NACQDSL';
                    break;
            }
        } else {
            switch ($packageType) {
                case strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE):
                case strtolower(Dyna_Catalog_Model_Type::TYPE_PREPAID):
                    switch ($processContext) {
                        case Dyna_Catalog_Model_ProcessContext::ACQ:
                            switch ($account_category) {
                                case 'KIAS':
                                    $useCase = 'XMOBMOB';
                                    break;
                                case 'FN':
                                    $useCase = 'XMOBDSL';
                                    break;
                                case 'KD':
                                    $useCase = 'XDSLMOBCAB';
                                    break;
                                case 'ALL':
                                    $useCase = 'XMOBMOB';
                                    break;
                            }
                            break;
                        case Dyna_Catalog_Model_ProcessContext::ILSBLU:
                            switch ($account_category) {
                                case 'KIAS':
                                    $useCase = 'ILSMOBCHG';
                                    break;
                            }
                            break;
                        case Dyna_Catalog_Model_ProcessContext::RETNOBLU:
                        case Dyna_Catalog_Model_ProcessContext::RETBLU:
                        case Dyna_Catalog_Model_ProcessContext::RETTER:
                            switch ($account_category) {
                                case 'KIAS':
                                    $useCase = 'ILSMOBVVL';
                                    break;
                            }
                            break;
                        case Dyna_Catalog_Model_ProcessContext::MIGCOC:
                            switch ($account_category) {
                                case 'KIAS':
                                    $useCase = 'ILSMOBD2C';
                                    break;
                            }
                            break;
                    }
                    break;
                case strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_INTERNET_PHONE):
                case strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_INDEPENDENT):
                case strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_TV):
                    switch ($processContext) {
                        case Dyna_Catalog_Model_ProcessContext::ACQ:
                            switch ($account_category) {
                                case 'KD':
                                    $useCase = 'XCABCAB';
                                    break;
                                case 'KIAS':
                                    $useCase = 'XCABMOB';
                                    break;
                                case 'FN':
                                    $useCase = 'XCABDSL';
                                    break;
                                case 'ALL':
                                    $useCase = 'XCABCAB';
                                    break;
                            }
                            break;
                        case Dyna_Catalog_Model_ProcessContext::ILSBLU:
                            switch ($account_category) {
                                case 'KD':
                                    $useCase = 'ILSCAB';
                                    break;
                                case 'FN':
                                    $useCase = 'ILSCAB';
                                    break;
                            }
                            break;
                        case Dyna_Catalog_Model_ProcessContext::MIGCOC:
                            switch ($account_category) {
                                case 'FN':
                                    $useCase = 'MIGDSLCAB';
                                    break;
                            }
                            break;
                    }
                    break;
                case strtolower(Dyna_Catalog_Model_Type::TYPE_FIXED_DSL):
                    switch ($processContext) {
                        case Dyna_Catalog_Model_ProcessContext::ACQ:
                            switch ($account_category) {
                                case 'KD':
                                    $useCase = 'XDSLCAB';
                                    break;
                                case 'KIAS':
                                    $useCase = 'XDSLMOB';
                                    break;
                                case 'FN':
                                    $useCase = 'XDSLDSL';
                                    break;
                                case 'ALL':
                                    $useCase = 'XDSLDSL';
                                    break;
                            }
                            break;
                        case Dyna_Catalog_Model_ProcessContext::ILSNOBLU:
                            switch ($account_category) {
                                case 'KD':
                                    $useCase = 'ILSDSL';
                                    break;
                            }
                            break;
                        case Dyna_Catalog_Model_ProcessContext::MOVE:
                            switch ($account_category) {
                                case 'KD':
                                    $useCase = 'MOVCABDSL';
                                    break;
                            }
                            break;
                    }
                    break;
            }
        }
        return $useCase;
    }

    /**
     * @param Dyna_Package_Model_Package $package
     * @return string
     */
    protected function determineDeliveryDate($package)
    {
        $deliveryDate = null;
        if($package->isFixed()) {
            $this->clientHelper->getCheckout('provider[' . strtolower($package->getType()) . '][wish_date_option]') == 1
             ? $deliveryDate = date("Y-m-d",
                strtotime($this->clientHelper->getCheckout('provider[' . strtolower($package->getType()) . '][wish_date]')))
             : null;
        }
        if($package->isCable()) {
            $dateChoiceField = $this->clientHelper->getCheckout('delivery[method]') == Dyna_Checkout_Block_Cart_Steps_SaveDeliveryAddress::DELIVERY_SPLIT_DELIVERY
                ? 'delivery[pakket]['.$package->getPackageId().'][date_choice]' : 'delivery[date_choice]';
            if($this->clientHelper->getCheckout($dateChoiceField) == 2) {
                $field = $this->clientHelper->getCheckout('delivery[method]') == Dyna_Checkout_Block_Cart_Steps_SaveDeliveryAddress::DELIVERY_SPLIT_DELIVERY
                    ?  'delivery[pakket]['.$package->getPackageId().'][date]' : 'delivery[date]';
                $deliveryDate = date("Y-m-d", strtotime($this->clientHelper->getCheckout($field)));
            }
        }
        return $deliveryDate;
    }
}
