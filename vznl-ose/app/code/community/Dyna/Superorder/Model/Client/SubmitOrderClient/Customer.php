<?php

/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Superorder_Model_Client_SubmitOrderClient_Customer
 */
class Dyna_Superorder_Model_Client_SubmitOrderClient_Customer extends Mage_Core_Model_Abstract
{
    /** @var  Dyna_Superorder_Helper_Client */
    protected $clientHelper;

    /**
     * Builds the array of customer data that needs to be sent in the processOrder request
     * @param Dyna_Customer_Model_Customer $customer
     * @param $customerEntity
     * @return array
     */
    public function getXMLData($customer, $customerEntity, Dyna_Superorder_Model_Superorder $superorder)
    {
        $this->clientHelper = Mage::helper('dyna_superorder/client');
        if ($customer->getId()) {
            $customerIsSoho = $customer->getIsSoho();
        } else {
            $customerType = strtolower($this->clientHelper->getCheckout('customer[type]'));
            $customerIsSoho = ($customerType == strtolower(Dyna_Customer_Model_Client_RetrieveCustomerInfo::PARTY_TYPE_SOHO)) ? true : false;
        }

        // customer BAN sent for known customer to ordering stack (@see OVG-2406)
        $customerOfCategory = Mage::getModel('customer/customer');
        if ($customerEntity['customer_number']) {
            $customerOfCategory = Mage::getSingleton('dyna_customer/session')->getCustomerByNumber($customerEntity['customer_number']);
        }

        $accountCategory = strtolower($customerEntity['account_type']);
        $categoryDsl = strtolower(Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_DSL);

        if ($customerIsSoho && $accountCategory == $categoryDsl) {
            if ($this->clientHelper->getCheckout('customer[business][commercial_register]') == 2) {
                $cityName = $this->clientHelper->getCheckout('customer[address][company_trade_location]');
            } else {
                $cityName = '';
            }
        } else {
            $cityName = $this->clientHelper->getCheckout('customer[address][city]');
        }

        return array(
            'ParentAccountNumber' => $customerOfCategory->getCustomerNumber() ? array("ID" => $customerOfCategory->getCustomerNumber()) : null,
            'PartyIdentification' => $customerOfCategory->getGlobalId() ? array("ID" => $customerOfCategory->getGlobalId()) : null,
            'PartyLegalEntity' => $this->clientHelper->getCustomerType('customer[type]') == Dyna_Customer_Model_Client_RetrieveCustomerInfo::PARTY_TYPE_PRIVATE ? null : array(
                'RegistrationName' => $this->clientHelper->getCheckout('customer[address][company_name]'),
                'CompanyID' => $this->clientHelper->getCheckout('customer[address][company_registration_number]'),
                'CompanyLegalForm' => $this->clientHelper->getCheckout('customer[address][commercial_register_type]'),
                'RegistrationAddress' => array(
                    'CityName' => $cityName,
                ),
            ),
            'SequenceID' => array(
                'ID' => $customerEntity['sequence_id']
            ),

            'PostalAddresses' => array(
                $this->getPostalAddresses()
            ),
            'ContactPhoneNumber' => $this->clientHelper->getContactPhoneNumbers($customer->getIsSoho(), $customerEntity['account_type']),
            'Contact' => $this->getContactData($customer),
            'Role' => ($customerIsSoho) ? $this->getRoleDataForSoho() : $this->getRoleData(),
            //'CustomerPassword' => $customer->getCustomerPassword(),
            'CustomerPassword' => $this->clientHelper->getCheckout('customer[account_password]'),
            'AccountCategory' => $customerEntity['account_type'],
            'AccountClass' => 'RG',//static value based on OMNVFDE-852
            'PartyType' => $this->clientHelper->getCustomerType('customer[type]'),
            'PortingInformation' => $this->getPortingInformationData($superorder, $customerEntity['account_type']),
            'AccountCreationDate' => $customer->getInstallBaseDataBy($customerEntity['customer_number'], 'contract_activation_date') ?: date('Y-m-d'),
            'DunningStatus' =>$customer->getDunningStatus()
        );
    }

    /**
     * Builds the array of installed base customer data that needs to be sent in the processOrder request
     * @param Dyna_Customer_Model_Customer $customer
     * @param $customerEntity
     * @return array
     */
    public function getInstalledBaseXMLData($installedBaseCustomerEntity)
    {
        $this->clientHelper = Mage::helper('dyna_superorder/client');

        $customerOfCategory = Mage::getSingleton('dyna_customer/session')->getCustomerByNumber($installedBaseCustomerEntity['customer_number']);
        $customerIsSoho = $customerOfCategory->getIsSoho();
        $noLegalInfo = !$customerOfCategory->getCompanyName() && !$customerOfCategory->getCompanyId() && !$customerOfCategory->getCompanyLegalForm() && !$customerOfCategory->getCompanyCity();

        return array(
            'PhonebookEntry' => null,
            'ParentAccountNumber' => $customerOfCategory->getCustomerNumber() ? array("ID" => $customerOfCategory->getCustomerNumber()) : null,
            'PartyIdentification' => $customerOfCategory->getGlobalId() ? array("ID" => $customerOfCategory->getGlobalId()) : null,
            'PartyLegalEntity' => !$customerIsSoho || $noLegalInfo
                ? null
                : array(
                    'RegistrationName' => $customerOfCategory->getCompanyName() ?: null,
                    'CompanyID' => $customerOfCategory->getCompanyId() ?: null,
                    'CompanyLegalForm' => $customerOfCategory->getCompanyLegalForm() ?: null,
                    'RegistrationAddress' => !$customerOfCategory->getCompanyCity() ? null : array(
                        'CityName' => $customerOfCategory->getCompanyCity(),
                    ),
                ),
            'SequenceID' => array(
                'ID' => $installedBaseCustomerEntity['sequence_id']
            ),
            'PostalAddresses' => array(
                $this->getPostalAddresses(true)
            ),
            'ContactPhoneNumber' => $this->getInstalledBaseContactPhoneNumbers($customerOfCategory),
            'Contact' => $this->getInstalledBaseContactData($customerOfCategory),
            'Role' => $this->getInstalledBaseRoleData($customerOfCategory),
            'CustomerPassword' => $customerOfCategory->getCustomerPassword() ?: null,
            'AccountCategory' => $installedBaseCustomerEntity['account_type'],
            'AccountClass' => 'RG',//static value based on OMNVFDE-852
            'PartyType' => $customerOfCategory->getPartyType(),
            'PortingInformation' => null,
            'AccountCreationDate' => $customerOfCategory->getInstallBaseDataBy($installedBaseCustomerEntity['customer_number'], 'contract_activation_date') ?: date('Y-m-d'),
            'DunningStatus' => $customerOfCategory->getDunningStatus()
        );
    }

    private function getPostalAddresses($installedBaseScope = false)
    {
        /**
         * When the shipping is in a Vodafone pickup store we should send in the MarkAttention field the
         * <ShopName1><SPACE><ShopName2><SPACE><ShopName3>
         * OMNVFDE-1610
         */
        return array(
            'ID' => null,
            'AddressTypeCode' => 'S',
            'StreetName' => '',
            'MarkAttention' => $installedBaseScope ? null : $this->getMarkAttention(),
            'Postbox' => null,
            'BuildingNumber' => null,
            'CityName' => null,
            'PostalZone' => null,
            'CountrySubentity' => null,
            'District' => null,
            'Country' => null,
            'HouseNumberAddition' => null,
            'Validated' => null,
            'Location' => null
        );

    }

    /**
     * When the shipping is in a Vodafone pickup store we should send in the MarkAttention field the
     * <ShopName1><SPACE><ShopName2><SPACE><ShopName3>
     * OMNVFDE-1610
     */
    private function getMarkAttention()
    {
        $markAttention = null;
        if ($this->clientHelper->getCheckout('delivery[pickup][store_id]')) {
            $markAttention = $this->clientHelper->getCheckout('delivery[pickup_address][shopName1]');
            if ($this->clientHelper->getCheckout('delivery[pickup_address][shopName2]')) {
                $markAttention .= ' ' . $this->clientHelper->getCheckout('delivery[pickup_address][shopName2]');
            }
            if ($this->clientHelper->getCheckout('delivery[pickup_address][shopName3]')) {
                $markAttention .= ' ' . $this->clientHelper->getCheckout('delivery[pickup_address][shopName3]');
            }
        }
        return $markAttention;
    }

    /**
     * Build the content for the Contact node
     * @param $customer
     * @return array
     */
    private function getContactData($customer)
    {
        return array(
            'Salutation' => $this->clientHelper->getCustomerContactSalutation(),
            'Telefax' => $customer->getFaxNumber(),
            'ElectronicMail' => $this->clientHelper->getCheckout('customer[contact_details][email]'),
            'FirstName' => $this->clientHelper->getCustomerContactFirstName(),
            'LastName' => $this->clientHelper->getCustomerContactLastName(),
            'AdditionalName' => $this->clientHelper->getCustomerAdditionalName(),
            'CallbackTime' => null,//ignored based on OMNVFDE-852,
        );
    }

    /**
     * Build the content for the Contact node belonging to an installed base Customer node
     * @param $customer Dyna_Customer_Model_Customer
     * @return array
     */
    private function getInstalledBaseContactData($customer)
    {
        return array(
            'Salutation' => $this->clientHelper->getSalutation($customer->getSubscriptionContactSalutation(true)) ?: null,
            'Telefax' => $customer->getFaxNumber() ?: null,
            'ElectronicMail' => $customer->getEmailAddressWithoutCreatingDummyOne() ?: null,
            'FirstName' => $customer->getSubscriptionContactFirstname(true) ?: null,
            'LastName' => $customer->getSubscriptionContactLastname(true) ?: null,
            'AdditionalName' => $customer->getCompanyContactAddName() ?: null,
            'CallbackTime' => null,//ignored based on OMNVFDE-852,
        );
    }

    /**
     * Build the Role Data
     * @return array
     */
    private function getRoleData()
    {
        return array(
            'Person' => array(
                'Salutation' => $this->clientHelper->getSalutation('customer[gender]'),
                'Title' => $this->clientHelper->getCheckout('customer[prefix]'),
                'FirstName' => $this->clientHelper->getCheckout('customer[firstname]'),
                'FamilyName' => $this->clientHelper->getCheckout('customer[lastname]'),
                'BirthDate' => !empty($this->clientHelper->getCheckout('customer[dob]')) ?
                    date("Y-m-d", strtotime($this->clientHelper->getCheckout('customer[dob]'))) : null,
                'IdentificationCardNumber' => $this->clientHelper->getCheckout('customer[id_number]'),
                'NationalityID' => $this->clientHelper->getCheckout('customer[nationality]'),
            )
        );
    }

    /**
     * Build the Role Data
     * @return array
     */
    private function getInstalledBaseRoleData($customer)
    {
        return array(
            'Person' => array(
                'Salutation' => $this->clientHelper->getSalutation($customer->getSalutation(true)) ?: null,
                'Title' => $customer->getTitle() ?: null,
                'FirstName' => $customer->getFirstname() ?: null,
                'FamilyName' => $customer->getLastname() ?: null,
                'BirthDate' => !empty($customer->getDob()) ? date("Y-m-d", strtotime($customer->getDob())) : null,
                'IdentificationCardNumber' => $customer->getIdentificationNumber() ?: null,
                'NationalityID' => $customer->getNationalityId() ?: null,
            )
        );
    }

    /**
     * Identify the soho data for role
     * @return array
     */
    private function getRoleDataForSoho()
    {
        return array(
            'Person' => array(
                'FirstName' => $this->clientHelper->getCheckout('customer[firstname]') ?:
                    $this->clientHelper->getCheckout('customer[contact_person][first_name]'),
                'FamilyName' => $this->clientHelper->getCheckout('customer[lastname]') ?:
                    $this->clientHelper->getCheckout('customer[contact_person][last_name]'),
                'BirthDate' => !empty($this->clientHelper->getCheckout('customer[dob]')) ?
                    date("Y-m-d", strtotime($this->clientHelper->getCheckout('customer[dob]'))) : null,
                'Title' => $this->clientHelper->getCheckout('customer[prefix]'),
                'NationalityID' => $this->clientHelper->getCheckout('customer[nationality]'),
                'Salutation' => $this->clientHelper->getSalutation('customer[gender]'),
            )
        );
    }

    /**
     * Build the content for the porting information node
     * @return array
     */
    private function getPortingInformationData(Dyna_Superorder_Model_Superorder $superorder, $accountType)
    {
        // setting it null to not send nodes if no porting required
        $portingData = null;
        foreach ($superorder->getPackages() as $package) {

            $omitPackage = false;
            switch( $accountType ) {
                case Dyna_Customer_Model_Customer::ACCOUNT_CATEGORY_MOBILE_POSTPAID:
                    if( !$package->isMobilePostpaid() ) {
                        $omitPackage = true;
                    }
                    break;
                case Dyna_Customer_Model_Customer::ACCOUNT_CATEGORY_MOBILE_PREPAID:
                    if( !$package->isPrepaid() ) {
                        $omitPackage = true;
                    }
                    break;
                case Dyna_Customer_Model_Customer::ACCOUNT_CATEGORY_FIXED:
                    if( !$package->isFixed() ) {
                        $omitPackage = true;
                    }
                    break;
                case Dyna_Customer_Model_Customer::ACCOUNT_CATEGORY_CABLE:
                    if( !$package->isCable() ) {
                        $omitPackage = true;
                    }
                    break;
            }
            if( $omitPackage ) {
                continue;
            }

            // the value 1 in key states that porting has been selected for this package and sent through post request
            $currentPackagePortingKey = "provider[" . strtolower($package->getType()) . "]";
            // If this key has value, Ja has been checked
            if ($this->clientHelper->getCheckout($currentPackagePortingKey . "[change_provider]") == Dyna_Superorder_Helper_Client::NP_CHANGE_PROVIDER_YES) {
                // making sure porting data is changing from null to array if it is null
                !is_null($portingData) ?: $portingData = array();
                $mobilePortingPhoneNumber = ($mobilePortingPhoneNumbers = $this->clientHelper->getPortingPhoneNumbers($package, 'Mobile', true)) ? $mobilePortingPhoneNumbers[0] : null;

                // VFDED1W3S-857: Add NumberOfPortings in ProcessOrder
                $numberOfPortings = null;

                $valueOfFieldNoPhonesTransfer = $this->clientHelper->getCheckout($currentPackagePortingKey . '[no_phones_transfer]');
                switch ($valueOfFieldNoPhonesTransfer) {
                    case Dyna_Superorder_Helper_Client::PORTING_ALL_NUMBERS:
                        $numberOfPortings = 'ALL_NUMBERS';
                        break;
                    case Dyna_Superorder_Helper_Client::PORTING_ONE_NUMBER:
                        $numberOfPortings = 'ONE_NUMBER';
                        break;
                    case Dyna_Superorder_Helper_Client::PORTING_MULTIPLE_NUMBERS:
                        $numberOfPortings = 'MULTIPLE_NUMBERS';
                        break;
                }

                $portingData[] = array(
                    'HandInDataLater' => $this->clientHelper->getCheckout($currentPackagePortingKey . '[change_provider]', Dyna_Superorder_Helper_Client::NP_CHANGE_PROVIDER_DATA_LATER),
                    'CarrierId' => $this->clientHelper->getCheckout($currentPackagePortingKey . '[current_provider_type]') ?: null,
                    'CarrierCustomerId' => null,
                    'CarrierContractCancel' => $this->clientHelper->getCarrierContractCancel($package->getType()),
                    'CarrierIdentificationPhoneNumber' => $mobilePortingPhoneNumber,
                    'PortingPhoneNumber' => $this->clientHelper->getPortingPhoneNumbers($package),
                    'PortingDate' => $this->clientHelper->getCheckout($currentPackagePortingKey . '[wish_date]') ? date("Y-m-d", strtotime($this->clientHelper->getCheckout($currentPackagePortingKey . '[wish_date]'))) : null,
                    'PortingOrderDate' => date("Y-m-d"),
                    'PortAllNumbers' => $this->clientHelper->getCheckout($currentPackagePortingKey . '[no_phones_transfer]', Dyna_Superorder_Helper_Client::PORTING_ALL_NUMBERS),
                    'EndOfTermMode' => $this->clientHelper->getEndOfTermMode(),
                    'EndOfTermDate' => $this->clientHelper->getCheckout($currentPackagePortingKey . '[notice_date]') ? date("Y-m-d", strtotime($this->clientHelper->getCheckout($currentPackagePortingKey . '[notice_date]'))) : null,
                    'NumberOfPortings' => $numberOfPortings,
                    'TempNumberNeeded' => null,
                    'InstallationDate' => null,
                    'DonorAccNumber' => null, //can be neglected based on OMNVFDE-852
                    'DesiredDate' => null, //can be neglected based on OMNVFDE-852
                    'DesiredDateType' => null, //can be neglected based on OMNVFDE-852
                    'ContCnclDate' => null, //can be neglected based on OMNVFDE-852
                    'ContEndDate' => null, //can be neglected based on OMNVFDE-852
                    'ThirdName' => null, //can be neglected based on OMNVFDE-852
                    'MnpInfoNameFormat' => null, //can be neglected based on OMNVFDE-852
                    'LongNameIndicator' => null, //can be neglected based on OMNVFDE-852
                    'MnpInfoContactName' => null, //can be neglected based on OMNVFDE-852
                );
            }  elseif ($this->clientHelper->getCheckout($currentPackagePortingKey . '[change_provider]') == Dyna_Superorder_Helper_Client::NP_CHANGE_PROVIDER_DATA_LATER) {
                !is_null($portingData) ?: $portingData = array();
                // Ja, daten werden nachgereicht
                $portingData[] = array(
                    'HandInDataLater' => $this->clientHelper->getCheckout($currentPackagePortingKey . '[change_provider]', Dyna_Superorder_Helper_Client::NP_CHANGE_PROVIDER_DATA_LATER),
                    'CarrierId' => null,
                    'CarrierCustomerId' => null,
                    'CarrierContractCancel' => null,
                    'PortingSubscriberAdress' => null,
                    'PortingSubscriberContact' => null,
                    'CarrierIdentificationPhoneNumber' => null,
                    'PortingPhoneNumber' => null,
                    'PortingDate' => null,
                    'PortingOrderDate' => null,
                    'PortAllNumbers' => null,
                    'EndOfTermMode' => null,
                    'EndOfTermDate' => null,
                    'NumberOfPortings' => null,
                    'TempNumberNeeded' => null,
                    'InstallationDate' => null,
                    'DonorAccNumber' => null, //can be neglected based on OMNVFDE-852
                    'DesiredDate' => null, //can be neglected based on OMNVFDE-852
                    'DesiredDateType' => null, //can be neglected based on OMNVFDE-852
                    'ContCnclDate' => null, //can be neglected based on OMNVFDE-852
                    'ContEndDate' => null, //can be neglected based on OMNVFDE-852
                    'ThirdName' => null, //can be neglected based on OMNVFDE-852
                    'MnpInfoNameFormat' => null, //can be neglected based on OMNVFDE-852
                    'LongNameIndicator' => null, //can be neglected based on OMNVFDE-852
                    'MnpInfoContactName' => null, //can be neglected based on OMNVFDE-852
                );
            }
        }

        return $portingData;
    }


    /**
     * Get customer's installed base phone numbers
     * @param $customer Dyna_Customer_Model_Customer
     * @return arrayh
     */
    private function getInstalledBaseContactPhoneNumbers($customer)
    {
        $numberData = array();
        $validPhoneNumberFound = false;
        $contactPhoneNumbers = $customer->getContactPhoneNumbers();
        foreach ($contactPhoneNumbers as $phoneNumber) {
            $validPhoneNumberFound = (!empty($phoneNumber['local_area_code']) && !empty($phoneNumber['phone_number'])) ?: $validPhoneNumberFound;
            if ($phoneNumber['local_area_code'] && $phoneNumber['phone_number']) {
                $numberData[] = array(
                    'CountryCode' => $phoneNumber['country_code'],
                    'LocalAreaCode' => $phoneNumber['local_area_code'],
                    'PhoneNumber' => $phoneNumber['phone_number'],
                    'Type' => $phoneNumber['type']
                );
            }
        }

        return !empty($numberData) && $validPhoneNumberFound ? $numberData : null;
    }
}
