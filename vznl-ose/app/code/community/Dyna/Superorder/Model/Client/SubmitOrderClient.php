<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

use Dompdf\Dompdf;

class Dyna_Superorder_Model_Client_SubmitOrderClient extends Dyna_Service_Model_Client
{
    protected $sequenceId = 1;
    const WSDL_CONFIG_KEY = "process_order/wsdl_process_order";
    const ENDPOINT_CONFIG_KEY = "process_order/endpoint_process_order";
    const CONFIG_STUB_PATH = 'process_order/use_stubs';
    const CONFIG_PORTAL_PATH = 'process_order/portal_value';
    const CONFIG_CREATOR_PATH = 'process_order/creator_value';

    const DEFAULT_MARKETING_CAMPAIGN = 'MGM';
    const DEFAULT_MARKETING_PROMOTION = 'INB';
    const DEFAULT_MARKETING_CODE = 'BRI';

    /**
     * @param Dyna_Superorder_Model_Superorder $superorder
     * @return array
     */
    public function executeSubmitOrder($superorder): array
    {
        $params = array();
        /** @var $clientHelper Dyna_Superorder_Helper_Client */
        $clientHelper = Mage::helper('dyna_superorder/client');
        $packages = $superorder->getPackages();
        $clientCustomerModel = new Dyna_Superorder_Model_Client_SubmitOrderClient_Customer();
        $clientPackageModel = new Dyna_Superorder_Model_Client_SubmitOrderClient_Package();
        //find checkout data by searching the order packages for which the data was saved
        $packageIds = array();
        foreach ($packages as $package) {
            $packageIds[] = $package->getId();
        }
        $orderCheckoutData = Mage::getModel('dyna_checkout/field')->loadQuoteData(array('package_id' => $packageIds));

        $campaign = null;
        if ($campaign_id = $superorder->getOriginalQuote()->getCampaignCode()) {
            $campaign = Mage::getModel('dyna_bundles/campaign')->load($campaign_id, 'campaign_id');
        }
        /** @var Dyna_Customer_Model_Customer $customer */
        $customer = $superorder->getCustomer();
        $customerEntities = Dyna_Superorder_Helper_Client::getCustomerEntities($packages);

        $clientHelper->setClientData($orderCheckoutData, $customer);
        $this->setRequestHeaderInfo($params);
        /** @var Dyna_Agent_Model_Agent $agent */
        $agent = Mage::getModel('agent/agent')->load($superorder->getCreatedAgentId());

        // Marketing details
        if (Mage::getSingleton('customer/session')->getAgent()
            && Mage::getSingleton('customer/session')->getAgent()->canSelectMarketingData()
            && !$campaign
        ) {
            $CampaignCode = $clientHelper->getCheckout('other[marketing][campaign]') ? $clientHelper->getCheckout('other[marketing][campaign]') : self::DEFAULT_MARKETING_CAMPAIGN;
            $AdvertisingCode = $clientHelper->getCheckout('other[marketing][campaign]') ? $clientHelper->getCheckout('other[marketing][promotion]') : self::DEFAULT_MARKETING_PROMOTION;
            $MarketingCode = $clientHelper->getCheckout('other[marketing][campaign]') ? $clientHelper->getCheckout('other[marketing][code]') : self::DEFAULT_MARKETING_CODE;
        } else {
            $CampaignCode = $campaign ? $campaign->getCampaignCode() : self::DEFAULT_MARKETING_CAMPAIGN;
            $AdvertisingCode = $campaign ? $campaign->getAdvertisingCode() : self::DEFAULT_MARKETING_PROMOTION;
            $MarketingCode = $campaign ? $campaign->getMarketingCode() : self::DEFAULT_MARKETING_CODE;
        }

        $Commentskias='';
        $OrderCommentText='';
        $OrderCommentId='';
        if($clientHelper->getCheckout('other[remarks1][mobile]')) {
            if($clientHelper->getCheckout('other[remarks1][mobile]')!='0') {
                $Commentskias=$clientHelper->getCheckout('other[remarks1][content]');
            }
        }
        if($clientHelper->getCheckout('other[remarks2][mobile]')) {
            if($clientHelper->getCheckout('other[remarks2][mobile]')!='0') {
                $Commentskias=$clientHelper->getCheckout('other[remarks2][content]');
            }
        }

        if($clientHelper->getCheckout('other[remarks1][cable]')) {
            if($clientHelper->getCheckout('other[remarks1][cable]')!='0') {
                $OrderCommentText=$clientHelper->getCheckout('other[remarks1][content]');
                $OrderCommentId=$clientHelper->getCheckout('other[remarks1][layout]');
            }
        }
        if($clientHelper->getCheckout('other[remarks2][cable]')) {
            if($clientHelper->getCheckout('other[remarks2][cable]')!='0') {
                $OrderCommentText=$clientHelper->getCheckout('other[remarks2][content]');
                $OrderCommentId=$clientHelper->getCheckout('other[remarks2][layout]');
            }
        }

        $saleOrderParams = array(
            'ID' => $superorder->getOrderNumber(),
            'Portal' => $this->getConfig(self::CONFIG_PORTAL_PATH),
            'Action' => $superorder->getActionType(),
            'Creator' => $this->getConfig(self::CONFIG_CREATOR_PATH),
            'SignatureDate' => date("Y-m-d"),
            'CampaignCode' => $CampaignCode,
            'AdvertisingFlag' => 'false',
            'AdvertisingCode' => $AdvertisingCode,
            'MarketingCode' => $MarketingCode,
            'UctID' => $superorder->getTransactionId() ?: null,
            'OrderOriginatingChannel' => Mage::app()->getWebsite($superorder->getWebsiteId())->getCode(),
            'OrderSubmittingChannel' => Mage::app()->getWebsite($superorder->getWebsiteId())->getCode(),
            'AgentBusinessFunctionID' => $superorder->getCreatedAgentId(),
            //'AdvertisingFlag' => 'N',
            //@todo - removed from UID on Vodafone request. Should be removed from submitOrder as well
            'MarketingFlags' => array(
                'Insert' => 0,
                'TelMarketing' => 0,
                'Advertising' => $clientHelper->getCheckout('voicelog[advertising]') ? $clientHelper->getCheckout('voicelog[advertising]') : 0,
                'MailMarketing' => 0
            ),
             'PersonalDataEvaluation' => $clientHelper->getCheckout('voicelog[personal_data_evaluation]') ? $clientHelper->getCheckout('voicelog[personal_data_evaluation]') : 'false',

            'ShipmentType' => null,
            // Mapping remarks to submit order @see OMNVFDE-2892
            'Comments' => $Commentskias,
            'OrderCommentText' => $OrderCommentText,
            'OrderCommentId' => $OrderCommentId,
            'TechnicianNeeded' => 'true',
            'LineAvailable' => ($clientHelper->getCheckout('provider[connection_active]')) ? 1 : 0,
            'TechnicalInstallationComment' => $clientHelper->getProviderHomeType(),
            'BonusPromotor' => null,//@todo - ignore for the moment based on client request,
            'BonusPromotorId' => null,//@todo - ignore for the moment based on client request
            'BonusCode' => null,//@todo - ignore for the moment based on client request
            'OrderCartVoucherCode' => null, //@todo get coupon code, if any,
            'DeliveryOrder' => array(
                'DeliveryOrderID' => $packages->getFirstItem()->getDeliveryOrder()->getIncrementId(),
                'DeliveryDate' => $packages->getFirstItem()->isFixed() && $clientHelper->getCheckout('provider[wish_date]')
                    ? date("Y-m-d", strtotime(Mage::helper('dyna_superorder/client')->getCheckout('provider[wish_date]')))
                    : null,
                'SAPDeliveryType' => $clientHelper->getSapDeliveryType()
            ),
            'Customer' => array(),
            'BundleDetails' => null
        );
        
        if ($superorder->hasNewHardware() == true) {
            $saleOrderParams['ShipmentType'] = Mage::helper('dyna_superorder/client')->getCheckout('delivery[deliver][address]') == 'store' ? 'ship2store' : 'normal';
        }

        foreach ($this->getDealerCodeData($superorder) as $dealerCodeType => $dealerCodeValue) {
            $saleOrderParams['DealerCodeList'][] = ['DealerCode' => $dealerCodeValue ?: '', 'DealerCodeType' => $dealerCodeType];
        }

        // Add Bundles to salesOrderParams
        foreach ($superorder->getBundles() as $bundle) {
            if ($saleOrderParams['BundleDetails'] == null) {
                $saleOrderParams['BundleDetails'] = array();
            }
            // Add bundle info to submit order
            $saleOrderParams['BundleDetails'][] = array(
                'BundleId' => $bundle->getId(),
                'BundleType' => $bundle->getBundleTypeByOrchestrationId(),
                'OGWOrchestrationType' => $bundle->getOgwOrchestrationType(),
                'LegacyBundleId' => $bundle->getLegacyBundleId(),
            );
        }

        //generate one or more Customer nodes based on the package types
        foreach ($customerEntities as &$customerEntity) {
            $customerEntity['sequence_id'] = $this->sequenceId;

            $customerData = $customerEntity['installed_base_entity']
                ? $clientCustomerModel->getInstalledBaseXMLData($customerEntity)
                : $clientCustomerModel->getXMLData($customer, $customerEntity, $superorder);
            
            $saleOrderParams['Customer'][] = $customerData;
            $this->sequenceId++;
        }
        unset($customerEntity);


        if (Mage::getStoreConfig('omnius_general/log_settings/multimapper_log')) {
            Mage::log('*********************************** Evaluating multimappers for order: ' . $superorder->getOrderNumber() . ' ***********************************', null,'SubmitOrder_Multimapping.log', true);
        }


        $packageData = array();
        $packageIndex = 1;
        foreach ($packages as $package) {
            // PostalAddresses node on Customer -> Role -> Person only added if a hardware order is being places (see OMNVFDE-4404)
            if ($package->hasHardwareProducts() && $package->isMobile()) {
                $subscriberObject = Mage::getModel('dyna_superorder/client_submitOrderClient_packageSubscriber');
                $subscriberData = $subscriberObject->getSubscriberData($package, $customer, Dyna_Customer_Model_Client_RetrieveCustomerInfo::ADDRESS_TYPE_DL, $packages);

                foreach ($saleOrderParams['Customer'] as $index => $customerData) {
                    $saleOrderParams['Customer'][$index]['PostalAddresses'][1] = $saleOrderParams['Customer'][$index]['PostalAddresses'][0];
                    $saleOrderParams['Customer'][$index]['PostalAddresses'][0] = $subscriberData['Address'];
                }
            }
            // VFDED1W3S-2281
            if ($package->getSaleType() == Dyna_Catalog_Model_ProcessContext::ACQ &&
                strtolower($package->getType()) == strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE) &&
                $package->getEditingDisabled() &&
                $package->getSharingGroupId() &&
                $package->isPartOfRedPlus()) {
                continue;
            }

            foreach ($customerEntities as $customerEntity) {
                if ($customerEntity['account_type'] == Dyna_Superorder_Helper_Client::getPackageType($package->getType())
                    && (!$customerEntity['customer_number'] || ($customerEntity['customer_number'] == $package->getParentAccountNumber()))
                ) {
                    $package->setCustomerSequenceId($customerEntity['sequence_id']);
                    break;
                }
            }
            // Get XML data for each package
            $packageData[] = $clientPackageModel->getXMLData($package, $customer, $packageIndex, $packages);
            $packageIndex++;
        }
        $saleOrderParams['LogicalPackage'] = $packageData;

        $params['SalesOrder'] = $saleOrderParams;
        $this->setRequestHeaderInfo($params);
        try {
            $response = $this->SubmitOrder($params);
        } catch (Exception $e) {
            $errorDescription = htmlentities($e->getMessage());
            $errorDetails = get_class($e) == 'SoapFault' ? htmlentities($e->getTraceAsString()) : $e->getMessage();
            $orderId = $superorder->getOrderNumber();

            $workItemParams = [
                'Action' => 'Trigger Order Recovery',
                'EntityType' => 'Order',
                'ErrorCategory' => 'Technical',
                'EntityID' => $orderId,
                'AgentID' => $agent->getId(),
                'Description' => $errorDescription,
                'Comments' => $errorDetails,
                'Order' => [
                    'ID' => $orderId
                ]
            ];

            /** @var Dyna_Configurator_Model_Client_CreateManualWorkItemClient $createManualWorkItemClient */
            $createManualWorkItemClient = Mage::getModel('dyna_configurator/client_createManualWorkItemClient');

            $createManualWorkItemClient->setRequestHeaderInfo($workItemParams);
            $request = $createManualWorkItemClient->buildRequest('CreateManualWorkItem', $workItemParams);
            Mage::getModel('dyna_service/queue')->pushWorkItem($request);

            throw $e;
        }

        //update superorder to associated it the OGW id
        if (isset($response['OrderID'])) {
            $superorder->setData('ogw_id', $response['OrderID'])->save();
        }

        return $response;
    }

    public function generateCallSummary($quote, $superorder)
    {
        if (!$superorder->hasDsl()) {
            $printOutput = Mage::app()->getLayout()
                ->createBlock('dyna_checkout/call')
                ->init($quote)
                ->setTemplate('checkout/cart/pdf/call.phtml')
                ->toHtml();
            $pdfInstance = new DOMPDF;
            $pdfInstance->loadHtml($printOutput);
            $pdfInstance->render();
            $pdfCall = $pdfInstance->output();

            if (!is_dir(Mage::getBaseDir('media') . '/calls')) {
                mkdir(Mage::getBaseDir('media') . '/calls', 0777, true);
            }

            file_put_contents(Mage::getBaseDir('media') . '/calls/' . date("Y-m-d-H-i-s") . '_' . $quote->getId() . '_' . 'call' . '.pdf', $pdfCall);
        }
    }

    /**
     * Get DealerCode data
     * @param $superorder
     * @return array
     */
    protected function getDealerCodeData($superorder)
    {
        $dealerCodes = array();
        foreach ($this->getDealerCodeType() as $dealerCodeType => $salesIdType) {
            $dealerCodes[$dealerCodeType] = $this->getDealerCodeValue($superorder, $salesIdType);
        }
        return $dealerCodes;
    }

    /**
     * Get Type for DealerCode according to cart packages
     * @return array
     */
    public function getDealerCodeType()
    {
        return array(
            'KIAS' => 'red_sales_id',
            'KD'   => 'yellow_sales_id',
            'FN'   => 'blue_sales_id'
        );
    }

    /**
     * Get Value for DealerCode
     * @param $superorder
     * @param $salesIdType
     * @return null
     */
    protected function getDealerCodeValue($superorder, $salesIdType)
    {
        $redSalesId = $superorder->getSalesId();

        if($redSalesId) {
            $agentProvisSales = Mage::getModel('agentde/provisSales')
                ->getCollection()
                ->addFieldToFilter('red_sales_id', $redSalesId)
                ->getFirstItem();

            return $agentProvisSales->getData($salesIdType);
        }

        return '';
    }
}
