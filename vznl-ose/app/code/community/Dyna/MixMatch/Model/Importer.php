<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_MixMatch_Model_Importer
 */
class Dyna_MixMatch_Model_Importer extends Mage_Core_Model_Abstract
{
    const CONFIG_PATH = "dataimport_configuration/mixmatch/file_path";
    /** @var array */
    protected $_products = [];
    /** @var string */
    private $_csvDelimiter = ';';
    /** @var int */
    public $_skippedFileRows = 0;
    /** @var int */
    public $_totalFileRows = 0;
    /** @var Dyna_Import_Helper_Data|null $_helper */
    public $_helper = null;
    /** @var array */
    protected $_header = [];
    protected $_debug = false;
    /** @var string $_logFileName */
    protected $_logFileName = "mixmatch_import";
    /** @var string $_logFileExtension */
    protected $_logFileExtension = "log";
    protected $stack;

    public $executionId = null;

    /**
     * Dyna_Import_Model_Import_Product_Abstract constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->_helper = Mage::helper("dyna_import/data");
        $this->_helper->setImportLogFile($this->_logFileName . '.' . $this->_logFileExtension);
    }

    /**
     * @param $path
     * @param $categoryImport
     * @return bool
     * @throws Exception
     * @internal param $websiteIds
     */
    public function importFile($path, $isCLI = false)
    {
        $contents = file_get_contents($path);
        $this->_helper->setPathToImportFile($path);

        if (!$contents) {
            $msg = '[ERROR] Cannot get content';
            $this->_logError($msg);

            $return = false;
        } else {
            //BOM deleting for UTF files
            if (ord($contents[0]) == 0xEF && ord($contents[1]) == 0xBB && ord($contents[2]) == 0xBF) {
                $contents = substr($contents, 3);
                if (!$contents) {
                    $msg = '[ERROR] Cannot get content substring';
                    $this->_logError($msg);

                    $return = false;
                }
                if (!file_put_contents($path, $contents)) {
                    $msg = '[ERROR] Cannot delete bom';
                    $this->_logError($msg);

                    $return = false;
                }
            }
        }

        if (isset($return)) {
            throw new Exception($msg);
//            return $return;
        }

        unset($contents);
        $startDate = gmdate('Y-m-d h:i:s');
        $userName = null;
        if ($isCLI && Mage::getSingleton('admin/session')->getUser()) {
            $userName = Mage::getSingleton('admin/session')->getUser()->getUsername();
        }

        $this->_log(sprintf('Starting import of file: %s', $path), false, Zend_Log::INFO);
        /** Check the file extension */
        $fileExtension = pathinfo($path, PATHINFO_EXTENSION);
        if ($fileExtension == 'xlsx') {
            $return = $this->importExcelFile($path, $startDate, $userName);
        } elseif ($fileExtension == 'csv') {
            //@todo add file header validation - check mandatory columns
            $return = $this->importCsvFile($path, $startDate, $userName);
        }

        return isset($return) ? $return : true;
    }

    /**
     * Method to import mixmatch rules from csv file
     * @param $path
     * @param $websiteIds
     * @param $startDate
     * @param $userName
     * @return bool
     */
    protected function importCsvFile($path, $startDate, $userName)
    {
        try {
            /**
             * @var $objReader PHPExcel_Reader_Abstract
             * @var $objPHPExcel PHPExcel
             */
            $csvValues = $this->getCsvNameValues($path);
            if (!$csvValues) {
                return false;
            }

            $this->importFileForWebsite($path, $csvValues, $startDate, $userName);

        } catch (Exception $e) {
            $this->_logError('[ERROR] Exception - Failed importing: ' . $e->getMessage());
            fwrite(STDERR, '[ERROR] Exception - Failed importing: ' . $e->getMessage());

            $return = false;
        }

        return isset($return) ? $return : true;
    }

    /**
     * Parse each row from the csv file and return combined array
     * @param $path
     * @return array
     */
    public function getCsvNameValues($path)
    {
        /* Open csv file */
        $file = fopen($path, 'r');
        /* Line counter */
        $lc = 0;
        /* Csv data header and body */
        $rows = [];

        /** Parsing csv file  */
        while (($line = fgetcsv($file, null, $this->_csvDelimiter)) !== false) {
            $lc++;
            /** Get header columns name */
            if ($lc == 1) {
                $tempHeader = [];
                /** @var Dyna_Import_Helper_Data $helperImport */
                $helperImport = $this->getImportHelper();
                $mappingHeader = $helperImport->getMapping('mixMatch', ['required', 'optional']);
                foreach ($line as $column) {
                    if (in_array($column, $mappingHeader)) {
                        $tempHeader[] = $column;
                    }
                }
                //check if match our template
                $checkHeader = $helperImport->checkHeader($tempHeader, 'mixMatch');
                //log messages
                $messages = $helperImport->getMessages();
                $attachments = [];
                foreach ($messages as $key => $msg) {
                    switch ($key) {
                        case 'success':
                        case 'extra':
                        case 'optional':
                            $level = Zend_Log::INFO;
                            break;
                        case 'required':
                        case 'no_type':
                        default:
                            $level = Zend_Log::ERR;
                            break;
                    }
                    //write file log
                    $this->_log($msg, false, $level);
                    if ($key !== 'success' && substr($path, 0, 9) != '/vagrant/') {
                        $attachments[] = ['key' => $key, 'text' => $msg];
                    }
                }
                if (count($attachments)) {
                    $msg = 'While processing file ' . $path . ' we encountered the following:';
                    $this->_helper->postToSlack($msg, $attachments);
                }
                //check if the headers are ok
                if (!$checkHeader) {
                    return false;
                }
                //Set header
                $this->_header = $tempHeader;
                unset($tempHeader);
                continue;
            }
            $this->_totalFileRows++;
            /** Parse each finded row from the csv file */
            if (!$csvLineData = array_combine($this->_header, $line)) {
                $this->_skippedFileRows++;
                $this->_logError("Inconsistency in import file as the first row (columns headers) columns count differs from the current line (" . $lc . ") columns count. Skipping row.");
                continue;
            }

            /** Push csv row */
            $rows[] = $csvLineData;
        }

        /** Return parsed rows */
        return $rows;
    }

    /**
     * Method for import mixmatch rules from excel file
     * @param $path
     * @param $websiteIds
     * @param $startDate
     * @param $userName
     * @return bool
     */
    protected function importExcelFile($path, $startDate, $userName)
    {
        try {
            /**
             * @var $objReader PHPExcel_Reader_Abstract
             * @var $objPHPExcel PHPExcel
             */
            $objReader = PHPExcel_IOFactory::createReaderForFile($path);
            $objReader->setReadDataOnly(true);
            $objPHPExcel = $objReader->load($path);
            $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
            $excelValues = $this->getExcelNameValues($objWorksheet);

            $this->importFileForWebsite($path, $excelValues, $startDate, $userName);

            $objPHPExcel->disconnectWorksheets();
            $objPHPExcel->garbageCollect();
        } catch (PHPExcel_Reader_Exception $e) {
            $this->_logError('[ERROR] Exception - Excel reader failed: ' . $e->getMessage());
            fwrite(STDERR, '[ERROR] Exception - Excel reader failed: ' . $e->getMessage());

            $return = false;
        } catch (Exception $e) {
            $this->_logError('[ERROR] Exception - Failed importing: ' . $e->getMessage());
            fwrite(STDERR, '[ERROR] Exception - Failed importing: ' . $e->getMessage());

            $return = false;
        }

        return isset($return) ? $return : true;
    }

    protected function getExcelNameValues($objWorksheet)
    {
        /** @var PHPExcel_Worksheet $objWorksheet */
        $highestRow = $objWorksheet->getHighestRow();
        $highestColumn = $objWorksheet->getHighestColumn();

        $headingsArray = $objWorksheet->rangeToArray('A1:' . $highestColumn . '1', null, true, true, true);
        $headingsArray = $headingsArray[1];
        $headingsArray = $this->mapOtherColumnsToHeadingsArray($headingsArray);

        $r = -1;
        $namedDataArray = [];
        for ($row = 2; $row <= $highestRow; ++$row) {
            $dataRow = $objWorksheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, null, true, true, true);
            ++$r;
            foreach ($headingsArray as $columnKey => $columnHeading) {
                $namedDataArray[$r][$columnHeading] = $dataRow[$row][$columnKey];
            }
        }

        return $namedDataArray;
    }

    /*
     * Just in case columns headers naming differs from the one that is needed by model
     */
    protected function mapOtherColumnsToHeadingsArray($headingArray)
    {
        return $headingArray;
    }

    /**
     * Perform excel parsing data and insert in database
     * @param $path
     * @param $websiteId
     * @param $excelValues
     * @param $startDate
     * @param $userName
     * @throws PHPExcel_Exception
     * @throws bool
     */
    protected function importFileForWebsite($path, $excelValues, $startDate, $userName)
    {
        $counter = 0;
        $deviceSubscription = new Varien_Object();
        $websiteId = '';
        $cachedWebsitesIds = [];

        foreach ($excelValues as $valuesArray) {
            // skip empty row
            if (!array_filter($valuesArray)) {
                continue;
            }

            $valuesArray['stack'] = $this->stack;
            $ws = new Varien_Object();

            $ws->setData($valuesArray);
            /** @var Mage_Core_Model_Resource_Transaction $transaction */
            $transaction = Mage::getResourceModel('core/transaction');

            $sourceSku = trim($ws->getSourceSku());
            $targetSku = trim($ws->getTargetSku());
            $targetCategory = trim($ws->getTargetCategory());
            $sourceCategory = trim($ws->getSourceCategory());
            $subscriberSegment = $ws->getSubscriberSegment();

            if ($subscriberSegment === Dyna_MixMatch_Model_SubscriberSegment::SUBSCRIBER_SEGMENT_VALUE_ALL) {
                $subscriberSegment = implode(Mage::getSingleton('dyna_mixmatch/subscriberSegment')->getAllValues(), ',');
            } else if ($subscriberSegment == Dyna_MixMatch_Model_SubscriberSegment::SUBSCRIBER_SEGMENT_VALUE_EMPTY) {
                $subscriberSegment = '';
            }

            if ($ws->getEffectiveDate()) {
                $effectiveDate = PHPExcel_Style_NumberFormat::toFormattedString($ws->getEffectiveDate(), PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2);
                $effectiveDate = date("Y-m-d", strtotime($effectiveDate));
            } else {
                $effectiveDate = "";
            }
            if ($ws->getExpirationDate()) {
                $expirationDate = PHPExcel_Style_NumberFormat::toFormattedString($ws->getExpirationDate(), PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2);
                $expirationDate = date("Y-m-d", strtotime($expirationDate));
            } else {
                $expirationDate = "";
            }
            $stream = $ws->getStream();
            $redSalesId = $ws->getRedSalesId();
            $priority = $ws->getPriority();
            $maf = str_replace(',', '.', trim($ws->getMaf()));
            $maf = is_numeric($maf) ? $maf : null;
            $price = str_replace(',', '.', trim($ws->getPrice()));
            $price = is_numeric($price) ? $price : null;
            // if no price and no maf entries skip row
            if (is_null($maf) && is_null($price)) {
                $this->_logError('[ERROR] Skipping row ' . $counter . ' because there is both Price and MAF are NULL on this row');
                $this->_skippedFileRows++;
                continue;
            }

            // get website id based on name
            foreach (Mage::app()->getWebsites() as $website) {
                if ($ws->getChannel() == $website->getName()) {
                    $websiteId = $website->getWebsite_id();
                }
            }
            // if no sku on subscription column, proceed to next column
            if (!$sourceSku && !$sourceCategory) {
                $this->_logError('[ERROR] Skipping row ' . $counter . ' because there is no source SKU and no source Category on this row');
                $this->_skippedFileRows++;
                continue;
            }
            // if no sku on devices row, proceed to next row
            if (!$targetSku && !$targetCategory) {
                $this->_logError('[ERROR] Skipping row ' . $counter . ' because there is no target SKU and no target Category on this row');
                $this->_skippedFileRows++;
                continue;
            }

            if (!$websiteId && $ws['channel']) {
                $channel = 0;
                if (!empty($cachedWebsitesIds[$ws['channel']])) {
                    $channel = $cachedWebsitesIds[$ws['channel']]['website_id'];

                    /** Clear all website data at first load for the current stream and mark it as emptied */
                    if (empty($cachedWebsitesIds[$ws['channel']][$ws['stream']])) {
                        $cachedWebsitesIds[$ws['channel']][$ws['stream']] = $this->clearMixmatchData($channel, $ws['stream']);
                    }
                } else {
                    /** Load website and cache it's id */
                    $channel = Mage::getModel('core/website')->load(strtolower($ws['channel']), 'code')->getId();
                    $cachedWebsitesIds[$ws['channel']]['website_id'] = $channel;

                    /** Clear all website data at first load for the current stream and mark it as emptied */
                    if (empty($cachedWebsitesIds[$ws['channel']][$ws['stream']])) {
                        $cachedWebsitesIds[$ws['channel']][$ws['stream']] = $this->clearMixmatchData($channel, $ws['stream']);
                    }
                }

                if ($channel) {
                    $this->importProduct($targetSku, $sourceSku, $targetCategory, $sourceCategory, $subscriberSegment, $redSalesId, $priority, $deviceSubscription, $transaction, $price, $maf, $startDate, $userName, $channel, $counter, $effectiveDate, $expirationDate, $stream);
                } else {
                    $this->_logError('[ERROR] Skipping row' . $counter . ' because the channel column contains an invalid website code');
                    $this->_skippedFileRows++;
                }
            } else {
                /** Clear all website data at first load for the current stream and mark it as emptied */
                if (empty($cachedWebsitesIds[$websiteId][$ws['stream']])) {
                    $cachedWebsitesIds[$websiteId][$ws['stream']] = $this->clearMixmatchData($websiteId, $ws['stream']);
                }
                $this->importProduct($targetSku, $sourceSku, $targetCategory, $sourceCategory, $subscriberSegment, $redSalesId, $priority, $deviceSubscription, $transaction, $price, $maf, $startDate, $userName, $websiteId, $counter, $effectiveDate, $expirationDate, $stream);
            }

            try {
                $transaction->save();
            } catch (Exception $e) {
                Mage::getSingleton('core/logger')->logException($e);
            }
            unset($transaction);

            $counter++;
        }

        $this->setImportedFileName($path, $websiteId);
    }

    protected function clearMixmatchData($channel, $stream)
    {
        $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
        $tableName = Mage::getResourceSingleton('omnius_mixmatch/price')->getMainTable();
        $connection->delete($tableName, ['website_id = ?' => (int)$channel, 'stream = ?' => $stream]);

        return "rules removed for this website and this channel";
    }

    /**
     * @param $msg
     */
    public function _log($msg, $verbose = false, $level = null)
    {
        if($verbose && !$this->_debug) {
            return;
        }

        $this->_helper->logMsg($msg, false, $level);
    }

    /**
     * @param $msg
     */
    public function _logError($msg)
    {
        $this->_helper->logMsg($msg, true, Zend_Log::ERR);
    }

    protected function importProduct($targetSku, $sourceSku, $targetCategory, $sourceCategory, $subscriberSegment, $redSalesId, $priority, $deviceSubscription, $transaction, $price, $maf, $startDate, $userName, $websiteId, $counter, $effectiveDate, $expirationDate, $stream)
    {
        try {
            if (!empty($targetSku) && empty($targetCategory) && !$this->getProductIdBySku($targetSku)) {
                $this->_logError(sprintf('[ERROR] Target product is not found: %s', $targetSku));
                $this->_skippedFileRows++;
                return;
            }
            if (!empty($sourceSku) && empty($sourceCategory) && !$this->getProductIdBySku($sourceSku)) {
                $this->_logError(sprintf('[ERROR] Source product is not found: %s', $sourceSku));
                $this->_skippedFileRows++;
                return;
            }

            if (trim($deviceSubscription->getValue())
                && $this->getProductIdBySku($deviceSubscription->getValue())
            ) {
                $transaction->addObject(Mage::getModel('omnius_mixmatch/price')
                    ->setTargetSku($targetSku)
                    ->setSourceSku($sourceSku)
                    ->setPriority($priority)
                    ->setDeviceSubscriptionSku($deviceSubscription->getValue())
                    ->setPrice($price)
                    ->setUploadedAt($startDate)
                    ->setUploadedBy($userName)
                    ->setStack($this->stack)
                    ->setWebsiteId($websiteId));
                /* $this->_log('Setting target ' . $targetSku . ' with source ' . $sourceSku . ' to price '
                     . $price . ' for website ' . $websiteId . ' using row ' . $counter);*/
                $this->_log(
                    sprintf(
                        'Setting device %s with subscription %s and subscription device %s to price %s for website %s using row %d',
                        $targetSku,
                        $sourceSku,
                        $deviceSubscription->getValue(),
                        $price,
                        $websiteId,
                        $counter
                    ), Zend_Log::INFO
                );
            } else {
                $transaction->addObject(Mage::getModel('omnius_mixmatch/price')
                    ->setTargetSku($targetSku)
                    ->setSourceSku($sourceSku)
                    ->setTargetCategory($targetCategory)
                    ->setSourceCategory($sourceCategory)
                    ->setRedSalesId($redSalesId)
                    ->setPriority($priority)
                    ->setSubscriberSegment($subscriberSegment)
                    ->setPrice($price)
                    ->setMaf($maf)
                    ->setUploadedAt($startDate)
                    ->setUploadedBy($userName)
                    ->setWebsiteId($websiteId)
                    ->setEffectiveDate($effectiveDate)
                    ->setExpirationDate($expirationDate)
                    ->setStack($this->stack)
                    ->setStream($stream));

                /*  $this->_log('Setting targetSku <' . $targetSku . '>, sourceSku <' . $sourceSku . '>, targetCategory <' . $targetCategory . '>, sourceCategory <' . $sourceCategory .
                      '>, subscriberSegment ' . $subscriberSegment . ', redSalesId ' . $redSalesId . ' to price ' . $price . ' for website ' . $websiteId . ' using row ' . $counter);*/
            }
        } catch (Exception $e) {
            Mage::getSingleton('core/logger')->logException($e);
        }
    }

    /**
     * @param $sku
     * @return int
     */
    protected function getProductIdBySku($sku)
    {
        if (!isset($this->_products[$sku])) {
            return $this->_products[$sku] = Mage::getModel("catalog/product")->getIdBySku($sku);
        }

        return $this->_products[$sku];
    }

    protected function setImportedFileName($path, $websiteId)
    {
        $config = new Mage_Core_Model_Config();
        $config->saveConfig(self::CONFIG_PATH, $path, 'stores', Mage::app()->getWebsite($websiteId)->getDefaultStore()->getId());
        Mage::getConfig()->cleanCache();
    }

    /**
     * @param array $skus
     * @return array
     */
    protected function getProductCollection(array $skus = [])
    {
        $collection = Mage::getResourceSingleton('catalog/product_collection')
            ->addAttributeToFilter('sku', ['in' => array_unique($skus)])
            ->load();
        $ids = [];
        foreach ($collection->getItems() as $item) {
            $ids[$item->getSku()] = $item->getId();
        }

        return $ids;
    }

    /**
     * @return Mage_Core_Helper_Abstract|null
     */
    protected function getImportHelper()
    {
        /** @var Dyna_Import_Helper_Data _helper */
        return $this->_helper == null ? Mage::helper("dyna_import/data") : $this->_helper;
    }
    public function setStack($stack) {
        return $this->stack = $stack;
    }
}
