<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Observer
 */
class Dyna_MixMatch_Model_Observer extends Mage_Core_Model_Abstract
{
    const BATCH_SIZE = 500;

    protected $skuToPackageTypeMapping = [];

    /**
     * @param $observer
     */
    public function calculateItemPrice($observer)
    {
        /** @var Dyna_Checkout_Model_Sales_Quote_Item $item */
        $item = $observer->getQuoteItem();

        // Ensure we have the parent item, if it has one
        $item = ($item->getParentItem() ? $item->getParentItem() : $item);

        /** Ignore footnote products */
        if($item->getProduct()->getFootnoteId()) {
            return;
        }

        // shipping fee price is registered in cart controller
        if($item->getSku() == Dyna_Catalog_Model_Type::MOBILE_SHIPPING_SKU) {
            $this->_updatePrice($item, Mage::registry(Dyna_Catalog_Model_Type::MOBILE_SHIPPING_SKU));
            return;
        }

        // Check if there is a special price to be set on item from a service call response
        if ($item->getProduct()->getUseServiceValue() && ($serviceName = $item->getProduct()->getServiceName()) && ($serviceXpath = $item->getProduct()->getServiceXpath())) {
            /** @var Dyna_Package_Model_Package $package */
            if (($package = $item->getQuote()->getCartPackage($item->getPackageId())) && $servicePrice = $package->getServiceValue($serviceName, $serviceXpath)) {
                // enable super mode on the product.
                $item->getProduct()->setIsSuperMode(true);
                //Set the custom price
                $item->setCustomPrice($servicePrice)
                    ->setOriginalCustomPrice($servicePrice);
            }
        }

        $this->_updateItemPrices($item);
    }

    /**
     * Recalculates device prices when a subscription is removed from the cart
     *
     * @param $observer
     */
    public function recalculatePrices($observer)
    {
        /** @var Mage_Sales_Model_Quote_Item $item */
        $item = $observer->getQuoteItem();

        // if item is a subscription, search for devices and update prices
        $this->_updateItemPrices($item);
    }

    /**
     * Index MixMatches at the end with highest priority
     */
    public function indexMixMatches($observer)
    {
        //DE logic doesn't add allowed rules for mixmatch products
    }

    /**
     * Overwrite the price of the added device (item) if there is a subscription in the cart
     * @param Dyna_Checkout_Model_Sales_Quote_Item $item
     * @return $this
     */
    protected function _updateItemPrices($item)
    {
        $quoteItems = $item->getQuote()->getPackageItems($item->getPackageId());
        foreach($quoteItems as $quoteItem) {
            // Check if there is a special price to be set on item from a service call response
            if ($quoteItem->getProduct()->getUseServiceValue() && ($serviceName = $quoteItem->getProduct()->getServiceName()) && ($serviceXpath = $quoteItem->getProduct()->getServiceXpath())) {
                /** @var Dyna_Package_Model_Package $package */
                if (($package = $quoteItem->getQuote()->getCartPackage($quoteItem->getPackageId())) && $servicePrice = $package->getServiceValue($serviceName, $serviceXpath)) {
                    // enable super mode on the product.
                    $quoteItem->getProduct()->setIsSuperMode(true);
                    //Set the custom price
                    $quoteItem->setCustomPrice($servicePrice)
                        ->setOriginalCustomPrice($servicePrice);

                    continue;
                }
            }

            $nonStandardRatesCacheKey = sprintf('non_standard_rates_%s_%s',
                $addressId = Mage::getSingleton('dyna_address/storage')->getAddressId(),
                $quoteItem->getProductId()
            );

            if ($cachedPrices = Mage::getSingleton('dyna_cache/cache')->load($nonStandardRatesCacheKey)) {
                $nonStandardRates = unserialize($cachedPrices);

                if ($nonStandardRates['price_with_tax'] !== null) {
                    $this->_updatePrice($quoteItem, (float)$nonStandardRates['price_with_tax']);
                    $quoteItem->setHasNonStandardRate(true);
                } else {
                    $this->_updatePrice($quoteItem, $this->getPricing()->getItemPrice($quoteItem));
                }
                if ($nonStandardRates['maf_with_tax'] !== null) {
                    $this->_updateMaf($quoteItem, (float)$nonStandardRates['maf_with_tax']);
                    $quoteItem->setHasNonStandardRate(true);
                } else {
                    $this->_updateMaf($quoteItem, $this->getPricing()->getItemMaf($quoteItem));
                }
            } else {
                $this->_updatePrice($quoteItem, $this->getPricing()->getItemPrice($quoteItem));
                $this->_updateMaf($quoteItem, $this->getPricing()->getItemMaf($quoteItem));
            }
        }

        $nonStandardRatesCacheKey = sprintf('non_standard_rates_%s_%s',
            $addressId = Mage::getSingleton('dyna_address/storage')->getAddressId(),
            $item->getProductId()
        );

        if ($cachedNonStandardRates = Mage::getSingleton('dyna_cache/cache')->load($nonStandardRatesCacheKey)) {
            $nonStandardRates = unserialize($cachedNonStandardRates);

            if ($nonStandardRates['price_with_tax'] !== null) {
                $this->_updatePrice($item, (float)$nonStandardRates['price_with_tax']);
                $item->setHasNonStandardRate(true);
            } else {
                $this->_updatePrice($item, $this->getPricing()->getItemPrice($item));
            }
            if ($nonStandardRates['maf_with_tax'] !== null) {
                $this->_updateMaf($item, (float)$nonStandardRates['maf_with_tax']);
                $item->setHasNonStandardRate(true);
            } else {
                $this->_updateMaf($item, $this->getPricing()->getItemMaf($item));
            }
        }

        return $this;
    }

    /**
     * @param $item
     * @param $price
     */
    protected function _updatePrice($item, $price)
    {
        if ($item instanceof Mage_Sales_Model_Quote_Item) {
            // if item is shipping fee, setting direct price and product price
            if ($item->getSku() == Dyna_Catalog_Model_Type::MOBILE_SHIPPING_SKU) {
                $item->setPrice($price);
                $item->getProduct()->setPrice($price);
                $item->getProduct()->setCustomPrice($price);
           } else {
                // enable super mode on the product.
                $item->getProduct()->setIsSuperMode(true);
                //Set the custom price
                $item->setCustomPrice($price)
                    ->setOriginalCustomPrice($price);
            }
        }
    }

    /**
     * @param $item
     * @param $maf
     */
    protected function _updateMaf($item, $maf)
    {
        if ($item instanceof Mage_Sales_Model_Quote_Item) {
            if ($item->isContract()) {
                return;
            }
            $item->getProduct()->setIsSuperMode(true);
            //Set the custom maf
            $item->setCustomMaf($maf)
                ->setOriginalCustomMaf($maf);
        }
    }

    /**
     * @return Dyna_MixMatch_Model_Pricing
     */
    protected function getPricing()
    {
        return Mage::getSingleton('omnius_mixmatch/pricing');
    }

    public function addLastExportButtonInAdmin($observer)
    {
        /** @var Omnius_MixMatch_Block_Adminhtml_Rules $rulesForm */
        $rulesForm = $observer->getButtons();

        $rulesForm->addButton('lastexport', [
            'label' => Mage::helper('omnius_mixmatch')->__('Download last uploaded Mixmatch'),
            'onclick' => 'setLocation(\'' . $rulesForm->getLastExportUrl() . '\')',
            'class' => 'go',
        ]);

        /** @todo - export needs to be adjusted to VDFDE flow */
        $rulesForm->removeButton('export');
    }
}
