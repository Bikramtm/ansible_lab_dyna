<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** Class  Dyna_MixMatch_Model_Pricing */
class Dyna_MixMatch_Model_Pricing extends Omnius_MixMatch_Model_Pricing
{
    protected $salesId = false;

    /**
     * @param $value
     * @return $this
     */
    public function setSalesId($value)
    {
        $this->salesId = $value;

        return $this;
    }

    /**
     * Overwrite the price of the added device (item) if there is a subscription in the cart
     * @param $item
     * @return bool
     */
    public function getItemPrice(Mage_Sales_Model_Quote_Item $item)
    {

        $subscription = null;

        $siblings = $item->getQuote()->getPackageItems($item->getPackageId());

        foreach ($siblings as $sibling) {
            // check if there is a subscription
            if (!$sibling->getProduct()->is(Dyna_Catalog_Model_Type::$subscriptions)
                || $sibling->getSku() == $item->getSku()
            ) {
                continue;
            }

            $subscription = $sibling->getSku();
        }

        if ($subscription && !$item->isContract()) {
            $collection = $this->getMixMatchCollection($item->getSku(), $subscription);
            if ($collection) {
                $mixmatch = $collection->getFirstItem();
            }
        }

        if (isset($mixmatch) && $mixmatch->getId() && $mixmatch->getPrice()) {
            $price = $mixmatch->getPrice();
        } else {
            // set one time product price 0 for install base products
            if ($item->getIsContract()) {
                $item->getProduct()->setPrice(0);
            }
            $price = $item->getProduct()->getPrice() ?: null;
        }

        return $price;
    }

    /**
     * Determine item maf if there is a mixmatch between subscription and current product
     * @param Mage_Sales_Model_Quote_Item $item current cart item
     * @return float item maf
     */
    public function getItemMaf(Mage_Sales_Model_Quote_Item $item)
    {
        $subscription = null;

        $siblings = $item->getQuote()->getPackageItems($item->getPackageId());

        foreach ($siblings as $sibling) {
            // check if there is a subscription
            if (!$sibling->getProduct()->is(Dyna_Catalog_Model_Type::$subscriptions)
                || $sibling->getSku() == $item->getSku()
            ) {
                continue;
            }

            $subscription = $sibling->getSku();
        }

        if ($subscription) {
            $collection = $this->getMixMatchCollection($item->getSku(), $subscription);
            if ($collection) {
                $mixmatch = $collection->getFirstItem();
            }
        }

        if (isset($mixmatch) && $mixmatch->getId() && $mixmatch->getMaf()) {
            $maf = $mixmatch->getMaf();
        } else {
            $maf = $item->getProduct()->getMaf();
        }

        return $maf;
    }

    /**
     * @param string $targetSku
     * @param string $sourceSku
     * @param int $websiteId
     * @return Omnius_MixMatch_Model_Resource_Price_Collection|Varien_Data_Collection
     */
    public function getMixMatchCollection($targetSku, $sourceSku, $loanSku = null, $websiteId = null)
    {
        if ($sourceSku == $targetSku) {
            return null;
        }

        if ($websiteId == null) {
            $websiteId = Mage::app()->getStore()->getWebsiteId();
        }

        if ($this->salesId === false) {
            /** @var Dyna_Checkout_Model_Sales_Quote $currentQuote */
            $currentQuote = Mage::getSingleton('checkout/cart')->getQuote();
            $redSalesId = $currentQuote->getSalesId();
        } else {
            $redSalesId = $this->salesId;
        }

        $key = serialize([
                __METHOD__,
                $targetSku,
                $sourceSku,
                $websiteId,
                $redSalesId,
            ]);
        if ($result = unserialize($this->getCache()->load($key))) {
            return $result;
        } else {
            /** @var Omnius_MixMatch_Model_Resource_Price_Collection $collection */
            $collection = Mage::getModel('dyna_mixmatch/priceIndex')->getCollection()
                ->addFieldToSelect(['source_sku', 'target_sku', 'price', 'maf'])
                ->addFieldToFilter('website_id', array('eq' => $websiteId))
                ->addFieldToFilter('effective_date', array('to' => date("Y-m-d H:i:s"), 'date' => true))
                ->addFieldToFilter('expiration_date', array('from' => date("Y-m-d H:i:s"), 'date' => true))
                ->addFieldToFilter('subscriber_segment', array('null' => true));

            if (!empty($redSalesId)) {
                $collection->addFieldToFilter(
                    array('red_sales_id', 'red_sales_id'),
                    array(array('eq' => $redSalesId), array('null' => true))
                );
            } else {
                $collection->addFieldToFilter('red_sales_id', array('null' => true));
            }

            if ($sourceSku !== null) {
                $collection->addFieldToFilter('source_sku', $sourceSku);
            }

            if ($targetSku !== null) {
                $collection->addFieldToFilter('target_sku', $targetSku);
            }

            $collection->getSelect()->order("priority DESC");

            $result = new Varien_Data_Collection();
            foreach ($collection->getItems() as $item) {
                $result->addItem($item);
            }

            $result->setOrder('price', Zend_Db_Select::SQL_ASC);

            $this->getCache()->save(serialize($result), $key, [Dyna_Cache_Model_Cache::PRODUCT_TAG],
                $this->getCache()->getTtl());

            return $result;
        }
    }
}
