<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_MixMatch_Block_Adminhtml_Rules_Edit_Tab_Form
 */
class Dyna_MixMatch_Block_Adminhtml_Rules_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset("dyna_mixmatch_form", array("legend" => Mage::helper("dyna_mixmatch")->__("Item information")));

        $fieldset->addField("source_sku", "text", array(
            "label" => Mage::helper("dyna_mixmatch")->__("Source SKU"),
            "class" => "",
            "required" => false,
            "name" => "source_sku",
        ));

        $fieldset->addField("target_sku", "text", array(
            "label" => Mage::helper("dyna_mixmatch")->__("Target SKU"),
            "class" => "",
            "required" => false,
            "name" => "target_sku",
        ));

        $fieldset->addField("source_category", "text", array(
            "label" => Mage::helper("dyna_mixmatch")->__("Source Category"),
            "class" => "",
            "required" => false,
            "name" => "source_category",
        ));

        $fieldset->addField("target_category", "text", array(
            "label" => Mage::helper("dyna_mixmatch")->__("Target Category"),
            "class" => "",
            "required" => false,
            "name" => "target_category",
        ));

        $fieldset->addField("subscriber_segment", "multiselect", array(
            "label" => Mage::helper("dyna_mixmatch")->__("Subscriber segment"),
            'values' => Mage::getSingleton('dyna_mixmatch/subscriberSegment')->getValuesForForm(),
            "class" => "",
            "required" => false,
            "name" => "subscriber_segment",
        ));

        $fieldset->addField("red_sales_id", "text", array(
            "label" => Mage::helper("dyna_mixmatch")->__("Red Sales Id"),
            "class" => "",
            "required" => false,
            "name" => "red_sales_id",
        ));

        $fieldset->addField("price", "text", array(
            "label" => Mage::helper("dyna_mixmatch")->__("Price"),
            "required" => false,
            "name" => "price",
        ));

        $fieldset->addField("maf", "text", array(
            "label" => Mage::helper("dyna_mixmatch")->__("MAF"),
            "required" => false,
            "name" => "maf",
        ));

        $fieldset->addField('website_id', 'multiselect', array(
            'label' => Mage::helper('dyna_mixmatch')->__('Website'),
            'values' => Mage::getSingleton('adminhtml/system_store')->getWebsiteValuesForForm(),
            'name' => 'website_id',
            "class" => "required-entry",
            "required" => true,
        ));

        $fieldset->addField('effective_date', 'date', array(
            'name' => 'effective_date',
            'label' => Mage::helper('dyna_mixmatch')->__('Effective Date'),
            'title' => Mage::helper('dyna_mixmatch')->__('Effective Date'),
            'image' => $this->getSkinUrl('images/grid-cal.gif'),
            'input_format' => Varien_Date::DATE_INTERNAL_FORMAT,
            'format' => Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT),
            'value' => ''
        ));

        $fieldset->addField('expiration_date', 'date', array(
            'name' => 'expiration_date',
            'label' => Mage::helper('dyna_mixmatch')->__('Expiration Date'),
            'title' => Mage::helper('dyna_mixmatch')->__('Effective Date'),
            'image' => $this->getSkinUrl('images/grid-cal.gif'),
            'input_format' => Varien_Date::DATE_INTERNAL_FORMAT,
            'format' => Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT),
            'value' => ''
        ));

        if (Mage::getSingleton("adminhtml/session")->getDynaMixMatchData()) {
            $form->setValues(Mage::getSingleton("adminhtml/session")->getDynaMixMatchData());
            Mage::getSingleton("adminhtml/session")->setDynaMixMatchData(null);
        } elseif (Mage::registry("dyna_mixmatch_data")) {
            $form->setValues(Mage::registry("dyna_mixmatch_data")->getData());
        }

        return parent::_prepareForm();
    }
}
