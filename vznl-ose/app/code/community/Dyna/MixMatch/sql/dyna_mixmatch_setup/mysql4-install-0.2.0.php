<?php
/** @var Mage_Core_Model_Resource_Setup $installer */
// Added new columns to support discount on Addon price and categories
$installer = $this;
$installer->startSetup();

/** @var Varien_Db_Adapter_Interface $connection */
$connection = $installer->getConnection();
$connection->addColumn($this->getTable('catalog_mixmatch'), 'subscriber_segment', 'varchar(255) null after subscription_sku');
$connection->addColumn($this->getTable('catalog_mixmatch'), 'void', 'varchar(255) null after subscriber_segment');
$connection->addColumn($this->getTable('catalog_mixmatch'), 'source_category', 'varchar(255) null after subscription_sku');
$connection->addColumn($this->getTable('catalog_mixmatch'), 'target_category', 'varchar(255) null after device_sku');
$connection->addColumn($this->getTable('catalog_mixmatch'), 'maf', 'decimal(12,4) null after price');

if ($connection->tableColumnExists($this->getTable('catalog_mixmatch'), 'subscription_sku')) {
    $connection->changeColumn($this->getTable('catalog_mixmatch'), 'subscription_sku', 'source_sku', 'varchar(64) null');
}
if ($connection->tableColumnExists($this->getTable('catalog_mixmatch'), 'device_sku')) {
    $connection->changeColumn($this->getTable('catalog_mixmatch'), 'device_sku', 'target_sku', 'varchar(64) null');
}

$installer->endSetup();
