<?php
/**
 * @category   Dyna
 * @package    Dyna_IpSecurity
 */

class Dyna_IpSecurity_Model_Observer
{
    protected $_redirectPage = null;
    protected $_redirectBlank = null;
    protected $_rawAllowIpData = null;
    protected $_rawBlockIpData = null;
    protected $_rawExceptIpData = null;
    protected $_eventEmail = "";
    protected $_sendEmails = 0;
    protected $_emailTemplate = 0;
    protected $_emailIdentity = null;
    protected $_storeType = null;
    protected $_lastFoundIp = null;
    protected $_isFrontend = false;
    protected $_isDownloader = false;
    protected $_alwaysNotify = false;
    protected $_environment = '';

    /**
     * If loading Frontend
     *
     * @param $observer
     */
    public function onLoadingFrontend($observer)
    {
        $this->_environment = 'frontend';
        $this->_readFrontendConfig();
        $this->_processIpCheck($observer);
    }

    /**
     * If loading Admin
     *
     * @param $observer
     */
    public function onLoadingAdmin($observer)
    {
        $this->_environment = 'admin';
        $this->_readAdminConfig();
        $this->_processIpCheck($observer);
    }

    /**
     * On failed login to Admin
     *
     * @param $observer
     */
    public function onAdminLoginFailed($observer)
    {
    }

    /**
     * On loading Downloader
     *
     * @param $observer
     */
    public function onLoadingDownloader($observer)
    {
        //only in downloader exists Maged_Controller class
        if (class_exists("Maged_Controller", false)) {
            $this->_readDownloaderConfig();
            $this->_processIpCheck($observer);
        }
    }

    /**
     * Reading configuration for Frontend
     */
    protected function _readFrontendConfig()
    {
        $this->_redirectPage = $this->trimTrailingSlashes(
            Mage::getStoreConfig('dynaipsecurity/ipsecurityfront/redirect_page'));
        $this->_redirectBlank = Mage::getStoreConfig('dynaipsecurity/ipsecurityfront/redirect_blank');
        $this->_rawAllowIpData = Mage::getStoreConfig('dynaipsecurity/ipsecurityfront/allow');
        $this->_rawBlockIpData = Mage::getStoreConfig('dynaipsecurity/ipsecurityfront/block');
        $this->_eventEmail = Mage::getStoreConfig('dynaipsecurity/ipsecurityfront/email_event');
        $this->_sendEmails = Mage::getStoreConfig('dynaipsecurity/ipsecurityfront/send_email_trigger');
        $this->_emailTemplate = Mage::getStoreConfig('dynaipsecurity/ipsecurityfront/email_template');
        $this->_emailIdentity = Mage::getStoreConfig('dynaipsecurity/ipsecurityfront/email_identity');
        $this->_alwaysNotify = Mage::getStoreConfig('dynaipsecurity/ipsecurityfront/email_always');
        $this->_rawExceptIpData = Mage::getStoreConfig('dynaipsecurity/ipsecuritymaintetance/except');

        $this->_storeType = Mage::helper("catalog")->__("Frontend");
        $this->_isFrontend = true;
    }

    /**
     * Reading configuration for Admin
     */
    protected function _readAdminConfig()
    {
        $this->_redirectPage = $this->trimTrailingSlashes(
            Mage::getStoreConfig('dynaipsecurity/ipsecurityadmin/redirect_page'));
        $this->_redirectBlank = Mage::getStoreConfig('dynaipsecurity/ipsecurityadmin/redirect_blank');
        $this->_rawAllowIpData = Mage::getStoreConfig('dynaipsecurity/ipsecurityadmin/allow');
        $this->_rawBlockIpData = Mage::getStoreConfig('dynaipsecurity/ipsecurityadmin/block');
        $this->_eventEmail = Mage::getStoreConfig('dynaipsecurity/ipsecurityadmin/email_event');
        $this->_emailTemplate = Mage::getStoreConfig('dynaipsecurity/ipsecurityadmin/email_template');
        $this->_emailIdentity = Mage::getStoreConfig('dynaipsecurity/ipsecurityadmin/email_identity');
        $this->_alwaysNotify = Mage::getStoreConfig('dynaipsecurity/ipsecurityadmin/alwaysnotify');

        $this->_storeType = Mage::helper("core")->__("Admin");
        $this->_isFrontend = false;
    }

    /**
     * Read configuration for Downloader (used Admin config)
     */
    protected function _readDownloaderConfig()
    {
        $this->_readAdminConfig();
        $this->_storeType = Mage::helper("dynaipsecurity")->__("Downloader");
        $this->_isDownloader = true;

        //$this->_redirectBlank = true;
    }

    /**
     * Get current Scope (frontend, admin, downloader)
     *
     * @return string
     */
    protected function _getScopeName()
    {
        if ($this->_isFrontend) {
            $scope = 'frontend';
        } elseif ($this->_isDownloader) {
            $scope = 'downloader';
        } else {
            $scope = 'admin';
        }

        return $scope;
    }

    /**
     * Checking current ip for rules
     *
     * @param $observer
     * @return Dyna_IpSecurity_Model_Observer
     */
    protected function _processIpCheck($observer)
    {
        $currentIp = $this->getCurrentIp();
        $allowIps = $this->_ipTextToArray($this->_rawAllowIpData);
        $blockIps = $this->_ipTextToArray($this->_rawBlockIpData);

        $allow = $this->isIpAllowed($currentIp, $allowIps, $blockIps);
        $this->_processAllowDeny($allow, $currentIp);

        return $this;
    }

    /**
     * Check IP for allow/deny rules
     *
     * @param $currentIp string
     * @param $allowIps array
     * @param $blockIps array
     * @return bool
     */
    public function isIpAllowed($currentIp, $allowIps, $blockIps)
    {
        $allow = true;

        # look for allowed
        if ($allowIps) {
            # block all except allowed
            $allow = false;

            # are there any allowed ips
            if ($this->isIpInList($currentIp, $allowIps)) {
                $allow = true;
            }
        }

        # look for blocked
        if ($blockIps) {
            # are there any blocked ips
            if ($this->isIpInList($currentIp, $blockIps)) {
                $allow = false;
            }
        }
        return $allow;
    }

    /**
     * Redirect denied users to block page or show maintenance page to visitor
     *
     * @param $allow boolean
     * @param $currentIp string
     */
    protected function _processAllowDeny($allow, $currentIp)
    {
        if(!$allow){
            $this->logDeny();
        }
        //TODO: Refactoring?
        $currentPage = $this->trimTrailingSlashes(Mage::helper('core/url')->getCurrentUrl());
        // searching for CMS page storeId
        // if we don't do it - we have loop in redirect with setting Add Store Code to Urls = Yes
        // (block access to admin redirects to admin)
        $pageStoreId = $this->getPageStoreId();
        $this->_redirectPage = $this->trimTrailingSlashes(Mage::app()->getStore($pageStoreId)->getBaseUrl())
            . "/" . $this->_redirectPage;
        $scope = $this->_getScopeName();

        if (!strlen($this->_redirectPage) && !$this->_isDownloader) {
            $this->_redirectPage = $this->trimTrailingSlashes(Mage::getUrl('no-route'));
        }

        if ($this->_redirectBlank == 1 && !$allow) {
            header("HTTP/1.1 403 Forbidden");
            header("Status: 403 Forbidden");
            header("Content-type: text/html");
            $needToNotify = $this->saveToLog(array('blocked_from' => $scope, 'blocked_ip' => $currentIp));
            if (($this->_alwaysNotify) || $needToNotify) {
                $this->_send();
            }
            exit("Access denied for IP:<b> " . $currentIp . "</b>");
        }

        if ($this->trimTrailingSlashes($currentPage) != $this->trimTrailingSlashes($this->_redirectPage) && !$allow) {
            header('Location: ' . $this->_redirectPage);
            $needToNotify = $this->saveToLog(array('blocked_from' => $scope, 'blocked_ip' => $currentIp));
            if (($this->_alwaysNotify) || $needToNotify) {
                $this->_send();
            }
            exit();
        }

        $exceptIps = $this->_ipTextToArray($this->_rawExceptIpData);
        $isMaintenanceMode = Mage::getStoreConfig('dynaipsecurity/ipsecuritymaintetance/enabled');
        if (($isMaintenanceMode) && ($this->_isFrontend)) {
            $doNotLoadSite = true;
            # look for except
            if ($exceptIps) {
                # are there any except ips
                if ($this->isIpInList($currentIp, $exceptIps)) {
                    Mage::app()->getResponse()->appendBody(
                        html_entity_decode(
                            Mage::getStoreConfig('dynaipsecurity/ipsecuritymaintetance/remindermessage'),
                            ENT_QUOTES,
                            "utf-8"
                        )
                    );
                    $doNotLoadSite = false;
                }
            }

            if ($doNotLoadSite) {
                header('HTTP/1.1 503 Service Temporarily Unavailable');
                header('Status: 503 Service Temporarily Unavailable');
                header('Retry-After: 7200'); // in seconds
                print html_entity_decode(
                    Mage::getStoreConfig('dynaipsecurity/ipsecuritymaintetance/message'),
                    ENT_QUOTES,
                    "utf-8"
                );
                exit();
            }

        }
    }


    /**
     * Get store id of target redirect cms page
     *
     * @return int
     */
    public function getPageStoreId()
    {
        $stores = array();
        $pageStoreIds = array();

        foreach (Mage::app()->getStores() as $store) {
            /* @var $store Mage_Core_Model_Store */
            $stores[] = $store->getId();
            $pageId = Mage::getModel('cms/page')->checkIdentifier($this->_redirectPage, $store->getId());
            if ($pageId === false) {
                continue;
            }
            $pageStoreIds = Mage::getResourceModel('cms/page')->lookupStoreIds($pageId);
            if (count($pageStoreIds)) { // found page
                break;
            }
        }

        if (!count($pageStoreIds)) { // no found in any store
            $pageStoreIds[] = 0;
        }
        //default
        $pageStoreId = 0;
        foreach ($pageStoreIds as $pageStoreId) {
            if ($pageStoreId > 0) {
                break;
            }
        }

        if ($pageStoreId == 0) {
            // $pageStoreId = $stores[0]; // Removed condition that was setting the default store
            $pageStoreId = Mage::app()->getStore()->getId(); // Added condition to get current store
            return $pageStoreId; // first available store
        }
        return $pageStoreId;
    }


    /**
     * Convert IP range as string to array with first and last IP of range
     *
     * @param $ipRange string
     * @return array[first,last]
     */
    protected function _convertIpStringToIpRange($ipRange)
    {
        $ip = explode("|", $ipRange);
        $ip = trim($ip[0]);
        $simpleRange = explode("-", $ip);
        //for xx.xx.xx.xx-yy.yy.yy.yy
        if (count($simpleRange) == 2) {
            $comparableIpRange = array(
                "first" => $this->_convertIpToComparableString($simpleRange[0]),
                "last" => $this->_convertIpToComparableString($simpleRange[1]));
            return $comparableIpRange;
        }
        //for xx.xx.xx.*
        if (strpos($ip, "*") !== false) {
            $fromIp = str_replace("*", "0", $ip);
            $toIp = str_replace("*", "255", $ip);
            $comparableIpRange = array(
                "first" => $this->_convertIpToComparableString($fromIp),
                "last" => $this->_convertIpToComparableString($toIp));
            return $comparableIpRange;
        }
        //for xx.xx.xx.xx/yy
        $maskRange = explode("/", $ip);
        if (count($maskRange) == 2) {
            $maskMoves = 32 - $maskRange[1];
            $mask = (0xFFFFFFFF >> $maskMoves) << $maskMoves;
            $subMask = 0;
            for ($maskDigits = 0; $maskDigits < $maskMoves; $maskDigits++) {
                $subMask = ($subMask << 1) | 1;
            }
            $fromIp = ip2long($maskRange[0]) & $mask;
            $toIp = long2ip($fromIp | $subMask);
            $fromIp = long2ip($fromIp);
            $comparableIpRange = array(
                "first" => $this->_convertIpToComparableString($fromIp),
                "last" => $this->_convertIpToComparableString($toIp));
            return $comparableIpRange;
        }

        $comparableIpRange = array(
            "first" => $this->_convertIpToComparableString($ip),
            "last" => $this->_convertIpToComparableString($ip)
        );

        return $comparableIpRange;

    }

    /**
     * Convert IP address (x.xx.xxx.xx) to easy comparable string (xxx.xxx.xxx.xxx)
     *
     * @param $ip string
     * @return string
     * @throws Exception
     */
    protected function _convertIpToComparableString($ip)
    {
        $partsOfIp = explode(".", trim($ip));
        if (count($partsOfIp) != 4) {
            throw new Exception("Incorrect IP format: " . $ip);
        }
        $comparableIpString = sprintf(
            "%03d%03d%03d%03d",
            $partsOfIp[0],
            $partsOfIp[1],
            $partsOfIp[2],
            $partsOfIp[3]
        );
        return $comparableIpString;

    }

    /**
     * Is ip in list of IP rules
     *
     * @param $searchIp string
     * @param $ipRulesList array
     * @return bool
     */
    public function isIpInList($searchIp, $ipRulesList)
    {
        $searchIpComparable = $this->_convertIpToComparableString($searchIp);
        if (count($ipRulesList) > 0) {
            foreach ($ipRulesList as $ipRule) {
                $ip = explode("|", $ipRule);
                $ip = trim($ip[0]);
                try {
                    $ipRange = $this->_convertIpStringToIpRange($ip);
                    if (count($ipRange) == 2) {
                        $ipFrom = $ipRange["first"];
                        $ipTo = $ipRange["last"];
                        if ((strcmp($ipFrom, $searchIpComparable) <= 0) &&
                            (strcmp($searchIpComparable, $ipTo) <= 0)
                        ) {
                            $this->_lastFoundIp = $ipRule;
                            return true;
                        }
                    }
                } catch (Exception $e) {
                    Mage::log($e->getMessage());
                }
                //}
            }
        }
        return false;
    }

    /**
     * Trim trailing slashes, except single "/"
     *
     * @param $str string
     * @return string
     */
    protected function trimTrailingSlashes($str)
    {
        $str = trim($str);
        return $str == '/' ? $str : rtrim($str, '/');
    }

    /**
     * Send to admin information about IP blocking
     */
    protected function _send()
    {
        if(!$this->_sendEmails){
            return false;
        }
        $sendResult = false;
        if (!$this->_eventEmail) {
            return $sendResult;
        }
        $currentIp = $this->getCurrentIp();
        //$storeId = 0; //admin

        $recipients = explode(",", $this->_eventEmail);

        /* @var $emailTemplate Mage_Core_Model_Email_Template */
        $emailTemplate = Mage::getModel('core/email_template');
        foreach ($recipients as $recipient) {
            $sendResult = $emailTemplate->setDesignConfig(array('area' => 'backend'))
                ->sendTransactional(
                $this->_emailTemplate,
                $this->_emailIdentity,
                trim($recipient),
                trim($recipient),
                array(
                    'ip' => $currentIp,
                    'ip_rule' => Mage::helper('dynaipsecurity')->__($this->getLastBlockRule()), // TODO: translation
                    'date' => Mage::app()->getLocale()->date(strtotime('now'), null, null, false)->toString(Mage::app()->getLocale()->getDateTimeFormat('medium')),
                    'storetype' => $this->_storeType,
                    'info' => base64_encode(serialize(array($this->_rawAllowIpData, $this->_rawBlockIpData))),
                )
            );
        }
        return $sendResult;
    }

    /**
     * Return block rule
     *
     * @return string
     */
    public function getLastBlockRule()
    {
        $lastBlockRule = 'Not in allowed list';
        if (!is_null($this->_lastFoundIp)) {
            $lastBlockRule = $this->_lastFoundIp;
        }
        return $lastBlockRule;
    }

    /**
     * Get IP of current client
     *
     * @return string
     */
    public function getCurrentIp()
    {
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) { //behind proxy
            $currentIp = $_SERVER['HTTP_X_FORWARDED_FOR'];
            $currentIp = explode(',', $currentIp);
            $currentIp = trim(reset($currentIp));
        } else {
            $currentIp = $_SERVER['REMOTE_ADDR'];
        }
        //$currentIp = $_SERVER['REMOTE_ADDR'];
        return $currentIp;
    }

    /**
     * Convert string with IP to IP array
     *
     * @param $text string
     * @return array
     */
    protected function _ipTextToArray($text)
    {
        $ips = preg_split("/[\n\r]+/", $text);
        foreach ($ips as $ipsk => $ipsv) {
            if (trim($ipsv) == "") {
                unset($ips[$ipsk]);
            }
        }
        return $ips;
    }

    /**
     * Save Blocked IP to log
     *
     * @param array $params
     * @return bool
     */
    protected function saveToLog($params = array())
    {
        $neednotify = true;

        if (!((isset($params['blocked_ip'])) && (strlen(trim($params['blocked_ip'])) > 0))) {
            $params['blocked_ip'] = $this->getCurrentIp();
        }

        if (!((isset($params['blocked_from'])) && (strlen(trim($params['blocked_from'])) > 0))) {
            $params['blocked_from'] = 'undefined';
        }

        $now = now();

        /* @var $logtable Dyna_IpSecurity_Model_Mysql4_Ipsecuritylog_Collection */
        $logtable = Mage::getModel('dynaipsecurity/ipsecuritylog')->getCollection();
        $logtable->getSelect()->where('blocked_from=?', $params['blocked_from'])
            ->where('blocked_ip=?', $params['blocked_ip']);

        if (count($logtable) > 0) {
            foreach ($logtable as $row) {
                /* @var $row Dyna_IpSecurity_Model_Ipsecuritylog */
                $timesBlocked = $row->getData('qty') + 1;
                $row->setData('qty', $timesBlocked);
                $row->setData('last_block_rule', $this->getLastBlockRule());
                $row->setData('update_time', $now);
                $row->save();
                if (($timesBlocked % 10) == 0) {
                    $neednotify = true;
                } else {
                    $neednotify = false;
                }
            }
        } else {
            $log = Mage::getModel('dynaipsecurity/ipsecuritylog');

            $log->setData('blocked_from', $params['blocked_from']);
            $log->setData('blocked_ip', $params['blocked_ip']);
            $log->setData('qty', '1');
            $log->setData('last_block_rule', $this->getLastBlockRule());
            $log->setData('create_time', $now);
            $log->setData('update_time', $now);

            $log->save();
            $neednotify = true;
        }

        // if returns true - IP blocked for first time or timesBloked is 10, 20, 30 etc.
        return $neednotify;
    }

    //log any denied attempts
    public function logDeny()
    {
        $filename = $this->createFilename('ip_restriction');
        if ( ! @file_put_contents($filename, date("Y-m-d H:m:s") . ' Restricted following IP from accessing ' . $this->_environment . ': ' . $this->getCurrentIp() . PHP_EOL, FILE_APPEND)) {
            throw new Exception('Could not write data to log file at "%s"', $filename);
        }
    }

    /**
     * @param $baseName
     * @return string
     */
    public function generateFilename($baseName)
    {
        return join(DS, array(Mage::getBaseDir('var'), 'log', 'security' . DS . 'access', $baseName . '_' . date("dmY") . '.log'));
    }

    /**
     * @param $baseName
     * @return string
     * @throws Exception
     */
    public function createFilename($baseName)
    {
        $filename = $this->generateFilename($baseName);
        if (is_file($filename) && is_writeable($filename)) {
            return $filename;
        } else {
            if ( ! realpath($filename)) {
                $file = new Varien_Io_File();
                $file->setAllowCreateFolders(true);
                $file->createDestinationDir(dirname($filename));
                unset($file);
                if (false == @touch($filename)) {
                    throw new Exception(sprintf("Could not create log file at path '%s'. Please check file permission.", $filename));
                }
            } elseif (realpath($filename) && is_dir($filename)) {
                throw new Exception(sprintf("Expected '%s' to be a file, directory found.", $filename));
            } else {
                throw new Exception(sprintf("Write permissions for '%s' file not given.", $filename));
            }
        }
        return $filename;
    }

}