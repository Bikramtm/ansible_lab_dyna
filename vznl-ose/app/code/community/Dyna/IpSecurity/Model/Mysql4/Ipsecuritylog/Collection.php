<?php
/**
 * @category   Dyna
 * @package    Dyna_IpSecurity
 */

class Dyna_IpSecurity_Model_Mysql4_Ipsecuritylog_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('dynaipsecurity/ipsecuritylog');
    }
}