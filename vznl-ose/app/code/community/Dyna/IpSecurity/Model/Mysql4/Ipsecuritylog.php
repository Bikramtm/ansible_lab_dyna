<?php
/**
 * @category   Dyna
 * @package    Dyna_IpSecurity
 */

class Dyna_IpSecurity_Model_Mysql4_Ipsecuritylog extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {
        // Note that the logid refers to the key field in your database table.
        $this->_init('dynaipsecurity/ipsecuritylog', 'logid');
    }
}