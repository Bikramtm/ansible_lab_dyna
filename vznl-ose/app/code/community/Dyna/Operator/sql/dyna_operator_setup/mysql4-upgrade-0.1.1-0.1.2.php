<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Core_Model_Resource_Setup $this */
$this->startSetup();
$connection = $this->getConnection();

$table = 'operator/operator_service_providers';
$column = [
    'name' => 'cable_provider',
    'type' => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
    'default' => 0,
    'comment'   => 'Cable Provider flag',
    'after'     => 'name',
    'required'  => true,
    'nullable'  => false,
];

if ($connection->isTableExists($this->getTable($table))) {
    $connection->addColumn($this->getTable($table), $column['name'],
        [
            'type' => $column['type'],
            'default' => $column['default'],
            'comment' => $column['comment'],
            'required' => $column['required'],
            'after' => $column['after'],
            'nullable' => $column['nullable'],
        ]
    );
}

$this->endSetup();
