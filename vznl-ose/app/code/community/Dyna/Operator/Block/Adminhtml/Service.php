<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Dyna_Operator_Block_Adminhtml_Service extends Omnius_Operator_Block_Adminhtml_Service
{
    public function __construct()
    {
        parent::__construct();
        $this->_removeButton('add');
    }
}
