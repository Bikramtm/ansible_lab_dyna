<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Admin list operator combinations
 */

class Dyna_Operator_Block_Adminhtml_Service_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('networkGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('operator/serviceProvider')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * Create the admin images list
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareColumns()
    {
        $this->addColumn('entity_id', array(
            'header'    => Mage::helper('operator')->__('ID'),
            'align'     =>'right',
            'width'     => '30px',
            'index'     => 'entity_id',
        ));

        $this->addColumn('code', array(
            'header'        => Mage::helper('operator')->__('Telcompany'),
            'align'         => 'left',
            'index'         => 'code'
        ));

        $this->addColumn('name', array(
            'header'    => Mage::helper('operator')->__('OSF GUI Name'),
            'align'     => 'left',
            'index'     => 'name'
        ));

        $this->addColumn('cable_provider', array(
            'header'    =>  Mage::helper('operator')->__('Cable provider'),
            'type'      => 'options',
            'options'   => [
                '1' => 'Yes',
                '0' => 'No'
            ],
            'index'     => 'cable_provider',
        ));

        $this->addColumn('fixed_line', array(
            'header'    =>  Mage::helper('operator')->__('Haftungsfreistellung'),
            'type'      => 'options',
            'options'   => [
                '1' => 'Yes',
                '0' => 'No'
            ],
            'index'     => 'fixed_line',
        ));



        return parent::_prepareColumns();
    }

    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('operator');


        return $this;
    }

    /**
     * @param $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return '';
    }
}
