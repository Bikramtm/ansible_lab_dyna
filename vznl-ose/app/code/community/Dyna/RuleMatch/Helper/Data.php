<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_RuleMatch_Helper_Data
 */
class Dyna_RuleMatch_Helper_Data extends Omnius_RuleMatch_Helper_Data
{
    /** @var Dyna_Checkout_Model_Sales_Quote quote */
    protected $quote = null;

    protected $_packageAttributes = array(
        'process_context'
    );

    protected $_quoteAttributes = array(
        'dealer',
        'red_sales_id'
    );

    protected $_dealerAttributes = array(
    );

    protected $_mappings = array(
        'quote_item' => array(
            'quote_item_price' => 'row_total',
            'quote_item_qty' => 'qty',
            'quote_item_row_total' => 'getBaseRowTotal',
        ),
        'quote' => array(
            'red_sales_id' => 'sales_id',
            'dealer' => 'dealer_id',
        ),
        'package' => array(
            'process_context' => 'getProcessContextId',
        ),
        'product' => array(
            'channel' => 'prodspecs_hawaii_channel',
            'memory' => 'prodspecs_opslagcapaciteit',
            'camera' => 'prodspecs_camera',
            'network' => 'identifier_subscr_brand',
            'category_ids' => 'getAvailableInCategories'
        ),
        'address' => array(

        ),
        'customer' => array(
            'customer_segment' => 'value_segment',
            'value' => 'customer_value',
            'campaign' => 'getCampaigns',
        ),
        'dealer' => array(
            'dealer_group' => 'group_id',
        ),
    );

    /** @var array List of attributes that can have "is one of" conditions */
    protected $isOneOffAttributes = [
        'dealer',
        'dealer_group',
        'process_context',
        'red_sales_id',
    ];

    /**
     * Parses attributes for the indexer conditions.
     * @return array
     */
    protected function _getAttributeValues()
    {
        $attrValues = array();
        $attrCode = false;
        $quote = $this->_getQuote();
        $activePackage = $this->getActivePackage();
        $packageEmpty = true;
        foreach ($quote->getAllItems() as $quoteItem) {
            foreach ($this->_quoteItemAttributes as $attrCode) {
                if ($quoteItem->getPackageId() == $activePackage->getPackageId()) {
                    $packageEmpty = false;
                    $attrValues[$attrCode][] = $this->getValue($quoteItem, $attrCode, 'quote_item');
                }
            }
        }

        // make sure to add sku null if package empty to restrict the number of sales rule to be processed
        if ($packageEmpty) {
            foreach ($this->_quoteItemAttributes as $attrCode) {
                $attrValues[$attrCode][] = null;
            }
        }

        foreach ($this->_quoteAttributes as $attrCode) {
            $attrValues[$attrCode][] = $this->getValue($quote, $attrCode, 'quote');
        }

        foreach ($this->_packageAttributes as $attrCode) {
            $attrValues[$attrCode][] = $this->getValue($activePackage, $attrCode, 'package');
        }

        $address = $this->_getAddress();
        foreach ($this->_addressAttributes as $attrCode) {
            $attrValues[$attrCode][] = $this->getValue($address, $attrCode, 'address');
        }

        $customer = $this->_getCustomer();
        foreach ($this->_customerAttributes as $attrCode) {
            $attrValues[$attrCode][] = $this->getValue($customer, $attrCode, 'customer');
        }

        $dealer = $this->_getDealer();
        foreach ($this->_dealerAttributes as $attrCode) {
            $attrValues[$attrCode][] = $this->getValue($dealer, $attrCode, 'dealer');
        }

        foreach ($attrValues as $attrCode => $values) {
            $attrValues[$attrCode] = array_unique($values, SORT_REGULAR);
        }

        return $attrValues;
    }

    /**
     * Overriding parent getQuote method due to parent calling getQuote with parameter true (orderEdit)
     * @return Dyna_Checkout_Model_Sales_Quote
     */
    protected function _getQuote()
    {
        if($this->quote == null){
            /** @var Dyna_Checkout_Model_Sales_Quote quote */
            $this->quote = $this->_getCart()->getQuote();
        }

        return $this->quote;
    }

    /**
     * Retrieves available sales rules from the index table based on the current cart attributes.
     * @param   string $coupon
     * @param   bool $asCollection
     * @return  array
     */
    public function getApplicableRules($asCollection = false, $coupon = '')
    {
        /** @var Omnius_RuleMatch_Model_Mysql4_Rulematch_Collection $collection */
        $collection = Mage::getResourceModel('rulematch/rulematch_collection');

        $attrValues = $this->_getAttributeValues();
        ksort($attrValues);
        $cacheKey = sprintf('sales_rules_%s_%s', md5(serialize($attrValues)), $coupon);
        if ($result = $this->getCache()->load($cacheKey)) {
            return unserialize($result);
        } else {
            foreach ($attrValues as $attrCode => $values) {
                $filters = array();
                $needsNull = true;
                if (in_array($attrCode, $this->isOneOffAttributes)) {
                    $collection->addFieldToFilter($attrCode, array(array('finset' => $values), array('null' => true)));
                } else {
                    foreach ($values as $value) {
                        $needsNull = $value !== null;
                        if ($value instanceof Varien_Data_Collection) {
                            array_push($filters, array('in' => $value->getAllIds()));
                        } elseif ($value instanceof Varien_Object) {
                            array_push($filters, array('eq' => $value->getId()));
                        } elseif (is_array($value)) {
                            array_push($filters, array('in' => $value));
                        } else {
                            array_push($filters, (!$needsNull) ? array('null' => true) : array('eq' => $value));
                        }
                    }
                    if ($needsNull) {
                        array_push($filters, array('null' => true));
                    }

                    $collection->addFieldToFilter(array_fill(0, count($filters), $attrCode), $filters);
                }
            }

            $select = $collection->getSelect()
                ->reset(Varien_Db_Select::COLUMNS)
                ->columns(array('rule_id'))
                ->distinct(true)
                ->assemble();

            try {
                unset($collection);
                $applicableRuleIds = $this->_getAdapter()->fetchCol($select);
            } catch (Exception $e) {
                Mage::getSingleton('core/logger')->logException($e);
                $rules = Mage::getResourceModel('salesrule/rule_collection');
                return $asCollection
                    ? $rules
                        ->setValidationFilter(Mage::app()->getWebsite()->getId(), Mage::getSingleton('customer/session')->getCustomerGroupId(), $coupon)
                        ->load()
                    : $rules
                        ->setValidationFilter(Mage::app()->getWebsite()->getId(), Mage::getSingleton('customer/session')->getCustomerGroupId(), $coupon)
                        ->load()
                        ->getColumnValues('rule_id');
            }

            $result = $asCollection
                ? Mage::getResourceModel('salesrule/rule_collection')
                    ->setValidationFilter(Mage::app()->getWebsite()->getId(), Mage::getSingleton('customer/session')->getCustomerGroupId(), $coupon)
                    ->addFieldToFilter('rule_id', array('in' => $applicableRuleIds))
                : $applicableRuleIds;

            if ($result instanceof Varien_Data_Collection) {
                //Serialization of 'Mage_Core_Model_Config_Element' is not allowed
                $tmp = new Varien_Data_Collection();
                foreach ($result->getItems() as $item) {
                    $tmp->addItem($item);
                }
                $result = $tmp;
            }

            $this->getCache()->save(serialize($result), $cacheKey, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());

            return $result;
        }
    }
}
