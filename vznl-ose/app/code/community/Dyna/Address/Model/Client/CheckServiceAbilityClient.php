<?php

class Dyna_Address_Model_Client_CheckServiceAbilityClient extends Dyna_Service_Model_Client
{
    const COLOR_DEFAULT = 'gray';
    const COLOR_AVAILABLE = 'green';
    const COLOR_LIGHT_GREEN = 'lightgreen';
    const COLOR_NOT_AVAILABLE = 'red';
    const COLOR_PLANNED = 'yellow';
    const CONFIG_STUB_PATH = 'service_ability/use_stubs';
    const WSDL_CONFIG_KEY = 'service_ability/wsdl';
    const ENDPOINT_CONFIG_KEY = 'service_ability/endpoint';
    const LTE_SERVICE_ID = "NGN-4G";

    /** @var array Client options */
    protected $_options = [];

    /** @var array Requested in OMNVFDE-3280 */
    protected $_skippedTechnologies = ["4G", "3G", "2G"];

    protected $xmlStubs = true;

    /**
     * Perform serviceability check and retrieve technology from services
     * @param $params
     * @return array
     */
    public function executeCheckServiceAbility($params)
    {
        $addressId = !empty($params['addressId']) ? $params['addressId'] : "";
        $myProductsCheck = !empty($params['myProductsCheck']) ? $params['myProductsCheck'] : null;
        $soapParams = $this->mapAddressParams($params);
        $this->setRequestHeaderInfo($soapParams);

        /** @var Dyna_Customer_Model_Session $customerSession */
        $customerSession = Mage::getSingleton('dyna_customer/session');

        $tasiOrTn = [
            'tasi' => null,
            'component_ctn' => null,
            'local_area_code' => null,
            'phone_number' => null
        ];
        $tnValue = [];

        // when the customer is logged
        if($customerSession->getCustomer()->isCustomerLoggedIn()) {
            /** @var Dyna_Customer_Helper_Customer $customerHelper */
            $customerHelper = Mage::helper('dyna_customer/customer');

            $serviceCustomers = $customerSession->getServiceCustomers();
            // tasi, component ctn or customer number are sent we will send these to the call
            if (!empty($params['tasi']) || !empty($params['component_ctn']) || !empty($params['customerNumber'])) {
                $tasiOrTn['tasi'] = !empty($params['tasi']) ? $params['tasi'] :  null;
                $tasiOrTn['component_ctn'] = !empty($params['component-ctn']) ? preg_replace('[\D]', '', $params['component-ctn']) :  null;
                if(!empty($params['customerNumber'])) {
                    if (!empty($serviceCustomers[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN][$params['customerNumber']]['contact_phone_numbers'][0]) ) {
                        $tnValue = $serviceCustomers[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN][$params['customerNumber']]['contact_phone_numbers'][0];
                    } else {
                        $tnValue = $customerHelper->getTASIOrTN($params['customerNumber']);
                    }
                }
            }
            // the tn, ctn and customer number are not sent => use the session customer
            else if($customerSession->getCustomer()->getAccountCategory() == Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN) {
                $customerNumber = $customerSession->getCustomer()->getCustomerNumber();
                $tasiOrTn = $customerHelper->getTASIOrTN($customerNumber);
                $tnValue = ($serviceCustomers[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN][$customerNumber]['contact_phone_numbers'][0]) ?? [];
            }

            // VFDED1W3S-4990
            if((isset($params['isCustomer']) && ($params['isCustomer'])) ||
                (isset($params['customerNumber']) && !empty($params['customerNumber']))) {
                $soapParams['PhoneNumber'] = ($tnValue && (isset($tnValue['local_area_code']) || isset($tnValue['phone_number']))) ? array(
                    'CountryCode' => !empty($tnValue['country_code']) ? $tnValue['country_code'] : null,
                    'LocalAreaCode' => !empty($tnValue['local_area_code']) ? $tnValue['local_area_code'] : null,
                    'PhoneNumber' => !empty($tnValue['phone_number']) ? $tnValue['phone_number'] : null,
                    'Type' => !empty($tnValue['type']) ? $tnValue['type'] : null
                ) : null;

                // no PhoneNumber yet
                if ($soapParams['PhoneNumber'] === null) {
                    if ($tasiOrTn['tasi'] === null && isset($tasiOrTn['phone_number'])) {
                        $soapParams['PhoneNumber'] = ($tasiOrTn && (isset($tasiOrTn['local_area_code']))) ? array(
                            'CountryCode' => null,
                            'LocalAreaCode' => !empty($tasiOrTn['local_area_code']) ? $tasiOrTn['local_area_code'] : null, // optional
                            'PhoneNumber' => !empty($tasiOrTn['phone_number']) ? $tasiOrTn['phone_number'] : null, // required
                            'Type' => null,
                        ) : null;
                    }
                } else {
                    if ($soapParams['PhoneNumber']['LocalAreaCode'] === null ) { // no LocalAreaCode yet
                        $soapParams['PhoneNumber'] = ($tasiOrTn && (isset($tasiOrTn['local_area_code']))) ? array(
                            'LocalAreaCode' => !empty($tasiOrTn['local_area_code']) ? $tasiOrTn['local_area_code'] : null,
                        ) : null;
                    }
                    if ($soapParams['PhoneNumber']['PhoneNumber'] === null ) { // no PhoneNumber yet
                        $soapParams['PhoneNumber'] = ($tasiOrTn && (isset($tasiOrTn['phone_number']))) ? array(
                            'PhoneNumber' => !empty($tasiOrTn['phone_number']) ? $tasiOrTn['phone_number'] : null,
                        ) : null;
                    }
                }
            } else {
                $soapParams['PhoneNumber'] = null;
            }
        } else {
            $soapParams['PhoneNumber'] = null;
        }

        // VFDED1W3S-1505: send one of ExistingCustomerTASI OR ExistingCustomerTN nodes
        if(isset($params['isCustomer']) && ($params['isCustomer'] || !empty($params['customerNumber']))) {
            $soapParams["ns0:ReferenceIDs/ns2:ID[@schemeName='ExistingCustomerTN']"] = null;
            $soapParams["ns0:ReferenceIDs/ns2:ID[@schemeName='ExistingCustomerTASI']"] = $tasiOrTn['tasi'];
            if ($tasiOrTn['tasi'] === null && $tasiOrTn['component_ctn'] !== null) {
                $soapParams["ns0:ReferenceIDs/ns2:ID[@schemeName='ExistingCustomerTN']"] = preg_replace("/[^0-9]/", "", $tasiOrTn['component_ctn']);
            }
        } else {
            $soapParams["ns0:ReferenceIDs/ns2:ID[@schemeName='ExistingCustomerTN']"] = null;
            $soapParams["ns0:ReferenceIDs/ns2:ID[@schemeName='ExistingCustomerTASI']"] = null;
        }

        $addressDetails = $this->CheckServiceAbility($soapParams);

        // Save address on session
        /** @var Dyna_Address_Model_Storage $sessionAddress */
        $sessionAddress = Mage::getSingleton('dyna_address/storage');
        $sessionAddress->setOriginalServiceAbility($addressDetails);

        if (isset($myProductsCheck) && $myProductsCheck || empty($validAddresses)) {
            $data = [
                'address_id' => $params['addressId'],
                'postcode' => $params['postalcode'],
                'street' => $params['street'],
                'house_addition' => !empty($params['houseAddition']) ? $params['houseAddition'] : null,
                'house_number' => $params['buildingNr'],
                'city' => $params['city'],
                'component_ctn' => !empty($params['component_ctn']) ? $params['component_ctn'] :  null,
                'tasi' => !empty($params['tasi']) ? $params['tasi'] : null,
                'customer_number' => !empty($params['customerNumber']) ? $params['customerNumber'] : null
            ];
            $sessionAddress->setData('address', $data);
        } else {

            if (isset($params['isCustomer']) && $params['isCustomer']) {
                $validAddress = $customerSession->getCustomer()->getServiceAddress();
                $sessionAddress->setData('address', $validAddress);
            }

            $validAddresses = $sessionAddress->getData('validAddresses');
            $validAddresses = isset($validAddresses[0]) ? $validAddresses : array($validAddresses);
            foreach ($validAddresses as $validAddress) {
                if ($validAddress['ID'] == $params['addressId'] || $validAddress['addressId'] == $params['addressId']) {
                    $sessionAddress->setData('address', $validAddress);
                }
            }
        }

        // Extract availability technologies used for service source expression and set it on address storage
        $this->updateAvailability($addressDetails);

        $result = $this->parseCheckServiceAbilityResponse($addressDetails);
        $result['address_id'] = $addressId;

        // Set serviceability on current address
        $sessionAddress->setServiceAbility($result);

        return $result;
    }

    /*
     * Building downstream/upstream availability and setting it on address storage
     * @param $addressDetails array
     * @return $this
     */

    public function mapAddressParams($params)
    {
        $params['Address']['ID'] = $params['addressId'];
        $params['Address']['StreetName'] = !empty($params['street']) ? $params['street'] : "";
        $params['Address']['BuildingNumber'] = !empty($params['buildingNr']) ? $params['buildingNr'] : "";
        $params['Address']['CityName'] = !empty($params['city']) ? $params['city'] : "";
        $params['Address']['PostalZone'] = !empty($params['postalcode']) ? $params['postalcode'] : "";
        $params['Address']['HouseNumberAddition'] = !empty($params['houseAddition']) ? $params['houseAddition'] : "";

        return $params;
    }

    public function updateAvailability($addressDetails)
    {
        $availabilityInfo = [];
        $areaCode = null;

        if (!isset($addressDetails['LineItem'][0])) {
            $addressDetails['LineItem'] = [$addressDetails['LineItem']];
        }

        foreach ($addressDetails["LineItem"] as $catalogLine) {
            $areaCode = $catalogLine['AreaCode'] ?? null;

            foreach ($catalogLine["CatalogueLine"] as $technology) {
                // Found a valid availability item for service source expression
                if (!empty($technology["Item"]["Bandwidth"]) && !empty($technology["Priority"]) && $technology["Priority"] == 1) {

                    /**
                     * As per OMNVFDE-1266 we need to filter out the streams to only keep one
                     * stream that has the highest bandwidth (downstream) and priority=1
                     * Anything else needs to go
                     */

                    $item = new Varien_Object();
                    // Set technology
                    $item->setData('technology', $technology['ID']);
                    // Set upstream
                    $item->setData("upstream", (int)$technology["Item"]["Bandwidth"]["UpStream"]);
                    // Set downstream
                    $item->setData("downstream", (int)$technology["Item"]["Bandwidth"]["DownStream"]);
                    // Set priority
                    $item->setData("priority", (int)$technology["Priority"]);
                    // Set availability indicator
                    $item->setData("availability_indicator", $technology["Item"]["Status"]["StatusReasonCode"]);
                    // Set product id
                    $item->setData("product", $technology["ID"]);

                    $availabilityOptions = [];
                    $additionalData = [];

                    if (isset($technology["AvailabilityOption"])) {
                        // Parsing availability option - as seen in response example can be one availability option or an array of availability options
                        $technology["AvailabilityOption"] = isset($technology["AvailabilityOption"][0]) ? $technology["AvailabilityOption"] : [$technology["AvailabilityOption"]];
                        foreach (array_filter($technology["AvailabilityOption"]) as $availability) {
                            $availabilityOption = new Varien_Object();
                            $availabilityOption->setData("option_name", $availability["Name"]);
                            $availabilityOption->setData("life_cycle_status_code", $availability["LifeCycleStatusCode"]);
                            $availabilityOptions[] = $availabilityOption;

                            if (isset($availability["AdditionalItemProperty"])) {
                                $availability["AdditionalItemProperty"] = isset($availability["AdditionalItemProperty"][0]) ? $availability["AdditionalItemProperty"] : [$availability["AdditionalItemProperty"]];
                                foreach (array_filter($availability["AdditionalItemProperty"]) as $additionalItem) {
                                    $additionalItemData = new Varien_Object();
                                    $additionalItemData->setData("information_key", $additionalItem["Name"]);
                                    $additionalItemData->setData("information_value", $additionalItem["Value"]);

                                    $additionalData[] = $additionalItemData;
                                }
                            }
                        }
                    }

                    $item->setData('availability_options', $availabilityOptions);
                    $item->setData('additional_information', $additionalData);

                    $availabilityInfo[] = $item;
                }
            }
        }

        $storage = Mage::getSingleton('dyna_address/storage');
        $storage->setAvailability($availabilityInfo);
        $storage->setAreaCode($areaCode);

        return $this;
    }

    /**
     * Identify VOD status and KIP bandwidth value
     * (function created to decrease code complexity)
     * @param $addressDetails
     * @param $sessionStorage
     * @param $topBarServices     *
     * @return array
     */
    protected function identifyVODStatusAndKIPBandwidthValue($addressDetails, $sessionStorage, $topBarServices)
    {
        $cableBandwidth = '';

        foreach ($addressDetails['ObjectInfo']['AdditionalItemProperty'] as $itemProperty) {
            switch ($itemProperty['Name']) {
                // VOD technology
                case 'vodVermarktbarkeitsKz':
                    if ($itemProperty['Value'] === 'J') {
                        $topBarServices['VOD']['color'] = static::COLOR_AVAILABLE;
                        $sessionStorage->setVODAvailable(true);
                    } else {
                        $topBarServices['VOD']['color'] = static::COLOR_NOT_AVAILABLE;
                        $sessionStorage->setVODAvailable(false);
                    }
                    $topBarServices['VOD']['tooltip'] = null;
                    $topBarServices['VOD']['original_tooltip'] = null;
                    $topBarServices['VOD']['active_tooltip'] = 'no';
                    break;
                case 'vermarktbareDatentransferrate':
                    $cableBandwidth = $itemProperty['Value'];
                    $topBarServices['KIP']['bandwidth'] = $cableBandwidth;
                    break;
                case 'selbstInstallationKz':
                    $sessionStorage->setSelfInstallAvailable($itemProperty['Value'] === 'J');
                    break;
                case 'MMDAdapterVersenden':
                    $sessionStorage->setMMDAvailable($itemProperty['Value'] !== 'J');
                    break;
                case 'satZusatzKz':
                    $sessionStorage->setSatAvailable($itemProperty['Value'] === 'J');
                    break;
                default:
                    break;
            }
        }

        return [
            'cableBandwidth' => $cableBandwidth,
            'topBarServices' => $topBarServices
        ];
    }

    /**
     * Identify if IPTV(HD) is available from previous $tvServices
     * (function created to decrease code complexity)
     * @param $iptvValues
     * @param $topBarServices
     * @return array
     */
    protected function identifyIfIPTVIsAvailableFromPreviousTVServices($iptvValues, $topBarServices)
    {
        foreach ($iptvValues as $iptvValue) {
            if ($iptvValue['label'] === 'HD' && $iptvValue['color'] === static::COLOR_AVAILABLE) {
                $topBarServices['IPTV(HD)'] = array_merge($topBarServices['IPTV(HD)'], $iptvValue);
                break;
            }
        }

        return $topBarServices;
    }

    /**
     * Display max bandwidth value in top bar label
     * (function created to decrease code complexity)
     * @param $lteServices
     * @param $topBarServices
     * @return array
     */
    protected function displayMaxBandwidthValueInTopBarLabel($lteServices, $topBarServices)
    {
        if (count($lteServices)) {
            krsort($lteServices);
            foreach ($lteServices as $lteService) {
                if ($lteService['lte_in'] && ($topBarServices['LTE IN']['bandwidth'] === 0)) {
                    $topBarServices['LTE IN']['bandwidth'] = $lteService['bandwidth'];
                } elseif ($lteService['lte_out'] && ($topBarServices['LTE OUT']['bandwidth'] === 0)) {
                    $topBarServices['LTE OUT']['bandwidth'] = $lteService['bandwidth'];
                }
            }
        }

        return $topBarServices;
    }

    /**
     * Process FN priorities by color
     * (function created to decrease code complexity)
     * @param $fnPriorities
     * @return $maxFn
     */
    protected function processFNPrioritiesByColor($fnPriorities)
    {
        foreach ($fnPriorities as $item) {
            if ($item['color'] === static::COLOR_AVAILABLE) {
                $maxFn = $item;
                break;
            } elseif (($item['color'] === static::COLOR_PLANNED)
                && ((isset($maxFn) && ($maxFn['bandwidth'] < $item['bandwidth']))
                    || !isset($maxFn))
            ) {
                $maxFn = $item;
            }
        }

        return $maxFn;
    }

    /**
     * Handle cable technologies
     * (function created to decrease code complexity)
     * @param $cableTechnologies
     * @param $cableBandwidth
     * @param $topBarServices
     * @return array
     */
    protected function handleCableTechnologies($cableTechnologies, $cableBandwidth, $topBarServices)
    {
        foreach ($cableTechnologies as &$option) {
            if ($option['bandwidth'] <= $cableBandwidth) {
                $option['color'] = $topBarServices['KIP']['color'];
            }
        }
        unset($option);

        return $cableTechnologies;
    }

    /**
     * Handle line item availability options
     * (function created to decrease code complexity)
     * @param $lineItemavailabilityOptions
     * @param $lteIn
     * @param $lteOut
     * @return array
     */
    protected function handleLineItemAvailabilityOptions($lineItemavailabilityOptions, $lteIn, $lteOut)
    {
        foreach ($lineItemavailabilityOptions as $lineItemavailabilityOption) {
            if ($lineItemavailabilityOption['Name'] === 'RADIO_METHOD_QUALITY') {
                $lteIn = strtolower($lineItemavailabilityOption['Value']) === 'indoor';
                $lteOut = strtolower($lineItemavailabilityOption['Value']) === 'outdoor';
                break;
            }
        }

        return [
            'lteIn' => $lteIn,
            'lteOut' => $lteOut
        ];
    }

    /**
     * Handle IP-Bitstream Service
     * (function created to decrease code complexity)
     * @param $serviceId
     * @param $serviceItem
     * @param $fnDownstream
     * @param $awards
     * @return array
     */
    protected function handleIPBitstreamService($serviceId, $serviceItem, $fnDownstream, $awards)
    {
        if ($serviceId === 'IP-Bitstream') {
            $award = ($serviceItem['Priority'] === 1) ? 1 : 0;
            if ($fnDownstream === 6000 || $fnDownstream === 2000 || $fnDownstream === 1000) {
                $awards[6000] = (int)((isset($awards[6000]) ? $awards[6000] : false) || $award);
            } else {
                $awards[$fnDownstream] = $award;
            }
        }

        return $awards;
    }

    /**
     * Assign max available bandwidth to the top bar display
     * (function created to decrease code complexity)
     * @param $maxFn
     * @param $topBarServices
     * @return array
     */
    protected function assignMaxAvailableBandwidthToTheTopBarDisplay($maxFn, $topBarServices)
    {
        if (isset($maxFn) && $maxFn) {
            // assign max available bandwidth to the top bar display
            $topBarServices['(V)DSL']['bandwidth'] = $maxFn['bandwidth'];
            $topBarServices['(V)DSL']['color'] = $maxFn['color'];
        }

        return $topBarServices;
    }

    /**
     * Setup the Top Bar Available Services
     * @param $addressDetails
     * @return mixed
     */
    public function parseCheckServiceAbilityResponse($addressDetails)
    {
        $topBarServices = $this->initTopBarServices();
        $cableBandwidth = 0;
        /** @var Dyna_Address_Model_Storage $sessionStorage */
        $sessionStorage = Mage::getSingleton('dyna_address/storage');

        $this->setServiceObjectClass($addressDetails);

        if (isset($addressDetails['ObjectInfo'])) {
            // Identify VOD status and KIP bandwidth value from 'ObjectInfo'->'AdditionalItemProperty' nodes
            $identifyVODKIPArray = $this->identifyVODStatusAndKIPBandwidthValue($addressDetails, $sessionStorage, $topBarServices);

            $topBarServices = $identifyVODKIPArray['topBarServices'];
            $cableBandwidth = $identifyVODKIPArray['cableBandwidth'];
        }

        $services = $topBarServices;
        $areaCode =  $postalZone = '';
        $technologies = $this->initTechnologies();

        $fnPriorities = $this->getFNPriorities();
        $networkOperators = [];
        $awards = [];

        if (!isset($addressDetails['LineItem'][0])) {
            $addressDetails['LineItem'] = [$addressDetails['LineItem']];
        }

        $lteServices = [];
        foreach ($addressDetails['LineItem'] as $serviceItems) {
            if (isset($serviceItems['AreaCode'])) {
                $areaCode = $serviceItems['AreaCode'];
            }

            if(isset($serviceItems['MainDistributionFrame']['Address']['PostalZone']) && $serviceItems['MainDistributionFrame']['Address']['PostalZone']) {
                $postalZone = $serviceItems['MainDistributionFrame']['Address']['PostalZone'];
            }

            if (isset($serviceItems['CatalogueLine']['ID'])) {
                $serviceItems['CatalogueLine'] = [$serviceItems['CatalogueLine']];
            }

            foreach ($serviceItems['CatalogueLine'] as $serviceItem) {
                $serviceId = trim($serviceItem['ID']);

                /**
                 * Some technologies can be skipped/omitted from the serviceabilitycheck response, as they are not needed in the result modal
                 * OMNVFDE-3280
                 */
                if (in_array($serviceId, $this->_skippedTechnologies)) {
                    continue;
                }

                $serviceStatus = $serviceItem['Item']['Status']['StatusReasonCode'];
                $color = $this->getMarketabilityColorByStatus($serviceStatus);

                // LTE handling
                if ($serviceId == self::LTE_SERVICE_ID) {
                    $lteDownstream = (int)$serviceItem['Item']['Bandwidth']['DownStream'];
                    $isAvailable = $color === self::COLOR_AVAILABLE;

                    if (isset($serviceItem['AvailabilityOption']['AdditionalItemProperty'])) {
                        $availabilityOptions = isset($serviceItem['AvailabilityOption']['AdditionalItemProperty'][0]) ?
                            $serviceItem['AvailabilityOption']['AdditionalItemProperty'] :
                            [$serviceItem['AvailabilityOption']['AdditionalItemProperty']];
                    } else {
                        $availabilityOptions = [];
                    }

                    $lteIn = $lteOut = 0;

                    if (isset($serviceItem['Item']['AdditionalItemProperty'])
                        || isset($serviceItem['Item'][0]['AdditionalItemProperty'])
                    ) {
                        $lineItemAvailabilityOptions = isset($serviceItem['Item']['AdditionalItemProperty']['Name'])
                            ? array($serviceItem['Item']['AdditionalItemProperty'])
                            : $serviceItem['Item']['AdditionalItemProperty'];

                        $lineItemAvailabilityOptionsArray = $this->handleLineItemAvailabilityOptions($lineItemAvailabilityOptions, $lteIn, $lteOut);
                        $lteIn = $lineItemAvailabilityOptionsArray['lteIn'];
                        $lteOut = $lineItemAvailabilityOptionsArray['lteOut'];
                    }

                    // Top bar display
                    if ($isAvailable && $lteIn) {
                        $topBarServices['LTE IN']['color'] = static::COLOR_AVAILABLE;
                    }
                    if ($isAvailable && $lteOut) {
                        $topBarServices['LTE OUT']['color'] = static::COLOR_AVAILABLE;
                    }

                    $lteServices[$lteDownstream] = [
                        'bandwidth' => str_replace('.', ',', $this->dimBandwidth($lteDownstream)),
                        'lte_in' => $isAvailable && $lteIn,
                        'lte_out' => $isAvailable && $lteOut,
                        'lteDownstream' => $lteDownstream,
                    ];
                    continue;
                }

                $isFn = isset($serviceItem['Item']['Bandwidth']['DownStream']);

                if ($isFn) {
                    $fnDownstream = (int)$serviceItem['Item']['Bandwidth']['DownStream'];
                    if (isset($fnPriorities[$fnDownstream])) {

                        $awards = $this->handleIPBitstreamService($serviceId, $serviceItem, $fnDownstream, $awards);

                        $fnServices[$fnDownstream][$serviceId] = [
                            'bandwidth' => $fnDownstream,
                            'color' => $color,
                            'status' => $serviceStatus,
                            'technology' => $serviceId,
                            'priority' => (int)$serviceItem['Priority'],
                        ];

                        $tvServices[$fnDownstream][$serviceId] = $serviceItem['AvailabilityOption'] ?? [];
                    } else {
                        Mage::log(sprintf('[NOTICE] Unprocessed FN Bandwidth %s returned by `CheckServiceAbility` service call', $fnDownstream), Zend_Log::NOTICE);
                    }
                } else {
                    $services[$serviceId] = [
                        'bandwidth' => 0,
                        'color' => $color,
                        'tooltip' => $serviceId,
                        'status' => $serviceStatus,
                    ];

                    if (isset($serviceItem['AvailabilityOption'])
                        && $serviceItem['AvailabilityOption']['Name'] == 'IPTV'
                    ) {
                        $topBarServices['IPTV(HD)']['color'] = $this->getMarketabilityColorByStatus($serviceItem['AvailabilityOption']['LifeCycleStatusCode']);
                    }

                    if (array_key_exists($serviceId, $topBarServices)) {
                        $topBarServices[$serviceId] = $services[$serviceId];
                    } elseif ($serviceId == 'KAI') {
                        $topBarServices['KIP']['color'] = $color;
                        $services['KIP']['color'] = $color;
                    }

                    $networkOperators[$serviceId] = $serviceItem['ContractSubdivision'];
                }
            }
        }

        // Display max bandwidth value in top bar label
        $topBarServices = $this->displayMaxBandwidthValueInTopBarLabel($lteServices, $topBarServices);

        $technologies['lte'] = array_values($lteServices);

        // Save Network Operators to be used by the Artifact
        $sessionStorage->setNetworkOperators($networkOperators);

        $technologies['cable'] = $this->handleCableTechnologies($technologies['cable'], $cableBandwidth, $topBarServices);

        $technologies['awards'] = $awards;

        if (isset($fnServices) && count($fnServices)) {
            $fnPriorities = $this->processFNPriorities($fnServices, $awards);

            $maxFn = $this->processFNPrioritiesByColor($fnPriorities);
            $topBarServices = $this->assignMaxAvailableBandwidthToTheTopBarDisplay($maxFn, $topBarServices);

            $iptvValues = isset($tvServices) ? $this->processTVServices($fnPriorities, $tvServices) : [];

            // Map info for modal display
            $technologies['dsl'] = array_values($fnPriorities);
            $technologies['iptv'] = array_values($iptvValues);

            // Identify if IPTV(HD) is available from previous $tvServices
            $topBarServices = $this->identifyIfIPTVIsAvailableFromPreviousTVServices($iptvValues, $topBarServices);

        }

        // Services info displayed in the top bar
        $response['top_bar_available_services'] = $topBarServices;
        // Address displayed in the top bar
        $response['full_address'] = strip_tags(Mage::getSingleton('dyna_address/storage')->getServiceAddress(true), '<br>');
        $response['full_address'] = str_replace('null', '', $response['full_address']);
        // All Cable services returned in the response
        $response['services'] = $services;
        // Technologies displayed in the modal (top section)
        $response['technology'] = $technologies;
        // Special Cable products in the modal
        $response['special_products'] = $this->getSpecialProducts($services);
        // Info used in the modal to display Technische Verfügbarkeit & Objektinformationen
        $response['ObjectInfo'] = $addressDetails['ObjectInfo'] ?? [];
        // Area code used in checkout
        $response['AreaCode'] = $areaCode;
        // Postal Zone will be used in Regional Goodies Availabilty
        $response['PostalZone'] = $postalZone;

        $this->setCanOrderCableOnSession($services);
        $this->setCanOrderFnOnSession($topBarServices);
        $defaultAvailability = array_combine(
            Dyna_Cable_Model_Product_Attribute_Option::$servicesForCableAvailability,
            array_fill(0, count(Dyna_Cable_Model_Product_Attribute_Option::$servicesForCableAvailability), false)
        );
        $defaultFNAvailability = array_combine(
            Dyna_Fixed_Model_Product_Attribute_Option::$servicesForFNAvailability,
            array_fill(0, count(Dyna_Fixed_Model_Product_Attribute_Option::$servicesForFNAvailability), false)
        );
        $response['cableAvailability'] = Mage::getSingleton('dyna_address/storage')->getCableAvailability($defaultAvailability);
        $response['fnAvailability'] = Mage::getSingleton('dyna_address/storage')->getFNAvailability($defaultFNAvailability);

        /** @var Dyna_Configurator_Helper_Data $configuratorHelper */
        $configuratorHelper = Mage::helper('dyna_configurator');
        $response['customerHasKipInstalled'] = $configuratorHelper->customerHasServiceInstalled(
            Dyna_Customer_Model_Client_RetrieveCustomerInfo::CABLE_PRODUCT_CATEGORY_KIP
        );

        $response['customerHasKudInstalled'] = $configuratorHelper->customerHasServiceInstalled(
            Dyna_Customer_Model_Client_RetrieveCustomerInfo::CABLE_PRODUCT_CATEGORY_CLS
        );

        return $response;
    }

    /**
     * Set the ServiceObjectClass on Address Storage
     * @param $addressDetails
     */
    protected function setServiceObjectClass($addressDetails)
    {
        /** @var Dyna_Address_Model_Storage $sessionStorage */
        $sessionStorage = Mage::getSingleton('dyna_address/storage');

        if (isset($addressDetails['ObjectInfo']['BuildingInfo']['StatusCode'])) {
            // Save ServiceObjectClass to be used by the Artifact
            $sessionStorage->setServiceObjectClass(strval($addressDetails['ObjectInfo']['BuildingInfo']['StatusCode']));
        }
    }

    /**
     * Initial state of the top bar services
     * By default they have bandwidth = 0 and gray color
     * @return array
     */
    protected function initTopBarServices()
    {
        /** @var Dyna_Configurator_Helper_Data $configuratorHelper */
        $configuratorHelper = Mage::helper('dyna_configurator');

        // List of all possible technologies
        $topBarServices = [
            'KIP' => [], 'KAA' => [], 'KAD' => [], 'VOD' => [], '(V)DSL' => [], 'IPTV(HD)' => [], 'LTE IN' => [], 'LTE OUT' => []
        ];
        foreach ($topBarServices as $serviceCode => $array) {
            $topBarServices[$serviceCode] = ['bandwidth' => 0, 'color' => 'gray', 'tooltip' => $configuratorHelper->__($serviceCode), 'status' => null];
        }

        return $topBarServices;
    }

    /**
     * Default states for technologies displayed in the modal (top section)
     * @return array
     */
    protected function initTechnologies()
    {
        // TODO: map real data from response for LTE
        return [
            'cable' => [
                ['bandwidth' => 400, 'color' => static::COLOR_DEFAULT],
                ['bandwidth' => 200, 'color' => static::COLOR_DEFAULT],
                ['bandwidth' => 100, 'color' => static::COLOR_DEFAULT],
                ['bandwidth' => 32, 'color' => static::COLOR_DEFAULT],
            ],
            'lte' => [],
            'dsl' => [],
            'iptv' => [],
        ];
    }

    /**
     * The priority-technology for each FN Bandwidth
     * @return array
     */
    protected function getFNPriorities()
    {
        // Default values for FN DSL
        return [
            100000 => ['bandwidth' => 100, 'color' => static::COLOR_DEFAULT],
            50000 => ['bandwidth' => 50, 'color' => static::COLOR_DEFAULT],
            25000 => ['bandwidth' => 25, 'color' => static::COLOR_DEFAULT],
            16000 => ['bandwidth' => 16, 'color' => static::COLOR_DEFAULT],
            6000 => ['bandwidth' => 6, 'color' => static::COLOR_DEFAULT],
            2000 => ['bandwidth' => 2, 'color' => static::COLOR_DEFAULT],
            1000 => ['bandwidth' => 1, 'color' => static::COLOR_DEFAULT],
        ];
    }

    /**
     * There are certain marketability values (letters) that are returned from the CheckServiceAbility call which indicate what color should be displayed.
     * If the marketability is 'A' (reject any order), than the traffic lights will show red color
     * If the marketability is 'B' (allow any order), than the traffic lights will show green color.
     * If the marketability is 'I', than the traffic lights will show red color
     * If the marketability is 'V', than the traffic lights will show green color
     * If the marketability is 'C', than the traffic lights will show green color.
     *
     * @param string $status
     * @return string
     */
    protected function getMarketabilityColorByStatus($status)
    {
        switch ($status) {
            case 'A':
            case 'I':
                $color = static::COLOR_NOT_AVAILABLE;
                break;
            case 'B':
            case 'V':
            case 'C':
            case 'TRUE':
            case 'GRUEN':
                $color = static::COLOR_AVAILABLE;
                break;
            case 'GELB':
                $color = static::COLOR_PLANNED;
                break;
            default:
                $color = static::COLOR_DEFAULT;
                break;
        }

        return $color;
    }

    /**
     * Identify each technology within a bandwidth based on priority
     * @param $fnServices
     * @param $awards array
     * @return array
     */
    protected function processFNPriorities($fnServices, $awards)
    {
        $result = $this->getFNPriorities();

        foreach ($fnServices as &$fnService) {
            usort($fnService, function ($a, $b) {
                return $a['priority'] <=> $b['priority'];
            });
        }
        unset($fnService);

        foreach ($fnServices as $bandwidth => $technologies) {
            $dimBandwidth = $this->dimBandwidth($bandwidth);
            if (count($technologies) == 1) {
                $result[$bandwidth] = array_shift($technologies);
                $result[$bandwidth]['bandwidth'] = $dimBandwidth;
            } else {
                foreach ($technologies as $technology) {
                    if ($technology['color'] == static::COLOR_AVAILABLE
                        || $technology['color'] == static::COLOR_PLANNED
                    ) {
                        $technology['bandwidth'] = $dimBandwidth;
                        $result[$bandwidth] = $technology;
                        break;
                    }
                }
            }
        }
        krsort($result);

        foreach ($result as $bandwidth => &$item) {
            if (isset($awards[$bandwidth])) {
                $item['award'] = $awards[$bandwidth];
            }
        }

        return $result;
    }

    /**
     * Helper method to format bandwidth for display
     * @param $bandwidth
     * @return float
     */
    protected function dimBandwidth($bandwidth)
    {
        return ($bandwidth >= 1000) ? $bandwidth / 1000 : $bandwidth;
    }

    /**
     * Determine TV options for each bandwidth
     * @param $fnPriorities
     * @param $tvServices
     * @return array
     */
    protected function processTVServices($fnPriorities, $tvServices)
    {
        $tmp = $result = [];
        foreach ($fnPriorities as $bandwidth => $data) {
            $key = $data['technology'] ?? null;
            $dimBandwidth = $this->dimBandwidth($bandwidth);

            if ($key) {
                $tmp = $this->extractTvServices($tvServices, $dimBandwidth, $bandwidth, $key);
            } else {
                $tmp[$bandwidth][] = [
                    'name' => '',
                    'bandwidth' => $dimBandwidth,
                    'color' => static::COLOR_DEFAULT,
                ];
            }
        }

        $tvPriorities = $this->getTVPriorities();

        foreach ($tmp as $bandwidth => $items) {
            foreach ($tvPriorities as $priority) {
                foreach ($items as $available) {
                    if (isset($available['name']) && ($priority == $available['name'])) {
                        $available['label'] = isset($available['options']) ? $this->sortTVOptions($available['options']) : $available['name'];

                        $result[$bandwidth] = $available;
                        break(2);
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Extract the available TV services from serviceability response
     * @param $tvServices
     * @param $dimBandwidth
     * @param $bandwidth
     * @param $key
     * @return array
     */
    protected function extractTvServices($tvServices, $dimBandwidth, $bandwidth, $key)
    {
        $tmp = [];
        $availabilityOptions = $tvServices[$bandwidth][$key];
        // Assure array of values
        $availabilityOptions = (isset($availabilityOptions[0]) ? $availabilityOptions : [$availabilityOptions]);
        foreach ($availabilityOptions as $option) {
            if (isset($option['Name']) && $option['Name'] == 'TV-Center inkl. QoS') {
                $tmp[$bandwidth]['IPTV'] = [
                    'name' => 'IPTV',
                    'bandwidth' => $dimBandwidth,
                    'color' => $this->getMarketabilityColorByStatus($option['LifeCycleStatusCode']),
                ];

                if (isset($option['AdditionalItemProperty'])) {
                    // Assure array of values
                    if (!isset($option['AdditionalItemProperty'][0])) {
                        $option['AdditionalItemProperty'] = [$option['AdditionalItemProperty']];
                    }
                    // Determine IPTV SD - HD options status
                    foreach ($option['AdditionalItemProperty'] as $property) {
                        if ($property['Name'] == 'TVC_BASIS_TV') {
                            $tmp[$bandwidth]['IPTV']['options'][] = [
                                'name' => 'SD',
                                'available' => $property['Value'],
                            ];
                        } elseif ($property['Name'] == 'TVC_HD_OPTION') {
                            $tmp[$bandwidth]['IPTV']['options'][] = [
                                'name' => 'HD',
                                'available' => $property['Value'],
                            ];
                        }
                    }
                } else {
                    $tmp[$bandwidth][] = [
                        'name' => 'IPTV',
                        'bandwidth' => $dimBandwidth,
                        'color' => $this->getMarketabilityColorByStatus($option['LifeCycleStatusCode']),
                    ];
                }
            } elseif (isset($option['Name']) && $option['Name'] == 'TV-Center') {
                $determinedColor = $this->getMarketabilityColorByStatus($option['LifeCycleStatusCode']);
                // Determine SAT TV status
                $tmp[$bandwidth]['SAT TV'] = [
                    'name' => 'SAT TV',
                    'bandwidth' => $dimBandwidth,
                    'color' => $determinedColor == self::COLOR_AVAILABLE ? self::COLOR_LIGHT_GREEN : $determinedColor,

                ];
            }
        }

        return $tmp;
    }

    /**
     * Within the TV options, there are some differences:
     * - First it differentiates between SAT-TV and IPTV:
     * -- Condition for SAT-TV available: TV Center is GREUN
     * -- Condition for IPTV available: TV Centre Ink. QoS is GREUN
     * -- But within IPTV, there are also differences:
     * --- When TVC_BASIS_TV = TRUE and TVC_HD_OPTION = FALSE, then SD is available
     * --- When TVC_HD_OPTION is TRUE, then HD is available.
     * --- If both SD and HD are available, show HD
     * -- When both SAT-TV and IPTV are available, always go for the IPTV option
     * -- If only SAT-TV is available, and IPTV isn't, then the field should be filled with light green and the IPTV options should be normal green
     *
     * @return array
     */
    protected function getTVPriorities()
    {
        return [
            'HD',
            'SD',
            'IPTV',
            'SAT TV',
            '',
        ];
    }

    /**
     * @param $options
     * @return null
     */
    protected function sortTVOptions($options)
    {
        $tvPriorities = $this->getTVPriorities();
        foreach ($tvPriorities as $priority) {
            foreach ($options as $option) {
                if (isset($option['name']) && ($priority == $option['name']) && $option['available'] != 'FALSE') {
                    return $option['name'];
                }
            }
        }
        return '';
    }

    /**
     * Get the special products from the modal (last section)
     * @param $services
     * @return array
     */
    protected function getSpecialProducts($services)
    {
        // VVO, ZIK, ZOB should not be used (OMNVFDE-3320)
        return [
            'BUV' => (isset($services['KAI']) && $services['KAI']['status'] == 'C') ? 1 : 0,
            'VVM' => (isset($services['KAI']) && $services['KAI']['status'] == 'V') ? 1 : 0,
        ];
    }

    /**
     * OMNVFDE-1274
     * The cable products are not available if all KAA/KAD,KAI,VOD,KUD are not available
     *
     * @param array $topBarServices
     */
    private function setCanOrderCableOnSession($topBarServices)
    {
        $availability = [];
        foreach ($topBarServices as $serviceName => $serviceValues) {
            if (in_array($serviceName, Dyna_Cable_Model_Product_Attribute_Option::$servicesForCableAvailability)) {
                $availability[$serviceName] = ($serviceValues['color'] == self::COLOR_AVAILABLE);
            }
        }

        $sessionAddress = Mage::getSingleton('dyna_address/storage');
        $sessionAddress->setCableAvailability($availability);
    }

    /**
     * OMNVFDE-1274
     * The cable products are not available if all KAA/KAD,KAI,VOD,KUD are not available
     *
     * @param array $topBarServices
     */
    private function setCanOrderFnOnSession($topBarServices)
    {
        $availability = [];
        foreach ($topBarServices as $serviceName => $serviceValues) {
            if (in_array($serviceName, Dyna_Fixed_Model_Product_Attribute_Option::$servicesForFNAvailability)) {
                $availability[$serviceName] = ($serviceValues['color'] == self::COLOR_AVAILABLE);
            }
        }

        $sessionAddress = Mage::getSingleton('dyna_address/storage');
        $sessionAddress->setFNAvailability($availability);
    }
}
