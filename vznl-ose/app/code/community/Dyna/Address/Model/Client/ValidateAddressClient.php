<?php

/**
 * Class Dyna_Address_Model_Client_ValidateAddressClient
 */
class Dyna_Address_Model_Client_ValidateAddressClient extends Dyna_Service_Model_Client
{
        // Address Types
    const SERVICE_ADDRESS = "service_address";
    const DELIVERY_ADDRESS = "delivery_other";
    const BILLING_ADDRESS = "billing_other";
    const CUSTOMER_ADDRESS = "customer";
    const SEPA_ADDRESS = "sepa_address";
    const CONFIG_STUB_PATH = 'validate_address/use_stubs';
    const WSDL_CONFIG_KEY = 'validate_address/wsdl';
    const ENDPOINT_CONFIG_KEY = 'validate_address/endpoint';
    const NO_ADDRESS_ID_MARKER = "no-address-id-returned";

    protected $invalidAddress;

    public function __construct($wsdl, array $options = array())
    {
        parent::__construct($wsdl, $options);

        $this->invalidAddress = [
            'data["type"] =="' . self::SERVICE_ADDRESS . '"and data["country"] != "DEU" and data["addressId"] > 0 and data["confirmation"] == "false" and data["status"] =="false"',
            'data["type"] =="' . self::SERVICE_ADDRESS . '"and data["country"] == "DEU" and data["addressId"] === 0 and data["confirmation"] == "false" and data["status"] =="false"',
            'data["type"] =="' . self::SERVICE_ADDRESS . '"and data["country"] != "DEU" and data["addressId"] === 0 and data["confirmation"] == "false" and data["status"] =="false"',
            'data["type"] =="' . self::DELIVERY_ADDRESS . '"and data["country"] == "DEU" and data["addressId"] === 0 and data["confirmation"] == "false" and data["status"] =="false"',
            'data["type"] =="' . self::BILLING_ADDRESS . '"and data["country"] == "DEU" and data["addressId"] === 0 and data["confirmation"] == "false" and data["status"] =="false"',
            'data["type"] =="' . self::CUSTOMER_ADDRESS . '"and data["country"] == "DEU" and data["addressId"] === 0 and data["confirmation"] == "false" and data["status"] =="false"',
            'data["type"] =="' . self::SEPA_ADDRESS . '"and data["country"] != "DEU" and data["addressId"] > 0 and data["confirmation"] == "false" and data["status"] =="false"',
            'data["type"] =="' . self::SEPA_ADDRESS . '"and data["country"] != "DEU" and data["addressId"] === 0 and data["confirmation"] == "false" and data["status"] =="false"',
        ];
    }

    /**
     * @param $params
     * @return mixed
     */
    public function isValidAddress($params)
    {
        $result = $this->executeValidateAddress($params);
        $data['type'] = isset($params['type']) ? $params['type'] : null;
        $data['confirmation'] = false;
        $data['status'] = false;
        $id = null;

        if (!isset($result['ValidateAddress'])) {
            return false;
        }

        if (!isset($result['ValidateAddress'][0])) {
            $result['ValidateAddress'] = [$result['ValidateAddress']];
        }

        /**
         * @var $dynaCoreHelper Dyna_Core_Helper_Data
         */
        $dynaCoreHelper = Mage::helper('dyna_core');

        $validAddress = false;

        foreach ($result['ValidateAddress'] as &$address) {
            $data["confirmation"] = $address['MarkAttention'];
            $data["status"] = $address['MarkCare'];
            $data['country'] = $address['Country']['Name'];
            $data['addressId'] = $address['ID'];

            $currentAddressValid = true;

            foreach ($this->invalidAddress as $exp) {
                if ($data['type'] == 'payment_monthly_alternative') {
                    $exp = str_replace('sepa_address', 'payment_monthly_alternative', $exp);
                }
                $passed = $dynaCoreHelper->evaluateExpressionLanguage(
                    $exp,
                    ['data' => $data]
                );
                if ($passed) {
                    $currentAddressValid = false;
                    break;
                }
            }
            //if current address has passed validation mark it as valid
            if ($currentAddressValid) {
                $id = $address["ID"];
                $address["ID"] = $address["ID"] ?: static::NO_ADDRESS_ID_MARKER;
                $validAddress = true;
            }
            //if current address is not a valid one remove it from the returned addresses
            if(($key = array_search($address, $result['ValidateAddress'])) !== false && !$currentAddressValid) {
                unset($result['ValidateAddress'][$key]);
            }
        }
        // break reference to address
        unset($address);

        //if valid address id was found, save it in the checkout fields
        if ($id) {
            $data['type'] = str_replace('_self', '', $data['type']);
            $data['type'] = str_replace('_alternative', '', $data['type']);
            /** @var Dyna_Checkout_Model_Sales_Quote $quote */
            $quote = Mage::getSingleton('checkout/cart')->getQuote();
            Mage::helper("dyna_checkout/fields")->addData(
                $quote,
                $quote->getCurrentStep(),
                array($data['type'] . (isset($params['package_id']) ? '_pakket_' . $params['package_id'] : '') . "_address_id" => $id)
            );
        }

        return array('result' => $result, 'validity' => $validAddress);
    }

    /**
     * @param array $params
     * @return array
     */
    public function executeValidateAddress($params = array()) : array
    {
        $cacheKey = sprintf('validate_address_%s', md5(implode(',', $params)));
        /** @var Dyna_Cache_Model_Cache $cacheModel */
        $cacheModel = Mage::getSingleton('dyna_cache/cache');
        if ($cachedResult = $cacheModel->load($cacheKey)) {
            $results = unserialize($cachedResult);
        } else {
            $params = $this->mapAddressParams($params);
            $this->setRequestHeaderInfo($params);

            $results = $this->ValidateAddress($params);

            $cacheModel->save(serialize($results), $cacheKey);
        }

        return $results;
    }

    /**
     * @param $params
     */
    public function mapAddressParams($params)
    {
        $params['ValidateAddress']['CityName'] = $params['city'];
        $params['ValidateAddress']['PostalZone'] = $params['postcode'];
        $params['ValidateAddress']['StreetName'] = $params['street'];
        $params['ValidateAddress']['BuildingNumber'] = $params['houseno'];
        $params['ValidateAddress']['District'] = !empty($params['district']) ? $params['district'] : "";
        $params['ValidateAddress']['Country']['IdentificationCode'] = "DEU";
        $params['ValidateAddress']['Country']['Name'] = "DEU";
        // @VFDEW3E2E-675
        $params['ValidateAddress']['HouseNumberAddition'] = !empty($params['addition']) ? $params['addition'] : $params['isCustomer'] ? '' : '*';

        return $params;
    }
}
