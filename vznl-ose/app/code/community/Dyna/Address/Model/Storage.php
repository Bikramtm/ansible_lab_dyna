<?php

/**
 * Class Dyna_Address_Model_Storage
 * @method setAddress($data)
 * @method getAddress()
 * @method setServiceAbility($data)
 * @method getServiceAbility()
 * @method setOriginalServiceAbility()
 * @method getOriginalServiceAbility()
 * @method setNetworkOperators($array) Used by the Cable Artifact
 * @method getNetworkOperators() Used by the Cable Artifact
 * @method setServiceObjectClass($string) Used by the Cable Artifact
 * @method getServiceObjectClass() Used by the Cable Artifact
 * @method setVODAvailable($bool) Used by the Cable Artifact
 * @method getVODAvailable() : bool Used by the Cable Artifact
 * @method setMMDAvailable($bool) Used by the Cable Artifact
 * @method getMMDAvailable() : bool Used by the Cable Artifact
 * @method setSatAvailable($bool) Used by the Cable Artifact
 * @method getSatAvailable() : bool Used by the Cable Artifact
 * @method setSelfInstallAvailable($bool) Used by the Cable Artifact
 * @method getSelfInstallAvailable() : bool  Returns whether or not the address has self installation service; Used by the Cable Artifact
 */
class Dyna_Address_Model_Storage extends Mage_Core_Model_Session_Abstract
{
    const CONN_NE3 = "NE3-Objekt";
    const CONN_NE4 = "NE4-Objekt";
    const CONN_MNV = "MNV";

    protected $ne3Objects = ["NE3"];
    protected $mnvObjects = ["NE3-INV", "NE3-INV-D", "NE3-NE4-INV", "NE3-NE4-INV-D", "NE3-NE4-INV-D-RET", "NE3-NE4-INV-HD-D", "NE4", "NE4-ADI", "NE4-GES", "NE4-GES-D", "NE4-M28", "NE4-M28-D", "NE4-M28-D-RET", "NE4-M28-HD-D", "NE4-M51", "NE4-M51-D", "NE4-M51-HD-D"];
    protected $ne4Objects = ["NE4-VVO", "NE4-VVO-D", "NE4-VVO-HD-D", "NE4-ZIB"];

    /**
     * Dyna_Address_Model_Storage constructor.
     */
    public function __construct()
    {
        $this->init('address_storage' . '_' . (Mage::app()->getStore()->getWebsite()->getCode()));
    }

    /**
     * Clear current loaded address
     */
    public function clearAddress()
    {
        $this->clear();
    }

    /**
     * Return the service address for current order in the following format if asString is set to false
     * array(
     *  "street, houseNo",
     *  "postalCode city"
     * )
     * @return array
     */
    public function getServiceAddress($asString = false)
    {
        //If model data is empty, than no serviceability check was performed
        if (empty($this->getData())) {
            if ($asString) {
                return "";
            }
            return [];
        }

        $address = $this->getData('address');
        $address = $this->uniformAddress($address);

        //An array containing street and houseNo in first key, postCode and city in the other key
        $result = [
            $address['street'] . " " . $address['houseno'] . (empty($address['addition']) ? '' : ' ' . $address['addition']),
            $address['postcode'] . " " . $address['city'] . " " . $address['district'],
        ];

        if ($asString) {
            //One string containing entire address
            $result = implode(' <br>', $result);
        }

        return $result;
    }

    /**
     * Return service address by components
     * @return array
     */
    public function getServiceAddressParts()
    {
        $address = $this->getData('address');
        $addressData = $this->uniformAddress($address);

        /** @var Dyna_Customer_Model_Customer $customer */
        $customer = Mage::getSingleton('customer/session')
            ->getCustomer();

        return [
            "full_name" => $customer->getName(),
            "firstname" => $customer->getFirstName(),
            "lastname" => $customer->getLastName(),
            "title" => $customer->getTitle(),
            "foundAddress" => (bool)$addressData,
        ] + $addressData;
    }

    /**
     * @param $address
     * @return array
     */
    protected function uniformAddress($address)
    {
        $street = !empty($address['StreetName']) ? $address['StreetName'] : $address['street'];
        $houseNo = !empty($address['BuildingNumber']) ? $address['BuildingNumber'] :  $address['house_number'];
        $postCode = !empty($address['PostalZone']) ? $address['PostalZone'] : $address['postcode'];
        $city = !empty($address['CityName']) ? $address['CityName'] : $address['city'];
        $addition = !empty($address['HouseNumberAddition']) ? $address['HouseNumberAddition'] : $address['house_addition'];
        $district = !empty($address['District']) ? $address['District'] : (!empty($address['district']) ? $address['district'] : null);
        $postbox = !empty($address['Postbox']) ? $address['Postbox'] : (!empty($address['postbox']) ? $address['postbox'] : null);

        return [
            'id' => !empty($address['ID']) ? $address['ID'] : null,
            "street" => $street,
            "houseno" => $houseNo,
            "addition"  => $addition,
            "postcode" => $postCode,
            "city" => $city,
            "district" => $district,
            "postbox" => $postbox,
        ];
    }

    /*
     * Returns whether or not the serviceability check has been performed for current order
     * return bool
     */
    public function isServiceAddressChecked()
    {
        return !empty($this->getServiceAbility());
    }

    /**
     * Return the current addressId
     * @return string|null
     */
    public function getAddressId()
    {
        $serviceAbility = $this->getServiceAbility();

        return !empty($serviceAbility['address_id']) ? $serviceAbility['address_id'] : null;
    }

    /**
     * @param $addressParts
     * @return null
     */
    public function getCustomerAddressId($addressParts)
    {
        $addressParts = array_map("strtolower", $addressParts);
        $key = md5(serialize($addressParts));

        $addressIds = $this->getAddressIds();
        return $addressIds[$key] ?? null;
    }

    /**
     * @param $addressParts
     * @param $addressId
     * @return $this
     */
    public function addCustomerAddressId($addressParts, $addressId)
    {
        $addressParts = array_map("strtolower", $addressParts);
        $key = md5(serialize($addressParts));

        $addressIds = $this->getAddressIds();
        $addressIds[$key] = $addressId;
        $this->setAddressIds($addressIds);

        return $this;
    }

    /**
     * Return the current area code
     * @return string|null
     */
    public function getAreaCode()
    {
        $serviceAbility = $this->getServiceAbility();

        return !empty($serviceAbility['AreaCode']) ? $serviceAbility['AreaCode'] : null;
    }

    /**
     * Return the current post code
     * @return string|null
     */
    public function getPostCode()
    {
        $serviceAbility = $this->getServiceAbility();

        return !empty($serviceAbility['PostalZone']) ? $serviceAbility['PostalZone'] : null;
    }

    /**
     * After the getServiceAbility call has been performed, availability options need to be retrieved from response and parsed so that expression language can have access
     * to them for product combination rules
     * @check Dyna_Address_Model_Client_CheckServiceAbilityClient::updateAvailability()
     *
     * @param array $availabilityData
     * @return $this
     */
    public function setAvailability(array $availabilityData)
    {
        $this->setData('availability', $availabilityData);

        return $this;
    }

    /**
     * Return availability details used in service expression combination rules
     * @return $array
     */
    public function getAvailability()
    {
        return $this->getData('availability');
    }

    /**
     * Perform serviceability check for cable and fixed packages
     *
     * @param Dyna_Package_Model_Package $package
     */
    public function loadServiceabilityByPackage(Dyna_Package_Model_Package $package)
    {
        if ($package->isFixed() || $package->isCable()) {
            $packageAddress = $package->getServiceAddressData();

            /** @var Dyna_Address_Model_Client_CheckServiceAbilityClient $serviceabilityClient */
            $serviceabilityClient = Mage::getModel("dyna_address/client_CheckServiceAbilityClient");
            $addressParams = [
                "addressId" => $packageAddress["address_id"] ?? ($packageAddress["id"] ?? null),
                "street" => $packageAddress["street"],
                "buildingNr" => $packageAddress["houseno"],
                "city" => $packageAddress["city"],
                "postalcode" => $packageAddress["postcode"],
                "houseAddition" => $packageAddress["addition"]
            ];

            $serviceabilityClient->executeCheckServiceAbility($addressParams);
        }
    }

    /**
     * Get connection type object NE3, NE4 or MNV (see OMNVFDE-2076)
     * @return string
     */
    public function getConnectionObject()
    {
        // Get the original serviceability response
        $serviceAbility = $this->getOriginalServiceAbility();

        $objectCategory = !empty($serviceAbility['ObjectInfo']['BuildingInfo']['StatusCode']) ? $serviceAbility['ObjectInfo']['BuildingInfo']['StatusCode'] : "";
        $connectionObject = "";
        if (in_array(strtoupper($objectCategory), $this->ne3Objects)) {
            $connectionObject = self::CONN_NE3;
        } else if (in_array(strtoupper($objectCategory), $this->ne4Objects)) {
            $connectionObject = self::CONN_NE4;
        } else if (in_array(strtoupper($objectCategory), $this->mnvObjects)) {
            $connectionObject = self::CONN_MNV;
        }

        return $connectionObject;
    }

    /**
     * Determine whether or not connection object is self install
     * @return bool
     */
    public function isConnectionSelfInstall()
    {
        // Get the original serviceability response
        $serviceAbility =  $this->getOriginalServiceAbility();

        $selfInstallValue = false;
        $additionalObjectInfo =  !empty($serviceAbility['ObjectInfo']['AdditionalItemProperty']) ? $serviceAbility['ObjectInfo']['AdditionalItemProperty'] : array();
        foreach ($additionalObjectInfo as $itemProperty) {
            if (strtoupper($itemProperty['Name']) != strtoupper('selbstInstallationKz')) {
                continue;
            }

            // Comparing against not (because not sure if Y or J are the values for true)
            if (strtoupper($itemProperty['Value']) != 'N') {
                $selfInstallValue = true;
            }
        }

        return $selfInstallValue;
    }

    /**
     * Identify asb value
     */
    public function getAsb()
    {
        $data = $this->getData();
        if(isset($data['original_service_ability']['LineItem'])) {
            foreach ($data['original_service_ability']['LineItem'] as $lineItem) {
                if (!empty($lineItem['Asb'])) {
                    return $lineItem['Asb'];
                }
            }
        }

        return null;
    }

    /**
     * @return array
     */
    public function getServiceAddressArray()
    {
        $address =  $this->getData('address');
        if(!empty($address)){
            return [
                'address_id' => !empty($address['address_id']) ? $address['address_id'] : '',
                'street' => !empty($address['StreetName']) ? $address['StreetName'] : $address['street'],
                'house_number' => !empty($address['BuildingNumber']) ? $address['BuildingNumber'] :  $address['house_number'],
                'postcode' => !empty($address['PostalZone']) ? $address['PostalZone'] : $address['postcode'],
                'city' => !empty($address['CityName']) ? $address['CityName'] : $address['city'],
                'house_addition' => !empty($address['HouseNumberAddition']) ? $address['HouseNumberAddition'] : $address['house_addition'],
                'district' => !empty($address['District']) ? $address['District'] : (!empty($address['district']) ? $address['district'] : null),
                'post_box' => !empty($address['Postbox']) ? $address['Postbox'] : (!empty($address['postbox']) ? $address['postbox'] : null)
            ];
        }
        return [];

    }
}
