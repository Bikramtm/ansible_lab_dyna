<?php

/**
 * Class Dyna_Address_IndexController
 */
class Dyna_Address_IndexController extends Mage_Core_Controller_Front_Action
{
    const MAX_SEARCH_RESULTS = 100;

    /** @var Omnius_Core_Helper_Data|null */
    private $dynaCoreHelper = null;

    /**
     * @return Mage_Core_Controller_Front_Action
     */
    public function preDispatch()
    {
        if (Mage::app()->getRequest()->getActionName() == 'check') {
            define('CM_REDISSESSION_LOCKING_ENABLED', false);
        }
        return parent::preDispatch();
    }

    /**
     * @return Omnius_Core_Helper_Data
     */
    protected function getDynaCoreHelper()
    {
        if ($this->dynaCoreHelper === null) {
            $this->dynaCoreHelper = Mage::helper('omnius_core');
        }

        return $this->dynaCoreHelper;
    }

    /**
     * @param $data
     * @param int $statusCode
     * @throws InvalidArgumentException
     */
    protected function jsonResponse($data, $statusCode = 200)
    {
        $response = $this->getDynaCoreHelper()
            ->jsonResponse($data, $statusCode);

        $this->getResponse()
            ->setBody($response['body'])
            ->setHeader('Content-Type', 'application/json')
            ->setHttpResponseCode($response['status']);
    }

    /**
     * If ServiceAddress, call isValidAddress with a second parameter
     * in order to avoid calling executeValidateAddress twice
     * (function created to decrease code complexity)
     * @param $params
     * @param $module Dyna_Address_Model_Client_ValidateAddressClient
     * @param $requestParams
     * @param $serviceAddress
     * @return array
     */
    protected function checkIfServiceAddress($params, $module, $requestParams, $serviceAddress) {
        if (isset($params['type']) && $params['type'] == 'service_address') {
            $requestParams['type'] = 'service_address';
            $serviceAddress = $module->isValidAddress($requestParams);
            $results = isset($serviceAddress['error']) ? $serviceAddress : $serviceAddress['result'];
        } else {
            $results = $module->executeValidateAddress($requestParams);
        }

        return [
            'requestParams' => $requestParams,
            'serviceAddress' => $serviceAddress,
            'results' => $results
        ];
    }

    /**
     * If type is validate_exact_address
     * return validate address
     * (function created to decrease code complexity)
     * @param $params
     * @param $results
     * return array
     */
    protected function checkIfValidateExactAddress($params, $results) {
        if (isset($params['type']) && $params['type'] == 'validate_exact_address') {
            $result = (!array_key_exists(0, $results['ValidateAddress'])) ? array($results['ValidateAddress']) : $results['ValidateAddress'];
        } else {
            $result = $results['ValidateAddress'];
        }

        return $result;
    }

    /**
     * If ServiceAddress, check if it is valid
     * (function created to decrease code complexity)
     * @param $params
     * @param $requestParams
     * @param $validity
     * @param $serviceAddress
     * @param $response
     * return array
     */
    protected function checkServiceAddressValidity($params, $requestParams, $validity, $serviceAddress, $response) {
        if (isset($params['type']) && $params['type'] === 'service_address') {
            $requestParams['type'] = 'service_address';
            $validity = $serviceAddress['validity'];
            $response = !$validity
                ? [ 'error' => true, 'message' => $this->__('Invalid address') ]
                : '';
        }

        return [
            'requestParams' => $requestParams,
            'validity' => $validity,
            'response' => $response
        ];
    }

    /**
     * If address is valid, set it
     * (function created to decrease code complexity)
     * @param $validity
     * @param $results
     * return void
     */
    protected function setAddressIfValid($validity, $results) {
        if (!$validity || ($validity && ($validity === true))) {
            $this->getStorage()->setData('validAddresses', $results['ValidateAddress']);
        }
    }

    /**
     * Set response for validate address process
     * (function created to decrease code complexity)
     * @param $response
     * @param $results
     * @param $result
     * return mixed
     */
    protected function setResponseForValidateAddress($response, $results, $result) {
        return $response && ($response != '') ? $response : [
            'error' => false,
            'message' => sprintf($this->__('%d addresses found'), $results['ReturnedNumberOfRecords']),
            'addresses' => $result,
        ];
    }

    /**
     * ValidateAddress
     */
    public function validateAddressAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }

        /** @todo, implement service call with params */
        $params = $this->getRequest()->getPost();

        // Rebuilding params array for better overview of sent parameters
        $requestParams = array(
            "street"    => (isset($params["street"])) ? $params["street"] : "",
            "houseno"   => (isset($params["houseNo"])) ? $params["houseNo"] : (isset($params["no"]) ? $params["no"] : ""),
            "postcode"  => (isset($params["postcode"])) ? $params["postcode"] : (isset($params["zipcode"]) ? $params["zipcode"] : ""),
            "city"      => (isset($params["city"])) ? $params["city"] : "",
            "addition"  => (isset($params["addition"])) ? $params["addition"] : (isset($params["addNo"]) ? $params["addNo"] : ""),
            "isCustomer"=> (isset($params["isCustomer"])) ? $params["isCustomer"] : false,
        );

        $serviceAddress = null;

        try {
            /** @var Dyna_Address_Model_Client_ValidateAddressClient $module */
            $module = Mage::getModel("dyna_address/client_validateAddressClient");

            /* if ServiceAddress, call isValidAddress with a second parameter
            in order to avoid calling executeValidateAddress twice */
            $checkIfServiceAddress = $this->checkIfServiceAddress($params, $module, $requestParams, $serviceAddress);

            $serviceAddress = $checkIfServiceAddress['serviceAddress'];
            $results = $checkIfServiceAddress['results'];

            if (isset($results['error']) && isset($results['message'])) {
                $response = [
                    'error' => true,
                    'message' => $results['message'],
                ];
            } elseif ($results['ReturnedNumberOfRecords'] == 0) {
                $response = [
                    'error' => true,
                    'message' => $this->__('No address found'),
                ];
            } elseif ($results['ReturnedNumberOfRecords'] > self::MAX_SEARCH_RESULTS) {
                $response = [
                    'error' => true,
                    'message' => $this->__('Too many results, please refine your search further'),
                ];
            } else {
                $response = '';
                $validity = false;

                // if ServiceAddress, check if it is valid
                $checkServiceAddressValidity = $this->checkServiceAddressValidity($params, $requestParams, $validity, $serviceAddress, $response);

                $validity = $checkServiceAddressValidity['validity'];
                $response = $checkServiceAddressValidity['response'];

                $result = $this->checkIfValidateExactAddress($params, $results);

                $response = $this->setResponseForValidateAddress($response, $results, $result);

                $this->setAddressIfValid($validity, $results);
            }
        } catch (InvalidArgumentException $e) {
            $response = [
                'error' => true,
                'message' => $this->__('An error occurred while validating the address. Please try again, or contact the VF Helpdesk.') .
                    ($e->statusReasonCode != Dyna_Service_Model_Client::NO_ERROR_CODE ? '<br/>' . $e->statusReasonCode : ''),
            ];
        } catch (Exception $e) {
            $response = [
                'error' => true,
                'message' => $this->__('An error occurred while validating the address. Please try again, or contact the VF Helpdesk.') .
                    ($e->statusReasonCode != Dyna_Service_Model_Client::NO_ERROR_CODE ? '<br/>' . $e->statusReasonCode : ''),
            ];
        }

        $this->jsonResponse($response);
    }

    public function checkServicesAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }

        // Building request data
        $postData = $this->getRequest()->getPost();
        try {
            /** @var Dyna_Address_Model_Storage $sessionAddress */
            $sessionAddress = Mage::getSingleton('dyna_address/storage');
            /** @var Dyna_Configurator_Helper_Data $configuratorHelper */
            $configuratorHelper = Mage::helper('dyna_configurator');

            $clearCartConfirmed = !empty($postData['clearCartConfirmed']) ? $postData['clearCartConfirmed'] : false;

            /**
             * @var $addressHelper Dyna_Address_Helper_Data
             */
            $addressHelper = Mage::helper('dyna_address');
            $warnAboutAddressChanged = $addressHelper->warnAboutAddressChanged($postData);

            if($warnAboutAddressChanged && !$clearCartConfirmed) {
                $response = [
                    'error' => false,
                    'requireConfirmation' => true,
                    'message' => $this->__("Please note that by continuing packages that require serviceability check will be removed from cart.")];
                $this->jsonResponse($response);
                return;
            }

            /** @var Dyna_Address_Model_Client_CheckServiceAbilityClient $client */
            $client = Mage::getModel("dyna_address/client_CheckServiceAbilityClient");
            $response = $client->executeCheckServiceAbility($postData);

            $serviceAbility = $sessionAddress->getServiceAbility();
            if ($addressId = $postData['addressId'] && isset($postData['setServiceAddressId'])) {
                $addressParts = array($postData['postalcode'], $postData['street'], $postData['buildingNr'], $postData['houseAddition'], $postData['city']);
                $sessionAddress->addCustomerAddressId($addressParts, $addressId);
            }
            $response['customerHasKipInstalled'] = $configuratorHelper->customerHasServiceInstalled(Dyna_Customer_Model_Client_RetrieveCustomerInfo::CABLE_PRODUCT_CATEGORY_KIP);
            $response['customerHasKudInstalled'] = $configuratorHelper->customerHasServiceInstalled(Dyna_Customer_Model_Client_RetrieveCustomerInfo::CABLE_PRODUCT_CATEGORY_CLS);
            // OMNVFDE-3773 If the address is changed and there are in the cart packages that require serviceability =>  clear all cart & notify Agent that cart is deleted
            $response['clearCart'] = false;
            if ($warnAboutAddressChanged) {
                $response['clearCart'] = true;
                /** @var Dyna_Configurator_Helper_Cart $helper */
                $helper = Mage::helper('dyna_configurator/cart');
                $responseEmptyCart = $helper->removePackagesWithServiceability();
                if (!$responseEmptyCart['error']) {
                    $response['clearCartMessage'] = $this->__("The address has changed and packages that required serviceability were removed!");
                    $response['removedPackageIds'] = $responseEmptyCart['removedPackageIds'];
                    $response['rightBlock'] = Mage::getSingleton('core/layout')->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/partials/package_block.phtml')->toHtml();
                } else {
                    $response['clearCartMessage'] = $this->__("There was a problem clearing the shopping cart! The address has changed and, since products from cart required serviceability, the current cart should have been deleted!");
                }
            }
            $serviceAbility['customerHasKipInstalled'] = $response['customerHasKipInstalled'];
            $serviceAbility['customerHasKudInstalled'] = $response['customerHasKudInstalled'];
            $sessionAddress->setServiceAbility($serviceAbility);

            if (isset($postData['myProductsCheck']) && $postData['myProductsCheck']) {
                $response['cartTotals'] = Mage::getSingleton('core/layout')->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/totals.phtml')->toHtml();
            }

            $response['error'] = false;
        } catch (InvalidArgumentException $e) {
            $response = [
                'error' => true,
                'message' => $e->getMessage(),
            ];
        } catch (Mage_Core_Exception $e) {
            $response = [
                'error' => true,
                'message' => $e->getMessage(),
            ];
        } catch (Exception $e) {
            $response = [
                'error' => true,
                'message' => $e->getMessage(),
            ];
        }

        $this->jsonResponse($response);
    }

    public function checkMigrationAddressAction()
    {
        $address = $this->getRequest()->getPost();

        try {
            $response = Mage::helper('dyna_address')->getMigrationAvailabilityForAddress($address);
            $response['error'] = false;
        } catch (InvalidArgumentException $e) {
            $response = [
                'error' => true,
                'message' => $e->getMessage(),
            ];
        } catch (Exception $e) {
            $response = [
                'error' => true,
                'message' => $e->getMessage(),
            ];
        }

        $this->jsonResponse($response);
    }

    public function clearServicesAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }

        $confirmation = $this->getRequest()->getParam("confirmation");
        /** @var Dyna_Package_Helper_Data $packageHelper */
        $packageHelper = Mage::helper('dyna_package');
        // Confirmation needed for renewing service address because it will delete all packages
        if (!$confirmation && $packageHelper->hasServiceAbilityRequiredPackages()) {
            $response = [
                'error' => true,
                'requireConfirmation' => true,
                'message' => $this->__("Please note that by continuing packages that require serviceability check will be removed from cart."),
            ];
            $this->jsonResponse($response);
        } else {
            /** @var Dyna_Checkout_Model_Sales_Quote $quote */
            $quote = Mage::getSingleton('checkout/cart')->getQuote();
            $triggerRefresh = false;
            foreach($quote->getCartPackages() as $package){
                if (($package->getServiceAddressId() || $package->getServiceAddressData()) && (!$package->getEditingDisabled())) {
                    Mage::helper('dyna_package')->removePackage(null, null, $package->getId());
                    // if at least one package has been deleted, send a flag to frontend to trigger page reload
                    $triggerRefresh = true;
                }
            }

            // collect totals
            Mage::getSingleton('checkout/cart')->save();
            // Go further with renewing service address
            $this->getStorage()->unsServiceAbility();
            $this->getStorage()->unsAddress();
            $response = [
                'error' => false,
                'message' => $this->__("Ready for new serviceability check"),
                'triggerRefresh' => $triggerRefresh,
            ];

            $this->jsonResponse($response);
        }
    }

    /**
     * @return Dyna_Address_Model_Storage
     */
    protected function getStorage()
    {
        return Mage::getSingleton('dyna_address/storage');
    }

    public function addressSearchAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }

        $params = [];
        $params['city'] = $this->getRequest()->getPost('city') ? $this->getRequest()->getPost('city') : null;
        $params['postcode'] = $this->getRequest()->getPost('postcode') ? $this->getRequest()->getPost('postcode') : null;
        $params['street'] = $this->getRequest()->getPost('street') ? $this->getRequest()->getPost('street') : null;

        try {
            $module = Mage::getModel("dyna_address/client_validateAddressClient");
            $response = $module->executeValidateAddress($params);
            $response['error'] = false;
        } catch (InvalidArgumentException $e) {
            $response = [
                'error' => true,
                'message' => $e->getMessage(),
            ];
        } catch (Exception $e) {
            $response = [
                'error' => true,
                'message' => $e->getMessage(),
            ];
        }

        $this->jsonResponse($response);
    }

    public function validateAction()
    {
        $params = [];
        $request = $this->getRequest();

        $params['gender'] = $request->getPost('gender', null);
        $params['prefix'] = $request->getPost('prefix', null);
        $params['firstname'] = $request->getPost('firstname', null);
        $params['lastname'] = $request->getPost('lastname', null);
        $params['city'] = $request->getPost('city', null);
        $params['postcode'] = $request->getPost('postcode', null);
        $params['street'] = $request->getPost('street', null);
        $params['district'] = $request->getPost('district', null);
        $params['houseno'] = $request->getPost('houseno', null);
        $params['addition'] = $request->getPost('addition', null);
        $params['type'] = $request->getPost('type', null);
        $params['package_id'] = $request->getPost('package_id', null);

        try {
            /** @var Dyna_Address_Model_Client_ValidateAddressClient $module */
            $module = Mage::getModel("dyna_address/client_validateAddressClient");
            $addressValidation = $module->isValidAddress($params);
            if(!isset($addressValidation['error'])) {
                $response['valid'] = $addressValidation;
                $response['error'] = false;
            } else {
                $response = [
                    'valid' => false,
                    'error' => true,
                    'message' => $addressValidation['message'],
                ];
            }
        } catch (InvalidArgumentException $e) {
            $response = [
                'valid' => false,
                'error' => true,
                'message' => $this->__('An error occurred while validating the address. Please try again, or contact the VF Helpdesk.') .
                    ($e->statusReasonCode != Dyna_Service_Model_Client::NO_ERROR_CODE ? '<br/>' . $e->statusReasonCode : ''),
            ];
        } catch (Exception $e) {
            $response = [
                'valid' => false,
                'error' => true,
                'message' => $this->__('An error occurred while validating the address. Please try again, or contact the VF Helpdesk.') .
                    ($e->statusReasonCode != Dyna_Service_Model_Client::NO_ERROR_CODE ? '<br/>' . $e->statusReasonCode : ''),
            ];
        }

        $this->jsonResponse($response);
    }
}
