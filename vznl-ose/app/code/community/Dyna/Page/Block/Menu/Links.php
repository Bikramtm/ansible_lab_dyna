<?php

/**
 * Class Dyna_Page_Block_Menu_Links
 */
class Dyna_Page_Block_Menu_Links extends Mage_Core_Block_Template
{
    /**
     * Dyna_Page_Block_Menu_Links constructor.
     * @param array $args
     */
    function __construct(array $args = array()) {

        parent::__construct($args);
        $this->validate();
    }

    /**
     *
     */
    protected function validate()
    {
        $agent = Mage::getSingleton('customer/session')->getAgent();
        if (is_null($agent) || ($agent instanceof Mage_Core_Block_Template)) {
            Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl('agent/account/logout'));
            return;
        }
    }
}