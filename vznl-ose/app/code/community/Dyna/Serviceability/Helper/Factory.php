<?php

use GuzzleHttp\Client;

class Dyna_Serviceability_Helper_Factory extends Mage_Core_Helper_Abstract
{
    public static function create()
    {
        return new Dyna_Serviceability_Helper_Data(new Client());
    }
}
