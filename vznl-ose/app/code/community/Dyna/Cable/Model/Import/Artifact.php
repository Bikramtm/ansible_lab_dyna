<?php

/**
 * Class Dyna_Cable_Model_Import_Artifact
 */
class Dyna_Cable_Model_Import_Artifact extends Mage_Core_Model_Abstract
{
    /** @var  Dyna_Import_Helper_Data $_helper */
    protected $_helper;
    protected $_debug = false;
    protected $logFileName = "cable_artifact_import";
    protected $path;
    public $_totalFileRows = 1;
    public $_skippedFileRows;

    public function __construct()
    {
        $this->_helper = Mage::helper("dyna_import");
        $this->_helper->setImportLogFile($this->logFileName . '.log');
    }

    public function import()
    {
        $artifactPharPath = Mage::helper('dyna_configurator')->getArtifactPath();

        try {

            if ($this->path) {

                if (!is_readable($this->path)) {
                    $msg = 'Invalid path or path not readable: ' . $this->path;
                    $this->_logError($msg . PHP_EOL);
                    $this->_helper->postToSlack($msg);
                    throw new InvalidArgumentException('Invalid path or path not readable');
                }

                $phar = new \Phar($this->path);
                unset($phar);

                $fileUpdated = copy($this->path, $artifactPharPath);
                if (!$fileUpdated) {
                    $this->_logError('Could not write to artifact file %s', $artifactPharPath);
                    throw new RuntimeException(sprintf('Could not write to artifact file %s', $artifactPharPath));
                } else {
                    $this->_log('Artifact file ' . $artifactPharPath . ' updated' . PHP_EOL);
                }
                return;
            }

        } catch (PharException $e) {
            $this->_skippedFileRows = 1;
            $msg = 'Invalid source phar file: ' . $this->path;
            $this->_logError( $msg . PHP_EOL);
            $this->_helper->postToSlack($msg);
            print('Invalid source phar file' . PHP_EOL);
        } catch (Exception $e) {
            $this->_skippedFileRows = 1;
            print($e->getMessage() . PHP_EOL);
            return;
        }
    }

    public function setPath($path) {
        $this->path = $path;
    }

    /**
     * Log Error function
     *
     * @param string $msg
     */
    public function _logError($msg)
    {
        $this->_helper->logMsg($msg);
    }

    /**
     * Log function
     *
     * @param string $msg
     * @param bool $verbose
     */
    public function _log($msg, $verbose = false)
    {
        if($verbose && !$this->_debug) {
            return;
        }
        $this->_helper->logMsg($msg);
    }
}
