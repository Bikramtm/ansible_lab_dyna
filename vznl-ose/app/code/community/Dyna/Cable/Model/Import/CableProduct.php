<?php

/**
 * Class Dyna_Cable_Model_Import_CableProduct
 */
class Dyna_Cable_Model_Import_CableProduct extends Dyna_Import_Model_Import_Product_Abstract
{
    /** @var string $_multiselectSplit */
    protected $_multiselectSplit = ",";
    /** @var string $_csvDelimiter */
    protected $_csvDelimiter = ',';
    /** @var string $_logFileName */
    protected $_logFileName = "cable_product_import";

    /**
     * Dyna_Import_Model_Import_CableProduct constructor.
     */
    public function __construct()
    {
        parent::__construct();
        
        $this->_helper->setImportLogFile($this->_logFileName . '.' . $this->_logFileExtension);
        $this->_qties = 0;
    }

    /**
     * Import cable product
     *
     * @param array $data
     * @return void
     */
    public function importProduct($data)
    {
        $skip = false;
        $this->_totalFileRows++;
        //todo check mapping file
        $data = $this->_setDataMapping($data);

        if (!array_key_exists('sku', $data) || empty($data['sku'])) {
            $this->_logError('Skipping line without sku');
            $skip = true;
        } elseif (!array_key_exists('name', $data) || empty($data['name'])) {
            $this->_logError('Skipping line without name');
            $skip = true;
        }

        if ($skip) {
            $this->_skippedFileRows++;
            return;
        }

        $data['sku'] = trim($data['sku']);
        $this->_currentProductSku = $data['sku'];
        $this->_log('Start importing ' . $data['name']);
        $data = $this->checkPrice($data);

        try {
            /** @var Mage_Catalog_Model_Product $product */
            $product = Mage::getModel('catalog/product');
            $id = $product->getIdBySku($data['sku']);
            $new = ($id == 0);
            if (!$new) {
                $this->_log('Loading existing product"' . $data['sku'] . '" with ID ' . $id);
                $product->load($id);
            }
            $skip = false;
            if (!$this->_setAttributeSetByCode($product, $data)) {
                $this->_logError('Unknown attribute set "' . $data['attribute_set'] . '" for product ' . $data['sku']);
                $skip = true;
            } elseif (!$this->_setWebsitesByCsvString($product, $data)) {
                $this->_logError('Unknown website "' . $data['_product_websites'] . '" for product ' . $data['sku']);
                $skip = true;
            } elseif (!$this->_setTaxClassIdByName($product, $data)) {
                $this->_logError((empty($data['tax_class_id']) ? 'Empty vat value' : 'Unknown vat value "' . $data['tax_class_id'] . '"') . ' for product ' . $data['sku']);
                $skip = true;
            }

            if ($skip) {
                $this->_skippedFileRows++;
                return;
            }

            $this->_setDefaults($product, $data);
            $this->_setStatus($product, $data);
            $this->_setProductData($product, $data);

            if ($new) {
                $this->_createStockItem($product);
            }

            if (!$this->getDebug()) {
                $product->save();
            }

            if (!$this->getDebug()) {
                $productVisibilityInfo = @json_decode($product->getProductVisibilityStrategy(), true);
                if ($productVisibilityInfo) {
                    Mage::getModel('productfilter/filter')->createRule($product->getId(), $productVisibilityInfo['strategy'], $productVisibilityInfo['groups']);
                } else {
                    //uncomment the following line if products that have no defined filters must be treated as available for all groups
                    //Mage::getModel('productfilter/filter')->createRule($product->getId(), Dyna_ProductFilter_Model_Filter::STRATEGY_INCLUDE_ALL, null); //available for all dealer groups
                }
            }

            unset($product);
            $this->_qties++;

            $this->_log('Finished importing ' . $data['name']);
        } catch (Exception $ex) {
            echo $ex->getMessage();
            $this->_logEx($ex);
        }
    }

    /**
     * Process custom data mapping for cable products
     *
     * @param array $data
     * @return array
     */
    protected function _setDataMapping($data)
    {
        // SKU format as specified in CAT-11_Product Catalogue HLD_version_2.4
        $data['sku'] = trim($data['product_code'] . '-' . $data['promotion_code'], '-');
        // Default Cable attribute set
        $data['attribute_set'] = 'KD_Cable_Products';
        // Default telesales website
        $data['_product_websites'] = 'telesales';
        // None
        $data['tax_class_id'] = 'Taxable Goods';

        $data['package_subtype'] = $this->_getPackageSubtype($data['type']);
        $data['price'] = $data['price_once'];
        $data['status'] = (empty($data['portal_sequence_no'])) ? 0 : 1;
        if ($data['portal_state_initial'] === "X") {
            $data['portal_state_initial'] = "X (not selectable)";
        }
        // attributes with code length > 30 chars
        $mappedAttributes = [
            'portal_variable_billing_frequency' => 'portal_variable_billing_freq',
            'required_rate_product_min_bound' => 'req_rate_min_bound',
            'required_rate_product_max_bound' => 'req_rate_max_bound',
            'required_rate_product_products_for_contracts' => 'req_rate_prod_for_contracts',
        ];

        foreach ($mappedAttributes as $importKey => $dbKey) {
            if (isset($data[$importKey])) {
                // attribute code length > 30 chars
                $data[$dbKey] = $data[$importKey];
                unset($data[$importKey]);
            }
        }

        /** Get value for checkout product attribute */
        $data['checkout_product'] = $this->getCheckoutProductValue($data);

        if (isset($data['product_visibility'])) {
            $data['product_visibility'] = $this->parseProductVisibility($data['product_visibility']);
        }

        return $data;
    }

    /**
     * Determine product package_subtype based on product['type'] value in CSV
     *
     * @param string $type
     * @return null|string
     */
    protected function _getPackageSubtype($type)
    {
        switch (strtolower($type)) {
            case 'bundle':
            case 'container':
            case 'product':
                $packageSubtype = 'Tariff';
                break;
            case 'device':
                $packageSubtype = 'Hardware';
                break;
            case 'option':
                $packageSubtype = 'Option';
                break;
            case 'accessory':
                $packageSubtype = 'Accessory';
                break;
            case 'service':
            case 'fee':
            case 'skip':
            case 'memo':
            case 'info':
            case 'feature':
            case 'virtual':
            case 'interest':
                $packageSubtype = 'Setting';
                break;
            default:
                $packageSubtype = null;
                break;
        }

        return $packageSubtype;
    }

    /**
     * Set product model default values
     *
     * @param Mage_Catalog_Model_Product $product
     * @param array $data
     */
    protected function _setDefaults($product, $data)
    {
        $product->setTypeId(Mage_Catalog_Model_Product_Type::TYPE_SIMPLE);
        // This will format itself trough observer
        $product->setUrlKey($data['name']);
        $product->setProductVisibilityStrategy('all');
        $product->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH);
    }

    /**
     * Checks whether or not price is listed in import file (can be 0, but needs to exists)
     */
    protected function checkPrice($data)
    {
        if (!isset($data['price']) || $data['price'] === "") {
            $data['price'] = 0;
            $this->_log("[NOTICE] Did not receive any value for price column. Setting price to 0");
        }

        return $data;
    }

    /**
     * Method to parse the value of the product visibility attributes
     * @param $value
     */
    protected function parseProductVisibility($values){
        $markedOptions = [];
        $markedOptionIds = [];
        $options = [];

        $attribute = Mage::getSingleton('eav/config')
            ->getAttribute(Mage_Catalog_Model_Product::ENTITY, Dyna_Catalog_Model_Product::CATALOG_PACKAGE_PRODUCT_VISIBILITY_ATTR);
        if ($attribute->usesSource()) {
            $options = $attribute->getSource()->getAllOptions(false);
        }

        if ('*' == $values) {
            $markedOptionIds = array_diff(
                array_column($options, 'label'),
                [Dyna_Catalog_Model_Product::CATALOG_PRODUCT_VISIBILITY_OPTION_NOT_SHOWN]
            );
        } else {
            $values = explode(',', $values);
            foreach ($values as $value) {
                $markedOptions[] = strtolower(str_replace(' ', '', $value));
            }
            foreach ($options as $option) {
                if (in_array(strtolower(str_replace(' ', '', $option['label'])), $markedOptions)) {
                    $markedOptionIds[] = $option['label'];
                }
            }
        }

        return implode(',', $markedOptionIds);
    }

    /**
     * Determine checkout product attribute value
     *
     * @param array $data
     * @return null|string
     */
    private function getCheckoutProductValue($data)
    {
        if(isset($data['checkout_product']) && $data['checkout_product']) {
            return ('yes' == strtolower($data['checkout_product'])) ? 'Yes' : 'No';
        }
        return 'No';
    }
}
