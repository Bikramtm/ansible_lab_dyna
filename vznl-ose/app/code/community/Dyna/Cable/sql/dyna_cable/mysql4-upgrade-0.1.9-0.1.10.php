<?php
$installer = $this;
/* @var $installer Dyna_Cable_Model_Resource_Setup */

$installer->startSetup();

$attributeId = $installer->getAttribute(Mage_Catalog_Model_Product::ENTITY, 'package_type', 'attribute_id');

$data['attribute_id'] = $attributeId;
$data['values'] = [
    'CABLE_INTERNET_PHONE',
    'CABLE_INDEPENDENT',
    'CABLE_TV',
];

$installer->addAttributeOption($data);

$installer->endSetup();
