<?php
/**
 * Installer that adds new attributes according to Cable proposal.docx (see OMNVFDE-133)
 */

$installer = $this;
/* @var $installer Dyna_Cable_Model_Resource_Setup */

$installer->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

$cableSet = 'KD_Cable_Products';

$attrSetId = $installer->getAttributeSetId($entityTypeId, $cableSet);

$generalAttributes = [
    'relation_alternatives' => [
        'label' => 'Relation Alternatives',
        'input' => 'text',
        'type' => 'varchar',
    ],
];

$sortOrder = 100;
foreach ($generalAttributes as $code => $options) {
    $attributeId = $installer->getAttributeId($entityTypeId, $code);
    if (!$attributeId) {
        $options['user_defined'] = 1;
        $installer->addAttribute($entityTypeId, $code, $options);
        $installer->addAttributeToSet($entityTypeId, $attrSetId, 'Cable', $code, $sortOrder);
        $sortOrder++;
    }
}

$installer->endSetup();
