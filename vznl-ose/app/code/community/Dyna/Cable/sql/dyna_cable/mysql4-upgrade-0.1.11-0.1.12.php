<?php
// update the tier_code attribute type: instead of price use text
$installer = $this;
/* @var $installer Dyna_Cable_Model_Resource_Setup */

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

// Update attributes
$newOptions = [
    'input' => 'text',
    'type' => 'text',
    'frontend_input' => 'text',
    'backend_type' => 'text'
];

$attributeId = $installer->getAttribute($entityTypeId, 'tier_code', 'attribute_id');
if ($attributeId) {
    $installer->updateAttribute($entityTypeId, $attributeId, $newOptions);
}

$installer->endSetup();
