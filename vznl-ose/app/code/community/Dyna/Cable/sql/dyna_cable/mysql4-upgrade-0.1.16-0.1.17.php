<?php
$installer = $this;
/* @var $installer Dyna_Cable_Model_Resource_Setup */

$installer->startSetup();

// Get catalog_product entity id
$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

$cableSet = 'KD_Cable_Products';

// Get KD attribute set id
$attrSetId = $installer->getAttributeSetId($entityTypeId, $cableSet);

$newCableAttribute = [
    'display_name_configurator' => [
        'label' => 'Display name in configurator',
        'input' => 'text',
        'type' => 'varchar',
        'note' => 'The display name used in configurator for this product',
    ],
    'display_name_basket' => [
        'label' => 'Display name in basket',
        'input' => 'text',
        'type' => 'varchar',
        'note' => 'The display name used in shopping cart for this product',
    ],
    'display_name_overview' => [
        'label' => 'Display name in overview',
        'input' => 'text',
        'type' => 'varchar',
        'note' => 'The display name used in overview for this product',
    ],
    'display_name_printout' => [
        'label' => 'Display name in printout',
        'input' => 'text',
        'type' => 'varchar',
        'note' => 'The display name used in printout for this product',
    ],
    'display_name_inventory' => [
        'label' => 'Display name in inventory',
        'input' => 'text',
        'type' => 'varchar',
        'note' => 'The display name used in inventory for this product',
    ],
];

$sortOrder = 200;
foreach ($newCableAttribute as $code => $options) {
    $attributeId = $installer->getAttributeId($entityTypeId, $code);
    if (!$attributeId) {
        $options['user_defined'] = 1;
        $installer->addAttribute($entityTypeId, $code, $options);
        $installer->addAttributeToSet($entityTypeId, $attrSetId, 'Cable', $code, $sortOrder);
        $sortOrder++;
    }
}

$installer->endSetup();
