<?php

/**
 *Add price_repeated attribute back to Cable Product. This was removed in Catalog/Data/dyna_catalog/data-upgrade-0.2.19-0.2.20.php
 */

$installer = $this;
/* @var $installer Dyna_Cable_Model_Resource_Setup */

$installer->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

// Create KD_Cable_Products attribute set
$attributeSetName = 'KD_Cable_Products';

// Create attributes
$attributes = [
    'price_repeated' => [
        'attribute_set' => $attributeSetName,
        'group' => 'Cable',
        'label' => 'Price repeated',
        'input' => 'text',
        'type' => 'text',
        'note' => 'Defines prices for billing frequency and priceSliding (p.e. : price_billingfrequency=1, price_sliding=2:12:, price_repeated=0:24:39 --> first 2 month no costs then 12 months 24 EUR and then 39 EUR per months',
    ]
];

$createAttributeSets = [];
// Create the attributes
foreach ($attributes as $code => $options) {
    $attributeSet = $options['attribute_set'];
    $group = $options['group'];
    unset($options['group'], $options['attribute_set']);

    $attributeId = $installer->getAttributeId($entityTypeId, $code);
    if (! $attributeId) {
        $options['user_defined'] = 1;
        $installer->addAttribute($entityTypeId, $code, $options);
    }

    $createAttributeSets[$attributeSet]['groups'][$group][] = $code;
}
$sortOrder = 14;
$installer->assignAttributesToSet($createAttributeSets, $sortOrder);
unset($attributes, $createAttributeSets);

$installer->endSetup();
