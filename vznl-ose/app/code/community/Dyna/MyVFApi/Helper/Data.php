<?php

class Dyna_MyVFApi_Helper_Data
{
    /**
     * @param $quote
     * @param string|array $ctn
     * @return Dyna_MyVFApi_Model_Projection
     */
    public function buildProjectionFromQuote(Dyna_Checkout_Model_Sales_Quote $quote, $ctn = ''): Dyna_MyVFApi_Model_Projection
    {
        $ctn = $ctn ?: Mage::getSingleton('customer/session')->getCustomer()->getCtn(true);

        /** @var Dyna_Superorder_Model_Offer $offerModel */
        $offerModel = Mage::getModel('dyna_superorder/offer');
        $quoteToRedis = $offerModel->extractOfferData($quote);

        $jobSessionData = $this->getJobSessionData();
        $jobData = array_merge($jobSessionData, [
            'quote_id' => $quote->getId(),
            'superorder_id' => null,
            'agent_id' => Mage::getSingleton('customer/session')->getAgent()->getId(),
        ]);

        $job = Mage::getModel('dyna_job/jobs_submitOrder');
        $job->setData($jobData);

        /** @var Dyna_MyVFApi_Model_Projection $projection */
        $projection = Mage::getModel('dyna_myvfapi/projection', [
            'id' => $quote->getId(),
            'offerData' => $quoteToRedis,
            'jobData' => $job,
            'ctn' => $ctn,
            'ban' => Mage::getSingleton('customer/session')->getCustomer()->getBan()
        ]);
        if($quote->getCampaignCode()) {
            $projection->setCampaign($quote->getCampaignCode());
        }

        return $projection;
    }

    /**
     * This method is used when we want to delete an offer. We don't need the offer's data for this action
     * @param $quote
     * @param string|array $ctn
     * @return Dyna_MyVFApi_Model_Projection
     */
    public function buildProjectionFromQuoteWithoutExtractingOfferData(Dyna_Checkout_Model_Sales_Quote $quote, $ctn = ''): Dyna_MyVFApi_Model_Projection
    {
        $ctn = $ctn ?: Mage::getSingleton('customer/session')->getCustomer()->getCtn(true);
        $jobSessionData = $this->getJobSessionData();
        $jobData = array_merge($jobSessionData, [
            'quote_id' => $quote->getId(),
            'superorder_id' => null,
            'agent_id' => Mage::getSingleton('customer/session')->getAgent()->getId(),
        ]);

        $job = Mage::getModel('dyna_job/jobs_submitOrder');
        $job->setData($jobData);

        /** @var Dyna_MyVFApi_Model_Projection $projection */
        $projection = Mage::getModel('dyna_myvfapi/projection', [
            'id' => $quote->getId(),
            'jobData' => $job,
            'ctn' => $ctn,
            'ban' => Mage::getSingleton('customer/session')->getCustomer()->getBan()
        ]);

        if($quote->getCampaignCode()) {
            $projection->setCampaign($quote->getCampaignCode());
        }

        return $projection;
    }

    protected function getJobSessionData()
    {
        $data = [
            'checkout' => Mage::getSingleton('checkout/session')->getData(),
            'customer' => Mage::getSingleton('customer/session')->getData(),
            'address' => Mage::getSingleton('dyna_address/storage')->getData(),
        ];

        unset($data['customer']['agent']);
        $data = array_map('serialize', $data);
        return array_map('base64_encode', $data);
    }

}