<?php

class Dyna_MyVFApi_Helper_Credis
{
    public function buildCredisClient($customOptions = []): Credis_Client
    {
        $options = [];
        if (Mage::getConfig()->getNode('global/cache')) {
            $options = Mage::getConfig()->getNode('global/cache')->asArray();
            $options = $options['backend_options'];
            $options['database'] = Mage::getStoreConfig('omnius_service/my_vodafone_api_credentials/vfde_api_db');
        }

        $options = array_merge($options, $customOptions);


        $port = $options['port'] ?? null;
        $timeout = $options['timeout'] ?? 2.5;
        $persistent = $options['persistent'] ?? '';
        $redis = new Credis_Client($options['server'], $port, $timeout, $persistent);

        if (!empty($options['force_standalone'])) {
            $redis->forceStandalone();
        }

        $connectRetries = (int)$options['connect_retries'] ?? 1;
        $redis->setMaxConnectRetries($connectRetries);

        if (!empty($options['read_timeout']) && $options['read_timeout'] > 0) {
            $redis->setReadTimeout((float)$options['read_timeout']);
        }

        if (!empty($options['password'])) {
            if (!$redis->auth($options['password'])) {
                throw new CredisException('Unable to authenticate with the redis server.');
            }
        }

        if (empty($options['database'])) {
            $options['database'] = 0;
        }
        if (!$redis->select((int)$options['database'])) {
            throw new CredisException('The redis database could not be selected.');
        }

        return $redis;
    }
}