<?php

class Dyna_MyVFApi_Model_Projection
{
    /**
     * @var quote_id
     */
    protected $id;

    protected $offerData;

    protected $jobData;

    protected $ban;

    protected $ctn;

    protected $campaign = null;

    /** @var array Magento */
    protected $requiredParameters = ['id', 'jobData'];

    public function __construct($arguments)
    {
        foreach ($this->requiredParameters as $parameter) {
            if (empty($arguments[$parameter])) {
                throw new InvalidArgumentException(sprintf('Dyna_MyVFApi_Model_Projection requires an id parameter'));
            }
        }

        $this->id = $arguments['id'];
        $this->offerData = $arguments['offerData'] ?? [];
        $this->jobData = $arguments['jobData'];

        $this->ban = $arguments['ban'] ?? null;
        $this->ctn = $arguments['ctn'] ?? null;
    }

    public function setCampaign($campaignId)
    {
        $this->campaign = $campaignId;

        return $this;
    }

    public function getCampaign()
    {
        return $this->campaign;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getOfferData()
    {
        return $this->offerData;
    }

    /**
     * @return mixed
     */
    public function getJobData()
    {
        return $this->jobData;
    }

    public function getCtn()
    {
        return $this->ctn;
    }

    public function getBan()
    {
        return $this->ban;
    }

}