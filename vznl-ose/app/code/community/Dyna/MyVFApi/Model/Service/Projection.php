<?php

/**
 * Class Dyna_MyVFApi_Model_Service_Projection
 */
class Dyna_MyVFApi_Model_Service_Projection
{
    protected $credisClient;

    const TAG_BAN = "ban:";
    const TAG_CUSTOMER_NO = "customer:";
    const TAG_OFFERS = "offers:";
    const TAG_JOBS = "jobs:";
    const TAG_CAMPAIGN = "campaign:";

    public function __construct($arguments)
    {
        if (empty($arguments['credisClient'])) {
            throw new InvalidArgumentException('Dyna_MyVFApi_Model_Service_Offer requires a credisClient argument');
        }

        $this->credisClient = $arguments['credisClient'];
    }

    public function create(Dyna_MyVFApi_Model_Projection $projection)
    {
        $offerRedisKey = self::TAG_OFFERS . $projection->getId();
        $offerJobRedisKey = self::TAG_JOBS . $projection->getId();

        $dbRepo = Mage::getModel('dyna_job/jobRepository_database');

        $jobHash = $dbRepo->storeProjection($projection);

        $this->credisClient->set($offerRedisKey, serialize($projection->getOfferData()));

        $this->credisClient->set($offerJobRedisKey, $jobHash);

        Mage::dispatchEvent('dyna_myvfapi_projection_created', ['projection' => $projection]);
    }

    public function delete(Dyna_MyVFApi_Model_Projection $projection)
    {
        $projectionID = $projection->getId();
        $offerRedisKey = self::TAG_OFFERS . $projectionID;
        $offerJobRedisKey = self::TAG_JOBS . $projectionID;

        // delete quote data from redis
        $this->credisClient->del($offerRedisKey);
        $this->credisClient->del($offerJobRedisKey);

        Mage::dispatchEvent('dyna_myvfapi_projection_deleted', ['projection' => $projection]);
    }
}