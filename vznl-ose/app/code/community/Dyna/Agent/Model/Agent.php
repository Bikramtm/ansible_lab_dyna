<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Agent_Model_Agent
 */
class Dyna_Agent_Model_Agent extends Mage_Core_Model_Abstract
{
    // How much the token is available in seconds
    const RESET_TOKEN_DURATION = 'agent/forgot_credentials/token_duration';
    // How much the password is available
    const PASSWORD_EXPIRY_DURATION = 'agent/logout_configuration/password_expiry_duration';
    // Until the agent can reset his password again
    const PASSWORD_NEXT_ATTEMPT_DELAY = 'agent/forgot_credentials/next_attempt_delay';
    const MAX_TOKENS = 'agent/forgot_credentials/max_tokens';
    const CREDENTIALS_EMAIL_TEMPLATE = 'agent/forgot_credentials/send_credentials';
    const LOCKED_AGENT = 1338;
    const TEMPORARY_PASSWORD = 1337;
    const AGENT_CHANGE_PACKAGE = 'CHANGE';
    const AGENT_CANCEL_PACKAGE = 'CANCEL';
    const TEMPLATE_ROW_KEY_PASS_RESET = 'AgentResetPassword';
    const LAST_PASSWORDS_CHECK = 10;

    /**
     * Length of salt
     */
    const HASH_SALT_LENGTH = 32;

    protected function _construct()
    {
        $this->_init("agent/agent");
    }

    /**
     * Retrieve encoded password
     *
     * @param string $password
     * @return string
     */
    protected function _getEncodedPassword($password)
    {
        return Mage::helper('core')->getHash($password, self::HASH_SALT_LENGTH);
    }

    /**
     * @param int $id
     * @param null $field
     * @return $this|Mage_Core_Model_Abstract
     */
    public function load($id, $field = null)
    {
        $key = 'agent_' . md5(serialize([$id, $field]));
        if ($product = Mage::objects()->load($key)) {
            $this->setData($product->getData());

            return $this;
        }

        parent::load($id, $field);
        Mage::objects()->save($this, $key);

        return $this;
    }

    /**
     * @param $permission
     * @return bool
     */
    public function isGranted($permission)
    {
        $role = $this->getRole();
        if ($role) {
            if (is_array($permission)) {
                foreach ($permission as $perm) {
                    if (!$role->isGranted($perm)) {
                        return false;
                    }
                }

                return true;
            }

            return $role->isGranted($permission);
        }

        return false;
    }

    /**
     * @return bool
     *
     * Checks if an agent has the rights to log in as another agent
     */
    public function getIsSuperAgent()
    {
        $currentWebsiteChannel = Mage::helper('agent')->getWebsiteChannel();
        $session = Mage::getSingleton('customer/session');

        if ($session->getSuperAgent()) {
            return true;
        }
        $role = $session->getAgent()->getRoleId();
        switch ($currentWebsiteChannel) {
            case Dyna_Agent_Model_System_Config_Channel::WEBSITE_CHANNEL_TELESALES_CODE :
                if ($role == Dyna_Agent_Model_Agentrole::SUPERAGENT_ROLE) {
                    return true;
                }
                break;
            case Dyna_Agent_Model_System_Config_Channel::WEBSITE_CHANNEL_RETAIL_CODE :
                if ($role != Dyna_Agent_Model_Agentrole::BUSINESS_SPECIALIST) {
                    return true;
                }
                break;
            default:
        }

        return false;
    }

    /**
     * Return agent full name
     *
     * @return string
     */
    public function getName()
    {
        return $this->getFirstName() . ' ' . $this->getLastName();
    }

    /**
     * @return $this
     * @throws Exception
     */
    public function forgotPassword()
    {
        $nextAttemptDelay = Mage::getStoreConfig(Dyna_Agent_Model_Agent::PASSWORD_NEXT_ATTEMPT_DELAY) ? : 0;
        if (strtotime($this->getPasswordChanged()) + $nextAttemptDelay > Mage::getModel('core/date')->timestamp(time())) {
            throw new Exception(Mage::helper('agent')->__('You have already changed the password of your account once today.'));
        }

        $tokensGenerated = $this->getTokensGenerated() ? : 0;
        $maxTokensGenerated = Mage::getStoreConfig(Dyna_Agent_Model_Agent::MAX_TOKENS) ? : 0;
        if ($tokensGenerated >= $maxTokensGenerated) {
            $this->setLocked(1);
            $this->save();
            throw new Exception(Mage::helper('agent')->__('The amount of password reset links generated has exceeded the limit, your account will be locked.'));
        }

        $token = $this->generateResetToken();
        $this->setPasswordResetToken($token);
        $this->setTokensGenerated($tokensGenerated + 1);
        $resetTokenDuration = Mage::getStoreConfig(Dyna_Agent_Model_Agent::RESET_TOKEN_DURATION) ? : 0;
        $this->setPasswordResetTokenExpiryDate(Mage::getModel('core/date')->timestamp(time()) + $resetTokenDuration);
        $this->save();
        $url = Mage::getUrl('agent/account/password', ['token' => $token]);
        $this->sendCredentialsMail($url, false);

        return $this;
    }

    public function forgotUsername()
    {
        $username = $this->getUsername();
        $this->sendCredentialsMail($username, true);

        return $this;
    }

    protected function sendCredentialsMail($body, $username = true)
    {
        if (Mage::helper('core')->isModuleEnabled('Dyna_Email')) {
            $nameLists = [['[Username]' => $username ? $body : $this->getName()]];
            if (!$username) {
                $nameLists[] = ['[Password]' => $body];
            }

            try {
                Mage::getSingleton('email/pool')->add(
                    [
                        'email_code' => self::TEMPLATE_ROW_KEY_PASS_RESET,
                        'created_at' => strftime('%Y-%m-%d %H:%M:%S', time()),
                        'deadline' => strftime('%Y-%m-%d %H:%M:%S', time()),
                        'receiver' => $this->getEmail(),
                        'culture_code' => str_replace('_', '-', Mage::app()->getLocale()->getLocaleCode()),
                        'parameters' => $nameLists,
                    ]
                );
            } catch (Exception $e) {
                Mage::getSingleton('core/logger')->logException($e);
            }
        } else {
            // TODO: Create recover password email template and assign it in Backend config
            $vars = ['username' => $username ? $body : $this->getName()];
            if (!$username) {
                $vars['password_reset_link'] = $body;
            }

            try {
                $templateId = Mage::getStoreConfig(self::CREDENTIALS_EMAIL_TEMPLATE);
                $receiverEmail = $this->getEmail();
                $emailTemplate = Mage::getModel('core/email_template')->load($templateId);

                $storeId = Mage::app()->getStore()->getStoreId();

                //If you'd like to view the processed template before sending the email, you print the following function:
                $emailTemplate->getProcessedTemplate($vars);
                //Otherwise, the next steps would be to set the email sender information. In this example we are pulling in our sending info from the Magento store email address configuration under the general contact tab:
                $emailTemplate->setSenderEmail(Mage::getStoreConfig('trans_email/ident_general/email', $storeId));
                $emailTemplate->setSenderName(Mage::getStoreConfig('trans_email/ident_general/name', $storeId));

                //And finally, we send out the email:
                $emailTemplate->send($receiverEmail, $vars['username'], $vars);
            } catch (Exception $e) {
                Mage::logException($e);
            }
        }
    }

    /**
     * Create a random string that will be used to reset the password
     *
     * @return string
     */
    protected function generateResetToken()
    {
        return md5(mt_rand());
    }

    /**
     * @return mixed
     */
    public function getDealerCode()
    {
        if ($this->getId()) {
            if ($this->getData('dealer_code') !== null && Mage::helper('agent')->isTelesalesChannel()) {
                return $this->getData('dealer_code');
            } else {
                return $this->getDealer()->getVfDealerCode();
            }
        } else {
            return $this->getData('dealer_code');
        }
    }

    /**
     * @return Dyna_Agent_Model_Dealer
     */
    public function getDealer()
    {
        if (!$this->getData('dealer')) {
            if ($this->getDealerId()) {
                $this->setData('dealer', Mage::getModel('agent/dealer')->load($this->getDealerId()));
            } elseif (Mage::helper('agent')->isTelesalesChannel() && $this->getData('dealer_code')) {
                $this->setData('dealer', Mage::getModel('agent/dealer')->load($this->getData('dealer_code'), 'dealer_code'));
            }
        }

        //make sure old agents that don't have default dealer don't crash the application
        return $this->getData('dealer') ?: Mage::getModel('agent/dealer');
    }

    /**
     * @return Dyna_Agent_Model_Agentrole
     */
    public function getRole()
    {
        if (!$this->hasData('role') && $this->getRoleId() && ($role = Mage::getModel('agent/agentrole')->load($this->getRoleId(),'role_id')) && $role->getRoleId()) {
            $this->setData('role', $role);
        }

        return $this->getData('role');
    }

    /**
     * Overridden to set password and password expiry date
     * @param $password
     * @return $this
     */
    public function setPassword($password)
    {
        $this->setData('password', $this->_getEncodedPassword($password));
        $this->setPasswordChanged(date('Y-m-d h:i:s', Mage::getModel('core/date')->timestamp(time())));
        $this->setTokensGenerated(null);
        Mage::register('agent_password_changed', true);

        return $this;
    }

    /**
     * Overridden to set the temporary password field at the first save of the agent
     * @return Mage_Core_Model_Abstract
     */
    public function save()
    {
        if ($this->getId() == null) {
            $this->setTemporaryPassword(1);
        }

        return parent::save();
    }

    /**
     * Checks last 10 used passwords against a string. Returns false in case it was used in the last 10 passwords.
     * @param $password
     * @return bool
     */
    public function checkLastTenPasswords($password)
    {
        if ($this->getId()) {

            $historyLimit = self::LAST_PASSWORDS_CHECK;
            // check if the module passwordHash is enabled
            if (Mage::helper('core')->isModuleEnabled('Dyna_PasswordStrength')) {
                $historyLimit = Mage::getStoreConfig('dynapasswordstrength/password/history_limit');
            }

            $lastTenPasswords = Mage::getModel('agent/agentpassword')->getCollection()
                ->addFieldToFilter('agent_id', $this->getId())
                ->setOrder('entity_id', 'DESC')
                ->setPageSize($historyLimit)
                ->setCurPage(1);

            if (Mage::helper('core')->isModuleEnabled('Dyna_PasswordHash')) {
                $encryptor = Mage::getModel(
                    'passwordhash/encryption',
                    [Mage::helper('core')->getEncryptor()]
                );
                foreach ($lastTenPasswords as $passwd) {
                    if ($encryptor->validateHash($password, $passwd->getPassword(), $passwd->getHashMethod())) {
                        return false;
                    }
                }
            } else {
                $encryptor = Mage::helper('core')->getEncryptor();

                foreach ($lastTenPasswords as $passwd) {
                    if ($encryptor->validateHash($password, $passwd->getPassword())) {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    /**
     * @return $this
     */
    public function incrementLoginAttempts()
    {
        $config = Mage::getStoreConfig('agent/password_validation/pwd_attempts');
        $finalAttempts = $this->getLoginAttempts() + 1;
        $this->setLoginAttempts($finalAttempts);
        if ($config && $finalAttempts >= $config) {
            $this->setLocked(1);
        }

        if ($this->getId()) {
            $this->save();
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getAgentsInSameGroup()
    {
        $dealerGroups = $this->getDealer()->getDealerGroups()->getAllIds();
        $dealers = Mage::getResourceModel('agent/dealergroup_collection')
            ->addFieldToFilter('group_id', ['in' => $dealerGroups]);

        return Mage::getResourceModel('agent/agent_collection')
            ->addFieldToFilter('dealer_id', ['in' => $dealers->getAllIds()])
            ->getAllIds();
    }

    /**
     * @return array
     */
    public function getAgentsWithSameDealer()
    {
        $conn = Mage::getSingleton('core/resource')->getConnection('core_read');
        $select = $conn->select()
            ->from('agent', ['agent_id'])
            ->where('agent.dealer_id = ?', $this->getData('dealer_id'));

        return array_unique($conn->fetchCol($select));
    }

    /**
     * @return mixed
     */
    public function getWebsiteId()
    {
        return Mage::getModel('core/store')->load($this->getStoreId())->getWebsiteId();
    }

    /**
     * Check if the agent can search failed orders in the telesales Validations tab
     *
     * @return bool
     */
    public function canSearchFailedOrders()
    {
        return $this->isGranted('SEARCH_FAILED_VALIDATIONS_OF_GROUP');
    }

    /**
     * Check if the agent can submit Risk orders for the telesales channel
     *
     * @return bool
     */
    public function canSubmitRiskOrders()
    {
        return $this->isGranted('SUBMIT_RISK_ORDERS_TELESALES');
    }

    /**
     * @return Mage_Core_Model_Abstract|void
     * If agents tried to be saved without username, log the trace to see when this occurs
     */
    public function _beforeSave()
    {
        if (!trim($this->getUsername())) {
            Mage::log(mageDebugBacktrace(true, false, true), null, 'EmptyAgents.log', true);
        }

        if (Mage::registry('agent_password_changed') && Mage::helper('core')->isModuleEnabled('Dyna_PasswordHash')) {
            $this->setData('hash_method', Mage::getStoreConfig('passwordhash/pbkdf2/hash_algorithm'));
        }

        return parent::_beforeSave();
    }

    /**
     * Authenticate Agent
     *
     * @param  string $login
     * @param  string $password
     * @param  bool $superAgent
     * @throws Mage_Core_Exception
     * @return true
     */
    public function authenticate($login, $password = null, $superAgent = false)
    {
        /** @var Dyna_Agent_Model_Mysql4_Agent_Collection $collection */
        $collection = Mage::getModel('agent/agent')->getCollection();

        $collection
            ->addFieldToFilter('username', $login)
            ->addFieldToFilter('is_active', 1)
            ->addFieldToFilter('store_id', Mage::app()->getStore()->getId());

        if (!$superAgent) {
            $collection->load();
            $agent = $collection->fetchItem();

            Mage::dispatchEvent('agent_authenticate_before', [
                'agent' => $agent
            ]);

            if ($collection->count() && $this->validatePassword($password, $agent)) {
                $model = $agent->load($agent->getId());
                $this->_getSession()->setAgent($model);
                $this->validateAgentAvailability($agent);
                $this->_getSession()->setSuperAgent(null);

                Mage::dispatchEvent('agent_authenticated', [
                    'agent' => $model,
                    'password' => $password,
                ]);
            } else {
                throw Mage::exception('Mage_Core', Mage::helper('customer')->__('Invalid login or password.'));
            }
        } else {
            $collection
                ->addFieldToFilter('is_active', 1)
                ->addFieldToFilter('store_id', Mage::app()->getStore()->getId());
            switch (Mage::helper('agent')->getWebsiteChannel()) {
                // Telesales
                case Dyna_Agent_Model_System_Config_Channel::WEBSITE_CHANNEL_TELESALES_CODE:
                    $collection->addFieldToFilter('role_id', ['nin' => [Dyna_Agent_Model_Agentrole::SUPERAGENT_ROLE, Dyna_Agent_Model_Agentrole::BUSINESS_SPECIALIST]]);
                    break;
                // Retail
                case Dyna_Agent_Model_System_Config_Channel::WEBSITE_CHANNEL_RETAIL_CODE:
                    $collection->addFieldToFilter('role_id', ['eq' => Dyna_Agent_Model_Agentrole::BUSINESS_SPECIALIST]);
                    break;
                default:
                    break;
            }

            $collection->load();
            if ($collection->count()) {
                $agent = $collection->fetchItem();
                /** @var Dyna_Agent_Model_Mysql4_Agent $loggedAsSuperAgent */
                $loggedAsSuperAgent = $this->_getSession()->getSuperAgent();
                if ($loggedAsSuperAgent) {
                    $this->_getSession()->setAgent($agent->load($agent->getId()));
                    $agent->setDealer($loggedAsSuperAgent->getDealer());
                } else {
                    $oldAgent = $this->_getSession()->getAgent();
                    $this->_getSession()->setAgent($agent->load($agent->getId()));
                    $this->_getSession()->setSuperAgent($oldAgent);
                }
                $this->validateAgentAvailability($agent);
                // clear cart
                Mage::getSingleton('checkout/session')->clear();
            } else {
                throw Mage::exception('Mage_Core', Mage::helper('customer')->__('Invalid user'));
            }
        }
    }

    /**
     * @param $password
     * @param $agent
     * @return bool
     */
    public function validatePassword($password, $agent)
    {
        if (!$hash = $agent->getPassword()) {
            return false;
        }

        return Mage::helper('core')->validateHash($password, $hash);
    }

    /**
     * Check if the agent account is locked or if its account has a temporary password
     *
     * @param Dyna_Agent_Model_Agent $agent
     * @throws Mage_Core_Exception
     */
    protected function validateAgentAvailability($agent)
    {
        if ($agent->getLocked()) {
            throw new Mage_Core_Exception(
                Mage::helper('agent')->__('This account is locked because of too many failed login or password reset attempts, please refer to an administrator.'),
                Dyna_Agent_Model_Agent::LOCKED_AGENT
            );
        }

        if ($agent->getTemporaryPassword()) {
            Mage::getSingleton('customer/session')->setTemporaryAgent($agent);
            throw new Mage_Core_Exception(
                Mage::helper('agent')->__('Please reset your password, this password is temporary.'),
                Dyna_Agent_Model_Agent::TEMPORARY_PASSWORD
            );
        }
    }

    /**
     * Retrieve customer session model object
     *
     * @return Mage_Customer_Model_Session
     */
    protected function _getSession()
    {
        return Mage::getSingleton('customer/session');
    }
}
