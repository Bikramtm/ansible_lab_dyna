<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Agent_Model_Agentpassword
 */
class Dyna_Agent_Model_Agentpassword extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init("agent/agentpassword");
    }
}
