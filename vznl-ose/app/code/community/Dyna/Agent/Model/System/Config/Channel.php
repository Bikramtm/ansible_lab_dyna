<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Used in creating options for Channel types config value selection
 *
 */
class Dyna_Agent_Model_System_Config_Channel
{
    const WEBSITE_CHANNEL_TELESALES_CODE = 'telesales';
    const WEBSITE_CHANNEL_RETAIL_CODE = 'retail';

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => self::WEBSITE_CHANNEL_TELESALES_CODE, 'label'=>Mage::helper('agent')->__('Telesales Channel')),
            array('value' => self::WEBSITE_CHANNEL_RETAIL_CODE, 'label'=>Mage::helper('agent')->__('Retail Channel')),
        );
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return array(
            self::WEBSITE_CHANNEL_TELESALES_CODE => Mage::helper('agent')->__('Telesales Channel'),
            self::WEBSITE_CHANNEL_RETAIL_CODE => Mage::helper('agent')->__('Retail Channel'),
        );
    }

}