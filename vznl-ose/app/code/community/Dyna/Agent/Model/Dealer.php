<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Agent_Model_Dealer
 */
class Dyna_Agent_Model_Dealer extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
       $this->_init("agent/dealer");
    }

    /**
     * @return Dyna_Agent_Model_Mysql4_Dealergroup_Collection
     */
    public function getDealerGroups()
    {
        $groupIds = $this->getData('group_id') ? $this->getData('group_id') : array();
        if (is_scalar($groupIds)) {
            $groupIds = explode(',', $groupIds);
        }
        return Mage::getResourceModel('agent/dealergroup_collection')
            ->addFieldToFilter('group_id', array('in' => $groupIds));
    }

    public function getAddress()
    {
        return $this->getCity() . ', ' . $this->getStreet() . ' ' . $this->getHouseNr();
    }
}
