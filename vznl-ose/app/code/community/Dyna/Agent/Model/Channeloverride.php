<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Agent_Model_Channeloverride
 */
class Dyna_Agent_Model_Channeloverride extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init("agent/channeloverride");
    }

    /**
     * @param int $id
     * @param null $field
     * @return $this|Mage_Core_Model_Abstract
     */
    public function load($id, $field=null)
    {
        $key = 'channeloverride_' . md5(serialize(array($id, $field)));
        if ($product = Mage::objects()->load($key)) {
            $this->setData($product->getData());
            return $this;
        }

        parent::load($id, $field);
        Mage::objects()->save($this, $key);
        return $this;
    }

    /**
     * @param $dealerCode
     * @return string
     */
    public function getStoreByDealerCode($dealerCode)
    {
        if ($dealerCode) {
            $channelOverride = $this->getCollection()->addFieldToSelect('store_id')->addFieldToFilter('dealer_code', $dealerCode)->getFirstItem();

            if ($channelOverride->getStoreId()) {
                return Mage::app()->getStore($channelOverride->getStoreId())->getCode();
            }
        }

        return Mage::helper('agent')->isTelesalesChannel() ? Dyna_Agent_Model_System_Config_Channel::WEBSITE_CHANNEL_TELESALES_CODE : Mage::app()->getStore()->getCode();
    }

    /**
     *
     */
    public function _beforeSave()
    {
        $user = Mage::getSingleton('admin/session');
        $userId = $user->getUser()->getUserId();

        if (! $this->getId()) {
            $this->setCreatedAt(now());
            $this->setCreatedAdminId($userId);
        } elseif ($this->hasDataChanges()) {
            $this->setUpdatedAt(now());
            $this->setUpdatedAdminId($userId);
        }
    }
}