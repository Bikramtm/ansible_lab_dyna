<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Dyna_Agent_Block_Form_Password extends Mage_Core_Block_Template
{
    protected function _prepareLayout()
    {
        $this->getLayout()->getBlock('head')->setTitle(Mage::helper('customer')->__('Password reset'));

        return parent::_prepareLayout();
    }

    protected function getAgent($token = null)
    {
        if ('temporaryPassword' == $this->getRequest()->getActionName()) {
            $agent = Mage::getSingleton('customer/session')->getTemporaryAgent();
            if ($agent && $agent->getId()) {
                return $agent;
            }
        } else {
            try {
                return Mage::helper('agent')->validatePasswordResetToken($token);
            } catch (Exception $e) {
                return null;
            }
        }
        return null;
    }
}
