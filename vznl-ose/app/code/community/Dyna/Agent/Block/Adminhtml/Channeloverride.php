<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Agent_Block_Adminhtml_Channeloverride
 */
class Dyna_Agent_Block_Adminhtml_Channeloverride extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = "adminhtml_channeloverride";
        $this->_blockGroup = "agent";
        $this->_headerText = Mage::helper("agent")->__("Channel Override Manager");
        $this->_addButtonLabel = Mage::helper("agent")->__("Add New Channel Override");

        parent::__construct();
    }
}