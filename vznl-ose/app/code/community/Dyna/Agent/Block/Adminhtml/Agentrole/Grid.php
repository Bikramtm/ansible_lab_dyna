<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Agent_Block_Adminhtml_Agentrole_Grid
 */
class Dyna_Agent_Block_Adminhtml_Agentrole_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId("agentroleGrid");
        $this->setDefaultSort("role_id");
        $this->setDefaultDir("ASC");
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel("agent/agentrole")->getCollection();
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn("role_id", array(
            "header" => Mage::helper("agent")->__("ID"),
            "align" => "right",
            "width" => "50px",
            "type" => "number",
            "index" => "role_id",
        ));

        $this->addColumn("role_name", array(
            "header" => Mage::helper("agent")->__("Role Name"),
            "index" => "role",
        ));

        $this->addColumn("role_description", array(
            "header" => Mage::helper("agent")->__("Role Description"),
            "index" => "role_description",
        ));

        $this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));

        $this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl("*/*/edit", array("id" => $row->getId()));
    }


    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('role_id');
        $this->getMassactionBlock()->setFormFieldName('role_ids');
        $this->getMassactionBlock()->setUseSelectAll(true);
        $this->getMassactionBlock()->addItem('remove_agentrole', array(
            'label' => Mage::helper('agent')->__('Remove Agent Role'),
            'url' => $this->getUrl('*/adminhtml_agentrole/massRemove'),
            'confirm' => Mage::helper('agent')->__('Are you sure?')
        ));
        return $this;
    }


}