<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Agent_Block_Adminhtml_Agent_Grid
 */
class Dyna_Agent_Block_Adminhtml_Agent_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId("agentGrid");
        $this->setDefaultSort("agent_id");
        $this->setDefaultDir("ASC");
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel("agent/agent")->getCollection();
        
        $this->setCollection($collection);
        parent::_prepareCollection();
        
        return $this;
    }

    protected function _prepareColumns()
    {
        $this->addColumn("agent_id", array(
            "header" => Mage::helper("agent")->__("ID"),
            "align" => "right",
            "width" => "50px",
            "type" => "number",
            "index" => "agent_id",
        ));

        $this->addColumn("username", array(
            "header" => Mage::helper("agent")->__("Username"),
            "index" => "username",
        ));

        $this->addColumn("first_name", array(
            "header" => Mage::helper("agent")->__("First Name"),
            "index" => "first_name",
        ));
        
        $this->addColumn("last_name", array(
            "header" => Mage::helper("agent")->__("Last Name"),
            "index" => "last_name",
        ));
        
        $this->addColumn("email", array(
            "header" => Mage::helper("agent")->__("Email"),
            "index" => "email",
            'sortable' => true
        ));

        $this->addColumn("phone", array(
            "header" => Mage::helper("agent")->__("Phone"),
            "index" => "phone",
        ));
        
        $this->addColumn("is_active", array(
            "header" => Mage::helper("agent")->__("Is active"),
            "index" => "is_active",
            'align'     =>'left',
            'type'      => 'options',
            'options'   => array('1' => Mage::helper('adminhtml')->__('Yes'), '0' => Mage::helper('adminhtml')->__('No')),
        ));
        
        $this->addColumn('role_id', array(
            'header' => Mage::helper('agent')->__('Role ID'),
            'index' => 'role_id',
            'type' => 'options',
            'options' => Dyna_Agent_Block_Adminhtml_Agent_Grid::getOptionArray6(),
        ));
        
        $this->addColumn('group_id', array(
            'header' => Mage::helper('agent')->__('Group ID'),
            'index' => 'group_id',
            'type' => 'options',
            'options' => Dyna_Agent_Block_Adminhtml_Agent_Grid::getOptionArray7(),
            'filter_condition_callback' => array($this, '_filterCategoriesCondition'),
            'sortable'  => false,
        ));

        $this->addColumn('dealer_id', array(
            'header' => Mage::helper('agent')->__('Default Dealer ID'),
            'index' => 'dealer_id',
            'type' => 'options',
            'options' => Dyna_Agent_Block_Adminhtml_Agent_Grid::getOptionArray8(),
        ));

        $this->addColumn("dealer_code", array(
            "header" => Mage::helper("agent")->__("Dealer code"),
            "index" => "dealer_code"
        ));

        $this->addColumn("employee_number", array(
            "header" => Mage::helper("agent")->__("Employee number"),
            "index" => "employee_number"
        ));

        $this->addColumn("red_sales_id", array(
            "header" => Mage::helper("agent")->__("Red Sales ID"),
            "index" => "red_sales_id"
        ));
        
        $this->addColumn('store_id', array(
            'header' => Mage::helper('agent')->__('Store'),
            'index' => 'store_id',
            'type' => 'options',
            'options' => Mage::getSingleton('adminhtml/system_store')->getStoreOptionHash(false),
        ));
             
        $this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl("*/*/edit", array("id" => $row->getId()));
    }

    protected function _filterCategoriesCondition($collection, $column)
    {
        $value = $column->getFilter()->getValue();
        foreach ($collection as $key => $item) {
            if ( ! in_array($value,$item->getGroupId())) {
                $collection->removeItemByKey($key);
            }
        }
        return $collection;        
    }
    
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('agent_id');
        $this->getMassactionBlock()->setFormFieldName('agent_ids');
        $this->getMassactionBlock()->setUseSelectAll(true);
        $this->getMassactionBlock()->addItem('remove_agent', array(
            'label' => Mage::helper('agent')->__('Remove Agent'),
            'url' => $this->getUrl('*/adminhtml_agent/massRemove'),
            'confirm' => Mage::helper('agent')->__('Are you sure?')
        ));
        return $this;
    }
    
    static public function getOptionArray6()
    {
        $collection = Mage::getModel("agent/agentrole")->getCollection()->load();
        $data_array = array();
        foreach ($collection as $role) {
            $data_array[$role->getRoleId()] = $role->getRole();
        }
        return $data_array;
    }

    static public function getValueArray6()
    {
        $collection = Mage::getModel("agent/agentrole")->getCollection()->load();
        $data_array = array();
        foreach ($collection as $role) {
            $data_array[$role->getRoleId()] = $role->getRole();
        }
        return $data_array;
    }

    static public function getOptionArray8()
    {
        $collection = Mage::getModel("agent/dealer")->getCollection()->load();
        $data_array = array();
        foreach ($collection as $dealer) {
            $data_array[$dealer->getId()] = $dealer->getVfDealerCode();
        }
        return $data_array;
    }
    
    static public function getValueArray8()
    {
        $collection = Mage::getModel("agent/dealer")->getCollection()->load();
        $data_array = array();
        $data_array[0] = Mage::helper('agent')->__('Default Empty Dealer');
        foreach ($collection as $dealer) {
            if ($dealer->getData('is_deleted')) {
                continue;
            }
            $data_array[$dealer->getId()] = $dealer->getVfDealerCode();
        }
        return $data_array;
    }

    /**
     * Retrieve Headers row array for Export
     *
     * @return array
     */
    protected function _getExportHeaders()
    {
        $row = array();
        foreach ($this->_columns as $column) { /** @var Mage_Adminhtml_Block_Widget_Grid_Column $column */
            if (!$column->getIsSystem()) {
                $row[] = $column->getIndex();
            }
        }
        $row[] = 'password';
        return array_unique($row);
    }
}
