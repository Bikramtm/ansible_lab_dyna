<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Agent_Block_Adminhtml_Channeloverride_Grid
 */
class Dyna_Agent_Block_Adminhtml_Channeloverride_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId("channeloverrideGrid");
        $this->setDefaultSort("entity_id");
        $this->setDefaultDir("ASC");
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel("agent/channeloverride")->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn("dealer_code", array(
            "header" => Mage::helper("agent")->__("Dealer Code"),
            "index" => "dealer_code",
        ));

        $this->addColumn('store_id', array(
            'header' => Mage::helper('agent')->__('Store'),
            'index' => 'store_id',
            'type' => 'options',
            'options' => Mage::getSingleton('adminhtml/system_store')->getStoreOptionHash(false, $this->getIsExport() ? 'code' : 'name'),
        ));

        $this->addColumn("created_at", array(
            "header" => Mage::helper("agent")->__("Created At"),
            "index" => "created_at",
        ));

        $this->addColumn("updated_at", array(
            "header" => Mage::helper("agent")->__("Updated At"),
            "index" => "updated_at",
        ));

        $this->addColumn("created_admin_id", array(
            "header" => Mage::helper("agent")->__("Created By"),
            "index" => "created_admin_id",
            "type" => "options",
            "options" => Dyna_Agent_Block_Adminhtml_Channeloverride_Grid::getAdminUsersArray(),
        ));

        $this->addColumn("updated_admin_id", array(
            "header" => Mage::helper("agent")->__("Updated By"),
            "index" => "updated_admin_id",
            "type" => "options",
            "options" => Dyna_Agent_Block_Adminhtml_Channeloverride_Grid::getAdminUsersArray(),
        ));

        $this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl("*/*/edit", array("id" => $row->getId()));
    }

    static public function getAdminUsersArray()
    {
        $collection = Mage::getModel("admin/user")->getCollection()->load();
        $data_array = array();
        foreach ($collection as $user) {
            $data_array[$user->getUserId()] = $user->getUsername();
        }

        return $data_array;
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('entity_ids');
        $this->getMassactionBlock()->setUseSelectAll(true);
        $this->getMassactionBlock()->addItem('remove_channeloverride', array(
            'label' => Mage::helper('agent')->__('Remove Channel Override(s)'),
            'url' => $this->getUrl('*/adminhtml_channeloverride/massRemove'),
            'confirm' => Mage::helper('agent')->__('Are you sure?')
        ));
        return $this;
    }
}
