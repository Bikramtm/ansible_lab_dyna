<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Agent_Block_Adminhtml_Channeloverride_Edit_Tabs
 */
class Dyna_Agent_Block_Adminhtml_Channeloverride_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();

        $this->setId("channeloverride_tabs");
        $this->setDestElementId("edit_form");
        $this->setTitle(Mage::helper("agent")->__("Channel Override Information"));
    }

    protected function _beforeToHtml()
    {
        $this->addTab("form_section", array(
            "label" => Mage::helper("agent")->__("Channel Override Information"),
            "title" => Mage::helper("agent")->__("Channel Override Information"),
            "content" => $this->getLayout()->createBlock("agent/adminhtml_channeloverride_edit_tab_form")->toHtml(),
        ));

        return parent::_beforeToHtml();
    }
}
