<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Agent_Block_Adminhtml_Channeloverride_Edit_Tab_Form
 */
class Dyna_Agent_Block_Adminhtml_Channeloverride_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset("agent_form", array("legend" => Mage::helper("agent")->__("Channel Override Information")));


        $fieldset->addField("dealer_code", "text", array(
            "label" => Mage::helper("agent")->__("Dealer code"),
            "class" => "required-entry",
            "required" => true,
            "name" => "dealer_code",
        ));

        $fieldset->addField('store_id', 'select', array(
            'label' => Mage::helper('agent')->__('Store'),
            'values' => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(),
            'name' => 'store_id',
            "class" => "required-entry",
            "required" => true,
        ));

        if (Mage::getSingleton("adminhtml/session")->getChanneloverrideData()) {
            $form->setValues(Mage::getSingleton("adminhtml/session")->getChanneloverrideData());
            Mage::getSingleton("adminhtml/session")->setChanneloverrideData(null);
        } elseif (Mage::registry("channeloverride_data")) {
            $form->setValues(Mage::registry("channeloverride_data")->getData());
        }
        return parent::_prepareForm();
    }
}
