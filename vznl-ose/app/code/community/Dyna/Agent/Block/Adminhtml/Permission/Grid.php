<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Agent_Block_Adminhtml_Permission_Grid
 */
class Dyna_Agent_Block_Adminhtml_Permission_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId("permissionGrid");
        $this->setDefaultSort("entity_id");
        $this->setDefaultDir("ASC");
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel("agent/permission")->getCollection();
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn("entity_id", array(
            "header" => Mage::helper("agent")->__("ID"),
            "align" => "right",
            "width" => "50px",
            "type" => "number",
            "index" => "entity_id",
        ));

        $this->addColumn("name", array(
            "header" => Mage::helper("agent")->__("Name"),
            "index" => "name",
        ));

        $this->addExportType('*/*/exportCsv', Mage::helper('agent')->__('CSV'));

        $this->addExportType('*/*/exportExcel', Mage::helper('agent')->__('Excel'));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl("*/*/edit", array("id" => $row->getId()));
    }


    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('entity_ids');
        $this->getMassactionBlock()->setUseSelectAll(true);
        $this->getMassactionBlock()->addItem('remove_permission', array(
            'label' => Mage::helper('agent')->__('Remove Permission'),
            'url' => $this->getUrl('*/adminhtml_permission/massRemove'),
            'confirm' => Mage::helper('agent')->__('Are you sure?')
        ));
        return $this;
    }


}