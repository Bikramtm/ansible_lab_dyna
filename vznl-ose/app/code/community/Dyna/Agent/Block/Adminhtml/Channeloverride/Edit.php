<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Agent_Block_Adminhtml_Channeloverride_Edit
 */
class Dyna_Agent_Block_Adminhtml_Channeloverride_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();

        $this->_objectId = "entity_id";
        $this->_blockGroup = "agent";
        $this->_controller = "adminhtml_channeloverride";
        $this->_updateButton("save", "label", Mage::helper("agent")->__("Save Channel Override"));
        $this->_updateButton("delete", "label", Mage::helper("agent")->__("Delete Channel Override"));

        $this->_addButton("saveandcontinue", array(
            "label" => Mage::helper("agent")->__("Save And Continue Edit"),
            "onclick" => "saveAndContinueEdit()",
            "class" => "save",
        ), -100);

        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
            ";
    }

    public function getHeaderText()
    {
        if (Mage::registry("channeloverride_data") && Mage::registry("channeloverride_data")->getId()) {
            return Mage::helper("agent")->__("Edit Channel Override '%s'",
                $this->htmlEscape(Mage::registry("channeloverride_data")->getId()));
        } else {
            return Mage::helper("agent")->__("Add Channel Override");
        }
    }
}