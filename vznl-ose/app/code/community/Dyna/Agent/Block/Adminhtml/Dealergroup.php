<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Agent_Block_Adminhtml_Dealergroup
 */
class Dyna_Agent_Block_Adminhtml_Dealergroup extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = "adminhtml_dealergroup";
        $this->_blockGroup = "agent";
        $this->_headerText = Mage::helper("agent")->__("Dealer Groups Manager");
        $this->_addButtonLabel = Mage::helper("agent")->__("Add New Dealer Group");

        parent::__construct();
    }
}