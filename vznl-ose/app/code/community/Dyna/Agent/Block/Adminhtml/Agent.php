<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Agent_Block_Adminhtml_Agent
 */
class Dyna_Agent_Block_Adminhtml_Agent extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = "adminhtml_agent";
        $this->_blockGroup = "agent";
        $this->_headerText = Mage::helper("agent")->__("Agents Manager");
        $this->_addButtonLabel = Mage::helper("agent")->__("Add New Agent");

        parent::__construct();

        $this->_addButton('import', array(
            'label'     => Mage::helper('agent')->__('Import Agents'),
            'onclick'   => 'javascript:new Popup(\'' . $this->getImportUrl() . '\', {title:\'' . Mage::helper('agent')->__('Import Agents') . '\', width: 600, height:220})',
            'class'     => 'go',
        ));
    }

    public function getImportUrl()
    {
        return $this->getUrl('*/*/import');
    }
}
