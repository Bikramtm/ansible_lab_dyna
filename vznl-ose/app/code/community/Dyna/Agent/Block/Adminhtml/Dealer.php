<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Agent_Block_Adminhtml_Dealer
 */
class Dyna_Agent_Block_Adminhtml_Dealer extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = "adminhtml_dealer";
        $this->_blockGroup = "agent";
        $this->_headerText = Mage::helper("agent")->__("Dealers Manager");
        $this->_addButtonLabel = Mage::helper("agent")->__("Add New Dealer");

        parent::__construct();

        $this->_addButton('import', array(
            'label'     => Mage::helper('agent')->__('Import Dealers'),
            'onclick'   => 'javascript:new Popup(\'' . $this->getImportUrl() . '\', {title:\'' . Mage::helper('agent')->__('Import Dealers') . '\', width: 600, height:220})',
            'class'     => 'go',
        ));
    }

    public function getImportUrl()
    {
        return $this->getUrl('*/*/import');
    }
}