<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Agent_Block_Adminhtml_Dealer_Edit_Tabs
 */
class Dyna_Agent_Block_Adminhtml_Dealer_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();

        $this->setId("dealer_tabs");
        $this->setDestElementId("edit_form");
        $this->setTitle(Mage::helper("agent")->__("Dealer Information"));
    }

    protected function _beforeToHtml()
    {
        $this->addTab("form_section", array(
            "label" => Mage::helper("agent")->__("Dealer Information"),
            "title" => Mage::helper("agent")->__("Dealer Information"),
            "content" => $this->getLayout()->createBlock("agent/adminhtml_dealer_edit_tab_form")->toHtml(),
        ));

        return parent::_beforeToHtml();
    }
}
