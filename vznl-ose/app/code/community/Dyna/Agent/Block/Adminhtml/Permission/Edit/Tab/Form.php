<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Agent_Block_Adminhtml_Permission_Edit_Tab_Form
 */
class Dyna_Agent_Block_Adminhtml_Permission_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset("agent_form", array("legend" => Mage::helper("agent")->__("Permission Information")));

        $fieldset->addField("name", "text", array(
            "label" => Mage::helper("agent")->__("Name"),
            "name" => "name",
        ));

        $fieldset->addField("description", "textarea", array(
            "label" => Mage::helper("agent")->__("Description"),
            "name" => "description",
        ));

        if ($dealerRoleData = Mage::getSingleton("adminhtml/session")->getPermissionData()) {
            $form->setValues($dealerRoleData);
            Mage::getSingleton("adminhtml/session")->setPermissionData(null);
        } elseif (Mage::registry("permission_data")) {
            $form->setValues(Mage::registry("permission_data")->getData());
        }

        return parent::_prepareForm();
    }
}
