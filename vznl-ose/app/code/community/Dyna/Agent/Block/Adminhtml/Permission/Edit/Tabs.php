<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Agent_Block_Adminhtml_Permission_Edit_Tabs
 */
class Dyna_Agent_Block_Adminhtml_Permission_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId("permission_tabs");
        $this->setDestElementId("edit_form");
        $this->setTitle(Mage::helper("agent")->__("Permission Information"));
    }

    protected function _beforeToHtml()
    {
        $this->addTab("form_section", array(
            "label" => Mage::helper("agent")->__("Permission Information"),
            "title" => Mage::helper("agent")->__("Permission Information"),
            "content" => $this->getLayout()->createBlock("agent/adminhtml_permission_edit_tab_form")->toHtml(),
        ));

        return parent::_beforeToHtml();
    }
}
