<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();
$dropSQL = <<<SQL
DROP TABLE IF EXISTS agent;
DROP TABLE IF EXISTS agent_role;
DROP TABLE IF EXISTS dealer;
SQL;
$installer->run($dropSQL);


/** Agent Role table */
$agentRoleTable = new Varien_Db_Ddl_Table();
$agentRoleTable->setName('agent_role');
$agentRoleTable->addColumn(
    'role_id',
    Varien_Db_Ddl_Table::TYPE_INTEGER,
    10,
    array('unsigned' => true, 'primary' => true, 'auto_increment' => true)
);
$agentRoleTable->addColumn(
    'role',
    Varien_Db_Ddl_Table::TYPE_VARCHAR,
    100
);

$agentRoleTable->setOption('type', 'InnoDB');
$agentRoleTable->setOption('charset', 'utf8');
$this->getConnection()->createTable($agentRoleTable);
/** End Agent Role table */


/** Dealer table */
$dealerTable = new Varien_Db_Ddl_Table();
$dealerTable->setName('dealer');
$dealerTable->addColumn(
    'dealer_id',
    Varien_Db_Ddl_Table::TYPE_INTEGER,
    10,
    array('unsigned' => true, 'primary' => true, 'auto_increment' => true)
);
$dealerTable->addColumn(
    'street',
    Varien_Db_Ddl_Table::TYPE_VARCHAR,
    255
);
$dealerTable->addColumn(
    'postcode',
    Varien_Db_Ddl_Table::TYPE_VARCHAR,
    10
);
$dealerTable->addColumn(
    'city',
    Varien_Db_Ddl_Table::TYPE_VARCHAR,
    50
);
$dealerTable->addColumn(
    'house_nr',
    Varien_Db_Ddl_Table::TYPE_VARCHAR,
    10
);
$dealerTable->addColumn(
    'vf_dealer_code',
    Varien_Db_Ddl_Table::TYPE_VARCHAR,
    20
);
$dealerTable->addColumn(
    'axi_store_code',
    Varien_Db_Ddl_Table::TYPE_VARCHAR,
    20
);
$dealerTable->addColumn(
    'store_id',
    Varien_Db_Ddl_Table::TYPE_INTEGER
);
$dealerTable->addColumn(
    'telephone',
    Varien_Db_Ddl_Table::TYPE_VARCHAR,
    50
);
$dealerTable->addColumn(
    'is_deleted',
    Varien_Db_Ddl_Table::TYPE_BOOLEAN
);

$dealerTable->setOption('type', 'InnoDB');
$dealerTable->setOption('charset', 'utf8');
$this->getConnection()->createTable($dealerTable);
/** End Dealer table */


/** Agent table */
$agentTable = new Varien_Db_Ddl_Table();
$agentTable->setName('agent');

$agentTable->addColumn(
    'agent_id',
    Varien_Db_Ddl_Table::TYPE_INTEGER,
    10,
    array('unsigned' => true, 'primary' => true, 'auto_increment' => true)
);
$agentTable->addColumn(
    'username',
    Varien_Db_Ddl_Table::TYPE_VARCHAR,
    100
);
$agentTable->addColumn(
    'password',
    Varien_Db_Ddl_Table::TYPE_TEXT
);
$agentTable->addColumn(
    'hash_method',
    Varien_Db_Ddl_Table::TYPE_VARCHAR,
    20
);
$agentTable->addColumn(
    'is_active',
    Varien_Db_Ddl_Table::TYPE_TINYINT
);
$agentTable->addColumn(
    'role_id',
    Varien_Db_Ddl_Table::TYPE_INTEGER
);
$agentTable->addColumn(
    'store_id',
    Varien_Db_Ddl_Table::TYPE_INTEGER
);
$agentTable->addColumn(
    'email',
    Varien_Db_Ddl_Table::TYPE_VARCHAR,
    50
);
$agentTable->addColumn(
    'phone',
    Varien_Db_Ddl_Table::TYPE_VARCHAR,
    20
);
$agentTable->addColumn(
    'first_name',
    Varien_Db_Ddl_Table::TYPE_VARCHAR,
    20
);
$agentTable->addColumn(
    'last_name',
    Varien_Db_Ddl_Table::TYPE_VARCHAR,
    20
);
$agentTable->addColumn(
    'dealer_id',
    Varien_Db_Ddl_Table::TYPE_INTEGER,
    10,
    array('unsigned' => true)
);
$agentTable->addColumn(
    'password_reset_token',
    Varien_Db_Ddl_Table::TYPE_VARCHAR,
    50
);
$agentTable->addColumn(
    'password_reset_token_expiry_date',
    Varien_Db_Ddl_Table::TYPE_DATETIME
);
$agentTable->addColumn(
    'everlasting_pass',
    Varien_Db_Ddl_Table::TYPE_BOOLEAN
);
$agentTable->addColumn(
    'employee_number',
    Varien_Db_Ddl_Table::TYPE_VARCHAR,
    50
);
$agentTable->addColumn(
    'password_changed',
    Varien_Db_Ddl_Table::TYPE_DATETIME
);
$agentTable->addColumn(
    'temporary_password',
    Varien_Db_Ddl_Table::TYPE_BOOLEAN
);
$agentTable->addColumn(
    'locked',
    Varien_Db_Ddl_Table::TYPE_BOOLEAN
);
$agentTable->addColumn(
    'login_attempts',
    Varien_Db_Ddl_Table::TYPE_INTEGER
);
$agentTable->addColumn(
    'dealer_code',
    Varien_Db_Ddl_Table::TYPE_VARCHAR,
    50
);

$agentTable->addForeignKey(
    'fk_agent_role',
    'role_id',
    'agent_role',
    'role_id',
    $agentRoleTable::ACTION_RESTRICT,
    $agentRoleTable::ACTION_CASCADE
);
$agentTable->addForeignKey(
    'fk_agent_dealer',
    'dealer_id',
    'dealer',
    'dealer_id',
    Varien_Db_Ddl_Table::ACTION_NO_ACTION,
    Varien_Db_Ddl_Table::ACTION_SET_NULL
);
$agentTable->setOption('type', 'InnoDB');
$agentTable->setOption('charset', 'utf8');
$this->getConnection()->createTable($agentTable);
/** End Agent table */

$installer->endSetup();
