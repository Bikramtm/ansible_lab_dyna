<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();
$dropSQL = <<<SQL
DROP TABLE IF EXISTS agent_password;
SQL;
$installer->run($dropSQL);

/** Agent Password table */
$agentPasswordTable = new Varien_Db_Ddl_Table();
$agentPasswordTable->setName('agent_password');
$agentPasswordTable->addColumn(
    'entity_id',
    Varien_Db_Ddl_Table::TYPE_INTEGER,
    10,
    array('unsigned' => true, 'primary' => true, 'auto_increment' => true)
);

$agentPasswordTable->addColumn(
    'agent_id',
    Varien_Db_Ddl_Table::TYPE_INTEGER,
    10,
    array('unsigned' => true)
);

$agentPasswordTable->addColumn(
    'password',
    Varien_Db_Ddl_Table::TYPE_TEXT
);
$agentPasswordTable->addColumn(
    'hash_method',
    Varien_Db_Ddl_Table::TYPE_VARCHAR,
    20
);

$agentPasswordTable->setOption('type', 'InnoDB');
$agentPasswordTable->setOption('charset', 'utf8');
$installer->getConnection()->createTable($agentPasswordTable);

/** Create foreign key from agent to agent_password*/
$installer->getConnection()->addForeignKey(
    'fk_agent_password_agent',
    $this->getTable('agent_password'),
    'agent_id',
    $this->getTable('agent'),
    'agent_id'
);

$installer->endSetup();
