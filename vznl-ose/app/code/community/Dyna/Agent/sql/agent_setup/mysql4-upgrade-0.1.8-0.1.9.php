<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer->startSetup();
$connection = $installer->getConnection();
$table = $this->getTable('agent_role');

// Add the role description
if (! $connection->tableColumnExists($table, 'role_description')) {
    $connection->addColumn($table, 'role_description',
        'VARCHAR(200) DEFAULT NULL'
    );
}

$installer->endSetup();