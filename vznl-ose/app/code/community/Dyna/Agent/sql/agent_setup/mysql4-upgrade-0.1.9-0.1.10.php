<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer->startSetup();
$connection = $installer->getConnection();
$table = $this->getTable('agent');
$column = 'tokens_generated';

// Add the role description
if (! $connection->tableColumnExists($table, $column)) {
    $connection->addColumn($table, $column,
        'integer DEFAULT NULL after password_reset_token_expiry_date'
    );
}

$installer->endSetup();
