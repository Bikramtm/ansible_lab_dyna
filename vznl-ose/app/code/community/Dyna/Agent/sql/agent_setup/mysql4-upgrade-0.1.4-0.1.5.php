<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;

$installer->startSetup();

$sql = <<<SQL
ALTER TABLE `dealer`
  ADD COLUMN `naw_data_dealer_code` VARCHAR(45) NULL AFTER `telephone`,
  ADD COLUMN `axi_code` VARCHAR(45) NULL AFTER `naw_data_dealer_code`,
  ADD COLUMN `paid_code` VARCHAR(45) NULL AFTER `axi_code`;
SQL;


$installer->getConnection()->query($sql);

$installer->endSetup();
