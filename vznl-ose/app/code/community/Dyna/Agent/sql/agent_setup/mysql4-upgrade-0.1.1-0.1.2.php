<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$dropSQL = <<<SQL
DROP TABLE IF EXISTS role_permission_link;
DROP TABLE IF EXISTS role_permission;
SQL;
$installer->run($dropSQL);

$permissionsTable = new Varien_Db_Ddl_Table();
$permissionsTable->setName('role_permission');
$permissionsTable
    ->addColumn(
        'entity_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        10,
        array('unsigned' => true, 'primary' => true, 'auto_increment' => true)
    )
    ->addColumn(
        'name',
        Varien_Db_Ddl_Table::TYPE_VARCHAR,
        255
    )
    ->addColumn(
        'description',
        Varien_Db_Ddl_Table::TYPE_VARCHAR,
        255
    )
    ->setOption('type', 'InnoDB')
    ->setOption('charset', 'utf8');
$this->getConnection()->createTable($permissionsTable);


$linkTable = new Varien_Db_Ddl_Table();
$linkTable->setName('role_permission_link')
    ->addColumn(
        'link_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        10,
        array('unsigned' => true, 'primary' => true, 'auto_increment' => true)
    )
    ->addColumn(
        'role_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        10,
        array('unsigned' => true)
    )
    ->addColumn(
        'permission_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        10,
        array('unsigned' => true)
    )
    ->addForeignKey(
        'fk_permission_id',
        'role_id',
        Mage::getSingleton('core/resource')->getTableName('agent/agentrole'),
        'role_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->addForeignKey(
        'fk_permission_id',
        'permission_id',
        Mage::getSingleton('core/resource')->getTableName('role_permission'),
        'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->addIndex(
        'unique_role_permission_id',
        array('role_id', 'permission_id'),
        array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE)
    )
    ->setOption('type', 'InnoDB')
    ->setOption('charset', 'utf8');
$this->getConnection()->createTable($linkTable);

$installer->endSetup();