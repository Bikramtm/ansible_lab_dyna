<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();
$dropSQL = <<<SQL
DROP TABLE IF EXISTS channel_override;
SQL;
$installer->run($dropSQL);

/** Channel Override table */
$channelOverrideTable = new Varien_Db_Ddl_Table();
$channelOverrideTable->setName('channel_override');

$channelOverrideTable->addColumn(
    'entity_id',
    Varien_Db_Ddl_Table::TYPE_INTEGER,
    10,
    array('unsigned' => true, 'primary' => true, 'auto_increment' => true)
);
$channelOverrideTable->addColumn(
    'dealer_code',
    Varien_Db_Ddl_Table::TYPE_VARCHAR,
    20,
    array('nullable'  => false)
);
$channelOverrideTable->addColumn(
    'store_id',
    Varien_Db_Ddl_Table::TYPE_SMALLINT,
    5,
    array('unsigned' => true, 'nullable'  => false)
);
$channelOverrideTable->addColumn(
    'created_at',
    Varien_Db_Ddl_Table::TYPE_DATETIME
);
$channelOverrideTable->addColumn(
    'updated_at',
    Varien_Db_Ddl_Table::TYPE_DATETIME
);

$channelOverrideTable->addForeignKey('fk_channel_override_core_store',
    'store_id', $installer->getTable('core_store'), 'store_id',
    Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
);
$channelOverrideTable->addIndex('unique_dealer_code', 'dealer_code', array(
    'type' => 'unique'
));

$channelOverrideTable->setOption('type', 'InnoDB');
$channelOverrideTable->setOption('charset', 'utf8');
$this->getConnection()->createTable($channelOverrideTable);
/** End Channel Override table */

$installer->endSetup();
