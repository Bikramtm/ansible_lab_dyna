<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();

$installer->getConnection()->addColumn($this->getTable('core_store'), 'show_agent', 'TINYINT');
$installer->getConnection()->addColumn($this->getTable('core_store'), 'show_store', 'TINYINT');

$installer->getConnection()->addForeignKey(
    'fk_agent_core_store',
    $this->getTable('agent'),
    'store_id',
    'core_store',
    'store_id',
    Varien_Db_Ddl_Table::ACTION_NO_ACTION,
    Varien_Db_Ddl_Table::ACTION_NO_ACTION
);

$installer->getConnection()->addForeignKey(
    'fk_dealer_core_store',
    $this->getTable('dealer'),
    'store_id',
    'core_store',
    'store_id',
    Varien_Db_Ddl_Table::ACTION_NO_ACTION,
    Varien_Db_Ddl_Table::ACTION_NO_ACTION
);

$installer->endSetup();
