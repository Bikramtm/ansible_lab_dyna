<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Agent_Adminhtml_AgentController
 */
class Dyna_Agent_Adminhtml_AgentController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()->_setActiveMenu("agent/agent")->_addBreadcrumb(Mage::helper("adminhtml")->__("Agent  Manager"), Mage::helper("adminhtml")->__("Agent Manager"));
        return $this;
    }

    public function indexAction()
    {
        $this->_title($this->__("Agent"));
        $this->_title($this->__("Manager Agent"));

        $this->_initAction();
        $this->renderLayout();
    }

    public function editAction()
    {
        $this->_title($this->__("Agent"));
        $this->_title($this->__("Agent"));
        $this->_title($this->__("Edit Item"));

        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("agent/agent")->load($id);

        if ($model->getId()) {
            Mage::register("agent_data", $model);
            $this->loadLayout();
            $this->_setActiveMenu("agent/agent");
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Agent Manager"), Mage::helper("adminhtml")->__("Agent Manager"));
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Agent Description"), Mage::helper("adminhtml")->__("Agent Description"));
            $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock("agent/adminhtml_agent_edit"))->_addLeft($this->getLayout()->createBlock("agent/adminhtml_agent_edit_tabs"));
            $this->renderLayout();
        } else {
            Mage::getSingleton("adminhtml/session")->addError(Mage::helper("agent")->__("Item does not exist."));
            $this->_redirect("*/*/");
        }
    }

    public function newAction()
    {

        $this->_title($this->__("Agent"));
        $this->_title($this->__("Agent"));
        $this->_title($this->__("New Item"));

        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("agent/agent")->load($id);

        $data = Mage::getSingleton("adminhtml/session")->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }

        Mage::register("agent_data", $model);

        $this->loadLayout();
        $this->_setActiveMenu("agent/agent");

        $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);

        $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Agent Manager"), Mage::helper("adminhtml")->__("Agent Manager"));
        $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Agent Description"), Mage::helper("adminhtml")->__("Agent Description"));


        $this->_addContent($this->getLayout()->createBlock("agent/adminhtml_agent_edit"))->_addLeft($this->getLayout()->createBlock("agent/adminhtml_agent_edit_tabs"));

        $this->renderLayout();

    }

    public function saveAction()
    {
        $postData = $this->getRequest()->getPost();

        if ($postData) {
            try {
                $currentAgent = Mage::getModel("agent/agent")->load($this->getRequest()->getParam("id"));
                $toSaveData = array();
                Mage::helper('agent')->validateNewAgentData($currentAgent, $this->getRequest()->getParams());
                foreach ($postData as $k=>$val){
                   $toSaveData[$k] = trim($val);
                   if ($k == 'password' || ($k == 'change_password' && strlen($val) > 0)) {
                       Mage::helper('agent')->validatePassword($currentAgent, $val);
                       $toSaveData['password'] = Mage::helper('core')->getHash($val, Dyna_Agent_Model_Agent::HASH_SALT_LENGTH);
                       Mage::register('agent_password_changed', true);
                   }

                }

                if ((isset($toSaveData['reset_attempts']) && $toSaveData['reset_attempts'] !== null) || (isset($toSaveData['locked']) && $currentAgent->getLocked() != $toSaveData['locked'] && !$toSaveData['locked'])){
                    $toSaveData['login_attempts'] = 0;
                }

                if(!$toSaveData['dealer_id']) {
                    $toSaveData['dealer_id']=null;
                }
                $model = Mage::getModel("agent/agent")
                    ->addData($toSaveData)
                    ->setId($this->getRequest()->getParam("id"))
                    ->save();

                if (isset($toSaveData['group_id'])) {
                    $model->setData('group_id', $toSaveData['group_id'])->save();
                }

                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Agent was successfully saved"));
                Mage::getSingleton("adminhtml/session")->setAgentData(false);

                if ($this->getRequest()->getParam("back")) {
                    $this->_redirect("*/*/edit", array("id" => $model->getId()));
                    return;
                }
                $this->_redirect("*/*/");
                return;
            } catch (Exception $e) {
                $agentData = $this->getRequest()->getPost();
                if($e->getCode() == Dyna_Agent_Helper_Data::AGENT_FAILED_PASSWORD){
                    $agentData['password'] = '';
                    $agentData['change_password'] = '';
                }
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                Mage::getSingleton("adminhtml/session")->setAgentData($agentData);
                if($this->getRequest()->getParam("id")){
                    $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
                }else {
                    $this->_redirect("*/*/new");
                }

                return;
            }
        }
        $this->_redirect("*/*/");
    }

    public function unlockAction()
    {
        if ($this->getRequest()->getParam("agent_id")){
            $agent = Mage::getModel("agent/agent")->load($this->getRequest()->getParam("agent_id"));
            if ($agent->getLocked()){
                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper('agent')->__('The agent has been unlocked.'));
                $agent->setLocked(0);
                $agent->setLoginAttempts(0);
                $agent->setLastLoginDate(date('Y-m-d'));
                $agent->save();
            }else{
                Mage::getSingleton("adminhtml/session")->addNotice(Mage::helper('agent')->__('The agent is already unlocked.'));
            }
        }
        $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("agent_id")));
    }


    public function deleteAction()
    {
        if ($this->getRequest()->getParam("id") > 0) {
            try {
                $model = Mage::getModel("agent/agent");
                $model->setId($this->getRequest()->getParam("id"))->delete();
                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item was successfully deleted"));
                $this->_redirect("*/*/");
            } catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
            }
        }
        $this->_redirect("*/*/");
    }


    public function massRemoveAction()
    {
        try {
            $ids = $this->getRequest()->getPost('agent_ids', array());
            foreach ($ids as $id) {
                $model = Mage::getModel("agent/agent");
                $model->setId($id)->delete();
            }
            Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item(s) was successfully removed"));
        } catch (Exception $e) {
            Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
        }
        $this->_redirect('*/*/');
    }

    /**
     * Export order grid to CSV format
     */
    public function exportCsvAction()
    {
        $fileName = 'agent.csv';
        $grid = $this->getLayout()->createBlock('agent/adminhtml_agent_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }

    /**
     * @param string $fileName
     * @param array|string $content
     * @param string $contentType
     * @param null $contentLength
     * @return $this|Mage_Core_Controller_Varien_Action
     * @throws Exception
     * @throws Mage_Core_Exception
     * @throws Zend_Controller_Response_Exception
     */
    protected function _prepareDownloadResponse($fileName, $content, $contentType = 'application/octet-stream',
                                                $contentLength = null
    ) {
        $session = Mage::getSingleton('admin/session');
        if ($session->isFirstPageAfterLogin()) {
            $this->_redirect($session->getUser()->getStartupPageUrl());
            return $this;
        }

        $isFile = false;
        $file   = null;
        if (is_array($content)) {
            if (!isset($content['type']) || !isset($content['value'])) {
                return $this;
            }
            if ($content['type'] == 'filename') {
                $isFile         = true;
                $file           = $content['value'];
                $contentLength  = filesize($file);
            }
        }

        $this->getResponse()
            ->setHttpResponseCode(200)
            ->setHeader('Pragma', 'public', true)
            ->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true)
            ->setHeader('Content-type', $contentType, true)
            ->setHeader('Content-Length', is_null($contentLength) ? strlen($content) : $contentLength)
            ->setHeader('Content-Disposition', 'attachment; filename="'.$fileName.'"')
            ->setHeader('Last-Modified', date('r'));

        if (!is_null($content)) {
            if ($isFile) {
                $this->getResponse()->clearBody();
                $this->getResponse()->sendHeaders();

                $ioAdapter = new Varien_Io_File();
                if (!$ioAdapter->fileExists($file)) {
                    Mage::throwException(Mage::helper('core')->__('File not found'));
                }
                $ioAdapter->open(array('path' => $ioAdapter->dirname($file)));
                $ioAdapter->streamOpen($file, 'r');
                while ($buffer = $ioAdapter->streamRead()) {
                    print $buffer;
                }
                $ioAdapter->streamClose();
                if (!empty($content['rm'])) {
                    $ioAdapter->rm($file);
                }

                exit(0);
            } else {
                $this->getResponse()->setBody($content);
            }
        }
        return $this;
    }

    public function importAction()
    {
        $this->loadLayout();
        $this->getLayout()->getBlock('catalog.product.edit.tab.related');

        $this->renderLayout();
    }

    public function approveImportAction()
    {
        if ( ! Mage::getSingleton('core/session')->getData('import_rows')) {
            return $this->_redirect('*/*');
        }

        $this->loadLayout();
        $approve = $this->getLayout()->createBlock('agent/adminhtml_approve');
        $mapper = Mage::getModel('agent/mapper_agent');
        Mage::register('mapper', $mapper);
        $this->_addContent($approve->setFinishUrl($this->getUrl('*/*/finishImport')));
        $this->renderLayout();
    }

    public function finishImportAction()
    {
        $session = Mage::getSingleton('core/session');
        $importRows = $session->getData('import_rows');
        $session
            ->unsetData('import_headers')
            ->unsetData('import_rows');

        /** @var Mage_Core_Model_Resource_Transaction $transaction */
        $transaction = Mage::getModel('core/resource_transaction');

        foreach ($importRows as $row) {
            if ($row instanceof Mage_Core_Model_Abstract) {
                if ($row instanceof Dyna_Agent_Model_Agent && !$row->getUsername()) {
                    continue;
                }
                $transaction->addObject($row);
            }
        }

        try {
            $transaction->save();
            Mage::getSingleton('adminhtml/session')->addSuccess(
                $this->__('Import file successfully parsed and imported into database')
            );
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError(
                $this->__($e->getMessage())
            );
            $this->_redirect('*/*');
        }
        $this->_redirect('*/*');
    }

    public function importAgentsAction()
    {
        if (isset($_FILES['file']) && isset($_FILES['file']['tmp_name'])) {

            /** @var Dyna_Agent_Model_GenericImporter $importer */
            $importer = Mage::getSingleton('agent/genericImporter');

            try {
                if ($_FILES['file']['tmp_name']) {
                    $uploader = new Mage_Core_Model_File_Uploader('file');
                    $uploader->setAllowedExtensions($importer->getSupportedFormats());
                    $path = Mage::app()->getConfig()->getTempVarDir() . '/import/';
                    $uploader->save($path);
                    if ($uploadFile = $uploader->getUploadedFileName()) {
                        $newFilename = 'import-' . date('YmdHis') . '_' . $uploadFile;
                        rename($path . $uploadFile, $path . $newFilename);
                    }
                }

                if (isset($newFilename) && $newFilename) {
                    $importer->import($path . $newFilename, Mage::getModel('agent/mapper_agent'));
                    return $this->_redirect('*/*/approveImport');
                }
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    $this->__($e->getMessage())
                );
                $this->_redirect('*/*');
            }
        }
        else {
            Mage::getSingleton('adminhtml/session')->addError(
                $this->__('Invalid POST data (please check post_max_size and upload_max_filesize settings in your php.ini file).')
            );
            $this->_redirect('*/*');
        }
    }

    /**
     *  Export order grid to Excel XML format
     */
    public function exportExcelAction()
    {
        $fileName = 'agent.xml';
        $grid = $this->getLayout()->createBlock('agent/adminhtml_agent_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }
    //Patch for SUPEE-6285
    protected function _isAllowed()
    {
        return true;
    }
}
