<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Dyna_Agent_Helper_Data extends Mage_Core_Helper_Abstract
{
    const CAR_REGEXP = 'agent/password_validation/car_number_regexp';
    const TEL_REGEXP = 'agent/password_validation/tel_regexp';
    const DOB_REGEXP = 'agent/password_validation/dob_regexp';
    const SEQ_NR_REGEXP = 'agent/password_validation/seqnr_regexp';
    const AUTO_LOGOUT_PATH = 'agent/logout_configuration/';
    /**
     * Query param name for last url visited
     */
    const REFERER_QUERY_PARAM_NAME = 'referer';
    const AGENT_FAILED_PASSWORD = 3333;
    const NEW_AGENT_FAILED_DATA = 4444;
    const DEFAULT_DEALERCODE = '00830000';

    /**
     * @param string $message
     * @throws Exception
     */
    protected function throwNewAgentFailed($message)
    {
        throw new Exception($message, self::NEW_AGENT_FAILED_DATA);
    }
    /**
     * @param string $message
     * @throws Exception
     */
    protected function throwAgentPasswordFailed($message)
    {
        throw new Exception($message, self::AGENT_FAILED_PASSWORD);
    }

    /**
     * @return mixed
     */
    public function getWharehouseCode()
    {
        return Mage::getStoreConfig('agent/general_config/axi_warehouse');
    }

    /**
     * @return string
     */
    public function getDaDealerCode()
    {
        $agent = $this->getAgent();

        if ($agent && $agent->getDealer()) {
            return $agent->getDealerCode();
        }

        $daDealercode = self::DEFAULT_DEALERCODE;
        if ($agent && ($dealer = Mage::getModel('agent/dealer')->load($agent->getDealerId()))) {
            $daDealercode = $dealer->getVfDealerCode();
        }
        return $daDealercode;
    }

    /**
     * @return int|null
     */
    public function getAgentId()
    {
        $agent = $this->getAgent();
        if ($agent) {
            return $agent->getId();
        }
        return null;
    }

    /**
     * @return mixed
     */
    public function getAgent()
    {
        $session = Mage::getSingleton('customer/session');
        return $session->getAgent();
    }

    /**
     * @return mixed
     */
    public function getAxiStore()
    {
        $currentStore = $this->getCurrentStoreId();
        if ($currentStore) {
            return $this->getCurrentStoreId();
        }

        $axiCode = $this->getWharehouseCode();
        $agent = $this->getAgent();
        if ($agent && ($dealer = Mage::getModel('agent/dealer')->load($agent->getDealerId()))) {
            $axiCode = $dealer->getAxiStoreCode();
        }
        return $axiCode;
    }

    /**
     * Retrieve customer login POST URL
     *
     * @return string
     */
    public function getDealerPostUrl()
    {
        $params = array();
        if ($this->_getRequest()->getParam(self::REFERER_QUERY_PARAM_NAME)) {
            $params = array(
                self::REFERER_QUERY_PARAM_NAME => $this->_getRequest()->getParam(self::REFERER_QUERY_PARAM_NAME)
            );
        }
        return $this->_getUrl('agent/account/dealerPost', $params);
    }

    /**
     * Retrieve agent login POST URL
     *
     * @return string
     */
    public function getLoginPostUrl()
    {
        $params = array();
        if ($this->_getRequest()->getParam(self::REFERER_QUERY_PARAM_NAME)) {
            $params = array(
                self::REFERER_QUERY_PARAM_NAME => $this->_getRequest()->getParam(self::REFERER_QUERY_PARAM_NAME)
            );
        }
        return $this->_getUrl('agent/account/loginPost', $params);
    }

    /**
     * Retrieve agent login POST URL
     *
     * @return string
     */
    public function getCustomerPostUrl()
    {
        $params = array();
        if ($this->_getRequest()->getParam(self::REFERER_QUERY_PARAM_NAME)) {
            $params = array(
                self::REFERER_QUERY_PARAM_NAME => $this->_getRequest()->getParam(self::REFERER_QUERY_PARAM_NAME)
            );
        }
        return $this->_getUrl('agent/account/customerPost', $params);
    }

    /**
     * @param bool $asJson
     * @return object|string
     *
     * Get list of dealers as a collection or a json list
     */
    public function getDealers($asJson = false, $noStore = false)
    {
        $collection = Mage::getModel('agent/dealer')->getCollection();
        if (!$noStore) {
            $collection->addFieldToFilter('store_id', Mage::app()->getStore()->getId());
        }

        if (!$asJson) {
            return $collection;
        }

        $dealers = array();
        /** @var Dyna_Agent_Model_Dealer $dealer */
        foreach ($collection as $dealer) {
            array_push($dealers, array(
                    'value' => $dealer->getAddress(),
                    'data' => $dealer->getId(),
                )
            );
        }

        return json_encode($dealers);
    }

    /**
     * @param string $config
     * @return string|null
     */
    public function getAutoLogoutConfig($config)
    {
        return Mage::getStoreConfig(self::AUTO_LOGOUT_PATH . $config);
    }

    /**
     * Retrieve an agent by the email address
     *
     * @param $email
     * @param int $storeId
     * @throws Exception
     * @return Dyna_Agent_Model_Agent
     */
    public function findAgentByEmail($email, $storeId = null)
    {
        $agent = Mage::getResourceModel('agent/agent_collection')->addFieldToFilter('email', $email);
        if ($storeId) {
            $agent->addFieldToFilter('store_id', $storeId);
        }
        if (!count($agent)) {
            throw new Exception($this->__('No agents found for this email address'));
        }

        return $agent;
    }

    /**
     * @return mixed
     */
    public function getCurrentStoreId()
    {
        if ($this->isTelesalesChannel()
            || !Mage::getSingleton('customer/session')->getAgent()
        ) {
            return $this->getWharehouseCode();
        } else {
            return Mage::getSingleton('customer/session')->getAgent()->getDealer()->getAxiStoreCode();
        }
    }

    /**
     * @param $token
     * @return mixed
     * @throws Exception
     */
    public function validatePasswordResetToken($token)
    {
        $agent = Mage::getModel('agent/agent')->getCollection()->addFieldToFilter('password_reset_token',
            $token)->fetchItem();
        if (!$agent) {
            throw new Exception($this->__('No agents found for this token'));
        }
        if (strtotime($agent->getPasswordResetTokenExpiryDate()) < Mage::getModel('core/date')->timestamp(time())) {
            throw new Exception($this->__('Token is expired'));
        }

        return $agent;
    }

    /**
     * Validates new agent data against certain criterias
     * @param $agent Dyna_Agent_Model_Agent
     * @param $data array
     * @throws Exception
     * @return bool
     */
    public function validateNewAgentData($agent, $data)
    {
        if ($agent->load($data['username'], 'username')->getId() && !array_key_exists('id', $data)) {
            $this->throwNewAgentFailed($this->__('Agent already exists with same Login Name (username)'));
        }
    }

    /**
     * Validates agent password against certain criterias
     * @param $agent Dyna_Agent_Model_Agent
     * @param $password string
     * @throws Exception
     * @return bool
     */
    public function validatePassword($agent, $password)
    {
        if (mb_strlen($password) < 8) {
            $this->throwAgentPasswordFailed($this->__('The new password must have at least 8 characters.'));
        }

        $hasDigit = 0;
        $hasLower = 0;
        $hasUpper = 0;
        $hasPunct = 0;

        for ($i = 0; $i < strlen($password); $i++) {
            if (!$hasLower && ctype_lower($password[$i])) {
                $hasLower = 1;
            }
            if (!$hasDigit && ctype_digit($password[$i])) {
                $hasDigit = 1;
            }
            if (!$hasPunct && ctype_punct($password[$i])) {
                $hasPunct = 1;
            }
            if (!$hasUpper && ctype_upper($password[$i])) {
                $hasUpper = 1;
            }
        }

        if ($hasPunct + $hasLower + $hasDigit + $hasUpper < 3) {
            $this->throwAgentPasswordFailed($this->__('The new password must have at least three out of the four following character types: uppercase literal, lowercase literal, digit or punctuation.'));
        }

        if (!$agent->checkLastTenPasswords($password)) {
            $this->throwAgentPasswordFailed($this->__('This password has been already used in the past. Please pick another password.'));
        }

        if (@preg_match($this->getRegex(self::CAR_REGEXP), $password)) {
            $this->throwAgentPasswordFailed($this->__('Car numbers cannot be used as a password for an agent.'));
        }

        if (@preg_match($this->getRegex(self::TEL_REGEXP), $password)) {
            $this->throwAgentPasswordFailed($this->__('Telephone numbers cannot be used as a password for an agent.'));
        }

        if (@preg_match($this->getRegex(self::DOB_REGEXP), $password)) {
            $this->throwAgentPasswordFailed($this->__('You cannot use a date of birth as a password for an agent.'));
        }

        if (@preg_match($this->getRegex(self::SEQ_NR_REGEXP), $password)) {
            $this->throwAgentPasswordFailed($this->__('Sequential numbers cannot be used as a password for an agent.'));
        }

        if (strtotime($agent->getPasswordChanged()) + (Mage::getStoreConfig(Dyna_Agent_Model_Agent::PASSWORD_NEXT_ATTEMPT_DELAY) ? : 0) > Mage::getModel('core/date')->timestamp(time())) {
            $this->throwAgentPasswordFailed($this->__("The password was already changed once in the last 24 hours."));
        }

        if ($password == $agent->getFirstName() || $password == $agent->getLastName() || $password == $agent->getUsername()) {
            $this->throwAgentPasswordFailed($this->__('The firstname/lastname/username cannot be used as a password for an agent.'));
        }
    }

    /**
     * @param $path
     * @return mixed|string
     */
    protected function getRegex($path)
    {
        $rule = Mage::getStoreConfig($path);
        return strlen(trim($rule)) > 0 ? $rule : '/(?!.*)/';
    }

    /**
     * @return array
     */
    public function getCurrentDealerGroups()
    {
        if ($dealerGroups = Mage::app()->getRequest()->getParam('dealer_groups')) {
            return array_map('trim', explode(',', $dealerGroups));
        } else {
            $session = Mage::getSingleton('customer/session');
            /** @var Dyna_Agent_Model_Agent $agent */
            $agent = $session->getAgent();
            if (!$agent) {
                return array();
            }
            $dealer = $agent->getDealer();
            $groupIds = array();
            foreach ($dealer->getDealerGroups() as $group) {
                array_push($groupIds, $group->getId());
            }
            return $groupIds;
        }
    }

    /**
     * @return array
     */
    public function getCurrentDealerGroupsNames()
    {
        $session = Mage::getSingleton('customer/session');
        /** @var Dyna_Agent_Model_Agent $agent */
        $agent = $session->getAgent();
        if (!$agent) {
            return array();
        }
        $dealer = $agent->getDealer();
        $groupNames = array();
        foreach ($dealer->getDealerGroups() as $group) {
            array_push($groupNames, $group->getName());
        }
        return $groupNames;
    }

    /**
     * @param $request
     * @param $controller
     */
    public function checkOrderEditMode($request, $controller)
    {
        if (Mage::getSingleton('customer/session')->getOrderEditMode() && $request->getRouteName() == 'cms' && $request->getControllerName() == 'index' && $request->getActionName() == 'index') {

            if (!$request->getParam('packageId')) {
                Mage::getSingleton('core/session')->setGeneralError(
                    Mage::helper('dyna_checkout')->__('In order edit you must select a package to edit or cancel edit mode.')
                );
                $controller->getResponse()->setRedirect(Mage::getUrl('checkout/cart'))->sendHeaders();
                exit;
            } else {
                if (!Mage::getSingleton('checkout/session')->getIsEditMode()) {
                    Mage::getSingleton('core/session')->setGeneralError(
                        Mage::helper('dyna_checkout')->__('In order edit you must select a package to edit or cancel edit mode.')
                    );
                    $controller->getResponse()->setRedirect(Mage::getUrl('checkout/cart'))->sendHeaders();
                    exit;
                }
            }
        }
    }

    /**
     * Check if the website is associated to the telesales channel
     * @return bool
     */
    public function isTelesalesChannel()
    {
        return ($this->getWebsiteChannel() == Dyna_Agent_Model_System_Config_Channel::WEBSITE_CHANNEL_TELESALES_CODE);
    }

    /**
     * Get the website channel from backend settings
     *
     * @return mixed|string
     */
    public function getWebsiteChannel()
    {
        $channel = Mage::getStoreConfig('agent/general_config/channel');
        // Default is the retail channel
        if (empty($channel)) {
            return Dyna_Agent_Model_System_Config_Channel::WEBSITE_CHANNEL_RETAIL_CODE;
        }

        return $channel;
    }

    /**
     * @return mixed
     */
    public function getResetPasswordText()
    {
        return Mage::getStoreConfig('agent/password_validation/reset_password');
    }

    /**
     * @return array|string
     */
    public function getConfigAllowedUrls()
    {
        $value = Mage::getStoreConfig('agent/general_config/allowed_urls');
        if (!empty($value)) {
            return explode('\n', $value);
        }

        return trim($value);
    }

    /**
     * @return mixed
     */
    public function getIsResetFormEnabled()
    {
        return Mage::getStoreConfig('agent/password_validation/reset_password_form');
    }
}
