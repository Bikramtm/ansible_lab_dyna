<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

//Create default dealer group
Mage::getModel('agent/dealergroup')
    ->setName('Default Dealer Group')
    ->save();

//Create default agent role
Mage::getModel('agent/agentrole')
    ->setRole('Default Role')
    ->save();
