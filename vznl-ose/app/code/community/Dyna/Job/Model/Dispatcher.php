<?php

/**
 * Dispatches jobs to consumers
 * Class Dyna_Job_Model_Dispatcher
 */
class Dyna_Job_Model_Dispatcher
{

    use Dyna_Job_Model_LoggerTrait;

    /**
     * @var Dyna_Job_Model_Consumers_ConsumerInterface[]
     */
    protected $consumers;

    /**
     * Dyna_Job_Model_Dispatcher constructor.
     */
    public function __construct()
    {
        $consumersList = [
            'dyna_job/consumers_submitOrderConsumer'
        ];

        foreach ($consumersList as $consumerId) {
            if (($consumer = Mage::getModel($consumerId)) && $consumer instanceof Dyna_Job_Model_Consumers_ConsumerInterface) {
                $this->consumers[] = $consumer;
            }
        }
    }

    /**
     *
     * @param Dyna_Job_Model_Jobs_Job $job
     * @return bool
     */
    public function dispatch(Dyna_Job_Model_Jobs_Job $job)
    {
        $jobWasProcessed = false;
        foreach ($this->consumers as $consumer) {
            if ($consumer->canProcess($job)) {
                $jobWasProcessed = $consumer->process($job);
                if ($jobWasProcessed) {
                    break;
                }
            }
        }

        return $jobWasProcessed;
    }
}