<?php

/**
 * Class Dyna_Job_Model_Config
 */
class Dyna_Job_Model_Config_Config
{
    /**
     * @var string
     */
    protected $hostname;
    /**
     * @var string
     */
    protected $port;
    /**
     * @var string
     */
    protected $pass;
    /**
     * @var string
     */
    protected $user;
    /**
     * @var string
     */
    protected $vhost;
    /**
     * @var string
     */
    protected $queue;
    /**
     * @var string
     */
    protected $exchange;
    /**
     * @var int
     */
    protected $maxRetries;
    /**
     * @var
     */
    protected $secondsBetweenRetries;
    /**
     * @var string
     * Delay queue name
     */
    protected $delayQueue;
    /**
     * @var mixed
     * Path to the PHP binary
     */
    protected $phpPath;
    /**
     * @var bool
     * Use SSL
     */
    protected $amqpSSL;
    /**
     * @var bool
     */
    protected $amqpVerifyPeer;
    /**
     * @var bool
     */
    protected $amqpAllowSelfSigned;

    /**
     * @return int
     */
    public function getMaxRetries(): int
    {
        return $this->maxRetries;
    }

    public function getPhpPath(): string
    {
        return $this->phpPath;
    }

    /**
     * @return mixed
     */
    public function getSecondsBetweenRetries()
    {
        return $this->secondsBetweenRetries;
    }

    public function __construct(array $configData)
    {
        $this->hostname = $configData['hostname'];
        $this->port = $configData['port'];
        $this->pass = $configData['pass'];
        $this->user = $configData['user'];
        $this->vhost = $configData['vhost'];
        $this->queue = $configData['queue'];
        $this->exchange = $configData['exchange'];
        $this->maxRetries = $configData['maxRetries'];
        $this->secondsBetweenRetries = $configData['secondsBetweenRetries'];
        $this->clearJobsOlderThan = $configData['clearJobsOlderThan'];
        $this->delayQueue = $configData['delayQueue'];
        $this->phpPath = $configData['phpPath'];
        $this->amqpSSL = $configData['amqpSSL'];
        $this->amqpVerifyPeer = $configData['amqpVerifyPeer'];
        $this->amqpAllowSelfSigned = $configData['amqpAllowSelfSigned'];
    }

    /**
     * @return mixed
     */
    public function getDelayQueue()
    {
        return $this->delayQueue;
    }

    /**
     * @return string
     */
    public function getHostname(): string
    {
        return $this->hostname;
    }

    /**
     * @return string
     */
    public function getPort(): string
    {
        return $this->port;
    }

    /**
     * @return string
     */
    public function getPass(): string
    {
        return $this->pass;
    }

    /**
     * @return string
     */
    public function getUser(): string
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getVhost(): string
    {
        return $this->vhost;
    }

    /**
     * @return string
     */
    public function getQueue(): string
    {
        return $this->queue;
    }

    /**
     * @return string
     */
    public function getExchange(): string
    {
        return $this->exchange;
    }

    /**
     * @return bool
     */
    public function isAmqpSSL(): bool
    {
        return $this->amqpSSL;
    }

    /**
     * @return bool
     */
    public function isAmqpVerifyPeer(): bool
    {
        return $this->amqpVerifyPeer;
    }

    /**
     * @return bool
     */
    public function isAmqpAllowSelfSigned(): bool
    {
        return $this->amqpAllowSelfSigned;
    }
}