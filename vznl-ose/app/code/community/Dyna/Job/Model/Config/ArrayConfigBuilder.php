<?php
class Dyna_Job_Model_Config_ArrayConfigBuilder
{
    public function build(): Dyna_Job_Model_Config_Config
    {
        $data = [
            'hostname' => '127.0.0.1',
            'port' => '5672',
            'pass' => 'vagrant',
            'user' => 'vagrant',
            'vhost' => '/',
            'queue' => 'dyna_jobs',
            'exchange' => 'jobs_exchange',
            'maxRetries' => 3,
            'secondsBetweenRetries' => 10,
            'clearJobsOlderThan' => 1000,
            'delayQueue' => 'delay_queue',
            'phpPath' => '',
        ];

        $config = new Dyna_Job_Model_Config_Config($data);

        return $config;
    }
}