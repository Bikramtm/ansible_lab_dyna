<?php

use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Wire\AMQPTable;

/**
 * Retrieves queue messages and invokes a php script to handle each message.
 * Class Dyna_Job_Model_StandaloneConsumer
 */
class Dyna_Job_Model_StandaloneJobRunner
{
    const INVALID_JOB = 65;
    const JOB_NOT_PROCESSED = 1;
    const JOB_PROCESSED = 0;

    use Dyna_Job_Model_LoggerTrait;

    /** @var  Dyna_Job_Model_Dispatcher */
    protected $dispatcher;
    /** @var  Dyna_Job_Model_Config_Config */
    protected $config;
    /**
     * @var \AMQPChannel
     */
    protected $channel;
    /**
     * @var Dyna_Job_Model_MessageTranslator
     * Converts JSON encoded jobs to Dyna_Job_Model_Jobs_Job
     */
    protected $translator;

    /**
     * Dyna_Job_Model_Consumer constructor.
     * @param $arguments
     */
    public function __construct($arguments)
    {
        if (isset($arguments['dispatcher']) && $arguments['dispatcher'] instanceof Dyna_Job_Model_Dispatcher) {
            $this->dispatcher = $arguments['dispatcher'];
        } else {
            throw new InvalidArgumentException('Dyna_Job_Model_Consumer requires a Dyna_Job_Model_Dispatcher argument');
        }

        if (isset($arguments['config']) && $arguments['config'] instanceof Dyna_Job_Model_Config_Config) {
            $this->config = $arguments['config'];
        } else {
            throw new InvalidArgumentException('Dyna_Job_Model_Consumer requires a Dyna_Job_Model_Config_Config argument');
        }

        if (isset($arguments['messageTranslator']) && $arguments['messageTranslator'] instanceof Dyna_Job_Model_MessageTranslator) {
            $this->translator = $arguments['messageTranslator'];
        } else {
            throw new InvalidArgumentException('Dyna_Job_Model_Consumer requires a Dyna_Job_Model_MessageTranslator argument');
        }
    }

    /**
     *
     * @param $msgBody
     * @internal param $msg
     * @return int
     */
    public function run($msgBody)
    {
        try {
            $job = $this->translator->setMessage($msgBody)->translateToJob();
        } catch (Exception $e) {
            printf('Message could not be converted to a valid job: %s', $msgBody);
            return self::INVALID_JOB;
        }

        $jobId = $job->getUniqueID();

        try {
            $this->dispatcher->dispatch($job);
        } catch (Exception $e) {
            printf('Failed to consume job %s with error %s', $jobId, $e->getMessage());
            return self::JOB_NOT_PROCESSED;
        }

        printf('Job %s consumed.', $jobId);
        return self::JOB_PROCESSED;
    }
}