<?php

interface Dyna_Job_Model_Consumers_ConsumerInterface
{
    public function canProcess(Dyna_Job_Model_Jobs_Job $job): bool;
    public function process(Dyna_Job_Model_Jobs_Job $job): bool;
}