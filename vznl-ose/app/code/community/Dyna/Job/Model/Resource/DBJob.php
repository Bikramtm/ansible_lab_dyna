<?php

class Dyna_Job_Model_Resource_DBJob extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('dyna_job/dyna_job_pending_jobs', 'entity_id');
    }

}