<?php
/**
 *
 * Class Dyna_Job_Model_DBJob
 * @method getJobHash() : string;
 * @method getJobContent() : string;
 * @method setJobHash($jobHash);
 * @method setJobContent($content);
 */
class Dyna_Job_Model_DBJob extends Mage_Core_Model_Abstract
{

    protected function _construct()
    {
        $this->_init('dyna_job/dBJob');
    }

}