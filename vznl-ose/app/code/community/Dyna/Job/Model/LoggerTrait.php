<?php

trait Dyna_Job_Model_LoggerTrait
{
    public function log($message, $level = null)
    {
        Mage::log($message, $level, 'dyna_job.log');
    }
}