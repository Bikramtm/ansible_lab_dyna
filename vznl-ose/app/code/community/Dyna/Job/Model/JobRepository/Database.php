<?php

class Dyna_Job_Model_JobRepository_Database
{
    /**
     * As the API does not have database access, we need to persist the projection to the database here and pass the hash around
     * @param Dyna_MyVFApi_Model_Projection $projection
     * @return string
     */
    public function storeProjection(Dyna_MyVFApi_Model_Projection $projection)
    {
        $job = $projection->getJobData();

        return $this->store($job);
    }

    /**
     *
     * @param Dyna_Job_Model_Jobs_Job $job
     * @return string
     * @throws Exception
     * Store method must return an identifier that will be used to retrieve the job at a later time
     */
    public function store(Dyna_Job_Model_Jobs_Job $job)
    {
        $messageBody = base64_encode(json_encode($job));
        $jobHash = md5($messageBody);

        $dbJob = Mage::getModel('dyna_job/dBJob');
        $dbJob->setJobHash($jobHash);
        $dbJob->setJobContent($messageBody);
        $dbJob->save();

        return $jobHash;
    }

    /**
     * Message is base64_encode(job_hash)
     * @param string $message
     * @return string JSON
     * @throws Exception
     */
    public function retrieve(string $message): string
    {
        $jobIdentifier = base64_decode($message);
        $dbJob = Mage::getModel('dyna_job/dBJob')->load($jobIdentifier, 'job_hash');
        if (!$dbJob->getId()) {
            throw new Exception('Could not find job in the database');
        }
        $content = $dbJob->getJobContent();
        $content = base64_decode($content);

        return $content;
    }

    public function cleanup(string $message)
    {
        $jobIdentifier = base64_decode($message);
        $dbJob = Mage::getModel('dyna_job/dBJob')->load($jobIdentifier, 'job_hash');
        if ($dbJob->getId()) {
            $dbJob->delete();
        }
    }
}