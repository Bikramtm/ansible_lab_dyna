<?php
error_reporting(E_ALL);
/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();


$installer->run("
ALTER TABLE `{$installer->getTable('dyna_job/dyna_job_pending_jobs')}` MODIFY COLUMN `job_content` longtext;
");
$this->endSetup();