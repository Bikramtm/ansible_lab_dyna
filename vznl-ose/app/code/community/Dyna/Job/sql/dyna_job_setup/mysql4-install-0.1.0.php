<?php

/** @var Mage_Catalog_Model_Resource_Setup $this */
$this->startSetup();
/** Create addons table */
$jobTable = new Varien_Db_Ddl_Table();
$jobTable->setName("dyna_jobs");

$jobTable->addColumn("entity_id", Varien_Db_Ddl_Table::TYPE_BIGINT, null, [
    'unsigned' => true,
    'primary' => true,
    'nullable' => false,
    'auto_increment' => true,
    'comment' => "Primary key",
])
    ->addColumn("job_hash", Varien_Db_Ddl_Table::TYPE_TEXT, 32, [
        'comment' => "Job ",
    ])
    ->addColumn("job_content", Varien_Db_Ddl_Table::TYPE_TEXT, null, [
        'comment' => "Job Content",
    ])
    ->addIndex(
        $this->getConnection()->getIndexName("dyna_jobs", "job_hash"),
        "job_hash"
    );


$this->getConnection()->createTable($jobTable);

$this->endSetup();

