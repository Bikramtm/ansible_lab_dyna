<?php
class Dyna_Job_Helper_PHPBinaryFinder
{
    public function find(): string
    {
        if (PHP_BINARY && is_file(PHP_BINARY)) {
            return PHP_BINARY;
        }
        if ($php = getenv('PHP_PATH')) {
            if (!is_executable($php)) {
                return '';
            }
            return $php;
        }

        return shell_exec('which php');
    }
}