<?php

class Dyna_ProductMatchRule_Block_Adminhtml_Rules_ContextRenderer extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    protected $_isExport = false;

    /**
     * Custom renderer for process context in Compatibility rules grid
     * @param Varien_Object $row
     * @return string
     */
    public function render(Varien_Object $row)
    {
        $contextNames = "";
        $contextIds = $row->getProcessContextIds();

        if ($contextIds) {
            foreach ($contextIds as $contextId) {
                $context = Mage::getModel('dyna_catalog/processContext')->load($contextId);

                // When exporting, export the website id, not website name
                if ($this->_isExport) {
                    if ($code = $context->getCode()) {
                        $contextNames = $contextNames ? $contextNames . "," . $code : $code;
                    }
                } else {
                    if ($context->getName()) {
                        $contextNames = $contextNames ? $contextNames . ",<br>" . $context->getCode() : $context->getCode();
                    }
                }
            }
        }

        return $contextNames;
    }
}
