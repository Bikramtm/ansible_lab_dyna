<?php
require_once Mage::getModuleDir('controllers', 'Omnius_ProductMatchRule') . DS . 'Adminhtml' . DS . 'RulesController.php';

/**
 * class Dyna_ProductMatchRule_Adminhtml_RulesController
 *
 * Default Rules Administration Controller
 */
class Dyna_ProductMatchRule_Adminhtml_RulesController extends Omnius_ProductMatchRule_Adminhtml_RulesController
{
    /**
     * Method that throws the error based on a message
     *
     * @param mixed $message
     * @param $data
     */
    protected function throwErrror($message, $data)
    {
        Mage::getSingleton('adminhtml/session')->addError($message);
        Mage::getSingleton('adminhtml/session')->setFormData($data);
        $this->_redirect('*/*/edit',
            array('product_match_rule_id' => $this->getRequest()->getParam('product_match_rule_id')));
    }

    /**
     * Perform the validation and saving of the announcements code
     *
     */
    public function saveAction()
    {
        //todo clean up the left_id functionality; now it is a condition
        /** @var Dyna_ProductMatchRule_Helper_Data $helper */
        $helper = Mage::helper('productmatchrule');
        if ($data = $this->getRequest()->getPost()) {
            $websiteIds = $data['website_id'];
            $processContextIds = isset($data['process_context']) ? $data['process_context'] : [];

            // if the source type == Product => the source is mandatory
            if (!$data['source_type'] && !$this->validateRequiredSource($data)) {
                $this->throwErrror('The Source condition is mandatory for Product selection Source type', $data);
                return;
            }

            if (isset($data['left_id']['product']) && $data['left_id']['product'] == -1) {
                unset($data['left_id']['product']);
            }
            if (isset($data['right_id']['product']) && $data['right_id']['product'] == -1) {
                unset($data['right_id']['product']);
            }

            if (isset($data['left_id']['category'])
                && (!isset($data['left_id']['category']) || $data['left_id']['category'] == -1)
            ) {
                unset($data['left_id']['category']);
            }
            if (isset($data['right_id']['product'])
                && (!isset($data['right_id']['category']) || $data['right_id']['category'] == -1)
            ) {
                unset($data['right_id']['category']);
            }

            if (isset($data['rule']['conditions'])) {
                $data['conditions'] = $data['rule']['conditions'];
            }
            unset($data['rule']);

            $leftId = (count($data['conditions']) > 1);

            $rightId = empty($data['right_id'])
                ? 0
                : ((empty($data['right_id']['product']) && empty($data['right_id']['category_visible']))
                    ? 0
                    : ((empty($data['right_id']['product']) || $data['right_id']['product'] == -1)
                        ? ((empty($data['right_id']['category_visible']) || $data['right_id']['category_visible'] == -1)
                            ? 0
                            : (int)$data['right_id']['category_visible']
                        )
                        : (int)$data['right_id']['product']
                    )
                );

            /**
             * the left_id is mandatory when the source type is product;
             * the service_source_expression is mandatory if the source type is service;
             * the right_id is always mandatory
             */
            if (!$leftId &&
                ($data['source_type'] && empty($data['service_source_expression'])) ||
                (!$rightId && empty($data['service_target_expression']))
            ) {
                $this->throwErrror('You need to select both Source and Target (for Product source type) or Source expression and Target (for Service source type)', $data);
                return;
            }

            if (!$data['source_type'] && empty($data['source_collection'])) {
                $this->throwErrror('The source collection is mandatory for Product Source type.', $data);
                return;
            }

            try {
                $this->handleOperation($data);
                $this->validateOperationValue($data);

                if (!in_array($data['operation'], $helper->getOperationsWithValues())) {
                    $data['operation_value'] = null;
                }

                $data['product_match_rule_id'] = $this->getRequest()->getParam('product_match_rule_id');
                // Parse POST data to Dyna_ProductMatchRule_Model_MatchRule model
                $models = $helper->setRuleData($data, false);
                if (!is_array($models)) {
                    // Error occurred
                    $this->throwErrror($models, $data);

                    return;
                }

                foreach ($models as $model) {
                    /** @var $model Dyna_ProductMatchRule_Model_Rule */
                    $model->save();
                    $model->setWebsiteIds($websiteIds);
                    $model->setProcessContextsIds($processContextIds);
                }

                Mage::getSingleton('adminhtml/session')->addSuccess(
                    $helper->__('The rule was successfully saved')
                );
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('product_match_rule_id' => $model->getProductMatchRuleId()));

                    return;
                }
            } catch (Exception $e) {
                $this->throwErrror($e->getMessage(), $data);

                return;
            }
            $this->_redirect('*/*/');

            return;
        }
        Mage::getSingleton('adminhtml/session')->addError(
            $helper->__('Unable to find item to save')
        );
        $this->_redirect('*/*/');
    }

    /**
     * Gather data and generate edit form
     *
     */
    public function editAction()
    {
        $id = $this->getRequest()->getParam('product_match_rule_id');
        $model = Mage::getModel('productmatchrule/rule')->load($id);

        if ($model->getProductMatchRuleId() || $id == 0) {
            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);

            if (!empty($data)) {
                $model->setData($data);
            }
            $websites = [];
            // Read the rule for all websites
            $allRules = $model->loadAll();
            foreach ($allRules as $rule) {
                $websites[] = $rule->getWebsiteId();
            }

            $model->setWebsiteId($websites);
            $connection = Mage::getSingleton('core/resource')->getConnection('core/read');
            if ($id != 0) {
                $processContexts = $connection->fetchCol("select process_context_id from product_match_rule_process_context where rule_id = $id");
                if (count($processContexts) > 0) {
                    $model->setProcessContextIdForFormDisplay($processContexts);
                }
            }

            Mage::register('rules_data', $model);

            $this->loadLayout();
            $this->_setActiveMenu('catalog/rule');

            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Rule manager'),
                Mage::helper('adminhtml')->__('Rule manager'));
            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'),
                Mage::helper('adminhtml')->__('Item News'));

            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

            $this->_addContent($this->getLayout()->createBlock('productmatchrule/adminhtml_rules_edit'))
                ->_addLeft($this->getLayout()->createBlock('productmatchrule/adminhtml_rules_edit_tabs'));

            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')
                ->addError(Mage::helper('productmatchrule')->__('Item does not exist'));
            $this->_redirect('*/*/');
        }
    }

    protected function validateRequiredSource($data)
    {
        if (isset($data['rule']) && isset($data['rule']['conditions']) && count($data['rule']['conditions']) > 1) {
            foreach ($data['rule']['conditions'] as $ruleCondition) {
                if (!isset($ruleCondition['value'])) {
                    return false;
                }
                $value = (string)$ruleCondition['value'];
                if($value == "") {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Validate the operation value:
     * - is required when the operation is min, max or equal and must be numeric
     * - should not be present for any other operation
     *
     * @access protected
     * @param array $data
     * @throws Exception
     */
    protected function validateOperationValue($data)
    {
        /** @var Dyna_ProductMatchRule_Helper_Data $helper */
        $helper = Mage::helper('productmatchrule');
        // the operation exists
        if (!empty($data['operation'])) {
            // the operation is min, max or eq
            if (in_array($data['operation'], $helper->getOperationsWithValues())) {
                // the operation value is empty => error
                if (empty($data['operation_value'])) {
                    throw new Exception($helper->__('Please insert an operation value'));
                }
                // the operation value is not numeric => error
                if (!is_numeric($data['operation_value'])) {
                    throw new Exception($helper->__('The operation value must be numeric.'));
                }
            } // the operation value is not min, max or eq and the operation value is present
            elseif (!empty($data['operation_value'])) {
                throw new Exception($helper->__('The operation value is needed only for operations: min, max, equal.'));
            }
        }
    }

   
    protected function parseFile()
    {
        if ($_FILES['file']['tmp_name']) {
            $uploader = new Mage_Core_Model_File_Uploader('file');
            $uploader->setAllowedExtensions(array('csv'));
            $path = Mage::app()->getConfig()->getTempVarDir() . '/import/';
            $uploader->save($path);
            if ($uploadFile = $uploader->getUploadedFileName()) {
                $newFilename = 'import-' . date('YmdHis') . '_' . $uploadFile;
                rename($path . $uploadFile, $path . $newFilename);
            }
        }

        if (isset($newFilename) && $newFilename) {
            /** @var Dyna_ProductMatchRule_Model_Importer $importer */
            $importer = Mage::getModel('productmatchrule/importer');
            $success = $importer->import($path . $newFilename);

            if ($success) {
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    $this->__('Import file successfully parsed and imported into database')
                );
            } else {
                Mage::getSingleton('adminhtml/session')->addError(
                    $this->__('Something went wrong with this file')
                );
            }

            $this->_redirect('*/*');
        }
    }

    /**
     * todo validate the operation
     * @param array $data
     * @throws Exception
     */
    protected function handleOperation(&$data)
    {
        // Check if operation type constant is defined
        if (!isset($data['operation_type']) || !$data['operation_type']) {
            throw new Exception('Operation type impossible.');
        }

        $left = isset($data['left_id']) ? key($data['left_id']) : null;
        $right = key($data['right_id']);
        if (!$left && $data['source_type'] && !empty($data['service_source_expression'])) {
            $left = 'service';
        }
        if (isset($data['conditions'])) {
            $left = 'mix';
        }

        if ($left && (isset($data['left_id']) && isset($data['left_id'][$left]))) {
            $data['left_id'] = $data['left_id'][$left];
        } else {
            unset($data['left_id']);
        }

        $data['right_id'] = $data['right_id'][$right];
    }
}
