<?php

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();

$tableName = $installer->getTable('product_match_rule_source_target_index');

if (! $installer->getConnection()->isTableExists($tableName)) {
// Defining table structure via object
    $table = $installer->getConnection()
        ->newTable($tableName)
        ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
            array(
                'unsigned' => true,
                'primary' => true,
                'nullable' => false,
                'auto_increment' => true,
            ))
        ->addColumn('website_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null,
            array(
                'unsigned' => true,
                'primary' => true,
                'nullable' => false,
            ))
        ->addColumn('rule_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
            array(
                'unsigned' => true,
                'primary' => true,
                'nullable' => false,
            ))
        ->addColumn('package_type', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
            array(
                'unsigned' => true,
                'nullable' => true,
            ))
        ->addColumn('source_product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
            array(
                'unsigned' => true,
                'nullable' => false,
            ))
        ->addColumn('target_product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
            array(
                'unsigned' => true,
                'nullable' => false,
            ))
        ->addForeignKey(
            $installer->getFkName('product_match_rule_source_target_index_website', 'website_id', 'core/website', 'website_id'),
            'website_id', $installer->getTable('core/website'), 'website_id',
            Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
        )
        ->addForeignKey(
            $installer->getFkName('product_match_rule_source_target_index_package_type', 'package_type', 'package/type', 'entity_id'),
            'package_type', $installer->getTable('package/type'), 'entity_id',
            Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
        )
        ->addForeignKey(
            $installer->getFkName('product_match_rule_source_target_index_rule_id', 'rule_id', 'productmatchrule/rule', 'product_match_rule_id'),
            'rule_id', $installer->getTable('productmatchrule/rule'), 'product_match_rule_id',
            Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
        )
        ->addForeignKey(
            $installer->getFkName('product_match_rule_source_target_index_source_product_id', 'source_product_id', 'catalog/product', 'entity_id'),
            'source_product_id', $installer->getTable('catalog/product'), 'entity_id',
            Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
        )
        ->addForeignKey(
            $installer->getFkName('product_match_rule_source_target_index_target_product_id', 'target_product_id', 'catalog/product', 'entity_id'),
            'target_product_id', $installer->getTable('catalog/product'), 'entity_id',
            Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
        );
    // Executing table creation object
    $installer->getConnection()->createTable($table);
}

$installer->endSetup();
