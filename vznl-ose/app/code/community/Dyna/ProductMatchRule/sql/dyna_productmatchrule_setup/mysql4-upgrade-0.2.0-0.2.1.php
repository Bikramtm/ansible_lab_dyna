<?php
$installer = $this;
$installer->startSetup();

$connection= $installer->getConnection();

$connection->addColumn($this->getTable('product_match_rule'), 'operation_value', array(
    'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
    'unsigned' => true,
    'after' => 'operation',
    'comment' => 'Operation value for operations min,max,eq',
));

$installer->endSetup();