<?php

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();

/** @var Varien_Db_Adapter_Interface $connection */
$connection = $installer->getConnection();

$tableName = $installer->getTable('product_match_rule_source_target_index');

if ($connection->isTableExists($tableName)) {
    $connection->modifyColumn($tableName, "source_product_id", [
        'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'nullable' => true,
        'unsigned' => true,
        'after' => 'package_type',
        'comment' => 'Source product id',
        "default" => null
    ], false);

}

$installer->endSetup();
