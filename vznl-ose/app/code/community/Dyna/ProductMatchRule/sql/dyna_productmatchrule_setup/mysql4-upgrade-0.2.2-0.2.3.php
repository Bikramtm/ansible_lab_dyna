<?php
/** @var Mage_Catalog_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();

$table = $this->getTable('product_match_rule');

/** @var Magento_Db_Adapter_Pdo_Mysql $connection */
$connection = $installer->getConnection();

$connection->addColumn($table, 'conditions_serialized', array(
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'after' => 'priority',
    'comment' => 'Serialized conditions'
));

$installer->endSetup();
