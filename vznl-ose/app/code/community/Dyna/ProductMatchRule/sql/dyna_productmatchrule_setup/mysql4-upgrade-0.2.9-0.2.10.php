<?php
/** @var Mage_Catalog_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();

/** @var Magento_Db_Adapter_Pdo_Mysql $connection */
$connection = $installer->getConnection();

if (!$connection->tableColumnExists($this->getTable('productmatchrule/rule'), 'override_initial_selectable')) {
    // Adding Override Initial Selectable column for Option of Option Products
    $connection->addColumn($this->getTable('productmatchrule/rule'), 'override_initial_selectable', array(
        'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'nullable' => true,
        'default' => null,
        'after' => "operation_value",
        'comment' => 'Overriding selectability for option of option products'
    ));
}
if (!$connection->tableColumnExists($this->getTable('productmatchrule/matchRule'), 'override_initial_selectable')) {
    $connection->addColumn($this->getTable('productmatchrule/matchRule'), 'override_initial_selectable', array(
        'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'nullable' => true,
        'default' => null,
        'comment' => 'Only for overriding selectability for option of option products'
    ));
}

$installer->endSetup();
