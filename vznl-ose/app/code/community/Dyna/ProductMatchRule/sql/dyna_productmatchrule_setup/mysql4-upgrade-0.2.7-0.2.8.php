<?php
/** @var Mage_Catalog_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();

/*****************************************************************************
 * TRUNCATING TABLES BECAUSE FOREIGN KEYS FAIL
 *****************************************************************************/

$truncateTableList = array(
    $this->getTable('dyna_productmatchrule/service'),
    $this->getTable('productmatchrule/defaulted'),
    $this->getTable('productmatchrule/matchRule'),
);

foreach ($truncateTableList as $tableName) {
    $installer->getConnection()->query("truncate " . $tableName);
}

$installer->getConnection()->query("delete from " .     $this->getTable("productmatchrule/rule"));

/*****************************************************************************
 * EOF TRUNCATING TABLES BECAUSE FOREIGN KEYS FAIL
 *****************************************************************************/

/*****************************************************************************
 * UPDATING SERVICE EXPRESSION RULES INDEXER
 *****************************************************************************/

$productMatchServiceIndexer = $this->getTable('dyna_productmatchrule/service');

/** @var Magento_Db_Adapter_Pdo_Mysql $connection */
$connection = $installer->getConnection();

// Dropping hash column as it is no longer needed. Logic moves to rule_id with foreign key to product match rule
$connection->dropColumn($productMatchServiceIndexer, "service_hash");

// Defining indexer tables on which reference columns will be added
$tablesList = array(
    $productMatchServiceIndexer,
    $this->getTable('productmatchrule/defaulted'),
    $this->getTable('productmatchrule/matchRule'),
);

foreach ($tablesList as $updatedTable) {

    // Adding rule reference column
    $connection->addColumn($updatedTable, 'rule_id', array(
        'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'nullable' => true,
        'default' => null,
        'after' => "website_id",
        'comment' => 'The rule id from which this entry comes'
    ));

    // Adding package type reference to service indexer
    $connection->addColumn($updatedTable, 'package_type', array(
        'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'nullable' => true,
        'default' => null,
        'after' => "rule_id",
        'comment' => 'The package type to which current rule applies'
    ));

    // Adding foreign key to defined rules
    $connection->addForeignKey(
        $connection->getForeignKeyName($updatedTable, "rule_id", $this->getTable("productmatchrule/rule"), "product_match_rule_id"),
        $updatedTable,
        "rule_id",
        $this->getTable("productmatchrule/rule"),
        "product_match_rule_id"
    );

    // Adding foreign key to package type to which this entry is related
    $connection->addForeignKey(
        $connection->getForeignKeyName($updatedTable, "package_type", $this->getTable("package/type"), "entity_id"),
        $updatedTable,
        "package_type",
        $this->getTable("package/type"),
        "entity_id"
    );
}

// Adding package type reference to service indexer
$connection->addColumn($this->getTable("productmatchrule/rule"), 'package_type', array(
    'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
    'nullable' => true,
    'default' => null,
    'after' => "product_match_rule_id",
    'comment' => 'The package type to which current rule applies'
));

// Updating product match rules to include package types
$connection->addForeignKey(
    $connection->getForeignKeyName($this->getTable("productmatchrule/rule"), "package_type", $this->getTable("package/type"), "entity_id"),
    $this->getTable("productmatchrule/rule"),
    "package_type",
    $this->getTable("package/type"),
    "entity_id"
);

$installer->endSetup();
