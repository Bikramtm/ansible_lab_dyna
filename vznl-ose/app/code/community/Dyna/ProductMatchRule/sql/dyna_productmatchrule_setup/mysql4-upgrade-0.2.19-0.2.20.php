<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Catalog_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();
$connection = $installer->getConnection();
$tableName = $installer->getTable('productmatchrule/rule');

if ($connection->tableColumnExists($tableName, 'target_source_expression') === true) {
    $connection->changeColumn($tableName, 'target_source_expression', 'service_target_expression', array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'default' => null,
        'nullable' => true,
        'after' => "service_source_expression",
        'comment' => 'Service Expression for targeting products'
    ));
}

$installer->endSetup();
