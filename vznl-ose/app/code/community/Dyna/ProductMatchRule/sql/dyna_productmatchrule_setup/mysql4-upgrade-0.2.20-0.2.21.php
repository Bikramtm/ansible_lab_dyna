<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;
$installer->startSetup();
$connection = $installer->getConnection();

// Adding is_source
if (!$connection->tableColumnExists('product_match_service_index', 'source_product_id')) {
    $connection->addColumn('product_match_service_index', 'source_product_id', array(
        'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'nullable' => true,
        'unsigned' => true,
        'after' => 'package_type',
        'comment' => 'Source product id'
    ));
}

if ($connection->tableColumnExists('product_match_service_index', 'target_product_id')) {
    $connection->changeColumn('product_match_service_index', 'target_product_id', 'target_product_id', array(
        'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'unsigned' => true,
        'nullable' => true,
    ));
}

$installer->endSetup();
