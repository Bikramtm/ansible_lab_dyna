<?php
/**
 * Copyright (c) 2017. Dynacommerce B.V.
 */

/** @var Mage_Catalog_Model_Resource_Setup $this */

$whitelistIndex = $this->getTable('productmatchrule/matchRule');
// get all indexes from db for this table
$indexes = $this->getConnection()->fetchAll(sprintf("SHOW INDEX FROM %s", $whitelistIndex));

$notNeededIndexedColumns = array('source_product_id', 'target_product_id', 'package_type', 'website_id');

$indexesByKeys = array();
// count each index's column
foreach ($indexes as $index) {
    $indexesByKeys[$index['Key_name']][] = $index['Column_name'];
}

$indexesToBeRemoved = array();
foreach ($indexesByKeys as $indexName => $columns) {
    // if index is set on only one column, check if it is in the "to be removed" list
    if (count($columns) == 1 && ($columnName = current($columns)) && in_array($columnName, $notNeededIndexedColumns)) {
        $indexesToBeRemoved[] = $indexName;
    }
}

foreach ($indexesToBeRemoved as $indexName) {
    $this->getConnection()->dropIndex($whitelistIndex, $indexName);
}
