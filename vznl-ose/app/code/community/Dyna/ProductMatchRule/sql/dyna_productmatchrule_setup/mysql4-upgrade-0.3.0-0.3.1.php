<?php
/**
 * Copyright (c) 2017. Dynacommerce B.V.
 */

/** @var Mage_Catalog_Model_Resource_Setup $this */
/** @var Magento_Db_Adapter_Pdo_Mysql  $connection */
$connection = $this->getConnection();
$theSwitch =
<<<SQL
RENAME TABLE product_match_whitelist_index TO product_match_whitelist_index_backup;

CREATE TABLE `product_match_whitelist_index` (
  `whitelist_id` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Whitelist_id',
  `website_id` TINYINT(2) UNSIGNED NOT NULL COMMENT 'Website_id',
  `rule_id` MEDIUMINT(5) UNSIGNED NOT NULL,
  `package_type` TINYINT(2) UNSIGNED NOT NULL,
  `source_product_id` MEDIUMINT(6) UNSIGNED NOT NULL COMMENT 'Source_product_id',
  `target_product_id` MEDIUMINT(6) UNSIGNED NOT NULL COMMENT 'Target_product_id',
  `override_initial_selectable` TINYINT(1) DEFAULT NULL COMMENT 'Only for overriding selectability for option of option products',
  `process_context_id` VARCHAR(60) COMMENT 'Process context ids',
  `source_collection` TINYINT(1) DEFAULT '0' COMMENT 'Source collection for defaulted rules',
  PRIMARY KEY (`whitelist_id`),
  UNIQUE KEY `IDX_PRD_MATCH_WHITELIST_UNIQUE_ID_ENTRY` (`website_id`,`package_type`,`source_product_id`,`target_product_id`,`override_initial_selectable`,`process_context_id`,`rule_id`,`source_collection`),
  KEY `IDX_PRD_MATCH_WHITELIST_IDX_WS_ID_PACKAGE_TYPE_SOURCE_PRD_ID` (`website_id`,`package_type`,`source_product_id`,`process_context_id`,`source_collection`),
  KEY `IDX_PRD_MATCH_WHITELIST_IDX_WS_ID_PACKAGE_TYPE_TARGET_PRD_ID` (`website_id`,`package_type`,`target_product_id`,`process_context_id`,`source_collection`)
) ENGINE=MYISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='product_match_whitelist_index';

INSERT INTO product_match_whitelist_index SELECT * FROM product_match_whitelist_index_backup;
DROP TABLE product_match_whitelist_index_backup;
SQL;
$connection->query($theSwitch);
