<?php
// Adds indexes to product_match_whitelist_index to speed it up

/** @var Mage_Catalog_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();
$connection = $installer->getConnection();
$tableName = $installer->getTable('productmatchrule/matchRule');

// Get all existing indexes to avoid duplicates
$indexList = $connection->getIndexList($tableName);

$indexName = strtoupper($installer->getIdxName($tableName, array('website_id')));

if (!isset($indexList[$indexName])) {
    $connection->addIndex($tableName,
        $indexName,
        array('website_id')
    );
}

$indexName = strtoupper($installer->getIdxName($tableName, array('website_id', 'package_type', 'source_product_id')));
if (!isset($indexList[$indexName])) {
    $connection->addIndex($tableName,
        $indexName,
        array('website_id', 'package_type', 'source_product_id')
    );
}

$indexName = strtoupper($installer->getIdxName($tableName, array('website_id', 'package_type', 'target_product_id')));
if (!isset($indexList[$indexName])) {
    $connection->addIndex($tableName,
        $indexName,
        array('website_id', 'package_type', 'target_product_id')
    );
}

$installer->endSetup();
