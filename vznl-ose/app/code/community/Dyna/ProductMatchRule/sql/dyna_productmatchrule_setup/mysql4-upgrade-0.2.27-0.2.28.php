<?php
/** @var Mage_Catalog_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();

/** @var Magento_Db_Adapter_Pdo_Mysql $connection */
$connection = $installer->getConnection();

$table = $this->getTable('product_match_rule');

if (!$connection->tableColumnExists($table, $column)) {
    $connection->addColumn($table, 'target_type', array(
        'type' => Varien_Db_Ddl_Table::TYPE_SMALLINT,
        'unsigned' => true,
        'after' => 'service_source_expression',
        'comment' => 'Product selection or Service.',
    ));
}

$installer->endSetup();