<?php
/**
 * Copyright (c) 2017. Dynacommerce B.V.
 */

/** @var Mage_Catalog_Model_Resource_Setup $this */
$this->startSetup();

/** @var Magento_Db_Adapter_Pdo_Mysql  $connection */
$connection = $this->getConnection();

$this->getConnection()->renameTable($this->getTable('product_match_rule_index_process_context'),$this->getTable('product_match_rule_process_context'));

$this->endSetup();
