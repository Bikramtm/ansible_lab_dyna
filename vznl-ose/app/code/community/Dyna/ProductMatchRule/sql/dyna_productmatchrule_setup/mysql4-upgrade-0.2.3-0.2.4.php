<?php

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer           = $this;
$connection          = $installer->getConnection();

$websitesTable       = $installer->getTable('core/website');
$rulesTable          = $installer->getTable('productmatchrule/rule');
$rulesWebsitesTable  = $installer->getTable('dyna_productmatchrule/website');

$installer->startSetup();
/**
 * Create table 'catalogrule/website' if not exists. This table will be used instead of
 * column website_ids of main catalog rules table
 */
if (!$connection->isTableExists($rulesWebsitesTable)) {
    $table = $connection->newTable($rulesWebsitesTable)
        ->addColumn('rule_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'unsigned'  => true,
            'nullable'  => false,
            'primary'   => true,
            "length"    => 10,
            ),
            'Rule Id'
        )
        ->addColumn('website_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned'  => true,
            'nullable'  => false,
            'primary'   => true
            ),
            'Website Id'
        )
        ->addIndex(
            $installer->getIdxName('dyna_productmatchrule/website', array('rule_id')),
            array('rule_id')
        )
        ->addIndex(
            $installer->getIdxName('dyna_productmatchrule/website', array('website_id')),
            array('website_id')
        )
        ->addForeignKey($installer->getFkName('dyna_productmatchrule/website', 'rule_id', 'productmatchrule/rule', 'product_match_rule_id'),
            'rule_id', $rulesTable, 'product_match_rule_id',
            Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
        )
        ->addForeignKey($installer->getFkName('dyna_productmatchrule/website', 'website_id', 'core/website', 'website_id'),
            'website_id', $websitesTable, 'website_id',
            Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
        )
        ->setComment('Product Match Rules To Websites Relations');

    $connection->createTable($table);
}

/**
 * Fill out relation table 'catalogrule/website' with website Ids
 */
if ($connection->tableColumnExists($rulesTable, 'website_id')) {
    $select = $connection->select()
        ->from(array('sr' => $rulesTable), array('sr.product_match_rule_id', 'cw.website_id'))
        ->join(
            array('cw' => $websitesTable),
            $connection->prepareSqlCondition(
                'sr.website_id', array('finset' =>  new Zend_Db_Expr('cw.website_id'))
            ),
            array()
        );
    $query = $select->insertFromSelect($rulesWebsitesTable, array('rule_id', 'website_id'));
    $connection->query($query);
}

/**
 * Eliminate obsolete columns
 */
$connection->dropColumn($rulesTable, 'website_id');

$installer->endSetup();
