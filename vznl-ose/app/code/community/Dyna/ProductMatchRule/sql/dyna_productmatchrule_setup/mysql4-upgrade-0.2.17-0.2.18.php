<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Catalog_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();
$connection = $installer->getConnection();
$tableName = $installer->getTable('productmatchrule/rule');

$connection->addColumn($tableName, 'target_source_expression', array(
    'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
    'default'   => null,
    'nullable'  => true,
    'after'     => "service_source_expression",
    'comment'   => 'Service Source Expression for targeting products'
));

$installer->endSetup();
