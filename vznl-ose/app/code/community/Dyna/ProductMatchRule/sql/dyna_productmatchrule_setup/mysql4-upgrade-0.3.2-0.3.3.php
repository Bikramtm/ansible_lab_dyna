<?php

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();

/** @var Varien_Db_Adapter_Interface $connection */
$connection = $installer->getConnection();

$tableName = $installer->getTable('product_match_rule_source_target_index');

if ($connection->isTableExists($tableName)) {
    $connection->dropTable($tableName);
}
$connection->query("DELETE FROM `index_process` where `indexer_code` = 'product_match_ruleSourceTarget'");

$installer->endSetup();
