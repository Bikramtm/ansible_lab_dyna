<?php
/** @var Mage_Catalog_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();

$table = $this->getTable('product_match_whitelist_index');
$columnName = 'process_context_id';

/** @var Magento_Db_Adapter_Pdo_Mysql $connection */
$connection = $installer->getConnection();

if (!$connection->tableColumnExists($table, $columnName)) {
    $connection->addColumn($table, $columnName, array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'comment' => 'Process context id'
    ));
}

$installer->endSetup();
