<?php

/**
 * Add new table product_match_rule_index_process_context
 * This is a relation table between the 2 tables: product_match_rule (rule_id column) and process_context (process_context_id column)
 *
 * Index:
 * FK rule_id to product_match_rule.product_match_rule_id
 * FK process_context_id to process_context.entity_id
 */
$installer = $this;

$installer->startSetup();

$tableName = $installer->getTable('product_match_rule_index_process_context');

if (! $installer->getConnection()->isTableExists($tableName)) {
    // Defining table structure via object
    $table = $installer->getConnection()
        ->newTable($tableName)
        ->addColumn('rule_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'unsigned'  => true,
            'nullable'  => false,
            "length"    => 10,
        ),
            'Rule Id'
        )
        ->addColumn('process_context_id',Varien_Db_Ddl_Table::TYPE_BIGINT, 10, [
            "unsigned" => true,
            "nullable" => false,
        ], "Process context id")
        ->addForeignKey(
            $installer->getFkName('product_match_rule_index_process_context', 'rule_id', 'product_match_rule', 'product_match_rule_id'),
            'rule_id', 'product_match_rule', 'product_match_rule_id',
            Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
        )
        ->addForeignKey(
            $installer->getFkName('product_match_rule_index_process_context', 'process_context_id', 'process_context', 'entity_id'),
            'process_context_id', 'process_context', 'entity_id',
            Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
        )
        ->addIndex($this->getIdxName('product_match_rule_index_process_context', ['rule_id', 'process_context_id'], Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE),
            ['rule_id', 'process_context_id'], ['type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE])
        ;

    // Executing table creation object
    $installer->getConnection()->createTable($table);
}


$installer->endSetup();

