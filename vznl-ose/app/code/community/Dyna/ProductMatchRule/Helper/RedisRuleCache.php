<?php

class Dyna_ProductMatchRule_Helper_RedisRuleCache
{
    protected $redisClient;

    public function __construct()
    {
        $this->redisClient = $this->getRedisClient();
    }

    public function set($key, $value)
    {
        $this->redisClient->set($key, $value);
    }

    public function get($key)
    {
        return $this->redisClient->get($key);
    }

    public function buildKey($websiteId, $leftID, $rightID)
    {
        return sprintf("%s_%s_%s", $websiteId, $leftID, $rightID);
    }

    /**
     * @return Credis_Client|mixed
     */
    protected function getRedisClient()
    {
        if(!$this->redisClient) {
            $redisHelper = Mage::helper('dyna_productmatchrule/redis');
            $this->redisClient = $redisHelper->buildCredisClient(['persistent' => uniqid()]);
        }

        return $this->redisClient;
    }
}
