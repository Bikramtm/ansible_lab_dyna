<?php

/**
 * Class Data
 */
class Dyna_ProductMatchRule_Helper_Data extends Omnius_ProductMatchRule_Helper_Data
{
    CONST SOURCE_TYPE_PRODUCT_SELECTION = 'Product selection';
    CONST SOURCE_TYPE_SERVICE = 'Service';
    CONST OPERATION_TYPE_PRODUCT = 'PRODUCT';
    CONST OPERATION_TYPE_SERVICE = 'SERVICE';
    CONST OPERATION_TYPE_CATEGORY = 'CATEGORY';

    CONST NO_SOURCE_COLLECTION = 0; // empty
    CONST SOURCE_COLLECTION_CQ = 1; // current quote
    CONST SOURCE_COLLECTION_IB = 2; // installed base
    CONST SOURCE_COLLECTION_QSD = 3; // quote selection delta

    /** @var Omnius_Import_Helper_Data $_helper */
    public $importHelper;
    public $websiteProducts;
    public $websiteCategoryProducts;

    /** @var Dyna_Cache_Model_Cache */
    protected $_cache;
    protected $_websites = [];
    protected $_ruleFromImporter = false;
    protected $packageTypes = [];
    protected $debugIndexer = true;
    protected $debugStaticRules = false;
    protected $debugServiceRules = false;
    protected $debugRulesExpressionParts = false;
    protected $debugSpecificRulesIds = []; // array with specific rule for which expression parts will be debugged
    protected $serviceCompRulesObjects = false;

    private $logFileName = 'product_match_rule_indexer';

    /**
     * @return array
     */
    public function getOperations()
    {
        return array(
            0 => 'Not Allowed',
            1 => 'Allowed',
            2 => 'Defaulted',
            3 => 'Obligated',
            4 => 'Defaulted-Obligated',
            5 => 'Min(n)',
            6 => 'Max(n)',
            7 => 'Eql(n)',
            8 => 'Removal Not Allowed',
        );
    }

    /**
     * Method used to debug specific rules by logging their expression parts
     * @return array
     */
    public function getDebugSpecificRulesIds()
    {
        return $this->debugSpecificRulesIds;
    }

    /**
     * @return array
     */
    public function getServiceInRightOperations()
    {
        $operationTypes = $this->getOperationTypes();

        return [
            array_search("SERVICE-SERVICE", $operationTypes),
            array_search("PRODUCT-SERVICE", $operationTypes),
            array_search("CATEGORY-SERVICE", $operationTypes),
        ];
    }

    /**
     * @return array
     */
    public function getCategoryInRightOperations()
    {
        $operationTypes = $this->getOperationTypes();

        return [
            array_search("PRODUCT-CATEGORY", $operationTypes),
            array_search("CATEGORY-CATEGORY", $operationTypes),
            array_search("SERVICE-CATEGORY", $operationTypes),
        ];
    }

    /**
     * @return array
     */
    public function getOperationTypes()
    {
        return array(
            1 => 'PRODUCT-PRODUCT',
            2 => 'PRODUCT-CATEGORY',
            3 => 'CATEGORY-CATEGORY',
            4 => 'CATEGORY-PRODUCT',
            5 => 'SERVICE-PRODUCT',
            6 => 'SERVICE-CATEGORY',
            9 => 'SERVICE-SERVICE',
            10 => 'PRODUCT-SERVICE',
            11 => 'CATEGORY-SERVICE',
        );
    }

    /**
     * Returns only the operations ids with values
     *
     * @access public
     * @return array
     */
    public function getOperationsWithValues()
    {
        return [
            (string)Dyna_ProductMatchRule_Model_Rule::OP_MIN_N,
            (string)Dyna_ProductMatchRule_Model_Rule::OP_MAX_N,
            (string)Dyna_ProductMatchRule_Model_Rule::OP_EQL_N
        ];
    }

    /**
     * Returns only the operations ids which can override initial selectable
     *
     * @access public
     * @return array
     */
    public function getOperationsWithOverrideInitialSelectable()
    {
        return [
            (string)Dyna_ProductMatchRule_Model_Rule::OP_ALLOWED,
            (string)Dyna_ProductMatchRule_Model_Rule::OP_DEFAULTED,
            (string)Dyna_ProductMatchRule_Model_Rule::OP_DEFAULTED_OBLIGATED,
            (string)Dyna_ProductMatchRule_Model_Rule::OP_OBLIGATED,
            (string)Dyna_ProductMatchRule_Model_Rule::OP_NOTALLOWED
        ];
    }

    /**
     * Not all operations are allowed to have all operation types
     *
     * @access public
     * @return array
     */
    public function getAllowedOperationTypeCombinations()
    {
        $combinations = [];
        $operationTypes = $this->getOperationTypes();
        foreach ($this->getOperations() as $key => $operation) {
            $operation = strtolower($operation);
            switch ($operation) {
                case "defaulted":
                case "defaulted-obligated":
                    // Defaulted* Operations can't have SERVICE or CATEGORY target
                    $combinations[$operation] = [
                        1 => 'PRODUCT-PRODUCT',
                        4 => 'CATEGORY-PRODUCT',
                        5 => 'SERVICE-PRODUCT',
                    ];
                    break;
                case "obligated":
                    // Defaulted* Operations can't have SERVICE or PRODUCT target
                    $combinations[$operation] = [
                        2 => 'PRODUCT-CATEGORY',
                        3 => 'CATEGORY-CATEGORY',
                        6 => 'SERVICE-CATEGORY',
                    ];
                    break;
                // @todo check if there are other operation-operationType special combinations
                default:
                    $combinations[$operation] = $operationTypes;
                    break;
            }
        }
        return $combinations;
    }

    /**
     * Returns only the operations ids which can override initial selectable
     *
     * @access public
     * @return array
     */
    public function getOperationWithOverrideInitialSelectableValues()
    {
        return ['0' => 'No', '1' => 'Yes'];
    }

    /**
     * @return array
     */
    public function getSourceTypes()
    {
        return [
            0 => self::SOURCE_TYPE_PRODUCT_SELECTION,
            1 => self::SOURCE_TYPE_SERVICE,
        ];
    }

    /**
     * @return array
     */
    public function getTargetTypes()
    {
        return [
            0 => self::SOURCE_TYPE_PRODUCT_SELECTION,
            1 => self::SOURCE_TYPE_SERVICE,
        ];
    }

    /**
     * @return array
     */
    public function getSourceCollections()
    {
        return [
            static::NO_SOURCE_COLLECTION => 'N/A',
            static::SOURCE_COLLECTION_CQ => 'Current quote',
            static::SOURCE_COLLECTION_IB => 'Installed base',
            static::SOURCE_COLLECTION_QSD => 'Quote selection delta',
        ];
    }

    /**
     * @return array
     */
    public function getPackageTypes()
    {
        if (empty($this->packageTypes)) {
            $packageTypesCollection = Mage::getModel('dyna_package/packageType')
                ->getCollection();
            foreach ($packageTypesCollection as $packageType) {
                $this->packageTypes[$packageType->getId()] = $packageType->getPackageCode();
            }
        }

        return $this->packageTypes;
    }

    /**
     * @return array
     */
    public function getRuleOrigin()
    {
        return [
            0 => 'Manual',
            1 => 'Import'
        ];
    }

    /**
     * @param $productIds
     * @param null $websiteId
     * @param Dyna_Package_Model_Package $packageModel
     * @param $sourceCollectionId
     * @param $removal
     * @return array
     */
    public function getDefaultedProducts($productIds, $websiteId = null, $packageModel, $sourceCollectionId = Dyna_ProductMatchRule_Helper_Data::SOURCE_COLLECTION_CQ, $removal = false)
    {
        if ($websiteId == null) {
            $websiteId = Mage::app()->getStore()->getWebsiteId();
        }
        /** @var Dyna_ProductMatchRule_Model_Defaulted $defaultedModel */
        $defaultedModel = Mage::getModel('dyna_productmatchrule/defaulted');
        $result = [];
        foreach ($productIds as $productId) {
            $result[$productId] = $defaultedModel->getDefaultedProductsIds($productId, $websiteId, $packageModel, $sourceCollectionId);
        }

        // Executed the service expression rules to check for defaulted products
        $serviceProducts = $defaultedModel->loadDefaultedServiceRules($productIds, $websiteId, $packageModel, $removal);

        if (count($serviceProducts)) {
            $result['service'] = $serviceProducts;
        }

        return $result;
    }

    /**
     * @param array $data
     * @param bool $ruleFromImporter
     * @return array
     */
    public function setRuleData($data, $ruleFromImporter = false)
    {
        $this->_ruleFromImporter = $ruleFromImporter;
        $models = [];

        /**
         * @var Dyna_ProductMatchRule_Model_Rule $model
         */
        $model = Mage::getModel('productmatchrule/rule');

        if (!empty($data['product_match_rule_id'])) {
            $model = $model->load($data['product_match_rule_id']);
        }
        $websitesIds = $data['website_id'];

        if (!empty($data['service_source_expression'])) {
            $data['service_source_expression'] = preg_replace('/\s+/', ' ', $data['service_source_expression']);
            $model->setServiceSourceExpression($data['service_source_expression']);
        }

        switch ($data['operation_type']) {
            case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_P2P:
                if (is_string($response = $this->validateRuleDataForOpTypeP2P($data, $websitesIds))) {
                    return $response;
                }
                /** Preserve left_id from condition */
                $data['left_id'] = $this->getLeftId($data);
                break;
            case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_P2C:
                if (is_string($response = $this->validateRuleDataForOpTypeP2C($data, $websitesIds))) {
                    return $response;
                }
                /** Preserve left_id from condition */
                $data['left_id'] = $this->getLeftId($data);
                break;
            case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_C2P:
                if (is_string($response = $this->validateRuleDataForOpTypeC2P($data, $websitesIds))) {
                    return $response;
                }
                /** Preserve left_id from condition */
                $data['left_id'] = $this->getLeftId($data);
                break;
            case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_C2C:
                if (is_string($response = $this->validateRuleDataForOpTypeC2C($data, $websitesIds))) {
                    return $response;
                }
                /** Preserve left_id from condition */
                $data['left_id'] = $this->getLeftId($data);
                break;
            case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_C2S:
                if (is_string($response = $this->validateRuleDataForOpTypeC2S($data, $websitesIds))) {
                    return $response;
                }
                /** Preserve left_id from condition */
                $data['left_id'] = $this->getLeftId($data);
                break;
            case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_P2S:
                if (is_string($response = $this->validateRuleDataForOpTypeP2S($data, $websitesIds))) {
                    return $response;
                }
                /** Preserve left_id from condition */
                $data['left_id'] = $this->getLeftId($data);
                break;
            default:
                break;
        }

        unset($data['website_id']);
        $packageTypes = $data['package_type'];
        // Back-end can set only one package type per rule so string will arrive
        if (!is_array($packageTypes)) {
            $packageTypes = array($packageTypes);
        }
        // Import files can contain multiple package types per line separated by comma
        if (count($packageTypes) > 1) {
            foreach ($packageTypes as $packageType) {
                $clone = clone($model);
                $data['package_type'] = $packageType;
                $clone->loadPost($data);
                $models[] = $clone;
            }
            unset($clone);
        } else {
            $data['package_type'] = array_shift($packageTypes);
            $model->loadPost($data);
            $models[] = $model;
        }

        return $models;
    }

    /**
     * @param $message
     * @param bool $debugIndexer
     */
    public function logIndexer($message, $debugIndexer = false)
    {
        if ($debugIndexer || $this->debugIndexer) {
            Mage::log($message, Zend_Log::DEBUG, $this->logFileName."_".(date("Y-m-d").".log"), false);
        }
    }

    /**
     * @param $data
     * @param $websitesIds
     * @return bool|string
     */
    protected function validateRuleDataForOpTypeP2P($data, $websitesIds)
    {
        if (is_string($response = $this->validateObligatedOperationForOpTypesC2PAndP2P($data['operation']))) {
            return $response;
        }
        /**
         * @var Mage_Catalog_Model_Product $firstProduct
         * @var Mage_Catalog_Model_Product $secondProduct
         */
        $this->validate2ProductsBelongToSameWebsites($data['left_id'], $data['right_id']);
        $this->validateProductBelongsToWebsite($websitesIds, $data['left_id']);
        $this->validateProductBelongsToWebsite($websitesIds, $data['right_id']);
        //todo - clarify if this is still needed
//                if (is_string($response = $this->validateSecondProductOnOpTypeP2PAndC2P($data['operation'], $secondProduct))) {
//                    return $response;
//                }
        return true;
    }

    /**
     * @param $operation
     * @return bool|string
     */
    protected function validateObligatedOperationForOpTypesC2PAndP2P($operation)
    {
        if ($operation == Dyna_ProductMatchRule_Model_Rule::OP_OBLIGATED) {
            return "Invalid operation: A product can't be set as mandatory.";
        }
        return true;
    }

    /**
     * @param $firstProduct
     * @param $secondProduct
     */
    protected function validate2ProductsBelongToSameWebsites($firstProduct, $secondProduct)
    {
        $productsBelongToSameWebsite = false;
        $products = [$firstProduct, $secondProduct];
        foreach ($this->websiteProducts as $websiteProducts) {
            if (count(array_intersect($products, $websiteProducts)) == 2) {
                $productsBelongToSameWebsite = true;
                break;
            }
        }

        if (!$productsBelongToSameWebsite) {
            $this->handleWarning('The products must belong at least to one common channel.');
        }
    }

    /**
     * Handle a warning display depending if the rule is imported from cli or if it is added manually
     */
    protected function handleWarning($message)
    {
        if ($this->_ruleFromImporter) {
            $this->_importerLog($message);
        } else {
            Mage::getSingleton('adminhtml/session')->addWarning($message);
        }
    }

    /**
     * Write the errors/warnings to the a log for an imported rule
     * @param $msg
     */
    protected function _importerLog($msg)
    {
        $this->importHelper->logMsg($msg, null, $this->importHelper->getImportLogFile());
    }

    /**
     * @param $websitesIds
     * @param $productId
     */
    protected function validateProductBelongsToWebsite($websitesIds, $productId)
    {
        foreach ($websitesIds as $websiteId) {
            if (!in_array($productId, $this->websiteProducts[$websiteId])) {
                $this->handleWarning(sprintf('The channel %s must include this product id: %s', $websiteId, $productId));
            }
        }
    }

    /**
     * Get the website name by Id
     *
     * @param $websiteId
     * @return string
     */
    public function getWebsiteById($websiteId)
    {
        if (!isset($this->_websites[$websiteId])) {
            $this->_websites[$websiteId] = Mage::app()->getWebsite($websiteId)->getName();
        }

        return $this->_websites[$websiteId];
    }

    /**
     * Return left id from conditions ready to be saved on product match rule model
     * @param $data
     * @return int|null
     */
    protected function getLeftId($data)
    {
        $left_id = null;
        if (!empty($conditions = $data['conditions'])) {
            foreach ($conditions as $currentCondition) {
                switch ($currentCondition['type']) {
                    case "catalogrule/rule_condition_product" :
                        if (!empty($currentCondition['attribute']) && $currentCondition['attribute'] == 'sku') {
                            $left_id = Mage::getModel('catalog/product')
                                ->getResource()
                                ->getIdBySku($currentCondition['value']);
                        } elseif (!empty($currentCondition['attribute']) && $currentCondition['attribute'] == 'category_ids') {
                            $left_id = (int)($currentCondition['value']);
                        }
                        break;
                    default :
                        break;
                }
            }
        }

        return $left_id;
    }

    /**
     * @param $data
     * @param $websitesIds
     * @return bool|string
     */
    protected function validateRuleDataForOpTypeP2C($data, $websitesIds)
    {
        if (is_string($response = $this->validateDefaultedOperationForOpTypesC2CAndP2C($data['operation']))) {
            return $response;
        }
        $this->validateProductBelongsToWebsite($websitesIds, $data['left_id']);
        $this->validateCategoryProductsBelongToWebsite($data, 'right_id', $websitesIds);

        return true;
    }

    /**
     * @param $operation
     * @return bool|string
     */
    protected function validateDefaultedOperationForOpTypesC2CAndP2C($operation)
    {
        if ($operation == Dyna_ProductMatchRule_Model_Rule::OP_DEFAULTED) {
            return "Invalid operation: A category can't be set as defaulted.";
        }
        return true;
    }

    /**
     * @param $data
     * @param $dataKey
     * @param $websitesIds
     */
    protected function validateCategoryProductsBelongToWebsite($data, $dataKey, $websitesIds)
    {
        foreach ($websitesIds as $websiteId) {
            $categoryId = $data[$dataKey];
            if ($this->websiteCategoryProducts[$websiteId] && empty($this->websiteCategoryProducts[$websiteId][$categoryId])) {
                $this->handleWarning(sprintf('Category id %s does not have products.', $categoryId));
                continue;
            }
        }
    }

    /**
     * Get a list of product ids and skus that are associated with this category: $product[$productId]=$productSku;
     *
     * @return array
     * @param category int
     */
    public function getProductIdsAndSkus($categoryId, $isAnchor = false)
    {
        $result = [];
        $query = "SELECT
                    t1.`category_id`,
                    t2.`entity_id`,
                    t2.`sku`
                  FROM catalog_category_product_index t1
                  RIGHT JOIN catalog_product_entity t2
                  ON t1.`product_id` = t2.`entity_id`
                  WHERE t1.`category_id`='{$categoryId}'";
        if (!$isAnchor) {
            $query .= " AND t1.is_parent = 1";
        }

        $allRows = $this->getConnection()->fetchAll($query);
        foreach ($allRows as $row) {
            $result[$row['entity_id']] = $row['sku'];
        }

        return $result;
    }

    /**
     * @param $data
     * @param $websitesIds
     * @return bool|string
     */
    protected function validateRuleDataForOpTypeC2P($data, $websitesIds)
    {
        $this->validateProductBelongsToWebsite($websitesIds, $data['right_id']);
        //todo - clarify if this is still needed
//            if (is_string($response = $this->validateSecondProductOnOpTypeP2PAndC2P($data['operation'], $product))) {
//                return $response;
//            }
        if (is_string($response = $this->validateObligatedOperationForOpTypesC2PAndP2P($data['operation']))) {
            return $response;
        }
        $this->validateCategoryProductsBelongToWebsite($data, 'left_id', $websitesIds);

        return true;
    }

    /**
     * @param $data
     * @param $websitesIds
     * @return bool|string
     */
    protected function validateRuleDataForOpTypeC2C($data, $websitesIds)
    {
        if (is_string($response = $this->validateDefaultedOperationForOpTypesC2CAndP2C($data['operation']))) {
            return $response;
        }
        $this->validateCategoryProductsBelongToWebsite($data, 'left_id', $websitesIds);
        $this->validateCategoryProductsBelongToWebsite($data, 'right_id', $websitesIds);

        return true;
    }

    /**
     * @param $data
     * @param $websitesIds
     * @return bool|string
     */
    protected function validateRuleDataForOpTypeC2S($data, $websitesIds)
    {
        $this->validateCategoryProductsBelongToWebsite($data, 'left_id', $websitesIds);
        return true;
    }

    /**
     * @param $data
     * @param $websitesIds
     * @return bool|string
     */
    protected function validateRuleDataForOpTypeP2S($data, $websitesIds)
    {
        $this->validateProductBelongsToWebsite($websitesIds, $data['left_id']);
        return true;
    }

    /**
     * Fetches all simple products as an associative array where the
     * key is the product id and the value is the product name.
     * @return array
     */
    public function getProductsForDropdown()
    {
        $key = md5(__CLASS__ . 'products_for_dropdown');
        $options = unserialize($this->getCache()->load($key));
        if (empty($options)) {
            $products = Mage::helper('omnius_catalog')->getProductCollection();
            $options = array(-1 => 'Please select a product..');
            foreach ($products as $productId => $product) {
                $options[] = array(
                    'label' => $product["type_id"] == "configurable" ? sprintf("%s (configurable)", $product['name']) : $product["name"],
                    'value' => $productId
                );
            }
            unset($products);

            $this->getCache()->save(
                serialize($options),
                $key,
                [Dyna_Cache_Model_Cache::CACHE_TAG],
                $this->getCache()->getTtl()
            );
        }

        return $options;
    }

    /**
     * @return Dyna_Cache_Model_Cache
     */
    protected function getCache()
    {
        if (!$this->_cache) {
            $this->_cache = Mage::getSingleton('dyna_cache/cache');
        }
        return $this->_cache;
    }

    /**
     * Fetches all simple products as an associative array where the
     * key is the product id and the value is the product sku.
     * @param bool $forJs
     * @return array
     */
    public function getProductSkusForDropdown($forJs = false)
    {
        $key = md5(__CLASS__ . 'products_for_dropdown_sku' . (int)$forJs);
        $options = unserialize($this->getCache()->load($key));

        if (empty($options)) {
            $products = Mage::helper('omnius_catalog')->getProductCollection();
            $options = [];
            $counter = 0;
            foreach ($products as $productId => $product) {
                $index = $forJs ? $counter : $productId;
                $options[$index] = array(
                    'value' => $product['sku'],
                    'data' => $productId
                );
                ++$counter;
            }
            unset($products);

            $this->getCache()
                ->save(
                    serialize($options),
                    $key,
                    [Dyna_Cache_Model_Cache::CACHE_TAG],
                    $this->getCache()->getTtl()
                );
        }

        return $options;
    }

    /**
     * Fetches all the categories as an associative array where the
     * key is the category id and the value is the category name.
     * @return array
     */
    public function getCategoriesForDropdown()
    {
        /** @var Mage_Catalog_Model_Resource_Category_Collection $categoriesCollection */
        $categoriesCollection = Mage::getModel('catalog/category')
            ->getCollection()
            ->addFieldToFilter('entity_id', array('nin' => [1, 2]))
            ->addAttributeToSort('name', Zend_Db_Select::SQL_ASC);

        $selectableCollection = array(-1 => 'Please select a category...');
        foreach ($categoriesCollection as $category) {
            $selectableCollection[$category->getId()] = $category->getPathAsString();
        }

        return $selectableCollection;
    }

    /**
     * @param $websiteId
     * @param $id
     * @param $catalogResource
     * @param $storeId
     * @param $rawAttributesValues
     * @param $attribute
     * @param $deviceCollection
     * @param $subsCollection
     * @param string $packageType
     * @return array
     */
    public function getRulesForId($websiteId, $id, $catalogResource, $storeId, &$rawAttributesValues, $attribute, $deviceCollection, $subsCollection, $packageType = "")
    {
        $rulesSource = $this->getRuleById($id, $websiteId, $packageType);

        /**
         * When choosing a device or subscription, normally the application would hide the rest of the devices or subscriptions
         * if there is no allowed rule between them. However, for devices and subscriptions, we must allow users to switch
         * between the allowed ones without hiding the other options.
         */
        $valueId = $catalogResource->getAttributeRawValue($id, Dyna_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR, $storeId);
        if (isset($rawAttributesValues[$valueId])) {
            $productType = $rawAttributesValues[$valueId];
        } else {
            $productType = json_decode(Mage::helper('omnius_catalog')->getAttributeAdminLabel($attribute, $valueId), true);
            $rawAttributesValues[$valueId] = $productType;
        }

        $similarProductIds = array();

        //if device, also retrieve all other devices to simulate that subscriptions are allowed between themselves
        if (count(array_intersect(Dyna_Catalog_Model_Type::$devices, $productType))) {
            $similarProductIds = $deviceCollection;
        }

        //if subscription, also retrieve all other subscriptions to simulate that subscriptions are allowed between themselves
        if (count(array_intersect(Dyna_Catalog_Model_Type::$subscriptions, $productType))) {
            $similarProductIds = array_merge($similarProductIds, $subsCollection);
        }

        $targetProductIds = [];
        $sourceProductIds = [];
        foreach ($rulesSource as $row) {
            $targetProductIds[$row['target_product_id']] = $row['target_product_id'];
            $sourceProductIds[$row['source_product_id']] = $row['source_product_id'];
        }

        $available = array_unique(
            array_merge(
                $similarProductIds,
                $targetProductIds,
                $sourceProductIds
            )
        );

        return $available;
    }

    /**
     * @param $id
     * @param $websiteId
     * @param $packageType
     * @return mixed
     */
    protected function getRuleById($id, $websiteId = null, $packageType = "")
    {
        $websiteId = $websiteId ?: Mage::app()->getStore()->getWebsiteId();
        $adapter = Mage::getSingleton('core/resource')->getConnection('core_read');
        $key = sprintf('%s_product_mix_match_white_rules_%s_%s', $id, $websiteId, $packageType);
        if ($result = $this->getCache()->load($key)) {
            return unserialize($result);
        } else {
            $collection = Mage::getResourceModel('productmatchrule/matchRule_collection')
                ->addFieldToSelect(array('source_product_id', 'target_product_id'))
                ->addFieldToFilter('website_id', $websiteId)
                ->addFieldToFilter(
                    array('source_product_id', 'target_product_id'),
                    array(
                        array('eq' => $id),
                        array('eq' => $id)
                    )
                );

            // Loading package type
            /** @var Dyna_Package_Model_PackageType $packageTypeModel */
            $packageTypeModel = Mage::getModel("dyna_package/packageType");
            $packageTypeModel->loadByCode($packageType);

            if ($packageTypeModel->getId()) {
                $collection->addFieldToFilter("package_type", array('eq' => $packageTypeModel->getId()));
            }

            $result = $adapter->fetchAll($collection->getSelectSql(true));

            $this->getCache()->save(serialize($result), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());

            return $result;
        }
    }

    /**
     * @param $operation
     * @param $product
     * @return bool|string
     */
    protected function validateSecondProductOnOpTypeP2PAndC2P($operation, $product)
    {
        // Only allow addons as the defaulted product
        if ($operation == Dyna_ProductMatchRule_Model_Rule::OP_DEFAULTED && !$product->is(Dyna_Catalog_Model_Type::SUBTYPE_ADDON)
        ) {
            return "The target product must be an Addon";
        }

        return true;
    }

    /**
     * Get removal not allowed product ids in legacy Omnius system by searching through removal not allowed index
     * @param $selectedProductsIds
     * @param $processContextId
     * @return array
     * @internal param $selectedProducts
     */
    public function getOmniusNotRemovableIds($selectedProductsIds, $processContextId)
    {
        $sourceCollectionId = Mage::helper('dyna_configurator/cart')->getCurrentSourceCollection();
        /** @var Dyna_ProductMatchRule_Model_Resource_RemovalNotAllowed_Collection $collection */
        $collection = Mage::getModel('dyna_productmatchrule/removalNotAllowed')
            ->getCollection();
        $collection
            ->join("productMatchRuleProcessContext", 'main_table.rule_id=productMatchRuleProcessContext.rule_id', null)
            // Source product ids must be in cart
            ->addFieldToFilter('main_table.source_product_id', array('in' => $selectedProductsIds))
            // Target product ids must also be in cart (is no need for removal not allowed on a product that is not present in cart)
            ->addFieldToFilter('main_table.target_product_id', array('in' => $selectedProductsIds))
            // Restrict it to the current package process context
            ->addFieldToFilter('productMatchRuleProcessContext.process_context_id', array('eq' => $processContextId))
            ->addFieldToFilter('main_table.source_collection', array('eq' => $sourceCollectionId))
            ->addFieldToSelect("target_product_id")
        ;

        return $collection->load()->getColumnValues('target_product_id');
    }

    /**
     * Get removal not allowed product ids by evaluating service to product, service to category and service to service rules
     * @param $selectedProductsIds
     * @param $processContextId
     * @param $packageType
     * @return array
     * @internal param $selectedProducts
     */
    public function getServiceNotRemovableIds($selectedProductsIds, $processContextId, $packageType)
    {
        $mandatory = array();
        $websiteId = (int)Mage::app()->getWebsite()->getId();

        /** @var Dyna_Configurator_Model_Expression_Cart $cartModel */
        $cartModel = Mage::getModel('dyna_configurator/expression_cart');
        $cartModel->appendCurrentProducts($selectedProductsIds);

        $inputParams = [
            'customer' => Mage::getModel('dyna_configurator/expression_customer'),
            'cart' => $cartModel,
            'availability' => Mage::getModel('dyna_configurator/expression_availability'),
            'agent' => Mage::getModel('dyna_configurator/expression_agent'),
            'installBase' => Mage::getModel('dyna_configurator/expression_installBase')
        ];

        /** @var Dyna_Configurator_Model_Expression_Catalog $catalogObject */
        $catalogObject = Mage::getModel('dyna_configurator/expression_catalog', $packageType);

        /** @var $dynaCoreHelper Dyna_Core_Helper_Data */
        $dynaCoreHelper = Mage::helper('dyna_core');

        /** @var Dyna_ProductMatchRule_Model_Resource_Rule_Collection $ruleCollection */
        $ruleCollection = Mage::getModel('dyna_productmatchrule/rule')->getCollection();
        $ruleCollection
            // Make sure the rules are filtered by the requested process context
            ->join(array('productMatchRuleProcessContext' => 'dyna_productmatchrule/productMatchRuleProcessContext'), 'main_table.product_match_rule_id=productMatchRuleProcessContext.rule_id', null)
            // Make sure rules are of type service (legacy Omnius rules should be already evaluated at this point)
            ->addFieldToFilter('source_type', array('eq' => 1))
            ->addFieldToFilter('operation_type', array(
                    'in' => array(
                        Dyna_ProductMatchRule_Model_Rule::OP_TYPE_S2P,
                        Dyna_ProductMatchRule_Model_Rule::OP_TYPE_S2C,
                        Dyna_ProductMatchRule_Model_Rule::OP_TYPE_S2S
                    )
                )
            )
            // Make sure rules have a service expression that can be evaluated
            ->addFieldToFilter('operation', array('eq' => Dyna_ProductMatchRule_Model_Rule::OP_REMOVAL_NOTALLOWED))
            ->addFieldToFilter('service_source_expression', array('notnull' => true))
            ->addFieldToFilter('productMatchRuleProcessContext.process_context_id', array('eq' => $processContextId))
            ->distinct(true)
        ;

        if ($packageType) {
            /** @var Dyna_Package_Model_PackageType $packageTypeModel */
            $packageTypeModel = Mage::getSingleton("dyna_package/packageType")
                ->loadByCode($packageType);
            if ($packageTypeModel->getId()) {
                $ruleCollection->addFieldToFilter("main_table.package_type", array('eq' => (int)$packageTypeModel->getId()));
            } else {
                Mage::throwException("Got package type value but cannot find resource in Allowed Service Rules call.");
            }
        } else {
            Mage::throwException("Empty package type in Allowed Service Rules call");
        }

        $ruleCollection->getSelect()
            ->join(array('rule_website' => 'product_match_rule_website'), 'rule_website.rule_id = main_table.product_match_rule_id', null)
            ->where('rule_website.website_id = ?', $websiteId)
            ->order('main_table.priority ASC');

        $errors = array();

        /** @var Dyna_ProductMatchRule_Model_Rule $rule */
        foreach ($ruleCollection as $rule) {
            try {
                if ($dynaCoreHelper->evaluateExpressionLanguage($rule->getServiceSourceExpression(), $inputParams)) {
                    if ($rule->getOperationType() == $rule::OP_TYPE_S2S) {
                        $targetedProductsExpression = trim($rule->getServiceTargetExpression());
                        $products = $dynaCoreHelper->evaluateExpressionLanguage($targetedProductsExpression, array(
                            // Let the evaluator know what kind of operation is evaluating
                            'catalog' => $catalogObject,
                            'cart' => $cartModel,
                        ));
						$products = array_values($products);
                    } else {
                        $products = $this->getIndexedRemovalNotAllowed($rule->getId(), $websiteId, $selectedProductsIds);
                    }
                    // Intersect with selected product ids as service to service might return unselected product ids
                    $mandatory = array_merge($mandatory, array_intersect($products, $selectedProductsIds));
                }
            } catch (Exception $e) {
                // Error
                $errors[] = '[Rule ID = ' . $rule->getId() . '] ' . $e->getMessage();
            }
        }

        if (count($errors) > 0) {
            $rule->log('[ERROR] Found ' . count($errors) . ' errors within the rules');
        }

        $messages = '';
        foreach ($errors as $key => $value) {
            $messages = sprintf("%s [RULE] \n Expression: %s \n Message: %s \n", $messages, $key, $value);
        }

        if (strlen($messages) > 0) {
            $rule->log($messages);
        }

        return $mandatory;
    }

    /**
     * Load all indexed target products for a S2C and S2P specific rule
     * @param int $ruleId
     * @param int $websiteId
     * @param int $selectedProducts
     * @return int[]
     */
    public function getIndexedRemovalNotAllowed($ruleId, $websiteId, $selectedProducts)
    {
        $cacheKey = 'removal_not_allowed_index_';
        $cacheKey .= md5(serialize([$ruleId, $websiteId, $selectedProducts]));

        $result = $this->getCache()->load($cacheKey);

        if ($result === false) {
            if ($ruleId && $selectedProducts) {
                /** @var Dyna_ProductMatchRule_Model_Resource_RemovalNotAllowed_Collection $notAllowedCollection */
                $notAllowedCollection = Mage::getModel('dyna_productmatchrule/removalNotAllowed')
                    ->getCollection()
                    ->addFieldToSelect('target_product_id')
                    ->addFieldToFilter('website_id', array('eq' => (int)$websiteId))
                    ->addFieldToFilter('rule_id', array('eq' => (int)$ruleId))
                    // Source product needs to be null (these null source indexed values are incoming from service rules as there is no left product id)
                    ->addFieldToFilter('source_product_id', array('null' => true))
                    // Target product id must be present in cart, removal not allowed is for cart products, other products are irrelevant
                    ->addFieldToFilter('target_product_id', array('in' => $selectedProducts));

                $result = $notAllowedCollection->load()->getColumnValues('target_product_id');

                $this->getCache()->save(serialize($result), $cacheKey);
            }

        } elseif($result) {
            $result = unserialize($result);
        }

        return $result ?? array();

    }

    /**
     * Method used to check enable/disable debug mode for static or service rules
     * It will log in var/log/rules_debug.log
     * @return bool
     */
    public function isDebugMode()
    {
        return $this->debugServiceRules || $this->debugStaticRules;
    }

    /**
     * Method used to check enable/disable debug log for service rules
     * It will log in var/log/rules_debug.log
     * @return bool
     */
    public function getDebugServiceRules()
    {
        return $this->debugServiceRules;
    }

    /**
     * Method used to check enable/disable debug log for static rules
     * It will log in var/log/rules_debug.log
     * @return bool
     */
    public function getDebugStaticRules()
    {
        return $this->debugStaticRules;
    }

    /**
     * Method used to verify if service expression parts debugging is enabled
     * @return bool
     */
    public function canDebugExpressionParts()
    {
        if (Mage::registry('debug_rule')) {
            return true;
        }

        return $this->debugRulesExpressionParts;
    }


    /**
     * @return array|bool
     */
    public function getServiceCompRulesObjects()
    {
        if ($this->serviceCompRulesObjects === false) {
            $this->serviceCompRulesObjects = array(
                'customer' => Mage::getModel('dyna_configurator/expression_customer'),
                'availability' => Mage::getModel('dyna_configurator/expression_availability'),
                'agent' => Mage::getModel('dyna_configurator/expression_agent'),
                'installBase' => Mage::getModel('dyna_configurator/expression_installBase'),
                'activeInstallBase' => Mage::getModel('dyna_configurator/expression_activeInstallBase'),
            );
        }

        return $this->serviceCompRulesObjects;
    }

    /**
     * Setter method for websiteProducts which contains a list of products from a specific website
     * @param $value
     * @return $this
     */
    public function setWebsiteProducts($value)
    {
        $this->websiteProducts = $value;

        return $this;
    }

    /**
     * Setter method for websiteCategoryProducts which contains a list of categories and their associated products from a specific website
     * @param $value
     * @return $this
     */
    public function setWebsiteCategoryProducts($value)
    {
        $this->websiteCategoryProducts = $value;

        return $this;
    }

    /**
     * Write log message to rules_debug.log file
     * @param $message
     */
    public function logRulesDebug($message, $level = null)
    {
        Mage::log($message, $level, 'rules_debug.log', true);
    }
}
