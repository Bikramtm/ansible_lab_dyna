<?php

/**
 * Class Dyna_ProductMatchRule_Model_Rule
 */
class Dyna_ProductMatchRule_Model_Defaulted extends Omnius_ProductMatchRule_Model_Defaulted
{
    // flag for debugging purpose
    protected $debug = false;

    /**
     * On construct inherit the debug mode flag from  Dyna_ProductMatchRule_Helper_Data::$debugRules
     */
    protected function _construct()
    {
        $this->debug = Mage::helper('dyna_productmatchrule')->isDebugMode();

        parent::_construct();
    }

    /**
     * @param $productId
     * @param $websiteId
     * @param Dyna_Package_Model_Package $packageModel
     * @return array Gets possible products that can be combined with a certain product id
     *
     * @throws Dyna_ProductMatchRule_Model_Exception
     */
    public function getDefaultedProductsIds($productId, $websiteId, $packageModel, $sourceCollectionId = Dyna_ProductMatchRule_Helper_Data::SOURCE_COLLECTION_CQ)
    {
        /** @var Dyna_Catalog_Helper_ProcessContext $processContextHelper */
        $processContextHelper = Mage::helper("dyna_catalog/processContext");
        $processContextId = $processContextHelper->getProcessContextId($packageModel);

        $cacheKey = sprintf('defaulted_products_cache_%s_%s_%s_%s', $productId, $websiteId, $processContextId, $sourceCollectionId);
        if ($result = $this->getCache()->load($cacheKey)) {
            return unserialize($result);
        } else {
            // Trying to load package type model
            /** @var Dyna_Package_Model_PackageType $packageTypeModel */
            $packageTypeModel = Mage::getSingleton('dyna_package/packageType')
                ->loadByCode($packageModel->getType());

            if (!$packageTypeModel->getId()) {
                /** @var Dyna_ProductMatchRule_Model_Exception $exception */
                $exception = Mage::getModel('dyna_productmatchrule/exception', "Not proper package type definition found for current cart package. Were imports ran?");
                throw $exception;
            }

            // todo: get process context from indexer table and remove join
            // Building the query string for defaulted products
            $sqlString = "SELECT pIndexer.`target_product_id`, pIndexer.`obligated` FROM product_match_defaulted_index pIndexer";
            $sqlString .= " INNER JOIN product_match_rule_process_context pContext ON pIndexer.`rule_id` = pContext.`rule_id`";
            $sqlString .= " WHERE pIndexer.`source_product_id`=:productId AND pIndexer.`source_collection`=:sourceCollection AND pIndexer.`website_id`=:websiteId AND pContext.process_context_id=:processContext";

            // Get all defaulted products from db
            $result = Mage::getSingleton('core/resource')->getConnection('core_read')->fetchAll($sqlString, array(
                'productId' => $productId,
                'websiteId' => $websiteId,
                'sourceCollection' => $sourceCollectionId,
                'processContext' => $processContextId,

            ), \PDO::FETCH_ASSOC);
        }

        $this->getCache()->save(
            serialize($result),
            $cacheKey,
            [Dyna_Cache_Model_Cache::CACHE_TAG],
            $this->getCache()->getTtl()
        );

        return $result;
    }

    /**
     * Get all service expression applicable rules
     * @param array $productIds Contains the currently added products
     * @param int $websiteId
     * @param Dyna_Package_Model_Package $packageModel
     * @param $removal
     * @return array
     * @throws Dyna_ProductMatchRule_Model_Exception
     */
    public function loadDefaultedServiceRules($productIds, $websiteId, $packageModel, $removal = false)
    {
        static $cache = null;

        /** @var Dyna_Catalog_Helper_ProcessContext $processContextHelper */
        $processContextHelper = Mage::helper("dyna_catalog/processContext");

        $packageType = $packageModel->getType();
        /** @var Dyna_Package_Model_PackageType $packageTypeModel */
        $packageTypeModel = Mage::getSingleton("dyna_package/packageType")
            ->loadByCode($packageType);

        $packageTypeId = $packageTypeModel->getId();
        $processContextId = $processContextHelper->getProcessContextId();

        $cacheKey = null;
        $cacheKey = "CACHED_DEFAULTED_SERVICE_RULES_" . md5(serialize(array($productIds, $websiteId, (int)$packageTypeId, $removal, $processContextId)));
        if (isset($cache[$cacheKey])) {
            return $cache[$cacheKey];
        }

        if (!$finalCollectionSerialized = $this->getCache()->load($cacheKey)) {
            /** @var Omnius_ProductMatchRule_Model_Resource_Rule_Collection $ruleCollection */
            $ruleCollection = Mage::getModel('dyna_productmatchrule/rule')->getCollection()
                ->addFieldToFilter('source_type', array('eq' => 1))
                // Filter only relevant rules -- for better performance
                ->addFieldToFilter('operation', array('in' => array(Dyna_ProductMatchRule_Model_Rule::OP_DEFAULTED, Dyna_ProductMatchRule_Model_Rule::OP_DEFAULTED_OBLIGATED)))
                ->addFieldToFilter('operation_type', array('eq' => Dyna_ProductMatchRule_Model_Rule::OP_TYPE_S2P))
                ->addFieldToFilter('service_source_expression', array('notnull' => true));

            if ($packageType) {
                if ($packageTypeId) {
                    $ruleCollection->addFieldToFilter('package_type', array('eq' => (int)$packageTypeId));
                } else {
                    /** @var Dyna_ProductMatchRule_Model_Exception $exception */
                    $exception = Mage::getModel('dyna_productmatchrule/exception', "Got package type value but cannot find resource. Were imports ran?");
                    throw $exception;
                }
            } else {
                /** @var Dyna_ProductMatchRule_Model_Exception $exception */
                $exception = Mage::getModel('dyna_productmatchrule/exception', "Not type set on active package");
                throw $exception;
            }

            $ruleCollection->getSelect()
                ->join(array('rule_website' => 'product_match_rule_website'), 'rule_website.rule_id = main_table.product_match_rule_id')
                ->where('rule_website.website_id = ?', $websiteId)
                ->order('main_table.priority ASC');


            if ($processContextHelper->getProcessContextId()) {
                $ruleCollection->getSelect()
                    ->join(array('product_match_rule_process_context' => 'product_match_rule_process_context'), 'product_match_rule_process_context.rule_id = main_table.product_match_rule_id')
                    ->where('product_match_rule_process_context.process_context_id = ?', (int)$processContextHelper->getProcessContextId());
            }

            $ruleCollection->load();

            $staticCollection = new Varien_Data_Collection();
            foreach ($ruleCollection->getItems() as $item) {
                $staticCollection->addItem($item);
            }
            $cache[$cacheKey] = $staticCollection;
            $this->getCache()->save(serialize($staticCollection), $cacheKey, array($this->getCache()::PRODUCT_TAG), $this->getCache()->getTtl());
        } else {
            $ruleCollection = unserialize($finalCollectionSerialized);
        }

        /** @var Dyna_Configurator_Model_Expression_Cart $cartModel */
        $cartModel = Mage::getModel('dyna_configurator/expression_cart');
        if (!$removal) {
            $cartModel->appendCurrentProducts($productIds);
        } else {
            $cartModel->updateCurrentPackages($packageModel->getQuote());
        }

        $inputParams = [
            'customer' => Mage::getModel('dyna_configurator/expression_customer'),
            'cart' => $cartModel,
            'availability' => Mage::getModel('dyna_configurator/expression_availability'),
            'agent' => Mage::getModel('dyna_configurator/expression_agent'),
            'installBase' => Mage::getModel('dyna_configurator/expression_installBase'),
        ];

        /**
         * @var $dynaCoreHelper Dyna_Core_Helper_Data
         */
        $dynaCoreHelper = Mage::helper('dyna_core');

        $defaulted = [];
        $errors = [];
        foreach ($ruleCollection as $rule) {
            $ssExpression = $rule->getServiceSourceExpression();
            try {
                $passed = false;

                if ($dynaCoreHelper->evaluateExpressionLanguage($ssExpression, $inputParams)) {
                    $passed = true;
                }

                if ($passed) {
                    $productId = $rule->getRightId();
                    if ($this->debug) {
                        $this->log(sprintf('Service defaulted rule evaluated to true. Id: %s, Name: %s', $rule->getId(), $rule->getRuleTitle()));
                    }
                    if ($productId) {
                        // inject product in service rules evaluation
                        $cartModel->appendCurrentProducts([$productId]);
                        switch ($rule->getOperation()) {
                            case Dyna_ProductMatchRule_Model_Rule::OP_DEFAULTED:
                                $defaulted[$productId] = array(
                                    'target_product_id' => $productId,
                                    'obligated' => 0,
                                    'rule_id' => $rule->getId(),
                                    'is_service' => 1
                                );

                                if ($this->debug) {
                                    $this->log(sprintf('Service rule should defaulted product: %s', $productId));
                                }

                                break;
                            case Dyna_ProductMatchRule_Model_Rule::OP_DEFAULTED_OBLIGATED:
                                $defaulted[$productId] = array(
                                    'target_product_id' => $productId,
                                    'obligated' => 1,
                                    'rule_id' => $rule->getId(),
                                    'is_service' => 1
                                );

                                if ($this->debug) {
                                    $this->log(sprintf('Service rule should default product: %s', $productId));
                                }
                                break;
                            default:
                                continue;
                        }
                    }
                }
            } catch (Exception $e) {
                // Error
                $errors[$ssExpression] = '[Rule ID = ' . $rule->getId() . '] ' . $e->getMessage();
            }
        }

        if (count($errors) > 0) {
            $this->log('[ERROR] Found ' . count($errors) . ' errors within the rules', Zend_Log::NOTICE);
        }

        $messages = '';
        foreach ($errors as $key => $value) {
            $messages = sprintf("%s [RULE] \n Expression: %s \n Message: %s \n", $messages, $key, $value);
        }

        if (strlen($messages) > 0) {
            $this->log($messages, Zend_Log::NOTICE);
        }

        if ($this->debug) {
            $this->log(sprintf('Service rules defaulted products: %s', var_export($defaulted, true)));
        }

        return $defaulted;
    }

    /**
     * @param $item
     * @param null $websiteId
     * @return array|bool|false|Mage_Core_Model_Abstract|mixed
     */
    public function getDefaultedProductsForItem($item, $websiteId = null)
    {
        if ($websiteId == null) {
            $websiteId = Mage::app()->getStore()->getWebsiteId();
        }
        $productId = $item->getProductId();

        // default source collection is current quote
        $sourceCollectionId = Dyna_ProductMatchRule_Helper_Data::SOURCE_COLLECTION_CQ;
        $isContract = $item->getData('is_contract');
        if ($isContract) {
            $sourceCollectionId = Dyna_ProductMatchRule_Helper_Data::SOURCE_COLLECTION_IB;
        }
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $packageModel = $quote->getCartPackage();

        return [$productId => $this->getDefaultedProductsIds($productId, $websiteId, $packageModel, $sourceCollectionId)];
    }

    /**
     * Log messages to custom file: var/log/ServiceExpressionRules.log
     * @param $message
     */
    public function log($message, $level = null)
    {
        Mage::log($message, $level, 'ServiceExpressionRules.log', true);
    }
}
