<?php

/**
 * Class Dyna_ProductMatchRule_Model_Importer
 */
class Dyna_ProductMatchRule_Model_Importer extends Omnius_Import_Model_Import_ImportAbstract
{
    const ALL_WEBSITES = "*";
    const ALL_PROCESS_CONTEXTS = "*";
    const CSV_DELIMITER = ";";

    protected $data = [];
    protected $opType;
    protected $operationsTypes = [];
    protected $operationsWithValues = [];
    protected $allowedOperationTypeCombinations = [];
    protected $operations = [];
    protected $line = [];
    protected $position = [];
    protected $websites = [];
    protected $processContexts = [];
    /** @var  array[websiteID[productID, productID]] $websiteProducts */
    protected $websiteProducts = [];
    /** @var  array[websiteID[$categoryID => [productID, productID]]] $websiteCategoryProducts */
    protected $websiteCategoryProducts = [];
    /** @var  array[path => categoryID] $categoryPaths */
    protected $categoryPaths = [];
    protected $sourceCollection = [];
    protected $sourceType = [];
    protected $ruleOrigin = [];
    /** @var Dyna_ProductMatchRule_Helper_Data $helper */
    protected $helper;
    /** @var Dyna_Catalog_Helper_ProcessContext $processContextHelper */
    protected $processContextHelper;
    protected $noImports = 0;

    // Used for building a package types mapped to package ids stack
    protected $packageTypes = [];

    protected $tableColumns = [];

    /** @var string $_logFileName */
    protected $_logFileName = "matchrules_import";
    /** @var string $_logFileExtension */
    protected $_logFileExtension = "log";

    /** @var Dyna_Import_Helper_Data $_helper */
    public $_helper;
    /** @var bool $_debug */
    protected $_debug = false;

    /**
     * Dyna_PriceRules_Model_ImportPriceRules constructor.
     */
    //to edit
    public function __construct()
    {
        parent::__construct();

        /** @var Omnius_Import_Helper_Data _helper */
        $this->_helper->setImportLogFile($this->_logFileName . '.' . $this->_logFileExtension);
    }

    /**
     * @param $path
     * @return bool
     */
    public function import($path)
    {
        /** @var Dyna_ProductMatchRule_Helper_Data $helper */
        $this->helper = Mage::helper('productmatchrule');
        $this->processContextHelper = Mage::helper("dyna_catalog/processContext");
        $this->helper->importHelper = $this->_helper;
        $file = fopen($path, 'r');
        $lc = 0;
        $success = false;

        $this->setDefaultOperationTypes();
        $this->operationsWithValues = $this->helper->getOperationsWithValues();
        $this->setDefaultOperations();
        $this->allowedOperationTypeCombinations = $this->productMatchRuleHelper->getAllowedOperationTypeCombinations();
        $this->setDefaultProcessContexts();
        $this->setDefaultSourceCollections();
        $this->setDefaultSourceType();
        $this->setDefaultRuleOrigin();
        $this->setAllDefaultWebsites();

        foreach ($this->websites as $websiteId => $websiteCode) {
            /** @var Dyna_Catalog_Model_Category $categoryModel */
            $categoryModel = Mage::getModel('catalog/category');
            $this->websiteProducts[$websiteId] = $categoryModel->getWebsitesProductsId($websiteId);

            /** @var Dyna_ProductMatchRule_Model_Rule $productMatchRuleModel */
            $productMatchRuleModel = Mage::getModel('productmatchrule/rule');
            $productCategories = $productMatchRuleModel->getAllCategoryProducts($websiteId, true);
            $this->websiteCategoryProducts[$websiteId] = $productCategories;
            unset($productCategories);
        }

        $this->helper->setWebsiteProducts($this->websiteProducts);
        $this->helper->setWebsiteCategoryProducts($this->websiteCategoryProducts);

        $this->position = [];
        /** @var Dyna_Import_Helper_Data $importHelper */
        $importHelper = $this->getImportHelper();

        /** @var Zend_Db_Adapter_Abstract $connection */
        $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
        $connection->exec('SET FOREIGN_KEY_CHECKS = 0;');

        while (($this->line = fgetcsv($file, null, self::CSV_DELIMITER)) !== false) {
            if ($lc == 0) {
                $this->line = $this->formatKey($this->line);
                $type = 'productMatchRules';
                //get header mapping
                $check = $importHelper->checkHeader(array_map('strtolower', $this->line), $type);
                //log messages
                $messages = $importHelper->getMessages();
                $attachments = [];
                foreach ($messages as $key => $msg) {
                    //write file log
                    $this->_logError($msg);
                    if ($key !== 'success' && substr($this->_pathToFile, 0, 9) != '/vagrant/') {
                        $attachments[] = ['key' => $key, 'text' => $msg];
                    }
                }
                if (count($attachments)) {
                    $msg = 'While processing file ' . $this->_pathToFile . ' we encountered the following:';
                    $importHelper->postToSlack($msg, $attachments);
                }
                //check if the headers are ok
                if (!$check) {
                    break;
                }
                $headers = $importHelper->getMapping($type);
                unset($type);
                //parse data
                foreach ($headers as $key => $value) {
                    if (in_array($key, $this->line)) {
                        $this->position[$value] = array_search($key, $this->line);
                    }
                }
            }

            $lc++;
            if ($lc == 1) {
                continue;
            }
            $this->opType = [];
            $this->data = [];
            $this->data['stack'] = $this->stack;
            $foundLeftIdOrSourceExpressionCollection = false;
            $foundRightIdOrTargetExpressionCollection = false;

            // get the rule title ; trim it
            if (isset($this->position['rule_title'])) {
                $this->data['rule_title'] = trim($this->line[$this->position['rule_title']]);
            }

            // determine the id of the operation type based on the string(PRODUCT-PRODUCT)
            if (isset($this->position['operation_type']) && !$this->setOperationType()) {
                $this->_logError('[ERROR] operation type cannot be determined for rule title: ' . $this->data['rule_title'] . '. Skipping row no' . $lc);
                continue;
            }

            if (isset($this->position['operation'])) {
                // determine the id of the operation based on the string (allowed, min)
                if (!$this->setOperation()) {
                    $this->_logError('[ERROR] operation cannot be determined for rule title: ' . $this->data['rule_title'] . '. Skipping row no ' . $lc);
                    continue;
                }

                // determine the the operation value for operations min, max, equal
                if (!$this->setOperationValue()) {
                    $this->_logError('[ERROR] Skipping row no ' . $lc);
                    continue;
                }
            }
            // determine the override_initial_selectable
            if (in_array($this->data['operation'], $this->helper->getOperationsWithOverrideInitialSelectable())) {
                $this->setOverrideInitialSelectable();
            }

            // determine the source_type; must be a string that can be associated with an id or a valid id; else error
            if (isset($this->position['source_type'])) {
                if (!$this->setSourceType()) {
                    $this->_logError('[ERROR] source type cannot be determined for rule title: ' . $this->data['rule_title'] . '. Skipping row no ' . $lc);
                    continue;
                }
            }

            // determine the target_type; must be a string that can be associated with an id or a valid id; else error
            if (isset($this->position['target_type'])) {
                if (!$this->setTargetType()) {
                    $this->_logError('[ERROR] target type cannot be determined for rule title: ' . $this->data['rule_title'] . '. Skipping row no ' . $lc);
                    continue;
                }
            }

            // determine the service_source_expression
            if (isset($this->position['service_source_expression'])) {
                $this->data['service_source_expression'] = trim($this->line[$this->position['service_source_expression']]);
                if ($this->data['service_source_expression']) {
                    //validate the service source expression
                    if (!$this->validateServiceSourceExpression($this->data['service_source_expression'])) {
                        $this->_logError('[ERROR] service_source_expression ' . $this->data['service_source_expression'] . ' is not valid ( rule tile = ' .
                            $this->data['rule_title'] . ') Skipping row no ' . $lc);
                        continue;
                    }
                    $foundLeftIdOrSourceExpressionCollection = true;
                }
                // the service source expression is mandatory if the source type is service OMNVFDE-408
                if (!$this->data['service_source_expression'] && isset($this->data['source_type']) && $this->data['source_type']) {
                    $this->_logError('[ERROR] service_source_expression cannot be determined and is mandatory when the source type is Service( rule tile = ' .
                        $this->data['rule_title'] . ') Skipping row no ' . $lc);
                    continue;
                }
            }

            // determine the service_target_expression
            if (isset($this->position['service_target_expression'])) {
                $this->data['service_target_expression'] = isset($this->line[$this->position['service_target_expression']]) ? trim($this->line[$this->position['service_target_expression']]) : '';
                if ($this->data['service_target_expression']) {
                    //validate the service source expression
                    if (!$this->validateServiceTargetExpression($this->data['service_target_expression'])) {
                        $this->_logError('[ERROR] service_target_expression ' . $this->data['service_target_expression'] . ' is not valid ( rule tile = ' .
                            $this->data['rule_title'] . ') Skipping row no ' . $lc);
                        continue;
                    }
                    $foundRightIdOrTargetExpressionCollection = true;
                }
                // the service source expression is mandatory if the source type is service OMNVFDE-408
                if (!$this->data['service_target_expression'] && isset($this->data['target_type']) && $this->data['target_type']) {
                    $this->_logError('[ERROR] service_target_expression cannot be determined and is mandatory when the source type is Service( rule tile = ' .
                        $this->data['rule_title'] . ') Skipping row no ' . $lc);
                    continue;
                }
            }

            // determine the the left id value for the given sku and op type
            if (isset($this->position['left_id']) && !empty($this->opType)) {
                if (!$foundLeftIdOrSourceExpressionCollection && $this->setLeftId()) {
                    $foundLeftIdOrSourceExpressionCollection = true;
                }
            }

            // validate if the operation_type has the required values
            if (!$this->validateRequiredProductIdOrServiceSourceExpression($foundLeftIdOrSourceExpressionCollection, 'source')) {
                $this->_logError('[ERROR] Skipping row no ' . $lc);
                continue;
            }

            // determine the the right id value for the given sku and op type
            if (isset($this->position['right_id']) && !empty($this->opType)) {
                if (!$foundRightIdOrTargetExpressionCollection && $this->setRightId()) {
                    $foundRightIdOrTargetExpressionCollection = true;
                }
            }

            if (!$this->validateRequiredProductIdOrServiceSourceExpression($foundRightIdOrTargetExpressionCollection, 'target')) {
                $this->_logError('[ERROR] Skipping row no ' . $lc);
                continue;
            }

            // determine the rule description
            if (isset($this->position['rule_description'])) {
                $this->data['rule_description'] = trim($this->line[$this->position['rule_description']]);
                if (!$this->data['rule_description']) {
                    unset($this->data['rule_description']);
                }
            }

            // determine the priority
            if (isset($this->position['priority'])) {
                if (!$this->setPriority()) {
                    $this->_logError('[ERROR] Skipping row no ' . $lc);
                    continue;
                }
            }

            // determine if is locked or not; this is not a required field; it will be locked only when the value is locked or 1, else it will be unlocked
            if (isset($this->position['locked'])) {
                $this->setIsLocked();
            }

            // determine the website ids (if is provided a code it must be mapped to a possible id; if is an id it must be a valid one, else -> error)
            if (isset($this->position['website_id'])) {
                if (!$this->setWebsiteIds()) {
                    $this->_logError('[ERROR] Skipping row no ' . $lc);
                    continue;
                }
            } else {
                foreach (Mage::app()->getWebsites() as $website) {
                    /**
                     * @var $website Mage_Core_Model_Website
                     */
                    $this->data['website_id'][] = $website->getId();
                }
            }

            // determine the rule origin, if is set we will use that value, else 1 = import
            if (isset($this->position['rule_origin'])) {
                $this->setRuleOrigin();
            } else {
                $this->_log('rule_origin was set to the default value for import 1', true);
                $this->data['rule_origin'] = 1;
            }


            // determine the process context. if is a string it must be a valid string that can be mapped to an id, if is an id it must be a valid one, else -> error)
            if (isset($this->position['process_context'])) {
                if (!$this->setProcessContextId()) {
                    $this->_logError('[ERROR] Skipping row no ' . $lc);
                    continue;
                }
            }

            // determine the effective date, if it's a string it must be a valid date string, else null is inserted
            if (isset($this->position['effective_date'])) {
                $this->setEffectiveDate();
            }

            // determine the expiration date, if it's a string it must be a valid date string, else null is inserted
            if (isset($this->position['expiration_date'])) {
                $this->setExpirationDate();
            }

            // determine to which package type this rule belongs
            if (isset($this->position['package_type'])) {
                $this->position['package_type_id'] = $this->position['package_type'];
            }
            if (isset($this->position['package_type_id'])) {
                try {
                    $this->setPackageType();
                } catch (Exception $e) {
                    $this->_logError('[ERROR]' . $e->getMessage());
                    $this->_logError('[ERROR] Trying to import a rule with an invalid package type (' . $this->position['package_type_id'] . '): ' . $this->data['rule_title'] . '. Skipping row no ' . $lc);
                    continue;
                }
            }

            // determine the source_collection; must be a string that can be associated with an id or a valid id; else error
            // the source collection is mandatory only if source type is product selection OMNVFDE-408
            if (isset($this->position['source_collection'])) {
                if ((!$this->setSourceCollection() || (isset($this->data['source_collection']) && $this->data['source_collection'] == 0) || !isset($this->data['left_id'])) && $this->data['source_type'] == 0) {
                    $this->_logError('[ERROR] source_collection and left_id are required for Product source type( rule tile = ' .
                        $this->data['rule_title'] . ') Skipping row no ' . $lc);
                    continue;
                }
            }

            // set the left_id as a condition
            if (isset($this->data['sourceSku'])) {
                $this->data['conditions'] = [
                    '1' => [
                        'type' => 'catalogrule/rule_condition_combine',
                        'aggregator' => 'all',
                        'value' => '1',
                        'new_child' => '',
                    ],
                    '1--2' => [
                        'type' => 'catalogrule/rule_condition_product',
                        'attribute' => 'sku',
                        'operator' => '==',
                        'value' => $this->data['sourceSku']
                    ]
                ];
            }
            if (isset($this->data['sourceCategoryId'])) {
                $this->data['conditions'] = [
                    '1' => [
                        'type' => 'catalogrule/rule_condition_combine',
                        'aggregator' => 'all',
                        'value' => '1',
                        'new_child' => '',
                    ],
                    '1--2' => [
                        'type' => 'catalogrule/rule_condition_product',
                        'attribute' => 'category_ids',
                        'operator' => '==',
                        'value' => $this->data['sourceCategoryId']
                    ]
                ];
            }

            // Parse POST data to Dyna_ProductMatchRule_Model_Rule model
            $response = $this->helper->setRuleData($this->data, true);

            if (!is_array($response)) {
                $this->_logError('[ERROR] ( rule tile = ' . $this->data['rule_title'] . ')' . $response);
                $this->_logError('[ERROR] Skipping row no ' . $lc);
                // Error occurred
                continue;
            }

            try {
                /** @var Dyna_ProductMatchRule_Model_Rule $model */
                foreach ($response as $model) {
                    $insertInto = $insertValues = [];

                    if (!empty($this->data['conditions'])) {
                        // For raw query, we need to get the conditions_serialized
                        $model->setData('conditions_serialized', $model->getSerializedConditions());
                    }

                    foreach ($model->getData() as $column => $value) {
                        if (in_array($column, $this->getRulesTableColumns())) {
                            $insertInto[] = $column;
                            $insertValues[':' . $column] = $value;
                        }
                    }

                    $sql = 'INSERT INTO `product_match_rule` ('.implode(', ', $insertInto).') VALUES ('.implode(', ', array_keys($insertValues)).')';

                    $connection->query($sql, $insertValues);
                    $lastInsertId = $connection->lastInsertId();
                    $model->setId($lastInsertId);

                    $model->setWebsiteIds($this->data['website_id']);
                    $model->setProcessContextsIds($this->data['process_context']);

                }

                unset($model);
                $success = true;
            } catch (Exception $e) {
                $this->_logError('[ERROR] Exception ( rule tile = ' .
                    $this->data['rule_title'] . ')' . $e->getMessage());
                $this->_logError('[ERROR] Skipping row no ' . $lc);
                fwrite(STDERR, '[ERROR] Exception ( rule tile = ' .
                    $this->data['rule_title'] . ')' . $e->getMessage());
                // Error occurred
                continue;
            }

            $this->noImports++;
            $this->_log('Imported row ' . print_r($this->line, true), true);
            $this->line = null;
            $this->data = null;
            $this->opType = null;
        }
        $connection->exec('SET FOREIGN_KEY_CHECKS = 1;');

        fclose($file);
        $this->_totalFileRows = $lc == 0 ? $lc : $lc - 1;
        $this->_skippedFileRows = $this->_totalFileRows - $this->noImports;

        return $success;
    }

    /**
     * Get all the column names of the product match rules table
     * @return array
     */
    protected function getRulesTableColumns()
    {
        if ($this->tableColumns === []) {
            $resource = Mage::getSingleton('core/resource');
            $readConnection = $resource->getConnection('core_read');
            /**
             * Get the table name
             */
            $tableName = $resource->getTableName('productmatchrule/rule');

            $saleafield = $readConnection->describeTable($tableName);

            $this->tableColumns = array_keys($saleafield);
        }

        return $this->tableColumns;
    }

    /**
     * Set the default operation types [id->name(strtolower)]
     */
    protected function setDefaultOperationTypes()
    {
        if (!$this->operationsTypes) {
            $defaultOperationTypes = $this->helper->getOperationTypes();
            foreach ($defaultOperationTypes as $id => $name) {
                $this->operationsTypes[$id] = strtolower($name);
            }
        }
    }

    /**
     * Set the default operation [id->name(strtolower)]
     */
    protected function setDefaultOperations()
    {
        if (!$this->operations) {
            $defaultOperations = $this->helper->getOperations();
            foreach ($defaultOperations as $id => $name) {
                $this->operations[$id] = strtolower($name);
            }
        }
    }

    /**
     * Set the default process contexts [id->code]
     */
    protected function setDefaultProcessContexts()
    {
        if (!$this->processContexts) {
            $this->processContexts = $this->processContextHelper->getProcessContextsByCodes();
        }
    }

    /**
     * Set the default sourceCollections [id->name(strtolower)]
     */
    protected function setDefaultSourceCollections()
    {
        if (!$this->sourceCollection) {
            $defaultSourceCollections = $this->helper->getSourceCollections();
            foreach ($defaultSourceCollections as $id => $name) {
                $this->sourceCollection[$id] = strtolower($name);
            }
        }
    }

    /**
     * Set the default sourceTypes [id->name(strtolower)]
     */
    protected function setDefaultSourceType()
    {
        if (!$this->sourceType) {
            $defaultSourceTypes = $this->helper->getSourceTypes();
            foreach ($defaultSourceTypes as $id => $name) {
                $this->sourceType[$id] = strtolower($name);
            }
        }
    }

    /**
     * Set the default rule_origin [id->name(strtolower)]
     */
    protected function setDefaultRuleOrigin()
    {
        if (!$this->ruleOrigin) {
            $defaultRuleOrigin = $this->helper->getRuleOrigin();
            foreach ($defaultRuleOrigin as $id => $name) {
                $this->ruleOrigin[$id] = strtolower($name);
            }
        }
    }

    /**
     * Set the websites that are available as an array of [websiteID => websiteCode]
     */
    protected function setAllDefaultWebsites()
    {
        if (!$this->websites) {
            $presentWebsites = Mage::app()->getWebsites();
            /**
             * @var Mage_Core_Model_Website $website
             */
            foreach ($presentWebsites as $website) {
                if ($website->getId() != 1) {
                    $this->websites[$website->getId()] = strtolower($website->getCode());
                }
            }
        }
    }

    /**
     * Format CSV header columns to underscore format
     *
     * @param array $header
     * @return array
     */
    public function formatKey($header)
    {
        foreach ($header as $key => $value) {
            $header[$key] = trim($header[$key]);
            $header[$key] = str_replace(' ', '_', $header[$key]);
            $header[$key] = strtolower($header[$key]);
        }

        return $header;
    }

    /**
     * @return Mage_Core_Helper_Abstract|null
     */
    public function getImportHelper()
    {
        /** @var Dyna_Import_Helper_Data _helper */
        return $this->_helper == null ? Mage::helper("dyna_import/data") : $this->_helper;
    }

    /**
     * Check Operation X OperationType combinations
     * For example defaulted* rules can't have target as SERVICE
     * Skip row if combination is not supported
     *
     * @return bool
     */
    protected function allowedOperationTypeCombination() {
        return isset($this->allowedOperationTypeCombinations[$this->operations[$this->_data['operation']]][$this->_data['operation_type']]);
    }

    /**
     * Log function
     *
     * @param string $msg
     * @param bool $verbose
     */
    public function _log($msg, $verbose = false)
    {
        if ($verbose && !$this->_debug) {
            return;
        }
        $this->_helper->logMsg('[' . $this->_currentProductSku . '] ' . $msg, false);
    }

    public function _logError($msg)
    {
        $this->_helper->logMsg($msg);
    }

    /**
     * Set operation type
     * If the type cannot be mapped to an ID (with the given name or the given id) => error, skip row
     *
     * @return bool
     */
    protected function setOperationType()
    {
        $this->opType = explode('-', trim(strtoupper($this->line[$this->position['operation_type']])));
        $valueToLookUp = strtolower(trim($this->line[$this->position['operation_type']]));

        return $this->lookupInKeyOrValueOfArray($valueToLookUp, 'operation_type', $this->operationsTypes, true);
    }

    /**
     * For a given value try the following match:
     * - if the given value is numeric try to match it with an ID - if is not possible error, skip row; if is found the ID is retained to be used for the import
     * - if the given value is not numeric try to match it with the value of an attribute (as it appear on the front end);
     * here we will 2 types of matches: exact name or if the name is contained in one of the attributes values
     * (for example for process_context in the csv can be the value '*' that will be matched with '* (all)')
     * if a match is found (exact or partial, depending on the parameter $exactLookup) than the ID is retained to be used for the import
     *
     * @param string $keyValueToLookUp (this is the name of the key that will help us for the logs)
     * @param string $valueToLookUp (this is the value that we are trying to match)
     * @param $arrayWhereToLookUp (this is the array with possible values as ID->name where we will try to find the  $valueToLookUp)
     * @param bool $exactLookup (this specifies if we should do a strpos or an exact match of string for the name)
     * @return bool
     */
    protected function lookupInKeyOrValueOfArray(
        $valueToLookUp,
        $keyValueToLookUp,
        $arrayWhereToLookUp,
        $exactLookup = true
    ) {
        if (is_numeric($valueToLookUp)) {
            if (array_key_exists($valueToLookUp, $arrayWhereToLookUp)) {
                $this->data[$keyValueToLookUp] = $valueToLookUp;
                return true;
            } else {
                $this->_logError('[ERROR] the ' . $keyValueToLookUp . ' =  ' . $valueToLookUp . ' is not valid (rule title = ' . $this->data['rule_title'] . '). skipping row');

                return false;
            }
        }

        foreach ($arrayWhereToLookUp as $key => $arrayValueWhereToLookUp) {
            if ($exactLookup) {
                if ($arrayValueWhereToLookUp == $valueToLookUp) {
                    $this->data[$keyValueToLookUp] = $key;
                    return true;
                }

            } else {
                if (strpos($arrayValueWhereToLookUp, $valueToLookUp) !== false) {
                    $this->data[$keyValueToLookUp] = $key;
                    return true;
                }
            }
        }

        $this->_logError('[ERROR] the ' . $keyValueToLookUp . ' =  ' . $valueToLookUp . ' is not valid (rule title = ' . $this->data['rule_title'] . ').  skipping row');
        $this->_logError(var_export($arrayWhereToLookUp, true));

        return false;
    }

    /**
     * Set operation
     * If the operation cannot be mapped to an ID => error, skip row
     *
     * @return bool
     */
    protected function setOperation()
    {
        // replace the value of the operation if is a min, max, eq operation with (n) in order to identify the operation
        $operation = strtolower(preg_replace('#\(.*?\)#s', '(n)', trim($this->line[$this->position['operation']])));

        return $this->lookupInKeyOrValueOfArray($operation, 'operation', $this->operations, true);
    }

    /**
     * Set the operation value
     * The operation value is required and must be numeric for operations: min, max, equal;
     * If the operation is min,max, eqal and the operation value is not numeric or is not present => skip row, error
     * @return bool
     */
    protected function setOperationValue()
    {
        // determine also the operation value if the operation is min, max, equal
        if (in_array($this->data['operation'], $this->operationsWithValues)) {
            // the operation value is provided between parentheses; if in parentheses is a number that number is the value; else it will be 0
            preg_match('#\((.*?)\)#', $this->line[$this->position['operation']], $match);
            // error skip row
            if (!is_numeric($match[1])) {
                $this->_logError('[ERROR] the operation_value is not valid (rule title = ' . $this->data['rule_title'] . '). skipping row');

                return false;
            }
            $this->data['operation_value'] = $match[1];
        }

        return true;
    }

    /**
     * Set override initial selectable value for Allowed rules
     *
     * @return bool
     */
    protected function setOverrideInitialSelectable()
    {
        $this->data['override_initial_selectable'] = isset($this->position['override_initial_selectable']) &&
        isset($this->line[$this->position['override_initial_selectable']]) ? $this->line[$this->position['override_initial_selectable']] : 0;

        return true;
    }

    /**
     * Set the left id based on the operation type and sku
     *
     * @return bool
     */
    protected function setLeftId()
    {
        $this->data['left_id'] = $this->_getEntity($this->opType[0], trim($this->line[$this->position['left_id']]));
        if (!$this->data['left_id']) {
            $this->_logError('The source cannot be determined.');

            return false;
        }

        // determine the source sku or source category id that will be used to create the source condition
        switch ($this->opType[0]) {
            case 'PRODUCT':
                $this->data['sourceSku'] = trim($this->line[$this->position['left_id']]);
                break;
            case 'CATEGORY':
                $this->data['sourceCategoryId'] = $this->data['left_id'];
                break;
        }

        return true;
    }

    /**
     * @param $type
     * @param $value
     * @return null
     */
    protected function _getEntity($type, $value)
    {
        switch (strtoupper($type)) {
            case 'PRODUCT':
                /**
                 * @var $productModel Dyna_Catalog_Model_Product
                 */
                $productModel = Mage::getModel("catalog/product");
                return $productModel->getIdBySku($value);
            case 'CATEGORY':
                /**
                 * @var $categoryModel Dyna_Catalog_Model_Category
                 */
                $categoryModel = Mage::getModel('catalog/category');
                $category = $categoryModel->getCategoryByNamePath(str_replace('->', '/', $value), '/');
                if ($category) {
                    return $category->getId();
                }
                return null;
            default:
                // Unknown entity
                break;
        }

        return null;
    }

    /**
     * Set the source_type id
     * if it is a string it must be mapped to one of the possible strings for source_type (it string must be contained in one of the default source_type) ->
     * and from that string we will used the id
     * if is an int it must be mapped to one of the possible ids for source_type
     * else -> skip row, error
     * @return bool
     */
    protected function setSourceType()
    {
        $valueToLookUp = strtolower(trim($this->line[$this->position['source_type']]));

        return $this->lookupInKeyOrValueOfArray($valueToLookUp, 'source_type', $this->sourceType, true);
    }

    /**
     * Set the target_type id
     * if it is a string it must be mapped to one of the possible strings for target_type (it string must be contained in one of the default source_type) ->
     * and from that string we will used the id
     * if is an int it must be mapped to one of the possible ids for source_type
     * else -> skip row, error
     * @return bool
     */
    protected function setTargetType()
    {
        $valueToLookUp = strtolower(trim($this->line[$this->position['target_type']]));

        return $this->lookupInKeyOrValueOfArray($valueToLookUp, 'target_type', $this->sourceType, true);
    }

    /**
     * Validate the presence of left_id or service_source_expression
     * If the operation_type is SERVICE-PRODUCT the service_source_expression must be present
     * If the operation_type is NOT SERVICE-PRODUCT the left_id must be present
     *
     * @param bool $foundLeftIdOrRightId
     * @return bool
     */
    protected function validateRequiredProductIdOrServiceSourceExpression($foundLeftIdOrRightId, $sourceOrTarget)
    {
        // if no left/right_id nor service_source/target_expression => error, skip row
        if (!$foundLeftIdOrRightId) {
            $this->_logError('[ERROR] no ' . $sourceOrTarget . ' nor service_' . $sourceOrTarget . '_expression can be determined (rule title = ' . $this->data['rule_title'] . '). skipping row');

            return false;
        }

        if ($this->data['operation_type'] == Dyna_ProductMatchRule_Model_Rule::OP_TYPE_S2P
            || $this->data['operation_type'] == Dyna_ProductMatchRule_Model_Rule::OP_TYPE_S2C
            || $this->data['operation_type'] == Dyna_ProductMatchRule_Model_Rule::OP_TYPE_S2S
        ) {
            if (empty($this->data['service_source_expression'])) {
                $this->_logError('[ERROR] when the operation type is SERVICE-PRODUCT or SERVICE-CATEGORY the service_source_expression must be present(rule title = ' . $this->data['rule_title'] . '). skipping row');

                return false;
            }
        } else {
            if (empty($this->data['left_id'])) {
                $this->_logError('[ERROR] when the operation type is not SERVICE-PRODUCT the left_id must be present (rule title = ' . $this->data['rule_title'] . '). skipping row');

                return false;
            }
        }

        return true;
    }

    /**
     * Set the right id based on the operation type and sku
     * If it cannot be determined => error, skip row
     *
     * @return bool
     */
    protected function setRightId()
    {
        $this->data['right_id'] = $this->_getEntity($this->opType[1], trim($this->line[$this->position['right_id']]));
        if (!$this->data['right_id']) {
            $this->_logError('[ERROR] The target is not valid (line = ' . $this->noImports . '). skipping row');

            return false;
        }

        return true;
    }

    /**
     * Set the priority
     * If the priority is not determined => skip row, error
     *
     * @return bool
     */
    protected function setPriority()
    {
        $this->data['priority'] = (int)$this->line[$this->position['priority']];
        if (!isset($this->data['priority'])) {
            $this->_logError('[ERROR] The priority is not present/valid (rule title = ' . $this->data['rule_title'] . '). skipping row');

            return false;
        }

        return true;
    }

    /**
     * Set locked
     * If the values = locked or 1 => than is locked (meaning 1)
     * Else is 0, meaning unlocked
     */
    protected function setIsLocked()
    {
        $lockedData = trim($this->line[$this->position['locked']]);
        if (is_numeric($lockedData)) {
            $this->data['locked'] = ($lockedData == 1) ? 1 : 0;
        } elseif (strtolower(trim($this->line[$this->position['locked']])) == 'locked') {
            $this->data['locked'] = 1;
        } else {
            $this->data['locked'] = 0;
        }

    }

    /**
     * Try to set the websites for the rule;
     * Go through each website id or website code provided (the list provided in the csv can be of the form "id1,id2" or "code1,code2"
     * and see if the code/id provided is in the list of websites codes/ids that are possible.
     * If one id/code is not found in the possible list => skip the row, error
     *
     * @return bool
     */
    protected function setWebsiteIds()
    {
        $websitesData = explode(',', strtolower(trim($this->line[$this->position['website_id']])));
        foreach ($websitesData as $key => $possibleWebsite) {
            // if one of the possible websites provided in the csv is "*" the rule will be available for all websites; skip the rest of the code
            if ($possibleWebsite == self::ALL_WEBSITES) {
                $this->data['website_id'] = array_keys($this->websites);

                return true;
            }
            // the website provided is the code and we should store the id
            if ($validWebsite = array_search($possibleWebsite, $this->websites)) {
                $this->data['website_id'][$key] = $validWebsite;
            } // the website provided is id and we should store it
            elseif (array_key_exists($possibleWebsite, $this->websites)) {
                $this->data['website_id'][$key] = $possibleWebsite;
            } // not match can be found between the given website and a code or an id
            else {
                $this->_logError('[ERROR] The website_id ' . print_r($websitesData,
                        true) . ' is not present/valid (rule title = ' . $this->data['rule_title'] . '). skipping row');

                return false;
            }
        }

        return true;
    }

    /**
     * Set the rule_origin data
     * @return bool
     */
    protected function setRuleOrigin()
    {
        $valueToLookUp = strtolower(trim($this->line[$this->position['rule_origin']]));
        if (!$this->lookupInKeyOrValueOfArray($valueToLookUp, 'rule_origin', $this->ruleOrigin, true)) {
            $this->_log('rule_origin was set to the default value for import 1', true);
            $this->data['rule_origin'] = 1;
        }
    }

    /**
     * Set the process context id
     * if it is a string it must be mapped to one of the possible strings for process context (it string must be contained in one of the default process context) ->
     * and from that string we will used the id
     * if is an int it must be mapped to one of the possible ids for process context
     * else -> skip row, error
     * @return bool
     */
    protected function setProcessContextId()
    {
        $processContextsForRule = strtoupper(trim($this->line[$this->position['process_context']]));

        if ($processContextsForRule == self::ALL_PROCESS_CONTEXTS) {
            $this->data['process_context'] = $this->processContexts;
            return true;
        }

        $processContextsForRuleArray = explode(',', $processContextsForRule);
        foreach ($processContextsForRuleArray as $processRuleContextForRule) {
            if (isset($this->processContexts[trim($processRuleContextForRule)])) {
                $this->data['process_context'][] = $this->processContexts[trim($processRuleContextForRule)];
            } else {
                $this->_logError('[ERROR] the process_context ' . trim($processRuleContextForRule) . ' is not valid (rule title = ' . $this->data['rule_title'] . '). skipping row');
                return false;
            }
        }

        return true;
    }

    /**
     * Set effective date
     */
    protected function setEffectiveDate()
    {
        $lockedData = trim($this->line[$this->position['effective_date']]);
        if (!empty($lockedData)) {
            $date = new DateTime($lockedData);
            $date = $date->format('Y-m-d');
            $errors = DateTime::getLastErrors();

            if (!empty($errors['warning_count'])) {
                $this->_logError('[ERROR] Invalid format for effective_date ' . $lockedData);
            } else {
                $this->data['effective_date'] = $date;
            }
        } else {
            $this->data['effective_date'] = null;
        }
    }

    /**
     * Set expiration date
     */
    protected function setExpirationDate()
    {
        $lockedData = trim($this->line[$this->position['expiration_date']]);
        if (!empty($lockedData)) {
            $date = new DateTime($lockedData);
            $date = $date->format('Y-m-d');
            $errors = DateTime::getLastErrors();

            if (!empty($errors['warning_count'])) {
                $this->_logError('Invalid format for expiration_date ' . $lockedData);
            } else {
                $this->data['expiration_date'] = $date;
            }
        } else {
            $this->data['expiration_date'] = null;
        }
    }

    /**
     * Set package type for current entry
     */
    public function setPackageType()
    {
        /** @var Dyna_Package_Model_PackageType $packageTypeModel */
        $packageTypeModel = Mage::getModel("dyna_package/packageType");

        $packageTypes = array_map(function ($packageType) {
            return trim($packageType);
        }, explode(",", $this->line[$this->position['package_type_id']]));
        foreach ($packageTypes as $packageType) {
            // If not previously loaded, load and cache it
            if (empty($this->packageTypes[$packageType])) {
                $packageTypeObj = $packageTypeModel->loadByCode($packageType);
                if (!$packageTypeObj->getId()) {
                    $errorMessage = "Trying to import combination rules for an invalid package code: " . $packageType . ". Have all the package types importers been run?";
                    $packageTypeCollection = Mage::getModel("dyna_package/packageType")
                        ->getCollection();
                    $packageTypeCodes = [];
                    foreach ($packageTypeCollection as $packageTypeModel) {
                        $packageTypeCodes[] = $packageTypeModel->getPackageCode();
                    }
                    $errorMessage .= " Valid backend package types are: " . implode(",", $packageTypeCodes);

                    Mage::throwException($errorMessage);
                }

                // Deleting old rules has been moved to shell script
                $this->packageTypes[$packageType] = $packageTypeObj->getId();
            }

            $this->data["package_type"][] = $this->packageTypes[$packageType];
        }

        return true;
    }

    /**
     * Set the source_collection id
     * if it is a string it must be mapped to one of the possible strings for source_collection (it string must be contained in one of the default source_collection) ->
     * and from that string we will used the id
     * if is an int it must be mapped to one of the possible ids for source_collection
     * else -> skip row, error
     * @return bool
     */
    protected function setSourceCollection()
    {
        if ($this->data['source_type'] == 0) {
            $valueToLookUp = strtolower(trim($this->line[$this->position['source_collection']]));

            return $this->lookupInKeyOrValueOfArray($valueToLookUp, 'source_collection', $this->sourceCollection, true);
        } else {
            return true;
        }
    }

    public function validateServiceSourceExpression($serviceSourceExpression)
    {
        /**
         * @var $dynaCoreHelper Dyna_Core_Helper_Data
         */
        $dynaCoreHelper = Mage::helper('dyna_core');

        $result = $dynaCoreHelper->parseExpressionLanguage($serviceSourceExpression,
            array(
                'customer',
                'availability',
                'cart',
                'order',
                'catalog',
                'installBase',
                'agent',
            ),
            $this->_helper->getImportLogFile());

        if ($result === null) {
            return false;
        }

        return true;
    }

    public function validateServiceTargetExpression($serviceTargetExpression)
    {
        /**
         * @var $dynaCoreHelper Dyna_Core_Helper_Data
         */
        $dynaCoreHelper = Mage::helper('dyna_core');

        $result = $dynaCoreHelper->parseExpressionLanguage($serviceTargetExpression,
            array(
                'customer',
                'availability',
                'cart',
                'order',
                'catalog',
                'installBase',
                'agent'
            ),
            $this->_helper->getImportLogFile());

        if ($result === null) {
            return false;
        }

        return true;
    }
}
