<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Dyna_ProductMatchRule_Model_Resource_Service_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{

    protected function _construct()
    {
        $this->_init('dyna_productmatchrule/service');
    }
}