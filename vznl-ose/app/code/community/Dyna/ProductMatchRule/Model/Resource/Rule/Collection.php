<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Dyna_ProductMatchRule_Model_Resource_Rule_Collection extends Omnius_ProductMatchRule_Model_Resource_Rule_Collection
{

    /**
     * Retrieve ids for limited collection
     *
     * @return array
     */
    public function getIds()
    {
        $this->_renderFilters()
            ->_renderOrders()
            ->_renderLimit();
        $idsSelect = clone $this->getSelect();

        $idsSelect->columns($this->getResource()->getIdFieldName(), 'main_table');
        return $this->getConnection()->fetchCol($idsSelect);
    }
}