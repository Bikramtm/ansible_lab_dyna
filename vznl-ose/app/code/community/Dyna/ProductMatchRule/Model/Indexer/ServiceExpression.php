<?php

class Dyna_ProductMatchRule_Model_Indexer_ServiceExpression extends Omnius_ProductMatchRule_Model_Indexer_Abstract
{
    protected $args = [];
    protected $packageTypes = [];

    const DELETE_ACTION = 0;
    const ADD_ACTION = 1;

    protected $_serviceTable;

    /** @var array */
    protected $_serviceMatches = array();

    /** @var array */
    protected $_prev = array();

    /** @var array */
    protected $_allProductIds = array();

    /** @var array */
    protected $_allCategoryIds = array();

    /** @var array */
    protected $_productCategories = array();

    /** @var array */
    protected $_websites = array();

    /** @var array */
    protected $_websiteIds = array();

    /**
     * Initiate indexer
     */
    public function __construct()
    {
        parent::__construct();

        $this->_serviceTable = (string)Mage::getResourceSingleton('dyna_productmatchrule/service')->getMainTable();

        $this->initMemoryLimits();

        $this->_init('dyna_productmatchrule/indexer_serviceExpression');
    }


    /**
     * Return current indexer name
     *
     * @return string
     */
    public function getName()
    {
        return 'Service expression rules';
    }

    /**
     * Return current indexer description
     *
     * @return string
     */
    public function getDescription()
    {
        return 'Index rules based on service expression and populate product_match_service_index';
    }

    /**
     * Empty index and generate whitelist based on rules
     */
    public function reindexAll()
    {
        // Package types can be sent as params from cli indexing
        if (!empty($this->args)) {
            // Expecting args to contain a list of package type codes based on which we will load their ids
            /** @var Dyna_Package_Model_PackageType $packageTypeModel */
            $packageTypeModel = Mage::getModel("dyna_package/packageType");
            foreach ($this->args as $packageCode) {
                $package = $packageTypeModel->loadByCode(strtoupper($packageCode));
                if ($package->getId()) {
                    $this->packageTypes[$package->getPackageCode()] = $package->getId();
                } else {
                    $this->productMatchRuleHelper->logIndexer("[ServiceExpression] Skipping argument package type: " . $packageCode . " because cannot be loaded (check Admin / Catalog / Packages Configuration / Package Types)");
                }
            }
        } else {
            // Loading all package types an executing indexer consecutive for each rule
            $packageTypeCollection = Mage::getModel("dyna_package/packageType")->getCollection();
            foreach ($packageTypeCollection as $package) {
                $this->packageTypes[$package->getPackageCode()] = $package->getId();
            }
        }

        if (empty($this->packageTypes)) {
            $this->productMatchRuleHelper->logIndexer("[ServiceExpression] Exiting indexer because there are no package types defined. Please run package types import and execute indexer later.");
            exit;
        }

        $this->_reindexAll();
    }

    /**
     * Params setter called from shell/deindexer.php to filter indexing for a certain package type
     * @param array $args
     * @return $this
     */
    public function setParams($args = [])
    {
        $this->args = $args;

        return $this;
    }

    /**
     * Reindex all
     */
    protected function _reindexAll()
    {
        $this->start();

        foreach ($this->packageTypes as $packageCode => $packageId) {
            $allowedOperationTypes = [
                Dyna_ProductMatchRule_Model_Rule::OP_TYPE_S2P,
                Dyna_ProductMatchRule_Model_Rule::OP_TYPE_S2C,
                Dyna_ProductMatchRule_Model_Rule::OP_TYPE_C2S,
                Dyna_ProductMatchRule_Model_Rule::OP_TYPE_P2S
            ];
            $this->productMatchRuleHelper->logIndexer("[ServiceExpression] Removing indexer entries for package type: " . $packageCode . ".");
            $this->getConnection()->query(sprintf('DELETE FROM `%s` WHERE package_type=%s;', $this->_serviceTable, $packageId));
            $this->closeConnection();

            $this->productMatchRuleHelper->logIndexer("[ServiceExpression] Building indexer entries for package type: " . $packageCode . ".");
            /** @var Omnius_ProductMatchRule_Model_Resource_Rule_Collection $collection */
            $collection = Mage::getResourceModel('productmatchrule/rule_collection')
                ->addFieldToFilter('operation_type', ['in' => $allowedOperationTypes])
                ->addFieldToFilter('package_type', ['eq' => $packageId])
                ->setPageSize(self::BATCH_SIZE);

            $currentPage = 1;
            $pages = $collection->getLastPageNumber();

            /**
             * Process the rules within batches to remove
             * the risk using all the allocated memory
             * only to load the rule collections
             * This way, we never load the whole
             * collection into the current memory
             */
            do {
                $collection->setCurPage($currentPage)->getSelect()
                    ->reset(Zend_Db_Select::ORDER)
                    ->order(new Zend_Db_Expr(
                        "CASE WHEN
                    `operation` <> " . Omnius_ProductMatchRule_Model_Rule::OP_DEFAULTED . "
                    AND `operation` <> " . Omnius_ProductMatchRule_Model_Rule::OP_OBLIGATED . "
                    THEN 1 ELSE 2 END,
                    priority ASC"));
                foreach ($collection as $rule) {
                    $this->processItem($rule);
                }
                $currentPage++;
                $collection->clear();
            } while ($currentPage <= $pages);

            $this->_applyChanges();
        }

        $this->end();
    }

    /**
     * @param $rule Dyna_ProductMatchRule_Model_Rule
     */
    protected function processItem($rule)
    {
        $websiteIds = implode(",", $rule->getWebsiteIds());

        switch ($rule['operation_type']) {
            case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_S2P:
                // Make sure that it is a service type rule and the product exists
                if ($rule['source_type'] == 0 || !in_array($rule['right_id'], $this->_allProductIds)) {
                    break;
                }

                $rightId = $rule['right_id'];
                $priority = $rule['priority'];
                $operation = $rule['operation'];
                $operationValue = $rule['operation_value'];

                foreach (explode(",", $websiteIds) as $websiteId) {
                    $this->addMatch(
                        $rule->getId(),
                        $rule->getPackageType(),
                        $rightId,
                        $websiteId,
                        $priority,
                        $operation,
                        $operationValue,
                        "null"
                    );
                }
                break;
            case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_S2C:
                // Make sure that it is a service type rule and the category exists
                $checkOk = true;
                foreach (explode(",", $websiteIds) as $websiteId) {
                    if ($rule['source_type'] == 0 || !isset($this->_productCategories[$websiteId][$rule['right_id']])) {
                        $checkOk = false;
                    }
                }

                if (!$checkOk) {
                    break;
                }

                foreach (explode(",", $websiteIds) as $websiteId) {
                    foreach ($this->_productCategories[$websiteId][$rule['right_id']] as &$catProdId) {
                        $priority = $rule['priority'];
                        $operation = $rule['operation'];
                        $operationValue = $rule['operation_value'];

                        $this->addMatch(
                            $rule->getId(),
                            $rule->getPackageType(),
                            $catProdId,
                            $websiteId,
                            $priority,
                            $operation,
                            $operationValue,
                            "null"
                        );
                    }
                    unset($catProdId);
                }
                break;
            case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_C2S:
                // Make sure that it is a service type rule and the category exists
                $checkOk = true;
                foreach (explode(",", $websiteIds) as $websiteId) {
                    if (!isset($this->_productCategories[$websiteId][$rule['left_id']])) {
                        $checkOk = false;
                    }
                }

                if (!$checkOk) {
                    break;
                }

                foreach (explode(",", $websiteIds) as $websiteId) {
                    foreach ($this->_productCategories[$websiteId][$rule['left_id']] as &$catProdId) {
                        $priority = $rule['priority'];
                        $operation = $rule['operation'];
                        $operationValue = $rule['operation_value'];

                        $this->addMatch(
                            $rule->getId(),
                            $rule->getPackageType(),
                            "null",
                            $websiteId,
                            $priority,
                            $operation,
                            $operationValue,
                            $catProdId
                        );
                    }
                    unset($catProdId);
                }
                break;
            case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_P2S:
                // Make sure that it is a service type rule and the product exists
                if (!in_array($rule['left_id'], $this->_allProductIds)) {
                    break;
                }


                $leftId = $rule['left_id'];
                $priority = $rule['priority'];
                $operation = $rule['operation'];
                $operationValue = $rule['operation_value'];

                foreach (explode(",", $websiteIds) as $websiteId) {
                    $this->addMatch(
                        $rule->getId(),
                        $rule->getPackageType(),
                        "null",
                        $websiteId,
                        $priority,
                        $operation,
                        $operationValue,
                        $leftId
                    );
                }
                break;
        }
    }

    /**
     * Executed before processing the collection
     */
    public function start()
    {
        $this->_websites = array();
        $this->_websiteIds = array_keys(Mage::app()->getWebsites());

        foreach ($this->_websiteIds as $websiteId) {
            $this->_websites[$websiteId] = array();
            $this->_productCategories[$websiteId] = Mage::getModel('productmatchrule/rule')->getAllCategoryProducts($websiteId);
        }

        //get id of all products to check if these are still available
        $this->_allProductIds = Mage::getResourceModel('catalog/product_collection')->getAllIds();
        $this->_allCategoryIds = Mage::getResourceModel('catalog/category_collection')->getAllIds();
    }

    /**
     * Executed after processing the collection
     */
    public function end()
    {
        $this->_applyChanges();
        parent::end();
    }

    /**
     * Groups gathered items together by statement (INSERT/DELETE)
     * to decrease the number of statements executed on the database
     */
    protected function _applyChanges()
    {
        $this->_serviceMatches = array_reverse($this->_serviceMatches);
        while (($_row = array_pop($this->_serviceMatches)) !== null) {
            if (!count($this->_prev) || $_row[0] === $this->_prev[0][0]) {
                $this->_prev[] = $_row;
            } elseif ($this->_prev[0][0] === self::ADD_ACTION) {
                $this->_insertMultipleService();
                $this->_prev = array($_row);
            } else {
                //should not happen
                throw new LogicException('Invalid action type for row');
            }
        }
        unset($_row);

        /**
         * If something remains unprocessed in the _prev array
         */
        if (count($this->_prev)) {
            if ($this->_prev[0][0] === self::ADD_ACTION) {
                $this->_insertMultipleService();
            } else {
                //should not happen
                throw new LogicException('Invalid action type for row');
            }
        }
        $this->_serviceMatches = [];
        $this->_prev = [];
    }

    /**
     * Register indexer required data inside event object
     *
     * @param   Mage_Index_Model_Event $event
     */
    protected function _registerEvent(Mage_Index_Model_Event $event)
    {
        // no-op
    }

    /**
     * Process event based on event state data
     *
     * @param   Mage_Index_Model_Event $event
     */
    protected function _processEvent(Mage_Index_Model_Event $event)
    {
        // no-op
    }

    /**
     * Method used to populate product_match_service_index table for service based rules
     * @param $ruleId
     * @param $packageType
     * @param $rightId
     * @param $websiteId
     * @param $priority
     * @param $operation
     * @param $operationValue
     * @param $sourceProductId
     * @internal param $right
     */
    protected function addMatch($ruleId, $packageType, $rightId, $websiteId, $priority, $operation, $operationValue, $sourceProductId)
    {
        $this->_serviceMatches[] = array(1, array($websiteId, $ruleId, $packageType, $rightId, $priority, $operation, $operationValue, $sourceProductId));
        $this->_assertMemory();
    }

    /**
     * Builds INSERT statements and executes them
     * Iterates over the items withing the $_prev array
     * and builds the INSERT statements, always checking
     * if we approach the MySQL max_allowed_packet limit.
     * If we approach the limit too much, we execute the current
     * SQL statement and start building the statements for the remaining items
     */
    protected function _insertMultipleService()
    {
        $values = '';

        $add = array();
        $this->_prev = array_reverse($this->_prev);
        while (($_row = array_pop($this->_prev)) !== null) {
            $add[$_row[1][0]][] = array($_row[1][1], $_row[1][2], $_row[1][3], $_row[1][4], $_row[1][5], $_row[1][6], $_row[1][7]);
        }
        unset($_row);
        foreach ($add as $websiteId => &$combinations) {
            foreach ($combinations as $key => &$combination) {
                $values .= sprintf('("%s","%s","%s",%s,"%s","%s","%s",%s),', $websiteId, $combination[0], $combination[1], $combination[2], $combination[3], $combination[4], $combination[5], $combination[6]);

                if (strlen($values) >= $this->_maxPacketsLength) {
                    $sql = sprintf(
                        'INSERT INTO `%s` (`website_id`,`rule_id`,`package_type`,`target_product_id`,`priority`,`operation`,`operation_value`, `source_product_id`) VALUES %s ON DUPLICATE KEY 
                        UPDATE `website_id`=VALUES(`website_id`), `priority`=VALUES(`priority`), `operation`=VALUES(`operation`), `operation_value`=VALUES(`operation_value`);' . PHP_EOL,
                        $this->_serviceTable,
                        trim($values, ',')
                    );
                    $this->_query = $this->getConnection()->query($sql);
                    $this->closeConnection();

                    unset($sql, $values);

                    $values = '';
                }
                unset($combinations[$key]);
            }
            unset($combination);
            unset($add[$websiteId]);
        }
        unset($combinations);
        unset($add);

        if ($values) {
            $sql = sprintf(
                'INSERT INTO `%s` (`website_id`,`rule_id`,`package_type`,`target_product_id`,`priority`,`operation`,`operation_value`, `source_product_id`) VALUES %s ON DUPLICATE KEY 
                        UPDATE `website_id`=VALUES(`website_id`), `priority`=VALUES(`priority`), `operation`=VALUES(`operation`), `operation_value`=VALUES(`operation_value`);' . PHP_EOL,
                $this->_serviceTable,
                trim($values, ',')
            );
            $this->_query = $this->getConnection()->query($sql);
            $this->closeConnection();

            unset($sql, $values);
        }
    }
}
