<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Adminhtml_ExportController
 */
require_once ('Omnius/Sandbox/controllers/Adminhtml/ReleaseController.php');
class Dyna_Sandbox_Adminhtml_ReleaseController extends Omnius_Sandbox_Adminhtml_ReleaseController
{
    /**
     * @return string
     */
    public function importViewAction()
    {
        $this->loadLayout();
        $block =  $this->getLayout()->createBlock('dyna_sandbox/adminhtml_release_import');
        echo $block->toHtml();
    }

    public function importAction()
    {
        $messages = Mage::helper("dyna_sandbox")->checkRunningImports();
        if (($data = $this->getRequest()->getPost())&& count($messages) == 1) {

            /** @var Dyna_Sandbox_Model_ImportInformation $model */
            $model = Mage::getModel('dyna_sandbox/importInformation');

            $model->setType(json_encode($data['type']));
            $model->setErrors(0);
            $model->setStartedAt(date("Y-m-d H:i:s", Mage::getModel('core/date')->timestamp()));
            $model->setStatus($model::STATUS_RUNNING);
            $model->save();

            if($model->getId()){
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Import started for types: '. implode(',',$data['type']) . '. This may take a while.'));
                /** @var Dyna_Sandbox_Helper_Data $importHelper */
                $importHelper = Mage::helper('dyna_sandbox');
                $importHelper->performImport($model);
            }
        }elseif(count($messages) > 1){
            foreach ($messages as $message){
                Mage::getSingleton('adminhtml/session')->addError($message);
            }
        }

        //redirect to index
        $this->_redirect('*/*');
    }

    /**
     * Persist the data in the database
     */
    public function saveAction()
    {
        $post_data = $this->getRequest()->getPost();
      
        if ($post_data) {
            if (isset($post_data['deadline'])) {
                $a = new DateTime(Mage::getSingleton('core/date')->gmtDate());
                $b = new DateTime(Mage::getSingleton('core/date')->date());
                $diff = $a->diff($b);
                $deadlineStr = $post_data['deadline'];
                $deadline = new DateTime($deadlineStr);
                if ($diff->invert) {
                    $deadline->add($diff);
                } else {
                    $deadline->sub($diff);
                }
                $post_data['deadline'] = $deadline->format('Y-m-d H:i:s');
                if(isset($post_data['is_heavy'])){
                    $post_data['is_heavy'] = 1;
                }else {
                    $post_data['is_heavy'] = 0;
                }

                if(isset($post_data['export_media'])){
                    $post_data['export_media'] = 1;
                }else {
                    $post_data['export_media'] = 0;
                }

                if(isset($post_data['import_check'])){
                    $post_data['import_check'] = 1;
                }else {
                    $post_data['import_check'] = 0;
                }
            }

            try {
                $model = Mage::getModel('sandbox/release')
                    ->addData($post_data)
                    ->setId($this->getRequest()->getParam('id'))
                    ->save();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Release was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setReleaseData(false);

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setReleaseData($this->getRequest()->getPost());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }

        }
        $this->_redirect('*/*/');
    }
}
