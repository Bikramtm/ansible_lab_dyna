<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Sandbox_Adminhtml_ExportController
 */
require_once ('Omnius/Sandbox/controllers/Adminhtml/ExportController.php');
require_once ('Dyna/Sandbox/Helper/Array2XML.php');

class Dyna_Sandbox_Adminhtml_ExportController extends Omnius_Sandbox_Adminhtml_ExportController
{
    protected $websites = null;

    /**
     * Dyna_Sandbox_Adminhtml_ExportController constructor.
     * @param Zend_Controller_Request_Abstract $request
     * @param Zend_Controller_Response_Abstract $response
     * @param array $invokeArgs
     */
    public function __construct(Zend_Controller_Request_Abstract $request, Zend_Controller_Response_Abstract $response, array $invokeArgs = array())
    {
        parent::__construct($request, $response, $invokeArgs);
    }

    /**
     * Render layout on default index action
     */
    public function indexAction()
    {
        $this->_getSession()->addNotice(
            Mage::helper('index')->__('Please note that the grid refreshes at a set interval to update messages about running exports.')
        );
        parent::indexAction();
    }

    public function checkExportsAction()
    {
        return $this->getResponse()->setBody(
            $this->getLayout()->createBlock('sandbox/adminhtml_export')->toHtml()
        );
    }
}
