<?php
/* @var $installer Dyna_Cable_Model_Resource_Setup */
$releasesTableName = 'sandbox/release';

$installer = $this;
$installer->startSetup();

$connection = $installer->getConnection();
$releasesTable = 'sandbox_release'; //$installer->getTable($releasesTableName);

$newColumns = [
    'schedule_type' => array(
        'type'      => Varien_Db_Ddl_Table::TYPE_SMALLINT,
        'nullable'  => false,
        'default'   => 0,
        'after'     => 'deadline',
        'comment'   => 'Scheduled execution type',
    ),
    'one_time_date' => array(
        'type'      => Varien_Db_Ddl_Table::TYPE_DATETIME,
        'nullable'  => false,
        'after'     => 'schedule_type',
        'comment'   => 'One time execution date',
    ),
    'period' => array(
        'type'      => Varien_Db_Ddl_Table::TYPE_SMALLINT,
        'nullable'  => false,
        'default'   => 0,
        'after'     => 'one_time_date',
        'comment'   => 'Period type',
    ),
    'week_day' => array(
        'type'      => Varien_Db_Ddl_Table::TYPE_SMALLINT,
        'nullable'  => false,
        'default'   => 0,
        'after'     => 'period',
        'comment'   => 'Periodic execution - Day of the week',
    ),
    'month_day' => array(
        'type'      => Varien_Db_Ddl_Table::TYPE_SMALLINT,
        'nullable'  => false,
        'default'   => 0,
        'after'     => 'week_day',
        'comment'   => 'Periodic execution - Day of month',
    ),
    'time' => array(
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable'  => true,
        'default'   => null,
        'after'     => 'month_day',
        'comment'   => 'Periodic execution - Time of day',
    )
];

if ($releasesTable) {
    foreach ($newColumns as $columnId => $options) {
        if (!$connection->tableColumnExists($releasesTable, $columnId)) {
            $connection->addColumn($releasesTable, $columnId, $options);
        }
    }
}

$installer->endSetup();
