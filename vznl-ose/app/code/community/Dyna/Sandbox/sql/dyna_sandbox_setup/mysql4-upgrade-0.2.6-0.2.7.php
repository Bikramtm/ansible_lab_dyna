<?php
/** @var Mage_Eav_Model_Entity_Setup $installer */
$installer = $this;
$installer->startSetup();

$executionTable = $this->getTable('import/execution');
$installer->getConnection()->modifyColumn($executionTable, 'type', 'VARCHAR(255) default NULL');
$installer->endSetup();
