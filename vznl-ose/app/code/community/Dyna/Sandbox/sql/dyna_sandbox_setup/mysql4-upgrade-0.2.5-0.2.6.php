<?php
/** @var Mage_Eav_Model_Entity_Setup $installer */
$installer = $this;
$installer->startSetup();
/**
 * Move column import_check from import_execution to sandbox_release
 */

$releaseTable = 'sandbox_release';
$importExecutionTable = 'import_execution';
$columnToMove = 'import_check';
$connection = $installer->getConnection();
if (!$connection->tableColumnExists($releaseTable, $columnToMove)) {
    $connection->addColumn($releaseTable, $columnToMove, Varien_Db_Ddl_Table::TYPE_TINYINT, null);
}
if ($connection->tableColumnExists($importExecutionTable, $columnToMove)) {
    $connection->dropColumn($importExecutionTable, $columnToMove);
}

$installer->endSetup();