<?php
/** @var Mage_Eav_Model_Entity_Setup $installer */
$installer = $this;
$installer->startSetup();

$executionTable = $this->getTable('import/execution');
$installer->getConnection()->addColumn($executionTable, "type", [
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'length' => 15,
    'comment' => 'Type of import'
]);

$installer->endSetup();