<?php
/** @var Mage_Eav_Model_Entity_Setup $installer */
$installer = $this;
$installer->startSetup();

$executionTable = $this->getTable('import/execution');
$installer->getConnection()->addColumn($executionTable, "import_check", Varien_Db_Ddl_Table::TYPE_TINYINT, null);

$installer->endSetup();