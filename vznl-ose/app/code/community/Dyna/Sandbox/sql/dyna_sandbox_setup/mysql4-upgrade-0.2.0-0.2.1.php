<?php
/** @var Mage_Eav_Model_Entity_Setup $installer */
$installer = $this;
$installer->startSetup();

$summaryTable = $installer->getTable("import/summary");
$executionTable = $this->getTable('import/execution');

if (!$this->tableExists($executionTable)) {
    $table = $this->getConnection()
        ->newTable($executionTable)
        ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true,], 'ID')
        ->addColumn('started_at', Varien_Db_Ddl_Table::TYPE_DATETIME, 5, [], 'Started At')
        ->addColumn('finished_at', Varien_Db_Ddl_Table::TYPE_DATETIME, 5, [], 'Finished At')
        ->addColumn('states', Varien_Db_Ddl_Table::TYPE_TEXT, 15, [], 'States')
        ->addColumn('errors', Varien_Db_Ddl_Table::TYPE_TINYINT, 1, [], 'Errors');
    $this->getConnection()->createTable($table);
}


$installer->getConnection()->addColumn($summaryTable, "execution_id", [
    'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
    'length' => 10,
    'comment' => 'Mapped to import_execution table'
]);



$installer->getConnection()->addForeignKey('FK_summary_to_execution',$summaryTable,'execution_id',$executionTable,'id');

$installer->endSetup();