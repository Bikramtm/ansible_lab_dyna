<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Sandbox_Model_Mysql4_ImportInformation
 */
class Dyna_Sandbox_Model_Mysql4_ImportInformation extends Mage_Core_Model_Mysql4_Abstract
{
    /**
     * Constructor override
     */
    protected function _construct()
    {
        $this->_init('dyna_sandbox/importInformation', 'id');
    }
}
