<?php
/**
 * Class Dyna_Sandbox_Helper_Data
 */
class Dyna_Sandbox_Helper_Data extends Mage_Core_Helper_Abstract
{
    protected $connection;
    private $_infoExecution = null;
    private $_params = [];
    private $_shellFile = 'import_full.sh';
    private $_typesTemplate = [
        'mobile'    => '-m',
        'cable'     => '-k',
        'fixed'       => '-f',
        'campaign' => '-c',
        'bundle'   => '-b',
        'cable_artifact' => '-a',
        'reference' => '-r',
        'salesid' => '-n',
    ];
    private $websites;
    protected $logfilePrefix = 'sandbox_import';
    protected $logfileSuffix = '.log';

    CONST FILENAME_FIXED_KEYWORD = 'FN';
    CONST FILENAME_CABLE_KEYWORD = 'Cable';
    CONST FILENAME_CABLE_ARTIFACT_KEYWORD = 'Cable_Rules_Plugin';
    CONST FILENAME_MOBILE_KEYWORD = 'Mobile';
    CONST FILENAME_BUNDLES_KEYWORD = 'Bundle';
    CONST FILENAME_CAMPAIGNS_KEYWORD = 'Campaign';
    CONST FILENAME_REFERENCE_KEYWORD = 'Reference';
    CONST FILENAME_SALESID_KEYWORD = 'Salesid';

    public $_keywordToLabel = [
        self::FILENAME_FIXED_KEYWORD => 'Fixed',
        self::FILENAME_CABLE_ARTIFACT_KEYWORD => 'Cable Artifact',
        self::FILENAME_CABLE_KEYWORD => 'Cable',
        self::FILENAME_MOBILE_KEYWORD => 'Mobile',
        self::FILENAME_BUNDLES_KEYWORD => 'Bundle',
        self::FILENAME_CAMPAIGNS_KEYWORD => 'Campaign',
        self::FILENAME_REFERENCE_KEYWORD => 'Reference',
        self::FILENAME_SALESID_KEYWORD => 'Salesid',
    ];

    public $_labelToKeyword = [
        'Fixed' => self::FILENAME_FIXED_KEYWORD,
        'Cable Artifact' => self::FILENAME_CABLE_ARTIFACT_KEYWORD,
        'Cable' => self::FILENAME_CABLE_KEYWORD,
        'Mobile' => self::FILENAME_MOBILE_KEYWORD,
        'Bundle' => self::FILENAME_BUNDLES_KEYWORD,
        'Campaign' => self::FILENAME_CAMPAIGNS_KEYWORD,
        'Reference' => self::FILENAME_REFERENCE_KEYWORD,
        'Salesid' => self::FILENAME_SALESID_KEYWORD,
    ];

    public $_fileToStack = [
        self::FILENAME_CABLE_ARTIFACT_KEYWORD => [
            'Cable_Rules_Plugin.phar'
        ],
        self::FILENAME_MOBILE_KEYWORD => [
            'Mobile_Package_Types.csv',
            'Mobile_Tariff.csv',
            'Mobile_Hardware.csv',
            'Mobile_Footnote.csv',
            'Mobile_Service.csv',
            'Mobile_Categories.csv',
            'Mobile_Device_Categories.csv',
            'Mobile_Comp_Rules.csv',
            'Mobile_Comp_Rules_Article_Constraints.csv',
            'Mobile_Comp_Rules_Footnote.csv',
            'Mobile_Mixmatch_Complete.csv',
            'Mobile_Sales_Rules_PostPaid.csv',
            'Mobile_Sales_Rules_Campaigns.csv',
            'Mobile_Version.xml'
        ],
        self::FILENAME_CABLE_KEYWORD => [
            'Cable_Package_Types.csv',
            'Cable_Products.json',
            'Cable_Categories.csv',
            'Cable_Compatibility.csv',
            'Cable_Version.xml'
        ],
        self::FILENAME_FIXED_KEYWORD => [
            'FN_Multimapper_Hardware.csv',
            'FN_Multimapper_SalesPackage.csv',
            'FN_Multimapper_Options.csv',
            'FN_Multimapper_Promotion.csv',
            'FN_PackageTypes.csv',
            'FN_Hardware.csv',
            'FN_Options.csv',
            'FN_Promotions.csv',
            'FN_Accessory.csv',
            'FN_Categories.csv',
            'FN_Comp_Rules.csv',
            'FN_SalesRules.csv',
            'FN_Version.xml'
        ],
        self::FILENAME_BUNDLES_KEYWORD => [
            'Bundles.xml',
            'Bundle_Categories.csv',
            'Bundle_Version.xml'
        ],
        self::FILENAME_CAMPAIGNS_KEYWORD => [
            'Campaigns.xml',
            'Campaign_Offers.xml',
            'Campaign_Version.xml'
        ],
        self::FILENAME_REFERENCE_KEYWORD => [
            'Reference_OSF_ServiceProvider.csv',
            'Reference_Ship2Store_List.csv',
            'Reference_option_type.csv',
            'Reference_telefonbuch-branchenliste.csv',
            'Reference_Stichwort_list.csv',
            'Reference_activityCodes.xml',
            'Reference_typeSubtypeCombinations.xml',
            'Reference_CommunicationVariables.csv',
        ],
        self::FILENAME_SALESID_KEYWORD => [
            'SalesId_Dealercode_Mapping.xml',
            'SalesId_Version.xml'
        ],
    ];
    /**
     * @todo $supportedFilenames will be replaced by dynamically loaded data from xml
     * @todo replace dependencies on public var with helper
     */
    public $supportedFilenames;

    public function __construct()
    {
        $this->supportedFilenames = Mage::helper('dyna_import/importSupportedFiles')->getSupportedFiles();
    }

    /*
     * Perform import
     */
    public function killImport($information)
    {

        if(empty($information)){
            return;
        }
        $this->_infoExecution = $information ;
        if($this->validateParams()){
            $this->_executeKillImport();
        }
    }
    /*
     * Perform import
     */
    public function performImport($information)
    {

        if(empty($information)){
            return;
        }
        $this->_infoExecution = $information ;
        if($this->validateParams()){
            $this->_executeImport();
        }
    }

    /**
     * Validate types params from execution row
     *
     * @return bool
     */
    private function validateParams(){
        if($types = $this->_infoExecution->getData('type')){
            $types = json_decode($types);
            $types = array_map('strtolower', $types);
            // @todo this should check against $supportedStacks in dyna_abstract and not typestemplate
            if(count(array_intersect($types, array_keys($this->_typesTemplate))) > 0){
                $this->_params = $types;
                return true;
            }
        }
        return false;
    }

    private function _executeKillImport()
    {
        $shellDir = Mage::getBaseDir('base') .DS. 'shell';
        $filePath = $shellDir . DS . 'sandbox_ops.php';
        $phpPath = 'php';
        try {
            if ($importXMLfile = file_get_contents(Mage::getBaseDir('etc').DS.'import.xml')) {
                $import_xml = new Omnius_Core_Model_SimpleDOM($importXMLfile);
                $config = new Varien_Simplexml_Config;
                $config->loadString($import_xml->asXML());
                if ($phpPath = $config->getXpath('php/path')) {
                    $phpPath = (string) $config->getXpath('php/path')[0];
                }
            }
        } catch (Exception $e) {
            Mage::logException($e);
        }
        $command = "nohup " . $phpPath . " " . $filePath . " --stopImport --executionId " . $this->_infoExecution->getId();
        $logFile = Mage::getBaseDir('base') .DS.'var/log/sandbox_kill_import_execution.log';
        $command .= " >" . $logFile . " 2>&1 &";
        @exec($command);
    }

    /**
     *
     */
    private function _executeImport()
    {
        Mage::register('sandbox_import_no_redis_logging', true);
        $finalCommand = 'nohup ' . $this->getFile() . ' -s true -i ' . $this->_infoExecution->getId(). ' -x false ' ;
        foreach ($this->_typesTemplate as $type => $command){
            if(in_array($type,$this->_params)){
                $finalCommand .= $command . ' true ';
            }
        }

        //not waiting for the shell output
        $logFile = Mage::getBaseDir('base') . DS . 'var/log/' . $this->logfilePrefix . $this->logfileSuffix;
        $logFileError = Mage::getBaseDir('base') . DS . 'var/log/' . $this->logfilePrefix . '-error' . $this->logfileSuffix;
        $finalCommand .= '-l "' . $logFile . '" -e "' . $logFileError . '"';
        $finalCommand .= " >" . $logFile . " 2>" . $logFileError . " &";

        @exec($finalCommand);

//        if ($status != 0) {
//            /** @var Dyna_Sandbox_Model_ImportInformation $executionModel */
//            $executionModel =  Mage::getModel('dyna_sandbox/importInformation');
//            $executionModel->load((int)$this->_infoExecution->getId())
//                ->setStatus($executionModel::STATUS_ERROR)
//                ->setFinishedAt(date("Y-m-d H:i:s", Mage::getModel('core/date')->timestamp()))
//                ->save();
//            Mage::unregister('sandbox_import_no_redis_logging');
//        }
    }

    /**
     * Get path of file
     * @return bool|string
     */
    private function getFile()
    {
        $shellDir = Mage::getBaseDir('base') .DS. 'shell';
        $filePath = $shellDir .DS. $this->_shellFile;
        return file_exists($filePath) ? $filePath :false;
    }

    public function getRunningImportPIDs($executionId) {
        //example command: ps -eF | awk '/--executionId 14/ || /-i 14/ {for(i=1;i<=NF;i++)printf "%s,",$i; print ""}'
        //string concatenation necessary because of too many simple and double quotes
        $command = "ps -eF | awk '/--executionId " . $executionId . "/ || /-i " . $executionId . "/ {for(i=1;i<=NF;i++)printf " . '"' . "%s," . '"' . ',$i' . "; print " . '""' . "}' | grep -v 'awk' | grep -v 'ps -eF' ";
        @exec($command, $output, $exitCode);

        return ['output' => $output, 'exitCode' => $exitCode];
    }

    public function setStatusForImportExecution($executionId, $status = Dyna_Sandbox_Model_ImportInformation::STATUS_ERROR) {
        try {
            /** @var Dyna_Sandbox_Model_ImportInformation $executionModel */
            $executionModel =  Mage::getModel('dyna_sandbox/importInformation');
            $finished = date("Y-m-d H:i:s", Mage::getModel('core/date')->timestamp());
            $executionModel->load($executionId);
            if ($status == Dyna_Sandbox_Model_ImportInformation::STATUS_ERROR) {
                $executionModel->setErrors(1);
            }
            if ($status == Dyna_Sandbox_Model_ImportInformation::STATUS_SUCCESS) {
                $executionModel->setErrors(0);
            }
            $executionModel
                ->setStatus($status)
                ->setFinishedAt($finished)
                ->save();
            Mage::unregister('sandbox_import_no_redis_logging');
            return $finished;
        } catch (Exception $e) {
            Mage::logException($e);
            return null;
        }
    }

    public function getStatusForImportExecution($executionId) {
        try {
            /** @var Dyna_Sandbox_Model_ImportInformation $executionModel */
            $executionModel =  Mage::getModel('dyna_sandbox/importInformation');
            $status = $executionModel->load($executionId)->getStatus();
            Mage::unregister('sandbox_import_no_redis_logging');
            return $status;
        } catch (Exception $e) {
            Mage::logException($e);
            return null;
        }
    }

    /**
     *
     */
    public function checkRunningImports()
    {
        $messages = [];
        $periodTimestamp = date('Y-m-d h:i:s',strtotime('-12 hours'));
        /** @var Dyna_Sandbox_Model_ImportInformation $model */
        $model = Mage::getModel('dyna_sandbox/importInformation');
        $runningExecutions = $model->getCollection()
            ->addFieldToFilter('finished_at', array('null' => true))
            ->addFieldToFilter('started_at', array('gteq' => $periodTimestamp))
            ->addFieldToFilter('status', array('eq' => $model::STATUS_RUNNING));

        if(count($runningExecutions) > 0){
            $messages[] = "Following imports are already running:";
            foreach($runningExecutions as $importExection){
                $messages[] = 'A ' . implode(',',json_decode($importExection->getType())) . ' import [ID #' . $importExection->getId() . '] is already running since ' . $importExection->getStartedAt() . '. Current status: ' . $importExection->getStatus();
            }
        } else {
            $messages[] = "No imports are currently running";
        }

        return $messages;
    }

    public function exportProductsXml($path, $filename) {
        // set memory limit
        ini_set('memory_limit', '-1');
        set_time_limit(0);

        $this->connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        // required variables
        $productsXml = $path . $filename;
        $productsGz  = $productsXml.".tmp.gz";

        // get all products collection
        /** @var Mage_Catalog_Model_Resource_Product_Collection $productsCollection */
        $productsCollection = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToSelect(array('entity_id', 'sku'))
            ->setPageSize(100);

        // get no. of pages
        $totalRecords = $productsCollection->getSize();
        $totalPages   = ceil($totalRecords / 100);
        $productsXmlFile = fopen($productsXml, "a");
        fwrite($productsXmlFile, '<?xml version="1.0" encoding="UTF-8"?>\n<products>');

        // parse each page
        for ($currentPage = 1; $currentPage <= $totalPages; $currentPage++) {
            // get current products
            $productsCollection->setCurPage($currentPage);

            // parse products
            foreach ($productsCollection as $product) {
                // create output array
                $productData = array(
                    'attributes'       => $this->getProductAttributes($product),
                    'categories'       => $this->getProductCategories($product),
                    'allowedWith'      => $this->getProductAllowedWith($product),
                    'multi_mappers'    => $this->getProductMultiMappers($product),
                    'non_static_rules' => array(),
                );

                // save data to file
                $xml = Dyna_Sandbox_Helper_Array2XML::createXML('product', $productData);
                $xmlText = $xml->saveXML();
                fwrite($productsXmlFile, str_replace('<?xml version="1.0" encoding="UTF-8"?>', '', $xmlText));
            }
            $productsCollection->clear();
            usleep(100);
        }

        fwrite($productsXmlFile, '</products>');
        fclose($productsXmlFile);
        // compress file
        $fp = gzopen($productsGz, 'w9');
        gzwrite($fp, file_get_contents($productsXml));
        gzclose($fp);

        // delete xml file
        unlink($productsXml);

        return $productsGz;
    }

    /**
     * Get rules allowed with a specific product
     * @param $product
     * @return array
     */
    protected function getProductAllowedWith($product)
    {
        $allowData   = array();

        // get allowed with
        $allowSql = "
          SELECT `pmwi1`.`target_product_id`, `cpe1`.`sku`, `cpev1`.`value` 
          FROM `product_match_whitelist_index` AS pmwi1
          LEFT JOIN `catalog_product_entity` AS cpe1 ON `cpe1`.`entity_id` = `pmwi1`.`target_product_id`
          LEFT JOIN `catalog_product_entity_varchar` AS `cpev1` 
               ON (`cpev1`.`entity_id` = `pmwi1`.`target_product_id`) AND 
                  (`cpev1`.`attribute_id` = (SELECT attribute_id FROM `eav_attribute` ea LEFT JOIN `eav_entity_type` et ON ea.entity_type_id = et.entity_type_id  WHERE `ea`.`attribute_code` = 'name' AND et.entity_type_code = 'catalog_product')) 
            WHERE `pmwi1`.`source_product_id` = :entityId
        
          UNION 
          
          SELECT `pmwi2`.`source_product_id`, `cpe2`.`sku`, `cpev2`.`value` 
          FROM `product_match_whitelist_index` AS pmwi2
          LEFT JOIN `catalog_product_entity` AS cpe2 ON `cpe2`.`entity_id` = `pmwi2`.`source_product_id`
          LEFT JOIN `catalog_product_entity_varchar` AS `cpev2` 
               ON (`cpev2`.`entity_id` = `pmwi2`.`source_product_id`) AND 
                  (`cpev2`.`attribute_id` = (SELECT attribute_id FROM `eav_attribute` ea LEFT JOIN `eav_entity_type` et ON ea.entity_type_id = et.entity_type_id  WHERE `ea`.`attribute_code` = 'name' AND et.entity_type_code = 'catalog_product'))
            WHERE `pmwi2`.`target_product_id` = :entityId";

        $allowResults = $this->connection->fetchAll($allowSql, array(':entityId' => $product->getEntityId()));

        // get defaulted/obligated
        $otherSql = "
        SELECT `pmdi`.`target_product_id`, `pmdi`.`obligated`, `cpe1`.`sku`, `cpev1`.`value` 
        FROM `product_match_defaulted_index` AS pmdi
        LEFT JOIN `catalog_product_entity` AS cpe1 ON `cpe1`.`entity_id` = `pmdi`.`target_product_id`
        LEFT JOIN 
          `catalog_product_entity_varchar` AS `cpev1` 
               ON (`cpev1`.`entity_id` = `pmdi`.`target_product_id`) AND 
                  (`cpev1`.`attribute_id` = (SELECT attribute_id FROM `eav_attribute` ea LEFT JOIN `eav_entity_type` et ON ea.entity_type_id = et.entity_type_id  WHERE `ea`.`attribute_code` = 'name' AND et.entity_type_code = 'catalog_product'))
              WHERE `pmdi`.`source_product_id` = :entityId";
        $otherResults = $this->connection->fetchAll($otherSql, array(':entityId' => $product->getEntityId()));


        foreach ($allowResults as $allowResult) {
            $mixMatchSql     = "SELECT `price`, `subscriber_segment` FROM `dyna_mixmatch_flat` WHERE `source_sku` = :source AND `target_sku` = :target'";
            $mixMatchData    = [];
            $mixMatchResults = $this->connection->fetchAll($mixMatchSql, array(':source' => $product->getSku(), ':target' => $allowResult['sku']));

            foreach ($mixMatchResults as $matchResult) {
                $mixMatchData['mix_match_price'][] = array(
                    '@value'      => "".$matchResult['price'],
                    '@attributes' => array('type' => $matchResult['subscriber_segment'])
                );
            }

            $allowData['product'][] = array(
                '@attributes'      => array('type' => 'allowed'),
                'name'             => $allowResult['value'],
                'sku'              => $allowResult['sku'],
                'mix_match_prices' => $mixMatchData,
            );
        }

        foreach ($otherResults as $otherResult) {
            $mixMatchSql     = "SELECT `price`, `subscriber_segment` FROM `dyna_mixmatch_flat` WHERE `source_sku` = :source AND `target_sku` = :target";
            $mixMatchData    = array();
            $mixMatchResults = $this->connection->fetchAll($mixMatchSql, array(':source' => $product->getSku(), ':target' => $otherResult['sku']));

            foreach ($mixMatchResults as $matchResult) {
                $mixMatchData['mix_match_price'][] = array(
                    '@value'      => "".$matchResult['price'],
                    '@attributes' => ($matchResult['subscriber_segment']) ? array('type' => $matchResult['subscriber_segment']) : []
                );
            }

            $allowData['product'][] = array(
                '@attributes'      => array('type' => (array_key_exists('obligated', $otherResult) && $otherResult['obligated'] == '1') ? 'obligated' : 'defaulted'),
                'name'             => $otherResult['value'],
                'sku'              => $otherResult['sku'],
                'mix_match_prices' => $mixMatchData,
            );
        }

        return $allowData;
    }

    /**
     * Get categories for a specific product
     * @param Dyna_Catalog_Model_Product $product
     * @return array
     */
    public function getProductCategories($product) {

        $pathArray = [];
        $collection1 = $product->getCategoryCollection()
            ->setStoreId(Mage::app()->getStore()->getId())
            ->addAttributeToSelect('path')
            ->addAttributeToSelect('is_active');

        foreach($collection1 as $cat1){
            $pathIds = explode('/', $cat1->getPath());
            $collection = Mage::getModel('catalog/category')->getCollection()
                ->setStoreId(Mage::app()->getStore()->getId())
                ->addAttributeToSelect('name')
                ->addAttributeToSelect('is_active')
                ->addFieldToFilter('entity_id', array('in' => $pathIds));

            $pathByName = '';
            foreach($collection as $cat){
                $catName = $cat->getName();
                $pathByName .= ($pathByName == '' ? '' : "\\") . (in_array($catName, ['Root Catalog', 'Default Category']) ? '' : $catName);
            }

            $pathArray['category'][] = $pathByName;

        }

        return $pathArray;
    }

    /**
     * Get attributes for a specific product
     * @param Dyna_Catalog_Model_Product $product
     * @return array
     */
    protected function getProductAttributes($product)
    {
        $productAttributes = array();
        if (is_null($this->websites)) {
            $this->websites = Mage::app()->getWebsites();
        }
        $productWebsites = $product->getWebsiteIds();
        foreach ($this->websites as $website) {
            foreach ($productWebsites as $i => $id) {
                if ($website->getId() == $id) {
                    $productWebsites[$i] = strtolower($website->getName());
                }
            }
        }

        /** @var Mage_Eav_Model_Attribute $attribute */
        foreach ($product->getAttributes() as $attribute) {
            $attributeValue =  Mage::getResourceModel('catalog/product')->getAttributeRawValue(
                $product->getEntityId(),
                $attribute->getAttributeCode(),
                Mage::app()->getStore()->getStoreId()
            );
            switch ($attribute->getAttributeCode()) {
                case 'attribute_set_id':
                    $model = Mage::getModel("eav/entity_attribute_set")->load($attributeValue);
                    $attributeValue  = $model->getAttributeSetName();
                    break;
                case 'entity_type_id':
                    $model = mage::getModel("eav/entity_type")->load($attributeValue);
                    $attributeValue = $model->getEntityTypeCode();
                    break;
                case 'package_type':
                case 'package_subtype':
                case 'product_visibility':
                    foreach (explode(',', $attributeValue) as $attrVal) {
                        $arrValue = json_decode(Mage::helper('omnius_catalog')->getAttributeAdminLabel($attribute, $attrVal));
                        $values[] = reset($arrValue);
                    }
                    $attributeValue = implode(',', $values);
                    break;
                case 'initial_selectable':
                    $attributeValue = $attributeValue == 1 ? 'Yes' : 'No';
                    break;
                case 'status':
                case 'visibility':
                    $options = Mage::getModel("catalog/product_" . $attribute->getAttributeCode())->getOptionArray();
                    $attributeValue  = $options[$attributeValue];
                    break;
                case 'tax_class_id':
                    $attributeValue = Mage::getModel("tax/class")->load($attributeValue)->getClassName();
                    break;
            }

            if (!$attribute->getIsGlobal()) {
                $productAttributes[$attribute->getAttributeCode()] = [
                    '@attributes' => array('channel' => implode(',', $productWebsites)),
                    '@value'      => $attributeValue
                ];
            } else {
                $productAttributes[$attribute->getAttributeCode()] = $attributeValue;
            }
        }

        return $productAttributes;
    }

    /**
     * Get multi mappers for a specific product
     * @param $product
     * @return array
     */
    protected function getProductMultiMappers($product)
    {
        $multiMapperData = array();

        $mapperSql  = "SELECT * FROM `dyna_multi_mapper` AS dmm WHERE dmm.sku = :sku";
        $mapperData = $this->connection->fetchAll($mapperSql, array(':sku' => $product->getSku()));

        foreach ($mapperData as $mapper) {
            $mapperAddonsSql = "SELECT * FROM `dyna_multi_mapper_addons` AS dmma WHERE dmma.mapper_id = :mapperId";
            $mapperAddons    = $this->connection->fetchAll($mapperAddonsSql, array(':mapperId' => $mapper['entity_id']));

            $addons = array();
            foreach ($mapperAddons as $mapperAddon) {
                $addons['addon'][] = array(
                    'addon_name'       => $mapperAddon['addon_name'],
                    'addon_additional' => $mapperAddon['addon_additional_value'],
                    'addon_value'      => $mapperAddon['addon_value'],
                    'technical_id'     => $mapperAddon['technical_id'],
                    'promo_id'         => $mapperAddon['promo_id'],
                    'backend'          => $mapperAddon['backend'],
                );
            }
            $multiMapperData['multi_mapper'][] = $addons;
        }

        return $multiMapperData;
    }

    public function setLogfilePrefix($prefix)
    {
        $this->logfilePrefix = $prefix;
    }

    public function setLogfileSuffix($suffix)
    {
        $this->logfileSuffix = $suffix;
    }

    /**
     * Retriueve a list of active releases that are not yet processed and have not yet meet deadline
     */
    public function getActiveReleases()
    {
        return Mage::getModel('sandbox/release')->getCollection()
            ->addFieldToFilter('status', Omnius_Sandbox_Model_Release::STATUS_PENDING)
            ->addFieldToFilter('deadline', array('from' => date("Y-m-d H:i:s"), 'date' => true));
    }
}
