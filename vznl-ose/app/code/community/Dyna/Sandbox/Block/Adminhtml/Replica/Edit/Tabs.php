<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Sandbox_Block_Adminhtml_Replica_Edit_Tabs
 */
class Dyna_Sandbox_Block_Adminhtml_Replica_Edit_Tabs extends Omnius_Sandbox_Block_Adminhtml_Replica_Edit_Tabs
{
    /**
     * Override the constructor to customize the tab content
     * Dyna_Sandbox_Block_Adminhtml_Replica_Edit_Tabs constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setTitle(Mage::helper('sandbox')->__('Replica Information'));
    }

    /**
     * Adds a custom tab on the form
     * @return Mage_Core_Block_Abstract
     * @throws Exception
     */
    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
            'label' => Mage::helper('sandbox')->__('Replica Information'),
            'title' => Mage::helper('sandbox')->__('Replica Information'),
            'content' => $this->getLayout()->createBlock('sandbox/adminhtml_replica_edit_tab_form')->toHtml(),
        ));
        return Mage_Adminhtml_Block_Widget_Tabs::_beforeToHtml();
    }
}
