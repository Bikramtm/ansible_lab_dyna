<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Sandbox_Block_Adminhtml_Replica_Edit
 */
class Dyna_Sandbox_Block_Adminhtml_Replica_Edit extends Omnius_Sandbox_Block_Adminhtml_Replica_Edit
{
    /**
     * Override the constructor to customize the edit form
     * Dyna_Sandbox_Block_Adminhtml_Replica_Edit constructor.
     */
    public function __construct()
    {
        Mage_Adminhtml_Block_Widget_Form_Container::__construct();

        $this->_objectId = 'id';
        $this->_blockGroup = 'sandbox';
        $this->_controller = 'adminhtml_replica';
        $this->_updateButton('save', 'label', Mage::helper('sandbox')->__('Save Replica'));
        $this->_updateButton('delete', 'label', Mage::helper('sandbox')->__('Delete Replica'));

        $this->_addButton('saveandcontinue', array(
            'label' => Mage::helper('sandbox')->__('Save And Continue Edit'),
            'onclick' => 'saveAndContinueEdit()',
            'class' => 'save',
        ), -100);

        $this->_formScripts[] = "
							function saveAndContinueEdit(){
								editForm.submit($('edit_form').action+'back/edit/');
							}
						";
    }

    /**
     * Adds a custom header text
     * @return string
     */
    public function getHeaderText()
    {
        if (Mage::registry('replica_data') && Mage::registry('replica_data')->getId()) {
            return Mage::helper('sandbox')->__('Edit Replica "%s"', $this->htmlEscape(Mage::registry('replica_data')->getName()));
        } else {
            return Mage::helper('sandbox')->__('Add Replica');
        }
    }
}
