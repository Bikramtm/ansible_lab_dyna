<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Sandbox_Block_Adminhtml_Release_Edit
 */
class Dyna_Sandbox_Block_Adminhtml_Release_Edit extends Omnius_Sandbox_Block_Adminhtml_Release_Edit
{
    /**
     * Override the constructor to customize the grid
     * Dyna_Sandbox_Block_Adminhtml_Release_Edit constructor.
     */
    public function __construct()
    {
        Mage_Adminhtml_Block_Widget_Form_Container::__construct();

        $this->_objectId = 'id';
        $this->_blockGroup = 'sandbox';
        $this->_controller = 'adminhtml_release';
        $this->_updateButton('save', 'label', Mage::helper('sandbox')->__('Save Release'));
        $this->_updateButton('delete', 'label', Mage::helper('sandbox')->__('Delete Release'));

        $this->_addButton('saveandcontinue', array(
            'label' => Mage::helper('sandbox')->__('Save And Continue Edit'),
            'onclick' => 'saveAndContinueEdit()',
            'class' => 'save',
        ), -100);

        $this->_formScripts[] = "
							function saveAndContinueEdit(){
								editForm.submit($('edit_form').action+'back/edit/');
							}
						";
    }

    /**
     * Adds a custom header on the new/edit form
     * @return string
     */
    public function getHeaderText()
    {
        if (Mage::registry('release_data') && Mage::registry('release_data')->getId()) {
            return Mage::helper('sandbox')->__('Edit Release "%s"', $this->htmlEscape(Mage::registry('release_data')->getId()));
        } else {
            return Mage::helper('sandbox')->__('Add Release');
        }
    }
}
