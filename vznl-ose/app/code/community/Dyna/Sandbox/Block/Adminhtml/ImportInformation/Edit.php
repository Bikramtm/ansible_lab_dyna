<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Sandbox_Block_Adminhtml_ImportInformation_Edit
 */
class Dyna_Sandbox_Block_Adminhtml_ImportInformation_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();

        $this->_objectId = 'id';
        $this->_blockGroup = 'dyna_sandbox';
        $this->_controller = 'adminhtml_importInformation';
        $buttonClass = 'kill';
        if(count(Mage::helper("dyna_sandbox")->checkRunningImports()) == 1){
            $buttonClass .= ' disabled';
        }
        $this->_addButton('Kill', array(
            "id"        => "kill",
            "label"     => $this->__("Kill Import Execution"),
            "onclick"   => "killImportExecution()",
            "class"     => $buttonClass,
        ), -100);
        $this->_removeButton('save');
        //$this->_updateButton('delete', 'label', Mage::helper('sandbox')->__('Delete Import Information'));
        $this->_removeButton('delete'); //@todo make the above work and actually delete import
    }

    /**
     * Adds a custom header on the new/edit form
     * @return string
     */
    public function getHeaderText()
    {
        return Mage::helper('sandbox')->__('Import Details');
    }
}
