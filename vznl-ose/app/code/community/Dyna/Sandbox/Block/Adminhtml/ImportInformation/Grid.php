<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Sandbox_Block_Adminhtml_ImportInformation_Grid
 */
class Dyna_Sandbox_Block_Adminhtml_ImportInformation_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    protected $sandboxList;

    public function __construct()
    {
        parent::__construct();

        $this->setId('importInformationGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);

        //read data from import_summary table and from *_import.log files
        //todo add import_event table to know when an import starts, is running, ends in success or error
    }

    /**
     * Initialize collection
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        /** @var  Dyna_Sandbox_Model_Mysql4_ImportInformation_Collection $collection */
        $collection = Mage::getModel('dyna_sandbox/importInformation')->getCollection();
        /** @var Dyna_Sandbox_Helper_Data $sandboxHelper */
        $sandboxHelper = Mage::helper('dyna_sandbox');

        /** @var Dyna_Sandbox_Model_ImportInformation $importExecution */
        foreach ($collection as $importExecution) {
            $dbStatus = strtolower($importExecution->getStatus());
            if ($dbStatus == 'running') {
                $response = $sandboxHelper->getRunningImportPIDs($importExecution->getId());
                foreach ($response['output'] as $k => $line) {
                    if (strpos($line, 'awk')) {
                        unset($response['output'][$k]);
                    }
                }
                $data = $importExecution->getData();
                $data['files_total'] = 0;
                foreach (json_decode($data['type']) as $type) {
                    foreach ($sandboxHelper->supportedFilenames as $values) {
                        if ($type == $values['stack'] && empty($values['optional'])) {
                            $data['files_total']++;
                        }
                    }
                }

                $importCollection = Mage::getModel('import/summary')
                    ->getCollection()
                    ->addFieldToFilter('execution_id', $data['id']);

                $data['files_skipped'] = 0;
                $data['files_imported'] = 0;
                foreach ($importCollection as $item) {
                    if ($item->getData('headers_validated') == 0 || $item->getData('has_errors') == 1) {
                        $data['files_skipped']++;
                    } else {
                        $data['files_imported']++;
                    }
                }

                if (($response['exitCode'] != 0 || count($response['output']) == 0) && ($data['files_total'] > $data['files_imported'])) {
                    $data['finished_at'] = $sandboxHelper->setStatusForImportExecution($data['id']) ?? $data['finished_at'];
                    /** @var Dyna_Sandbox_Model_ImportInformation $importExecution */
                    $data['status'] = $importExecution::STATUS_ERROR;
                    $data['errors'] = 1;
                } elseif ($data['files_total'] <= $data['files_imported'] && count($response['output']) == 0) {
                    $data['finished_at'] = $sandboxHelper->setStatusForImportExecution($data['id']) ?? $data['finished_at'];
                    $data['status'] = $importExecution::STATUS_SUCCESS;
                    $data['errors'] = 0;
                }

                $importExecution->setData($data);
                $importExecution->save();
            }
        }
        $collection->clear();
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * Override prepareColumns method to show custom columns
     * @return $this
     * @throws Exception
     */
    protected function _prepareColumns()
    {
        $this->addColumn('id', array(
            'header' => Mage::helper('sandbox')->__('ID'),
            'align' => 'right',
            'width' => '50px',
            'type' => 'number',
            'index' => 'id',
        ));

        $this->addColumn('started_at', array(
            'header' => Mage::helper('sandbox')->__('Execution Date'),
            'sortable' => true,
            'index' => 'started_at',
        ));

        $this->addColumn('status', array(
            'header' => Mage::helper('sandbox')->__('Status'),
            'sortable' => true,
            'index' => 'status',
            'renderer' => 'dyna_sandbox/adminhtml_renderer_status',
        ));

        $this->addColumn('errors', array(
            'header' => Mage::helper('sandbox')->__('Errors'),
            'default' => 'N/A',
            'index' => 'errors',
            'type' => 'options',
            'sortable' => true,
            'options' => ['0' => 'No', '1' => 'Yes']
        ));

        $this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

        return parent::_prepareColumns();
    }

    /**
     * Get edit url for a record
     * @param $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    /**
     * Customize mass remove/process actions
     * @return $this
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('ids');
        $this->getMassactionBlock()->setUseSelectAll(true);

        return $this;
    }
}
