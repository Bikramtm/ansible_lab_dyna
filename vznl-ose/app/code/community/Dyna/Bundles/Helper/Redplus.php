<?php

/**
 * Class Dyna_Bundles_Helper_Redplus
 */
class Dyna_Bundles_Helper_Redplus extends Mage_Core_Helper_Abstract
{
    /**
     * Returns the eligible owner packages in cart
     *
     * @return Varien_Data_Collection
     */
    public function getRedPlusEligiblePackages()
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $packages = $quote->getCartPackages();

        $eligiblePackages = new Varien_Data_Collection();

        /** @var Dyna_Bundles_Helper_Data $bundlesHelper */
        $bundlesHelper = Mage::helper('bundles');
        $eligibleBundles = $bundlesHelper->getFrontendEligibleBundles(true, false);

        foreach ($packages as $package) {
            foreach ($eligibleBundles as $bundle) {
                if ($bundle['redPlusBundle']) {
                    foreach ($bundle['targetedPackagesInCart'] as $targetPackage) {
                        if ($targetPackage['packageId'] == $package['package_id']) {
                            $package->setData('eligibleProductSku', $targetPackage['eligibleProductSku']);
                            $package->setData('groupBundleId', $bundle['bundleId']);
                            !$eligiblePackages->getItemById($package->getId()) && $eligiblePackages->addItem($package);
                        }
                    }
                }
            }
        }

        return $eligiblePackages;
    }

    /**
     * Determines whether or not the cart contains any
     * Red+ eligible packages
     *
     * @return bool
     */
    public function cartIsRedPlusEligible()
    {
        return $this->getRedPlusEligiblePackages()->getSize() > 0;
    }

    /**
     * Returns the number of Mobile packages that are
     * eligible for Red+ in the current cart
     *
     * @return int
     */
    public function getNumberOfRedPlusEligiblePackagesInCart()
    {
        return $this->getRedPlusEligiblePackages()->getSize();
    }

    /**
     * Get eligible KIAS bundles
     * @param bool $parentPackage
     * @return array
     */
    public function getEligibleKiasBundles($parentPackage = false)
    {
        $serviceCustomers = Mage::getSingleton('dyna_customer/session');
        $customerProducts = $serviceCustomers->getCustomerProducts();
        $kiasProducts = $customerProducts[Dyna_Superorder_Helper_Client::BACKEND_KIAS];

        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $packages = $quote->getCartPackages();
        $kiasLinkedAccount = $serviceCustomers->getKiasLinkedAccountNumber();

        $maxPackageId = 0;
        $firstPackageMobile = false;
        foreach ($packages as $package) {
            if ($package->isPartOfRedPlus() && $maxPackageId < $package->getPackageId()) {
                $maxPackageId = $package->getPackageId();
            }
            if (strtolower($package->getType()) == strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE) && (!$package->getEditingDisabled()) && !$package->isRedPlus()) {
                // if redplus is a child and linked account has been previously set, display subscriptions just for the selected BAN
                $firstPackageMobile = true;
            }
        }

        /** @var Dyna_Bundles_Helper_Data $bundlesHelper */
        $bundlesHelper = Mage::helper('bundles');
        $eligibleBundles = $bundlesHelper->getFrontendEligibleBundles(true, $firstPackageMobile);

        $eligibleKiasBundles = array();
        $eligibleContracts = array();
        $redPlusBundleId = '';

        foreach ($kiasProducts as $customerId => $product) {
            // Skip bans that are not in sync with the chosen linked accounts
            if (empty($customerId)) {
                continue;
            }

            if ($maxPackageId <= 2 || ($maxPackageId > 2 && $customerId == $kiasLinkedAccount)) {
                $currentKiasProduct = $product;
                foreach ($product['contracts'] as $contractKey => $contract) {
                    if ($parentPackage &&
                        ($contract['subscriptions'][0]['product_id'] == $parentPackage->getServiceLineId())
                    ) {
                        $eligibleContracts[$customerId] = $contract;
                    } else {
                        $this->getEligibleContracts($eligibleBundles, $contract, $eligibleContracts, $contractKey, $redPlusBundleId);
                        if (!isset($eligibleContracts[$contract['customer_number']])
                            || !in_array($contractKey, $eligibleContracts[$contract['customer_number']])
                        ) {
                            unset($currentKiasProduct['contracts'][$contractKey]);
                        }
                    }
                }
                if (!empty($eligibleContracts[$customerId]) && count($eligibleContracts[$customerId]) > 0) {
                    $eligibleKiasBundles[$customerId] = $currentKiasProduct;
                }
            }

        }

        return array(
            'eligibleKiasBundles' => $eligibleKiasBundles,
            'redPlusBundleId' => $redPlusBundleId,
            'allKiasProducts' => $kiasProducts
        );
    }

    /**
     * Return all BANS to which Red+ members can be added
     * @param $eligibleRedPlusBundles
     * @return array
     */
    public function getValidContracts(&$eligibleRedPlusBundles)
    {
        $bans = array();
        foreach ($eligibleRedPlusBundles as &$bundle) {
            foreach ($bundle['targetedSubscriptions'] as $subscription) {
                $bans[] = $subscription['customerNumber'];
            }
        }

        return array_unique($bans);
    }

    /**
     * Get eligible contracts from Kias accounts
     * @param $eligibleBundles
     * @param $contract
     * @param $eligibleContracts
     * @param $contractKey
     */
    protected function getEligibleContracts($eligibleBundles, $contract, &$eligibleContracts, $contractKey, &$redPlusBundleId)
    {
        foreach($eligibleBundles as $bundle){
            if ($bundle['redPlusBundle'] && count($bundle['targetedSubscriptions'])) {
                $redPlusBundleId = $bundle['bundleId'];
                foreach($bundle['targetedSubscriptions'] as $subscription){
                    if(($contract['subscriptions'][0]['customer_number'] == $subscription['customerNumber'])
                        && ($contract['subscriptions'][0]['product_id'] == $subscription['productId'])
                        && ($contract['subscriptions'][0]['product_status'] != Dyna_Customer_Helper_Customer::MOBILE_CONTRACT_PRODUCT_STATUS_R)){
                        $eligibleContracts[$contract['customer_number']] = array();
                        array_push($eligibleContracts[$contract['customer_number']],$contractKey);
                    }
                }
            }
        }
    }

    /**
     * @param Dyna_Package_Model_Package $package
     * @param $installBaseSubscriptionNumber
     * @param string $processContext
     * @param string $productId
     * @return Dyna_Package_Model_Package
     */
    public function convertToMember(Dyna_Package_Model_Package $package, $installBaseSubscriptionNumber, $processContext = Dyna_Catalog_Model_ProcessContext::MIGCOC, $productId = null)
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();

        // try to get the first eligible subscription in installed base
        /** @var Dyna_Bundles_Helper_Data $bundlesHelper */
        $bundlesHelper = Mage::helper('dyna_bundles');
        /** @var Dyna_Configurator_Helper_Cart $cartHelper */
        $cartHelper = Mage::helper('dyna_configurator/cart');

        /** @var Dyna_Customer_Model_Session $customerSession */
        $customerSession = Mage::getSingleton('dyna_customer/session');

        $eligibleBundles = $bundlesHelper->getFrontendEligibleBundles(true);
        $bundlesHelper->filterBundlesBySubscriptionStatus($eligibleBundles);

        foreach ($eligibleBundles as $bundle) {
            if ($bundle['redPlusBundle'] && !empty($bundle['targetedPackagesInCart'])) {
                $redPlusEligiblePackages = $this->getRedPlusEligiblePackages();

                if ($redPlusEligiblePackages->getSize() > 0) {
                    $parentPackage = $redPlusEligiblePackages->getFirstItem();
                    $package->setParentId($parentPackage->getId());

                    $bundlesHelper->addBundlePackageEntry($bundle['bundleId'], $package->getId());
                    $bundlesHelper->addBundlePackageEntry($bundle['bundleId'], $parentPackage->getId());
                }

                break 1;
            } elseif ($bundle['redPlusBundle'] && !empty($bundle['targetedSubscriptions'])) {
                foreach ($bundle['targetedSubscriptions'] as $subscription) {
                    $serviceLineId = !empty($subscription['service_line_id']) ? $subscription['service_line_id'] : false;
                    if($processContext !== Dyna_Catalog_Model_ProcessContext::MIGCOC
                        && ($subscription['customerNumber'] != $installBaseSubscriptionNumber
                            || ($package->getServiceLineId() == $serviceLineId)
                            || ($productId == $subscription['productId']))
                    ){
                        continue;
                    }

                    $parentPackage = $quote->getGhostPackageForIBSubscription($subscription['customerNumber'], $subscription['productId']);

                    if ($parentPackage) {
                        $bundlesHelper->addBundlePackageEntry($bundle['bundleId'], $package->getId());
                        $package->setParentId($parentPackage->getId());
                        $package->setParentAccountNumber($parentPackage->getParentAccountNumber());
                    } else {
                        $packageCreationTypeId = $cartHelper->getCreationTypeId(Dyna_Catalog_Model_Type::TYPE_MOBILE);
                        // create new dummy package
                        $parentPackageId = $cartHelper->initNewPackage(
                            Dyna_Catalog_Model_Type::TYPE_MOBILE,
                            $processContext,
                            null,
                            null,
                            null,
                            null,
                            null,
                            null,
                            $packageCreationTypeId
                        );

                        $parentPackage = $quote->getCartPackage($parentPackageId);
                        $parentPackage
                            ->setEditingDisabled(true)
                            ->setRedplusRelatedProduct($subscription['relatedProductSku'])
                            ->setInitialInstalledBaseProducts($subscription['relatedProductSku'])
                            ->setInstalledBaseProducts($subscription['relatedProductSku'])
                            ->setServiceLineId($subscription['productId'])
                            ->setParentAccountNumber($subscription['customerNumber'])
                            ->setSharingGroupId($subscription['sharingGroupId'])
                            ->save();

                        $package
                            ->setParentId($parentPackage->getId())
                            ->setParentAccountNumber($customerSession->getKiasLinkedAccountNumber() ?? $subscription['customerNumber']);

                        $bundlesHelper->addBundlePackageEntry($bundle['bundleId'], $parentPackage->getId());
                        $bundlesHelper->addBundlePackageEntry($bundle['bundleId'], $package->getId());

                        $package->updateBundleTypes();
                        $parentPackage->updateBundleTypes();

                        if(!$customerSession->getKiasLinkedAccountNumber()){
                            $customerSession->setKiasLinkedAccountNumber($subscription['customerNumber']);
                        }
                    }
                    break 2;
                }
            }
        }

        return $package;
    }

    /**
     * Get subscription from cart of corresponding install base package
     * @param $customerId
     * @param $productId
     * @return false / subscription item
     */
    public function getInstallBasePackageTariff($customerId,$productId)
    {
        $cartPackageTariff = false;

        $cartPackage = Mage::getSingleton('checkout/cart')->getQuote()->getInstallBasePackage($customerId,$productId,true);
        if ($cartPackage) {
            foreach ($cartPackage->getAllItems() as $item) {
                if ($item->getProduct()->isSubscription()) {
                    $cartPackageTariff = $item;
                }
            }
        }

        return $cartPackageTariff;
    }
}
