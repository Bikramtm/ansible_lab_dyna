<?php

declare(strict_types = 1);

/**
 * Restricts logging to a specific log level
 * Class Dyna_Bundles_Model_Service_Logger
 */
class Dyna_Bundles_Model_Service_Logger
{

    protected $debug = false;
    /**
     * @var mixed
     */
    protected $logFile;

    /**
     * Dyna_Bundles_Model_Service_Log constructor.
     * @param $args
     * @internal $logLevel
     * @internal $logFile
     */
    public function __construct(array $args)
    {
        if(isset($args['logFile'])) {
            $this->logFile = $args['logFile'];
        }
    }

    /**
     * @param $message
     * @param null $level
     * @param string $file
     * @param bool $forceLog
     */
    public function log($message, $level = null, $file = '', $forceLog = false)
    {
        if ($this->debug) {
            if (empty($file)) {
                $file = $this->logFile;
            }

            Mage::log($message, $level, $file, $forceLog);
        }
    }
}