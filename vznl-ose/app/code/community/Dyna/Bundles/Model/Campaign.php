<?php

/**
 * Class Dyna_Bundles_Model_Campaign
 */
class Dyna_Bundles_Model_Campaign extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        $this->_init('dyna_bundles/campaign');
    }

    /**
     * Get Campaign Offers
     * @return array
     */
    public function getOffers($page = 1, $pageSize = 10) {
        $campaignId = $this->getData('entity_id');
        $campaignOfferIds = Mage::getModel('dyna_bundles/campaignOfferRelation')
                                ->getCollection()
                                ->addFieldToFilter('campaign_id', array('eq' => $campaignId))
                                ->setOrder('entity_id', 'asc')
                                ->getColumnValues('offer_id');
        $campaignOfferIds = array_unique($campaignOfferIds);
        $totalCampaignOffers = count($campaignOfferIds);
        /** @var Dyna_Bundles_Model_Mysql4_CampaignOffer_Collection $campaignOffersUnsorted */
        $campaignOffersUnsorted = Mage::getModel('dyna_bundles/campaignOffer')
                                ->getCollection()
                                ->setPageSize($pageSize)
                                ->setCurPage($page)
                                ->addFieldToFilter('entity_id', array('in' => $campaignOfferIds));

        $campaignOfferIds = array_flip($campaignOfferIds);
        foreach ($campaignOffersUnsorted as $key => $campaignOffer) {
            $sortedPosition = $campaignOfferIds[$campaignOffer->getId()];
            $campaignOffers[$sortedPosition] = $campaignOffer;
        }
        ksort($campaignOffers);

        return ['campaignOffers' => $campaignOffers ? $campaignOffers : false, 'totalCampaignOffers' => $totalCampaignOffers];
    }


    /**
     * Return campaign entity_id based on campaign code
     * @param $campaignCode
     * @return int
     */
    public static function getIdByCampaignCode($campaignCode)
    {
        /** @var Dyna_Bundles_Model_Mysql4_Campaign_Collection $collection */
        return (int)(new self())
            ->getCollection()
            ->addFieldToFilter('campaign_id', array('eq' => $campaignCode))
            ->getSelect()
            ->reset(Varien_Db_Select::COLUMNS)
            ->columns('entity_id')
            ->query()
            ->fetchColumn();
    }

    /**
     * Determine whether or not current campaign is valid
     * If no valid_to value is specified, campaign is considered to be active
     * @return bool
     */
    public function isValid()
    {
        if ($this->getValidTo() && strtotime($this->getValidTo()) < strtotime(date("Y-m-d"))) {
            return false;
        }

        return true;
    }


    /**
     * @return mixed
     */
    public function getAllOffers()
    {
        $campaignId = $this->getId();

        $campaignOfferRelations = Mage::getModel('dyna_bundles/campaignOfferRelation')
            ->getCollection()
            ->addFieldToFilter('campaign_id', array('eq' => $campaignId))
            ->setOrder('entity_id', 'asc')
            ->getColumnValues('offer_id');

        $campaignOfferRelations = array_unique($campaignOfferRelations);
        $campaignOffersUnsorted = Mage::getModel('dyna_bundles/campaignOffer')
            ->getCollection()
            ->addFieldToFilter('entity_id', array('in' => $campaignOfferRelations));

        $campaignOfferIds = array_flip($campaignOfferRelations);
        foreach ($campaignOffersUnsorted as $key => $campaignOffer) {
            $sortedPosition = $campaignOfferIds[$campaignOffer->getId()];
            $campaignOffers[$sortedPosition] = $campaignOffer;
        }
        ksort($campaignOffers);

        return $campaignOffers;
    }
}
