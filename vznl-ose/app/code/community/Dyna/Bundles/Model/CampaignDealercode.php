<?php

/**
 * Class Dyna_Bundles_Model_CampaignDealercode
 * @method getPrimaryDealerCode()
 * @method getSecondaryDealerCode()
 */
class Dyna_Bundles_Model_CampaignDealercode extends Mage_Core_Model_Abstract
{
    const DEFAULT_CLUSTER_CATEGORY = 'Andere';

    public function _construct()
    {
        $this->_init('dyna_bundles/campaignDealercode');
    }
}
