<?php

/**
 * Class Dyna_Bundles_Model_Hint
 *
 * @method getLocation()
 * @method getTitle()
 * @method getText()
 * @method getHintEnabled()
 */
class Dyna_Bundles_Model_BundleHint extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        $this->_init('dyna_bundles/bundleHint');
    }
}
