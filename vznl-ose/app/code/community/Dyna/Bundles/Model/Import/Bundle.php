<?php

/**
 * Class Dyna_Bundles_Model_Import_Campaign
 */
class Dyna_Bundles_Model_Import_Bundle extends Dyna_Bundles_Model_Import_Abstract
{
    /** @var string $_logFileName */
    protected $_logFileName = "bundle_bundle_rule_import";

    const ALL_CONTEXTS = "*";

    /**
     * Import campaign
     *
     * @param array $data
     *
     * @return void
     */
    public function import($data)
    {
        $skip = false;
        //for logging purposes
        $this->_totalFileRows++;

        $data = $this->_setDataMapping($data);

        $mandatoryAttributes = [
          'bundle_rule_name',
          'conditions',
          'website_id',
          'processContext',
        ];
        foreach ($mandatoryAttributes as $attribute) {
            if (!isset($data[$attribute])) {
                $this->_logError('Skipping line without node: ' . $attribute);
                $skip = true;
            }
        }

        if ($skip) {
            $this->_skippedFileRows++;
            return;
        }

        $this->_log('Start importing ' . $data['bundle_rule_name'], false);

        try {
            /** @var Dyna_Bundles_Model_BundleRule $bundleRule */
            $bundleRule = Mage::getModel('dyna_bundles/bundleRule');
            $existingBundleRule = $bundleRule->load($data['bundle_rule_name'], 'bundle_rule_name');

            if ($existingBundleRule->getId()) {
                $bundleRule = $existingBundleRule;
                $this->_log('Loading existing bundle rule "' . $data['bundle_rule_name'] . '" with ID ' . $bundleRule->getId(), false);
            }

            $params = [
              'bundle_rule_name',
              'bundle_gui_name',
              'description',
              'bundle_type',
              'ogw_orchestration_type',
              'website_id',
            ];
            // all parameters are default null
            $defaultParams = array_combine($params, array_fill(0, count($params), null));
            $defaultParams[5] = 1; // default website_id: 1
            $this->_setDataWithDefaults($bundleRule, $data, $defaultParams);

            if (!$this->getDebug()) {
                $bundleRule->save();
            }

            if (!$this->getDebug()) {
                $this->_addBundleActions($bundleRule, $data['actions'] ?? []);
                $this->_addBundleConditions($bundleRule, $data['conditions']);
                $this->_addBundleHints($bundleRule, $data['hints']);
                $this->_addBundleProcessContexts($bundleRule, $data['processContext']);
            }

            unset($bundleRule);
            $this->_qties++;

            $this->_log('Finished importing ' . $data['bundle_rule_name'], false);
        } catch (Exception $ex) {
            $this->_logError(print_r($ex,true));
            $this->_logEx($ex);
            fwrite(STDERR, $ex->getMessage());
        }
    }

    /**
     * Add bundle conditions to current bundle rule
     * @param Dyna_Bundles_Model_BundleRule $bundleRule
     * @param array $bundleConditions
     * @return $this
     */
    protected function _addBundleConditions($bundleRule, $bundleConditions)
    {
        // make sure condition is a non-associative array
        if (!is_array($bundleConditions['condition'])) {
            $bundleConditions['condition'] = array($bundleConditions['condition']);
        }

        foreach ($bundleConditions['condition'] as $bundleCondition) {
            if (empty($bundleCondition)) {
                continue;
            }

            Mage::getModel('dyna_bundles/bundleCondition')
                ->setData('bundle_rule_id', $bundleRule->getId())
                ->setData('condition', $bundleCondition)
                ->save();
        }

        return $this;
    }

    /**
     * Add relation to offers
     *
     * @param Dyna_Bundles_Model_BundleRule $bundleRule
     * @param array $bundleActions
     *
     */
    protected function _addBundleActions($bundleRule, $bundleActions)
    {
        if (empty($bundleActions)) {
            return false;
        }
        // make sure bundleActions is a non-associative array
        if (!isset($bundleActions['entry'][0])) {
            $bundleActions['entry'] = array($bundleActions['entry']);
        }

        $defaultParams = [
            'bundle_rule_id' => null,
            'priority' => 1,
            'scope'    => 'quote',
            'condition'=> null,
            'action'   => null,
            'package_creation_type_id' => null,
        ];

        foreach ($bundleActions['entry'] as $action) {
            $action = (array) $action;
            $action['condition'] = $this->getXmlValue($action, "condition");
            $action['package_creation_type_id'] = $this->getXmlValue($action, "packagecreationtypeid");
            $bundleAction = Mage::getModel('dyna_bundles/bundleAction');
            $action['bundle_rule_id'] = $bundleRule->getId();
            $this->_setDataWithDefaults($bundleAction, $action, $defaultParams);
            $bundleAction->save();
        }
    }

    /**
     * Add relation between bundle rule and process contexts
     *
     * @param $bundleRule
     * @param $processContext
     */
    protected function _addBundleProcessContexts($bundleRule, $processContext)
    {
        if (isset($processContext) && !empty($processContext)) {
            if ($processContext == self::ALL_CONTEXTS) {
                // retrieve all existing process contexts and link them to the bundle
                $contexts = Mage::getModel('dyna_catalog/processContext')
                    ->getCollection();

                foreach ($contexts as $context) {
                    $bundleProcessContextLink = Mage::getModel('dyna_bundles/bundleProcessContext');

                    $bundleProcessContextLink
                        ->setBundleRuleId($bundleRule->getId())
                        ->setProcessContextId($context->getId());

                    $bundleProcessContextLink->save();
                }
            } else {
                $contexts = array_map('trim', explode(',', $processContext));

                foreach ($contexts as $context) {
                    $contextId = Mage::getModel('dyna_catalog/processContext')
                        ->getProcessContextIdByCode($context);

                    if ($contextId) {
                        $bundleProcessContextLink = Mage::getModel('dyna_bundles/bundleProcessContext');
                        $bundleProcessContextLink
                            ->setBundleRuleId($bundleRule->getId())
                            ->setProcessContextId($contextId);

                        $bundleProcessContextLink->save();
                    } else {
                        Mage::throwException(sprintf('Process context with code %s does not exist.', $context));
                    }
                }
            }
        } else {
            Mage::throwException("No process context specified for bundle rule");
        }
    }

    /**
     * @param Dyna_Bundles_Model_BundleRule $bundleRule
     * @param $bundleHints
     */
    protected function _addBundleHints($bundleRule, $bundleHints)
    {
        if (!isset($bundleHints['hint'][0])) {
            $bundleHints['hint'] = array($bundleHints['hint']);
        }

        $defaultParams = [
            'bundle_rule_id' => null,
            'hint_enabled' => 0,
            'title' => null,
            'text' => null,
            'location' => null
        ];

        foreach ($bundleHints['hint'] as $hint) {
            $hint = (array) $hint;
            $hint['hint_enabled'] = in_array($this->getXmlValue($hint, "enabled"), ["true", "yes"]) ? 1 : 0;
            $hint['title'] = $this->getXmlValue($hint, "title");
            $hint['text'] = $this->getXmlValue($hint, "text");
            $hint['location'] = $this->getXmlValue($hint, "location");
            $hint['bundle_rule_id'] = $bundleRule->getId();

            $bundleHint = Mage::getModel('dyna_bundles/bundleHint');
            $this->_setDataWithDefaults($bundleHint, $hint, $defaultParams);

            $bundleHint->save();
        }
    }

    /**
     * Process custom data mapping for bundles
     *
     * @param array $data
     *
     * @return array
     */
    protected function _setDataMapping($data)
    {
        if (is_object($data)) {
            $data = (array) $data;
        }
        $channel = strtolower($data['channel']);
        $websites = Mage::app()->getWebsites(false, true);
        if (isset($websites[$channel])) {
            $data['website_id'] = $websites[strtolower($data['channel'])]->getId();
        }
        unset($data['channel']);

        if (isset($data['OGWOrchestrationType'])) {
            $data['ogw_orchestration_type'] = $this->getXmlValue($data, "OGWOrchestrationType");
            unset($data['OGWOrchestrationType']);
        }

        unset ($data['hint']);

        return $data;
    }

    /**
     * Get a string value from a node (node is either array because of closing tag or string)
     * @param $node
     */
    public function getXmlValue($array, $node)
    {
        return isset($array[$node]) && !is_array($array[$node]) ? $array[$node] : null;
    }

    public function clearBundleActionsTable()
    {
        $resource = Mage::getSingleton('core/resource');
        $conn = $resource->getConnection('core_write');
        $bundleActionsTable = $resource->getTableName('dyna_bundles/bundle_actions');

        $conn->query(sprintf('TRUNCATE TABLE %s', $bundleActionsTable));

        return $this;
    }

    public function clearBundleConditionsTable()
    {
        $resource = Mage::getSingleton('core/resource');
        $conn = $resource->getConnection('core_write');
        $bundleConditionsTable = $resource->getTableName('dyna_bundles/bundle_conditions');

        $conn->query(sprintf('TRUNCATE TABLE %s', $bundleConditionsTable));

        return $this;
    }

    public function clearBundleHintsTable()
    {
        $resource = Mage::getSingleton('core/resource');
        $conn = $resource->getConnection('core_write');
        $bundleHintsTable = $resource->getTableName('dyna_bundles/bundle_hints');

        $conn->query(sprintf('TRUNCATE TABLE %s', $bundleHintsTable));

        return $this;
    }
}
