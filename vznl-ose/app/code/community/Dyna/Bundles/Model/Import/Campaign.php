<?php

/**
 * Class Dyna_Bundles_Model_Import_Campaign
 */
class Dyna_Bundles_Model_Import_Campaign extends Dyna_Bundles_Model_Import_Abstract
{
    /** @var string $_logFileName */
    protected $_logFileName = "bundle_campaign_import";

    /**
     * Import campaign
     *
     * @param array $data
     *
     * @return void
     */
    public function import($data)
    {
        $skip = false;
        //for logging purposes
        $this->_totalFileRows++;

        $data = $this->_setDataMapping($data);

        $mandatoryAttributes = ['campaign_id'];
        foreach ($mandatoryAttributes as $attribute) {
            if (!isset($data[$attribute])) {
                $this->_logError('Skipping line without ' . $attribute);
                $skip = true;
            }
        }

        if ($skip) {
            $this->_skippedFileRows++;
            return;
        }

        $this->_log('Start importing ' . $data['campaign_id'], false);

        try {
            /** @var Dyna_Bundles_Model_Campaign $campaign */
            $campaign = Mage::getModel('dyna_bundles/campaign');
            $existingCampaign = $campaign->load($data['campaign_id'], 'campaign_id');

            if ($existingCampaign->getEntityId()) {
                $campaign = $existingCampaign;
                $this->_log('Loading existing campaign "' . $data['campaign_id'] . '" with ID ' . $campaign->getEntityId(), false);
            }

            $data = $this->_filterData($data);
            $data = $this->_promotionhints($data);
            $params = [
              'campaign_id',
              'campaign_title',
              'cluster',
              'cluster_category',
              'advertising_code',
              'campaign_code',
              'marketing_code',
              'campaign_hint',
              'offer_hint',
              'promotion_hint1',
              'promotion_hint2',
              'promotion_hint3',
              'valid_from',
              'valid_to',
              'default',
            ];
            // all parameters are default null
            $defaultParams = array_combine($params, array_fill(0, count($params), null));

            $this->_setDataWithDefaults($campaign, $data, $defaultParams);

            if (!$this->getDebug()) {
                $campaign->save();
            }

            if (!$this->getDebug()) {
                $this->_setOffers($campaign, $data);
            }

            unset($campaign);
            $this->_qties++;

            $this->_log('Finished importing ' . $data['campaign_id'], false);
        } catch (Exception $ex) {
            echo $ex->getMessage();
            $this->_logEx($ex);
            fwrite(STDERR, $ex->getMessage());
        }
    }

    /**
     * Filter and process invalid data types
     *
     * @param array $data
     *
     * @return array
     */
    protected function _filterData($data)
    {
        $data['valid_from'] = (empty($data['valid_from']) ? $this->formatDate('0') : $this->formatDate($data['valid_from']));
        $data['valid_to'] = (empty($data['valid_to']) ? null : $this->formatDate($data['valid_to']));

        return $data;
    }

    /**
     * promotionhints process the processhints array and convert to required fields to insert into database
     *
     * @param array $data
     *
     * @return array
     */
    protected function _promotionhints($data)
    {
        if(isset($data["promotion_hint"]))
        {
          if(is_array($data["promotion_hint"]))
          {
            if(isset($data["promotion_hint"]['0']))
            { $data["promotion_hint1"]=$data["promotion_hint"]['0']; }
            if(isset($data["promotion_hint"]['1']))
            { $data["promotion_hint2"]=$data["promotion_hint"]['1']; }
            if(isset($data["promotion_hint"]['2']))
            { $data["promotion_hint3"]=$data["promotion_hint"]['2']; }
          }
          else
          {
            $data["promotion_hint1"]=$data["promotion_hint"];
          }

          unset($data["promotion_hint"]);
        }
        return $data;
    }

    /**
     * Add relation to offers
     *
     * @param Dyna_Bundles_Model_Campaign $campaign
     * @param array $data
     */
    protected function _setOffers($campaign, $data)
    {
        if (!empty($data['offer_ids'])) {
            /** @var Dyna_Bundles_Model_CampaignOffer $offerModel */
            foreach (explode(',', $data['offer_ids']) as $offerId) {
                $offerModel = Mage::getModel('dyna_bundles/campaignOffer');
                $offerId = trim($offerId);
                $offer = $offerModel->load($offerId, 'offer_id');
                if ($offer->getEntityId()) {
                    $offerModel = $offer;
                } else {
                    $offerModel->setData([
                        'offer_id' => $offerId
                    ]);
                    $offerModel->save();
                }

                $query = "INSERT INTO `bundle_campaign_offer_relation` (`campaign_id`, `offer_id`) VALUES (" . $campaign->getEntityId() . ", " . $offerModel->getEntityId() . ") 
                ON DUPLICATE KEY UPDATE `campaign_id` = `campaign_id`";
                Mage::getSingleton('core/resource')->getConnection('core_write')->exec($query);
            }
        }
    }
}
