<?php
abstract class Dyna_Bundles_Model_Expression_AbstractScope implements Dyna_Bundles_Model_Expression_ScopeInterface
{
    /**
     * @var string[]
     */
    protected $chooseFromCategories = [];

    public function chooseFromCategory($category, $default = null)
    {
        if (!($category instanceof Mage_Catalog_Model_Category)) {
            $category = $this->getCategoryIdFromPath($category);
        } else {
            // keep only the id from this category
            $category = $category->getId();
        }

        if ($category && !in_array($category, $this->chooseFromCategories)) {
            $this->chooseFromCategories[$this->getId()][] = $category;

            if ($default) {
                $this->add($default);
            }
        }

        return true;
    }

    public function chooseFromCategoryId($categoryId, $default = null)
    {
        if (($category = $this->getCategoryIdByUniqueId($categoryId)) && $category->getId()) {
            return $this->chooseFromCategory($category, $default);
        }

        return true;
    }

    public function getChooseFromFamiliesIds()
    {
        return $this->chooseFromCategories;
    }


    public function getCategoryIdFromPath($path)
    {
        /** @var Dyna_Catalog_Model_Category $categoryModel */
        $categoryModel = Mage::getModel("catalog/category");
        $currentCategory = $categoryModel->getCategoryByNamePath($path);

        if ($currentCategory) {
            return $currentCategory->getId();
        }

        return null;
    }

    /**
     * Return the category instance by a certain unique id
     * @param $id
     * @return Dyna_Catalog_Model_Category
     */
    public function getCategoryIdByUniqueId($id)
    {
        return Mage::getModel("catalog/category")->getCategoryByUniqueId($id);
    }
}
