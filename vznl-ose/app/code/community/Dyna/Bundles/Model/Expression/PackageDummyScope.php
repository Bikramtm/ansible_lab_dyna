<?php

class Dyna_Bundles_Model_Expression_PackageDummyScope extends Dyna_Bundles_Model_Expression_PackageScope
{
    /**
     * @var Dyna_Package_Model_Package
     */
    protected $package = null;
    /**
     * @var
     */
    protected $packageItems;
    /**
     * @var mixed
     */
    protected $categories;
    protected $chooseCategory;

    public $bundleProductsSummary;

    public function delete(...$skusToDelete)
    {
        if (!$this->package) {
            return false;
        }
        if (Mage::registry('bundleProductsSummary')) {
            $this->bundleProductsSummary = Mage::registry('bundleProductsSummary');
            Mage::unregister('bundleProductsSummary');
        }
        //todo remove if everything is ok
        $this->quote = $this->getQuote();
        $this->quoteItems = $this->quote->getAllItems();

        $productsArray = array_flip($this->packageItems);
        foreach ($skusToDelete as $sku) {
            if (in_array($sku, $productsArray)) {
                $productData = $this->getProductData($sku);
                $this->packageItems[] = $sku;

                $bundleProduct = $productData;
                unset($bundleProduct['packageType']);
                $bundleProduct['type'] = 'remove';
                $this->bundleProductsSummary[$productData['packageType']][] = $bundleProduct;

                unset($this->packageItems[$productsArray[$sku]]);
            }
        }
        Mage::register('bundleProductsSummary', $this->bundleProductsSummary);

        return true;
    }

    public function add(...$skus)
    {
        if (!$this->package) {
            return false;
        }

        if (Mage::registry('bundleProductsSummary')) {
            $this->bundleProductsSummary = Mage::registry('bundleProductsSummary');
            Mage::unregister('bundleProductsSummary');
        }

        foreach ($skus as $sku) {
            if (!in_array($sku, $this->packageItems)) {
                $productData = $this->getProductData($sku);

                $bundleProduct = $productData;
                unset($bundleProduct['packageType']);
                $bundleProduct['type'] = 'add';
                if($this->chooseCategory){
                    $bundleProduct['choose'] = 1;
                    $this->chooseCategory = null;
                }
                $this->bundleProductsSummary[$productData['packageType']][] = $bundleProduct;

                $this->packageItems[] = $sku;
            }
        }

        Mage::register('bundleProductsSummary', $this->bundleProductsSummary);
        return true;
    }

    public function addPromoProduct($skus, $target)
    {
        return $this->add($skus);
    }

    /**
     * @return bool
     */
    public function onParsingDone(): bool
    {
        if (!$this->package) {
            return false;
        }

        return true;
    }

    public function getId()
    {
        if (!$this->package) {
            return false;
        }

        return $this->package->getPackageId();
    }

    private function getProductData($sku)
    {
        $product = Mage::getModel("catalog/product")->loadByAttribute('sku', $sku);
        if ($product) {
            return [
                'packageType' => $product->getPackageType(),
                'name' => $product->getDisplayNameConfigurator() ?: $product->getName(),
                'productId' => (int)$product->getId(),
                'maf' => Mage::helper('tax')->getPrice($product, $product->getMaf(), false),
                'promoDiscount' => $product->getMafDiscount() ?? 0,
                'discountFormatted' => $product->getMafDiscount() ? "-" . Mage::app()->getStore()->formatPrice($product->getMafDiscount(), 1) : null
            ];
        }
        return [];
    }

    public function chooseFromCategory($category, $default = null)
    {
        if (!($category instanceof Dyna_Catalog_Model_Category)) {
            $category = $this->getCategoryIdFromPath($category);
        } else {
            // keep only category id from category instance
            $category = $category->getId();
        }

        if ($category && !in_array($category, $this->chooseFromCategories)) {
            if ($default) {
                $this->chooseCategory = $category;
                if ($item = $this->add($default)) {
                    $this->chooseFromCategories[$this->package->getEntityId()][$this->package->getEntityId()] = $category;
                }
            }
        }

        return true;
    }

    public function chooseFromCategoryId($categoryId, $default = null)
    {
        if (($category = $this->getCategoryIdByUniqueId($categoryId)) && $category->getId()) {
            return $this->chooseFromCategory($category, $default);
        }

        return true;
    }

    public function containsProductInCategoryId($categoryId)
    {
        return true;
    }
}
