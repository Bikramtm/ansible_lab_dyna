<?php

class Dyna_Bundles_Model_Expression_Parser
{
    /** @var Dyna_Bundles_Model_BundleRule */
    protected $bundle = null;
    /** @var Dyna_Bundles_Model_Service_Logger */
    protected $priorityLogger;
    /** @var Dyna_Bundles_Model_Expression_ScopeInterface[] */
    protected $scopes;
    protected $newActivePackage = null;
    /** @var Dyna_Package_Model_PackageCreationTypes */
    protected $packageCreationType = false;

    /**
     * Families to choose from, after parsing the bundle rules
     * scope.chooseFromCategory rule
     * @var array
     */
    protected $chooseFromFamilies = [];

    /**
     * @var $dynaCoreHelper Dyna_Core_Helper_Data
     */
    private $dynaCoreHelper;

    /**
     * Dyna_Bundles_Model_Expression_Parser constructor.
     * @param $args
     * @internal param $scopes
     * @internal param bundleActions
     * @internal param logger
     */
    public function __construct(array $args)
    {
        if (isset($args['scopes'])) {
            $this->scopes = $args['scopes'];
        } else {
            throw new InvalidArgumentException("scopes parameter missing");
        }

        if (isset($args['bundle']) && $args['bundle'] instanceof Dyna_Bundles_Model_BundleRule) {
            $this->bundle = $args['bundle'];
        } else {
            throw new InvalidArgumentException("bundleActions parameter missing or invalid");
        }

        if (isset($args['logger']) && $args['logger'] instanceof Dyna_Bundles_Model_Service_Logger) {
            $this->priorityLogger = $args['logger'];
        } else {
            throw new InvalidArgumentException("logger parameter missing or invalid");
        }

        $this->dynaCoreHelper = Mage::helper('dyna_core');
    }

    /**
     * @return Dyna_Bundles_Model_Expression_ParserResponse|false|Mage_Core_Model_Abstract
     */
    public function parse()
    {
        $this->priorityLogger->log(PHP_EOL . "---Started executing bundle rules---", Zend_Log::INFO);

        $bundleRuleId = $this->bundle->getId();
        foreach ($this->bundle->getActions() as $action) {

            $this->priorityLogger->log(sprintf("Evaluating action %d", $action->getId()), Zend_Log::INFO);

            // make sure scope object was sent to this instance through construct args
            if (isset($this->scopes[$action->getScope()]) && $this->getPackageTypeIdByCode($action->getPackageCreationTypeId()) == $this->scopes[$action->getScope()]->getPackage()->getPackageCreationTypeId()) {
                $data['scope'] = $this->scopes[$action->getScope()];
                $data['packageTypeId'] = $action->getPackageTypeId();
                $condition = $action->getCondition();
                // evaluate action only if no condition or given condition evaluates to true
                if (empty(trim($condition)) || $this->dynaCoreHelper->evaluateExpressionLanguage($condition, $data)) {
                    $this->priorityLogger->log(sprintf("Condition %s true, executing action %s on scope %s", $condition, $action->getAction(), $action->getScope()), Zend_Log::INFO);
                    if (!empty($action->getAction())) {
                        $this->dynaCoreHelper->setPackageCreationTypeId($this->getPackageTypeIdByCode($action->getPackageCreationTypeId()));
                        $this->dynaCoreHelper->evaluateExpressionLanguage($action->getAction(), $data);
                        $this->dynaCoreHelper->clearPackageType();
                    }
                }
            } else {
                $this->priorityLogger->log(sprintf("Action not evaluated, requested scope %s was not found in scopes: %s", $action->getScope(), implode (", ",
                    array_map(function($scope) {
                        return get_class($scope);
                    }, $this->scopes))), Zend_Log::ERR);
            }

            $this->priorityLogger->log(sprintf("Finished evaluating action %d", $action->getId()), Zend_Log::INFO);
        }

        $categories = [];


        // Scope cleanup
        foreach ($this->scopes as $scope) {
            if ($scope) {
                $categories = $scope->getChooseFromFamiliesIds() + $categories;
                $scope->onParsingDone();
            }
        }

        $this->priorityLogger->log(PHP_EOL . "---Finished executing bundle rules---", Zend_Log::INFO);

        $this->newActivePackage = $this->dynaCoreHelper->getPackage()->getPackageId();

        return Mage::getModel("dyna_bundles/expression_parserResponse", ['mandatoryChooseFromFamilies' => $categories, 'bundleRuleId' => $bundleRuleId]);
    }

    protected function getPackageTypeIdByCode($packageCode)
    {
        if ($this->packageCreationType === false) {
            $this->packageCreationType = Mage::getModel('dyna_package/packageCreationTypes');
        }
        return ($packageType = $this->packageCreationType->getTypeByPackageCode($packageCode)) ? $packageType->getId() : null;

    }

    /**
     * Method that returns the new active package from quote (it will be changed if bundle action create new packages)
     * @return int
     */
    public function getNewActivePackage()
    {
        return $this->newActivePackage;
    }
}
