<?php

/**
 * Class Dyna_Bundles_Model_Expression_QuotePackageScope
 */
class Dyna_Bundles_Model_Expression_QuotePackageScope extends Dyna_Bundles_Model_Expression_AbstractScope implements Dyna_Bundles_Model_Expression_ScopeInterface
{
    /** @var Dyna_Checkout_Model_Sales_Quote|Mage_Sales_Model_Quote */
    protected $quote;
    /** @var  Dyna_Bundles_Model_BundleRule */
    protected $bundle;
    /** @var mixed */
    protected $categories;
    /** @var array|Omnius_Checkout_Model_Sales_Quote_Item[] */
    protected $quoteItems;
    /** @var Dyna_Configurator_Helper_Cart */
    protected $cartHelper = null;
    /** @var Dyna_Core_Helper_Data */
    protected $dynaCoreHelper = null;
    protected $bundlePackageIds = array();
    /**
     * @var Dyna_Package_Model_Package
     */
    protected $package = null;

    public function __construct($args)
    {
        if (empty($args['bundle']) || !($args['bundle'] instanceof Dyna_Bundles_Model_BundleRule)) {
            throw new InvalidArgumentException("invalid or no bundle received for quote package scope");
        }

        if($args['package']){
            $this->package = $args['package'];
        }

        $this->bundle = $args['bundle'];
        $this->quote = $this->getQuote();
        $this->quoteItems = $this->package->getAllItems();

        $this->bundlePackageIds[] = $args['package']->getPackageId();
        $this->bundlePackageIds[] = !empty($args['interStackPackageId']) ? $args['interStackPackageId'] : '';

        foreach ($this->quoteItems as $item) {
            if ($item->getProduct()->isSubscription()) {
                $item->setBundleComponent($this->bundle->getId());
            }
        }

        if (!$this->categories) {
            if (Mage::registry('allcategories')) {
                $this->categories = Mage::registry('allcategories');
            } else {
                $allCategories = Mage::getModel('catalog/category')
                    ->getCollection()
                    ->addAttributeToSelect('name');
                foreach ($allCategories as $category) {
                    $this->categories[$category->getId()] = $category->getName();
                }
                Mage::register('allcategories', $this->categories);
            }
        }
    }

    public function containsProduct($sku): bool
    {
        foreach ($this->quoteItems as $item) {
            if (in_array($item->getPackageId(), $this->bundlePackageIds) && $item->getSku() == $sku) {
               return true;
            }
        }
        return false;
    }

    public function containsProductInCategory($categoryName): bool
    {
        $categoryId = is_int($categoryName) ? $categoryName : $this->getCategoryIdFromPath($categoryName);

        foreach ($this->quoteItems as $item) {
            if (in_array($item->getPackageId(), $this->bundlePackageIds)) {
                $cats = $item->getProduct()->getCategoryIds();
                if(in_array($categoryId, $cats)) {
                    return true;
                }
            }
        }

        return false;
    }

    public function add(...$skus)
    {
        foreach ($skus as $sku) {
            try {
                $this->addSkuToCart($sku);
            } catch (Exception $e) {
                Mage::log(sprintf("Unable to add %s to cart : %s", $sku, $e->getMessage()), Zend_Log::WARN, "bundleRules.log");
            }
        }

        return true;
    }

    public function replace($skuToReplace, $skusToReplaceWith)
    {
        $skusToReplaceList = explode(',', $skuToReplace);
        $skusToReplaceList = array_map('trim', $skusToReplaceList);

        $skusToReplaceWithList = explode(',', $skusToReplaceWith);
        $skusToReplaceWithList = array_map('trim', $skusToReplaceWithList);

        $this->delete(...$skusToReplaceList);
        $this->add(...$skusToReplaceWithList);

        return true;
    }

    public function delete(...$skusToDelete)
    {
        $activeIdInitial = $this->quote->getActivePackageId();
        $this->quote->saveActivePackageId($this->package->getPackageId());

        $product = Mage::getModel('catalog/product');

        foreach ($skusToDelete as $sku) {
            $productId[] = $product->getIdBySku($sku);
        }

        if (is_array($productId)) {
            $ids = array_filter(array_filter($productId, 'trim'));

            foreach ($ids as $id) {
                $item = $this->getQuote()->getItemsCollection()->getItemsByColumnValue('product_id', (int)$id);
                $item = reset($item);
                if ($item) {
                    $this->getCart()
                        ->removeItem($item->getId());
                }
            }
        }
        $this->quote->setActivePackageId($activeIdInitial);

        return true;
    }

    public function onParsingDone(): bool
    {
        return true;
    }

    public function getId()
    {
        $packageTypeId = $this->getCoreHelper()->getPackageCreationTypeId();
        $packageCreationId = Mage::getModel('dyna_package/packageCreationTypes')->getTypeByPackageCode($packageTypeId);

        foreach ($this->bundlePackageIds as $packageId) {
            $packageInBundle = $this->getQuote()->getCartPackage($packageId);

            if ($packageCreationId == $packageInBundle->getPackageCreationTypeId()) {
                return $packageInBundle->getId();
            }
        }

        return $this->getQuote()->getCartPackage()->getId();
    }

    public function chooseFromCategory($category, $default = null)
    {
        if (!($category instanceof Dyna_Catalog_Model_Category)) {
            $category = $this->getCategoryIdFromPath($category);
        } else {
            // keep only category id from category instance
            $category = $category->getId();
        }

        if ($category && !in_array($category, $this->chooseFromCategories)) {
            if ($default) {
                if ($item = $this->addSkuToCart($default)) {
                    $item->setBundleChoice(true);
                    $item->setBundleComponent(false);
                    $this->chooseFromCategories[$this->package->getEntityId()][$this->package->getEntityId()] = $category;
                }
            }
        }

        return true;
    }

    public function chooseFromCategoryId($categoryId, $default = null)
    {
        if (($category = $this->getCategoryIdByUniqueId($categoryId)) && $category->getId()) {
            return $this->chooseFromCategory($category, $default);
        }

        return true;
    }

    /**
     * @param $sku
     * @return Dyna_Checkout_Model_Sales_Quote_Item|Mage_Checkout_Model_Cart|Mage_Sales_Model_Quote_Item|Omnius_Checkout_Model_Sales_Quote_Item
     */
    protected function addSkuToCart($sku)
    {
        $websiteId = Mage::app()->getWebsite()->getId();
        if (($emulatedWebsiteId = Mage::helper('superorder')->shouldEmulateStore($websiteId)) != $websiteId) {
            // If a product from another shop should be added, emulate that store
            $storeId = Mage::app()->getWebsite($emulatedWebsiteId)->getDefaultStore()->getId();
        } else {
            $storeId = Mage::app()->getStore()->getId();
        }

        $cart = $this->getCart();
        if ($sku) {
            $product = Mage::getModel('catalog/product')
                ->setStoreId($storeId);
            $product->load($product->getIdBySku($sku));
            if (!$product->getId()) {
                Mage::throwException('Invalid product ID given');
            }
        } else {
            Mage::throwException('Invalid product ID given');
        }

        $numberCtns = count($this->getQuote()->getPackageCtns());
        $isOneOfDeal = Mage::helper('omnius_configurator/cart')->isOneOfDealActive();

        $result = false;
        if (!$this->exists($product)) {
            $result = $cart->addProduct($product, array(
                    'qty' => $numberCtns ?: 1,
                    'one_of_deal' => ($isOneOfDeal && $numberCtns) ? 1 : 0,
                )
            );

            if (is_string($result)) {
                Mage::throwException($result);
            } else {
                // get the package id and package type of the product in the current quote and bundle
                $packageInfo = $this->getCartPackageInfo();

                $result
                    ->setPackageType($packageInfo['packageType'])
                    ->setBundleComponent($this->bundle->getId())
                    ->setPackageId($packageInfo['packageId']);
            }
            $cart->save();
        } else {
            $this->markProductAsBundleComponent($product);
        }

        return $result;
    }

    /**
     * Get cart instance from session
     * @return Dyna_Checkout_Model_Cart|Mage_Core_Model_Abstract
     */
    protected function getCart()
    {
        return Mage::getSingleton('checkout/cart');
    }

    /**
     * Get current session quote
     * @return Dyna_Checkout_Model_Sales_Quote|Mage_Sales_Model_Quote
     */
    protected function getQuote()
    {
        return $this->getCart()->getQuote();
    }


    /**
     * @param Mage_Catalog_Model_Product $product
     * @return bool|Mage_Sales_Model_Quote_Item
     */
    protected function exists(Mage_Catalog_Model_Product $product)
    {
        /* Re-fetch quote items */
        $quoteItems =  $this->quote->getAllItems();
        /** @var Mage_Sales_Model_Quote_Item $quoteItem */
        foreach ($quoteItems as $quoteItem) {
            if (($quoteItem->getPackageId() == $this->getActivePackage()->getId() || $quoteItem->getPackageId() == $this->getActivePackage()->getPackageId())&&
                $quoteItem->getProductId() == $product->getId()
            ) {
                return $quoteItem;
            }
        }
        return false;
    }

    /**
     * If a product is present in the quote scope, mark it as a bundle component
     * @param Mage_Catalog_Model_Product $product
     * @param bool $isPartOfBundle
     * @return bool
     */
    public function markProductAsBundleComponent(Mage_Catalog_Model_Product $product, $isPartOfBundle = true)
    {
        $isPartOfBundle = $isPartOfBundle ? $this->bundle->getId() : 0;
        $marked = false;
        $quoteItem = $this->exists($product);
        if ($quoteItem) {
            $quoteItem->setBundleComponent($isPartOfBundle);
            $marked = true;
        }
        return $marked;
    }

    public function markProductAsBundleChoice($product, $isBundleChoice = true)
    {
        $marked = false;
        $quoteItem = $this->exists($product);
        if($quoteItem) {
            $quoteItem->setBundleChoice($isBundleChoice);
            if ($isBundleChoice) {
                $quoteItem->setBundleComponent($this->bundle->getId());
            }
            $marked = true;
        }
        return $marked;
    }

    public function addPromoProduct($skus, $target)
    {
        $websiteId = Mage::app()->getWebsite()->getId();
        if (($emulatedWebsiteId = Mage::helper('superorder')->shouldEmulateStore($websiteId)) != $websiteId) {
            // If a product from another shop should be added, emulate that store
            $storeId = Mage::app()->getWebsite($emulatedWebsiteId)->getDefaultStore()->getId();
        } else {
            $storeId = Mage::app()->getStore()->getId();
        }

        $aSkus = explode(',', $skus);
        foreach ($aSkus as $sku) {
            $quote = $this->getQuote();
            $cart = $this->getCart();
            if ($sku) {
                $product = Mage::getModel('catalog/product')
                    ->setStoreId($storeId);
                $product->load($product->getIdBySku($sku));
                if (!$product->getId()) {
                    Mage::throwException('Invalid product ID given');
                }
            } else {
                Mage::throwException('Invalid product ID given');
            }

            $numberCtns = count($this->getQuote()->getPackageCtns());
            $isOneOfDeal = Mage::helper('omnius_configurator/cart')->isOneOfDealActive();

            $targetProductId = null;
            if (!$this->exists($product)) {
                $allItems = $this->package->getAllItems();
                foreach ($allItems as $targetItem) {
                    if ($targetItem->getSku() == $target) {
                        $targetProductId = $targetItem->getProductId();
                        break;
                    }
                }

                // adding a product to a target that doesn't exist should never happen as the action should have an existence condition for this
                // but still returning evaluation response to true to prevent logging
                if (!$targetProductId) {
                    return true;
                }

                $result = $cart->addProduct($product, array(
                        'qty' => $numberCtns ?: 1,
                        'one_of_deal' => ($isOneOfDeal && $numberCtns) ? 1 : 0,
                    )
                );

                if (is_string($result)) {
                    Mage::throwException($result);
                } else {
                    // get the package id and package type of the product in the current quote and bundle
                    $packageInfo = $this->getCartPackageInfo();

                    $result
                        ->setPackageType($packageInfo['packageType'])
                        ->setBundleComponent($this->bundle->getId())
                        ->setTargetId($targetProductId)
                        ->setPackageId($packageInfo['packageId'])
                        ->setIsBundlePromo(1);
                }
            } else {
                $this->markProductAsBundleComponent($product);
            }

        }

        return true;
    }

    /**
     * Method that adds a package in current session quote
     * @return true
     */
    public function addPackage(string $packageType)
    {
        /** @var Dyna_Configurator_Helper_Cart $cartHelper */
        $cartHelper = $this->getCartHelper();
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = $this->getCart()->getQuote();
        $packageCreationTypeId = $cartHelper->getCreationTypeId($packageType);

        $newPackageId = $cartHelper->initNewPackage(
            strtolower($packageType),
            Dyna_Checkout_Model_Sales_Quote_Item::ACQUISITION,
            null,
            null,
            null,
            null,
            null,
            null,
            $packageCreationTypeId
        );
        $newPackage = $quote->getCartPackage($newPackageId);
        /** @var Dyna_Bundles_Helper_Data $bundlesHelper */
        $bundlesHelper = Mage::helper('dyna_bundles');

        $newPackage
            ->setCreatedAt(now())
            ->setBundleName($this->bundle->getBundleRuleName());

        $bundlesHelper->addBundlePackageEntry($this->bundle->getId(), $newPackage->getId());

        // remember this package on expression helper, if products will further be added, then these products will be added to this package
        $this->getCoreHelper()->addNewCreatedPackage($newPackage);

        return true;
    }

    /**
     * Method that returns the cart helper
     * @return Dyna_Configurator_Helper_Cart
     */
    protected function getCartHelper()
    {
        if (!$this->cartHelper) {
            $this->cartHelper = Mage::helper('dyna_configurator/cart');
        }

        return $this->cartHelper;
    }

    /**
     * Get the dyna core helper instance
     * Initially implemented for setting last added package on it
     * @return Dyna_Core_Helper_Data|Mage_Core_Helper_Abstract
     */
    protected function getCoreHelper()
    {
        if (!$this->dynaCoreHelper) {
            $this->dynaCoreHelper = Mage::helper('dyna_core');
        }

        return $this->dynaCoreHelper;
    }

    /**
     * Method that returns the active package from core helper for determining to which package products will be added
     * @return Dyna_Package_Model_Package
     */
    protected function getActivePackage()
    {
        return $this->getCoreHelper()->getPackage();
    }

    /**
     * Method that returns the active package from core helper for determining to which package products will be added
     * @return Dyna_Package_Model_Package
     */
    protected function getCurrentActionPackageId()
    {
        $packageTypeId = $this->getCoreHelper()->getPackageCreationTypeId();
        foreach ($this->bundlePackageIds as $packageId) {
            if (($package = $this->quote->getCartPackage($packageId)) && $package->getPackageCreationTypeId() == $packageTypeId) {
                return $package->getPackageId();
            }
        }

        return null;
    }

    /**
     * Method used to determine the package id and package type of
     * a product in quote. The method is required due to the interstack
     * bundles that allow 2 cart packages to be bundled, so the active
     * package cannot be used to determine the type and id
     *
     * @param Dyna_Catalog_Model_Product $product
     * @return array
     */
    protected function getCartPackageInfo()
    {
        $properPackage = $this->getQuote()->getCartPackage($this->getCurrentActionPackageId());

        return array(
            'packageId' => $properPackage ? $properPackage ->getPackageId() : $this->getQuote()->getActivePackageId(),
            'packageType' => strtolower($properPackage ? $properPackage->getType() : $this->getQuote()->getActivePackage()->getType())
        );
    }

    /**
     * @param $categoryId
     * @return mixed
     */
    public function containsProductInCategoryId($categoryId)
    {
        if (($category = $this->getCategoryIdByUniqueId($categoryId)) && $category->getId()) {
            return $this->containsProductInCategory((int)$category->getId());
        }

        return false;
    }

    /**
     * Return package
     */
    public function getPackage()
    {
        return $this->package;
    }

    /**
     * Deletes every product of "$categoryName" in the corresponding package
     *
     * @param $categoryName
     * @return bool
     */
    public function deleteProductsFromCategory($categoryName)
    {
        $categoryId = is_int($categoryName) ? $categoryName : $this->getCategoryIdFromPath($categoryName);

        foreach ($this->quoteItems as $item) {

            if ($item->getPackageId() == $this->getCurrentActionPackageId()) {
                $cats = $item->getProduct()->getCategoryIds();
                if(in_array($categoryId, $cats)) {
                    $this->getCart()
                        ->removeItem($item->getId());
                }
            }
        }

        return true;
    }
}
