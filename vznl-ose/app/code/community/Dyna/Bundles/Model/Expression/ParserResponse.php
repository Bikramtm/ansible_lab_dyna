<?php

/**
 * Class Dyna_Bundles_Model_Expression_ParserResponse
 */
class Dyna_Bundles_Model_Expression_ParserResponse
{
    /**
     * Mandatory choose families
     */
    protected $mandatoryChooseFromFamilies = [];
    /**
     * Bundle rule id
     */
    protected $bundleRuleId = null;
    /**
     * Dyna_Bundles_Model_Expression_ParserResponse constructor.
     * @param array $args
     */
    public function __construct(array $args)
    {
        if(isset($args['mandatoryChooseFromFamilies'])) {
            $this->mandatoryChooseFromFamilies = $args['mandatoryChooseFromFamilies'];
        }
        if(isset($args['bundleRuleId'])) {
            $this->bundleRuleId = $args['bundleRuleId'];
        }
    }

    /**
     * @return mixed
     */
    public function getMandatoryChooseFromFamilies()
    {
        return $this->mandatoryChooseFromFamilies;
    }

    public function getBundleRuleId()
    {
        return $this->bundleRuleId;
    }

    public function getMandatoryChooseFromFamiliesFormat()
    {
        $category = Mage::getModel("catalog/category");
        $categories = [];
        foreach ($this->mandatoryChooseFromFamilies as $bundleRuleId => $families) {
            foreach ($families as $packageEntityId => $familyId) {
                $cat = [];
                $cat['packageId'] = $packageEntityId;
                $cat['familyId'] = $familyId;

                $categoryLoaded = $category->load($familyId);
                $cat['familyName'] = $categoryLoaded ? $categoryLoaded->getName() : '';

                $categories[$bundleRuleId][] = $cat;
            }
        }


        return $categories;
    }

    public function toArray()
    {
        return [
            'mandatoryChooseFromFamilies' => $this->mandatoryChooseFromFamilies,
            'bundleRuleId' => $this->bundleRuleId
        ];
    }
}
