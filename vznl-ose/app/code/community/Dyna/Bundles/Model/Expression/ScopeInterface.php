<?php

interface Dyna_Bundles_Model_Expression_ScopeInterface
{
    // Conditions
    public function containsProduct($sku): bool;

    public function containsProductInCategory($categoryName): bool;

    //Actions
    public function add(...$skus);

    public function replace($skuToReplace, $skusToReplaceWith);

    public function delete(...$skusToDelete);

    public function chooseFromCategory($category, $default);

    public function chooseFromCategoryId($categoryId, $default);

    //Cleanup
    public function getChooseFromFamiliesIds();

    public function onParsingDone(): bool;

    // package ID
    public function getId();
}
