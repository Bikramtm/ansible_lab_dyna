<?php

/**
 * Class Dyna_Bundles_Model_BundleRule
 * @method string getBundleType()
 * @method string getBundleRuleName()
 * @method string getBundleGuiName()
 * @method string|nulL getDescription()
 * @method string getWebsiteId()
 * @method string|nulL getBasedOnALifecycle()
 * @method string|nulL getDisplayBundleBenefits1()
 * @method string|nulL getDisplayBundleBenefits2()
 * @method string|nulL getHintEnabled()
 * @method string|nulL getHintTitle()
 * @method string|nulL getHintText()
 * @method string|nulL getHintLocation()
 */
class Dyna_Bundles_Model_BundleRule extends Mage_Core_Model_Abstract
{
    CONST LOCATION_DRAW = "draw";
    CONST LOCATION_OPTION = "options";
    CONST LOCATION_CART = "cart";
    CONST LOCATION_LIGHT_BULB = "lightbulb";
    CONST LOCATION_SLIDE_IN = "slidein";

    // used for allowing red plus package type logic
    CONST TYPE_RED_PLUS = "RedPlus";
    // used for combinations of Cable or Dsl with Prepaid
    CONST TYPE_SUSO = "Suso";
    // used for install base
    CONST TYPE_KOMBI = "GigaKombi";
    CONST TYPE_INTRA = "IntraStack";
    // generic bundle type
    CONST TYPE_OTHER = "Other";

    CONST TYPE_LOWENTRY = "Lowentry";

    CONST DEFAULT_MAX_NUMBER_OF_MEMBERS = 4;

    /** @var Dyna_Configurator_Helper_Cart */
    protected $cartHelper = null;
    /** @var Dyna_Checkout_Model_Cart */
    protected $cart = null;
    /** @var Dyna_Customer_Model_Session */
    protected $customerSession = null;

    /** @var Dyna_Bundles_Helper_Data $bundleHelper */
    protected $bundleHelper = null;

    protected $_actions = null;
    protected $_conditions = null;
    protected $_hints = null;
    protected $_processContexts = false;
    protected $_processContextsIds = false;

    protected $susoTypes = array('SurfSofortDSL', 'SurfSofortKIP', 'SurfSofort', 'SurfSofortKabel');
    protected $redPlusTypes = array('RedPlus','Postpaid_RedPlus');
    protected $marketingTypes = array('Marketing');
    protected $gigaKombiTypes = array('GigaKombi');
    protected $lowEntryTypes = array('Lowentry');
    /** @var null Dyna_Cache_Model_Cache */
    protected $_cache = null;

    /** @var Dyna_Bundles_Helper_Data */
    protected $_bundleHelper = null;

    public function _construct()
    {
        $this->_init('dyna_bundles/bundleRule');

        $this->_bundleHelper = Mage::helper('dyna_bundles');
    }

    /**
     * @return Dyna_Cache_Model_Cache
     */
    protected function getCache()
    {
        if (!$this->_cache) {
            $this->_cache = Mage::getSingleton('dyna_cache/cache');
        }

        return $this->_cache;
    }

    /**
     * Get all bundle actions for this instance
     * @return Dyna_Bundles_Model_BundleAction[]
     */
    public function getActions()
    {
        if ($this->_actions) {
            return $this->_actions;
        }

        $cacheKey = "BUNDLE_ACTIONS_ID_" . $this->getId();
        if (($result = $this->getCache()->load($cacheKey)) && $result = unserialize($result)) {
            $this->_actions = $result;
        } else {
            $result = new Varien_Data_Collection();
            $actions = Mage::getModel('dyna_bundles/bundleAction')
                ->getCollection()
                ->addFieldToSelect('*')
                ->addFieldToFilter('bundle_rule_id', $this->getId())
                ->setOrder("priority", "ASC");
            foreach ($actions as $action) {
                $result->addItem($action);
            }
            $this->getCache()->save(serialize($result), $cacheKey, array($this->getCache()::PRODUCT_TAG));
            $this->_actions = $result;
        }

        return $this->_actions;
    }

    /**
     * Return the unique bundle id for a bundle_rule package combination
     * @return Dyna_Bundles_Model_BundlePackages
     */
    public function getBundlePackage(Dyna_Package_Model_Package $package)
    {
        return Mage::getModel('dyna_bundles/bundlePackages')
            ->getCollection()
            ->addFieldToFilter('bundle_rule_id', array('eq' => $this->getId()))
            ->addFieldToFilter('package_id', array('eq' => $package->getId()))
            ->getFirstItem();
    }

    /**
     * @param $name
     *
     * @return mixed
     */
    public function getByBundleRuleName($name)
    {
        return $this->getCollection()
          ->addFieldToSelect('*')
          ->addFieldToFilter('bundle_rule_name', $name)
          ->getFirstItem();
    }

    /**
     * Get bundle conditions for current instance
     * @return Dyna_Bundles_Model_BundleCondition[]
     */
    public function getConditions()
    {
        if ($this->_conditions) {
            return $this->_conditions;
        }

        $cacheKey = "BUNDLE_CONDITIONS_ID_" . $this->getId();
        if (($result = $this->getCache()->load($cacheKey)) && $result = unserialize($result)) {
            $this->_conditions = $result;
        } else {
            $result = new Varien_Data_Collection();
            $conditions = Mage::getModel('dyna_bundles/bundleCondition')
                ->getCollection()
                ->addFieldToSelect('*')
                ->addFieldToFilter('bundle_rule_id', $this->getId());
            foreach ($conditions as $condition) {
                $result->addItem($condition);
            }
            $this->getCache()->save(serialize($result), $cacheKey, array($this->getCache()::PRODUCT_TAG));
            $this->_conditions = $result;
        }

        return $this->_conditions;
    }

    /**
     * Get the bundle hints for current bundle
     * @return Dyna_Bundles_Model_BundleHint[]
     */
    public function getHints()
    {
        if (!$this->_hints) {
            $this->_hints = Mage::getModel('dyna_bundles/bundleHint')
                ->getCollection()
                ->addFieldToSelect('*')
                ->addFieldToFilter('bundle_rule_id', $this->getId());
        }

        return $this->_hints;
    }

    public function getProcessContexts()
    {
        if ($this->_processContexts !== false) {
            return $this->_processContexts;
        }

        $cacheKey = "BUNDLE_PROCESS_CONTEXT_ID_" . $this->getId();
        if (($result = $this->getCache()->load($cacheKey)) && $result = unserialize($result)) {
            $this->_processContexts = $result;
        } else {
            $result = new Varien_Data_Collection();
            /** @var Dyna_Bundles_Model_Mysql4_BundleProcessContext_Collection $processContextRelations */
            $processContextRelations = Mage::getModel('dyna_bundles/bundleProcessContext')
                ->getCollection();
            $processContextRelations
                ->addFieldToSelect('*')
                ->addFieldToFilter('bundle_rule_id', $this->getId())
                ->getSelect()
                ->reset(Varien_Db_Select::COLUMNS)
                ->columns('process_context_id');
            $processContextIds = $processContextRelations->getConnection()->fetchCol($processContextRelations->getSelectSql());
            /** @var Dyna_Catalog_Model_Resource_ProcessContext_Collection $processContextRelations */
            $processContexts = Mage::getModel('dyna_catalog/processContext')
                ->getCollection()
                ->addFieldToFilter('entity_id', array('in' => $processContextIds));
            foreach ($processContexts as $context) {
                $result->addItem($context);
            }
            $this->getCache()->save(serialize($result), $cacheKey, array($this->getCache()::PRODUCT_TAG));
            $this->_processContexts = $result;
        }

        return $this->_processContexts;
    }

    public function getProcessContextIds() {
        if ($this->_processContexts === false) {
            $this->_processContexts = $this->getProcessContexts();
        }
        if ($this->_processContextsIds === false) {
            $this->_processContextsIds = [];
            foreach ($this->_processContexts as $processContext) {
                $this->_processContextsIds[$processContext->getId()] = $processContext->getId();
            }
        }
        return $this->_processContextsIds;
    }

    /**
     * Return current bundle type (currently only three special cases are treated: Red+, Suso, Kombi)
     * @return string
     */
    public function getType()
    {
        switch (true) {
            case in_array($this->getBundleType(), $this->susoTypes):
                $type = static::TYPE_SUSO;
                break;
            case in_array($this->getBundleType(), $this->redPlusTypes):
                $type = static::TYPE_RED_PLUS;
                break;
            case in_array($this->getBundleType(), $this->gigaKombiTypes):
                $type = static::TYPE_KOMBI;
                break;
            case in_array($this->getBundleType(), $this->lowEntryTypes):
                $type = static::TYPE_LOWENTRY;
                break;
            case $this->getBundleType() == self::TYPE_INTRA:
                $type = static::TYPE_INTRA;
                break;
            default:
                $type = static::TYPE_OTHER;
                break;
        }

        return $type;
    }

    /**
     * Determine whether or not current bundle is of type SurfSohort
     * @return bool
     */
    public function isSuso()
    {
        return $this->getType() === static::TYPE_SUSO;
    }

    /**
     * Determine whether or not current bundle is of type Red+
     * @return bool
     */
    public function isRedPlus()
    {
        return $this->getType() === static::TYPE_RED_PLUS;
    }

    /**
     * Determines whether or not the bundle is of type Lowentry
     * @return bool
     */
    public function isLowEntry()
    {
        return $this->getType() === static::TYPE_LOWENTRY;
    }

    /**
     * Determines whether or not the bundle is of type GigaKombi
     * @return bool
     */
    public function isGigaKombi()
    {
        return $this->getType() === static::TYPE_KOMBI;
    }

    /**
     * Determines whether or not the bundle is of type GigaKombi
     * @return bool
     */
    public function isIntraStack()
    {
        return $this->getType() === static::TYPE_INTRA;
    }

    /**
     * Method that creates a wave1 bundle with an install base product
     * Return the new active package forwarded towards frontend
     * @return int
     */
    public function createLegacyBundle($customerNumber, $productId)
    {
        // Gather all needed instances
        $cHelper = $this->getCartHelper();
        $bHelper = $this->getBundlesHelper();
        /** @var Dyna_Customer_Model_Customer $customer */
        $customer = $this->getCustomerSession()->getCustomer();

        // Get package type from customer contract
        $packageType = $customer->getInstalledBaseContractType($customerNumber, $productId);

        if (strtolower($packageType) === strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE)) {
            $this->getCustomerSession()->setKiasLinkedAccountNumber($customerNumber);
        } else if (in_array(strtolower($packageType), Dyna_Catalog_Model_Type::getCablePackages())) {
            $this->getCustomerSession()->setKdLinkedAccountNumber($customerNumber);
        }

        // Get a list of skus customer has in its install base
        $installBaseProducts = $customer->getInstalledBaseProducts($customerNumber, $productId);

        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = $this->getCart()->getQuote();

        // Set bundle id pe active cart package
        /** @var Dyna_Package_Model_Package $activePackage */
        $activePackage = $quote->getCartPackage();
        if (!$activePackage) {
            Mage::throwException($this->__("Cannot create legacy bundle without an active package in configurator."));
        }

        //VFDED1W3S-3797 : save the package products before creating the bundle
        $cartProducts = [];
        foreach ($activePackage->getItems() as $packageItem) {
            $cartProducts[] = $packageItem->getSku();
        }

        if (count($cartProducts)) {
            $beforeBundleProducts = Mage::helper('core')->jsonEncode($cartProducts);
            $activePackage->setBeforeBundleProducts($beforeBundleProducts);
        }
        //END VFDED1W3S-3797

        $activePackage
            ->setBundleName($this->getBundleRuleName())
            ->save();

        $bHelper->addBundlePackageEntry($this->getId(), $activePackage->getEntityId());

        // Retain active package id
        $activePackageId = $activePackage->getPackageId();

        if ($packageModel = $quote->getGhostPackageForIBSubscription($customerNumber, $productId)) {
            $bHelper->addBundlePackageEntry($this->getId(), $packageModel->getEntityId());
        } else {
            $packageCreationTypeId = $cHelper->getCreationTypeId(Dyna_Catalog_Model_Type::getCreationTypesMapping()[strtolower($packageType)]);
            // Init package
            $packageId = $cHelper->initNewPackage(
                $packageType,
                Dyna_Checkout_Model_Sales_Quote_Item::ACQUISITION,
                null,
                null,
                null,
                null,
                null,
                null,
                $packageCreationTypeId
            );

            $packageModel = $quote->getPackageById($packageId);

            // Save install base package
            /** @var Dyna_Package_Model_Package $packageModel */
            $packageModel
                ->setInstalledBaseProducts(implode(",", $installBaseProducts))
                ->setInitialInstalledBaseProducts(implode(",", $installBaseProducts))
                ->setEditingDisabled(true)
                ->setCurrentStatus(Omnius_Configurator_Model_AttributeSet::PACKAGE_STATUS_COMPLETED)
                ->setCreatedAt(now())
                ->setBundleName($this->getBundleRuleName())
                ->setQuoteId($quote->getId())
                ->setParentAccountNumber($customerNumber)
                ->setServiceLineId($productId)
                ->save();

            $bHelper->addBundlePackageEntry($this->getId(), $packageModel->getEntityId());
        }


        // Preserve active package on quote
        $quote
            ->setActivePackageId($activePackageId);

        $ruleParser = $bHelper->buildRuleParserFromBundleRule($this, $quote->getCartPackage($activePackageId), $packageModel);
        $ruleResponse = $ruleParser->parse();

        $this->getCustomerSession()->setBundleRuleParserResponse($ruleResponse);
        // @todo cange this session setter (for showing hint on checkout) to getActiveBundles call from checkout
        $this->getCustomerSession()->setActiveBundleId($this->getId());

        $activePackage->updateBundleTypes()->save();
        $packageModel->updateBundleTypes()->save();

        // return the new package bundled with the ghost install base one
        return $activePackageId;
    }

    /**
     * @param null $customerNumber
     * @param null $productId
     * @return array
     */
    public function getSummaryBundleDetails($customerNumber = null, $productId = null, $selectedInstalledBaseProductEntityId = null, $interStackPackageId = null)
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = $this->getCart()->getQuote();
        $bHelper = $this->getBundlesHelper();
        /** @var Dyna_Package_Model_Package $activePackage */
        $activePackage = $quote->getCartPackage();
        $dummyPackage = null;
        $bundleProductsSummaryTotals = [];
        $response = [];

        if ($customerNumber) {
            $customer = $this->getCustomerSession()->getCustomer();
            $installBaseProducts = $customer->getInstalledBaseProducts($customerNumber, $productId);
            /** @var Dyna_Package_Model_Package $dummyPackage */
            $dummyPackage = Mage::getModel("package/package");

            // Save install base package
            $dummyPackage
                ->setInstalledBaseProducts(implode(",", $installBaseProducts))
                ->setInitialInstalledBaseProducts(implode(",", $installBaseProducts))
                ->setEditingDisabled(true)
                ->setCurrentStatus(Omnius_Configurator_Model_AttributeSet::PACKAGE_STATUS_COMPLETED)
                ->setCreatedAt(now())
                ->setBundleId($this->getId())
                ->setBundleName($this->getBundleRuleName())
                ->setQuoteId($quote->getId())
                ->setParentAccountNumber($customerNumber)
                ->setServiceLineId($productId);

        }

        Mage::unregister('bundleSummaryProducts');
        if ($interStackPackageId) {
            $secondPackage = $quote->getCartPackage($interStackPackageId);
            $bundlePackages = [
                $activePackage,
                $secondPackage
            ];

            foreach ($bundlePackages as $package) {
                $ruleParser = $bHelper->buildRuleParserFromBundleRuleForDummyScope($this, $package, ($package != $secondPackage ? $secondPackage : $activePackage), $interStackPackageId);
                $ruleParser->parse();
            }
        } else {
            $ruleParser = $bHelper->buildRuleParserFromBundleRuleForDummyScope($this, $activePackage, $dummyPackage, $interStackPackageId);
            $ruleParser->parse();
        }
        $response['products'] = Mage::registry('bundleProductsSummary');
        $response['totals'] = $bundleProductsSummaryTotals;

        return $response;
    }

    /**
     * Method that creates an interstack bundle
     * Return the new active package forwarded towards frontend
     * @return int
     */
    public function createInterStackBundle($exitingCartPackageId)
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = $this->getCart()->getQuote();
        $bHelper = $this->getBundlesHelper();


        foreach ($this->getBundleComponentItems() as $item) {
            $item->setBundleComponent($this->getId())->save();
        }

        $activePackage = $quote->getCartPackage();
        $existingPackage = $quote->getCartPackage($exitingCartPackageId);

        $this->setBeforeBundleProducts($activePackage);
        $this->setBeforeBundleProducts($existingPackage);

        $bundlePackages = [
            $activePackage,
            $existingPackage
        ];
        foreach ($bundlePackages as $package) {
            $bHelper->addBundlePackageEntry($this->getId(), $package->getEntityId());
            $ruleParser = $bHelper->buildRuleParserFromBundleRule($this, $package, $package != $existingPackage ? $existingPackage : $activePackage);
            $ruleResponse = $ruleParser->parse();
            $this->_bundleHelper->setBundleRuleParser($ruleResponse);
            $package->updateBundleTypes()->save();
        }

        return $quote->getActivePackageId();
    }

    /**
     * Saves the products that were on the package before the bundle is created
     *
     * @param Dyna_Package_Model_Package $package
     */
    protected function setBeforeBundleProducts($package)
    {
        $cartProducts = [];
        foreach ($package->getItems() as $packageItem) {
            $cartProducts[] = $packageItem->getSku();
        }

        if (count($cartProducts)) {
            $beforeBundleProducts = Mage::helper('core')->jsonEncode($cartProducts);
            $package->setBeforeBundleProducts($beforeBundleProducts);
        }
    }

    /**
     * Method that creates Surfsofort or RedPlus
     * Return the new active package forwarded towards frontend
     * @return int
     */
    public function createSusoBundle()
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = $this->getCart()->getQuote();
        $bHelper = $this->getBundlesHelper();

        foreach ($this->getBundleComponentItems() as $item) {
            $item->setBundleComponent($this->getId());
        }

        $activePackage = $quote->getCartPackage();
        $bHelper->addBundlePackageEntry($this->getId(), $activePackage->getEntityId());

        $ruleParser = $bHelper->buildRuleParserFromBundleRule($this, $activePackage, null);
        $ruleResponse = $ruleParser->parse();

        $this->getCustomerSession()->setBundleRuleParserResponse($ruleResponse);
        // @todo cange this session setter (for showing hint on checkout) to getActiveBundles call from checkout
        $this->getCustomerSession()->setActiveBundleId($this->getId());

        $activePackage->updateBundleTypes()->save();

        // returning red plus package id for frontend to trigger initConfigurator for this package
        return $ruleParser->getNewActivePackage();
    }

    /**
     * Return the quote items that are required for bundling (targeted products in cart)
     * @return Dyna_Checkout_Model_Sales_Quote_Item[]
     */
    public function getBundleComponentItems()
    {
        $items = array();

        // get expression language instance
        /** @var Dyna_Bundles_Helper_ExpressionLanguage $expressionLanguage */
        $expressionLanguage = Mage::helper('dyna_bundles/expressionLanguage');
        $expressionLanguage->resetResponse();

        /** @var Dyna_Configurator_Helper_Expression $bundlesSimulatorHelper */
        $bundlesSimulatorHelper = Mage::helper('dyna_bundles/expression');
        $bundlesSimulatorHelper->updateInstance(Mage::getSingleton('checkout/cart')->getQuote());
        $bundlesSimulatorHelper->setCurrentBundle($this);

        $cartObject = Mage::getModel('dyna_configurator/expression_cart')->setBundlesHelper($bundlesSimulatorHelper);

        // get expression objects
        $expressionObjects = array(
            'customer' => Mage::getModel('dyna_configurator/expression_customer'),
            'cart' => $cartObject,
            'availability' => Mage::getModel('dyna_configurator/expression_availability'),
            'installBase' => Mage::getModel('dyna_configurator/expression_installBase'),
            'agent' => Mage::getModel('dyna_configurator/expression_agent'),
        );

        // throw exception if at least one fails, it should not be possible to access create action in this case
        foreach ($this->getConditions() as $bundleCondition) {
            if (!$expressionLanguage->evaluate($bundleCondition->getCondition(), $expressionObjects)) {
                Mage::throwException("You cannot create a bundle which evaluates to false");
            }
        }

        $evaluationResponse = $expressionLanguage->getResponse();
        $targetedProductIds = $evaluationResponse[Dyna_Bundles_Helper_Data::EXPRESSION_REQUIRED_PRODUCTS] ?? array();

        $activePackageId = $this->getCart()->getQuote()->getActivePackageId();
        foreach ($this->getCart()->getQuote()->getAllItems() as $quoteItem) {
            if ($quoteItem->getPackageId() !== $activePackageId) {
                continue;
            }

            if (in_array($quoteItem->getProductId(), $targetedProductIds) || $quoteItem->getProduct()->isSubscription()) {
                $items[] = $quoteItem;
            }
        }

        return $items;
    }

    /**
     * The Red+ bundle creation implies creating an owner ghost package and a RedPlus package that the agent can configure later
     * Return the new active package forwarded towards frontend
     * @see OVG-182
     * @return int
     */
    public function createRedPlusBundle($customerNumber, $productId)
    {
        /** @var Dyna_Configurator_Helper_Cart $cHelper */
        $cHelper = $this->getCartHelper();
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = $this->getCart()->getQuote();

        $bHelper = $this->getBundlesHelper();

        // searching for an existing package with the combination bundleId - subscription number - product id in current quote
        // if found forwarding request to addRedPlusPackage method
        foreach ($quote->getCartPackages() as $package) {
            if ($package->getParentAccountNumber() == $customerNumber && $package->getServiceLineId() == $productId) {
                $bHelper->addBundlePackageEntry($this->getId(), $package->getEntityId());
                return $this->addChildPackage($package);
            }
        }

        /** @var Dyna_Customer_Model_Customer $customer */
        $customer = $this->getCustomerSession()->getCustomer();

        // Get package type from customer contract which will be the owner
        $packageType = $customer->getInstalledBaseContractType($customerNumber, $productId);
        $creationType = $cHelper->getCreationTypeId(Dyna_Catalog_Model_Type::CREATION_TYPE_MOBILE);

        // Init owner package
        $packageId = $cHelper->initNewPackage(
            strtolower($packageType),
            Dyna_Checkout_Model_Sales_Quote_Item::ACQUISITION,
            null,
            null,
            null,
            null,
            null,
            null,
            $creationType
        );

        $ownerPackage = $quote->getPackageById($packageId);

        // get related product from this install base
        $relatedProductSku = $this->getIBBundleRelatedProduct($this->getId(), $customerNumber, $productId);

        // Save the owner package as ghost package
        /** @var Dyna_Package_Model_Package $ownerPackage */
        $ownerPackage
            ->setEditingDisabled(true)
            ->setCreatedAt(now())
            ->setBundleName($this->getBundleRuleName())
            ->setQuoteId($quote->getId())
            ->setParentAccountNumber($customerNumber)
            ->setInitialInstalledBaseProducts($relatedProductSku)
            ->setInstalledBaseProducts($relatedProductSku)
            ->setServiceLineId($productId)
            ->save();

        Mage::getSingleton('customer/session')->setKiasLinkedAccountNumber($customerNumber);
        $bHelper->addBundlePackageEntry($this->getId(), $ownerPackage->getEntityId());
        $ownerPackage->updateBundleTypes()->save();

        // Proceeding further to creating Red+ type package as child package
        // Create new Red+ package and set its parent to the above ghost package
        $redPlusPackageId = $this->addChildPackage($ownerPackage);

        $ruleParser = $bHelper->buildRuleParserFromBundleRule($this, $ownerPackage, $quote->getCartPackage($redPlusPackageId));
        $ruleResponse = $ruleParser->parse();

        $this->getCustomerSession()->setBundleRuleParserResponse($ruleResponse);
        // @todo cange this session setter (for showing hint on checkout) to getActiveBundles call from checkout
        $this->getCustomerSession()->setActiveBundleId($this->getId());

        // returning red plus package id for frontend to trigger initConfigurator for this package
        return $redPlusPackageId;
    }

    /**
     * Add a child package to an existing parent package belonging to this bundle
     * @param Dyna_Package_Model_Package $parentPackage
     * @param $subscriptionNumber
     * @param $productId
     * @return int
     */
    public function addChildPackage(Dyna_Package_Model_Package $parentPackage, $packageType = Dyna_Catalog_Model_Type::TYPE_MOBILE)
    {
        /** @var Dyna_Configurator_Helper_Cart $cHelper */
        $cHelper = $this->getCartHelper();
        $bHelper = $this->getBundlesHelper();
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = $this->getCart()->getQuote();
        $creationTypeId = $cHelper->getCreationTypeId(Dyna_Catalog_Model_Type::CREATION_TYPE_REDPLUS);

        $childPackageId = $cHelper->initNewPackage(
            strtolower($packageType),
            Dyna_Checkout_Model_Sales_Quote_Item::ACQUISITION,
            null,
            null,
            null,
            null,
            null,
            null,
            $creationTypeId
        );
        $childPackage = $quote->getCartPackage($childPackageId);

        $childPackage
            ->setParentId($parentPackage->getEntityId())
            ->setQuoteId($quote->getId())
            ->setAddedFromMyProducts(1)
            ->setParentAccountNumber($parentPackage->getParentAccountNumber());

        $bHelper->addBundlePackageEntry($this->getId(), $childPackage->getEntityId());
        $childPackage->updateBundleTypes()->save();


        return $childPackageId;
    }

    /**
     * Return the install base related product from eligible bundles call
     * @param $subscriptionNumber
     * @param $productId
     * @return string|null
     */
    public function getIBBundleRelatedProduct($bundleId, $subscriptionNumber, $productId)
    {
        $relatedProductSku = null;
        foreach ($this->getBundlesHelper()->getFrontendEligibleBundles() as $bundle) {
            if ($bundle['bundleId'] != $bundleId) {
                continue;
            }

            foreach ($bundle['targetedSubscriptions'] as $subscriptionData) {
                if ($subscriptionData['customerNumber'] === $subscriptionNumber && $subscriptionData['productId'] === $productId) {
                    $relatedProductSku = $subscriptionData['relatedProductSku'] ?? null;
                }
            }
        }

        return $relatedProductSku;
    }

    /**
     * Method that returns the cart helper
     * @return Dyna_Configurator_Helper_Cart
     */
    public function getCartHelper()
    {
        if (!$this->cartHelper) {
            $this->cartHelper = Mage::helper('dyna_configurator/cart');
        }

        return $this->cartHelper;
    }

    //public function addPackageToBundle(Dyna_Bundles_Model_BundleRule $bundle, )

    /**
     * Method that returns the bundles helper
     * @return Dyna_Bundles_Helper_Data
     */
    public function getBundlesHelper()
    {
        if (!$this->bundleHelper) {
            $this->bundleHelper = Mage::helper("bundles");
        }

        return $this->bundleHelper;
    }

    /**
     * Return session cart object
     * @return Dyna_Checkout_Model_Cart|Mage_Core_Model_Abstract
     */
    public function getCart()
    {
        if (!$this->cart) {
            $this->cart = Mage::getSingleton('checkout/cart');
        }

        return $this->cart;
    }

    /**
     * Get customer session instance
     * @return Dyna_Customer_Model_Session|Mage_Core_Model_Abstract
     */
    public function getCustomerSession()
    {
        if (!$this->customerSession) {
            $this->customerSession = Mage::getSingleton('customer/session');
        }

        return $this->customerSession;
    }

    /**
     * Checks whether the bundle has a hint with location options
     *
     * @return bool
     */
    public function hasLocationOption()
    {
        /** @var Dyna_Bundles_Model_BundleHint $hint */
        foreach ($this->getHints() as $hint) {
            if (strtolower($hint->getLocation()) == strtolower(self::LOCATION_OPTION)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Checks whether the bundle has a hint with location draw
     *
     * @return bool
     */
    public function hasLocationDraw()
    {
        /** @var Dyna_Bundles_Model_BundleHint $hint */
        foreach ($this->getHints() as $hint) {
            if (strtolower($hint->getLocation()) == strtolower(self::LOCATION_DRAW)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Checks whether the bundle has a hint with location cart
     *
     * @return bool
     */
    public function hasLocationCart()
    {
        /** @var Dyna_Bundles_Model_BundleHint $hint */
        foreach ($this->getHints() as $hint) {
            if (strtolower($hint->getLocation()) == strtolower(self::LOCATION_CART)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Returns the hint with the specified location for the current bundle
     *
     * @param $location
     * @return Dyna_Bundles_Model_BundleHint|null
     */
    public function getHintWithLocation($location)
    {
        /** @var Dyna_Bundles_Model_BundleHint $hint */
        foreach ($this->getHints() as $hint) {
            if (strtolower($hint->getLocation()) == strtolower($location)) {
                return $hint;
            }
        }

        return null;
    }

    public function getHintLocations()
    {
        $hints = [];
        /** @var Dyna_Bundles_Model_BundleHint $hint */
        foreach ($this->getHints() as $hint) {
            if($hint->getHintEnabled() && trim($hint->getLocation()) !== ''){
                $hints[] = $hint->getLocation() == 'options' ? 'option' : strtolower($hint->getLocation());
            }
        }

        return $hints;
    }

    /**
     * Returns the bundle type mapped from ogw_orchestration_id
     *
     * @return string
     */
    public function getBundleTypeByOrchestrationId() {
        switch( $this->getOgwOrchestrationType() ) {
            case 0:
                $type = 'Other';
                break;
            case 1:
                $type = 'Marketing';
                break;
            case 2:
                $type =  'RedPlus';
                break;
            case 3:
                $type =  'SurfSofortDSL';
                break;
            case 4:
                $type =  'SurfSofortKIP';
                break;
            default :
                $type =  '';
                break;
        }
        // Returns empty on unsupported value
        return $type;
    }

    /**
     * Check if bundle has process context
     * @param null $processContext
     * @return bool
     */
    public function hasProcessContext($processContext = null)
    {
        if ($processContext) {
            if ($this->_processContexts === false) {
                $this->getProcessContexts();
            }

            if (!empty($this->_processContexts)) {
                foreach ($this->_processContexts as $processContextDb) {
                    if ($processContextDb->getCode() == $processContext) {
                        return true;
                    }
                }
            }
        }

        return false;
    }
}
