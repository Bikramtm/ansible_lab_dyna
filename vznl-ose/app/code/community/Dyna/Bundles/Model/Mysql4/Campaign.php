<?php

/**
 * Class Dyna_Bundles_Model_Mysql4_Campaign
 */
class Dyna_Bundles_Model_Mysql4_Campaign extends Mage_Core_Model_Mysql4_Abstract
{
    /**
     * Dyna_Bundles_Model_Mysql4_Campaign constructor.
     */
    public function _construct()
    {
        $this->_init('dyna_bundles/campaign', 'entity_id');
    }
}
