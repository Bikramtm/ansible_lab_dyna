<?php

/**
 * Class Dyna_Bundles_Model_Mysql4_CampaignDealercode
 */
class Dyna_Bundles_Model_Mysql4_CampaignDealercode extends Mage_Core_Model_Mysql4_Abstract
{
    /**
     * Dyna_Bundles_Model_Mysql4_CampaignDealercode constructor.
     */
    public function _construct()
    {
        $this->_init('dyna_bundles/campaign_dealercode', 'entity_id');
    }
}
