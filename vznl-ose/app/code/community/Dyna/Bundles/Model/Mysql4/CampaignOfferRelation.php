<?php

/**
 * Class Dyna_Bundles_Model_Mysql4_CampaignOfferRelation
 */
class Dyna_Bundles_Model_Mysql4_CampaignOfferRelation extends Mage_Core_Model_Mysql4_Abstract
{
    /**
     * Dyna_Bundles_Model_Mysql4_CampaignOfferRelation constructor.
     */
    public function _construct()
    {
        $this->_init('dyna_bundles/campaign_offer_relation', 'entity_id');
    }
}
