<?php

/**
 * Class Dyna_Bundles_Model_Mysql4_BundleProcessContext
 */
class Dyna_Bundles_Model_Mysql4_BundleProcessContext extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {
        $this->_init('dyna_bundles/bundle_processcontext', 'entity_id');
    }
}
