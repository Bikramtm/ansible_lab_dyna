<?php
class Dyna_Bundles_Model_Mysql4_BundleCondition_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract {

    protected function _construct()
    {
        $this->_init('dyna_bundles/bundleCondition');
    }
}
