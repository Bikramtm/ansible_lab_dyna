<?php
/** @var Mage_Sales_Model_Entity_Setup $installer */

$installer = $this;
$installer->startSetup();

$bundleActionsTable = $installer->getTable('dyna_bundles/bundle_actions');

if (!$installer->getConnection()->tableColumnExists($bundleActionsTable, 'package_type')) {
    $installer->getConnection()
        ->addColumn($bundleActionsTable, 'package_type', array(
            'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
            'length' => 100,
            'comment' => 'Used for evaluating the bundle actions on the desired package type',
            'nullable' => true,
            'default' => null,
            'after' => 'condition'
        ));
}

$installer->endSetup();
