<?php

/** @var Mage_Catalog_Model_Resource_Setup $this */

$this->startSetup();

$this->getConnection()->addColumn($this->getTable("dyna_bundles/campaign_dealercode"), "secondary_dealer_code", array(
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT, 'comment' => ' '
));

$dealerCodeColumnExists = $this->getConnection()->tableColumnExists($this->getTable("dyna_bundles/campaign_dealercode"), "dealer_code");
$primaryDealerCodeColumnExists = $this->getConnection()->tableColumnExists($this->getTable("dyna_bundles/campaign_dealercode"), "primary_dealer_code");

if($dealerCodeColumnExists && !$primaryDealerCodeColumnExists) {
    $this->getConnection()->changeColumn($this->getTable("dyna_bundles/campaign_dealercode"), "dealer_code", "primary_dealer_code", array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT, 'length' => 255
    ));
}

$productCategoryColumnExists = $this->getConnection()->tableColumnExists($this->getTable("dyna_bundles/campaign_dealercode"), "product_category");
$packageTypeColumnExists = $this->getConnection()->tableColumnExists($this->getTable("dyna_bundles/campaign_dealercode"), "package_type");

if($productCategoryColumnExists && !$packageTypeColumnExists) {
    $this->getConnection()->changeColumn($this->getTable("dyna_bundles/campaign_dealercode"), "product_category", "package_type", array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT, 'length' => 255
    ));
}

$this->run("DROP TABLE IF EXISTS `bundle_campaign_offer_hint`;");

$this->endSetup();
