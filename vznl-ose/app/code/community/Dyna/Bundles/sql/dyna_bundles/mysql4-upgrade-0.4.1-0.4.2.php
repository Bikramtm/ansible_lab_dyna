<?php
/**
 * Copyright (c) 2017. Dynacommerce B.V.
 */

/** @var Mage_Sales_Model_Entity_Setup $this */

$this->startSetup();

// Updating bundle structure
$bundleActionsTable = $this->getTable('dyna_bundles/bundle_actions');

if ($this->getConnection()->tableColumnExists($bundleActionsTable, 'package_type')) {
    $this->getConnection()
        ->dropColumn($bundleActionsTable, 'package_type');
}

if (!$this->getConnection()->tableColumnExists($bundleActionsTable, 'package_type_id')) {
    $this->getConnection()
        ->addColumn($bundleActionsTable, 'package_type_id', array(
            'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
            'length' => 100,
            'comment' => 'Used for evaluating the bundle actions on the desired package type id',
            'nullable' => true,
            'default' => null,
            'after' => 'condition'
        ));
}

// Updating foreign key on bundle_packages
$this->getConnection()->dropForeignKey($this->getTable('dyna_bundles/bundle_packages'), $this->getFkName($this->getTable('dyna_bundles/bundle_packages'), 'bundle_rule_id', $this->getTable('dyna_bundles/bundle_rules'), 'id'));
$this->getConnection()->addForeignKey(
    $this->getFkName($this->getTable('dyna_bundles/bundle_packages'), 'bundle_rule_id', $this->getTable('dyna_bundles/bundle_rules'), 'id'),
    $this->getTable('dyna_bundles/bundle_packages'),
    'bundle_rule_id',
    $this->getTable('dyna_bundles/bundle_rules'),
    'id'
);

$this->endSetup();
