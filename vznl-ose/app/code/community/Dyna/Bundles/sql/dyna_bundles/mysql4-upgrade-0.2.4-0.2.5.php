<?php
/**
 * Adding campaign hint to campaign table
 */

/** @var Mage_Catalog_Model_Resource_Setup $this */

$this->startSetup();

$this->getConnection()->addColumn($this->getTable("dyna_bundles/campaign"), "offer_hint", array(
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT, 'comment' => ' '
));

$this->run("DROP TABLE IF EXISTS `bundle_campaign_offer_hint`;");

$this->endSetup();
