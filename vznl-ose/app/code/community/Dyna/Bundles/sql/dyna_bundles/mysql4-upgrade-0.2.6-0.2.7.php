<?php
/**
 * Removes:
 *  - products_category_in_installed_base
 *  - products_category_path_in_installed_base
 *
 * fields from bundle rules, replaces them with products_expression_in_installed_base
 */
/* @var $installer Mage_Sales_Model_Entity_Setup */
$installer = $this;
$installer->startSetup();

$bundleRulesTable = $installer->getTable('dyna_bundles/bundle_rules');

$fields = [
    'products_category_in_installed_base',
    'products_category_path_in_installed_base'
];
foreach($fields as $field) {
    $installer->getConnection()->dropColumn($bundleRulesTable, $field);
}

$installer->getConnection()
    ->addColumn($bundleRulesTable, 'products_expression_in_installed_base', [
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'comment' => 'Expression determining if the products in the install base are part of a bundle',
        'nullable' => false,
        'default' => '',
    ]);


