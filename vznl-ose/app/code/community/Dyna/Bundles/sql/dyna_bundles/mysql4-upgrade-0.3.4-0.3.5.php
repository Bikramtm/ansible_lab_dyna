<?php

/**
 * Installer that removes the bundle hint columns from bundle rules and creates a separate table for the hinting mechanism
 */

/* @var $installer Mage_Sales_Model_Entity_Setup */
$installer = $this;
$installer->startSetup();

$bundleRulesTable = $installer->getTable('dyna_bundles/bundle_rules');

$columns = [
    'hint_enabled',
    'hint_title',
    'hint_text',
    'hint_location',
];

// remove the columns from the bundle rules table if they exist
foreach ($columns as $column) {
    if ($installer->getConnection()->tableColumnExists($bundleRulesTable, $column)) {
        $installer->getConnection()->dropColumn($bundleRulesTable, $column);
    }
}

// create a new table, bundle_hints

if (!$installer->getConnection()->isTableExists($installer->getTable('dyna_bundles/bundle_hints'))) {
    $bundleHintsTable = new Varien_Db_Ddl_Table();
    $bundleHintsTable->setName($installer->getTable('dyna_bundles/bundle_hints'));

    $bundleHintsTable
        ->addColumn('id', Varien_Db_Ddl_Table::TYPE_BIGINT, 10, array(
            "unsigned" => true,
            "primary" => true,
            "auto_increment" => true,
            "nullable" => false,
        ), 'Bundle hint id')
        ->addColumn('bundle_rule_id', Varien_Db_Ddl_Table::TYPE_BIGINT, 10, array(
            "unsigned" => true,
            "nullable" => false
        ), 'Reference to the bundle rule to which this hint is related')
        ->addColumn('hint_enabled', Varien_Db_Ddl_Table::TYPE_BOOLEAN, null, array(
            "nullable" => false,
            "default" => 0
        ), 'Flag to determine if the current hint is enabled')
        ->addColumn('title', Varien_Db_Ddl_Table::TYPE_TEXT, 200, array(
            "nullable" => true,
            "default" => null
        ), 'Title of the bundle hint')
        ->addColumn('text', Varien_Db_Ddl_Table::TYPE_TEXT, 400, array(
            "nullable" => true,
            "default" => null
        ), 'The text of the bundle hint, a message to be displayed when the hint is enabled')
        ->addColumn('location', Varien_Db_Ddl_Table::TYPE_TEXT, 30, array(
            "nullable" => true,
            "default" => null
        ), 'Location of the hint')
        ->addForeignKey(
            $installer->getFkName($bundleHintsTable->getName(), 'bundle_rule_id', $bundleRulesTable, 'id'),
            'bundle_rule_id', $bundleRulesTable, 'id',
            Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
        );

    $this->getConnection()->createTable($bundleHintsTable);
}

$installer->endSetup();
