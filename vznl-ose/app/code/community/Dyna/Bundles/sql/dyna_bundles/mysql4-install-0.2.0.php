<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */
/**
 * Installer that creates the campaign fields resource table
 */

/** @var Mage_Core_Model_Resource_Setup $this */
$installer = $this;

$installer->startSetup();

$campaignDealercodeTable = new Varien_Db_Ddl_Table();
$campaignDealercodeTable->setName($this->getTable("dyna_bundles/campaign_dealercode"));

$campaignDealercodeTable
  ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_BIGINT, 10, [
    "unsigned" => true,
    "primary" => true,
    "auto_increment" => true,
    "nullable" => false,
  ], "Primary key for campaign_dealercode table")
  ->addColumn("agency", Varien_Db_Ddl_Table::TYPE_TEXT, null, [
  ])
  ->addColumn("city", Varien_Db_Ddl_Table::TYPE_TEXT, null, [
  ])
  ->addColumn("marketing_code", Varien_Db_Ddl_Table::TYPE_TEXT, null, [
  ])
  ->addColumn("cluster_category", Varien_Db_Ddl_Table::TYPE_TEXT, null, [
  ])
  ->addColumn("product_category", Varien_Db_Ddl_Table::TYPE_TEXT, null, [
  ])
   ->addColumn("scenario", Varien_Db_Ddl_Table::TYPE_TEXT, null, [
  ])
   ->addColumn("dealer_code", Varien_Db_Ddl_Table::TYPE_TEXT, null, [
  ])
   ->addColumn("comment1", Varien_Db_Ddl_Table::TYPE_TEXT, null, [
  ])
   ->addColumn("comment2", Varien_Db_Ddl_Table::TYPE_TEXT, null, [
  ])
;
if (!$installer->tableExists($this->getTable("dyna_bundles/campaign_dealercode"))) {
    $this->getConnection()->createTable($campaignDealercodeTable);
}


$campaignOfferTable = new Varien_Db_Ddl_Table();
$campaignOfferTable->setName($this->getTable("dyna_bundles/campaign_offer"));

$campaignOfferTable
  ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_BIGINT, 10, [
    "unsigned" => true,
    "primary" => true,
    "auto_increment" => true,
    "nullable" => false,
  ], "Primary key for campaign_dealercode table")
  ->addColumn("offer_id", Varien_Db_Ddl_Table::TYPE_TEXT, null, [
  ])
  ->addColumn("title_description", Varien_Db_Ddl_Table::TYPE_TEXT, null, [
  ])
  ->addColumn("usp1", Varien_Db_Ddl_Table::TYPE_TEXT, null, [
  ])
  ->addColumn("usp2", Varien_Db_Ddl_Table::TYPE_TEXT, null, [
  ])
  ->addColumn("discounted_months", Varien_Db_Ddl_Table::TYPE_INTEGER, null, [
  ])
  ->addColumn("discounted_price_per_month", Varien_Db_Ddl_Table::TYPE_DECIMAL, [12,4], [
  ])
  ->addColumn("undiscounted_price_per_month", Varien_Db_Ddl_Table::TYPE_DECIMAL, [12,4], [
  ])
;
if (!$installer->tableExists($this->getTable("dyna_bundles/campaign_offer"))) {
    $this->getConnection()->createTable($campaignOfferTable);
}

$campaignTable = new Varien_Db_Ddl_Table();
$campaignTable->setName($this->getTable("dyna_bundles/campaign"));

$campaignTable
  ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_BIGINT, 10, [
    "unsigned" => true,
    "primary" => true,
    "auto_increment" => true,
    "nullable" => false,
  ], "Primary key for campaign_dealercode table")
  ->addColumn("campaign_id", Varien_Db_Ddl_Table::TYPE_TEXT, null, [
  ])
  ->addColumn("cluster", Varien_Db_Ddl_Table::TYPE_TEXT, null, [
  ])
  ->addColumn("cluster_category", Varien_Db_Ddl_Table::TYPE_TEXT, null, [
  ])
  ->addColumn("advertising_code", Varien_Db_Ddl_Table::TYPE_TEXT, null, [
  ])
  ->addColumn("campaign_code", Varien_Db_Ddl_Table::TYPE_TEXT, null, [
  ])
  ->addColumn("campaign_hint", Varien_Db_Ddl_Table::TYPE_TEXT, null, [
  ])
  ->addColumn("marketing_code", Varien_Db_Ddl_Table::TYPE_TEXT, null, [
  ])
  ->addColumn("promotion_hint", Varien_Db_Ddl_Table::TYPE_TEXT, null, [
  ])
  ->addColumn("valid_from", Varien_Db_Ddl_Table::TYPE_DATE, null, [
  ])
  ->addColumn("valid_to", Varien_Db_Ddl_Table::TYPE_TEXT, null, [
  ])
;
if (!$installer->tableExists($this->getTable("dyna_bundles/campaign"))) {
    $this->getConnection()->createTable($campaignTable);
}



$campaignOfferRelationTable = new Varien_Db_Ddl_Table();
$campaignOfferRelationTable->setName($this->getTable("dyna_bundles/campaign_offer_relation"));

$campaignOfferRelationTable
  ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_BIGINT, 10, [
    "unsigned" => true,
    "primary" => true,
    "auto_increment" => true,
    "nullable" => false,
  ], "Primary key for campaign_dealercode table")
  ->addColumn('campaign_id', Varien_Db_Ddl_Table::TYPE_BIGINT, 10, [
    "unsigned" => true,
  ])
  ->addColumn('offer_id', Varien_Db_Ddl_Table::TYPE_BIGINT, 10, [
    "unsigned" => true,
  ])
  ->addForeignKey(
    $this->getFkName($campaignOfferRelationTable->getName(), 'campaign_id', 'dyna_bundles/campaign', 'entity_id'),
    'campaign_id', $this->getTable('dyna_bundles/campaign'), 'entity_id',
    Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
  )
  ->addForeignKey(
    $this->getFkName($campaignOfferRelationTable->getName(), 'campaign_offer_id', 'dyna_bundles/campaign_offer', 'entity_id'),
    'offer_id', $this->getTable('dyna_bundles/campaign_offer'), 'entity_id',
    Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
  )

;
if (!$installer->tableExists($this->getTable("dyna_bundles/campaign_offer_relation"))) {
    $this->getConnection()->createTable($campaignOfferRelationTable);
}

$campaignOfferProductTable = new Varien_Db_Ddl_Table();
$campaignOfferProductTable->setName($this->getTable("dyna_bundles/campaign_offer_product"));

$campaignOfferProductTable
  ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_BIGINT, 10, [
    "unsigned" => true,
    "primary" => true,
    "auto_increment" => true,
    "nullable" => false,
  ], "Primary key for campaign_dealercode table")
  ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, [
    "unsigned" => true,
  ])
  ->addColumn('offer_id', Varien_Db_Ddl_Table::TYPE_BIGINT, 10, [
    "unsigned" => true,
  ])
  ->addForeignKey(
    $this->getFkName($campaignOfferProductTable->getName(), 'product_id', 'catalog/product', 'entity_id'),
    'product_id', $this->getTable('catalog/product'), 'entity_id',
    Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
  )
  ->addForeignKey(
    $this->getFkName($campaignOfferProductTable->getName(), 'campaign_offer_id', 'dyna_bundles/campaign_offer', 'entity_id'),
    'offer_id', $this->getTable('dyna_bundles/campaign_offer'), 'entity_id',
    Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
  )
;
if (!$installer->tableExists($this->getTable("dyna_bundles/campaign_offer_product"))) {
    $this->getConnection()->createTable($campaignOfferProductTable);
}
$installer->endSetup();
