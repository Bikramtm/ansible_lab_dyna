<?php
/**
 * Add default flag on the bundle_campaign table
 */
/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();

$table = $installer->getTable("dyna_bundles/campaign");

if ($installer->tableExists($table)) {
    $installer->getConnection()->addColumn($table, 'default', [
        'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'length' => 1,
        'nullable' => false,
        'default' => 0,
        'comment' => 'Marks a default offer'
    ]);
}

$installer->endSetup();