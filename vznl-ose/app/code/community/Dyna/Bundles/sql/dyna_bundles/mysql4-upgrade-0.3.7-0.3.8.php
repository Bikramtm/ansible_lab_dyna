<?php

/** @var  Mage_Sales_Model_Entity_Setup $installer */
$installer = $this;

$bundleRulesTable = $installer->getTable('dyna_bundles/bundle_rules');
$processContextTable = $installer->getTable('dyna_catalog/processcontext');

if (!$installer->getConnection()->isTableExists('dyna_bundles/bundle_processcontext') &&
    $installer->getConnection()->isTableExists($processContextTable)
) {
    $bundleProcessContextTable = new Varien_Db_Ddl_Table();
    $bundleProcessContextTable->setName($installer->getTable('dyna_bundles/bundle_processcontext'));

    $bundleProcessContextTable
        ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_BIGINT, 20, array(
            "unsigned" => true,
            "primary" => true,
            "auto_increment" => true,
            "nullable" => false,
        ), 'Bundle to process context link id')
        ->addColumn('bundle_rule_id', Varien_Db_Ddl_Table::TYPE_BIGINT, 20, array(
            "unsigned" => true,
            "nullable" => false
        ), 'Reference to the bundle rule')
        ->addColumn('process_context_id', Varien_Db_Ddl_Table::TYPE_BIGINT, 20, array(
            "unsigned" => true,
            "nullable" => false
        ), 'Reference to the entity id of process context')
        ->addForeignKey($installer->getFkName($bundleProcessContextTable->getName(), 'bundle_rule_id', $bundleRulesTable, 'id'),
            'bundle_rule_id', $bundleRulesTable, 'id',
            Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
        ->addForeignKey($installer->getFkName($bundleProcessContextTable->getName(), 'process_context_id', $processContextTable, 'entity_id'),
            'process_context_id', $processContextTable, 'entity_id',
            Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE);

    $installer->getConnection()->createTable($bundleProcessContextTable);
}

// remove lifecycle and bundle benefits from bundle_rules
if ($installer->getConnection()->isTableExists($bundleRulesTable)) {
    $columnsToRemove = [
        'based_on_a_lifecycle',
        'display_bundle_benefits1',
        'display_bundle_benefits2'
    ];

    foreach ($columnsToRemove as $column) {
        if ($installer->getConnection()->tableColumnExists($bundleRulesTable, $column)) {
            $installer->getConnection()->dropColumn($bundleRulesTable, $column);
        }
    }
}

$installer->endSetup();
