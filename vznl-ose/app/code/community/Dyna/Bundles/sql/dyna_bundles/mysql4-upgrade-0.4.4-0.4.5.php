<?php

/** @var Mage_Catalog_Model_Resource_Setup $this */
$installer = $this;
$installer->startSetup();
$table = 'bundle_packages';
$connection = $installer->getConnection();
if ($installer->tableExists($table) && !$connection->tableColumnExists($table, 'mandatory_families')) {
    $connection ->addColumn(
        $installer->getTable('bundle_packages'),
        "mandatory_families",
        array(
            'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
            'default'   => NULL,
            'comment'   => 'Removed bundle rule parser (mandatory families) from magento session and added to this field'
        )
    );
}

$installer->endSetup();