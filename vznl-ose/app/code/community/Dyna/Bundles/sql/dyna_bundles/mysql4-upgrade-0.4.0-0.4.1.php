<?php
$installer = $this;

$installer->startSetup();

$campaigntable = new Varien_Db_Ddl_Table();
$campaigntable->setName($this->getTable("dyna_bundles/campaign"));


$this->getConnection()->addColumn($this->getTable("dyna_bundles/campaign"), "campaign_title", array(
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT, 'comment' => ' ', 'after' => 'campaign_id'
));
$this->getConnection()->addColumn($this->getTable("dyna_bundles/campaign"), "promotion_hint1", array(
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT, 'comment' => ' ', 'after' => 'promotion_hint'
));
$this->getConnection()->addColumn($this->getTable("dyna_bundles/campaign"), "promotion_hint2", array(
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT, 'comment' => ' ', 'after' => 'promotion_hint1'
));
$this->getConnection()->addColumn($this->getTable("dyna_bundles/campaign"), "promotion_hint3", array(
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT, 'comment' => ' ', 'after' => 'promotion_hint2'
));



$campaignoffertable = new Varien_Db_Ddl_Table();
$campaignoffertable->setName($this->getTable("dyna_bundles/campaign_offer"));

$this->getConnection()->addColumn($this->getTable("dyna_bundles/campaign_offer"), "subtitle_description", array(
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT, 'comment' => ' ', 'after' => 'title_description'
));

$this->getConnection()->addColumn($this->getTable("dyna_bundles/campaign_offer"), "usp3", array(
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT, 'comment' => ' ', 'after' => 'usp2'
));
$this->getConnection()->addColumn($this->getTable("dyna_bundles/campaign_offer"), "usp4", array(
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT, 'comment' => ' ', 'after' => 'usp3'
));
$this->getConnection()->addColumn($this->getTable("dyna_bundles/campaign_offer"), "usp5", array(
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT, 'comment' => ' ', 'after' => 'usp4'
));
$this->getConnection()->addColumn($this->getTable("dyna_bundles/campaign_offer"), "usp1pakagetype", array(
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT, 'comment' => ' ', 'after' => 'usp1'
));
$this->getConnection()->addColumn($this->getTable("dyna_bundles/campaign_offer"), "usp2pakagetype", array(
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT, 'comment' => ' ', 'after' => 'usp2'
));
$this->getConnection()->addColumn($this->getTable("dyna_bundles/campaign_offer"), "usp3pakagetype", array(
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT, 'comment' => ' ', 'after' => 'usp3'
));
$this->getConnection()->addColumn($this->getTable("dyna_bundles/campaign_offer"), "usp4pakagetype", array(
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT, 'comment' => ' ', 'after' => 'usp4'
));
$this->getConnection()->addColumn($this->getTable("dyna_bundles/campaign_offer"), "usp5pakagetype", array(
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT, 'comment' => ' ', 'after' => 'usp5'
));


$installer->endSetup();

?>