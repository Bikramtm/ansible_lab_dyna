<?php

/** @var Mage_Catalog_Model_Resource_Setup $this */
$installer = $this;
$installer->startSetup();
if ($installer->tableExists($installer->getTable('bundle_campaign_offer_relation'))) {
    $installer->getConnection()
        ->addIndex(
            $installer->getTable('bundle_campaign_offer_relation'),
            "IDX_DYNA_BUNDLE_CAMPAIGN_OFFER_RELATION",
            array('campaign_id', 'offer_id'),
            Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
        );
}

$installer->endSetup();
