<?php

require_once Mage::getModuleDir('controllers', 'Omnius_Bundles') . DS . 'Adminhtml' . DS . 'BundlesController.php';

class Dyna_Bundles_Adminhtml_CampaignsController extends Omnius_Bundles_Adminhtml_BundlesController
{
    /**
     * @return $this
     */
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('bundles/campaigns')
            ->_addBreadcrumb(Mage::helper('bundles')->__('Manage Campaigns'),
                Mage::helper('bundles')->__('Manage Campaigns'));

        return $this;
    }

    /**
     * Display the admin grid (table) with all the announcements
     */
    public function indexAction()
    {
        $this->_initAction();
        $this->renderLayout();
    }

    /**
     * Initialize the addition of a new record
     */
    public function newAction()
    {
        $this->_title(Mage::helper('bundles')->__('New Campaign'));

        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('dyna_bundles/campaign')->load($id);
        $data = Mage::getSingleton('adminhtml/session')->getFormData(true);

        if (!empty($data)) {
            $model->setData($data);
        }

        Mage::register('bundle_campaigns_data', $model);
        $this->loadLayout();
        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
        $this->_addContent($this->getLayout()->createBlock('dyna_bundles/adminhtml_campaigns_edit'))
             ->_addLeft($this->getLayout()->createBlock('dyna_bundles/adminhtml_campaigns_edit_tabs'));

        $this->renderLayout();

    }

    /**
     * Trigger edit mode for a record
     */
    public function editAction()
    {
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('dyna_bundles/campaign')->load($id);

        if ($model->getId()) {
            Mage::register('bundle_campaigns_data', $model);
            $this->loadLayout();
            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
            $this->_addLeft($this->getLayout()->createBlock('dyna_bundles/adminhtml_campaigns_edit_tabs'));
            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('bundles')->__('Item does not exist.'));
            $this->_redirect('*/*/');
        }
    }

    /**
     * Trigger save mode for a record
     */
    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            try {
                unset($data['form_key']);
                /** @var Dyna_Bundles_Model_Campaign $model */
                $model = Mage::getModel('dyna_bundles/campaign')
                    ->addData($data)
                    ->setId($this->getRequest()->getParam('id'))
                    ->save();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('bundles')->__('The bundle campaign was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', ['id' => $model->getId()]);

                    return;
                }
            } catch (Exception $e) {
                $this->_throwErrror($e->getMessage(), $data);

                return;
            }
            $this->_redirect('*/*/');

            return;
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('bundles')->__('Unable to find campaign to save'));
        $this->_redirect('*/*/');
    }

    public function deleteAction()
    {
        if ($this->getRequest()->getParam("id")) {
            $option = Mage::getModel("dyna_bundles/campaign")->load($this->getRequest()->getParam("id"));
            $option->delete();
            Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('bundles')->__('Campaign successfully deleted.'));
            Mage::getSingleton('adminhtml/session')->setFormData(false);
            Mage::unregister("bundle_campaigns_data");
        }

        $this->_redirect("*/*/");
    }

    /**
     * Method that throws the error based on a message
     *
     * @param mixed $message
     */
    private function _throwErrror($message, $data)
    {
        Mage::getSingleton('adminhtml/session')->addError($message);
        Mage::getSingleton('adminhtml/session')->setFormData($data);
        $this->_redirect('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
    }
}
