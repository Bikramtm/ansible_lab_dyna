<?php

/**
 * Class Dyna_Bundles_Block_Redplus
 * @method getSharingGroupId() : int Returns the sharing group id of the current package
 * @method getKiasProducts() : array Returns the KIAS products saved for the customer
 * @method getActivePackageParentId() : int Returns the id of the parent of the current active package
 */
class Dyna_Bundles_Block_Redplus extends Mage_Core_Block_Template
{
    /** @var  Dyna_Bundles_Helper_Redplus */
    protected $redPlusHelper;
    protected $sortedPackages = null;

    public function __construct()
    {
        $this->redPlusHelper = Mage::helper('dyna_bundles/redplus');
    }

    /**
     * @return Varien_Data_Collection
     */
    public function getEligiblePackages()
    {
        return $this->redPlusHelper->getRedPlusEligiblePackages();
    }

    /**
     * Method used to check whether the parent of active package is
     * eligible for Red+ owner
     *
     * @return bool
     */
    public function currentParentIsEligible()
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $parentPackage = $quote->getCartPackageByEntityId($quote->getActivePackage()->getParentId());

        if ($parentPackage) {
            foreach ($this->getEligiblePackages() as $eligiblePackage) {
                if ($eligiblePackage->getId() == $parentPackage->getId()) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Checks whether the current parent of the active package is a dummy
     *
     * @return bool
     */
    public function currentParentIsDummy()
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();

        return $quote->getCartPackageByEntityId($quote->getActivePackage()->getParentId())->getEditingDisabled();
    }

    /**
     * Checks whether the current parent package is ILS
     *
     * @return bool
     */
    public function currentParentIsILS()
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();

        return ($quote->getCartPackageByEntityId($quote->getActivePackage()->getParentId())->getSaleType() !== Dyna_Catalog_Model_ProcessContext::ACQ);
    }

    /**
     * @param Varien_Data_Collection $eligiblePackages
     * @return Varien_Data_Collection
     */
    public function injectCurrentParentPackage($eligiblePackages)
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();

        $currentParentPackage = $quote->getCartPackageByEntityId($quote->getActivePackage()->getParentId());
        $currentParentPackage->setData('forceDisabled', true);

        foreach ($currentParentPackage->getAllItems() as $item) {
            if ($item->getProduct()->is(Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_SUBSCRIPTION)) {
                $currentParentPackage->setData('eligibleProductSku', $item->getSku());
                break;
            }
        }

        $this->sortedPackages = new Varien_Data_Collection();
        $isAdded = false;

        foreach ($eligiblePackages as $package) {
            if ($currentParentPackage->getPackageId() == 1 && !$isAdded) {
                $this->sortedPackages->addItem($currentParentPackage);
                $isAdded = true;
            } else {
                if ($currentParentPackage->getPackageId() > $package->getPackageId() && !$isAdded) {
                    $this->sortedPackages->addItem($currentParentPackage);
                    $isAdded = true;
                }
            }
            $this->sortedPackages->addItem($package);
        }

        if ($this->sortedPackages->getSize() == 0) {
            $this->sortedPackages->addItem($currentParentPackage);
        }

        return $this->sortedPackages;
    }

    /**
     * Return eligible packages for red+ after injection of the parent red+ package
     * @return null
     */
    public function getRedPlusEligiblePackages()
    {
        return $this->sortedPackages;
    }

    /**
     * @param Varien_Data_Collection $packages
     * @return mixed
     */
    public function removeILSPackages($packages)
    {
        foreach ($packages as $key => $package) {
            if ($package->isIlsSaleType()) {
                $packages->removeItemByKey($key);
            }
        }

        return $packages;
    }

    /**
     * @param $packages
     * @return mixed
     */
    public function removeProlongationPackages($packages)
    {
        /**
         * @var  $key
         * @var Dyna_Package_Model_Package $package
         */
        foreach ($packages as $key => $package) {
            if (in_array($package->getSaleType(), Dyna_Catalog_Model_ProcessContext::getProlongationProcessContexts())) {
                $packages->removeItemByKey($key);
            }
        }

        return $packages;
    }
}
