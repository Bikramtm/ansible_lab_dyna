<?php

/**
 * @method getEligibleBundles()
 * Class Dyna_Bundles_Block_Hint
 */
class Dyna_Bundles_Block_Hint extends Mage_Core_Block_Template
{
    private $packageTypes;

    public function getBundlesToDisplay()
    {
        $eligibleBundles = $this->filterBundlesByHintLocation(Dyna_Bundles_Model_BundleRule::LOCATION_DRAW, $this->getEligibleBundles());

        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = $this->getQuote();
        $activePackage = $quote->getActivePackage();
        $activePackageId = $quote->getActivePackageId();
        /** @var Dyna_Customer_Model_Customer $customer */
        $customer = Mage::getSingleton('dyna_customer/session')->getCustomer();
        $serviceAddress = Mage::getSingleton('dyna_address/storage')->getServiceAddress(true);

        $bundles = array();
        $quotePackages = $quote->getCartPackages();
        $bundledPackages = array();
        //get bundled packages from cart
        foreach ($quotePackages as $package) {
            foreach ($package->getBundles() as $bundle) {
                if ($bundle->getId()) {
                    $bundledPackages[$package->getPackageId()] = $package->isPartOfRedPlus();
                }
            }
        }

        foreach ($eligibleBundles as $eligibleBundle) {
            // inter stack bundle
            if (!empty($eligibleBundle['targetedPackagesInCart'])) {
                foreach ($eligibleBundle['targetedPackagesInCart'] as $bundleCartPackage) {
                    if ($bundleCartPackage['packageId'] == $activePackageId || (in_array($bundleCartPackage['packageId'], array_keys($bundledPackages)) && !$bundledPackages[$bundleCartPackage['packageId']] )) {
                        continue;
                    }
                    $bundle = Mage::getModel('dyna_bundles/bundleRule')->load((int)$eligibleBundle['bundleId']);

                    // Determine the Tariff product in quote package
                    $quoteItem = $this->getActivePackageTariff();
                    $quoteBaseItem = $this->getTariffForPackage($bundleCartPackage['packageId']);
                  
                    $quoteBasePackage['product_sku'] = $quoteBaseItem->getSku();
                    $quoteBasePackage['active_bundle_id'] = $eligibleBundle['bundleId'];
                    $quoteBasePackage['package_id'] = $quoteBasePackage['index_id'] = $quoteBaseItem->getPackageId();
                    $quoteBasePackage['ctn'] = $this->getCtnForPackage($bundleCartPackage['packageId'], $customer);
                    $quoteBasePackage['package_type'] = $quoteBaseItem->getPackageType();
                    $quoteBasePackage['package_type_name'] = $this->getPackageNameByCode($quoteBasePackage['package_type']);
                    $quoteBasePackage['product_id'] = $quoteBaseItem->getProductId();
                    $quoteBasePackage['title'] = $quoteBaseItem->getName();
                    $quoteBasePackage['is_interstack'] = true;
                    $quoteBasePackage['account_number'] = $customer->getBan();
                    $quoteBasePackage['address'] = !in_array(strtolower($quoteBaseItem->getPackageType()), Dyna_Catalog_Model_Type::getMobilePackages()) ? $serviceAddress : null;
                    $packageType = $activePackage->getType();

                    $quotePackage = [
                        'type' => $packageType,
                        'ctn' => $customer->getPhoneNumber(),
                        'package_type_name' => $this->getPackageNameByCode($packageType),
                        'product' => $quoteItem ? $quoteItem->getProduct()->getDisplayNameConfigurator() : $quoteItem->getName(),
                        'address' => !in_array($packageType, Dyna_Catalog_Model_Type::getMobilePackages()) ? $serviceAddress : null,
                        'account_number' => $customer->getBan(),
                        'description' => $quoteItem ? $quoteItem->getProduct()->getServiceDescription() : ''
                    ];

                    $bundle
                        ->setQuoteBasePackages([$quoteBasePackage])
                        ->setQuotePackage($quotePackage)
                        ->setIsInterStackBundle(true);

                    $bundles[] = $bundle;
                }
            }
            // install base bundle
            if (!empty($eligibleBundle['targetedSubscriptions'])) {
                $allInstallBaseProducts = $customer->getAllInstallBaseProducts();
                foreach ($eligibleBundle['targetedSubscriptions'] as $targetedSubscription) {
                    foreach ($allInstallBaseProducts as $installBaseProduct) {
                        if ($installBaseProduct['contract_id'] == $targetedSubscription['customerNumber'] && $installBaseProduct['product_id'] == $targetedSubscription['productId']) {
                            /** @var Dyna_Bundles_Model_BundleRule $bundle */
                            $bundle = Mage::getModel('dyna_bundles/bundleRule')->load((int)$eligibleBundle['bundleId']);

                            $bundle
                                ->setCtn($installBaseProduct['ctn'])
                                ->setPackageType($installBaseProduct['package_type'])
                                ->setBundleId($eligibleBundle['bundleId'])
                                ->setSubscriptionNumber($targetedSubscription['customerNumber'])
                                ->setProductId($targetedSubscription['productId']);
                            $installBasePackages = [];
                            foreach ($installBaseProduct['products'] as $product) {
                                if ($product['product_package_subtype'] == 'tariff') {
                                    $product['contract_id'] = $installBaseProduct['contract_id'];
                                    $product['entity_id'] = $product['product_id'];
                                    $product['product_id'] = $installBaseProduct['product_id'];
                                    $product['ctn'] = $installBaseProduct['ctn'];
                                    $product['package_type'] = $installBaseProduct['package_type'];
                                    $product['package_type_icon'] = strtolower(trim($installBaseProduct['package_type_icon']) !== '' ? $installBaseProduct['package_type_icon'] : $installBaseProduct['package_type']);
                                    $product['type'] = $installBaseProduct['package_type'];
                                    $product['active_bundle_id'] = $eligibleBundle['bundleId'];
                                    $product['address'] = !in_array(strtolower($installBaseProduct['package_type']), Dyna_Catalog_Model_Type::getMobilePackages()) ? $serviceAddress : null;

                                    $installBasePackages[] = $product;
                                }
                            }
                            $bundle->setInstalledBasePackages($installBasePackages);
                            foreach ($installBaseProduct['products'] as $product) {
                                if ($product['product_package_subtype'] == 'tariff') {
                                    $bundle->setTitle($product['title']);
                                    break;
                                }
                            }
                            $packageType = $quote->getPackageType($activePackage->getPackageId());
                            $quoteItem = null;
                            $items = $quote->getPackageItems($activePackage->getPackageId());

                            foreach ($items as $item) {
                                if ($item->getProduct()->isSubscription()) {
                                    $quoteItem = $item;
                                    break;
                                }
                            }

                            $quotePackage = [
                                'package_id' => $activePackage->getPackageId(),
                                'type' => $packageType,
                                'package_type_name' => $this->getPackageNameByCode($packageType),
                                'product' => $quoteItem ? $quoteItem->getName() : '',
                                'address' => !in_array($packageType, Dyna_Catalog_Model_Type::getMobilePackages()) ? $serviceAddress : null,
                                'ctn' => $customer->getPhoneNumber(), // todo ??
                                'description' => $quoteItem->getProduct()->getServiceDescription(),
                                'is_interstack' => false
                            ];

                            $bundle->setQuotePackage($quotePackage);
                            $bundles[] = $bundle;

                        }
                    }
                }
            }
        }

        return $bundles;
    }

    protected function getQuote()
    {
        return Mage::getSingleton('checkout/cart')->getQuote();
    }

    public function getPackageNameByCode($code)
    {
        /** @var Dyna_Package_Helper_Data $packageHelper */
        $packageHelper = Mage::helper('dyna_package');
        //get package types from db
        if (!$this->packageTypes) {
            $this->packageTypes = array_change_key_case($packageHelper->getPackageTypes(), CASE_LOWER);
        }

        return $this->packageTypes[strtolower($code)];
    }

    /**
     * @todo: check if method is still needed
     * @param $package
     * @return array
     */
    public function getPackageQuoteItems($package)
    {
        $items = $this->getQuote()->getPackageItems($package);
        $products = [];

        foreach ($items as $item) {
            $products['product_sku'] = $item->getSku();
        }

        return $products;
    }

    /**
     * Returns the source product and other identifiers (like subscription number, CTN, etcn) for bundle drawer
     * @return array (
     * 'tariffSku' => 'tarrifName',
     *      'ctn' => 'CTN',
     *      'intrastackPackaId' => 1,null
     *      'bundleId' => ...
     *      'subscriptionNumber' => ....
     *      'product_id' =>
     *      'data-inter-stack-package-id' => package from cart
     *      'more_details'
     * )
     * @todo Implement it
     */
    public function getSourcePackageProductsForInstallBase(array $installBaseProducts, $bundleId, $subscriptionNumber, $productId)
    {

    }

    /**
     * This method will return the cart details after bundle has been created
     * Will use the installBase and cart expression objects for evaluating the conditions for bundle actions
     * Will use dummy installBase and cart expression objects for returning the products that will be added to cart
     * @param $bundleId
     * @param $subscriptionNumber
     * @param $productId
     * @param $interStackPackageId
     * @return array (
     * )
     */
    public function getMoreDetails($bundleId, $subscriptionNumber, $productId, $interStackPackageId)
    {

    }

    /**
     * @param string $hintLocation
     * @param array $bundles
     * @return array
     */
    public function filterBundlesByHintLocation(string $hintLocation, array $bundles): array
    {
        $filteredBundles = array();

        foreach ($bundles as $bundle) {
            if (in_array($hintLocation, $bundle['addButtonInSection'])) {
                $filteredBundles[] = $bundle;
            }
        }

        return $filteredBundles;
    }

    public function getActivePackageTariff()
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $items = $quote->getActivePackage()->getItems();

        foreach ($items as $item) {
            if ($item->getProduct()->isSubscription()) {
                return $item;
            }
        }

        return null;
    }

    /*
     * Get tariff for specified package
     */
    public function getTariffForPackage($packageId)
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $items = $quote->getCartPackage($packageId)->getAllItems();

        foreach ($items as $item) {
            if ($item->getProduct()->isSubscription()) {
                return $item;
            }
        }

        return null;
    }

    /**
     * Get install base ctn if current package is in inlife flow
     * @param int $packageId
     * @param Dyna_Customer_Model_Customer $customer
     */
    public function getCtnForPackage($packageId, $customer)
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $package = $quote->getCartPackage($packageId);

        if ($package && (($parentAccountNumber = $package->getParentAccountNumber()) && $serviceLineId = $package->getServiceLineId()) && $installBaseProduct = $customer->getInstalledBaseProduct($parentAccountNumber, $serviceLineId)) {
            return $installBaseProduct['ctn'] ?? null;
        }

        return null;
    }
}
