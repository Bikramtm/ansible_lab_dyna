<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Bundles_Block_Adminhtml_Offers_Grid
 */
class Dyna_Bundles_Block_Adminhtml_Offers_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Dyna_Bundles_Block_Adminhtml_Offers_Grid constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('bundleOffersGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    /**
     * Prepare grid collection object
     *
     * @return $this
     */
    protected function _prepareCollection()
    {
        /** @var Dyna_Bundles_Model_CampaignOffer $collection */
        $collection = Mage::getModel('dyna_bundles/campaignOffer')->getCollection();
        $this->setCollection($collection);
        parent::_prepareCollection();

        return $this;
    }

    /**
     * Create the admin grid
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */

    protected function _prepareColumns()
    {
        parent::_prepareColumns();

        $this->addColumn('entity_id', array(
            'header' => $this->_getColumnTitle('ID'),
            'align' => 'left',
            'width' => '30px',
            'type' => 'number',
            'index' => 'entity_id',
        ));

        $this->addColumn('offer_id', array(
            'header' => Mage::helper('bundles')->__('Offer Identification'),
            'align' => 'left',
            'index' => 'offer_id',
        ));

        $this->addColumn('title_description', array(
            'header' => $this->_getColumnTitle('Title description'),
            'align' => 'left',
            'index' => 'title_description',
        ));

        $this->addColumn('usp1', array(
            'header' => $this->_getColumnTitle('USP 1'),
            'align' => 'left',
            'index' => 'usp1',
        ));

        $this->addColumn('usp2', array(
            'header' => $this->_getColumnTitle('USP 2'),
            'align' => 'left',
            'index' => 'usp2',
        ));

        $this->addColumn('discounted_months', array(
            'header' => $this->_getColumnTitle('Discounted Months'),
            'align' => 'left',
            'index' => 'discounted_months',
        ));

        $this->addColumn('discounted_price_per_month', array(
            'header' => $this->_getColumnTitle('Discounted price per month'),
            'align' => 'left',
            'index' => 'discounted_price_per_month',
            'width' => '10px'
        ));

        $this->addColumn('undiscounted_price_per_month', array(
            'header' => $this->_getColumnTitle('Undiscounted price per month'),
            'align' => 'left',
            'index' => 'undiscounted_price_per_month',
            'width' => '10px'
        ));

        $this->addColumn('product_ids', array(
            'header' => $this->_getColumnTitle('Product IDs'),
            'align' => 'left',
            'index' => 'product_ids',
            'width' => '250px'
        ));

        return $this;
    }
    /**
     * Get edit url for a record
     * @param $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    function _getColumnTitle($text){
        return Mage::helper('bundles')->__($text);
    }
}
