<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Bundles_Block_Adminhtml_Rules_Actions
 */
class Dyna_Bundles_Block_Adminhtml_Rules_Actions extends Mage_Adminhtml_Block_Widget_Grid
{
    private $_ruleData = null;

    /**
     * Dyna_Bundles_Block_Adminhtml_Rules_Actions constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('bundleActionGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    /**
     * Prepare grid collection object
     *
     * @return $this
     */
    protected function _prepareCollection()
    {
        if (Mage::getSingleton("adminhtml/session")->getRulesData()) {
            $this->_ruleData = Mage::getSingleton("adminhtml/session")->getRulesData();
            Mage::getSingleton("adminhtml/session")->getRulesData(null);
        } elseif (Mage::registry("bundle_rules_data")) {
            $this->_ruleData = Mage::registry("bundle_rules_data")->getData();
        }
        
        /** @var Dyna_Bundles_Model_BundleAction $collection */
        $collection = Mage::getModel('dyna_bundles/bundleAction')
            ->getCollection()
            ->addFieldToFilter('bundle_rule_id', $this->_ruleData['id']);

        $this->setCollection($collection);
        parent::_prepareCollection();

        return $this;
    }

    /**
     * Create the admin grid
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */

    protected function _prepareColumns()
    {

        parent::_prepareColumns();

        $this->addColumn('id_grid', array(
            'header' => $this->_getColumnTitle('ID'),
            'align' => 'left',
            'width' => '30px',
            'index' => 'id',
        ));

        $this->addColumn('scope_grid', array(
            'header' => $this->_getColumnTitle('Scope'),
            'align' => 'left',
            'index' => 'scope',
        ));

        $this->addColumn('condition_grid', array(
            'header' => $this->_getColumnTitle('Condition'),
            'align' => 'left',
            'index' => 'condition',
        ));

        $this->addColumn('action_grid', array(
            'header' => $this->_getColumnTitle('Action'),
            'align' => 'left',
            'index' => 'action',
        ));

        $this->addColumn('priority_grid', array(
            'header' => $this->_getColumnTitle('Priority'),
            'align' => 'left',
            'index' => 'priority',
            'width' => '20px'
        ));


        return $this;
    }

    function _getColumnTitle($text){
        return Mage::helper('bundles')->__($text);
    }
}
