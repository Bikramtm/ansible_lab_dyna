<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Bundles_Block_Adminhtml_Dealercode_Grid
 */
class Dyna_Bundles_Block_Adminhtml_Dealercode_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Dyna_Bundles_Block_Adminhtml_Dealercode_Grid constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('bundleDealercodeGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    /**
     * Prepare grid collection object
     *
     * @return $this
     */
    protected function _prepareCollection()
    {
        /** @var Dyna_Bundles_Model_BundleAction $collection */
        $collection = Mage::getModel('dyna_bundles/campaignDealercode')->getCollection();
        $this->setCollection($collection);
        parent::_prepareCollection();

        return $this;
    }

    /**
     * Create the admin grid
     *
     * @return Dyna_Bundles_Block_Adminhtml_Dealercode_Grid
     */

    protected function _prepareColumns()
    {
        parent::_prepareColumns();

        $columns = [
            'entity_id' => array(
                'header' => $this->_getColumnTitle('ID'),
                'align' => 'left',
                'width' => '30px',
                'type' => 'number',
                'index' => 'entity_id',
            ),
            'agency' => array(
                'header' => Mage::helper('bundles')->__('Agency'),
                'align' => 'left',
                'index' => 'agency',
            ),
            'city' => array(
                'header' => $this->_getColumnTitle('City'),
                'align' => 'left',
                'index' => 'city',
            ),
            'marketing_code' => array(
                'header' => $this->_getColumnTitle('Marketing code'),
                'align' => 'left',
                'index' => 'marketing_code',
            ),
            'cluster_category' => array(
                'header' => $this->_getColumnTitle('Cluster Category'),
                'align' => 'left',
                'index' => 'cluster_category',
            ),
            'red_sales_id' => array(
                'header' => $this->_getColumnTitle('Red Sales Id'),
                'align' => 'left',
                'index' => 'red_sales_id',
            ),
            'comment1' => array(
                'header' => $this->_getColumnTitle('Comment 1'),
                'align' => 'left',
                'index' => 'comment1',
            ),
            'comment2' => array(
                'header' => $this->_getColumnTitle('Comment 2'),
                'align' => 'left',
                'index' => 'comment2',
            )
        ];

        foreach ($columns as $columnKey => $columnValue) {
            $this->addColumn($columnKey, $columnValue);
        }

        return $this;
    }
    /**
     * Get edit url for a record
     * @param $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    /**
     * Customize mass remove/process actions
     * @return $this
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('ids');
        $this->getMassactionBlock()->setUseSelectAll(true);

        return $this;
    }


    function _getColumnTitle($text){
        return Mage::helper('bundles')->__($text);
    }
}
