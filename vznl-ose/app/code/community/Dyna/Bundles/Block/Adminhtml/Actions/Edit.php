<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Bundles_Block_Adminhtml_Actions_Edit
 */
class Dyna_Bundles_Block_Adminhtml_Actions_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * Override the constructor to customize the grid
     * Dyna_Bundles_Block_Adminhtml_Actions_Edit constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_objectId = 'id';
        $this->_blockGroup = 'dyna_bundles';
        $this->_controller = 'adminhtml_actions';
        $this->_updateButton('save', 'label', Mage::helper('bundles')->__('Save Item'));
        $this->_updateButton('delete', 'label', Mage::helper('bundles')->__('Delete Item'));

        $this->_addButton('saveandcontinue', array(
            'label' => Mage::helper('bundles')->__('Save And Continue Edit'),
            'onclick' => 'saveAndContinueEdit()',
            'class' => 'save',
        ), -100);

        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').readAttribute('action')+'back/edit/');
            }
        ";
    }

    /**
     * Adds a custom header on the new/edit form
     * @return string
     */
    public function getHeaderText()
    {
        if (Mage::registry('bundle_actions_data') && Mage::registry('bundle_actions_data')->getId()) {
            return Mage::helper('bundles')->__('Edit Item "%s"', $this->htmlEscape(Mage::registry('bundle_actions_data')->getId()));
        } else {
            return Mage::helper('bundles')->__('Add Item');
        }
    }
}
