<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Bundles_Block_Adminhtml_Offers
 */
class Dyna_Bundles_Block_Adminhtml_Offers extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * Dyna_Bundles_Block_Adminhtml_Offers constructor.
     */
    public function __construct()
    {
        $this->_controller = 'adminhtml_offers';
        $this->_blockGroup = 'dyna_bundles';
        $this->_headerText = Mage::helper('bundles')->__('Manage Offers');

        parent::__construct();
    }
}
