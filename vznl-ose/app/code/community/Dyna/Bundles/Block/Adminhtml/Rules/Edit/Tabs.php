<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Bundles_Block_Adminhtml_Rules_Edit_Tabs
 */
class Dyna_Bundles_Block_Adminhtml_Rules_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * Dyna_Bundles_Block_Adminhtml_Rules_Edit_Tabs constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId("bundles_rules_edit_tabs");
        $this->setDestElementId("edit_form");
        $this->setTitle(Mage::helper("bundles")->__("Bundle Rule InformationS"));
    }

    /**
     * Before rendering html, but after trying to load cache
     *
     * @return Mage_Core_Block_Abstract
     * @throws Exception
     */
    protected function _beforeToHtml()
    {
        $this->addTab("edit_form", array(
            "label" => Mage::helper("bundles")->__("Bundle Rule Information"),
            "title" => Mage::helper("bundles")->__("Bundle Rule Information"),
            'content' => $this->getLayout()->createBlock('dyna_bundles/adminhtml_rules_edit_tab_form')->toHtml(),
        ));

        $this->addTab("edit_form_actions_view", array(
            "label" => Mage::helper("bundles")->__("See linked actions"),
            "title" => Mage::helper("bundles")->__("See linked actions"),
            'content' => $this->getLayout()->createBlock('dyna_bundles/adminhtml_rules_actions')->toHtml(),
        ));
        return parent::_beforeToHtml();
    }

}
