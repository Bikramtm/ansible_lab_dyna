<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Bundles_Block_Adminhtml_Campaigns
 */
class Dyna_Bundles_Block_Adminhtml_Campaigns extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * Dyna_Bundles_Block_Adminhtml_Campaigns constructor.
     */
    public function __construct()
    {
        $this->_controller = 'adminhtml_campaigns';
        $this->_blockGroup = 'dyna_bundles';
        $this->_headerText = Mage::helper('bundles')->__('Manage Campaigns');
        
        parent::__construct();
    }
}
