<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Dyna_Bundles_Block_Adminhtml_Actions_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Class constructor
     *
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setDestElementId('edit_form');
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldSet = $form->addFieldset("bundle_actions_form", array("legend" => Mage::helper("bundles")->__("Bundle Actions Information")));

        $fieldSet->addField("bundle_rule_id", "select", array(
            "label" => Mage::helper("bundles")->__("Bundle rule"),
            "class" => "required-entry",
            "required" => true,
            "name" => "bundle_rule_id",
            'values' => $this->getBundleRules(),
        ));

        $fieldSet->addField("priority", "text", array(
            "label" => Mage::helper("bundles")->__("Priority"),
            "name" => "priority",
            'after_element_html' => $this->getPriorityText()
        ));

        $fieldSet->addField("scope", "text", array(
            "label" => Mage::helper("bundles")->__("Scope"),
            "name" => "scope",
        ));

        $fieldSet->addField("condition", "textarea", array(
            "label" => Mage::helper("bundles")->__("Condition"),
            "name" => "condition",
        ));

        $fieldSet->addField("action", "textarea", array(
            "label" => Mage::helper("bundles")->__("Action"),
            "name" => "action",
            "required" => true
        ));

        if (Mage::getSingleton("adminhtml/session")->getActionsData()) {
            $form->setValues(Mage::getSingleton("adminhtml/session")->getActionsData());
            Mage::getSingleton("adminhtml/session")->setActionsData(null);
        } elseif (Mage::registry("bundle_actions_data")) {
            $form->setValues(Mage::registry("bundle_actions_data")->getData());
        }

        
        return parent::_prepareForm();
    }

    /**
     * Map bundle rules
     * @return array
     */
    protected function getBundleRules()
    {
        $options = array(
            null => Mage::helper('bundles')->__('Please select...'),
        );
        /** @var Dyna_Bundles_Model_BundleAction $collection */
        $bundleRules = Mage::getModel('dyna_bundles/bundleRule')->getCollection();
        foreach ($bundleRules as $rule) {
            $options[$rule->getId()] = $rule->getBundleRuleName();
        }

        return $options;
    }

    /**
     * Get max priority
     * @return string
     */
    protected function getPriorityText()
    {
        $model = Mage::getModel('dyna_bundles/bundleAction');
        return $model->getMaxPriority() != null ? '<p class="note">The highest priority now is ' . $model->getMaxPriority() . '.</p>' : '';
    }
}
