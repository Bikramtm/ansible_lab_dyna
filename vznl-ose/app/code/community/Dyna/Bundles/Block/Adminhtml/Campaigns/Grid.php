<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Admin list Customer images grid
 */
class Dyna_Bundles_Block_Adminhtml_Campaigns_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('bundleCampaignsGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    /**
     * Prepare grid collection object
     *
     * @return $this
     */
    protected function _prepareCollection()
    {
        /** @var Dyna_Bundles_Model_Campaign $collection */
        $collection = Mage::getModel('dyna_bundles/campaign')->getCollection();
        $this->setCollection($collection);
        parent::_prepareCollection();

        return $this;
    }

    /**
     * Create the admin grid
     *
     * @return Dyna_Bundles_Block_Adminhtml_Campaigns_Grid
     */

    protected function _prepareColumns()
    {
        parent::_prepareColumns();

        $columns = [
            'entity_id' => array(
                'header' => $this->_getColumnTitle('ID'),
                'align' => 'left',
                'width' => '30px',
                'type' => 'number',
                'index' => 'entity_id'
            ),
            'campaign_id' => array(
                'header' => $this->_getColumnTitle('Campaign ID'),
                'align' => 'left',
                'width' => '30px',
                'index' => 'campaign_id'
            ),
            'cluster' => array(
                'header' => $this->_getColumnTitle('Cluster'),
                'align' => 'left',
                'width' => '30px',
                'index' => 'cluster'
            ),
            'cluster_category' => array(
                'header' => $this->_getColumnTitle('Cluster Category'),
                'align' => 'left',
                'width' => '30px',
                'index' => 'cluster_category'
            ),
            'advertising_code' => array(
                'header' => $this->_getColumnTitle('Advertising Code'),
                'align' => 'left',
                'width' => '30px',
                'index' => 'advertising_code'
            ),
            'campaign_code' => array(
                'header' => $this->_getColumnTitle('Campaign Code'),
                'align' => 'left',
                'width' => '30px',
                'index' => 'campaign_code'
            ),
            'marketing_code' => array(
                'header' => $this->_getColumnTitle('Marketing Code'),
                'align' => 'left',
                'width' => '30px',
                'index' => 'marketing_code'
            ),
            'promotion_hint' => array(
                'header' => $this->_getColumnTitle('Promotion Hint'),
                'align' => 'left',
                'width' => '30px',
                'index' => 'promotion_hint'
            ),
            'valid_from' => array(
                'header' => $this->_getColumnTitle('Valid From'),
                'align' => 'left',
                'width' => '30px',
                'index' => 'valid_from',
                'type' => 'date',
                'format' => Varien_Date::DATE_INTERNAL_FORMAT
            ),
            'valid_to' => array(
                'header' => $this->_getColumnTitle('Valid To'),
                'align' => 'left',
                'width' => '30px',
                'index' => 'valid_to',
                'type' => 'date',
                'format' => Varien_Date::DATE_INTERNAL_FORMAT
            ),
            'offer_hint' => array(
                'header' => $this->_getColumnTitle('Offer Hint'),
                'align' => 'left',
                'width' => '30px',
                'index' => 'offer_hint',
            ),
            'campaign_hint' => array(
                'header' => $this->_getColumnTitle('Campaign Hint'),
                'align' => 'left',
                'width' => '30px',
                'index' => 'campaign_hint',
            )
        ];

        foreach ($columns as $columnKey => $columnValue) {
            $this->addColumn($columnKey, $columnValue);
        }

        return $this;
    }


    function _getColumnTitle($text){
        return Mage::helper('bundles')->__($text);
    }

    /**
     * Get edit url for a record
     * @param $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }
}
