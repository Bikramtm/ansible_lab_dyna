<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Bundles_Block_Adminhtml_Rules_Actions
 */
class Dyna_Bundles_Block_Adminhtml_Campaigns_Offers extends Mage_Adminhtml_Block_Widget_Grid
{
    private $_campaignData = null;

    /**
     * Dyna_Bundles_Block_Adminhtml_Rules_Actions constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('bundleOfferGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    /**
     * Prepare grid collection object
     *
     * @return $this
     */
    protected function _prepareCollection()
    {
        if (Mage::getSingleton("adminhtml/session")->getCampaignsData()) {
            $this->_campaignData = Mage::getSingleton("adminhtml/session")->getCampaignsData();
            Mage::getSingleton("adminhtml/session")->getCampaignsData(null);
        } elseif (Mage::registry("bundle_campaigns_data")) {
            $this->_campaignData = Mage::registry("bundle_campaigns_data")->getData();
        }

        /** @var Dyna_Bundles_Model_CampaignOffer $collection */
        $collection = Mage::getModel('dyna_bundles/campaignOffer')->getCollection();
        $collection->getSelect()->joinLeft(
            array('campaignOfferRelation'=>'bundle_campaign_offer_relation'), //trebuie verificat
            'main_table.entity_id = campaignOfferRelation.offer_id',
            []
        );
        $collection->addFieldToFilter('campaignOfferRelation.campaign_id', isset($this->_campaignData['entity_id']) ? $this->_campaignData['entity_id'] : '');
       
        $this->setCollection($collection);
        parent::_prepareCollection();

        return $this;
    }

    /**
     * Create the admin grid
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */

    protected function _prepareColumns()
    {
        parent::_prepareColumns();

        $this->addColumn('entity_id_grid', array(
            'header' => $this->_getColumnTitle('ID'),
            'align' => 'left',
            'width' => '30px',
            'type' => 'number',
            'index' => 'entity_id',
        ));

        $this->addColumn('offer_id_grid', array(
            'header' => Mage::helper('bundles')->__('Offer Identification'),
            'align' => 'left',
            'index' => 'offer_id',
        ));

        $this->addColumn('title_description_grid', array(
            'header' => $this->_getColumnTitle('Title description'),
            'align' => 'left',
            'index' => 'title_description',
        ));

        $this->addColumn('usp1_grid', array(
            'header' => $this->_getColumnTitle('USP 1'),
            'align' => 'left',
            'index' => 'usp1',
        ));

        $this->addColumn('usp2_grid', array(
            'header' => $this->_getColumnTitle('USP 2'),
            'align' => 'left',
            'index' => 'usp2',
        ));

        $this->addColumn('discounted_months_grid', array(
            'header' => $this->_getColumnTitle('Discounted Months'),
            'align' => 'left',
            'index' => 'discounted_months',
        ));

        $this->addColumn('discounted_price_per_month_grid', array(
            'header' => $this->_getColumnTitle('Discounted price per month'),
            'align' => 'left',
            'index' => 'discounted_price_per_month',
            'width' => '10px'
        ));

        $this->addColumn('undiscounted_price_per_month_grid', array(
            'header' => $this->_getColumnTitle('Undiscounted price per month'),
            'align' => 'left',
            'index' => 'undiscounted_price_per_month',
            'width' => '10px'
        ));

        $this->addColumn('product_ids_grid', array(
            'header' => $this->_getColumnTitle('Product IDs'),
            'align' => 'left',
            'index' => 'product_ids',
            'width' => '10px'
        ));


        return $this;
    }

    public function _getColumnTitle($text)
    {
        return Mage::helper('bundles')->__($text);
    }
}
