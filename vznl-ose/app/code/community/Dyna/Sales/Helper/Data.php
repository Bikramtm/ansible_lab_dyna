<?php

class Dyna_Sales_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getAlternateQuoteItems($quoteId)
    {
        return Mage::getModel('sales/quote_item')->getCollection()
            ->getSelect()
            ->reset(Zend_Db_Select::WHERE)
            ->where('quote_id IS NULL')
            ->where('alternate_quote_id = ?', $quoteId)
            ->query()
            ->fetchAll();
    }

    public function getAlternateItemsFromQuote($quote)
    {
        $alternateQuoteItems = $this->getAlternateQuoteItems($quote->getId());
        $result = [];
        foreach ($alternateQuoteItems as $alternateQuoteItem) {
            $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $alternateQuoteItem['sku']);
            $alternateQuoteItem['product'] = $product->getData();
            $result[] = $alternateQuoteItem;
        }

        return $result;
    }

    /**
     * @param array $options
     */
    public function additionalValidation(array $options = [])
    {
    }
}
