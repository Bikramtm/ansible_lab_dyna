<?php

/**
 * Parser for the checkout steps fields
 * Class Dyna_Checkout_Helper_Fields
 */

class Dyna_Checkout_Helper_Fields extends Mage_Core_Helper_Data
{
    protected $checkoutData = null;

    CONST PAYMENT_CASH_ON_DELIVERY = 'cashondelivery';
    CONST PAYMENT_DIRECT_DEBIT = 'direct_debit';
    CONST PAYMENT_CHECKMO = 'checkmo';
    CONST PAYMENT_ALREADY_PAID = 'alreadypaid';
    CONST PAYMENT_PAY_IN_STORE = 'payinstore';
    CONST PAYMENT_BANK_TRANSFER = 'banktransfer';
    CONST PAYMENT_SPLIT_PAYMENT = 'split';
    CONST PAYMENT_DIRECT_DEBIT_REUSE = 'direct_debit_reuse';
    CONST PAYMENT_DIRECT_DEBIT_DIFF = 'direct_debit_diff';
    CONST BILLING_TO_CUSTOMER_ADDRESS = 1;
    CONST BILLING_TO_ALTERNATIVE_ADDRESS = 2;
    CONST HANDELSREGISTEREINTRAG_YES = 2;
    CONST HANDELSREGISTEREINTRAG_NO = 1;
    CONST CHANGE_PROVIDER_YES = 1;
    CONST CHANGE_PROVIDER_NO = 0;
    CONST CHANGE_PROVIDER_SINGLE_FAMILY_HOUSE = 0;
    CONST CHANGE_PROVIDER_MULTI_FAMILY_HOUSE = 1;


    /**
     * Iterates through each array value and sub value and returns an array containing the variable path as key
     * and the fields value as value
     * @param $arrayData
     * @return array
     */
    public function buildVariableName($arrayData, $currentKey = "", &$result = [])
    {
        foreach ($arrayData as $key => $value) {
            //First key should not be encapsulated in brackets
            $extendedKey = $currentKey ? $currentKey . "[" . $key . "]" : $key;
            //If it's an array, go deeper
            if (is_array($value)) {
                $this->buildVariableName($value, $extendedKey, $result);
            } else {
                $result[$extendedKey] = $value;
            }
        }

        return $result;
    }

    /**
     * Performs validations against customer data
     * @return array
     * @todo implement it
     */
    public function validateCustomerData()
    {
        return [
            "error" => false,
//            "message" => "Invalid customer data fields",
//            "invalid_fields" => [
//                'customer[dob]' => $this->__("Only customers over 18 years old can order Iphone 7"),
//            ]
        ];
    }

    /**
     * Get previously saved checkout data for certain step and quote
     * @param $currentStep
     * @param $quoteId
     * @return array
     */
    public function getCheckoutFieldsAsArray($currentStep, $quoteId)
    {
        $checkoutData = [];

        /** @var Dyna_Checkout_Model_Resource_Field_Collection $checkoutFields */
        $checkoutFields = Mage::getModel("dyna_checkout/field")->getCollection()
            ->getAllFieldsForStep($currentStep, $quoteId);

        foreach ($checkoutFields as $field) {
            $checkoutData[$field->getFieldName()] = htmlspecialchars($field->getFieldValue());
        }

        return $checkoutData;
    }

    /**
     * Get saved checkout data for certain step and package
     * @param $currentStep
     * @param $packageId
     * @return array
     */
    public function getCheckoutFieldsByPackage($currentStep, $packageId)
    {
        $checkoutData = [];

        /** @var Dyna_Checkout_Model_Resource_Field_Collection $checkoutFields */
        $checkoutFields = Mage::getModel("dyna_checkout/field")->getCollection()
            ->getAllFieldsForStepFromPackage($currentStep, $packageId);

        foreach ($checkoutFields as $field) {
            $checkoutData[$field->getFieldName()] = htmlspecialchars($field->getFieldValue());
        }

        return $checkoutData;
    }

    /**
     * Returns an array of checkout data for the specified checkout steps
     * and package ids.
     *
     * @param $currentStep
     * @param $packageIds
     * @return array
     */
    public function getOrderCheckoutFieldsByPackage($currentStep, $packageIds)
    {
        $checkoutData = [];

        /** @var Dyna_Checkout_Model_Resource_Field_Collection $checkoutFields */
        $checkoutFields = Mage::getModel("dyna_checkout/field")
            ->getCollection()
            ->addFieldToFilter("checkout_step", ["eq" => $currentStep])
            ->addFieldToFilter("package_id", ["in" => $packageIds]);

        foreach ($checkoutFields as $field) {
            $checkoutData[$field->getFieldName()] = htmlspecialchars($field->getFieldValue());
        }

        return $checkoutData;
    }

    /**
     * Set checkout data on current instance
     * @param $currentStep string
     * @param $quoteId int
     * @return $this
     */
    public function setCheckoutData($currentStep, $quoteId)
    {
        $this->checkoutData = $this->getCheckoutFieldsAsArray($currentStep, $quoteId);

        return $this;
    }

    /**
     * @param $currentStep
     * @param $packageIds
     * @return $this
     */
    public function setOrderCheckoutData($currentStep, $packageIds)
    {
        $this->checkoutData = $this->getOrderCheckoutFieldsByPackage($currentStep, $packageIds);

        return $this;
    }

    /**
     * Set the checkout data on the specified step and package id
     * @param $currentStep
     * @param $packageId
     * @return $this
     */
    public function setCheckoutDataForPackage($currentStep, $packageId)
    {
        $this->checkoutData = $this->getCheckoutFieldsByPackage($currentStep, $packageId);

        return $this;
    }

    /**
     * Return checkout field value by field name and checkout data previously set by setCheckoutData
     * @param $fieldName
     * @return mixed|false
     */
    public function getFieldValue($fieldName, $defaultValue = null)
    {
        //If array key exists, than submit form included this field
        if (isset($this->checkoutData[$fieldName])) {
            return htmlspecialchars($this->checkoutData[$fieldName]);
        }

        if ($defaultValue) {
            if ( is_array($defaultValue) ) {
                $defaultArray = [];
                foreach ( $defaultValue as $key => $value ) {
                    $defaultArray[$key] = htmlspecialchars($value);
                }
                return $defaultArray;
            }

            return htmlspecialchars($defaultValue);
        }

        return false;
    }

    public function getOrderRow($label, $fieldName)
    {
        $value = $this->getFieldValue($fieldName);

        if ($value) {
            return "<div class='row'>
        <div class='col-md-4'>
            <span>$label</span>
        </div>
        <div class='col-md-8'>
            $value
        </div>
    </div>";
        }

        return null;
    }

    public function getOrderRowFromArray($label, $fields)
    {
        $values = [];
        foreach($fields as $field) {
            $values[] = $this->getFieldValue($field);
        }

        return "<div class='row'>
        <div class='col-md-4'>
            <span>$label</span>
        </div>
        <div class='col-md-8'>
            " . implode(' ', $values) . "
        </div>
    </div>";
    }

    /**
     * Return checkout field value by field name and checkout data previously set by setCheckoutData
     * @param string $fieldNameBeginning
     * @param null $defaultValue
     * @return array|false|string
     */
    public function getFieldValueBeginsWith($fieldNameBeginning, $defaultValue = null)
    {
        $foundFields = array();
        foreach ($this->checkoutData as $key => $value) {
            if (strpos($key, $fieldNameBeginning) === 0) {
                // we extract the key as from a string like provider[phone_transfer_list][telephone_prefix][0] ; the new key here we consider to be 0;
                $newKey = str_replace($fieldNameBeginning, "", $key);
                $newKey = str_replace("[", "", $newKey);
                $newKey = str_replace("]", "", $newKey);
                $foundFields[(int)$newKey] = $value;
            }
        }

        if(count($foundFields)) {
            return $foundFields;
        }

        if ($defaultValue) {
            return $defaultValue;
        }

        return false;
    }

    /*
     * Checks whether or not there are saved fields for current checkout step
     * @return bool
     */
    public function hasCheckoutData()
    {
        return !empty($this->checkoutData);
    }

    /**
     * Gets checkout data
     * @return array
     */
    public function getCheckoutData()
    {
        return $this->checkoutData;
    }

    /**
     * Retrieve the checkout Billing Address of the customer
     * @return array
     */
    public function getBillingAddress()
    {
        $quoteId = Mage::getSingleton('checkout/session')->getQuoteId();
        $customerData = $this->getCheckoutFieldsAsArray('save_customer', $quoteId);

        $addressFields = [
            'postcode' => 'customer[address][postcode]',
            'city' => 'customer[address][city]',
            'district' => 'customer[address][district]',
            'street' => 'customer[address][street]',
            'houseno' => 'customer[address][houseno]',
            'addition' => 'customer[address][addition]',
        ];

        $addressSohoFields = [
            'postcode' => 'customer_soho[address][postcode]',
            'city' => 'customer_soho[address][city]',
            'district' => 'customer_soho[address][district]',
            'street' => 'customer_soho[address][street]',
            'houseno' => 'customer_soho[address][houseno]',
            'addition' => 'customer_soho[address][addition]',
        ];

        // Default empty address
        $address = [
            'postcode' => '',
            'city' => '',
            'district' => '',
            'street' => '',
            'houseno' => '',
            'addition' => '',
        ];

        foreach ($customerData as $name => $value) {
            if ($key = array_search($name, $addressFields)) {
                $address[$key] = $value;
            }
        }

        if(strtoupper($customerData['customer[type]']) == 'SOHO') {
            foreach ($customerData as $name => $value) {
                if ($key = array_search($name, $addressSohoFields)) {
                    $address[$key] = $value;
                }
            }
        }

        return $address;
    }

    /**
     * Method to save checkout field data
     * @param array $quote
     * @param $step
     * @param $data
     */
    public function addData($quote, $step, $data)
    {
        foreach($data as $fieldName => $fieldValue){
            //first find if field was already saved
            $model = Mage::getModel("dyna_checkout/field")->getCollection()
                ->addFieldToFilter('quote_id', $quote->getId())
                ->addFieldToFilter('package_number', $quote->getActivePackageId())
                ->addFieldToFilter('field_name', $fieldName)
                ->addFieldToFilter('field_value', $fieldValue);
            if($step){
                $model->addFieldToFilter('checkout_step', $step);
            }
            //if more than 1 records were found, create new record
            if($model->count() == 0 || $model->count() > 1){
                $model = Mage::getModel("dyna_checkout/field");
            }
            else{
                $model = $model->getFirstItem();
            }

            //either update existing field or insert a new one
            $model->setQuoteId($quote->getId())
                ->setPackageNumber($quote->getActivePackageId())
                ->setCheckoutStep($step)
                ->setFieldName($fieldName)
                ->setFieldValue($fieldValue)
                ->setDate(date("Y-m-d H:i:s"))
                ->save();
        }
    }

    /**
     * Retrieve the checkout send offer data
     * @return array
     */
    public function getSendOfferData()
    {
        $quoteId = Mage::getSingleton('checkout/session')->getQuoteId();
        $customerData = $this->getCheckoutFieldsAsArray('send_document', $quoteId);

        $callbackFields = [
            'email' => 'email',
            'date' => 'callbackDate',
            'time' => 'callbackTime',
        ];

        // Default empty address
        $callback = [
            'email' => '',
            'date' => '',
            'time' => '',
        ];

        foreach ($customerData as $name => $value) {
            if ($key = array_search($name, $callbackFields)) {
                $callback[$key] = $value;
            }
        }

        return $callback;
    }
}
