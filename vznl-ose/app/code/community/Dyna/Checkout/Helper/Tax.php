<?php

/**
 * Class Dyna_Checkout_Helper_Tax
 */
class Dyna_Checkout_Helper_Tax extends Mage_Tax_Helper_Data
{
    /**
     * Gets tax class id by class name
     * @return float
     */
    public function getTaxClassIdByName($className)
    {
        $taxClass = Mage::getSingleton('tax/class')
            ->getCollection()
            ->addFieldToFilter('class_name', ['eq' => $className])
            ->getFirstItem();

        return $taxClass->getId();
    }
}
