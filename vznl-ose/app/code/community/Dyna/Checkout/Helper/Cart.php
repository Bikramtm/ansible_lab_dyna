<?php

/**
 * Class Dyna_Checkout_Helper_Cart
 */
class Dyna_Checkout_Helper_Cart extends Mage_Core_Helper_Data
{
    use Dyna_Checkout_Block_Cart_Steps_Common;

    protected $_incompatibleDefaultedProducts = [];
    // List of all the exclusive families that are contained in the package
    protected $_exclusiveFamilyCategoryIds = [];
    protected $_section = null;
    // Id, sku, action and defaulted state of clicked configurator item
    protected $_clickedProductId = null;
    protected $_clickedProductSku = null;
    protected $_clickedItemSelected = null;
    protected $_clickedItemDefaulted = null;

    /** @var Dyna_Configurator_Helper_Cart $configuratorCartHelper */
    protected $configuratorCartHelper = false;

    /** @var Dyna_Configurator_Helper_ExtraLogCart $rulesHelper */
    protected $logRulesHelper = false;
    /** @var Dyna_Checkout_Model_Cart cart */
    protected $cart = false;

    /**
     * A new customer is created as a prospect customer
     * - The customer should not be login (OMNVFDE-937)
     * - Method moved from CartController (for multiple usage)
     * @param $vars
     * @return Dyna_Customer_Model_Customer
     */
    public function createDeProspectFromCart($vars)
    {
        /** @var Dyna_Customer_Model_Customer $customer */
        $customer = Mage::getModel('customer/customer');
        // dob is available only for private customers
        if ($vars['guestCustomerIsSoho']) {
            $customer->setCompanyName($vars['companyname']);
        }

        $customer->setFirstname($vars['firstname'])
            ->setLastname($vars['lastname'])
            ->setAdditionalEmail($vars['email_send'])
            ->setAdditionalTelephone($customer::COUNTRY_CODE . $vars['additional_telephone'])
            ->setPhoneLocalAreaCode($vars['prefix'])
            ->setPhoneNumber($vars['telephone'])
            ->setEmail(uniqid() . Omnius_Customer_Model_Customer_Customer::DEFAULT_EMAIL_SUFFIX)
            ->setIsProspect(true)
            ->setIsBusiness((int)$vars['guestCustomerIsSoho'])
            ->setDob($vars['dob'])
            ->save();

        return $customer;
    }

    /**
     * Determine if quote/order contains a product of specific type
     * @param $packages
     * @param $type
     * @param $editingDisabled
     * @return bool
     */
    public function hasPackageOfType($packages, $type, $editingDisabled = false)
    {
        /** @var Dyna_Package_Model_Package $package */
        foreach ($packages as $package) {
            if (is_array($type)) {
                if ($editingDisabled) {
                    if (!$package->getEditingDisabled() && in_array(strtolower($package->getType()), $type)) {
                        return true;
                    }
                } else {
                    if (in_array(strtolower($package->getType()), $type)) {
                        return true;
                    }
                }
            } else {
                if ($editingDisabled) {
                    if (!$package->getEditingDisabled() && strtolower($package->getType()) == strtolower($type)) {
                        return true;
                    }
                } else {
                    if (strtolower($package->getType()) == strtolower($type)) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * Check if the quote/order only exists of the specified package type
     *
     * @param $packages
     * @param $type
     * @param bool $editingDisabled
     * @return bool
     */
    public function hasOnlyPackagesOfType($packages, $type)
    {
        /** @var Dyna_Package_Model_Package $package */
        foreach ($packages as $package) {
            $packageType = is_array($package) ? $package['type'] : $package->getType();
            if (strtolower($packageType) != strtolower($type)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Update previous carts that are of the same type with the ones that are now saved
     * Set the status to CART_STATUS_OLD
     */
    public function updatePreviousCarts($customer = null)
    {
        $customer = $customer ?? $this->getCustomer();
        /**
         * @var $customerModel Dyna_Customer_Model_Customer
         */
        $customerModel = Mage::getModel('customer/customer');

        $dbShoppingCarts = array();
        if ($customer->getCustomerNumber()) {
            $dbShoppingCarts = $customerModel->getShoppingCartsForCustomer(null, $customer->getCustomerNumber());
        } else if ($customer->getId()) {
            $dbShoppingCarts = $customerModel->getShoppingCartsForCustomer($customer->getId());
        }

        $maxNumberOfShoppingCarts = Mage::getStoreConfig('omnius_general/sales_settings/shopping_carts_number');

        $this->updateCarts($dbShoppingCarts, $maxNumberOfShoppingCarts);
    }

    /**
     * Update saved shopping carts according to maximum value set in backend (VFDED1W3S-394)
     * @param $customerShoppingCarts
     * @param $quotePackagesType
     * @param $maxNumberOfShoppingCarts
     */
    public function updateCarts($customerShoppingCarts, $maxNumberOfShoppingCarts)
    {
        // sort shopping carts
        ksort($customerShoppingCarts);

        $cartsToRemove = array();
        // The configuration parameter refers to the maximum allowed shopping carts per customer (either saved cart or offer)
        // In order to maintain the limit, older shopping carts will be removed for the customer in question
        // See VFDED1W3S-471
        if (count($customerShoppingCarts) > $maxNumberOfShoppingCarts) {
            $cartsOverLimit = count($customerShoppingCarts) - $maxNumberOfShoppingCarts;
            foreach ($customerShoppingCarts as $cartId => $cartAndPackages) {
                if ($cartsOverLimit == 0) {
                    break;
                }
                $cartsOverLimit--;
                array_push($cartsToRemove, $cartId);
            }
        }
        $cartsToRemove = array_unique($cartsToRemove);
        // set status to old to the carts kept in $cartsToRemove
        foreach ($cartsToRemove as $cartId) {
            $customerShoppingCarts[$cartId]['cart'][0]
                ->setCartStatus(Dyna_Checkout_Model_Sales_Quote::CART_STATUS_OLD)
                ->save();
        }
    }

    /**
     * @param $allItems ; All quote items from the specific package (active package)
     * @param $packageModel ; Package model
     * @param $section ; Configurator section (package_subtype)
     * @return array
     * @deprecated // It cannot be used anymore because service defaulted products will not be re-added to quote
     */
    protected function removeAllItemsBySection($allItems, $packageModel, $section)
    {
        $hadSubscription = false;
        $packageId = $packageModel->getPackageId();
        $packageType = $packageModel->getType();

        /** @var Dyna_Checkout_Model_Sales_Quote_Item $item */
        foreach ($allItems as $item) {
            if ($item->getProduct()->isInSection(strtolower($section))) {
                if ($item->getProduct()->is(Dyna_Catalog_Model_Type::$subscriptions)) {
                    $hadSubscription = true;
                }
            }
            if ($this->_clickedProductId && $item->getProductId() == $this->_clickedProductId && !$this->_clickedItemSelected) {
                $item->setSystemAction(Dyna_Checkout_Model_Sales_Quote_Item::SYSTEM_ACTION_NOT_APPLICABLE);
            }
        }

        $this->emptyCartOnTariffChangeOrRemove($hadSubscription, $packageType, $allItems, $packageModel);

        // check and remove any redPlus childs for current package
        $this->checkAndRemoveRedPlusChildren($packageModel);
        try {
            // Clear all item from the current package
            return $this->removeAllItems($section, $packageId, $packageType, $packageModel);
        } catch (Exception $e) {
            Mage::getSingleton('core/logger')->logException($e);

            return array(
                'error' => true,
                'message' => $this->__($e->getMessage()),
            );
        }
    }

    /**
     * Method that performs adding product to cart
     * @return mixed
     */
    public function processAddMulti($data)
    {
        // package type
        $type = $data['type'];
        // section in the configurator (product type)
        $section = $data['section'];
        // current package id
        $packageId = $data['packageId'];
        // products from the current section
        $products = $data['products'] ?? [];

        $this->_section = $section;
        // product that was clicked in the configurator (selected or deselected)
        $this->_clickedProductId = $data['clickedItemId'] ?? null;
        // flag to know if the product was added or removed
        $this->_clickedItemSelected = $data['clickedItemSelected'] && ($data['clickedItemSelected'] === 'true');

        $this->_getQuote()->setCurrentStep(null)
            ->saveActivePackageId($packageId);
        // Setting package status OPEN, it will be evaluated on expanded cart since OMNVFDE-3896
        $this->_getQuote()->getCartPackage($packageId)->setCurrentStatus(Omnius_Configurator_Model_AttributeSet::PACKAGE_STATUS_OPEN)->save();

        $removedDefaultFlag = false;

        // Retrieve all the exclusive families
        $allExclusiveFamilyCategoryIds = Mage::getModel('catalog/category')->getAllExclusiveFamilyCategoryIds();
        $products = array_filter(
            array_filter(
                is_array($products) ? $products : array(), 'trim'
            )
        );

        /** @var Dyna_ProductMatchRule_Helper_Data $productMatchHelper */
        $productMatchHelper = Mage::helper('dyna_productmatchrule');

        // Create the new package for the quote or update if already created
        /** @var Dyna_Package_Model_Package $packageModel */
        $packageModel = $this->_getQuote()->getCartPackage($packageId);
        if (!$packageModel || !$packageModel->getId()) {
            $packageModel = $packageModel ?: Mage::getModel('package/package');
            $packageModel->setQuoteId($this->_getQuote()->getId())->setPackageId($packageId)->setType($type);
        }

        $packageModel->updateUseCaseIndication();

        $added = [];
        $hadSubscription = false;
        $allItems = $this->_getQuote()->getCartPackage()->getAllItems();
        $persistedProducts = [];
        $productsInCart = $appliedRulesArray = [];

        $deletedItems = [];
        $removedDefault = [];
        // It will be set to true if it had one and it gets removed
        $notDefaulted = array();

        try {
            /** @var Dyna_Checkout_Model_Sales_Quote_Item $item */
            foreach ($allItems as $item) {

                if (($this->_clickedProductId && $item->getProductId() == $this->_clickedProductId)
                    && (($item->getIsDefaulted() || $item->getIsServiceDefaulted()) && $item->getProduct()->isOption())
                ) {
                    $this->_clickedProductSku = $item->getSku();
                    $this->_clickedItemDefaulted = true;
                }

                if (isset($deletedItems[$item->getId()])) {
                    continue;
                }

                $productsInCart[$item->getId()] = $item->getProductId();

                if (($categoriesExclusive = array_intersect($item->getProduct()->getCategoryIds(), $allExclusiveFamilyCategoryIds))
                    && ($item->getPackageId() == $packageId)
                ) {
                    foreach ($categoriesExclusive as $categoryExclusive) {
                        $this->_exclusiveFamilyCategoryIds[$item->getProductId()] = $categoryExclusive;
                    }
                }

                if ($item->getPackageId() == $packageId && ($item->getProduct()->isInSection(strtolower($section)))) {
                    if ($item->getProduct()->is(Dyna_Catalog_Model_Type::$subscriptions)) {
                        $hadSubscription = true;
                    }
                    if (!in_array($item->getProductId(), $products)) {
                        $productsInCart = $this->removeItemAndParseDefault(
                            $item,
                            $productsInCart,
                            $packageModel,
                            $allItems,
                            $deletedItems,
                            $removedDefaultFlag,
                            $removedDefault,$type
                        );
                        $this->checkAndRemoveRedPlusChildren($packageModel);
                        if ($item->getBundleComponent()) {
                            $this->checkAndRemoveSusoBundle($packageModel);
                            $this->checkAndRemoveBundle($packageModel);
                            $this->removeContractBundleMarkers($packageModel);
                        }
                    } else {
                        $persistedProducts[] = $item->getProductId();
                    }
                    continue;
                }
            }
        } catch (Exception $e) {
            Mage::getSingleton('core/logger')->logException($e);
            return array(
                'error' => true,
                'message' => $this->__('Cannot add the item to shopping cart'),
            );
        }

        if ($this->emptyCartOnTariffChangeOrRemove($hadSubscription, $type, $allItems, $packageModel)) {
            $productsInCart = [];
            $persistedProducts = [];
        }

        // If it had a subscription, and one of them was bundle component, break bundle
        /// @see OVG-2270
        $stillHasSubscription = false;
        if ($hadSubscription) {
            // Check whether or not subscription remains in cart
            foreach ($packageModel->getAllItems() as $item) {
                if ($item->getProduct()->is(Dyna_Catalog_Model_Type::$subscriptions)) {
                    $stillHasSubscription = true;
                }
            }
            // If it is not in cart, break bundle
            if (!$stillHasSubscription) {
                $this->checkAndRemoveBundle($packageModel);
            }
        }

        // Reinitialising items with remaining quote items
        $allItems = $this->_getQuote()->getPackageItems($this->_getQuote()->getActivePackageId());
        foreach ($allItems as $item) {
            if (!$item->getIsDefaulted()) {
                $notDefaulted[] = $item->getProductId();
            }
        }

        $productsInCart = array_unique(array_merge($productsInCart, $products));

        try {
            $this->validateProductsDE($productsInCart, $allItems, $packageId, $type);
        } catch (Exception $e) {
            return array(
                'error' => true,
                'message' => $e->getMessage(),
            );
        }

        $products = array_diff($products, array_keys($removedDefault));

        $isAcqTariff = false;
        foreach ($packageModel->getAllItems() as $item) {
            if ($item->getProduct()->isSubscription() && !$item->isContract()) {
                $isAcqTariff = true;
            }
        }

        foreach ($products as $productId) {
            try {
                if (in_array($productId, $persistedProducts)) {
                    continue;
                }
                $quoteItem = $this->parseProduct($packageId, $productId, $appliedRulesArray, $added);
                // Changes source collection in Inlife if tariff is changed
                if ((strtoupper($packageModel->getSaleType()) != Dyna_Catalog_Model_ProcessContext::ACQ) && ($isAcqTariff || $quoteItem->getProduct()->isSubscription())) {
                    if ($productMatchHelper->isDebugMode()) {
                        $productMatchHelper->logRulesDebug("************************************************");
                        $productMatchHelper->logRulesDebug("****TARIFF CHANGE - CALLING CQ SOURCE COLLECTION******");
                        $productMatchHelper->logRulesDebug("************************************************");
                    }
                    Mage::register('rules_source_collection', Dyna_ProductMatchRule_Helper_Data::SOURCE_COLLECTION_CQ);
                }

                $infringingExclusiveCategoryProducts = [];
                if (isset($this->_exclusiveFamilyCategoryIds[$productId])) {
                    $infringingExclusiveCategoryProducts = array_keys($this->_exclusiveFamilyCategoryIds, $this->_exclusiveFamilyCategoryIds[$productId]);
                }
                if (count($infringingExclusiveCategoryProducts) > 1) {
                    $toBeRemoved = array_shift(array_diff($infringingExclusiveCategoryProducts, [$productId]));
                    unset($this->_exclusiveFamilyCategoryIds[$toBeRemoved]);
                    $this->removeProductFromCart($toBeRemoved, $packageModel);
                }
            } catch (RuntimeException $e) {
                continue;
            } catch (Mage_Core_Exception $e) {
                $messages = array_map(function ($el) {
                    return Mage::helper('core')->escapeHtml($el);
                }, array_unique(explode("\n", $e->getMessage())));

                $this->removeProductFromCart($added, $packageModel);
                $this->_getCart()->save();
                return array(
                    'error' => true,
                    'message' => join(', ', $messages),
                );
            } catch (Exception $e) {
                Mage::getSingleton('core/logger')->logException($e);

                $this->removeProductFromCart($added, $packageModel);
                $this->_getCart()->save();

                return array(
                    'error' => true,
                    'message' => $this->__('Cannot add the item to shopping cart'),
                );
            }
        }

        $finalProducts = array_unique(array_merge($persistedProducts, $added, $productsInCart));

        if (empty($added)) {
            $serviceDefaultedProducts = $this->parseServiceDefaultedProducts($notDefaulted, $packageModel, $finalProducts, $allExclusiveFamilyCategoryIds, $this->_exclusiveFamilyCategoryIds);
            $addedDefaultItems = $serviceDefaultedProducts;
        } else {
            /** @var Dyna_Checkout_Model_Sales_Quote_Item[] $addedDefaultItems */
            $addedDefaultItems = $this->parseDefaultedProducts($added, $packageModel, $finalProducts, $allExclusiveFamilyCategoryIds, $this->_exclusiveFamilyCategoryIds);
        }

        /*****************************
         * SPECIAL HANDLING FOR CABLE
         ******************************/
        $cablePackages = Dyna_Catalog_Model_Type::getCablePackages();

        // Searching for defaulted items for all products in cart, not only newly added because of fee logic
        // Only one fee is allowed per quote so some of them (resulted from defaulted rules) are skipped from adding
        // But when removing one from quote we need to re-add the previously added one by a product which is all ready present in cart and not a new one
        $addedDefaultFees = [];
        if (!empty($added) && in_array($type, $cablePackages)) {
            $addedDefaultFees = $this->parseFees($addedDefaultItems, $packageModel);
        }
        $addedDefaultItems = array_merge($addedDefaultItems, $addedDefaultFees);
        $addedDefaultItemsIds = [];
        foreach ($addedDefaultItems as $addedDefaultItem) {
            $addedDefaultItemsIds[] = $addedDefaultItem->getProductId();
        }

        $this->_getCart()->save();

        /*****************************
         * SPECIAL HANDLING FOR CABLE
         ******************************/
        if (in_array(strtolower($type), $cablePackages)) {
            $quoteProducts = [];
            foreach ($this->_getQuote()->getAllItems() as $item) {
                $quoteProducts[] = $item->getProductId();
            }
            // Parse CABLE own hardware products
            $this->parseOwnHardware($packageModel);
        }

        $this->checkOfferPackage($packageModel, $finalProducts);
        $packageModel->updateDefaultedRemoved($removedDefaultFlag);
        $packageModel->save();

        return $this->returnCombinations(null, null, Omnius_Configurator_Model_AttributeSet::PACKAGE_STATUS_OPEN);
    }

    /**
     * Empties a product section from the configurator.
     * @param $section
     * @param $packageId
     * @param $packageType
     * @param Dyna_Package_Model_Package $packageModel
     * @return array
     */
    protected function removeAllItems($section, $packageId, $packageType, $packageModel)
    {
        $removedDefaultFlag = false;
        $hasBundleComponent = false;
        /** @var Omnius_Checkout_Model_Sales_Quote_Item $item */
        foreach ($this->_getQuote()->getItemsCollection() as $item) {
            if ($item->getPackageId() != $packageId) {
                continue;
            }
            if ($item->getProduct()->isInSection($section)) {
                if ($item->getBundleComponent()) {
                    $hasBundleComponent = true;
                }
                $this->doRemoveItemOfAll($packageModel, $item, $removedDefaultFlag, $removedDefault);
            }
        }

        $remainingQuoteItems = $this->_getQuote()->getAllItems();
        if (count($remainingQuoteItems) > 0) {
            $productIds = array_map(function ($quoteItem) {
                return $quoteItem->getProductId();
            }, $remainingQuoteItems);

            $rules = $this->getConfiguratorCartHelper()
                ->getCartRules($productIds, [], null, false, $packageType);

            // needs second iteration to remove "initial electable false" products that aren't explicitly made visible by compatibility rules
            foreach ($remainingQuoteItems as $item) {
                // skip items that are not in the addMulti section
                if ($item->getPackageId() != $packageId) {
                    continue;
                }
                if (!$item->getProduct()->getInitialSelectable()
                    && !$item->getBundleChoice()
                    && empty(array_intersect($rules, [$item->getProductId()]))
                    && $item->getPackageId() == $packageId
                ) {
                    $this->_getCart()->removeItem($item->getId());
                }
            }
        }

        /** @var Omnius_Package_Model_Package $packageModel */
        $packageModel = $this->_getQuote()->getCartPackage($packageId);

        // check if we removed an offer item
        $this->checkOfferPackage($packageModel);

        // Setting package status OPEN, it will be evaluated on expanded cart since OMNVFDE-3896
        $packageModel->setCurrentStatus(Omnius_Configurator_Model_AttributeSet::PACKAGE_STATUS_OPEN);
        $packageModel->updateDefaultedRemoved($removedDefaultFlag);
        $packageModel->save();

        // is no subscription found in cart, proceed to bundle removal evaluation
        // this is handled on the presumption that by deselecting a tariff, a modal was shown to agent that this action will break bundling
        if (in_array(strtolower($section), array_map("strtolower", Dyna_Catalog_Model_Type::$subscriptions))) {
            $this->checkAndRemoveRedPlusChildren($packageModel);
            if ($hasBundleComponent) {
                $this->checkAndRemoveSusoBundle($packageModel);
                $this->checkAndRemoveBundle($packageModel);
                $this->removeContractBundleMarkers($packageModel);
            }
        }
        $this->_getCart()->save();

        return $this->returnCombinations(false, null, Omnius_Configurator_Model_AttributeSet::PACKAGE_STATUS_OPEN);
    }

    /**
     * Method used to remove Red+ children if the package
     * no longer contains the related product that triggered
     * the group creation
     *
     * @param $packageModel
     */
    protected function checkAndRemoveRedPlusChildren($packageModel)
    {
        $previousRelatedProductSku = $packageModel->getRedplusRelatedProduct();
        /** @var Dyna_Bundles_Helper_Data $bundleHelper */
        $bundleHelper = Mage::helper('dyna_bundles');
        $breakRedplusGroup = true;
        if ($previousRelatedProductSku) {
            $quoteItems = $this->_getQuote()->getAllItems();

            foreach ($quoteItems as $item) {
                if ($item->getPackageId() == $this->_getQuote()->getActivePackageId()
                    && $item->getSku() == $previousRelatedProductSku
                ) {
                    $breakRedplusGroup = false;
                    break;
                }
            }

            if ($breakRedplusGroup) {
                $packageModel
                    ->setRedplusRelatedProduct(null)
                    ->save();

                $childRedplusPackages = $this->_getQuote()->getCartPackagesWithParent($packageModel->getEntityId());

                foreach ($childRedplusPackages as $redPlusPackage) {
                    $redPlusPackage->delete();
                }
                $bundleHelper->removeBundlePackageEntry($packageModel->getBundleOfType(Dyna_Bundles_Model_BundleRule::TYPE_RED_PLUS)->getId(), $packageModel->getId());
            }
        }

    }

    /**
     * @param $packageModel
     * @param $products
     */
    protected function checkOfferPackage($packageModel, $products = array())
    {
        /** @var Dyna_Customer_Model_Session $customerSession */
        $customerSession = Mage::getSingleton('customer/session');
        $uctParams = Mage::getSingleton('customer/session')->getUctParams() ?: null;
        $offerId = $packageModel->getOfferId();
        //Remove offer id if we remove product from offer
        if (!empty($offerId) && !empty($uctParams)) {
            /** @var Dyna_Bundles_Helper_Data $helperCampaign */
            $helperCampaign = Mage::helper('bundles');

            if (empty($products) || !$helperCampaign->productsInOffer($products, $offerId)) {
                $packageModel->setOfferId(null)->save();
                $uctParams['offerId'] = null;
                $customerSession->setUctParams($uctParams);
            }
        }
    }

    /**
     * Returns list of products that are not allowed based on current selection together with package totals
     * @param bool|false $simError
     * @param null $simName
     * @param array $addedDefault
     * @param array $removedDefault
     * @param null $packageStatus
     * @return array
     */
    public function returnCombinations($simError = false, $simName = null, $packageStatus = null)
    {
        /** @var Dyna_Checkout_Model_Sales_Quote_Item $el */
        $closure = function ($result, $el) {
            $result[(int)$el->getProductId()] = [
                'product_id' => (int)$el->getProductId(),
                'type' => strtolower($el->getProduct()->getTypeText()),
                'mandatory' => (bool)$el->getIsObligated(),
                'isSuso' => $el->getPackage()->isPartOfSuso(),
                'isGiven' => $el->getIsDefaulted(),
                'isDefaulted' => $el->getIsDefaulted(),
                'isContract' => $el->getIsContract()
            ];
            return $result;
        };

        $quoteItems = $this->_getQuote()->getPackageItems($this->_getQuote()->getActivePackageId());
        $cartProducts = array_reduce($quoteItems, $closure, array());

        $campaignProducts = [];
        if ($uctParams = Mage::getSingleton('customer/session')->getUctParams()) {
            $campaignProducts = Mage::helper('bundles')->getCampaignProducts($uctParams['campaignId']);
        }

        foreach ($cartProducts as &$cartProduct) {
            if (in_array($cartProduct['product_id'], $campaignProducts)) {
                $cartProduct['isCampaign'] = 1;
            } else {
                $cartProduct['isCampaign'] = 0;
            }
        }

        // If an installed base product was removed from cart, we should still show the `CONTRACT` label in configurator
        $alternateItems = array_merge($this->_getQuote()->getAlternateItems(), $this->_getQuote()->getAlternateItems(null, false, false));
        if (count($alternateItems)) {
            foreach ($cartProducts as &$cartProduct) {
                foreach ($alternateItems as $alternateItem) {
                    if ($cartProduct['product_id'] == $alternateItem['product_id']) {
                        $cartProduct['isContract'] = 1;
                    }
                }
            }
        }

        // From this point models can be cached (cart products must be retrieved before activating the freeze_models flag)
        Mage::register('freeze_models', true, true);
        $totalsUpdated = Mage::helper('omnius_configurator')->getActivePackageTotals();
        $totals = Mage::getSingleton('core/layout')->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/totals.phtml')->toHtml();
        $packageId = $this->_getQuote()->getActivePackageId();
        $package = $this->_getQuote()->getPackageById($packageId);
        $rightBlock = Mage::getSingleton('core/layout')->createBlock('checkout/cart_totals')
            ->setTemplate('checkout/cart/partials/package_block.phtml')
            ->setData(array(
                'guiSection' => Dyna_Catalog_Model_Product::GUI_CART
            ))
            ->toHtml();
        // no promo rules needed to be sent in frontend for now
        // $promoRules = Mage::helper('pricerules')->findApplicablePromoRules(true);

        if (!isset($cartProducts[$this->_clickedProductId]) &&
            $this->_clickedItemSelected === false &&
            $this->_clickedItemDefaulted === true &&
            $this->_clickedProductSku
        ) {
            $refusedDefaultedItems = $package->getRefusedDefaultedItems() ? explode(',', $package->getRefusedDefaultedItems()) : [];
            if (!in_array($this->_clickedProductSku, $refusedDefaultedItems)) {
                $refusedDefaultedItems[] = $this->_clickedProductSku;
            }
            $package->setRefusedDefaultedItems(implode(',', $refusedDefaultedItems))->save();
        }

        $response = array(
            'error' => false,
            'activePackageId' => (int)$packageId,
            'totalMaf' => $totalsUpdated['totalmaf'],
            'totalPrice' => $totalsUpdated['totalprice'],
            'totals' => $totals,
            'rightBlock' => $rightBlock,
            // no promo rules needed to be sent in frontend for now
            // 'promoRules' => $promoRules,
            'complete' => Mage::helper('dyna_checkout')->hasAllPackagesCompleted(),
            'quoteHash' => Mage::helper('dyna_checkout/cart')->getQuoteProductsHash(),
            'processContextCode' => $this->getContextHelper()->getProcessContextCode(),
            'hintMessages' => Mage::getSingleton('customer/session')->getHints() ?: array(),
            'packageEntityId' => (int)$package->getEntityId(),
            'salesId' => Mage::getSingleton('dyna_checkout/cart')->getQuote()->getSalesId(),
            'cartIsRedPlusEligible' => Mage::helper('bundles')->cartIsRedPlusEligible(),
        );

        if (Mage::helper('bundles')->isBundleChoiceFlow()) {
            $bundleChoiceBlock = Mage::getSingleton('core/layout')->createBlock('dyna_bundles/options')->setTemplate('bundles/options.phtml')->setCurrentPackageId($packageId)->toHtml();
            $response['bundleChoiceBlock'] = $bundleChoiceBlock;
        } else {
            $response['removeBundleChoiceBlock'] = 1;
        }

        if ($packageStatus === null) {
            $packageStatus = Mage::helper('omnius_checkout')->checkPackageStatus($packageId);
        }
        if (($incompleteSections = $packageStatus) && is_array($incompleteSections)) {
            $response['incomplete_sections'] = $incompleteSections;
        }

        // Removed default not needed anymore to be sent to frontend

        $response['cartProducts'] = $cartProducts;

        if ($simError) {
            $response['simError'] = $simError;
        } else {
            $response['sim'] = $simName;
        }

        if (Mage::helper('bundles')->hasMandatoryFamiliesCompleted()) {
            $response['removeBundleErrorNotice'] = 1;
        }

        $productModel = Mage::getSingleton('catalog/product');
        $warningDefaulted = '';
        $productNames = [];
        $message = 'The following product(s): %s cannot be added because they are incompatible with the current selection';
        foreach ($this->_incompatibleDefaultedProducts as $productId) {
            $productNames[] = $productModel->load($productId)->getDisplayNameConfigurator() ?: $productModel->load($productId)->getName();
        }
        if (!empty($productNames)) {
            $warningDefaulted .= htmlspecialchars(Mage::helper('dyna_configurator')->__($message, implode(', ', $productNames)));
        }

        if ($warningDefaulted != '') {
            $logMessage = sprintf(
                "\n!!!=======================!!!\n"
                . "Quote ID: %s\n%s\n"
                . "!!!=======================!!!\n",
                $this->_getQuote()->getId(),
                $warningDefaulted
            );

            /**
             * Log defaulted products that don't have allowed rules with the current cart to help debugging
             */
            Mage::log($logMessage, Zend_Log::DEBUG, 'defaulted_missing_product_allowed_rules.log', true);
            $response['warningDefaulted'] = $warningDefaulted;
        }

        return $response;
    }

    /**
     * Method that process the removal of a deselected product in configurator and handles removal of all its defaulted products
     *
     * @param $item Dyna_Checkout_Model_Sales_Quote_Item Cart
     * @param array $productsInCart Array of product ids
     * @param Dyna_Package_Model_Package $packageModel Active package on which addMulti updates products
     * @param Dyna_Checkout_Model_Sales_Quote_Item[] $allItems
     * @param $deletedItems
     * @param $removedDefaultFlag
     * @param $removedDefault
     * @param $type
     * @return mixed
     * @throws Mage_Core_Model_Store_Exception
     */
    protected function removeItemAndParseDefault($item, $productsInCart, $packageModel, $allItems, &$deletedItems, &$removedDefaultFlag, &$removedDefault)
    {
        // if item is service defaulted, added to the list of deselected defaulted items for later to know not to add it again to cart
        // @see VFDED1W3S-2335
        if ($item->getIsServiceDefaulted() || $item->getIsDefaulted()) {
            $packageDeselectedItems = array_filter(explode(",", $packageModel->getDeselectedDefaultedItems()));
            if (!in_array($item->getProductId(), $packageDeselectedItems)) {
                $packageDeselectedItems[] = $item->getProductId();
                $packageModel->setDeselectedDefaultedItems(implode(",", $packageDeselectedItems));
            }
        }

        if ($this->_clickedProductId && $item->getProductId() == $this->_clickedProductId && !$this->_clickedItemSelected) {
            $item->setSystemAction(Dyna_Checkout_Model_Sales_Quote_Item::SYSTEM_ACTION_NOT_APPLICABLE);
        }

        if ($item->getIsDefaulted()) {
            $removedDefaultFlag = true;
        }
        if ($packageModel->allowDefaulted()) {
            // Check if the removed item had defaulted items, and remove them
            $removedDefault = $this->getDefaultedProductsForRemoval($item, $allItems, $packageModel);
            /** @var Dyna_Checkout_Model_Sales_Quote_Item $pI */
            foreach ($removedDefault as $pI) {
                $this->doRemoveItem($productsInCart, $deletedItems, $pI);
                unset($productsInCart[$pI->getId()]);
            }
        }

        // Remove the item itself
        $this->doRemoveItem($productsInCart, $deletedItems, $item);

        return $productsInCart;
    }

    /**
     * Compute if the item being removed has a defaulted item that should also be removed
     *
     * @param $removedItem
     * @param $allItems
     * @param $package Dyna_Package_Model_Package
     * @return array
     * @throws Mage_Core_Model_Store_Exception
     */
    protected function getDefaultedProductsForRemoval($removedItem, $allItems, $package)
    {
        $websiteId = Mage::app()->getStore()->getWebsiteId();
        /** @var Dyna_ProductMatchRule_Helper_Data $productMatchHelper */
        $productMatchHelper = Mage::helper('dyna_productmatchrule');

        $productId = $removedItem->getProductId();
        $rules = $productMatchHelper->getDefaultedProducts([$productId], $websiteId, $package, Dyna_ProductMatchRule_Helper_Data::SOURCE_COLLECTION_CQ, true);

        $rules = array_filter($rules);
        $found = [];

        if (!empty($rules[$productId]) || !empty($rules['service'])) {
            $packageId = $removedItem->getPackageId();

            $this->getExtraLogHelper()->extraLog("In get default products for removal: ");
            $this->getExtraLogHelper()->extraLog($rules);

            if (!empty($rules[$productId])) {
                $normalAddedProducts = array_column($rules[$productId], "target_product_id");
            }
            if (!empty($rules['service'])) {
                $serviceDefaultedProducts = array_column($rules['service'], "target_product_id");
            }
            $defaultAddedProducts = array_merge($normalAddedProducts ?? [], $serviceDefaultedProducts ?? []);

            foreach ($allItems as $foundItem) {
                if ($packageId == $foundItem->getPackageId()
                    // If item has been added by service rules, it will be removed and added again
                    && (in_array($foundItem->getProductId(), $defaultAddedProducts) || $foundItem->getIsServiceDefaulted())
                    && $foundItem->getIsDefaulted()
                ) {
                    $found[$foundItem->getProductId()] = $foundItem;
                }
            }
        }

        return $found;
    }

    /**
     * @param $productsInCart
     * @param $deletedItems
     * @param $pI
     */
    protected function doRemoveItem(&$productsInCart, &$deletedItems, $pI)
    {
        $this->getExtraLogHelper()->extraLog("Entered remove product: " . $pI->getProductId());
        $this->_getCart()->removeItem($pI->getId());
        $deletedItems[$pI->getId()] = true;
        if (in_array($pI->getProductId(), $productsInCart)) {
            foreach ($productsInCart as $productsInCartKey => $productInCartId) {
                if ($productInCartId == $pI->getProductId()) {
                    $this->getExtraLogHelper()->extraLog("Removing and un-setting product: " . $pI->getProductId());
                    unset($productsInCart[$productsInCartKey]);
                    unset($this->_exclusiveFamilyCategoryIds[$pI->getProductId()]);
                }
            }
        }
    }

    /**
     * Checks whether the product is eligible with the rest of the cart.
     *
     * @param $productsInCart
     * @param $allItems
     * @param $packageId
     * @param $type
     * @throws Exception
     */
    protected function validateProductsDE(&$productsInCart, $allItems, $packageId, $type)
    {
        $websiteId = Mage::app()->getWebsite()->getId();

        $validProducts = $this->getConfiguratorCartHelper()
            ->getCartRules($productsInCart, [], $websiteId, false, $type);

        $invalidProducts = [];
        $quoteId = null;
        $isCablePackage = in_array(strtolower($type), Dyna_Catalog_Model_Type::getCablePackages());

        /** @var Dyna_Checkout_Model_Sales_Quote_Item $item */
        foreach ($allItems as $item) {
            $quoteId = $item->getQuoteId();
            if ($item->getPackageId() == $packageId) {

                if ($item->getIsContract()
                    && !in_array($item->getProductId(), $validProducts)
                    && strtolower($this->getContextHelper()->getProcessContextCode()) != strtolower(Dyna_Catalog_Model_ProcessContext::ACQ)
                ) {
                    $item->isDeleted(true);
                    $productsInCart = array_diff($productsInCart, [$item->getProductId()]);
                } elseif ($item->getProduct()->isSim()
                    || (!$isCablePackage
                        && !$item->getProduct()->getInitialSelectable()
                        && !$item->getBundleChoice()
                        && empty(array_intersect($validProducts, [$item->getProductId()]))
                    )
                ) {
                    // Option of option logic
                    // Remove products that are not initially selectable and are not found in allowed rules
                    $item->isDeleted(true);
                    $productsInCart = array_diff($productsInCart, [$item->getProductId()]);
                } elseif ($item->getProduct()->getType() && !$item->getProduct()->is(Omnius_Catalog_Model_Type::SUBTYPE_NONE)) {
                    if (in_array($item->getProduct()->getId(), $validProducts)
                        || $item->getProduct()->isServiceItem()
                    ) {
                        continue;
                    }
                    $invalidProducts[$item->getProduct()->getSku()] = $item->getProduct()->getDisplayNameConfigurator() ?: $item->getProduct()->getName();
                }
            }
        }
        /**
         * Try and make the error message more explicit by pointing to the exact items that don't have the needed rules
         */
        if (count($invalidProducts)) {
            $adapter = Mage::getSingleton('core/resource')->getConnection('core_read');
            if (!empty($validProducts)) {
                $validProductSkus = array_column($adapter
                    ->fetchAll('SELECT sku FROM catalog_product_entity WHERE entity_id IN (' . implode(', ', array_filter(array_map('intval', $validProducts))) . ')'),
                    'sku');
            } else {
                $validProductSkus = ["No available skus"];
            }

            if ($productsInCart) {
                $cartProducts = array_column($adapter
                    ->fetchAll('SELECT sku FROM catalog_product_entity WHERE entity_id IN (' . implode(', ', array_filter(array_map('intval', $productsInCart))) . ')'),
                    'sku');
            } else {
                $cartProducts = ["No products in cart"];
            }

            $logMessage = sprintf(
                "\n!!!=======================!!!\n"
                . "Quote ID: %s\nProducts in cart: %s\nInvalid: %s\nAllowed: %s\n"
                . "!!!=======================!!!\n",
                $quoteId,
                join(', ', $cartProducts),
                join(', ', $invalidProducts),
                join(', ', $validProductSkus)
            );

            /**
             * Log this info to help debugging
             */
            Mage::log($logMessage, Zend_Log::NOTICE, 'missing_product_allowed_rules.log', true);

            throw new Exception(sprintf(
                '%s.<br/><strong>%s:</strong> %s',
                $this->__("The following products cannot be added to the cart because they don't have allowed rules"),
                $this->__("Invalid"),
                join(', ', $invalidProducts)
            ));
        }
    }

    /**
     * @param $added
     * @param $packageModel Dyna_Package_Model_Package
     * @param $finalProducts
     * @param $allExclusiveFamilyCategoryIds
     * @param $exclusiveFamilyCategoryIds
     * @return array
     */
    public function parseServiceDefaultedProducts($added, $packageModel, $finalProducts, &$allExclusiveFamilyCategoryIds, &$exclusiveFamilyCategoryIds)
    {
        // Check if there are "defaulted" products, and if there are, add them to the cart
        $addedDefault = $this->getDefaultedProducts($added, $packageModel);
        $addedDefaultItems = [];
        $packageType = $packageModel->getType();

        if ($addedDefault && count($addedDefault) && $packageModel->allowDefaulted()) {
            // skip adding service defaulted products that were previously deselected by agent
            $packageDeselectedIds = array_filter(explode(",", $packageModel->getDeselectedDefaultedItems()));
            foreach ($addedDefault as $key => $toAdd) {
                if ($key == 'service') {
                    $productsWithFlag = [];
                    foreach ($toAdd as $itemToAdd) {
                        $serviceProductIds = [];

                        // Check if the products are still compatible
                        $validProducts = $this->getConfiguratorCartHelper()
                            ->getCartRules($finalProducts, array(), null, false, $packageType);

                        $productId = $itemToAdd['target_product_id'];
                        $serviceProductIds[] = $productId;
                        $productsWithFlag[$productId] = $itemToAdd['obligated'];

                        $arrayToAdd = array_intersect($serviceProductIds, $validProducts);
                        $arrayToAdd = array_diff($arrayToAdd, $packageDeselectedIds);
                        $arrayToAdd['service'] = 1;
                        $this->addDefaultedProducts($finalProducts, $allExclusiveFamilyCategoryIds,
                            $exclusiveFamilyCategoryIds, $arrayToAdd, $addedDefaultItems, $productsWithFlag);
                    }
                }
            }
        }

        return $addedDefaultItems;
    }

    /**
     * Compute if the combination has a defaulted item that is not in the package
     *
     * @param $productsIds
     * @param Dyna_Package_Model_Package $packageModel
     * @return array
     * @internal param $products
     */
    public function getDefaultedProducts($productsIds, $packageModel, $sourceCollectionId = Dyna_ProductMatchRule_Helper_Data::SOURCE_COLLECTION_CQ)
    {
        $current = [];
        foreach ($productsIds as $productId) {
            $current[$productId] = $productId;
        }
        /** @var Dyna_ProductMatchRule_Helper_Data $helper */
        $helper = Mage::helper('productmatchrule');

        $rules = $helper->getDefaultedProducts($current, null, $packageModel, $sourceCollectionId);
        if (isset($rules['service'])) {
            $serviceRules = $rules['service'];
        }
        // Loading defaulted rules for entire cart
        $found = $rules;
        foreach ($found as $productId => $defaulted) {
            foreach ($defaulted as $index => $item) {
                // If defaulted product already present in cart, skip it
                if (isset($current[$item["target_product_id"]])) {
                    unset($found[$productId][$index]);
                }
            }
        }

        if (isset($serviceRules)) {
            // Persist the products added by service expressions
            $found['service'] = $serviceRules;
        }

        return $found;
    }

    /**
     * @param $finalProducts
     * @param $allExclusiveFamilyCategoryIds
     * @param $exclusiveFamilyCategoryIds
     * @param $packageType
     * @param $arrayToAdd
     * @param $addedDefaultItems
     */
    protected function addDefaultedProducts(&$finalProducts, &$allExclusiveFamilyCategoryIds, &$exclusiveFamilyCategoryIds, $arrayToAdd, &$addedDefaultItems, $productsWithFlags = [])
    {
        if ($arrayToAdd) {
            $this->getExtraLogHelper()->extraLog("Initial array to add: ");
            $this->getExtraLogHelper()->extraLog($arrayToAdd);
        }

        $isServiceDefaulted = false;
        if (isset($arrayToAdd['service'])) {
            unset($arrayToAdd['service']);
            $isServiceDefaulted = true;
        }

        $currentWebsite = Mage::app()->getWebsite()->getId();
        /** @var Omnius_Catalog_Model_Resource_Product_Collection $defaultedCollection */
        $defaultedCollection = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('footnote_id')
            ->addWebsiteFilter($currentWebsite)
            ->addIdFilter($arrayToAdd);

        $quote = $this->_getQuote();
        foreach ($arrayToAdd as $itemToAdd) {
            $product = $defaultedCollection->getItemById($itemToAdd);
            if (!$product) {
                // Skip products that don't exist
                $this->getExtraLogHelper()->extraLog("Skipping as product doesn't exist: " . $itemToAdd);
                continue;
            }

            if (!$product->getFootnoteId()) {
                // Product is part of a mutually exclusive category
                if ($categoriesExclusive = array_intersect($product->getCategoryIds(), $allExclusiveFamilyCategoryIds)) {
                    if (array_intersect($product->getCategoryIds(), $exclusiveFamilyCategoryIds)) {
                        $this->getExtraLogHelper()->extraLog("Skipping from exclusive category: " . $itemToAdd);
                        // There is already a product in this mutually exclusive category
                        continue;
                    } else {
                        // Add the categories to the list so we make sure the next product is not in them
                        foreach ($categoriesExclusive as $categoryExclusive) {
                            $exclusiveFamilyCategoryIds[$product->getId()] = $categoryExclusive;
                        }
                    }
                }
            }

            $newItem = $this->addProductToCart($itemToAdd, true, $isServiceDefaulted, $productsWithFlags[$itemToAdd]);
            if ($newItem) {
                $addedDefaultItems[] = $newItem;
                // Add the product to the list to make sure the next ones are also compatible with it
                array_push($finalProducts, $itemToAdd);
            }
        }
        Mage::dispatchEvent('current_quote_content_changed', array('quote' => $quote));
    }

    /**
     * @param $productId
     * @param $packageType
     * @param bool $isDefaulted
     * @param bool $isServiceDefaulted
     * @return bool|Dyna_Checkout_Model_Sales_Quote_Item
     * @param bool $saleType
     * @return bool
     */
    public function addProductToCart($productId, $isDefaulted = false, $isServiceDefaulted = false, $isObligated = false, $saleType = false)
    {
        $productId = (int)$productId;
        $websiteId = Mage::app()->getWebsite()->getId();
        if (($emulatedWebsiteId = Mage::helper('superorder')->shouldEmulateStore($websiteId)) != $websiteId) {
            // If a product from another shop should be added, emulate that store
            $storeId = Mage::app()->getWebsite($emulatedWebsiteId)->getDefaultStore()->getId();
        } else {
            $storeId = Mage::app()->getStore()->getId();
        }
        /** @var Dyna_Checkout_Model_Cart $cart */
        $cart = $this->_getCart();

        $numberCtns = count($this->_getCart()->getQuote()->getPackageCtns());
        $isOneOfDeal = Mage::helper('omnius_configurator/cart')->isOneOfDealActive();

        /** Mage_Sales_Model_Quote_Item $item */
        if (!($item = $this->exists($productId))) {
            if ($productId) {
                $product = Mage::getModel('catalog/product')
                    ->setStoreId($storeId)
                    ->load($productId);
                if (!$product->getId()) {
                    Mage::throwException('Invalid product ID given');
                }

                if ($product->getData('is_deleted')) {
                    // Skip products that are marked as "deleted" in backend
                    return Mage::getModel('sales/quote_item');
                }
            } else {
                Mage::throwException('Invalid product ID given');
            }
            $result = $cart->addProduct($product, array(
                    'qty' => $numberCtns ?: 1,
                    'one_of_deal' => ($isOneOfDeal && $numberCtns) ? 1 : 0
                )
            );

            if (is_string($result)) {
                return false;
            } else {
                $item = $result;
            }
        } else {
            $item->isDeleted(false);
            $this->getExtraLogHelper()->extraLog("Skipping product: " . $productId);
        }

        // Make sure the item has the flag so we know when to delete it
        if ($item) {
            if ($isDefaulted) {
                $item->setIsDefaulted(true);
                $item->setSystemAction(Dyna_Checkout_Model_Sales_Quote_Item::SYSTEM_ACTION_ADD);
            }
            if ($isServiceDefaulted) {
                $item->setIsServiceDefaulted(true);
                $item->setSystemAction(Dyna_Checkout_Model_Sales_Quote_Item::SYSTEM_ACTION_ADD);
            }
            if ($isObligated) {
                $item->setIsObligated($isObligated);
                $item->setSystemAction(Dyna_Checkout_Model_Sales_Quote_Item::SYSTEM_ACTION_ADD);
            }
            if ($saleType) {
                $item->setSaleType($saleType);
            }
        }

        return $item;
    }

    /**
     * @param $added
     * @param $packageModel Dyna_Package_Model_Package
     * @param $finalProducts
     * @param $allExclusiveFamilyCategoryIds
     * @param $exclusiveFamilyCategoryIds
     * @return array
     */
    protected function parseDefaultedProducts($added, $packageModel, $finalProducts, &$allExclusiveFamilyCategoryIds, &$exclusiveFamilyCategoryIds)
    {
        /** @var Dyna_Configurator_Helper_Expression $serviceHelper */
        $serviceHelper = Mage::helper('dyna_configurator/expression');
        $serviceHelper->updateInstance($this->_getQuote());
        Mage::register("no_updated_for_expression_helper_needed", true);

        /** @var Dyna_ProductMatchRule_Helper_Data $productMatchHelper */
        $productMatchHelper = Mage::helper('dyna_productmatchrule');
        $debugRules = $productMatchHelper->isDebugMode();

        // Check if there are "defaulted" products, and if there are, add them to the cart
        $addedDefault = $this->getDefaultedProducts($added, $packageModel);
        $packageType = $packageModel->getType();
        $addedDefaultItems = [];
        if ($debugRules) {
            $productMatchHelper->logRulesDebug("************************************************************");
            $productMatchHelper->logRulesDebug("****ADDING DEFAULTED PRODUCTS******");
            $productMatchHelper->logRulesDebug("************************************************************");
            $productMatchHelper->logRulesDebug("****The list of defaulted products ids: " . json_encode($addedDefault));
        }

        if ($addedDefault && count($addedDefault) && $packageModel->allowDefaulted()) {
            // Recalculate valid combinations
            foreach ($addedDefault as $key => $toAdd) {
                if (empty($toAdd)) {
                    continue;
                }
                // If skipped from compatibility rules, continue
                $targetProductIds = array_column($toAdd, 'target_product_id');
                $obligatedFlags = array_column($toAdd, 'obligated');
                $productsWithFlag = array_combine($targetProductIds, $obligatedFlags);
                // Check if the products are still compatible
                if (strtolower($packageModel->getSaleType()) == strtolower(Dyna_Catalog_Model_ProcessContext::ACQ)) {
                    $validProducts = $this->getConfiguratorCartHelper()
                        ->getCartRules($finalProducts, array(), null, false, $packageType);
                    if ($debugRules) {
                        $productMatchHelper->logRulesDebug("****[CQ] Getting compatible products for: " . json_encode($finalProducts));
                        $productMatchHelper->logRulesDebug("****[CQ] Compatible products: " . json_encode($validProducts));
                    }
                } else {
                    $finalProducts = array_map(function ($item) {
                        return $item->getProductId();
                    }, $packageModel->getItemsByProductId($finalProducts, $packageModel->ilsTariffChanged()));
                    // filter inlife ones from final products
                    $validProducts = $this->getConfiguratorCartHelper()->getCartRules(
                        $finalProducts,
                        array(),
                        null,
                        false,
                        $packageType
                    );

                    if ($debugRules) {
                        $productMatchHelper->logRulesDebug("****[IB] Getting compatible products for: " . json_encode($finalProducts));
                        $productMatchHelper->logRulesDebug("****[IB] Compatible products: " . json_encode($validProducts));
                    }
                }



                if ($key != 'service') {
                    $arrayToAdd = array_intersect($targetProductIds, $validProducts);
                    $this->_incompatibleDefaultedProducts = array_unique(array_merge($this->_incompatibleDefaultedProducts, array_diff($targetProductIds, $validProducts)));
                    $this->addDefaultedProducts($finalProducts, $allExclusiveFamilyCategoryIds, $exclusiveFamilyCategoryIds, $arrayToAdd, $addedDefaultItems, $productsWithFlag);
                } else {
                    $serviceProductIds = [];
                    $productsWithFlag = [];
                    foreach ($toAdd as $itemToAdd) {
                        $productId = $itemToAdd['target_product_id'];
                        $serviceProductIds[] = $productId;
                        $productsWithFlag[$productId] = $itemToAdd['obligated'];
                    }
                    $arrayToAdd = array_intersect($serviceProductIds, $validProducts);
                    // skip adding service defaulted products that were previously deselected by agent
                    $packageDeselectedIds = array_filter(explode(",", $packageModel->getDeselectedDefaultedItems()));
                    $arrayToAdd = array_diff($arrayToAdd, $packageDeselectedIds);
                    $finalProducts = array_diff($finalProducts, $packageDeselectedIds);
                    // refresh the list of deselected products as some of these might not be defaulted by services anymore so we need to remove them
                    $packageDeselectedIds = array_intersect($packageDeselectedIds, $serviceProductIds);
                    $packageModel->setDeselectedDefaultedItems(implode(",", $packageDeselectedIds));
                    $arrayToAdd['service'] = 1;
                    $this->_incompatibleDefaultedProducts = array_unique(array_merge($this->_incompatibleDefaultedProducts, array_diff($serviceProductIds, $validProducts)));
                    $this->addDefaultedProducts($finalProducts, $allExclusiveFamilyCategoryIds, $exclusiveFamilyCategoryIds, $arrayToAdd, $addedDefaultItems, $productsWithFlag);
                }
            }
        }

        if ($debugRules) {
            $productMatchHelper->logRulesDebug("************************************************************");
            $productMatchHelper->logRulesDebug("****EOF ADDING DEFAULTED PRODUCTS******");
            $productMatchHelper->logRulesDebug("************************************************************");
        }

        Mage::unregister("no_updated_for_expression_helper_needed");

        return $addedDefaultItems;
    }

    /**
     * Method that handles default added product for all items in cart and filter only fees
     * @param $defaultedQuoteItems Quote items of products that were defaulted
     * @return array
     */
    protected function parseFees($defaultedQuoteItems)
    {
        $possibleFees = [];
        foreach ($defaultedQuoteItems as $defaultedQuoteItem) {
            $possibleFees[$defaultedQuoteItem->getProductId()] = array(
                'product_id' => $defaultedQuoteItem->getProductId(),
                'obligated' => $defaultedQuoteItem->getIsObligated()
            );
        }

        /** @var Dyna_Catalog_Helper_Data $catalogHelper */
        $catalogHelper = Mage::helper('dyna_catalog');
        $feeProductIds = $catalogHelper->filterFeeProductIds(array_keys($possibleFees));

        $addedItems = [];
        if (empty($feeProductIds)) {
            return [];
        }

        foreach ($feeProductIds as $feeId) {
            if (!$this->_getQuote()->hasProductId($feeId)
                && ($item = $this->addProductToCart($feeId, true, false, $possibleFees[$feeId]['obligated']))
            ) {
                $addedItems[] = $item;
            }
        }

        return $addedItems;
    }

    /**
     * @param Dyna_Package_Model_Package $packageModel
     */
    protected function parseOwnHardware(Dyna_Package_Model_Package $packageModel)
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = $this->_getCart()->getQuote();
        $items = $quote->getPackageItems($packageModel->getPackageId());

        $products = [];
        foreach ($items as $item) {
            $products[] = $item->getProductId();
        }

        /** @var Dyna_Customer_Model_Customer $customer */
        $customer = Mage::getSingleton('customer/session')->getCustomer();

        // If no products in cart, or own customer hardware has changed
        if ($customer->hasOwnHardware() && empty($products)) {
            $customer->clearOwnReceiver()
                ->clearOwnSmartcard();
            $packageModel->setSmartcardSerialNumber(NULL);
            $packageModel->setReceiverSerialNumber(NULL);
            $packageModel->save();
        }

        // If customer has own hardware, set serial on package
        if ($customer->hasOwnHardware() && !empty($products)) {
            if ($customer->getOwnReceiver()) {
                $packageModel->setReceiverSerialNumber(
                    Mage::helper('core')->jsonEncode([
                        'productSku' => $customer->getOwnReceiver()->getProductSku(),
                        'sn' => $customer->getOwnReceiver()->getSerialNumber(),
                    ])
                );
            }
            if ($customer->getOwnSmartCard()) {
                $packageModel->setSmartcardSerialNumber(
                    Mage::helper('core')->jsonEncode([
                        'productSku' => $customer->getOwnSmartCard()->getProductSku(),
                        'sn' => $customer->getOwnSmartCard()->getSerialNumber(),
                    ])
                );
            }
            $packageModel->save();
        }

        // OMNVFDE-699: Implement the service GetIpEquipmentFeatures from OGW
        // If current package is of type cable and contains modem, execute service call and get modem specifications
        if (strtolower($packageModel->getType()) === strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_TV)) {
            $hasModem = false;

            // Check if modem exists in cart and update session customer to include modem data
            foreach ($products as $productId) {
                /** @var Dyna_Catalog_Model_Product $product */
                $product = Mage::getModel('catalog/product')->load($productId);
                if ($product->getSubType() == Dyna_Catalog_Model_Type::CABLE_MODEM_SUBTYPE) {
                    $hasModem = true;
                    $newServiceRequest = false;
                    // Check if customer already has one
                    if ($customerModem = $customer->getCustomerModem()) {
                        if ($customerModem->getJobCode() != $product->getSku()) {
                            $newServiceRequest = true;
                        }
                    } else {
                        $newServiceRequest = true;
                    }

                    // A new service request needs to be made for current this modem
                    if ($newServiceRequest) {
                        /** @var Dyna_Configurator_Model_Client_IpEquipmentFeaturesClient $client */
                        $client = Mage::getModel('dyna_configurator/client_ipEquipmentFeaturesClient');
                        $params = array(
                            "JobCode" => $product->getSku(),
                        );
                        // Execute service request for modem data
                        $modemData = $client->executeGetIpEquipmentFeatures($params);
                        // Set service response on customer model
                        $modemDataObject = new Varien_Object();
                        $modemDataObject->addData($modemData);
                        $customer->setCustomerModem($modemDataObject);
                    }

                }
            }

            // Check if there is a modem set on customer that is no longer in cart and remove it
            if (!$hasModem && $customer->getCustomerModem()) {
                $customer->setCustomerModem(null);
            }
        }
    }

    /**
     * Checks whether a product already exists in the current package of the shopping cart.
     * @param Mage_Catalog_Model_Product $product
     * @return bool|Mage_Sales_Model_Quote_Item
     */
    protected function exists($productId)
    {
        /** @var Mage_Sales_Model_Quote_Item $quoteItem */
        foreach ($this->_getQuote()->getItemsCollection() as $quoteItem) {
            if ($quoteItem->getPackageId() == $this->_getQuote()->getActivePackageId()
                && $quoteItem->getProductId() == $productId
            ) {
                return $quoteItem;
            }
        }

        return false;
    }

    /**
     * @param $packageId
     * @param $productId
     * @param $type
     * @param $appliedRulesArray
     * @param $added
     * @return Dyna_Checkout_Model_Sales_Quote_Item
     */
    protected function parseProduct($packageId, $productId, $appliedRulesArray, &$added)
    {
        Mage::unregister('add_multi_package_id');
        Mage::register('add_multi_package_id', $packageId);
        $item = $this->addProductToCart($productId);

        $allExclusiveFamilyCategoryIds = Mage::getModel('catalog/category')->getAllExclusiveFamilyCategoryIds();
        if ($item && ($categoriesExclusive = array_intersect($item->getProduct()->getCategoryIds(), $allExclusiveFamilyCategoryIds))
            && ($item->getPackageId() == $packageId)
        ) {
            foreach ($categoriesExclusive as $categoryExclusive) {
                $this->_exclusiveFamilyCategoryIds[$item->getProductId()] = $categoryExclusive;
            }
        }

        if ($item && !empty($appliedRulesArray) && isset($appliedRulesArray[$item->getPackageId() . '-' . $item->getProductId()])) {
            $item->setAppliedRuleIds(implode(',', $appliedRulesArray[$item->getPackageId() . '-' . $item->getProductId()]));
        }

        array_push($added, $productId);

        return $item;
    }

    /**
     * Return the cart helper
     * @return Dyna_Configurator_Helper_Cart|Mage_Core_Helper_Abstract
     */
    protected function getConfiguratorCartHelper()
    {
        if ($this->configuratorCartHelper === false) {
            $this->configuratorCartHelper = Mage::helper('dyna_configurator/cart');
        }

        return $this->configuratorCartHelper;
    }

    /**
     * Remove a certain bundle from cart
     * @param Dyna_Package_Model_Package $package
     * @throws Exception
     */
    protected function checkAndRemoveBundle(Dyna_Package_Model_Package $package)
    {
        // get gigakombi bundle for current package
        $bundle = $package->getBundleOfType(Dyna_Bundles_Model_BundleRule::TYPE_KOMBI);
        // if not a gigakombi bundle, try intrastack
        if ($bundle || $bundle = $package->getBundleOfType(Dyna_Bundles_Model_BundleRule::TYPE_INTRA)) {
            $bundleId = $bundle->getId();
            /** @var Dyna_Package_Helper_Data $packageHelper */
            $packageHelper = Mage::helper('dyna_package');
            /** @var Dyna_Bundles_Helper_Data $bundleHelper */
            $bundleHelper = Mage::helper('dyna_bundles');
            foreach ($package->getPackagesInSameBundle($bundleId) as $package) {
                // Is a dummy package, let's remove it
                if ($package->getEditingDisabled()) {
                    $packageHelper->removePackage($package->getPackageId());
                    continue;
                }

                // Mark all items for deletion (clear package)
                foreach ($package->getAllItems() as $item) {
                    $item->isDeleted(true);
                }
                $bundleHelper->removeBundlePackageEntry($bundleId, $package->getId());
                $package->updateBundleTypes();
            }
            Mage::getSingleton('customer/session')->setBundleRuleParserResponse(null);
        }
    }

    /**
     * @param Dyna_Package_Model_Package $package
     */
    protected function checkAndRemoveSusoBundle(Dyna_Package_Model_Package $package)
    {
        if ($package->isPartOfSuso()) {
            /** @var Dyna_Bundles_Helper_Data $bundlesHelper */
            $bundlesHelper = Mage::helper('dyna_bundles');

            $packagesInSameBundle = $package->getPackagesInSameBundle($package->getBundleOfType(Dyna_Bundles_Model_BundleRule::TYPE_SUSO)->getId(), true);
            $bundlePackage = null;

            foreach ($packagesInSameBundle as $packageInBundle) {
                if ($packageInBundle->isPrepaid() && !$package->isPrepaid()) {
                    $bundlePackage = $packageInBundle;
                    break;
                } elseif (!$packageInBundle->isPrepaid() && $package->isPrepaid()) {
                    $bundlePackage = $package;
                    $package = $packageInBundle;
                    break;
                }
            }

            if ($bundlePackage) {
                Mage::helper('dyna_package')->removePackage($bundlePackage->getPackageId());

                if ($package->getBundleOfType(Dyna_Bundles_Model_BundleRule::TYPE_SUSO)) {
                    $bundlesHelper->removeBundlePackageEntry(
                        $package->getBundleOfType(Dyna_Bundles_Model_BundleRule::TYPE_SUSO)->getId(),
                        $package->getEntityId()
                    );
                }
            } else {
                Mage::throwException('Cannot find the prepaid package in the SurfSofort Bundle');
            }
        }
    }

    /**
     * Return the cart helper
     * @return Dyna_Configurator_Helper_ExtraLogCart|Mage_Core_Helper_Abstract
     */
    protected function getExtraLogHelper()
    {
        if ($this->logRulesHelper === false) {
            $this->logRulesHelper = Mage::helper('dyna_configurator/extraLogCart');
        }

        return $this->logRulesHelper;
    }

    /**
     * Retrieve shopping cart model object
     * @return Dyna_Checkout_Model_Cart
     */
    protected function _getCart()
    {
        if ($this->cart === false) {
            $this->cart = Mage::getSingleton('checkout/cart');
        }

        return $this->cart;
    }

    /**
     * Get current active quote instance
     *
     * @return Dyna_Checkout_Model_Sales_Quote
     */
    protected function _getQuote()
    {
        return $this->_getCart()->getQuote();
    }

    /**
     * @param $productIds
     * @param Dyna_Package_Model_Package $package
     * @return bool
     */
    protected function removeProductFromCart($productIds, $package)
    {
        if (!is_array($productIds)) {
            $productIds = array($productIds);
        }
        $productIds = array_filter(array_filter($productIds, 'trim'));
        foreach ($package->getAllItems() as $packageItem) {
            if (in_array($packageItem->getProductId(), $productIds)) {
                $packageItem->isDeleted(true);
            }
        }
        return true;
    }

    /**
     * @param $packageModel
     * @param $item Mage_Sales_Model_Quote_Item
     * @param $removedDefaultFlag
     * @param $removeSim
     * @param $removedDefault
     */
    protected function doRemoveItemOfAll($packageModel, $item, &$removedDefaultFlag, &$removedDefault)
    {
        $this->_getCart()->removeItem($item->getId());
        if ($item->getIsDefaulted() || $item->getIsServiceDefaulted()) {
            $removedDefaultFlag = true;
            $packageDeselectedIds = array_filter(explode(",", $packageModel->getDeselectedDefaultedItems()));
            if (!in_array($item->getProductId(), $packageDeselectedIds)) {
                $packageDeselectedIds[] = $item->getProductId();
                $packageModel->setDeselectedDefaultedItems(implode(",", $packageDeselectedIds));
            }
        }

        if ($item->getProduct()->isSubscription()) {
            $packageModel->setDeselectedDefaultedItems(null);
        }

        if ($packageModel->allowDefaulted()) {
            // Check if the removed item had defaulted items, and remove them
            $removedDefault = $this->getDefaultedProductsForRemoval($item, $this->_getQuote()->getItemsCollection(), $packageModel);
            foreach ($removedDefault as $pI) {
                $this->_getCart()->removeItem($pI->getId());
            }
        }
    }

    /**
     * Helper methos to invalidate getRules hash when quote products are changed
     * @return string
     */
    public function getQuoteProductsHash()
    {
        $quoteItems = Mage::getSingleton('checkout/cart')->getQuote()->getAllItems();
        $sessionAddress = Mage::getSingleton('dyna_address/storage');
        $quoteProducts = [];
        foreach ($quoteItems as $quoteItem) {
            $quoteProducts[] = $quoteItem->getProductId();
        }
        sort($quoteProducts);

        if (count($quoteProducts)) {
            return md5($sessionAddress->getServiceAddress(true) . implode(',', $quoteProducts));
        } else {
            return md5($sessionAddress->getServiceAddress(true));
        }
    }

    /**
     * @return Dyna_Catalog_Helper_ProcessContext
     */
    protected function getContextHelper()
    {
        return Mage::helper("dyna_catalog/processContext");
    }

    /**
     * Throw Exception caught for returning a json response
     * @param $message
     * @throws Dyna_Checkout_Model_Exception_Json
     */
    public static function throwJsonException($message)
    {
        /** @var Dyna_Checkout_Model_Exception_Json $exception */
        $exception = Mage::getModel('dyna_checkout/exception_json', $message);
        throw $exception;
    }

    /**
     * Throw Exception caught for setting it on customer session (removed after page reload)
     * @param $message
     * @throws Dyna_Checkout_Model_Exception_Session
     */
    public static function throwSessionException($message)
    {
        /** @var Dyna_Checkout_Model_Exception_Session $exception */
        $exception = Mage::getModel('dyna_checkout/exception_session', $message);
        throw $exception;
    }

    /**
     * @param Dyna_Checkout_Model_Sales_Quote $quote
     * @param $controllerInstance Dyna_Configurator_CartController
     * @return string[]
     */
    public function validateSavedQuote(Dyna_Checkout_Model_Sales_Quote $quote)
    {
        $initialQuote = $quote;
        $quote =
            $quote->cloneQuote(false)
                ->setQuoteParentId($quote->getId());
        // as there is a new quote, clear old cached packages
        $quote->clearCachedPackages();
        // we'll keep all validation warning in this variable for later to be set on session
        $validationErrors = array();
        /** @var Dyna_Checkout_Helper_Data $checkoutDataHelper */
        $checkoutDataHelper = Mage::helper('dyna_checkout');
        /** @var Dyna_Customer_Helper_Data $customerHelper */
        $customerHelper = Mage::helper('dyna_customer');
        /** @var Dyna_Configurator_Helper_Rules $rulesHelper */
        $rulesHelper = Mage::helper('dyna_configurator/rules');
        /** @var Dyna_Customer_Model_Session $customerSession */
        $customerSession = Mage::getSingleton('customer/session');

        // Check if agent has the right permissions for loading shopping cart
        if ($quote->getCartStatus() == Omnius_Checkout_Model_Sales_Quote::CART_STATUS_SAVED && !$customerSession->getAgent()->isGranted(array('CHANGE_SAVED_SHOPPINGCART'))) {
            $this::throwJsonException($this->__("You do not have the permission for this action"));
        }

        // Check if agent has the right permissions for loading offer
        if ($quote->getIsOffer() && !$customerHelper->hasPermissionLoadOffer(Mage::getModel('agent/agent')->load($quote->getAgentId()))) {
            $this::throwJsonException($this->__("You do not have the permission for this action"));
        }

        // Check for offer/shopping cart to still be valid
        if ($checkoutDataHelper->isOfferExpired($quote->getCreatedAt()) || $checkoutDataHelper->isOfferReadonly($quote->getCreatedAt())) {
            $this::throwJsonException($this->__('Cannot continue with expired/readonly ' . ($quote->getIsOffer() ? 'offer' : 'cart') . '.'));
        }

        // Check quote for valid campaign
        if ($campaignCode = $quote->getCampaignCode()) {
            /** @var Dyna_Bundles_Model_Campaign $campaign */
            $campaign = Mage::getModel('dyna_bundles/campaign');
            // if campaign no longer exists or it is expired notify frontend that offer is invalid
            if (!($campaignId = $campaign::getIdByCampaignCode($campaignCode)) || !$campaign->load($campaignId)->isValid()) {
                $validationErrors['campaignInvalid'] = $this->__(($quote->isOffer() ? "Offer" : "Shopping cart") . " no longer valid, please check content");
            }
        }

        // Checking pending orders for current customer (if pending orders found, cannot load offer / shopping cart @see OMNVFDE-4616)
        // Updated pending orders logic per stack (@see OMNVFDE-4739)
        $packageTypes = array();
        foreach ($quote->getCartPackages(true) as $package) {
            // existence of a mobile package in a pending order is considered pending for all mobile packages
            if ($package->isMobile()) {
                $packageTypes = array_merge($packageTypes, Dyna_Catalog_Model_Type::getMobilePackages());
            }
            // existence of a cable package a pending order is considered pending for that specific cable package
            if ($package->isCable()) {
                $packageTypes = array_merge($packageTypes, [$package->getType()]);
            }
            // existence of a fixed package in a pending order is out of scope
            if ($package->isFixed()) {
                continue;
            }
        }

        if (($customerNumber = $customerSession->getCustomer()->getCustomerNumber()) && $customerHelper->hasOpenOrders($customerNumber, $packageTypes)) {
            $validationErrors['openOrders'] = $this->__("Not able to continue due to open orders");
            $this::throwJsonException($this->__("Not able to continue due to open orders"));
        }

        /** @var Dyna_Package_Model_Package $activePackage */
        $activePackage = $quote->getCartPackage();

        Mage::getSingleton('dyna_address/storage')->loadServiceabilityByPackage($activePackage);
        if ($quote->getIsOffer()) {
            $servicePackageTypes = array_merge(Dyna_Catalog_Model_Type::getCablePackages(), Dyna_Catalog_Model_Type::getFixedPackages());

            foreach ($quote->getCartPackages(true) as $package) {
                if (in_array(strtolower($package->getType()), $servicePackageTypes)) {
                    if (!$this->hasAvailableService($package->getType())) {
                        $validationErrors['serviceability'] = $this->__("Offer is not valid, as the required technology is no longer available at the service address.");
                        $this::throwJsonException($this->__("Offer is not valid, as the required technology is no longer available at the service address."));
                        break;
                    }
                }
            }
        }

        // gathering all offer products to determine if these are still valid
        $initialProducts = $quote->getAllProductsIds(true);

        // Set new sales id on current quote
        $redSalesId = Mage::helper('bundles')->getRedSalesId();

        // de-activate current quote
        Mage::getSingleton('checkout/cart')->getQuote()->setIsActive(0)->save();
        // Offer mode
        $quote
            ->setIsActive(1)
            ->setSalesId($redSalesId)
            ->setCurrentStep(null);
        Mage::getSingleton('checkout/cart')
            ->setQuote($quote)
            ->setQuoteId($quote->getId());
        // call mixmatch observer to updated prices (might have been changed)
        foreach ($quote->getAllItems() as $item) {
            Mage::dispatchEvent(
                'checkout_cart_product_add_after',
                array(
                    'quote_item' => $item,
                    'quote' => $quote,
                )
            );
        }

        // dummy variables
        $deletedItems = array();
        $removedDefaultFlag = array();
        $removedDefault = array();
        $foundInvalid = false;
        $allExclusiveFamilyCategoryIds = Mage::getModel('catalog/category')->getAllExclusiveFamilyCategoryIds();
        foreach ($quote->getCartPackages(true) as $package) {
            $packageChanges = false;
            $this->_exclusiveFamilyCategoryIds = array();
            $initialPackageProducts = $initialProducts[$package->getPackageId()];
            $initialPackageItems = array();
            $currentPackageProducts = array();
            $expiredItems = array();
            foreach ($quote->getAllItems() as $item) {
                if ($item->getPackageId() == $package->getPackageId()) {
                    // Check product expiration date
                    if ($item->getProduct()->getExpirationDate() && (strtotime($item->getProduct()->getExpirationDate()) < strtotime(date('Y-m-d')))) {
                        $expiredItems[] = $item;
                    } else {
                        $currentPackageProducts[$item->getId()] = $item->getProductId();
                        $initialPackageItems[$item->getProductId()] = $item;
                        if ($categoriesExclusive = array_intersect($item->getProduct()->getCategoryIds(), $allExclusiveFamilyCategoryIds)) {
                            foreach ($categoriesExclusive as $categoryExclusive) {
                                $this->_exclusiveFamilyCategoryIds[$item->getProductId()] = $categoryExclusive;
                            }
                        }
                    }
                }
            }

            // Remove expired products along with default added by it
            foreach ($expiredItems as $item) {
                $packageChanges = true;
                $currentPackageProducts = $this->removeItemAndParseDefault($item, $currentPackageProducts, $package, $quote->getAllItems(), $deletedItems, $removedDefaultFlag, $removedDefault);
                isset($validationErrors['expiredProducts']) ?: $validationErrors['expiredProducts'] = $this->__(($quote->isOffer() ? "Offer" : "Shopping cart") . " no longer valid, please check content");
            }

            // validating offer products against precondition rules
            $validProducts = $rulesHelper->getPreconditionRules($currentPackageProducts, $package->getType());
            // if there are differences between initial products and current products (filtered by expiration date and compatibility rules)
            // not collect totals needed as this quote (offer) will be set to an invalid state and a new quote containing all valid products will be created
            if ($invalidProductIds = array_diff($initialPackageProducts, array_intersect($initialPackageProducts, $validProducts['rules']))) {
                $foundInvalid = true;
                foreach ($invalidProductIds as $productId) {
                    if ($initialPackageItems[$productId]) {
                        $packageChanges = true;
                        $currentPackageProducts = $this->removeItemAndParseDefault($initialPackageItems[$productId], $currentPackageProducts, $package, $quote->getAllItems(), $deletedItems, $removedDefaultFlag, $removedDefault);
                    }
                }
            }

            if ($packageChanges) {
                $this->parseServiceDefaultedProducts(array(), $package, $currentPackageProducts, $allExclusiveFamilyCategoryIds, $this->_exclusiveFamilyCategoryIds);
            }
        }

        if ($foundInvalid) {
            $validationErrors['incompatibleProducts'] = $this->__(($quote->isOffer() ? "Offer" : "Shopping cart") . " no longer valid, please check content");
        }

        // proceed to collecting totals (prices need to be updated @see OMNVFDE-4616)
        Mage::getSingleton('checkout/cart')->save();

        // checking initial products against current products
        foreach ($quote->getAllProductsIds(true) as $packageId => $packageProducts) {
            if (array_diff($initialProducts[$packageId], $packageProducts)) {
                isset($validationErrors['incompatibleProducts']) ?: $validationErrors['incompatibleProducts'] = $this->__(($quote->isOffer() ? "Offer" : "Shopping cart") . " no longer valid, please check content");
            }
        }

        if (!empty($validationErrors)) {
            $initialQuote->setIsInvalid(1)->save();
        }

        return array($validationErrors, $quote);
    }

    /**
     * Removes bundle components if the package is part of an installed base bundle
     *
     * @param Dyna_Package_Model_Package $package
     * @param Dyna_Checkout_Model_Sales_Quote_Item|null $quoteItem
     */
    public function removeContractBundleMarkers(Dyna_Package_Model_Package $package)
    {
        $customer = $this->getCustomer();

        if ($customer && $customer->isCustomerLoggedIn()) {
            $serviceLineId = $package->getServiceLineId();
            $parentAccountNumber = $package->getParentAccountNumber();

            if ($serviceLineId && $parentAccountNumber && $customer->isIBProductPartOfBundle($parentAccountNumber, $serviceLineId)) {
                foreach ($package->getAllItems() as $item) {
                    if ($item->getBundleComponent() && !$item->getProduct()->is(Dyna_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION)) {
                        $item->isDeleted(true);
                    }
                }
                foreach ($package->getAllItems(true) as $item) {
                    if ($item->getProduct()->is(Dyna_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION)) {
                        $item->setBundleComponent(0);
                    }
                }
            }
        }
    }

    /**
     * @param $packageType
     * @return bool
     */
    protected function hasAvailableService($packageType)
    {
        /** @var Dyna_Address_Model_Storage $addressStorage */
        $addressStorage = Mage::getSingleton('dyna_address/storage');
        $cableAvailability = $addressStorage->getServiceAbility()['cableAvailability'];
        $fnAvailability = $addressStorage->getServiceAbility()['fnAvailability'];
        $packageType = strtoupper($packageType);

        $isAvailable = false;

        switch ($packageType) {
            case Dyna_Catalog_Model_Type::TYPE_CABLE_INTERNET_PHONE:
                if ($cableAvailability['KIP'] || $cableAvailability['KAI']) {
                    $isAvailable = true;
                }
                break;
            case Dyna_Catalog_Model_Type::TYPE_CABLE_TV:
                if ($cableAvailability['KAD'] || $cableAvailability['KAA']) {
                    $isAvailable = true;
                }
                break;
            case Dyna_Catalog_Model_Type::TYPE_CABLE_INDEPENDENT:
                if ($cableAvailability['KUD']) {
                    $isAvailable = true;
                }
                break;
            case Dyna_Catalog_Model_Type::TYPE_FIXED_DSL:
                if ($fnAvailability['(V)DSL']) {
                    $isAvailable = true;
                }
                break;
        }

        return $isAvailable;
    }

    /**
     * Determine if quote/order contains a product of specific creation type
     * @param $packages
     * @param $type
     * @param $editingDisabled
     * @return bool
     */
    public function hasPackageOfCreationType($packages, $type, $editingDisabled = false)
    {
        $hasPackage = false;
        /** @var Dyna_Package_Model_Package $package */
        foreach ($packages as $package) {
            if (is_array($type)) {
                if ($editingDisabled) {
                    if (!$package->getEditingDisabled() && in_array(strtolower($package->getPackageCreationTypeId()), $type)) {
                        $hasPackage = true;
                    }
                } else {
                    if (in_array(strtolower($package->getPackageCreationTypeId()), $type)) {
                        $hasPackage = true;
                    }
                }
            } else {
                if ($editingDisabled) {
                    if (!$package->getEditingDisabled() && strtolower($package->getPackageCreationTypeId()) == strtolower($type)) {
                        $hasPackage = true;
                    }
                } else {
                    if (strtolower($package->getPackageCreationTypeId()) == strtolower($type)) {
                        $hasPackage = true;
                    }
                }
            }
            if ($hasPackage) {
                return $hasPackage;
            }
        }

        return false;
    }


    /*
     * Restricting empty package on subscription change only for DSL and LTE: OMNVFDE-2633 / comments / VFDED1W3S-3692 only for ACQ
     * @param $hadSubscription bool
     * @param $type string
     * @param $allItems
     * @param Dyna_Package_Model_Package $packageModel
     * @return bool
     */
    protected function emptyCartOnTariffChangeOrRemove($hadSubscription, $type, &$allItems, Dyna_Package_Model_Package &$packageModel)
    {
        if ($hadSubscription
            && (strtolower($type) == strtolower(Dyna_Catalog_Model_Type::TYPE_FIXED_DSL)
                || strtolower($type) == strtolower(Dyna_Catalog_Model_Type::TYPE_LTE)
            )
            && (strtolower($packageModel->getSaleType()) == strtolower(Dyna_Catalog_Model_ProcessContext::ACQ))
        ) {
            // Removing all items from cart
            /** @var Dyna_Checkout_Model_Sales_Quote_Item $item */
            foreach ($allItems as $item) {
                $item->isDeleted(true);
                unset($this->_exclusiveFamilyCategoryIds[$item->getProductId()]);
            }

            $packageModel->setDeselectedDefaultedItems(null);

            $this->checkAndRemoveSusoBundle($packageModel);
            return true;
        }

        return false;
        // EOF OMNVFDE-2633
    }

    /**
     * Checks whether the product is eligible with the rest of the cart.
     *
     * @param array $productsInCart
     * @param array $allItems
     * @param string $packageId
     * @param string $type
     * @throws Exception
     */
    public function validateProducts(array &$productsInCart, array $allItems, string $packageId, string $type)
    {
        $this->validateProductsDE($productsInCart, $allItems, $packageId, $type);
    }
}
