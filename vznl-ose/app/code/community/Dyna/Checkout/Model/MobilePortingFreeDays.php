<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */


/** Class Dyna_Checkout_Model_MobilePortingFreeDays */
class Dyna_Checkout_Model_MobilePortingFreeDays extends Mage_Core_Model_Abstract
{
    /**
     * Dyna_Checkout_Model_MobilePortingFreeDays constructor.
     */
    protected function _construct() {
        $this->_init('dyna_checkout/mobilePortingFreeDays');
    }
}
