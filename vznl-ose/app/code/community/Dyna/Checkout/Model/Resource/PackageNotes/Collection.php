<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Dyna_Checkout_Model_Resource_PackageNotes_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    /**
     * Dyna_Checkout_Model_Resource_Uct_Collection constructor
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('dyna_checkout/packageNotes');
    }
}
