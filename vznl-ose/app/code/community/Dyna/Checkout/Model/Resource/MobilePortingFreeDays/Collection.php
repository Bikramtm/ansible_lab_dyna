<?php

/**
 * Class Dyna_Checkout_Model_Resource_MobilePortingFreeDays_Collection
 */
class Dyna_Checkout_Model_Resource_MobilePortingFreeDays_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    /**
     * Dyna_Checkout_Model_Resource_MobilePortingFreeDays_Collection constructor
     */
    protected function _construct()
    {
        $this->_init('dyna_checkout/mobilePortingFreeDays');
    }
}
