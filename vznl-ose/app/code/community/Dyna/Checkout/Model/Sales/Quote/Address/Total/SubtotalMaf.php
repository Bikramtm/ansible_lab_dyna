<?php

/**
 * Class Dyna_Checkout_Model_Sales_Quote_Address_Total_SubtotalMaf
 */
class Dyna_Checkout_Model_Sales_Quote_Address_Total_SubtotalMaf extends Omnius_Checkout_Model_Sales_Quote_Address_Total_Subtotal
{
    /**
     * Address item initialization
     *
     * @param  $item Dyna_Checkout_Model_Sales_Quote_Item
     * @return bool
     */
    protected function _initItem($address, $item)
    {
        if ($item instanceof Mage_Sales_Model_Quote_Address_Item) {
            $quoteItem = $item->getAddress()->getQuote()->getItemBySku($item->getSku(), $item->getPackageId());
            // Search for item in alternative items (dropped contract items)
            if (!$quoteItem) {
                $quoteItem = $item->getAddress()->getQuote()->getAlternateItemById($item->getQuoteItemId());
            }
        } else {
            $quoteItem = $item;
        }
        $product = $quoteItem->getProduct();
        $product->setCustomerGroupId($quoteItem->getQuote()->getCustomerGroupId());

        /**
         * Quote super mode flag mean what we work with quote without restriction
         */
        if ($item->getQuote()->getIsSuperMode()) {
            if (!$product) {
                return false;
            }
        } else {
            if (!$product || !$product->isVisibleInCatalog()) {
                return false;
            }
        }

        if ($quoteItem->getParentItem() && $quoteItem->isChildrenCalculated()) {
            $finalMaf = $quoteItem->getParentItem()->getProduct()->getPriceModel()->getChildFinalMaf(
                $quoteItem->getParentItem()->getProduct(),
                $quoteItem->getParentItem()->getQty(),
                $quoteItem->getProduct(),
                $quoteItem->getQty()
            );

            if ($item->getIsContractDrop()) {
                $finalMaf = 0;
            }

            $item->setMaf($finalMaf)
                ->setBaseOriginalMaf($finalMaf);
            $item->calcMafRowTotal();
        } elseif (!$quoteItem->getParentItem() && !$quoteItem->isPromotion()) {
            $finalMaf = $product->getFinalMaf($quoteItem->getQty());
            if ($item->getIsContractDrop()) {
                $finalMaf = 0;
            }

            $item->setMaf($finalMaf)
                ->setBaseOriginalMaf($finalMaf);
            $item->calcMafRowTotal();
            $this->_addMafAmount($item->getMafRowTotal());
            $this->_addBaseMafAmount($item->getBaseMafRowTotal());
            $address->setTotalQty($address->getTotalQty() + $item->getQty());
        } elseif ($quoteItem->isPromotion()) {
            // Set quote item final maf price - negative one
            $finalMaf = -abs($product->getFinalMaf($quoteItem->getQty()));
            // Set price item as negative for Promotion items
            $finalPrice = -abs($product->getFinalPrice($quoteItem->getQty()));

            if ($item->getIsContractDrop()) {
                $finalMaf = 0;
                $finalPrice = 0;
            }
            $item->setMaf($finalMaf)
                ->setBaseOriginalMaf($finalMaf);

            $item->setPrice($finalPrice)
                ->setBasePrice($finalPrice);

            $item->calcMafRowTotal();
            $item->calcRowTotal();

            $this->_addMafAmount($item->getMafRowTotal());
            $this->_addBaseMafAmount($item->getBaseMafRowTotal());

            $address->setTotalQty($address->getTotalQty() + $item->getQty());
        }

        return true;
    }

    /**
     * Set total model amount value to address
     *
     * @param   float $amount
     */
    protected function _setMafAmount($amount)
    {
        $amount = $amount >= 0 ? $amount : 0;

        return parent::_setMafAmount($amount);
    }

    public function _setBaseMafAmount($baseAmount)
    {
        $baseAmount = $baseAmount >= 0 ? $baseAmount : 0;

        return parent::_setBaseMafAmount($baseAmount);
    }


    /**
     * @deprecated old logic
     */
    protected function checkIfValid($packages, $item, $quote, $isOverridden, $initialItems)
    {
        return true;
    }
}
