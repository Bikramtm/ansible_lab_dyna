<?php

class Dyna_Checkout_Model_Sales_Quote_Address_Total_Subtotal extends Mage_Sales_Model_Quote_Address_Total_Subtotal
{
    /**
     * Address item initialization
     *
     * @param  $item
     * @return bool
     */
    protected function _initItem($address, $item)
    {
        if ($item instanceof Mage_Sales_Model_Quote_Address_Item) {
            $quoteItem = $item->getAddress()->getQuote()->getItemById($item->getQuoteItemId());
            // Search for item in alternative items (dropped contract items)
            if (!$quoteItem) {
                $quoteItem = $item->getAddress()->getQuote()->getAlternateItemById($item->getQuoteItemId());
            }
        }
        else {
            $quoteItem = $item;
        }
        $product = $quoteItem->getProduct();
        $product->setCustomerGroupId($quoteItem->getQuote()->getCustomerGroupId());

        /**
         * Quote super mode flag mean what we work with quote without restriction
         */
        if ($item->getQuote()->getIsSuperMode()) {
            if (!$product) {
                return false;
            }
        }
        else {
            if (!$product || !$product->isVisibleInCatalog()) {
                return false;
            }
        }

        if ($quoteItem->getParentItem() && $quoteItem->isChildrenCalculated()) {
            $finalPrice = $quoteItem->getParentItem()->getProduct()->getPriceModel()->getChildFinalPrice(
                $quoteItem->getParentItem()->getProduct(),
                $quoteItem->getParentItem()->getQty(),
                $quoteItem->getProduct(),
                $quoteItem->getQty()
            );
            $item->setPrice($finalPrice)
                ->setBaseOriginalPrice($finalPrice);
            $item->calcRowTotal();
        } else if (!$quoteItem->getParentItem() && !$item->getHasError()) {
            // persisting item price for shipping fee
            if ($item->getSku() === Dyna_Catalog_Model_Type::MOBILE_SHIPPING_SKU) {
                $product->setFinalPrice($item->getPriceInclTax());
                $product->setBasePrice($item->getPriceInclTax());
            }

            //VFDED1W3S-565: Do not consider One time prices for installed base products (ILS)
            if ($item->getIsContract()) {
                $finalPrice = 0;
            } else {
                $finalPrice = $product->getFinalPrice($quoteItem->getQty());
            }

            $item->setPrice($finalPrice)
                ->setBaseOriginalPrice($finalPrice);
            $item->calcRowTotal();
            $this->_addAmount($item->getRowTotal());
            $this->_addBaseAmount($item->getBaseRowTotal());
            $address->setTotalQty($address->getTotalQty() + $item->getQty());
        }

        return true;
    }
}
