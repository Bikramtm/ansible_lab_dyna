<?php

/**
 * Class Dyna_Checkout_Model_Sales_Quote
 * @method getSalesId() Returns VOID
 * @method string getCartStatus()
 * @method $this setShippingData(string $data)
 * @method string getShippingData()
 * @method string getCampaignCode() Returns the campaign ID
 */
class Dyna_Checkout_Model_Sales_Quote extends Omnius_Checkout_Model_Sales_Quote
{
    const CART_STATUS_OLD = 3;

    protected $feeSubtypes = null;

    /** @var Dyna_Package_Model_Package[] */
    protected $packages = false;
    /** @var array (packageId => $quoteItems) */
    protected $alternateItems = array();

    /**
     * Trigger before save even to set offer
     */
    protected function _beforeSave()
    {
        parent::_beforeSave();

        //If quote is saved during a transaction page, always save/associate the transaction id
        $uctParams = Mage::getSingleton('customer/session')->getUctParams();
        if (isset($uctParams['transactionId'])) {
            $this->setTransactionId($uctParams['transactionId']);
        }

        /** @var Dyna_Checkout_Model_Sales_Quote_Item $item */
        foreach ($this->getCartPackages(true) as $package) {
            foreach ($this->getItemsCollection() as $item) {
                if ($item->getPackageId() != $package->getPackageId()) {
                    continue;
                }

                if ($item->isDeleted() && $item->getIsContract() && !$item->getAlternateQuoteId() && !$this->checkAlternateQuoteItem($item->getProduct(), $package->getPackageId())) {
                    /** @var Dyna_Checkout_Model_Sales_Quote_Item $newItem */
                    $newItem = clone $item;
                    $newItem->isDeleted(false);
                    $systemAction = is_null($newItem->getSystemAction()) ? 'remove' : $newItem->getSystemAction();
                    $newItem
                        ->setQuoteId(null)
                        ->setAlternateQuoteId($this->getId())
                        ->setIsContractDrop(1)
                        ->setSystemAction($systemAction)
                        ->save();
                }
            }
        }
        // clear cached alternate items
        $this->clearCachedAlternateItems($this->getActivePackageId());
    }

    /**
     * @param int $packageId
     * @return Dyna_Checkout_Model_Sales_Quote_Item[]
     */
    public function getPackageItems($packageId)
    {
        $items = [];
        foreach ($this->getAllItems() as $item) {
            if ($item->getPackageId() != $packageId) {
                continue;
            }

            $items[] = $item;
        }

        return $items;
    }

    /**
     * Return all packages for current quote
     * @return Dyna_Package_Model_Package[]
     */
    public function getCartPackages($excludeDummy = false)
    {
        if ($this->packages === false) {
            $this->packages = array();
            $packages = Mage::getModel('package/package')->getCollection()
                ->addFieldToFilter('quote_id', $this->getId())
                ->setOrder('package_id', 'ASC')
                ->load();
            if (count($packages)) {
                foreach ($packages as $package) {
                    $this->packages[$package->getPackageId()] = $package;
                }
            }
        }

        if ($excludeDummy) {
            $newResponse = array();
            foreach ($this->packages as $package) {
                if ($package->getEditingDisabled()) {
                    continue;
                }
                $newResponse[$package->getPackageId()] = $package;
            }
            return $newResponse;
        }

        return $this->packages !== false ? $this->packages : [];
    }

    public function getTotalShippingFee()
    {
        $shippingFee = 0.00;

        foreach ($this->getAllItems() as $item) {
            if ($item->getSku() == Dyna_Catalog_Model_Type::MOBILE_SHIPPING_SKU) {
                $shippingFee += $item->getPriceInclTax();
            }
        }

        return $shippingFee;
    }

    /**
     * Return a certain package or active package belonging to current quote
     * @param $packageId
     * @return Dyna_Package_Model_Package|null
     */
    public function getCartPackage($packageId = null)
    {
        $packageId = $packageId ?: $this->getActivePackageId();
        $cartPackages = $this->getCartPackages();
        if ($cartPackages) {
            foreach ($cartPackages as $package) {
                if ($package->getPackageId() == $packageId) {
                    return $package;
                }
            }
        }

        return null;
    }

    /**
     * Return the install base package for the given account number and line id combination
     * @param $parentAccountNumber
     * @param $serviceLineId
     * @return Dyna_Package_Model_Package|null
     */
    public function getInstallBasePackage($parentAccountNumber, $serviceLineId, $excludeDummy = false)
    {
        // get the install base product to check if it is from the cable stack
        /** @var Dyna_Customer_Model_Session $customerSession */
        $customerSession = Mage::getSingleton('customer/session');
        $installBaseProduct = $customerSession->getCustomer()->getInstalledBaseProduct($parentAccountNumber, $serviceLineId);
        if ($installBaseProduct['contract_category'] == Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD) {
            // get KD linked account number from session
            $parentAccountNumber = $customerSession->getKdLinkedAccountNumber();
        }

        foreach ($this->getCartPackages() as $package) {
            if ($excludeDummy && $package->getEditingDisabled()) {
                continue;
            }
            if ($package->getParentAccountNumber() == $parentAccountNumber && $package->getServiceLineId() == $serviceLineId) {
                return $package;
            }
        }

        return null;
    }

    /**
     * Return a certain package or active package in the current quote
     * filtered by the entity id of the package
     *
     * @param null $packageId
     * @return Dyna_Package_Model_Package|null
     */
    public function getCartPackageByEntityId($packageId = null)
    {
        foreach ($this->getCartPackages() as $package) {
            if ($package->getId() == $packageId) {
                return $package;
            }
        }

        return null;
    }

    /**
     * Retrieve customer model object
     *
     * @return Mage_Customer_Model_Customer
     */
    public function getCustomer()
    {
        if (is_null($this->_customer)) {
            if ($customerId = $this->getCustomerId()) {
                $this->_customer = Mage::getModel("customer/customer")->load($customerId);
                $this->_customer->load($customerId);
                if (!$this->_customer->getId()) {
                    $this->_customer->setCustomerId(null);
                }
            } else {
                // If no customer && no customer id set on current quote, load the customer from session
                $this->_customer = Mage::getSingleton("customer/session")->getCustomer();
            }
        }
        return $this->_customer;
    }

    public function clearCustomer()
    {
        $this->_customer = null;
        $this->setData('customer_id', null);
        return $this;
    }

    /**
     * @param $status
     * @param $quoteItems
     * @param $packages
     * @return array
     */
    public function getPackageDataByType($status, $quoteItems, $packages)
    {
        $packagesAll = array();
        /** @var Dyna_Checkout_Model_Sales_Quote_Item $item */
        foreach ($quoteItems as $item) {
            if ($item->isDeleted()) {
                continue;
            }

            // if this item is an order item, skip fetching data from product and get it from the frozen product data saved on it
            $itemType = $item instanceof Dyna_Checkout_Model_Sales_Order_Item ? $item->getPackProductType() : current($item->getProduct()->getType());
            $packageId = $item->getPackageId();

            if ($status) {
                if ($packages[$packageId]['current_status'] == $status) {
                    $packagesAll[$packageId][$itemType][$item->getId()] = $item;
                }
            } else {
                $packagesAll[$packageId][$itemType][$item->getId()] = $item;
            }
        }

        return $packagesAll;
    }

    /**
     * @param int|null $packageId
     * @param bool|false $isOffer
     * @return array
     */
    public function calculateTotalsForPackage($packageId = null, $isOffer = false)
    {
        $items = $this->getAllItems();
        $subtotalPrice = 0;
        $subtotalMaf = 0;
        $taxPrice = 0;
        $taxMaf = 0;
        $total = 0;
        $totalMaf = 0;
        $noPromoMaf = 0;
        $noPromoTax = 0;
        /** @var Dyna_Checkout_Model_Sales_Quote_Item $item */
        foreach ($items as $item) {
            $correctId = $item->getPackageId() == $packageId || $packageId == null;
            $isOfferOrPromo = !($item->isPromo() || $item->isBundlePromo() || $isOffer);
            if ($correctId && $isOfferOrPromo) {
                //SREQ-6384 - Removed the else
                if ($item->isPromotion()) {
                    $rowTotal = $item->getRowTotal() ?: 0;
                } else {
                    $rowTotal = $item->getRowTotal() ? $item->getItemFinalPriceExclTax() : 0;
                }

                $subtotalPrice += $rowTotal;
                $noPromoMaf += $item->getMaf();
                $noPromoTax += $item->getMafInclTax();
                $rowTotalMaf = $item->getMafRowTotal() ? $item->getItemFinalMafExclTax() : 0;
                $subtotalMaf += $rowTotalMaf;
                $mafTaxAmmount = $item->getMafTaxAmount() ? $item->getMafTaxAmount() : 0;
                $taxMaf += $mafTaxAmmount;
                $taxAmmount = $item->getTaxAmount() ? $item->getTaxAmount() : 0;
                $taxPrice += $taxAmmount;
                $total += ($rowTotal + $taxAmmount);
                $totalMaf += ($rowTotalMaf + $mafTaxAmmount);
            }
        }

        $subtotalPrice = $isOffer && $this->getInitialMixmatchSubtotal() !== null ? $this->getInitialMixmatchSubtotal() : $subtotalPrice;
        $taxPrice = $isOffer && $this->getInitialMixmatchSubtotal() !== null ? $this->getInitialMixmatchTax() : $taxPrice;
        $total = $isOffer && $this->getInitialMixmatchSubtotal() !== null ? $this->getInitialMixmatchTotal() : $total;

        return array(
            'no_promo_maf' => round($noPromoMaf, 2),
            'no_promo_maf_tax' => round($noPromoTax, 2),
            'no_promo_tax_amount' => round($noPromoTax - $noPromoMaf, 2),
            'subtotal_price' => round($subtotalPrice, 2),
            'subtotal_maf' => max(round($subtotalMaf, 2), 0),
            'tax_price' => round($taxPrice, 2),
            'tax_maf' => max(round($taxMaf, 2), 0),
            'total' => round($total, 2),
            'total_maf' => max(round($totalMaf, 2), 0),
        );
    }

    /**
     * @param int $packageId
     * @return Dyna_Package_Model_Package|null|Omnius_Package_Model_Package
     */
    public function getPackageById($packageId)
    {
        return $this->getCartPackage($packageId);
    }

    /**
     *
     * @param string $status
     * Get list of packages for a certain quote
     * @return Dyna_Package_Model_Package []
     * @throws Mage_Core_Exception
     */
    public function getPackages($status = null)
    {
        $key = 'quote_packages_' . $this->getId() . '_' . md5($this->getUpdatedAt() . $status);
        $currentQuotePackages = [];
        $this->getQuotePackagesInfo($currentQuotePackages);

        if ((($packages = Mage::registry($key)) === null) || ($currentQuotePackages != $packages)) {
            $packages = $disabledPackages = [];
            // Set packages type and sale type
            $this->getQuotePackagesInfo($packages, $disabledPackages);
            /** @var Omnius_Checkout_Model_Sales_Quote_Item[] $quoteItems */
            $quoteItems = Mage::getResourceModel('sales/quote_item_collection')
                ->setQuote($this)
                ->addOrder('package_id', 'asc');

            // Ensure the items for the package come in order, Sub, Sim, Device, Addon, Acc
            $packagesAll = $this->getPackageDataByType($status, $quoteItems, $packages);

            /** @var Dyna_Catalog_Model_Product $productModel */
            $productModel = Mage::getModel('catalog/product');

            // Special handling for Installed base packages that are used in bundles
            foreach ($disabledPackages as $packageId => $disabledPackage) {
                $skus = explode(',', $disabledPackage['installed_base_products']);
                $oldSkus = explode(',', $disabledPackage['initial_installed_base_products']);
                $diff = array_diff($skus, $oldSkus);
                $items = [];
                foreach ($skus as $sku) {
                    $productSku = trim($sku);
                    $productId = $productModel->getIdBySku($productSku);
                    if ($productId) {
                        /** @var Dyna_Checkout_Model_Sales_Quote_Item $quoteItemModel */
                        $quoteItemModel = Mage::getModel('sales/quote_item');
                        /** @var Omnius_Catalog_Model_Product $product */
                        $product = Mage::getModel('catalog/product')
                            ->setStoreId($this->getStoreId())
                            ->load($productId);

                        $items[] = $quoteItemModel->setProduct($product);
                        $quoteItemModel->setRowTotalInclTax(0);
                        $quoteItemModel->setMafRowTotalInclTax(0);
                        if (!in_array($sku, $diff)) {
                            $quoteItemModel->setOldInstallBase(true);
                        }
                    }
                }
                $packages[$packageId]['items'] = $items;
            }

            foreach ($packagesAll as $packageId => $tempPackage) {
                if (array_key_exists($packageId, $disabledPackages)) {
                    continue;
                }
                $items = [];
                foreach ($tempPackage as $type => $i) {
                    $items += $tempPackage[$type];
                }
                $packages[$packageId]['items'] = $items;
            }

            if ($this->getId()) {
                Mage::unregister($key);
                Mage::register($key, $packages);
            }
        }

        return $packages;
    }

    /**
     * Get info of quote's packages
     * @param $packages
     * @param $disabledPackages
     */
    protected function getQuotePackagesInfo(&$packages, &$disabledPackages = array())
    {
        if ($packagesInfo = $this->getPackagesInfo()) {
            foreach ($packagesInfo as $package) {
                $packageId = $package['package_id'];
                $packages[$packageId] = $package;
                $packages[$packageId]['items'] = array();

                if ($package['editing_disabled']) {
                    $disabledPackages[$packageId] = $package;
                }
            }
        }
    }

    /**
     * Retrieve quote items array
     * @return Dyna_Checkout_Model_Sales_Quote_Item[]
     */
    public function getAllItems($includingAlternateItems = false, $includingDeleted = false)
    {
        $result = Mage_Sales_Model_Quote::getAllItems();

        if ($includingAlternateItems) {
            foreach ($this->getAlternateItems(null, true) as $item) {
                $result[] = $item;
            }
        }

        if ($includingDeleted) {
            foreach ($this->getItemsCollection() as $item) {
                if ($item->isDeleted()) {
                    $result[] = $item;
                }
            }
        }

        return $result;
    }

    /**
     * Get all quote product ids from all packages
     * @return int[]
     */
    public function getAllProductsIds($byPackage = false)
    {
        $ids = array();
        $idsbyPackage = array();
        foreach ($this->getAllItems() as $item) {
            if ($byPackage) {
                $idsbyPackage[$item->getPackageId()][] = $item->getProductId();
            } else {
                $ids = (int)$item->getProductId();
            }
        }

        return $byPackage ? $idsbyPackage : array_unique($ids);
    }

    /**
     * @return array
     * Removed quote items that are set to cancelled
     */
    public function getAllVisibleItems()
    {
        $items = Mage_Sales_Model_Quote::getAllVisibleItems();

        foreach ($items as $key => $item) {
            if ($item->getIsCancelled() == 1) {
                unset($items[$key]);
            }
        }

        return $items;
    }

    public function addProduct(Mage_Catalog_Model_Product $product, $request = null)
    {
        try {
            /** @var Dyna_Configurator_Helper_ExtraLogCart $extraLogHelper */
            $extraLogHelper = Mage::helper('dyna_configurator/extraLogCart');
            // OMNVFDE-1073: Only one fee with the highest priority should remain in cart for cable packages
            // OMNVFDE-1559: Cable fee logic changed from package_subtype attribute to sub_type attribute
            // TODO: make check only for Cable packages
            // Skip this logic if added product is not a fee
            if (in_array(str_replace(" ", "", strtolower($product->getSubType())), $this->getFeeSubtypes())) {
                $extraLogHelper->extraLog("Trying to add fee product: " . $product->getSku() . " with priority: " . $product->getFeePriority());
                foreach ($this->getAllItems() as $item) {
                    $itemProduct = $item->getProduct();
                    $productSubType = $itemProduct->getSubType();
                    if (in_array(str_replace(" ", "", strtolower($productSubType)), $this->getFeeSubtypes())) {
                        $extraLogHelper->extraLog("Existing fee product in cart: " . $itemProduct->getSku() . " with priority: " . $itemProduct->getFeePriority());
                        // Found one fee in cart, let's check if this product has higher priority than the one in cart (highest is the one with the lowest fee_priority attribute value)
                        if ($product->getFeePriority()
                            && (($itemProduct->getFeePriority() > $product->getFeePriority())
                                || !$itemProduct->getFeePriority())
                        ) {
                            $extraLogHelper->extraLog("Removed existing fee product from cart: " . $item->getProduct()->getSku());
                            $item->isDeleted(true);
                        } else {
                            $extraLogHelper->extraLog("Skipped adding fee product:" . $product->getSku());
                            // Skip adding the current fee product
                            /** @var Dyna_Checkout_Model_Exception_Fee $feeException */
                            $feeException = Mage::getModel('dyna_checkout/exception_fee', "Skipped adding product: " . $product->getSku() . " because another fee with higher priority already exists in current cart");
                            throw $feeException;
                        }
                    }
                }

                $extraLogHelper->extraLog("Added new fee product to cart: " . $product->getSku());
            }
            // EOF OMNVFDE-1073

            if (!$item = $this->checkAlternateQuoteItem($product)) {
                $item = parent::addProduct($product, $request);
                $item->setQuote($this);
                $item
                    ->setCtn($this->getActivePackage()->getCtn())
                    ->setSaleType($this->getActivePackage()->getSaleType());
            } else {
                /** @var Dyna_Checkout_Model_Sales_Quote_Item $item */
                $item
                    ->setAlternateQuoteId(null)
                    ->setQuote($this)
                    ->setIsContractDrop(null);
                if ($item->getBundleComponent() == -1) {
                    foreach ($this->getAlternateItems($item->getPackageId()) as $bItem) {
                        if ($item->getBundleComponent() == -1) {
                            $bItem
                                ->setAlternateQuoteId(null)
                                ->setQuote($this)
                                ->setIsContractDrop(null);
                            if (!$this->getItemsCollection()->getItemById($bItem->getId())) {
                                $this->getItemsCollection()->addItem($bItem);
                            }
                        }
                    }
                }
                if (!$this->getItemsCollection()->getItemById($item->getId())) {
                    $this->getItemsCollection()->addItem($item);
                }
                // trigger clearing of cached alternate items for this package
                $this->clearCachedAlternateItems($this->getActivePackageId());
            }
        } catch (Dyna_Checkout_Model_Exception_Fee $e) {
            return $e->getMessage();
        } catch (Dyna_Checkout_Model_Exception_Expired $e) {
            return $e->getMessage();
        }

        return $item;
    }

    /**
     * Clear cached alternate items for a certain package (when adding removed inlife product back to cart)
     * @return $this
     */
    public function clearCachedAlternateItems($forPackageId)
    {
        if (isset($this->alternateItems[$forPackageId])) {
            unset ($this->alternateItems[$forPackageId]);
        }

        return $this;
    }

    /**
     * Determine whether or not there is already an alternate item for this active package with this sku
     * @param Mage_Catalog_Model_Product $product
     * @param $packageId|null
     * @return Dyna_Checkout_Model_Sales_Quote_Item|null
     */
    public function checkAlternateQuoteItem(Mage_Catalog_Model_Product $product, $packageId = null)
    {
        $installBase = array_merge($this->getAlternateItems(), $this->getAlternateItems($packageId, false, false));
        /** @var Dyna_Checkout_Model_Sales_Quote_Item $quoteItem */
        foreach ($installBase as $quoteItem) {
            if ($quoteItem->getSku() != $product->getSku()) {
                continue;
            }

            return $quoteItem;
        }

        return null;
    }

    /**
     * Return active package instance
     * @return Dyna_Package_Model_Package
     */
    public function getActivePackage()
    {
        $packages = $this->getCartPackages();

        return isset($packages[$this->getActivePackageId()]) ? $packages[$this->getActivePackageId()] : null;
    }

    /**
     * Get all fee product subtypes
     * @return array|null
     */
    protected function getFeeSubtypes()
    {
        if (!$this->feeSubtypes) {
            // Converting all fee subtypes to a lower case string with no spaces to prevent double spacing and casing issues
            $this->feeSubtypes = array_map(function ($element) {
                return str_replace(" ", "", strtolower($element));
            }, Dyna_Catalog_Model_Type::getFeesSubtypes());
        }

        return $this->feeSubtypes;
    }

    public function hasPackage(string $packageType)
    {
        /** @var Dyna_Package_Model_Package $package */
        foreach ($this->getCartPackages() as $package) {
            if (strtoupper($package->getType()) === strtoupper($packageType)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Method that clears packages property set on current quote instance when first called getCartPackages
     * @return $this
     */
    public function clearCachedPackages()
    {
        $this->packages = false;

        return $this;
    }

    /**
     * Get quote item by sku from a certain packageId
     * If no package is provided, the entire cart will be evaluated
     * @param $sku
     * @return Dyna_Checkout_Model_Sales_Quote_Item|null
     */
    public function getItemBySku($sku, $packageId = null, $includingDeleted = false)
    {
        if ($packageId) {
            foreach ($this->getCartPackage($packageId)->getAllItems(false, true, false, $includingDeleted) as $item) {
                if ($item->getSku() == $sku) {
                    return $item;
                }
            }
        } else {
            // evaluate entire cart
            foreach ($this->getAllItems(false, $includingDeleted) as $item) {
                if ($item->getSku() == $sku) {
                    return $item;
                }
            }
        }

        // not found
        return null;
    }

    /**
     * Returns the packages in the current quote with the specified parent id
     * @param $packageId
     * @return array
     */
    public function getCartPackagesWithParent($packageId)
    {
        $cartPackages = $this->getCartPackages();
        $packagesWithParent = array();

        foreach ($cartPackages as $package) {
            if ($package->getParentId() == $packageId && $package->getSharingGroupId() == null) {
                $packagesWithParent[] = $package;
            }
        }

        return $packagesWithParent;
    }

    /**
     * Returns the number of child packages of the dummy package that matches the
     * install base subscription identified by the subscription number and product id
     * @param $bundleId
     * @param $subscriptionNumber
     * @param $productId
     * @return int
     */
    public function getNumberOfChildPackagesForIBSubscription($bundleId, $subscriptionNumber, $productId)
    {
        $packages = $this->getCartPackages();
        $numberOfChildPackages = 0;

        if ($packages) {
            foreach ($packages as $package) {
                if (($bundle = $package->getBundleOfType(Dyna_Bundles_Model_BundleRule::TYPE_RED_PLUS))
                    && $bundleId == $bundle->getId()
                    && $subscriptionNumber == $package->getParentAccountNumber()
                    && $productId == $package->getServiceLineId()
                ) {
                    // found a dummy in cart corresponding to this subscription
                    foreach ($packages as $childPackage) {
                        // count its children
                        if ($childPackage->getParentId() == $package->getEntityId() && $childPackage->getSharingGroupId() == null) {
                            $numberOfChildPackages++;
                        }
                    }
                }
            }
        }

        return $numberOfChildPackages;
    }

    /**
     * Checks whether a ghost (dummy) package was already added in cart for a certain
     * install base subscription
     * @param $subscriptionNumber
     * @param $productId
     * @return Dyna_Package_Model_Package|null
     */
    public function getGhostPackageForIBSubscription($subscriptionNumber, $productId)
    {
        $packages = $this->getCartPackages();

        foreach ($packages as $package) {
            // No need for checking against bundle id as there can be only one Red+ bundle with an install base subscription
            if ($subscriptionNumber == $package->getParentAccountNumber()
                && $productId == $package->getServiceLineId()
                && $package->getParentId() == null
            ) {
                return $package;
            }
        }

        return null;
    }

    /**
     * Returns the packages in the same bundle in the current
     * shopping cart
     *
     * @param $bundleId
     * @return Dyna_Package_Model_Package[]
     */
    public function getCartPackagesInBundle($bundleId)
    {
        $cartPackages = $this->getCartPackages();
        $bundledPackages = array();

        foreach ($cartPackages as $package) {
            foreach ($package->getBundles() as $bundle) {
                if ($bundle->getId() == $bundleId) {
                    $bundledPackages[] = $package;
                }
            }
        }

        return $bundledPackages;
    }

    /**
     * Collect totals
     *
     * @return Mage_Sales_Model_Quote
     */
    public function collectTotals()
    {
        /**
         * Protect double totals collection
         */
        if ($this->getTotalsCollectedFlag()) {
            return $this;
        }

        $isMultishipping = $this->getIsMultiShipping();
        $this->setData('collect_totals_cache_items', $isMultishipping);
        Mage::unregister('collect_totals_quote');
        Mage::register('collect_totals_quote', $this);

        Mage::dispatchEvent($this->_eventPrefix . '_collect_totals_before', array($this->_eventObject => $this));

        if (Mage::getSingleton('customer/session')->getOrderEdit() || $this->getIsOffer()) {
            $this->setIsSuperMode(true);
        }

        $this->setSubtotal(0);
        $this->setBaseSubtotal(0);

        $this->setSubtotalWithDiscount(0);
        $this->setBaseSubtotalWithDiscount(0);

        $this->setGrandTotal(0);
        $this->setBaseGrandTotal(0);

        $this->setMafSubtotal(0);
        $this->setBaseMafSubtotal(0);

        $this->setMafSubtotalWithDiscount(0);
        $this->setBaseMafSubtotalWithDiscount(0);

        $this->setMafGrandTotal(0);
        $this->setBaseMafGrandTotal(0);

        /** @var Omnius_Checkout_Model_Sales_Quote_Address $address */
        foreach ($this->getAllShippingAddresses() as $address) {

            $address->setSubtotal(0);
            $address->setBaseSubtotal(0);

            $address->setGrandTotal(0);
            $address->setBaseGrandTotal(0);

            $address->setMafSubtotal(0);
            $address->setBaseMafSubtotal(0);

            $address->setMafGrandTotal(0);
            $address->setBaseMafGrandTotal(0);

            $address->collectTotals();


            $this->setSubtotal((float)$this->getSubtotal() + $address->getSubtotal());
            $this->setBaseSubtotal((float)$this->getBaseSubtotal() + $address->getBaseSubtotal());

            $this->setSubtotalWithDiscount(
                (float)$this->getSubtotalWithDiscount() + $address->getSubtotalWithDiscount()
            );
            $this->setBaseSubtotalWithDiscount(
                (float)$this->getBaseSubtotalWithDiscount() + $address->getBaseSubtotalWithDiscount()
            );

            $this->setGrandTotal((float)$this->getGrandTotal() + $address->getGrandTotal());
            $this->setBaseGrandTotal((float)$this->getBaseGrandTotal() + $address->getBaseGrandTotal());

            $this->setMafSubtotal((float)$this->getMafSubtotal() + ($address->getMafSubtotal() + $address->getMafDiscountAmount() + $address->getHiddenMafTaxAmount()));
            $this->setBaseMafSubtotal((float)$this->getBaseMafSubtotal() + ($address->getBaseMafSubtotal() + $address->getBaseMafDiscountAmount() + $address->getBaseHiddenMafTaxAmount()));

            $this->setMafSubtotalWithDiscount(
                (float)$this->getMafSubtotalWithDiscount() + $address->getMafSubtotalWithDiscount()
            );
            $this->setBaseMafSubtotalWithDiscount(
                (float)$this->getBaseMafSubtotalWithDiscount() + $address->getBaseMafSubtotalWithDiscount()
            );

            $this->setMafGrandTotal((float)$this->getMafGrandTotal() + $address->getMafGrandTotal());
            $this->setBaseMafGrandTotal((float)$this->getBaseMafGrandTotal() + $address->getBaseMafGrandTotal());
        }
        Mage::helper('sales')->checkQuoteAmount($this, $this->getGrandTotal());
        Mage::helper('sales')->checkQuoteAmount($this, $this->getBaseGrandTotal());

        $this->setItemsCount(0);
        $this->setItemsQty(0);
        $this->setVirtualItemsQty(0);

        foreach ($this->getAllVisibleItems() as $item) {
            if ($item->getParentItem()) {
                continue;
            }

            $children = $item->getChildren();
            if ($children && $item->isShipSeparately()) {
                foreach ($children as $child) {
                    if ($child->getProduct()->getIsVirtual()) {
                        $this->setVirtualItemsQty($this->getVirtualItemsQty() + $child->getQty());
                    }
                }
            }

            if ($item->getProduct()->getIsVirtual()) {
                $this->setVirtualItemsQty($this->getVirtualItemsQty() + $item->getQty());
            }
            $this->setItemsCount($this->getItemsCount() + 1);
            $this->setItemsQty((float)$this->getItemsQty() + $item->getQty());
        }

        $this->setData('trigger_recollect', 0);
        $this->_validateCouponCode();
        $this->setIsMultiShipping($isMultishipping);
        $this->unsetData('collect_totals_cache_items');
        Mage::dispatchEvent($this->_eventPrefix . '_collect_totals_after', array($this->_eventObject => $this));
        Mage::unregister('collect_totals_quote');
        $this->setTotalsCollectedFlag(true);

        return $this;
    }

    /*
     * Clone checkout fields to newly created quote
     * @return $this
     */
    public function cloneCheckoutFields(self $toQuote, $copyMarketing = false)
    {
        $marketingCondition = !$copyMarketing ? " and `field_name` NOT LIKE '%other[marketing]%'" : "";

        $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
        $connection->query("
        INSERT INTO sales_flat_quote_checkout_fields(`quote_id`, `checkout_step`, `field_name`, `field_value`, `date`, `package_id`, `package_number`)
        SELECT :to, `checkout_step`, `field_name`, `field_value`, `date`, `package_id`, `package_number` 
        FROM sales_flat_quote_checkout_fields 
        WHERE `quote_id`=:this" . $marketingCondition, array(':to' => $toQuote->getId(), ':this' => $this->getId()));
    }

    /**
     * Method used to clone another quote. Also updates the parent
     * package ids of the packages so they use the entity id of the
     * newly created packages
     *
     * @param bool $recollectTotals
     * @return Dyna_Checkout_Model_Sales_Quote
     * @throws Exception
     * @throws Mage_Core_Exception
     * @throws Mage_Core_Model_Store_Exception
     */
    public function cloneQuote($recollectTotals = true)
    {
        /** @var Mage_Core_Model_Resource_Transaction $transaction */
        $transaction = Mage::getResourceModel('core/transaction');

        // clone the quote
        /** @var Dyna_Checkout_Model_Sales_Quote $newQuote */
        $newQuote = Mage::getModel('sales/quote')
            ->setData($this->getData())
            ->setActivePackageId(1)
            ->setCurrentPackageId(1)
            ->unsetData('entity_id')
            ->unsetData('created_at')
            ->setQuoteParentId($this->getId())
            ->save();

        // clone items
        /**@var Omnius_Checkout_Model_Sales_Quote_Item $item */
        foreach ($this->getAllItems() as $item) {
            // Check if the product is available for the current store
            if (!in_array(Mage::app()->getStore()->getId(), $item->getProduct()->getStoreIds())) {
                continue;
            }
            if ($item->getProduct()->getExpirationDate() && (strtotime($item->getProduct()->getExpirationDate()) < strtotime(date('Y-m-d')))) {
                continue;
            }
            if ($this->getCartPackage($item->getPackageId())->getEditingDisabled()) {
                continue;
            }
            // add the old item in the new quote
            Mage::getModel('sales/quote_item')
                ->setData($item->getData())
                ->unsetData('item_id')
                ->setQuote($newQuote)
                ->save();
        }

        // clone shipping address
        /**@var Omnius_Checkout_Model_Sales_Quote_Address $address */
        foreach ($this->getAllAddresses() as $address) {
            $newAddress = Mage::getModel('sales/quote_address');
            $newAddress->setData($address->getData());
            $newAddress->unsetData('address_id');
            $newAddress->setQuoteId($newQuote->getId());
            $transaction->addObject($newAddress);
        }

        //clone payment
        $oldPayment = $this->getPayment();

        $newPayment = Mage::getModel('sales/quote_payment');
        $newPayment->setData($oldPayment->getData());
        $newPayment->setQuoteId($newQuote->getId());
        $transaction->addObject($newPayment);

        // Create packages
        $packagesModelOld = Mage::getModel('package/package')->getCollection()
            ->addFieldToFilter('quote_id', $this->getId());
        foreach ($packagesModelOld as $packageModelOld) {
            $packageModelNew = Mage::getModel('package/package');
            $packageModelNew->setData($packageModelOld->getData())->setId(null)->setQuoteId($newQuote->getId())->setOrderId(null)->setEntityId(null);

            //VFDED1W3S-3797
            Mage::dispatchEvent(
                'checkout_cart_clone_package_after',
                ['package' => $packageModelNew, 'quote' => $newQuote]
            );

            if (!$packageModelNew->getSkipSave()) {
                $transaction->addObject($packageModelNew);
            }
        }
        try {
            $transaction->save();
            // clear cached packages before retrieving for the first time a package from the new quote
            $newQuote->clearCachedPackages();

            //VFDED1W3S-3797: Add to the package only the products that were before bundle
            if ($beforeBundleProducts = $newQuote->getActivePackage()->getBeforeBundleProducts()) {
                foreach ($newQuote->getAllVisibleItems() as $item) {
                    $quoteItemsSkus[] = $item->getSku();
                }
                $beforeBundleProducts = Mage::helper('core')->jsonDecode($beforeBundleProducts);
                $diffBetweenArrays = array_diff($beforeBundleProducts, $quoteItemsSkus);
                foreach ($diffBetweenArrays as $key => $sku) {
                    /**@var Dyna_Catalog_Model_Product $productModel */
                    $productModel = Mage::getModel('catalog/product');
                    $newQuote->addProduct($productModel->load($productModel->getIdBySku($sku)));
                }
            }

            // If all the products have been successfully saved, update their prices
            foreach ($newQuote->getAllVisibleItems() as $item) {
                Mage::dispatchEvent(
                    'checkout_cart_clone_product_after',
                    ['product' => $item->getProduct(), 'quote_item' => $item]
                );
            }
        } catch (Exception $e) {
            $newQuote->delete();
            throw new \RuntimeException('Cannot clone order');
        }

        // Assure prices
        if ($recollectTotals) {
            $newQuote->getBillingAddress();
            $newQuote->getShippingAddress()->setCollectShippingRates(true);
            $newQuote
                ->setTotalsCollectedFlag(false)
                ->collectTotals()
                ->save();
        }

        // load the parent quote and update the package parent ids
        /** @var Dyna_Checkout_Model_Sales_Quote $parentQuote */
        $parentQuote = Mage::getModel('sales/quote')
            ->load($newQuote->getQuoteParentId());

        if ($parentQuote) {
            foreach ($newQuote->getCartPackages() as $newPackage) {
                if ($parentId = $newPackage->getParentId()) {
                    $oldPackage = $parentQuote->getCartPackageByEntityId($parentId);
                    $oldPackageId = $oldPackage->getPackageId();

                    $newPackage->setParentId($newQuote->getCartPackage($oldPackageId)->getEntityId());
                    $newPackage->save();
                }
            }
            $this->updateNoteOnQuoteCloning($newQuote);
        }

        return $newQuote;
    }

    /**
     * Update note as well on the package/s of the new quote
     * @param $newQuote
     */
    protected function updateNoteOnQuoteCloning($newQuote)
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $parentQuote */
        $parentQuote = Mage::getModel('sales/quote')
            ->load($newQuote->getQuoteParentId());

        foreach ($parentQuote->getCartPackages() as $oldPackage) {
            // get the corresponding package from the new quote
            $newPackage = $newQuote->getCartPackage($oldPackage->getPackageId());

            // package notes
            $packageNote = Mage::getModel('dyna_checkout/packageNotes')
                ->getCollection()
                ->addFieldToFilter('package_id', $oldPackage->getEntityId())
                ->getFirstItem();

            if (!empty($packageNote->getData())) {
                $updatedData = array(
                    'package_id' => $newPackage->getEntityId(),
                    'quote_id' => $newQuote->getId()
                );
                // update note
                Mage::getModel('dyna_checkout/packageNotes')->addData($updatedData)->setId($packageNote->getEntityId())->save();
            }
        }
    }

    /**
     * ILS Contract items that were removed from quote
     * Added cache at instance level
     * @param int $packageId
     * @param boolean $allItems
     * @param boolean $preselect
     * @return Dyna_Checkout_Model_Sales_Quote_Item[]
     */
    public function getAlternateItems($packageId = null, $allItems = false, $preselect = true)
    {
        // if all items is set, make sure to cache all package items from db on instance level
        if ($allItems) {
            foreach ($this->getCartPackages(true) as $package) {
                $this->getAlternateItems($package->getPackageId(), false, $preselect);
            }
            // return all quote items merged into a single array
            return array_reduce($this->alternateItems, function ($result, $packageItems) {
                return $result ? array_merge($result, $packageItems) : $packageItems;
            });
        }

        if ($packageId === null) {
            $packageId = $this->getActivePackageId();
        }

        // If there are no items yet fetched for this package, fetch them and cache them at instance level
        if (!isset($this->alternateItems[$packageId . "_" . strval($preselect)])) {
            $this->alternateItems[$packageId] = array();

            // Keep reference to collection instance
            /** @var Mage_Sales_Model_Resource_Quote_Item_Collection $alternateItems */
            $alternateItems = Mage::getModel('sales/quote_item')
                ->getCollection()
                ->setQuote($this);
            $alternateItems->getSelect()
                ->reset(Zend_Db_Select::WHERE)
                ->where('quote_id IS NULL')
                ->where('alternate_quote_id = ?', $this->getId())
                ->where('package_id = ?', $packageId);
            if ($preselect !== 'both') {
                $alternateItems->getSelect()
                    ->where('preselect_product = ?', $preselect ? 1 : 0);
            }
            $alternateItems->getSelect()
                ->order('item_id', 'asc');

            /** @var Dyna_Checkout_Model_Sales_Quote_Item $item */
            foreach ($alternateItems as $item) {
                $this->alternateItems[$packageId][] = $item;
            }
        }

        return $this->alternateItems[$packageId] ?? array();
    }

    /**
     * Method used in checkout/cart/partials/package_block.phtml
     * for displaying the installed base items in shopping cart (strike-trough)
     * @return array
     */
    public function getAlternatePackages()
    {
        $packagesAll = [];
        // todo: add process context condition
        $alternateItems = $this->getAlternateItems();
        if (count($alternateItems)) {
            $packages = $disabledPackages = [];

            // Set packages type and sale type
            $this->getQuotePackagesInfo($packages, $disabledPackages);

            $quotePackages = $this->getPackages();
            $package = $quotePackages[$this->getActivePackageId()];
            $package['items'] = $alternateItems;

            $alternatePackages[] = $package;

            $packagesAll = Mage::getModel('dyna_checkout/cart')->extractPackageInfo($alternatePackages);
        }

        return $packagesAll;
    }

    /**
     * Retrieve item model object by item identifier
     *
     * @param   int $itemId
     * @return  Mage_Sales_Model_Quote_Item
     */
    public function getAlternateItemById($itemId)
    {
        $sql = "SELECT * FROM `sales_flat_quote_item` WHERE `item_id` = :item_id AND `alternate_quote_id` = :id LIMIT 1";
        $conn = Mage::getSingleton('core/resource')->getConnection('core_write');

        $binds = array(
            ':item_id' => (int)$itemId,
            ':id' => $this->getId()
        );
        $arrayItem = $conn->fetchRow($sql, $binds);

        if ($arrayItem) {
            $arrayItem['is_alternate'] = true;
            return Mage::getModel('sales/quote_item')->addData($arrayItem);
        }

        return null;
    }

    /**
     * Retrieve item model object by item identifier
     *
     * @param   int $itemId
     * @param   boolean $preselect
     * @return  Mage_Sales_Model_Quote_Item
     */
    public function getItemById($itemId, $preselect = true)
    {
        /** @var Dyna_Checkout_Model_Sales_Quote_Item $item */
        foreach ($this->getItemsCollection() as $item) {
            if ($item->getId() == $itemId) {
                return $item;
            }
        }

        // if not found, try searching through alternate items
        foreach ($this->getAlternateItems(null, true, $preselect) as $item) {
            if ($item->getId() == $itemId) {
                return $item;
            }
        }

        return null;
    }

    /**
     * Get red part (mobile) of salesId triple
     * @return string
     */
    public function getRedSalesId()
    {
        return $this->getSalesId();
    }

    /**
     * Check if this quote has at least a mobile package (excluding dummy packages)
     * @return bool
     */
    public function hasMobilePackage()
    {
        foreach ($this->getCartPackages(true) as $package) {
            if ($package->isMobile()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check if this quote has at least a mobile package with young tariff
     * @return bool
     */
    public function hasYoungItems()
    {
        foreach ($this->getCartPackages(true) as $package) {
            if ($package->hasYoungItems()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Remove quote item by item identifier
     *
     * @param $itemSku
     * @param $fromPackageId
     * @return Mage_Sales_Model_Quote
     */
    public function removeItemBySku($itemSku, $fromPackageId)
    {
        $item = $this->getItemBySku($itemSku, $fromPackageId);

        if ($item) {
            $item->setQuote($this);
            /**
             * If we remove item from quote - we can't use multishipping mode
             */
            $this->setIsMultiShipping(false);
            $item->isDeleted(true);
            if ($item->getHasChildren()) {
                foreach ($item->getChildren() as $child) {
                    $child->isDeleted(true);
                }
            }

            $parent = $item->getParentItem();
            if ($parent) {
                $parent->isDeleted(true);
            }

            Mage::dispatchEvent('sales_quote_remove_item', array('quote_item' => $item));
        }

        return $this;
    }

    /**
     * Clears the cached alternate items
     *
     * @return $this
     */
    public function clearAlternateItems()
    {
        $this->alternateItems = array();

        return $this;
    }
}
