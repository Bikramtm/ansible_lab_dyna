<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Checkout_Model_Sales_Order_Item
 * @method      int getPackageId()
 * @method      float|null getMaf()
 * @method      float|null getMafInclTax()
 * @method      float|null getMafDiscountAmount()
 * @method      float|null getMafRowTotal()
 * @method      float|null getMafRowTotalInclTax()
 * @method      float|null getHiddenMafTaxAmount()
 * @method      int getPrijsPromoDuurMaanden()
 * @method      Dyna_Catalog_Model_Product getProduct()
 * @method      int getIsContractDrop()
 * @method      int getIsContract()
 * @method      string|null getSystemAction()
 */
class Dyna_Checkout_Model_Sales_Order_Item extends Omnius_Checkout_Model_Sales_Order_Item
{
    /**
     * Returns true if the promo product was added by a bundle rule
     * @return mixed
     */
    public function isBundlePromo()
    {
        return $this->getData('is_bundle_promo');
    }

    /**
     * Returns true if the promo product was added by a bundle rule
     * @return mixed
     */
    public function isContract()
    {
        return $this->getData('is_contract');
    }

    /**
     * Determine whether or not current order item is a promotion
     * @return bool
     */
    public function isPromotion()
    {
        return in_array(strtolower($this->getPackProductType()), array_map("strtolower", Dyna_Catalog_Model_Type::$promotions));
    }

    /**
     * @return Omnius_Catalog_Model_Product
     */
    public function getDeletedProduct()
    {
        /** @var Dyna_Catalog_Model_Product $deletedProduct */
        $deletedProduct = Mage::getModel('catalog/product');
        $deletedProduct->setIsDeleted(true);
        $deletedProduct->setData('old_id', $this->getProductId());
        $deletedProduct->setType(array('0' => $this->getPackProductType()));
        $deletedProduct->setPackageType($this->getPackageType());
        $deletedProduct->setSku($this->getSku());
        $deletedProduct->setHasOptions($this->getProductOptions());
        $deletedProduct->setCreatedAt($this->getCreatedAt());
        $deletedProduct->setStatus($this->getCustomStatus());
        $deletedProduct->setUpdateAt($this->getUpdatedAt());
        $deletedProduct->setStatus($this->getCustomStatus());
        $deletedProduct->setName($this->getName());
        $deletedProduct->setData('purged_product', true);
        $deletedProduct->setIsPromo($this->getIsPromo());
        $deletedProduct->setProductVisibility($this->getProductVisibility());

        return $deletedProduct;
    }
}
