<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Discount calculation model
 *
 * @category    Dyna
 * @package     Dyna_Checkout
 */
class Dyna_Checkout_Model_SalesRule_Quote_Discount extends Omnius_Checkout_Model_SalesRule_Quote_Discount
{
    /** @var Dyna_Checkout_Helper_Data|null */
    protected $checkoutHelper = null;
    /** @var array List of promo items added by bundle rules */
    protected $_bundlePromos = [];

    /**
     * @param $address
     * @return array|null
     */
    protected function _initBundlePromos($address)
    {
        $this->_bundlePromos = array();
        $items = $this->_getAddressItems($address);
        foreach ($items as $item) {
            if (!$item->isDeleted() && $item->isBundlePromo()) {
                $this->_bundlePromos[$item->getPackageId()][] = $item;
            }
        }

        return $this->_bundlePromos;
    }

    /**
     * Collect address discount amount
     *
     * @param   Mage_Sales_Model_Quote_Address $address
     * @return  Dyna_Checkout_Model_SalesRule_Quote_Discount
     */
    public function collect(Mage_Sales_Model_Quote_Address $address)
    {
        $conn = Mage::getSingleton('core/resource')->getConnection('core_write');
        // setting address on current instance
        $this->_setAddress($address);
        $address->unsetData('cached_items_nonnominal');
        $address->unsetData('cached_items_all');
        $address->unsetData('cached_items_nominal');
        // loading promo items from current package that target other package items
        $this->_initPromos($address);
        $this->_initBundlePromos($address);

        /**
         * Reset amounts
         */
        $this->_setMafAmount(0);
        $this->_setBaseMafAmount(0);

        $items = $this->_getAddressItems($address);
        if (!count($items)) {
            return $this;
        }

        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = $address->getQuote();
        $couponsPerPackage = $quote->getCouponsPerPackage();
        // Remember the previous active package id.
        $prevActivePackageId = $quote->getActivePackageId();
        $store = Mage::app()->getStore($quote->getStoreId());

        $packages = $quote->getPackageModels();

        $this->_calculator->init($store->getWebsiteId(), $quote->getCustomerGroupId(), '', $packages, $address);

        // delete promo items, will be added back by valid sales rules
        foreach ($items as $item) {
            $item->isPromo() && $item->isDeleted(true);
        }

        // calculating mixmatch subtotals before
        $this->collectProcessPrices($address, $items);

        foreach ($items as $item) {
            $packageModel = $quote->getCartPackage($item->getPackageId());

            if (!$packageModel) {
                continue;
            }

            $quote->setActivePackageId($item->getPackageId());
            $coupon = isset($couponsPerPackage[$item->getPackageId()]) ? $couponsPerPackage[$item->getPackageId()] : $packageModel->getCoupon();
            $this->_calculator
                ->setActivePackage($packageModel)
                ->setCouponCode($coupon)
                ->setWebsiteId($store->getWebsiteId())
                ->setCustomerGroupId($quote->getCustomerGroupId());

            $eventArgs = array(
                'website_id' => $store->getWebsiteId(),
                'customer_group_id' => $quote->getCustomerGroupId(),
                'coupon_code' => $packageModel->getCoupon(),
            );

            $eventArgs['item'] = $item;
            Mage::dispatchEvent('sales_quote_address_discount_item', $eventArgs);

            $this->_initPromos($address);
            $this->collectProcessItem($item);
            $this->collectProcessDiscount($item);
        }

        $address->unsetData('cached_items_nonnominal');
        $address->unsetData('cached_items_all');
        $address->unsetData('cached_items_nominal');

        $quote->setActivePackageId($prevActivePackageId);
        Mage::helper('dyna_configurator/expression')->updateInstance($quote);

        $itemIds = array();
        foreach ($packages as $pack) {
            if (
                isset($this->_calculator->couponsApplied[$pack->getPackageId()])
                && $this->_calculator->couponsApplied[$pack->getPackageId()] != $pack->getCoupon()
            ) {
                Mage::helper('pricerules')->incrementCoupon(
                    $this->_calculator->couponsApplied[$pack->getPackageId()],
                    $quote->getCustomer()->getId()
                );
                $pack->setCoupon($this->_calculator->couponsApplied[$pack->getPackageId()])->save();
            } elseif (
                !isset($this->_calculator->couponsApplied[$pack->getPackageId()])
                && $pack->getCoupon() != null
            ) {
                Mage::helper('pricerules')->decrementCoupon($pack->getCoupon(), $quote->getCustomer()->getId());
                $itemIds[] = $pack->getId();
            }
        }

        if (count($itemIds)) {
            $conn->update('catalog_package',
                array('coupon' => null, 'updated_at' => now()),
                sprintf('entity_id in (%s)', join(',', $itemIds))
            );
        }

        return $this;
    }


    /**
     * Return checkout helper
     * @return Dyna_Checkout_Helper_Data
     */
    public function getCheckoutHelper()
    {
        if (!$this->checkoutHelper) {
            $this->checkoutHelper = Mage::helper('omnius_checkout');
        }

        return $this->checkoutHelper;
    }

    /**
     * Quote item discount calculation process
     *
     * @param   Mage_Sales_Model_Quote_Item_Abstract|Dyna_Checkout_Model_Sales_Quote_Item $item
     * @return  Dyna_Checkout_Model_SalesRule_Quote_Discount
     */
    public function process(Mage_Sales_Model_Quote_Item_Abstract $item)
    {
        if ($item->isPromo() || $item->isBundlePromo()) {
            return $this;
        }

        $this->_initPromos($this->_getAddress());
        $item->setDiscountAmount(0);
        $item->setBaseDiscountAmount(0);
        $item->setConvertedPrice(null);
        $quote = $item->getQuote();

        $itemPrice = $item->getPriceInclTax() ?: $this->_getItemPrice($item);
        $baseItemPrice = $item->getBasePriceInclTax() ?: $this->_getItemBasePrice($item);


        if ($itemPrice <= 0) {
            return $this;
        }

        $discount = $item->getProduct()->getPriceDiscount();
        $quoteAmount = $quote->getStore()->convertPrice($discount);

        if (isset($this->_promos[$item->getPackageId()])) {
            $discount = 0;
            $qty = $item->getTotalQty();

            /** @var Dyna_Checkout_Model_Sales_Quote_Item $promo */
            foreach ($this->_promos[$item->getPackageId()] as $promo) {
                $promoProd = $promo->getProduct();
                $unitDiscount = 0;

                if ($promo->getTargetId() == $item->getProductId()) {

                    if ($promoProd->getPriceDiscount()) {
                        if ($promoProd->getPriceDiscount() < $itemPrice) {
                            $unitDiscount = $qty * (float)$promoProd->getPriceDiscount();
                        } else {
                            $unitDiscount = $itemPrice;
                        }
                    } elseif ($promoProd->getPriceDiscountPercent()) {
                        $percentDiscount = (int)$promoProd->getPriceDiscountPercent();
                        if ($percentDiscount > 0 && $percentDiscount <= 100) {
                            $unitDiscount = ((float)$qty * $itemPrice * $percentDiscount) / 100;
                        }
                    }
                }

                if ($unitDiscount > 0) {
                    $discount += $unitDiscount;

                    $promo->setAppliedPromoAmount($quote->getStore()->roundPrice($quote->getStore()->convertPrice($unitDiscount)));
                    $promo->setBaseAppliedPromoAmount($quote->getStore()->roundPrice($unitDiscount));
                }
            }

            $quoteAmount = $quote->getStore()->convertPrice($discount);
        }

        $qty = $item->getTotalQty();

        $discountAmount = $qty * $quoteAmount;
        $baseDiscountAmount = $qty * $discount;
        $discountAmount = $quote->getStore()->roundPrice($discountAmount);
        $baseDiscountAmount = $quote->getStore()->roundPrice($baseDiscountAmount);

        /**
         * We can't use row total here because row total not include tax
         * Discount can be applied on price included tax
         */

        $itemDiscountAmount = $item->getDiscountAmount();
        $itemBaseDiscountAmount = $item->getBaseDiscountAmount();

        $discountAmount = min($itemDiscountAmount + $discountAmount, $itemPrice * $qty);
        $baseDiscountAmount = min($itemBaseDiscountAmount + $baseDiscountAmount, $baseItemPrice * $qty);

        // setting price discounts on item
        $item->setDiscountAmount($discountAmount);
        $item->setBaseDiscountAmount($baseDiscountAmount);

        return $this;
    }

    /*
    * Return item price
    *
    * @param Mage_Sales_Model_Quote_Item_Abstract $item
    * @return float
    */
    protected function _getItemMaf($item)
    {
        $maf = $item->getDiscountCalculationMaf();
        $calcMaf = $item->getCalculationMaf();

        // Removed device/option restriction for mix match calculation
        if ($item->getMixmatchMafSubtotal()) {
            return $item->getMixmatchMafSubtotal() + $item->getMixmatchMafTax();
        }

        return ($maf !== null) ? $maf : $calcMaf;
    }

    /**
     * @param Mage_Sales_Model_Quote_Item_Abstract $item
     * @return $this
     */
    public function processMaf(Mage_Sales_Model_Quote_Item_Abstract $item)
    {
        if ($item->isPromo() || $item->isBundlePromo()) {
            return $this;
        }

        $item->setMafDiscountAmount(0);
        $item->setBaseMafDiscountAmount(0);
        $quote = $item->getQuote();

        $itemMaf = $this->_getItemMaf($item);
        $baseItemPrice = $this->_getItemBaseMaf($item);

        if ($itemMaf <= 0) {
            return $this;
        }

        $discount = $item->getProduct()->getMafDiscount();

        if (isset($this->_promos[$item->getPackageId()]) || isset($this->_bundlePromos[$item->getPackageId()])) {
            $discount = 0;
            $qty = $item->getTotalQty();

            $this->_promos[$item->getPackageId()] = $this->_promos[$item->getPackageId()] ?? array();
            $applicablePromos = $this->_promos[$item->getPackageId()];
            // Inject bundle promos as well
            if (isset($this->_bundlePromos[$item->getPackageId()])) {
                $applicablePromos = array_merge($applicablePromos, $this->_bundlePromos[$item->getPackageId()]);
            }

            /** @var Dyna_Checkout_Model_Sales_Quote_Item $promo */
            foreach ($applicablePromos as $promo) {
                $promoProd = $promo->getProduct();
                $unitDiscount = $this->getUnitDiscount($item, $promo, $promoProd, $itemMaf, $qty);
                if ($unitDiscount) {
                    $discount += $unitDiscount;

                    // store also on promo maf
                    $promo->setAppliedMafPromoAmount($quote->getStore()->convertPrice($unitDiscount));
                    $promo->setBaseAppliedMafPromoAmount($quote->getStore()->roundPrice($unitDiscount));
                }
            }
        }

        $qty = $item->getTotalQty();

        $discountAmount = $qty * $discount;
        $baseDiscountAmount = $qty * $discount;
        $discountAmount = $quote->getStore()->roundPrice($discountAmount);
        $baseDiscountAmount = $quote->getStore()->roundPrice($baseDiscountAmount);

        /**
         * We can't use row total here because row total not include tax
         * Discount can be applied on price included tax
         */

        $itemDiscountAmount = $item->getMafDiscountAmount();
        $itemBaseDiscountAmount = $item->getBaseMafDiscountAmount();

        $discountAmount = min($itemDiscountAmount + $discountAmount, $itemMaf * $qty);
        $baseDiscountAmount = min($itemBaseDiscountAmount + $baseDiscountAmount, $baseItemPrice * $qty);

        // setting maf discounts on item
        $item->setCalculationMaf(null);

        $item->setMafDiscountAmount($discountAmount);
        $item->setBaseMafDiscountAmount($baseDiscountAmount);

        return $this;
    }

    /**
     * @param Mage_Sales_Model_Quote_Address $address
     * @param $items
     */
    protected function collectProcessPrices(Mage_Sales_Model_Quote_Address $address, $items)
    {
        foreach ($items as $item) {
            $prices = $this->getCheckoutHelper()->updateMixMatches($address->getQuote(), $item->getPackageId(), $item);
            if ($prices) {
                // VFDED1W3S-565: Ignore OC prices for ILS products
                // so we do not update mixMatch prices for products with is_contract = true
                if (!$item->isContract()) {
                    $item->setMixmatchSubtotal($prices['subtotal'])
                        ->setMixmatchTax($prices['tax'])
                        ->setMixmatchMafSubtotal($prices['maf_subtotal'])
                        ->setMixmatchMafTax($prices['maf_tax']);
                }
            }
        }
    }

    /**
     * @param Mage_Sales_Model_Quote_Item_Abstract $item
     * @param $promo
     * @param $promoProd Dyna_Catalog_Model_Product
     * @param $itemPrice
     * @param $qty
     * @return float|int
     */
    protected function getUnitDiscount(Mage_Sales_Model_Quote_Item_Abstract $item, $promo, $promoProd, $itemPrice, $qty)
    {
        $unitDiscount = 0;

        if ($promo->getTargetId() == $item->getProductId()) {
            if ($promoProd->getMafDiscount()) {
                if ($promoProd->getMafDiscount() < $itemPrice) {
                    $unitDiscount = $promoProd->getMafDiscount() * $qty;
                } else {
                    $unitDiscount = $itemPrice;
                }
            } elseif ($promoProd->getMafDiscountPercent()) {
                if ($promoProd->getMafDiscountPercent() > 0 && $promoProd->getMafDiscountPercent() <= 100) {
                    $unitDiscount = ($promoProd->getMafDiscountPercent() * $itemPrice * $qty) / 100;
                }
            }
        }

        return $unitDiscount;
    }

    /**
     * @param $item
     */
    protected function collectProcessDiscount($item)
    {
        if ($item->getHasChildren() && $item->isChildrenCalculated()) {
            foreach ($item->getChildren() as $child) {
                $this->process($child);
                $this->processMaf($child);
            }
        } else {
            $this->process($item);
            $this->processMaf($item);
        }
    }
}
