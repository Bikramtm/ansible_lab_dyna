<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Dyna_Checkout_Model_Field extends Mage_Core_Model_Abstract
{
    /**
     * Dyna_Checkout_Model_Field constructor.
     */
    public function _construct()
    {
        $this->_init('dyna_checkout/field');
    }

    /**
     * Load current model data from combination of quote id, field name and checkout step
     * @param $checkoutStep
     * @param $fieldName
     * @param $quoteId
     * @return $this
     */
    public function loadByQuote($checkoutStep, $fieldName, $quoteId)
    {
        /** @var $collection  Dyna_Checkout_Model_Resource_Field_Collection */
        $oldItem = $this->getCollection()
            ->addFieldToFilter("quote_id", ["eq" => $quoteId])
            ->addFieldToFilter("checkout_step", ["eq" => $checkoutStep])
            ->addFieldToFilter("field_name", ["eq" => $fieldName])
            ->getFirstItem();

        $this->setData($oldItem->getData());

        return $this;
    }

    /**
     * Load all data for a given filter
     * @param $filters
     * @return $this
     */
    public function loadQuoteData($filters)
    {
        /** @var $collection  Dyna_Checkout_Model_Resource_Field_Collection */
        $collection = $this->getCollection();
        foreach($filters as $field => $value){
            if(is_array($value) && count($value) > 1){
                $collection->addFieldToFilter($field, ["in" => $value]);
            }
            else{
                $collection->addFieldToFilter($field, ["eq" => is_array($value) ? current($value) : $value]);
            }
        }

        return $collection;
    }

    /**
     * @param $packageIds
     * @return Dyna_Checkout_Model_Resource_Field_Collection
     */
    public function loadPackageData($packageIds)
    {
        /** @var $collection  Dyna_Checkout_Model_Resource_Field_Collection */
        $collection = $this->getCollection();
        $collection->addFieldToFilter('package_id', ['in' => $packageIds]);

        return $collection;
    }
}
