<?php

/**
 * Class Dyna_Checkout_Model_Cart
 * @method setProcessContext($string) Flag for the current context of the sale
 * @method getProcessContext() return string
 */
class Dyna_Checkout_Model_Cart extends Omnius_Checkout_Model_Cart
{
    /**
     * Add product to shopping cart (quote)
     *
     * @param   int|Mage_Catalog_Model_Product $productInfo
     * @param   mixed $requestInfo
     * @return  Dyna_Checkout_Model_Sales_Quote_Item|string
     */
    public function addProduct($productInfo, $requestInfo = null)
    {
        $product = $this->_getProduct($productInfo);
        $request = $this->_getProductRequest($requestInfo);

        $productId = $product->getId();

        if ($product->getStockItem()) {
            $minimumQty = $product->getStockItem()->getMinSaleQty();
            //If product was not found in cart and there is set minimal qty for it
            if ($minimumQty && $minimumQty > 0 && $request->getQty() < $minimumQty
                && !$this->getQuote()->hasProductId($productId)
            ) {
                $request->setQty($minimumQty);
            }
        }

        $extraLogger = Mage::helper('dyna_configurator/extraLogCart');
        if ($productId) {
            try {
                $extraLogger->extraLog("Adding product to cart: " . $productId);
                /** @var Dyna_Checkout_Model_Sales_Quote_Item $result */
                $result = $this->getQuote()->addProduct($product, $request);

                if (is_string($result)) {
                    return $result;
                }

                $result->addData($request->getData());
                $result->setContractPossibleCancellationDate($request->getContractPossibleCancellationDate());
                // todo: make sure that below condition still works
                // set contract value from request only if result of adding product to cart (quote item) doesn't have is_contract set
                // because result might be a previously removed contract item which was added back
                // $result->setIsContract($result->getIsContract() ?: $request->getIsContract());
            } catch (Mage_Core_Exception $e) {
                $this->getCheckoutSession()->setUseNotice(false);
                $result = $e->getMessage();
                $extraLogger->extraLog("Could not add product to cart: " . $productId . "; Error message: " . $result);
            }
            /**
             * String we can get if prepare process has error
             */
            if (is_string($result)) {
                $redirectUrl = ($product->hasOptionsValidationFail())
                    ? $product->getUrlModel()->getUrl(
                        $product,
                        array('_query' => array('startcustomization' => 1))
                    )
                    : $product->getProductUrl();
                $this->getCheckoutSession()->setRedirectUrl($redirectUrl);
                if ($this->getCheckoutSession()->getUseNotice() === null) {
                    $this->getCheckoutSession()->setUseNotice(true);
                }
                Mage::throwException($result);
            }
        } else {
            Mage::throwException(Mage::helper('checkout')->__('The product does not exist.'));
        }

        Mage::dispatchEvent('checkout_cart_product_add_after', array('quote_item' => $result, 'product' => $product, 'quote' => $this->getQuote()));
        $this->getCheckoutSession()->setLastAddedProductId($productId);

        return $result;
    }

    /**
     * Very good reasons to override this method
     *
     * @return Mage_Checkout_Model_Cart
     */
    public function save()
    {
        Mage::dispatchEvent('checkout_cart_save_before', array('cart' => $this));

        $this->getQuote()->getBillingAddress();
        $this->getQuote()->getShippingAddress()->setCollectShippingRates(true);
        $this->getQuote()->collectTotals();
        $this->getQuote()->save();
        // No need to override active quote id for every flow (cart session model will handle that)
        // Cart save usually called after changes with cart items.
        Mage::dispatchEvent('checkout_cart_save_after', array('cart' => $this));
        return $this;
    }
}
