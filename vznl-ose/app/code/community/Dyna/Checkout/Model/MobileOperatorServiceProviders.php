<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */


/** Class Dyna_Checkout_Model_MobileOperatorServicerProviders */
class Dyna_Checkout_Model_MobileOperatorServiceProviders extends Mage_Core_Model_Abstract
{
    /**
     * Dyna_Checkout_Model_MobileOperatorServicerProviders constructor.
     */
    protected function _construct() {
        $this->_init('dyna_checkout/mobileOperatorServiceProviders');
    }
}