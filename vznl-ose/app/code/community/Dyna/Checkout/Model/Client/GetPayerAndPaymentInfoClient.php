<?php

/**
 * Class Dyna_Customer_Model_Client_GetPayerAndPaymentInfoClient
 */
class Dyna_Checkout_Model_Client_GetPayerAndPaymentInfoClient extends Dyna_Service_Model_Client
{
    const WSDL_CONFIG_KEY = "payer_and_payment_info/wsdl";
    const ENDPOINT_CONFIG_KEY = "payer_and_payment_info/endpoint";
    const CONFIG_STUB_PATH = 'omnius_service/payer_and_payment_info/use_stubs';

    /**
     * @param $params
     * @return mixed
     */
    public function executeGetPayerAndPaymentInfo($params)
    {
        $params = $this->mapRequestFields($params);
        $this->setRequestHeaderInfo($params);
        $values = $this->GetPayerAndPaymentInfo($params);

        return $values;
    }

    private function mapRequestFields(array $params) : array
    {
        $parametersMapping = [
            'IBAN' => $params['iban']
        ];

        return $parametersMapping;
    }
}

