<?php

class Dyna_Checkout_Model_Client_ConvertAndValidateIBANClient extends Dyna_Service_Model_Client
{
    protected $response = array();

    const WSDL_CONFIG_KEY = "convert_iban/wsdl_convert_iban";
    const ENDPOINT_CONFIG_KEY = "convert_iban/usage_url";
    const CONFIG_STUB_PATH = 'convert_iban/use_stubs';

    /**
     * @param string $IBAN
     * @return bool
     */
    public function executeIsValidIBAN(string $IBAN) : bool
    {
        $params = [];
        $params['ConvertAndValidateIBAN']['FinancialAccount']['ID'] = $IBAN;
        $this->response = $this->executeConvertAndValidateIBAN($params);

        return (isset($this->response['Status']) && $this->response['Status'] == true);
    }

    /**
     * @param string $accountNumber - AccountTypeCode
     * @param string $bankIdentifier - ID
     * @param string $countryCode - Country
     * @return mixed
     */
    public function executeConvertAndValidateByBankData(string $accountNumber, string $bankIdentifier, string $countryCode = "DE")
    {
        $params = [];
        $params['ConvertAndValidateIBAN']['FinancialAccount']['AccountTypeCode'] = $accountNumber;
        $params['ConvertAndValidateIBAN']['FinancialInstitution']['ID'] = $bankIdentifier;
        $params['ConvertAndValidateIBAN']['FinancialAccount']['Country']['IdentificationCode'] = $countryCode;

        return $this->executeConvertAndValidateIBAN($params);
    }

    /**
     *
     * @param array $params
     * @return mixed
     */
    private function executeConvertAndValidateIBAN(array $params = [])
    {
        $this->setRequestHeaderInfo($params);
        $response = $this->ConvertAndValidateIBAN($params);

        return [
            'bankData' => [
                [
                    'ID' => $response['ConvertAndValidateIBAN']['FinancialAccount']['ID'],
                    'Name' => $response['ConvertAndValidateIBAN']['FinancialAccount']['Name'],
                    'BIC' => $response['ConvertAndValidateIBAN']['FinancialAccount']['BIC'],
                    'BankIdentifier' => $response['ConvertAndValidateIBAN']['FinancialAccount']['BankIdentifier']
                ]
            ],
            'Status' => $response['ConvertAndValidateIBAN']['ConversionStatus']['Success'],
            'IbanCode' => isset($response['ConvertAndValidateIBAN']['ConversionStatus']['StatusReason']) ? $response['ConvertAndValidateIBAN']['ConversionStatus']['StatusReason'] : '',
            'IbanError' => isset($response['ConvertAndValidateIBAN']['ConversionStatus']['Text']) ? $response['ConvertAndValidateIBAN']['ConversionStatus']['Text'] : '',
        ];
    }

    /**
     * @param string $IBAN
     * @return array|mixed
     */
    public function getResponseData(string $IBAN)
    {
        if(empty($this->response)){
            $params = [];
            $params['ConvertAndValidateIBAN']['FinancialAccount']['ID'] = $IBAN;
            $this->response = $this->executeConvertAndValidateIBAN($params);
        }

        return $this->response;
    }
}
