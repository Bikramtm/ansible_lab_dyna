<?php

/**
 * Class Dyna_Checkout_Model_Client_SAPShippingConditionClient
 */
class Dyna_Checkout_Model_Client_SAPShippingConditionClient extends Dyna_Service_Model_Client
{
    const WSDL_CONFIG_KEY = "sap_shipping_condition/wsdl";
    const ENDPOINT_CONFIG_KEY = "sap_shipping_condition/usage_url";
    const CONFIG_STUB_PATH = 'sap_shipping_condition/use_stubs';

    /**
     * @param $postcode
     * @return bool
     */
    public function isValidSameDayDelivery($postcode) : bool
    {
        $deliveryDate = date("Y-m-d");
        $data = [
            'postcode' => $postcode,
            'delivery_date' => $deliveryDate
        ];

        $result = $this->executeCheckShippingCondition($data);

        return $result['SAPStatus']['ResponseDetails']['Success'] === "Y";
    }
    
    /**
     * @param $postcode
     * @return bool
     */
    public function isValidSameDayDeliveryPerPackage($postcode) : bool
    {
        $deliveryDate = date("Y-m-d");
        $data = [
            'postcode' => $postcode,
            'delivery_date' => $deliveryDate
        ];

        $result = $this->executeCheckShippingConditionPerPackage($data);

        return $result['SAPStatus']['ResponseDetails']['Success'] === "Y";
    }
    
    /**
     * @param array $params
     * @return array
     */ 
    public function executeCheckShippingConditionPerPackage($params = []) : array
    {
        $params = $this->mapRequestFieldsPerPackage($params);
        $this->setRequestHeaderInfo($params);

        $result = $this->CheckShippingCondtition($params);

        return $result;
    }

    /**
     * @param array $params
     * @return array
     */
    public function executeCheckShippingCondition($params = []) : array
    {
        $params = $this->mapRequestFields($params);
        $this->setRequestHeaderInfo($params);

        $result = $this->CheckShippingCondtition($params);

        return $result;
    }
    
    /**
     * @param $params
     * @return array
     */
    private function mapRequestFieldsPerPackage(array $params) : array
    {
        $parametersMapping = [
            'ShippingCondition' => [
                'Address' => [
                    'Postbox' => $params['postcode']
                ],
                'ShipmentStage' => [
                    'RequiredDeliveryDate' => $params['delivery_date']
                ]
            ]
        ];

        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $quoteItemsByType = $quote->getPackageDataByType(null, $quote->getAllItems(), null);

        $devices = [];
        if ($quoteItemsByType) {
            foreach ($quoteItemsByType as $quoteItem) {
                $devices = array_merge($devices, $quoteItem[Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_DEVICE]);
            }
        }

        if (count($devices)) {
            /** @var Dyna_Checkout_Model_Sales_Quote_Item $device */
            foreach ($devices as $index => $device) {
                $containedGoodsItems['ContainedGoodsItem'][] = [
                    'Item' => substr((string) (1000000 + $index + 1), 1),
                    'MatNR' => $device->getProduct()->getArticleNo(),
                    'TotalGoodsItemQuantity' => (int) $device->getQty()
                ];
            }
        }

        $parametersMapping['ShippingCondition']['Shipment']['GoodsItem'] = $containedGoodsItems;

        //OMNVFDE-3210
        $parametersMapping['SalesOrder']['Customer']['PartyIdentification']['ID'] = Mage::getSingleton("core/session")->getEncryptedSessionId();

        return $parametersMapping;
    }

    /**
     * @param $params
     * @return array
     */
    private function mapRequestFields(array $params) : array
    {
        $parametersMapping = [
            'ShippingCondition' => [
                'Address' => [
                    'Postbox' => $params['postcode']
                ],
                'ShipmentStage' => [
                    'RequiredDeliveryDate' => $params['delivery_date']
                ]
            ]
        ];

        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $quoteItemsByType = $quote->getPackageDataByType(null, $quote->getAllItems(), null);

        $devices = [];
        
        if ($quoteItemsByType) {
            foreach ($quoteItemsByType as $quoteItem) {
                $devices = array_merge($devices, $quoteItem[Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_DEVICE]);
            }
        }
        
        $values = [];

        if (count($devices)) {
            /** @var Dyna_Checkout_Model_Sales_Quote_Item $device */
            foreach ($devices as $index => $device) {
                $values[] = $index;
                $containedGoodsItems['ContainedGoodsItem'][] = [
                    'Item' => substr((string) (1000000 + $index + 1), 1),
                    'MatNR' => $device->getProduct()->getArticleNo(),
                    'TotalGoodsItemQuantity' => (int) $device->getQty()
                ];
            }
        }

        $parametersMapping['ShippingCondition']['Shipment']['GoodsItem'] = $containedGoodsItems;

        //OMNVFDE-3210
        $parametersMapping['SalesOrder']['Customer']['PartyIdentification']['ID'] = Mage::getSingleton("core/session")->getEncryptedSessionId();

        return $parametersMapping;
    }

    /**
     * @return string Hardcoded to SAP for SAP services
     */
    public function getPartyName()
    {
        return 'SAP';
    }

    /**
     * @param array $response
     * @return bool
     */
    protected function hasServiceErrors($response)
    {
        return !isset($response['Success']) || $response['SAPStatus']['ResponseDetails']['Success'] != 'Y';
    }
}
