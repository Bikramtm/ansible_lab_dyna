<?php

/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */
class Dyna_Checkout_Block_Call extends Omnius_Checkout_Block_Cart
{
    use Dyna_Checkout_Block_Cart_Steps_Common {
        Dyna_Checkout_Block_Cart_Steps_Common::getCustomerFields as traitGetCustomerFields;
    }

    // ### Block identifiers ###
    const CALL_SUMMARY_BLOCK_IDENTIFIER_GIGAKOMBI_CABLE = 'call_summary_gigakombi_cable';
    const CALL_SUMMARY_BLOCK_IDENTIFIER_GIGAKOMBI_CABLE_MIGRATION = 'call_summary_gigakombi_cable_migration';
    const CALL_SUMMARY_BLOCK_IDENTIFIER_GIGAKOMBI_MOBILE = 'call_summary_gigakombi_mobile';
    const CALL_SUMMARY_BLOCK_IDENTIFIER_CABLE = 'call_summary_cable';
    const CALL_SUMMARY_BLOCK_IDENTIFIER_CABLE_MIGRATION = 'call_summary_cable_migration';
    const CALL_SUMMARY_BLOCK_IDENTIFIER_MOBILE = 'call_summary_mobile';
    const CALL_SUMMARY_BLOCK_IDENTIFIER_DSL = 'call_summary_dsl';

    // Next Steps
    const CALL_SUMMARY_BLOCK_IDENTIFIER_POSTPAID_NEXT_STEPS = 'call_summary_postpaid_next_steps';
    const CALL_SUMMARY_BLOCK_IDENTIFIER_PREPAID_NEXT_STEPS = 'call_summary_prepaid_next_steps';
    const CALL_SUMMARY_BLOCK_IDENTIFIER_CABLE_NEXT_STEPS = 'call_summary_cable_next_steps';
    const CALL_SUMMARY_BLOCK_IDENTIFIER_DSL_NEXT_STEPS = 'call_summary_dsl_next_steps';
    const CALL_SUMMARY_BLOCK_IDENTIFIER_GIGAKOMBI_NEXT_STEPS = 'call_summary_gigakombi_next_steps';

    const CALL_SUMMARY_BLOCK_IDENTIFIER_POSTPAID_NEXT_STEPS_ACTIVATION = 'call_summary_postpaid_next_steps_activation';
    const CALL_SUMMARY_BLOCK_IDENTIFIER_POSTPAID_NEXT_STEPS_PROLONGATION = 'call_summary_postpaid_next_steps_prolongation';
    const CALL_SUMMARY_BLOCK_IDENTIFIER_POSTPAID_NEXT_STEPS_CHANGE = 'call_summary_postpaid_next_steps_change';
    const CALL_SUMMARY_BLOCK_IDENTIFIER_POSTPAID_NEXT_STEPS_DEBIT = 'call_summary_postpaid_next_steps_debit';
    const CALL_SUMMARY_BLOCK_IDENTIFIER_POSTPAID_NEXT_STEPS_SHIP = 'call_summary_postpaid_next_steps_ship';
    const CALL_SUMMARY_BLOCK_IDENTIFIER_PREPAID_NEXT_STEPS_ACTIVATION = 'call_summary_prepaid_next_steps_activation';
    const CALL_SUMMARY_BLOCK_IDENTIFIER_PREPAID_NEXT_STEPS_SHIP = 'call_summary_prepaid_next_steps_ship';
    const CALL_SUMMARY_BLOCK_IDENTIFIER_CABLE_NEXT_STEPS_ACTIVATION = 'call_summary_cable_next_steps_activation';
    const CALL_SUMMARY_BLOCK_IDENTIFIER_CABLE_NEXT_STEPS_PROVIDER_CHANGE = 'call_summary_cable_next_steps_provider_change';
    const CALL_SUMMARY_BLOCK_IDENTIFIER_CABLE_NEXT_STEPS_CANCELLATION = 'call_summary_cable_next_steps_cancellation';
    const CALL_SUMMARY_BLOCK_IDENTIFIER_CABLE_NEXT_STEPS_CHANGE = 'call_summary_cable_next_steps_change';
    const CALL_SUMMARY_BLOCK_IDENTIFIER_CABLE_NEXT_STEPS_SELF = 'call_summary_cable_next_steps_self';
    const CALL_SUMMARY_BLOCK_IDENTIFIER_DSL_NEXT_STEPS_CANCELLATION = 'call_summary_dsl_next_steps_cancellation';
    const CALL_SUMMARY_BLOCK_IDENTIFIER_DSL_NEXT_STEPS_ACTIVATION = 'call_summary_dsl_next_steps_activation';
    const CALL_SUMMARY_BLOCK_IDENTIFIER_DSL_NEXT_STEPS_CHANGE = 'call_summary_dsl_next_steps_change';
    const CALL_SUMMARY_BLOCK_IDENTIFIER_GIGAKOMBI_NEXT_STEPS_ACTIVATION = 'call_summary_gigakombi_next_steps_activation';

    // End Notes
    const CALL_SUMMARY_BLOCK_IDENTIFIER_END_NOTES = 'call_summary_end_notes';

    const CALL_SUMMARY_BLOCK_IDENTIFIER_END_NOTES_NOT_BINDING = 'call_summary_end_notes_not_binding';
    const CALL_SUMMARY_BLOCK_IDENTIFIER_END_NOTES_POSTPAID = 'call_summary_end_notes_postpaid';
    const CALL_SUMMARY_BLOCK_IDENTIFIER_END_NOTES_DEDICATED = 'call_summary_end_notes_dedicated';

    // ### Block titles ###
    const CALL_SUMMARY_BLOCK_TITLE_GIGAKOMBI_CABLE = 'Call Summary - Gigakombi Cable New';
    const CALL_SUMMARY_BLOCK_TITLE_GIGAKOMBI_CABLE_MIGRATION = 'Call Summary - Gigakombi Cable New Migration';
    const CALL_SUMMARY_BLOCK_TITLE_GIGAKOMBI_MOBILE = 'Call Summary - Gigakombi Mobile New';
    const CALL_SUMMARY_BLOCK_TITLE_CABLE = 'Call Summary - Cable New';
    const CALL_SUMMARY_BLOCK_TITLE_CABLE_MIGRATION = 'Call Summary - Cable New Migration';
    const CALL_SUMMARY_BLOCK_TITLE_MOBILE = 'Call Summary - Mobile New';
    const CALL_SUMMARY_BLOCK_TITLE_DSL = 'Call Summary - DSL/LTE New';
    //  Next Steps
    const CALL_SUMMARY_BLOCK_TITLE_POSTPAID_NEXT_STEPS = 'Call Summary - Postpaid Next Steps';
    const CALL_SUMMARY_BLOCK_TITLE_PREPAID_NEXT_STEPS = 'Call Summary - Prepaid Next Steps';
    const CALL_SUMMARY_BLOCK_TITLE_CABLE_NEXT_STEPS = 'Call Summary - Cable Next Steps';
    const CALL_SUMMARY_BLOCK_TITLE_DSL_NEXT_STEPS = 'Call Summary - Dsl Next Steps';
    const CALL_SUMMARY_BLOCK_TITLE_GIGAKOMBI_NEXT_STEPS = 'Call Summary - GigaKombi Next Steps';

    const CALL_SUMMARY_BLOCK_TITLE_POSTPAID_NEXT_STEPS_ACTIVATION = 'Call Summary - Next Steps | Postpaid Activation';
    const CALL_SUMMARY_BLOCK_TITLE_POSTPAID_NEXT_STEPS_PROLONGATION = 'Call Summary - Next Steps | Prolong Mobile Postpaid';
    const CALL_SUMMARY_BLOCK_TITLE_POSTPAID_NEXT_STEPS_CHANGE = 'Call Summary - Next Steps | Change Mobile Postpaid Package';
    const CALL_SUMMARY_BLOCK_TITLE_POSTPAID_NEXT_STEPS_DEBIT = 'Call Summary - Next Steps | Debit to Credit';
    const CALL_SUMMARY_BLOCK_TITLE_POSTPAID_NEXT_STEPS_SHIP = 'Call Summary - Next Steps | Ship to store Mobile Postpaid';
    const CALL_SUMMARY_BLOCK_TITLE_PREPAID_NEXT_STEPS_ACTIVATION = 'Call Summary - Next Steps | Prepaid Activation';
    const CALL_SUMMARY_BLOCK_TITLE_PREPAID_NEXT_STEPS_SHIP = 'Call Summary - Next Steps | Ship to store Mobile Prepaid';
    const CALL_SUMMARY_BLOCK_TITLE_CABLE_NEXT_STEPS_ACTIVATION = 'Call Summary - Next Steps | Cable Activation';
    const CALL_SUMMARY_BLOCK_TITLE_CABLE_NEXT_STEPS_PROVIDER_CHANGE = 'Call Summary - Next Steps | Cable Provider Change';
    const CALL_SUMMARY_BLOCK_TITLE_CABLE_NEXT_STEPS_CANCELLATION = 'Call Summary - Next Steps | Cable Cancellation';
    const CALL_SUMMARY_BLOCK_TITLE_CABLE_NEXT_STEPS_CHANGE = 'Call Summary - Next Steps | Change Cable Package';
    const CALL_SUMMARY_BLOCK_TITLE_CABLE_NEXT_STEPS_SELF = 'Call Summary - Next Steps | Cable Self Install';
    const CALL_SUMMARY_BLOCK_TITLE_DSL_NEXT_STEPS_CANCELLATION = 'Call Summary - Next Steps | DSL/LTE Cancellation';
    const CALL_SUMMARY_BLOCK_TITLE_DSL_NEXT_STEPS_ACTIVATION = 'Call Summary - Next Steps | DSL/LTE Activation';
    const CALL_SUMMARY_BLOCK_TITLE_DSL_NEXT_STEPS_CHANGE = 'Call Summary - Next Steps | Change DSL/LTE Package';
    const CALL_SUMMARY_BLOCK_TITLE_GIGAKOMBI_NEXT_STEPS_ACTIVATION = 'Call Summary - Next Steps | GigaKombi Activation';

    // End Notes
    const CALL_SUMMARY_BLOCK_TITLE_END_NOTES = 'Call Summary - End notes';

    const CALL_SUMMARY_BLOCK_TITLE_END_NOTES_NOT_BINDING = 'Call Summary - End notes | Not binding contract';
    const CALL_SUMMARY_BLOCK_TITLE_END_NOTES_POSTPAID = 'Call Summary - End notes | Mobile Postpaid Specific';
    const CALL_SUMMARY_BLOCK_TITLE_END_NOTES_DEDICATED = 'Call Summary - End notes | Dedicated contract / Separate fulfilment';

    /** @var $quote Dyna_Checkout_Model_Sales_Quote */
    protected $quote = null;
    protected $superorder = null;
    protected $packages = null;
    protected $checkoutData = null;

    /**
     * Initialize block parameters
     * @param $quote
     * @param $superorder
     * @param Dyna_Checkout_Model_Resource_Field_Collection $checkoutData
     * @return $this
     */
    public function init($quote, $superorder = null, $checkoutData = null)
    {
        $this->quote = $quote;
        $this->superorder = $superorder;
        $this->checkoutData = $checkoutData;

        return $this;
    }

    /**
     * Get a list of quote products
     * @return array
     */
    public function getProducts()
    {
        $products = array();
        foreach ($this->quote->getAllItems() as $item) {
            $products[] = $item->getProduct();
        }

        return $products;
    }

    public function isCustomerSoho()
    {
        return Mage::getSingleton('customer/session')
            ->getCustomer()
            ->getIsSoho();
    }

    public function getGreetings()
    {
        $customerFields = $this->getCustomerFields();
        $greetings = '';

        if (isset($customerFields['customer[contact_person][last_name]']) && $customerFields['customer[contact_person][last_name]']) {
            $greetings .= $this->isCustomerSoho() ? (($customerFields['customer[contact_person][gender]'] == '1') ? $this->__('Dear Mr') : $this->__('Dear Mrs')) :
                (($customerFields['customer[contact_person][gender]'] == '1') ? 'Lieber' : 'Liebe');
            $greetings .= $this->isCustomerSoho() ? ' ' . $customerFields['customer[contact_person][last_name]']
                : ' ' . $customerFields['customer[contact_person][first_name]'] . ' ' . $customerFields['customer[contact_person][last_name]'];
        } else {
            $greetings .= $this->isCustomerSoho() ? (($customerFields['customer[gender]'] == '1') ? $this->__('Dear Mr') : $this->__('Dear Mrs')) :
                (($customerFields['customer[gender]'] == '1') ? 'Lieber' : 'Liebe');
            $greetings .= $this->isCustomerSoho() ? ' ' . $customerFields['customer[lastname]']
                : ' ' . $customerFields['customer[firstname]'] . ' ' . $customerFields['customer[lastname]'];
        }
        return $greetings;
    }

    public function getComponents()
    {
        $components = array();
        foreach ($this->quote->getCartPackages() as $package) {
            $clientHelper = Mage::helper('dyna_superorder/client');
            $components[] = $clientHelper->getComponents($package);
        }
        return $components;
    }

    public function getPersonalData()
    {
        $customerFields = $this->getCustomerFields();
        $htmlOutput = '';
        $htmlOutput .= $this->isCustomerSoho() ? ($customerFields['customer[address][company_name]'] ? $customerFields['customer[address][company_name]'] . '<br>' : '') : '';
        if ($this->isCustomerSoho() && $this->hasCable() &&
            (isset($customerFields['customer[contact_person][first_name]']) && $customerFields['customer[contact_person][first_name]'] != '')) {
            $htmlOutput .= (($customerFields['customer[contact_person][gender]'] == '1') ? 'Herr' : 'Frau') . $customerFields['customer[contact_person][first_name]'] . ' ' . $customerFields['customer[contact_person][last_name]'] . '<br>';
        } else {
            $htmlOutput .= (($customerFields['customer[gender]'] == '1') ? 'Herr' : 'Frau') . ($customerFields['customer[prefix]'] ? ' ' .
                $customerFields['customer[prefix]'] : '') . ' ' . $customerFields['customer[firstname]']
                . ' ' . $customerFields['customer[lastname]'] . '<br>';
        }
        $htmlOutput .= $customerFields['customer[address][street]'] . ' ' . $customerFields['customer[address][houseno]'] . ' ' . $customerFields['customer[address][addition]'] . '<br>';
        $htmlOutput .= $customerFields['customer[address][postcode]'] . ' ' . $customerFields['customer[address][city]'];

        return $htmlOutput;
    }

    public function getDeliveryData()
    {
        $customerFields = $this->getCustomerFields('save_delivery');
        if ($customerFields['delivery[method]'] == 'other_address') {
            $result = array();

            $result['name'] = (($customerFields['delivery[other_address][gender]'] == '1') ? 'Herr' : 'Frau') . ($customerFields['delivery[other_address][prefix]'] ? ' ' . $customerFields['delivery[other_address][prefix]'] : '') . ' ' . ($this->isCustomerSoho() ? '' : ($customerFields['customer[firstname]'] . ' ')) . $customerFields['delivery[other_address][lastname]'];
            $result['address'] = $customerFields['delivery[other_address][street]'] . ' ' . $customerFields['delivery[other_address][houseno]'] . ' ' . $customerFields['delivery[other_address][addition]'];
            $result['city'] = $customerFields['delivery[other_address][postcode]'] . ' ' . $customerFields['delivery[other_address][city]'];
            return $result;
        }

        return $this->getPersonalData(true);
    }

    public function getDurationText($product)
    {
        $string = '';
        if ($product->getData('initial_period')) {
            $string .= 'Inkl. Tarifaufpreis bei vergünstigtem Handy<br>' . $product->getData('initial_period') . ' Monate Mindestlaufzeit';
        } elseif ($product->getData('minimum_contract_duration')) {
            $string .= $product->getData('minimum_contract_duration') . ' Monate Mindestlaufzeit';
        } elseif ($product->getData('contract_period')) {
            $string .= $product->getData('contract_period') . ' Monate Mindestlaufzeit';
        }
        return $string;
    }

    public function preliminaryBasePriceFootnote()
    {
        $tarifOptions = ['REDLHPR', 'REDMHPR', 'REDSHPR', 'REDPLDH10', 'REDPLVOS', 'REDPLVH10', 'REDPLVH5', 'REDPLDOS', 'REDPLDH5', 'REDPLVH20', 'REDPLDH20', 'DPAALH5', 'DPAALH10', 'DPAALH20', 'DPAALOS', 'DPAAMOS', 'DPAAMH20', 'DPAAMH10', 'DPAAMH5', 'DPAAPRH5', 'DPAAPRH10', 'DPAAPRH20', 'DPAAPROS', 'DPAASH5', 'DPAASH10', 'DPAASH20', 'DPAASOS', 'DPAAXSOS', 'DPAAXSH5', 'DPAAXSH10', 'DPAAXSH20', 'DPAVLH10', 'DPAVLH5', 'DPAVLH20', 'DPAVLOS', 'DPAVMH20', 'DPAVMH10', 'DPAVMH5', 'DPAVMOS', 'DPAVSH10', 'DPAVSH20', 'DPAVSH5', 'DPAVSOS', 'DPAVXSH20', 'DPAVXSH10', 'DPAVXSH5', 'DPAVXSOS', 'DP13ASH20', 'DP13ASH10', 'DP13ASH5', 'DP13ASOS', 'DP13VSH5', 'DP13VSOS', 'DP13VSH10', 'DP13VSH20', 'SMARTMH20', 'SMARTMH10', 'SMARTMH05', 'SMARTMOS', 'SMARTSOS', 'SMARTSH05', 'SMARTSH10', 'SMARTSH20', 'SMARXLH05', 'SMARXLH10', 'SMARXLH20', 'SMARTLOS', 'SMARTLH05', 'SMARTLH10', 'SMARTLH20', 'SMARXLOS', 'REDXSOS', 'REDXSH5', 'REDXSH10', 'REDXSHPR', 'REDSOS', 'REDSH5', 'REDSH10', 'REDMOS', 'REDMH5', 'REDMH10', 'REDLOS', 'REDLH5', 'REDLH10', 'SMARTLHPR', 'SMARXLHPR', 'MDBSL_OS', 'MDBSL05', 'MDBSL10', 'MDBSL20', 'RED3XXOS', 'RED3XXH10', 'RED3XXH15', 'RED3XXH25', 'MDB_SOS', 'MDB_SH05', 'MDB_SH10', 'RED3XSOS', 'RED3XSH10', 'RED3XSH15', 'RED3XSH25', 'RED3SOS', 'RED3SH10', 'RED3SH15', 'RED3SH25', 'RED3MOS', 'RED3MH10', 'RED3MH15', 'RED3MH25', 'FAREDMOS', 'REDVOCOS', 'REDVOCH05', 'REDVOCH10', 'RED3LOS', 'RED3LH10', 'RED3LH15', 'RED3LH25', 'RED3XLOS', 'RED3XLH10', 'RED3XLH15', 'RED3XLH25', 'MDBR3_OS', 'MDBR3H5', 'MDBR3H10', 'MDBR3HPR', 'MDBR1_0S', 'MDBR1H5', 'MDBR1H10', 'MDBR1HPR',];
        $products = $this->getProducts();
        foreach ($products as $product) {
            $code = $product->getKiasCode();
            if (in_array($code, $tarifOptions)) {
                return true;
            }
        }

        return false;
    }

    public function isSpeedGo()
    {
        $avliableProducts = ['REDDATXS', 'REDDATAS', 'REDDATAM', 'REDDATAL', 'RED3DATS', 'RED3DATM', 'RED3DATL', 'RED3DATXL', 'RED3DATXX', 'ASB_ON'];
        $barrings = ['BARASBBC', 'BARASBBC2', 'BARASBBC3', 'SO_BARASB'];
        $products = $this->getProducts();
        foreach ($products as $product) {
            $code = $product->getKiasCode();
            if (in_array($code, $barrings)) {
                return false;
            }
        }
        foreach ($products as $product) {
            $code = $product->getKiasCode();
            if (in_array($code, $avliableProducts)) {
                return true;
            }
        }

        return false;
    }

    public function getOrderId()
    {
        return $this->superorder->getOrderNumber();
    }

    public function getOrderDate()
    {
        return date('d.m.Y', strtotime($this->superorder->getCreatedAt()));
    }

    public function getPackages()
    {
        if ($this->packages === null) {
            $this->packages = $this->superorder->getPackages();
        }

        return $this->packages;
    }

    private function getNextSteps()
    {
        $blockModel = Mage::getModel('cms/block')
            ->setStoreId(Mage::app()->getStore()->getId());
        $stacks = $blockVariables = array();

        foreach ($this->getPackages() as $package) {
            if (strtolower($package->getType()) == strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE)) {
                $this->handlePostpaidNextStepsConditions($blockModel,$stacks,$blockVariables,$package);
            }
            if (strtolower($package->getType()) == strtolower(Dyna_Catalog_Model_Type::TYPE_PREPAID)) {
                $this->handlePrepaidNextStepsConditions($blockModel,$stacks,$blockVariables,$package);
            }
            if (in_array(strtolower($package->getType()),Dyna_Catalog_Model_Type::getCablePackages())) {
                $this->handleCableNextStepsConditions($blockModel,$stacks,$blockVariables,$package);
            }
            if (in_array(strtolower($package->getType()),Dyna_Catalog_Model_Type::getFixedPackages())) {
                $this->handleFixedNextStepsConditions($blockModel,$stacks,$blockVariables,$package);
            }
            if ($package->isPartOfGigaKombi()) {
                $this->handleGigaKombiNextStepsConditions($blockModel,$stacks,$blockVariables);
            }
        }

        // call each of the above methods one more time in order to retrieve each one's custom messages combination,
        // in order to avoid message duplication if there are multiple packages of same stack with same conditions
        $this->handlePostpaidNextStepsConditions($blockModel,$stacks,$blockVariables);
        $this->handlePrepaidNextStepsConditions($blockModel,$stacks,$blockVariables);
        $this->handleCableNextStepsConditions($blockModel,$stacks,$blockVariables);
        $this->handleFixedNextStepsConditions($blockModel,$stacks,$blockVariables);

        return $this->getNextStepsContent($blockModel,$stacks,$blockVariables);
    }

    private function handlePostpaidNextStepsConditions($blockModel,&$stacksArray,&$blockVariables,$package = false)
    {
        /** @var  $cartHelper  Dyna_Configurator_Helper_Cart */
        $cartHelper = Mage::helper('dyna_configurator/cart');
        // if package is provided, get corresponding message/s
        if ($package) {
            array_push($stacksArray,Dyna_Catalog_Model_Type::TYPE_MOBILE);
            $blockVariables['postpaid']['postpaidStackVisibility'] = '';
            $blockVariables['postpaid']['postpaidIconPath'] = Mage::getBaseDir('skin').'/frontend/vodafone/default/images/de/pdf-mobilfunkvertrag.png';

            /** @var Dyna_Checkout_Helper_Fields $checkoutData */
            $checkoutData = Mage::helper("dyna_checkout/fields");
            $checkoutData->setCheckoutData("save_delivery", $this->quote->getId());

            if (in_array($package->getSaleType(), Dyna_Catalog_Model_ProcessContext::ILSProcessContexts())) {
                // modify postpaid package
                $block = $blockModel->load(Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_POSTPAID_NEXT_STEPS_CHANGE);
                $blockVariables['postpaid']['content']['modify'] = $this->getBlockContent($block);
            } elseif (($package->getPackageCreationTypeId() == $cartHelper->getCreationTypeId(Dyna_Catalog_Model_Type::CREATION_TYPE_MOBILE))
                && (in_array($package->getSaleType(), array(Dyna_Catalog_Model_ProcessContext::MIGCOC,Dyna_Catalog_Model_ProcessContext::MIGNOCOC)))) {
                // debit to credit
                if (!isset($blockVariables['postpaid']['content']['activation'])) {
                    $block = $blockModel->load(Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_POSTPAID_NEXT_STEPS_DEBIT);
                    $blockVariables['postpaid']['content']['debitToCredit'] = $this->getBlockContent($block);
                }
            } elseif (in_array($package->getSaleType(), array(Dyna_Catalog_Model_ProcessContext::RETBLU,Dyna_Catalog_Model_ProcessContext::RETNOBLU,Dyna_Catalog_Model_ProcessContext::RETTER))) {
                // prolongation
                $block = $blockModel->load(Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_POSTPAID_NEXT_STEPS_PROLONGATION);
                $blockVariables['postpaid']['content']['prolongation'] = $this->getBlockContent($block);
            } elseif ($package->getSaleType() == Dyna_Catalog_Model_ProcessContext::ACQ) {
                // activation
                $block = $blockModel->load(Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_POSTPAID_NEXT_STEPS_ACTIVATION);
                $blockVariables['postpaid']['content']['activation'] = $this->getBlockContent($block);
                if (isset($blockVariables['postpaid']['content']['debitToCredit'])) {
                    unset($blockVariables['postpaid']['content']['debitToCredit']);
                }
            }
            // if ship to store has been selected on checkout, display its section message
            if ($checkoutData->getFieldValue("delivery[pickup][store]")) {
                $block = $blockModel->load(Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_POSTPAID_NEXT_STEPS_SHIP);
                $blockVariables['postpaid']['content']['shipToStore'] = $this->getBlockContent($block);
                // also, according to rules 29 and 30 from Comms_Matrix excel file (VFDED1W3S-65) do not display either activation or prolongation message
                if (isset($blockVariables['postpaid']['content']['activation'])) {
                    unset($blockVariables['postpaid']['content']['activation']);
                } elseif (isset($blockVariables['postpaid']['content']['prolongation'])) {
                    unset($blockVariables['postpaid']['content']['prolongation']);
                }
            }
        } else {
            // else, get all messages from above and concatenate them into a html output
            $blockVariables['postpaid']['postpaidNextStepsContent'] = '';

            if (array_key_exists('content', $blockVariables['postpaid'])) {
                foreach ($blockVariables['postpaid']['content'] as $postpaidCaseMessage) {
                    $blockVariables['postpaid']['postpaidNextStepsContent'] .= $postpaidCaseMessage;
                }
            }
        }
    }

    private function handlePrepaidNextStepsConditions($blockModel,&$stacksArray,&$blockVariables,$package = false)
    {
        // if package is provided, get corresponding message/s
        if ($package) {
            array_push($stacksArray,Dyna_Catalog_Model_Type::TYPE_PREPAID);
            $blockVariables['prepaid']['prepaidStackVisibility'] = '';
            $blockVariables['prepaid']['prepaidIconPath'] = Mage::getBaseDir('skin').'/frontend/vodafone/default/images/de/pdf-prepaid.png';

            /** @var Dyna_Checkout_Helper_Fields $checkoutData */
            $checkoutData = Mage::helper("dyna_checkout/fields");
            $checkoutData->setCheckoutData("save_delivery", $this->quote->getId());

            // if ship to store has been selected on checkout, display its section message
            if ($checkoutData->getFieldValue("delivery[pickup][store]")) {
                $block = $blockModel->load(Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_PREPAID_NEXT_STEPS_SHIP);
                $blockVariables['prepaid']['content']['shipToStore'] = $this->getBlockContent($block);
            } else {
                // otherwise, display prepaid activation message
                $block = $blockModel->load(Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_PREPAID_NEXT_STEPS_ACTIVATION);
                $blockVariables['prepaid']['content']['activation'] = $this->getBlockContent($block);
            }
        } else {
            // else, get all messages from above and concatenate them into a html output
            $blockVariables['prepaid']['prepaidNextSteps'] = '';

            if (array_key_exists('content', $blockVariables['prepaid'])) {
                foreach ($blockVariables['prepaid']['content'] as $prepaidCaseMessage) {
                    $blockVariables['prepaid']['prepaidNextSteps'] .= $prepaidCaseMessage;
                }
            }
        }
    }

    private function handleCableNextStepsConditions($blockModel,&$stacksArray,&$blockVariables,$package = false)
    {
        // if package is provided, get corresponding message/s
        if ($package) {
            array_push($stacksArray,Dyna_Catalog_Model_Type::TYPE_CABLE_INTERNET_PHONE);
            $blockVariables['cable']['cableStackVisibility'] = '';
            $blockVariables['cable']['cableIconPath'] = Mage::getBaseDir('skin').'/frontend/vodafone/default/images/de/pdf-kabel.png';

            if (in_array($package->getSaleType(), Dyna_Catalog_Model_ProcessContext::ILSProcessContexts())) {
                // modify cable package
                $block = $blockModel->load(Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_CABLE_NEXT_STEPS_CHANGE);
                $blockVariables['cable']['content']['modify'] = $this->getBlockContent($block);
            } elseif ($package->getSaleType() == Dyna_Catalog_Model_ProcessContext::MIGCOC) {
                // dsl cancellation (migration to cable)
                array_push($stacksArray,Dyna_Catalog_Model_Type::TYPE_FIXED_DSL);
                $blockVariables['dsl']['dslStackVisibility'] = '';
                $blockVariables['dsl']['dslIconPath'] = Mage::getBaseDir('skin').'/frontend/vodafone/default/images/de/pdf-dsl.png';
                $block = $blockModel->load(Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_DSL_NEXT_STEPS_CANCELLATION);
                $blockVariables['dsl']['content']['cancellation'] = $this->getBlockContent($block);
            } elseif ($package->getSaleType() == Dyna_Catalog_Model_ProcessContext::ACQ) {
                // cable activation
                $block = $blockModel->load(Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_CABLE_NEXT_STEPS_ACTIVATION);
                $blockVariables['cable']['content']['activation'] = $this->getBlockContent($block);
            }

            /** @var Dyna_Checkout_Helper_Fields $checkoutData */
            $checkoutData = Mage::helper("dyna_checkout/fields");
            $checkoutData->setCheckoutData("save_delivery", $this->quote->getId());

            // self install - check if installation service has been checked (yes) on checkout; if not, assume that this is self install scenario
            if (!$checkoutData->getFieldValue("delivery[install_service]") || ($checkoutData->getFieldValue("delivery[install_service]") == 'off')) {
                $block = $blockModel->load(Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_CABLE_NEXT_STEPS_SELF);
                $blockVariables['cable']['content']['selfInstall'] = $this->getBlockContent($block);
            }

            $checkoutData->setCheckoutDataForPackage("save_change_provider", $this->quote->getId());

            // provider change (checkout)
            if ($checkoutData->getFieldValue('provider[change_provider]') == Dyna_Superorder_Helper_Client::NP_CHANGE_PROVIDER_YES) {
                $block = $blockModel->load(Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_CABLE_NEXT_STEPS_PROVIDER_CHANGE);
                $blockVariables['cable']['content']['saveChangeProvider'] = $this->getBlockContent($block);
            }
        } else {
            // else, get all messages from above and concatenate them into a html output
            $blockVariables['cable']['cableNextStepsContent'] = '';

            if (array_key_exists('content', $blockVariables['cable'])) {
                foreach ($blockVariables['cable']['content'] as $cableCaseMessage) {
                    $blockVariables['cable']['cableNextStepsContent'] .= $cableCaseMessage;
                }
            }
        }
    }

    private function handleFixedNextStepsConditions($blockModel,&$stacksArray,&$blockVariables,$package = false)
    {
        // if package is provided, get corresponding message/s
        if ($package) {
            array_push($stacksArray,Dyna_Catalog_Model_Type::TYPE_FIXED_DSL);
            $blockVariables['dsl']['dslStackVisibility'] = '';
            $blockVariables['dsl']['dslIconPath'] = Mage::getBaseDir('skin').'/frontend/vodafone/default/images/de/pdf-dsl.png';

            if (in_array($package->getSaleType(), Dyna_Catalog_Model_ProcessContext::ILSProcessContexts())) {
                // modify dsl package
                $block = $blockModel->load(Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_DSL_NEXT_STEPS_CHANGE);
                $blockVariables['dsl']['content']['modify'] = $this->getBlockContent($block);
            } elseif ($package->getSaleType() == Dyna_Catalog_Model_ProcessContext::ACQ) {
                // dsl activation
                $block = $blockModel->load(Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_DSL_NEXT_STEPS_ACTIVATION);
                $blockVariables['dsl']['content']['activation'] = $this->getBlockContent($block);
            } elseif ($package->getSaleType() == Dyna_Catalog_Model_ProcessContext::MIGCOC) {
                // cable cancellation (migration to dsl)
                array_push($stacksArray,Dyna_Catalog_Model_Type::TYPE_CABLE_INTERNET_PHONE);
                $blockVariables['cable']['cableStackVisibility'] = '';
                $blockVariables['cable']['cableIconPath'] = Mage::getBaseDir('skin').'/frontend/vodafone/default/images/de/pdf-kabel.png';
                $block = $blockModel->load(Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_CABLE_NEXT_STEPS_CANCELLATION);
                $blockVariables['cable']['content']['cancellation'] = $this->getBlockContent($block);
            }
        } else {
            // else, get all messages from above and concatenate them into a html output
            $blockVariables['dsl']['dslNextStepsContent'] = '';

            if (array_key_exists('content', $blockVariables['dsl'])) {
                foreach ($blockVariables['dsl']['content'] as $dslCaseMessage) {
                    $blockVariables['dsl']['dslNextStepsContent'] .= $dslCaseMessage;
                }
            }
        }
    }

    private function handleGigaKombiNextStepsConditions($blockModel,&$stacksArray,&$blockVariables)
    {
        array_push($stacksArray,Dyna_Bundles_Model_BundleRule::TYPE_KOMBI);
        $blockVariables['gigakombi']['gigakombiVisibility'] = '';
        $blockVariables['gigakombi']['gigakombiIconPath'] = Mage::getBaseDir('skin').'/frontend/vodafone/default/images/de/pdf-gigakombi.png';

        $block = $blockModel->load(Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_GIGAKOMBI_NEXT_STEPS_ACTIVATION);
        $blockVariables['gigakombi']['gigakombiNextStepsContent'] = $this->getBlockContent($block);
    }

    private function getNextStepsContent($blockModel,$stacks,$blockVariables)
    {
        // cable block handling
        $blockCable = $blockModel->load(Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_CABLE_NEXT_STEPS);
        $blockVariables['cable']['cableHeadlineVisibility'] = (count(array_unique($stacks)) > 1) ? '' : 'hidden';
        $cableBlockVariables = in_array(Dyna_Catalog_Model_Type::TYPE_CABLE_INTERNET_PHONE,$stacks) ? $blockVariables['cable'] : array('cableStackVisibility' => 'hidden');
        $htmlOutput = $this->getBlockContent($blockCable,$cableBlockVariables);

        // dsl block handling
        $blockDsl = $blockModel->load(Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_DSL_NEXT_STEPS);
        $blockVariables['dsl']['dslHeadlineVisibility'] = (count(array_unique($stacks)) > 1) ? '' : 'hidden';
        $dslBlockVariables = in_array(Dyna_Catalog_Model_Type::TYPE_FIXED_DSL,$stacks) ? $blockVariables['dsl'] : array('dslStackVisibility' => 'hidden');
        $htmlOutput .= $this->getBlockContent($blockDsl,$dslBlockVariables);

        // postpaid block handling
        $blockPostpaid = $blockModel->load(Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_POSTPAID_NEXT_STEPS);
        $blockVariables['postpaid']['postpaidHeadlineVisibility'] = (count(array_unique($stacks)) > 1) ? '' : 'hidden';
        $postpaidBlockVariables = in_array(Dyna_Catalog_Model_Type::TYPE_MOBILE,$stacks) ? $blockVariables['postpaid'] : array('postpaidStackVisibility' => 'hidden');
        $htmlOutput .= $this->getBlockContent($blockPostpaid,$postpaidBlockVariables);

        // prepaid block handling
        $blockPrepaid = $blockModel->load(Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_PREPAID_NEXT_STEPS);
        $blockVariables['prepaid']['prepaidHeadlineVisibility'] = (count(array_unique($stacks)) > 1) ? '' : 'hidden';
        $prepaidBlockVariables = in_array(Dyna_Catalog_Model_Type::TYPE_PREPAID,$stacks) ? $blockVariables['prepaid'] : array('prepaidStackVisibility' => 'hidden');
        $htmlOutput .= $this->getBlockContent($blockPrepaid,$prepaidBlockVariables);

        // gigakombi block handling
        $blockGigakombi = $blockModel->load(Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_GIGAKOMBI_NEXT_STEPS);
        $blockVariables['gigakombi']['gigakombiHeadlineVisibility'] = (count(array_unique($stacks)) > 1) ? '' : 'hidden';
        $gigakombiBlockVariables = in_array(Dyna_Bundles_Model_BundleRule::TYPE_KOMBI,$stacks) ? $blockVariables['gigakombi'] : array('gigakombiVisibility' => 'hidden');
        $htmlOutput .= $this->getBlockContent($blockGigakombi,$gigakombiBlockVariables);

        return (!empty($stacks)) ? '<div class="page-break"></div><h2>So geht es weiter</h2>' . $htmlOutput : $htmlOutput;
    }

    public function getWishDate($packages)
    {
        $customerFields = $this->getCustomerFields('save_delivery');
        $wishDateCable = array_key_exists('delivery[date]', $customerFields)
            ? $customerFields['delivery[date]']
            : null;

        $customerFieldsChangeProvider = $this->getCustomerFields('save_change_provider');
        $wishDateDsl = array_key_exists('provider[dsl][wish_date]', $customerFieldsChangeProvider)
            ? $customerFieldsChangeProvider['provider[dsl][wish_date]']
            : null;

        $datesOutput = '';

        if ($this->hasCable() || $this->hasDsl()) {
            if ($wishDateCable || $wishDateDsl) {
                $datesOutput .= $this->hasCable() ? '<p>Wir haben den ' . $wishDateCable . ' als Wunschtermin für die Lieferung Ihrer Kabel-Hardware notiert.</p>' .
                    ($this->hasDsl() ? '<p>Wir haben den ' . $wishDateDsl . ' als Wunschtermin für die DSL-Anschaltung notiert.</p>' : '') : '';
            } else {
                foreach ($packages as $package) {
                    $date = $customerFields["delivery[pakket][".$package->getPackageId()."][date]"];
                    if (isset($date) && $date != '') {
                        $datesOutput .= $package->isCable() ? '<p>Wir haben den ' . $date . ' als Wunschtermin für die Lieferung Ihrer Kabel-Hardware notiert.</p>' :
                            '<p>Wir haben den ' . $date . ' als Wunschtermin für die DSL-Anschaltung notiert.</p>';
                    }
                }
            }
        }
        return $datesOutput != '' ? $datesOutput : false;
    }

    public function getEndingNote()
    {
        $blockModel = Mage::getModel('cms/block')
            ->setStoreId(Mage::app()->getStore()->getId());
        $block = $blockModel->load(Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_END_NOTES_NOT_BINDING);
        $htmlOutput = $this->getBlockContent($block);

        if ($this->hasMobile()) {
            $block = $blockModel->load(Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_END_NOTES_POSTPAID);
            $htmlOutput .= $this->getBlockContent($block);
        }
        // if MPO (multi package order)
        if (count($this->getPackages()) > 1) {
            $block = $blockModel->load(Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_END_NOTES_DEDICATED);
            $htmlOutput .= $this->getBlockContent($block);
        }

        $blockVariables['endNotesContent'] = $htmlOutput;
        $block = $blockModel->load(Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_END_NOTES);
        return $this->getBlockContent($block,$blockVariables);
    }

    public function getOrderPrices()
    {
        $initialMixmatchSubtotal = $initialMixmatchTax = $initialMixmatchTotal = $initialMixmatchMafSubtotal = $initialMixmatchMafTax = $initialMixmatchMafTotal = 0;
        $orders = $this->superorder->getOrders();

        foreach ($orders as $order) {
            $initialMixmatchSubtotal = (float)$initialMixmatchSubtotal + (float)$order->getInitialMixmatchSubtotal();
            $initialMixmatchTax = (float)$initialMixmatchTax + (float)$order->getInitialMixmatchTax();
            $initialMixmatchTotal = (float)$initialMixmatchTotal + (float)$order->getInitialMixmatchTotal();
            $initialMixmatchMafSubtotal = (float)$initialMixmatchMafSubtotal + (float)$order->getInitialMixmatchMafSubtotal();
            $initialMixmatchMafTax = (float)$initialMixmatchMafTax + (float)$order->getInitialMixmatchMafTax();
            $initialMixmatchMafTotal = (float)$initialMixmatchMafTotal + (float)$order->getInitialMixmatchMafTotal();
        }

        return array(
            'initialMixmatchSubtotal' => $initialMixmatchSubtotal,
            'initialMixmatchMafSubtotal' => $initialMixmatchMafSubtotal,
            'initialMixmatchTax' => $initialMixmatchTax,
            'initialMixmatchMafTax' => $initialMixmatchMafTax,
            'initialMixmatchTotal' => $initialMixmatchTotal,
            'initialMixmatchMafTotal' => $initialMixmatchMafTotal
        );
    }

    public function getAddresses($packages)
    {
        // Get company name and contact person if customer is SOHO
        $customerFields = $this->getCustomerFields();
        $customer = (($customerFields['customer[gender]'] == '1') ? 'Herr' : 'Frau') . ($customerFields['customer[prefix]'] ? ' ' .
                $customerFields['customer[prefix]'] : '') . ' ' . $customerFields['customer[firstname]']
            . ' ' . $customerFields['customer[lastname]'] . '<br>';
        $sohoCustomer = $this->isCustomerSoho() ? ($customerFields['customer[address][company_name]'] ? $customerFields['customer[address][company_name]'] . '<br>' : '') : '';
        $sohoCustomer .= array_key_exists('customer[contact_person][first_name]', $customerFields) && $customerFields['customer[contact_person][first_name]']
            ? ((($customerFields['customer[contact_person][gender]'] == '1') ? 'Herr' : 'Frau')
                . $customerFields['customer[contact_person][first_name]']
                . ' ' . $customerFields['customer[contact_person][last_name]'] . '<br>')
            : $customer;

        $customerFieldsSaveDelivery = $this->getCustomerFields('save_delivery');
        $addresses = array();
        $multipleServiceAddresses = $multipleDeliveryAddresses = 0;
        // Get service/installation addresses
        foreach ($packages as $package) {
            // Get service/installation addresses
            if (($package->isCable() || $package->isFixed()) && !isset($addresses['installation'][$package->getType()])) {
                $packageAddress = $package->getServiceAddressData();
                $addresses['installation'][$package->getType()]['content'] = (($sohoCustomer != '') ? $sohoCustomer : '') . $packageAddress['street'] . ' ' .
                    $packageAddress['houseno'] . '<br>' . $packageAddress['postcode'] . ' ' . $packageAddress['city'];
                $multipleServiceAddresses++;
            }

            // Get delivery data for this package if split delivery or deliver to other address has been selected
            if (isset($customerFieldsSaveDelivery['delivery[deliver][address]'])
                && ($customerFieldsSaveDelivery['delivery[deliver][address]'] == 'split')) {
                // Get delivery addresses from checkout if split delivery has been selected in order to get the other details as well
                $deliveryAddressOutput = ($customerFieldsSaveDelivery['delivery[pakket]['.$package->getPackageId().'][otherAddress][gender]'] == '1') ? 'Herr' : 'Frau';
                $deliveryAddressOutput .= ' ' . $customerFieldsSaveDelivery['delivery[pakket]['.$package->getPackageId().'][otherAddress][prefix]'] .
                    ' ' . $customerFieldsSaveDelivery['delivery[pakket]['.$package->getPackageId().'][otherAddress][firstname]'] .
                    ' ' . $customerFieldsSaveDelivery['delivery[pakket]['.$package->getPackageId().'][otherAddress][lastname]'] . '<br>';
                $deliveryAddressOutput .= $customerFields['customer[address][company_name]'] ? $customerFields['customer[address][company_name]'] . '<br>' : '';
                $deliveryAddressOutput .= $customerFieldsSaveDelivery['delivery[pakket]['.$package->getPackageId().'][otherAddress][street]'] .
                    ' ' . $customerFieldsSaveDelivery['delivery[pakket]['.$package->getPackageId().'][otherAddress][houseno]'] . '<br>';
                $deliveryAddressOutput .= $customerFieldsSaveDelivery['delivery[pakket]['.$package->getPackageId().'][otherAddress][postcode]'] .
                    ' ' . $customerFieldsSaveDelivery['delivery[pakket]['.$package->getPackageId().'][otherAddress][city]'] . '<br>';
            } elseif(isset($customerFieldsSaveDelivery['delivery[deliver][address]'])
                && ($customerFieldsSaveDelivery['delivery[deliver][address]'] == 'other_address')) {
                // Get delivery address from checkout if delivery to other address has been selected, and assign it to each package
                $deliveryAddressOutput = ($customerFieldsSaveDelivery['delivery[other_address][gender]'] == '1') ? 'Herr' : 'Frau';
                $deliveryAddressOutput .= ' ' . $customerFieldsSaveDelivery['delivery[other_address][prefix]'] .
                    ' ' . $customerFieldsSaveDelivery['delivery[other_address][firstname]'] .
                    ' ' . $customerFieldsSaveDelivery['delivery[other_address][lastname]'] . '<br>';
                $deliveryAddressOutput .= $customerFields['customer[address][company_name]'] ? $customerFields['customer[address][company_name]'] . '<br>' : '';
                $deliveryAddressOutput .= $customerFieldsSaveDelivery['delivery[other_address][street]'] .
                    ' ' . $customerFieldsSaveDelivery['delivery[other_address][houseno]'] . '<br>';
                $deliveryAddressOutput .= $customerFieldsSaveDelivery['delivery[other_address][postcode]'] .
                    ' ' . $customerFieldsSaveDelivery['delivery[other_address][city]'] . '<br>';
            } elseif(isset($customerFieldsSaveDelivery['delivery[deliver][address]'])
                && ($customerFieldsSaveDelivery['delivery[deliver][address]'] == 'pickup')) {
                // Get store address from checkout if ship to store has been selected, and assign it to current package
                $deliveryAddressOutput = $customerFieldsSaveDelivery['delivery[pickup_address][shopName1]'] . '<br>';
                $deliveryAddressOutput .= $customerFieldsSaveDelivery['delivery[pickup_address][street]'] .
                    (!empty($customerFieldsSaveDelivery['delivery[pickup_address][houseno]']) ?
                        ' ' . $customerFieldsSaveDelivery['delivery[pickup_address][houseno]'] . '<br>' : '');
                $deliveryAddressOutput .= $customerFieldsSaveDelivery['delivery[pickup_address][postcode]'] .
                    ' ' . $customerFieldsSaveDelivery['delivery[pickup_address][city]'] . '<br>';
            }
            if (isset($deliveryAddressOutput)) {
                $addresses['shipping'][$package->getType()][$package->getPackageId()]['content'] = $deliveryAddressOutput;
                $multipleDeliveryAddresses++;
            }

        }

        // Get delivery/shipping addresses
        $orders = $this->superorder->getOrders();
        $packagesIds = array();
        /** @var Dyna_Checkout_Model_Sales_Order $order */
        foreach ($orders as $order) {
            if ($multipleDeliveryAddresses) {
                // if delivery addresses have been saved above, skip this loop
                break;
            }
            $shippingAddress = Mage::getModel('sales/order_address')->load($order->getShippingAddressId())->getData();
            foreach ($order->getAllItems() as $item) {
                if (in_array(strtolower($item->getPackageType()),
                    array_merge(Dyna_Catalog_Model_Type::getMobilePackages(),Dyna_Catalog_Model_Type::getCablePackages(),Dyna_Catalog_Model_Type::getFixedPackages()))) {
                    $addresses['shipping'][$item->getPackageType()][$item->getPackageId()]['content'] = (($sohoCustomer != '') ? $sohoCustomer : '') . $shippingAddress['street'] .
                        '<br>' . $shippingAddress['postcode'] . ' ' . $shippingAddress['city'];
                    array_push($packagesIds,$item->getPackageId());
                }
            }
            $multipleDeliveryAddresses++;
        }
        // if split delivery has not been selected on checkout but the order has more than 1 package, update delivery counter also
        $multipleDeliveryAddresses = ($multipleDeliveryAddresses == 1) && (count(array_unique($packagesIds)) > 1) ? count(array_unique($packagesIds)) : $multipleDeliveryAddresses;

        $sortedAddresses['cable'] = $sortedAddresses['dsl'] = array();
        $counter = 0;

        if (array_key_exists('installation', $addresses) && is_array($addresses['installation'])) {
            foreach ($addresses['installation'] as $type => $address) {
                if (in_array(strtolower($type), Dyna_Catalog_Model_Type::getCablePackages())) {
                    $stack = ($multipleServiceAddresses > 1) ? 'Kabel ' : '';
                    $sortedAddresses['cable'][$counter]['content'] = $address['content'];
                    $sortedAddresses['cable'][$counter]['header'] = '<h3 class="delivery-address">' . $stack . 'Installationsadresse:</h3>';
                } elseif (in_array(strtolower($type), Dyna_Catalog_Model_Type::getFixedPackages())) {
                    $stack = ($multipleServiceAddresses > 1) ? 'DSL/LTE ' : '';
                    $sortedAddresses['dsl'][$counter]['content'] = $address['content'];
                    $sortedAddresses['dsl'][$counter]['header'] = '<h3 class="delivery-address">' . $stack . 'Installationsadresse:</h3>';
                }
                $counter++;
            }
        }
        // sort installation addresses by following order (cable, dsl)
        $addresses['installation'] = array_merge($sortedAddresses['cable'],$sortedAddresses['dsl']);
        $sortedAddresses['cable'] = $sortedAddresses['dsl'] = $sortedAddresses['mobile'] = array();
        $counter = 0;

        if (is_array($addresses['shipping'])) {
            foreach ($addresses['shipping'] as $type => $packageAddresses) {
                foreach ($packageAddresses as $address) {
                    if (in_array(strtolower($type),Dyna_Catalog_Model_Type::getCablePackages())) {
                        $stack = ($multipleDeliveryAddresses > 1) ? 'Lieferadresse für Ihr Kabel Gerät:' : 'Lieferadresse:';
                        $sortedAddresses['cable'][$counter]['content'] = $address['content'];
                        $sortedAddresses['cable'][$counter]['header'] = '<h3 class="delivery-address">' . $stack . '</h3>';
                    } elseif(in_array(strtolower($type),Dyna_Catalog_Model_Type::getFixedPackages())) {
                        $stack = ($multipleDeliveryAddresses > 1) ? 'Lieferadresse für Ihr DSL/LTE Gerät:' : 'Lieferadresse:';
                        $sortedAddresses['dsl'][$counter]['content'] = $address['content'];
                        $sortedAddresses['dsl'][$counter]['header'] = '<h3 class="delivery-address">' . $stack . '</h3>';
                    } else {
                        $stack = ($multipleDeliveryAddresses > 1) ? 'Lieferadresse für Ihr Mobilfunk Gerät:' : 'Lieferadresse:';
                        $sortedAddresses['mobile'][$counter]['content'] = $address['content'];
                        $sortedAddresses['mobile'][$counter]['header'] = '<h3 class="delivery-address">' . $stack . '</h3>';
                    }
                    $counter++;
                }
            }
        }
        // sort delivery addresses by following order (cable, dsl, mobile)
        $addresses['shipping'] = array_merge($sortedAddresses['cable'],$sortedAddresses['dsl'],$sortedAddresses['mobile']);

        return $this->getAddressesOutput($addresses);
    }

    /**
     * Get addresses as html output for a static block
     * @param $addresses
     * @return string
     */
    private function getAddressesOutput($addresses)
    {
        if ((isset($addresses['installation']) && !empty($addresses['installation']))
            || (isset($addresses['shipping']) && !empty($addresses['shipping']))) {
            // get in $firstArray the array which has a bigger size
            if ((count($addresses['installation']) > count($addresses['shipping']))
                || (count($addresses['installation']) == count($addresses['shipping']))) {
                $firstArray = $addresses['installation'];
                $secondArray = $addresses['shipping'];
            } else {
                $firstArray = $addresses['shipping'];
                $secondArray = $addresses['installation'];
            }

            $htmlOutput = '';
            $counter = 0;
            $skipCurrentInteration = false;

            foreach ($firstArray as $key => $firstAddress) {
                if ($skipCurrentInteration) {
                    // if in last iteration, has been added the address from this iteration, then skip this one and go to the next
                    $skipCurrentInteration = false;
                    continue;
                }
                $htmlOutput .= '<tr><td class="first-column"><div class="adresses">' . $firstAddress['header'] . $firstAddress['content'] . '</div></td>';
                if (isset($secondArray[$counter])) {
                    // if second array has items, take the corresponding one from there
                    $htmlOutput .= '<td><div class="adresses">' . $secondArray[$counter]['header'] . $secondArray[$counter]['content'] . '</div></td></tr>';
                } else {
                    // else compose <td> by using the address from next iteration
                    if (!isset($firstArray[(int)$key + 1])) {
                        // also, if there isn't a next address in $firstArray, add an empty <td> to close the current <tr>
                        $htmlOutput .= '<td><div class="adresses"></div></td></tr>';
                    } else {
                        $htmlOutput .= '<td><div class="adresses">' . $firstArray[(int)$key + 1]['header'] . $firstArray[(int)$key + 1]['content'] . '</div></td></tr>';
                        // skip next iteration if its address has been added now
                        $skipCurrentInteration = true;
                    }
                }
                $counter++;
            }
            return $htmlOutput;
        }
        return '';
    }

    /**
     * This getter will return the html of your static block
     * @return string
     */
    public function getStaticBlock()
    {
        $migrationOrMoveOffnetState = Mage::getSingleton('checkout/cart')->getCheckoutSession()->getInMigrationOrMoveOffnetState();
        $inMigrationState = ($migrationOrMoveOffnetState == Dyna_Catalog_Model_Type::MIGRATION);

        $blockModel = Mage::getModel('cms/block')
            ->setStoreId(Mage::app()->getStore()->getId());

        $isGigaKombi = false;

        foreach ($this->getPackages() as $package) {
            if ($package->isPartOfGigaKombi()) {
                $isGigaKombi = true;
            }
        }

        if ($isGigaKombi && $this->hasCable() && $inMigrationState) {
            $block = $blockModel->load(Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_GIGAKOMBI_CABLE_MIGRATION);
        } else if ($isGigaKombi && $this->hasCable()) {
            $block = $blockModel->load(Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_GIGAKOMBI_CABLE);
        } else if ($isGigaKombi && $this->hasMobile()) {
            $block = $blockModel->load(Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_GIGAKOMBI_MOBILE);
        } else if ($this->hasCable() && $inMigrationState) {
            $block = $blockModel->load(Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_CABLE_MIGRATION);
        } else if ($this->hasCable()) {
            $block = $blockModel->load(Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_CABLE);
        } else if ($this->hasFixedTypePackage()) {
            $block = $blockModel->load(Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_DSL);
        } else {
            $block = $blockModel->load(Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_MOBILE);
        }

        // setting the associative array we send to the filter.
        $variables = array();
        // the array keys are named after the tags in the static block
        $variables['RhombusRedLogoPath'] = Mage::getBaseDir('skin').'/frontend/vodafone/default/images/Rhombus_Red.png';
        $variables['greetings'] = $this->getGreetings();
        $variables['personalInformation'] = $this->getPersonalData();
        $variables['orderId'] = $this->getOrderId();
        $variables['orderDate'] = $this->getOrderDate();
        $variables['addresses'] = $this->getAddresses($this->getPackages());
        $variables['orderTable'] = $this->getLayout()
            ->createBlock('dyna_checkout/call')
            ->init($this->quote, $this->superorder)
            ->setTemplate('checkout/cart/pdf/call_partials/order_table.phtml')
            ->setPackages($this->getPackages())
            ->setOrderPrices($this->getOrderPrices())
            ->toHtml();
        $variables['lostProducts'] = Mage::helper('dyna_checkout/layout')->getLostProducts($this->getPackages());
        $variables['wishDate'] = $this->getWishDate($this->getPackages());
        $variables['nextSteps'] = $this->getNextSteps();
        $variables['endingNote'] = $this->getEndingNote();

        return $this->getBlockContent($block,$variables);
    }

    public function getBlockContent($block,$blockVariables = array())
    {
        // loading the filter which will get the array we created and parse the block content
        /* @var $filter Mage_Cms_Model_Template_Filter */
        $filter = Mage::getModel('cms/template_filter');
        if (!empty($blockVariables)) {
            $filter->setVariables($blockVariables);
        }

        // return the filtered block content.
        return $filter->filter($block->getContent());
    }

    /**
     * Override trait method for SubmitOrder calls where the checkout data cannot be retrieved by quote
     *
     * @param string $step
     * @return array
     */
    public function getCustomerFields($step = "save_customer")
    {
        if ($this->checkoutData !== null) {
            $checkoutData = [];

            foreach ($this->checkoutData as $field) {
                $checkoutData[$field->getFieldName()] = htmlspecialchars($field->getFieldValue());
            }

            return $checkoutData;
        } else {
            return $this->traitGetCustomerFields($step);
        }
    }
}
