<?php

class Dyna_Checkout_Block_Info extends Omnius_Checkout_Block_Cart
{
    const INFO_SUMMARY_KEY = 'info_summary';
    const INFO_SUMMARY_TITLE = 'Info Summary';


    use Dyna_Checkout_Block_Cart_Steps_Common;
    protected $quote = null;

    /**
     * Initialize block parameters
     * @param $quote
     * @return $this
     */
    public function init($quote)
    {
        $this->quote = $quote;
        return $this;
    }

    /**
     * Check if there is at least a package of type DSL in the cart
     *
     * @return bool
     */
    public function hasDsl()
    {
        return Mage::helper('dyna_checkout/cart')->hasPackageOfType(
            $this->quote->getCartPackages(),
            Dyna_Catalog_Model_Type::TYPE_FIXED_DSL
        );
    }

    public function isCustomerSoho()
    {
        return Mage::getSingleton('customer/session')
            ->getCustomer()
            ->getIsSoho();
    }

    public function getGreetings()
    {
        $customer = Mage::getSingleton('dyna_customer/session')->getCustomer();
        if ($this->isCustomerSoho()) {
            $greetings = ($customer->getGender() == '1') ? 'Lieber' : 'Liebe';
            $greetings .= ' ' . (($this->getCustomerLastname() != '') ? $this->getCustomerLastname() : $customer->getLastname());
        } else {
            $firstName = ($this->getCustomerFirstname() != '') ? $this->getCustomerFirstname() : $customer->getFirstname();
            $lastName = ($this->getCustomerLastname() != '') ? $this->getCustomerLastname() : $customer->getLastname();
            $greetings = 'Hallo ' . $firstName . ' ' . $lastName;
        }

        return $greetings;
    }

    public function getShoppingCartId()
    {
        /** @var Mage_Checkout_Model_Session $session */
        $session = Mage::getSingleton('checkout/session');
        return $session->getQuoteId();
    }

    public function checkCallbackDatetime()
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $currentQuote */
        $currentQuote = Mage::getSingleton('checkout/cart')->getQuote();
        return $currentQuote->getCallbackDatetime();
    }

    public function getCallbackDatetime($part)
    {
        $callbackDatetime = $this->checkCallbackDatetime();

        if($callbackDatetime != null) {
            if($part) { // get date
                return date('d.m.Y', strtotime($callbackDatetime));
            } else { // get hour
                return date('H:i', strtotime($callbackDatetime));
            }
        }
    }

    public function getHotlineNumber()
    {
        return Mage::getSingleton('customer/session')->getAgent()->getDealer()->getHotlineNumber();
    }

    public function getStaticBlock()
    {
        /* @var $block Mage_Cms_Model_Block */
        $block = Mage::getModel('cms/block')
            ->setStoreId(Mage::app()->getStore()->getId());

        $array = array();
        $array['greetings'] = $this->getGreetings();
        $array['shoppingCartId'] = $this->getShoppingCartId();
        $array['callbackDatetime'] = $this->checkCallbackDatetime() != null;
        $array['callbackDate'] = $this->getCallbackDatetime(true);
        $array['callbackTime'] = $this->getCallbackDatetime(false);
        $array['hotlineNumber'] = $this->getHotlineNumber();
        $array['hasDsl'] = ($this->hasDsl()) ? true : false;
        $array['hasCable'] = ($this->hasCable()) ? true : false;
        $array['imagePath'] = Mage::getBaseDir('skin').'/frontend/vodafone/default/images/Rhombus_Red.png';
        $array['offer_table'] = $this
            ->getLayout()
            ->createBlock('dyna_checkout/offer')
            ->init($this->quote)
            ->setTemplate('checkout/cart/pdf/offer_partials/offer_table.phtml')
            ->setData(array(
                'isInfoSummary' => true
            ))
            ->toHtml();
        $array['lostProducts'] = Mage::helper('dyna_checkout/layout')->getLostProducts();
        $array['technicalAvailabilityVisibility'] = ($this->hasDsl() || $this->hasCable()) ? '' : 'hidden';

        $block->load('offer_block_' . Dyna_Checkout_Block_Info::INFO_SUMMARY_KEY);

        $filter = Mage::getModel('cms/template_filter');
        /* @var $filter Mage_Cms_Model_Template_Filter */
        $filter->setVariables($array);

        // return the filtered block content.
        return $filter->filter($block->getContent());
    }
}
