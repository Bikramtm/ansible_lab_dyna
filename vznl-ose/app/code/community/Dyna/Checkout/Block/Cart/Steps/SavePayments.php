<?php

class Dyna_Checkout_Block_Cart_Steps_SavePayments extends Omnius_Checkout_Block_Cart
{
    use Dyna_Checkout_Block_Cart_Steps_Common;

    const MAXIMUM_PRICE_FOR_DIRECT_DEBIT = 50;

    private $paymentDebitProduct = null;
    private $paymentTransferProduct = null;


    public function getTotalOTCOnPrepaidPackage()
    {
        $price = 0;
        /** @var Dyna_Package_Model_Package $package */
        foreach ($this->getPackages() as $package) {
            if ($package->isPrepaid()) {
                $quote = $this->getQuote();
                $packageTotals = $quote->calculateTotalsForPackage($package->getPackageId());
                $price = $packageTotals['total'];
                /*$items = $package->getData('items');
                foreach ($items as $item) {
                    $product = $item->getData('product');
                    $itemData = $item->getData();
                    $subType = current($product->getType());

                    if (strtolower($subType) == strtolower(Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_DEVICE)) {
                        $price += $itemData['price_incl_tax'];
                    }
                }*/
            }
        }

        return $price;
    }

    public function hasSimOnly()
    {
        $simOnlyCategory = Mage::getModel('catalog/category')->getCategoryByNamePath( 'Mobile/ALL_SIMONLY_TO', '/' );

        /** @var Dyna_Package_Model_Package $package */
        foreach ($this->getPackages() as $package) {
            $items = $package->getData('items');

            foreach ($items as $item) {
                $catalogProduct = Mage::getModel('catalog/product')->load($item->getProduct()->getId());
                $productCategories = $catalogProduct->getCategoryIds();
                if( $item->getProduct()->isSubscription() ) {
                    if ( ( $package->isPrepaid() && empty( $productCategories ) || in_array( $simOnlyCategory->getId(), $productCategories ) ) ) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public function getPaymentDebit()
    {
        return null;
        /* Deprecated as of VFDED1W3S-2641
        if (!$this->paymentDebitProduct) {
            $this->paymentDebitProduct = Mage::helper('dyna_catalog')->getConfigurableCheckoutProducts('payment_debit');
        }
        return $this->paymentDebitProduct->getFirstItem()->getData();*/
    }

    public function getPaymentTransfer()
    {
        return null;
        /* Deprecated as of VFDED1W3S-2641
        if (!$this->paymentTransferProduct) {
            $this->paymentTransferProduct = Mage::helper('dyna_catalog')->getConfigurableCheckoutProducts('payment_transfer');
        }
        return $this->paymentTransferProduct->getFirstItem()->getData();*/
    }
}
