<?php

/**  */
class Dyna_Checkout_Block_Cart_Steps_SaveCustomer extends Omnius_Checkout_Block_Cart_Steps_SaveCustomer
{
    use Dyna_Checkout_Block_Cart_Steps_Common;
    const SAME_AS_CUSTOMER_ADDRESS = 1;
    const BILLING_ANOTHER_ADDRESS = 2;
    const BILLING_PO_BOX = 3;
    const REUSE_BILLING_ADDRESS = 4;
    const MULTIPLE_BILLING_ADDRESSES = 5;
    const PRIVATE_TELEPHONE_TYPE = 'MOBILE_PHONE';
    const FIXED_TELEPHONE_TYPE = 'FIXED_PHONE';
    const LANDLINE_TELEPHONE_TYPE = 'landline';
    const MOBILE_TELEPHONE_TYPE = 'mobile';
    const ALTERNATIVE_ACCOUNT_HOLDER = 1;

    protected $mobilePackages = [];

    private $billingOnlineProduct = null;
    private $billingPaperProduct = null;

    public function __construct()
    {
        $this->mobilePackages = Dyna_Catalog_Model_Type::getMobilePackages();

        parent::__construct();
    }


    public function getTelephoneType()
    {
        return [
            self::LANDLINE_TELEPHONE_TYPE,
            self::MOBILE_TELEPHONE_TYPE
        ];
    }

    public function getTelephoneTypeMapping() {
        return [
            self::FIXED_TELEPHONE_TYPE => self::LANDLINE_TELEPHONE_TYPE,
            self::PRIVATE_TELEPHONE_TYPE => self::MOBILE_TELEPHONE_TYPE,
        ];
    }

    public function getCommercialRegisterType()
    {
        // todo make this more readable if this is needed in this hardcoded form
        return [
            'A',
            'HRA',
            'HRB',
            'GR',
            'PR',
            'VR',
            'KOE',
            'GEW',
            'PAR',
            'STA',
            'STI',
            'Kein Eintrag',
        ];
    }

    public function getServiceAddressAsString($addDistrict = false)
    {
        $serviceAddressParts = $this->getServiceAddressDetails();

        return  $serviceAddressParts['postcode'] . ' ' . $serviceAddressParts['city']
                . ($addDistrict ? ' ' . $serviceAddressParts['district'] : '') . ', <br/> '
                . $serviceAddressParts['street'] . ' ' . $serviceAddressParts['houseno'] . ' ' . $serviceAddressParts['addition'];
    }

    public function getBillingPaper()
    {
        if (!$this->billingPaperProduct) {
            $this->billingPaperProduct = Mage::helper('dyna_catalog')->getConfigurableCheckoutProducts('billing_paper');
        }
        return $this->billingPaperProduct->getFirstItem()->getData();
    }

    public function getBillingOnline()
    {
        if (!$this->billingOnlineProduct) {
            $this->billingOnlineProduct = Mage::helper('dyna_catalog')->getConfigurableCheckoutProducts('billing_online');
        }
        return $this->billingOnlineProduct->getFirstItem()->getData();
    }

    /**
     * Gathers the process context of all packages from current cart.
     * @return array
     */
    public function getCartProcessContexts()
    {
        $sale_types = [];

        foreach ($this->getCartPackages() as $package) {
            if (!in_array($package['sale_type'], $sale_types)) {
                $sale_types[] = $package['sale_type'];
            }
        }

        return $sale_types;
    }


}