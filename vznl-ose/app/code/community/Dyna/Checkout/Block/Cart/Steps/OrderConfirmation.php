<?php

/**
 * Class Dyna_Checkout_Block_Cart_Steps_OrderConfirmation
 */
class Dyna_Checkout_Block_Cart_Steps_OrderConfirmation extends Omnius_Checkout_Block_Cart
{

    /**
     * Return the latest created superorder number to the frontend
     *
     * @return string
     */
    public function getSuperOrderNumber()
    {
        $id = $this->getCustomerSession()->getLastSuperOrderId();
        $this->getCustomerSession()->unsLastSuperOrderId();

        return $id;
    }
}
