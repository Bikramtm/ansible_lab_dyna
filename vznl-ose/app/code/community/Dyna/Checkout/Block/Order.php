<?php

class Dyna_Checkout_Block_Order extends Mage_Core_Block_Template
{
    use Dyna_Checkout_Block_Cart_Steps_Common;

    /**
     * Returns the superorder based on the specified order number
     * @param $orderNumber
     * @return Dyna_Superorder_Model_Superorder
     */
    public function getSuperOrder()
    {
        /** @var Dyna_Superorder_Model_Superorder $superorder */
        $superorder = Mage::getModel('superorder/superorder')->getCollection()
            ->addFieldToFilter('order_number', $this->getOrderNumber())
            ->getFirstItem();

        return $superorder;
    }

    /**
     * Returns the package type of the order
     * @return mixed
     */
    public function getPackageType()
    {
        $packageType = Mage::helper('dyna_superorder/client')->getOrderPackageType($this->getSuperOrder()->getEntityId());

        return $packageType;
    }

    /**
     * Returns the id of the package associated with the current superorder
     * (for multiple packages, returns the id of the last one)
     * @return mixed
     */
    public function getOrderPackageId()
    {
        return $this->getPackageWithCheckoutData()->getId();
    }

    /**
     * Returns an array of package ids in the current order
     *
     * @return array
     */
    public function getOrderPackageIds()
    {
        $packageIds = [];
        $orderPackages = $this
            ->getSuperOrder()
            ->getOrders()
            ->getFirstItem()
            ->getPackages();

        foreach ($orderPackages as $package) {
            $packageIds[] = $package->getId();
        }

        return $packageIds;
    }

    /**
     * Returns the service address for the current order package
     * @return mixed
     */
    public function getPackageServiceAddress()
    {
        return $this->getPackageWithCheckoutData()->getServiceAddressData();
    }

    /**
     * Returns the package associated with the stored checkout data
     * @return Dyna_Package_Model_Package
     */
    public function getPackageWithCheckoutData()
    {
        $superOrder = $this->getSuperOrder();
        $orderPackages = $superOrder->getOrders()
            ->getFirstItem()
            ->getPackages();

        if ($this->isPartOfBundle($orderPackages)) {
            /** @var Dyna_Package_Model_Package $package */
            foreach ($orderPackages as $package) {
                if (!$package->getData('editing_disabled')) {
                    return $package;
                }
            }
        }

        return sizeof($orderPackages) > 1 ? $orderPackages->getLastItem() : $orderPackages->getFirstItem();
    }

    /**
     * Returns whether the package contains a student discount
     * @return bool
     */
    public function hasStudentDiscount()
    {
        $package = $this->getPackageWithCheckoutData();
        $items = $package->getItems();

        foreach ($items as $item) {
            if ($item->getData('sku') == Dyna_Catalog_Model_Type::getMobileStudentDiscount()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Returns whether the package contains a young discount
     * @return bool
     */
    public function hasYoungDiscount()
    {
        $package = $this->getPackageWithCheckoutData();
        $items = $package->getItems();

        foreach ($items as $item) {
            if ($item->getData('sku') == Dyna_Catalog_Model_Type::getMobileYoungDiscount()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Returns whether the package contains a disabled discount
     * @return bool
     */
    public function hasDisabledDiscount()
    {
        $package = $this->getPackageWithCheckoutData();
        $items = $package->getItems();

        foreach ($items as $item) {
            if ($item->getData('sku') == Dyna_Catalog_Model_Type::getMobileDisabledDiscount()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Returns the customer telephone numbers saved in the first step of the checkout
     * @return array
     */
    public function getCustomerPhoneNumbers()
    {
        $index = 0;
        $phoneNumbers = [];

        /** @var Dyna_Checkout_Helper_Fields $checkoutData */
        $checkoutData = Mage::helper('dyna_checkout/fields');
        $checkoutData->setOrderCheckoutData("save_customer", $this->getOrderPackageIds());

        while ($checkoutData->getFieldValue('customer[contact_details][phone_list]['. $index.'][telephone]')) {
            $phoneNumbers[] = [
                'prefix' => $checkoutData->getFieldValue('customer[contact_details][phone_list]['. $index.'][telephone_prefix]'),
                'phone' => $checkoutData->getFieldValue('customer[contact_details][phone_list]['. $index.'][telephone]'),
                'type' => $checkoutData->getFieldValue('customer[contact_details][phone_list]['. $index.'][telephone_type]')
            ];

            $index++;
        }

        return $phoneNumbers;
    }

    /**
     * Checks if the order has a package containing the selfinstallation service for cable
     * @return bool
     */
    public function hasInstallationServiceProduct()
    {
        $order = $this->getSuperOrder()->getOrders()->getFirstItem();

        foreach ($order->getPackages() as $package) {
            $items = $package->getItems();

            foreach ($items as $item) {
                if ($item->getSku() == Dyna_Catalog_Model_Type::INSTALLATION_SERVICE_CABLE_SKU) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Returns phonebook directory entries value for order details
     * @return string
     */
    public function getDirectoryEntries($packageId,$packageType)
    {
        $directoryEntries = [];

        /** @var Dyna_Checkout_Helper_Fields $checkoutData */
        $checkoutData = Mage::helper('dyna_checkout/fields');
        $checkoutData->setOrderCheckoutData("save_other", $this->getOrderPackageIds());

        if ($checkoutData->getFieldValue('other[extended]['.$packageId.']['.$packageType.'][directories_entry][electronic]') == 'on') {
            $directoryEntries[] = $this->__('Electronic');
        }
        if ($checkoutData->getFieldValue('other[extended]['.$packageId.']['.$packageType.'][directories_entry][printed]') == 'on') {
            $directoryEntries[] = $this->__('Printed');
        }

        return implode(' / ', $directoryEntries);
    }
}
