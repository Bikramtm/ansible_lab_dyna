<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Dyna_Checkout_Block_Offer extends Omnius_Checkout_Block_Cart
{
    const OFFER_GIGAKOMBI_DSL_KEY = 'content_gigacombi_dsl';
    const OFFER_GIGAKOMBI_KABEL_KEY = 'content_gigacombi_kabel';
    const OFFER_GIGAKOMBI_MOBILE_KEY = 'content_gigacombi_mobile';

    const OFFER_GIGAKOMBI_DSL_TITLE = 'Offer GigaCombi DSL';
    const OFFER_GIGAKOMBI_KABEL_TITLE = 'Offer GigaCombi Kabel';
    const OFFER_GIGAKOMBI_MOBILE_TITLE = 'Offer GigaCombi Mobile';

    const OFFER_SUMMARY_DSL_KEY = 'content_summary_dsl';
    const OFFER_SUMMARY_KABEL_KEY = 'content_summary_kabel';
    const OFFER_SUMMARY_MOBILE_KEY = 'content_summary_mobile';

    const OFFER_SUMMARY_DSL_TITLE = 'Offer Summary DSL';
    const OFFER_SUMMARY_KABEL_TITLE = 'Offer Summary Kabel';
    const OFFER_SUMMARY_MOBILE_TITLE = 'Offer Summary Mobile';


    use Dyna_Checkout_Block_Cart_Steps_Common;
    protected $quote = null;

    /**
     * Initialize block parameters
     * @param $quote
     * @return $this
     */
    public function init($quote)
    {
        $this->quote = $quote;
        return $this;
    }

    public function getPackages()
    {
        return $this->quote->getCartPackages();
    }

    /**
     * Get a list of quote products
     * @return array
     */
    public function getProducts()
    {
        $products = array();
        foreach($this->quote->getAllItems() as $item){
            $products[] = $item->getProduct();
        }

        return $products;
    }
    
    /**
     * Check if there is at least a package of type DSL in the cart
     *
     * @return bool
     */
    public function hasDsl()
    {
        return Mage::helper('dyna_checkout/cart')->hasPackageOfType(
            $this->quote->getCartPackages(),
            Dyna_Catalog_Model_Type::TYPE_FIXED_DSL
        );
    }

    /**
     * Check if there is at least a cable package in the cart
     *
     * @return bool
     */
    public function hasCable()
    {
        return Mage::helper('dyna_checkout/cart')->hasPackageOfType(
            $this->quote->getCartPackages(),
            Dyna_Catalog_Model_Type::getCablePackages()
        );
    }
    
    public function isCustomerSoho()
    {
        return Mage::getSingleton('customer/session')
            ->getCustomer()
            ->getIsSoho();
    }
    
    public function getGreetings()
    {
        $customerFields = $this->getCustomerFields();
        $greetings = '';

        if (isset($customerFields['customer[contact_person][last_name]']) && $customerFields['customer[contact_person][last_name]']) {
            $greetings .= $this->isCustomerSoho() ? (($customerFields['customer[contact_person][gender]'] == '1') ? $this->__('Dear Mr') : $this->__('Dear Mrs')) :
                (($customerFields['customer[contact_person][gender]'] == '1') ? 'Lieber' : 'Liebe');
            $greetings .= $this->isCustomerSoho() ? ' ' . $customerFields['customer[contact_person][last_name]']
                : ' ' . $customerFields['customer[contact_person][first_name]'] . ' ' . $customerFields['customer[contact_person][last_name]'];
        } else {
            $greetings .= $this->isCustomerSoho() ? (($customerFields['customer[gender]'] == '1') ? $this->__('Dear Mr') : $this->__('Dear Mrs')) :
                (($customerFields['customer[gender]'] == '1') ? 'Lieber' : 'Liebe');
            $greetings .= $this->isCustomerSoho() ? ' ' . $customerFields['customer[lastname]']
                : ' ' . $customerFields['customer[firstname]'] . ' ' . $customerFields['customer[lastname]'];
        }
        return $greetings;
    }

    public function getPersonalData()
    {
        $customerFields = $this->getCustomerFields();
        $htmlOutput = '';
        $htmlOutput .= $this->isCustomerSoho() ? ($customerFields['customer[address][company_name]'] ? $customerFields['customer[address][company_name]'] . '<br>' : '') : '';
        if ($this->isCustomerSoho() && $this->hasCable() &&
            (isset($customerFields['customer[contact_person][first_name]']) && $customerFields['customer[contact_person][first_name]'] != '')) {
            $htmlOutput .= (($customerFields['customer[contact_person][gender]'] == '1') ? 'Herr' : 'Frau') . $customerFields['customer[contact_person][first_name]'] . ' ' . $customerFields['customer[contact_person][last_name]'] . '<br>';
        } else {
            $htmlOutput .= (($customerFields['customer[gender]'] == '1') ? 'Herr' : 'Frau') . ($customerFields['customer[prefix]'] ? ' ' .
                    $customerFields['customer[prefix]'] : '') . ' ' . $customerFields['customer[firstname]']
                . ' ' . $customerFields['customer[lastname]'] . '<br>';
        }
        $htmlOutput .= $customerFields['customer[address][street]'] . ' ' . $customerFields['customer[address][houseno]'] . ' ' . $customerFields['customer[address][addition]'] . '<br>';
        $htmlOutput .= $customerFields['customer[address][postcode]'] . ' ' . $customerFields['customer[address][city]'];

        return $htmlOutput;
    }
    
    public function getCallback()
    {
        return Mage::helper("dyna_checkout/fields")->getSendOfferData();
    }
    
    public function getComponents()
    {
        $components = array();
        foreach ($this->quote->getCartPackages() as $package) {
            $clientHelper = Mage::helper('dyna_superorder/client'); 
            $components[] = $clientHelper->getComponents($package);
        }
        return $components;
    }
    
    public function getDurationText($product)
    {
        $string = '';
        if($product->getData('initial_period')) {
            $string .= 'Inkl. Tarifaufpreis bei vergünstigtem Handy<br>'.$product->getData('initial_period').' Monate Mindestlaufzeit';
        } elseif($product->getData('minimum_contract_duration')) {
            $string .= $product->getData('minimum_contract_duration').' Monate Mindestlaufzeit';
        } elseif($product->getData('contract_period')) {
            $string .= $product->getData('contract_period').' Monate Mindestlaufzeit';
        }
        return $string;
    }
    
    public function getHotlineNumber()
    {
        return Mage::getSingleton('customer/session')->getAgent()->getDealer()->getHotlineNumber();
    }

    public function getOrderPrices()
    {
        return array(
            'initialMixmatchSubtotal' => $this->quote->getInitialMixmatchSubtotal(),
            'initialMixmatchMafSubtotal' => $this->quote->getInitialMixmatchMafSubtotal(),
            'initialMixmatchTax' => $this->quote->getInitialMixmatchTax(),
            'initialMixmatchMafTax' => $this->quote->getInitialMixmatchMafTax(),
            'initialMixmatchTotal' => $this->quote->getInitialMixmatchTotal(),
            'initialMixmatchMafTotal' => $this->quote->getInitialMixmatchMafTotal(),
            'totalShippingCosts' => $this->quote->getTotalShippingFee()
        );
    }

    public function getAddresses($packages)
    {
        // Get company name and contact person if customer is SOHO
        $customerFields = $this->getCustomerFields();
        $customer = (($customerFields['customer[gender]'] == '1') ? 'Herr' : 'Frau') . ($customerFields['customer[prefix]'] ? ' ' .
                $customerFields['customer[prefix]'] : '') . ' ' . $customerFields['customer[firstname]']
            . ' ' . $customerFields['customer[lastname]'] . '<br>';
        $sohoCustomer = $this->isCustomerSoho() ? ($customerFields['customer[address][company_name]'] ? $customerFields['customer[address][company_name]'] . '<br>' : '') : '';
        $sohoCustomer .= !empty($customerFields['customer[contact_person][first_name]']) ? ((($customerFields['customer[contact_person][gender]'] == '1') ? 'Herr' : 'Frau') .
            $customerFields['customer[contact_person][first_name]'] . ' ' . $customerFields['customer[contact_person][last_name]'] . '<br>') : $customer;

        $addresses = array();
        $multipleServiceAddresses = $multipleDeliveryAddresses = 0;
        // Get service/installation addresses
        foreach ($packages as $package) {
            if (($package->isCable() || $package->isFixed()) && !isset($addresses['installation'][$package->getType()])) {
                $packageAddress = $package->getServiceAddressData();
                $addresses['installation'][$package->getType()]['content'] = (($sohoCustomer != '') ? $sohoCustomer : '') . $packageAddress['street'] . ' ' .
                    $packageAddress['houseno'] . '<br>' . $packageAddress['postcode'] . ' ' . $packageAddress['city'];
                $multipleServiceAddresses++;
            }
        }

        // Get delivery/shipping addresses
        $customerFieldsSaveDelivery = $this->getCustomerFields('save_delivery');
        $shippingData = json_decode($this->quote->getShippingData(), true);
        /** @var Dyna_Checkout_Helper_Delivery $deliveryHelper */
        $deliveryHelper = Mage::helper('dyna_checkout/delivery');

        foreach ($packages as $package) {
            // Get delivery data for this package
            if (isset($customerFieldsSaveDelivery['delivery[deliver][address]'])
                && ($customerFieldsSaveDelivery['delivery[deliver][address]'] == 'split')) {
                // Get delivery addresses from checkout if split delivery has been selected in order to get the other details as well
                $deliveryAddressOutput = ($customerFieldsSaveDelivery['delivery[pakket]['.$package->getPackageId().'][otherAddress][gender]'] == '1') ? 'Herr' : 'Frau';
                $deliveryAddressOutput .= ' ' . $customerFieldsSaveDelivery['delivery[pakket]['.$package->getPackageId().'][otherAddress][prefix]'] .
                    ' ' . $customerFieldsSaveDelivery['delivery[pakket]['.$package->getPackageId().'][otherAddress][firstname]'] .
                    ' ' . $customerFieldsSaveDelivery['delivery[pakket]['.$package->getPackageId().'][otherAddress][lastname]'] . '<br>';
                $deliveryAddressOutput .= $customerFields['customer[address][company_name]'] ? $customerFields['customer[address][company_name]'] . '<br>' : '';
                $deliveryAddressOutput .= $customerFieldsSaveDelivery['delivery[pakket]['.$package->getPackageId().'][otherAddress][street]'] .
                    ' ' . $customerFieldsSaveDelivery['delivery[pakket]['.$package->getPackageId().'][otherAddress][houseno]'] . '<br>';
                $deliveryAddressOutput .= $customerFieldsSaveDelivery['delivery[pakket]['.$package->getPackageId().'][otherAddress][postcode]'] .
                    ' ' . $customerFieldsSaveDelivery['delivery[pakket]['.$package->getPackageId().'][otherAddress][city]'] . '<br>';
            } elseif(isset($customerFieldsSaveDelivery['delivery[deliver][address]'])
                && ($customerFieldsSaveDelivery['delivery[deliver][address]'] == 'other_address')) {
                // Get delivery address from checkout if delivery to other address has been selected, and assign it to each package
                $deliveryAddressOutput = ($customerFieldsSaveDelivery['delivery[other_address][gender]'] == '1') ? 'Herr' : 'Frau';
                $deliveryAddressOutput .= ' ' . $customerFieldsSaveDelivery['delivery[other_address][prefix]'] .
                    ' ' . $customerFieldsSaveDelivery['delivery[other_address][firstname]'] .
                    ' ' . $customerFieldsSaveDelivery['delivery[other_address][lastname]'] . '<br>';
                $deliveryAddressOutput .= (isset($customerFields['customer[address][company_name]']) && $customerFields['customer[address][company_name]']) ? $customerFields['customer[address][company_name]'] . '<br>' : '';
                $deliveryAddressOutput .= $customerFieldsSaveDelivery['delivery[other_address][street]'] .
                    ' ' . $customerFieldsSaveDelivery['delivery[other_address][houseno]'] . '<br>';
                $deliveryAddressOutput .= $customerFieldsSaveDelivery['delivery[other_address][postcode]'] .
                    ' ' . $customerFieldsSaveDelivery['delivery[other_address][city]'] . '<br>';
            } elseif(isset($customerFieldsSaveDelivery['delivery[deliver][address]'])
                && ($customerFieldsSaveDelivery['delivery[deliver][address]'] == 'pickup')) {
                // Get store address from checkout if ship to store has been selected, and assign it to current package
                $deliveryAddressOutput = $customerFieldsSaveDelivery['delivery[pickup_address][shopName1]'] . '<br>';
                $deliveryAddressOutput .= $customerFieldsSaveDelivery['delivery[pickup_address][street]'] .
                    (!empty($customerFieldsSaveDelivery['delivery[pickup_address][houseno]']) ?
                        ' ' . $customerFieldsSaveDelivery['delivery[pickup_address][houseno]'] . '<br>' : '');
                $deliveryAddressOutput .= $customerFieldsSaveDelivery['delivery[pickup_address][postcode]'] .
                    ' ' . $customerFieldsSaveDelivery['delivery[pickup_address][city]'] . '<br>';
            } else {
                // Or get them from quote
                $deliveryData = $deliveryHelper->getDeliveryData($shippingData, $package->getPackageId());
                $deliveryAddressOutput = (($sohoCustomer != '') ? $sohoCustomer : '') . $deliveryData['street'][0] .
                    ' ' . $deliveryData['street'][1] . '<br>' . $deliveryData['postcode'] . ' ' . $deliveryData['city'];
            }

            $addresses['shipping'][$package->getType()][$package->getPackageId()]['content'] = $deliveryAddressOutput;
            $multipleDeliveryAddresses++;
        }

        $sortedAddresses['cable'] = $sortedAddresses['dsl'] = array();
        $counter = 0;

        if (!empty($addresses['installation']) && is_array($addresses['installation'])) {
            foreach ($addresses['installation'] as $type => $address) {
                if (in_array(strtolower($type),Dyna_Catalog_Model_Type::getCablePackages())) {
                    $stack = ($multipleServiceAddresses > 1) ? 'Kabel ' : '';
                    $sortedAddresses['cable'][$counter]['content'] = $address['content'];
                    $sortedAddresses['cable'][$counter]['header'] = '<h3 class="delivery-address">' . $stack . 'Installationsadresse:</h3>';
                } elseif(in_array(strtolower($type),Dyna_Catalog_Model_Type::getFixedPackages())) {
                    $stack = ($multipleServiceAddresses > 1) ? 'DSL/LTE ' : '';
                    $sortedAddresses['dsl'][$counter]['content'] = $address['content'];
                    $sortedAddresses['dsl'][$counter]['header'] = '<h3 class="delivery-address">' . $stack . 'Installationsadresse:</h3>';
                }
                $counter++;
            }
        }
        // sort installation addresses by following order (cable, dsl)
        $addresses['installation'] = array_merge($sortedAddresses['cable'],$sortedAddresses['dsl']);
        $sortedAddresses['cable'] = $sortedAddresses['dsl'] = $sortedAddresses['mobile'] = array();
        $counter = 0;

        if (is_array($addresses['shipping'])) {
            foreach ($addresses['shipping'] as $type => $packageAddresses) {
                foreach ($packageAddresses as $address) {
                    if (in_array(strtolower($type), Dyna_Catalog_Model_Type::getCablePackages())) {
                        $stack = ($multipleDeliveryAddresses > 1) ? 'Lieferadresse für Ihr Kabel Gerät:' : 'Lieferadresse:';
                        $sortedAddresses['cable'][$counter]['content'] = $address['content'];
                        $sortedAddresses['cable'][$counter]['header'] = '<h3 class="delivery-address">' . $stack . '</h3>';
                    } elseif (in_array(strtolower($type), Dyna_Catalog_Model_Type::getFixedPackages())) {
                        $stack = ($multipleDeliveryAddresses > 1) ? 'Lieferadresse für Ihr DSL/LTE Gerät:' : 'Lieferadresse:';
                        $sortedAddresses['dsl'][$counter]['content'] = $address['content'];
                        $sortedAddresses['dsl'][$counter]['header'] = '<h3 class="delivery-address">' . $stack . '</h3>';
                    } else {
                        $stack = ($multipleDeliveryAddresses > 1) ? 'Lieferadresse für Ihr Mobilfunk Gerät:' : 'Lieferadresse:';
                        $sortedAddresses['mobile'][$counter]['content'] = $address['content'];
                        $sortedAddresses['mobile'][$counter]['header'] = '<h3 class="delivery-address">' . $stack . '</h3>';
                    }
                    $counter++;
                }
            }
        }
        // sort delivery addresses by following order (cable, dsl, mobile)
        $addresses['shipping'] = array_merge($sortedAddresses['cable'],$sortedAddresses['dsl'],$sortedAddresses['mobile']);

        return $this->getAddressesOutput($addresses);
    }

    /**
     * Get addresses as html output for a static block
     * @param $addresses
     * @return string
     */
    private function getAddressesOutput($addresses)
    {
        if ((isset($addresses['installation']) && !empty($addresses['installation']))
            || (isset($addresses['shipping']) && !empty($addresses['shipping']))) {
            // get in $firstArray the array which has a bigger size
            if ((count($addresses['installation']) > count($addresses['shipping']))
                || (count($addresses['installation']) == count($addresses['shipping']))) {
                $firstArray = $addresses['installation'];
                $secondArray = $addresses['shipping'];
            } else {
                $firstArray = $addresses['shipping'];
                $secondArray = $addresses['installation'];
            }

            $htmlOutput = '';
            $counter = 0;
            $skipCurrentInteration = false;

            foreach ($firstArray as $key => $firstAddress) {
                if ($skipCurrentInteration) {
                    // if in last iteration, has been added the address from this iteration, then skip this one and go to the next
                    $skipCurrentInteration = false;
                    continue;
                }
                $htmlOutput .= '<tr><td class="first-column"><div class="adresses">' . $firstAddress['header'] . $firstAddress['content'] . '</div></td>';
                if (isset($secondArray[$counter])) {
                    // if second array has items, take the corresponding one from there
                    $htmlOutput .= '<td><div class="adresses">' . $secondArray[$counter]['header'] . $secondArray[$counter]['content'] . '</div></td></tr>';
                } else {
                    // else compose <td> by using the address from next iteration
                    if (!isset($firstArray[(int)$key + 1])) {
                        // also, if there isn't a next address in $firstArray, add an empty <td> to close the current <tr>
                        $htmlOutput .= '<td><div class="adresses"></div></td></tr>';
                    } else {
                        $htmlOutput .= '<td><div class="adresses">' . $firstArray[(int)$key + 1]['header'] . $firstArray[(int)$key + 1]['content'] . '</div></td></tr>';
                        // skip next iteration if its address has been added now
                        $skipCurrentInteration = true;
                    }
                }
                $counter++;
            }
            return $htmlOutput;
        }
        return '';
    }

    public function getStaticBlock()
    {
        /* @var $block Mage_Cms_Model_Block */
        $block = Mage::getModel('cms/block')
            ->setStoreId(Mage::app()->getStore()->getId());

        $isGigaKombi = false;

        foreach ($this->quote->getCartPackages() as $package) {
            if ($package->isPartOfGigaKombi()) {
                $isGigaKombi = true;
            }
        }

        $callback = $this->getCallback();
        $array = array();
        $array['greetings'] = $this->getGreetings();
        $array['personalInformation'] = $this->getPersonalData();
        $array['addresses'] = $this->getAddresses($this->quote->getCartPackages());
        $array['hotlineNumber'] = $this->getHotlineNumber();
        $array['callbackDateTime'] = $callback['date'] != '';
        $array['callbackDate'] = $callback['date'];
        $array['callbackTime'] = $callback['time'];
        $array['hasDsl'] = $this->hasDsl();
        $array['hasCable'] = $this->hasCable();
        $array['isGigacombi'] = $isGigaKombi;
        $array['imagePath'] = Mage::getBaseDir('skin').'/frontend/vodafone/default/images/Rhombus_Red.png';
        $array['offerId'] = $this->quote->getId();
        $array['offerDate'] = Mage::getModel('core/date')->date('d.m.Y');
        $array['lostProducts'] = Mage::helper('dyna_checkout/layout')->getLostProducts($this->quote->getPackages());
        $array['offer_table'] = $this->getLayout()
            ->createBlock('dyna_checkout/call')
            ->init($this->quote)
            ->setTemplate('checkout/cart/pdf/call_partials/order_table.phtml')
            ->setIsOfferSummary(true)
            ->setPackages($this->getPackages())
            ->setOrderPrices($this->getOrderPrices())
            ->setQuoteId($this->quote->getId())
            ->toHtml();

        if($array['isGigacombi'] && $array['hasDsl']) {
            $block->load('offer_block_' . Dyna_Checkout_Block_Offer::OFFER_GIGAKOMBI_DSL_KEY);
        }elseif ($array['isGigacombi'] && $array['hasCable']){
            $block->load('offer_block_' . Dyna_Checkout_Block_Offer::OFFER_GIGAKOMBI_KABEL_KEY);
        }elseif($array['isGigacombi'] && !($array['hasDsl'] || $array['hasCable'])){
            $block->load('offer_block_' . Dyna_Checkout_Block_Offer::OFFER_GIGAKOMBI_MOBILE_KEY);
        }

        elseif( !($array['isGigacombi']) && $array['hasDsl']){
            $block->load('offer_block_' . Dyna_Checkout_Block_Offer::OFFER_SUMMARY_DSL_KEY);
        }elseif( !($array['isGigacombi']) && $array['hasCable']){
            $block->load('offer_block_' . Dyna_Checkout_Block_Offer::OFFER_SUMMARY_KABEL_KEY);
        }else{
            $block->load('offer_block_' . Dyna_Checkout_Block_Offer::OFFER_SUMMARY_MOBILE_KEY);
        }

        $filter = Mage::getModel('cms/template_filter');
        /* @var $filter Mage_Cms_Model_Template_Filter */
        $filter->setVariables($array);

        // return the filtered block content.
        return $filter->filter($block->getContent());
    }
}
