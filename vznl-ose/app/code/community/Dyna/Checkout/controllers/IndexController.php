<?php

require_once Mage::getModuleDir('controllers', 'Omnius_Checkout') . DS . 'IndexController.php';

class Dyna_Checkout_IndexController extends Omnius_Checkout_IndexController
{
    /** @var Dyna_Checkout_Helper_Data $checkoutHelper */
    protected $checkoutHelper;
    /** @var Dyna_Checkout_Helper_Fields */
    protected $checkoutFieldsHelper;

    protected $currentStep = "";

    /**
     * Allowed* actions for this controller
     * (*Verified in pre-dispatch)
     * @var array
     */
    protected $_classActions = array(
        'saveCustomer',
        'saveChangeProvider',
        'saveDeliveryAddress',
        'savePayments',
        'saveBilling',
        'savePayment',
        'saveOther',
        'saveOrderOverview',
        'saveOverview',
        'sendOffer',
        'saveSuperorderId',
        'applyMobileYoungRules'
    );

    /**
     * Check if one type of invoice is selected in a SOHO customer scenario; if not, return a message error
     * @param $packageType
     * @param $checkoutData
     * @return array
     */
    protected function checkSohoInvoice($packageType, $checkoutData)
    {
        if (((strtolower($packageType) == strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE)))
            && $checkoutData['customer']['type'] == 'Soho') {
            if (($checkoutData['billing']['email_invoice'] != 1)
                && ($checkoutData['billing']['post_invoice'] != 1)) {
                return array(
                    'error' => true,
                    'message' => $this->__('One of Billing by e-mail or Invoice by post is a required field')
                );
            }
        }
        return array(
            'error' => false
        );
    }

    /**
     * Validates the data submitted in the Customer/Billing step
     * @param $postData
     * @return array
     */
    protected function validateCustomerData($postData)
    {
        return Mage::helper('dyna_validators/checkout')->validateCustomerData($postData);
    }

    /**
     * Save customer data step in checkout
     */
    public function saveCustomerAction()
    {
        $this->currentStep = "save_customer";
        $request = $this->getRequest();

        if ($request->isPost()) {
            try {
                // Validate there is a quote
                $quote = $this->getQuote();
                $this->throwInvalidQuote($quote);

                //adding sales triple id to the sales quote for validation
                $bundlesHelper = Mage::helper('bundles');
                $salesIdsTriple = $bundlesHelper->getProvisDataByRedSalesId();

                $yellow_blue_ids = $this->getYellowBlueIds($salesIdsTriple);
                $quote->setData('yellow_sales_id', $yellow_blue_ids['yellow_sales_id']);
                $quote->setData('blue_sales_id', $yellow_blue_ids['blue_sales_id']);

                $checkoutData = $request->getPost();
                if (array_key_exists('business', $checkoutData['customer'])) {
                    if (false === empty($checkoutData['customer']['business']['commercial_register'])
                        || $checkoutData['customer']['business']['commercial_register'] == 2
                        || strtolower($checkoutData['customer']['business']['commercial_register'] == 'on')) {
                        $checkoutData['customer']['business']['commercial_register'] = 2;
                    }
                } else {
                    $checkoutData['customer']['business']['commercial_register'] = 1;
                }

                $errors = $this->validateCustomerData($checkoutData);

                $customer = Mage::getSingleton('customer/session')->getCustomer();

                $checkoutLayout = Mage::helper("dyna_checkout/layout");
                $checkoutLayout->setupFormLayout();

                // VFDED1W3S-47
                if ($quote->hasYoungItems()
                    && $checkoutLayout->isFieldAvailable('customer[c1-2b]', null, intval($customer->getIsSoho()), intval($customer->getCommercialRegister()))
                ) {
                    $youngItems = 0;
                    $youbiageItems = 0;

                    //In case of SoHo with Commercial register, there is no dob field for the customer (and no Young items)
                    $customerDob = $checkoutData['customer']['dob'] ?? date('d.m.Y');

                    $currentAge = DateTime::createFromFormat('d.m.Y', $customerDob)
                        ->diff(new DateTime('now'))
                        ->y;

                    /** @var $validatorHelper Dyna_Validators_Helper_Data */
                    $validatorHelper = Mage::helper('dyna_validators');
                    $validYoungAge = $validatorHelper->validateAgeBetween18And28($customerDob);

                    /** @var Dyna_Checkout_Model_Cart $cart */
                    $cart = Mage::getSingleton('checkout/cart');

                    $youbiageSku = Dyna_Catalog_Model_Type::getYouBiageOption();
                    $youbiageProductId = Mage::getModel('dyna_catalog/product')->getIdBySku($youbiageSku);
                    /** @var $youbiageProduct Dyna_Catalog_Model_Product */
                    $youbiageProduct = Mage::getModel('dyna_catalog/product')->load($youbiageProductId);

                    // throw exception if mandatory young option cannot be found in DB
                    $this->throwInvalidYoung($youbiageProduct, $youbiageSku);

                    //VFDED1W3S-2519 Check if minimum age is 7
                    $minNewPrepaidAge = Dyna_Catalog_Model_Type::getNewPrepaidCustomerMinimumAge();
                    $minNewCustomerAge = Dyna_Catalog_Model_Type::getNewCustomerMinimumAge();

                    // count young/youbiage items
                    $countofYoungItems = $this->countYoungItems($youngItems, $youbiageItems, $quote);
                    $youngItems = $countofYoungItems['youngItems'];
                    $youbiageItems = $countofYoungItems['youbiageItems'];

                    $this->checkYoungAgeLimit($youngItems, $currentAge, $checkoutData['customer']['dob'], $errors);

                    $this->validateAndSaveYoungItemsToCart($youngItems, $validYoungAge, $youbiageItems,
                        $quote, $youbiageProductId, $cart, $youbiageProduct, $currentAge, $minNewPrepaidAge,
                        $checkoutData, $errors, $minNewCustomerAge);
                }

                if (strtolower(Dyna_Customer_Model_Client_RetrieveCustomerInfo::PARTY_TYPE_SOHO) != strtolower($checkoutData['customer']['type'])) {
                    $error = $this->_validateCustomerData($checkoutData);
                    if ($error) {
                        $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                            Mage::helper('core')->jsonEncode($error)
                        );
                        return;
                    }
                }

                /** @var Dyna_Customer_Model_Session $customerSession */
                $customerSession = $this->_getCustomerSession();

                // Check if customer is already logged in
                $customer = $customerSession->getCustomer();

                if (!$customer->getId()) {
                    $data = $this->_buildCustomerData($request);

                    $checkCustomer = 'invalid';
                    $checkCustomer = $this->getCheckCustomer($checkCustomer);

                    //for soho the user doesn't specify main customer firstname/lastname so contact names are used
                    $filldata = $this->fillUnfilledData($data);
                    $data['email'] = $filldata['email'];
                    $data['firstname'] = $filldata['firstname'];
                    $data['lastname'] = $filldata['lastname'];
                    $data['gender'] = $filldata['gender'];

                    //save telephone number
                    if (isset($data['contact_details']['phone_list'])) {
                        foreach ($data['contact_details']['phone_list'] as $telephone_number) {
                            $data['telephone'] = $telephone_number['telephone_prefix'] . $telephone_number['telephone'];
                            break;
                        }
                    }

                    // Save all customer and billing address information to the quote
                    // ToDo: Update customer firstname and lastname in order to validate billing for SOHO customer. Currently Dummy firstname and lastname are set
                    $totalsCollectedFlag = $quote->getTotalsCollectedFlag();
                    $quote->setTotalsCollectedFlag(true);
                    $result = $this->getOnepage()->saveBilling($data, null);
                    $quote->setTotalsCollectedFlag($totalsCollectedFlag);

                    $this->throwExceptionForBilling($result);

                } else {
                    // Validating existing customer data
                    $result = $this->getCheckoutFieldsHelper()
                        ->validateCustomerData($checkoutData['customer']);

                    // Validate input data
                    $data = $this->_buildCustomerData($request);

                    //save telephone number
                    if (isset($data['contact_details']['phone_list'])) {
                        foreach ($data['contact_details']['phone_list'] as $telephone_number) {
                            $data['telephone'] = $telephone_number['telephone_prefix'] . $telephone_number['telephone'];
                            break;
                        }
                    }

                    if ($customer->getIsProspect()) {
                        // Also save all customer and billing address information to the quote
                        $totalsCollectedFlag = $quote->getTotalsCollectedFlag();
                        $quote->setTotalsCollectedFlag(true);
                        $result = $this->getOnepage()->saveBilling($data, null);
                        $quote->setTotalsCollectedFlag($totalsCollectedFlag);
                    }
                    /** we need to update customer with data from checkout */
                    $this->updateCustomerData($data, $customer);

                    $emailOverwrite = trim($request->getParam('email_overwrite'));
                    $this->setNewEmailForCustomer($emailOverwrite);
                }

                //In case of error, send validation result directly to frontend
                //Checkout fields need to be updated
                $this->throwErrorsInCustomerSave($result);

                //Fields are valid, proceed to saving
                $checkoutInfo = $this->getCheckoutFieldsHelper()
                    ->buildVariableName($checkoutData);
                $this->saveCheckoutFields($checkoutInfo);

                $quote->setCustomStatus(Omnius_Checkout_Model_Sales_Quote::VALIDATION);

                $this->saveQuoteforCustomer($request, $quote);

                // Set successful response
                $result['error'] = false;
                $result['message'] = "Step data successfully saved";

                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );

            } catch (Mage_Customer_Exception $e) {
                // Set fail response for validations
                $result['error'] = true;
                $result['message'] = $this->checkoutHelper->__("Please correct the following errors");
                $result['fields'] = Mage::helper('core')->jsonDecode($e->getMessage());
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );

                return;
            } catch (Exception $e) {
                // Set fail response for other errors
                $result['error'] = true;
                $result['message'] = $this->__($e->getMessage());
                if ($this->getServiceHelper()->isDev()) {
                    $result['trace'] = $e->getTrace();
                }
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );

                return;
            }

        }
    }

    protected function _validateCustomerData($data)
    {
        $errors = [];
        /**
         * @var Dyna_Validators_Helper_Data $validators
         */
        $validators = Mage::helper('dyna_validators');
        if (!$validators->validateMaxAge($data['customer']['dob'])) {
            $errors = array('customer[dob]' => $this->__("The age limit for the young people benefit is max. 25 years."));
        }

        return $errors;
    }

    /**
     * @param Zend_Controller_Request_Http $request
     * @return mixed
     */
    protected function _buildCustomerData($request)
    {
        //todo: Validating new customer data

        $customerData = $request->getPost('customer');

        // By default set the shipping address same as billing
        $customerData['use_for_shipping'] = 1;

        $addressData = $customerData['address'];

        $addressData['street'] = isset($addressData['street']) ? (is_array($addressData['street']) ? $addressData['street'] : [$addressData['street']]) : [];
        $addressData['country_id'] = 'DE';

        if (strtoupper($customerData['type']) == 'SOHO') {
            if (isset($customerData['business'])) {
                if (false === empty($customerData['business']['commercial_register'])
                    || ($customerData['business']['commercial_register'] == 2)
                    || strtolower($customerData['business']['commercial_register'] == 'on')
                ) {
                    $customerData['business']['commercial_register'] = 2;
                } else {
                    $customerData['business']['commercial_register'] = 1;
                }
            }
            $sohoData = $request->getPost('customer_soho');
            if (false === empty($sohoData)) {
                $customerData = array_merge($customerData, $sohoData);
                if (true === empty($addressData['postcode']) && false === empty($sohoData['address']['postcode'])) {
                    $addressData['postcode'] = $sohoData['address']['postcode'];
                }
                if (true === empty($addressData['city']) && false === empty($sohoData['address']['city'])) {
                    $addressData['city'] = $sohoData['address']['city'];
                }
                if (true === empty($addressData['street'][0]) && false === empty($sohoData['address']['street'])) {
                    $addressData['street'][0] = $sohoData['address']['street'];
                }
                if (true === empty($addressData['houseno']) && false === empty($sohoData['address']['houseno'])) {
                    $addressData['houseno'] = $sohoData['address']['houseno'];
                }
                if (true === empty($addressData['addition']) && false === empty($sohoData['address']['addition'])) {
                    $addressData['addition'] = $sohoData['address']['addition'];
                }
            }
        }

        return $customerData + $addressData;
    }

    /**
     * Returns the checkout cart helper
     * @return Dyna_Checkout_Helper_Fields|Mage_Core_Helper_Abstract
     */
    protected function getCheckoutFieldsHelper()
    {
        if (!$this->checkoutFieldsHelper) {
            $this->checkoutFieldsHelper = Mage::helper('dyna_checkout/fields');
        }

        return $this->checkoutFieldsHelper;
    }

    /**
     * Save current checkout fields on quote checkout
     * @param $checkoutInfo
     * @return $this
     */
    protected function saveCheckoutFields($checkoutInfo)
    {
        // Deleting all previous fields for current step
        Mage::getModel("dyna_checkout/field")
            ->getCollection()
            ->deleteAllFieldsForStop($this->currentStep, $this->getQuote()->getId(), $checkoutInfo);

        //save all fields except the current step
        unset($checkoutInfo['current_step']);
        Mage::helper("dyna_checkout/fields")->addData($this->getQuote(), $this->currentStep, $checkoutInfo);

        return $this;
    }

    /**
     * Validate data submitted during Delivery and Payment
     * @param $postData
     * @return mixed
     */
    protected function validateDeliveryAndPaymentData($postData)
    {
        return Mage::helper('dyna_validators/checkout')->validateDeliveryAndPaymentData($postData);
    }

    /**
     * Persist delivery address data
     */
    public function saveDeliveryAddressAction()
    {
        $this->currentStep = "save_delivery";
        $request = $this->getRequest();
        $doCartSave = false;

        if ($request->isPost()) {
            try {
                $checkoutLayout = Mage::helper("dyna_checkout/layout");
                $checkoutLayout->setupFormLayout();

                /** @var Dyna_Checkout_Model_Sales_Quote $quote */

                $quote = $this->getQuote();

                $this->throwInvalidQuote($quote);

                $validatorHelper = Mage::helper('dyna_validators/checkout');
                $errors = $validatorHelper->validateDeliveryData($request->getPost());

                $this->throwErrorsCommon($errors);

                if ($currentStep = $request->getPost('current_step')) {
                    $quote->setCurrentStep($currentStep);
                }

                $checkoutData = $this->getRequest()->getPost();

                //Fields are valid, proceed to saving
                $checkoutInfo = $this->getCheckoutFieldsHelper()
                    ->buildVariableName($checkoutData);
                $this->saveCheckoutFields($checkoutInfo);

                $delivery = $request->getPost('delivery', array());
                $data = array();
                $this->throwMissingShipmentError($delivery, $checkoutLayout);

                /** @var Dyna_Checkout_Model_Cart $cart */
                $cart = Mage::getSingleton('checkout/cart');
                $res = $this->checkForDeliveryMethodErrors($delivery, $data, $checkoutInfo, $request, $quote,
                    $checkoutLayout);
                $errors = $res['errors'];
                $data = $res['data'];
                $this->throwDeliveryErrorsIfAny($errors);

                $quote->setShippingData(Mage::helper('core')->jsonEncode($data));

                // Add mobile shipping fees to cart depending on chosen delivery
                // @see VFDED1W3S-1894
                $doCartSave = Mage::helper('dyna_checkout/delivery')->calculateShippingFee($quote) || $doCartSave;

                // save cart after shipping fee calculated
                $quote->setSkipCablePostConditions(true);
                $cart->save();

                $result['error'] = false;
                $result['message'] = "Step data successfully saved";
                $result['cart'] = "Step data successfully saved";

                $result['order_overview_content'] = $this
                    ->getLayout()
                    ->createBlock('dyna_checkout/cart_steps_saveOrderOverview')
                    ->setTemplate('checkout/cart/steps/save_order_overview_partials/package_content.phtml')
                    ->toHtml();

                $result['overview_content'] = $this
                    ->getLayout()
                    ->createBlock("dyna_checkout/cart_steps_saveOverview")
                    ->setTemplate("checkout/cart/steps/save_overview_partials/package_content.phtml")
                    ->toHtml();

                $result['payments_content'] = $this
                    ->getLayout()
                    ->createBlock("dyna_checkout/cart_steps_savePayments")
                    ->setTemplate("checkout/cart/steps/save_payments_partials/onetime_payments.phtml")
                    ->toHtml();

                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );

            } catch (Mage_Customer_Exception $e) {
                // Set fail response for validations
                $result['error'] = true;
                $result['message'] = $this->checkoutHelper->__("Please correct the following errors");
                $result['fields'] = Mage::helper('core')->jsonDecode($e->getMessage());
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            } catch (Exception $e) {
                // Set fail response for other errors
                $result['error'] = true;
                $result['message'] = $this->__($e->getMessage());
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            }
        }
    }

    /**
     * Since we receive only the vodafone_ship2store entity_id we have extracted the $deliveryAddress details from the table
     * and we will set the address on session for a consistent use later in the submit
     *
     * @param $checkoutInfo
     * @param $deliveryAddress
     * @param $pickupStoreId
     */
    private function setThePickupAddressOnSession(&$checkoutInfo, $deliveryAddress, $pickupStoreId)
    {
        $checkoutInfo['delivery_pickup_address_id'] = $pickupStoreId;
        $checkoutInfo['delivery[pickup_address][postcode]'] = $deliveryAddress['postcode'];
        $checkoutInfo['delivery[pickup_address][city]'] = $deliveryAddress['city'];
        $checkoutInfo['delivery[pickup_address][street]'] = $deliveryAddress['street'];
        $checkoutInfo['delivery[pickup_address][houseno]'] = $deliveryAddress['houseNo'];
        $checkoutInfo['delivery[pickup_address][shopName1]'] = $deliveryAddress['shopName1'];
        $checkoutInfo['delivery[pickup_address][shopName2]'] = $deliveryAddress['shopName2'];
        $checkoutInfo['delivery[pickup_address][shopName3]'] = $deliveryAddress['shopName3'];
        $this->saveCheckoutFields($checkoutInfo);
    }

    protected function validateChangeProviderData($data)
    {
        return Mage::helper('dyna_validators/checkout')->validateChangeProviderData($data);
    }

    protected function validateMobileYoubiageData($data)
    {
        return Mage::helper('dyna_validators/checkout')->validateMobileYoubiageData($data);
    }

    public function saveChangeProviderAction()
    {
        $this->currentStep = "save_change_provider";
        $request = $this->getRequest();
        if ($request->isPost()) {
            try {
                $checkoutData = $this->getRequest()->getPost();
                if (array_key_exists('provider', $checkoutData)) {
                    foreach ($checkoutData['provider'] as &$provider) {
                        if (isset($provider['change_provider'])) {
                            if ($provider['change_provider'] == 'on' || $provider['change_provider'] == Dyna_Checkout_Block_Cart_Steps_SaveChangeProvider::YES_CHANGE_PROVIDER) {
                                $provider['change_provider'] = Dyna_Checkout_Block_Cart_Steps_SaveChangeProvider::YES_CHANGE_PROVIDER;
                            } else {
                                $provider['change_provider'] = Dyna_Checkout_Block_Cart_Steps_SaveChangeProvider::NO_CHANGE_PROVIDER;
                            }
                        }
                    }
                }

                $errors = $this->validateChangeProviderData($checkoutData);
                $errors += $this->validateMobileYoubiageData($checkoutData);
                $errors += $this->validateOtherData($checkoutData);

                $this->throwErrorsCommon($errors);

                //Fields are valid, proceed to saving
                $checkoutInfo = $this->getCheckoutFieldsHelper()
                    ->buildVariableName($checkoutData);
                $this->saveCheckoutFields($checkoutInfo);

                /** @var Dyna_Customer_Model_Session $currentTechnician */
                $currentTechnician = Mage::getSingleton('customer/session')->getCustomer()->getTechnicianNeeded();

                if (array_key_exists('provider', $checkoutData)) {
                    foreach ($checkoutData['provider'] as $type => $providerData) {
                        //Add configurable checkout products for number porting
                        $this->addConfigurableCheckoutProductsForNumberPorting($providerData, $type);

                        //Add configurable checkout products for technician needed
                        $this->addConfigurableCheckoutProductsForTechnicianNeeded($providerData, $type,
                            $currentTechnician);
                    }
                }

                /** @var Dyna_Checkout_Helper_Data $checkoutHelper */
                $checkoutHelper = Mage::helper('dyna_checkout');

                // Remove phone_book_entry1, phone_book_entry2, phone_book_entry3 configurable checkout products
                $checkoutHelper->removeConfigurableCheckoutProducts(array(
                    'phone_book_entry1',
                    'phone_book_entry2',
                    'phone_book_entry3'
                ));

                // Phonebook Entry handling for configurable checkout products
                if (isset($checkoutData['other'])) {
                    foreach ($checkoutData['other'] as $type => $otherData) {

                        if($type == "extended") {
                            foreach($otherData as $other) {
                                if (isset($other) && !empty($other)) {
                                    foreach ($other as $packageType => $phonebookDetails) {
                                        $this->addConfigurableCheckoutProductsForPhonebookEntry(
                                            $phonebookDetails,
                                            $packageType);
                                    }
                                }
                            }
                        }
                    }
                }

                /** @var Dyna_Checkout_Model_Sales_Quote $quote */
                $quote = $this->getQuote();

                $quote->setSkipCablePostConditions(false);
                //region Execute cable rules artifact with the corresponding TechnicianNeeded flag per package
                /** @var Dyna_Configurator_Helper_Rules $rulesHelper */
                $rulesHelper = Mage::helper('dyna_configurator/rules');
                $rulesHelper->setPostIterations($rulesHelper::MAX_ITERATIONS);
                $rulesHelper->executeCablePostRules($quote, $type, false);

                //Save current step on quote
                $quote
                    ->setCurrentStep($this->currentStep)
                    ->save();

                $result['error'] = false;
                $result['message'] = "Step data successfully saved";
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );

            } catch (Mage_Customer_Exception $e) {
                // Set fail response for validations
                $result['error'] = true;
                $result['message'] = $this->checkoutHelper->__("Please correct the following errors");
                $result['fields'] = Mage::helper('core')->jsonDecode($e->getMessage());
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            } catch (Exception $e) {
                // Set fail response for other errors
                $result['error'] = true;
                $result['message'] = $this->__($e->getMessage());
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
            }

            return;
        }
    }

    /**
     * Save billing data step in checkout
     */
    public function saveBillingAction()
    {
        $this->currentStep = "save_billing";
        $request = $this->getRequest();
        if ($request->isPost()) {
            try {
                $checkoutData = $this->getRequest()->getPost();

                //Fields are valid, proceed to saving
                $checkoutInfo = $this->getCheckoutFieldsHelper()
                    ->buildVariableName($checkoutData);
                $this->saveCheckoutFields($checkoutInfo);

                //Save current step on quote
                $this->getQuote()
                    ->setCurrentStep($checkoutData['current_step'])
                    ->save();

                $result['error'] = false;
                $result['message'] = "Step data successfully saved";
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
            } catch (Exception $e) {
                // Set fail response for other errors
                $result['error'] = true;
                $result['message'] = $this->__($e->getMessage());
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
            }

            return;
        }
    }

    /**
     * Payments section
     */
    public function savePaymentsAction()
    {
        $this->currentStep = "save_payments";
        $request = $this->getRequest();
        $doCartSave = false;

        if ($request->isPost()) {
            try {
                $checkoutLayout = Mage::helper("dyna_checkout/layout");
                $checkoutLayout->setupFormLayout();

                /** @var Dyna_Checkout_Model_Sales_Quote $quote */

                $quote = $this->getQuote();

                $this->throwInvalidQuote($quote);

                if ($currentStep = $request->getPost('current_step')) {
                    $quote->setCurrentStep($currentStep);
                }

                $checkoutData = $this->getRequest()->getPost();

                //Fields are valid, proceed to saving
                $checkoutInfo = $this->getCheckoutFieldsHelper()
                    ->buildVariableName($checkoutData);
                $this->saveCheckoutFields($checkoutInfo);


                $errors = array();

                $paymentData = $request->getPost('payment', array());
                $paymentMethod = $this->getPaymentMethods($paymentData);

                /** @var Dyna_Checkout_Model_Cart $cart */
                $cart = Mage::getSingleton('checkout/cart');

                $res = $this->getPaymentMethodInfo($paymentMethod, $paymentData, $quote, $cart, $doCartSave, $errors);
                $payment = $res['payment'];
                $quote = $res['quote'];
                $cart = $res['cart'];
                $errors = $res['errors'];
                $doCartSave = $res['doCartSave'];

                //Add configurable checkout products for Billing paper/online
                /** @var Dyna_Checkout_Model_Cart $cart */
                $cart = Mage::getSingleton('checkout/cart');
                $this->addCheckoutProductsForBilling($checkoutData);

                $data['payment'] = $payment;

                // Validate each delivery address has same payment method
                $errors += $this->_validateSplitPaymentAndDelivery($data);

                $this->throwDeliveryErrorsIfAny($errors);


                $shippingData = Mage::helper('core')->jsonDecode($quote->getShippingData());
                $data = $data + $shippingData;
                $quote->setShippingData(Mage::helper('core')->jsonEncode($data));


                // save cart after shipping fee calculated
                $quote->setSkipCablePostConditions(true);
                $quote->save();
                $cart->save();

                // Set successful response
                $result['error'] = false;
                $result['message'] = "Step data successfully saved";
                $result['cart'] = "Step data successfully saved";

                $result['order_overview_content'] = $this
                    ->getLayout()
                    ->createBlock('dyna_checkout/cart_steps_saveOrderOverview')
                    ->setTemplate('checkout/cart/steps/save_order_overview_partials/package_content.phtml')
                    ->toHtml();

                $result['overview_content'] = $this
                    ->getLayout()
                    ->createBlock("dyna_checkout/cart_steps_saveOverview")
                    ->setTemplate("checkout/cart/steps/save_overview_partials/package_content.phtml")
                    ->toHtml();

                $result['payments_content'] = $this
                    ->getLayout()
                    ->createBlock("dyna_checkout/cart_steps_savePayments")
                    ->setTemplate("checkout/cart/steps/save_payments_partials/onetime_payments.phtml")
                    ->toHtml();

                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );

            } catch (Mage_Customer_Exception $e) {
                // Set fail response for validations
                $result['error'] = true;
                $result['message'] = $this->checkoutHelper->__("Please correct the following errors");
                $result['fields'] = Mage::helper('core')->jsonDecode($e->getMessage());
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            } catch (Exception $e) {
                // Set fail response for other errors
                $result['error'] = true;
                $result['message'] = $this->__($e->getMessage());
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            }
        }
    }

    /**
     * Save payment data step in checkout
     */
    public function savePaymentAction()
    {
        $this->currentStep = "save_payment";
        $request = $this->getRequest();
        if ($request->isPost()) {
            try {
                $checkoutData = $this->getRequest()->getPost();

                //Fields are valid, proceed to saving
                $checkoutInfo = $this->getCheckoutFieldsHelper()
                    ->buildVariableName($checkoutData);
                $this->saveCheckoutFields($checkoutInfo);

                //Save current step on quote
                $this->getQuote()
                    ->setCurrentStep($checkoutData['current_step'])
                    ->save();

                $result['error'] = false;
                $result['message'] = "Step data successfully saved";
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
            } catch (Exception $e) {
                // Set fail response for other errors
                $result['error'] = true;
                $result['message'] = $this->__($e->getMessage());
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
            }

            return;
        }
    }

    protected function validateOtherData($otherData)
    {
        return Mage::helper('dyna_validators/checkout')->validateOtherData($otherData);
    }

    /**
     * Save other data step in checkout
     */
    public function saveOtherAction()
    {
        $this->currentStep = "save_other";
        $request = $this->getRequest();
        if ($request->isPost()) {
            try {
                $checkoutData = $this->getRequest()->getPost();

                $errors = $this->validateOtherData($checkoutData);
                if (!empty($errors)) {
                    throw new Mage_Customer_Exception(Mage::helper('core')->jsonEncode($errors));
                }

                //Fields are valid, proceed to saving
                $checkoutInfo = $this->getCheckoutFieldsHelper()
                    ->buildVariableName($checkoutData);
                $this->saveCheckoutFields($checkoutInfo);

                //Save current step on quote
                if (!empty($checkoutData['current_step'])) {
                    $this->getQuote()
                        ->setCurrentStep($checkoutData['current_step'])
                        ->save();
                }

                $result['error'] = false;
                $result['message'] = "Step data successfully saved";
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
            } catch (Mage_Customer_Exception $e) {
                // Set fail response for validations
                $result['error'] = true;
                $result['message'] = $this->checkoutHelper->__("Please correct the following errors");
                $result['fields'] = Mage::helper('core')->jsonDecode($e->getMessage());
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            } catch (Exception $e) {
                // Set fail response for other errors
                $result['error'] = true;
                $result['message'] = $this->__($e->getMessage());
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
            }

            return;
        }
    }

    /**
     * Save order overview data step in checkout
     */
    public function saveOrderOverviewAction()
    {
        $this->currentStep = "save_order_overview";
        $request = $this->getRequest();
        if ($request->isPost()) {
            try {
                $checkoutData = $this->getRequest()->getPost();

                //Fields are valid, proceed to saving
                $checkoutInfo = $this->getCheckoutFieldsHelper()
                    ->buildVariableName($checkoutData);
                $this->saveCheckoutFields($checkoutInfo);

                //Save current step on quote
                if (!empty($checkoutData['current_step'])) {
                    $this->getQuote()
                        ->setCurrentStep($checkoutData['current_step'])
                        ->save();
                }

                $customerSession = Mage::getSingleton('customer/session');
                /** @var Dyna_Customer_Model_Customer $customer */
                $customer = $customerSession->getCustomer();

                // If available, set customerGlobalId on quote
                if (!empty($customer->getCustomerGlobalId())) {
                    $this->getQuote()
                        ->setCustomerGlobalId($customer->getCustomerGlobalId())
                        ->save();
                }

                $result['error'] = false;
                $result['message'] = "Step data successfully saved";
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
            } catch (Exception $e) {
                // Set fail response for other errors
                $result['error'] = true;
                $result['message'] = $this->__($e->getMessage());
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
            }

            return;
        }
    }

    /**
     * Send offer based on checkout data
     */
    public function sendOfferAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            try {
                /** @var Dyna_Customer_Model_Customer $customer */
                $customer = Mage::getSingleton('customer/session')->getCustomer();
                $isSoho = $customer->getIsSoho();
                /** @var Dyna_Checkout_Model_Sales_Quote $quote */
                $quote = $this->getQuote();
                foreach ($quote->getCartPackages(true) as $package) {
                    $package->buildChecksum();
                }
                $quote->setIsOffer(1)->save();

                //if campaign offer, report to UCT
                $uctParams = $this->_getCustomerSession()->getUctParams();
                if (isset($uctParams["transactionId"])) {
                    $quote->setTransactionId($uctParams["transactionId"]);
                }
                Mage::helper('dyna_checkout/data')->sendUCTReportProgress(null, $quote);

                // check if the customer is already logged in
                if (!$customer->isCustomerLoggedIn()) {
                    //create the customer
                    $customer = $this->createOfferPersistCustomer($isSoho);
                    //set the persist customer on the quote and save it
                    $quote->setCustomer($customer);
                }

                /** @var Dyna_Checkout_Helper_Cart $cartDataHelper */
                $cartDataHelper = Mage::helper('dyna_checkout/cart');
                $cartDataHelper->updatePreviousCarts($customer);

                $postData = $request->getPost();
                $pushPhone = trim($postData['pushphone']);

                // make sure cart status sets to null as updatePreviousCarts might have been set it to old (offer resulted from another offer flow)
                $quote->setCartStatus(null)->setQuoteParentId(null)->setIsOffer(1);
                if ($pushPhone) {
                    $quote->setOfferMsisdn($pushPhone);
                }
                $quote->save();
                $client = Mage::getModel("dyna_superorder/client_sendDocumentClient");
                Mage::helper("dyna_checkout/fields")->addData($quote, 'send_document', $request->getPost());
                //execute send document with the offer
                $client->executeSendDocument($quote, 'Offer');
                //result
                $result['error'] = false;
                $result['message'] = $this->__('Offer send successful');


                // in case a phone is specified for push notifications
                if ($pushPhone) {
                    $pushResponse = $this->executeSendPushNotification($pushPhone, $this->getQuote()->getId());

                    if ($pushResponse['error']) {
                        $result['error'] = true;
                        $result['message'] = $pushResponse['message'];
                    }
                }

                $this->checkoutHelper->processNotesAndPickup($request);
                // save offer to redis
                $projection = Mage::helper('dyna_myvfapi')->buildProjectionFromQuote($this->getQuote(), $pushPhone);
                Mage::getModel('dyna_myvfapi/service_projection',
                    ['credisClient' => Mage::helper('dyna_myvfapi/credis')->buildCredisClient()])->create($projection);

                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );

            } catch (Exception $e) {
                // Set fail response for other errors
                $result['error'] = true;
                $result['message'] = $this->__($e->getMessage());
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
            }
        }
    }

    /**
     * If no customer logged in, create a new one
     * @param bool $isSoho
     * @return Dyna_Customer_Model_Customer|Mage_Core_Model_Abstract
     */
    protected function createOfferPersistCustomer($isSoho = false)
    {
        $checkoutData = [];
        /** @var Dyna_Checkout_Model_Resource_Field_Collection $checkoutFields */
        $checkoutFields = Mage::getModel("dyna_checkout/field")->getCollection()
            ->addFieldToFilter("quote_id", ["eq" => $this->getQuote()->getId()]);
        //get checkout data of the customer
        foreach ($checkoutFields as $field) {
            $checkoutData[$field->getFieldName()] = $field->getFieldValue();
        }

        /** @var Dyna_Checkout_Helper_Cart $cartDataHelper */
        $cartDataHelper = Mage::helper('dyna_checkout/cart');
        $vars['email_send'] = $this->getRequest()->getPost('email');
        $vars['firstname'] = $checkoutData['customer[firstname]'];
        $vars['lastname'] = $checkoutData['customer[lastname]'];
        // for Mobile Prepaid we have the possibility of Ansprechpartner which doesn't have a separate local_area_code field: customer[contact_person_details][0][telephone]
        // so we remove the leading 0 (trunk number) if the field has 11 digits (the standard length of a DE local_area_code+telephone_number
        $customerTelephone = $checkoutData['customer[contact_details][phone_list][0][telephone]'];
        $contactPersonTelephone = ($checkoutData['customer[contact_person_details][0][telephone_prefix]'] ?? '') . ($checkoutData['customer[contact_person_details][0][telephone]'] ?? '');
        $vars['telephone'] = $customerTelephone ?:
            (strlen($contactPersonTelephone) == 11 && substr($contactPersonTelephone, 0, 1) == '0' ?
                substr($contactPersonTelephone, 1) : $contactPersonTelephone);
        $vars['prefix'] = $checkoutData['customer[contact_details][phone_list][0][telephone_prefix]'];
        $vars['additional_telephone'] = (substr($vars['prefix'], 0, 1) === '0' ? substr($vars['prefix'], 1) : $vars['prefix']) . $vars['telephone'];
        $vars['companyname'] = $checkoutData['billing[other_address][company_name]'] ?? '';
        $vars['dob'] = $checkoutData['customer[dob]'];
        $vars['guestCustomerIsSoho'] = $isSoho;

        //search for a customer with same data
        $customer = Mage::getModel('customer/customer');
        $customer = $customer->getCollection()
            ->addAttributeToFilter('is_prospect', true)
            ->addAttributeToFilter('additional_email', $vars['email_send'])
            ->addAttributeToFilter('firstname', $vars['firstname'])
            ->addAttributeToFilter('lastname', $vars['lastname'])
            ->addAttributeToFilter('additional_telephone', $vars['additional_telephone'])
            ->addAttributeToFilter('additional_telephone', $customer::COUNTRY_CODE . $vars['additional_telephone'])
            ->addAttributeToFilter('additional_telephone',
                $customer::SHORT_COUNTRY_CODE . $vars['additional_telephone'])
            ->addAttributeToFilter('phone_number', $vars['telephone'])
            ->addAttributeToFilter('phone_local_area_code', $vars['prefix'])
            ->addAttributeToFilter('is_business', $vars['guestCustomerIsSoho'])
            ->addAttributeToFilter('dob', date("Y-m-d H:i:s", strtotime($vars['dob'])));

        $customer->getFirstItem();
        //check if a customer with same data was found and return it
        if (count($customer) > 0 && isset($customer->getData()[0]['entity_id'])) {
            $customerResponse = Mage::getModel('customer/customer')->load($customer->getData()[0]['entity_id']);
        } else {
            //create prospect customer
            $customerResponse = $cartDataHelper->createDeProspectFromCart($vars);
        }
        //return customer
        return $customerResponse;
    }

    /**
     * @return Zend_Controller_Response_Abstract
     */
    public function saveSuperorderIdAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }
        $response = [];

        /** @var Dyna_Checkout_Helper_Data $checkoputHelper */
        $checkoutHelper = Mage::helper('dyna_checkout');

        // if sales ids triple is not existing, display corresponding error message when trying to submit the order
        $salesIdsTriple = $checkoutHelper->checkSalesIdTriple();
        if (isset($salesIdsTriple['error'])) {
            $response['error'] = true;
            $response['salesIdError'] = true;
            $response['message'] = $salesIdsTriple['message'];

            $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                Mage::helper('core')->jsonEncode($response)
            );
            return;
        }

        $checkoutData = $this->getRequest()->getPost();

        $errors = Mage::helper('dyna_validators/checkout')->validateVoicelogData($checkoutData);
        if (!empty($errors)) {
            $response['error'] = true;
            $response['termsAndConditionsError'] = true;
            $response['message'] = '';
            foreach ($errors as $error) {
                $response['message'] .= $error . '</br>';
            }

            $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                Mage::helper('core')->jsonEncode($response)
            );
            return;
        }

        //Fields are valid, proceed to saving
        $checkoutInfo = $this->getCheckoutFieldsHelper()
            ->buildVariableName($checkoutData);
        $this->saveCheckoutFields($checkoutInfo);

        try {
            $superOrder = Mage::getModel('superorder/superorder')->createNewSuperorder();
            $response = [
                'addresses' => [],
                'error' => false,
                'message' => 'Step data successfully saved',
                'orders' => $superOrder->getOrderNumber(),
                'order_id' => $superOrder->getId()
            ];
        } catch (Exception $e) {
            $response['error'] = true;
            $response['message'] = $this->__($e->getMessage());
        }

        return $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
            Mage::helper('core')->jsonEncode($response)
        );

    }

    protected function getJobSessionData()
    {
        $data = [
            'checkout' => Mage::getSingleton('checkout/session')->getData(),
            'customer' => Mage::getSingleton('customer/session')->getData(),
            'address' => Mage::getSingleton('dyna_address/storage')->getData(),
        ];

        unset($data['customer']['agent']);
        $data = array_map('serialize', $data);
        return array_map('base64_encode', $data);
    }

    /**
     * Performs main saving action
     */
    public function saveOverviewAction()
    {
        $useJobQueue = Mage::getStoreConfig('omnius_service/process_order/use_amqp');
        if ($useJobQueue) {
            return $this->jobQueueSubmitOrder();
        } else {
            return $this->submitOrder();
        }
    }

    protected function submitOrder()
    {
        $result = array();

        $request = $this->getRequest();
        if ($request->isPost()) {

            try {
                $this->checkoutHelper->processNotesAndPickup($request);
                $superOrderId = $request->get('order_id');

                $quote = $this->getQuote();

                if (!$quote->getId()) {
                    throw new Exception($this->__('Invalid quote'));
                }

                // Create the order and place the ESB Order id on the response
                try {
                    /** @var Omnius_Superorder_Model_Superorder $superOrder */
                    $superOrder = $this->createOrder($superOrderId);
                } catch (LogicException $e) {

                    $result['error'] = false;
                    $result['message'] = "Step data successfully saved";
                    $result['polling_id'] = 'undefined';
                    $result['orders'] = $this->checkoutHelper->__('Risk incident recorded, order was not created');
                    $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                        Mage::helper('core')->jsonEncode($result)
                    );
                    return;
                }
                // createOrder failed and already set the response, return early
                if (!$superOrder) {
                    return;
                }

                $result['orders'] = $superOrder->getOrderNumber();
                $result['order_id'] = $superOrder->getId();

                // Append the address to the response in order to have them in the credit check step
                $result['addresses'] = $this->_buildCreditCheckAddresses($quote);

                if ($request->getPost('current_step')) {
                    $quote->setCurrentStep($request->getPost('current_step'));
                }
                $quote->save();
                $result['error'] = false;
                $result['message'] = "Step data successfully saved";

                // Mark the quote as not a cart
                if ($quote->getQuoteParentId()) {
                    $parentQuote = Mage::getModel('sales/quote')->load($quote->getQuoteParentId());
                    if ($parentQuote->getId()) {
                        $parentQuote->setCartStatus(Dyna_Checkout_Model_Sales_Quote::CART_STATUS_OLD)
                            ->save();
                    }
                }

                $customerHelper = Mage::helper('dyna_customer');
                $customerHelper->unloadCustomer();
                // Unload UCT data
                $this->_getCustomerSession()->unsUctParams();

                $request->setPost('superorderNumber', $superOrder->getOrderNumber());
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            } catch (Exception $e) {
                Mage::logException($e);
                // Set fail response for other errors
                $result['error'] = true;
                $result['message'] = $this->__($e->getMessage());
                //unset data if failure
                $this->unsetStoredDataOnFailure();

                Mage::getSingleton('core/logger')->logException($e);
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            }
        }
    }

    protected function jobQueueSubmitOrder()
    {
        $result = array();

        $request = $this->getRequest();
        if ($request->isPost()) {

            try {
                $this->checkoutHelper->processNotesAndPickup($request);
                $superOrderId = $request->get('order_id');

                $quote = $this->getQuote();

                if (!$quote->getId()) {
                    throw new Exception($this->__('Invalid quote'));
                }

                $jobSessionData = $this->getJobSessionData();

                $jobData = array_merge($jobSessionData, [
                    'quote_id' => $quote->getId(),
                    'superorder_id' => $superOrderId,
                    'agent_id' => Mage::getSingleton('customer/session')->getAgent()->getId(),
                ]);


                $job = Mage::getModel('dyna_job/jobs_submitOrder');
                $job->setData($jobData);
                $helper = Mage::helper('dyna_job');
                $publisher = $helper->buildPublisher();
                $publisher->publish($job);

                $customerHelper = Mage::helper('dyna_customer');
                $customerHelper->clearCustomerInfo();
                $customerHelper->unloadCustomer();
                // Unload UCT data
                $this->_getCustomerSession()->unsUctParams();

                $request->setPost('superorderNumber', $superOrderId);
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            } catch (Exception $e) {
                Mage::logException($e);
                // Set fail response for other errors
                $result['error'] = true;
                $result['message'] = $this->__($e->getMessage());

                /** @var Dyna_Superorder_Model_Superorder $superOrder */
                $superOrder = Mage::getModel('superorder/superorder')->load($request->get('order_id'));
                if ($superOrder) {
                    $superOrder->setOrderStatus(Dyna_Superorder_Model_Superorder::SO_COMPLETED_FAILED);
                    $superOrder->save();
                }
                //unset data if failure
                $this->unsetStoredDataOnFailure();

                Mage::getSingleton('core/logger')->logException($e);
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            }
        }
    }

    protected function omniusCreateOrder($superorderId)
    {
        $checkout = $this->getCheckout();
        $checkout->setCollectRatesFlag(true);
        $quote = $this->getQuote();
        if ($quote->getCustomer()->getId() && !$quote->getCustomer()->getIsProspect()) {
            $customer = Mage::getModel('customer/customer')->load($quote->getCustomer()->getId());
            if ($this->_getCustomerSession()->getNewEmail()) {
                // When the email was overwritten, also set it on customer when the order is created
                $emails = explode(';', $customer->getAdditionalEmail());
                $emails[0] = $this->_getCustomerSession()->getNewEmail();
                $customer->setEmailOverwritten(1)->setAdditionalEmail(implode(';', $emails))->save();
                $this->_getCustomerSession()->setNewEmail(null);
            }
        } else {
            $customer = $this->_addCustomer();

            //if customer exists, load the entity to retrieve the campaigns, ctn records and other data
            $session = $this->_getCustomerSession();
            $session->setCustomer($customer);
            $session->unsBtwState();

            $quote->save();
        }
        Mage::getSingleton('customer/session')->setCustomerInfoSync(true);
        $quote->setIsMultiShipping(true);
        $shipToInfo = $this->_buildDeliveryAddresses($customer->getId());
        $checkout->setShippingItemsInformation($shipToInfo);
        // TODO Since we need to hard-code at this moment, we manually set them
        $addresses = $quote->getAllShippingAddresses();
        foreach ($addresses as $address) {
            $address->setShippingMethod('flatrate_flatrate');
        }

        $this->_setDefaultPayment();
        /*********************************************************/
        $checkout->createOrders();

        // From this point on cache all models for faster processing, since they won't be changed
        if (!Mage::registry('freeze_models')) {
            Mage::unregister('freeze_models');
            Mage::register('freeze_models', true);
        }

        $orderPackages = array();
        $packagesTypes = $this->_checkIfDeliveryInShop();

        $orderIds = Mage::getSingleton('core/session')->getOrderIds();
        foreach ($orderIds as $orderIncrementId) {
            /** @var Dyna_Checkout_Model_Sales_Order $order */
            $order = Mage::getModel('sales/order')->loadByAttribute('increment_id', $orderIncrementId);

            //create new package entities to set their status
            foreach ($order->getAllItems() as $item) {
                $orderPackages[] = $item->getPackageId();
            }

            // Set the order delivery type
            if (isset($item) && $order->getShippingAddress()) {
                $order->getShippingAddress()->setDeliveryType($packagesTypes[$item->getPackageId()]['type']);
                if ($packagesTypes[$item->getPackageId()]['type'] == 'store') {
                    $order->getShippingAddress()->setDeliveryStoreId($packagesTypes[$item->getPackageId()]['store_id']);
                }
                $order->getShippingAddress()->save();
            }
        }

        // Create a new superOrder
        $superOrder = $this->createANewSuperOrder($superorderId);
        $quotePackages = $quote->getCartPackages();

        // If the superorder was successfully created, move the packages to the order
        $isOneOff = false;
        /** @var Omnius_Package_Model_Package $quotePackage */
        $isOneOff = $this->movePackageToOrder($quotePackages, $isOneOff, $superOrder);

        unset($quotePackages);

        // Assign all the orders to the super order and custom increment id
        foreach ($orderIds as $orderIncrementId) {
            /** @var Omnius_Checkout_Model_Sales_Order $order */
            $order = Mage::getModel('sales/order')->loadByAttribute('increment_id', $orderIncrementId);
            $order
                ->setSuperorderQuery($superOrder->getId())
                ->setEditedOrder(0)
                ->oneOff($isOneOff)
                ->processPackageChecksum();
        }

        return $superOrder;
    }

    /**
     * Override createOrder to also call the processOrder call
     * Create delivery orders and superorder through Omnius
     * @return mixed
     * @throws LogicException
     */
    public function createOrder($superOrderId = false)
    {
        $quote = $this->getQuote();
        $quoteId = $this->getQuote()->getId();
        $request = $this->getRequest();

        if ($request->isPost()) {
            try {
                $superOrder = $this->omniusCreateOrder($superOrderId);
                $superOrder->setSalesId($quote->getSalesId());

            } catch (Exception $e) {
                // Set fail response for other errors
                $result['error'] = true;
                $result['message'] = $this->__($e->getMessage());
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                // order creation failed, cannot continue
                return;
            }
            try {
                $packageIds = [];
                //also associate quote checkout fields to package
                foreach ($superOrder->getPackages() as $package) {
                    $checkoutFields = Mage::getModel('dyna_checkout/field')->loadQuoteData(
                        array(
                            'quote_id' => $quoteId,
                            'package_number' => $package->getPackageId()
                        )
                    );
                    $checkoutFields->massUpdate(array('quote_id' => null, 'package_id' => $package->getId()));
                    $packageIds[] = $package->getId();
                }

                // No need for result of executeProcessOrderRequest / use throw LogicException in client
                /** @var Dyna_Superorder_Model_Client_SubmitOrderClient $client */
                $client = Mage::getModel("dyna_superorder/client_submitOrderClient");
                $response = $client->executeSubmitOrder($superOrder);

                /** @var Dyna_Superorder_Model_Client_SendDocumentClient $client */
                $client = Mage::getModel("dyna_superorder/client_sendDocumentClient");

                if (!$client->getUseStubs()) {
                    $checkoutFieldsByPackage = Mage::getModel('dyna_checkout/field')->loadPackageData($packageIds);
                    //execute send document with the offer
                    $sendResult = $client->executeSendDocument($quote, 'Order', $superOrder, null,
                        $checkoutFieldsByPackage);

                    if (isset($sendResult['Success'])) {
                        if (!$sendResult['Success']) {
                            $result['error'] = true;
                            $result['message'] = $sendResult['Status']['StatusReasonCode'] . ' - ' . $sendResult['Status']['StatusReason'];
                            $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                                Mage::helper('core')->jsonEncode($result)
                            );
                            return;
                        }
                    } else {
                        $result['error'] = true;
                        $result['message'] = 'SendDocument ' . $this->__('service did not return a valid response')
                            . PHP_EOL . json_encode($sendResult);
                        $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                            Mage::helper('core')->jsonEncode($result)
                        );
                        return;
                    }
                }
            } catch (Exception $e) {
                Mage::logException($e);
                $result['error'] = true;
                $result['message'] = $this->__($e->getMessage());
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            }
        }

        //if campaign shopping cart, report to UCT
        $session = $this->_getCustomerSession();
        $uctParams = $session->getUctParams();
        if (isset($uctParams["transactionId"])) {
            $quote->setTransactionId($uctParams["transactionId"]);
        }
        Mage::helper('dyna_checkout/data')->sendUCTReportProgress($superOrder, $quote);

        $this->throwErrorsIfNotSuccess($response);

        return $superOrder;
    }

    /**
     * This method will unset all data from session(address, customer, quote)
     */
    private function unsetStoredDataOnFailure()
    {
        /** @var Omnius_Checkout_Model_Sales_Quote $theQuote */
        $theQuote = Mage::getSingleton('checkout/cart')->getQuote();
        $theQuote->setIsActive(false)->save();
        // Unload UTC data
        $this->_getCustomerSession()->unsUctParams();
        // Unset RetrieveCustomerInfo data
        $this->_getCustomerSession()->unsServiceCustomers();

        /** Clear serviceability data from session when unloading customer */
        Mage::getSingleton('dyna_address/storage')->clearAddress();

        $this->_getCustomerSession()->unsetCustomer();
        $this->_getCustomerSession()->unsOrderEdit();
        $this->_getCustomerSession()->unsOrderEditMode();
        $this->_getCustomerSession()->unsNewEmail();
        $this->_getCustomerSession()->unsWelcomeMessage();
        $this->_getCustomerSession()->unsProspect();
        $this->_getCheckoutSession()->unsIsEditMode();
        $this->_getCheckoutSession()->setCustomerInfoSync(false);
        $this->_getCheckoutSession()->clear();
    }

    /**
     * Contains the backend validations for the customer fields
     * @return array
     */
    protected function _getCustomerDataFields()
    {
        return [];
    }

    /**
     * Overrides omnius's method to fix OVG-2484 (null lastname) validation error.
     *
     * @param $customerId
     * @param $addressData
     * @return bool|mixed
     * @throws Exception
     */
    protected function _addCustomerShippingAddress($customerId, $addressData)
    {
        $customer = $this->getQuote()->getCustomer();

        $shipAddress = $this->getQuote()->getShippingAddress();
        $addressData['firstname'] = (trim($shipAddress->getFirstname()) != '') ? $shipAddress->getFirstname() : $customer->getFirstname();
        $addressData['company'] = $shipAddress->getCompany();
        // Delivery can only be in Netherlands!
        $addressData['country_id'] = Omnius_Checkout_Model_Sales_Order::DEFAULT_COUNTRY;
        $addressData['fax'] = $shipAddress->getFax();
        $addressData['lastname'] = (trim($shipAddress->getLastname()) != '') ? $shipAddress->getLastname() : $customer->getLastname();
        $addressData['middlename'] = (trim($shipAddress->getMiddlename()) != '') ? $shipAddress->getMiddlename() : $customer->getMiddlename();
        $addressData['suffix'] = (trim($shipAddress->getSuffix()) != '') ? $shipAddress->getSuffix() : $customer->getSuffix();
        $addressData['prefix'] = (trim($shipAddress->getPrefix()) != '') ? $shipAddress->getPrefix() : $customer->getPrefix();
        $addressData['telephone'] = $shipAddress->getTelephone();
        // Check if the address was already added, in that case just return the id.
        $addressId = false;

        foreach ($customer->getAddresses() as $tempAddress) {
            if ($addressData['street'] == array(
                    $tempAddress->getStreetName(),
                    $tempAddress->getHouseNo(),
                    $tempAddress->getHouseAdd(),
                ) &&
                $addressData['postcode'] == $tempAddress->getPostcode() &&
                $addressData['city'] == $tempAddress->getCity()
            ) {
                $addressId = $tempAddress->getId();
                break;
            }
        }
        if (!$addressId) {
            // address does not exist in the db , create it
            $address = Mage::getModel('customer/address');

            $addressForm = Mage::getModel('customer/form');
            $addressForm->setFormCode('customer_address_edit')
                ->setEntity($address);

            $addressErrors = $addressForm->validateData($addressData);
            if (true === $addressErrors) {
                $addressForm->compactData($addressData);
                $address->setCustomerId($customer->getId())
                    ->setIsDefaultBilling(false)
                    ->setIsDefaultShipping(false)
                    ->save();

                return $address->getId();
            } else {
                throw new Exception(Mage::helper('core')->jsonEncode($addressErrors));
            }

        } else {
            // just return the id
            return $addressId;
        }
    }

    private function executeSendPushNotification($phoneNumber, $offeringId)
    {
        $apiURL = trim(Mage::getStoreConfig('omnius_service/my_vodafone_api_credentials/vfde_api_url'));
        $apiData = $this->formatDataForPushNotifications($phoneNumber, $offeringId);

        // if no URL/ API Data is defined
        if ($apiURL == "" || !$apiData) {
            Mage::log('MyVfAPI URL/Data missing', Zend_Log::ERR, 'exception.log');
            return array(
                'error' => true,
                'message' => 'API URL/Data missing'
            );
        }
        $basicAuthUser = trim(Mage::getStoreConfig('omnius_service/my_vodafone_api_credentials/basic_auth_username'));
        $basicAuthPass = trim(Mage::getStoreConfig('omnius_service/my_vodafone_api_credentials/basic_auth_password'));

        $headers = array(
            'Content-Type:application/json',
            'Accept:application/vnd.aps-version2+json'
        );

        if ($basicAuthUser && $basicAuthPass) {
            $headers[] = 'Authorization: Basic ' . base64_encode("$basicAuthUser:$basicAuthPass");
        }

        $options = array(
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_HEADER => 1,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $apiData,
            CURLOPT_RETURNTRANSFER => true,
            CURLINFO_HEADER_OUT => true,
        );
        if ($basicAuthUser && $basicAuthPass) {
            $options[CURLOPT_USERPWD] = $basicAuthUser . ':' . $basicAuthPass;
        }
        if ($proxy = trim(Mage::getStoreConfig('omnius_service/my_vodafone_api_credentials/proxy_server'))) {
            $options[CURLOPT_PROXY] = $proxy;
        }
        $curl = new Varien_Http_Adapter_Curl();
        $curl->setOptions($options);
        $logged = false;
        try {
            $curl->write(Zend_Http_Client::POST, $apiURL, '1.0', $headers, $apiData);
            $return = $curl->read();
            $header_size = $curl->getInfo(CURLINFO_HEADER_SIZE);
            $header = substr($return, 0, $header_size);
            $body = substr($return, $header_size);
            $requestLogData = sprintf(
                "Request Headers:\n%s\n\nRequest Body:\n%s\n\n",
                $curl->getInfo(CURLINFO_HEADER_OUT),
                $apiData
            );
            $responseLogData = sprintf(
                "Response Headers:\n%s\n\nResponse Body:\n%s\n\n",
                $header,
                $body
            );
            $this->logApiCall($requestLogData, $responseLogData);
            $logged = false;

            $curl->close();
            $responseData = json_decode($body, true);

            //sometimes response comes as array instead of object
            $responseData = isset($responseData[0]['ok']) ? $responseData[0] : $responseData;

            $hasError = !isset($responseData['ok']) || !$responseData['ok'];

            return array(
                'error' => $hasError,
                'message' => $hasError
                    ? (isset($responseData['error'])
                        ? $responseData['error'] : 'SendPushNotification ' . $this->__('call failed, but there was no error given from this service'))

                    : (isset($responseData['details'])
                        ? $responseData['details'] : 'SendPushNotification ' . $this->__('call completed without errors, but there were no details in the response'))
            );
        } catch (Exception $e) {
            if (!$logged) {
                $this->logApiCall($requestLogData, $responseLogData);
            }
            Mage::logException($e);
            return array(
                'error' => true,
                'message' => $e->getMessage()
            );
        }
    }

    protected function logApiCall($requestData, $responseData)
    {
        Mage::getSingleton('omnius_service/logger')->logTransfer(
            sprintf("%s\n%s", $requestData, $responseData),
            'services' . DS . 'MyVFAPS',
            'PushNotification' . '.' . sha1(rand(0, 1000) . time()),
            'log'
        );
    }

    private function formatDataForPushNotifications($phoneNumber, $offeringId)
    {
        $dataTemplate = Mage::getStoreConfig('omnius_service/my_vodafone_api_credentials/vfde_api_data');

        // if no template is defined
        if (!$dataTemplate) {
            return false;
        }
        $date = new Zend_Date(Mage::getModel('core/date')->timestamp());
        $expiry_date = date('o-m-d') . "T" . date('H:i:s');
        $expiry_time = date('o-m-d', strtotime($expiry_date . ' + 14 days')) . "T" . date('H:i:s');

        $pushType = trim(Mage::getStoreConfig('omnius_service/my_vodafone_api_credentials/push_type'));
        $strFindReplace = array(
            "{{phone_number}}" => $phoneNumber,
            "{{current_date}}" => $date->toString('YYYY-MM-ww HH:mm:ss'),
            "{{offering_id}}" => $offeringId,
            "{{push_type}}" => $pushType,
            "{{expiry_time}}" => $expiry_time,
            "{{correlation_id}}" => '"' . uniqid() . '"'
        );

        return strtr($dataTemplate, $strFindReplace);
    }

    /**
     * @param $data
     * @param $packageId
     * @return array
     * @throws Exception
     */
    protected function _validateSplitDeliveryAddressData($data, $packageId)
    {
        $errors = array();
        switch ($data['address']) {
            case 'pickup':
            case 'store':
                $addressValidations = array(
                    'street' => array(
                        'required' => true,
                        'validation' => array()
                    ),
                    'postcode' => array(
                        'required' => true,
                        'validation' => array()
                    ),
                    'city' => array(
                        'required' => true,
                        'validation' => array()
                    ),
                );
                if ($errors = $this->checkoutHelper->_performValidate($data, $addressValidations,
                    'delivery[pakket][' . $packageId . '][store][', ']')) {
                    $errors += array('delivery[pakket][' . $packageId . '][store]' => $this->__(current($errors)));
                }
                break;
            case 'deliver':
            case 'billing_address':
            case 'other_address' :
            case 'service':
                $addressValidations = array(
                    'street' => array(
                        'required' => true,
                        'validation' => array()
                    ),
                    'houseno' => array(
                        'required' => false,
                        'validation' => array(
                            'validateNumber'
                        )
                    ),
                    'addition' => array(
                        'required' => false,
                        'validation' => array()
                    ),
                    'postcode' => array(
                        'required' => true,
                        'validation' => array()
                    ),
                    'city' => array(
                        'required' => true,
                        'validation' => array()
                    ),
                );
                $errors += $this->checkoutHelper->_performValidate($data, $addressValidations,
                    'delivery[pakket][' . $packageId . '][otherAddress][', ']');
                break;
            case 'foreign_address' :
                $addressValidations = array(
                    'street' => array(
                        'required' => true,
                        'validation' => array()
                    ),
                    'houseno' => array(
                        'required' => false,
                        'validation' => array(
                            'validateNumber'
                        )
                    ),
                    'addition' => array(
                        'required' => false,
                        'validation' => array()
                    ),
                    'extra' => array(
                        'required' => false,
                        'validation' => array()
                    ),
                    'city' => array(
                        'required' => true,
                        'validation' => array()
                    ),
                    'region' => array(
                        'required' => false,
                        'validation' => array()
                    ),
                    'postcode' => array(
                        'required' => true,
                        'validation' => array()
                    ),
                    'country_id' => array(
                        'required' => true,
                        'validation' => array()
                    ),
                );
                $errors += $this->checkoutHelper->_performValidate($data, $addressValidations,
                    'delivery[pakket][' . $packageId . '][foreignAddress][', ']');
                break;
            default :
                throw new Exception($this->__('Missing delivery address data'));
        }

        return $errors;
    }

    /**
     * @param $customerId
     * @return array
     * @throws Exception
     */
    protected function _buildDeliveryAddresses($customerId)
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = $this->getQuote();
        // Retrieve the previously saved data for addresses
        $data = Mage::helper('core')->jsonDecode($quote->getShippingData());

        $addressArray = array();
        if (isset($data['deliver'])) { // Single Delivery
            foreach ($quote->getPackages() as $packageId => $package) {
                if (!empty($data['deliver']['address'])) {
                    $addressArray[$packageId] = $data['deliver']['address'];
                }
            }
        } elseif (isset($data['pakket'])) { // Split delivery
            foreach ($quote->getPackages() as $packageId => $package) {
                $addressArray[$packageId] = $data['pakket'][$packageId]['address'];
            }
        }

        $addresses = array();
        foreach ($addressArray as $packageId => $address) {
            if (is_numeric($address)) {
                $address = Mage::getModel('customer/address')->load($address);
                if (!$address) {
                    throw new Exception($this->__('Invalid delivery address id'));
                }
                $addresses[$packageId] = $address->getId();
            } elseif (is_array($address)) {
                $addresses[$packageId] = $this->_addCustomerShippingAddress($customerId, $address);
            }
        }

        $result = array();

        $items = array_merge($quote->getAllItems(), $quote->getAlternateItems(null, true, 'both'));

        foreach ($items as $item) {
            $result[] = array(
                $item->getId() => array(
                    'address' => $addresses[$item->getPackageId()],
                    'qty' => (int)$item->getQty(),
                    'is_contract_drop' => $item->getAlternateQuoteId() ? 1 : null
                )
            );
        }

        return $result;
    }

    public function applyMobileYoungRulesAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $checkoutData = $request->getPost();

            try {
                // Validate there is a quote
                $quote = $this->getQuote();

                $this->throwInvalidQuote($quote);

                //In case of SoHo with Commercial register, there is no dob field for the customer (and no Young items)
                $customerDob = $checkoutData['dob'] ?? date('d.m.Y');

                $this->throwInvalidExistDateError($customerDob);

                $currentAge = DateTime::createFromFormat('d.m.Y', $customerDob);

                $this->throwinvalidDOBError($currentAge);
                $currentAge = $currentAge->diff(new DateTime('now'))->y;

                //VFDED1W3S-2519 Check if minimum age is 7
                $minNewPrepaidAge = Dyna_Catalog_Model_Type::getNewPrepaidCustomerMinimumAge();
                $minNewCustomerAge = Dyna_Catalog_Model_Type::getNewCustomerMinimumAge();

                $checkoutLayout = Mage::helper("dyna_checkout/layout");
                $checkoutLayout->setupFormLayout();

                $checkout = Mage::helper("dyna_validators/checkout");

                $displayFields = array();
                $hideFields = array();

                // VFDED1W3S-47
                if ($quote->hasYoungItems() && $checkoutLayout->isAvailableField('customer[dob]')) {
                    $youngItems = 0;
                    $youbiageItems = 0;

                    /** @var $validatorHelper Dyna_Validators_Helper_Data */
                    $validatorHelper = Mage::helper('dyna_validators');
                    $validYoungAge = $validatorHelper->validateAgeBetween18And28($customerDob);

                    /** @var Dyna_Checkout_Model_Cart $cart */
                    $cart = Mage::getSingleton('checkout/cart');

                    $youbiageSku = Dyna_Catalog_Model_Type::getYouBiageOption();
                    $youbiageProductId = Mage::getModel('dyna_catalog/product')->getIdBySku($youbiageSku);
                    /** @var $youbiageProduct Dyna_Catalog_Model_Product */
                    $youbiageProduct = Mage::getModel('dyna_catalog/product')->load($youbiageProductId);

                    // throw exception if mandatory young option cannot be found in DB
                    $this->throwErrorIfNotInDB($youbiageProduct, $youbiageSku);

                    // count young/youbiage items
                    $countofYoungItems = $this->countYoungItems($youngItems, $youbiageItems, $quote);
                    $youngItems = $countofYoungItems['youngItems'];
                    $youbiageItems = $countofYoungItems['youbiageItems'];

                    $mobileYoungAge = Dyna_Catalog_Model_Type::getMobileYoungMinAge();

                    $this->checkYoungAgeLimit($youngItems, $currentAge, $mobileYoungAge, $checkoutData['dob']);
                    if ($youngItems > 0) {
                        if ($validYoungAge && $youngItems == $youbiageItems) {
                            $hideFields = $this->saveYoungItemsCart($quote, $youbiageProductId, $cart);
                        } elseif (!$validYoungAge && $youngItems != $youbiageItems) {
                            $displayFields = $this->saveYoungCartWithMissingYoubiageItems($quote, $youbiageProduct,
                                $cart);
                        }
                    }
                } else {
                    $this->throwErrorForAgeValidation($quote, $currentAge, $minNewPrepaidAge, $checkoutData,
                        $minNewCustomerAge);
                }

                if (true === $checkout->hasYoungDiscount()) {
                    $this->throwErrorForYoungDiscountValidation($currentAge);
                }

                if (true === $checkout->hasStudentDiscount()) {
                    $this->throwErrorForStudentDiscountValidation($currentAge);
                }

                // Set successful response
                $result['error'] = false;
                $result['message'] = "Step data successfully saved";
                $result['display_fields'] = $displayFields;
                $result['hide_fields'] = $hideFields;

                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );

            } catch (Mage_Customer_Exception $e) {
                // Set fail response for validations
                $result['error'] = true;
                $result['message'] = $this->checkoutHelper->__("Please correct the following errors");
                $result['fields'] = Mage::helper('core')->jsonDecode($e->getMessage());
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );

                return;
            } catch (Exception $e) {
                // Set fail response for other errors
                $result['error'] = true;
                $result['message'] = $this->__($e->getMessage());

                if ($this->getServiceHelper()->isDev()) {
                    $result['trace'] = $e->getTrace();
                }

                $this
                    ->getResponse()
                    ->setHeader('Content-Type', 'application/json')
                    ->setBody(Mage::helper('core')->jsonEncode($result));
                return;
            }
        }
    }

    /**
     * This function is written specifically to handle the cyclomatic complexity
     * to throw exception if the quote is invalid
     * @param $quote
     * @return void
     */
    protected function throwInvalidQuote($quote)
    {
        if (!$quote->getId()) {
            throw new Exception($this->__('Invalid quote'));
        }
    }

    /**
     * This function is written specifically to handle the cyclomatic complexity
     * to throw exception if the young product is not avialable
     * @param $youbiageProduct
     * @param $youbiageSku
     * @return void
     */
    protected function throwInvalidYoung($youbiageProduct, $youbiageSku)
    {
        if (!$youbiageProduct->getId()) {
            throw new Exception(sprintf("Young product %s is not found in database.", $youbiageSku));
        }
    }

    /**
     * This function is written specifically to handle the cyclomatic complexity
     * to get the count of Young Items
     * @param $youngItems
     * @param $youbiageItems
     * @param $quote
     * @return array
     */
    protected function countYoungItems($youngItems, $youbiageItems, $quote)
    {
        /** @var Dyna_Package_Model_Package $package */
        foreach ($quote->getPackages() as $package) {
            $package = Mage::getModel('package/package')->load($package['entity_id']);
            if ($package->hasYoungItems()) {
                $youngItems++;
            }

            if ($package->hasYoubiageItems()) {
                $youbiageItems++;
            }
        }
        return array(
            'youngItems' => $youngItems,
            'youbiageItems' => $youbiageItems
        );
    }

    /**
     * This function is written specifically to handle the cyclomatic complexity
     * to validate the age of customer against Young plan's age limit
     * @param $youngItems
     * @param $currentAge
     * @param customerDob
     * @param $errors
     * @return void
     * @throws Mage_Customer_Exception
     */
    protected function checkYoungAgeLimit($youngItems, $currentAge, $customerDob, $errors = [])
    {
        if ($youngItems > 0 && $customerDob) {
            $mobileYoungMinAge = Dyna_Catalog_Model_Type::getMobileYoungMinAge();
            $mobileYoungMaxAge = Dyna_Catalog_Model_Type::getMobileYoungMaxAge();

            if ($currentAge < $mobileYoungMinAge || $currentAge > $mobileYoungMaxAge) {
                $errors['customer[dob]'] = sprintf($this->__('Age has to be between %s and %s years'),
                    $mobileYoungMinAge, $mobileYoungMaxAge);
                $errors['validationClass'] = 'validate-young-age';

                throw new Mage_Customer_Exception(Mage::helper('core')->jsonEncode($errors));
            }
        }
    }

    /**
     * This function is written specifically to handle the cyclomatic complexity
     * to save Young cart
     * @param $quote
     * @param $youbiageProductId
     * @param $cart
     * @return array
     */
    protected function saveYoungItemsCart($quote, $youbiageProductId, $cart)
    {
        $hideFields = [];
        foreach ($quote->getPackages() as $packageId => $package) {
            /** @var $package Dyna_Package_Model_Package */
            $package = Mage::getModel('package/package')->load($package['entity_id']);
            // remove first youbiage found
            if ($package->hasYoungItems() && $package->hasYoubiageItems()) {
                $packageItem = $package->getItemsByProductId($youbiageProductId);
                $cart->removeItem($packageItem->getId());
                $cart->save();
                $hideFields['young[' . $packageId . '][' . $package->getType() . '][sub_user_dob]'] = 'validate-age-young required-entry';
                break;
            }
        }
        return $hideFields;
    }

    /**
     * This function is written specifically to handle the cyclomatic complexity
     * to save Young cart
     * @param $quote
     * @param $youbiageProduct
     * @param $cart
     * @return array
     */
    protected function saveYoungCartWithMissingYoubiageItems($quote, $youbiageProduct, $cart)
    {
        $displayFields = [];
        foreach ($quote->getPackages() as $packageId => $package) {
            $package = Mage::getModel('package/package')->load($package['entity_id']);
            // ADD youbiage for each package missing one
            if ($package->hasYoungItems() &&
                !$package->hasYoubiageItems() &&
                $package->getType() == strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE)
            ) {
                $quote->saveActivePackageId($packageId);
                $youbiageProduct->setPackageId($packageId);
                $addedItem = $cart->addProduct($youbiageProduct->getId());

                if (!is_string($addedItem)) {
                    $addedItem->setSystemAction(Dyna_Checkout_Model_Sales_Quote_Item::SYSTEM_ACTION_ADD);
                    $addedItem->save();
                    if (!$addedItem->getId()) {
                        $cart->getQuote()->addItem($addedItem);
                    }
                }
                $cart->save();
                $displayFields['young[' . $packageId . '][' . $package->getType() . '][sub_user_dob]'] = 'validate-age-young required-entry';
            }
        }
        return $displayFields;
    }

    /**
     * This function is written specifically to handle the cyclomatic complexity
     * to save Young cart
     * @param $quote
     * @param $currentAge
     * @param $minNewPrepaidAge
     * @param $checkoutData
     * @param $errors
     * @param $minNewCustomerAge
     * @return void
     */
    protected function checkCustomerAge(
        $quote,
        $currentAge,
        $minNewPrepaidAge,
        $checkoutData,
        $errors,
        $minNewCustomerAge
    ) {
        if (Mage::helper('dyna_checkout/cart')->hasOnlyPackagesOfType($quote->getPackages(),
                Dyna_Catalog_Model_Type::TYPE_PREPAID)
            && ($currentAge < $minNewPrepaidAge) && $checkoutData['customer']['dob']
        ) {
            $errors['customer[dob]'] = $this->__('You must be at least ' . $minNewPrepaidAge . ' years old');
            throw new Mage_Customer_Exception(Mage::helper('core')->jsonEncode($errors));
        } else {
            if (($currentAge < $minNewCustomerAge) && $checkoutData['customer']['dob']) {
                $errors['customer[dob]'] = $this->__('You must be at least ' . $minNewCustomerAge . ' years old');
                throw new Mage_Customer_Exception(Mage::helper('core')->jsonEncode($errors));
            }
        }
    }

    /**
     * This function is written specifically to handle the cyclomatic complexity
     * to fill unfilled data like firstname,lastname and gender
     * @param $data
     * @return array
     */
    protected function fillUnfilledData($data)
    {
        if (!isset($data['firstname'])) {
            $return['firstname'] = isset($data['contact_person']['first_name']) && ($data['contact_person']['first_name'] != '')
                ? $data['contact_person']['first_name'] : $data['address']['company_name'];
        } else {
            $return['firstname'] = $data['firstname'];
        }
        if (!isset($data['lastname'])) {
            $return['lastname'] = isset($data['contact_person']['last_name']) && ($data['contact_person']['last_name'] != '')
                ? $data['contact_person']['last_name'] : $data['address']['company_name'];
        } else {
            $return['lastname'] = $data['lastname'];
        }
        if (!isset($data['email'])) {
            $return['email'] = isset($data['contact_person']['email']) && ($data['contact_person']['email'] != '')
                ? $data['contact_person']['email']
                : $data['contact_details']['email'];
        } else {
            $return['email'] = $data['email'];
        }
        //for soho the user doesn't specify gender, so we add a default "male" value
        if (!isset($data['gender'])) {
            $return['gender'] = 'male';
        } else {
            $return['gender'] = $data['gender'];
        }
        return $return;
    }

    /**
     * This function is written specifically to handle the cyclomatic complexity
     * to throw exceptions for the billing validation errors if there are any
     * @param $result
     * @return void
     */
    protected function throwExceptionForBilling($result)
    {
        if ($result) {
            if (isset($result['message']) && $result['message']) {
                throw new Exception(Mage::helper('core')->jsonEncode($result['message']));
            }
            throw new Exception(Mage::helper('core')->jsonEncode($result));
        }
    }

    /**
     * This function is written specifically to handle the cyclomatic complexity
     * to set new email for the customer
     * @param $emailOverwrite
     * @return void
     */
    protected function setNewEmailForCustomer($emailOverwrite)
    {
        if ($emailOverwrite) {
            if (Mage::helper('omnius_validators')->validateEmailSyntax($emailOverwrite)) {
                $this->_getCustomerSession()->setNewEmail($emailOverwrite);
            } else {
                throw new Mage_Customer_Exception(Mage::helper('core')->jsonEncode(['email_overwrite' => $this->__('Please enter a valid email address. For example johndoe@domain.com.')]));
            }
        } else {
            $this->_getCustomerSession()->setNewEmail(null);
        }
    }

    /**
     * This function is written specifically to handle the cyclomatic complexity
     * to save quote for customer
     * @param $request
     * @param $quote
     * @return void
     */
    protected function saveQuoteforCustomer($request, $quote)
    {
        if ($request->getPost('current_step') && !$request->getParam('is_offer')) {
            $quote->setCurrentStep($request->getPost('current_step'));
        }

        $quote->save();
    }

    /**
     * This function is written specifically to handle the cyclomatic complexity
     * to set yellow or blue Id's
     * @param $salesIdsTriple
     * @return array
     */
    protected function getYellowBlueIds($salesIdsTriple)
    {
        if ($salesIdsTriple && (empty($salesIdsTriple['error']))) {
            $return['yellow_sales_id'] = $salesIdsTriple["fn"];
            $return['blue_sales_id'] = $salesIdsTriple["kd"];
        } else {
            $return['yellow_sales_id'] = null;
            $return['blue_sales_id'] = null;
        }
        return $return;
    }

    /**
     * This function is written specifically to handle the cyclomatic complexity
     * to set yellow or blue Id's
     * @param $youngItems
     * @param $validYoungAge
     * @param $youbiageItems
     * @param $quote
     * @param $youbiageProductId
     * @param $cart
     * @param $youbiageProduct
     * @param $currentAge
     * @param $minNewPrepaidAge
     * @param $checkoutData
     * @param $errors
     * @param $minNewCustomerAge
     * @return array
     * @throws Mage_Customer_Exception
     */
    protected function validateAndSaveYoungItemsToCart(
        $youngItems,
        $validYoungAge,
        $youbiageItems,
        $quote,
        $youbiageProductId,
        $cart,
        $youbiageProduct,
        $currentAge,
        $minNewPrepaidAge,
        $checkoutData,
        $errors,
        $minNewCustomerAge
    ) {
        $hideFields = array();
        $displayFields = array();
        if ($youngItems > 0) {
            if ($validYoungAge && $youngItems == $youbiageItems) {
                $hideFields = $this->saveYoungItemsCart($quote, $youbiageProductId, $cart);
            } elseif (!$validYoungAge && $youngItems != $youbiageItems) {
                $displayFields = $this->saveYoungCartWithMissingYoubiageItems($quote, $youbiageProduct, $cart);
            }
        } else {
            $this->checkCustomerAge($quote, $currentAge, $minNewPrepaidAge, $checkoutData, $errors, $minNewCustomerAge);
        }
        return array('hideFields' => $hideFields, 'displayFields' => $displayFields);
    }

    /**
     * This function is written specifically to handle the cyclomatic complexity
     * to check customer
     * @param $checkCustomer
     * @return array
     */
    protected function getCheckCustomer($checkCustomer)
    {
        while ($checkCustomer != null) {
            $email = microtime(true) . uniqid("", true) . Omnius_Customer_Model_Customer_Customer::DEFAULT_EMAIL_SUFFIX;
            $customerModel = Mage::getModel('customer/customer')->setWebsiteId(Mage::app()->getWebsite()->getId());
            $checkCustomer = $customerModel->loadByEmail($email)->getId();
        }
        return $checkCustomer;
    }

    /**
     * This function is written specifically to handle the cyclomatic complexity
     * to throw errors in common
     * @param $errors
     * @return void
     */
    private function throwErrorsCommon($errors)
    {
        if (!empty($errors)) {
            throw new Mage_Customer_Exception(Mage::helper('core')->jsonEncode($errors));
        }
    }

    /**
     * This function is written specifically to handle the cyclomatic complexity
     * to throw errors in Delivery shipment
     * @param $delivery
     * @param $checkoutLayout
     * @return void
     */
    private function throwMissingShipmentError($delivery, $checkoutLayout)
    {
        if (!isset($delivery['method']) && $checkoutLayout->isDeliveryOptionsSectionAvailable()) {
            throw new Exception($this->__('Missing shipment method'));
        }
    }

    /**
     * This function is written specifically to handle the cyclomatic complexity
     * to check for delivery errors in method and to set $data
     * @param $delivery
     * @param $data
     * @param $checkoutInfo
     * @param $request
     * @param $quote
     * @param $checkoutLayout
     * @return array
     */
    private function checkForDeliveryMethodErrors($delivery, $data, $checkoutInfo, $request, $quote, $checkoutLayout)
    {

        $errors = array();
        $showDeliverySteps = true;

        /** @var Dyna_Checkout_Helper_Data $dynaCheckoutHelper */
        $dynaCheckoutHelper = Mage::helper('dyna_checkout');

        switch ($delivery['method']) {
            case 'service':
                $delivery = Mage::getSingleton('dyna_address/storage')->getServiceAddressArray();
                $delivery['address'] = 'service';
                $data['deliver']['address'] = $delivery;
                break;
            case 'direct' :
                $addressData = $this->_getCustomerSession()->getAgent(true)->getDealer()->getId();

                $data['deliver']['address'] = $this->checkoutHelper->_getAddressByType(array(
                    'address' => 'store',
                    'store_id' => $addressData,
                ));
                $errors = $this->validateAddressDataForDirect($data);
                $showDeliverySteps = true;
                break;
            case 'pickup' :
                /** @var Dyna_Agent_Model_Mysql4_Dealer_Collection $addressData */
                if (!isset($delivery['pickup']['store_id']) || empty($delivery['pickup']['store_id'])) {
                    $errors += array('delivery[pickup][store]' => htmlspecialchars($this->__('Invalid store chosen')));
                } else {
                    $this->setThePickupAddressOnSession($checkoutInfo, $data['deliver']['address'],
                        $delivery['pickup']['store_id']);
                }
                break;
            case 'deliver' :

                if (!isset($delivery['deliver']['address'])) {
                    throw new Exception($this->__('Missing shipping address info'));
                }

                $billingAddress = $this->getCheckoutFieldsHelper()->getBillingAddress();

                $data['deliver']['address'] = $dynaCheckoutHelper->_getAddressByType(array(
                    'address' => $delivery['deliver']['address'],
                    'billingAddress' => $billingAddress,
                ));
                break;
            case 'other_address' :

                if (!isset($delivery['deliver']['address'])) {
                    throw new Exception($this->__('Missing shipping address info'));
                }

                $data['deliver']['address'] = $this->checkoutHelper->_getAddressByType(array(
                    'address' => $delivery['deliver']['address'],
                    'otherAddress' => isset($delivery['other_address']) ? $delivery['other_address'] : [],
                    'foreignAddress' => $request->getPost('foreignAddress', array()),
                ));
                $errors += $this->checkoutHelper->_validateDeliveryAddressData($data['deliver']['address']);
                break;
            case 'split' :
                $splitData = isset($delivery['pakket']) ? $delivery['pakket'] : array();
                $this->throwErrorIfSplitIncomplete($splitData);

                $cancelledPackages = Mage::helper('omnius_package')->getCancelledPackages();
                $cancelledPackages = !empty($cancelledPackages) ? $cancelledPackages : [];
                $cancelledPackagesNow = Mage::getSingleton('customer/session')->getCancelledPackagesNow();
                $cancelledPackagesNow = !empty($cancelledPackagesNow) ? $cancelledPackagesNow : [];

                $res = $this->handleSplitMethodAddress($splitData, $cancelledPackages, $cancelledPackagesNow, $quote,
                    $errors, $showDeliverySteps);
                $showDeliverySteps = $res['showDeliverySteps'];
                $errors = $res['errors'];
                $data = $res['data'];
                break;
            default:
                // If no delivery method set that means, delivery section is not available.
                // In that case we'll set Customer's Billing Address as Shipping address
                $billingAddress = $this->getCheckoutFieldsHelper()->getBillingAddress();
                $delivery['deliver']['address'] = "deliver";
                $data['deliver']['address'] = $dynaCheckoutHelper->_getAddressByType(array(
                    'address' => $delivery['deliver']['address'],
                    'billingAddress' => $billingAddress,
                ));
                break;
        }

        return array(
            'showDeliverySteps' => $showDeliverySteps,
            'errors' => $errors,
            'data' => $data
        );
    }

    /**
     * This function is written specifically to handle the cyclomatic complexity
     * to throw errors in Delivery
     * @param $errors
     * @return void
     */
    private function throwDeliveryErrorsIfAny($errors)
    {
        if ($errors) {
            throw new Mage_Customer_Exception(Mage::helper('core')->jsonEncode($errors));
        }
    }

    /**
     * This function is written specifically to handle the cyclomatic complexity
     * To add configurable checkout products for number port
     * @param $providerData
     * @param $type
     * @return void
     */
    private function addConfigurableCheckoutProductsForNumberPorting($providerData, $type)
    {
        /** @var Dyna_Checkout_Helper_Data $checkoutHelper */
        $checkoutHelper = Mage::helper('dyna_checkout');
        if (isset($providerData['change_provider'])) {
            if ($providerData['no_phones_transfer'] == Dyna_Superorder_Helper_Client::PORTING_ONE_NUMBER) {
                // Remove number_porting_one, number_porting_more, number_porting_all configurable checkout products
                $checkoutHelper->removeConfigurableCheckoutProducts(array(
                    'number_porting_one',
                    'number_porting_more',
                    'number_porting_all'
                ), $type);

                // Add number_porting_one configurable checkout products
                $checkoutHelper->addConfigurableCheckoutProducts('number_porting_one', $type);

            } elseif ($providerData['no_phones_transfer'] == Dyna_Superorder_Helper_Client::PORTING_MULTIPLE_NUMBERS) {

                // Remove number_porting_one, number_porting_more, number_porting_all configurable checkout products
                $checkoutHelper->removeConfigurableCheckoutProducts(array(
                    'number_porting_one',
                    'number_porting_more',
                    'number_porting_all'
                ), $type);

                // Add number_porting_more configurable checkout products
                $checkoutHelper->addConfigurableCheckoutProducts('number_porting_more', $type);

            } elseif ($providerData['no_phones_transfer'] == Dyna_Superorder_Helper_Client::PORTING_ALL_NUMBERS) {

                // Remove number_porting_one, number_porting_more, number_porting_all configurable checkout products
                $checkoutHelper->removeConfigurableCheckoutProducts(array(
                    'number_porting_one',
                    'number_porting_more',
                    'number_porting_all'
                ), $type);

                // Add number_porting_all configurable checkout products
                $checkoutHelper->addConfigurableCheckoutProducts('number_porting_all', $type);

            } elseif ($providerData['no_phones_transfer'] == Dyna_Superorder_Helper_Client::PORTING_NO_NUMBER) {

                // Remove number_porting_one, number_porting_more, number_porting_all configurable checkout products
                $checkoutHelper->removeConfigurableCheckoutProducts(array(
                    'number_porting_one',
                    'number_porting_more',
                    'number_porting_all'
                ), $type);
            }

        } else {
            // Remove number_porting_one, number_porting_more, number_porting_all configurable checkout products
            $checkoutHelper->removeConfigurableCheckoutProducts(array(
                'number_porting_one',
                'number_porting_more',
                'number_porting_all'
            ), $type);
        }
    }

    /**
     * This function is written specifically to handle the cyclomatic complexity
     * To add configurable checkout products for technician needed
     * @param $providerData
     * @param $type
     * @param $currentTechnician
     * @return void
     */
    private function addConfigurableCheckoutProductsForTechnicianNeeded($providerData, $type, $currentTechnician)
    {
        /** @var Dyna_Checkout_Helper_Data $checkoutHelper */
        $checkoutHelper = Mage::helper('dyna_checkout');

        /** @var Dyna_Catalog_Helper_Data $catalogHelper */
        $catalogHelper = Mage::helper('dyna_catalog');
        if (isset($providerData['connection_active']) && $providerData['connection_active']) {

            // Remove technician_needed_no, technician_needed_yes configurable checkout products
            $checkoutHelper->removeConfigurableCheckoutProducts(array('technician_needed_no', 'technician_needed_yes'),
                $type);

            if (!empty($catalogHelper->getConfigurableCheckoutProducts('technician_needed_yes', $type))) {
                // Add technician_needed_yes configurable checkout products
                $checkoutHelper->addConfigurableCheckoutProducts('technician_needed_yes', $type);

                //Trigger Cable Artifacts for technician_needed_yes
                $checkoutHelper->triggerCableArtifacts($providerData['connection_active'], $type);
            } else {
                if (isset($currentTechnician[$type])) {
                    // Trigger Cable Artifacts if technician was already called and there is no SKUs for technician_needed_yes
                    $checkoutHelper->triggerCableArtifacts($providerData['connection_active'], $type);
                }
            }

        } else {
            if (isset($providerData['connection_active']) && !$providerData['connection_active']) {
                // Remove technician_needed_no, technician_needed_yes configurable checkout products
                $checkoutHelper->removeConfigurableCheckoutProducts(array(
                    'technician_needed_no',
                    'technician_needed_yes'
                ), $type);

                if (!empty($catalogHelper->getConfigurableCheckoutProducts('technician_needed_no', $type))) {
                    // Add technician_needed_no configurable checkout products
                    $checkoutHelper->addConfigurableCheckoutProducts('technician_needed_no', $type);

                    // Trigger Cable Artifacts for technician_needed_no
                    $checkoutHelper->triggerCableArtifacts($providerData['connection_active'], $type);
                } else {
                    if (isset($currentTechnician[$type])) {
                        // Trigger Cable Artifacts if technician was already called and there is no SKUs for technician_needed_no
                        $checkoutHelper->triggerCableArtifacts($providerData['connection_active'], $type);
                    }
                }
            }

        }
    }

    /**
     * This function is written specifically to handle the cyclomatic complexity
     * To get payment method
     * @param $paymentData
     * @return string
     */
    private function getPaymentMethods($paymentData)
    {
        $paymentMethod = "";
        if (!isset($paymentData['one_time']['type']) && !isset($paymentData['monthly']['type'])) {
            throw new Exception('Missing payment method');
        } else {
            $paymentMethod = isset($paymentData['monthly']['type']) ? $paymentData['monthly']['type'] : $paymentData['one_time']['type'];
        }
        return $paymentMethod;
    }

    /**
     * This function is written specifically to handle the cyclomatic complexity
     * To get payment method info
     * @param $paymentMethod
     * @param $paymentData
     * @param $quote
     * @param $cart
     * @param $doCartSave
     * @param $errors
     * @return array
     */
    private function getPaymentMethodInfo($paymentMethod, $paymentData, $quote, $cart, $doCartSave, $errors)
    {
        $payment = array();
        $methods = array(
            Dyna_Checkout_Helper_Fields::PAYMENT_CASH_ON_DELIVERY,
            Dyna_Checkout_Helper_Fields::PAYMENT_ALREADY_PAID,
            Dyna_Checkout_Helper_Fields::PAYMENT_CHECKMO,
            Dyna_Checkout_Helper_Fields::PAYMENT_PAY_IN_STORE,
            Dyna_Checkout_Helper_Fields::PAYMENT_DIRECT_DEBIT,
            Dyna_Checkout_Helper_Fields::PAYMENT_BANK_TRANSFER,
            Dyna_Checkout_Helper_Fields::PAYMENT_DIRECT_DEBIT_REUSE,
            Dyna_Checkout_Helper_Fields::PAYMENT_DIRECT_DEBIT_DIFF
        );
        switch ($paymentMethod) {
            case Dyna_Checkout_Helper_Fields::PAYMENT_SPLIT_PAYMENT:
                foreach ($paymentData['pakket'] as $packageId => $packageData) {
                    if (isset($packageData['type'])) {
                        $payment[$packageId] = $packageData['type'];
                        $payment[$packageId] = $this->translatePaymentTypeForMagento($payment[$packageId]);
                        if (!in_array($payment[$packageId], $methods)) {
                            $errors += array('payment[method]' => $this->__('Invalid payment method: ' . $payment[$packageId]));
                        }
                    }
                }
                break;
            default:
                $paymentMethod = $this->translatePaymentTypeForMagento($paymentMethod);
                if (in_array($paymentMethod, $methods)) {
                    $payment = $paymentMethod;
                } else {
                    $errors += array('payment[method]' => $this->__('Invalid payment method: ' . $paymentMethod));
                }
        }
        return array(
            'payment' => $payment,
            'quote' => $quote,
            'cart' => $cart,
            'errors' => $errors,
            'doCartSave' => $doCartSave
        );
    }

    /**
     * Translates internal payment methods to magento payment methods
     * @param $method
     * @return string
     */
    private function translatePaymentTypeForMagento($method)
    {
        switch (true) {
            case $method == Dyna_Checkout_Helper_Fields::PAYMENT_DIRECT_DEBIT_DIFF:
                return Dyna_Checkout_Helper_Fields::PAYMENT_DIRECT_DEBIT;
            case substr($method, 0, strpos($method, "|")) == Dyna_Checkout_Helper_Fields::PAYMENT_DIRECT_DEBIT_REUSE:
                return Dyna_Checkout_Helper_Fields::PAYMENT_DIRECT_DEBIT;
        }
        return $method;
    }

    /**
     * This function is written specifically to handle the cyclomatic complexity
     * To add checkout products for billing
     * @param $checkoutData
     * @return void
     */
    private function addCheckoutProductsForBilling($checkoutData)
    {
        if (isset($checkoutData['billing']['billing_type'])) {
            if ($checkoutData['billing']['billing_type'] == 'mail') {

                // Remove billing_paper, billing_online configurable checkout products
                Mage::helper('dyna_checkout')->removeConfigurableCheckoutProducts(array('billing_paper', 'billing_online'));

                // Add billing_online configurable checkout products
                Mage::helper('dyna_checkout')->addConfigurableCheckoutProducts('billing_online');

            } else {
                if ($checkoutData['billing']['billing_type'] == 'invoice') {
                    // Remove billing_online, billing_paper configurable checkout products
                    Mage::helper('dyna_checkout')->removeConfigurableCheckoutProducts(array(
                        'billing_online',
                        'billing_paper'
                    ));

                    // Add billing_paper configurable checkout products
                    Mage::helper('dyna_checkout')->addConfigurableCheckoutProducts('billing_paper');
                }
            }
        }
    }

    /**
     * This function is written specifically to handle the cyclomatic complexity
     * To create a new super order
     * @param $superorderId
     * @return array
     */
    private function createANewSuperOrder($superorderId)
    {
        if ($superorderId) {
            $superOrder = Mage::getModel('superorder/superorder')->load($superorderId);
        } else {
            $superOrder = Mage::getModel('superorder/superorder')->createNewSuperorder();
        }
        return $superOrder;
    }

    /**
     * This function is written specifically to handle the cyclomatic complexity
     * To move package to an order
     * @param $quotePackages
     * @param $isOneOff
     * @param $superOrder
     * @return boolean
     */
    private function movePackageToOrder($quotePackages, $isOneOff, $superOrder)
    {
        foreach ($quotePackages as $quotePackage) {
            $quotePackage->setOrderId($superOrder->getId())
                ->setQuoteId(null)
                ->setStatus(Omnius_Package_Model_Package::ESB_PACKAGE_STATUS_INITIAL)
                ->setCreditcheckStatus(Omnius_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_CC_INITIAL)
                ->setCreditcheckStatusUpdated(now());

            if ($quotePackage->getOneOfDeal()) {
                $isOneOff = true;
            }

            if ($quotePackage->isNumberPorting()) {
                $quotePackage->setPortingStatus(Omnius_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_NP_INITIAL)
                    ->setPortingStatusUpdated(now());
            }

            $quotePackage->save();
        }
        return $isOneOff;
    }

    /**
     * This function is written specifically to handle the cyclomatic complexity
     * To throw errors if anything wrong happens
     * @param $response
     * @return void
     */
    private function throwErrorsIfNotSuccess($response)
    {
        if (!$response['Success']) {
            $errorMessage = '';
            if (is_string($response['StatusReasonCode'])) {
                $errorMessage = $response['StatusReasonCode'];
            }
            if (is_string($response['StatusReason'])) {
                $errorMessage = ":" . $response['StatusReasonCode'];
            }
            if (!$errorMessage) {
                $errorMessage = $this->__("Could not create order");
            }
            throw new Exception($errorMessage);
        }
    }

    /**
     * This function is written specifically to handle the cyclomatic complexity
     * To throw errors for validation of errors
     * @param $quote
     * @param $currentAge
     * @param $minNewPrepaidAge
     * @param $checkoutData
     * @param $errors
     * @param $minNewCustomerAge
     * @return void
     */
    private function throwErrorForAgeValidation(
        $quote,
        $currentAge,
        $minNewPrepaidAge,
        $checkoutData,
        $minNewCustomerAge
    ) {
        $errors = [];
        if (Mage::helper('dyna_checkout/cart')->hasOnlyPackagesOfType($quote->getPackages(),
                Dyna_Catalog_Model_Type::TYPE_PREPAID)
            && ($currentAge < $minNewPrepaidAge) && $checkoutData['dob']
        ) {
            $errors['customer[dob]'] = $this->__('You must be at least ' . $minNewPrepaidAge . ' years old');
            $errors['validationClass'] = 'validate-min-age-7';
            throw new Mage_Customer_Exception(Mage::helper('core')->jsonEncode($errors));
        } else {
            if (!Mage::helper('dyna_checkout/cart')->hasOnlyPackagesOfType($quote->getPackages(),
                    Dyna_Catalog_Model_Type::TYPE_PREPAID)
                && ($currentAge < $minNewCustomerAge) && $checkoutData['dob']) {
                $errors['customer[dob]'] = $this->__('You must be at least ' . $minNewCustomerAge . ' years old');
                $errors['validationClass'] = 'validate-min-age-' . $minNewCustomerAge;
                throw new Mage_Customer_Exception(Mage::helper('core')->jsonEncode($errors));
            }
        }
    }

    /**
     * Function checks if date is in young discount limit
     * @param $currentAge
     * @return void
     */
    private function throwErrorForYoungDiscountValidation($currentAge)
    {
        $maxAge = Dyna_Catalog_Model_Type::getYoungDiscountMaxAge();
        if ($currentAge > $maxAge) {
            $errors['customer[dob]'] = sprintf($this->__('The age limit for the young people benefit is max. %s years.'),
                $maxAge);
            $errors['validationClass'] = 'validate-max-age-young-discount';
            throw new Mage_Customer_Exception(Mage::helper('core')->jsonEncode($errors));
        }

        return true;
    }

    /**
     * Function checks if date is in student discount limit
     * @param $currentAge
     * @return void
     */
    private function throwErrorForStudentDiscountValidation($currentAge)
    {
        $maxAge = Dyna_Catalog_Model_Type::getStudentDiscountMaxAge();
        if ($currentAge > $maxAge) {
            $errors['customer[dob]'] = $this->__('The age limit for the student benefit is max. 30 years.');
            $errors['validationClass'] = 'validate-max-age-30';
            throw new Mage_Customer_Exception(Mage::helper('core')->jsonEncode($errors));
        }

        return true;
    }

    /**
     * Function checks if date exists in calendar (eg. 1980.02.31 - false)
     * @param $date
     * @return void
     */
    private function throwInvalidExistDateError($date)
    {
        $invalidDate = true;
        // Check if the date contains 'atleast' 3 values
        if (substr_count($date, '.') > 2) {
            list($day, $month, $year) = explode('.', $date);
            // all should be numeric
            if (is_numeric($day) && is_numeric($month) && is_numeric($year)) {
                $invalidDate = (false === checkdate($month, $day, $year));
            }
        }

        if ($invalidDate) {
            $errors['customer[dob]'] = $this->__('Invalid date of birth - doesn\'t exist in calendar.');
            $errors['validationClass'] = 'validate-date-de';
            throw new Mage_Customer_Exception(Mage::helper('core')->jsonEncode($errors));
        }
    }

    /**
     * This function is written specifically to handle the cyclomatic complexity
     * To throw errors if invalid entry in DOB happens
     * @param $currentAge
     * @return void
     */
    private function throwinvalidDOBError($currentAge)
    {
        if (!$currentAge) {
            $errors['customer[dob]'] = $this->__('Invalid date of birth.');
            throw new Mage_Customer_Exception(Mage::helper('core')->jsonEncode($errors));
        }
    }

    /**
     * This function is written specifically to handle the cyclomatic complexity
     * To throw errors if invalid entry in DB happens
     * @param $youbiageProduct
     * @param $youbiageSku
     * @return void
     */
    private function throwErrorIfNotInDB($youbiageProduct, $youbiageSku)
    {
        if (!$youbiageProduct->getId()) {
            throw new Exception(sprintf("Young product %s is not found in database.", $youbiageSku));
        }
    }

    /**
     * This function is written specifically to handle the cyclomatic complexity
     * To throw errors if split data is incomplete
     * @param $splitData
     * @return void
     */
    private function throwErrorIfSplitIncomplete($splitData)
    {
        if (!$splitData) {
            throw new Exception($this->__('Incomplete data'));
        }
    }

    /**
     * This function is written specifically to handle the cyclomatic complexity
     * To handle methods for split data
     * @param $splitData
     * @param $cancelledPackages
     * @param $cancelledPackagesNow
     * @param $quote
     * @param $errors
     * @param $showDeliverySteps
     * @return array
     */
    private function handleSplitMethodAddress(
        $splitData,
        $cancelledPackages,
        $cancelledPackagesNow,
        $quote,
        $errors,
        $showDeliverySteps
    ) {
        foreach ($splitData as $packageId => &$address) {
            if (empty($address)) {
                unset($splitData[$packageId]);
                continue;
            }
            if (isset($address['method'])) {
                $address['address'] = $address['method'];
                unset($address['method']);
            }
            if (!isset($address['address']) || in_array($packageId, $cancelledPackages) ||
                (!empty($cancelledPackagesNow[$quote->getId()]) &&
                    in_array($packageId, $cancelledPackagesNow[$quote->getId()])
                )) {
                continue;
            }
            $data['pakket'][$packageId]['address'] = $this->checkoutHelper->_getAddressByType($address);
            if ($data['pakket'][$packageId]['address']['address'] == 'pickup' &&
                $data['pakket'][$packageId]['address']['store_id'] == Mage::getSingleton('customer/session')->getAgent(true)->getDealer()->getId()
            ) {
                $showDeliverySteps = true;
            }
            $errors += $this->_validateSplitDeliveryAddressData($data['pakket'][$packageId]['address'], $packageId);
        }
        return array(
            'showDeliverySteps' => $showDeliverySteps,
            'data' => $data,
            'errors' => $errors
        );
    }

    /**
     * This function is written specifically to handle the cyclomatic complexity
     * To validate address data for direct delivery
     * @param $data
     * @return array
     */
    private function validateAddressDataForDirect($data)
    {
        $errors = array();
        if ($this->checkoutHelper->_validateDeliveryAddressData($data['deliver']['address'])) {
            $errors += array('delivery[method][text]' => htmlspecialchars($this->__('Invalid store chosen')));
        }
        return $errors;
    }

    /**
     * This function is written specifically to handle the cyclomatic complexity
     * To validate address data for pickup of order
     * @param $data
     * @return array
     */
    private function validateAddressDataForPickup($data)
    {
        $errors = array();
        if ($this->checkoutHelper->_validateDeliveryAddressData($data['deliver']['address'])) {
            $errors += array('delivery[pickup][store]' => htmlspecialchars($this->__('Invalid store chosen')));
        }
        return $errors;
    }

    /**
     * This function is written specifically to handle the cyclomatic complexity
     * To update the customer data
     * @param $data
     * @param $customer
     * @return void
     */
    protected function updateCustomerData($data, $customer)
    {
        foreach ($data as $key => $value) {
            // Map data to the customer
            $customer->setData($key, $value);
        }
        $customer->save();
    }

    /**
     * This function is written specifically to handle the cyclomatic complexity
     * To throw errors while saving customer
     * @param $result
     * @return void
     */
    protected function throwErrorsInCustomerSave($result)
    {
        if (isset($result['error']) && $result['error']) {
            $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                Mage::helper('core')->jsonEncode($result));
            return;
        }
    }

    /**
     * This function is written specifically to handle the cyclomatic complexity
     * To get delivery errors
     * @param $checkoutLayout
     * @return array
     */
    private function getValidDeliveryMethodException($checkoutLayout)
    {
        $errors = array();
        if ($checkoutLayout->isDeliveryOptionsSectionAvailable()) {
            $errors += array('delivery[method]' => $this->__('Invalid delivery method'));
        }
        return $errors;
    }

    /**
     * This function is written specifically to handle the cyclomatic complexity
     * To add configurable checkout products for phonebook entry
     * @param $providerData
     * @param $type
     * @return void
     */
    private function addConfigurableCheckoutProductsForPhonebookEntry($providerData, $type)
    {
        //echo $type;print_r($providerData); die("IN");
        /** @var Dyna_Checkout_Helper_Data $checkoutHelper */
        $checkoutHelper = Mage::helper('dyna_checkout');
        if (isset($providerData['phone_entries_count'])) {
            if ($providerData['phone_entries_count'] == 0) {

                // Add phone_book_entry1 configurable checkout products
                $checkoutHelper->addConfigurableCheckoutProducts('phone_book_entry1', $type);

            } elseif ($providerData['phone_entries_count'] == 1) {

                // Add phone_book_entry2 configurable checkout products
                $checkoutHelper->addConfigurableCheckoutProducts('phone_book_entry2', $type);

            } elseif ($providerData['phone_entries_count'] == 2) {

                // Add phone_book_entry3 configurable checkout products
                $checkoutHelper->addConfigurableCheckoutProducts('phone_book_entry3', $type);

            }
        }
    }
}
