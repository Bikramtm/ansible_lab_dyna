<?php
require_once Mage::getModuleDir('controllers', 'Omnius_Checkout') . DS . 'CartController.php';

use Dompdf\Dompdf;

/**
 * Class Dyna_Checkout_CartController
 */
class Dyna_Checkout_CartController extends Omnius_Checkout_CartController
{
    /** @var  Omnius_Customer_Model_Session */
    protected $_customerSession;
    protected $quotePackages;
    protected $errors = [];
    protected $guestCustomerIsSoho;
    protected $vars;
    /** @var Dyna_Checkout_Model_Sales_Quote $theQuote */
    protected $theQuote;
    protected $saveCartResult;
    /** @var  Dyna_Customer_Model_Customer $prospectCustomer */
    protected $prospectCustomer = null;

    public function indexAction()
    {
        // Validate Configurable Checkout Products
        /** Dyna_Checkout_Helper_Data */
        Mage::helper('dyna_checkout')->validateConfigurableCheckoutProducts();

        $complete = Mage::helper('omnius_checkout')->hasAllPackagesCompleted() && Mage::helper('bundles')->hasMandatoryFamiliesCompleted()
            && !is_null($this->getCustomerSession()->getAgent()->getRoleId());
        if (!$complete) {
            Mage::getSingleton('core/session')->setGeneralError(Mage::helper('omnius_checkout')->__('You must have at least one complete package'));
            //redirect back to referer or home
            $this->_redirect('/');
        } else {
            $this->renderIndexLayout();
        }
    }

    /**
     * Process package notes from expanded cart for checkout
     */
    public function processNotesAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            Mage::helper('dyna_checkout')->processNotes($request);
        }
    }

    /**
     * Just render the layout for the indexAction
     */
    protected function renderIndexLayout()
    {
        $this
            ->loadLayout()
            ->_initLayoutMessages('checkout/session')
            ->_initLayoutMessages('catalog/session')
            ->getLayout()->getBlock('head')->setTitle($this->__('Shopping Cart'));
        $this->renderLayout();
    }

    /**
     * Save customer shopping cart for later usage:
     * - will save only if the agent's role is defined
     * - will save only if there is something in the cart
     * - will save only if the e-mail is unique (there is not a is_prospect customer with the same e-mail saved)
     * - if no customer is login: save a new customer with is_prospect = true)
     *
     */
    public function persistCartAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }

        if (!($this->getRequest()->isPost() && $this->getRequest()->isXmlHttpRequest())) {
            $this->getResponse()
                ->setHeader('Content-Type', 'application/json')
                ->setBody(Mage::helper('core')->jsonEncode(array('error' => true, 'message' => 'Method not supported')));
        }

        try {
            if (is_null($this->getCustomerSession()->getAgent()->getRoleId())) {
                $this->getResponse()
                    ->setHeader('Content-Type', 'application/json')
                    ->setBody(
                        Mage::helper('core')->jsonEncode(array('error' => true, 'message' => $this->__('Unauthorized action')))
                    );

                return;
            }

            $this->vars = array();
            $this->theQuote = $this->_getCart()->getQuote();
            $this->quotePackages = $this->theQuote->getPackages();
            $this->guestCustomerIsSoho = Mage::getSingleton('customer/session')->getCustomer()->getIsSoho();

            //adding sales triple id to the sales quote for validation
            $bundlesHelper = Mage::helper('bundles');
            $salesIdsTriple = $bundlesHelper->getProvisDataByRedSalesId();

            $yellow_blue_ids = $this->getYellowBlueIds($salesIdsTriple);
            $this->theQuote->setData('yellow_sales_id', $yellow_blue_ids["yellow_sales_id"]);
            $this->theQuote->setData('blue_sales_id', $yellow_blue_ids["blue_sales_id"]);

            if (count($this->quotePackages) < 1) {
                $this->getResponse()
                    ->setHeader('Content-Type', 'application/json')
                    ->setBody(
                        Mage::helper('core')->jsonEncode(array('error' => true, 'message' => $this->__('You have no packages in the current cart.')))
                    );

                return;
            }

            /** Update the use case indication value for each package in cart */
            $cartPackages = $this->theQuote->getCartPackages();
            if ($cartPackages) {
                foreach ($cartPackages as $cartPackage) {
                    $cartPackage->updateUseCaseIndication($cartPackage);
                }
            }

            /** Clear the active bundle from session */
            Mage::getSingleton('dyna_customer/session')->unsActiveBundleId();

            $requestData = $this->getRequest();
            
            $this->setCustomerData($requestData);

            $this->setLoadedProspectCustomer();
            // for a guest customer -> save the customer as a prospect customer
            // This function is written specifically to hanle the cyclomatic complexity
            $this->saveNewProspectCustomerIfNeeded();

            // if errors were already encounter -> do not continue;
            if (count($this->errors) > 0) {
                $this->saveCartResult = array(
                    'error' => true,
                    'message' => implode('<br />', $this->errors)
                );
                $this->getResponse()
                    ->setHeader('Content-Type', 'application/json')
                    ->setBody(Mage::helper('core')->jsonEncode($this->saveCartResult));

                return;
            }

            $this->theQuote->setCartStatus(Omnius_Checkout_Model_Sales_Quote::CART_STATUS_SAVED)
                ->setChannel(Mage::app()->getWebsite()->getCode())
                ->setIsOffer(0)
                ->setCartStatusStep(Omnius_Checkout_Model_Sales_Quote::CART_STATUS_STEP_CONFIGURATOR);

            $this->setCallBackDateTimeQuote($requestData);

            $this->setNewProspectCustomerOnQuote();

            $incrementId = Mage::helper('omnius_checkout')->getQuoteIncrementPrefix() . $this->theQuote->getId();
            $this->theQuote->setData('prospect_saved_quote', $incrementId);

            $session = Mage::getSingleton('dyna_customer/session');
            $uctParams = $session->getUctParams();

            $this->sendTheUCTProgress($uctParams);

            $this->theQuote->save();

            // preserve products snapshot on current quote
            $this->preserveProductsSnapshot($this->theQuote->getCartPackages(true));

            // send e-mail (to_send post parameter is checked)
            $this->sendEmailWithCart($this->vars);

            // send e-mail (new_cart post parameter is checked)
            $this->createNewCart();


            /** @var Dyna_Checkout_Helper_Cart $cartDataHelper */
            $cartDataHelper = Mage::helper('dyna_checkout/cart');
            $cartDataHelper->updatePreviousCarts($this->prospectCustomer);

        } catch (Exception $e) {
            $this->saveCartResult = array(
                'error' => true,
                'message' => sprintf('Action failed with message "%s"', $e->getMessage())
            );
        }

        $this->getResponse()
            ->setHeader('Content-Type', 'application/json')
            ->setBody(Mage::helper('core')->jsonEncode($this->saveCartResult));
    }

    protected function setLoadedProspectCustomer()
    {
        $this->prospectCustomer = null;
        $customer = Mage::getSingleton('dyna_customer/session')->getCustomer();
        $customerLoggedIn = $customer->isCustomerLoggedIn();
        if ($customerLoggedIn) {
            $this->prospectCustomer = $customer;
        }
    }

    /**
     * Save new prospect customer for a guest customer (no login)
     * The e-mail must be unique for this (there should be no saved is_prospect customer with the same e-mail)
     * Some basic validation is done: on e-mail syntax, dob and name
     */
    protected function saveNewProspectCustomer()
    {
        $this->errors = array();

        $validators = Mage::helper('dyna_validators');
        foreach ($this->vars as $k => $var) {
            $this->validateFormKey($var, $k, $validators, $this->errors);
        }

        if (!Mage::helper('omnius_validators')->validateEmailSyntax($this->getRequest()->get('email_send', null))) {
            $this->errors[] = $this->__('Please provide a valid email address');
            return;
        }

        if (count($this->errors) > 0) {
            return;
        }

        $customer = Mage::getModel('customer/customer');
        $customer = $customer->getCollection()
            ->addAttributeToFilter('is_prospect', true)
            ->addAttributeToFilter('additional_email', $this->getRequest()->get('email_send'))
            ->addAttributeToFilter('firstname', $this->getRequest()->get('firstname'))
            ->addAttributeToFilter('lastname', $this->getRequest()->get('lastname'))
            ->addAttributeToFilter('additional_telephone', array('in' => array($this->vars['additional_telephone'],
                $customer::COUNTRY_CODE . $this->vars['additional_telephone'], $customer::SHORT_COUNTRY_CODE . $this->vars['additional_telephone'])))
            ->addAttributeToFilter('phone_number', $this->vars['telephone'])
            ->addAttributeToFilter('phone_local_area_code', $this->vars['prefix'])
            ->addAttributeToFilter('is_business', $this->guestCustomerIsSoho)
            ->addAttributeToFilter('dob', date("Y-m-d H:i:s", strtotime($this->vars['dob'])));

        if ($this->guestCustomerIsSoho) {
            $customer->addAttributeToFilter('company_name', $this->vars['companyname']);
        }
        $customer->getFirstItem();
        if (count($customer) > 0 && isset($customer->getData()[0]['entity_id'])) {
            $this->prospectCustomer = Mage::getModel('customer/customer')->load($customer->getData()[0]['entity_id']);
        } else {
            /** @var Dyna_Checkout_Helper_Cart $cartDataHelper */
            $cartDataHelper = Mage::helper('dyna_checkout/cart');
            $this->vars['email_send'] = $this->getRequest()->get('email_send');
            $this->vars['firstname'] = $this->getRequest()->get('firstname');
            $this->vars['lastname'] = $this->getRequest()->get('lastname');
            $this->vars['guestCustomerIsSoho'] = $this->guestCustomerIsSoho;
            $this->prospectCustomer = $cartDataHelper->createDeProspectFromCart($this->vars);
        }
    }

    /**
     * @param $var
     * @param $k
     * @param Dyna_Validators_Helper_Data $validators
     * @param $errors
     */
    protected function validateFormKey($var, $k, $validators, &$errors)
    {
        $var = trim($var);

        if ($k == 'dob') {
            $k = 'date of birth';
            if (!$validators->validateIsDate($var)) {
                $errors[] = Mage::helper('omnius_checkout')->__(
                    'The date format is invalid. Please use the following format: dd.mm.yyyy'
                );
            }

            if (!$validators->validateMinAge($var)) {
                $errors[] = Mage::helper('omnius_checkout')->__(
                    'The customer must be older than 18 years.'
                );
            }

            if (!$validators->validateMaxAge($var)) {
                $errors[] = Mage::helper('omnius_checkout')->__(
                    'The customer must be younger than 100 years.'
                );
            }
        } else {

            if (!$validators->validateName($var)) {
                $errors[] = Mage::helper('omnius_checkout')->__(
                    sprintf('The %s field contains invalid characters.', $k)
                );
            }
        }

        if ($k != 'initial' && ($var == null || empty($var))) {
            $errors[] = sprintf('The %s field is empty.', Mage::helper('omnius_checkout')->__($k));
        }
    }

    protected function setNewProspectCustomerOnQuote()
    {
        $this->theQuote->setCustomerId($this->prospectCustomer->getId())
            ->setCustomerFirstname($this->vars['firstname'])
            ->setCustomerLastname($this->vars['lastname'])
            ->setCustomerDob(date('Y-m-d H:i:s', strtotime($this->vars['dob'])));

        if ($this->guestCustomerIsSoho) {
            $this->theQuote->setCustomerCompanyname($this->vars['companyname']);
        }
        $customer = $this->prospectCustomer;

        $this->theQuote->setCustomerEmail($this->getRequest()->get('email_send'))
            ->setAdditionalEmail($this->getRequest()->get('email_send'))
            ->setCustomerAdditionalTelephone($customer::COUNTRY_CODE . $this->vars['prefix'] . $this->vars['telephone'])
            ->setCustomerPhoneLocalAreaCode($this->vars['prefix'])
            ->setCustomerPhoneNumber($this->vars['telephone'])
            ->setCustomerIsBusiness($this->guestCustomerIsSoho);
    }

    protected function sendEmailWithCart($customerParams)
    {
        if ($this->getRequest()->get('to_send', null) != null && $this->getRequest()->get('to_send', null) != 'false') {
            $this->saveCartResult = $this->sendCartEmail($this->theQuote);
        } else {
            $this->saveCartResult = array(
                'error' => false,
                'message' => $this->__("Your shopping cart has been successfully saved"),
                'title' => $this->__("Offer save")
            );
        }
    }

    /**
     * @param $theQuote
     * @return array
     */
    protected function sendCartEmail($theQuote)
    {
        $customerParams = array();
        $result = array(
            'error' => false,
            'message' => $this->__("Your cart has been successfully saved and sent"),
            'title' => $this->__("Offer save & send")
        );

        $client = Mage::getModel("dyna_superorder/client_sendDocumentClient");
        if (!$client->getUseStubs()) {
            //execute send document with the offer
            $sendResult = $client->executeSendDocument($theQuote, 'Cart', null, $customerParams);

            if (isset($sendResult['Success'])) {
                if (!$sendResult['Success']) {
                    $result['error'] = true;
                    $result['message'] = $sendResult['Status']['StatusReasonCode'] . ' - ' . $sendResult['Status']['StatusReason'];
                    $result['title'] = $this->__('Error');
                }
            } else {
                $result['error'] = true;
                $result['message'] = 'SendDocument ' . $this->__('service did not return a valid response')
                    . PHP_EOL . json_encode($sendResult);
                $result['title'] = $this->__('Error');
            }
        }

        return $result;
    }

    protected function createNewCart()
    {
        if ($this->getRequest()->get('new_cart', null) != null && $this->getRequest()->get('new_cart', null) != 'false') {
            Mage::helper('omnius_checkout')->createNewQuote();
            Mage::helper('pricerules')->decrementAll($this->theQuote);
        } elseif ($this->getRequest()->get('new_cart', null) != null && $this->getRequest()->get('new_cart', null) == 'false') {
            // clone quote and set it as current quote so that many users can edit the same saved quote
            $newQuote = $this->theQuote
                ->cloneQuote()
                ->setIsActive(true)
                ->setCartStatus(null)
                ->setProspectSavedQuote(null)
                ->setStoreId(Mage::app()->getStore(true)->getId())
                ->clearCustomer()
                ->save();

            /** @var Dyna_Bundles_Helper_Data $bundlesHelper */
            $bundlesHelper = Mage::helper('dyna_bundles');
            $bundlesHelper->recreateBundles($newQuote);

            $this->theQuote->setIsActive(false)->save();

            $this->_getCart()->setQuote($newQuote);
            Mage::getSingleton('checkout/session')->setQuoteId($newQuote->getId());
        }
    }

    public function successAction()
    {
        // Clear any packages on quote
        // Clear data from address search
        Mage::getSingleton('dyna_address/storage')->clearAddress();
        Mage::getSingleton('customer/session')->unsetCustomer();
        if (!$this->getCustomerSession()->getLastSuperOrderId()) {
            // If no order was recently finished, redirect to front page
            $this->_redirect('/');
        } else {
            $quote = Mage::getSingleton('checkout/cart')->getQuote();
            $quote->setIsActive(false)->save();
            $this->renderIndexLayout();
        }
    }

    /**
     * Get instance of the Omnius_Customer_Model_Session
     *
     * @return Omnius_Customer_Model_Session
     */
    protected function getCustomerSession()
    {
        if (!$this->_customerSession) {
            $this->_customerSession = Mage::getSingleton('customer/session');
        }

        return $this->_customerSession;
    }

    public function updateProductsOnQuoteAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }
        $map_content=array();
        if($this->getRequest()->getParam('deliveryType')=='map') {
            $map_content = $this
                ->getLayout()
                ->createBlock("dyna_checkout/cart_steps_saveOverview")
                ->setTemplate("checkout/cart/steps/save_delivery_address/save_delivery_address_place.phtml")
                ->setResponse($this->getRequest()->getParam('selectedpackageId'))
                ->toHtml();
        }
        $productSkus = array_filter(
            array_filter(
                is_array($this->getRequest()->getParam('skus')) ? $this->getRequest()->getParam('skus') : array(), 'trim'
            )
        );

        $doCartSave = false;
        try {
            /** @var Dyna_Checkout_Model_Cart $cart */
            $cart = Mage::getSingleton('checkout/cart');
            /** @var Dyna_Checkout_Model_Sales_Quote $quote */
            $quote = $cart->getQuote();
            $wasAdded = [];

            // shipping fee handling moved to save delivery action

            // region Add/remove product for cables
            /** @var Dyna_Package_Model_Package $activePackage */
            $activePackage = $quote->getActivePackage();

            $activePackageType = $activePackage->getType();
            $isCable = in_array(strtolower($activePackageType), array_map('strtolower', Dyna_Catalog_Model_Type::getCablePackages()));
            $isCableInternetPhoneActive = strtolower($activePackageType) == strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_INTERNET_PHONE);

            if (!$isCable || $isCableInternetPhoneActive) {
                foreach ($productSkus as $skus => $mustBePresent) {
                    $mustBePresent = ($mustBePresent === 'true');
                    $skusList = [];
                    if (strpos($skus, ",") !== FALSE) {
                        $skusList = explode(",", $skus);
                    } else {
                        $skusList[] = $skus;
                    }

                    foreach ($skusList as $sku) {
                        /** @var Dyna_Catalog_Model_Product $product */
                        $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);

                        $isPresent = false;
                        // @todo this WILL not work as expected, needs refactoring
                        $item = $quote->getItemBySku($sku);

                        if (!empty($item) && $item->getId()) {
                            $isPresent = $item->getId();
                        }

                        if (isset($item) && !$item->getIsDefaulted() &&
                            !in_array($sku, $wasAdded) &&
                            !$mustBePresent &&
                            $isPresent &&
                            ($item->getPackageId() == $quote->getActivePackageId())
                        ) {
                            $doCartSave = true;
                            $item->isDeleted(true);
                            $item->save();
                        } else if ($mustBePresent && !$isPresent && $product) {
                            $doCartSave = true;
                            // @todo what happens when checkout is reached with a Mobile Postpaid active package
                            // @todo needs refactoring
                            $cart->addProduct($product->getId(), ['qty' => 1, 'system_action' => 'add']);
                            $wasAdded[] = $sku;
                        }
                    }
                }
            }
            //endregion
            $this->saveCartOnUpdate($doCartSave, $cart);

            $availableItems = $this->getAvailableItems($quote);
            //endregion

            $order_overview_content = $this
                ->getLayout()
                ->createBlock('dyna_checkout/cart_steps_saveOrderOverview')
                ->setTemplate('checkout/cart/steps/save_order_overview_partials/package_content.phtml')
                ->toHtml();

            $overview_content = $this
                ->getLayout()
                ->createBlock("dyna_checkout/cart_steps_saveOverview")
                ->setTemplate("checkout/cart/steps/save_overview_partials/package_content.phtml")
                ->toHtml();


            $response = [
                'error' => false,
                'order_overview_content' => $order_overview_content,
                'overview_content' => $overview_content,
                'available_items' => $availableItems,
                'map_content' => $map_content
            ];

            /* Add / remove the shipping price on quote */
        } catch (Exception $e) {
            Mage::logException($e);
            $response = [
                'error' => true,
                'message' => $e->getMessage(),
            ];
        }

        $this->getResponse()
            ->setHeader('Content-Type', 'application/json')
            ->setBody(Mage::helper('core')->jsonEncode($response));
    }

    public function loadDateTimeAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }

        $time_content = $this
                ->getLayout()
                ->createBlock("dyna_checkout/cart_steps_saveOverview")
                ->setTemplate("checkout/cart/steps/save_delivery_address/save_delivery_address_datetime.phtml")
                ->setResponse($this->getRequest()->getParam('selectedpackageId'))
                ->toHtml();

        try {
            $response = [
                'error' => false,
                'time_content' => $time_content
            ];
        } catch (Exception $e) {
            Mage::logException($e);
            $response = [
                'error' => true,
                'message' => $e->getMessage(),
            ];
        }

        $this->getResponse()
            ->setHeader('Content-Type', 'application/json')
            ->setBody(Mage::helper('core')->jsonEncode($response));
    }

    // activate cart moved on configurator cart controller for usage of common implementation

    public function validateSameDayDeliveryAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }

        try {
            $response['valid'] = false;
            $response['error'] = false;

            $postcode = $this->getRequest()->get("postcode");
            /** @var Dyna_Checkout_Model_Client_SAPShippingConditionClient $client */
            $client = Mage::getModel("dyna_checkout/client_SAPShippingConditionClient");
            $response['valid'] = $client->isValidSameDayDelivery($postcode);

        } catch (Exception $e) {
            $response = [
                'error' => true,
                'message' => $e->getMessage(),
            ];
        }


        $this->setJsonResponse($response);
    }

    public function validateSameDayDeliveryPerPackageAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }

        try {
            $response['valid'] = false;
            $response['error'] = false;

            $postcode = $this->getRequest()->get("postcode");
            /** @var Dyna_Checkout_Model_Client_SAPShippingConditionClient $client */
            $client = Mage::getModel("dyna_checkout/client_SAPShippingConditionClient");
            $response['valid'] = $client->isValidSameDayDeliveryPerPackage($postcode);

        } catch (Exception $e) {
            $response = [
                'error' => true,
                'message' => $e->getMessage(),
            ];
        }

        $this->setJsonResponse($response);
    }

    /**
     * @param $result
     */
    protected function setJsonResponse($result)
    {
        $this->getResponse()->setHeader('Content-Type', 'application/json');
        $this->getResponse()->setBody(json_encode($result));
        $this->getResponse()->setHttpResponseCode(200);
    }

    /**
     * Gets VF pickup locations according to location (GPS or postcode)
     */
    public function getPickupLocationsAction()
    {
            if(!$this->getRequest()->isPost()){
                return;
            }

        try {
                $parameters = array(
                    'longitude' => $this->getRequest()->get("long"),
                    'latitude' => $this->getRequest()->get("lat"),
                    'postcode' => $this->getRequest()->get("postcode"),
                );

                /** @var Dyna_Checkout_Model_Client_GetPickupLocationClient $client */
                $client = Mage::getModel("dyna_checkout/client_GetPickupLocationClient");
                $result = $client->executeGetPickupLocation($parameters);
                if($result === null){
                    Mage::throwException("No results found in this area");
                }

                $response = array(
                    'error' => false,
                    'data' => $result
                );

            } catch (Exception $e){
                $response = array(
                    'error' => true,
                    'message' => $this->__('No results found in this area'),
                );
            }

            $this->setJsonResponse($response);

    }

    /**
     * Gets VF getAvailableShippingOptions time
     */
    public function getAvailableShippingOptionsAction()
    {
        if(!$this->getRequest()->isPost()){
            return;
        }

        try {
                $parameters = array(
                    'postcode' => $this->getRequest()->get("postcode"),
                );

                /** @var Dyna_Checkout_Model_Client_GetAvailableShippingOptionsClient $client */
                $client = Mage::getModel("dyna_checkout/client_GetAvailableShippingOptionsClient");
                $result = $client->executeGetAvailableShippingOptions($parameters);
                if($result === null){
                    Mage::throwException("No results found in this area");
                }

                $response = array(
                    'error' => false,
                    'data' => $result
                );

            } catch (Exception $e){
                $response = array(
                    'error' => true,
                    'message' => $this->__('No results found in this area 111'),
                );
            }

            $this->setJsonResponse($response);

    }

    /**
     *
     */
    public function validateIBANAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }

        $IBAN = $this->getRequest()->get("iban");
        $quote = Mage::getSingleton('checkout/session')->getQuote();

        try {
            $IBANClient = Mage::getModel("dyna_checkout/client_convertAndValidateIBANClient");
            $isValid = $IBANClient->executeIsValidIBAN($IBAN);

            //also save response data in checkout field
            $responseData = $IBANClient->getResponseData($IBAN);
            $checkoutData = array(
                "[$IBAN][account_number]" => $responseData['bankData'][0]['ID'],
                "[$IBAN][bank_code]" => $responseData['bankData'][0]['BIC'],
                "[$IBAN][credit_provider]" => $responseData['bankData'][0]['Name'],
                "[$IBAN][usage]" => $responseData['bankData'][0]['BankIdentifier']
            );
            Mage::helper("dyna_checkout/fields")->addData(
                Mage::getSingleton('checkout/session')->getQuote(),
                'save_delivery',
                $checkoutData
            );

            $response['valid'] = $isValid;
            $response['error'] = false;

            if ($isValid) {
                $response['bankData'] = array(
                    "accountNumber" => $responseData['bankData'][0]['ID'],
                    "bankCode" => $responseData['bankData'][0]['BIC'],
                    "creditProvider" => $responseData['bankData'][0]['Name'],
                    "usage" => $responseData['bankData'][0]['BankIdentifier']
                );
            }

            $SEPA = $this->getRequest()->get("sepa");
            if ($SEPA == 'yes') {
                $response['isCablePackage'] = true;
            }


            if ($isValid && $SEPA == 'yes') {

                $response['validAddress'] = false;
                /** @var Dyna_Checkout_Model_Client_GetPayerAndPaymentInfoClient $payerInfo */
                $payerInfo = Mage::getModel("dyna_checkout/client_getPayerAndPaymentInfoClient");
                $payerInfoResponse = $payerInfo->executeGetPayerAndPaymentInfo(['iban' => $IBAN]);

                /** @var Dyna_Checkout_Helper_Fields $fieldsHelper */
                $fieldsHelper = Mage::helper("dyna_checkout/fields");
                $customerAddress = $fieldsHelper->getCheckoutFieldsAsArray("save_customer", $quote->getId());

                if (!empty($payerInfoResponse['PayerAndPaymentInfo']['PartyLegalEntity']['RegistrationAddress'])) {
                    $address = $payerInfoResponse['PayerAndPaymentInfo']['PartyLegalEntity']['RegistrationAddress'];
                    $customerData = $payerInfoResponse['PayerAndPaymentInfo']['Role']['Person'];

                    if (isset($customerAddress['billing[other_address][selected]']) && $customerAddress['billing[other_address][selected]'] == Dyna_Checkout_Helper_Fields::BILLING_TO_ALTERNATIVE_ADDRESS) {
                        $addressComp = [
                            $customerAddress['billing[other_address][firstname]'] == $customerData['FirstName'],
                            $customerAddress['billing[other_address][lastname]'] == $customerData['FamilyName'],
                            $customerAddress['billing[other_address][postcode]'] == $address['PostalZone'],
                            $customerAddress['billing[other_address][city]'] == $address['CityName'],
                            $customerAddress['billing[other_address][street]'] == $address['StreetName'],
                            $customerAddress['billing[other_address][houseno]'] == $address['BuildingNumber'],
                        ];
                    } else {
                        $addressComp = [
                            $customerAddress['customer[firstname]'] == $customerData['FirstName'],
                            $customerAddress['customer[lastname]'] == $customerData['FamilyName'],
                        ];
                        if (isset($customerAddress['customer[address][postcode]'])) {
                             $addressComp[] = ($customerAddress['customer[address][postcode]'] == $address['PostalZone']);
                        }
                        if (isset($customerAddress['customer[address][city]'])) {
                             $addressComp[] = ($customerAddress['customer[address][city]'] == $address['CityName']);
                        }
                        if (isset($customerAddress['customer[address][street]'])) {
                             $addressComp[] = ($customerAddress['customer[address][city]'] == $address['StreetName']);
                        }
                        if (isset($customerAddress['customer[address][city]'])) {
                             $addressComp[] = ($customerAddress['customer[address][houseno]'] == $address['BuildingNumber']);
                        }
                    }

                    $isValidAddress = array_reduce($addressComp, function ($result, $item) {
                        return $result && $item;
                    }, true);

                    $response['sepaIsKnown'] = true;

                    $customerInfo = [
                        'firstName' => $customerData['FirstName'],
                        'lastName' => $customerData['FamilyName'],
                        'gender' => $customerData['Salutation'],
                        'postCode' => $address['PostalZone'],
                        'city' => $address['CityName'],
                        'street' => $address['StreetName'],
                        'houseNumber' => $address['BuildingNumber']
                    ];
                    $response['customerInfo'] = $customerInfo;

                    if ($isValidAddress) {
                        $response['validAddress'] = true;
                        $response['exactMatch'] = true;
                    } else {
                        $response['validAddress'] = false;
                    }
                } else {
                    $response['sepaIsKnown'] = false;

                    /** @var Dyna_Checkout_Helper_Fields $fieldsHelper */
                    $fieldsHelper = Mage::helper("dyna_checkout/fields");
                    $quote = Mage::getSingleton("checkout/session")->getQuote();
                    $customer = $fieldsHelper->getCheckoutFieldsAsArray("save_customer", $quote->getId());

                    if ($customerAddress['billing[other_address][selected]'] == Dyna_Checkout_Helper_Fields::BILLING_TO_ALTERNATIVE_ADDRESS) {
                        $requestParams = [
                            "street" => $customer["billing[other_address][street]"],
                            "houseno" => $customer["billing[other_address][houseno]"],
                            "postcode" => $customer["billing[other_address][postcode]"],
                            "city" => $customer["billing[other_address][city]"],
                            "type" => Dyna_Address_Model_Client_ValidateAddressClient::SEPA_ADDRESS
                        ];
                    } else {
                        $requestParams = [
                            "street" => $customer["customer[address][street]"],
                            "houseno" => $customer["customer[address][houseno]"],
                            "postcode" => $customer["customer[address][postcode]"],
                            "city" => $customer["customer[address][city]"],
                            "type" => Dyna_Address_Model_Client_ValidateAddressClient::SEPA_ADDRESS
                        ];
                    }

                    /** @var Dyna_Address_Model_Client_ValidateAddressClient $validateAddressModule */
                    $validateAddressModule = Mage::getModel("dyna_address/client_validateAddressClient");
                    $isValidSEPA = $validateAddressModule->isValidAddress($requestParams);

                    $response['validAddress'] = $this->checkIfValidSEPA($isValidSEPA);
                }
            }

        } catch (Exception $e) {
            $response = [
                'error' => true,
                'message' => $e->getMessage(),
            ];
        }
        $this->setJsonResponse($response);
    }

    /**
     * Generate and return bank account data
     */
    public function generateBankDataAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }

        $params = [];
        $params['account_number'] = $this->getRequest()->getPost('account_number');
        $params['bank_code'] = $this->getRequest()->getPost('bank_code');
        $params['credit_provider'] = $this->getRequest()->getPost('credit_provider');

        try {
            $IBANClient = Mage::getModel("dyna_checkout/client_convertAndValidateIBANClient");
            $response = $IBANClient->executeConvertAndValidateByBankData($params['account_number'], $params['bank_code']);
            if (isset($response['bankData'][0]['ID'])) {
                $ibanNr = $response['bankData'][0]['ID'];
                Mage::helper("dyna_checkout/fields")->addData(
                    Mage::getSingleton('checkout/session')->getQuote(),
                    'save_delivery',
                    array(
                        "[$ibanNr][account_number]" => $params['account_number'],
                        "[$ibanNr][bank_code]" => $params['bank_code'],
                        "[$ibanNr][credit_provider]" => $params['credit_provider'],
                    )
                );
            }
            $response['error'] = false;
        } catch (Exception $e) {
            $response = [
                'error' => true,
                'message' => $e->getMessage(),
            ];
        }

        $this->setJsonResponse($response);
    }

    /**
     * Verify if validity period of a campaign ends before the validity period of a saved offer
     */
    public function savedOffersValidityPeriodAction()
    {
        /** @var Dyna_Checkout_Model_Cart $cart */
        $cart = Mage::getSingleton('checkout/cart');
        $request = $this->getRequest();

        if ($request->isPost()) {
            try {
                $offerProhibition = $this->checkOfferProhibition();
                if ($offerProhibition['error']) {
                    $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                        Mage::helper('core')->jsonEncode($offerProhibition)
                    );
                    return;
                }

                //validate checked required terms and conditions
                $checkoutData = $request->getPost();
                $errors = Mage::helper('dyna_validators/checkout')->validateVoicelogData($checkoutData);
                if (!empty($errors)) {
                    $response['error'] = true;
                    $response['termsAndConditionsError'] = true;
                    $response['message'] = '';
                    foreach ($errors as $error) {
                        $response['message'] .= $error . '</br>';
                    }

                    $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                        Mage::helper('core')->jsonEncode($response)
                    );
                    return;
                }

                // Validate there is a quote
                /** @var Dyna_Checkout_Model_Sales_Quote $quote */
                $quote = $cart->getQuote();
                $validity = "P" . (int)Mage::helper('dyna_checkout')->getSavedOfferValidityPeriod() . "D";

                if (!$quote->getId()) {
                    throw new Exception($this->__('Invalid quote'));
                }

                foreach ($quote->getPackages() as $package) {
                    if (isset($package['campaign_id']) && $package['campaign_id'] != "") {
                        $campaign = Mage::getModel('dyna_bundles/campaign')->load($package['campaign_id'], 'campaign_id');

                        // get saved offer expiration date
                        $offerExpirationDate = new \DateTime();
                        $offerExpirationDate
                            ->setTimestamp(Mage::getModel('core/date')->timestamp($cart->getQuote()->getData('created_at')))
                            ->add(new DateInterval($validity));

                        // get campaign valid to
                        $campaignValidTo = new \DateTime();
                        $campaignValidTo
                            ->setTimestamp(Mage::getModel('core/date')->timestamp($campaign->getData('valid_to')));

                        if ($campaign->getData('valid_to') && $offerExpirationDate > $campaignValidTo) {
                            $result['error'] = false;
                            $result['message'] = $this->__("Validity period of the campaign ends before the validity period of the saved offer.");

                            $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                                Mage::helper('core')->jsonEncode($result)
                            );
                            return;
                        }
                    }
                }

                // Set successful response
                $result['error'] = false;
                $result['message'] = false;

                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;

            } catch (Exception $e) {
                // Set fail response for other errors
                $result['error'] = true;
                $result['message'] = $this->__($e->getMessage());

                if ($this->getServiceHelper()->isDev()) {
                    $result['trace'] = $e->getTrace();
                }

                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            }
        }
    }

    /**
     * Gets the customer' phone numbers if he entered it in the Kontaktdaten section
     * @return void
     */
    public function getOfferTelephoneNumbersAction()
    {
        if ($this->getRequest()->isPost()) {
            try {
                $i = 0;
                $numbers = array();
                $quote = Mage::getSingleton('checkout/cart')->getQuote();
                $checkoutData = Mage::getModel('dyna_checkout/field')->loadQuoteData(array('quote_id' => $quote->getId()));

                $checkoutHelper = Mage::helper('omnius_checkout');

                while ($checkoutHelper->getCheckoutFieldData($checkoutData, "customer[contact_details][phone_list][$i][telephone]")) {
                    if ($checkoutHelper->getCheckoutFieldData($checkoutData, "customer[contact_details][phone_list][$i][telephone_type]") == Dyna_Checkout_Block_Cart_Steps_SaveCustomer::MOBILE_TELEPHONE_TYPE) {
                        $localAreaCode = $checkoutHelper->getCheckoutFieldData($checkoutData, "customer[contact_details][phone_list][$i][telephone_prefix]");
                        $phoneNumber = $checkoutHelper->getCheckoutFieldData($checkoutData, "customer[contact_details][phone_list][$i][telephone]");
                        $numbers[] = $localAreaCode . $phoneNumber;
                    }
                    $i++;
                }

                $data = empty(array_filter($numbers)) ? false : json_encode($numbers);

                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode(
                        array(
                            'error' => false,
                            'message' => false,
                            'data' => $data
                        )
                    )
                );
                return;
            } catch (Exception $e) {
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode(
                        array(
                            'error' => true,
                            'message' => $e->getMessage(),
                            'data' => false
                        )
                    )
                );
            }
        }
    }

    /**
     * View order details page
     */
    public function viewAction()
    {
        $orderNumber = $this->getRequest()->getParam('order_number');
        /** @var Dyna_Checkout_Helper_Data $checkoutHelper */
        $checkoutHelper = Mage::helper('dyna_checkout');
        $hasViewPermission = $checkoutHelper->checkAgentOrderPermissions($orderNumber);

        $checkoutHelper->updateOrderItemProducts($orderNumber);
        $this->loadLayout()
            ->getLayout()
            ->getBlock('checkout.order')
            ->setOrderNumber($orderNumber)
            ->setHasPermission($hasViewPermission);

        $this->renderLayout();
    }

    /**
     * Display order details
     */
    public function displayOrderAction()
    {
        if ($postData = $this->getRequest()->getPost()) {
            try {
                $orderNumber = $postData['orderNumber'];
                $orderStatus = $postData['orderStatus'];
                /** @var Dyna_Checkout_Helper_Data $checkoutHelper */
                $checkoutHelper = Mage::helper('dyna_checkout');
                $checkoutHelper->updateOrderItemProducts($orderNumber);
                $hasViewPermission = $checkoutHelper->checkAgentOrderPermissions($orderNumber);
                $order_overview_content =
                    $this->loadLayout()
                        ->getLayout()
                        ->getBlock('checkout.order')
                        ->setOrderNumber($orderNumber)
                        ->setOrderStatus($orderStatus)
                        ->setHasPermission($hasViewPermission)
                        ->toHtml();

                $response = array(
                    "error" => false,
                    "data" => $order_overview_content
                );

                $this->setJsonResponse($response);
                return;
            } catch (\Exception $e) {
                Mage::logException($e);
                $this->setJsonResponse([
                    'error' => true,
                    'message' => $e->getMessage()
                ]);
            }
        }
    }

    /**
     * Get notes on package
     */
    public function getNotesOfPackageAction()
    {
        if ($postData = $this->getRequest()->getPost()) {
            try {
                $packageId = $postData['packageId'];
                /** @var Dyna_Checkout_Model_Sales_Quote $quote */
                $quote = Mage::getSingleton('checkout/cart')->getQuote();

                $package = $quote->getCartPackage($packageId);
                $package->setData('items', $package->getAllItems());

                // package title
                $packageTypeAndTariffTitles = Mage::helper('dyna_package')->getTypeAndSubtypeTitle($package, Dyna_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION);
                $response['packageTitle'] = $packageTypeAndTariffTitles['packageSubtypeTitle'] ?: '';
                // package notes
                $packagesNotes = Mage::getModel('dyna_checkout/packageNotes')
                    ->getCollection()
                    ->addFieldToFilter('package_id', $package->getEntityId());

                if ($postData['noteId']) {
                    $packagesNotes->addFieldToFilter('entity_id', $postData['noteId']);
                }

                foreach ($packagesNotes as $packageNote) {
                    $lastUpdateAgentDetails = json_decode($packageNote->getCreatedAgent());
                    foreach ($lastUpdateAgentDetails as $key => $value) {
                        $packageNote->setData('agent_' . $key, $value);
                    }
                    $packageStatus = $packageNote->getCreatedDate() == $packageNote->getLastUpdateDate() ? strtoupper($this->__("Created by ")) : strtoupper($this->__("Last edited by "));
                    $packageNote->setPackageAgentStatus($packageStatus);
                    $packageNote->setCreatedDate(date('d.m.Y H:i', strtotime($packageNote->getCreatedDate())));
                    $packageNote->setLastUpdateDate(date('d.m.Y H:i', strtotime($packageNote->getLastUpdateDate())));
                    $packageNote->setData('note', htmlspecialchars($packageNote->getData('note')));
                    $response['notes'][] = $packageNote->getData();
                }

                $this->setJsonResponse($response);
                return;
            } catch (\Exception $e) {
                Mage::logException($e);
                $this->setJsonResponse([
                    'error' => true,
                    'message' => $e->getMessage()
                ]);
            }
        }
    }

    /**
     * Set note on package
     */
    public function setNoteOnPackageAction()
    {
        if ($postData = $this->getRequest()->getPost()) {
            try {
                $note = strip_tags($postData['note']);
                $createdByAgent = $this->getAgentDetailsForNote();

                $localizeDate = new DateTime("now", new DateTimeZone(Mage::getStoreConfig('general/locale/timezone')));
                $createdDate = $localizeDate->format('Y-m-d H:i:s');

                /** @var Dyna_Checkout_Model_Sales_Quote $quote */
                $quote = Mage::getSingleton('checkout/cart')->getQuote();

                $packageId = $postData['packageId'];
                $cartPackage = $quote->getCartPackage($packageId);

                $packagesNotes = Mage::getModel('dyna_checkout/packageNotes')
                    ->getCollection()
                    ->addFieldToFilter('package_id', $cartPackage->getEntityId());

                if (0 < $packagesNotes->count()) {
                    $this->setJsonResponse([
                        'error' => true,
                        'message' => $this->__("Note for this package already exists.")
                    ]);
                    return;
                }

                // add note on package
                $noteDetails = Mage::getModel('dyna_checkout/packageNotes')
                    ->setPackageId($cartPackage->getEntityId())
                    ->setQuoteId($cartPackage->getQuoteId())
                    ->setCreatedDate($createdDate)
                    ->setCreatedAgent($createdByAgent)
                    ->setLastUpdateDate($createdDate)
                    ->setLastUpdateAgent($createdByAgent)
                    ->setNote($note);
                $noteDetails->save();

                $this->setJsonResponse(array('noteId' => $noteDetails->getId()));
                return;
            } catch (\Exception $e) {
                Mage::logException($e);
                $this->setJsonResponse([
                    'error' => true,
                    'message' => $e->getMessage()
                ]);
            }
        }
    }

    /**
     * Edit note
     */
    public function editNoteOnPackageAction()
    {
        if ($postData = $this->getRequest()->getPost()) {
            try {
                $noteId = $postData['noteId'];
                $updatedNote = strip_tags($postData['note']);
                $updatedByAgent = $this->getAgentDetailsForNote();

                $localizeDate = new DateTime("now", new DateTimeZone(Mage::getStoreConfig('general/locale/timezone')));
                $updatedDate = $localizeDate->format('Y-m-d H:i:s');
                $updatedData = array(
                    'last_update_date' => $updatedDate,
                    'last_update_agent' => $updatedByAgent,
                    'note' => $updatedNote
                );

                if (strlen($updatedNote) > 0) {
                    // edit note
                    Mage::getModel('dyna_checkout/packageNotes')->addData($updatedData)->setId($noteId)->save();
                } else {
                    // delete note
                    $note = Mage::getModel('dyna_checkout/packageNotes')->load($noteId);
                    $note->delete();

                    $packagesNotes = Mage::getModel('dyna_checkout/packageNotes')
                        ->getCollection()
                        ->addFieldToFilter('package_id', $note->getPackageId());

                    $this->setJsonResponse(array('total' => count($packagesNotes)));
                }
            } catch (\Exception $e) {
                Mage::logException($e);
                $this->setJsonResponse([
                    'error' => true,
                    'message' => $e->getMessage()
                ]);
            }
        }
    }

    /**
     * Return agent details for the creation/edit of the current note
     * @return string
     */
    public function getAgentDetailsForNote()
    {
        $session = Mage::getSingleton('customer/session');
        /** @var Dyna_Agent_Model_Agent $agent */
        $agent = $session->getAgent();
        if ($agent) {
            /** @var Dyna_Agent_Model_Agent $superAgent */
            $superAgent = $session->getSuperAgent();
            $agentId = $superAgent ? $superAgent->getAgentId() : $agent->getAgentId();
            $agentFirstName = $superAgent ? $superAgent->getFirstName() : $agent->getFirstName();
            $agentLastName = $superAgent ? $superAgent->getLastName() : $agent->getLastName();
            $agentEmail = $superAgent ? $superAgent->getEmail() : $agent->getEmail();
            $agentPhone = $superAgent ? $superAgent->getPhone() : $agent->getPhone();
            $createdAgent = json_encode(array(
                'id' => $agentId,
                'name' => $agentFirstName . ' ' . $agentLastName,
                'email' => $agentEmail,
                'phone' => $agentPhone
            ));
        }

        return $createdAgent;
    }

    /**
     * Check if offer should be prohibited or not due to missing salesIds / attributes
     * @return array
     */
    protected function checkOfferProhibition()
    {
        $result['error'] = false;
        /** @var Dyna_Checkout_Helper_Data $checkoutHelper */
        $checkoutHelper = Mage::helper('dyna_checkout');

        //verify if agent has permission to save offer
        /** @var Dyna_Agent_Model_Dealer $agent */
        $agent = Mage::getSingleton('dyna_customer/session')->getAgent();
        if (!$agent->getId() || !$agent->isGranted(array('SEND_OFFER'))) {
            $result['error'] = true;
            $result['agentError'] = true;
            $result['message'] = $this->__('You do not have permission to send an offer');
        }

        // if sales ids triple is not existing, display corresponding error message when trying to save the offer
        $salesIdsTriple = $checkoutHelper->checkSalesIdTriple();
        if (isset($salesIdsTriple['error'])) {
            $result['error'] = true;
            $result['salesIdError'] = true;
            $result['message'] = $salesIdsTriple['message'];
        }

        // if offer has a missing attribute (either Marketing Code, Campaign Code or Werbe Code), display an error
        $cableOfferAttributes = $checkoutHelper->checkCableOfferAttributes();
        if (!$cableOfferAttributes) {
            $result['error'] = true;
            $result['attributesError'] = true;
            $result['message'] = $this->__('This offer is missing (one of) the following attributes: Marketing Code, Campaign Code, Werbe Code');
        }

        return $result;
    }
    
    /**
     * This function is written specifically to handle the cyclomatic complexity
     * to fetch the customer data
     * @param $requestData
     * @return void
     */
    protected function setCustomerData($requestData)
    {
        $this->setVariable($requestData, 'dob');
        $this->setVariable($requestData, 'firstname');
        $this->setVariable($requestData, 'lastname');
        $this->setVariable($requestData, 'telephone');
        $this->setVariable($requestData, 'prefix');
        $this->vars['email'] = $requestData->get('email_send', null);
        $this->vars['additional_telephone'] = (substr($this->vars['prefix'], 0, 1) === '0' ? substr($this->vars['prefix'], 1) : $this->vars['prefix']) . $this->vars['telephone'];
        if ($this->guestCustomerIsSoho) {
            $this->setVariable($requestData, 'companyname');
        }
    }
    
    protected function setVariable($data, $variable)
    {
        $this->vars[$variable] = $data->get($variable, null);
    }

    /**
     * This function is written specifically to handle the cyclomatic complexity
     * to save new prospect customer
     * @return void
     */
    protected function saveNewProspectCustomerIfNeeded()
    {
        if (!$this->prospectCustomer) {
            $this->saveNewProspectCustomer();
        }
    }

    /**
     * This function is written specifically to handle the cyclomatic complexity
     * save the cart on any update
     * @param $doCartSave
     * @param $cart
     * @return void
     */
    protected function saveCartOnUpdate($doCartSave, $cart)
    {
        if ($doCartSave) {
            $cart->save();
        }
    }

    /**
     * This function is written specifically to handle the cyclomatic complexity
     * to get all available items
     * @param $quote
     * @return array
     */
    protected function getAvailableItems($quote)
    {
        $availableItems = array();
        foreach ($quote->getAllItems() as $item) {
            $availableItems[] = $item->getSku();
        }
        return $availableItems;
    }

    public function searchIndustriesAction()
    {
        $query = $this->getRequest()->getParam('query');
        $result = Mage::helper('agentde')->getIndustryList($query);
        $this->getResponse()
            ->setHeader('Content-Type', 'application/json')
            ->setBody(
                Mage::helper('core')->jsonEncode(array('query' => $query, 'suggestions' => $result))
            );
    }

    /**
     * This function is written specifically to handle the cyclomatic complexity
     * to preserve product snapshot
     * @param $packages
     * @return void
     */
    protected function preserveProductsSnapshot($packages)
    {
        foreach ($packages as $package) {
            $package->buildChecksum();
        }
    }

    /**
     * This function is written specifically to handle the cyclomatic complexity
     * setting call back date and time
     * @param $requestData
     * @return void
     */
    protected function setCallBackDateTimeQuote($requestData)
    {
        $callbackDate = $requestData->get('callback_date', null);
        $callbackTime = $requestData->get('callback_time', null);
        if ($callbackDate && $callbackTime) {
            $dbDate = new DateTime($callbackDate . ' ' . $callbackTime . ":00");
            $this->theQuote->setCallbackDatetime($dbDate->format('Y-m-d H:i:s'));
        }
    }

    /**
     * This function is written specifically to handle the cyclomatic complexity
     * to check SEPA is valid or not
     * @param $isValidSEPA
     * @return boolean
     */
    protected function checkIfValidSEPA($isValidSEPA)
    {
        if (!empty($isValidSEPA)) {
            $validAddress = true;
        } else {
            $validAddress = false;
        }
        return $validAddress;
    }

    /**
     * This function is written specifically to handle the cyclomatic complexity
     * to set yellow or blue Id's
     * @param $salesIdsTriple
     * @return array
     */
    protected function getYellowBlueIds($salesIdsTriple)
    {
        if ($salesIdsTriple && (empty($salesIdsTriple['error']))) {
            $return['yellow_sales_id'] = $salesIdsTriple["fn"];
            $return['blue_sales_id'] = $salesIdsTriple["kd"];
        } else {
            $return['yellow_sales_id'] = null;
            $return['blue_sales_id'] = null;
        }
        return $return;
    }

    /**
     * This function is written specifically to handle the cyclomatic complexity
     * to send the UCT progress
     * @param $uctParams
     * @return void
     */
    protected function sendTheUCTProgress($uctParams)
    {
        if (isset($uctParams["transactionId"])) {
            $this->theQuote->setTransactionId($uctParams["transactionId"]);
            Mage::helper('dyna_checkout/data')->sendUCTReportProgress(null, $this->theQuote);
        }
    }
}
