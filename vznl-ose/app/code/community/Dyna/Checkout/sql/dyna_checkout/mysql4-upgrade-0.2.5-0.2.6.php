<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $salesInstaller Mage_Sales_Model_Resource_Setup */
$salesInstaller = new Mage_Sales_Model_Resource_Setup('core_setup');
$salesInstaller->startSetup();
$tables = ['quote', 'order', 'quote_address'];

$options = [
    'type' => Varien_Db_Ddl_Table::TYPE_DECIMAL,
    'precision' => 12,
    'scale' => 4,
    'required' => false
];

$attributes = [
    'initial_mixmatch_maf_subtotal' => 'Initial mixmatch maf subtotal',
    'initial_mixmatch_maf_tax'      => 'Initial mixmatch maf tax',
    'initial_mixmatch_maf_total'    => 'Initial mixmatch maf grand total'

];

foreach($tables as $table) {
    foreach($attributes as $key => $comment){
        $tempOps = $options;
        $tempOps['comment'] = $comment;
        $salesInstaller->addAttribute($table, $key, $tempOps);
    }
}

// Custom MAF attributes
$customMafAttributes = [
    'custom_maf' => 'Custom maf price',
    'original_custom_maf' => 'Original custom maf price',
];

foreach($customMafAttributes as $key => $comment){
    $tempOps = $options;
    $tempOps['comment'] = $comment;
    $salesInstaller->addAttribute('quote_item', $key, $tempOps);
}

/* end QUOTE and ORDER item attributes */
$salesInstaller->endSetup();
