<?php
$floorOptions =
    'Keller
EG
01
02
03
04
05
06
07
08
09
10
11
12
13
14
15
16
17
18
19
20
20+';

$prefixOptions =
    'Dipl.-Betriebswirt
Dipl.-Betriebswirt(BA)
Dipl.-Betriebswirt(FH)
Dipl.-Finanzwirt
Dipl.-Finanzwirtin
Dipl.-Ing.
Dipl.-Ing.(B,A)
Dipl.-Ing.(FH)
Dipl.-Ing.oec.
Dipl.-Kfm.
Dipl.-Med.
Dipl.-Oec.
Dipl.-Psychologe
Dipl.-Psychologin
Dipl.-Volkswirt
Dipl.-Wirtschaftsing.
Dipl.-Wirtschaftsing.(FH)
Dr.
Dr. Dr.
Dr.-Ing.
Dr.jur.
Dr.med.
Dr.med.dent.
Dr.rer.nat.
Prof.
Prof. Dr.
Prof. Dr.-Ing.
Prof. Dr.med.';

Mage::getConfig()->saveConfig('checkout/options/max_levels' , $floorOptions, 'default', 0);

Mage::getConfig()->saveConfig('checkout/options/prefix_list' , $prefixOptions, 'default', 0);

Mage::getModel('core/config')->cleanCache();