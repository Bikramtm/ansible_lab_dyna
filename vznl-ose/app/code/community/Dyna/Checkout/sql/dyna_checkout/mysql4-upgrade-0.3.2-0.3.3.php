<?php

$contentCss = <<< End
    <style>
        @page {
            margin: 0px;
        }

        body {
            color: #000000;
            font-size: 14px;
            line-height: 22px;
            font-family: "Arial Rounded MT", Arial, Helvetica, sans-serif;
            margin: 30px 50px 20px 50px;
        }

        h1, h2 {
            color: #E60000;
            margin: 0;
        }

        h3, h4 {
            margin: 0;
        }

        h1 {
            color: #E60000;
            font-size: 28px;
            line-height: 32px;
            font-family: "Arial Rounded MT", Arial, Helvetica, sans-serif;
            font-weight: bold;
        }

        h2 {
            font-size: 24px;
            line-height: 28px;
            font-weight: normal;
        }

        .ghost-table {
            border: none;
        }

        .ghost-table td {
            background: none
        }

        .greetings {
            padding-top: 30px;
        }

        .call-back dl {
            margin: 1em 0;
        }

        .call-back dt {
            float: left;
            width: 60px;
        }

        .ending {
            line-height: 2;
        }

        table {
            width: 100%;
            border-collapse: collapse;
            border-style: solid;
            border-width: thin;
            border-color: #cbcbcb;
            margin-top: 10px;
            margin-bottom: 40px;
            padding: 0;
        }

        th, td {
            padding: 15px 10px;
            text-align: left;
        }

        td {
            padding: 8px 10px;
            background-color: #eeeeee;
        }

        .borderless {
            border-style: none;
        }

        .no-bg {
            background-color: transparent;
        }

        .center {
            border-left-style: solid;
            border-right-style: solid;
            border-width: thin;
            border-color: #cbcbcb;
        }

        .brder-top {
            border-top-style: solid;
            border-width: thin;
            border-color: #cbcbcb;
        }

        .brder-bottom {
            border-bottom-style: solid;
            border-width: thin;
            border-color: #cbcbcb;
        }

        .brder-left {
            border-left-style: solid;
            border-width: thin;
            border-color: #cbcbcb;
        }

        .brder-right {
            border-right-style: solid;
            border-width: thin;
            border-color: #cbcbcb;
        }

        .brder {
            border-style: solid;
            border-width: thin;
            border-color: #cbcbcb;
        }

        .additional {
            font-size: smaller;
        }

        .narrow {
            width: 80px;
        }

        .wider {
            width: 360px;
        }

        .mwst {
            font-weight: normal;
            font-size: smaller;
        }

        .table-spacing {
            border-style: none;
            height: 20px;
        }

        .empty-row {
            border-bottom-style: solid;
            border-bottom-width: thin;
        }

        .title h1 {
            padding: 50px 0 45px 85px;
        }

        .personal-data {
            padding-bottom: 30px;
        }

        .presonal-data {
            font-size: 16px;
            line-height: 20px;
        }

        .personal-data td {
            padding: 10px 20px 10px 0;
        }

        .offer-table {
            margin-top: 2em;
        }

        .offer-table h2 {
            padding-top: 20px;
        }

        .price {
            text-align: right;
            font-weight: bold;
            color: #262626;
            font-size: 16px;
            line-height: 22px;
        }

        img.top-logo {
            position: absolute;
            top: -30px;
            left: 0px;
        }

        .adresses {
            padding: 10px 20px 10px 0px;
            vertical-align: top;
            display: inline-block;
        }

        h3.delivery-address {
            margin-bottom: 1em;
        }

        .iamge-cell {
            vertical-align: top;
        }

        .delivery-img {
            height: 2.5em;
        }

        .discount {
            color: #427D00;
        }

        h3 {
            color: #333333;
            font-size: 20px;
            line-height: 24px;
        }

        h4 {
            color: #262626;
            font-size: 16px;
            line-height: 22px;
        }

        h5 {
            color: #262626;
            font-size: 14px;
            line-height: 24px;
            margin: 0;
        }

        td.item-name {
            color: #333333;
            font-size: 14px;
            line-height: 20px;
        }

        .price-bold {
            font-size: 20px;
            line-height: 24px;
        }

        .black h4 {
            color: #000000;
        }
        .ghost-table td {
            padding: 0;
            vertical-align: text-top;
        }

        .ghost-table th {
            padding: 0;
        }
        td.monthly, th.monthly {
            width: 18.57%;
        }
        td.once, th.once {
            width: 17.14%;
        }
        tr.empty-row td {
            height: 30px;
            padding: 0;
        }
        .sum-table {
            margin-bottom: 24px;
        }
        .order-table {
            margin-bottom: 40px;
            margin-top: 0;
        }
        .hidden{
            display: none;
            visibility: hidden;
        }
        .order-no {
            border: none;
            margin-bottom: 1em;
        }
        .goes-by {
            margin-top: 2em;
        }
        .personal-data-table {
            margin-bottom: 0;
        }
    </style>
End;

$contentCable = <<< End
<div class="title">
    <img class="top-logo" src="{{var RhombusRedLogoPath}}" /><h1>Bestellbestätigung</h1>
</div>
<div class="greetings">
    {{var greetings}},<br><br>
    vielen Dank für Ihre Bestellung. Hier nochmal alles Wichtige im Überblick:<br><br>
</div>

<table class="ghost-table personal-data-table">
    <tr>
        <th colspan="2">
            <h2>Ihre persönlichen Daten</h2>
        </th>
    </tr>
    <tr>
        <td class="first-column">
            <table class="order-no">
                <tbody>
                    <tr>
                        <td>
                            {{var name}}<br>
                            {{var address}}<br>
                            {{var city}}<br>
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
        <td>
            <table class="order-no">
                <tbody>
                    <tr>
                        <td class="narrow"><strong>Bestellnummer</strong></td>
                        <td><span>: {{var orderId}}</span></td>
                    </tr>
                    <tr>
                        <td class="narrow"><strong>Bestelldatum</strong></td>
                        <td><span>: {{var orderDate}}</span></td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td class="first-column">
            <div class="adresses">
                <h3 class="delivery-address">Installationsadresse:</h3>
                {{var deliveryDataName}}<br>
                {{var seviceDataStreet}} {{var serviceDataHouseNo}} {{var serviceDataAddition}}<br>
                {{var serviceDataPostcode}} {{var serviceDataCity}}<br>
            </div>
        </td>
        <td>
            <div class="adresses">
                <h3 class="delivery-address">Lieferadresse:</h3>
                {{var deliveryDataName}} <br>
                {{var deliveryDataAddress}} <br>
                {{var deliveryDataCity}} <br>
            </div>
        </td>
    </tr>
</table>
<div class="offer-table">
    {{var orderTable}}
</div>
<div class="goes-by">
    <h2>So geht es weiter</h2>
    <table>
        <tr>
            <td class="no-bg brder-left brder-top brder-bottom image-cell">
                <img class="delivery-img" src="{{var mailIconPath}}" />
            </td>
            <td class="black no-bg brder-right brder-top brder-bottom">
                <h4>Sie erhalten in Kürze Ihre Auftragsbestätigung.</h4>
                Darin teilen wir Ihnen Ihre Bestellnummer mit. Nachdem wir Ihre Bestellung bearbeitet haben - dies dauert in der Regel 1 bis 2 Werktage - erhalten Sie alle notwendigen Unterlagen per Post von uns.
            </td>
        </tr>
    </table>
</div>
<div class="footer">
    <p>
        <strong>Bitte beachten Sie: </strong> Diese Zusammenfassung dient ausschließlich der Bestätigung des Einganges Ihrer Bestellung und stellt noch keine Annahme Ihres Angebotes auf Abschluss eines Vertrages dar.
    </p>
</div>
<br>
Vielen Dank für Ihr Vertrauen!
<br><br><br>
Freundliche Grüße
<br>
Ihr Vodafone-Team
End;

$contentCableMigration = $contentCable;

$contentMobile = <<< End
<div class="title">
    <img class="top-logo" src="{{var RhombusRedLogoPath}}" /><h1>Bestellbestätigung</h1>
</div>
<div class="greetings">
    {{var greetings}},<br><br>
    vielen Dank für Ihre Bestellung. Hier nochmal alles Wichtige im Überblick:<br><br>
</div>

<table class="ghost-table personal-data-table">
    <tr>
        <th colspan="2">
            <h2>Ihre persönlichen Daten</h2>
        </th>
    </tr>
    <tr>
        <td class="first-column">
            <table class="order-no">
                <tbody>
                    <tr>
                        <td>
                            {{var name}}<br>
                            {{var address}}<br>
                            {{var city}}<br>
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
        <td>
            <table class="order-no">
                <tbody>
                    <tr>
                        <td class="narrow"><strong>Bestellnummer</strong></td>
                        <td><span>: {{var orderId}}</span></td>
                    </tr>
                    <tr>
                        <td class="narrow"><strong>Bestelldatum</strong></td>
                        <td><span>: {{var orderDate}}</span></td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td class="first-column">
            <div class="adresses">
                <h3 class="delivery-address">Lieferadresse:</h3>
                {{var deliveryDataName}} <br>
                {{var deliveryDataAddress}} <br>
                {{var deliveryDataCity}} <br>
            </div>
        </td>
    </tr>
</table>
<div class="offer-table">
    {{var orderTable}}
</div>
<div class="goes-by">
    <h2>So geht es weiter</h2>
    <table>
        <tr>
            <td class="no-bg brder-left brder-top brder-bottom image-cell">
                <img class="delivery-img" src="{{var truckIconPath}}" />
            </td>
            <td class="black no-bg brder-right brder-top brder-bottom">
                <h4>1. Ihre Ware wird in Kürze zugestellt.</h4>
                Unser Versandpartner DHL bringt Ihre Bestellung zu Ihnen.
            </td>
        </tr>
        <tr>
            <td class="no-bg brder-left brder-top brder-bottom image-cell">
                <img class="delivery-img" src="{{var iCardIconPath}}" />
            </td>
            <td class="black no-bg brder-right brder-top brder-bottom">
                <h4>2. Nächste Schritte.</h4>
                Bitte seien Sie vor Ort. Halten Sie bitte Ihren Ausweis bereit, um sich zu identifizieren und den Vertrag zu unterschreiben. Wenn Sie sich mit Ihrem Reisepass ausweisen möchten, legen Sie bitte zusätzlich eine Meldebescheinigung vor. Sie bekommen die passende SIM-Karte zu Ihrem Gerät mit Ihrem Paket.
            </td>
        </tr>
    </table>
</div>
<div class="footer">
    <p>
        <strong>Bitte beachten Sie: </strong> Diese Zusammenfassung dient ausschließlich der Bestätigung des Einganges Ihrer Bestellung und stellt noch keine Annahme Ihres Angebotes auf Abschluss eines Vertrages dar. Ihr Vertrag über Mobilfunkdienstleistungen kommt mit Aktivierung/Freischaltung der Mobilfunkdienstleistung bzw. soweit Sie nur Hardware oder Zubehör bestellt haben, mit dem Versand des Artikels an Sie zustande.
    </p>
    {{if preliminaryBasePriceFootnote}}
        <p>
            <strong>Basispreis im Voraus: </strong> Im Vodafone-Red-Tarif zahlen Sie den monatlichen Basispreis immer im Voraus. Auf der 1. Rechnung in Ihrem neuen Tarif zahlen Sie den Basispreis für den nächsten Monat und den anteiligen Basispreis für den laufenden Monat. Selbstverständlich wird Ihnen auf Ihrer letzten Rechnung kein weiterer Basispreis berechnet.
        </p>
    {{/if}}
    {{if isSpeedGo}}
        <p>
            <strong>Vodafone SpeedGo: </strong> Für Ihr bestes Surferlebnis informieren wir Sie per SMS bei Verbrauch von 90% Ihrer Inklusiv-Daten und schalten bei 100% weitere MB frei. Je nach Tarif 100 MB für 2€ oder 250 MB für 3€. Maximal 3-mal in Folge. Sie können immer per SMS ablehnen - dann surfen Sie ab Verbrauch von 100% langsamer mit bis zu 32 kbit/s.
        </p>
    {{/if}}
</div>
<br>
Vielen Dank für Ihr Vertrauen!
<br><br><br>
Freundliche Grüße
<br>
Ihr Vodafone-Team
End;

$contentGigakombiCable = <<< End
<div class="title">
    <img class="top-logo" src="{{var RhombusRedLogoPath}}" /><h1>Bestellbestätigung</h1>
</div>
<div class="greetings">
    {{var greetings}},<br><br>
    vielen Dank für Ihre Bestellung. Hier nochmal alles Wichtige zu Ihrem Kabel-Vertrag mit GigaKombi im Überblick:<br><br>
</div>

<table class="ghost-table personal-data-table">
    <tr>
        <th colspan="2">
            <h2>Ihre persönlichen Daten</h2>
        </th>
    </tr>
    <tr>
        <td class="first-column">
            <table class="order-no">
                <tbody>
                    <tr>
                        <td>
                            {{var name}}<br>
                            {{var address}}<br>
                            {{var city}}<br>
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
        <td>
            <table class="order-no">
                <tbody>
                    <tr>
                        <td class="narrow"><strong>Bestellnummer</strong></td>
                        <td><span>: {{var orderId}}</span></td>
                    </tr>
                    <tr>
                        <td class="narrow"><strong>Bestelldatum</strong></td>
                        <td><span>: {{var orderDate}}</span></td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td class="first-column">
            <div class="adresses">
                <h3 class="delivery-address">Installationsadresse:</h3>
                {{var deliveryDataName}}<br>
                {{var seviceDataStreet}} {{var serviceDataHouseNo}} {{var serviceDataAddition}}<br>
                {{var serviceDataPostcode}} {{var serviceDataCity}}<br>
            </div>
        </td>
        <td>
            <div class="adresses">
                <h3 class="delivery-address">Lieferadresse:</h3>
                {{var deliveryDataName}} <br>
                {{var deliveryDataAddress}} <br>
                {{var deliveryDataCity}} <br>
            </div>
        </td>
    </tr>
</table>
<div class="offer-table">
    {{var orderTable}}
</div>
<div>
    <div>
        <p>
            Gut zu wissen:  Die GigaKombi löst Red One ab. In Ihren Dokumenten heißt die GigaKombi aber noch eine Zeit lang Red One.<br>
        </p>
        <span>Ihre Mobilfunk-Vorteile</span>
        <ul>
            <li>10 Euro mtl. Rabatt auf Mobilfunkrechnung</li>
            <li>5 Euro mtl. Rabatt auf bis zu 4 Red+ Allnet</li>
            <li>WiFi Calling kostenlos</li>
        </ul>
        <h3>Ihre Festnetz-Vorteile</h3>
        <ul>
            <li>Mobilfunk-Flat aus dem Festnetz</li>
            <li>2 Euro Rabatt auf WLAN-Option bei Buchung eines Kabelanschlusses</li>
        </ul>
        <p>
            Rabatte werden gewährt, sobald der Festnetzauftrag aktiv ist.
        </p>
    </div>
</div>
<div class="goes-by">
    <h2>So geht es weiter</h2>
    <table>
        <tr>
            <td class="no-bg brder-left brder-top brder-bottom image-cell">
                <img class="delivery-img" src="{{var mailIconPath}}" />
            </td>
            <td class="black no-bg brder-right brder-top brder-bottom">
                <h4>Sie erhalten in Kürze Ihre Auftragsbestätigung.</h4>
                Darin teilen wir Ihnen Ihre Bestellnummer mit. Nachdem wir Ihre Bestellung bearbeitet haben - dies dauert in der Regel 1 bis 2 Werktage - erhalten Sie alle notwendigen Unterlagen per Post von uns.
            </td>
        </tr>
    </table>
</div>
<div class="footer">
    <div class="possible-soc">
        <div>
            <p>
                Gut zu wissen:  Die GigaKombi löst Red One ab. In Ihren Dokumenten heißt die GigaKombi aber noch eine Zeit lang Red One.<br>
            </p>
            <b>Ihre Mobilfunk-Vorteile</b>
            <ul>
                <li>10 Euro mtl. Rabatt auf Mobilfunkrechnung</li>
                <li>5 Euro mtl. Rabatt auf bis zu 4 Red+ Allnet</li>
                <li>WiFi Calling kostenlos</li>
            </ul>
            <b>Ihre Festnetz-Vorteile</b>
            <ul>
                <li>Mobilfunk-Flat aus dem Festnetz</li>
                <li>2 Euro Rabatt auf WLAN-Option bei Buchung eines Kabelanschlusses</li>
            </ul>
            <p>
                Rabatte werden gewährt, sobald der Festnetzauftrag aktiv ist.
            </p>
        </div>
    </div>
    <p>
        <strong>Bitte beachten Sie: </strong> Diese Zusammenfassung dient ausschließlich der Bestätigung des Einganges Ihrer Bestellung und stellt noch keine Annahme Ihres Angebotes auf Abschluss eines Vertrages dar.
    </p>
</div>
<br>
Vielen Dank für Ihr Vertrauen!
<br><br><br>
Freundliche Grüße
<br>
Ihr Vodafone-Team
End;

$contentGigakombiCableMigration = $contentGigakombiCable;

$contentGigakombiMobile = <<< End
<div class="title">
    <img class="top-logo" src="{{var RhombusRedLogoPath}}" /><h1>Bestellbestätigung</h1>
</div>
<div class="greetings">
    {{var greetings}},<br><br>
    vielen Dank für Ihre Bestellung. Hier nochmal alles Wichtige zu Ihrem Mobilfunkvertrag mit GigaKombi im Überblick:<br><br>
</div>

<table class="ghost-table personal-data-table">
    <tr>
        <th colspan="2">
            <h2>Ihre persönlichen Daten</h2>
        </th>
    </tr>
    <tr>
        <td class="first-column">
            <table class="order-no">
                <tbody>
                    <tr>
                        <td>
                            {{var name}}<br>
                            {{var address}}<br>
                            {{var city}}<br>
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
        <td>
            <table class="order-no">
                <tbody>
                    <tr>
                        <td class="narrow"><strong>Bestellnummer</strong></td>
                        <td><span>: {{var orderId}}</span></td>
                    </tr>
                    <tr>
                        <td class="narrow"><strong>Bestelldatum</strong></td>
                        <td><span>: {{var orderDate}}</span></td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td class="first-column">
            <div class="adresses">
                <h3 class="delivery-address">Lieferadresse:</h3>
                {{var deliveryDataName}} <br>
                {{var deliveryDataAddress}} <br>
                {{var deliveryDataCity}} <br>
            </div>
        </td>
    </tr>
</table>
<div class="offer-table">
    {{var orderTable}}
</div>
<div>
    <div>
        <p>
            Gut zu wissen:  Die GigaKombi löst Red One ab. In Ihren Dokumenten heißt die GigaKombi aber noch eine Zeit lang Red One.<br>
        </p>
        <span>Ihre Mobilfunk-Vorteile</span>
        <ul>
            <li>10 Euro mtl. Rabatt auf Mobilfunkrechnung</li>
            <li>5 Euro mtl. Rabatt auf bis zu 4 Red+ Allnet</li>
            <li>WiFi Calling kostenlos</li>
        </ul>
        <h3>Ihre Festnetz-Vorteile</h3>
        <ul>
            <li>Mobilfunk-Flat aus dem Festnetz</li>
            <li>2 Euro Rabatt auf WLAN-Option bei Buchung eines Kabelanschlusses</li>
        </ul>
        <p>
            Rabatte werden gewährt, sobald der Festnetzauftrag aktiv ist.
        </p>
    </div>
</div>
<div class="goes-by">
    <h2>So geht es weiter</h2>
    <table>
        <tr>
            <td class="no-bg brder-left brder-top brder-bottom image-cell">
                <img class="delivery-img" src="{{var truckIconPath}}" />
            </td>
            <td class="black no-bg brder-right brder-top brder-bottom">
                <h4>1. Ihre Ware wird in Kürze zugestellt.</h4>
                Unser Versandpartner DHL bringt Ihre Bestellung zu Ihnen.
            </td>
        </tr>
        <tr>
            <td class="no-bg brder-left brder-top brder-bottom image-cell">
                <img class="delivery-img" src="{{var iCardIconPath}}" />
            </td>
            <td class="black no-bg brder-right brder-top brder-bottom">
                <h4>2. Nächste Schritte.</h4>
                Bitte seien Sie vor Ort. Halten Sie bitte Ihren Ausweis bereit, um sich zu identifizieren und den Vertrag zu unterschreiben. Wenn Sie sich mit Ihrem Reisepass ausweisen möchten, legen Sie bitte zusätzlich eine Meldebescheinigung vor. Sie bekommen die passende SIM-Karte zu Ihrem Gerät mit Ihrem Paket.
            </td>
        </tr>
    </table>
</div>
<div class="footer">
    <div class="possible-soc">
        <div>
            <p>
                Gut zu wissen:  Die GigaKombi löst Red One ab. In Ihren Dokumenten heißt die GigaKombi aber noch eine Zeit lang Red One.<br>
            </p>
            <b>Ihre Mobilfunk-Vorteile</b>
            <ul>
                <li>10 Euro mtl. Rabatt auf Mobilfunkrechnung</li>
                <li>5 Euro mtl. Rabatt auf bis zu 4 Red+ Allnet</li>
                <li>WiFi Calling kostenlos</li>
            </ul>
            <b>Ihre Festnetz-Vorteile</b>
            <ul>
                <li>Mobilfunk-Flat aus dem Festnetz</li>
                <li>2 Euro Rabatt auf WLAN-Option bei Buchung eines Kabelanschlusses</li>
            </ul>
            <p>
                Rabatte werden gewährt, sobald der Festnetzauftrag aktiv ist.
            </p>
        </div>
    </div>
    <p>
        <strong>Bitte beachten Sie: </strong> Diese Zusammenfassung dient ausschließlich der Bestätigung des Einganges Ihrer Bestellung und stellt noch keine Annahme Ihres Angebotes auf Abschluss eines Vertrages dar. Ihr Vertrag über Mobilfunkdienstleistungen kommt mit Aktivierung/Freischaltung der Mobilfunkdienstleistung bzw. soweit Sie nur Hardware oder Zubehör bestellt haben, mit dem Versand des Artikels an Sie zustande.
    </p>
{{if preliminaryBasePriceFootnote}}
    <p>
        <strong>Basispreis im Voraus: </strong> Im Vodafone-Red-Tarif zahlen Sie den monatlichen Basispreis immer im Voraus. Auf der 1. Rechnung in Ihrem neuen Tarif zahlen Sie den Basispreis für den nächsten Monat und den anteiligen Basispreis für den laufenden Monat. Selbstverständlich wird Ihnen auf Ihrer letzten Rechnung kein weiterer Basispreis berechnet.
    </p>
{{/if}}
{{if isSpeedGo}}
    <p>
        <strong>Vodafone SpeedGo: </strong> Für Ihr bestes Surferlebnis informieren wir Sie per SMS bei Verbrauch von 90% Ihrer Inklusiv-Daten und schalten bei 100% weitere MB frei. Je nach Tarif 100 MB für 2€ oder 250 MB für 3€. Maximal 3-mal in Folge. Sie können immer per SMS ablehnen - dann surfen Sie ab Verbrauch von 100% langsamer mit bis zu 32 kbit/s.
    </p>
{{/if}}

</div>
<br>
Vielen Dank für Ihr Vertrauen!
<br><br><br>
Freundliche Grüße
<br>
Ihr Vodafone-Team
End;


// ############################
// Static block details array
// ############################

$staticBlockDetailsArray = [
    [
        'identifier' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_CABLE,
        'title' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_TITLE_CABLE,
        'content' => $contentCss.$contentCable,
    ],
    [
        'identifier' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_CABLE_MIGRATION,
        'title' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_TITLE_CABLE_MIGRATION,
        'content' => $contentCss.$contentCableMigration,
    ],
    [
        'identifier' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_MOBILE,
        'title' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_TITLE_MOBILE,
        'content' => $contentCss.$contentMobile,
    ],
    [
        'identifier' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_GIGAKOMBI_CABLE,
        'title' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_TITLE_GIGAKOMBI_CABLE,
        'content' => $contentCss.$contentGigakombiCable,
    ],
    [
        'identifier' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_GIGAKOMBI_CABLE_MIGRATION,
        'title' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_TITLE_GIGAKOMBI_CABLE_MIGRATION,
        'content' => $contentCss.$contentGigakombiCableMigration,
    ],
    [
        'identifier' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_GIGAKOMBI_MOBILE,
        'title' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_TITLE_GIGAKOMBI_MOBILE,
        'content' => $contentCss.$contentGigakombiMobile,
    ],

];


// ############################
// Logic starts here
// ############################

$store = Mage::getModel('core/store')
    ->getCollection()
    ->addFieldToFilter('code', ['eq'=>'admin'])
    ->getFirstItem();

foreach ($staticBlockDetailsArray as $key => $details) {
    /** @var Mage_Cms_Model_Block $blockModel */
    $blockModel = Mage::getModel('cms/block');
    $blockId = $blockModel
        ->load($details['identifier'])
        ->getId();

    if (!$blockId) {
        $blockModel->setTitle($details['title'])
            ->setIdentifier($details['identifier'])
            ->setStores(array($store->getId()))
            ->setIsActive(1)
            ->setContent($details['content'])
            ->save();
    }
}

