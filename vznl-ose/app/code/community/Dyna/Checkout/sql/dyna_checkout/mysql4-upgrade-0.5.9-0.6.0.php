<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;
$installer->startSetup();

// Creating a new table mobile_operator_servicer_providers if not exists
if($installer->getConnection()->isTableExists('mobile_operator_service_providers') != true) {
    $table = $installer->getConnection()
        ->newTable('mobile_operator_service_providers')
        ->addColumn(
            'entity_id',
            Varien_Db_Ddl_Table::TYPE_INTEGER,
            null,
            array(
                'identity' => true,
                'unsigned' => true,
                'nullable' => false,
                'primary' => true,
                'auto_increment' => true,
            ),
            'Entity Id'
        )
        ->addColumn('code', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(), 'Code')
        ->addColumn('description', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(), 'Description')
        ->addColumn('npind', Varien_Db_Ddl_Table::TYPE_TINYINT, 1, array(), 'Number Porting Indicator')
        ->addColumn('effective_date', Varien_Db_Ddl_Table::TYPE_DATE, 5, array(), 'Effective Date')
        ->addColumn('expiration_date', Varien_Db_Ddl_Table::TYPE_DATE, 5, array(), 'Expiration Date');

    $installer->getConnection()->createTable($table);
}

// Creating a new table mobile_porting_free_days if not exists
if($installer->getConnection()->isTableExists('mobile_porting_free_days') != true) {
    $table = $installer->getConnection()
        ->newTable('mobile_porting_free_days')
        ->addColumn(
            'entity_id',
            Varien_Db_Ddl_Table::TYPE_INTEGER,
            null,
            array(
                'identity' => true,
                'unsigned' => true,
                'nullable' => false,
                'primary' => true,
                'auto_increment' => true,
            ),
            'Entity Id'
        )
        ->addColumn('free_day', Varien_Db_Ddl_Table::TYPE_DATE, 5, array(), 'Free Date')
        ->addColumn('reason', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(), 'Reason');

    $installer->getConnection()->createTable($table);
}

$installer->endSetup();