<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Core_Model_Resource_Setup $this */
$installer = $this;
$installer->startSetup();
$connection = $installer->getConnection();

if (!$connection->tableColumnExists($installer->getTable('sales/quote_item'), 'preselect_product')) {
    $connection->addColumn($this->getTable('sales/quote_item'), 'preselect_product', [
            "type" => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
            "after" => "is_contract_drop",
            'default' => 1,
            "comment" => "Preselect value from product attribute"
        ]
    );
}
$installer->endSetup();