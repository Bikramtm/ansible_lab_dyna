<?php

/** @var Mage_Catalog_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();
$connection = $installer->getConnection();
$tableName = $installer->getTable('sales/quote_item');

$connection->addColumn($tableName, 'system_action', array(
    'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
    'length'      => 10,
    'default'   => false,
    'nullable'  => true,
    'comment'   => 'Flag for Added or Removed by System items'
));

$installer->endSetup();
