<?php
/**
 * Installer that creates the sap orders table
 */

/** @var Mage_Core_Model_Resource_Setup $this */
$installer = $this;

$installer->startSetup();

if (!$installer->getConnection()->isTableExists('sap_orders')) {
    $uctTable = new Varien_Db_Ddl_Table();
    $uctTable->setName('sap_orders');

    $uctTable
        ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, [
            "unsigned" => true,
            "primary" => true,
            "auto_increment" => true,
            'nullable' => false
        ], "Primary key for table")
        ->addColumn('order_id', Varien_Db_Ddl_Table::TYPE_VARCHAR, 100, [
            'nullable' => false
        ], "Order id")
        ->addColumn('package_id', Varien_Db_Ddl_Table::TYPE_VARCHAR, 11, [
            'nullable' => false
        ], "Package id")
        ->addColumn("ogw_id", Varien_Db_Ddl_Table::TYPE_VARCHAR, 50, [
            'nullable' => false
        ], "OGW id")
        ->addColumn("sap_data", Varien_Db_Ddl_Table::TYPE_TEXT, null, [
            'nullable' => false
        ], "SAP Data")
        ->addColumn('status', Varien_Db_Ddl_Table::TYPE_BOOLEAN, null, [
            'nullable' => false,
            'default' => false
        ], "Status");

    $this->getConnection()->createTable($uctTable);
}

$installer->endSetup();
