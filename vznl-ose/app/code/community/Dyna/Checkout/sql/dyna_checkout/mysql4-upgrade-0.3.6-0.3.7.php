<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $salesInstaller Mage_Sales_Model_Resource_Setup */
$salesInstaller = new Mage_Sales_Model_Resource_Setup('core_setup');
$salesInstaller->startSetup();

$orderItemAttr = array(
    'is_contract_drop' => array(
        'type'      => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
        'after'   => 'is_contract',
        'nullable'  => true,
        'comment'   => 'Flag for installed base products that were dropped during ILS'
    ),
);
foreach ($orderItemAttr as $attributeCode => $attributeProp) {
    $salesInstaller->addAttribute('quote_item', $attributeCode, $attributeProp);
    $salesInstaller->addAttribute('quote_address_item', $attributeCode, $attributeProp);
    $salesInstaller->addAttribute('order_item', $attributeCode, $attributeProp);
}

$salesInstaller->endSetup();
