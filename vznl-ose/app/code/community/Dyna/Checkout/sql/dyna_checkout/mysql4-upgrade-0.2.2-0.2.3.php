<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;
$installer->startSetup();

$installer->getConnection()->addColumn($this->getTable('package/package'), 'offer_id', 'varchar(255)');

$installer->endSetup();
