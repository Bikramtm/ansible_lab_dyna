<?php

$contentCss = <<< End
    <style>
        @page {
            margin: 0px;
        }

        body {
            color: #000000;
            font-size: 14px;
            line-height: 22px;
            font-family: "Arial Rounded MT", Arial, Helvetica, sans-serif;
            margin: 30px 50px 20px 50px;
        }

        h1, h2 {
            color: #E60000;
            margin: 0;
        }

        h3, h4 {
            margin: 0;
        }

        h1 {
            color: #E60000;
            font-size: 28px;
            line-height: 32px;
            font-family: "Arial Rounded MT", Arial, Helvetica, sans-serif;
            font-weight: bold;
        }

        h2 {
            font-size: 24px;
            line-height: 28px;
            font-weight: normal;
        }

        .ghost-table {
            border: none;
        }

        .ghost-table td {
            background: none
        }

        .greetings {
            padding-top: 30px;
        }

        .call-back dl {
            margin: 1em 0;
        }

        .call-back dt {
            float: left;
            width: 60px;
        }

        .ending {
            line-height: 2;
        }

        table {
            width: 100%;
            border-collapse: collapse;
            border-style: solid;
            border-width: thin;
            border-color: #cbcbcb;
            margin-top: 10px;
            margin-bottom: 40px;
            padding: 0;
        }

        th, td {
            padding: 15px 10px;
            text-align: left;
        }

        td {
            padding: 8px 10px;
            background-color: #eeeeee;
        }

        .borderless {
            border-style: none;
        }

        .no-bg {
            background-color: transparent;
        }

        .center {
            border-left-style: solid;
            border-right-style: solid;
            border-width: thin;
            border-color: #cbcbcb;
        }

        .brder-top {
            border-top-style: solid;
            border-width: thin;
            border-color: #cbcbcb;
        }

        .brder-bottom {
            border-bottom-style: solid;
            border-width: thin;
            border-color: #cbcbcb;
        }

        .brder-left {
            border-left-style: solid;
            border-width: thin;
            border-color: #cbcbcb;
        }

        .brder-right {
            border-right-style: solid;
            border-width: thin;
            border-color: #cbcbcb;
        }

        .brder {
            border-style: solid;
            border-width: thin;
            border-color: #cbcbcb;
        }

        .additional {
            font-size: smaller;
        }

        .narrow {
            width: 80px;
        }

        .wider {
            width: 360px;
        }

        .mwst {
            font-weight: normal;
            font-size: smaller;
        }

        .table-spacing {
            border-style: none;
            height: 20px;
        }

        .empty-row {
            border-bottom-style: solid;
            border-bottom-width: thin;
        }

        .title h1 {
            padding: 50px 0 45px 85px;
        }

        .personal-data {
            padding-bottom: 30px;
        }

        .presonal-data {
            font-size: 16px;
            line-height: 20px;
        }

        .personal-data td {
            padding: 10px 20px 10px 0;
        }

        .offer-table {
            margin-top: 2em;
        }

        .offer-table h2 {
            padding-top: 20px;
        }

        .price {
            text-align: right;
            font-weight: bold;
            color: #262626;
            font-size: 16px;
            line-height: 22px;
        }

        img.top-logo {
            position: absolute;
            top: -30px;
            left: 0px;
        }

        .adresses {
            padding: 10px 20px 10px 0px;
            vertical-align: top;
            display: inline-block;
        }

        h3.delivery-address {
            margin-bottom: 1em;
        }

        .iamge-cell {
            vertical-align: top;
        }

        .delivery-img {
            height: 2.5em;
        }

        .discount {
            color: #427D00;
        }

        h3 {
            color: #333333;
            font-size: 20px;
            line-height: 24px;
        }

        h4 {
            color: #262626;
            font-size: 16px;
            line-height: 22px;
        }

        h5 {
            color: #262626;
            font-size: 14px;
            line-height: 24px;
            margin: 0;
        }

        td.item-name {
            color: #333333;
            font-size: 14px;
            line-height: 20px;
        }

        .price-bold {
            font-size: 20px;
            line-height: 24px;
        }

        .black h4 {
            color: #000000;
        }
        .ghost-table td {
            padding: 0;
            vertical-align: text-top;
        }

        .ghost-table th {
            padding: 0;
        }
        td.monthly, th.monthly {
            width: 18.57%;
        }
        td.once, th.once {
            width: 17.14%;
        }
        tr.empty-row td {
            height: 30px;
            padding: 0;
        }
        .sum-table {
            margin-bottom: 24px;
        }
        .order-table {
            margin-bottom: 40px;
            margin-top: 0;
        }
        .hidden{
            display: none;
            visibility: hidden;
        }
        .order-no {
            border: none;
            margin-bottom: 1em;
        }
        .goes-by {
            margin-top: 2em;
        }
        .personal-data-table {
            margin-bottom: 0;
        }
    </style>
End;

$contentDsl = <<< End
<div class="title">
    <img class="top-logo" src="{{var RhombusRedLogoPath}}" /><h1>Bestellbestätigung</h1>
</div>
<div class="greetings">
    {{var greetings}},<br><br>
    Vielen Dank für Ihre Bestellung. Hier nochmal alles Wichtige im Überblick:<br><br>
</div>

<table class="ghost-table personal-data-table">
    <tr>
        <th colspan="2">
            <h2>Ihre persönlichen Daten</h2>
        </th>
    </tr>
    <tr>
        <td class="first-column">
            <table class="order-no">
                <tbody>
                    <tr>
                        <td>
                            {{var personalInformation}}
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
        <td>
            <table class="order-no">
                <tbody>
                    <tr>
                        <td class="narrow"><strong>Bestellnummer</strong></td>
                        <td><span>: {{var orderId}}</span></td>
                    </tr>
                    <tr>
                        <td class="narrow"><strong>Bestelldatum</strong></td>
                        <td><span>: {{var orderDate}}</span></td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
    {{var addresses}}
</table>
<div class="offer-table">
    {{var orderTable}}
</div>
<div>
    {{var lostProducts}}
</div>
<div>
    <p>Sie haben noch Fragen zur Bestellung? Sie erreichen uns täglich rund um die Uhr: <b>vodafone.de/kontakt</b></p>
    <p>Sie haben noch Fragen zu unseren Services und Geräten? Besuchen Sie unsere Hilfe-Seiten: <b>vodafone.de/hilfe</b></p>
</div>
<div>
    {{var wishDate}}
</div>
<div class="goes-by">
    {{var nextSteps}}
</div>
<div class="footer">
    {{var endingNote}}
</div>
<br>
Vielen Dank für Ihr Vertrauen!
<br><br><br>
Freundliche Grüße
<br>
Ihr Vodafone-Team
End;

$contentCable = <<< End
<div class="title">
    <img class="top-logo" src="{{var RhombusRedLogoPath}}" /><h1>Bestellbestätigung</h1>
</div>
<div class="greetings">
    {{var greetings}},<br><br>
    Vielen Dank für Ihre Bestellung. Hier nochmal alles Wichtige im Überblick:<br><br>
</div>

<table class="ghost-table personal-data-table">
    <tr>
        <th colspan="2">
            <h2>Ihre persönlichen Daten</h2>
        </th>
    </tr>
    <tr>
        <td class="first-column">
            <table class="order-no">
                <tbody>
                    <tr>
                        <td>
                            {{var personalInformation}}
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
        <td>
            <table class="order-no">
                <tbody>
                    <tr>
                        <td class="narrow"><strong>Bestellnummer</strong></td>
                        <td><span>: {{var orderId}}</span></td>
                    </tr>
                    <tr>
                        <td class="narrow"><strong>Bestelldatum</strong></td>
                        <td><span>: {{var orderDate}}</span></td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
    {{var addresses}}
</table>
<div class="offer-table">
    {{var orderTable}}
</div>
<div>
    {{var lostProducts}}
</div>
<div>
    <p>Sie haben noch Fragen zur Bestellung? Sie erreichen uns täglich rund um die Uhr: <b>vodafone.de/kontakt</b></p>
    <p>Sie haben noch Fragen zu unseren Services und Geräten? Besuchen Sie unsere Hilfe-Seiten: <b>vodafone.de/hilfe</b></p>
</div>
<div>
    {{var wishDate}}
</div>
<div class="goes-by">
    {{var nextSteps}}
</div>
<div class="footer">
    {{var endingNote}}
</div>
<br>
Vielen Dank für Ihr Vertrauen!
<br><br><br>
Freundliche Grüße
<br>
Ihr Vodafone-Team
End;

$contentCableMigration = $contentCable;

$contentMobile = <<< End
<div class="title">
    <img class="top-logo" src="{{var RhombusRedLogoPath}}" /><h1>Bestellbestätigung</h1>
</div>
<div class="greetings">
    {{var greetings}},<br><br>
    Vielen Dank für Ihre Bestellung. Hier nochmal alles Wichtige im Überblick:<br><br>
</div>

<table class="ghost-table personal-data-table">
    <tr>
        <th colspan="2">
            <h2>Ihre persönlichen Daten</h2>
        </th>
    </tr>
    <tr>
        <td class="first-column">
            <table class="order-no">
                <tbody>
                    <tr>
                        <td>
                            {{var personalInformation}}
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
        <td>
            <table class="order-no">
                <tbody>
                    <tr>
                        <td class="narrow"><strong>Bestellnummer</strong></td>
                        <td><span>: {{var orderId}}</span></td>
                    </tr>
                    <tr>
                        <td class="narrow"><strong>Bestelldatum</strong></td>
                        <td><span>: {{var orderDate}}</span></td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
    {{var addresses}}
</table>
<div class="offer-table">
    {{var orderTable}}
</div>
<div>
    {{var lostProducts}}
</div>
<div>
    <p>Sie haben noch Fragen zur Bestellung? Sie erreichen uns täglich rund um die Uhr: <b>vodafone.de/kontakt</b></p>
    <p>Sie haben noch Fragen zu unseren Services und Geräten? Besuchen Sie unsere Hilfe-Seiten: <b>vodafone.de/hilfe</b></p>
</div>
<div class="goes-by">
    {{var nextSteps}}
</div>
<div class="footer">
    {{var endingNote}}
</div>
<br>
Vielen Dank für Ihr Vertrauen!
<br><br><br>
Freundliche Grüße
<br>
Ihr Vodafone-Team
End;

$contentGigakombiCable = <<< End
<div class="title">
    <img class="top-logo" src="{{var RhombusRedLogoPath}}" /><h1>Bestellbestätigung</h1>
</div>
<div class="greetings">
    {{var greetings}},<br><br>
    Vielen Dank für Ihre Bestellung. Hier nochmal alles Wichtige zu Ihrem Kabel-Vertrag mit GigaKombi im Überblick:<br><br>
</div>

<table class="ghost-table personal-data-table">
    <tr>
        <th colspan="2">
            <h2>Ihre persönlichen Daten</h2>
        </th>
    </tr>
    <tr>
        <td class="first-column">
            <table class="order-no">
                <tbody>
                    <tr>
                        <td>
                            {{var personalInformation}}
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
        <td>
            <table class="order-no">
                <tbody>
                    <tr>
                        <td class="narrow"><strong>Bestellnummer</strong></td>
                        <td><span>: {{var orderId}}</span></td>
                    </tr>
                    <tr>
                        <td class="narrow"><strong>Bestelldatum</strong></td>
                        <td><span>: {{var orderDate}}</span></td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
    {{var addresses}}
</table>
<div class="offer-table">
    {{var orderTable}}
</div>
<div>
    {{var lostProducts}}
</div>
<div>
    <p>Sie haben noch Fragen zur Bestellung? Sie erreichen uns täglich rund um die Uhr: <b>vodafone.de/kontakt</b></p>
    <p>Sie haben noch Fragen zu unseren Services und Geräten? Besuchen Sie unsere Hilfe-Seiten: <b>vodafone.de/hilfe</b></p>
</div>
<div>
    {{var wishDate}}
</div>
<div class="goes-by">
    {{var nextSteps}}
</div>
<div class="footer">
    {{var endingNote}}
</div>
<br>
Vielen Dank für Ihr Vertrauen!
<br><br><br>
Freundliche Grüße
<br>
Ihr Vodafone-Team
End;

$contentGigakombiCableMigration = $contentGigakombiCable;

$contentGigakombiMobile = <<< End
<div class="title">
    <img class="top-logo" src="{{var RhombusRedLogoPath}}" /><h1>Bestellbestätigung</h1>
</div>
<div class="greetings">
    {{var greetings}},<br><br>
    Vielen Dank für Ihre Bestellung. Hier nochmal alles Wichtige zu Ihrem Mobilfunkvertrag mit GigaKombi im Überblick:<br><br>
</div>

<table class="ghost-table personal-data-table">
    <tr>
        <th colspan="2">
            <h2>Ihre persönlichen Daten</h2>
        </th>
    </tr>
    <tr>
        <td class="first-column">
            <table class="order-no">
                <tbody>
                    <tr>
                        <td>
                            {{var personalInformation}}
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
        <td>
            <table class="order-no">
                <tbody>
                    <tr>
                        <td class="narrow"><strong>Bestellnummer</strong></td>
                        <td><span>: {{var orderId}}</span></td>
                    </tr>
                    <tr>
                        <td class="narrow"><strong>Bestelldatum</strong></td>
                        <td><span>: {{var orderDate}}</span></td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
    {{var addresses}}
</table>
<div class="offer-table">
    {{var orderTable}}
</div>
<div>
    {{var lostProducts}}
</div>
<div>
    <p>Sie haben noch Fragen zur Bestellung? Sie erreichen uns täglich rund um die Uhr: <b>vodafone.de/kontakt</b></p>
    <p>Sie haben noch Fragen zu unseren Services und Geräten? Besuchen Sie unsere Hilfe-Seiten: <b>vodafone.de/hilfe</b></p>
</div>
<div class="goes-by">
    {{var nextSteps}}
</div>
<div class="footer">
    {{var endingNote}}
</div>
<br>
Vielen Dank für Ihr Vertrauen!
<br><br><br>
Freundliche Grüße
<br>
Ihr Vodafone-Team
End;


// ############################
// Static block details array
// ############################

$staticBlockDetailsArray = [
    [
        'identifier' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_DSL,
        'title' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_TITLE_DSL,
        'content' => $contentCss.$contentDsl,
    ],
    [
        'identifier' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_CABLE,
        'title' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_TITLE_CABLE,
        'content' => $contentCss.$contentCable,
    ],
    [
        'identifier' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_CABLE_MIGRATION,
        'title' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_TITLE_CABLE_MIGRATION,
        'content' => $contentCss.$contentCableMigration,
    ],
    [
        'identifier' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_MOBILE,
        'title' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_TITLE_MOBILE,
        'content' => $contentCss.$contentMobile,
    ],
    [
        'identifier' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_GIGAKOMBI_CABLE,
        'title' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_TITLE_GIGAKOMBI_CABLE,
        'content' => $contentCss.$contentGigakombiCable,
    ],
    [
        'identifier' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_GIGAKOMBI_CABLE_MIGRATION,
        'title' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_TITLE_GIGAKOMBI_CABLE_MIGRATION,
        'content' => $contentCss.$contentGigakombiCableMigration,
    ],
    [
        'identifier' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_IDENTIFIER_GIGAKOMBI_MOBILE,
        'title' => Dyna_Checkout_Block_Call::CALL_SUMMARY_BLOCK_TITLE_GIGAKOMBI_MOBILE,
        'content' => $contentCss.$contentGigakombiMobile,
    ],

];


// ############################
// Logic starts here
// ############################

$store = Mage::getModel('core/store')
    ->getCollection()
    ->addFieldToFilter('code', ['eq'=>'admin'])
    ->getFirstItem();

foreach ($staticBlockDetailsArray as $key => $details) {
    /** @var Mage_Cms_Model_Block $blockModel */
    $blockModel = Mage::getModel('cms/block');
    $blockId = $blockModel
        ->load($details['identifier'])
        ->getId();

    if (!$blockId) {
        $blockModel->setTitle($details['title'])
            ->setIdentifier($details['identifier'])
            ->setStores(array($store->getId()))
            ->setIsActive(1)
            ->setContent($details['content'])
            ->save();
    } else {
        $loadedBlock = $blockModel->load('offer_block_' . $details['key']);
        $loadedBlock->setContent($contentCss . $details['content'])
            ->save();
    }
}