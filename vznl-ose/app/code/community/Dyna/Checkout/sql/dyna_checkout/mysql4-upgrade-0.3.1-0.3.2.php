<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $salesInstaller Mage_Sales_Model_Resource_Setup */
$salesInstaller = new Mage_Sales_Model_Resource_Setup('core_setup');
$salesInstaller->startSetup();

/* start ORDER item attributes */
$orderItemAttr = array(
    'custom_maf' => array(
        'type' => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'precision' => 12,
        'scale' => 4,
        'required' => false,
        'comment' => 'Custom maf price',
    ),
    'original_custom_maf' => array(
        'type' => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'precision' => 12,
        'scale' => 4,
        'required' => false,
        'comment' => 'Original custom maf price',
    ),
    'is_contract' => array(
        'type'      => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
        'default'   => false,
        'nullable'  => false,
        'comment'   => 'Current service line installed base flag'
    ),
    'system_action' => array(
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'length'      => 10,
        'default'   => false,
        'nullable'  => true,
        'comment'   => 'Flag for Added or Removed by System items'
    ),
);
foreach ($orderItemAttr as $attributeCode => $attributeProp) {
    $salesInstaller->addAttribute('order_item', $attributeCode, $attributeProp);
    $salesInstaller->addAttribute('quote_address_item', $attributeCode, $attributeProp);
}
/* end ORDER item attributes */

$salesInstaller->endSetup();
