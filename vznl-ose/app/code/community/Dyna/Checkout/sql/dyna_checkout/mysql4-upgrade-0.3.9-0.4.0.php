<?php
/**
 * Copyright (c) 2017. Dynacommerce B.V.
 */

/* @var $installer Mage_Sales_Model_Entity_Setup */
$installer = $this;

$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote'), 'is_invalid', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
        'comment' => 'Set saved shopping cart or saved offer as invalid',
        'default' => 0,
        'after'   => 'is_offer'
    ));

$installer->endSetup();
