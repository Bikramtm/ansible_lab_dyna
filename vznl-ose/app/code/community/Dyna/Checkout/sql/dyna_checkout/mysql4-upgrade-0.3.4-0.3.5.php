<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Catalog_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();

$installer->getConnection()->addColumn($this->getTable('sales/quote'), 'callback_datetime', Varien_Db_Ddl_Table::TYPE_DATETIME);

$installer->endSetup();
