<?php
/**
 * Installer that adds column in_minimum_duration column on quote item table
 * Copyright (c) 2017. Dynacommerce B.V.
 */

/* @var $this Mage_Core_Model_Resource_Setup */

$this->startSetup();

$this->getConnection()->addColumn($this->getTable('sales/quote_item'), 'in_minimum_duration', array(
    'type'    => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
    'comment' => 'Marks cart item in minimum duration',
    'default' => 0,
));

$this->endSetup();
