<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;
$installer->startSetup();

$installer->getConnection()->addColumn($this->getTable('sales/quote'), 'sales_id', Varien_Db_Ddl_Table::TYPE_TEXT);

$installer->endSetup();
