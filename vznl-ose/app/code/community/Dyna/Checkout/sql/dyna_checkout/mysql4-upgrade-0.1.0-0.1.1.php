<?php
/**
 * Installer that removes the field_id primary column to prevent issues regarding the dimension of checkout fields table
 */

/** @var Mage_Core_Model_Resource_Setup $this */
$installer = $this;
$installer->startSetup();

$installer->getConnection()->dropColumn(
    $installer->getTable("dyna_checkout/fields"),
    "field_id"
);

$installer->endSetup();
