<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Catalog_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();

$installer->getConnection()->changeColumn( 'sap_orders', 'sap_data', 'sap_data', 'MEDIUMTEXT');

$installer->endSetup();
