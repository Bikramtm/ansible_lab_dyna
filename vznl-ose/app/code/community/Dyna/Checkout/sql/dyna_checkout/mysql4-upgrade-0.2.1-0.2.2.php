<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;
$installer->startSetup();

$installer->getConnection()->addColumn('sap_orders', 'created_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array('default' => Varien_Db_Ddl_Table::TIMESTAMP_INIT));

$installer->endSetup();
