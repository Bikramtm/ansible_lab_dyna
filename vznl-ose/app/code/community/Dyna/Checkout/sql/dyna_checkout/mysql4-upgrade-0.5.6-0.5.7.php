<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Core_Model_Resource_Setup $this */
$installer = $this;
$installer->startSetup();
$connection = $installer->getConnection();

if (!$connection->tableColumnExists($installer->getTable('sales_flat_quote_item'), 'contract_possible_cancellation_date')) {
    $connection->addColumn($this->getTable('sales_flat_quote_item'), 'contract_possible_cancellation_date', [
            'comment'       => 'Contract Possible Cancellation Date',
            'type'          => Varien_Db_Ddl_Table::TYPE_DATETIME,
            'required'      => false
        ]
    );
}
$installer->endSetup();
