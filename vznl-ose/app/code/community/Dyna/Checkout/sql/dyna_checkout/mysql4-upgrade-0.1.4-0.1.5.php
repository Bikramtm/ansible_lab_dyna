<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * @category    Dyna
 * @package     Dyna_Checkout
 */

$installer = $this;

$installer->startSetup();
$connection = $installer->getConnection();
if(!$connection->tableColumnExists($this->getTable("dyna_checkout/fields"), "entity_id")){
    $this->run("alter table " . $this->getTable("dyna_checkout/fields") . " add column `entity_id` bigint(10) unsigned primary KEY AUTO_INCREMENT FIRST;");
}


$connection->addColumn($this->getTable("dyna_checkout/fields"), 'package_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
    array(
        'nullable' => true
    ), 'Package id');
$connection->addForeignKey(
    $this->getFkName($this->getTable("dyna_checkout/fields"), 'package_id', 'catalog_package', 'entity_id'),
    $this->getTable("dyna_checkout/fields"),
    'package_id', $this->getTable('catalog_package'), 'entity_id',
    Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE);


$installer->endSetup();
