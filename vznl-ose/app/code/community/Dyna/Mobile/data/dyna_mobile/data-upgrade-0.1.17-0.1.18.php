<?php
$installer = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;
$targetAttributeCode = 'extra_handle_indicator';

$attr = Mage::getModel('eav/config')->getAttribute($entityTypeId, $targetAttributeCode);
$attrId = $attr->getId();
$attrOptions = $attr->getSource()->getAllOptions();

$deletedOptions = [];
$deletedOptions['attribute_id'] = $attrId;

$newOptions = [
    'attribute_id' => $attrId,
    'values' => [
        Dyna_Mobile_Model_Product_Attribute_Option::HIS_OPTION,
        Dyna_Mobile_Model_Product_Attribute_Option::PAPERBILL_OPTION,
        Dyna_Mobile_Model_Product_Attribute_Option::CONNECTION_OVERVIEW_OPTION,
    ]
];

$first = true;
foreach ($attrOptions as $key => $details) {
    if (!$first) {
        $deletedOptions['delete'][$details['value']] = true;
        $deletedOptions['value'][$details['value']] = true;
    }
    $first = false;
}
$installer->addAttributeOption($deletedOptions);
$installer->addAttributeOption($newOptions);

$installer->endSetup();
?>
