<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

$insertData = [
    ["A" => "AS"],
    ["A" => "CN"],
    ["A" => "JO"],
    ["A" => "KY"],
    ["A" => "MS"],
    ["A" => "PL"],
    ["A" => "RO"],
    ["A" => "SC"],
    ["A" => "SL"],
    ["A" => "SP"],
    ["B" => "AR"],
    ["B" => "AS"],
    ["B" => "AT"],
    ["B" => "CA"],
    ["B" => "CN"],
    ["B" => "EH"],
    ["B" => "FH"],
    ["B" => "GD"],
    ["B" => "JO"],
    ["B" => "MS"],
    ["B" => "NM"],
    ["B" => "PL"],
    ["B" => "RO"],
    ["B" => "SC"],
    ["B" => "SL"],
    ["B" => "SP"],
    ["E" => "RG"],
    ["I" => "AK"],
    ["I" => "AS"],
    ["I" => "AT"],
    ["I" => "CA"],
    ["I" => "FH"],
    ["I" => "FT"],
    ["I" => "ME"],
    ["I" => "MM"],
    ["I" => "MO"],
    ["I" => "NE"],
    ["I" => "NM"],
    ["I" => "NR"],
    ["I" => "PB"],
    ["I" => "PL"],
    ["I" => "SC"],
    ["I" => "SP"],
    ["I" => "UI"],
    ["S" => "AS"],
    ["S" => "CN"],
    ["S" => "FH"],
    ["S" => "JO"],
    ["S" => "PL"],
    ["S" => "SC"],
    ["S" => "SP"]
];

$siteModel   = Mage::getResourceModel('core/website_collection')->addFieldToFilter('name', 'telesales');
$websiteId   = $siteModel->getFirstItem()->getId();
$mobileModel = Mage::getModel('dyna_mobile/nonSupportedCombinations');

if ($installer->tableExists('dyna_mobile_non_supported_combinations')) {
    foreach ($insertData as $data) {
        foreach ($data as $accType => $accSubType) {
            $mobileModel->setAccType($accType);
            $mobileModel->setAccSubType($accSubType);
            $mobileModel->setWebsiteId($websiteId);
            $mobileModel->save();
            $mobileModel->unsetData();
        }
    }
}

$installer->endSetup();
