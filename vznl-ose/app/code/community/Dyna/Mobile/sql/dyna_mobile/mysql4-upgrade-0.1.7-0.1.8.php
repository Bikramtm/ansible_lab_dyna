<?php
/**
 * Remove attribute gui_description
 */

/** @var Mage_Eav_Model_Entity_Setup $installer */
$installer = $this;
$installer->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;
$installer->removeAttribute($entityTypeId, 'gui_description');

$installer->endSetup();
