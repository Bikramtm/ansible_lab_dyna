<?php
/* @var $installer Dyna_Mobile_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

$this->_generalGroupName = "Mobile";
$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

// add the red plus attribute set to backend
$attributeSetCode = 'RedPlus_Voice_Data_Tariff';
$installer->addAttributeSet($entityTypeId, $attributeSetCode);
$attributeSetId = $installer->getAttributeSetId($entityTypeId, $attributeSetCode);

// proceeding to adding all mobile tariff attributes to the newly created attribute set
$mobileTariffSetId = $installer->getAttributeSetId($entityTypeId, "Mobile_Voice_Data_Tariff");

$attributes = Mage::getModel('catalog/product_attribute_api')->items($mobileTariffSetId);

foreach ($attributes as $attribute) {
    $installer->addAttributeToSet($entityTypeId, $attributeSetId, "Mobile", $attribute['attribute_id']);
}

/** MERGED INSTALLER FROM WAVE 1 INTO THIS ONE */

$attributeCode = 'ident_needed';

$attributeId = $installer->getAttributeId($entityTypeId, $attributeCode);
if (!$attributeId) {
    $installer->addAttribute($entityTypeId, $attributeCode, array(
        'label' => 'Identification Needed',
        'input' => 'boolean',
        'type' => 'int',
        'required' => false,
        'default' => 0,
        'source' => 'eav/entity_attribute_source_boolean',
        'note' => 'Mark (Y/N) to set if additional identification is needed for this product.'
    ));
}
$groupName = 'Mobile';
$name = "Mobile_Prepaid_Tariff";
$attrSetId = $installer->getAttributeSetId($entityTypeId, $name);
$installer->addAttributeToSet($entityTypeId, $attrSetId, $groupName, $attributeCode);

$installer->endSetup();
