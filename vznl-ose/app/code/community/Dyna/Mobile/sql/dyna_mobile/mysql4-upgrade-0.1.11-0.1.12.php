<?php
/**
 * Map effective_date and expiration_date to all Mobile attribute sets
 */

/* @var $this Dyna_Mobile_Model_Resource_Setup */

$this->_generalGroupName = "Mobile";

$installer = $this;
$installer->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

// Assign effective_date and expiration_date to all Mobile attribute sets
$attributeCodes = [
    "effective_date",
    "expiration_date",
];

$attributeSets = [
    "Mobile_Voice_Data_Tariff",
    "Mobile_Data_Tariff",
    "Mobile_Prepaid_Tariff",
    "Mobile_Additional_Service",
    "Mobile_Footnote",
];

foreach ($attributeCodes as $attributeCode) {
    foreach ($attributeSets as $attributeSetCode) {
        $attributeSetId = $installer->getAttributeSet($entityTypeId, $attributeSetCode, "attribute_set_id");
        if ($attributeSetId) {
            $installer->addAttributeToSet($entityTypeId, $attributeSetId, "Mobile", $attributeCode);
        }
    }
}

// Remove obsolete attributes
$obsoleteAttributes = [
    'sale_eff_date',
    'sale_exp_date',
    'start_date',
    'end_date',
];

foreach ($obsoleteAttributes as $attribute) {
    $installer->removeAttribute($entityTypeId, $attribute);
}

$installer->endSetup();
