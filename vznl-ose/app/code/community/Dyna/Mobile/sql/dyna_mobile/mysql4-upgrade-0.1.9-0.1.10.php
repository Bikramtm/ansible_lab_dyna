<?php
/**
 * Rename Footnote attribute set to Mobile_Footnote
 */

/** @var Mage_Eav_Model_Entity_Setup $installer */
$installer = $this;
$installer->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

$attributeSetId = $installer->getAttributeSet($entityTypeId, 'Footnote', 'attribute_set_id');
if ($attributeSetId) {
    $installer->updateAttributeSet($entityTypeId, $attributeSetId, 'attribute_set_name', 'Mobile_Footnote');
}

$installer->endSetup();
