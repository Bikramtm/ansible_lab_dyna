<?php

/* @var $installer Dyna_Cable_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();
$entityTypeId = Mage_Catalog_Model_Product::ENTITY;
$attributeCode = 'ident_needed';
$attributeSet = 'Mobile_Prepaid_Tariff';
$groupName = 'Mobile';
$sortOrder = 50;

$attributeId = $installer->getAttributeId($entityTypeId, $attributeCode);
if ($attributeId) {
    $installer->removeAttribute($entityTypeId, $attributeCode);
}

$installer->addAttribute($entityTypeId, $attributeCode, array(
    'label' => 'Identification Needed',
    'input' => 'boolean',
    'type' => 'int',
    'required' => false,
    'default' => 0,
    'user_defined' => 1,
    'source' => 'eav/entity_attribute_source_boolean',
    'note' => 'Mark (Y/N) to set if additional identification is needed for this product.'
));


$createAttributeSets = [];
$createAttributeSets[$attributeSet]['groups'][$groupName][] = $attributeCode;
$installer->assignAttributesToSet($createAttributeSets, $sortOrder);

unset($createAttributeSets);

/** MERGED INSTALLER FROM WAVE 1 INTO THIS ONE */

$installer->updateAttribute($entityTypeId, 'redplus_role', 'is_required', 0);

$installer->endSetup();
