<?php

/**
 * Installer that aligns discount attributes with "Mobile_Additional_Service" attribute set (@see OMNVFDE-3206)
 */

/* @var $installer Dyna_Mobile_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$entityTypeId = $entityTypeId = $installer->getEntityTypeId(Mage_Catalog_Model_Product::ENTITY);


$newAttributesList = array(
    'price_discount' => [
        'label' => 'Price discount amount',
        'input' => 'price',
        'type' => 'decimal'
    ],
    'price_discount_percent' => [
        'label' => 'Price discount percent',
        'input' => 'text',
        'type' => 'text',
    ],
);

foreach ($newAttributesList as $attributeCode => $attributeData) {
    $attributeData['user_defined'] = 1;
    $installer->addAttribute($entityTypeId, $attributeCode, $attributeData);
}

$createAttributeSets = array(
    'Mobile_Additional_Service' => array(
        'groups' => array(
            'Mobile' => array(
                'maf_discount',
                'maf_discount_percent',
                'price_discount',
                'price_discount_percent',
                'discount_period_amount',
                'discount_period',
            ),
        ),
    ),
);

$discountAmountAttributeId = $installer->getAttributeId($entityTypeId, "discount_amount");
$attributeSetId = $installer->getAttributeSetId($entityTypeId, "Mobile_Additional_Service");
$attributeGroupId = $installer->getAttributeGroupId($entityTypeId, $attributeSetId, "Mobile");

$sortOrderSql =
<<<SQL
    SELECT 
      sort_order 
    FROM
      eav_entity_attribute 
    WHERE entity_type_id = :entityTypeId 
      AND attribute_set_id = :attributeSetId 
      AND attribute_group_id = :attributeGroupId 
      AND attribute_id = :attributeId 
    LIMIT 1
SQL;


$sortOrder = (int)$installer->getConnection()->fetchOne($sortOrderSql, array(
    'entityTypeId' => $entityTypeId,
    'attributeSetId' => $attributeSetId,
    'attributeGroupId' => $attributeGroupId,
    'attributeId' => $discountAmountAttributeId,
));

$installer->assignAttributesToSet($createAttributeSets, $sortOrder);
unset($createAttributeSets);

$installer->endSetup();
