<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * @category    Omnius
 * @package     Omniu_MixMatch
 */

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();

/**
 * Create table
 */
if (!$installer->tableExists('dyna_mobile_non_supported_combinations')) {
    // Creating non-supported table of combinations between account type/subtype
    $table = $installer->getConnection()
        ->newTable("dyna_mobile_non_supported_combinations")
        ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity'  => true,
            'unsigned'  => true,
            'nullable'  => false,
            'primary'   => true,
        ), 'ID')
        ->addColumn('acc_type', Varien_Db_Ddl_Table::TYPE_TEXT, 11, array(
        ), 'Account type')
        ->addColumn('acc_sub_type', Varien_Db_Ddl_Table::TYPE_TEXT, 11, array(
        ), 'Account subtype')
        ->addColumn('website_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, 5, array(
            'unsigned' => true,
            'nullable'  => false,
        ), 'Channel')
        ->setComment('Non-supported combinations of account type/subtype');

    $installer->getConnection()->createTable($table);
}

$installer->endSetup();
