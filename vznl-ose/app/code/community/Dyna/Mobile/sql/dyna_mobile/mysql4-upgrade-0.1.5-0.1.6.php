<?php
/**
 * Update attribute set name from Mobile_Sim_Card to Mobile_Simcard
 */

$installer = $this;
/* @var $installer Dyna_Mobile_Model_Resource_Setup */

$installer->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

$installer->updateAttributeSet(
    $entityTypeId,
    $installer->getAttributeSetId($entityTypeId, 'Mobile_SIM_Card'),
    'attribute_set_name',
    'Mobile_Simcard');

$installer->endSetup();
