<?php

/**
 * Class Dyna_Mobile_Model_Import_CableProduct
 */
class Dyna_Mobile_Model_Import_MobileProduct extends Dyna_Import_Model_Import_Product_Abstract
{
    /** @var string $_multiselectSplit */
    protected $_multiselectSplit = ",";
    /** @var string $_csvDelimiter */
    protected $_csvDelimiter = ';';
    /** @var string $_logFileName */
    protected $_logFileName = "mobile_product_import";
    protected $_debug = false;
    /** @var Dyna_Catalog_Model_ProductVersion */
    protected $productVersionModel;
    /** @var Dyna_Catalog_Model_ProductFamily */
    protected $productFamilyModel;

    /**
     * Dyna_Import_Model_Import_CableProduct constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_helper->setImportLogFile($this->_logFileName . '.log');
        $this->_qties = 0;

        $this->productFamilyModel = Mage::getSingleton('dyna_catalog/productFamily');
        $this->productVersionModel = Mage::getSingleton('dyna_catalog/productVersion');
    }

    /**
     * Import cable product
     *
     * @param array $data
     * @return void
     */
    public function importProduct($data)
    {
        $skip = false;
        //for logging purposes
        $this->_totalFileRows++;
        $data = $this->_setDataMapping($data);

        if (!$this->_helper->hasValidPackageDefined($data)) {
            $this->_skippedFileRows++;
            return;
        }

        if (!array_key_exists('sku', $data) || empty($data['sku'])) {
            $this->_logError('Skipping line without sku');
            $skip = true;
        } elseif (!array_key_exists('name', $data) || empty($data['name'])) {
            $this->_logError('Skipping line without name');
            $skip = true;
        }

        if ($skip) {
            //for logging purposes
            $this->_skippedFileRows++;
            return;
        }

        $data['stack'] = $this->stack;
        $data['sku'] = trim($data['sku']);
        $this->_currentProductSku = $data['sku'];
        $this->_log('Start importing ' . trim($data['name']), true);
        $data = $this->checkPrice($data);

        try {
            /** @var Dyna_Catalog_Model_Product $product */
            $product = Mage::getModel('catalog/product');
            $id = $product->getIdBySku($data['sku']);
            $new = ($id == 0);
            if (!$new) {
                $this->_log('Loading existing product "' . $data['sku'] . '" with ID ' . $id, true);
                $product->load($id);
            }
            // check vat value and tax
            $vatValue = $data['vat'];
            $vatClass = Mage::helper('dyna_catalog')->getVatTaxClass($vatValue);

            $skip = false;
            if (!$this->_setAttributeSetByCode($product, $data)) {
                $this->_logError('Unknown attribute set "' . $data['attribute_set'] . '" for product ' . $data['sku']);
                $skip = true;
            } elseif (!$this->_setWebsitesByCsvString($product, $data)) {
                $this->_logError('Unknown website "' . $data['_product_websites'] . '" for product ' . $data['sku']);
                $skip = true;
            } elseif (!$this->setTaxClassIdByName($product, $vatClass)) {
                $this->_logError((empty($vatClass) ? 'Empty vat value' : 'Unknown vat value "' . $vatClass . '"') . ' for product ' . $data['sku']);
                $skip = true;
            }

            if ($skip) {
                //for logging purposes
                $this->_skippedFileRows++;
                return;
            }

            $this->_setDefaults($product, $data);
            $this->_setStatus($product, $data);
            $this->_setProductData($product, $data);

            // set product categories
            if (isset($data['category']) && is_array($data['category']) && count($data['category'])) {
                $product->setCategoryIds($data['category']);
            }

            if ($new) {
                $this->_createStockItem($product);
            }

            // Adding product versions to product
            if (isset($data['product_version'])) {
                $versionIds = array();
                foreach (array_filter(explode(",", $data['product_version'])) as $versionCode) {
                    $versionIds[] = $this->productVersionModel::getProductVersionIdByCode($versionCode);
                }
                $product->setData("product_version", implode(",", $versionIds));
            }
            // Adding product family to product
            if (isset($data['product_family'])) {
                $familyIds = array();
                foreach (array_filter(explode(",", $data['product_family'])) as $familyCode) {
                    $familyIds[] = $this->productFamilyModel::getProductFamilyIdByCode($familyCode);
                }
                $product->setData("product_family", implode(",", $familyIds));
            }


            if (!$this->getDebug()) {
                $product->save();
            }

            if (!$this->getDebug()) {
                $productVisibilityInfo = @json_decode($product->getProductVisibilityStrategy(), true);
                if ($productVisibilityInfo) {
                    Mage::getModel('productfilter/filter')->createRule($product->getId(), $productVisibilityInfo['strategy'], $productVisibilityInfo['groups']);
                } else {
                    //uncomment the following line if products that have no defined filters must be treated as available for all groups
                    //Mage::getModel('productfilter/filter')->createRule($product->getId(), Dyna_ProductFilter_Model_Filter::STRATEGY_INCLUDE_ALL, null); //available for all dealer groups
                }
            }

            unset($product);
            $this->_qties++;

            $this->_log('Finished importing ' . $data['name'], true);
        } catch (Exception $ex) {
            //for logging purposes
            $this->_skippedFileRows++;
            echo $ex->getMessage();
            $this->_logEx($ex);
        }
    }

    /**
     * Process custom data mapping for cable products
     *
     * @param array $data
     * @return array
     */
    protected function _setDataMapping($data)
    {
        $header_mappings = array_flip($this->_helper->getMapping('product_template'));
        foreach ($header_mappings as $new => $old) {
            if (!isset($data[$new]) && isset($data[$old])) {
                if (!empty($data[$old])){
                    $data[$new] = $data[$old];
                }
                unset($data[$old]);
            }
        }
        // SKU format as specified in CAT-11_Product Catalogue HLD_version_2.4
        $data['package_subtype'] = $data['package_subtype'] == "add on" ? "Addon" : $data['package_subtype'];

        // Default telesales website
        $data['_product_websites'] = 'telesales';


        // If tax class id is not provided, we set it to Taxable Goods by default
        if (! isset($data['tax_class_id'])) {
            $data['tax_class_id'] = 'Taxable Goods';
        } elseif ($data['tax_class_id'] == 'None') {
            $data['tax_class_id'] = 0;
        }

        if(empty($data['redplus_role'])){
            $data['redplus_role'] = 'none';
        }

        // new name => old name
        $renamedFields = [
            'bank_collection_mandatory' => 'bank_collection',
            'sku' => 'kias_code',
        ];

        foreach ($renamedFields as $new => $old) {
            if (empty($data[$new]) && !empty($data[$old])) {
                $data[$new] = $data[$old];
            }
        }

        if (!empty($data['net_material_price'])) {
            $data['net_material_price'] = $this->getFormattedPrice($data['net_material_price']);
        }

        if (!empty($data['gross_material_price'])) {
            if (empty($data['price'])) {
                $data['price'] = $data['gross_material_price'];
            }
            $data['gross_material_price'] = $this->getFormattedPrice($data['gross_material_price']);
        }

        // Format price decimals
        $data['price'] = $this->getFormattedPrice($data['price']);

        if (!empty($data['maf_discount'])) {
            $data['maf_discount'] = $this->getFormattedPrice($data['maf_discount']);
        }

        if (!empty($data['price_discount'])) {
            $data['price_discount'] = $this->getFormattedPrice($data['price_discount']);
        }

        // determine the name based on the gui description or sku
        $data['name'] = $this->determineName($data);
        // if there is no gui description or sku -> unset the name
        if (!$data['name']) {
            unset($data['name']);
        }

        $dateTimeFields = array(
            'effective_date',
            'expiration_date',
        );

        foreach ($dateTimeFields as $dateTimeField) {
            if (!empty($data[$dateTimeField])) {
                $data[$dateTimeField] = date("Y-m-d H:i:s", strtotime($data[$dateTimeField]));
            }
        }
        // determine if is prepaid based on attribute set
        $data['prepaid'] = $this->determineIsPrepaid($data);
        // if the attribute set does not meet the condition for prepaid -> unset the prepaid value
        if (! $data['prepaid']) {
            unset($data['prepaid']);
        }

        /** Text retrieved for service_description attribute needs to be parsed as BB code */
        $bbParser = new Dyna_Mobile_Model_Import_BBParser();
        if (!empty($data['service_description'])) {
            $bbParser->setText($data['service_description']);
            $data['service_description'] = $bbParser->parseText();
        }

        /** Get value for checkout product attribute */
        $data['checkout_product'] = $this->getCheckoutProductValue($data);

        /** Get correct value for product visibility attribute */
        if (isset($data['product_visibility'])) {
            $data['product_visibility'] = $this->parseProductVisibility($data['product_visibility']);
        }

        return $data;
    }

    /**
     * @param $price
     * @return string
     */
    private function getFormattedPrice($price)
    {
        $price = floatval(str_replace(',', '.', $price));

        return number_format($price, 4, '.', null);
    }

    /**
     * Determine the name based on gui description or sku
     *
     * @param array $data
     * @return string
     */
    private function determineName($data)
    {
        $productName = null;
        if (!empty($data['name'])) {
            $productName = $data['name'];
        } elseif (isset($data['sku']) && $data['sku']) {
            $productName = $data['sku'];
        }

        return $productName;
    }

    /**
     * Determine if is prepaid based on the attribute set
     *
     * @param array $data
     * @return null|string
     */
    private function determineIsPrepaid($data)
    {
        $prepaid = null;
        if (!empty($data['prepaid'])) {
            $prepaid = $data['prepaid'];
        } elseif (isset($data['attribute_set'])
            && ($data['attribute_set'] == 'prepaid_package' || $data['attribute_set'] == 'mobile_prepaid_tariff')
        ) {
            $prepaid = 'Yes';
        }

        return $prepaid;
    }

    /**
     * Determine checkout product attribute value
     *
     * @param array $data
     * @return null|string
     */
    private function getCheckoutProductValue($data)
    {
        if(isset($data['checkout_product']) && $data['checkout_product']) {
            return ('yes' == strtolower($data['checkout_product'])) ? 'Yes' : 'No';
        }
        return 'No';
    }

    /**
     * Method to parse the value of the product visibility attributes
     * @param $values
     * @return string
     */
    protected function parseProductVisibility($values){
        $markedOptions = [];
        $markedOptionIds = [];
        $options = [];

        $attribute = Mage::getSingleton('eav/config')
            ->getAttribute(Mage_Catalog_Model_Product::ENTITY, Dyna_Catalog_Model_Product::CATALOG_PACKAGE_PRODUCT_VISIBILITY_ATTR);
        if ($attribute->usesSource()) {
            $options = $attribute->getSource()->getAllOptions(false);
        }

        if ('*' == $values) {
            $markedOptionIds = array_diff(
                array_column($options, 'label'),
                [Dyna_Catalog_Model_Product::CATALOG_PRODUCT_VISIBILITY_OPTION_NOT_SHOWN]
            );
        } else {
            $values = explode(',', $values);
            foreach($values as $value){
                $markedOptions[] = strtolower(str_replace(' ', '', $value));
            }
            foreach ($options as $option) {
                if(in_array(strtolower(str_replace(' ', '', $option['label'])), $markedOptions)){
                    $markedOptionIds[] = $option['label'];
                }
            }
        }

        return implode(',', $markedOptionIds);
    }

    /**
     * Checks whether or not product data has both package type and package subtype valid
     * @return bool
     */
    public function hasValidPackageDefined($productData)
    {
        $valid = true;
        //Convert package_type to upper because the values from $types are Upper
        $productData['package_type'] = trim($productData['package_type']);
        /** @var Dyna_Package_Helper_Data $packageHelper */
        $packageHelper = Mage::helper('dyna_package');
        //get package types from db
        $types = $packageHelper->getPackageTypes();

        if(!empty($types)){
            $types = array_keys($types);
        }

        if (empty($productData['package_type']) || !in_array((strtolower($productData['package_type'])), array_map('strtolower',$types))) {
            $productData['package_type'] = !empty($productData['package_type']) ? $productData['package_type'] : "none provided";
            echo "(SKU \e[1;32m" . $productData['sku'] . "\e[0m) Skipped entire product because of invalid package \e[1;33mtype\e[0m value: \e[1;31m" . $productData['package_type'], "\033[0m (case sensitive)\n";
            $this->_logError("(SKU " . $productData['sku'] . ") Skipped entire product because of invalid package type value: " . $productData['package_type'] . " (case insensitive)");
            $valid = false;
        }

        if (empty($productData['package_subtype']) || empty($packageHelper->getPackageSubtype($productData['package_type'], $productData['package_subtype'], false, []))) {
            $productData['package_subtype'] = !empty($productData['package_subtype']) ? $productData['package_subtype'] : "none provided";
            echo "(SKU \e[1;32m" . $productData['sku'] . "\e[0m) Skipped entire product because of invalid package \e[1;33msubType\e[0m value: \e[1;31m" . $productData['package_subtype'], "\033[0m (case sensitive)\n";
            $this->_logError("(SKU " . $productData['sku'] . ") Skipped entire product because of invalid package subType value: " . $productData['package_subtype'] . " (case sensitive)");
            $valid = false;
        }
        
        return $valid;
    }

    /**
     * Checks whether or not price is listed in import file (can be 0, but needs to exists)
     */
    protected function checkPrice($data)
    {
        if (!isset($data['price']) || $data['price'] === "") {
            $data['price'] = 0;
            $this->_log("[NOTICE] Did not receive any value for price column. Setting price to 0");
        }

        return $data;
    }

    /**
     * Set product class id based on the provided tax_class_id in the CSV file
     *
     * @param $product
     * @param string $className
     * @return bool
     * @todo: move in abstract class
     */
    public function setTaxClassIdByName($product, $className)
    {
        $productTaxClass = Mage::getModel('tax/class')
            ->getCollection()
            ->addFieldToFilter('class_name', $className)
            ->load()
            ->getFirstItem();
        if (!$productTaxClass || !$productTaxClass->getId()) {
            return false;
        }

        $product->setTaxClassId($productTaxClass->getId());

        return true;
    }

    /**
     * Set product model default values
     *
     * @param Mage_Catalog_Model_Product $product
     * @param array $data
     */
    protected function _setDefaults($product, $data)
    {
        $product->setTypeId(Mage_Catalog_Model_Product_Type::TYPE_SIMPLE);
        // This will format itself trough observer
        $product->setUrlKey($data['name']);
        $product->setProductVisibilityStrategy('all');
        $product->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH);
        $product->setIsDeleted('0');
    }

    /**
     * Set product status
     *
     * @param Mage_Catalog_Model_Product $product
     * @param array $data
     */
    protected function _setStatus($product, $data)
    {
        if (array_key_exists('status', $data) && $data['status']) {
            switch ($data['status']) {
                case 'Enabled':
                case 'enabled':
                    $product->setStatus(1);
                    break;
                case 'Disabled':
                case 'disabled':
                default:
                    $product->setStatus(2);
                    break;
            }
        } else {
            $product->setStatus(2);
        }
    }

    public function setImportHelper($helper) {
        $this->_helper = $helper;
    }

    /**
     * Log function
     *
     * @param string $msg
     */
    public function _logError($msg)
    {
        $this->_helper->logMsg('[' . $this->_currentProductSku . '] ' . $msg);
    }

    /**
     * Log function
     *
     * @param string $msg
     * @param bool $verbose
     */
    public function _log($msg, $verbose = false)
    {
        if($verbose && !$this->_debug) {
            return;
        }
        $this->_helper->logMsg('[' . $this->_currentProductSku . '] ' . $msg, false);
    }
}
