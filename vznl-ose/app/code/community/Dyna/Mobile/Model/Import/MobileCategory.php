<?php

/**
 * Class Dyna_Mobile_Model_Import_MobileCategory
 */
class Dyna_Mobile_Model_Import_MobileCategory extends Omnius_Import_Model_Import_Category_Abstract
{
    const DEFAULT_CATEGORY_PATH = '1/2';
    const CATEGORY_PATH_DELIMITER = '/';
    const TREE_CATEGORIES_DELIMITER = "->";
    const CATEGORY_DISPLAY_MODE = "PRODUCTS";
    const CATEGORY_IS_ACTIVE = 1;
    /** @var string $_multiselectSplit */
    protected $_multiselectSplit = "|";
    /** @var string $_csvDelimiter */
    protected $_csvDelimiter = ';';
    private $categoriesAndSubcategories = [];
    private $parentPath;
    protected $dbCategories = [];

    /** @var Mage_Catalog_Model_Product $product */
    private $productId;
    private $categoryId;
    private $categoryPath;
    private $categoryPaths;
    private $categoryModels;
    private $productSkus;
    private $storeId;
    private $readConnection;
    /** @var string $_logFileName */
    protected $_logFileName = "categories_import";
    /** @var string $_logFileExtension */
    protected $_logFileExtension = "log";

    /** @var Dyna_Import_Helper_Data $_helper */
    public $_helper;

    /** @var bool $_debug */
    protected $_debug = false;

    /**
     * Dyna_Mobile_Model_Import_Categories constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->storeId = Mage::app()->getStore('telesales')->getId();
        $this->readConnection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $this->productSkus = $this->readConnection->fetchPairs(
            "select entity_id, sku from catalog_product_entity"
        );
        $allCategories = $this->readConnection->fetchAll(
            "SELECT catalog_category_entity.*, catalog_category_entity_varchar.value AS name 
             FROM catalog_category_entity 
             INNER JOIN catalog_category_entity_varchar on catalog_category_entity_varchar.entity_id = catalog_category_entity.entity_id 
             WHERE catalog_category_entity_varchar.attribute_id = (SELECT eav_attribute.attribute_id FROM eav_attribute WHERE 
	            eav_attribute.attribute_code = \"name\" AND 
	            eav_attribute.entity_type_id = (SELECT eav_entity_type.entity_type_id FROM eav_entity_type WHERE eav_entity_type.entity_type_code = 'catalog_category' LIMIT 1) LIMIT 1)");

        foreach ($allCategories as $cat) {
            $this->categoryPaths[$cat['path']] = $cat['name'];
            $this->categoryModels[$cat['entity_id']] = $cat;
        }

        $this->_helper->setImportLogFile($this->_logFileName . '.' . $this->_logFileExtension);
        $this->_qties = 0;
    }

    /**
     * Import categories
     *
     * @param array $data
     * @return void
     */
    public function importCategory($data)
    {
        $skip = false;
        $this->parentPath = self::DEFAULT_CATEGORY_PATH;
        $this->productId = null;
        $this->categoryId = null;
        $this->_totalFileRows++;
        if (!isset($data['sku']) || empty($data['sku'])) {
            $this->_logError('Skipping line without sku');
            $skip = true;
        } elseif (!isset($data['category_tree']) || empty($data['category_tree'])) {
            $this->_logError('Skipping line without category tree');
            $skip = true;
        } else {
            if (!in_array($data['sku'], $this->productSkus)) {
                $this->_logError('Product with sku ' . $data['sku'] . ' does not exist');
                $skip = true;
            }
            $this->productId = array_search($data['sku'], $this->productSkus);
        }

        if ($skip) {
            $this->_skippedFileRows++;
            return;
        }

        $categoryId = $this->_setDataMapping($data);
        $this->_log('Start importing the category tree ' . $data['category_tree'] . ' for product ' . $data['sku'], true);

        try {
            if (!$categoryId) {
                $this->createCategoryPath();
            }
            $this->assignProductToCategory();

            $this->_qties++;
            $this->_log('Finished importing the category tree ' . $data['category_tree'], true);
        } catch (Exception $ex) {
            fwrite(STDERR, $ex->getMessage());
            $this->_logError($ex->getMessage());
        }
    }

    /**
     * Map the data from csv with the categories that are exploded from the category tree;
     * @param array $data
     * @return int|null
     */
    protected function _setDataMapping($data)
    {
        $this->categoriesAndSubcategories = [];

        // Check if category id was already determined for the category_tree path
        if (!empty($this->dbCategories[$data['category_tree']])) {
            $this->categoryId = $this->dbCategories[$data['category_tree']];

            return $this->categoryId;
        }

        // set the category and subcategories names that need to be created
        $categoriesAndSubcategories = explode(self::TREE_CATEGORIES_DELIMITER, $data["category_tree"]);
        $key = [];

        foreach ($categoriesAndSubcategories as $index => $name) {
            $key[] = $index;
            $this->categoriesAndSubcategories[implode(self::TREE_CATEGORIES_DELIMITER, $key)]['name'] = $name;
            $this->categoriesAndSubcategories[implode(self::TREE_CATEGORIES_DELIMITER, $key)]['category_tree'] = $data["category_tree"];
            $this->categoriesAndSubcategories[implode(self::TREE_CATEGORIES_DELIMITER, $key)]['level'] = count($key) + 1;
            $this->categoriesAndSubcategories[implode(self::TREE_CATEGORIES_DELIMITER, $key)]['is_anchor'] = isset($data["is_anchor"]) ? $data["is_anchor"] : 0;
            $this->categoriesAndSubcategories[implode(self::TREE_CATEGORIES_DELIMITER, $key)]['family_position'] = isset($data["family_category_sorting"]) ? $data["family_category_sorting"] : 0;
            if (count($key) == count($categoriesAndSubcategories)) {
                $this->categoriesAndSubcategories[implode(self::TREE_CATEGORIES_DELIMITER, $key)]['description'] = isset($data["description"]) ? $data["description"] : '';
                $this->categoriesAndSubcategories[implode(self::TREE_CATEGORIES_DELIMITER, $key)]['is_family'] = isset($data["is_family"]) ? $data["is_family"] : 0;
                $this->categoriesAndSubcategories[implode(self::TREE_CATEGORIES_DELIMITER, $key)]['allow_mutual_products'] = isset($data["mutually_exclusive"]) ? $data["mutually_exclusive"] : '';
                $this->categoriesAndSubcategories[implode(self::TREE_CATEGORIES_DELIMITER, $key)]['hide_not_selected_products'] = isset($data["hide_not_selected_products"]) ? $data["hide_not_selected_products"] : 0;
            }
        }

        return false;
    }

    /**
     * Creates/Updates categories
     * And assigns the product sku
     */
    protected function createCategoryPath()
    {
        $this->categoryPath = self::DEFAULT_CATEGORY_PATH;
        $this->isLastNode = false;

        $length = count($this->categoriesAndSubcategories);
        $i = 0;
        /**
         * @var Mage_Catalog_Model_Category $category
         */
        // check if categories exist and also if the assigned subcategories exist
        foreach ($this->categoriesAndSubcategories as $key => $category) {
            $i++;
            $existentCategoryId = false;

            foreach ($this->categoryPaths as $path => $categoryName) {
                $subpath = explode('/', $path);
                $exid = array_pop($subpath);
                $subpath = implode('/', $subpath);
                if ($categoryName == $category['name'] && $subpath == $this->categoryPath) {
                    $existentCategoryId = $exid;
                }
            }

            if ($existentCategoryId) {
                $this->categoryPath .= self::CATEGORY_PATH_DELIMITER . $existentCategoryId;
                $this->categoriesAndSubcategories[$key]['path'] = $this->categoryPath;
                $this->categoriesAndSubcategories[$key]['existentCategory'] = $this->categoryModels[$existentCategoryId];
                $this->_log('Existing category found with name: ' . $category['name'] . ' and path ' . $this->categoryPath, true);
                $this->parentPath = $this->categoryPath;
                if ($i == $length) {
                    $this->isLastNode = true;
                }
                $this->categoryId = $existentCategoryId;
            } else {
                $this->_log('No existing category found with name: ' . $category['name'] . ', creating it now', true);
                $this->categoriesAndSubcategories[$key]['existentCategory'] = null;
                $this->categoriesAndSubcategories[$key]['path'] = $this->parentPath;
                if ($i == $length) {
                    $this->isLastNode = true;
                }

                $this->createCategory($this->categoriesAndSubcategories[$key]);
            }
        }
    }

    /**
     * Create or update a category
     *
     * @access private
     * @param array $categoryData (contains all category data: the category that has been found with the given name and the new data that
     * needs to be added for add and update)
     * @return void
     */
    private function createCategory($categoryData)
    {
        /**
         * @var Mage_Catalog_Model_Category $existentCategory
         * @var Mage_Catalog_Model_Category $categoryModel
         */
        $existentCategory = $categoryData['existentCategory'];

        /**
         * verify if the category exists:
         * - the category with the same name exists;
         * - has the same level;
         * - has the same path;
         * - is on the same website;
         * // todo: send website as parameter
         */

        $categoryIds = str_replace('/', ',', $categoryData['path']);
        $storeIds = $this->readConnection
            ->fetchPairs("SELECT core_store.code, core_store.store_id FROM core_store 
            INNER JOIN core_store_group ON core_store.group_id = core_store_group.group_id 
            WHERE core_store_group.root_category_id in ($categoryIds)");

        if ($existentCategory
            && $existentCategory['level'] == $categoryData['level']
            && $existentCategory['path'] == $categoryData['path']
            && in_array($this->storeId, $storeIds)
        ) {
            $this->_log('Category will be updated', true);
            // category exists => update category
            $this->saveCategory($categoryData, false);

            return;
        }

        $this->_log('Category will be added', true);
        $this->saveCategory($categoryData, true);
    }

    /**
     * Saves or updates a category
     * The update is done on the following fields: description, is_anchor and is_family
     * The save is done with the following fields: store, name, path, description, display mode, is active, is anchor, is family
     * All categories are added as active and with the display mode: products
     *
     * @access private
     * @param array $categoryData (contains the found category if is an update; also contains the data that needs to be updated or added)
     * @param bool $newCategory
     */
    private function saveCategory($categoryData, $newCategory = false)
    {
        $categoryModel = ($newCategory) ? Mage::getModel('catalog/category') : Mage::getModel('catalog/category')->load($categoryData['existentCategory']['entity_id']);
        $general = [];
        $general['stack'] = $this->stack;
        if ($newCategory) {
            $general['name'] = $categoryData['name'];
            $general['path'] = $this->categoryPath;
        }
        $general['display_mode'] = self::CATEGORY_DISPLAY_MODE;
        $general['is_active'] = self::CATEGORY_IS_ACTIVE;
        $this->_log('is_anchor is determined:', true);
        $general['is_anchor'] = $this->_getYesNoData($categoryData['is_anchor']);
        $general['family_position'] = $categoryData['family_position'];

        if ($this->isLastNode) {
            $this->_log('is_family is determined:', true);
            $general['is_family'] = $this->_getYesNoData($categoryData['is_family']);
            $general['description'] = $categoryData['description'];
            $general['allow_mutual_products'] = $this->_getYesNoData($categoryData['allow_mutual_products']);
            $general['hide_not_selected_products'] = $this->_getYesNoData($categoryData['hide_not_selected_products']);
        }

        $categoryModel->addData($general);
        try {
            $categoryModel->save();
            if ($newCategory) {
                $this->categoryId = $categoryModel->getId();
                $this->parentPath = $this->categoryPath;
                $this->categoryPath .= self::CATEGORY_PATH_DELIMITER . $this->categoryId;
            }
            $this->categoryPaths[$this->categoryPath] = $categoryData['name'];
            $this->categoryModels[$categoryModel->getId()] = $categoryModel;
            $this->dbCategories[$categoryData['category_tree']] = $this->categoryId;

            $this->_log('New category was added/updated with name <' . $categoryData['name'] . '> and path ' . $this->parentPath, true);
            unset($categoryModel);
        } catch (Exception $ex) {
            echo $ex->getMessage();
            $this->_logEx($ex);
        }
    }

    /**
     * Assign the product SKU to the category
     *
     * @access private
     * @return void
     */
    protected function assignProductToCategory()
    {
        if ($this->categoryId && $this->productId) {
            try {
                $query = 'INSERT INTO catalog_category_product VALUES (' . $this->categoryId . ', ' . $this->productId . ', 1) ON DUPLICATE KEY UPDATE POSITION=POSITION';
                Mage::getSingleton('core/resource')->getConnection('core_write')->exec($query);
                $this->_log('Product with sku ' . $this->productSkus[$this->productId] . ' was added to category id ' . $this->categoryId, true);
            } catch (Exception $ex) {
                echo $ex->getMessage();
                $this->_logEx($ex);
            }
        }
    }

    /**
     * Log function
     *
     * @param string $msg
     * @param bool $verbose
     */
    public function _log($msg, $verbose = false)
    {
        if($verbose && !$this->_debug) {
            return;
        }
        $this->_helper->logMsg('[' . $this->_currentProductSku . '] ' . $msg, false);
    }

    /**
     * Log function
     *
     * @param string $msg
     */
    public function _logError($msg)
    {
        $this->_helper->logMsg('[' . $this->_currentProductSku . '] ' . $msg);
    }
}
