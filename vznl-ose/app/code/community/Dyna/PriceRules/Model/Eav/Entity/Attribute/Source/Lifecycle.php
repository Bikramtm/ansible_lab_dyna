<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Dyna_PriceRules_Model_Eav_Entity_Attribute_Source_Lifecycle extends Omnius_PriceRules_Model_Eav_Entity_Attribute_Source_Lifecycle
{
    /**
     * Retrieve all options array
     *
     * @return array
     */
    public function getAllOptions()
    {
        if (is_null($this->_options)) {
            $this->_options = array(
                array(
                    "label" => Mage::helper("eav")->__("Acquisition"),
                    "value" => Dyna_Catalog_Model_ProcessContext::ACQ
                ),
                array(
                    "label" => Mage::helper("eav")->__("Reorder"),
                    "value" => Dyna_Catalog_Model_ProcessContext::REO
                ),
                array(
                    "label" => Mage::helper("eav")->__("Retention with BaseLine Update"),
                    "value" => Dyna_Catalog_Model_ProcessContext::RETBLU
                ),
                array(
                    "label" => Mage::helper("eav")->__("Retention without Baseline Update"),
                    "value" => Dyna_Catalog_Model_ProcessContext::RETNOBLU
                ),
                array(
                    "label" => Mage::helper("eav")->__("Retention with Direct Terminate of old contract"),
                    "value" => Dyna_Catalog_Model_ProcessContext::RETTER
                ),
                array(
                    "label" => Mage::helper("eav")->__("In Life Sale with Baseline Update"),
                    "value" => Dyna_Catalog_Model_ProcessContext::ILSBLU
                ),
                array(
                    "label" => Mage::helper("eav")->__("In Life Sale without Baseline Update"),
                    "value" => Dyna_Catalog_Model_ProcessContext::ILSNOBLU
                ),
                array(
                    "label" => Mage::helper("eav")->__("Migration with Change Of ContractPeriod"),
                    "value" => Dyna_Catalog_Model_ProcessContext::MIGCOC
                ),
                array(
                    "label" => Mage::helper("eav")->__("Migration without Change of ContractPeriod"),
                    "value" => Dyna_Catalog_Model_ProcessContext::MIGNOCOC
                ),
                array(
                    "label" => Mage::helper("eav")->__("Move"),
                    "value" => Dyna_Catalog_Model_ProcessContext::MOVE
                ),
                array(
                    "label" => Mage::helper("eav")->__("Transfer of User"),
                    "value" => Dyna_Catalog_Model_ProcessContext::TOU
                ),
            );
        }

        return $this->_options;
    }
}
