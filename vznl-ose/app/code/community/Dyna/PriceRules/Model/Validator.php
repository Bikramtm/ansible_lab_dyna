<?php

class Dyna_PriceRules_Model_Validator extends Omnius_PriceRules_Model_Validator
{
    const HINT_NONE = 0;
    const HINT_ONCE = 1;
    const HINT_ALWAYS = 2;
    const SALES_HINT = "sales_hint";

    protected $_perPackage;

    /** @var Dyna_Configurator_Model_Expression_Availability|null */
    protected $expressionAvailability = null;
    /** @var Dyna_Configurator_Model_Expression_Customer|null */
    protected $expressionCustomer = null;
    /** @var Dyna_Configurator_Model_Expression_Cart|null */
    protected $expressionCart = null;
    /** @var Dyna_Configurator_Model_Expression_InstallBase|null */
    protected $expressionInstallBase = null;
    protected $expressionActiveInstallBase = null;

    public function _construct()
    {
        $this->expressionAvailability = Mage::getModel('dyna_configurator/expression_availability');
        $this->expressionCustomer = Mage::getModel('dyna_configurator/expression_customer');
        $this->expressionCart = Mage::getModel('dyna_configurator/expression_cart');
        $this->expressionInstallBase = Mage::getModel('dyna_configurator/expression_installBase');
        $this->expressionActiveInstallBase = Mage::getModel('dyna_configurator/expression_activeInstallBase');
    }

    /**
     * Return item price
     *
     * @param Mage_Sales_Model_Quote_Item_Abstract $item
     * @return float
     */
    protected function _getItemPrice($item)
    {
        $price = $item->getDiscountCalculationPrice();
        $calcPrice = $item->getCalculationPrice();
        // Removed device/option restriction for mix match calculation
        if ($item->getMixmatchSubtotal()) {
            return $item->getMixmatchSubtotal() + $item->getMixmatchTax();
        }

        return ($price !== null) ? $price : $calcPrice;
    }

    /**
     * Quote item discount calculation process
     *
     * Footnote products should be excluded from rule processing
     *
     * @param   Omnius_Checkout_Model_Sales_Quote_Item|Mage_Sales_Model_Quote_Item_Abstract $item
     * @return  Mage_SalesRule_Model_Validator
     */
    public function process(Mage_Sales_Model_Quote_Item_Abstract $item)
    {
        if($item->getProduct()->getFootnoteId()) {
            return $this;
        }

        if ($item->getIsPromo()) {
            return $this;
        }

        if ($item->getPackageId() == $this->getActivePackage()->getPackageId()) {
            // Get applied rules ids from active package
            $appliedRuleIds = array_map('trim', explode(',', $this->getActivePackage()->getAppliedRuleIds()));
            /** @var Omnius_PriceRules_Helper_Data $priceHelper */
            $priceHelper = $this->getPriceRulesHelper();
            // We have a max accepted iteration for the no of applied rules per packet
            if(count($appliedRuleIds) >=  $priceHelper::$maxIterations){
                return $this;
            }

            $allRules = $this->_getRules();
            if (!count($allRules)) {
                return $this;
            }

            $address = $this->_getAddress($item);

            /** @var Omnius_Checkout_Model_Sales_Quote $quote */
            $quote = $address->getQuote();
            $initialItems = Mage::getSingleton('customer/session')->getOrderEditMode() ? Mage::getSingleton('checkout/session')->getInitialItems() : [];
            $existingRules = $this->getPriceRulesHelper()->explodeTrimmed($item->getAppliedRuleIds());
            $isDelivered = $this->getActivePackage()->isDelivered();
            $deliveredNotPromoCond = $isDelivered && Mage::getSingleton('customer/session')->getOrderEditMode() && !$item->isPromo();
            $deliveredPromoCond = $isDelivered && Mage::getSingleton('customer/session')->getOrderEditMode() && $item->isPromo();
            $itemProdCond = $deliveredNotPromoCond && isset($initialItems[$item->getProductId()]) && $quote->hasProductId($item->getProductId());
            $itemTargetCond = $deliveredPromoCond && isset($initialItems[$item->getTargetId()]) && $quote->hasProductId($item->getTargetId());

            $overrideCond = Mage::registry('is_override_promos') || $quote->getIsOverrulePromoMode();

            $isOverridden = $overrideCond || $itemProdCond || $itemTargetCond;
            $rules = $this->composeRules($item, $isOverridden, $allRules, $existingRules, $address);

            // overwrite rules
            $key = $this->getWebsiteId() . '_' . $this->getCustomerGroupId() . '_' . $this->getCouponCode() . '_' . $item->getPackageId() . '_' . $item->getProductId();
            $this->_rules[$key] = $rules;

            $allowedActions = [
                'ampromo_items' =>"promo_process_items_after",
                'ampromo_remove_items' =>"promo_process_items_remove",
                'ampromo_replace_items' =>"promo_process_items_replace",

                'process_add_product' => "process_add_product",
                'process_remove_product' => "process_remove_product",
                'process_replace_product' =>"process_replace_product",
                'sales_hint' => "sales_hint"
            ];

            $productActions = [
                'process_add_product', 'process_replace_product'
            ];

            $validHints = array();
            foreach ($rules as $rule) {
                if (!in_array($rule->getSimpleAction(), array_keys($allowedActions))
                    || (in_array($rule->getId(), $appliedRuleIds) && in_array($rule->getSimpleAction(), $productActions))
                ) {
                    continue;
                }

                if ($rule->getShowHint() && $rule->getServiceSourceExpression() && $rule->getSimpleAction() !== self::SALES_HINT) {
                    $validHints[] = $rule->getId();
                } else {
                    Mage::dispatchEvent($allowedActions[$rule->getSimpleAction()], array(
                        'rule' => $rule,
                        'item' => $item,
                        'quote' => $quote,
                        'address' => $address
                    ));

                    $this->getActivePackage()->setAppliedRuleIds($this->mergeIds($this->getActivePackage()->getAppliedRuleIds(), $rule->getId()))->save();
                }
            }

            // iterating again for hinting (@see VFDED1W3S-869) for validating each rule
            foreach ($rules as $rule) {
                if (!in_array($rule->getId(), $validHints)) {
                    continue;
                }
                // do a real validation by evaluation expression
                $this->validateRuleServiceSourceExpression($rule, true);
            }
        }

        return $this;
    }

    /**
     * @param Mage_Sales_Model_Quote_Item_Abstract $item
     * @return $this
     */
    public function processItem(Mage_Sales_Model_Quote_Item_Abstract $item)
    {
        $key = $this->getWebsiteId() . '_' . $this->getCustomerGroupId() . '_' . $this->getCouponCode() . '_' . $item->getPackageId() . '_' . $item->getProductId();
        // Commented this reset as price discounts can also be added by Dyna_Checkout_Model_SalesRule_Quote_Discount::process()
        // $item->setDiscountAmount(0);
        // $item->setBaseDiscountAmount(0);
        // $item->setDiscountPercent(0);
        $quote = $item->getQuote();
        $address = $this->_getAddress($item);

        $itemPrice = $this->_getItemPrice($item);
        $baseItemPrice = $this->_getItemBasePrice($item);
        $itemOriginalPrice = $this->_getItemOriginalPrice($item);
        $baseItemOriginalPrice = $this->_getItemBaseOriginalPrice($item);

        if ($itemPrice < 0) {
            return $this;
        }

        $appliedRuleIds = array();
        if (isset($this->_rules[$key])) {
            /** @var Omnius_Checkout_Model_SalesRule_Rule $rule */
            foreach ($this->_rules[$key] as &$rule) {
                $qty = $this->_getItemQty($item, $rule);
                $rulePercent = min(100, $rule->getDiscountAmount());

                $discountAmount = 0;
                $baseDiscountAmount = 0;
                //discount for original price
                $originalDiscountAmount = 0;
                $baseOriginalDiscountAmount = 0;

                switch ($rule->getSimpleAction()) {
                    case Mage_SalesRule_Model_Rule::TO_PERCENT_ACTION:
                        $rulePercent = max(0, 100 - $rule->getDiscountAmount());
                    //no break;
                    case Mage_SalesRule_Model_Rule::BY_PERCENT_ACTION:
                        $step = $rule->getDiscountStep();
                        if ($step) {
                            $qty = floor($qty / $step) * $step;
                        }
                        $_rulePct = $rulePercent / 100;
                        $discountAmount = ($qty * $itemPrice - $item->getDiscountAmount()) * $_rulePct;
                        $baseDiscountAmount = ($qty * $baseItemPrice - $item->getBaseDiscountAmount()) * $_rulePct;
                        //get discount for original price
                        $originalDiscountAmount = ($qty * $itemOriginalPrice - $item->getDiscountAmount()) * $_rulePct;
                        $baseOriginalDiscountAmount =
                            ($qty * $baseItemOriginalPrice - $item->getDiscountAmount()) * $_rulePct;

                        if (!$rule->getDiscountQty() || $rule->getDiscountQty() > $qty) {
                            $discountPercent = min(100, $item->getDiscountPercent() + $rulePercent);
                            $item->setDiscountPercent($discountPercent);
                        }
                        break;
                    case Mage_SalesRule_Model_Rule::TO_FIXED_ACTION:
                        $quoteAmount = $quote->getStore()->convertPrice($rule->getDiscountAmount());
                        $discountAmount = $qty * ($itemPrice - $quoteAmount);
                        $baseDiscountAmount = $qty * ($baseItemPrice - $rule->getDiscountAmount());
                        //get discount for original price
                        $originalDiscountAmount = $qty * ($itemOriginalPrice - $quoteAmount);
                        $baseOriginalDiscountAmount = $qty * ($baseItemOriginalPrice - $rule->getDiscountAmount());
                        break;

                    case Mage_SalesRule_Model_Rule::BY_FIXED_ACTION:
                        $step = $rule->getDiscountStep();
                        if ($step) {
                            $qty = floor($qty / $step) * $step;
                        }
                        $quoteAmount = $quote->getStore()->convertPrice($rule->getDiscountAmount());
                        $discountAmount = $qty * $quoteAmount;
                        $baseDiscountAmount = $qty * $rule->getDiscountAmount();
                        break;

                    case Mage_SalesRule_Model_Rule::CART_FIXED_ACTION:
                        if (empty($this->_rulesItemTotals[$rule->getId()])) {
                            Mage::throwException(Mage::helper('salesrule')->__('Item totals are not set for rule.'));
                        }

                        /**
                         * prevent applying whole cart discount for every shipping order, but only for first order
                         */
                        if ($quote->getIsMultiShipping()) {
                            $usedForAddressId = $this->getCartFixedRuleUsedForAddress($rule->getId());
                            if ($usedForAddressId && $usedForAddressId != $address->getId()) {
                                break;
                            } else {
                                $this->setCartFixedRuleUsedForAddress($rule->getId(), $address->getId());
                            }
                        }
                        $cartRules = $address->getCartFixedRules();
                        if (!isset($cartRules[$rule->getId()])) {
                            $cartRules[$rule->getId()] = $rule->getDiscountAmount();
                        }

                        if ($cartRules[$rule->getId()] > 0) {
                            if ($this->_rulesItemTotals[$rule->getId()]['items_count'] <= 1) {
                                $quoteAmount = $quote->getStore()->convertPrice($cartRules[$rule->getId()]);
                                $baseDiscountAmount = min($baseItemPrice * $qty, $cartRules[$rule->getId()]);
                            } else {
                                $discountRate = $baseItemPrice * $qty /
                                    $this->_rulesItemTotals[$rule->getId()]['base_items_price'];
                                $maximumItemDiscount = $rule->getDiscountAmount() * $discountRate;
                                $quoteAmount = $quote->getStore()->convertPrice($maximumItemDiscount);

                                $baseDiscountAmount = min($baseItemPrice * $qty, $maximumItemDiscount);
                                $this->_rulesItemTotals[$rule->getId()]['items_count']--;
                            }

                            $discountAmount = min($itemPrice * $qty, $quoteAmount);
                            $discountAmount = $quote->getStore()->roundPrice($discountAmount);
                            $baseDiscountAmount = $quote->getStore()->roundPrice($baseDiscountAmount);

                            //get discount for original price
                            $originalDiscountAmount = min($itemOriginalPrice * $qty, $quoteAmount);
                            $baseOriginalDiscountAmount = $quote->getStore()->roundPrice($baseItemOriginalPrice);

                            $cartRules[$rule->getId()] -= $baseDiscountAmount;
                        }
                        $address->setCartFixedRules($cartRules);

                        break;

                    case Mage_SalesRule_Model_Rule::BUY_X_GET_Y_ACTION:
                        $x = $rule->getDiscountStep();
                        $y = $rule->getDiscountAmount();
                        if (!$x || $y > $x) {
                            break;
                        }
                        $buyAndDiscountQty = $x + $y;

                        $fullRuleQtyPeriod = floor($qty / $buyAndDiscountQty);
                        $freeQty = $qty - $fullRuleQtyPeriod * $buyAndDiscountQty;

                        $discountQty = $fullRuleQtyPeriod * $y;
                        if ($freeQty > $x) {
                            $discountQty += $freeQty - $x;
                        }

                        $discountAmount = $discountQty * $itemPrice;
                        $baseDiscountAmount = $discountQty * $baseItemPrice;
                        //get discount for original price
                        $originalDiscountAmount = $discountQty * $itemOriginalPrice;
                        $baseOriginalDiscountAmount = $discountQty * $baseItemOriginalPrice;
                        break;
                    default:
                        break;
                }

                if (
                    $discountAmount > $item->getRowTotalInclTax()
                    && $rule->getCouponType() != Omnius_Checkout_Model_SalesRule_Rule::COUPON_TYPE_NO_COUPON
                ) {
                    $discountAmount = $item->getRowTotalInclTax();
                }

                $result = new Varien_Object(array(
                    'discount_amount' => $discountAmount,
                    'base_discount_amount' => $baseDiscountAmount,
                ));
                Mage::dispatchEvent('salesrule_validator_process', array(
                    'rule' => $rule,
                    'item' => $item,
                    'address' => $address,
                    'quote' => $quote,
                    'qty' => $qty,
                    'result' => $result,
                ));

                $discountAmount = $result->getDiscountAmount();
                $baseDiscountAmount = $result->getBaseDiscountAmount();

                $percentKey = $item->getDiscountPercent();
                /**
                 * Process "delta" rounding
                 */
                if ($percentKey) {
                    $delta = isset($this->_roundingDeltas[$percentKey]) ? $this->_roundingDeltas[$percentKey] : 0;
                    $baseDelta = isset($this->_baseRoundingDeltas[$percentKey])
                        ? $this->_baseRoundingDeltas[$percentKey]
                        : 0;
                    $discountAmount += $delta;
                    $baseDiscountAmount += $baseDelta;

                    $this->_roundingDeltas[$percentKey] = $discountAmount -
                        $quote->getStore()->roundPrice($discountAmount);
                    $this->_baseRoundingDeltas[$percentKey] = $baseDiscountAmount -
                        $quote->getStore()->roundPrice($baseDiscountAmount);
                    $discountAmount = $quote->getStore()->roundPrice($discountAmount);
                    $baseDiscountAmount = $quote->getStore()->roundPrice($baseDiscountAmount);
                } else {
                    $discountAmount = $quote->getStore()->roundPrice($discountAmount);
                    $baseDiscountAmount = $quote->getStore()->roundPrice($baseDiscountAmount);
                }

                /**
                 * We can't use row total here because row total not include tax
                 * Discount can be applied on price included tax
                 */

                $itemDiscountAmount = $item->getDiscountAmount();
                $itemBaseDiscountAmount = $item->getBaseDiscountAmount();

                $discountAmount = min($itemDiscountAmount + $discountAmount, $itemPrice * $qty);
                $baseDiscountAmount = min($itemBaseDiscountAmount + $baseDiscountAmount, $baseItemPrice * $qty);

                $item->setDiscountAmount($discountAmount);
                $item->setBaseDiscountAmount($baseDiscountAmount);

                $item->setOriginalDiscountAmount($originalDiscountAmount);
                $item->setBaseOriginalDiscountAmount($baseOriginalDiscountAmount);

                $appliedRuleIds[$rule->getRuleId()] = $rule->getRuleId();

                $this->_maintainAddressCouponCode($address, $rule);
                $this->_addDiscountDescription($address, $rule);

                if ($rule->getStopRulesProcessing()) {
                    break;
                }
            }
            $item->setAppliedRuleIds(join(',', $appliedRuleIds));
            $address->setAppliedRuleIds($this->mergeIds($address->getAppliedRuleIds(), $appliedRuleIds));
            $quote->setAppliedRuleIds($this->mergeIds($quote->getAppliedRuleIds(), $appliedRuleIds));
        }

        return $this;
    }

    /**
     * Check if rule can be applied for specific address/quote/customer
     *
     * @param   Mage_SalesRule_Model_Rule $rule
     * @param   Mage_Sales_Model_Quote_Address $address
     * @param   int $packageId
     * @param array $existingRules
     * @return bool
     */
    protected function _canProcessRule($rule, $address, $packageId = null, $existingRules = [])
    {
        if ($rule->hasIsValidForAddress($address) && isset($this->_perPackage[$packageId][$rule->getRuleId()])) {
            return $rule->getIsValidForAddress($address) && $this->_perPackage[$packageId][$rule->getRuleId()];
        }

        // Check if the rule is available
        if (($rule->getFromDate() && strtotime($rule->getFromDate()) > time())
            || ($rule->getToDate() && strtotime($rule->getToDate()) + 86400 < time())
        ) {
            $rule->setIsValidForAddress($address, false);
            $this->_perPackage[$packageId][$rule->getRuleId()] = false;

            return false;
        }

        // Check if the rule's service source expression is met
        if (!$this->validateRuleServiceSourceExpression($rule)) {
            $rule->setIsValidForAddress($address, false);
            $this->_perPackage[$packageId][$rule->getRuleId()] = false;
            return false;
        }

        // Check per coupon usage limit
        if ($rule->getCouponType() != Mage_SalesRule_Model_Rule::COUPON_TYPE_NO_COUPON) {
            $couponCode = $this->getCouponCode();
            if (strlen($couponCode)) {
                $coupon = Mage::getModel('salesrule/coupon');
                $coupon->load($couponCode, 'code');

                if ($coupon->getId() && $this->getActivePackage()->getCoupon() != $couponCode) {
                    // check entire usage limit
                    if ($coupon->getUsageLimit() && $coupon->getTimesUsed() >= $coupon->getUsageLimit()) {
                        $rule->setIsValidForAddress($address, false);
                        $this->_perPackage[$packageId][$rule->getRuleId()] = false;

                        return false;
                    }
                    // check per customer usage limit
                    $customerId = $address->getCustomerId();
                    if ($customerId && $coupon->getUsagePerCustomer()) {
                        $couponUsage = new Varien_Object();
                        Mage::getResourceModel('salesrule/coupon_usage')->loadByCustomerCoupon($couponUsage, $customerId, $coupon->getId());
                        if ($couponUsage->getCouponId() &&
                            $couponUsage->getTimesUsed() >= $coupon->getUsagePerCustomer()
                        ) {
                            $rule->setIsValidForAddress($address, false);
                            $this->_perPackage[$packageId][$rule->getRuleId()] = false;

                            return false;
                        }
                    }
                }
            }
        }

        // Check per rule usage limit
        $ruleId = $rule->getId();
        if ($ruleId && $rule->getUsesPerCustomer()) {
            $customerId = $address->getCustomerId();
            $ruleCustomer = Mage::getModel('salesrule/rule_customer');
            $ruleCustomer->loadByCustomerRule($customerId, $ruleId);
            if ($ruleCustomer->getId()
                && !in_array($rule->getId(), $existingRules)
                && ($ruleCustomer->getTimesUsed() >= $rule->getUsesPerCustomer())
            ) {
                $rule->setIsValidForAddress($address, false);
                $this->_perPackage[$packageId][$rule->getRuleId()] = false;

                return false;
            }
        }
        $rule->afterLoad();
        // Quote does not meet rule's conditions
        if (!$rule->validate($address)) {
            $rule->setIsValidForAddress($address, false);
            $this->_perPackage[$packageId][$rule->getRuleId()] = false;

            return false;
        }

        // Passed all validations, remember to be valid
        $rule->setIsValidForAddress($address, true);
        $this->_perPackage[$packageId][$rule->getRuleId()] = true;

        return true;
    }
    /**
     * Public accessor for the _canProcessRule method.
     * @param Omnius_Checkout_Model_SalesRule_Rule $rule
     * @param Omnius_Checkout_Model_Sales_Quote_Address $address
     * @param int $activePackageId
     * @return bool
     *
     * Public method for canProcessRule
     */
    public function canProcessRules($rule, $address, $activePackageId)
    {
        $existingRules = [];
        $allItems = $address->getQuote()->getAllItems();
        /** @var $item Dyna_Checkout_Model_Sales_Quote_Item */
        foreach ($allItems as $item) {
            if ($item->getPackageId() == $activePackageId) {
                $existingRules = array_merge($this->getPriceRulesHelper()->explodeTrimmed($item->getAppliedRuleIds()), $existingRules);
            }
        }
        return $this->_canProcessRule($rule, $address, $activePackageId, $existingRules = []);
    }

    /**
     * @param Omnius_Checkout_Model_SalesRule_Rule $rule
     * @return bool
     */
    public function validateRuleServiceSourceExpression(Omnius_Checkout_Model_SalesRule_Rule $rule, $doNotSkipHints = false) {
        if ($rule->getSimpleAction() != self::SALES_HINT && $rule->getShowHint() && !$doNotSkipHints) {
            return true;
        }
        try {
            $this->expressionCart->setRule($rule);
            /**
             * @var $dynaCoreHelper Dyna_Core_Helper_Data
             */
            $dynaCoreHelper = Mage::helper('dyna_core');
            $inputParams = [
                'availability' => $this->expressionAvailability,
                'customer' => $this->expressionCustomer,
                'cart' => $this->expressionCart,
                'installBase' => $this->expressionInstallBase,
                'activeInstallBase' => $this->expressionActiveInstallBase,
                'agent' => Mage::getModel('dyna_configurator/expression_agent'),
            ];
            $ssExpression = $rule->getServiceSourceExpression();
            $passed = false;

            if (empty(trim($ssExpression)) || $dynaCoreHelper->evaluateExpressionLanguage($ssExpression, $inputParams)) {
                $passed = true;
            }
            return $passed;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @param int $websiteId
     * @param int $customerGroupId
     * @param string $couponCode
     * @param array $packages
     * @param null $address
     * @return Mage_SalesRule_Model_Validator
     */
    public function init($websiteId, $customerGroupId, $couponCode, $packages = [], $address = null) {
        // Installed base packages should be excluded
        foreach($packages as $key => $package) {
            if($package->getInitialInstalledBaseProducts()) {
                if (is_array($packages)) {
                    unset($packages[$key]);
                } else {
                    $packages->removeItemByKey($package->getId());
                }
            }
        }

        if (!$address || count($packages) == 0) {
            return $this;
        }

        $quote = $address->getQuote();
        $couponsPerPackage = $quote->getCouponsPerPackage();
        foreach ($packages as $package) {
            $couponCode = (is_array($couponsPerPackage) && isset($couponsPerPackage[$package->getPackageId()])) ? $couponsPerPackage[$package->getPackageId()] : $package->getCoupon();
            $key = $websiteId . '_' . $customerGroupId . '_' . $couponCode . '_' . $package->getPackageId();
            if (!isset($this->_rules[$key])) {
                $this->_rules[$key] = [];
                // Set the active package id, as some validators need to be aware of the current package.
                $quote->setActivePackageId($package->getPackageId());

                $allRules = Mage::helper('rulematch')
                    ->reset()
                    ->setActivePackage($package)
                    ->getApplicableRules(true, $couponCode);

                if (!count($allRules)) {
                    continue;
                }

                $this->expressionCart->updateCurrentPackages($quote);

                /** @var Omnius_Checkout_Model_SalesRule_Rule $rule */
                foreach ($allRules as &$rule) {
                    if ($this->_canProcessRule($rule, $address, $package->getPackageId())) {
                        $this->_rules[$key][] = $rule;
                    }
                }
            }
        }

        return $this;
    }

    public function _clearData()
    {
        unset($this->_cache);
        unset($this->_promoRules);
        unset($this->_rules);
        unset($this->_perPackage);
        unset($this->_baseRoundingDeltas);
        unset($this->_roundingDeltas);
        unset($this->couponsApplied);
        unset($this->_removeCoupon);
        unset($this->_resultCache);
        unset($this->_address);
        unset($this->_rulesItemTotals);
        unset($this->_cartFixedRuleUsedForAddress);
    }

    /**
     * @param Omnius_Checkout_Model_Sales_Quote_Item|Mage_Sales_Model_Quote_Item_Abstract $item
     * @param $isOverridden
     * @param $allRules
     * @param $appliedRuleIds
     * @param $address
     * @return array
     */
    protected function composeRules($item, $isOverridden, $allRules, $appliedRuleIds, $address)
    {
        if (!$isOverridden) {
            $promoRules = [];
            $rules = [];
            $couponRules = [];

            $quote = $address->getQuote();


            $foundValidRule = false;
            /** @var Omnius_Checkout_Model_SalesRule_Rule $rule */
            foreach ($allRules as $rule) {
                if (!$this->validateItemByRule($rule, $item)) {
                    continue;
                }
                $this->processRule($item->getPackageId(), $rule, $appliedRuleIds, $address, $promoRules, $couponRules, $rules);
                $foundValidRule = true;
            }

            if ($foundValidRule && $this->getCouponCode()) {
                /** @var Dyna_Package_Model_Package $packageModel */
                if ($orderEditQuoteId = Mage::getSingleton('customer/session')->getOrderEdit()) {
                    $orderEditQuote = Mage::getModel('sales/quote')->load($orderEditQuoteId);
                    $packageModel = Mage::getModel('package/package')->getCollection()
                        ->addFieldToFilter('package_id', $item->getPackageId())
                        ->addFieldToFilter('order_id', $orderEditQuote->getSuperOrderEditId())
                        ->getFirstItem();
                } else {
                    $packageModel = $quote->getCartPackage($item->getPackageId());
                }

                /** @var Omnius_PriceRules_Helper_External_Coupon_Validator $externalValidator */
                $externalValidator = Mage::helper('pricerules/external_coupon_validator');

                if($externalValidator->isValid($this->getCouponCode(), $quote, $packageModel)) {
                    $this->couponsApplied[$item->getPackageId()] = $this->getCouponCode();
                }
            }


            if (!empty($promoRules)) {
                $rules = array_merge($promoRules, $couponRules);
            } else {
                $rules = array_merge($rules, $couponRules);
            }
        } else {
            $appliedRuleIds = $this->getPriceRulesHelper()->explodeTrimmed($item->getAppliedRuleIds());
            if ($item->getAppliedRuleIds() == $item->getManualRuleId() && $item->getManualRuleId() != null) {
                $appliedRuleIds = $this->getPriceRulesHelper()->explodeTrimmed($item->getPreviousRules());
            }
            $rules = Mage::getModel('salesrule/rule')->getCollection()->addFieldToFilter('rule_id', array('in' => $appliedRuleIds));
            $this->couponsApplied[$item->getPackageId()] = $this->getCouponCode();
        }

        return $rules;
    }
}
