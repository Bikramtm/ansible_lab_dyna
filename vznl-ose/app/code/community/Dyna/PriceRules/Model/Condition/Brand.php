<?php

/**
 * Class Dyna_PriceRules_Model_Condition_Brand
 */
class Dyna_PriceRules_Model_Condition_Brand extends Omnius_PriceRules_Model_Condition_Abstract
{

    public function loadAttributeOptions()
    {
        $this->setupSelectCondition('Brand', 'brand');
        return $this;
    }

    public function getValueSelectOptions()
    {
        $options[] = array('value' => 'vodafone', 'label' => 'Vodafone');
        return $this->setupOptions($options);
    }

    public function validate(Varien_Object $object)
    {
        return $this->validateAttribute('vodafone');
    }

}
