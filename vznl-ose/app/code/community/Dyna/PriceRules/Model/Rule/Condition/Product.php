<?php

class Dyna_PriceRules_Model_Rule_Condition_Product extends Mage_SalesRule_Model_Rule_Condition_Product
{
    /**
     * @var null|array
     */
    protected static $_categoryIds = null;

    /**
     * Preloads category IDs with anchors without doing a fetchCol for every product.
     *
     * @return array|null
     */
    protected static function _getCategoryIdsWithAnchors()
    {
        if (self::$_categoryIds === null) {
            /** @var Mage_Core_Model_Resource $resource */
            $resource = Mage::getSingleton('core/resource');

            /** @var Varien_Db_Adapter_Interface $connection */
            $connection = $resource->getConnection('default_read');

            $selectRootCategories = $connection->select()
                ->from(
                    array($resource->getTableName('catalog/category')),
                    array('entity_id')
                )
                ->where('level <= 1');
            $rootIds = $connection->fetchCol($selectRootCategories);
            $select = $connection->select()
                ->from(
                    array($resource->getTableName('catalog/category_product_index')),
                    array('product_id', 'category_id')
                )
                ->where('category_id NOT IN(?)', $rootIds);

            foreach ($connection->fetchAll($select) as $row) {
                self::$_categoryIds[$row['product_id']][] = $row['category_id'];
            }
        }
        return self::$_categoryIds;
    }

    /**
     * Validate product attribute value for condition
     * Overriding category ids with anchor categories
     * @OMNVFDE-3266: categories associated to a product should take into account anchor categories when evaluating rules
     *
     * @param Varien_Object $object
     * @return bool
     */
    public function validate(Varien_Object $object)
    {
        /** @var Mage_Catalog_Model_Product $product */
        $product = ($object instanceof Mage_Catalog_Model_Product) ? $object : $object->getProduct();
        if (!($product instanceof Mage_Catalog_Model_Product)) {
            $product = Mage::getModel('catalog/product')->load($object->getProductId());
        }

        $product
            ->setQuoteItemQty($object->getQty())
            ->setQuoteItemPrice($object->getPrice()) // possible bug: need to use $object->getBasePrice()
            ->setQuoteItemRowTotal($object->getBaseRowTotal());

        $attrCode = $this->getAttribute();

        if ('category_ids' == $attrCode) {
            $categoryIds = self::_getCategoryIdsWithAnchors();
            if (isset($categoryIds[$product->getId()])) {
                return $this->validateAttribute($categoryIds[$product->getId()]);
            }
            return false;
        }

        return parent::validate($product);
    }
}