<?php

/**
 *
 */
class Dyna_PriceRules_Block_Adminhtml_Promo_Quote_Edit extends Omnius_PriceRules_Block_Adminhtml_Promo_Quote_Edit
{
    public function __construct()
    {
        parent::__construct();

        $this->_formScripts[] = " 
			function promo_check() {
				$('rule_discount_qty').up().up().show();
				$('rule_discount_step').up().up().show();
				$('rule_apply_to_shipping').up().up().show();
				$('rule_actions_fieldset').up().show();
				$('rule_promo_sku').up().up().hide();
			    $('rule_promo_sku_replace').up().up().hide();
			
			    $('rule_discount_amount').up().up().show();
                $('rule_discount_qty').up().up().show();
                $('rule_discount_step').up().up().show();
                $('rule_apply_to_shipping').up().up().show();
                $('rule_simple_free_shipping').up().up().show();
                $('rule_stop_rules_processing').up().up().show();
                $('rule_sales_hint_message').up().up().hide();
                               
			
				if ('ampromo_cart' == $('rule_simple_action').value) {
				    $('rule_actions_fieldset').up().hide(); 
					$('rule_discount_qty').up().up().hide();
					$('rule_discount_step').up().up().hide();
					$('rule_sales_hint_message').setValue(0).up().up().hide();
					
					$('rule_apply_to_shipping').up().up().hide();
					$('rule_promo_sku').up().up().show();
				} 
				if ('ampromo_items' == $('rule_simple_action').value 
				|| 'ampromo_remove_items' == $('rule_simple_action').value
				|| 'ampromo_replace_items' == $('rule_simple_action').value
				|| 'process_add_product' == $('rule_simple_action').value
				|| 'process_remove_product' == $('rule_simple_action').value
				|| 'process_replace_product' == $('rule_simple_action').value)
				{
					$('rule_apply_to_shipping').setValue(0).up().up().hide();
					$('rule_sales_hint_message').setValue(0).up().up().hide();
					$('rule_promo_sku').up().up().show();
                    
                    $('rule_discount_amount').setValue(0).up().up().hide();
                    $('rule_discount_qty').setValue(0).up().up().hide();
                    $('rule_discount_step').setValue(0).up().up().hide();
                    $('rule_apply_to_shipping').setValue(0).up().up().hide();
                    $('rule_simple_free_shipping').setValue(0).up().up().hide();
                    $('rule_stop_rules_processing').setValue(0).up().up().hide();
                    if ('ampromo_replace_items' == $('rule_simple_action').value || 'process_replace_product' == $('rule_simple_action').value){
                        $('rule_promo_sku_replace').up().up().show();
                    }
				}
				if ('sales_hint' == $('rule_simple_action').value)
				{
					$('rule_apply_to_shipping').setValue(0).up().up().hide();
					$('rule_promo_sku').up().up().hide();
                    $('rule_promo_sku_replace').up().up().hide();
                    $('rule_discount_amount').setValue(0).up().up().hide();
                    $('rule_discount_qty').setValue(0).up().up().hide();
                    $('rule_discount_step').setValue(0).up().up().hide();
                    $('rule_apply_to_shipping').setValue(0).up().up().hide();
                    $('rule_simple_free_shipping').setValue(0).up().up().hide();
                    $('rule_stop_rules_processing').setValue(0).up().up().hide();
                    $('rule_sales_hint_message').up().up().show();
                    
				}
			}
			promo_check();
        ";
    }
}
