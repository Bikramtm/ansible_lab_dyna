<?php
/**
 * Add service_source_expression field
 */
$this->startSetup();


$fieldsSql = 'SHOW COLUMNS FROM ' . $this->getTable('salesrule/rule');
$cols = $this->getConnection()->fetchCol($fieldsSql);

if (!in_array('service_source_expression', $cols)){
    $this->run("ALTER TABLE `{$this->getTable('salesrule/rule')}` ADD `service_source_expression` text NULL");
}

$this->endSetup();
