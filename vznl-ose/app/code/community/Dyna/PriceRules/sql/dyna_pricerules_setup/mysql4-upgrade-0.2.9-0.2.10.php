<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Core_Model_Resource_Setup $this */
$this->startSetup();
$salesTable = $this->getTable('salesrule/rule');

if (!$this->getConnection()->tableColumnExists($salesTable, 'sales_hint_message')) {
    $this->getConnection()
        ->addColumn(
            $salesTable,
            'sales_hint_message',
            array(
                'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
                'comment' => 'Message for sales hint',
                'nullable' => true,
                'default' => null
            )
        );
}

$this->endSetup();
