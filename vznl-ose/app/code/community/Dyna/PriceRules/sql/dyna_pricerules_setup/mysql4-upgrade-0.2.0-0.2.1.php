<?php
/**
 * Add discount period fields
 */
$this->startSetup();


$fieldsSql = 'SHOW COLUMNS FROM ' . $this->getTable('salesrule/rule');
$cols = $this->getConnection()->fetchCol($fieldsSql);

if (!in_array('discount_period_amount', $cols)){
    $this->run("ALTER TABLE `{$this->getTable('salesrule/rule')}` ADD `discount_period_amount` smallint(5) unsigned NULL");
}
if (!in_array('discount_period', $cols)){
    $this->run("ALTER TABLE `{$this->getTable('salesrule/rule')}` ADD `discount_period` varchar(32) NULL");
}

$this->endSetup();
