<?php
/**
 * Add red_sales_id field
 */

/** @var Mage_Core_Model_Resource_Setup $this */
$installer = $this;
$installer->startSetup();

$tableName = $this->getTable('sales_rule_product_match');
// Add new process_context column
$this->getConnection()->addColumn($tableName, 'red_sales_id', [
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'length' => 50,
    'default' => null,
    'required' => false,
    'comment' => "Red Sales Id",
]);

$installer->endSetup();