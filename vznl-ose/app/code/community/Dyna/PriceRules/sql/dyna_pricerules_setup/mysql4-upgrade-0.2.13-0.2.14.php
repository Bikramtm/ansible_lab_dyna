<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Core_Model_Resource_Setup $this */

$this->startSetup();

if ($this->getConnection()->tableColumnExists($this->getTable('salesrule/rule'), 'install_base_target')) {
    $this->getConnection()->dropColumn($this->getTable('salesrule/rule'), 'install_base_target');
}

$this->endSetup();
