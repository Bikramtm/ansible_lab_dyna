<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Core_Model_Resource_Setup $this */
$this->startSetup();

$this->getConnection()->addColumn($this->getTable('salesrule/rule'), 'show_hint', array(
    'type' => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
    'nullable'  => true,
    'comment'   => 'Determine whether or not hint should be shown on sales rule',
));

$this->endSetup();
