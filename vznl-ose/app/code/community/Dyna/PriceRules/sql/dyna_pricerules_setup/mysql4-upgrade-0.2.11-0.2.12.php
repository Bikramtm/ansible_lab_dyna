<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Core_Model_Resource_Setup $this */
$this->startSetup();
$salesTable = $this->getTable('salesrule/rule');

if (!$this->getConnection()->tableColumnExists($salesTable, 'sales_hint_title')) {
    $this->getConnection()
        ->addColumn(
            $salesTable,
            'sales_hint_title',
            array(
                'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
                'comment' => 'Sales hint title',
                'nullable' => true,
                'after' => 'show_hint',
                'default' => null
            )
        );
}

if (!$this->getConnection()->tableColumnExists($salesTable, 'rule_type')) {
    $this->getConnection()
        ->addColumn(
            $salesTable,
            'rule_type',
            array(
                'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
                'comment' => 'Rule type',
                'nullable' => true,
                'after' => 'description',
                'default' => null
            )
        );
}

$this->endSetup();
