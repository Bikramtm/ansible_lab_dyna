<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Cache_Model_Cache
 */
class Dyna_Cache_Model_Cache extends Varien_Object
{
    /** Debug settings */
    const ENVIRONMENT_CACHE_TTL = 'omnius_general/cache/cache_ttl';
    const ENVIRONMENT_FRONTEND_CACHE_TTL = 'omnius_general/cache/frontend_cache_ttl';
    const ENVIRONMENT_FRONTEND_CACHE_PREFIX = 'omnius_general/cache/frontend_cache_prefix';
    const ENVIRONMENT_MODE_CONFIG_PATH = 'omnius_general/cache/is_dev';
    const VANISH_IP_PATH = 'omnius_general/cache/varnish_ip';

    const CACHE_TAG = 'APPLICATION_CACHE';
    const AGENT_TAG = 'AGENT_DEALER_CACHE';
    const PRODUCT_TAG = 'DYNA_PRODUCT_CACHE';

    /** @var string */
    protected $_prefix = 'dyna_cache';

    /** @var Varien_Cache_Core */
    protected $_cache;

    /** @var int|mixed */
    protected $_cacheTTL = 86400; //default in case it's not set in the backend

    /** @var bool */
    protected $_isDev = false;

    /** @var array */
    protected $_registry = array();

    /** @var null */
    protected $_currMemoryLimit = -1;

    /**
     * Minimum amount of memory allowed for PHP to use
     * If limit is greater or equal to this, values saved and
     * retrieved from cache will be also kept and updated in
     * memory to reduce calls to the cache backend
     * This limit is present to prevent "Out of memory" issues that may appear
     *
     * @var int
     */
    protected $_minAllocatedMemory = 2048000000; //bytes

    /**
     * Prepare cache instance and config
     */
    public function __construct()
    {
        parent::_construct();

        $appVersion = Mage::helper('dyna_cache')->getAppVersion();
        $this->_prefix = $appVersion ? md5($appVersion) : $this->_prefix;
        $this->_cache = Mage::app()->getCache();
        if (Mage::getStoreConfig(self::ENVIRONMENT_CACHE_TTL) !== NULL) {
            $this->_cacheTTL = (int) Mage::getStoreConfig(self::ENVIRONMENT_CACHE_TTL);
        }
        $this->_isDev = (bool) Mage::getStoreConfigFlag(self::ENVIRONMENT_MODE_CONFIG_PATH);

        if (function_exists('ini_get')) {
            $value = trim(ini_get('memory_limit'));
            $unit = strtolower(substr($value, -1, 1));
            $value = (int) $value;
            switch ($unit) {
                case 'g':
                    $value *= 1024;
                // no break (cumulative multiplier)
                case 'm':
                    $value *= 1024;
                // no break (cumulative multiplier)
                case 'k':
                    $value *= 1024;
            }

            $this->_currMemoryLimit = $value;

            unset($value);
            unset($unit);
        }
    }

    /**
     * @return mixed
     */
    public function isTestMode()
    {
        return $this->_isDev;
    }

    /**
     * @return int|mixed
     */
    public function getTtl()
    {
        return $this->_cacheTTL;
    }

    /**
     * @param bool $forceNewPrefix
     * @return string
     */
    public function getFrontendCachePrefix($forceNewPrefix = false)
    {
        if (!$forceNewPrefix
            && strlen($prefix = trim(Mage::getStoreConfig(self::ENVIRONMENT_FRONTEND_CACHE_PREFIX)))
        ) {
            return md5($prefix);
        }
        Mage::getConfig()->saveConfig(self::ENVIRONMENT_FRONTEND_CACHE_PREFIX, $prefix = md5(time() . rand(0, 1000)));
        Mage::getConfig()->cleanCache();

        return md5($prefix);
    }

    /**
     * @return int|mixed
     */
    public function getFrontendCacheTtl()
    {
        if (0 === ($ttl = (int) Mage::getStoreConfig(self::ENVIRONMENT_FRONTEND_CACHE_TTL))) {
            return $this->_cacheTTL;
        }

        return $ttl;
    }

    /**
     * @param $data
     * @param null $id
     * @param array $tags
     * @param bool $specificLifetime
     * @param int $priority
     * @return Mage_Core_Model_Abstract|void
     */
    public function save($data, $id = null, $tags = array(), $specificLifetime = false, $priority = 8)
    {
        $this->_cache->save($data, $this->prepareId($id), $this->prepareTags($tags), $specificLifetime, $priority);
    }

    /**
     * @param int $id
     * @param bool $doNotTestCacheValidity
     * @param bool $doNotUnserialize
     * @return bool|false|Mage_Core_Model_Abstract|mixed
     */
    public function load($id, $doNotTestCacheValidity = false, $doNotUnserialize = false, $cli = false)
    {
        //on admin, no cache should be used, to prevent confusion
        // (if a product is changed, the change will work but will not be visible)
        if (!$cli && (Mage::app()->getStore()->isAdmin() || Mage::getDesign()->getArea() == 'adminhtml')) {
            return false;
        }

        if ($this->isTestMode()) {
            return false;
        }

        $result = $this->_cache->load($this->prepareId($id), $doNotTestCacheValidity, $doNotUnserialize);

        return $result;
    }

    /**
     * @param array $tags
     * @param string $mode
     */
    public function clean($tags = array(), $mode = 'all')
    {
        $this->_registry = array();
        $this->_cache->clean($mode, $tags);
    }

    /**
     * @param $id
     * @return string
     */
    protected function prepareId($id)
    {
        return $id ? sprintf('%s_%s', $this->_prefix, (string) $id) : $id;
    }

    /**
     * @param array $tags
     * @return array
     */
    protected function prepareTags(array $tags)
    {
        return $tags;
    }

    /**
     * @param $key
     * @param $value
     */
    protected function preserve($key, $value)
    {
        if ($this->_currMemoryLimit != -1 && $this->_currMemoryLimit >= $this->_minAllocatedMemory) {
            $this->_registry[$key] = $value;
        }
    }

    /**
     * @param $key
     * @return bool
     */
    protected function alreadyRequested($key)
    {
        return isset($this->_registry[$key])
            ? $this->_registry[$key]
            : false;
    }
}
