<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Cache_Block_Cache
 */
class Dyna_Cache_Block_Cache extends Mage_Adminhtml_Block_Cache
{
    /**
     * Class constructor
     */
    public function __construct()
    {
        parent::__construct();

        $confirm = 'Flushing the %s cache will strain the system. Are you sure that you want flush it?';


        $message = sprintf(Mage::helper('core')->__($confirm), 'application');
        $this->_addButton('flush_application', array(
            'label' => Mage::helper('core')->__('Flush Application Cache'),
            'onclick' => 'confirmSetLocation(\'' . $message . '\', \'' . $this->getFlushApplicationCacheUrl() . '\')',
            'class' => 'delete',
        ));

        $message = sprintf(Mage::helper('core')->__($confirm), 'product');
        $this->_addButton('flush_product', array(
            'label' => Mage::helper('core')->__('Flush Product Cache'),
            'onclick' => 'confirmSetLocation(\'' . $message . '\', \'' . $this->getFlushProductCacheUrl() . '\')',
            'class' => 'delete',
        ));

        $message = sprintf(Mage::helper('core')->__($confirm), 'varnish');
        $this->_addButton('flush_varnish', array(
            'label' => Mage::helper('core')->__('Flush Varnish Cache'),
            'onclick' => 'confirmSetLocation(\'' . $message . '\', \'' . $this->getFlushVarnishCacheUrl() . '\')',
            'class' => 'delete',
        ));
    }

    /**
     * Get url for clean application cache storage
     */
    public function getFlushApplicationCacheUrl()
    {
        return $this->getUrl('*/*/flushApplicationCache');
    }

    /**
     * Get url for clean product cache storage
     */
    public function getFlushProductCacheUrl()
    {
        return $this->getUrl('*/*/flushProductCache');
    }

    /**
     * Get url for clean application varnish cache storage
     */
    public function getFlushVarnishCacheUrl()
    {
        return $this->getUrl('*/*/flushVarnishCache');
    }
}
