<?php

require_once Mage::getModuleDir('controllers', 'Omnius_MultiMapper') . DS . 'Adminhtml' . DS . 'MapperController.php';

/**
 * Class Dyna_MultiMapper_Adminhtml_MapperController
 */
class Dyna_MultiMapper_Adminhtml_MapperController extends Omnius_MultiMapper_Adminhtml_MapperController
{
    private $adminSession = null;

    /**
     * Save mapper addon data
     */
    public function saveAction()
    {
        $post_data = $this->getRequest()->getPost();

        if ($post_data) {
            try {
                /** Unlink addons from mapper data */
                /** @var $multiMapperModel Dyna_MultiMapper_Model_Mapper */
                $multiMapperModel = Mage::getModel("multimapper/mapper");
                $mapper = $this->getRequest()->getParam("id") ?  $multiMapperModel->load($this->getRequest()->getParam("id")) : $multiMapperModel;
                $newAddons = [];
                $addonProcessContexts = [];

                $i=1;
                while (true) {
                    if (array_key_exists("addon" . $i . "_technical_id", $post_data)) {
                        $newAddons["addon" . $i . "_id"] = [
                            "mapper_id" => $mapper->getId(),
                            "addon_id" => !empty($post_data["addon" . $i . "_id"]) ? (int)$post_data["addon" . $i . "_id"] : null,
                            "addon_name" => !empty($post_data["addon" . $i . "_name"]) ? trim($post_data["addon" . $i . "_name"]) : "",
                            "addon_value" => !empty($post_data["addon" . $i . "_value"]) ? trim($post_data["addon" . $i . "_value"]) : "",
                            "addon_additional_value" => !empty($post_data["addon" . $i . "_additional_value"]) ? trim($post_data["addon" . $i . "_additional_value"]) : "",
                            "nature_code" => !empty($post_data["addon" . $i . "_nature_code"]) ? trim($post_data["addon" . $i . "_nature_code"]) : "",
                            "promo_id" => !empty($post_data["addon" . $i . "_promo_id"]) ? trim($post_data["addon" . $i . "_promo_id"]) : "",
                            "technical_id" => !empty($post_data["addon" . $i . "_technical_id"]) ? trim($post_data["addon" . $i . "_technical_id"]) : "",
                            "backend" => !empty($post_data["addon" . $i . "_backend"]) ? trim($post_data["addon" . $i . "_backend"]) : "",
                        ];
                        $addonProcessContexts[$post_data['addon' . $i . '_id']] = $post_data["addon" . $i . "_process_context"] ?? [];
                        unset($post_data["addon" . $i . "_id"]);
                        unset($post_data["addon" . $i . "_name"]);
                        unset($post_data["addon" . $i . "_value"]);
                        unset($post_data["addon" . $i . "_additional_value"]);
                        unset($post_data["addon" . $i . "_nature_code"]);
                        unset($post_data["addon" . $i . "_promo_id"]);
                        unset($post_data["addon" . $i . "_technical_id"]);
                        unset($post_data["addon" . $i . "_backend"]);
                        unset($post_data["addon" . $i . "_process_context"]);
                        $i++;
                    } else {
                        break;
                    }
                }

                /** Saving multiMapper model */
                $mapper->addData($post_data)
                    ->save();

                /** Going further to saving addons */
                foreach ($newAddons as $newAddonData) {
                    foreach ($newAddonData as $key => $value) {
                        if ($key != 'mapper_id' && !empty(trim($value))) {
                            // Save the addon if at least one field contains a value
                            /** @var Dyna_MultiMapper_Model_Addon $addon */
                            $addon = Mage::getModel("dyna_multimapper/addon")
                                ->setId($newAddonData['addon_id'])
                                ->addData($newAddonData)
                                ->setMapperId($mapper->getId()); // get mapper id for new entries
                            $addon->save();
                            $addon->setProcessContextsIds($addonProcessContexts[$addon->getId()]);
                            break;
                        }
                    }
                }

                $this->getAdminSession()->addSuccess(Mage::helper("adminhtml")->__("Mapper was successfully saved"));
                $this->getAdminSession()->setMapperData(false);

                if ($this->getRequest()->getParam("back")) {
                    $this->_redirect("*/*/edit", array("id" => $mapper->getId()));

                    return;
                }
                $this->_redirect("*/*/");

                return;
            } catch (Exception $e) {
                $this->getAdminSession()->addError($e->getMessage());
                $this->getAdminSession()->setMapperData($this->getRequest()->getPost());
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));

                return;
            }

        }
        $this->_redirect("*/*/");
    }

    public function editAction()
    {
        $this->_title($this->__("MultiMapper"));
        $this->_title($this->__("Mapper"));
        $this->_title($this->__("Edit Item"));

        $id = $this->getRequest()->getParam("id");
        /**
         * @var Dyna_MultiMapper_Model_Mapper $model
         */
        $model = Mage::getModel("multimapper/mapper")->load($id);
        if ($model->getId()) {
            Mage::register("mapper_data", $model);
            $this->loadLayout();
            $this->_setActiveMenu("multimapper/mapper");
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Mapper Manager"), Mage::helper("adminhtml")->__("Mapper Manager"));
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Mapper Description"), Mage::helper("adminhtml")->__("Mapper Description"));
            $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock("multimapper/adminhtml_mapper_edit"))->_addLeft($this->getLayout()->createBlock("multimapper/adminhtml_mapper_edit_tabs"));
            $this->renderLayout();
        } else {
            Mage::getSingleton("adminhtml/session")->addError(Mage::helper("multimapper")->__("Item does not exist."));
            $this->_redirect("*/*/");
        }
    }

    /**
     * @return Mage_Adminhtml_Model_Session
     */
    public function getAdminSession()
    {
        return $this->adminSession ? $this->adminSession : Mage::getSingleton("adminhtml/session");
    }
}
