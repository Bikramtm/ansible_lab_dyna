<?php

/** @var Mage_Catalog_Model_Resource_Setup $this */
$this->startSetup();

/** Add columns to mapper */
$this->getConnection()->addColumn($this->getTable("multimapper/mapper"), "service_expression", [
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'after' => 'promo_id',
    'comment' => 'Service expression',
]);

$this->getConnection()->addColumn($this->getTable("multimapper/mapper"), "priority", [
    'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
    'after' => 'service_expression',
    'comment' => 'MultiMapper priority',
]);

$this->getConnection()->addColumn($this->getTable("multimapper/mapper"), "stop_execution", [
    'type' => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
    'after' => 'priority',
    'comment' => 'Stop execution',
]);

/** Rename column priceplan to commercial */
$this->getConnection()->changeColumn($this->getTable("multimapper/mapper"), "priceplan_id", "commercial_id", [
            'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
            'nullable' => false,
]);
$this->getConnection()->changeColumn($this->getTable("multimapper/mapper"), "priceplan_sku", "commercial_sku", [
            'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
            'nullable' => false,
            'length' => 64,
]);
/** Rename column priceplan to commercial */
$this->getConnection()->dropIndex($this->getTable("multimapper/mapper"), "IDX_DYNA_MULTIMAPPER_PRICEPLAN_ID_PROMO_ID");
$this->getConnection()->dropIndex($this->getTable("multimapper/mapper"), "IDX_DYNA_MULTIMAPPER_PRICEPLAN_ID");
$this->getConnection()->addIndex(
    $this->getTable("multimapper/mapper"),
    "IDX_DYNA_MULTIMAPPER_COMMERCIAL_ID_PROMO_ID",
    ["commercial_id", "promo_id"]);
$this->getConnection()->addIndex(
    $this->getTable("multimapper/mapper"),
    "IDX_DYNA_MULTIMAPPER_COMMERCIAL_ID",
    "commercial_id");

/** Remove columns from mapper */
for ($i=1; $i<=10; $i++) {
    $this->getConnection()->dropColumn($this->getTable("multimapper/mapper"), "addon".  $i . "_desc");
    $this->getConnection()->dropColumn($this->getTable("multimapper/mapper"), "addon".  $i . "_nature");
    $this->getConnection()->dropColumn($this->getTable("multimapper/mapper"), "addon".  $i . "_soc");
    $this->getConnection()->dropColumn($this->getTable("multimapper/mapper"), "addon".  $i . "_name");
}

/** Create addons table */
$addonsTable = new Varien_Db_Ddl_Table();
$addonsTable->setName("dyna_multi_mapper_addons");

$addonsTable->addColumn("entity_id", Varien_Db_Ddl_Table::TYPE_BIGINT, null, [
        'unsigned' => true,
        'primary' => true,
        'auto_increment' => true,
        'comment' => "Primary key",
    ])
    ->addColumn("mapper_id", Varien_Db_Ddl_Table::TYPE_INTEGER, null, [
        'unsigned' => true,
        'comment' => "Map to dyna_multi_mapper",
    ])
    ->addColumn("addon_name", Varien_Db_Ddl_Table::TYPE_TEXT, 255, [
        'comment' => "Addon name",
    ])
    ->addColumn("addon_desc", Varien_Db_Ddl_Table::TYPE_TEXT, 255, [
        'comment' => "Addon description",
    ])
    ->addColumn("addon_nature", Varien_Db_Ddl_Table::TYPE_TEXT, 45, [
        'comment' => "Addon nature",
    ])
    ->addColumn("addon_soc", Varien_Db_Ddl_Table::TYPE_TEXT, 45, [
        'comment' => "Addon soc",
    ])
    ->addIndex(
        $this->getConnection()->getIndexName("dyna_multi_mapper_addons", "addon_soc"),
        "addon_soc"
    )
    ->addForeignKey(
        $this->getConnection()->getForeignKeyName("dyna_multi_mapper_addons", "mapper_id", "dyna_multi_mapper", "entity_id"),
        "mapper_id",
        "dyna_multi_mapper",
        "entity_id",
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
    );

$this->getConnection()->createTable($addonsTable);

$this->endSetup();
