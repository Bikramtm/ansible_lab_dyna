<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

// Add indexes for Mapper Addons table

/** @var Mage_Catalog_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();
$connection = $installer->getConnection();
$tableName = $installer->getTable('dyna_multimapper/addon');

$connection->addIndex($tableName,
    $installer->getIdxName($tableName, array('technical_id')),
    array('technical_id')
);

$connection->addIndex($tableName,
    $installer->getIdxName($tableName, array('addon_name', 'addon_value', 'addon_additional_value')),
    array('addon_name', 'addon_value', 'addon_additional_value')
);

$installer->endSetup();
