<?php
// Add Direction column to Multimapper table to separate normal multimappers from reverse and forward multimappers

/** @var Mage_Catalog_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();
$connection = $installer->getConnection();
$tableName = $installer->getTable('multimapper/mapper');

$connection->addColumn($tableName, 'direction', array(
    'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
    'length'    => 4,
    'default'   => 'both',
    'nullable'  => FALSE,
    'comment'   => 'Direction used to differentiate Reverse Multimappers'
));
$connection->addIndex($tableName,
    $installer->getIdxName($tableName, array('direction')),
    array('direction')
);

$installer->endSetup();
