<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;
$installer->startSetup();

$dropColumns = array(
    'article_number',
    'article_number_additional',
    'article_name',
    'article_name_aditional',
    'hardware_service_code',
    'hardware_service_code_additional',
    'hardware_type',
    'hardware_type_additional',
    'subvention_code',
    'subvention_code_additional',
    'shipping_costs',
    'shipping_costs_additional',
    'id',
    'id_additional',
    'ref_function_id',
    'ref_function_id_additional',
    'order_reason',
    'order_reason_additional',
);

foreach($dropColumns as $dropColumn){
    $this->getConnection()->dropColumn($this->getTable('multimapper/mapper'), $dropColumn);
}


/** Create addons table */
$addonsTable = new Varien_Db_Ddl_Table();
$addonsTable->setName("dyna_multi_mapper_addons");

$addonsTable->addColumn("entity_id", Varien_Db_Ddl_Table::TYPE_BIGINT, null, [
    'unsigned' => true,
    'primary' => true,
    'nullable' => false,
    'auto_increment' => true,
    'comment' => "Primary key",
])
    ->addColumn("mapper_id", Varien_Db_Ddl_Table::TYPE_INTEGER, null, [
        'unsigned' => true,
        'comment' => "Map to dyna_multi_mapper",
    ])
    ->addColumn("addon_name", Varien_Db_Ddl_Table::TYPE_TEXT, 255, [
        'comment' => "Addon name",
    ])
    ->addColumn("addon_additional_value", Varien_Db_Ddl_Table::TYPE_TEXT, 255, [
        'comment' => "Addon additional value",
    ])
    ->addColumn("addon_value", Varien_Db_Ddl_Table::TYPE_TEXT, 45, [
        'comment' => "Addon value",
    ])
    ->addColumn("promo_id", Varien_Db_Ddl_Table::TYPE_TEXT, 255, [
        'comment' => "Promo_id",
    ])
    ->addColumn("technical_id", Varien_Db_Ddl_Table::TYPE_TEXT, 255, [
        'comment' => "technical id",
    ])
    ->addColumn("nature_code", Varien_Db_Ddl_Table::TYPE_TEXT, 255, [
        'comment' => "nature code",
    ])
    ->addForeignKey(
        $this->getConnection()->getForeignKeyName("dyna_multi_mapper_addons", "mapper_id", "dyna_multi_mapper", "entity_id"),
        "mapper_id",
        "dyna_multi_mapper",
        "entity_id",
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
    );

$this->getConnection()->createTable($addonsTable);

$this->endSetup();


$installer->endSetup();
