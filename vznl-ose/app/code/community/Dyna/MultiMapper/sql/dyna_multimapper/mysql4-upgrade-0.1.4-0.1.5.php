<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 * OMNVFDE-2479 Add backend property for mobile in Multimapper
 */
$installer = $this;
$installer->startSetup();
//addons table
$addonsTable = 'dyna_multi_mapper_addons';
//add new property 'backend'
$installer->getConnection()->addColumn($addonsTable,'backend', array(
    'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
    'nullable'  => TRUE,
    'length'    => 8,
    'comment'   => 'Backend type for import (SAP|KIAS)'
));
//END Setup connection
$installer->endSetup();
