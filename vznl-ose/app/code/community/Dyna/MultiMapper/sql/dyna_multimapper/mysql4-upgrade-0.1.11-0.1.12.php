<?php
/** @var Mage_Catalog_Model_Resource_Setup $installer */
$installer = $this;

$installer->startSetup();

$tableName = 'dyna_multi_mapper_index_process_context';
/** @var Varien_Db_Adapter_Interface $connection */
$connection = $installer->getConnection();

if ($connection->isTableExists($tableName)) {
    if ($connection->tableColumnExists($tableName, 'mapper_id')){
        $connection->dropForeignKey($tableName, $installer->getFkName($tableName, 'mapper_id', 'dyna_multi_mapper', 'entity_id'));
        $connection->dropIndex($tableName, $this->getIdxName($tableName, ['mapper_id', 'process_context_id'], Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE));
        $connection->changeColumn($tableName, 'mapper_id', 'addon_id', [
                'type' => Varien_Db_Ddl_Table::TYPE_BIGINT,
                'length' => 11,
                'unsigned' => true,
                'nullable'  => false,
                'comment' => "Map to dyna_multi_mapper_addons",
            ]);
    }
    $connection->addForeignKey(
            $installer->getFkName($tableName, 'addon_id', 'dyna_multi_mapper_addons', 'entity_id'), $tableName,'addon_id', 'dyna_multi_mapper_addons', 'entity_id',
            Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
        );
    $connection->addIndex($tableName, $this->getIdxName($tableName, ['addon_id', 'process_context_id'], Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE),
            ['addon_id', 'process_context_id'], Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE)
    ;
}


$installer->endSetup();

