<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;
$installer->startSetup();

$newColumns = array(
    'sku',
    'article_number',
    'article_number_additional',
    'article_name',
    'article_name_aditional',
    'hardware_service_code',
    'hardware_service_code_additional',
    'hardware_type',
    'hardware_type_additional',
    'subvention_code',
    'subvention_code_additional',
    'shipping_costs',
    'shipping_costs_additional',
    'id',
    'id_additional',
    'ref_function_id',
    'ref_function_id_additional',
    'order_reason',
    'order_reason_additional',
    'xpath_outgoing',
    'xpath_incomming'
);

$dropColumns = array(
    'commercial_id',
    'promo_id',
    'commercial_sku',
    'promo_sku'
);
foreach($dropColumns as $dropColumn){
    $this->getConnection()->dropColumn($this->getTable('multimapper/mapper'), $dropColumn);
}
foreach($newColumns as $newAddonColumn){
    $this->getConnection()->addColumn($this->getTable('multimapper/mapper'), $newAddonColumn, 'varchar(255)');
}

$installer->run("DROP TABLE IF EXISTS dyna_multi_mapper_addons");

$installer->endSetup();
