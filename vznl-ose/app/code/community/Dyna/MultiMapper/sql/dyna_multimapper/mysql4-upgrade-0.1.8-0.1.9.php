<?php

/** @var Mage_Catalog_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();
$connection = $installer->getConnection();
$tableName = $installer->getTable('multimapper/mapper');

$connection->addColumn($tableName, 'defaulted', array(
    'type'      => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
    'default'   => false,
    'nullable'  => false,
    'comment'   => 'Flag which indicates if a product has been manually or automatically added by an agent'
));

$installer->endSetup();
