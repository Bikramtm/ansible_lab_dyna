<?php

/**
 * Class Dyna_MultiMapper_Model_AddonProcessContext
 */
class Dyna_MultiMapper_Model_addonProcessContext extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init("dyna_multimapper/addonProcessContext");
    }
}
