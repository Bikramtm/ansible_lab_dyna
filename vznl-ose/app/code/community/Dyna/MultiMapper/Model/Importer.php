<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_MultiMapper_Model_Importer
 */
class Dyna_MultiMapper_Model_Importer extends Omnius_Import_Model_Import_ImportAbstract
{
    const ALL_PROCESS_CONTEXTS = "*";

    private $defaultProcessContexts;
    private $currentRuleProcessContext;
    /** @var Dyna_Catalog_Helper_ProcessContext $processContextHelper */
    private $processContextHelper;

    protected $header;
    /**
     * @var string
     */
    protected $_csvDelimiter = ";";

    protected $mainFields = [
        'sku',
        'service_expression',
        'priority',
        'stop_execution',
        'comment',
        'xpath_outgoing',
        'xpath_incomming',
        'component_type',
        'direction',
        'process_context',
    ];
    /** @var string $_logFileName */
    protected $_logFileName = "multimapper_import";
    /** @var string $_logFileExtension */
    protected $_logFileExtension = "log";
    /** @var Dyna_Import_Helper_Data $_helper */
    public $_helper;

    /**
     * Dyna_MultiMapper_Model_Importer constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->_helper->setImportLogFile($this->_logFileName . '.' . $this->_logFileExtension);
    }

    /**
     * @param $path
     * @return bool
     * @throws Exception
     */
    public function import($path)
    {
        $this->_helper->setPathToImportFile($path);
        $file = fopen($path, 'r');
        $lc = 0;
        $success = true;

        // Variable for logging purposes
        $successCount = 0;
        $this->processContextHelper = Mage::helper("dyna_catalog/processContext");
        $this->setDefaultProcessContexts();

        while (($line = fgetcsv($file, null, $this->_csvDelimiter)) !== false) {
            $lc++;
            if ($lc == 1) {
                $line = array_map('strtolower', $line);
                //check if match our template
                $checkHeader = $this->getImportHelper()->checkHeader($line, 'multimapper');
                //log messages
                $messages = $this->getImportHelper()->getMessages();
                $attachments = [];
                foreach ($messages as $key => $msg){
                    //write file log
                    $this->_log($msg);
                    if ($key !== 'success' && substr($this->_pathToFile, 0, 9) != '/vagrant/') {
                        $attachments[] = ['key' => $key, 'text' => $msg];
                    }
                }
                if (count($attachments)) {
                    $msg = 'While processing file ' . $this->_pathToFile . ' we encountered the following:';
                    $this->_helper->postToSlack($msg, $attachments);
                }
                //check if the headers are ok
                if (!$checkHeader) {
                    return false;
                }
                $this->prepareColumns($line);
                continue;
            }
            //used for logging purposes
            $this->_totalFileRows++;
            $this->currentRuleProcessContext = null;

            /** Parse addon like values */
            if (!$csvLineData = array_combine($this->header, $line)) {
                $this->_logError("[ERROR] Inconsistency in import file as the first row (columns headers) columns count differs from the current line (" . $lc . ") columns count. Skipping row.");
                continue;
            }

            $csvLineData = $this->parseMultiMapperData($csvLineData);
            $sku = trim($csvLineData['sku'] ?: $csvLineData['SKU'] ?: "");
            $direction = trim($csvLineData['direction'] ? $csvLineData['direction'] : "both");
            $product = Mage::getModel("catalog/product")->getIdBySku($sku);
            if (!$product) {
                $success = false;
                $this->_logError('[ERROR] Skipping row ' . print_r($line, true) . ' because the product could not be found using the SKU "' . $sku . '"');
                continue;
            }

            /** @var Dyna_MultiMapper_Model_Mapper $mapperModel */
            $mapperModel = Mage::getModel('multimapper/mapper');
            $csvLineData['service_expression'] = $csvLineData['service_expression'] ?? null;
            $mapperModels = $mapperModel->getMappersBySku($sku, trim($csvLineData['service_expression']), true);
            if ($mapperModels->count()) {
                foreach ($mapperModels as $dbMapperModel) {
                    // Make sure the direction matches for the existing DB item
                    if ($dbMapperModel->getDirection() == $direction) {
                        $mapperModel = $dbMapperModel;
                        break;
                    }
                }
            }
            // determine the process context. if is a string it must be a valid string that can be mapped to an id, if is an id it must be a valid one, else -> error)
            // @todo remove hardcode * for process_context if not provided when VFDED1W3S-2478 is done
            if (!isset($csvLineData["process_context"])) {
                $csvLineData["process_context"] = "*";
            }
            if (!$this->setProcessContextId($csvLineData["process_context"], $line)) {
                $this->_logError('[ERROR] Skipping row no ' . $lc);
                continue;
            }

            foreach ($this->mainFields as $mainField) {
                if (isset($csvLineData[$mainField])) {
                    $mapperModel->setData($mainField, trim($csvLineData[$mainField]));
                }
            }
            $mapperModel->setStack($this->stack);
            $mapperModel->save();

            /** @var Dyna_MultiMapper_Model_Addon $addon */
            $addon = Mage::getModel("dyna_multimapper/addon");
            $addon->setStack($this->stack);
            $addon->setData('mapper_id', $mapperModel->getId());
            $addon->setData('backend', isset($csvLineData['backend']) ? $csvLineData['backend'] : (isset($csvLineData['default_backend']) ? $csvLineData['default_backend'] : null));

            if (isset($csvLineData['technical_id'])) {

                foreach ($this->fields as $field) {
                    if (isset($this->mapping[$field])) {
                        // mapping for fields
                        $field = $this->mapping[$field];
                    }
                    $addon->setData($field, isset($csvLineData[$field]) ? trim($csvLineData[$field]) : null);
                }

                $addon->save();

                if($this->currentRuleProcessContext) {
                    $addon->setProcessContextsIds($this->currentRuleProcessContext);
                }
            } else {
                $fields = array_diff($this->header, $this->mainFields, $this->mapping);

                foreach ($fields as $field) {
                    if (strpos($field, 'Additional') !== false) {
                        if (!in_array(str_replace('Additional', '', $field), $fields)) {
                            $this->_logError(sprintf('Did not find field %s for %s', str_replace('Additional', '', $field), $field));
                            continue;
                        }
                        $newAddon = clone $addon;
                        $newAddon->setData('addon_name', trim(str_replace('Additional', '', $field)))
                            ->setData('addon_additional_value', trim($csvLineData[$field]))
                            ->setData('addon_value', trim($csvLineData[str_replace('Additional', '', $field)]))
                            ->save();
                        if($this->currentRuleProcessContext) {
                            $newAddon->setProcessContextsIds($this->currentRuleProcessContext);
                        }
                    } else {
                        continue;
                    }
                }
            }

            $this->_log(sprintf('Successfully imported CSV line %d [product `%s` ] multimapper entry.', $lc, $sku), true);
            $successCount++;
        }
        //used for logging purposes
        $this->_skippedFileRows = $this->_totalFileRows - $successCount;

        return $success;
    }

    /**
     * @return Mage_Core_Helper_Abstract|null
     */
    protected function getImportHelper()
    {
        /** @var Dyna_Import_Helper_Data _helper */
        return $this->_helper == null ? Mage::helper("dyna_import/data") : $this->_helper;
    }

    /**
     * @param $firstRow
     * @return Dyna_MultiMapper_Model_Importer
     */
    protected function prepareColumns($firstRow) : self
    {
        $firstRow = array_filter($firstRow, [$this, "cleanEmptyValues"]);
        /** @var Dyna_Import_Helper_Data mapping */
        $this->mapping = $this->getImportHelper()->getMapping('multimapper', ['mapping']);
        $this->fields = $this->getImportHelper()->getMapping('multimapper', ['required', 'optional']);
        foreach ($firstRow as $entry) {
            /** Checking whether or not this field is mapped to another one */
            if (array_key_exists($entry, $this->mapping)) {
                $this->header[] = $this->mapping[$entry];
            } else {
                /** Not a mapped field, then save it as it is;  */
                $this->header[] = $entry;
            }
        }

        return $this;
    }

    /**
     * @param $data
     * @return mixed
     */
    protected function parseMultiMapperData($data)
    {
        /** stop_execution */
        if (!empty($data['stop_execution'])) {
            if (strtolower($data['stop_execution']) == "no") {
                $data['stop_execution'] = 0;
            } else {
                $data['stop_execution'] = 1;
            }
        }

        /** direction - OMNVFDE-2582
         * in = Technical coming in = 360 View
         * out = Technical going out = Submitorder
         * both = all of the above
         */
        if (empty($data['direction']) || !in_array($data['direction'], array('in', 'out', 'both'))) {
            $data['direction'] = 'both';
        }

        return $data;
    }

    /**
     * Return true or false if a string is empty
     * @param $value string
     * @return string
     */
    protected function cleanEmptyValues($value) : string
    {
        return str_replace("-", "", trim($value));
    }

    /**
     * Set the default process contexts [id->code]
     */
    private function setDefaultProcessContexts()
    {
        if (!$this->defaultProcessContexts) {
            $this->defaultProcessContexts = $this->processContextHelper->getProcessContextsByCodes();
        }
    }

    /**
     * Set the process context id
     * if it is a string it must be mapped to one of the possible strings for process context (it string must be contained in one of the default process context) ->
     * and from that string we will used the id
     * if is an int it must be mapped to one of the possible ids for process context
     * else -> skip row, error
     * @param string $processContextsForRule
     * @param array $line
     * @return bool
     */
    private function setProcessContextId($processContextsForRule, $line)
    {
        if (trim($processContextsForRule) == self::ALL_PROCESS_CONTEXTS) {
            $this->currentRuleProcessContext = $this->defaultProcessContexts;
            return true;
        }

        $processContextsForRuleArray = explode(',', strtoupper(trim($processContextsForRule)));
        foreach ($processContextsForRuleArray as $processRuleContextForRule) {
            if (isset($this->defaultProcessContexts[trim($processRuleContextForRule)])) {
                $this->currentRuleProcessContext[] = $this->defaultProcessContexts[trim($processRuleContextForRule)];
            } else {
                $this->_logError('[ERROR] the process_context ' . trim($processRuleContextForRule) . ' is not valid (rule line  ' . print_r($line, true) . '). skipping row');
                return false;
            }
        }

        return true;
    }

    /**
     * Log function
     *
     * @param string $msg
     * @param bool $verbose
     */
    public function _log($msg, $verbose = false)
    {
        if($verbose && !$this->_debug) {
            return;
        }
        $this->_helper->logMsg('[' . $this->_currentProductSku . '] ' . $msg, false);
    }

    /**
     * Log Error function
     *
     * @param string $msg
     */
    public function _logError($msg)
    {
        $this->_helper->logMsg('[' . $this->_currentProductSku . '] ' . $msg);
    }
}
