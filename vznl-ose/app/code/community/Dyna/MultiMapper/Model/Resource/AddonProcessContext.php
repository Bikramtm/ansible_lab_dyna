<?php

/**
 * Class Dyna_MultiMapper_Model_Resource_AddonProcessContext
 */
class Dyna_MultiMapper_Model_Resource_addonProcessContext extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {
        $this->_init("dyna_multimapper/addonProcessContext", 'addon_id');
    }
}
