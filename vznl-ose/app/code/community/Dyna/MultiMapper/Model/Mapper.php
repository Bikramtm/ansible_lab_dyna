<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_MultiMapper_Model_Mapper
 * @method setStack($string) Set stack for current mapper
 */
class Dyna_MultiMapper_Model_Mapper extends Omnius_MultiMapper_Model_Mapper
{
    CONST DIRECTION_IN = 'in';
    CONST DIRECTION_OUT = 'out';
    CONST DIRECTION_BOTH = 'both';

    protected function _construct()
    {
        $this->_init("multimapper/mapper");
        $this->getSessionProcessContextId();
    }

    /** @var Dyna_Cache_Model_Cache */
    protected $_cache;
    private $sessionProcessContextId;

    /**
     * Get a list of records for a given article number
     * @param $soc
     * @return mixed
     */
    public function getSkusForSoc($soc)
    {
        /**
         * @var $addonModel Dyna_MultiMapper_Model_Addon
         */
        $addonModel = Mage::getModel("dyna_multimapper/addon");
        $cache_key = sprintf('multimapper_addons_for_soc_%s_%s', md5($soc), $this->sessionProcessContextId);
        if (!($addons = unserialize($this->getCache()->load($cache_key)))) {
            $addons = $addonModel->getAddonsByTechnicalId($soc);
            $this->getCache()->save(serialize($addons), $cache_key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
        }
        $mapperIds = [];
        foreach ($addons as $addon) {
            $mapperIds[] = $addon->getMapperId();
        }
        $mappers = $this->getMappersWithInDirection($mapperIds);
        $skus = [];
        foreach ($addons as $addon) {
            foreach ($mappers as $mapper) {
                if ($addon->getMapperId() == $mapper->getId()) {
                    $skus[] = trim($mapper->getSku());
                    break;
                }
            }
        }
        return $skus;
    }

    /**
     *  OMNVFDE-2582: Return only IN and BOTH multimappers to 360 View
     * The In direction mappers should not be filtered with the process context ID
     * since these are used at the panel display
     *
     * @param $mapperIds
     * @return mixed
     */
    public function getMappersWithInDirection($mapperIds)
    {
        if(!$mapperIds) {
            return null;
        }
        $cache_key = sprintf('multimappers_for_given_ids_%s', md5(implode(',', $mapperIds)));
        $cachedValues = $this->getCache()->load($cache_key);
        if (!$cachedValues) {
            // OMNVFDE-2582: Return only IN and BOTH multimappers to 360 View
            $mappersCollection = $this->getCollection();

            $mappersCollection->addFieldToFilter('entity_id', array('in' => $mapperIds))
                ->addFieldToFilter('direction', array('in' => array('in', 'both')))
                ->getItems();

            $this->getCache()->save(serialize($mappersCollection->exportToArray()), $cache_key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
        } else {
            $mappersCollection = $this->getCollection();
            $mappersCollection->setIsLoaded(true);

            $mappersArray = unserialize($cachedValues);
            $mappersCollection->importFromArray($mappersArray);
        }

        return $mappersCollection;
    }

    /**
     * @param $quoteSkus
     * @return mixed
     */
    public function getMappersBySkusWithOutDirection($quoteSkus)
    {
        $mappers = $this->getCollection();

        return $mappers->addFieldToFilter('direction', array('in' => ['out', 'both']))
            ->addFieldToFilter('sku', array('in' => $quoteSkus));
    }

    /**
     * Get entity id of first mapper from collection
     * @param $sku
     * @return string
     */
    public function getEntityIdBySku($sku)
    {
        $record = $this->getMappersBySku($sku)->getFirstItem();
        if ($record->getId()) {
            return $record->getData('entity_id');
        }
        return null;
    }

    /**
     * Get multimapper records by SKU and optionally by service expression
     * @param $sku
     * @param $service_expression
     * @param $import
     * @return mixed
     */
    public function getMappersBySku($sku, $service_expression = null, $import = false)
    {
        $collection = $this->getCollection();

        if (is_array($sku)) {
            $collection->addFieldToFilter('sku', array('in' => $sku));
        } else {
            $collection->addFieldToFilter('sku', $sku);
        }

        // OMNVFDE-2582: If we're not importing multimappers, Return only OUT and BOTH multimappers to Submit Order
        if (!$import) {
            $collection->addFieldToFilter('direction', array('in' => array('out', 'both')));
        }
        if ($service_expression) {
            $collection->addFieldToFilter('service_expression', $service_expression);
        } elseif ($import) {
            $collection->addFieldToFilter('service_expression', array('null' => true));
        }

        return $collection;
    }

    public function getMappersBySkus($skus)
    {
        $collection = $this->getCollection();

        return $collection->addFieldToFilter('sku', array('in' => $skus));
    }

    public function getSingleMappersBySku($sku)
    {
        return $this->getMappersBySku($sku)->getFirstItem();
    }

    /**
     * @return Dyna_Cache_Model_Cache
     */
    protected function getCache()
    {
        if (!$this->_cache) {
            $this->_cache = Mage::getSingleton('dyna_cache/cache');
        }

        return $this->_cache;
    }

    private function getSessionProcessContextId()
    {
        /** @var Dyna_Catalog_Helper_ProcessContext $processContextHelper */
        $processContextHelper = Mage::helper("dyna_catalog/processContext");
        $this->sessionProcessContextId = $processContextHelper->getProcessContextId();
    }
}
