<?php

/**
 * Class Dyna_MultiMapper_Model_Addon
 * @method setStack($string) Set stack for current mapper addon
 * @method string|null getBasketAction()
 */
class Dyna_MultiMapper_Model_Addon extends Mage_Core_Model_Abstract
{
    CONST ATTRIBUTE_ACTION_ADD = 'add';
    CONST ATTRIBUTE_ACTION_OVERWRITE = 'overwrite';
    CONST ATTRIBUTE_ACTION_TAKEOVER = 'takeover';

    CONST BASKET_ACTION_ADD = 'add';
    CONST BASKET_ACTION_CHANGE = 'change';
    CONST BASKET_ACTION_KEEP = 'keep';
    CONST BASKET_ACTION_REMOVE = 'remove';

    protected $_addonProcessContextTable;
    private $sessionProcessContextId = false;

    protected function _construct()
    {
        $this->_init("dyna_multimapper/addon");
        $this->_addonProcessContextTable = (string) Mage::getResourceSingleton('dyna_multimapper/addonProcessContext')->getMainTable();
        $this->getSessionProcessContextId();
    }

    /**
     * Get addon record by mapper_id
     * @param $mapperId
     * @return mixed
     */
    public function getAddonByMapperId($mapperId, $processContext = null)
    {
        $addons = $this->getCollection()
            ->addFieldToFilter('mapper_id', $mapperId);
        if ($processContext) {
            /** @var Dyna_Catalog_Model_ProcessContext $processContextHelper */
            $processContextHelper = Mage::getModel('dyna_catalog/processContext');
            $processContextId = $processContextHelper->getProcessContextIdByCode($processContext);
            $addons->getSelect()->join(array('dmmpc' => 'dyna_multi_mapper_index_process_context'),
                'dmmpc.addon_id = main_table.entity_id', array('dmmpc.process_context_id')
            );
            $addons->addFieldToFilter('dmmpc.process_context_id', $processContextId);
        }
        return $addons;
    }

    /**
     * @param $mapperId
     * @param null $processContext
     * @param null $direction
     * @return mixed
     */
    public function getAddonsWithDirection($mapperId, $processContext = null, $direction = null)
    {
        $addons = $this->getAddonByMapperId($mapperId, $processContext);
        $addons->addFieldToFilter('direction', $direction);

        return $addons;
    }

    /**
     * Get addon record by mapper_id
     * @param $mapperId
     * @return mixed
     */
    public function getSingleAddonByMapperId($mapperId)
    {
        return $this->getAddonByMapperId($mapperId)->getFirstItem();
    }

    /**
     * Get a specific addon filtered by a mapper_id and an addon_name
     * @param $mapperId
     * @param $name
     * @return mixed
     */
    public function getAddonByMapperAndName($mapperId, $name)
    {
        $addons = $this->getCollection()
            ->addFieldToFilter('mapper_id', $mapperId)
            ->addFieldToFilter('addon_name', $name);

        return $addons->getFirstItem();
    }

    /**
     * The result of this method is later used in the getMappersWithInDirection
     * The In direction mappers should not be filtered with the process context ID
     * since these are used at the panel display
     * @param $code
     * @param $value
     * @param $additional
     * @return mixed
     */
    public function getMapperIdsByFNData($code, $value, $additional)
    {
        $addons = $this->getCollection();
        return $addons->addFieldToSelect('mapper_id')
            ->addFieldToFilter('direction', ['in' => [Dyna_MultiMapper_Model_Mapper::DIRECTION_IN, Dyna_MultiMapper_Model_Mapper::DIRECTION_BOTH]])
            ->addFieldToFilter('addon_name', $code)
            ->addFieldToFilter('addon_value', $value)
            ->addFieldToFilter('addon_additional_value', $additional);
    }

    /**
     *
     */
    public function quoteSkuMappersHaveSAPAddons($quoteSkus, $processContext = null)
    {
        $mapperIds = [];

        /**
         * @var $mapperModel Dyna_MultiMapper_Model_Mapper
         */
        $mapperModel = Mage::getModel("dyna_multimapper/mapper");
        $mapperList = $mapperModel->getMappersBySkusWithOutDirection($quoteSkus);
        foreach ($mapperList as $mapper) {
            $mapperIds[] = $mapper->getId();
        }

        return $this->mappersHaveSAPAddons($mapperIds, $processContext);
    }

    /**
     * Get the all addons that have the same technical_id value
     * @param $soc
     * @return mixed
     */
    public function getAddonsByTechnicalId($soc)
    {
        $addons = $this->getCollection()
            ->addFieldToFilter('technical_id', $soc);
        return $addons->getItems();
    }

    /**
     * Get all addons that belong to a specific list of mapper ids
     * @param $ids
     * @return mixed
     */
    public function getAddonsByMapperIds($ids)
    {
        $addons = $this->getCollection()
            ->addFieldToFilter('mapper_id', array('in' => $ids));
        return $addons;
    }

    /**
     * @param array $mapperIds
     * @return bool
     */
    private function mappersHaveSAPAddons($mapperIds, $processContext = null)
    {
        $addons = $this->getCollection()
            ->addFieldToFilter('mapper_id', array('in' => $mapperIds))
            ->addFieldToFilter('backend', array('eq' => 'SAP'));
        if ($processContext) {
            /** @var Dyna_Catalog_Model_ProcessContext $processContextHelper */
            $processContextHelper = Mage::getModel('dyna_catalog/processContext');
            $processContextId = $processContextHelper->getProcessContextIdByCode($processContext);
            $addons->getSelect()->join(array('dmmpc' => 'dyna_multi_mapper_index_process_context'),
                'dmmpc.addon_id = main_table.entity_id', array('dmmpc.process_context_id')
            );
            $addons->addFieldToFilter('dmmpc.process_context_id', $processContextId);
        }

        $addonsCount = $addons->getSize();

        return ($addonsCount) ? true : false;
    }

    /**
     * Get the current process context of the active package
     */
    private function getSessionProcessContextId()
    {
        if ($this->sessionProcessContextId === false) {
            /** @var Dyna_Catalog_Helper_ProcessContext $processContextHelper */
            $processContextHelper = Mage::helper("dyna_catalog/processContext");
            $this->sessionProcessContextId = $processContextHelper->getProcessContextId();
        }
    }

    /**
     * @param array $contextIds
     * @return Dyna_MultiMapper_Model_Addon
     */
    public function setProcessContextsIds($contextIds)
    {
        $contextIds = $contextIds ?? [];
        $currentIds = $this->getProcessContextIds();
        $remove = array_diff($currentIds, $contextIds);
        $add = array_diff($contextIds, $currentIds);

        if ($remove || $add) {
            /** @var Zend_Db_Adapter_Abstract $connection */
            $connection = Mage::getModel('core/resource')->getConnection('core_write');

            if ($remove) {
                $remove = array_map(function($n) { return (int)$n; }, $remove);
                $removeIds = implode(',', $remove);
                $sql = "DELETE FROM $this->_addonProcessContextTable WHERE addon_id = :id AND process_context_id IN ($removeIds)";
                $connection->query($sql,
                    array(
                        'id' => $this->getId(),
                    )
                );
            }

            if ($add) {
                foreach ($add as $contextId) {
                    $sql = "INSERT IGNORE INTO $this->_addonProcessContextTable VALUES (:id, :process_context_id)";
                    $connection->query($sql,
                        array(
                            'id' => $this->getId(),
                            'process_context_id' => $contextId
                        )
                    );
                }
            }
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getProcessContextIds()
    {
        /** @var Zend_Db_Adapter_Abstract $connection */
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');

        $sql = "SELECT process_context_id FROM $this->_addonProcessContextTable WHERE addon_id = :id;";

        return array_column($connection->fetchAll($sql, ['id' => $this->getId()], Zend_Db::FETCH_NUM), 0);
    }
}
