<?php

/**
 * Class Dyna_MultiMapper_Block_Adminhtml_Mapper_Grid
 */
class Dyna_MultiMapper_Block_Adminhtml_Mapper_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId("mapperGrid");
        $this->setDefaultSort("entity_id");
        $this->setDefaultDir("DESC");
        $this->setSaveParametersInSession(true);
    }

    /**
     * @param $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl("*/*/edit", array("id" => $row->getId()));
    }

    /**
     * @param $collection
     * @param $column
     * @return $this
     */
    public function mapperGridCustomEntityFilter($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return $this;
        }
        if (!empty($value['from'])) {
            $this->getCollection()->getSelect()->where("main_table.entity_id >= ?", $value['from']);
        }
        if (!empty($value['to'])) {
            $this->getCollection()->getSelect()->where("main_table.entity_id <= ?", $value['to']);
        }
    }

    /**
     * @param $collection
     * @param $column
     * @return $this
     */
    public function mapperGridCustomServiceExpressionFilter($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return $this;
        }
        $this->getCollection()->getSelect()->where("main_table.service_expression like ?", "%" . $value . "%");
    }

    /**
     * @param $collection
     * @param $column
     * @return $this
     */
    public function mapperGridCustomDirectionFilter($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return $this;
        }
        $this->getCollection()->getSelect()->where("main_table.direction like ?", "%" . $value . "%");
    }

    /**
     * @throws Exception
     */
    protected function _prepareColumns()
    {
        if (!$this->_isExport) {
            $this->addColumn("entity_id", array(
                "header" => Mage::helper("multimapper")->__("ID"),
                "align" => "right",
                "width" => "50px",
                "type" => "number",
                "index" => "entity_id",
                'filter_condition_callback' => array($this, 'mapperGridCustomEntityFilter')
            ));
        }
        $this->addColumn("sku", array(
            "header" => Mage::helper("multimapper")->__("SKU"),
            "index" => "sku",
        ));
        $this->addColumn("service_expression", array(
            "header" => Mage::helper("multimapper")->__("Service expression"),
            "index" => "service_expression",
            'filter_condition_callback' => array($this, 'mapperGridCustomServiceExpressionFilter')
        ));
        $this->addColumn("direction", array(
            "header" => Mage::helper("multimapper")->__("Direction"),
            "index" => "direction",
            'filter_condition_callback' => array($this, 'mapperGridCustomDirectionFilter')
        ));
        $this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));

        Mage_Adminhtml_Block_Widget_Grid::_prepareColumns();
    }

    protected function _prepareCollection()
    {
        /** @var Omnius_MultiMapper_Model_Mysql4_Mapper_Collection $collection */
        $collection = Mage::getModel("multimapper/mapper")->getCollection();

        $collection->getSelect()
            ->joinLeft(
                array('dyna_multi_mapper_addons'=>'dyna_multi_mapper_addons'),
                'dyna_multi_mapper_addons.mapper_id = main_table.entity_id',
                array('addonID' => 'dyna_multi_mapper_addons.entity_id')
            )
            ->joinLeft(
                array('dyna_multi_mapper_index_process_context'=>'dyna_multi_mapper_index_process_context'),
                'dyna_multi_mapper_index_process_context.addon_id = dyna_multi_mapper_addons.entity_id',
                array('processContextAddonID' => 'dyna_multi_mapper_index_process_context.addon_id','processContextID' => 'dyna_multi_mapper_index_process_context.process_context_id')
            )
            ->joinLeft(
                array('process_context'=>'process_context'),
                'process_context.entity_id = dyna_multi_mapper_index_process_context.process_context_id',
                array('name' => 'process_context.name')
            )
            ->group('main_table.entity_id');

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('entity_ids');
        $this->getMassactionBlock()->setUseSelectAll(true);
        $this->getMassactionBlock()->addItem('remove_mapper', array(
            'label' => Mage::helper('multimapper')->__('Remove Mapper'),
            'url' => $this->getUrl('*/adminhtml_mapper/massRemove'),
            'confirm' => Mage::helper('multimapper')->__('Are you sure?')
        ));
        return $this;
    }
}
