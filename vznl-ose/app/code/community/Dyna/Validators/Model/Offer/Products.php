<?php

/**
 * Class Dyna_Validators_Model_Offer_Products
 */
class Dyna_Validators_Model_Offer_Products implements Dyna_Validators_Model_Offer_Interface
{
    protected static $message = "Products no longer available.";

    /**
     * Checks whether the products that were saved in the offer
     * are still available in the catalogue
     *
     * @param Dyna_Checkout_Model_Sales_Quote $quote
     * @return Dyna_Validators_Model_Offer_ValidationResult
     */
    public function validate(Dyna_Checkout_Model_Sales_Quote $quote): Dyna_Validators_Model_Offer_ValidationResult
    {
        /** @var Dyna_Catalog_Model_Product $productModel */
        $productModel = Mage::getModel('catalog/product');
        $isValid = true;

        // manually load quote items, cannot use $quote->getAllItems here because it loads products by id
        $itemsQuery = "SELECT `sku`, `product_id` FROM `sales_flat_quote_item` WHERE `quote_id` = :id OR `alternate_quote_id` = :id";
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $items = $connection->fetchAll($itemsQuery, array(':id' => $quote->getId()));

        foreach ($items as $item) {
            $product = $productModel->loadByAttribute('sku', $item['sku']);

            if ($product === false || $product->getId() !== (int)$item['product_id']) {
                $isValid = false;
                break;
            }
        }

        /** @var Dyna_Validators_Model_Offer_ValidationResult $validationResult */
        $validationResult = Mage::getModel('dyna_validators/offer_validationResult');

        $validationResult
            ->setIsValid($isValid)
            ->setShowMessage(true)
            ->setMessage(self::$message);

        return $validationResult;
    }
}