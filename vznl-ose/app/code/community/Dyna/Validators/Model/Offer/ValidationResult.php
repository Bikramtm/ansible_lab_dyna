<?php

/**
 * Class Dyna_Validators_Model_Offer_ValidationResult
 */
class Dyna_Validators_Model_Offer_ValidationResult
{
    /** @var  bool */
    protected $isValid;
    /** @var  string */
    protected $message = '';

    protected $showMessage = false;

    /**
     *
     * @return bool
     */
    public function getIsValid()
    {
        return $this->isValid;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return $this
     */
    public function setMessage(string $message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @param bool $isValid
     * @return $this
     */
    public function setIsValid(bool $isValid)
    {
        $this->isValid = $isValid;

        return $this;
    }

    /**
     * @param bool $showMessage
     * @return $this
     */
    public function setShowMessage(bool $showMessage)
    {
        $this->showMessage = $showMessage;

        return $this;
    }

    /**
     * @return bool
     */
    public function getShowMessage()
    {
        return $this->showMessage;
    }
}