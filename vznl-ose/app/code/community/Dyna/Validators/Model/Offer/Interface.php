<?php

/**
 * Interface Dyna_Validators_Model_Offer_Interface
 */
interface Dyna_Validators_Model_Offer_Interface
{
    public function validate(Dyna_Checkout_Model_Sales_Quote $quote) : Dyna_Validators_Model_Offer_ValidationResult;
}