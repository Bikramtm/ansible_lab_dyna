<?php

/**
 * Class Dyna_Validators_Helper_Data
 */
class Dyna_Validators_Helper_Data extends Omnius_Validators_Helper_Data
{
    //todo refactor to use the omnius max age with a rule
    const MAX_AGE_FOR_YOUNG_DISCOUNT = 25;
    const MAX_LENGTH_FOR_PACKAGE_NOTES = 150;
    /**
     * Validate a date is entered in the format dd.mm.yyyy
     *
     * @param $date
     * @return bool
     */
    public function validateIsDate($date)
    {
        if (empty($date)) {
            return false;
        }

        $dateData = explode('.', $date);
        if (count($dateData) !== 3) {
            return false;
        }

        return preg_match('/^[0-9]{2}.[0-9]{2}.[0-9]{4}$/', $date) && checkdate($dateData[1], $dateData[0], $dateData[2]);
    }

    //todo refactor to use the omnius max age
    public function validateMaxAge25($date)
    {
        $valid = true;
        $rule = self::MAX_AGE_FOR_YOUNG_DISCOUNT;
        $year = (int) date('Y');
        $dob_year = (int) date('Y', strtotime($date));
        $cond1 = $year - $rule > $dob_year;
        $cond2 = ($year - $rule == $dob_year) && (strtotime(date('d-m-', strtotime($date)) . $year) < strtotime(date('d-m-Y')));
        if ($cond1 || $cond2) {
            $valid = false;
        }

        return $valid;
    }

    /**
     * Validates package notes length
     *
     * @param $notes
     * @return bool
     */
    public function validatePackageNotesLength($notes)
    {
        $valid = true;

        if (strlen($notes) > self::MAX_LENGTH_FOR_PACKAGE_NOTES) {
            $valid = false;
        }

        return $valid;
    }

    /**
     * Validate smart card serial number
     *
     * @param $serial
     * @return bool
     */
    public function validateSmartCardSerial($serial)
    {
        return preg_match('/^[0-9]{12}$/', $serial);
    }

    /**
     * Validate receiver serial number
     *
     * @param $serial
     * @return bool
     */
    public function validateReceiverSerial($serial)
    {
        return preg_match('/^[0-9]{14}$/', $serial);
    }

    public function validateIDNumber($data)
    {
        /** Temporary disabled until clarified */
        return true;
//        if (strlen($data) != 10) {
//            return false;
//        }
//
//        $values = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
//
//        $sum = 0;
//        $sum += strpos($values, $data[0]) * 7;
//        $sum += strpos($values, $data[1]) * 3;
//        $sum += strpos($values, $data[2]) * 1;
//
//        $sum += strpos($values, $data[3]) * 7;
//        $sum += strpos($values, $data[4]) * 3;
//        $sum += strpos($values, $data[5]) * 1;
//
//        $sum += strpos($values, $data[6]) * 7;
//        $sum += strpos($values, $data[7]) * 3;
//        $sum += strpos($values, $data[8]) * 1;
//
//        if (($sum % 10) == $data[9]) {
//            return true;
//        }
//        return false;
    }

    public function validateCustomerPassword($data)
    {
        return preg_match('/^[a-zA-Z0-9ÄÖÜäöüß_]{2,20}$/', $data);
    }

    public function validatePhoneNumberPrefix($data)
    {
        return preg_match('/^0[1-9][0-9]{1,4}$/', $data);
    }

    public function validatePhoneNumber($data)
    {
        return preg_match('/^[1-9][0-9]{1,8}$/', $data);
    }

    public function validatePercentage($data)
    {
        return ($data >= 0) && ($data <= 100);
    }

    public function validateIs18YearsOld($data)
    {
        $rule = 18;

        $valid = true;
        $year = (int) date('Y');
        $dob_year = (int) date('Y', strtotime($data));
        $cond1 = $year - $rule < $dob_year;
        $cond2 = ($year - $rule == $dob_year) && (strtotime(date('d-m-', strtotime($data)) . $year) > strtotime(date('d-m-Y')));
        if ($cond1 || $cond2) {
            $valid = false;
        }

        return $valid;
    }

    public function validateIs7YearsOld($data)
    {
        $rule = 7;

        $valid = true;
        $year = (int) date('Y');
        $dob_year = (int) date('Y', strtotime($data));
        $cond1 = $year - $rule < $dob_year;
        $cond2 = ($year - $rule == $dob_year) && (strtotime(date('d-m-', strtotime($data)) . $year) > strtotime(date('d-m-Y')));
        if ($cond1 || $cond2) {
            $valid = false;
        }

        return $valid;
    }

    /**
     * Checks whether the expiration date (MM.YYYY) is greater or equal
     * than the current date
     *
     * @param $date
     * @return bool
     */
    public function validateExpirationDate($date)
    {
        if (empty($date)) {
            return false;
        }

        // check whether the date is in MM.YYYY format
        if (!preg_match("/^\d{2}\.\d{4}$/", $date)) {
            return false;
        }

        $dateParts = explode(".", $date);
        $month = $dateParts[0];
        $year = $dateParts[1];

        if ($month < 1 || $month > 12) {
            return false;
        }

        $currentDateTime = new DateTime();
        $currentYear = $currentDateTime->format('Y');
        $currentMonth = $currentDateTime->format('m');

        if ($year > $currentYear) {
            return true;
        } elseif ($year == $currentYear && $month >= $currentMonth) {
            return true;
        }

        return false;
    }

    public function validateAgeBetween10And18($data)
    {
        $currentAge = DateTime::createFromFormat('d.m.Y', $data);
        if (!$currentAge) {
            return false;
        }

        $currentAge = $currentAge->diff(new DateTime('now'))->y;

        if ($currentAge < Dyna_Catalog_Model_Type::getYouBiageMinAge() || $currentAge >= Dyna_Catalog_Model_Type::getYouBiageMaxAge()) {
            return false;
        }

        return true;
    }

    public function validateAgeBetween18And28($data)
    {
        $currentAge = DateTime::createFromFormat('d.m.Y', $data);
        if (!$currentAge) {
            return false;
        }

        $currentAge = $currentAge->diff(new DateTime('now'))->y;

        if ($currentAge < Dyna_Catalog_Model_Type::getMobileYoungMinAge() || $currentAge > Dyna_Catalog_Model_Type::getMobileYoungMaxAge()) {
            return false;
        }

        return true;
    }
}
