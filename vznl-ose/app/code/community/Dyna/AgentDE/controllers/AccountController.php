<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class AccountController
 */
require_once Mage::getModuleDir('controllers', 'Dyna_Agent') . DS . 'AccountController.php';

class Dyna_AgentDE_AccountController extends Dyna_Agent_AccountController
{
    public function callCreateWorkItemAction()
    {
        if ($params = $this->getRequest()->getPost()) {
            /** @var Dyna_Configurator_Model_Client_CreateManualWorkItemClient $createManualWorkItemClient */
            $createManualWorkItemClient = Mage::getModel('dyna_configurator/client_createManualWorkItemClient');
            try {
                $workItemRequest = $createManualWorkItemClient->executeCreManWorkItemRequest($params);

                if (!$workItemRequest) {
                    $this->getResponse()
                        ->setHeader('Content-Type', 'application/json')
                        ->setBody(Mage::helper('core')->jsonEncode([
                            'error' => true,
                            'data' => $workItemRequest
                        ]));
                } else {
                    $this->getResponse()
                        ->setHeader('Content-Type', 'application/json')
                        ->setBody(Mage::helper('core')->jsonEncode([
                            'error' => false,
                            'data' => $workItemRequest
                        ]));
                }
                return;
            } catch (\Exception $e) {
                $this->getResponse()
                    ->setHeader('Content-Type', 'application/json')
                    ->setBody(Mage::helper('core')->jsonEncode([
                        'error' => true,
                        'message' => $e->getMessage()
                    ]));
            }
        }
    }

    /**
     * Define target URL and redirect customer after logging in
     */
    protected function _loginPostRedirect()
    {
        $redirectUrl = Mage::getUrl();
        // if require agent but not set
        if (!$this->_getSession()->getAgent() && Mage::app()->getStore()->getShowAgent()) {
            $redirectUrl = Mage::getUrl('agent/account/agent');
        }
        // if require dealer but not set
        if ($this->_getSession()->getAgent() && Mage::app()->getStore()->getShowStore()) {
            $redirectUrl = Mage::getUrl('agent/account/dealer');
        }

        // else either agent/dealer is set or not required so we're good
        $coreSession = Mage::getSingleton('core/session');
        $transactionId = $coreSession->getData('transactionId');

        if ($transactionId) {
            $coreSession->unsetData('transactionId');
            $uctUrl = Mage::getUrl() . "uct/?transactionId=" . $transactionId;
            $redirectUrl = $uctUrl;
        }

        $this->_redirectUrl($redirectUrl);
        return;
    }


    /**
     * Agent logout action
     */
    public function logoutAction()
    {
        Mage::getModel('agent/agent')->logout();

        if ($this->isAjax()) {
            $this->getResponse()->setHeader('Content-Type', 'application/json');
            $this->getResponse()->setBody(json_encode(array('success' => true)));
            $this->getResponse()->setHttpResponseCode(200);
        } else {
            $this->_redirectUrl(Mage::getUrl('agent/account/login'));
        }
    }

    public function switchAgentRoleAction()
    {
        $error = false;
        $message = null;
        if ($this->getRequest()->isPost()) {
            /** @var Dyna_Customer_Model_Session $session */
            $session = $this->_getSession();
            $roleId = (int)$this->getRequest()->getParam('agentRoleId');
            /** @var Dyna_AgentDE_Model_Agent $agent */
            $agent = Mage::getSingleton('customer/session')->getAgent();
            if ($role = $agent->getAdditionalRoles($roleId)) {
                $session->setData($agent::AGENT_IMPERSONATES_ROLE_KEY, $role->getRoleId());
                $agentReload = Mage::getModel('agent/agent')->load($agent->getId());
                $session->setAgent($agentReload);
                // create new quote
                $actualQuote = Mage::getSingleton('checkout/cart')->getQuote();
                Mage::helper('dyna_checkout')->createNewQuote($actualQuote);
            } else {
                $error = true;
                $message = $this->__("You are not allowed to use this role.");
            }
        } else {
            $error = true;
            $message = $this->__("Request for changing business function needs to be post.");
        }

        return $this->getResponse()
            ->setHeader('Content-Type', 'application/json')
            ->setBody(Mage::helper('core')->jsonEncode([
                'error' => $error,
                'message' => $message,
            ]));
    }

    public function leaveAgentRoleAction()
    {
        $error = false;
        $message = null;
        if ($this->getRequest()->isPost()) {
            /** @var Dyna_Customer_Model_Session $session */
            $session = $this->_getSession();
            /** @var Dyna_AgentDE_Model_Agent $agent */
            $agent = Mage::getSingleton('customer/session')->getAgent();
            $session->unsetData($agent::AGENT_IMPERSONATES_ROLE_KEY);
            $agentReload = Mage::getModel('agent/agent')->load($agent->getId());
            $session->setAgent($agentReload);
            // create new quote
            $actualQuote = Mage::getSingleton('checkout/cart')->getQuote();
            Mage::helper('dyna_checkout')->createNewQuote($actualQuote);
        } else {
            $error = true;
            $message = $this->__("Request for changing business function needs to be post.");
        }

        return $this->getResponse()
            ->setHeader('Content-Type', 'application/json')
            ->setBody(Mage::helper('core')->jsonEncode([
                'error' => $error,
                'message' => $message,
            ]));
    }

    /**
     * Login as Another Agent Action
     */
    public function loginAsAgentAction()
    {
        $session = $this->_getSession();
        $agent = $session->getAgent();
        $agentDe = ($agent) ? Mage::getModel("agentde/agent")->load($agent->getId()) : false;

        if (!($agentDe->isGranted(Dyna_AgentDE_Model_Agent::AGENT_ROLE_PERMISSION_IMPERSONATE))) {
            $session->addError($this->__('Impersonation rights are required.'));
            return;
        } elseif ($this->isAjax()) {
            $login = $this->getRequest()->getPost();

            try {
                $actualQuote = Mage::getSingleton('checkout/cart')->getQuote();
                $agentDe->authenticate($login['username'], null, true);
                $username = $this->_getSession()->getAgent()->getUsername();
                $dealerId = $this->_getSession()->getAgent()->getDealer() ? $this->_getSession()->getAgent()->getDealer()->getVfDealerCode() : '';
                // get the new permissions for displaying warnings on 360 view
                $customerProducts = Mage::getSingleton('customer/session')->getCustomerProducts();
                $impersonatedAgent = $this->_getSession()->getAgent();

                $response = [
                    "error" => false,
                    "username" => $username,
                    "dealer" => $dealerId,
                    "is_prospect" => (bool)Mage::getSingleton('customer/session')->getCustomer()->getIsProspect(),
                    "show_kias" => (!$impersonatedAgent->isGranted('ALLOW_MOB')) ? ((isset($customerProducts[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS])) ? true : false) : false,
                    "show_kd" => (!$impersonatedAgent->isGranted('ALLOW_CAB')) ? ((isset($customerProducts[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD])) ? true : false) : false,
                    "show_fn" => (!$impersonatedAgent->isGranted('ALLOW_DSL')) ? ((empty($customerProducts[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN])) ? false : true) : false,
                ];

                $this->getResponse()->setHeader('Content-Type', 'application/json');
                $this->getResponse()->setBody(json_encode($response));
                // clear cart and create new quote
                Mage::helper('dyna_checkout')->createNewQuote($actualQuote);
            } catch (Mage_Core_Exception $e) {
                $this->getResponse()->setHeader('Content-Type', 'application/json');
                $this->getResponse()->setBody(json_encode(array("error" => true, "message" => $e->getMessage())));
                $this->getResponse()->setHttpResponseCode(200);
            } catch (Exception $e) {
                $this->getResponse()->setHeader('Content-Type', 'application/json');
                $this->getResponse()->setHttpResponseCode(200);
                $this->getResponse()->setBody(json_encode(array(
                    "error" => true,
                    "message" => $this->__('Server Error')
                )));
                // Mage::getSingleton('core/logger')->logException($e); // PA DSS violation: this exception log can disclose customer password
            }
            return;
        } elseif ($this->getRequest()->isPost()) {
            $login = $this->getRequest()->getPost('login');
            if (!empty($login['username'])) {
                try {
                    $actualQuote = Mage::getSingleton('checkout/cart')->getQuote();
                    $agentDe->authenticate($login['username'], null, true);
                    // clear cart and create new quote
                    Mage::helper('dyna_checkout')->createNewQuote($actualQuote);
                } catch (Mage_Core_Exception $e) {
                    $session->addError($e->getMessage());
                } catch (Exception $e) {
                    $session->addError($this->__('Server Error'));
                    // Mage::getSingleton('core/logger')->logException($e); // PA DSS violation: this exception log can disclose customer password
                }
            } else {
                $session->addError($this->__('Login is required.'));
            }
            return;
        }
        $this->_loginPostRedirect();
    }

    /**
     * Super Agent logged in as another agent logout action
     */
    public function logoutAsAgentAction()
    {
        $superAgent = $this->_getSession()->getSuperAgent();
        if (!$superAgent) {
            $this->getResponse()->setHttpResponseCode(200);
            $this->getResponse()->setHeader('Content-Type', 'application/json');
            $this->getResponse()->setBody(json_encode(array(
                "error" => true,
                "message" => $this->__('You are not logged in as another agent')
            )));
        } else {
            $this->_getSession()->setAgent($superAgent);
            $this->_getSession()->setSuperAgent(null);
            // clear cart and create new quote
            Mage::helper('dyna_checkout')->createNewQuote();
            $customerProducts = Mage::getSingleton('customer/session')->getCustomerProducts();
            $response = [
                "is_prospect" => (bool)Mage::getSingleton('customer/session')->getCustomer()->getIsProspect(),
                "show_kias" => (!$superAgent->isGranted('ALLOW_MOB')) ? ((isset($customerProducts[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS])) ? true : false) : false,
                "show_kd" => (!$superAgent->isGranted('ALLOW_CAB')) ? ((isset($customerProducts[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD])) ? true : false) : false,
                "show_fn" => (!$superAgent->isGranted('ALLOW_DSL')) ? ((empty($customerProducts[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN])) ? false : true) : false,
            ];

            $this->getResponse()->setHttpResponseCode(200);
            $this->getResponse()->setHeader('Content-Type', 'application/json');
            $this->getResponse()->setBody(json_encode($response));
        }
        return;
    }

    /**
     * Get sales Ids triple from matching Provis import based on either COPS or Telesales agent's redSalesId
     * @return $voids|bool
     */
    public function getAgentSalesIdsAction()
    {
        /** @var Dyna_Bundles_Helper_Data $bundlesHelper */
        $bundlesHelper = Mage::helper('bundles');

        $salesIdsTriple = $bundlesHelper->getProvisDataByRedSalesId();
        if ($salesIdsTriple && (empty($salesIdsTriple['error']))) {
            $this->getResponse()
                ->setHeader('Content-Type', 'application/json')
                ->setBody(Mage::helper('core')->jsonEncode([
                    'error' => false,
                    'data' => $salesIdsTriple
                ]));
            return;
        }
        $this->getResponse()
            ->setHeader('Content-Type', 'application/json')
            ->setBody(Mage::helper('core')->jsonEncode([
                'error' => true,
                'message' => $salesIdsTriple['message']
            ]));
        return false;
    }

    public function changeAgentLockStatusAction()
    {
        $post = $this->getRequest()->getPost();
        $customerSession = Mage::getSingleton('dyna_customer/session');
        $agent = $customerSession->getAgent();
        $agentHasLockingAccounts = $agent->isGranted('LOCKING_ACCOUNTS');

        if (!$agentHasLockingAccounts) {
            $this->getResponse()
                ->setHeader('Content-Type', 'application/json')
                ->setBody(Mage::helper('core')->jsonEncode([
                    'error' => true,
                    'message' => $this->__('Permission LOCKING_ACCOUNTS is required in order to perform this action.')
                ]));
            return;
        }

        if ($post) {
            $agentId = (int)$this->getRequest()->getParam("agentId");
            $currentAgent = Mage::getModel('agent/agent')->load($agentId);

            if ($currentAgent->getLocked()) {
                $currentAgent->setLocked(null)->save();
            } else {
                $currentAgent->setLocked(1)->save();
            }

            $this->getResponse()
                ->setHeader('Content-Type', 'application/json')
                ->setBody(Mage::helper('core')->jsonEncode([
                    'error' => false,
                    'data' => $currentAgent
                ]));
        }
    }

    public function searchAgentsAction()
    {
        $post = $this->getRequest()->getPost();
        $customerSession = Mage::getSingleton('dyna_customer/session');
        $agent = $customerSession->getAgent();
        $agentHasLockingAccounts = $agent->isGranted('LOCKING_ACCOUNTS');

        if (!$agentHasLockingAccounts) {
            $this->getResponse()
                ->setHeader('Content-Type', 'application/json')
                ->setBody(Mage::helper('core')->jsonEncode([
                    'error' => true,
                    'message' => $this->__('Permission LOCKING_ACCOUNTS is required in order to perform this action.')
                ]));
            return;
        }

        if ($post) {
            $terms = explode(' ', trim($this->getRequest()->getParam("search")));
            $customerSession = Mage::getSingleton('dyna_customer/session');
            $agent = $customerSession->getAgent();

            /** @var Dyna_Agent_Model_Mysql4_Agent_Collection $collection */
            $collection = Mage::getModel('agent/agent')
                ->getCollection()
                ->addFieldToFilter('is_active', 1)
                ->addFieldToFilter('agent_id', ['neq' => $agent->getId()]);

            foreach ($terms as $term) {
                $collection->addFieldToFilter(
                    array('first_name', 'last_name'),
                    array(
                        array('like' => "%{$term}%"),
                        array('like' => "%{$term}%")
                    )
                );
            }

            $collection->setPageSize(10)
                ->setCurPage(1);

            $response = [];
            foreach ($collection as $agent) {
                $response[] = $agent->getData();
            }

            $this->getResponse()
                ->setHeader('Content-Type', 'application/json')
                ->setBody(Mage::helper('core')->jsonEncode([
                    'error' => false,
                    'data' => $response
                ]));
        }
    }

    /**
     * Login post action
     */
    public function loginPostAction()
    {
        if ($this->_getSession()->getAgent()) {
            $this->_redirectUrl(Mage::getUrl());
            return;
        }
        $session = $this->_getSession();

        if ($this->getRequest()->isPost()) {
            $login = $this->getRequest()->getPost('login');
            if (!empty($login['username']) && !empty($login['password'])) {
                $agent = Mage::getModel('agent/agent')->getCollection()->addFieldToFilter('username',
                    $login['username'])->getFirstItem();
                try {
                    $agent->authenticate($login['username'], $login['password'])
                        ->validateActivityTreshold($login['username']);
                } catch (Mage_Core_Exception $e) {
                    switch ($e->getCode()) {
                        case Mage_Customer_Model_Customer::EXCEPTION_INVALID_EMAIL_OR_PASSWORD:
                            $message = $e->getMessage();
                            Mage::getSingleton('customer/session')->unsTemporaryAgent();
                            $agent->incrementLoginAttempts();
                            break;
                        case Dyna_Agent_Model_Agent::LOCKED_AGENT:
                            $this->_getSession()->setAgent(null);
                            $this->_getSession()->setSuperAgent(null);
                            Mage::getSingleton('customer/session')->unsTemporaryAgent();
                            $message = $e->getMessage();
                            break;
                        case Dyna_Agent_Model_Agent::TEMPORARY_PASSWORD:
                            $this->_getSession()->setAgent(null);
                            $this->_getSession()->setSuperAgent(null);
                            if (Mage::getSingleton('customer/session')->getTemporaryAgent()) {
                                $this->_getSession()->setFirstPasswordFlag(true);
                                return $this->_redirect('*/*/temporaryPassword');
                            }
                            break;
                        default:
                            $message = $e->getMessage();
                            Mage::getSingleton('customer/session')->unsTemporaryAgent();
                            $agent->incrementLoginAttempts();
                    }
                    $session->addError($message);
                    $session->setUsername($login['username']);
                    return $this->_redirect('*/*/login');
                } catch (Exception $e) {
                    // Mage::getSingleton('core/logger')->logException($e); // PA DSS violation: this exception log can disclose customer password
                }
            } else {
                $session->addError($this->__('Login and password are required.'));
            }
        }

        Mage::getSingleton('customer/session')->unsTemporaryAgent();
        $agent = $session->getAgent();
        if ($agent && !$agent->getEverlastingPass()) {
            $passwordDeadline = new DateTime($agent->getPasswordChanged());
            $passwordDeadline->add(new DateInterval(sprintf('P%sD',
                abs((int)Mage::getStoreConfig(Dyna_Agent_Model_Agent::PASSWORD_EXPIRY_DURATION)))));
            if ($passwordDeadline->getTimestamp() <= Mage::getModel('core/date')->timestamp(time())) {
                Mage::getSingleton('customer/session')->setTemporaryAgent($agent);
                $this->_getSession()->setAgent(null);
                $this->_getSession()->setSuperAgent(null);
                $this->_getSession()->setTemporaryPasswordFlag(true);
                return $this->_redirect('*/*/temporaryPassword');
            }
        }
        if ($session->getAgent()
            && !empty($dealerForm['address'])
            && ($dealer = $this->validateAddress($dealerForm['address']))
            && $dealer->getStoreId() == Mage::app()->getStore()->getId()
        ) {
            $session->getAgent()->setDealer($dealer);
        }

        $this->_getSession()->renewSession();
        Mage::dispatchEvent("dyna_agent_login");

        $this->_loginPostRedirect();
    }

    /**
     * Login Webservice action
     */
    public function loginWebServiceAction()
    {

        $session = $this->_getSession();

        if ($this->getRequest()->isPost()) {
            $login = $this->getRequest()->getPost();
            if (!empty($login['username']) && !empty($login['password'])) {
                $agent = Mage::getModel('agent/agent')->getCollection()->addFieldToFilter('username',
                    $login['username'])->getFirstItem();
                try {
                    $agent->authenticate($login['username'], $login['password'])
                        ->validateActivityTreshold($login['username']);
                } catch (Mage_Core_Exception $e) {
                    switch ($e->getCode()) {
                        case Mage_Customer_Model_Customer::EXCEPTION_INVALID_EMAIL_OR_PASSWORD:
                            $message = $e->getMessage();
                            Mage::getSingleton('customer/session')->unsTemporaryAgent();
                            $agent->incrementLoginAttempts();
                            break;
                        case Dyna_Agent_Model_Agent::LOCKED_AGENT:
                            $this->_getSession()->setAgent(null);
                            $this->_getSession()->setSuperAgent(null);
                            Mage::getSingleton('customer/session')->unsTemporaryAgent();
                            $message = $e->getMessage();
                            break;
                        case Dyna_Agent_Model_Agent::TEMPORARY_PASSWORD:
                            $this->_getSession()->setAgent(null);
                            $this->_getSession()->setSuperAgent(null);
                            if (Mage::getSingleton('customer/session')->getTemporaryAgent()) {
                                $message = "Temporary Password";
                            }
                            break;
                        default:
                            $message = $e->getMessage();
                            Mage::getSingleton('customer/session')->unsTemporaryAgent();
                            $agent->incrementLoginAttempts();
                    }
                    $session->addError($message);
                    $session->setUsername($login['username']);
                } catch (Exception $e) {
                    // Mage::getSingleton('core/logger')->logException($e); // PA DSS violation: this exception log can disclose customer password
                }
            } else {
                $message = $this->__('Login and password are required.');
            }

            $agent = $session->getAgent();
            $agentData = array(
                'id' => $agent->getId(),
                'username' => $agent->getUsername()
            );

            $this->getResponse()
                ->setHeader('Content-Type', 'application/json')
                ->setBody(Mage::helper('core')->jsonEncode([
                    'error' => $message,
                    'data' => $agentData
                ]));
        }

    }

    /**
     * Customer login form page
     */
    public function loginAction()
    {
        if ($this->_getSession()->getAgent()) {
            $this->_redirectUrl(Mage::getUrl());
            return;
        }
        $adapter = Mage::getStoreConfig('omnius_service/authentication_configuration/adapter_type');
        if ($adapter == "OpenID Connect") {
            //@todo : Code to redirect the agent to Login page which follows OpenID Protocol
        } else {                      // continue to the default omnius login page
            $this->getResponse()->setHeader('Login-Required', 'true');
            $this->loadLayout();
            $this->_initLayoutMessages('customer/session');
            $this->_initLayoutMessages('catalog/session');
            $this->renderLayout();
        }
    }
}
