<?php
/**
 * Class Dyna_AgentDE_Adminhtml_Vodafoneship2storesController
 */
class Dyna_AgentDE_Adminhtml_Vodafoneship2storesController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->_title($this->__("Agent"));
        $this->_title($this->__("Vodafone Ship To Stores"));

        $this->_initAction();
        $this->renderLayout();
    }

    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu("agent/vodafoneship2stores")
            ->_addBreadcrumb(Mage::helper("adminhtml")
                ->__("Vodafone Ship To Stores"), Mage::helper("adminhtml")->__("Vodafone Ship To Stores"));
        return $this;
    }

    public function editAction()
    {
        $this->_title($this->__("AgentDE"));
        $this->_title($this->__("Vodafone ship to store"));
        $this->_title($this->__("Edit Item"));

        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("agentde/vodafoneShip2Stores")->load($id);
        if ($model->getId()) {
            Mage::register("vodafoneship2stores_data", $model);
            $this->loadLayout();
            $this->_setActiveMenu("agent/vodafoneship2stores");
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Vodafone Ship To Stores Manager"), Mage::helper("adminhtml")->__("Vodafone Ship To Stores Manager"));
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Vodafone Ship To Stores Description"), Mage::helper("adminhtml")->__("Vodafone Ship To Stores Description"));
            $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock("agentde/adminhtml_vodafoneship2stores_edit"))->_addLeft($this->getLayout()->createBlock("agentde/adminhtml_vodafoneship2stores_edit_tabs"));
            $this->renderLayout();
        } else {
            Mage::getSingleton("adminhtml/session")->addError(Mage::helper("agentde")->__("Item does not exist."));
            $this->_redirect("*/*/");
        }
    }

    public function saveAction()
    {
        $post_data = $this->getRequest()->getPost();

        if ($post_data) {
            try {
                $model = Mage::getModel("agentde/vodafoneShip2Stores")
                    ->addData($post_data)
                    ->setId($this->getRequest()->getParam("id"))
                    ->save();

                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Vodafone Ship To Stores was successfully saved"));
                Mage::getSingleton("adminhtml/session")->setProvissalesData(false);

                if ($this->getRequest()->getParam("back")) {
                    $this->_redirect("*/*/edit", array("id" => $model->getId()));
                    return;
                }
                $this->_redirect("*/*/");
                return;
            } catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                Mage::getSingleton("adminhtml/session")->setProvissalesData($this->getRequest()->getPost());
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
                return;
            }

        }
        $this->_redirect("*/*/");
    }

    public function deleteAction()
    {
        if ($this->getRequest()->getParam("id") > 0) {
            try {
                $id = $this->getRequest()->getParam("id");
                $model = Mage::getModel("agentde/vodafoneShip2Stores");
                $model->setId($id)->delete();
                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item was successfully deleted"));
                $this->_redirect("*/*/");
            } catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
            }
        }
        $this->_redirect("*/*/");
    }

    public function massRemoveAction()
    {
        try {
            $ids = $this->getRequest()->getPost('entity_ids', array());
            foreach ($ids as $id) {
                $model = Mage::getModel("agentde/vodafoneShip2Stores");
                $model->setId($id)->delete();
            }
            Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item(s) was successfully removed"));
        } catch (Exception $e) {
            Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
        }
        $this->_redirect('*/*/');
    }

    //Patch for SUPEE-6285

    protected function _isAllowed()
    {
        return true;
    }
}
