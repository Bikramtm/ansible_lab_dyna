<?php
/**
 * Class Dyna_AgentDE_Adminhtml_PhonebookindustriesController
 */
class Dyna_AgentDE_Adminhtml_PhonebookindustriesController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->_title($this->__("Agent"));
        $this->_title($this->__("Phone Book Industries"));

        $this->_initAction();
        $this->renderLayout();
    }

    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu("agent/phonebookindustries")
            ->_addBreadcrumb(Mage::helper("adminhtml")
                ->__("Phone Book Industries"), Mage::helper("adminhtml")->__("Phone Book Industries"));
        return $this;
    }

    public function newAction()
    {

        $this->_title($this->__("Agent"));
        $this->_title($this->__("Phone Book Industries"));
        $this->_title($this->__("New Item"));

        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("agentde/phoneBookIndustries")->load($id);

        $data = Mage::getSingleton("adminhtml/session")->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }

        Mage::register("phonebookindustries_data", $model);

        $this->loadLayout();
        $this->_setActiveMenu("agent/phonebookindustries");

        $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);

        $this->_addContent($this->getLayout()->createBlock("agentde/adminhtml_phonebookindustries_edit"))->_addLeft($this->getLayout()->createBlock("agentde/adminhtml_phonebookindustries_edit_tabs"));

        $this->renderLayout();

    }

    public function editAction()
    {
        $this->_title($this->__("AgentDE"));
        $this->_title($this->__("Phone Book Industries"));
        $this->_title($this->__("Edit Item"));

        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("agentde/phoneBookIndustries")->load($id);
        if ($model->getId()) {
            Mage::register("phonebookindustries_data", $model);
            $this->loadLayout();
            $this->_setActiveMenu("agent/phonebookindustries");
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Phone Book Industries"), Mage::helper("adminhtml")->__("Phone Book Industries"));
            $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock("agentde/adminhtml_phonebookindustries_edit"))->_addLeft($this->getLayout()->createBlock("agentde/adminhtml_phonebookindustries_edit_tabs"));
            $this->renderLayout();
        } else {
            Mage::getSingleton("adminhtml/session")->addError(Mage::helper("agentde")->__("Item does not exist."));
            $this->_redirect("*/*/");
        }
    }

    public function saveAction()
    {
        $post_data = $this->getRequest()->getPost();

        if ($post_data) {
            try {
                $model = Mage::getModel("agentde/phoneBookIndustries")
                    ->addData($post_data)
                    ->setId($this->getRequest()->getParam("id"))
                    ->save();

                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Phone Book Industry was successfully saved"));
                Mage::getSingleton("adminhtml/session")->setPhonebookindustriesData(false);

                if ($this->getRequest()->getParam("back")) {
                    $this->_redirect("*/*/edit", array("id" => $model->getId()));
                    return;
                }
                $this->_redirect("*/*/");
                return;
            } catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                Mage::getSingleton("adminhtml/session")->setPhonebookindustriesData($this->getRequest()->getPost());
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
                return;
            }

        }
        $this->_redirect("*/*/");
    }

    public function deleteAction()
    {
        if ($this->getRequest()->getParam("id") > 0) {
            try {
                $id = $this->getRequest()->getParam("id");
                $model = Mage::getModel("agentde/phoneBookIndustries");
                $model->setId($id)->delete();
                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item was successfully deleted"));
                $this->_redirect("*/*/");
            } catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
            }
        }
        $this->_redirect("*/*/");
    }

    public function massRemoveAction()
    {
        try {
            $ids = $this->getRequest()->getPost('entity_ids', array());
            foreach ($ids as $id) {
                $model = Mage::getModel("agentde/phoneBookIndustries");
                $model->setId($id)->delete();
            }
            Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item(s) was successfully removed"));
        } catch (Exception $e) {
            Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
        }
        $this->_redirect('*/*/');
    }

    protected function _isAllowed()
    {
        return true;
    }

    public function importAction()
    {
        $this->loadLayout();
        $this->getLayout()->getBlock('catalog.product.edit.tab.related');

        $this->renderLayout();
    }

    public function importPhonebookindustriesAction()
    {
        if (isset($_FILES['file']) && isset($_FILES['file']['tmp_name'])) {

            /** @var Dyna_Agent_Model_GenericImporter $importer */
            $importer = Mage::getSingleton('agent/genericImporter');

            try {
                if ($_FILES['file']['tmp_name']) {
                    $uploader = new Mage_Core_Model_File_Uploader('file');
                    $uploader->setAllowedExtensions($importer->getSupportedFormats());
                    $path = Mage::app()->getConfig()->getTempVarDir() . '/import/';
                    $uploader->save($path);
                    if ($uploadFile = $uploader->getUploadedFileName()) {
                        // check and convert to UTF8
                        $contents = file_get_contents($path . $uploadFile);
                        $this->_hasError = false;
                        $bom = pack("CCC", 0xef, 0xbb, 0xbf);
                        (0 === strncmp($contents, $bom, 3)) ? $withBoom = true : $withBoom = false;
                        $isUTF8Converted = false;

                        if (in_array(pathinfo($path . $uploadFile, PATHINFO_EXTENSION), ['json', 'csv', 'xml']) && (!mb_check_encoding($contents, 'UTF-8') || $withBoom)) {
                            if ($withBoom) {
                                $contents = substr($contents, 3);
                            } else {
                                $encoding = mb_detect_encoding($contents, mb_detect_order(), true);
                                if ($encoding) {
                                    iconv($encoding, "UTF-8", $contents);
                                } else {
                                    Mage::getSingleton('adminhtml/session')->addError(
                                        $this->__('Failed to detect encoding for uploaded content')
                                    );
                                }
                            }
                            file_put_contents($path . '_UTF-8_' . $uploadFile, $contents);
                            $isUTF8Converted = true;
                        }

                        $newFilename = 'import-' . date('YmdHis') . '_' . $uploadFile;
                        rename($path . ($isUTF8Converted ? '_UTF-8_' : '' ) .$uploadFile, $path . $newFilename);
                    }
                }

                if (isset($newFilename) && $newFilename) {
                    $importer->import($path . $newFilename, Mage::getModel('agentde/mapper_phoneBookIndustries'));
                    return $this->_redirect('*/*/approveImport');
                }
            } catch (TypeError $err) {
                Mage::getSingleton('adminhtml/session')->addError(
                    $this->__('Unexpected content format')
                );
                $this->_redirect('*/*');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    $this->__($e->getMessage())
                );
                $this->_redirect('*/*');
            }
        }
        else {
            Mage::getSingleton('adminhtml/session')->addError(
                $this->__('Invalid POST data (please check post_max_size and upload_max_filesize settings in your php.ini file).')
            );
            $this->_redirect('*/*');
        }
    }

    public function approveImportAction()
    {
        if ( ! Mage::getSingleton('core/session')->getData('import_rows')) {
            return $this->_redirect('*/*');
        }

        $this->loadLayout();
        // reuse template defined in omnius agent
        $approve = $this->getLayout()->createBlock('agent/adminhtml_approve');
        $mapper = Mage::getModel('agentde/mapper_phoneBookIndustries');
        Mage::register('mapper', $mapper);
        $this->_addContent($approve->setFinishUrl($this->getUrl('*/*/finishImport')));
        $this->renderLayout();
    }

    public function finishImportAction()
    {
        $session = Mage::getSingleton('core/session');
        $importRows = $session->getData('import_rows');
        $session
            ->unsetData('import_headers')
            ->unsetData('import_rows');

        /** @var Mage_Core_Model_Resource_Transaction $transaction */
        $transaction = Mage::getModel('core/resource_transaction');

        foreach ($importRows as $row) {
            if ($row instanceof Mage_Core_Model_Abstract) {
                if ($row instanceof Dyna_AgentDE_Model_PhoneBookIndustries && !$row->getName()) {
                    continue;
                }
                $transaction->addObject($row);
            }
        }

        try {
            $transaction->save();
            Mage::getSingleton('adminhtml/session')->addSuccess(
                $this->__('Import file successfully parsed and imported into database')
            );
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError(
                $this->__($e->getMessage())
            );
            $this->_redirect('*/*');
        }
        $this->_redirect('*/*');
    }
}