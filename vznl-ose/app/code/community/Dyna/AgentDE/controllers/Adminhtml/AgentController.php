<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_AgentDE_Adminhtml_AgentController
 */
require_once Mage::getModuleDir('controllers', 'Dyna_Agent') . DS . 'Adminhtml' . DS . 'AgentController.php';

class Dyna_AgentDE_Adminhtml_AgentController extends Dyna_Agent_Adminhtml_AgentController
{
    public function importAgentsAction()
    {
        if (isset($_FILES['file']) && isset($_FILES['file']['tmp_name'])) {

            /** @var Dyna_AgentDE_Model_Importer $importer */
            $importer = Mage::getSingleton('agentde/Importer');
            try {
                if ($_FILES['file']['tmp_name']) {
                    $uploader = new Mage_Core_Model_File_Uploader('file');
                    $uploader->setAllowedExtensions($importer->getSupportedFormats());
                    $path = Mage::app()->getConfig()->getTempVarDir() . '/import/';
                    $uploader->save($path);
                    if ($uploadFile = $uploader->getUploadedFileName()) {
                        $newFilename = 'import-' . date('YmdHis') . '_' . $uploadFile;
                        rename($path . $uploadFile, $path . $newFilename);
                    }
                }

                if (isset($newFilename) && $newFilename) {
                    $importer->import($path . $newFilename, Mage::getModel('agent/mapper_agent'));
                    return $this->_redirect('*/*/approveImport');
                }
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    $this->__($e->getMessage())
                );
                $this->_redirect('*/*');
            }
        }
        else {
            Mage::getSingleton('adminhtml/session')->addError(
                $this->__('Invalid POST data (please check post_max_size and upload_max_filesize settings in your php.ini file).')
            );
            $this->_redirect('*/*');
        }
    }

    public function commisionAction()
    {
        $agentId = $this->getRequest()->getParam("id");
        $this->loadLayout();
        $this->getLayout()->getBlock('admin.agentde.commision')
            ->setAgentId($agentId);
        $this->renderLayout();
    }

    public function editAction()
    {
        $this->_title($this->__("Agent"));
        $this->_title($this->__("Agent"));
        $this->_title($this->__("Edit Item"));

        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("agentde/agent")->load($id);

        if ($model->getId()) {
            $additionalRoleIds = $model->getAdditionalRoleIds();
            $model->setAdditionalRoleIds($additionalRoleIds ? explode(",", $additionalRoleIds) : array());
            Mage::register("agent_data", $model);
            $this->loadLayout();
            $this->_setActiveMenu("agent/agent");
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Agent Manager"), Mage::helper("adminhtml")->__("Agent Manager"));
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Agent Description"), Mage::helper("adminhtml")->__("Agent Description"));
            $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock("agent/adminhtml_agent_edit"))->_addLeft($this->getLayout()->createBlock("agent/adminhtml_agent_edit_tabs"));
            $this->renderLayout();
        } else {
            Mage::getSingleton("adminhtml/session")->addError(Mage::helper("agent")->__("Item does not exist."));
            $this->_redirect("*/*/");
        }
    }

    public function saveAction()
    {
        $postData = $this->getRequest()->getPost();

        if ($postData) {
            try {
                $currentAgent = Mage::getModel("agent/agent")->load($this->getRequest()->getParam("id"));
                $toSaveData = array();
                foreach ($postData as $k=>$val) {
                    $toSaveData[$k] = trim($val);
                    if ($k == 'password' || ($k == 'change_password' && strlen($val) > 0)) {
                        Mage::helper('agent')->validatePassword($currentAgent, $val);
                        $toSaveData['password'] = Mage::helper('core')->getHash($val, Dyna_Agent_Model_Agent::HASH_SALT_LENGTH);
                        Mage::register('agent_password_changed', true);
                    }

                }

                if ((isset($toSaveData['reset_attempts']) && $toSaveData['reset_attempts'] !== null) || (isset($toSaveData['locked']) && $currentAgent->getLocked() != $toSaveData['locked'] && !$toSaveData['locked'])){
                    $toSaveData['login_attempts'] = 0;
                }

                if (!$toSaveData['dealer_id']) {
                    $toSaveData['dealer_id']=null;
                }

                if(!$currentAgent->getAgentId()){
                    $toSaveData['last_login_date'] = date('Y-m-d');
                }

                $model = Mage::getModel("agent/agent")
                    ->addData($toSaveData)
                    ->setId($this->getRequest()->getParam("id"));

                if (isset($toSaveData['group_id'])) {
                    $model->setData('group_id', $toSaveData['group_id']);
                }

                // save additional role ids
                if ($roleIds = $this->getRequest()->getParam("additional_role_ids")) {
                    $model->setAdditionalRoleIds(implode(',', $roleIds));
                } else {
                    $model->setAdditionalRoleIds(null);
                }

                // save mode
                $model->save();

                // save impersonation data
                $connection     = Mage::getSingleton('core/resource')->getConnection('core_write');
                $impersonateIds = isset($postData['impersonate_ids']) ? $postData['impersonate_ids'] : array();
                $impersonateIds = array_filter(array_map('intval', $impersonateIds));

                if (!count($impersonateIds)) {
                    $sql = sprintf("DELETE FROM `agent_impersonate` WHERE agent_id='%s'", $model->getId());
                } else {
                    $sql = sprintf("DELETE FROM `agent_impersonate` WHERE `impersonate_id` NOT IN (%s) AND agent_id='%s'", join(',', $impersonateIds), $model->getId());
                }

                $connection->query($sql);
                foreach ($impersonateIds as $id) {
                    try {
                        $connection->insert('agent_impersonate', array('agent_id' => $model->getId(), 'impersonate_id' => $id));
                    } catch (Exception $e) {
                        //record already exists
                    }
                }
                // end impersonation save

                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Agent was successfully saved"));
                Mage::getSingleton("adminhtml/session")->setAgentData(false);

                if ($this->getRequest()->getParam("back")) {
                    $this->_redirect("*/*/edit", array("id" => $model->getId()));
                    return;
                }
                $this->_redirect("*/*/");
                return;
            } catch (Exception $e) {
                $agentData = $this->getRequest()->getPost();
                if($e->getCode() == Dyna_Agent_Helper_Data::AGENT_FAILED_PASSWORD){
                    $agentData['password'] = '';
                    $agentData['change_password'] = '';
                }
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                Mage::getSingleton("adminhtml/session")->setAgentData($agentData);
                if($this->getRequest()->getParam("id")){
                    $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
                }else {
                    $this->_redirect("*/*/new");
                }

                return;
            }
        }
        $this->_redirect("*/*/");
    }

    public function deleteAction()
    {
        if ($this->getRequest()->getParam("id") > 0) {
            try {
                $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
                $connection->query(sprintf("DELETE FROM `agent_impersonate` WHERE agent_id='%s'", $this->getRequest()->getParam("id")));

                $model = Mage::getModel("agent/agent");
                $model->setId($this->getRequest()->getParam("id"))->delete();

                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item was successfully deleted"));
                $this->_redirect("*/*/");
            } catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
            }
        }
        $this->_redirect("*/*/");
    }

    public function massRemoveAction()
    {
        try {
            $ids = $this->getRequest()->getPost('agent_ids', array());
            $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
            foreach ($ids as $id) {
                $connection->query(sprintf("DELETE FROM `agent_impersonate` WHERE agent_id='%s'", $id));

                $model = Mage::getModel("agent/agent");
                $model->setId($id)->delete();
            }
            Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item(s) was successfully removed"));
        } catch (Exception $e) {
            Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
        }
        $this->_redirect('*/*/');
    }
}
