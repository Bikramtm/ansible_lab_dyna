<?php
/**
 * Class Dyna_AgentDE_Adminhtml_AgentroleController
 */
require_once Mage::getModuleDir('controllers', 'Dyna_Agent') . DS . 'Adminhtml' . DS . 'AgentroleController.php';

class Dyna_AgentDE_Adminhtml_AgentroleController extends Dyna_Agent_Adminhtml_AgentroleController
{
    public function massRemoveAction()
    {
        try {
            $ids = $this->getRequest()->getPost('role_ids', array());
            $deleted = 0;

            foreach ($ids as $id) {
                $collection = Mage::getModel('agent/agent')
                    ->getCollection()
                    ->addFieldToFilter('role_id', $id);

                if ($collection->count()) {
                    Mage::getSingleton("adminhtml/session")->addError('Skipping role_id "'.$id.'" as it is assigned to an existing agent.');
                } else {
                    $model = Mage::getModel("agent/agentrole");
                    $model->setId($id)->delete();
                    $deleted++;
                }
            }

            if ($deleted > 0 ) {
                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item(s) was successfully removed"));
            }
        } catch (Exception $e) {
            Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
        }
        $this->_redirect('*/*/');
    }
}
