<?php

$this->startSetup();

$configPath = "agent/general_config/inactivity_threshold";

if (Mage::getStoreConfig($configPath)) {
    // Get default value listed in config.xml file from Dyna_AgentDE module
    $value = Mage::getConfig()
        ->loadModulesConfiguration('config.xml')
        ->getNode("default/" . $configPath);
    Mage::getConfig()->saveConfig($configPath, $value, 'default', 0);
}

$this->endSetup();