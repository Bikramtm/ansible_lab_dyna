<?php

$installer = $this;

// add permission to SEND OFFER
$permissions = array(
    'CHANGE_PASSWORD' => 'Agent is able to change own password',
);

$conn = Mage::getSingleton('core/resource')->getConnection('core_write');
foreach ($permissions as $code => $description) {
    $conn->insert('role_permission', array('name' => $code, 'description' => $description));
}

$installer->endSetup();