<?php
$conn = Mage::getSingleton('core/resource')->getConnection('core_write');
$conn->exec("DELETE FROM core_config_data WHERE path='agent/password_validation/reset_password';");
$conn->exec("INSERT INTO core_config_data(scope, scope_id,path, value) values ('default', 0, 'agent/password_validation/reset_password', 'Wenn Sie Ihr Passwort vergessen haben, wenden Sie sich bitte an Ihren Vorgesetzten (Agentur Mitarbeiter) oder melden Sie sich in nGUM an (Vodafone Mitarbeiter), um Ihr Passwort zurückzusetzten')");