<?php
$tsWebsite = Mage::app()->getWebsite('telesales');
$tsStore = $tsWebsite->getDefaultStore();
if ($tsWebsite->getId() && $tsStore->getId()) {
    // Enable agent login for telesales website
    $tsStore->setShowAgent(1)->save();
    // Create a role with all permissions
    /** @var Dyna_Agent_Model_Agentrole $role */
    $role = Mage::getModel('agent/agentrole')->getCollection()->addFieldToFilter('role', 'Test Role')->getFirstItem();
    if (!$role || !$role->getId()) {
        $role = Mage::getModel('agent/agentrole')
            ->setRole('Test Role')
            ->setRoleDescription('All Roles')
            ->save();
        $permissionIds = Mage::getResourceModel('agent/permission_collection')->getAllIds();
        /** @var Magento_Db_Adapter_Pdo_Mysql $conn */
        $conn = Mage::getSingleton('core/resource')->getConnection('core_write');
        foreach ($permissionIds as $id) {
            try {
                $conn->insert('role_permission_link', ['role_id' => $role->getId(), 'permission_id' => $id]);
            } catch (Exception $e) {
                //record already exists
            }
        }
    }
    // Find a dealer for telesales or create one
    /** @var Dyna_Agent_Model_Dealer $dealer */
    $dealer = Mage::getModel('agent/dealer')->getCollection()->addFieldToFilter('store_id', $tsStore->getId())->getFirstItem();
    if (!$dealer || !$dealer->getId()) {
        $dealerData = [
            'vf_dealer_code' => '34413071',
            'street' => 'Ferdinand-Braun-Platz',
            'postcode' => '40549',
            'city' => 'Dusseldorf',
            'house_nr' => '1',
            'store_id' => $tsStore->getId(),
        ];
        $dealer = Mage::getModel('agent/dealer')
            ->addData($dealerData)
            ->save();
    }
    // Create an agent to enable login for telesales
    $agentData = [
        'username' => 'demo-agent',
        'password' => 'Dyna123',
        'is_active' => true,
        'role_id' => $role->getId(),
        'store_id' => $tsStore->getId(),
        'email' => 'agent@test-agent.no',
        'phone' => '045 524 1283',
        'first_name' => 'Test',
        'last_name' => 'Agent',
        'group_id' => [],
        'dealer_id' => $dealer->getId(),
        'everlasting_pass' => true,
        'employee_number' => '1231231',
    ];
    $agent = Mage::getModel('agent/agent')
        ->addData($agentData)
        ->save();
    $agent->setPassword($agentData['password'])
        ->setTemporaryPassword(false)
        ->save();
}
