<?php
/**
 * VFDED1W3S-3208
 */
$conn = Mage::getSingleton('core/resource')->getConnection('core_write');
$newPermissions = ["SUBMIT_ORDER"];

//link permissions with agent roles
foreach ($newPermissions as $code) {
    // Check if permissions already exist
    $sql = "SELECT `entity_id` FROM `role_permission` WHERE `name` LIKE '$code'";
    $result = $conn->fetchAll($sql, [], Zend_Db::FETCH_COLUMN);

    //if permission doesn't exist, create it
    if (sizeof($result) == 0) {
        $conn->insert('role_permission', ['name' => $code]);
    }
}