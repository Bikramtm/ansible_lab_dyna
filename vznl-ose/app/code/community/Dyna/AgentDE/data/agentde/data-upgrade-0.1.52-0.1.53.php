<?php
/**
 * VFDED1W3S-4473
 */


$conn = Mage::getSingleton('core/resource')->getConnection('core_write');

// Check if permissions already exist
$sql = "SELECT `entity_id` FROM `role_permission` WHERE `name` LIKE 'SUBMIT_ORDER'";
$result = $conn->fetchAll($sql, [], Zend_Db::FETCH_COLUMN);

//if permission doesn't exist, create it
if (sizeof($result) == 0) {
    $conn->insert('role_permission', ['name' => $code]);
    $permissionId = $conn->lastInsertId();
} else {
    $permissionId = current($result);
}

// search role id
$roleId = false;
$sqlRole = "SELECT `role_id` FROM `agent_role` WHERE `role` = 'Telesales Agent'";
$resultRole = $conn->fetchAll($sqlRole, [], Zend_Db::FETCH_COLUMN);

if (sizeof($resultRole) != 0) {
    $roleId = current($resultRole);
}

// assign permission to role
if ($permissionId && $roleId) {
    $sql = "SELECT `link_id` FROM `role_permission_link` WHERE `role_id` = ".$roleId." AND `permission_id` = " . $permissionId;
    $result = $conn->fetchAll($sql, [], Zend_Db::FETCH_COLUMN);

    if (sizeof($result) == 0) {
        $conn->insert('role_permission_link', [
            'role_id' => $roleId,
            'permission_id' => $permissionId
        ]);
    }
}