<?php

$installer = $this;

// add permission to SEARCH_CUSTOMER
$permissions = array(
    'SEARCH_CUSTOMER' => 'Agent is able to search a customer',
);

$conn = Mage::getSingleton('core/resource')->getConnection('core_write');
foreach ($permissions as $code => $description) {
    // Check if permissions already exist
    $sql = "SELECT `name` FROM `role_permission` WHERE `name` LIKE '$code'";
    $result = $conn->fetchAll($sql);

    if (sizeof($result) == 0) {
        $conn->insert('role_permission', array('name' => $code, 'description' => $description));
    }
}

$installer->endSetup();