<?php

$installer = $this;

// add permission
$permissionsToAdd = array(
    'CHANGE_SAVED_OFFER_DEALER' => 'Agent can change a saved offer of the same dealer.',
    'DELETE_SAVED_OFFER' => 'Agent can delete any saved offer of a customer',
    'MIGRATION_DSL_TO_CABLE' => 'Agent is able to migrate the customer from DSL to Cable',
    'ONLINE_REPORT' => 'Agent is able to see the online report',
);

$conn = Mage::getSingleton('core/resource')->getConnection('core_write');
foreach ($permissionsToAdd as $code => $description) {
    // Check if permissions already exist
    $sql = "SELECT `name` FROM `role_permission` WHERE `name` LIKE '$code'";
    $result = $conn->fetchAll($sql);

    if (sizeof($result) == 0) {
        $conn->insert('role_permission', array('name' => $code, 'description' => $description));
    }
}

// rename permission MOVE_OFFNET to MOVE_OFFNET_CABLE_TO_DSL
$sql = "SELECT entity_id FROM `role_permission` WHERE `name` LIKE 'MOVE_OFFNET'";
$conn = Mage::getSingleton('core/resource')->getConnection('core_write');
$permissionId = $conn->fetchOne($sql);
if ($permissionId) {
    $conn->update('role_permission', array('name' => 'MOVE_OFFNET_CABLE_TO_DSL'), 'entity_id = "'.$permissionId.'"');
}

// rename permission DELETE_SHOPPING_CART to DELETE_SAVED_SHOPPING_CART
$sql = "SELECT entity_id FROM `role_permission` WHERE `name` LIKE 'DELETE_SHOPPING_CART'";
$conn = Mage::getSingleton('core/resource')->getConnection('core_write');
$permissionId = $conn->fetchOne($sql);
if ($permissionId) {
    $conn->update('role_permission', array('name' => 'DELETE_SAVED_SHOPPING_CART'), 'entity_id = "'.$permissionId.'"');
}

$installer->endSetup();