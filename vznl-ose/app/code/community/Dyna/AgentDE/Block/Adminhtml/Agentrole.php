<?php

/**
 * Class Dyna_AgentDE_Block_Adminhtml_Vodafoneship2stores
 */
class Dyna_AgentDE_Block_Adminhtml_Agentrole extends Dyna_Agent_Block_Adminhtml_Agentrole
{
    public function __construct()
    {
        $this->_controller = "adminhtml_agentrole";
        $this->_blockGroup = "agentde";
        $this->_headerText = Mage::helper("agent")->__("Agent Roles Manager");
        $this->_addButtonLabel = Mage::helper("agent")->__("Add New Agent Role");

        Mage_Adminhtml_Block_Widget_Grid_Container::__construct();
    }
}