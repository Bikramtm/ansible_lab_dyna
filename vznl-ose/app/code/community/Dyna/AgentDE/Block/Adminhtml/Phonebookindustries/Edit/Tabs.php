<?php
/**
 * Class Dyna_AgentDE_Block_Adminhtml_Phonebookindustries_Edit_Tabs
 */
class Dyna_AgentDE_Block_Adminhtml_Phonebookindustries_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();

        $this->setId("phonebookindustries_tabs");
        $this->setDestElementId("edit_form");
        $this->setTitle(Mage::helper("agentde")->__("Phone Book Industry Information"));
    }

    protected function _beforeToHtml()
    {
        $this->addTab("form_section", array(
            "label" => Mage::helper("agentde")->__("Phone Book Industry Information"),
            "title" => Mage::helper("agentde")->__("Phone Book Industry Information"),
            "content" => $this->getLayout()->createBlock("agentde/adminhtml_phonebookindustries_edit_tab_form")->toHtml(),
        ));

        return parent::_beforeToHtml();
    }
}
