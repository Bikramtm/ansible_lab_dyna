<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */


/**
 * Handles additional agent roles for an agent @see VFDED1W3S-1130
 * Class Dyna_AgentDE_Block_Adminhtml_Agent_Edit_Tab_AdditionalRoles
 */
class Dyna_AgentDE_Block_Adminhtml_Agent_Edit_Tab_AdditionalRoles extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $data = Mage::registry("agent_data")->getData();
        $form = new Varien_Data_Form();
        $this->setForm($form);

        $fieldset = $form->addFieldset(
            "agent_form",
            array("legend" => Mage::helper("agent")->__("Business functions"))
        );

        $fieldset->addField("additional_role_ids", "multiselect", array(
            "label" => Mage::helper("agentde")->__("Additional agent roles"),
            "required" => false,
            "name" => "additional_role_ids",
            "values" => $this->getAgentsRoles($data)
        ));

        if (Mage::getSingleton("adminhtml/session")->getAgentData()) {
            $form->setValues(Mage::getSingleton("adminhtml/session")->getAgentData());
            Mage::getSingleton("adminhtml/session")->setAgentData(null);
        } elseif (Mage::registry("agent_data")) {
            $form->setValues(Mage::registry("agent_data")->getData());
        }

        return parent::_prepareForm();
    }

    /**
     * @param array $data
     * @return array
     */
    protected function getAgentsRoles($data)
    {
        $roles = Mage::getModel('agent/agentrole')
            ->getCollection();

        if (isset($data['role_id'])) {
            $roles->addFieldToFilter('role_id', array('neq' => $data['role_id']));
        }

        $options = array();
        foreach ($roles as $role) {
            $options[] = array(
                'value' => $role->getRoleId(),
                'label' => $role->getRole(),
            );
        }

        return $options;
    }
}
