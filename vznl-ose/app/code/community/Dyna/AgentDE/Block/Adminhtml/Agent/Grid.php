<?php

/**
 * Class Dyna_AgentDE_Block_Adminhtml_Agent_Grid
 */
class Dyna_AgentDE_Block_Adminhtml_Agent_Grid extends Dyna_Agent_Block_Adminhtml_Agent_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId("agentGrid");
        $this->setDefaultSort("agent_id");
        $this->setDefaultDir("ASC");
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareColumns()
    {
        $this->addColumn("agent_id", array(
            "header" => Mage::helper("agent")->__("ID"),
            "align" => "right",
            "width" => "50px",
            "type" => "number",
            "index" => "agent_id",
        ));

        $this->addColumn("username", array(
            "header" => Mage::helper("agent")->__("Username"),
            "index" => "username",
        ));

        $this->addColumn("first_name", array(
            "header" => Mage::helper("agent")->__("First Name"),
            "index" => "first_name",
        ));

        $this->addColumn("last_name", array(
            "header" => Mage::helper("agent")->__("Last Name"),
            "index" => "last_name",
        ));

        $this->addColumn("email", array(
            "header" => Mage::helper("agent")->__("Email"),
            "index" => "email",
            'sortable' => true
        ));

        $this->addColumn("phone", array(
            "header" => Mage::helper("agent")->__("Phone"),
            "index" => "phone",
        ));

        $this->addColumn("is_active", array(
            "header" => Mage::helper("agent")->__("Is active"),
            "index" => "is_active",
            'align'     =>'left',
            'type'      => 'options',
            'options'   => array('1' => Mage::helper('adminhtml')->__('Yes'), '0' => Mage::helper('adminhtml')->__('No')),
        ));

        $this->addColumn('role_id', array(
            'header' => Mage::helper('agent')->__('Role ID'),
            'index' => 'role_id',
            'type' => 'options',
            'options' => Dyna_Agent_Block_Adminhtml_Agent_Grid::getOptionArray6(),
        ));

        $this->addColumn('dealer_id', array(
            'header' => Mage::helper('agent')->__('Default Dealer ID'),
            'index' => 'dealer_id',
            'type' => 'options',
            'options' => Dyna_Agent_Block_Adminhtml_Agent_Grid::getOptionArray8(),
        ));

        $this->addColumn("employee_number", array(
            "header" => Mage::helper("agent")->__("Employee number"),
            "index" => "employee_number"
        ));

        $this->addColumn('store_id', array(
            'header' => Mage::helper('agent')->__('Store'),
            'index' => 'store_id',
            'type' => 'options',
            'options' => Mage::getSingleton('adminhtml/system_store')->getStoreOptionHash(false),
        ));

        $this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

        return $this;
    }
}
