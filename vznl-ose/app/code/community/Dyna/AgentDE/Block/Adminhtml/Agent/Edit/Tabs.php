<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Agent_Block_Adminhtml_Agent_Edit_Tabs
 */
class Dyna_AgentDE_Block_Adminhtml_Agent_Edit_Tabs extends Dyna_LoginSecurity_Block_Menuagent
{
    public function __construct()
    {
        parent::__construct();

        $this->setId("agent_tabs");
        $this->setDestElementId("edit_form");
        $this->setTitle(Mage::helper("agent")->__("Agent Information"));
    }

    protected function _beforeToHtml()
    {
        $tabs = [
            "form_section" => array(
                "label" => Mage::helper("agent")->__("Agent Information"),
                "title" => Mage::helper("agent")->__("Agent Information"),
                "content" => $this->getLayout()->createBlock("agent/adminhtml_agent_edit_tab_form")->toHtml(),
                'active' => true
            ),
            'sales_id_section' => array(
                'label' => Mage::helper('catalog')->__('Commision details'),
                'url'   => $this->getUrl('agentdeadmin/adminhtml_agent/commision', array('_current' => true)),
                'class' => 'ajax',
            ),
        ];

        foreach ($tabs as $tabKey => $tabValue) {
            $this->addTab($tabKey, $tabValue);
        }

        $agentData = Mage::registry("agent_data");
        // Impersonate agent has been changed into impersonate role. Leaving agent impersonation temporarily disabled
        // @see VFDED1W3S-1130
        if (false && $agentData && $agentData->isGranted(Dyna_AgentDE_Model_Agent::AGENT_ROLE_PERMISSION_IMPERSONATE)) {
            $this->addTab("impersonate_details", array(
                "label" => Mage::helper("agent")->__("Impersonate"),
                "title" => Mage::helper("agent")->__("Impersonate"),
                "content" => $this->getLayout()->createBlock("agentde/adminhtml_agent_edit_tab_impersonate")->toHtml(),
                "active" => false
            ));
        }

        $this->addTab("impersonate_role", array(
            "label" => Mage::helper("agent")->__("Business functions"),
            "title" => Mage::helper("agent")->__("Business functions"),
            "content" => $this->getLayout()->createBlock("agentde/adminhtml_agent_edit_tab_additionalRoles")->toHtml(),
            "active" => false
        ));

        return parent::_beforeToHtml();
    }

}
