<?php
/**
 * Class Dyna_AgentDE_Block_Adminhtml_Vodafoneship2stores_Edit
 */
class Dyna_AgentDE_Block_Adminhtml_Vodafoneship2stores_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();

        $this->_objectId = "id";
        $this->_blockGroup = "agentde";
        $this->_controller = "adminhtml_vodafoneship2stores";
        $this->_updateButton("save", "label", Mage::helper("agent")->__("Save Vodafone ship to store"));
        $this->_updateButton("delete", "label", Mage::helper("agent")->__("Delete Vodafone ship to store"));

        $this->_addButton("saveandcontinue", array(
            "label" => Mage::helper("agentde")->__("Save And Continue Edit"),
            "onclick" => "saveAndContinueEdit()",
            "class" => "save",
        ), -100);

        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
            ";
    }

    public function getHeaderText()
    {
        if (Mage::registry("vodafoneship2stores_data") && Mage::registry("vodafoneship2stores_data")->getId()) {
            return Mage::helper("agentde")->__("Edit Vodafone ship to store '%s'",
                $this->htmlEscape(Mage::registry("vodafoneship2stores_data")->getId()));
        } else {
            return Mage::helper("agentde")->__("Add Vodafone ship to store");
        }
    }
}