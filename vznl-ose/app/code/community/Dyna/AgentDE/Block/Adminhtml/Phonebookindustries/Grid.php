<?php

/**
 * Class Dyna_AgentDE_Block_Adminhtml_Phonebookindustries_Grid
 */
class Dyna_AgentDE_Block_Adminhtml_Phonebookindustries_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId("phonebookindustriesGrid");
        $this->setDefaultSort("entity_id");
        $this->setDefaultDir("ASC");
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel("agentde/phoneBookIndustries")->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $columns = [
            "name" => array(
                "header" => Mage::helper("agentde")->__("Name"),
                "index" => "name",
            ),
        ];


        foreach ($columns as $columnKey => $columnValue) {
            $this->addColumn($columnKey, $columnValue);
        }

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl("*/*/edit", array("id" => $row->getId()));
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('entity_ids');
        $this->getMassactionBlock()->setUseSelectAll(true);
        $this->getMassactionBlock()->addItem('remove_phonebookindustriess', array(
            'label' => Mage::helper('agentde')->__('Remove Phone Book Industry(s)'),
            'url' => $this->getUrl('*/adminhtml_phonebookindustries/massRemove'),
            'confirm' => Mage::helper('agent')->__('Are you sure?')
        ));
        return $this;
    }
}