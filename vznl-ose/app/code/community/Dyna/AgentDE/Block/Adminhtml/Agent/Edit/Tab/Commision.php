<?php

/**
 * Class Dyna_AgentDE_Block_Adminhtml_Dealer_Grid
 */
class Dyna_AgentDE_Block_Adminhtml_Agent_Edit_Tab_Commision extends Mage_Adminhtml_Block_Widget_Grid
{
    /** @var int */
    protected $agentId;

    public function __construct()
    {
        parent::__construct();
        $this->setId("agentSalesidGrid");
        $this->setUseAjax(true);

        $this->setDefaultSort("type");
        $this->setDefaultDir("ASC");
    }

    /**
     * @param int $agentId
     */
    public function setAgentId(int $agentId)
    {
        $this->agentId = $agentId;
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        /** @var Dyna_AgentDE_Model_Mysql4_Commision_Collection $collection */
        $commissionCollection = Mage::getModel("agentde/commision")->getCollection();
        if ($this->agentId) {
            $commissionCollection->addFieldToFilter('agent_id', $this->agentId);
        }

        $commissionData = [];
        foreach ($commissionCollection as $commissionRow) {
            $commissionData[$commissionRow->getImportLine()][$commissionRow->getAgentId()][$commissionRow->getType()] = $commissionRow->getSalesId();
        }

        $adminCollection = new Varien_Data_Collection();
        foreach ($commissionData as $commissionAgentRow) {
            $commissionAgentObject = new Varien_Object();
            foreach($commissionAgentRow as $commissionAgentId => $commissionAgentStack) {
                $commissionAgentObject->setData($commissionAgentStack);
                $commissionAgentObject->setData('agent_id', $commissionAgentId);
            }
            $adminCollection->addItem($commissionAgentObject);
        }

        $this->setCollection($adminCollection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn("agent_id", array(
            "header" => Mage::helper("agent")->__("Agent id"),
            "index" => "agent_id",
        ));

        foreach (Dyna_AgentDE_Model_Commision::ALLOWED_PRODUCT_TYPES as $type) {
            $this->addColumn($type, array(
                "header" => Mage::helper("agent")->__($type),
                "index" => $type,
            ));
        }

        return parent::_prepareColumns();
    }

    /**
     * Rerieve grid URL
     *
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('agentdeadmin/adminhtml_agent/commision', array('_current' => true));
    }
}