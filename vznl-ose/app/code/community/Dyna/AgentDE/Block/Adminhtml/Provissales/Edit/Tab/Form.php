<?php

/**
 * Class Dyna_AgentDE_Block_Adminhtml_Provissales_Edit_Tab_Form
 */
class Dyna_AgentDE_Block_Adminhtml_Provissales_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset("agent_form", array("legend" => Mage::helper("agent")->__("Provis Sales Information")));


        $fields = [
            array(
                "name" => "red_sales_id",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("agentde")->__("Red sales id"),
                    "class" => "required-entry",
                    "required" => true,
                    "name" => "red_sales_id",
                )
            ),
            array(
                "name" => "status",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("agentde")->__("Status"),
//                    "class" => "required-entry",
                    "required" => false,
                    "name" => "status",
                )
            ),
            array(
                "name" => "level",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("agentde")->__("Level"),
                    "class" => "required-entry",
                    "required" => true,
                    "name" => "level",
                )
            ),
            array(
                "name" => "sales_channel",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("agentde")->__("Sales channel"),
                    "class" => "required-entry",
                    "required" => true,
                    "name" => "sales_channels",
                )
            ),
            array(
                "name" => "sales_subchannel",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("agentde")->__("Sales subchannel"),
                    "class" => "required-entry",
                    "required" => true,
                    "name" => "sales_subchannel",
                )
            ),
            array(
                "name" => "classification",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("agentde")->__("Classification"),
                    "class" => "required-entry",
                    "required" => true,
                    "name" => "classification",
                )
            ),
            array(
                "name" => "red_sales_id_level-0",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("agentde")->__("Red sales id level 0"),
                    "class" => "required-entry",
                    "required" => true,
                    "name" => "red_sales_id_level-0",
                )
            ),
            array(
                "name" => "red_sales_id_level-1",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("agentde")->__("Red sales id level 1"),
                    "class" => "required-entry",
                    "required" => true,
                    "name" => "red_sales_id_level-1",
                )
            ),
            array(
                "name" => "red_sales_id_level-2",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("agentde")->__("Red sales id level 2"),
                    "class" => "required-entry",
                    "required" => true,
                    "name" => "red_sales_id_level-2",
                )
            ),
            array(
                "name" => "blue_sales_id",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("agentde")->__("Blue sales id"),
                    "class" => "required-entry",
                    "required" => true,
                    "name" => "blue_sales_id",
                )
            ),
            array(
                "name" => "yellow_sales_id",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("agentde")->__("Yellow sales id"),
                    "class" => "required-entry",
                    "required" => true,
                    "name" => "yellow_sales_id",
                )
            ),
            array(
                "name" => "blue_top_vo",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("agentde")->__("Blue top vo"),
                    "class" => "required-entry",
                    "required" => true,
                    "name" => "blue_top_vo",
                )
            ),
        ];

        foreach ($fields as $field) {
            $fieldset->addField($field["name"], $field["type"], $field["parameters"]);
        }

        if (Mage::getSingleton("adminhtml/session")->getProvissalesData()) {
            $form->setValues(Mage::getSingleton("adminhtml/session")->getDealergroupData());
            Mage::getSingleton("adminhtml/session")->setProvissalesData(null);
        } elseif (Mage::registry("provissales_data")) {
            $form->setValues(Mage::registry("provissales_data")->getData());
        }

        return parent::_prepareForm();
    }
}
