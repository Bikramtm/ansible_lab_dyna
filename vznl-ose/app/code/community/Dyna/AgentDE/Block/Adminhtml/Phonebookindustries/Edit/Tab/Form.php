<?php

/**
 * Class Dyna_AgentDE_Block_Adminhtml_Phonebookindustries_Edit_Tab_Form
 */
class Dyna_AgentDE_Block_Adminhtml_Phonebookindustries_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset("agent_form", array("legend" => Mage::helper("agent")->__("Phone Book Industry Information")));


        $fields = [
            array(
                "name" => "name",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("agentde")->__("Name"),
                    "class" => "required-entry",
                    "required" => true,
                    "name" => "name",
                )
            ),
        ];

        foreach ($fields as $field) {
            $fieldset->addField($field["name"], $field["type"], $field["parameters"]);
        }

        if (Mage::getSingleton("adminhtml/session")->getPhonebookindustriesData()) {
            $form->setValues(Mage::getSingleton("adminhtml/session")->getDealergroupData());
            Mage::getSingleton("adminhtml/session")->setPhonebookindustriesData(null);
        } elseif (Mage::registry("phonebookindustries_data")) {
            $form->setValues(Mage::registry("phonebookindustries_data")->getData());
        }

        return parent::_prepareForm();
    }
}
