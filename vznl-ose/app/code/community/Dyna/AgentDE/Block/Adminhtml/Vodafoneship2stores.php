<?php

/**
 * Class Dyna_AgentDE_Block_Adminhtml_Vodafoneship2stores
 */
class Dyna_AgentDE_Block_Adminhtml_Vodafoneship2stores extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = "adminhtml_vodafoneship2stores";
        $this->_blockGroup = "agentde";
        $this->_headerText = Mage::helper("agentde")->__("Vodafone ship to stores");

        parent::__construct();
        $this->_removeButton('add');
    }
}