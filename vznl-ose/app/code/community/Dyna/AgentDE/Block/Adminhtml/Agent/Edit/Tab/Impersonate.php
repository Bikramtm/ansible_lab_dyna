<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Agent_Block_Adminhtml_Agent_Edit_Tab_Form
 */
class Dyna_AgentDE_Block_Adminhtml_Agent_Edit_Tab_Impersonate extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $data = Mage::registry("agent_data")->getData();
        $form = new Varien_Data_Form();
        $this->setForm($form);

        $fieldset = $form->addFieldset(
            "agent_form",
            array("legend" => Mage::helper("agent")->__("Impersonation"))
        );

        $fieldset->addField("impersonate_ids", "multiselect", array(
            "label" => Mage::helper("agentde")->__("Impersonate"),
            "required" => false,
            "name" => "impersonate_ids",
            "values" => $this->getAgentsOptions($data)
        ));

        if (Mage::getSingleton("adminhtml/session")->getAgentData()) {
            $form->setValues(Mage::getSingleton("adminhtml/session")->getAgentData());
            Mage::getSingleton("adminhtml/session")->setAgentData(null);
        } elseif (Mage::registry("agent_data")) {
            $form->setValues(Mage::registry("agent_data")->getData());
        }

        return parent::_prepareForm();
    }

    protected function getAgentsOptions($data)
    {
        $agents = Mage::getModel('agent/agent')
            ->getCollection();
        $agents->addFieldToFilter('agent_id', array("neq" => $data['agent_id']));

        $options = array();
        foreach ($agents as $agent) {
            $options[] = array(
                'value' => $agent->getId(),
                'label' => $agent->getFirstName()." ".$agent->getLastName(),
            );
        }

        return $options;
    }
}
