<?php

/**
 * Class Dyna_AgentDE_Block_Adminhtml_Provissales
 */
class Dyna_AgentDE_Block_Adminhtml_Provissales extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = "adminhtml_provissales";
        $this->_blockGroup = "agentde";
        $this->_headerText = Mage::helper("agentde")->__("Provis sales");

        parent::__construct();
        $this->_removeButton('add');
    }
}