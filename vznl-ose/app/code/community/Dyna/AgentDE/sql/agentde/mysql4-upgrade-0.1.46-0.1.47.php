<?php

/**
 *  Installer for updating agent table field properties:
 *  username: changing it to TEXT
 *  password: changing it to TEXT
 *  ngumid: changing it to TEXT
 *  email: changing it to TEXT
 *  externalmail: changing it to TEXT
 *  partnermanager: changing it to TEXT
 *  reportingto: changing it to TEXT
 *  orgunitcode: changing it to TEXT
 *  orgunitlong: changing it to TEXT
 *  location: changing it to TEXT
 *  workplace: changing it to TEXT
 */

/** @var Mage_Catalog_Model_Resource_Setup $this */

$this->startSetup();

$this->getConnection()->modifyColumn($this->getTable("agent"), "username", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT
], false);

$this->getConnection()->modifyColumn($this->getTable("agent"), "password", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT
], false);


$this->getConnection()->modifyColumn($this->getTable("agent"), "ngumid", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT
], false);


$this->getConnection()->modifyColumn($this->getTable("agent"), "email", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT
], false);

$this->getConnection()->modifyColumn($this->getTable("agent"), "externalmail", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT
], false);

$this->getConnection()->modifyColumn($this->getTable("agent"), "partnermanager", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT
], false);

$this->getConnection()->modifyColumn($this->getTable("agent"), "reportingto", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT
], false);

$this->getConnection()->modifyColumn($this->getTable("agent"), "orgunitcode", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT
], false);

$this->getConnection()->modifyColumn($this->getTable("agent"), "orgunitlong", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT
], false);

$this->getConnection()->modifyColumn($this->getTable("agent"), "location", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT
], false);

$this->getConnection()->modifyColumn($this->getTable("agent"), "workplace", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT
], false);

/**
 *  Installer for updating dealer table field properties:
 *  street: changing it to TEXT
 */
$this->getConnection()->modifyColumn($this->getTable("dealer"), "street", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT
]);


/**
 *  Installer for updating dealer_group table field properties:
 *  name: changing it to TEXT
 */
$this->getConnection()->modifyColumn($this->getTable("dealer_group"), "name", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT
], false);


$this->endSetup();