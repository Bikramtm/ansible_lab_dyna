<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

$connection = $installer->getConnection();
$impersonateTableName = 'agent_impersonate';

$impersonateFKPrimayKey = 'fk_agent_primary_id';
$impersonateFKImpersonateKey = 'fk_agent_impersonate_id';
$impersonateUniqueIndex = 'IDX_IMPERSONATE_LEFT_RIGHT_UNIQUE';


// if table doesn't exist, then create
if (!$installer->getConnection()->isTableExists($impersonateTableName)) {
    $impersonateTable = new Varien_Db_Ddl_Table();
    $impersonateTable->setName($impersonateTableName);

    $impersonateTable
        ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, [
            "unsigned" => true,
            "primary" => true,
            "auto_increment" => true,
            "nullable" => false,
        ], "Primary key")
        ->addColumn(
            'agent_id',
            Varien_Db_Ddl_Table::TYPE_INTEGER
        )
        ->addColumn(
            'impersonate_id',
            Varien_Db_Ddl_Table::TYPE_INTEGER
        )
        ->addForeignKey(
            "{$impersonateFKPrimayKey}",
            'agent_id',
            'agent',
            'agent_id'
        )
        ->addForeignKey(
            "{$impersonateFKImpersonateKey}",
            'impersonate_id',
            'agent',
            'agent_id'
        )
    ;

    $connection->createTable($impersonateTable);

    $connection->addIndex(
        $impersonateTableName,
        $impersonateUniqueIndex,
        array('agent_id', 'impersonate_id'),
        Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
    );

    // add impersonation privileges
    $permissionsToAdd = array(
        'IMPERSONATE' => 'Agent can impersonate another agent.'
    );

    foreach ($permissionsToAdd as $code => $description) {
        // Check if permissions already exist
        $sql    = "SELECT `name` FROM `role_permission` WHERE `name` LIKE '$code'";
        $result = $connection->fetchAll($sql);

        if (sizeof($result) == 0) {
            $connection->insert('role_permission', array('name' => $code, 'description' => $description));
        }
    }
}

$installer->endSetup();
