<?php
/** @var Mage_Eav_Model_Entity_Setup $this */
$installer = $this;
$installer->startSetup();

$table = $this->getTable('provis_sales_id');

/** @var Magento_Db_Adapter_Pdo_Mysql $connection */
$connection = $installer->getConnection();

if ($connection->isTableExists("provis_sales_id")) {
    $connection->changeColumn($table, 'red_sales_id', 'red_sales_id', 'VARCHAR(8) NOT NULL');
    $connection->changeColumn($table, 'status', 'status', 'VARCHAR(2) NOT NULL');
    $connection->changeColumn($table, 'sales_channel', 'sales_channel', 'VARCHAR(2) NOT NULL');
    $connection->changeColumn($table, 'sales_subchannel', 'sales_subchannel', 'VARCHAR(2) NOT NULL');
    $connection->changeColumn($table, 'classification', 'classification', 'VARCHAR(2) NOT NULL');
    $connection->changeColumn($table, 'red_sales_id_level-0', 'red_sales_id_level-0', 'VARCHAR(8) NULL');
    $connection->changeColumn($table, 'red_sales_id_level-1', 'red_sales_id_level-1', 'VARCHAR(8) NULL');
    $connection->changeColumn($table, 'red_sales_id_level-2', 'red_sales_id_level-2', 'VARCHAR(8) NULL');
    $connection->changeColumn($table, 'blue_sales_id', 'blue_sales_id', 'VARCHAR(8) NOT NULL');
    $connection->changeColumn($table, 'yellow_sales_id', 'yellow_sales_id', 'VARCHAR(8) NOT NULL');
    $connection->changeColumn($table, 'blue_top_vo', 'blue_top_vo', 'VARCHAR(8) NULL');
    $installer->run('ALTER TABLE provis_sales_id MODIFY level INT(1) UNSIGNED NOT NULL');
}
// update red_sales_id as well for agent
$table = $this->getTable('agent');

if ($connection->isTableExists("agent")) {
    $connection->changeColumn($table, 'red_sales_id', 'red_sales_id', 'VARCHAR(50)');
}

$installer->endSetup();