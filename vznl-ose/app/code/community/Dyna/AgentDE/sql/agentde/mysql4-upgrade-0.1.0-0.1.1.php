<?php

/** @var Mage_Catalog_Model_Resource_Setup $this */
$installer = $this;
$installer->startSetup();
if ($installer->tableExists($installer->getTable('dealer_allowed_campaigns'))) {
    $installer->getConnection()
        ->addIndex(
            $installer->getTable('dealer_allowed_campaigns'),
            $installer->getIdxName('dealer_allowed_campaigns', array('dealer_id', 'campaign_id'), Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE),
            array('dealer_id', 'campaign_id'),
            Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
        );
}

$installer->endSetup();
