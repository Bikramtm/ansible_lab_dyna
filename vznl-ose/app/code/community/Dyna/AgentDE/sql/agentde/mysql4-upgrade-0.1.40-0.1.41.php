<?php

/**
 *  Installer for adding one field:
 *  other_red_sales_ids: this will hold all the other red sales IDs for agents
 */

/** @var Mage_Catalog_Model_Resource_Setup $this */

$this->startSetup();

//adding the other_red_sales_ids field to store multiple sales ids from the UM API
$this->getConnection()->addColumn($this->getTable("agent"), "other_red_sales_ids", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "nullable" => true,
    "default"  => null,
    "after" => "red_sales_id",
    "comment" => "Other red sales IDs",
]);

$this->endSetup();