<?php

/** @var Mage_Catalog_Model_Resource_Setup $this */

$this->startSetup();
$this->getConnection()
    ->addColumn($this->getTable('agent'),
        'fraud',
        [
            'type' => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
            'length' => 1,
            'nullable' => true,
            'default' => null,
            "after" => "login_attempts",
            'comment' => 'Fraud',
        ]
    );
$this->endSetup();
