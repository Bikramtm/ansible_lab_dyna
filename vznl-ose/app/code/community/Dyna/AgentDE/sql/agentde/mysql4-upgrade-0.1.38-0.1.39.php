<?php
/** @var Mage_Eav_Model_Entity_Setup $this */
$installer = $this;
$installer->startSetup();

/** @var Magento_Db_Adapter_Pdo_Mysql $connection */
$connection = $installer->getConnection();

if ($connection->isTableExists("role_permission")) {
    $permissions = array(
        'PREPAID_TO_POSTPAID' => 'The permission controls for which agent it shall be allowed to change from prepaid to postpaid',
        'PREPAID_TO_RED+' => 'The permission controls for which agent it shall be allowed to change from prepaid to Red+ member',
        'RENEW_PACKAGE' => 'The permission controls for which agent it shall be allowed to renew a package',
        'POSTPAID_TO_RED+' => 'The permission controls for which agent it shall be allowed to move to Red+ member',
    );

    foreach ($permissions as $code => $description) {
        $connection->insert('role_permission', array('name' => $code, 'description' => $description));
    }
}

$installer->endSetup();