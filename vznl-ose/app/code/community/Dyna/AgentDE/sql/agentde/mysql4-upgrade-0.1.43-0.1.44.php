<?php

/** @var Mage_Catalog_Model_Resource_Setup $this */

$installer = $this;
$installer->startSetup();

$connection = $installer->getConnection();

// Create dyna_phone_book_keywords table
$keywordsTable = new Varien_Db_Ddl_Table();
$keywordsTable->setName('dyna_phone_book_keywords');

$dropSQL = "DROP TABLE IF EXISTS " . $keywordsTable->getName();
$this->run($dropSQL);

$keywordsTable
    ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_BIGINT, 10, [
        "unsigned" => true,
        "primary" => true,
        "auto_increment" => true,
        "nullable" => false,
    ], "Primary key for dyna_phone_book_keywords table");
$keywordsTable
    ->addColumn(
        'name',
        Varien_Db_Ddl_Table::TYPE_VARCHAR,
        255,
        [
            "nullable" => false,
        ]
    );
$this->getConnection()->createTable($keywordsTable);

// Create dyna_phone_book_industries table
$industriesTable = new Varien_Db_Ddl_Table();
$industriesTable->setName('dyna_phone_book_industries');

$dropSQL = "DROP TABLE IF EXISTS " . $industriesTable->getName();
$this->run($dropSQL);

$industriesTable
    ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_BIGINT, 10, [
        "unsigned" => true,
        "primary" => true,
        "auto_increment" => true,
        "nullable" => false,
    ], "Primary key for dyna_phone_book_industries table");
$industriesTable
    ->addColumn(
        'name',
        Varien_Db_Ddl_Table::TYPE_VARCHAR,
        255,
        [
            "nullable" => false,
        ]
    );
$this->getConnection()->createTable($industriesTable);

$this->endSetup();