<?php

/**
 *  Installer for updating agent table field properties:
 *  usertype: changing it to VARCHAR 64
 *  username: changing it to VARCHAR 255
 *  password: changing it to VARCHAR 255
 *  firstname: changing it to VARCHAR 64
 *  lastname: changing it to VARCHAR 64
 *  ngumid: changing it to VARCHAR 255
 *  red_sales_id: changing it to VARCHAR 64
 *  phone: changing it to VARCHAR 64 so it can handle externalphone value.
 *  email: changing it to VARCHAR 255
 *  externalmail: changing it to VARCHAR 64
 *  partnermanager: changing it to VARCHAR 255
 *  reportingto: changing it to VARCHAR 255
 *  orgunitcode: changing it to VARCHAR 255
 *  orgunitlong: changing it to VARCHAR 255
 *  location: changing it to VARCHAR 255
 *  workplace: changing it to VARCHAR 255
 */

/** @var Mage_Catalog_Model_Resource_Setup $this */

$this->startSetup();

$this->getConnection()->modifyColumn($this->getTable("agent"), "usertype", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 64
], false);

$this->getConnection()->modifyColumn($this->getTable("agent"), "username", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 255
], false);

$this->getConnection()->modifyColumn($this->getTable("agent"), "password", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 255
], false);

$this->getConnection()->modifyColumn($this->getTable("agent"), "first_name", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 64
], false);

$this->getConnection()->modifyColumn($this->getTable("agent"), "last_name", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 64
], false);

$this->getConnection()->modifyColumn($this->getTable("agent"), "ngumid", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 255
], false);

$this->getConnection()->modifyColumn($this->getTable("agent"), "red_sales_id", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 64
], false);

$this->getConnection()->modifyColumn($this->getTable("agent"), "phone", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 64
], false);

$this->getConnection()->modifyColumn($this->getTable("agent"), "email", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 255
], false);

$this->getConnection()->modifyColumn($this->getTable("agent"), "externalmail", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 64
], false);

$this->getConnection()->modifyColumn($this->getTable("agent"), "partnermanager", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 255
], false);

$this->getConnection()->modifyColumn($this->getTable("agent"), "reportingto", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 255
], false);

$this->getConnection()->modifyColumn($this->getTable("agent"), "orgunitcode", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 255
], false);

$this->getConnection()->modifyColumn($this->getTable("agent"), "orgunitlong", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 255
], false);

$this->getConnection()->modifyColumn($this->getTable("agent"), "location", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 255
], false);

$this->getConnection()->modifyColumn($this->getTable("agent"), "workplace", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 255
], false);

/**
 *  Installer for updating dealer table field properties:
 *  street: changing it to VARCHAR 255
 *  postcode: changing it to VARCHAR 64
 *  city: changing it to VARCHAR 64
 */

$this->getConnection()->modifyColumn($this->getTable("dealer"), "street", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 255
]);

$this->getConnection()->modifyColumn($this->getTable("dealer"), "postcode", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 64
]);

$this->getConnection()->modifyColumn($this->getTable("dealer"), "city", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 64
]);

/**
 *  Installer for updating dealer_group table field properties:
 *  name: changing it to VARCHAR 255
 */

$this->getConnection()->modifyColumn($this->getTable("dealer_group"), "name", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 255
], false);


$this->endSetup();