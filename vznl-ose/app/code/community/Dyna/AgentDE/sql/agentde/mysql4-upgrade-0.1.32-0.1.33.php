<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $this Mage_Core_Model_Resource_Setup */

$this->startSetup();

$this->getConnection()->addColumn($this->getTable('agent/agent'), 'additional_role_ids', array(
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'nullable' => true,
    'default' => null,
    "after" => "role_id",
    'comment' => 'Additional role ids',
));

$this->endSetup();
