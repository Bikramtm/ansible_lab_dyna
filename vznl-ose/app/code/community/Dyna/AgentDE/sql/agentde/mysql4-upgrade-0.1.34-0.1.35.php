<?php

/** @var Mage_Catalog_Model_Resource_Setup $this */

$this->startSetup();
$this->getConnection()
    ->addColumn($this->getTable('agent'),
        'red_sales_id',
        [
            'type' => Varien_Db_Ddl_Table::TYPE_BIGINT,
            'length' => 10,
            'nullable' => true,
            'default' => null,
            'comment' => 'Red Sales Id is used as a matching key in Provis import in order to retrieve Blue and Yellow Sales Id',
        ]
    );
$this->endSetup();
