<?php
$installer = $this;

$html = "<div class=\"page-title\"><h1>404 Page Not Found...</h1></div>";

$pageId = $collection = Mage::getModel('cms/page')->getCollection()->addFieldToFilter('identifier', 'no-route')->getFirstItem()->getId();
$page = Mage::getModel('cms/page')->load($pageId);

$cmsPageData = array(
    'page_id' => $pageId,
    'title' => '404 Not Found',
    'root_template' => 'empty',
    'meta_keywords' => '',
    'meta_description' => '',
    'identifier' => 'no-route',
    'content_heading' => '',
    'stores' => array(0),//available for all store views
    'content' => $html,
);
Mage::getModel('cms/page')->setData($cmsPageData)->save();


$installer->endSetup();

