<?php

/** @var Mage_Catalog_Model_Resource_Setup $this */

$this->startSetup();
$this->getConnection()->addColumn($this->getTable("agent"), "digest_auth_hash", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 50,
    "after" => "dealer_code",
    "comment" => "Digest Auth hash",
]);

$this->endSetup();
