<?php

/**
 * Class Dyna_AgentDE_Model_Import_ProvisSalesIds
 */
class Dyna_AgentDE_Model_Import_ProvisSalesIds extends Omnius_Import_Model_Import_ImportAbstract
{
    /** As per IDD v1.10 */
    const NO_ERROR = 0;
    const TEHNICAL_ERROR = 1;
    const PROVIS_ERROR = 2;

    protected $_data;
    protected $_csvDelimiter = ';';
    protected $_header;
    protected $_rowNumber = 0;
    protected $_logFileName = "provis_sales_ids_import";
    protected $_logFileExtension = "log";

    /** @var Dyna_Import_Helper_Data $_helper */
    public $_helper;

    /** This array position must be the position in file */
    protected $_headerColumns = [
        'red_sales_id',
        'status',
        'level',
        'sales_channel',
        'sales_subchannel',
        'classification',
        'red_sales_id_level-0',
        'red_sales_id_level-1',
        'red_sales_id_level-2',
        'blue_sales_id',
        'yellow_sales_id',
        'blue_top_vo',
    ];

    protected $_tableSchema = [];

    /** @var int $_errorCode */
    public $_errorCode = self::NO_ERROR;

    /** @var array $_alreadySavedRedSalesIds */
    protected $_alreadySavedRedSalesIds = [];

    /** @var Varien_Db_Adapter_Interface $writeAdapter */
    protected $writeAdapter;

    /** @var  boolean $skipped */
    protected $skipped;

    public function __construct()
    {
        parent::__construct();
        $this->_helper->setImportLogFile($this->_logFileName . '.' . $this->_logFileExtension);

        $this->writeAdapter = Mage::getSingleton('core/resource')->getConnection('core_write');

        $query = "SELECT COLUMN_NAME, DATA_TYPE, COLUMN_TYPE 
                  FROM information_schema.columns 
                  WHERE table_schema ='" . Mage::getConfig()->getResourceConnectionConfig("default_setup")->dbname . "' AND table_name = 'provis_sales_id'";
        $tableData = $this->writeAdapter->fetchAll($query);
        foreach ($tableData as $row) {
            if (in_array($row['COLUMN_NAME'], $this->_headerColumns)) {
                $matches = [];
                $this->_tableSchema[$row['COLUMN_NAME']]['type'] = $row['DATA_TYPE'];
                preg_match_all("/\(([^\)]*)\)/", $row['COLUMN_TYPE'], $matches);
                if ($matches[1]) {
                    $this->_tableSchema[$row['COLUMN_NAME']]['length'] = $matches[1][0];
                }
            }
        }
    }

    public function process($data)
    {
        $this->_data = [];
        $this->skipped = false;
        $this->_rowNumber++;

        try {
            // check row
            if (count($this->_headerColumns) == count($data)) {
                $this->_data = array_combine($this->_headerColumns, $data);
            } elseif (count($this->_headerColumns) < count($data)) {
                $this->skipRow("In line " . $this->_rowNumber . ", the record has too many fields ([" . count($data) . "]) instead of " . count($this->_headerColumns), self::PROVIS_ERROR);
            } else {
                $this->skipRow("In line " . $this->_rowNumber . ", the record has not enough fields ([" . count($data) . "]) instead of " . count($this->_headerColumns), self::PROVIS_ERROR);
            }

            // check mandatory fields
            $missingKeys = [];
            foreach ($this->_helper->getRequiredColumns('provisSalesId') as $key) {
                if (!isset($this->_data[$key]) || empty($this->_data[$key])) {
                    if ($this->_tableSchema[$key]['type'] == 'int' && $this->_data[$key] == '0') {
                        continue;
                    }
                    $missingKeys[] = $key;
                }
            }
            if (count($missingKeys)) {
                $this->skipRow("In line " . $this->_rowNumber . ", the mandatory attributes [" . implode(',', $missingKeys) . "] are missing.", self::PROVIS_ERROR);
            }

            // check field values
            if (!empty($this->_tableSchema)) {
                foreach ($this->_headerColumns as $column) {
                    switch ($this->_tableSchema[$column]['type']) {
                        case 'varchar':
                            if (strlen($this->_data[$column]) > $this->_tableSchema[$column]['length']) {
                                $this->skipRow("In line " . $this->_rowNumber . ", the record does not match with the defined data-length [" . $this->_tableSchema[$column]['length'] . "] for attribute [" . $column . "]", self::PROVIS_ERROR);
                            }
                            break;
                        case 'int':
                            if (!ctype_digit($this->_data[$column])) {
                                $this->skipRow("In line " . $this->_rowNumber . ", the record does not match with the defined data-type [" . $this->_tableSchema[$column]['type'] . "] for attribute [" . $column . "]", self::PROVIS_ERROR);
                            } elseif (strlen($this->_data[$column]) > $this->_tableSchema[$column]['length']) {
                                $this->skipRow("In line " . $this->_rowNumber . ", the record does not match with the defined data-length [" . $this->_tableSchema[$column]['length'] . "] for attribute [" . $column . "]", self::PROVIS_ERROR);
                            }
                            break;
                    }
                }
            }

            // check Duplicated red Sales ID
            if (in_array($this->_data['red_sales_id'], $this->_alreadySavedRedSalesIds)) {
                $this->skipRow("In line " . $this->_rowNumber . ", the red_sales_id [" . $this->_data['red_sales_id'] . "] is present while it was already imported.", self::TEHNICAL_ERROR);
            } elseif (!$this->skipped) {
                $this->saveProvisSalesId();
            }

            $this->_totalFileRows++;
        } catch (Exception $e) {
            $this->skipRow("[ERROR] Skipped row because: " . $e->getMessage(), self::PROVIS_ERROR);
            fwrite(STDERR, "[ERROR] Skipped row because :" . $e->getMessage());
        }
    }

    /**
     * Save current provis
     * @return int
     * @throws Exception
     */
    public function saveProvisSalesId()
    {
        /** @var Dyna_AgentDE_Model_ProvisSales $model */
        $model = Mage::getModel('agentde/provisSales');
        $model->addData($this->_data);
        $model->save();
        $this->_alreadySavedRedSalesIds[] = $model->getRedSalesId();
    }

    private function skipRow($msg, $errorCode)
    {
        if (!$this->skipped) {
            $this->skipped = true;
            $this->_skippedFileRows++;
        }
        $this->_helper->logMsg($msg, false);

        if ($errorCode != self::TEHNICAL_ERROR) {
            $this->_errorCode = $errorCode;
        }
    }

    /**
     * Log function (if debug is disabled)
     *
     * @param string $msg
     * @param bool $verbose
     */
    public function _log($msg, $verbose = false)
    {
        if (!$this->_debug || ($verbose && $this->_debug)) {
            $this->_helper->logMsg($msg);
        }
    }

    public function beginTransaction()
    {
        $this->writeAdapter->beginTransaction();
    }

    public function rollBack()
    {
        $this->writeAdapter->rollBack();
    }

    public function commit()
    {
        $this->writeAdapter->commit();
    }

    public function deletePreviousProvisSalesIds()
    {
        $this->writeAdapter->delete('provis_sales_id');
    }
}