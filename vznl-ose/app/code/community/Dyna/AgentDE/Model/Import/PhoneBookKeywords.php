<?php

/**
 * Class Dyna_AgentDE_Model_Import_PhoneBookKeywords
 */
class Dyna_AgentDE_Model_Import_PhoneBookKeywords extends Omnius_Import_Model_Import_ImportAbstract
{
    const NUMBER_OF_COLUMNS = 1;
    protected $_data;
    protected $_headerColumns;
    protected $_csvDelimiter = '|';
    protected $_header;
    protected $_rowNumber = 1;
    protected $_logFileName = "phone_book_keywords_import";
    protected $_logFileExtension = "log";

    /** @var Dyna_Import_Helper_Data $_helper */
    public $_helper;

    
    /**
     * Dyna_AgentDE_Model_Import_VodafoneShip2Stores constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->_helper->setImportLogFile($this->_logFileName . '.' . $this->_logFileExtension);
    }

    /**
     * @param $header
     * @return bool
     */
    public function setHeader($header)
    {
        if ($this->validateHeader($header)) {
            $this->_header = $header;
            return true;
        }
        return false;
    }

    private function validateHeader($header)
    {
        if (count($header) != self::NUMBER_OF_COLUMNS) {
            $this->_logError("[ERROR] File rejected: header is invalid. The header should be " . implode(",", self::importFileHeader()));
            return false;
        }
        $validHeader = self::importFileHeader();
        foreach ($header as $columnName) {
            if (false === $keyFound = array_search($columnName, self::importFileHeader())) {
                $this->_logError("[ERROR] File rejected: header is invalid. The column is not valid: " . $columnName);
                $this->_logError("[ERROR] File rejected: header is invalid. The header should be: " . implode(",", self::importFileHeader()));
                return false;
            } else {
                unset($validHeader[$keyFound]);
            }
        }
        if (count($validHeader)) {
            $this->_logError("[ERROR] File rejected: header is invalid. These columns were not present in the import: " . implode(",", self::importFileHeader()));
        }
        return true;
    }

    public static function importFileHeader()
    {
        return [
            "name",
        ];
    }

    public function process($rawData)
    {
        $this->_rowNumber++;
        $rawData = array_combine($this->_header, $rawData);
        if (!array_filter($rawData)) {
            return false;
        }

        try {
            $this->clearData();
            $this->_data['name'] = $rawData['name'];

            /** leave rules processing to other method */
            $this->savePhoneBookKeyword();
            $this->_totalFileRows++;
        } catch (Exception $e) {
            $this->_skippedFileRows++;
            $this->_logError("[ERROR] Skipped row because :" . $e->getMessage());
            fwrite(STDERR, $e->getMessage());
        }
    }

    /**
     * Reset phone book keywords information
     */
    public function clearData()
    {
        $this->_data = array(
            'name' => '',
        );
    }

    /**
     * @return $this
     * @throws Exception
     */
    public function savePhoneBookKeyword()
    {
        /** @var Dyna_AgentDE_Model_PhoneBookKeywords $model */
        $model = Mage::getModel('agentde/phoneBookKeywords');

        $validateResult = $model->validateData(new Varien_Object($this->_data));

        if (!$validateResult['success']) {
            $this->_logError("[ERROR] Skipped row. Missing mandatory fields :" . implode(",", $validateResult['missingFields']) . " at row " . $this->_rowNumber);
        } else {
            $model->addData($this->_data);
            $model->save();
            $this->_log("Successfully imported CSV line " . $this->_rowNumber . " : " . implode('|', $this->_data));
        }

        return $this;
    }
    public function _log($msg, $verbose = false)
    {
        if($verbose && !$this->_debug) {
            return;
        }
        $this->_helper->logMsg('[' . $this->_currentProductSku . '] ' . $msg, false);
    }

    public function _logError($msg)
    {
        $this->_helper->logMsg('[' . $this->_currentProductSku . '] ' . $msg);
    }
}
