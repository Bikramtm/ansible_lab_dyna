<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Dyna_AgentDE_Model_Mysql4_ProvisSales_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    /**
     * Dyna_AgentDE_Model_Mysql4_ProvisSales_Collection constructor
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('agentde/provisSales');
    }
}
