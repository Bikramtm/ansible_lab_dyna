<?php

/**
 * Class Dyna_AgentDE_Model_Mysql4_ProvisSales
 */
class Dyna_AgentDE_Model_Mysql4_ProvisSales extends Mage_Core_Model_Mysql4_Abstract
{
    /**
     * Dyna_AgentDE_Model_Mysql4_ProvisSales constructor.
     */
    public function _construct()
    {
        $this->_init('agentde/provisSales', 'entity_id');
    }
}
