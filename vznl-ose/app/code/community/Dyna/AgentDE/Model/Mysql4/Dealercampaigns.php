<?php

/**
 * Class Dyna_AgentDE_Model_Mysql4_DealerCampaigns
 */
class Dyna_AgentDE_Model_Mysql4_DealerCampaigns extends Mage_Core_Model_Mysql4_Abstract
{
    /**
     * Dyna_AgentDE_Model_Mysql4_DealerCampaigns constructor.
     */
    public function _construct()
    {
        // Note that the campaign_id refers to the key field in your database table.
        $this->_init('agentde/dealercampaigns', 'campaign_id');
    }
}
