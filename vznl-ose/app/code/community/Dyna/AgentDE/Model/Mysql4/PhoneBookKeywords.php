<?php

/**
 * Class Dyna_AgentDE_Model_Mysql4_VodafoneShip2Stores
 */
class Dyna_AgentDE_Model_Mysql4_PhoneBookKeywords extends Mage_Core_Model_Mysql4_Abstract
{
    /**
     * Dyna_AgentDE_Model_Mysql4_PhoneBookKeywords constructor.
     */
    public function _construct()
    {
        $this->_init('agentde/phoneBookKeywords', 'entity_id');
    }
}