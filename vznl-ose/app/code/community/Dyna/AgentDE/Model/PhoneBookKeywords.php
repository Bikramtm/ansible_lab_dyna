<?php

/**
 * Class Dyna_AgentDE_Model_PhoneBookKeywords
 */
class Dyna_AgentDE_Model_PhoneBookKeywords extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('agentde/phoneBookKeywords');
    }

    /**
     * @param int $id
     * @param null $field
     * @return $this|Mage_Core_Model_Abstract
     */
    public function load($id, $field=null)
    {
        $key = 'phoneBookKeywords' . md5(serialize(array($id, $field)));
        if ($vfStore = Mage::objects()->load($key)) {
            $this->setData($vfStore->getData());
            return $this;
        }

        parent::load($id, $field);
        Mage::objects()->save($this, $key);
        return $this;
    }

    public function validateData($dataObject)
    {
        $response = [
            'success' => true,
            'missingFields' => []
        ];
        if(!$dataObject->getName()) {
            $response['success'] = false;
            $response['missingFields'][] = 'Name';
        }
        return $response;
    }
}