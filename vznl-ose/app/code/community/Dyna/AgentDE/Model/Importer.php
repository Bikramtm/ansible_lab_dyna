<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Agent_Model_GenericImporter
 */
class Dyna_AgentDE_Model_Importer extends Dyna_Agent_Model_GenericImporter
{
    protected $header;
    /** @var string $_totalFileRows */
    protected $_totalFileRows = 0;
    /** @var string $_skippedFileRows */
    protected $_skippedFileRows = 0;
    /** @var string $_skippedFileRows */
    protected $_rowNumber = 1;
    /** @var string $_multiselectSplit */
    protected $_multiselectSplit = ",";
    /** @var string $_csvDelimiter */
    protected $_csvDelimiter = ';';
    /** @var string $_logFileName */
    protected $_logFileName = "agent_import";
    /** @var string $_logFileExtension */
    protected $_logFileExtension = "log";
    /** @var string $_pathToFile */
    protected $_pathToFile = null;
    /** @var Dyna_Import_Helper_Data $_helper */
    protected $_helper = null;


    public function __construct()
    {
        $this->_helper = Mage::helper('dyna_import/data');
        $this->_helper->setImportLogFile($this->_logFileName . '.' . $this->_logFileExtension);
    }

    /**
     * @param $path
     * @param Dyna_Agent_Model_Mapper_AbstractMapper $mapper
     */
    protected function importCsv($path, Dyna_Agent_Model_Mapper_AbstractMapper $mapper)
    {
        /** @var Mage_Core_Model_Resource_Transaction $transaction */
        $transaction = Mage::getModel('core/resource_transaction');

        try {
            /** @var Omnius_Import_Model_Adapter_Csv $csv */
            $csv = new Dyna_Import_Model_Adapter_Csv($path);
            $importHeaders = $csv->getColNames();

            //check if match our template
            $checkHeader = $this->_helper->checkHeader($importHeaders, 'agent');
            //log messages
            $messages = $this->_helper->getMessages();
            foreach ($messages as $msg) {
                //write file log
                $this->_log($msg);
            }
            //check if the headers are ok
            if (!$checkHeader) {
                return false;
            }

            $objs = array();
            while(!is_null($csv->key())) {
                $this->_totalFileRows++;
                $this->_rowNumber++;
                $row = $csv->current();
                $obj = $mapper->map($row);
                $validateResult = $this->validateLine($obj);

                if (!$validateResult['success']) {
                    $this->_log("[ERROR] Skipped row. Missing data for mandatory fields :" . implode(",", $validateResult['missingData']) . " at row " . $this->_rowNumber);
                    $this->_skippedFileRows++;
                } else {
                    $action = $obj->getData('is_new') ? 'add' : 'update';
                    $this->_log("[INFO] Row " . $this->_rowNumber . " sucessfully processed. Result: " . $action . " agent " . $obj->getData('username'));
                    $objs[] = $obj;
                }

                $csv->next();
            }
            $importRows = $objs;

            foreach ($importRows as $row) {
                if ($row instanceof Mage_Core_Model_Abstract) {
                    if ($row instanceof Dyna_Agent_Model_Agent && !$row->getUsername()) {
                        continue;
                    }
                    $transaction->addObject($row);
                }
            }
            $transaction->save();
        } catch (Exception $e) {
            $this->_log($e->getMessage());
            return false;
        }
    }

    /**
     * @return Mage_Core_Helper_Abstract|null
     */
    protected function getImportHelper()
    {
        /** @var Dyna_Import_Helper_Data _importHelper */
        return $this->_helper == null ? Mage::helper("dyna_import/data") : $this->_helper;
    }

    /**
     * Validates import file row
     * 
     * @return array
     */
    protected function validateLine($lineObj)
    {
        $response = [
            'success' => true,
            'missingData' => [],
        ];

        $lineData = $lineObj->getData();
        $lineOrigData = $lineObj->getOrigData();
        $requiredColumns = $this->_helper->getRequiredColumns('agent');

        foreach ($requiredColumns as $item) {
            if (!isset($lineData[$item]) && !isset($lineOrigData[$item])) {
                $response['success'] = false;
                $response['missingData'][] = $item;
            }
        }

        return $response;
    }

    /**
     * Logs the header of the imported file
     */
    public function logHeader()
    {
        $this->_log(sprintf('Starting import of file: %s', $this->_pathToFile), Zend_Log::INFO);
    }

    /**
     * Logs the end of the imported file
     */
    public function logFooter()
    {
        $this->_log(sprintf('Import contains in total %d rows', $this->_totalFileRows), Zend_Log::INFO);
        $this->_log(sprintf('Successfully imported %d rows', $this->_totalFileRows - $this->_skippedFileRows), Zend_Log::INFO);
        $this->_log(sprintf('Skipped rows with error %d', $this->_skippedFileRows), Zend_Log::INFO);
        $this->_log(sprintf('Ending import of file: %s', $this->_pathToFile), Zend_Log::INFO);
    }

    /**
     * @param $msg
     */
    public function _log($msg, $level = null)
    {
        $this->_helper->logMsg($msg, true, $level);
    }

    /**
     * Sets path to file property
     *
     * @param $path
     */
    public function setPathToFile($path) {
        $this->_pathToFile = $path;
    }
} 
