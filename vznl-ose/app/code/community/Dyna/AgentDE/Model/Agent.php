<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Agent_Model_Agent
 */
class Dyna_AgentDE_Model_Agent extends Dyna_LoginSecurity_Model_Agent
{
    const AGENT_ROLE_PERMISSION_IMPERSONATE = 'IMPERSONATE';
    const AGENT_IMPERSONATES_ROLE_KEY = "impersonates_additional_role";
    const AGENT_ROLE_COPS = 'SELECT_MARKETING_DATA';
    public $salesid = null;
    private $provisSalesInfo = null;
    protected $additionalRoles = false;

    /**
     * @param int $id
     * @param null $field
     * @return $this|Mage_Core_Model_Abstract
     */
    public function load($id, $field = null)
    {
        $key = 'agent_' . md5(serialize([$id, $field]));
        if ($product = Mage::objects()->load($key)) {
            $this->setData($product->getData());

            return $this;
        }

        parent::load($id, $field);
        $this->getImpersonationIds();
        Mage::objects()->save($this, $key);

        return $this;
    }

    /**
     * @return mixed
     */
    public function getImpersonationIds()
    {
        if (!$this->hasData('impersonate_ids')) {
            $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
            $sql = "SELECT `impersonate_id` FROM `agent_impersonate` WHERE agent_id=:id";
            $this->setData('impersonate_ids', $connection->fetchCol($sql, array(':id' => $this->getId())));
        }
        return $this->getData('impersonate_ids');
    }

    /**
     * @return mixed
     */
    public function getImpersonationAgents()
    {
        if (!$this->hasData('impersonate_agents')) {
            $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
            $agentSql = "SELECT * FROM `agent_impersonate` 
              LEFT JOIN `agent` ON `agent`.`agent_id` = `agent_impersonate`.`impersonate_id` 
              WHERE `agent_impersonate`.`agent_id`=:id";

            $this->setData('impersonate_agents', $connection->fetchAll($agentSql, array(':id' => $this->getId())));
        }
        return $this->getData('impersonate_agents');
    }

    /**
     * @return bool
     */
    public function canSelectMarketingData()
    {
        if ($this->isGranted(array(self::AGENT_ROLE_COPS))) {
            return true;
        }

        return false;
    }

    /**
     * Validate the time the Agent didn't log into the system and lock the account after reaching the time treshold
     *
     * @param  string $login
     * @throws Mage_Core_Exception
     * @return Dyna_AgentDE_Model_Agent
     */
    public function validateActivityTreshold($login)
    {
        /** @var Dyna_Agent_Model_Mysql4_Agent_Collection $collection */
        $collection = Mage::getModel('agent/agent')->getCollection();
        $collection
            ->addFieldToFilter('username', $login)
            ->addFieldToFilter('is_active', 1)
            ->addFieldToFilter('store_id', Mage::app()->getStore()->getId())
            ->load();

        if ($collection->count()) {
            $agent = $collection->fetchItem();
            $today = new DateTime();
            $timeInterval = $today->diff(new DateTime($agent->getLastLoginDate()));
            $inactivityThreshold = Mage::getStoreConfig('agent/general_config/inactivity_threshold');
            if ($timeInterval->format('%d') > $inactivityThreshold || $agent->getLocked()) {
                $agent->setLocked(1);
                $agent->save();
                throw Mage::exception('Mage_Core',
                    Mage::helper('customer')->__('You have been locked out of your account, please contact your line manager'),
                    1338);
            } else {
                $agent->setLastLoginDate(date('Y-m-d H:m:s'));
                $agent->save();
            }

        } else {
            throw Mage::exception('Mage_Core', Mage::helper('customer')->__('Invalid user'));
        }
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAgents()
    {
        $customerSession = Mage::getSingleton('dyna_customer/session');
        $agent = $customerSession->getAgent();

        return Mage::getModel('agent/agent')
            ->getCollection()
            ->addFieldToFilter('is_active', 1)
            ->addFieldToFilter('store_id', Mage::app()->getStore($this->getStoreId())->getWebsiteId())
            ->addFieldToFilter('agent_id', ['neq' => $agent->getId()])
            ->setPageSize(10)
            ->setCurPage(1);
    }

    /**
     * Update provis sales information for current red sales id from quote.
     */
    protected function getProvisSalesInfo()
    {
        if ($this->provisSalesInfo == null) {
            /** @var Dyna_Checkout_Model_Sales_Quote $quote */
            $quote = Mage::getSingleton('checkout/session')->getQuote();

            if ($quote->getRedSalesId() == null) {
                $this->provisSalesInfo = Mage::getModel('agentde/provisSales');
            } else {
                // @todo: Make sure that red_sales_id is unique
                $this->provisSalesInfo = Mage::getModel('agentde/provisSales')
                    ->getCollection()
                    ->addFieldToFilter('red_sales_id', $quote->getRedSalesId())
                    ->getFirstItem();

            }
        }
        return $this->provisSalesInfo;
    }

    /**
     * Logic of isGranted aligned with VFDED1W3S-1130: impersonate role not agent
     * Will search through default agent role permissions
     * If permission is not found in default role, continue iterating through additional roles until one of these roles includes permission
     * @param $permission
     * @return bool
     */
    public function isGranted($permission)
    {
        // check whether or not agent is impersonating a role
        if ((($roleId = $this->impersonatesRole()) && $role = $this->getAdditionalRoles($roleId))) {
            if (is_array($permission)) {
                foreach ($permission as $perm) {
                    if (!$role->isGranted($perm)) {
                        return false;
                    }
                }

                return true;
            } else {
                return $role->isGranted($permission);
            }
        } else {
            // otherwise, fall back to default behavior
            return parent::isGranted($permission);
        }
    }

    /**
     * Get agent additional roles
     * @return Dyna_Agent_Model_Agentrole[]|Dyna_Agent_Model_Agentrole|null
     */
    public function getAdditionalRoles($roleId = null)
    {
        if ($this->additionalRoles === false) {
            $this->additionalRoles = array();

            if ($additionalRoleIds = $this->getAdditionalRoleIds()) {
                $idsExploded = explode(',', $additionalRoleIds);
                $rolesCollection = Mage::getModel('agent/agentrole')
                    ->getCollection()
                    ->addFieldToFilter('role_id', array('in' => $idsExploded));
                foreach ($rolesCollection as $role) {
                    $this->additionalRoles[] = $role;
                }
            }
        }

        if ($roleId) {
            foreach ($this->additionalRoles as $role) {
                if ($role->getRoleId() != $roleId) {
                    continue;
                }
                return $role;
            }
            return null;
        } else {
            return $this->additionalRoles;
        }
    }


    /**
     * Check if agent is allowed to switch to another business function
     * @return bool
     */
    public function hasBusinessFunction()
    {
        return (bool)count($this->getAdditionalRoles());
    }

    /**
     * Check whether or not agent has chosen to impersonate another agent role
     * @return bool
     */
    public function impersonatesRole()
    {
        return Mage::getSingleton('customer/session')->getData(self::AGENT_IMPERSONATES_ROLE_KEY);
    }

    /**
     * Method for logging out the Agent
     */
    public function logout()
    {
        Mage::getSingleton('checkout/session')->clear();
        /** @var Mage_Customer_Model_Session $customerSession */
        $customerSession = Mage::getSingleton('customer/session');
        $customerSession->clear();
        $customerSession->unsetData(Mage::getModel('agent/agent')::AGENT_IMPERSONATES_ROLE_KEY);
        $customerSession->setAgent(null);
        $customerSession->setSuperAgent(null);
        $customerSession->unsOrderEdit();
        $customerSession->unsOrderEditMode();
        $customerSession->logout();
        $customerSession->renewSession();
        Mage::dispatchEvent("dyna_agent_logout");
    }
}
