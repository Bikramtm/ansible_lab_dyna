<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_AgentDE_Model_Mapper_PhoneBookIndustries
 */
class Dyna_AgentDE_Model_Mapper_PhoneBookIndustries extends Dyna_Agent_Model_Mapper_AbstractMapper
{
    /** @var Dyna_AgentDE_Model_Mysql4_PhoneBookIndustries_Collection */
    protected $_collection;

    /**
     * @param array $data
     * @return Dyna_AgentDE_Model_Agent
     */
    public function map(array $data)
    {
        /** @var Dyna_Agent_Model_Agent $agent */
        if (isset($data['name']) && $data['name'] && ($existingPhoneBookIndustry = $this->_getCollection()->getItemByColumnValue('name', $data['name']))) {
            $phoneBookIndustry = $existingPhoneBookIndustry->load($existingPhoneBookIndustry->getId());
            $phoneBookIndustry->setIsNew(false);
        } else {
            $phoneBookIndustry = Mage::getModel('agentde/phoneBookIndustries');
            $phoneBookIndustry->setIsNew(true);
        }

        foreach ($data as $property => $value) {
            try {
                $phoneBookIndustry->setData($property, $this->mapProperty($property, $value, $phoneBookIndustry));
            } catch (UnexpectedValueException $e) {
                //no-op as this ex will be thrown for old agents
            }
        }

        return $phoneBookIndustry;
    }

    /**
     * @return Dyna_AgentDE_Model_Mysql4_PhoneBookIndustries_Collection
     */
    protected function _getCollection()
    {
        if ( ! $this->_collection) {
            return $this->_collection = Mage::getResourceModel('agentde/phoneBookIndustries_collection');
        }
        return $this->_collection;
    }
}