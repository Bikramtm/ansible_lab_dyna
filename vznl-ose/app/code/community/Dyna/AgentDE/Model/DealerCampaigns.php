<?php

/**
 * Class Dyna_AgentDE_Model_DealerCampaigns
 */
class Dyna_AgentDE_Model_DealerCampaigns extends Mage_Core_Model_Abstract
{
    /**
     * Dyna_AgentDE_Model_DealerCampaigns constructor.
     */
    public function _construct()
    {
        parent::_construct();

        $this->_init('agentde/dealercampaigns');
    }
}
