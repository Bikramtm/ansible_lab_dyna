<?php

/**
 * Import dealer assigned to campaigns class
 * Class Dyna_AgentDE_Model_DealerImportCampaign
 */
class Dyna_AgentDE_Model_DealerImportCampaign extends Omnius_Import_Model_Import_ImportAbstract
{
    protected $_csvDelimiter = ';';

    protected $_header;

    protected $_mapping;

    protected $_currentLine = 0;

    protected $_dealers;

    protected $_cache = null;
    /** @var string $_logFileName */
    protected $_logFileName = "dealer_campaigns_import";
    /** @var string $_logFileExtension */
    protected $_logFileExtension = "log";

    /** @var Omnius_Import_Helper_Data $_helper */
    public $_helper;
    

    /**
     * Dyna_AgentDE_Model_DealerImportCampaign constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_helper->setImportLogFile($this->_logFileName . '.' . $this->_logFileExtension);

        $this->_mapping = Mage::helper("dyna_import/data")->getMapping('dealerCampaign');

        $this->_dealers = array();
    }

    /**
     * @param $data
     * @return $this|bool
     */
    public function importDealer($data)
    {
        /** if the number of columns from current line differs from number of columns that header has, log error */
        if (count($this->_header) !== count($data)) {
            $this->_log("[ERROR] Skipping line: invalid data. Header columns differ from data columns");
            $this->_skippedFileRows++;
            return false;
        }

        try {
            /** add keys from header to data */
            $data = array_combine($this->_header, $data);

            if (!$this->_isValidData($data)) {
                throw new Exception('[ERROR] Invalid data');
            }

            $vfDealerCode = trim($data['vf_dealer_code']);
            $campaignId = trim($data['name']);

            /** Let's check if dealer exists in our DB */
            /** @var Dyna_AgentDE_Model_Dealer $dealer */
            $dealer = $this->_getDealerFromCache($vfDealerCode);

            /** append this campaign ID to the ones associated to this dealer */
            $hasCampaigns = $dealer->getDealerCampaigns(true);

            if (!empty($hasCampaigns[$campaignId])) {
                if ($data['allowed/not_allowed_for'] == 'allowed') {
                    $this->_log(sprintf("[ERROR] Skipping line: dealer %s has already assigned campaign %s", $dealer->getVfDealerCode(), $campaignId));
                    $this->_skippedFileRows++;
                    return;
                }

                unset($hasCampaigns[$campaignId]);
            }

            /** Checking if campaign is already present in DB  */
            $campaign = Mage::getModel('agentde/DealerCampaigns')
                ->load($campaignId, 'name');

            if (!$campaign->getId()) {
                $campaign->setData('name', $campaignId);
                $campaign->save();
            }

            /** If it is allowed, add it to current campaigns list */
            if ($data['allowed/not_allowed_for'] == 'allowed') {
                $hasCampaigns[] = $campaign->getId();
                $message = sprintf(
                    "Added campaign %s to dealer %s",
                    $campaign->getName(),
                    $dealer->getVfDealerCode()
                );
            } else {
                $message = sprintf(
                    "Removing campaign %s from dealer %s",
                    $campaign->getName(),
                    $dealer->getVfDealerCode()
                );
            }

            $dealer->setData('campaign_id', $hasCampaigns)->save();
            $this->_saveDealerToCache($dealer);

            $this->_log($message);

        } catch (Exception $e) {
            $this->_skippedFileRows++;
            $this->_logError($e->getMessage());
            fwrite(STDERR, $e->getMessage());
        }

        return $this;
    }

    /**
     * @param $header
     * @return $this
     */
    public function mapHeaderToFields($header)
    {
        $this->_header = array();

        /** Set mapping key to dealer properties */
        foreach ($header as $columnHead) {
            !empty($this->_mapping[$columnHead]) ? $this->_header[$this->_mapping[$columnHead]] = $this->_mapping[$columnHead] : $this->_header[$columnHead] = $columnHead;
        }

        return $this;
    }

    /**
     * @param $lineNumber
     * @return $this
     */
    public function setCurrentLine($lineNumber)
    {
        $this->_currentProductSku = $lineNumber;

        return $this;
    }

    /**
     * @param $data
     * @return bool
     */
    protected function _isValidData($data)
    {
        if (empty($data['vf_dealer_code'])) {
            $this->_log("[ERROR] Skipping line: no vf_dealer_code found.");

            return false;
        }

        if (empty($data['name'])) {
            $this->_log("[ERROR] Skipping line: no campaign ID found.");

            return false;
        }

        return true;
    }

    /**
     * @param $code
     * @return Mage_Core_Model_Abstract|mixed
     */
    protected function _getDealerFromCache($code)
    {
        $key = serialize(array(
            __METHOD__,
            $code,
        ));

        if ($dealer = unserialize($this->getCache()->load($key))) {
            return $dealer;
        }

        $dealer = Mage::getModel('agentde/dealer')->getCollection()
            ->addFieldToFilter('vf_dealer_code', $code)
            ->getFirstItem();

        if (!$dealer->getId()) {
            $dealer->setData('vf_dealer_code', $code);
            $dealer->save();
        }

        $this->_saveDealerToCache($dealer);

        return $dealer;
    }

    /**
     * @param $dealer
     * @return mixed
     */
    protected function _saveDealerToCache($dealer)
    {
        $key = serialize(array(
            __METHOD__,
            $dealer->getVfDealerCode(),
        ));

        $this->getCache()->save(
            serialize($dealer),
            $key,
            array(Dyna_Cache_Model_Cache::AGENT_TAG),
            $this->getCache()->getTtl()
        );

        return $dealer;
    }

    /**
     * @return Dyna_Cache_Model_Cache
     */
    protected function getCache()
    {
        if (!$this->_cache) {
            $this->_cache = Mage::getSingleton('dyna_cache/cache');
        }

        return $this->_cache;
    }

}
