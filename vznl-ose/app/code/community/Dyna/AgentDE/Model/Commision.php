<?php

/**
 * Class Dyna_AgentDE_Model_Agent_Commision
 */
class Dyna_AgentDE_Model_Commision extends Mage_Core_Model_Abstract
{
    const ALLOWED_PRODUCT_TYPES = [
        'Red',
        'Blue + Red',
        'Blue',
        'VPKN',
    ];

    protected function _construct()
    {
        parent::_construct();
        $this->_init("agentde/commision");
    }
}
