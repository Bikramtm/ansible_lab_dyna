<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Observer
 */
class Dyna_AgentDE_Model_Observer extends Dyna_Agent_Model_Observer
{
    const CUSTOM_STUBS_NAMESPACE_PARAM = 'TC_ID';
    const CUSTOM_STUBS_PARAM_DELETE_VALUE = 'NULL';

    /**
     * Listener for the preDispatch method of the Mage_Core_Controller_Front_Action
     * If the customers are logged in but no dealer is found on their session, redirect
     * them to the customer/account/dealer
     *
     * @param $observer
     */
    public function controllerActionPreDispatch($observer)
    {
        /** @var Mage_Core_Controller_Front_Action $controller */
        $controller = $observer->getEvent()->getControllerAction();
        /** @var Mage_Core_Controller_Request_Http $request */
        $request = $controller->getRequest();
        /** @var Mage_Customer_Model_Session $customerSession */
        $customerSession = Mage::getSingleton('customer/session');
        /** @var Dyna_AgentDE_Model_Agent $sessionAgent */
        $sessionAgent = $customerSession->getAgent();
        $postKey = $sessionPostKey = null;
        if ($request->isPost()) {
            $postKey = !empty($request->getHeader('PostKey')) ?
                $request->getHeader('PostKey') :
                (!empty($request->getParam('post_key')) ?
                    $request->getParam('post_key') :
                    ''
                );
            $sessionPostKey = Mage::getSingleton('core/session')->getPostKey();
        }
        $enableXSRF = Mage::getStoreConfig('agent/general_config/enablexsrf');

        if ($request->isPost() && ($postKey !== $sessionPostKey || empty($postKey)) && $enableXSRF) {
            $request->setDispatched(true);
            $controller->setFlag(
                '',
                Mage_Core_Controller_Front_Action::FLAG_NO_DISPATCH,
                true
            );
            $coreHelper = Mage::helper('core');
            if ($observer->getControllerAction()->getRequest()->isXmlHttpRequest()) {
                $controller->getResponse()->setHeader('Content-Type', 'application/json');
                $response = array(
                    'error' => true,
                    'message' => $coreHelper->__("XSRF Security Token missing or invalid.")
                );

                if (!($customerSession->getAgent() && $customerSession->getAgent()->getId())) {
                    $response['session'] = 'expired';
                }

                $controller->getResponse()->setBody(json_encode($response));
                $controller->getResponse()->setHttpResponseCode(401)->sendResponse();
                exit;
            } else {
                echo $coreHelper->__("XSRF Security Token missing.") . "<br>" .
                    $coreHelper->__("The requested action could not be completed because of a missing form token.") . "<br>" .
                    $coreHelper->__("You may have cleared your browser cookies, which could have resulted in the expiry of your current form token.") . "<br>" .
                    $coreHelper->__("Please return to the") . " <a href='" . Mage::getBaseUrl() . "'>" . $coreHelper->__('homepage') . "</a>.";
                return;
            }
        }
        // Check for public allowed URLs
        $allowedUrls = Mage::helper('agentde')->getConfigAllowedUrls();
        if ($allowedUrls && is_array($allowedUrls)) {
            foreach ($allowedUrls as $allowedUrl) {
                if (trim($allowedUrl) == $observer->getEvent()->getControllerAction()->getFullActionName()) {
                    return;
                }
            }
        }

        $pageAlias = $observer->getControllerAction()->getRequest()->getAliases();
        $action = isset($pageAlias["rewrite_request_path"]) ? $pageAlias["rewrite_request_path"] : '';

        //only redirect to login if store is set to show agent login
        if (!Mage::app()->getStore()->getShowAgent()) {
            return;
        }

        // Check TC_ID parameter and set stub namespace
        $tcId = str_replace(' ', '',
            $observer->getControllerAction()->getRequest()->getParam(self::CUSTOM_STUBS_NAMESPACE_PARAM));
        if (!empty($tcId)) {
            if (strtolower($tcId) === strtolower(self::CUSTOM_STUBS_PARAM_DELETE_VALUE)) {
                $customerSession->setData(self::CUSTOM_STUBS_NAMESPACE_PARAM, null);
            } else {
                $customerSession->setData(self::CUSTOM_STUBS_NAMESPACE_PARAM, $tcId);
            }
        }

        Mage::helper('agent')->checkOrderEditMode($request, $controller);
        $isIpSecurityEnabled = Mage::helper('core')->isModuleEnabled('IpSecurity');
        if ($isIpSecurityEnabled) {
            $ipSecurityPage = Mage::getStoreConfig('ipsecurity/ipsecurityfront/redirect_page');
        }

        if ($sessionAgent) {
            $dbAgent = Mage::getModel('agent/agent')->load($sessionAgent->getUsername(), 'username');
            if ($dbAgent->getId() && ($dbAgent->getPassword() != $sessionAgent->getPassword())) {
                $sessionAgent->logout();
                /** @var Mage_Customer_Model_Session $customerSession */
                $customerSession = Mage::getSingleton('customer/session');
            }
        }

        if (!$customerSession->getData('agent')
            && 'agent_account_login' !== $observer->getEvent()->getControllerAction()->getFullActionName()
            && 'configurator_cart_newtab' !== $observer->getEvent()->getControllerAction()->getFullActionName()
            && 'agent_account_loginPost' !== $observer->getEvent()->getControllerAction()->getFullActionName()
            && 'agent_account_forgotCredentials' !== $observer->getEvent()->getControllerAction()->getFullActionName()
            && 'agent_account_password' !== $observer->getEvent()->getControllerAction()->getFullActionName()
            && 'agent_account_temporaryPassword' !== $observer->getEvent()->getControllerAction()->getFullActionName()
            && 'agent_account_passwordReset' !== $observer->getEvent()->getControllerAction()->getFullActionName()
            && 'agent_account_dealer' !== $observer->getEvent()->getControllerAction()->getFullActionName()
            && 'agent_account_dealerPost' !== $observer->getEvent()->getControllerAction()->getFullActionName()
            && 'adyen_process_ins' !== $observer->getEvent()->getControllerAction()->getFullActionName()
            && (!$action || ($isIpSecurityEnabled && $action !== $ipSecurityPage))
        ) {
            // Check if the agent is not logged in
            if ($observer->getControllerAction()->getRequest()->isXmlHttpRequest()) {
                $controller->getResponse()->setHeader('Content-Type', 'application/json');
                $controller->getResponse()->setBody(json_encode(array('error' => true, 'session' => 'expired')));
                $controller->getResponse()->setHttpResponseCode(401)->sendResponse();
                exit;
            } else {
                if ('core_index_noCookies' == $observer->getEvent()->getControllerAction()->getFullActionName()) {
                    Mage::getSingleton('core/session')->addNotice(Mage::helper('dyna_customer')->__('Your cookies expired. Please try again.'));
                }

                $urlParams = $observer->getControllerAction()->getRequest()->getParams();
                $coreSession = Mage::getSingleton('core/session');

                if (isset($urlParams['transactionId']) && $urlParams['transactionId'] != null) {
                    $coreSession->setData($urlParams);
                }

                $controller->getResponse()->setRedirect(Mage::getUrl('agent/account/login'))->sendHeaders();
                exit;
            }
        }
        if (Mage::app()->getStore()->getShowStore()
            && $customerSession->getData('agent')
            && !$customerSession->getData('agent')->getData('dealer')
            && 'agent_account_dealer' !== $observer->getEvent()->getControllerAction()->getFullActionName()
        ) {
            $controller->getResponse()->setRedirect(Mage::getUrl('agent/account/dealer'))->sendHeaders();
            return;
        }
    }

    /**
     * Listener for the preDispatch method of the Mage_Core_Controller_Front_Action
     * If the customers are logged in but no dealer is found on their session, redirect
     * them to the customer/account/dealer
     *
     * @param $observer
     */
    public function validateBlackListUrlsPreDispatch($observer)
    {
        /** @var Mage_Core_Controller_Front_Action $controller */
        $controller = $observer->getEvent()->getControllerAction();
        $request = $observer->getControllerAction()->getRequest();
        $this->validateBlackList($request, $controller);
    }

    /**
     * @param $request
     * @param $controller
     */
    protected function validateBlackList($request, $controller)
    {
        // Check for black listed routes - they will not be resolved
        $blackList = Mage::getConfig()->getNode('global/black_list_urls');

        if ($blackList) {
            $requestStr = $request->getRequestString();
            foreach ($blackList->children() as $url) {
                if (preg_match($url, $requestStr, $matches)) {
                    $controller->getResponse()->setRedirect(Mage::getUrl('/'))->sendHeaders();
                }
            }
        }
    }
}
