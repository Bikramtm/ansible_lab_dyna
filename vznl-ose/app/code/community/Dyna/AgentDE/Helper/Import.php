<?php

/**
 * Class Dyna_AgentDE_Helper_Import
 * Helper class for Dyna_AgentDE module
 */
class Dyna_AgentDE_Helper_Import extends Mage_Core_Helper_Abstract
{

    const AGENT_SALES_ID_LOG_FILE = 'agent_sales_ids_import.log';

    public function importAgentCommisions($filepath) 
    {
        if (!file_exists($filepath)) {
            Mage::log(sprintf('Cannot find file to import at %s', $filepath), null, self::AGENT_SALES_ID_LOG_FILE);
            return false;
        }

        /** @var \Kdg\CsvReader\CsvReader $csvReader */
        $csvReader = \Kdg\CsvReader\CsvReaderFactory::newFactory()->csvReaderForFile($filepath);
        // Go to the first row and skip header aswell (so 2 nexts)
        $csvReader->next();
        $csvReader->next();

        while ($currentRow = $csvReader->current()) {
            $data = explode(';', $currentRow[0]);

            $agentId = isset($data[0]) ? $data[0] : null;
            $agent = Mage::getModel('agent/agent')->load($agentId);
            // Check if the agent exists
            if (!$agent->getAgentId()) {
                Mage::log(sprintf('Agent with given agent id does not exist, got agent id: %s', $agentId), null, self::AGENT_SALES_ID_LOG_FILE);
                $csvReader->next();
                continue;
            }

            for ($i = 0; $i < count(Dyna_AgentDE_Model_Commision::ALLOWED_PRODUCT_TYPES); $i++) {
                $type = Dyna_AgentDE_Model_Commision::ALLOWED_PRODUCT_TYPES[$i];
                $salesId = isset($data[$i + 1]) ? $data[$i + 1] : null;

                // Check if the data is complete
                if (!$agentId || !$type) {
                    Mage::log(sprintf('Row data was incomplete, was: %s', $currentRow[0]), null, self::AGENT_SALES_ID_LOG_FILE);
                    break;
                }

                // Add the data to the db
                /** @var Dyna_AgentDE_Model_Commision $agentCommision */
                $agentCommision = Mage::getModel('agentde/commision');
                $agentCommision->setImportLine($csvReader->key());
                $agentCommision->setAgentId($agentId);
                if ($salesId) {
                    $agentCommision->setSalesId($salesId);
                }
                $agentCommision->setType($type);
                $agentCommision->save();
            }
            // Next
            $csvReader->next();
        }
        return true;
    }
}
