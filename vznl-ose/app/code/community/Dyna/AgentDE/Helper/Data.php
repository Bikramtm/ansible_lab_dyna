<?php

/**
 * Class Dyna_AgentDE_Helper_Data
 * Helper class for Dyna_AgentDE module
 */
class Dyna_AgentDE_Helper_Data extends Mage_Core_Helper_Abstract
{

    /*
     * Get all active campaigns from database
     */
    public function getAvailableCampaigns()
    {
        $campaignsCollection = Mage::getModel('agentde/dealerCampaigns')->getCollection();

        $allCampaigns = array();
        foreach ($campaignsCollection as $campaign) {
            $allCampaigns[] = array('value' => $campaign->getId(), 'label' => $campaign->getName());
        }

        return $allCampaigns;
    }

    /*
     * Map campaign data to dealer edit values
     */
    public function mapCampaignData()
    {
        /** This empty will not work beneath php 5.5 */
        if (!empty($dealer = Mage::registry('dealer_data'))) {
            $dealerId = $dealer->getId();

            $dealerCampaigns = Mage::getModel('agentde/DealerCampaigns')->getCollection()->getCampaignsForDealer($dealerId, true);

            /** fetch allready set data on registry */
            $data = Mage::registry("dealer_data");

            /** if the multiselect id changes in Dyna_AgentDE_Block_Adminhtml_Dealer_Edit_Tab_Form it needs to be changed here too */
            $data['campaign_id'] = $dealerCampaigns;

            /** unregister and register data */
            Mage::unregister('dealer_data');
            Mage::register('dealer_data', $data);
        }
    }

    public function getVodafoneShipToStoresAddresses($asJson)
    {
        $collection = Mage::getModel('agentde/vodafoneShip2Stores')->getCollection();

        if (!$asJson) {
            return $collection;
        }

        $vodafoneShip2Stores = array();
        /** @var Dyna_AgentDE_Model_VodafoneShip2Stores $vodafoneShip2Store */
        foreach ($collection as $vodafoneShip2Store) {
            array_push($vodafoneShip2Stores, [
                'data' => $vodafoneShip2Store->getId(),
                'value' => $vodafoneShip2Store->getAddressHtml(),
                'object' => $vodafoneShip2Store->getAddressJson(),
            ]);
        }

        return json_encode($vodafoneShip2Stores);
    }

    /**
     * @return array|string
     */
    public function getConfigAllowedUrls()
    {
        $value = Mage::getStoreConfig('agent/general_config/allowed_urls');
        if (!empty($value)) {
            $value = preg_replace('/(\r\n$|\r$)/', "\n", $value);
            return explode("\n", $value);
        }

        return trim($value);
    }

    public function getIndustryList($search)
    {
        $collection = Mage::getModel('agentde/phoneBookIndustries')->getCollection()
            ->addFieldToFilter('name', array("like" => '%'.$search.'%'));


        $industries = array();
        foreach ($collection as $industry) {
            $industries[] = $industry->getName();
        }

        return $industries;
    }

    public function getKeywordList()
    {
        $collection = Mage::getModel('agentde/phoneBookKeywords')->getCollection();

        $keywords = array();
        foreach ($collection as $keyword) {
            $keywords[] = $keyword->getName();
        }

        return $keywords;
    }
}
