<?php
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;

/**
 * Class Dyna_Core_Helper_Data
 */
class Dyna_Core_Helper_Data extends Omnius_Core_Helper_Data
{
    protected $cache = null;
    protected $expressionLanguage = null;
    protected $dynaCache = null;

    protected $packageTypeId = null;

    /**
     * XML path to cookie config "secure"
     */
    const XML_PATH_COOKIE_SECURE = 'web/cookie/cookie_secure';

    const REQ_UNIQUE_ID_KEY = 'request_unique_id';

    /**
     * Store object
     *
     * @var Mage_Core_Model_Store|null
     */
    protected $_store;

    // after evaluation, expression object will fill up this array with newly created packages, if any
    // is created packages, every action of addProduct will result in adding product to the last created package
    // so, for ensuring that products are added to the correct package, rules should prioritized consecutive for each package
    /** @var Dyna_Package_Model_Package[] */
    protected $packages = array();

    public function _construct()
    {
        $this->getArrayParserCache();

        return $this;
    }

    public function setPackageCreationTypeId($packageTypeId)
    {
        $this->packageTypeId = $packageTypeId;

        return $this;
    }

    /**
     *
     * @return null|string
     */
    public function getPackageCreationTypeId()
    {
        return $this->packageTypeId;
    }

    public function clearPackageType()
    {
        $this->packageTypeId = null;

        return $this;
    }

    public function parseExpressionLanguage($expression, $names, $logFile = "invalid_expression_rules.log")
    {
        try {
            $eLang = $this->getExpressionLanguage();
            $expressionCleaned = str_replace([' AND ', ' OR ', ' NOT '], [' and ', ' or ', ' not '], $expression);

            $result = true;

            switch (true) {
                case stripos($expressionCleaned, " having ") !== false:
                    $expressions = explode(" having ", $expressionCleaned);
                    foreach ($expressions as $brokenExpression) {
                        $result = $result && $eLang->parse($brokenExpression, $names);
                    }
                    break;
                case stripos($expressionCleaned, " union ") !== false:
                    $expressions = explode(" union ", $expressionCleaned);
                    foreach ($expressions as $brokenExpression) {
                        $result = $result && $eLang->parse($brokenExpression, $names);
                    }
                    break;
                default:
                    $eLang->parse($expressionCleaned, $names);
                    break;
            }

        } catch (Exception $e) {
            Mage::log(sprintf("Invalid expression language parsed %s : %s", $expression, $e->getMessage()), Zend_Log::WARN, $logFile);
            return null;
        }

        return $result;
    }

    public function evaluateExpressionLanguage($expression, $data, $logFile = "invalid_expression_rules.log")
    {
        $result = false;

        $debugRules = Mage::helper('dyna_productmatchrule')->isDebugMode();

        try {
            $eLang = $this->getExpressionLanguage();
            $expressionCleaned = str_replace([' AND ', ' OR ', ' NOT '], [' and ', ' or ', ' not '], $expression);

            switch (true) {
                case stripos($expressionCleaned, " having ") !== false:
                    $expressions = explode(" having ", $expressionCleaned);
                    foreach ($expressions as $brokenExpression) {
                        if ($debugRules) {
                            Mage::log('HAVING:    ' .$brokenExpression, null, 'ServiceExpressionRules.log');
                            Mage::log(json_encode($eLang->evaluate($brokenExpression, $data)), null, 'ServiceExpressionRules.log');
                            Mage::log("========================", null, 'ServiceExpressionRules.log');
                        }
                        $result = $result !== false ? array_intersect($result, $eLang->evaluate($brokenExpression, $data)) : $eLang->evaluate($brokenExpression, $data);
                    }
                    break;
                case stripos($expressionCleaned, " union ") !== false:
                    $expressions = explode(" union ", $expressionCleaned);
                    foreach ($expressions as $brokenExpression) {
                        if ($debugRules) {
                            Mage::log('UNION:   ' . $brokenExpression, null, 'ServiceExpressionRules.log');
                            Mage::log(json_encode($eLang->evaluate($brokenExpression, $data)), null, 'ServiceExpressionRules.log');
                            Mage::log("========================", null, 'ServiceExpressionRules.log');
                        }
                        $result = $result !== false ? array_merge($result, $eLang->evaluate($brokenExpression, $data)) : $eLang->evaluate($brokenExpression, $data);
                    }
                    break;
                default:
                    $result = $eLang->evaluate($expressionCleaned, $data);
                    break;
            }
        } catch (Exception $e) {
            Mage::log(sprintf("Invalid expression language %s : %s", stripos($expressionCleaned, " having ") ? $brokenExpression : $expression, $e->getMessage()), Zend_Log::WARN, $logFile);
        }

        return $result;
    }

    public function getExpressionLanguage()
    {
        return $this->expressionLanguage ?: $this->expressionLanguage = new ExpressionLanguage($this->getArrayParserCache());
    }

    public function getArrayParserCache()
    {
        return $this->cache ?: $this->cache = Mage::helper('dyna_core/parserCache');
    }

    /*
 * Return dyna cache for caching parser expressions
 * @return Dyna_Cache_Model_Cache
 */
    public function getCache()
    {
        return $this->dynaCache ?: $this->dynaCache = Mage::getSingleton('dyna_cache/cache');
    }

    /**
     * Method that refreshes the token for a GET request
     * @param $method
     * @param $includeNewLine
     * @throws Exception
     */
    public function setPostKey($method, $includeNewLine = true) {
        if ($method != 'POST') {
            $post_key = bin2hex(random_bytes(32));
            Mage::getSingleton('core/session')->setPostKey($post_key);
        } else {
            $post_key = Mage::getSingleton('core/session')->getPostKey();
        }
        echo '<input name="post_key" type="hidden" value="' . $post_key . '" />' . ($includeNewLine ? "\n" : "");
    }


    /**
     * Returns the last package set on this session if found, otherwise returns the cart active package
     * used only for evaluating quote scope bundle actions
     * @return Dyna_Package_Model_Package|null
     */
    public function getPackage()
    {
        if (!empty($this->packages)) {
            return current($this->packages);
        } else {
            return Mage::getSingleton('checkout/cart')->getQuote()->getCartPackage();
        }
    }

    /**
     * Called by quote scope evaluation method for adding a resulted package from evaluating a bundle action
     * @param Dyna_Package_Model_Package $package
     * @return $this
     */
    public function addNewCreatedPackage(Dyna_Package_Model_Package $package)
    {
        $this->packages[] = $package;

        return $this;
    }
    public function checkSecureFrontend()
    {
        $store = $this->getStore();
        if (!$store->getConfig(self::XML_PATH_COOKIE_SECURE) || !$store->getConfig(Mage_Core_Model_Store::XML_PATH_SECURE_IN_FRONTEND)) {
            return false;
        }
        $baseLinkUrl = $store->getConfig(Mage_Core_Model_Store::XML_PATH_UNSECURE_BASE_LINK_URL);
        return (substr($baseLinkUrl, 0, 8) == 'https://');
    }
    /**
     * Retrieve Store object
     *
     * @return Mage_Core_Model_Store
     */
    public function getStore()
    {
        if (is_null($this->_store)) {
            $this->_store = Mage::app()->getStore();
        }
        return $this->_store;
    }

    /**
     * Encode elements to UTF-8
     * @param $data
     */
    public static function utf8Encode($data)
    {
        if (is_string($data)) {
            $encoded = utf8_encode($data);
            if ($encoded !== $data) {
                echo $data, "\n";
            }
            return $encoded;
        }
        if (!is_array($data)) {
            return $data;
        }
        $ret = array();
        foreach ($data as $i => $d) {
            $ret[$i] = self::utf8Encode($d);
        }
        return $ret;
    }

    /**
     * Generate a unique id that will be used to associate each OSE request to OIL service calls
     */
    public static function getRequestUniqueTransactionId()
    {
        $requestUniqueId = Mage::registry(self::REQ_UNIQUE_ID_KEY);
        if(!$requestUniqueId){
            $requestUniqueId = uniqid();
            Mage::register(self::REQ_UNIQUE_ID_KEY, $requestUniqueId);
        }

        return $requestUniqueId;
    }

    public static function arrayRecursiveUnique($array, $c = array())
    {
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $value = static::arrayRecursiveUnique($value);
                if (!in_array($value, $c)) {
                    $c[$key] = $value;
                }
            } elseif (!in_array($value, $c)) {
                $c[$key] = $value;
            }
        }

        return $c;
    }
}
