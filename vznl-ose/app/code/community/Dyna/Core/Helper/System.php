<?php

class Dyna_Core_Helper_System extends Mage_Core_Helper_Abstract
{
    /** @var int */
    protected $memoryLimit;

    /** @var float */
    protected $memoryLimitAllowed = 0.6;

    /** @var int */
    protected $mysqlPacketSize;

    /** @var float */
    protected $mysqlPacketSizeAllowed = 0.6;

    /**
     * @return int
     */
    public function getMemoryLimit()
    {
        if ($this->memoryLimit) {
            return $this->memoryLimit;
        }

        $phpMemoryLimit = $this->getPhpMemoryLimit();

        if ($phpMemoryLimit) {
            $memoryLimit = $phpMemoryLimit;
        } else {
            $memoryLimit = $this->getSystemMemoryLimit() ?? $this->getDefaultMemoryLimit();
        }

        $this->memoryLimit = floor($memoryLimit * $this->memoryLimitAllowed);

        return $this->memoryLimit;
    }

    /**
     * @return int
     */
    public function getMysqlMaxAllowedPacketSize()
    {
        if ($this->mysqlPacketSize) {
            return $this->mysqlPacketSize;
        }

        $maxAllowedPacketSize = false;

        $mysqlVariables = Mage::getSingleton('core/resource')->getConnection('core_write')->fetchAll('SHOW VARIABLES;');
        foreach ($mysqlVariables as $mysqlVariable) {
            if ($mysqlVariable['Variable_name'] === 'max_allowed_packet') {
                $maxAllowedPacketSize = (int)$mysqlVariable['Value'];
            }
        }

        $this->mysqlPacketSize = floor(($maxAllowedPacketSize ?? $this->getDefaultMaxAllowedPacketSize()) * $this->mysqlPacketSizeAllowed);

        return $this->mysqlPacketSize;
    }

    /**
     * @return int
     */
    protected function getDefaultMaxAllowedPacketSize()
    {
        return 16 * 1024 * 1024;
    }

    /**
     * @return int
     */
    protected function getDefaultMemoryLimit()
    {
        return 32 * 1024 * 1024;
    }

    /**
     * @return bool|int
     */
    protected function getPhpMemoryLimit()
    {
        $phpMemoryLimit = trim(ini_get('memory_limit'));

        if ($phpMemoryLimit <= 0) {
            return false;
        }

        $unit = strtolower(substr($phpMemoryLimit, -1, 1));
        $phpMemoryLimit = (int)$phpMemoryLimit;

        //Let the switch fall through each other so the value gets 'converted' to (G)iga/(M)ega or (K)iloBytes

        switch ($unit) {
            case 'g':
                $phpMemoryLimit *= 1024;
            case 'm':
                $phpMemoryLimit *= 1024;
            case 'k':
                $phpMemoryLimit *= 1024;
                break;
            default:
                $phpMemoryLimit *= 1;
                break;
        }

        return $phpMemoryLimit;
    }

    /**
     * @return bool|int
     */
    protected function getSystemMemoryLimit()
    {
        $memInfoFile = '/proc/meminfo';

        if (!file_exists($memInfoFile) || is_readable($memInfoFile)) {
            return false;
        }

        $fileHandle = fopen($memInfoFile, 'r');
        $memoryTotal = false;
        while ($line = fgets($fileHandle)) {
            if (preg_match('/^MemTotal:\s+(\d+)\skB$/', $line, $matches)) {
                $memoryTotal = $matches[1];
                break;
            }
        }
        fclose($fileHandle);

        return $memoryTotal ? $memoryTotal * 1024 : false;
    }
}
