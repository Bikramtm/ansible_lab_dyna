<?php

class Dyna_Core_Block_Profiler extends Mage_Core_Block_Profiler
{
    protected function _toHtml()
    {
        $html = parent::_toHtml();
        if ($html) {
            $html = <<<EOD
<div class="modal fade" id="profilerModal" tabindex="-1" role="dialog" aria-labelledby="profilerModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="profilerModalLabel">Profiler Modal</h4>
      </div>
      <div class="modal-body">
        $html
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

EOD;
        }
        return $html;
    }
}
