<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */
$installer = new Mage_Eav_Model_Entity_Setup('core_setup');

$installer->startSetup();

$installer->updateAttribute(Mage_Catalog_Model_Product::ENTITY, 'package_type', 'source_model', null);

$installer->updateAttribute(Mage_Catalog_Model_Product::ENTITY, 'package_subtype', 'source_model', null);

$installer->endSetup();
