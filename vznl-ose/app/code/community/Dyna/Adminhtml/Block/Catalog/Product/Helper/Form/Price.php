<?php

class Dyna_Adminhtml_Block_Catalog_Product_Helper_Form_Price extends Mage_Adminhtml_Block_Catalog_Product_Helper_Form_Price
{
    public function __construct($attributes=array())
    {
        parent::__construct($attributes);
        $this->removeClass('validate-zero-or-greater');
    }

    /**
     * Set the price to be saved with 4 decimals instead of 2
     * 
     * @param null $index
     * @return null|string
     */
    public function getEscapedValue($index = null)
    {
        $index = $index;    /* To avoid php error saying that $index is not used inside this function . We can't remove $index from the
                             function definition because the corresponding core version of this function is throwing error saying that $index
                             parameter can't be removed
                            */
        $value = $this->getValue();

        if (!is_numeric($value)) {
            return null;
        }

        return number_format($value, 4, null, '');
    }

}