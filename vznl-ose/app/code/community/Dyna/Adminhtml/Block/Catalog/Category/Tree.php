<?php


class Dyna_Adminhtml_Block_Catalog_Category_Tree extends Mage_Adminhtml_Block_Catalog_Category_Tree
{
    /**
     * Get category name
     *
     * @param Varien_Object $node
     * @return string
     */
    public function buildNodeName($node)
    {
        $result = $this->escapeHtml($node->getName());
        $externalIdentifier = Mage::getResourceModel('catalog/category')
            ->getAttributeRawValue($node->getId(), "external_identifier", Mage::app()->getStore()->getId());

        if ($this->_withProductCount) {
            $result .= ' (' . $node->getProductCount() . ')';
        }
        if ($externalIdentifier) {
            $result = '[' . $externalIdentifier .'] ' .$result;
        }
        return $result;
    }
}