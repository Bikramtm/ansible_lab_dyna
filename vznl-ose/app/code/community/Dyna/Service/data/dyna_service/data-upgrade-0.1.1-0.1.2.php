<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */
Mage::getConfig()->saveConfig(Dyna_Core_Helper_Data::XML_PATH_COOKIE_SECURE , 1, 'default', 0);
Mage::getConfig()->saveConfig(Mage_Core_Model_Store::XML_PATH_SECURE_IN_FRONTEND , 1, 'default', 0);
Mage::getModel('core/config')->cleanCache();