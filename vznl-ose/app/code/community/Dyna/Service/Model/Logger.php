<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Service_Model_Logger
 */
class Dyna_Service_Model_Logger extends Omnius_Service_Model_Logger
{
    protected $logLevel = Zend_Log::ERR;
    protected $logfiles = [];

    /**
     * @param $filename
     * @param $input
     */
    protected function loggerWrite($filename, $input)
    {
        $this->log(
            Mage::helper('omnius_service/request')->convertEncoding($input),
            $this->logLevel,
            trim(str_replace($this->_rootDir, '', $filename), DS),
            true
        );
    }

    /**
     * @param Zend_Log $logLevel
     */
    public function setLogLevel($logLevel = Zend_Log::ERR) {
        $this->logLevel = $logLevel;
    }

    /**
     * @param $folder
     * @param $type
     * @param $uniqueIdentifier
     * @param $format
     * @return string
     */
    protected function createFilename($folder, $type, $uniqueIdentifier, $format)
    {
        if (!is_dir("{$this->_rootDir}/{$folder}")) {
            mkdir("{$this->_rootDir}/{$folder}", 0777, true);
        }

        $uniqueIdentifier = ($uniqueIdentifier ? ".{$uniqueIdentifier}" : "");
        $format = ($format ? ".{$format}" : "");

        $fileIdentifier = $folder . '-' . $uniqueIdentifier . '-' . $type . '-' . $format;
        if (empty($this->logfiles[$fileIdentifier])) {
            $this->logfiles[$fileIdentifier] = sprintf(
                "{$this->_rootDir}/{$folder}/%s{$uniqueIdentifier}.{$type}{$format}",
                (string) $this->udate("YmdHis.u")
            );
        }

        return $this->logfiles[$fileIdentifier];
    }
}