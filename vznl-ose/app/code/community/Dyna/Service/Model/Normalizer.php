<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Service_Model_Normalizer
 */
class Dyna_Service_Model_Normalizer extends Omnius_Service_Model_Normalizer
{
    /**
     * @param $data
     * @return array
     */
    public function normalizeKeys($data)
    {
        $normalized = array();
        if (!is_array($data)) {
            return $normalized;
        }
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                if (key($value) === "_") {
                    // Hack for SearchPotentialLinksResponse
                    $normalized[$key] = array_shift($value);
                } else {
                    $normalized[$key] = $this->normalizeKeys($value);
                }
            } else {
                $normalized[$key] = $value;
            }
        }

        return $normalized;
    }
}
