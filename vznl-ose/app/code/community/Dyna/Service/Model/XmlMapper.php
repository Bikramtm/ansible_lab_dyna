<?php
class Dyna_Service_Model_XmlMapper extends Omnius_Service_Model_XmlMapper
{
    public static $parent = null;
    public static $xpathNode = '*[local-name()="%s"][1]';

    /**
     * @param array $data
     * @param SimpleXMLElement $xml
     * @param bool $throw
     * @return SimpleXMLElement
     */
    public static function map(array $data = array(), SimpleXMLElement &$xml, $throw = false, $parent='', $elementNr = 1)
    {
        $callback = function ($paramName, $paramVal, &$el, $namespace = null, $parent = '', $originalXml) use ($throw) {
            $failed = true;
            if (is_object($paramVal) && $paramVal instanceof Closure) {
                $el[0] = $paramVal($el, $namespace);
                $failed = false;
            } elseif (is_array($paramVal)) {
                if(!count(array_filter(array_keys($paramVal), 'is_string'))){
                    // if array of data, first modify the xml schema to clone nodes and add them as siblings
                    if (!count((array_filter(array_values($paramVal), 'is_array')))) {
                        self::duplicateNode($originalXml, $parent, count($paramVal), $paramVal);
                    } else {
                        self::duplicateNode($originalXml, $parent, count($paramVal));
                        foreach($paramVal as $key => $paramSingleVal){
                            self::$parent = $paramName;
                            if (is_array($paramSingleVal)) {
                                $el[0] = self::map($paramSingleVal, $originalXml, false, $parent, $key+1);
                            }
                        }
                    }
                }
                else{
                    self::$parent = $paramName;
                    $el[0] = self::map($paramVal, $originalXml, false, $parent);
                }
                $failed = false;
            } elseif (is_scalar($paramVal)) {
                $failed = false;
                $el[0][0] = self::getParsedValue($paramVal);
            } elseif (is_null($paramVal)) {
                $failed = false;
                unset($el[0][0]);
            }
            if ($failed && $throw) {
                throw new InvalidArgumentException(sprintf('Invalid value given for key "%s". Expected callable, array or scalar, %s given.', $paramName, gettype($paramVal)));
            }
        };
        foreach ($data as $paramName => $paramVal) {
            // todo: graceful handling of xpath attribute targeting
            if (false === strpos($paramName, '@schemeName')) {
                if($elementNr>1){
                    $parent = substr($parent, 0, strrpos($parent, '[')-strlen($parent)) . "[$elementNr]/";
                }
                $parent2 = $parent . sprintf(self::$xpathNode, $paramName);
            } else {
                $parent2 = $paramName;
            }
            $xpath = sprintf("//%s", $parent2);
            if ($el = @$xml->xpath($xpath)) {
                $callback($paramName, $paramVal, $el, null, $parent2.'/', $xml);
            }
        }
        self::$parent = null;

        return $xml;
    }

    /**
     * Add a cloned node after itself
     *
     * @param SimpleXMLElement $xml The xml to be modified
     * @param string $parent The node to be duplicated
     * @param int $numberOfTimes number of siblings needed
     * @param array $values
     *
     * @return mixed
     */
    private static function duplicateNode(&$xml, $parent, $numberOfTimes, array $values = array())
    {
        //find the node that needs to be cloned
        $xpath = sprintf("//%s", substr($parent, 0, -1));
        $el = current(@$xml->xpath($xpath));

        //if found, duplicate it a given number of times
        if(!is_null($el)){
            if ($values) {
                $el[0] = current($values);
            }
            while ($numberOfTimes > 1) {
                $el->insertBeforeSelf($el->cloneNode(true));
                if ($values) {
                    $el[0] = next($values);
                }
                $numberOfTimes--;
            }
        }
    }

    /**
     * Return value inside CDATA if necessarily @see OMNVFDE-2859
     * @param string $value
     * @return string
     */
    private static function getParsedValue(string $value)
    {
        // If more than ascii characters can be found or one character of the 5 that would break xml schema, wrap it inside CDATA
        if (!mb_detect_encoding($value, 'ASCII', true) || preg_match('/["\'&<>]/', $value)) {
            return '<![CDATA[' . $value . ']]>';
        } else {
            return $value;
        }
    }
}
