<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Service_Model_ResponseMapper
 */
class Dyna_Service_Model_ResponseMapper extends Omnius_Service_Model_ResponseMapper
{
    protected function getMapper($method)
    {
        $className = get_class($this);
        $cacheKey = md5(serialize([$className . "::" . __FUNCTION__, $method]));
        preg_match('/^(.*)_Model_/', $className, $parts);

        $prefix = $parts[0] . 'Mapper';

        $mapperClass = sprintf('%s_%s', $prefix, str_replace(" ", "", ucwords(strtr($method, "_-", "  "))) . 'Mapper');

        if (class_exists($mapperClass)) {
            try {
                $this->_registry[$cacheKey] = @new $mapperClass();
                if (!$this->_registry[$cacheKey]) {
                    Mage::log(sprintf('Failed to response mapper. Error: %s', json_encode(error_get_last())), Zend_Log::ERR);
                    Mage::throwException('Failed to response mapper. Please review logs.');
                }
            } catch (Exception $e) {
                Mage::log($e->getMessage() . "\n" . $e->getTraceAsString(), Zend_Log::ERR, strtolower('soap_debug_' . $method . '.log'));
                Mage::throwException('Failed to create response mapper. Please review logs.');
            }
        } else {
            throw new InvalidArgumentException(sprintf('Invalid type argument provided. No client class "%s" found.', $mapperClass));
        }

        return $this->_registry[$cacheKey];
    }
}