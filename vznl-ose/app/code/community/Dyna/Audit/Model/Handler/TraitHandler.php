<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

trait Dyna_Audit_Model_Handler_TraitHandler
{
    /** @var Dyna_Agent_Model_Agent */
    protected $_agent;

    /**
     * @param Dyna_Agent_Model_Agent $agent
     * @return bool
     */
    public function setAgent(Dyna_Agent_Model_Agent $agent = null)
    {
        $this->_agent = $agent;
        return $this;
    }

    /**
     * @return Dyna_Agent_Model_Agent
     */
    public function getAgent()
    {
        return $this->_agent;
    }
}
