<?php
/* @var $installer Mage_Eav_Model_Entity_Setup */
$installer  = $this;
/* @var $connection Varien_Db_Adapter_Pdo_Mysql */
$connection = $installer->getConnection();

$installer->startSetup();

$flat_entities = [
    'package/type',
    'package/subtype',
    'dyna_package/packageSubtypeCardinality',
    'dyna_package/packageCreationTypes',
    'dyna_package/packageCreationGroups',
    'dyna_catalog/productVersion',
    'dyna_catalog/productFamily',
    'productmatchrule/rule',
    'omnius_mixmatch/mixmatch',
    'rulematch/rulematch',
    'multimapper/mapper',
    'dyna_multimapper/addon',
    'cataloglog/cataloglog',
    'dyna_bundles/bundle_rules',
    'dyna_bundles/bundle_actions',
    'dyna_bundles/bundle_conditions',
    'dyna_bundles/bundle_hints',
    'dyna_bundles/bundle_processcontext',
    'dyna_bundles/campaign_offer',
    'dyna_bundles/campaign',
    'dyna_bundles/campaign_offer_relation',
    'dyna_bundles/campaign_dealercode',
];
$eav_entities = [
    Mage_Catalog_Model_Product::ENTITY,
    Mage_Catalog_Model_Category::ENTITY,
];
$column = 'stack';
$attribute_name = 'stack';
$attribute_properties = [
    'input' => 'text',
    'type' => 'text',
    'group' => 'General'
];

foreach ($flat_entities as $entity) {
    $table = $installer->getTable($entity);
    if (!$connection->tableColumnExists($table, $column)) {
        $connection->addColumn($table, $column, [
            'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
            'length' => 10,
            'comment' => 'Stack'
        ]);
    }
}
// No need to add it to attribute sets because it won't be displayed in the backoffice
foreach ($eav_entities as $entityTypeId) {
    $attributeId = $installer->getAttributeId($entityTypeId, $attribute_name);
    if (!$attributeId) {
        $attribute_properties['user_defined'] = 1;
        $installer->addAttribute($entityTypeId, $attribute_name, $attribute_properties);
    }
}

$installer->endSetup();
