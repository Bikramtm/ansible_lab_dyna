<?php
/* @var $installer Mage_Eav_Model_Entity_Setup */
$installer  = $this;
/* @var $connection Varien_Db_Adapter_Pdo_Mysql */
$connection = $installer->getConnection();

$installer->startSetup();

$flat_entities = [
    'salesrule/rule',
];
$column = 'stack';
foreach ($flat_entities as $entity) {
    $table = $installer->getTable($entity);
    if (!$connection->tableColumnExists($table, $column)) {
        $connection->addColumn($table, $column, [
            'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
            'length' => 10,
            'comment' => 'Stack'
        ]);
    }
}
$installer->endSetup();
