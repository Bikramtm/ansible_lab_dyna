<?php

/**
 * Class Dyna_Import_Model_Summary
 * @method Dyna_Import_Model_Summary setFilename($filename)
 * @method Dyna_Import_Model_Summary setCategory($import_category)
 * @method Dyna_Import_Model_Summary setCreatedAt($date)
 * @method Dyna_Import_Model_Summary setHeadersValidated(int $i)
 * @method Dyna_Import_Model_Summary setHasErrors(int $i)
 * @method Dyna_Import_Model_Summary setTotalRows(int $i)
 * @method Dyna_Import_Model_Summary setImportedRows(int $i)
 * @method Dyna_Import_Model_Summary setSkippedRows(int $i)
 * @method Dyna_Import_Model_Summary setUpdatedRows(int $i)
 * @method Dyna_Import_Model_Summary setExecutionId(int $i)
 */
class Dyna_Import_Model_Summary extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('import/summary');
    }
}
