<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Import_Model_Import_Product_Abstract
 */
abstract class Dyna_Import_Model_Import_Product_Abstract extends Omnius_Import_Model_Import_Product_Abstract
{
    /** @var string $_logFileName */
    protected $_logFileName = "product_import";
    /** @var string $_logFileExtension */
    protected $_logFileExtension= "log";

    /** @var bool $_debug */
    protected $_debug = false;
    /** @var Dyna_Import_Helper_Data _helper */
    protected $_helper;

    /**
     * Dyna_Import_Model_Import_Product_Abstract constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Gets log file name
     *
     * @return string
     */
    public function getLogFileName() {
        return $this->_logFileName;
    }

    /**
     * Sets log file name
     *
     * @param string $filename
     * @return string
     */
    public function setLogFileName($filename) {
        $this->_logFileName = $filename;
    }

    /**
     * Gets log file extension
     *
     * @return string
     */
    public function getLogFileExtension() {
        return $this->_logFileExtension;
    }

    /**
     * Sets log file extension
     *
     * @param string $filename
     * @return string
     */
    public function setLogFileExtension($extension) {
        $this->_logFileExtension = $extension;
    }

    /**
     * Sets log file
     *
     * @param string $filename
     * @return string
     */
    public function saveImportLogFile($filename = false, $extension = false) {
        $filename = $filename === false ? $this->_logFileName : $filename;
        $extension = $extension === false ? $this->_logFileExtension : $extension;
        $this->_helper->setImportLogFile($filename . '.' . $extension);
    }

    /**
     * Save data for multiselect attributes after mapping the provided values to their corresponding option_id
     * If the provided value does not exist, it is added to the attribute option list
     *
     * @param Mage_Catalog_Model_Product $product
     * @param array $data
     * @param string $code
     */
    protected function _setMultiSelectData($product, $data, $code)
    {
        $this->_log('Setting multiselect attribute ' . $code . ' to value ' . $data[$code], true);
        $split = preg_quote($this->_multiselectSplit);
        $values = preg_split("/{$split}/", $data[$code]);
        if (empty($values)) {
            $this->_log('No values found for multiselect attribute ' . $code, true);

            return;
        }
        $valueCodes = [];
        if (!isset($this->_attributeOptions[$code])) {
            $this->_attributeOptions[$code] = $this->_getAttrOptions($code);
        }
        foreach ($values as $value) {
            $value = strtolower(trim($value));
            if (!isset($this->_attributeOptions[$code][$value])) {
                $optionId = $this->_addAttrOption($code, $value);
                if ($optionId) {
                    $this->_attributeOptions[$code][$value] = $optionId;
                }
            }
            if (isset($this->_attributeOptions[$code][$value])) {
                $valueCodes[] = $this->_attributeOptions[$code][$value];
            }
        }

        $product->setData($code, $valueCodes);
    }

    /**
     * Log function
     *
     * @param string $msg
     * @param bool $verbose
     */
    public function _log($msg, $verbose = false)
    {
        if($verbose && !$this->_debug) {
            return;
        }
        $this->_helper->logMsg('[' . $this->_currentProductSku . '] ' . $msg);
    }

    public function _logError($msg) {
        $this->_helper->logMsg($msg);
    }
}
