<?php
/**
 * ISAAC Dyna_Import
 *
 * @category ISAAC
 * @package Dyna_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */

abstract class Dyna_Import_Model_Generator_Fixed_Definition_ProductAbstract extends Dyna_Import_Model_Generator_ProductDefinitionAbstract
{
    /** @var array */
    protected $dateTimeFields = [
        'effective_date',
        'expiration_date',
        'fn_new_start_date_nc',
        'fn_new_end_date_nc',
        'fn_change_start_date_bc',
        'fn_change_end_date_bc',
        'portfolio',
    ];

    /** @var array */
    protected $renameFields = [
        'gui_name' => 'hardware_gui_name',
        'name' => 'internal_hardware_name',
        'fn_new_start_date_nc' => 'fn_new_start_sales_date_new_customer',
        'fn_new_end_date_nc' => 'fn_new_end_sales_date_new_customer',
        'fn_change_start_date_bc' => 'fn_change_start_sales_date_base_customer',
        'fn_change_end_date_bc' => 'fn_change_end_sales_date_base_customer',
        'isdn_network_capable_nt_spi' => 'isdn_network_capable(nt-split_integrated)',
        'hw_sub_type' => 'hw_sub-type',
        'monthly_price' => 'monthly_price(rental_or_surcharge)',
        'allowed_unsubsidized_sales' => 'allow_for_unsubsidized_sales',
        'vdsl2_vectoring' => 'vdsl2-vectoring',
        'vdsl2_legacy' => 'vdsl2-vectoring',
        'main_access_technology' => 'access_technology',
        'isdn_enabled' => 'isdn-enabled(s0)',
        'ipv6_enabled' => 'i_pv6enabled',
        'adsl2_plus' => 'adsl2+',
        'acs_enabled' => 'acs-enabled',
        'tax_class_id' => 'tax_class',
        'maf' => 'monthly_price',
    ];

    /** @var array */
    protected $reservedAttributeCodes = [
        'visibility',
        'Product in websites',
        'Product Dealer Visibility',
        'product_visibility_strategy',
        '_product_websites',
        'attribute_set',
        'vat',
    ];

    /** @var array */
    protected $reservedDataValues = [
        '_attribute_set',
        '_entity_type',
        '_stock_item',
        '_websites',
    ];

    /** @var int */
    protected $status = 1;

    /**
     *
     * @param string $startDate
     * @return string
     */
    private function getFnStartDate(string $startDate)
    {
        $startDate = in_array(
            $startDate,
            array(
                'keine Vermarktung (GK Artikel)',
                'keine Vermarktung',
            )
        ) ? '' : $startDate;

        if (preg_match('/[0-9]{2}.[0-9]{2}.[0-9]{4}/', $startDate, $matches)) {
            $startDate = current($matches);
        }

        return $startDate;
    }

    protected function setAttributeSet()
    {
        if (isset($this->data['attribute_set'])) {
            $this->data['_attribute_set'] = trim($this->data['attribute_set']);
        } else {
            $attributeSet = 'FN_Unknown';

            if (isset($this->data['hw_type'])) {
                switch ($this->data['hw_type']) {
                    case 'IAD' :
                    case 'STB' :
                    case 'SIM' :
                        $attributeSet = 'FN_Hardware';
                        break;
                    case 'ACC' :
                        $attributeSet = 'FN_Accessory';
                        break;
                    case 'NTD' :
                    case 'VMO' :
                    case 'LMO' :
                        $attributeSet = 'Default_Set';
                        break;
                    default:
                        break;
                }
            }

            $this->data['_attribute_set'] = $attributeSet;
        }
    }

    protected function setFnChangeStartDateBc()
    {
        if(!empty($this->data['fn_change_start_date_bc'])) {
            $this->data['fn_change_start_date_bc'] = $this->getFnStartDate($this->data['fn_change_start_date_bc']);
        }
    }

    protected function setFnNewStartDateNc()
    {
        if(!empty($this->data['fn_new_start_date_nc'])) {
            $this->data['fn_new_start_date_nc'] = $this->getFnStartDate($this->data['fn_new_start_date_nc']);
        }
    }

    protected function setGuiName()
    {
        if(empty($this->data['gui_name'])) {
            $this->data['gui_name'] = $this->data['name'];
        }
    }

    protected function setName()
    {
        if(empty($this->data['name']) && !empty($this->data['gui_name'])) {
            $this->data['name'] = $this->data['gui_name'];
        }
    }

    protected function setPrices()
    {
        if (!empty($this->data['price'])) {
            $this->data['price'] = $this->importHelper->formatPrice($this->data['price']);
        }

        if (!empty($this->data['price_subsidized'])) {
            $this->data['price_subsidized'] = $this->importHelper->formatPrice($this->data['price_subsidized']);
        }

        if (!empty($this->data['price_unsubsidized'])) {
            $this->data['price_unsubsidized'] = $this->importHelper->formatPrice($this->data['price_unsubsidized']);
        }

        if (!empty($this->data['monthly_price'])) {
            $this->data['monthly_price'] = $this->importHelper->formatPrice($this->data['monthly_price']);
        }

        if (!empty($this->data['maf'])) {
            $this->data['maf'] = $this->importHelper->formatPrice($this->data['maf']);
        }

        if (empty($this->data['price']) && !empty($this->data['ot_value_euro_brutto'])) {
            $this->data['price'] = $this->importHelper->formatPrice($this->data['ot_value_euro_brutto']);
        }

        if (!isset($this->data['price']) || $this->data['price'] === '') {
            $this->data['price'] = 0;
            $this->logMessage('[NOTICE] Did not receive any value for price column. Setting price to 0');
        }
    }

    protected function setSku()
    {
        if(empty($this->data['sku']) && !empty($this->data['hw_materianumer'])) {
            $this->data['sku'] = $this->data['hw_materianumer'];
        }

        $this->data['sku'] = trim($this->data['sku']);
    }

    protected function setStatus()
    {
        if(isset($this->data['status']) && $this->data['status']) {
            $this->data['status'] = 1;
        } else {
            $this->data['status'] = 2;
        }
    }

    protected function setTaxClass()
    {
        $this->data['tax_class_id'] = $this->getVatTaxClass($this->data['vat']);
    }

    public function transform(array $data)
    {
        $this->setData($data);

        // set defaults
        $this->setWebsites();
        $this->setStatus();

        // transformers
        $this->renameFields();
        $this->setSku();
        $this->setName();
        $this->setGuiName();
        $this->setFnNewStartDateNc();
        $this->setFnChangeStartDateBc();
        $this->setDateTimeFields();
        $this->setPrices();
        $this->setAttributeSet();
        $this->setProductVisibility();
        $this->setTaxClass();
        $this->setCheckoutProduct();

        // set data
        $this->setAttributeSet();
        $this->setType();
        $this->setUrlKey();
        $this->setProductVisibilityStrategy();
        $this->setVisibility();
        $this->setIsDeleted();
        $this->setStock();

        // unset reserved
        $this->unsetReservedAttributeCodes();

        // set correct data for attribute type
        $this->setAttributeTypeValues();

        return $this->createCatalogProduct();
    }

    /**
     * @param array $data
     * @return bool
     */
    public function validate(array $data)
    {
        $this->setData($data);

        if(!isset($this->data['sku'])) {
            $this->logMessage('Skipping line without sku');
            return false;
        }

        if(!isset($this->data['name'])) {
            $this->logMessage('Skipping line without name');
            return false;
        }

        if (!isset($this->data['package_type'])) {
            $this->logMessage('[ERROR] (SKU ' . $this->data['sku'] . ') Skipped entire product because of empty package type');
            return false;
        }

        if (!isset($this->data['package_subtype'])) {
            $this->logMessage('[ERROR] (SKU ' . $this->data['sku'] . ') Skipped entire product because of empty package subtype');
            return false;
        }

        $packageTypes = explode(',', $this->data['package_type']);

        foreach ($packageTypes as $packageType) {
            if (!in_array(strtolower($packageType), Dyna_Cable_Model_Product_Attribute_Option::packageTypeOptions())) {
                $this->logMessage('[ERROR] (SKU ' . $this->data['sku'] . ') Skipped entire product because of invalid package type value: ' . $packageType . ' (case insensitive)');
                return false;
            }
        }

        if (empty($this->data['package_subtype']) || !in_array($this->data['package_subtype'],
                Dyna_Cable_Model_Product_Attribute_Option::packageSubtypeOptions())
        ) {
            $this->logMessage('(SKU ' . $this->data['sku'] . ') Skipped entire product because of invalid package subtype value: ' . $this->data['package_subtype'] . ' (case insensitive)');
            return false;
        }

        return true;
    }
}