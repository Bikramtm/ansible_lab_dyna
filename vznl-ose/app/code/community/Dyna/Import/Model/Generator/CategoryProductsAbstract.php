<?php
/**
 * ISAAC Dyna_Import
 *
 * @category ISAAC
 * @package Dyna_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */

class Dyna_Import_Model_Generator_CategoryProductsAbstract implements ISAAC_Import_Generator
{
    /** @var bool */
    protected $canExecute;

    /** @var string */
    protected $defaultCategoryCode;

    /** @var Dyna_Import_Helper_File */
    protected $fileHelper;

    /** @var string */
    protected $importFile;

    /** @var ISAAC_Import_Helper_Data */
    protected $importHelper;

    /** @var array */
    protected $productSkus = [];

    /**
     * Dyna_Import_Model_Generator_Product constructor.
     */
    public function __construct()
    {
        $this->fileHelper = Mage::helper('dyna_import/file');
        $this->importHelper = Mage::helper('isaac_import');
        $this->importFile = $this->fileHelper->getFilePath($this->importFile);

        $this->canExecute = $this->canExecute();

        $categoryCollection = Mage::getResourceModel('catalog/category_collection');
        $categoryCollection->addAttributeToSelect('name');
        $categoryCollection->addAttributeToFilter('path', Dyna_Import_Model_Generator_CategoryDefinitionAbstract::DEFAULT_CATEGORY_PATH);
        $this->defaultCategoryCode = $this->buildCategoryCode($categoryCollection->getFirstItem());

        $this->productSkus = Mage::getSingleton('core/resource')->getConnection('core_read')->fetchPairs(
            "select entity_id, sku from catalog_product_entity"
        );
    }

    /**
     * @return bool
     */
    public function canExecute()
    {
        if (!file_exists($this->importFile)) {
            return false;
        }

        return true;
    }

    /**
     * @param Dyna_Catalog_Model_Category $category
     * @param array $tree
     * @return string
     */
    private function buildCategoryCode(Dyna_Catalog_Model_Category $category, $tree = [])
    {
        $tree[] = $category->getName();

        /** @var Dyna_Catalog_Model_Category $parentCategory */
        $parentCategory = $category->getParentCategory();

        if ($parentCategory->getId()) {
            return $this->buildCategoryCode($parentCategory, $tree);
        }

        return implode(Dyna_Import_Model_Generator_CategoryDefinitionAbstract::TREE_DELIMITER, array_reverse($tree));
    }

    /**
     * @inheritdoc
     */
    public function getIterator()
    {
        if (!$this->canExecute) {
            return new ArrayIterator();
        }

        $csvDictReader = new ISAAC_Import_CSV_Dict_Reader($this->importFile);
        $csvDictReader->setDelimiter(';');
        $csvDictReader->setSkipEmptyLines(true);

        $productFilterIterator = new CallbackFilterIterator($csvDictReader, function ($data) {
            return $this->validate($data);
        });

        $products = [];
        foreach ($productFilterIterator as $product) {
            $products[$product['sku']][] = implode(
                Dyna_Import_Model_Generator_CategoryDefinitionAbstract::TREE_DELIMITER,
                [$this->defaultCategoryCode, $product['category_tree']]
            );
        }

        $productsIterator = new ArrayIterator();
        foreach ($products as $sku => $categories) {
            $productsIterator->append($this->createCatalogProduct($sku, $categories));
        }

        return $productsIterator;
    }

    public function cleanup()
    {
    }

    /**
     * @param string $sku
     * @param array $categories
     * @return ISAAC_Import_Model_Import_Value_Catalog_Product
     */
    public function createCatalogProduct($sku, array $categories)
    {
        $productData['_category_codes'] = array_unique(array_merge($categories, $this->getProductCategories($sku)));

        $importValue = new ISAAC_Import_Model_Import_Value_Catalog_Product($sku);
        $importValue->setLoadingFlags([
            ISAAC_Import_Model_Import_Value_Catalog_Product::CREATE,
            ISAAC_Import_Model_Import_Value_Catalog_Product::UPDATE
        ]);
        $importValue->addDataValues($productData);

        return $importValue;
    }

    /**
     * @param string $sku
     * @return array
     */
    protected function getProductCategories(string $sku)
    {
        $categories = [];
        /** @var Mage_Catalog_Model_Product $product */
        $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
        $productCategories = $product->getCategoryCollection();
        foreach ($productCategories as $productCategory) {
            /** @var Mage_Catalog_Model_Category $category */
            $category = Mage::getModel('catalog/category')->load($productCategory->getId());
            $categories[] = $category->getCategoryCode();
        }
        return $categories;
    }

    /**
     * @param array $data
     * @return bool
     */
    protected function validate(array $data)
    {
        if (!isset($data['sku'])) {
            return false;
        }

        if (!isset($data['category_tree'])) {
            return false;
        }

        if (!in_array($data['sku'], $this->productSkus)) {
            return false;
        }

        return true;
    }
}