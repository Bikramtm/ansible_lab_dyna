<?php
/**
 * ISAAC Dyna_Import
 *
 * @category ISAAC
 * @package Dyna_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */

abstract class Dyna_Import_Model_Generator_Mobile_Definition_ProductAbstract extends Dyna_Import_Model_Generator_ProductDefinitionAbstract
{
    /** @var array */
    protected $bbParseFields = [
        'service_description'
    ];

    /** @var array */
    protected $dateTimeFields = [
        'effective_date',
        'expiration_date',
    ];

    /** @var array */
    protected $renameFields = [
        'bank_collection_mandatory' => 'bank_collection',
        'sku' => 'kias_code',
    ];

    /** @var array */
    protected $reservedAttributeCodes = [
        'visibility',
        'Product in websites',
        'Product Dealer Visibility',
        'product_visibility_strategy',
        '_product_websites',
        'attribute_set',
        'vat',
    ];

    /** @var array */
    protected $reservedDataValues = [
        '_attribute_set',
        '_entity_type',
        '_websites',
        '_specifications',
        '_attachments',
        '_up_sells',
        '_cross_sells',
        '_related_links',
        '_stock_item',
        '_images',
        '_category_codes',
        '_configurable_products_data',
        '_product_options',
    ];

    protected function setAttributeSet()
    {
        $this->data['_attribute_set'] = $this->data['attribute_set'];
    }

    protected function setName()
    {
        if (empty($this->data['name'])) {
            $this->data['name'] = $this->data['sku'];
        }
    }

    protected function setPackageSubtype()
    {
        // SKU format as specified in CAT-11_Product Catalogue HLD_version_2.4
        if ($this->data['package_subtype'] == 'add on') {
            $this->data['package_subtype'] = 'Addon';
        }
    }

    protected function setPrepaid()
    {
        if (!empty($this->data['prepaid'])) {
            // leave data as-is
        } elseif (isset($this->data['attribute_set'])
            && ($this->data['attribute_set'] == 'prepaid_package' || $this->data['attribute_set'] == 'mobile_prepaid_hardware')
        ) {
            $this->data['prepaid'] = 'Yes';
        } else {
            unset($this->data['prepaid']);
        }
    }

    protected function setPrices()
    {
        if (!empty($this->data['one_time_charge']) && empty($this->data['price'])) {
            $this->data['price'] = $this->data['one_time_charge'];
        }

        if (!empty($this->data['net_material_price'])) {
            $this->data['net_material_price'] = $this->importHelper->formatPrice($this->data['net_material_price']);
        }

        if (!empty($this->data['gross_material_price'])) {
            if (empty($this->data['price'])) {
                $this->data['price'] = $this->data['gross_material_price'];
            }
            $this->data['gross_material_price'] = $this->importHelper->formatPrice($this->data['gross_material_price']);
        }

        if (!empty($this->data['maf'])) {
            $this->data['maf'] = $this->importHelper->formatPrice($this->data['maf']);
        }

        if (!isset($this->data['price']) || $this->data['price'] === "") {
            $this->data['price'] = 0;
            $this->logMessage("[NOTICE] Did not receive any value for price column. Setting price to 0");
        }

        $this->data['price'] = $this->importHelper->formatPrice($this->data['price']);
    }

    protected function setStatus()
    {
        if(isset($this->data['status']) && $this->data['status']) {
            $this->data['status'] = 1;
        } else {
            $this->data['status'] = 2;
        }
    }

    protected function setSku()
    {
        $this->data['sku'] = trim($this->data['sku']);
    }

    protected function setTaxClassId()
    {
        $this->data['tax_class_id'] = $this->getVatTaxClass($this->data['vat']);
    }

    /**
     * @param array $data
     * @return ISAAC_Import_Model_Import_Value_Catalog_Product
     */
    public function transform(array $data)
    {
        $this->setData($data);

        // transformers
        $this->setSku();
        $this->setPackageSubtype();
        $this->setTaxClassId();
        $this->renameFields();
        $this->setPrices();
        $this->setName();
        $this->setDateTimeFields();
        $this->setStatus();
        $this->setPrepaid();
        $this->bbParseFields();
        $this->setProductVisibility();
        $this->setAttributeSet();
        $this->setCheckoutProduct();

        // set defaults
        $this->setWebsites();
        $this->setStock();
        $this->setUrlKey();
        $this->setProductVisibilityStrategy();
        $this->setVisibility();
        $this->setIsDeleted();
        $this->setType();

        // unset reserved
        $this->unsetReservedAttributeCodes();

        // set correct data for attribute type
        $this->setAttributeTypeValues();

        return $this->createCatalogProduct();
    }

    /**
     * @param array $data
     * @return bool
     */
    public function validate(array $data)
    {
        $this->setData($data);

        if(!isset($this->data['sku'])) {
            $this->logMessage('Skipping line without sku');
            return false;
        }

        if(!isset($this->data['name'])) {
            $this->logMessage('Skipping line without name');
            return false;
        }

        /** @var Dyna_Package_Helper_Data $packageHelper */
        $packageHelper = Mage::helper('dyna_package');
        $packageTypes = $packageHelper->getPackageTypes();

        if (empty($this->data['package_type']) || !!in_array((strtolower($this->data['package_type'])), array_map('strtolower', $packageTypes))) {
            $this->logMessage('(SKU ' . $this->data['sku'] . ') Skipped entire product because of invalid package type value: ' . $this->data['package_type'] . ' (case insensitive)');
            return false;
        }

        if (empty($this->data['package_subtype']) || empty($packageHelper->getPackageSubtype($this->data['package_type'], $this->data['package_subtype'], false, []))) {
            $this->logMessage('(SKU ' . $this->data['sku'] . ') Skipped entire product because of invalid package subtype value: ' . $this->data['package_subtype'] . ' (case insensitive)');
            return false;
        }

        return true;
    }
}