<?php
/**
 * ISAAC Dyna_Import
 *
 * @category ISAAC
 * @package Dyna_Import
 * @copyright Copyright (c) 2017 ISAAC Software Solutions B.V. (https://www.isaac.nl)
 * @author ISAAC Software Solutions B.V. (https://www.isaac.nl)
 */

abstract class Dyna_Import_Model_Generator_CategoryDefinitionAbstract extends Dyna_Import_Model_Generator_DefinitionAbstract
{
    const DEFAULT_CATEGORY_PATH = '1/2';
    const PATH_DELIMITER = '/';
    const TREE_DELIMITER = '->';

    /** @var bool */
    protected $assignProductsAutomitically = true;

    /** @var array */
    protected $categoryTreeList = [];

    /** @var string */
    protected $defaultCategoryCode;

    /** @var string */
    protected $displayMode = 'PRODUCTS';

    /** @var bool */
    protected $isActive = true;

    /** @var array */
    protected $parentCategoriesCodes = [];

    /** @var array */
    protected $productSkus = [];

    /** @var array */
    protected $reservedAttributeCodes = [
        'anchor_category',
        'Description', // yes: caseSensitive!
        'category_tree',
        'family_category',
        'family_category_sorting',
        'mutually_exclusive',
        'parent_category_code',
        'sku',
    ];

    public function __construct()
    {
        parent::__construct();

        /** @var Dyna_Catalog_Model_Category */
        $this->categoryModel = Mage::getModel('catalog/category');

        $categoryCollection = Mage::getResourceModel('catalog/category_collection');
        $categoryCollection->addAttributeToSelect('name');
        $categoryCollection->addAttributeToFilter('path', self::DEFAULT_CATEGORY_PATH);
        $this->defaultCategoryCode = $this->buildCategoryCode($categoryCollection->getFirstItem());

        $this->productSkus = Mage::getSingleton('core/resource')->getConnection('core_read')->fetchPairs(
            "select entity_id, sku from catalog_product_entity"
        );
    }

    /**
     * @param Dyna_Catalog_Model_Category $category
     * @param array $tree
     * @return string
     */
    private function buildCategoryCode(Dyna_Catalog_Model_Category $category, $tree = [])
    {
        $tree[] = $category->getName();

        /** @var Dyna_Catalog_Model_Category $parentCategory */
        $parentCategory = $category->getParentCategory();

        if($parentCategory->getId()) {
            return $this->buildCategoryCode($parentCategory, $tree);
        }

        return implode(self::TREE_DELIMITER, array_reverse($tree));
    }

    /**
     * @return Generator
     */
    public function createCatalogCategory()
    {
        $importValue = new ISAAC_Import_Model_Import_Value_Catalog_Category($this->data['category_code']);
        $importValue->setLoadingFlags([
            ISAAC_Import_Model_Import_Value_Catalog_Category::CREATE,
            ISAAC_Import_Model_Import_Value_Catalog_Category::UPDATE
        ]);
        $importValue->setParent($this->data['parent_category_code']);

        $this->unsetReservedAttributeCodes();

        $importValue->addDataValues($this->data);

        return $importValue;
    }

    protected function setAllowMutualProducts()
    {
        $this->data['allow_mutual_products'] = '';

        if(isset($this->data['mutually_exclusive'])) {
            $this->data['allow_mutual_products'] = $this->setBooleanData($this->data['mutually_exclusive']);
        }
    }

    protected function setAssignProductsAutomatically()
    {
        $this->data['assign_products_automatically'] = $this->assignProductsAutomitically;
    }

    protected function setCategoryCode()
    {
        $this->data['category_code'] = implode(self::TREE_DELIMITER, [$this->defaultCategoryCode, $this->data['category_tree']]);
    }

    protected function setDescription()
    {
        if(!isset($this->data['description'])) {
            $this->data['description'] = '';
        }
    }

    protected function setDisplayMode()
    {
        if(isset($this->displayMode)) {
            $this->data['display_mode'] = $this->displayMode;
        }
    }

    protected function setIsActive()
    {
        $this->data['is_active'] = false;

        if(isset($this->isActive)) {
            $this->data['is_active'] = $this->setBooleanData($this->isActive);
        }
    }

    protected function setIsAnchor()
    {
        if(!isset($this->data['is_anchor'])) {
            $this->data['is_anchor'] = false;
        } else {
            $this->data['is_anchor'] = $this->setBooleanData($this->data['is_anchor']);
        }
    }

    protected function setIsFamily()
    {
        if(!isset($this->data['is_family'])) {
            $this->data['is_family'] = false;
        } else {
            $this->data['is_family'] = $this->setBooleanData($this->data['is_family']);
        }
    }

    protected function setName()
    {
        $categoryTree = explode(self::TREE_DELIMITER, $this->data['category_tree']);
        $this->data['name'] = array_pop($categoryTree);
    }

    protected function setParentCategoryCode()
    {
        $categoryTree = explode(self::TREE_DELIMITER, $this->data['category_code']);
        array_pop($categoryTree);

        $prefix = false;
        $categoryCode = '';
        foreach($categoryTree as $category) {
            $categoryCode = ($prefix) ? $prefix . self::TREE_DELIMITER . $category : $category;

            if(!isset($this->parentCategoriesCodes[$categoryCode])) {
                /** @var Mage_Catalog_Model_Resource_Category_Collection $categoryCollection */
                $categoryCollection = Mage::getResourceModel('catalog/category_collection');
                $categoryCollection->addAttributeToFilter('category_code', $categoryCode);
                /** @var Dyna_Catalog_Model_Category $dbCategory */
                $dbCategory = $categoryCollection->getFirstItem();

                if(!$dbCategory->getId()) {
                    $dbCategory->setData('category_code', $categoryCode);
                    $dbCategory->setData('name', $category);
                    $dbCategory->setData('display_mode', $this->displayMode);
                    $dbCategory->setData('is_active', $this->isActive);
                    $dbCategory->setData('is_anchor', false);
                    $dbCategory->setData('path', $this->parentCategoriesCodes[$prefix]);
                    $dbCategory = $dbCategory->save();

                    // reload model to determine path
                    $dbCategory = Mage::getModel('catalog/category')->load($dbCategory->getId());
                }

                $path = implode(self::PATH_DELIMITER, $dbCategory->getPathIds());
                $this->parentCategoriesCodes[$categoryCode] = $path;
            }

            $prefix = $categoryCode;
        }

        $this->data['parent_category_code'] = $categoryCode;
    }

    /**
     * @param array $data
     * @return Generator
     */
    public function transform(array $data)
    {
        $this->setData($data);

        // set data
        $this->setName();
        $this->setCategoryCode();
        $this->setParentCategoryCode();
        $this->setIsAnchor();
        $this->setDescription();
        $this->setIsFamily();
        $this->setAllowMutualProducts();

        // set defaults
        $this->setDisplayMode();
        $this->setIsActive();
        $this->setAssignProductsAutomatically();

        return $this->createCatalogCategory();
    }

    /**
     * @param array $data
     * @return bool
     */
    public function validate(array $data)
    {
        $this->setData($data);

        if(!isset($this->data['sku']) || empty($this->data['sku'])) {
            $this->logMessage('Skipping line without sku');
            return false;
        }

        if(!isset($this->data['category_tree']) || empty($this->data['category_tree'])) {
            $this->logMessage('Skipping line without category tree');
            return false;
        }

        if(!in_array($this->data['sku'], $this->productSkus)){
            $this->logMessage('Product with sku ' . $this->data['sku'] . ' does not exist');
            return false;
        }

        if(in_array($this->data['category_tree'], $this->categoryTreeList)) {
            return false;
        }
        $this->categoryTreeList[] = $this->data['category_tree'];

        return true;
    }

    protected function unsetReservedAttributeCodes()
    {
        if($this->reservedAttributeCodes) {
            foreach($this->reservedAttributeCodes as $attributeCode) {
                unset($this->data[$attributeCode]);
            }
        }
    }
}