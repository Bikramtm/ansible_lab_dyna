<?php

/**
 * Class StopWatch
 */
class Dyna_Import_Model_Adapter_Stopwatch
{
    private static $stopwatchStartTimes = array();

    public static function start($timerName = 'default')
    {
        self::$stopwatchStartTimes[$timerName] = microtime(true);
    }

    public static function elapsed($timerName = 'default')
    {
        $duration = microtime(true) - self::$stopwatchStartTimes[$timerName];
        $hours = (int)($duration/60/60);
        $minutes = (int)($duration/60)-$hours*60;
        $seconds = (int)$duration-$hours*60*60-$minutes*60;

        return implode(':', [$hours, $minutes, $seconds]);
    }
}
