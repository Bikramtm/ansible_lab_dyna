<?php

/**
 * Copyright (c) 2017. Dynacommerce B.V.
 */
class Dyna_Import_Model_ExpressionLanguage_Source_Conditions extends ArrayIterator
{
    const AGGREGATOR_INVALID = "Invalid";
    const AGGREGATOR_ANY = "any";
    const AGGREGATOR_ANY_RETURN = "or";
    const AGGREGATOR_ALL = "all";
    const AGGREGATOR_ALL_RETURN = "and";
    const AGGREGATOR_VALUE_TRUE = "true";
    const AGGREGATOR_VALUE_FALSE = "false";

    protected $aggregator;
    protected $value;

    /**
     * Parse a new set of conditions and attach these to current instance
     * @param SimpleXMLElement|SimpleXMLElement[] $data
     * @param $conditionType
     * @return $this
     */
    public function parseConditions(SimpleXMLElement $data, $conditionType)
    {
        $conditionString = (strtolower($conditionType) == 'source') ? 'SourceCondition' : 'TargetCondition';

        // set aggregator on current instance
        $this->aggregator = (strtolower($conditionType) == 'source') ? $this->getAggregator($data) : $this->getTargetAggregator($data);
        // set aggregator value on current instance
        // ConditionValue is not required for Service Target rules
        $this->value = (strtolower($conditionType) == 'source') ? $this->getAggregatorValue($data) : '';
        // check conditions for current source conditions
        if (!isset($data->$conditionString)) {
            $this->getXmlParser()->throwException("Invalid Conditions node: no conditions are found on it");
        }
        // make sure data is encapsulated into an array
        foreach ($data->$conditionString as $condition) {
            $this->parseCondition($condition, $conditionType);
        }
        return $this;
    }

    /**
     * Parse and insert condition in this iterator
     * @param SimpleXMLElement $conditionData
     * @param $conditionType
     * @return $this
     */
    protected function parseCondition(SimpleXMLElement $conditionData, $conditionType)
    {
        /** @var Dyna_Import_Model_ExpressionLanguage_Source_Condition $condition */
        $condition = Mage::getModel('dyna_import/expressionLanguage_source_condition');
        $condition->parseCondition($conditionData, $conditionType);
        $this->append($condition);
        return $this;
    }

    /**
     * Render all conditions into a string
     * @return string
     */
    public function asString()
    {
        $childResults = array();
        /** @var Dyna_Import_Model_ExpressionLanguage_Source_Condition $condition */
        foreach ($this as $condition) {
            $childResults[] = $condition->asString();
        }
        return implode(" " . $this->aggregator . " ", $childResults);
    }

    /**
     * Get import parser
     * @return Dyna_Import_Model_ExpressionLanguage_Parser|Mage_Core_Model_Abstract
     */
    public function getXmlParser()
    {
        return Mage::getSingleton('dyna_import/expressionLanguage_parser');
    }

    /**
     * Parse and validate aggregator value for this node
     * @param SimpleXMLElement $data
     * @return string
     * @throws Dyna_Import_Model_ExpressionLanguage_Exception
     */
    public function getAggregator(SimpleXMLElement $data)
    {
        $aggregator = null;
        switch (true) {
            case !isset($data->Aggregator):
                $this->getXmlParser()->throwException('Missing Aggregator for source conditions (valid: any/all)');
                $aggregator = self::AGGREGATOR_INVALID;
                break;
            case strtolower($data->Aggregator) == self::AGGREGATOR_ANY:
                $aggregator = self::AGGREGATOR_ANY_RETURN;
                break;
            case strtolower($data->Aggregator) == self::AGGREGATOR_ALL:
                $aggregator = self::AGGREGATOR_ALL_RETURN;
                break;
            default:
                $aggregator = $data->Aggregator;
                break;
        }

        return $aggregator;
    }

    /**
     * Parse and validate aggregator value for this node
     * @param SimpleXMLElement $data
     * @return string
     * @throws Dyna_Import_Model_ExpressionLanguage_Exception
     */
    public function getTargetAggregator(SimpleXMLElement $data)
    {
        $aggregator = null;
        switch (true) {
            case !isset($data->TargetAggregator):
                $this->getXmlParser()->throwException('Missing Aggregator for target conditions (valid: having/union)');
                $aggregator = self::AGGREGATOR_INVALID;
                break;
            case strtolower($data->TargetAggregator) == self::AGGREGATOR_ANY:
                $aggregator = self::AGGREGATOR_ANY_RETURN;
                break;
            case strtolower($data->TargetAggregator) == self::AGGREGATOR_ALL:
                $aggregator = self::AGGREGATOR_ALL_RETURN;
                break;
            default:
                $aggregator = $data->TargetAggregator;
                break;
        }

        return $aggregator;
    }

    /**
     * Parse and validate aggregator value
     * @param SimpleXMLElement $data
     * @return string
     * @throws Dyna_Import_Model_ExpressionLanguage_Exception
     */
    public function getAggregatorValue(SimpleXMLElement $data)
    {
        //strtolower($data->ConditionValue) === self::AGGREGATOR_VALUE_TRUE
        // do not move the default case at the end of the cases as it will log warnings in case ConditionValue node is not present in $data
        switch (true) {
            case !isset($data->ConditionValue):
            default:
                $this->getXmlParser()->throwException((!isset($data->ConditionValue) ? 'Missing' : 'Invalid') . ' ConditionValue node for conditions (valid: true/false)');
                return self::AGGREGATOR_INVALID;
            case strtolower($data->ConditionValue) == self::AGGREGATOR_VALUE_TRUE:
                return self::AGGREGATOR_VALUE_TRUE;
            case strtolower($data->ConditionValue) == self::AGGREGATOR_VALUE_FALSE:
                return self::AGGREGATOR_VALUE_FALSE;
        }
    }
}
