<?php
/**
 * Copyright (c) 2017. Dynacommerce B.V.
 */

class Dyna_Import_Model_ExpressionLanguage_Exception extends Exception
{
    /**
     * Overriding constructor due to magento's way of passing arguments encapsulated into an array
     * Dyna_Import_Model_ExpressionLanguage_Exception constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        if (is_array($message)) {
            $code = $message['code'] ?? $code;
            $previous = $message['previous'] ?? $previous;
            $message = $message['message'] ?? $message;
        }
        parent::__construct($message, $code, $previous);

        throw $this;
    }
}
