<?php
/**
 * Copyright (c) 2017. Dynacommerce B.V.
 */

class Dyna_Import_Model_ExpressionLanguage_Source_Condition
{
    const TYPE_SIMPLE = "simple";
    const TYPE_COMPLEX = "complex";
    const CONDITION_VALUE_TRUE = "true";
    const CONDITION_VALUE_FALSE = "false";
    const OPERATOR_CONDITIONS = array(
        'is' => '+expression+',
        'is not'  => '!+expression+',
        'equals or greater than' => '+expression+ >= +conditionValue+',
        'equals or less than' => '+expression+ <= +conditionValue+',
        'greater than' => '+expression+ > +conditionValue+',
        'less than' => '+expression+ < +conditionValue+',
        'contains' => '(+conditionValue+ in +expression+)',
        'does not contain' => '(+conditionValue+ not in +expression+)',
        'is one of' => '(+expression+ in [+conditionValue+])',
        'is not one of' => '(+expression+ not in [+conditionValue+])',
    );

    /** @var Dyna_Import_Model_ExpressionLanguage_Source_Conditions[] */
    protected $conditions = null;
    protected $type = self::TYPE_SIMPLE;
    protected $expression= array();
    protected $operator = array();
    protected $conditionValue = array();
    protected $params = array();

    /**
     * Constructor for current condition
     * Dyna_Import_Model_ExpressionLanguage_Source_Condition constructor.
     * @param SimpleXMLElement $data
     * @param $conditionType
     * @return $this
     */
    public function parseCondition(SimpleXMLElement $data, $conditionType)
    {
        $conditionsString = (strtolower($conditionType) == 'source') ? 'SourceConditions' : 'TargetConditions';

        // complex condition (wrapper over another set of conditions)
        if (isset($data->$conditionsString)) {
            /** @var Dyna_Import_Model_ExpressionLanguage_Source_Conditions $sourceCondition */
            $sourceCondition = Mage::getModel('dyna_import/expressionLanguage_source_conditions');
            $this->conditions = $sourceCondition->parseConditions($data->$conditionsString, $conditionType);
            // complex is used for wrapping string result into parenthesis
            $this->type = self::TYPE_COMPLEX;
        } else {
            // set expression current condition, we'll append params later
            $this->expression = ((array)$data->Type)[0];
            // prepare and add params to current condition
            foreach ($data->Params->Param ?? [] as $param) {
                $this->addParam($param);
            }
            // prepare operator for current condition
            $operator = strtolower(trim($data->Operator));
            if ($operator && !array_key_exists($operator, self::OPERATOR_CONDITIONS)) {
                // throw exception that operator is invalid
                $this->getXmlParser()->throwException("Invalid Operator node received for condition");
            }
            $this->operator = $operator;

            $conditionValue = $data->ConditionValue;
            if (in_array($this->operator, ['is one of', 'is not one of'])) {
                $this->params = [];
                $conditionValue = $data->Params;
            }

            // parse and add condition value/values
            $this->addConditionValue($conditionValue);
        }
        return $this;
    }

    /**
     * Parse and add condition value to current condition
     * @param $value
     * @return $this
     */
    public function addConditionValue(SimpleXMLElement $value)
    {
        $value = current($value);

        if (is_array($value)) {
            $value = implode(", ", $value);
        }

        switch (true) {
            // it is true or false, no single quotes around it
            case in_array(strtolower($value), array(self::CONDITION_VALUE_TRUE, self::CONDITION_VALUE_FALSE)):
            // can be converted to integer, doesn't need single quotes around it
            case (string)(int)$value === $value:
                $this->conditionValue = $value;
                break;
            // it is a list, needs and instead of commas
            case strpos($value, ",") !== false:
                // parsing needed quotes or int conversion
                $values = $this->parseStringToArray($value);
                // contains or does not contains requires values to be converted to 'value1' and 'value2' and 'value3'
                $this->conditionValue = in_array($this->operator, array('contains', 'does not contain'))
                    ? implode(" and ", $values)
                    // otherwise, the remaining operators for a list should be 'is one of' or 'is not one of' so this requires values to be converted to ('value1', 'value2', 'value3')
                    : $this->conditionValue = implode(", ", $values);
                break;
            // default needs single quotes around it
            default:
                $this->conditionValue = "'" . $value . "'";
                break;
        }

        return $this;
    }

    /**
     * Break condition values to array by exploding them
     * @param $string
     * @return array
     */
    protected function parseStringToArray($string)
    {
        $parsedValues = array();
        $values = array_map("trim", explode(",", $string));
        foreach ($values as $value) {
            switch (true) {
                // check for boolean or null
                case in_array(strtolower($value), array("null", "true", "false")):
                    $parsedValues[] = strtolower($value);
                    break;
                // check for int (no single quotes needed)
                case (string)(int)$value === $value:
                    $parsedValues[] = $value;
                    break;
                // add single quotes
                default:
                    $parsedValues[] = "'" . $value . "'";
            }
        }

        return $parsedValues;
    }

    /**
     * Prepare param for insertion into expression
     * @return $this
     */
    protected function addParam(SimpleXMLElement $param)
    {
        if (!property_exists($param, "TargetConditions")) {
            $param = ((array) $param)[0];
            switch (true) {
                // the param needs to be forwarded as null or bool to the Expression Language parser so no quotes around it
                case in_array(strtolower($param), array("null", "true", "false")):
                    $this->params[] = strtolower($param);
                    break;
                // param can be converted to int, so no need for quotes around it
                case (string) (int) $param === $param:
                    $this->params[] = $param;
                    break;
                // default will treat values as string and will encapsulate them between single quotes
                default:
                    $this->params[] = "'" . $param . "'";
            }
        } else {
            $sourceCondition = Mage::getModel('dyna_import/expressionLanguage_source_conditions');
            $this->params[] = $sourceCondition->parseConditions($param->TargetConditions, "target")->asString();
        }

        return $this;
    }

    /**
     * Render this condition to string
     * @return string
     */
    public function asString()
    {
        // if it is a complex condition, then it holds a reference to the conditions instance which will handle multiple conditions by its own aggregator
        if ($this->type === self::TYPE_COMPLEX) {
            $childResult = $this->conditions->asString();
        } else {
            // otherwise, it is a simple conditions whos properties will build up a expression language string
            // add params to expression so it turns into a function call
            $childResult = $this->expression . "(" . implode(", ", $this->params) . ")";

            if ($this->operator) {
                $pattern = self::OPERATOR_CONDITIONS[$this->operator];
                $childResult = str_replace(array("+expression+", "+conditionValue+"), array($childResult, $this->conditionValue), $pattern);
                if (in_array($this->conditionValue, array(self::CONDITION_VALUE_TRUE, self::CONDITION_VALUE_FALSE))) {
                    $childResult .= " == " . $this->conditionValue;
                }
            }
        }

        return $this->type === self::TYPE_COMPLEX ? "(" .  $childResult . ")" : $childResult;
    }

    /**
     * Get import parser
     * @return Dyna_Import_Model_ExpressionLanguage_Parser|Mage_Core_Model_Abstract
     */
    public function getXmlParser()
    {
        return Mage::getSingleton('dyna_import/expressionLanguage_parser');
    }
}
