<?php

class Dyna_Import_Model_Loader_Model_Catalog_Product_Collection extends ISAAC_Import_Model_Loader_Model_Catalog_Product_Collection
{
    /**
     * @return string[]
     */
    public function getMandatoryAttributeCodes()
    {
        if ($this->mandatoryAttributeCodes === null) {
            $attributeCodes = $this->getAttributeCodesWithDefaultValueAndDefaultBackendModel();
            $attributeCodes[] = 'url_key';
            $attributeCodes[] = 'store_id';
            $attributeCodes[] = 'product_code';

            $this->mandatoryAttributeCodes = $attributeCodes;
        }

        return $this->mandatoryAttributeCodes;
    }

    /**
     * @inheritdoc
     */
    public function import()
    {
        $groupByIterator = $this->getGroupByIterator();

        foreach ($groupByIterator as $groupValue => $importValues) {
            $collection = $this->getCollectionByGroupValueAndImportValues($groupValue, $importValues);
            foreach ($importValues as $importValue) {
                /** @var ISAAC_Import_Model_Import_Value $importValue */
                if (array_key_exists($importValue->getIdentifier(), $collection)) {
                    $model = $this->getProductCollectionForSku($groupValue, $importValue->getIdentifier());
                } else {
                    /** @var Dyna_Catalog_Model_Product $model */
                    $model = $this->getNewModelByImportValue($importValue);
                    $collection[$model->getData(Dyna_Import_Model_Generator_ProductDefinitionAbstract::ENTITY_IDENTIFIER)] = $model;
                }

                $this->importModel($importValue, $model);
            }
        }
    }

    /**
     * @return ISAAC_Import_Groupby_Iterator
     */
    protected function getGroupByIterator()
    {
        return new ISAAC_Import_Groupby_Iterator(
            new ArrayIterator($this->getImportValues()),
            function (ISAAC_Import_Model_Import_Value $importValue) {
                /** @var ISAAC_Import_Model_Loader_Model_Collection $this */
                return $this->getGroupValueByImportValue($importValue);
            }
        );
    }

    /**
     * @param $groupValue
     * @param $sku
     * @return Mage_Catalog_Model_Product|mixed
     */
    protected function getProductCollectionForSku($groupValue, $sku)
    {
        $skusAsKeysForCurrentStore = [];
        $attributeCodesToSelectAsKeys = [];
        $storeId = $this->getStoreIdByStoreCode($groupValue);

        /** @var ISAAC_Import_Model_Import_Value_Catalog_Product $importValue */
        foreach ($this->getImportValues() as $importValue) {
            $skusAsKeysForCurrentStore[$importValue->getIdentifier()] = 1;
            foreach ($this->getAttributeCodesToSelect($importValue) as $attributeCode) {
                if (!isset($attributeCodesToSelectAsKeys[$attributeCode])) {
                    $attributeCodesToSelectAsKeys[$attributeCode] = 1;
                }
            }
        }

        /** @var Mage_Catalog_Model_Resource_Product_Collection $productCollection */
        $productCollection = Mage::getResourceModel('catalog/product_collection');
        $productCollection->setStore($storeId);
        $productCollection->addStoreFilter($storeId);
        $productCollection->addAttributeToFilter('sku', $sku);
        $productCollection->addAttributeToSelect('description');
        $productCollection->addAttributeToSelect(array_keys($attributeCodesToSelectAsKeys));
        $productCollection->setFlag('require_stock_items', true);
        $productCollection->addWebsiteNamesToResult();
        $productCollection->addOptionsToResult();
        
        foreach ($productCollection as $product) {
            /** @var Mage_Catalog_Model_Product $product */
            $createdAtAttribute = $this->eavConfig->getAttribute(Mage_Catalog_Model_Product::ENTITY, 'created_at');
            $createdAtAttribute->getBackend()->afterLoad($product);

            $product->setData('store_id', $storeId);
            $product->setData('website_ids', $product->getData('websites'));
            return $product;
        }

        Mage::throwException('error (re)loading product data during import');
    }
}