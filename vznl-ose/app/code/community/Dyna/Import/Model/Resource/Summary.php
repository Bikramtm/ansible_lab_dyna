<?php
class Dyna_Import_Model_Resource_Summary extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('import/summary', 'import_id');
    }
}