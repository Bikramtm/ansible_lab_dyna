<?php
/**  */
class Dyna_Import_Model_Resource_Summary_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('import/summary');
    }
}