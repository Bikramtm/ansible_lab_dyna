<?php

class Dyna_Package_Block_Adminhtml_PackageCreationGroups_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form(
            array(
                "id" => "edit_form",
                "action" => $this->getUrl("*/*/save", array("id" => $this->getRequest()->getParam("id"))),
                "method" => "post",
                "enctype" => "multipart/form-data",
            )
        );
        $form->setUseContainer(true);
        $this->setForm($form);

        $fieldSet = $form->addFieldset(
            "general",
            array(
                "legend" => "Package creation groups"
            )
        );

        $fieldSet->addField(
            "name",
            "text",
            array(
                "name" => "name",
                "label" => "Name",
                "input" => "text",
                "required" => true,
            )
        );

        $fieldSet->addField(
            "gui_name",
            "text",
            array(
                "name" => "gui_name",
                "label" => "GUI Name",
                "input" => "text",
                "required" => true,
            )
        );

        $fieldSet->addField(
            "sorting",
            "select",
            array(
                "name" => "sorting",
                "label" => "Sorting",
                "input" => "text",
                "required" => true,
                "values" => [1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10],
            )
        );

        $packageCreationGroupsData = Mage::registry("package_creation_groups_data");
        if ($packageCreationGroupsData) {
            $form->addValues($packageCreationGroupsData->getData());
        }

        return parent::_prepareForm();
    }
}
