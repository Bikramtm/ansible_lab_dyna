<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Dyna_Package_Block_Adminhtml_PackageCreationTypes_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId("packageCreationTypesGrid");
        $this->setDefaultSort("entity_id");
        $this->setDefaultDir("DESC");
        $this->setSaveParametersInSession(true);
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel("dyna_package/packageCreationTypes")->getCollection();
        $collection
            ->getSelect()
            ->join(
                array('cpt'=> 'catalog_package_types'),
                'cpt.entity_id = main_table.package_type_id',
                array('package_front_end_name' => 'cpt.front_end_name')
            )
            ->join(
                array('pcg'=> 'package_creation_groups'),
                'pcg.entity_id = main_table.package_creation_group_id',
                array('package_creation_group_name' => 'pcg.gui_name')
            );

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * @return self|Mage_Adminhtml_Block_Widget_Grid
     * @throws Exception
     */
    protected function _prepareColumns()
    {
        $helper = Mage::helper('dyna_package');

        $this->addColumn("entity_id", array(
            "header" => $helper->__("ID"),
            "type" => "number",
            "index" => "entity_id",
        ));

        $this->addColumn("name", array(
            "header" => $helper->__("Name"),
            "index" => "name",
        ));

        $this->addColumn("gui_name", array(
            "header" => $helper->__("GUI Name"),
            "index" => "gui_name",
        ));

        $this->addColumn("filter_attributes", array(
            "header" => $helper->__("Filter Attributes"),
            "index" => "filter_attributes",
        ));

        $this->addColumn("package_subtype_filter_attributes", array(
            "header" => $helper->__("Package Subtype Filter Attributes"),
            "index" => "package_subtype_filter_attributes",
        ));

        $this->addColumn("package_type_code", array(
            "header" => $helper->__("Package Type Code"),
            "index" => "package_type_code",
        ));

        $this->addColumn("package_type_id", array(
            "header" => $helper->__("Package Type ID"),
            "index" => "package_front_end_name",
        ));

        $this->addColumn("package_creation_group_id", array(
            "header" => $helper->__("Package Creation Group ID"),
            "index" => "package_creation_group_name",
        ));

        $this->addColumn("sorting", array(
            "header" => $helper->__("Sorting"),
            "index" => "sorting",
        ));

        return parent::_prepareColumns();
    }

    /**
     * @param $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl("*/*/edit", array("id" => $row->getId()));
    }

    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('entity_ids');
        $this->getMassactionBlock()->setUseSelectAll(true);

        return $this;
    }
}
