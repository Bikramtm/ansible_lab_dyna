<?php

class Dyna_Package_Block_Adminhtml_PackageCreationTypes_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        /** @var $helper Dyna_Package_Helper_Data */
        $helper = Mage::helper('dyna_package');
        $form = new Varien_Data_Form(
            array(
                "id" => "edit_form",
                "action" => $this->getUrl("*/*/save", array("id" => $this->getRequest()->getParam("id"))),
                "method" => "post",
                "enctype" => "multipart/form-data",
            )
        );
        $form->setUseContainer(true);
        $this->setForm($form);

        $fieldSet = $form->addFieldset(
            "general",
            array(
                "legend" => "Package creation types"
            )
        );

        $fieldSet->addField(
            "name",
            "text",
            array(
                "name" => "name",
                "label" => "Name",
                "input" => "text",
                "required" => true,
            )
        );

        $fieldSet->addField(
            "gui_name",
            "text",
            array(
                "name" => "gui_name",
                "label" => "GUI Name",
                "input" => "text",
                "required" => true,
            )
        );

        $fieldSet->addField(
            "sorting",
            "select",
            array(
                "name" => "sorting",
                "label" => "Sorting",
                "input" => "text",
                "required" => true,
                "values" => [1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10],
            )
        );

        $fieldSet->addField(
            "filter_attributes",
            "text",
            array(
                "name" => "filter_attributes",
                "label" => "Filter Attributes",
                "input" => "text"
            )
        );

        $fieldSet->addField(
            "package_subtype_filter_attributes",
            "textarea",
            array(
                "name" => "package_subtype_filter_attributes",
                "label" => "Package Subtype Filter Attributes",
                "input" => "textarea"
            )
        );

        $fieldSet->addField(
            "package_type_code",
            "text",
            array(
                "name" => "package_type_code",
                "label" => "Package Type Code",
                "input" => "text",
                "required" => true,
            )
        );

        $fieldSet->addField(
            "package_type_id",
            "select",
            array(
                "name" => "package_type_id",
                "label" => "Package Type ID",
                "input" => "text",
                "required" => true,
                'values' => $helper->getPackageTypesId()
            )
        );

        $fieldSet->addField(
            "package_creation_group_id",
            "select",
            array(
                "name" => "package_creation_group_id",
                "label" => "Package Creation Group ID",
                "input" => "text",
                "required" => true,
                'values' => $helper->getPackageCreationGroups()
            )
        );

        $packageCreationTypesyData = Mage::registry("package_creation_types_data");
        if ($packageCreationTypesyData) {
            $form->addValues($packageCreationTypesyData->getData());
        }

        return parent::_prepareForm();
    }
}
