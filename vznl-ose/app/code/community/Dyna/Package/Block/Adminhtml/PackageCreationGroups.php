<?php

class Dyna_Package_Block_Adminhtml_PackageCreationGroups extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = "adminhtml_packageCreationGroups";
        $this->_blockGroup = "dyna_package";
        $this->_headerText = Mage::helper("dyna_package")->__("Package Creation Groups");
        $this->_addButtonLabel = Mage::helper("dyna_package")->__("Add New Package Creation Group");
        parent::__construct();
    }
}
