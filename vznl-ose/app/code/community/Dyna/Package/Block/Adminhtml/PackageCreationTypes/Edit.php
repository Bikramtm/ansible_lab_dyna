<?php

class Dyna_Package_Block_Adminhtml_PackageCreationTypes_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        $this->_blockGroup = "dyna_package";
        $this->_controller = "adminhtml_packageCreationTypes";
        $this->_headerText = Mage::helper("dyna_package")->__("Add Package Creation Type");
        $this->_mode = "edit_tab";
        $this->_updateButton("save", "label", Mage::helper("dyna_package")->__("Save type"));

        parent::__construct();
    }
}
