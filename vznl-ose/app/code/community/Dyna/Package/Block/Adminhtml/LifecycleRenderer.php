<?php

class Dyna_Package_Block_Adminhtml_LifecycleRenderer extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    /**
     * Renders grid column for package type visibility
     * @param   Varien_Object $row
     * @return  string
     */
    public function render(Varien_Object $row)
    {
        $lifeCycleValue = $row->getPackageSubtypeLifecycleStatus();
        $lifeCycleValues = Dyna_Catalog_Model_Lifecycle::getLifecycleTypes();
        return  $lifeCycleValues[$lifeCycleValue] ?? "-";
    }
}
