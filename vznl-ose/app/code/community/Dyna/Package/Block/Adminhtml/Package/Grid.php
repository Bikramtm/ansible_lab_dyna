<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Dyna_Package_Block_Adminhtml_Package_Grid extends Omnius_Package_Block_Adminhtml_Package_Grid
{
    /**
     * @return $this
     * @throws Exception
     */
    protected function _prepareColumns()
    {
        parent::_prepareColumns();
        $this->addColumn("ignored_by_compatibility_rules", array(
            "header" => Mage::helper("dyna_package")->__("Ignored by compatibility rules"),
            "index" => "ignored_by_compatibility_rules",
            "renderer" => "dyna_package/adminhtml_package_ignored",
            'type' => 'options',
            'options' => self::getIgnoredCompatibilityOptions(),
        ));
        $this->removeColumn('is_visible');

        $this->addColumn("package_subtype_lifecycle_status", array(
            "header" => Mage::helper("omnius_package")->__("Lifecycle status"),
            "index" => "package_subtype_lifecycle_status",
            "searchable" => false,
            "renderer" => "dyna_package/adminhtml_lifecycleRenderer",
            'type' => 'options',
            'options' => Dyna_Catalog_Model_Lifecycle::getLifecycleTypes(),
        ));

        $this->addColumn("manual_override_allowed", array(
            "header" => Mage::helper("dyna_package")->__("Manual Override Allowed"),
            "index" => "manual_override_allowed",
            "renderer" => "dyna_package/adminhtml_package_manualOverrideAllowed",
            'type' => 'options',
            'options' => self::getManualOverrideAllowedOptions(),
        ));

        return $this;
    }

    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {
        return $this;
    }

    public static function getPackageSubtypeVisibilityOptions()
    {
        $options[] = array('value' => Dyna_Package_Model_PackageSubtype::PACKAGE_SUBTYPE_VISIBILITY_INVENTORY, 'label' => Mage::helper("dyna_package")->__("Inventory"));
        $options[] = array('value' => Dyna_Package_Model_PackageSubtype::PACKAGE_SUBTYPE_VISIBILITY_CONFIGURATOR, 'label' => Mage::helper("dyna_package")->__("Configurator"));
        $options[] = array('value' => Dyna_Package_Model_PackageSubtype::PACKAGE_SUBTYPE_VISIBILITY_SHOPPING_CART, 'label' => Mage::helper("dyna_package")->__("Shopping cart"));
        $options[] = array('value' => Dyna_Package_Model_PackageSubtype::PACKAGE_SUBTYPE_VISIBILITY_EXTENDED_SHOPPING_CART, 'label' => Mage::helper("dyna_package")->__("Extended shopping cart"));
        $options[] = array('value' => Dyna_Package_Model_PackageSubtype::PACKAGE_SUBTYPE_VISIBILITY_ORDER_OVERVIEW, 'label' => Mage::helper("dyna_package")->__("Order overview"));
        $options[] = array('value' => Dyna_Package_Model_PackageSubtype::PACKAGE_SUBTYPE_VISIBILITY_VOICELOG, 'label' => Mage::helper("dyna_package")->__("Voice log"));
        $options[] = array('value' => Dyna_Package_Model_PackageSubtype::PACKAGE_SUBTYPE_VISIBILITY_OFFER_PDF, 'label' => Mage::helper("dyna_package")->__("Offer PDF"));
        $options[] = array('value' => Dyna_Package_Model_PackageSubtype::PACKAGE_SUBTYPE_VISIBILITY_CALL_SUMMARY, 'label' => Mage::helper("dyna_package")->__("Call summary"));
        $options[] = array('value' => Dyna_Package_Model_PackageSubtype::PACKAGE_SUBTYPE_VISIBILITY_CHECKOUT, 'label' => Mage::helper("dyna_package")->__("Checkout"));

        return $options;
    }

    public static function getIgnoredCompatibilityOptions()
    {
        return array(
            0 => "No",
            1 => "Yes"
        );
    }

    public static function getManualOverrideAllowedOptions()
    {
        return array(
            0 => "False",
            1 => "True"
        );
    }
}
