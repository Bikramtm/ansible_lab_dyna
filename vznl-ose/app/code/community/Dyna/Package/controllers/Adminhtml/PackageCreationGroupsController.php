<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Dyna_Package_Adminhtml_PackageCreationGroupsController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu("catalog/package")
            ->_addBreadcrumb(
                Mage::helper("adminhtml")->__("Package Creation Groups"),
                Mage::helper("adminhtml")->__("Package Creation Groups")
            );
        return $this;
    }

    public function indexAction()
    {
        $this->_title($this->__("Package Creation Groups"));
        $this->_title($this->__("Manage Package Creation Groups"));

        $this->_initAction();
        $this->renderLayout();
    }

    //Patch for SUPEE-6285
    protected function _isAllowed()
    {
        return true;
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function editAction()
    {
        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("dyna_package/packageCreationGroups")->load($id);

        if ($model->getId()) {
            Mage::register("package_creation_groups_data", $model);

            $this->_title($this->__("Package creation group"));
            $this->_title($this->__("Edit package creation group"));

            $this->_initAction();
            $this->renderLayout();
        } else {
            $this->_initAction();
            $this->renderLayout();
        }
    }

    public function saveAction()
    {
        $postData = $this->getRequest()->getPost();

        if ($postData) {
            $model = Mage::getModel("dyna_package/packageCreationGroups");

            if ($this->getRequest()->getParam("id")) {
                $existingOption = $model->load($this->getRequest()->getParam("id"));
                $existingOption->addData($postData);

                $existingOption->save();
            } else {
                $model->setData($postData);
                $model->save();
            }
        }

        $this->_redirect("*/*/");
    }

    /**
     * Method that handles deletion of package type
     */
    public function deleteAction()
    {
        $params = $this->getRequest()->getParam("id");
        Mage::getModel('dyna_package/packageCreationGroups')
            ->load($params)
            ->delete();

        $this->_redirect("*/*/");
    }
}
