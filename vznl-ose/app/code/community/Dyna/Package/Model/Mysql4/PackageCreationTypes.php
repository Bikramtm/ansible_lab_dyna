<?php

/**
 * Class Dyna_Package_Model_Mysql4_PackageCreationTypes
 */
class Dyna_Package_Model_Mysql4_PackageCreationTypes extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {
        $this->_init('dyna_package/packageCreationTypes', 'entity_id');
    }
}
