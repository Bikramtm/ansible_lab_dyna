<?php

/**
 * Class Dyna_Package_Model_Mysql4_PackageCreationGroups
 */
class Dyna_Package_Model_Mysql4_PackageCreationGroups extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {
        $this->_init('dyna_package/packageCreationGroups', 'entity_id');
    }
}
