<?php

/**
 * Class Dyna_Package_Model_Mysql4_PackageCreationTypesProcessContext_Collection
 */
class Dyna_Package_Model_Mysql4_PackageCreationTypesProcessContext_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        $this->_init('dyna_package/packageCreationTypesProcessContext');
    }
}
