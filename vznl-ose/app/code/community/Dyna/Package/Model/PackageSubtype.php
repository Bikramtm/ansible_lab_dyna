<?php

/**
 * Class Dyna_Package_Model_PackageSubtype
 * @method string getFrontEndName() Returns the front_end_name (e.g. "Besonderheiten zu Ihrem Tarif")
 * Reference to the PackageType
 * @method string getTypePackageId() Reference to the PackageType
 */
class Dyna_Package_Model_PackageSubtype extends Omnius_Package_Model_PackageSubtype
{
    CONST STR_ID = "package_code";
    CONST PACKAGE_SUBTYPE_VISIBILITY_INVENTORY = 'inventory';
    CONST PACKAGE_SUBTYPE_VISIBILITY_CONFIGURATOR = 'configurator';
    CONST PACKAGE_SUBTYPE_VISIBILITY_SHOPPING_CART = 'shopping_cart';
    CONST PACKAGE_SUBTYPE_VISIBILITY_EXTENDED_SHOPPING_CART = 'extended_shopping_cart';
    CONST PACKAGE_SUBTYPE_VISIBILITY_ORDER_OVERVIEW = 'order_overview';
    CONST PACKAGE_SUBTYPE_VISIBILITY_VOICELOG = 'voice_log';
    CONST PACKAGE_SUBTYPE_VISIBILITY_OFFER_PDF = 'offer_pdf';
    CONST PACKAGE_SUBTYPE_VISIBILITY_CALL_SUMMARY = 'call_summary';
    CONST PACKAGE_SUBTYPE_VISIBILITY_CHECKOUT = 'checkout';

    public function getPackageSubtypeVisibilityForEditForm()
    {
        $options = [];
        $visibilityValues = $this->getPackageSubtypeVisibility();
        $visibilityArray = explode(',',$visibilityValues);

        if(!$visibilityArray) {
            return $options;
        }
        foreach ($visibilityArray as $visibilityValue) {
            $options[$visibilityValue] = trim($visibilityValue);
        }

        return $options;
    }

    public function getVisibleConfigurator()
    {
        $visibilityValues = $this->getPackageSubtypeVisibility();
        $visibilityArray = explode(',',$visibilityValues);

        if(!$visibilityArray) {
            return false;
        }
        foreach ($visibilityArray as $visibilityValue) {
            if(trim($visibilityValue) == Dyna_Package_Model_PackageSubtype::PACKAGE_SUBTYPE_VISIBILITY_CONFIGURATOR) {
                return true;
            }
        }

        return false;
    }

    /**
     * Get the package_subtype_visibility for a package subtype determined by the package type and package subtype code
     *
     * @param int $packageTypeId
     * @param  $packageSubtypeCode
     * @return string
     */
    public function getPackageSubtypeVisibilityByTypeAndSubtype($packageTypeId, $packageSubtypeCode)
    {
        /**
         * @var Dyna_Package_Model_PackageSubtype $packageSubtype
         */
        $packageSubtype = $this->getCollection()
            ->addFieldToFilter('type_package_id', ['eq' => $packageTypeId])
            ->addFieldToFilter(self::STR_ID, ["eq" => ucfirst(trim($packageSubtypeCode))])
            ->getFirstItem();
        return $packageSubtype->getPackageSubtypeVisibility();
    }

    /**
     * @param $packageType
     * @return array|mixed
     */
    public function getSkippedSubTypes($packageType)
    {
        //get cache
        /** @var Dyna_Cache_Model_Cache $cache */
        $cache = Mage::getSingleton('dyna_cache/cache');
        $cacheKey = sprintf('skipped_subtypes_from_compatibility_rules_for_package_%s', $packageType);

        $cachedValues = $cache->load($cacheKey);
        //check if the subtypes are cached
        if ($cachedValues === false) {
            $subPackageTypeValues = [];
            //package subtye
            $attribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', Dyna_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR);
            $subTypesPackages = $this->getCollection()
                ->addFieldToSelect(['subtype_code' => 'package_code'])
                ->addFieldToFilter('ignored_by_compatibility_rules', ['eq' => 1]);

            // join with package codes
            $subTypesPackages->getSelect()
                ->joinLeft(['package_table' => 'catalog_package_types'], 'main_table.type_package_id = package_table.entity_id', ['package_code' => 'package_table.package_code'])
                ->where("package_table.package_code = ?", $packageType);

            //get eav id for packageSubTypes
            foreach ($subTypesPackages as $subType) {
                $subPackageTypeValues[$subType['subtype_code']] = Mage::helper('omnius_catalog')->getAttributeAdminLabel($attribute, null, $subType['subtype_code']);
            }

            //cache the result
            $cache->save(serialize($subPackageTypeValues), $cacheKey, array(Dyna_Cache_Model_Cache::PRODUCT_TAG), $cache->getTtl());
        } else {
            $subPackageTypeValues = unserialize($cachedValues);
        }

        //return skipped subtypes
        return $subPackageTypeValues;
    }

    /*
     * Get eav ids for skipped packageSubTypes
     */

    /**
     * Get current package code for this instance
     * @param bool $lowerCased
     * @return string
     */
    public function getPackageCode(bool $lowerCased = false)
    {
        return $lowerCased ? strtolower($this->getData(self::STR_ID)) : $this->getData(self::STR_ID);
    }

    /**
     * Before save, convert package code to lower case and replace spaces with underscore
     * @return $this
     */
    public function save()
    {
        $this->setData('package_code', str_replace(" ", "_", $this->getData('package_code')));

        return parent::save();
    }

    /**
     * Get current instance associated package type
     * @return Dyna_Package_Model_PackageType|Mage_Core_Model_Abstract
     */
    public function getPackageType()
    {
        return Mage::getModel('dyna_package/packageType')->load($this->getTypePackageId());
    }

    /**
     * Returns all configurator products from Dyna Catalog needed for this section
     * @return Dyna_Catalog_Model_Product[]
     */
    public function getConfiguratorProducts()
    {
        // get catalog model
        /** @var Dyna_Configurator_Model_Catalog $catalogModel */
        $catalogModel = Mage::getModel('dyna_configurator/catalog');
        // sent forward to getProductsOfType
        $websiteId = Mage::app()->getWebsite()->getId();
        return $catalogModel->getProductsOfType(ucfirst($this->getPackageCode(true)), $websiteId, array(), ucfirst($this->getPackageType()->getPackageCode(true)));
    }

    /**
     * Returns PackageSubtype for package type id and package code
     * @param int $packageTypeId
     * @param string $packageCode
     * @return mixed
     */
    public function loadByPackageIdAndCode($packageTypeId, $packageCode)
    {
        $result = $this->getCollection()
            ->addFieldToFilter('type_package_id', $packageTypeId)
            ->addFieldToFilter('package_code', $packageCode)
            ->getFirstItem();

        return $result;
    }

    /**
     * Returns the cardinality for the specified process context
     *
     * @param string $processContext The process context code
     * @return Dyna_Package_Model_PackageSubtypeCardinality|null
     */
    public function getCardinalityForProcessContext($processContext)
    {
        /** @var Dyna_Catalog_Model_ProcessContext $processContextModel */
        $processContextModel = Mage::getModel('dyna_catalog/processContext');

        if (!$processContext || !in_array($processContext, $processContextModel->getAllProcessContextIdCodePairs())) {
            return null;
        }

        /** @var Dyna_Package_Model_PackageSubtypeCardinality $processContextCardinality */
        $processContextCardinality = Mage::getModel('dyna_package/packageSubtypeCardinality')
            ->getCollection()
            ->addFieldToFilter('package_subtype_id', ['eq' => $this->getId()])
            ->addFieldToFilter('process_context_id', ['eq' => $processContextModel->getProcessContextIdByCode($processContext)]);

        return $processContextCardinality->getFirstItem();
    }
}
