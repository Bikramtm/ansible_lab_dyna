<?php

/**
 * Class Dyna_Package_Model_PackageCreationGroups
 */
class Dyna_Package_Model_PackageCreationGroups extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        $this->_init('dyna_package/packageCreationGroups');
    }

    /**
     * Get all package creation groups with the package types
     * - first they are ordered by package_creation_groups.sorting
     * - secondly they are ordered by package_creation_types.sorting
     *
     * @return array
     */
    public function getPackageCreationGroupsWithPackageTypesOrdered()
    {
        $packageGroups = $this->getCollection()->getSelect()->order('main_table.sorting', 'ASC')->query()->fetchAll();
        /** @var Dyna_Package_Model_PackageCreationTypes $model */
        $model = Mage::getModel('dyna_package/packageCreationTypes');

        foreach ($packageGroups as &$group) {
            $group['package_creation_types'][] = [];
            $packageCreationTypes = $model->getPackageCreationTypesByGroup($group['entity_id']);

            foreach ($packageCreationTypes as &$creationType) {
                $targetPackage = Mage::getModel('package/packageType')
                    ->getCollection()
                    ->addFieldToFilter('entity_id', $creationType['package_type_id'])
                    ->getFirstItem();

                $creationType['target_package'] = $targetPackage->getPackageCode();
            }

            if($packageCreationTypes) {
                $group['package_creation_types'] = $packageCreationTypes;
            }
        }

        return $packageGroups;
    }

    public function getById($packageCreationGroupId)
    {
            $packageGroup = $this->getCollection()
                ->addFieldToFilter('entity_id', array('eq' => $packageCreationGroupId))
                ->getFirstItem();
        return $packageGroup;
    }
}
