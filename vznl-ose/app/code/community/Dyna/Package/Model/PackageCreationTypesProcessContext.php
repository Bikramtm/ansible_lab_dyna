<?php

/**
 * Class Dyna_Package_Model_PackageCreationTypesProcessContext
 * @method string getFilterAttributes()
 */
class Dyna_Package_Model_PackageCreationTypesProcessContext extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        $this->_init('dyna_package/packageCreationTypesProcessContext');
    }

    public function getByPackageCreationIdAndProcessContextId($packageCreationTypeId, $processContextId)
    {
        /** @var Dyna_Package_Model_PackageCreationTypesProcessContext $packageCreationTypesProcessContext */
        $packageCreationTypesProcessContext = $this->getCollection()
            ->addFieldToFilter('package_creation_type_id', ['eq' => $packageCreationTypeId])
            ->addFieldToFilter('process_context_id', ["eq" => $processContextId])
            ->getFirstItem();

        return $packageCreationTypesProcessContext;
    }
}
