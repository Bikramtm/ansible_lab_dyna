<?php
/**
 * Drop customer_number column because it's the same as linked_account_number
 * Change the name of column linked_account_number to parent_account_number for code consistency
 * Drop install_base_product_id, install_base_subscription_number. We use parent_account_number and service_line_id instead
 */

/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();
$catalogPackageTable = $installer->getTable('package/package');
$connection = $installer->getConnection();

if ($connection->tableColumnExists($catalogPackageTable, "customer_number")) {
    $connection->dropColumn($catalogPackageTable,"customer_number");
}

if ($connection->tableColumnExists($catalogPackageTable, "install_base_subscription_number")) {
    $connection->dropColumn($catalogPackageTable,"install_base_subscription_number");
}

if ($connection->tableColumnExists($catalogPackageTable, "install_base_product_id")) {
    $connection->dropColumn($catalogPackageTable,"install_base_product_id");
}

if($connection->tableColumnExists($catalogPackageTable, "linked_account_number") && !$connection->tableColumnExists($catalogPackageTable, "parent_account_number")){
    $connection->changeColumn($catalogPackageTable, "linked_account_number", "parent_account_number", 'varchar(255) null');
}

$installer->endSetup();