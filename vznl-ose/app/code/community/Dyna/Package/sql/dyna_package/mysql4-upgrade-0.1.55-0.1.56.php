<?php

/** @var Mage_Sales_Model_Entity_Setup $installer */
$installer = $this;
$installer->startSetup();

$packageSubtypesTable = $installer->getTable('catalog_package_subtypes');
$processContextTable = $installer->getTable('process_context');

// create new table for process context cardinality
if (!$installer->getConnection()->isTableExists($installer->getTable('package_subtype_cardinality'))) {
    $cardinalityTable = new Varien_Db_Ddl_Table();
    $cardinalityTable->setName($installer->getTable('package_subtype_cardinality'));

    $cardinalityTable
        ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_BIGINT, 10, array(
            'unsigned' => true,
            'primary' => true,
            'auto_increment' => true,
            'nullable' => false,
        ), 'Cardinality entity id')
        ->addColumn('package_subtype_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
            'unsigned' => true,
            'nullable' => false,
        ), 'Id of the package subtype')
        ->addColumn('process_context_id', Varien_Db_Ddl_Table::TYPE_BIGINT, 20, array(
            'unsigned' => true,
            'nullable' => false,
        ), 'Id of the process context')
        ->addColumn('cardinality', Varien_Db_Ddl_Table::TYPE_TEXT, 50, array(

        ), 'Cardinality of the package subtype in the current context')
        ->addForeignKey($installer->getFkName($cardinalityTable->getName(), 'package_subtype_id', $packageSubtypesTable, 'entity_id'),
            'package_subtype_id', $packageSubtypesTable, 'entity_id', Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
        ->addForeignKey($installer->getFkName($cardinalityTable->getName(), 'process_context_id', $processContextTable, 'entity_id'),
            'process_context_id', $processContextTable, 'entity_id', Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE);

    $installer->getConnection()->createTable($cardinalityTable);
}

$installer->endSetup();
