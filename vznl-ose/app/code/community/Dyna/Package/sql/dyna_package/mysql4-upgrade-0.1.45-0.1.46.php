<?php
/** @var Mage_Catalog_Model_Resource_Setup $this */
$this->startSetup();

$this->getConnection()->addColumn("sales_flat_quote", "transaction_id", array(
    'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
    'length'    => 100,
    'comment'   => "The id of the transaction that was used to create the current package"
));

$this->endSetup();
