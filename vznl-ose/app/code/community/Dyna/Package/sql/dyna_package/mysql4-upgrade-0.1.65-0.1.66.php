<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 *
 * Added column to save bundle types for a package
 * Values will be comma separated
 */

/** @var Mage_Core_Model_Resource_Setup $this */

$installer = $this;
$installer->startSetup();

$packageTable = $this->getTable('package/package');
$columnName = 'prolongation_type';

if (!$installer->getConnection()->tableColumnExists($packageTable, $columnName)) {
    $installer->getConnection()->addColumn($packageTable, $columnName, array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'length' => 30,
        'comment' => 'Text field to hold the prolongation type (STN, ART, INO, INL etc)'
    ));
}

$installer->endSetup();
