<?php
/**
 * Installer that adds a new column in the catalog_package table.
 * Column: sharing_group_id
 */

/** @var Mage_Core_Model_Resource_Setup $this */

$this->startSetup();

$packageResourceTable = $this->getTable('package/package');

if (!$this->getConnection()->tableColumnExists($this->getTable('package/package'), 'added_from_my_products')) {
    $this->getConnection()->addColumn($packageResourceTable, 'added_from_my_products', array(
        'type' => Varien_Db_Ddl_Table::TYPE_SMALLINT,
        'length' => 2,
        'comment' => 'Column used for identifying if the red plus was added from 360'
    ));
}

$this->endSetup();
