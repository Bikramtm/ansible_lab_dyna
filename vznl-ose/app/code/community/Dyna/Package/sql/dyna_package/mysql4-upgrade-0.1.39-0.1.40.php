<?php

/** @var Mage_Core_Model_Resource_Setup $this */

$this->startSetup();

$packageTable = $this->getTable('package/package');

// Add service address data column
$this->getConnection()->addColumn($packageTable, 'service_address_data', [
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'after' => 'service_address_id',
    'comment' => 'Serialized json of address data',
]);

$this->endSetup();
