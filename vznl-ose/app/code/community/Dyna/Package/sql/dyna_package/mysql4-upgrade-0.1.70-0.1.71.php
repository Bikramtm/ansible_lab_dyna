<?php

/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;

$installer->startSetup();
$catalogPackageTable = $installer->getTable('package/package');
$packageCreationTypesTable = $installer->getTable('dyna_package/packageCreationTypes');
$packageCreationGroupsTable = $installer->getTable('dyna_package/packageCreationGroups');
$catalogPackageTypesTable = $installer->getTable('package/type');
$columnName = 'package_creation_type_id';

if ($installer->tableExists($packageCreationTypesTable)) {
    // truncate this table first
    $installer->getConnection()->truncateTable($packageCreationTypesTable);

    // remove the foreign key from package_type_code
    $installer->getConnection()->dropForeignKey(
        $packageCreationTypesTable,
        $installer->getFkName($packageCreationTypesTable, 'package_type_code', 'catalog_package_types', 'package_code')
    );
}

if ($installer->tableExists($packageCreationGroupsTable)) {
    // truncate the package_creation_groups table because its values were previously added by another installer
    $installer->getConnection()->truncateTable($packageCreationGroupsTable);
}

if (!$installer->getConnection()->tableColumnExists($catalogPackageTable, $columnName)) {
    $installer->getConnection()->addColumn($catalogPackageTable, $columnName, array(
        'type' => Varien_Db_Ddl_Table::TYPE_BIGINT,
        'nullable' => false,
        'size' => 20,
        'comment' => 'Reference to the package_creation_types table',
        'after' => 'type',
        'unsigned' => true
    ));

    $installer->getConnection()->addForeignKey(
        $installer->getFkName($catalogPackageTable, 'entity_id', $packageCreationTypesTable, 'entity_id'),
        $catalogPackageTable, $columnName,
        $packageCreationTypesTable, 'entity_id',
        Varien_Db_Ddl_Table::ACTION_NO_ACTION, Varien_Db_Ddl_Table::ACTION_NO_ACTION
    );
}

if (!$installer->getConnection()->tableColumnExists($packageCreationTypesTable, 'package_type_id')) {
    $installer->getConnection()->addColumn($packageCreationTypesTable, 'package_type_id', array(
        'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'nullable' => false,
        'size' => 10,
        'comment' => 'Reference to the catalog_package_types table',
        'after' => 'package_type_code',
        'unsigned' => true
    ));

    $installer->getConnection()->addForeignKey(
        $installer->getFkName($packageCreationTypesTable, 'entity_id', $catalogPackageTypesTable, 'entity_id'),
        $packageCreationTypesTable, 'package_type_id',
        $catalogPackageTypesTable, 'entity_id',
        Varien_Db_Ddl_Table::ACTION_NO_ACTION, Varien_Db_Ddl_Table::ACTION_NO_ACTION
    );
}


$installer->endSetup();
