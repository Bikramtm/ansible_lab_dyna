<?php
/**
 * Add table package_creation_groups with columns
 * - entity_id
 * - name
 * - gui_name
 * - sorting
 */


/** @var Mage_Sales_Model_Entity_Setup $installer */
$installer = $this;
$installer->startSetup();

// create new table for process context cardinality
if (!$installer->getConnection()->isTableExists($installer->getTable('package_creation_groups'))) {
    $packageCreationGroupTable = new Varien_Db_Ddl_Table();
    $packageCreationGroupTable->setName($installer->getTable('package_creation_groups'));

    $packageCreationGroupTable
        ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_BIGINT, 20, array(
            'unsigned' => true,
            'primary' => true,
            'auto_increment' => true,
            'nullable' => false,
        ), 'Package creation group entity id')
        ->addColumn('name', Varien_Db_Ddl_Table::TYPE_TEXT, null, [
        ], "The package creation group name")
        ->addColumn('gui_name', Varien_Db_Ddl_Table::TYPE_TEXT, null, [
        ], "The package creation group display name")
        ->addColumn('sorting', Varien_Db_Ddl_Table::TYPE_BIGINT, 20, array(
            'unsigned' => true,
            'nullable' => false,
        ), 'The order of the package groups');

    $installer->getConnection()->createTable($packageCreationGroupTable);
}

$installer->endSetup();


