<?php
/** @var Mage_Core_Model_Resource_Setup $this */

$this->startSetup();

$packageCreationTypesTable = $this->getTable('package_creation_types');
$processContextTable = $this->getTable('process_context');

$packageCreationTypeProcessContextTable = new Varien_Db_Ddl_Table();
$packageCreationTypeProcessContextTable->setName($this->getTable('package_creation_types_process_context'));

$packageCreationTypeProcessContextTable
    ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_BIGINT, 20, [
        "unsigned" => true,
        "primary" => true,
        "auto_increment" => true,
        'nullable' => false
    ], "Primary key for table")
    ->addColumn('package_creation_type_id', Varien_Db_Ddl_Table::TYPE_BIGINT, 20, [
        'unsigned' => true,
        'nullable' => false,
    ], 'Package creation type entity id')
    ->addColumn('process_context_id', Varien_Db_Ddl_Table::TYPE_BIGINT, 20, [
        'unsigned' => true,
        'nullable' => false,
    ], "Process context entity id")
    ->addColumn('filter_attributes', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, [
        'nullable' => false,
    ], "Filters attributes")
    ->addIndex(
        $this->getIdxName(
            $packageCreationTypeProcessContextTable->getName(),
            array('package_creation_type_id', 'process_context_id', 'filter_attributes'),
            Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
        ),
        array('package_creation_type_id', 'process_context_id', 'filter_attributes'),
        array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE))
    ->addForeignKey($this->getFkName($packageCreationTypeProcessContextTable->getName(), 'package_creation_type_id',
        $processContextTable, 'entity_id'),
        'package_creation_type_id', $packageCreationTypesTable, 'entity_id', Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->addForeignKey($this->getFkName($packageCreationTypeProcessContextTable->getName(), 'process_context_id',
        $processContextTable, 'entity_id'),
        'process_context_id', $processContextTable, 'entity_id', Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE);
$this->getConnection()->createTable($packageCreationTypeProcessContextTable);

$this->endSetup();