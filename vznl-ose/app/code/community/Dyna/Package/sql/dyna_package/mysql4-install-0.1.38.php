<?php

/** @var Mage_Core_Model_Resource_Setup $this */

$this->startSetup();

$pacakgeTable = $this->getTable('package/package');

// Add receiver serial number column
$this->getConnection()->addColumn($pacakgeTable, 'receiver_serial_number', [
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'after' => 'current_simcard_number',
    'comment' => 'Receiver serial number for Use my own receiver product',
]);

// Add smartcard serial number column
$this->getConnection()->addColumn($pacakgeTable, 'smartcard_serial_number', [
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'after' => 'receiver_serial_number',
    'comment' => 'Smartcard serial number for Use my own smartcard product',
]);

$this->endSetup();
