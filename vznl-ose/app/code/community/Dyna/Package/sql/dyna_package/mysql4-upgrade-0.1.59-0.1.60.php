<?php
/**
 * Installer that changes column "package_code" to be unique and varchar (255) for catalog_package_types table:
 */

/** @var Mage_Core_Model_Resource_Setup $this */

$this->startSetup();

if ($this->getConnection()->tableColumnExists('catalog_package_types', 'package_code')) {
    $this->getConnection()->changeColumn('catalog_package_types', 'package_code', 'package_code', 'VARCHAR(255)');

    $this->getConnection()
        ->addIndex(
            $this->getTable('catalog_package_types'),
            $this->getIdxName('catalog_package_types', array('package_code'), Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE),
            array('package_code'), Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
        );
}

$this->endSetup();

