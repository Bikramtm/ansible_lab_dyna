<?php
/** @var Mage_Catalog_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();

$table = $this->getTable('package/package');

$connection = $installer->getConnection();

$connection->addColumn($table, 'notes', array(
    'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
    'length'    => 150,
    'comment'   => 'Package notes'
));

$installer->endSetup();
