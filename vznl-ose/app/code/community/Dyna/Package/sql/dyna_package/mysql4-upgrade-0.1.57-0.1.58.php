<?php
/**
 * 1 column is added in catalog package:
 * service_line_id - Unique identifier used in ILS flow
 */

/** @var Mage_Core_Model_Resource_Setup $this */

$this->startSetup();

$packageResourceTable = $this->getTable('package/package');

$this->getConnection()->addColumn($packageResourceTable, 'service_line_id', array(
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'length' => 255,
    'comment' => 'Column used as an unique identifier for an in life sale flow (ils)'
));

$this->endSetup();
