<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** @var Mage_Sales_Model_Entity_Setup $installer */
$installer = $this;

$installer->startSetup();
$connection = $installer->getConnection();

$cardinalityTable = $installer->getTable('package_subtype_cardinality');
$packageSubtypesTable = $installer->getTable('catalog_package_subtypes');
$processContextTable = $installer->getTable('process_context');

// Existing FKs names
$fk1 = $installer->getFkName($cardinalityTable, 'package_subtype_id', $packageSubtypesTable, 'entity_id');
$fk2 = $installer->getFkName($cardinalityTable, 'process_context_id', $processContextTable, 'entity_id');

// Remove existing FKs that do not have onDelete/onUpdate CASCADE
$connection->dropForeignKey($cardinalityTable, $fk1);
$connection->dropForeignKey($cardinalityTable, $fk2);

// New FKs names
$fk1 .= '_V2';
$fk2 .= '_V2';

// Add new FKs that do have onDelete/onUpdate CASCADE
$connection->addForeignKey(
    $fk1,
    $cardinalityTable,
    'package_subtype_id',
    $packageSubtypesTable,
    'entity_id',
    Varien_Db_Ddl_Table::ACTION_CASCADE,
    Varien_Db_Ddl_Table::ACTION_CASCADE
);

$connection->addForeignKey(
    $fk2,
    $cardinalityTable,
    'process_context_id',
    $processContextTable,
    'entity_id',
    Varien_Db_Ddl_Table::ACTION_CASCADE,
    Varien_Db_Ddl_Table::ACTION_CASCADE
);

$installer->endSetup();