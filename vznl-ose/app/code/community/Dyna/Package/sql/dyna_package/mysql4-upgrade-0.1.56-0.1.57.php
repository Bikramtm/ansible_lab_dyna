<?php
/**
 * Installer that adds 1 column for catalog package resource:
 * ils_initial_products - Column used for setting customer all initial component products skus for an in life sale flow (ils)
 */

/** @var Mage_Core_Model_Resource_Setup $this */

$this->startSetup();

$packageResourceTable = $this->getTable('package/package');
if (!$this->getConnection()->tableColumnExists($packageResourceTable, 'ils_initial_products')) {
    $this->getConnection()->addColumn($packageResourceTable, 'ils_initial_products', array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'comment' => 'Column used for setting package initial component skus for an in life sale flow (ils)'
    ));
}

$this->endSetup();
