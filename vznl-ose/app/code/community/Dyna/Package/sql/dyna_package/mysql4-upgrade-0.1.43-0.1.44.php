<?php
/** @var Mage_Core_Model_Resource_Setup $this */
$installer = $this;
$installer->startSetup();

// The catalog_package_configurations table
$configurationTable = $installer->getTable("package/subtype");

$connection = $installer->getConnection();

$connection->addColumn($configurationTable, "ignored_by_compatibility_rules", array(
    'type'      => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
    'default'   => 0,
    'comment'   => "Products of this subtype will be ignored by compatibility rules and showed in the configurator"
));

$installer->endSetup();
