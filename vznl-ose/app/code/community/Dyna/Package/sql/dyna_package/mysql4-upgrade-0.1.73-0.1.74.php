<?php
/**
 * Change the columns types for package_subtype_id, package_type_id from product_version & product_family tables
 */

/* @var $installer Dyna_Cable_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();
$connection = $installer->getConnection();
$productVersionTable = $this->getTable('product_version');
$productFamilyTable = $this->getTable('product_family');
$packageTypeTable =  $this->getTable('package/type');
$packageSubtypeTable = $this->getTable('package/subtype');

if ($connection->tableColumnExists($productFamilyTable, 'package_subtype_id')) {
    $connection->modifyColumn($productFamilyTable, 'package_subtype_id', "INT(11) unsigned");
    $connection->addConstraint(
        'FK_PACKAGE_SUBTYPE_ID_FAMILY',
        $productFamilyTable,
        'package_subtype_id',
        $packageSubtypeTable,
        'entity_id',
        Varien_Db_Adapter_Interface::FK_ACTION_NO_ACTION,
        Varien_Db_Adapter_Interface::FK_ACTION_NO_ACTION,
        true
    );
}

if ($connection->tableColumnExists($productFamilyTable, 'package_type_id')) {
    $connection->modifyColumn($productFamilyTable, 'package_type_id', "INT(11) unsigned");
    $connection->addConstraint(
        'FK_PACKAGE_TYPE_ID_FAMILY',
        $productFamilyTable,
        'package_type_id',
        $packageTypeTable,
        'entity_id',
        Varien_Db_Adapter_Interface::FK_ACTION_NO_ACTION,
        Varien_Db_Adapter_Interface::FK_ACTION_NO_ACTION,
        true
    );
}

if ($connection->tableColumnExists($productVersionTable, 'package_type_id')) {
    $connection->modifyColumn($productVersionTable, 'package_type_id', "INT(11) unsigned");
    $connection->addConstraint(
        'FK_PACKAGE_TYPE_ID_VERSION',
        $productVersionTable,
        'package_type_id',
        $packageTypeTable,
        'entity_id',
        Varien_Db_Adapter_Interface::FK_ACTION_NO_ACTION,
        Varien_Db_Adapter_Interface::FK_ACTION_NO_ACTION,
        true
    );
}

$installer->endSetup();