<?php
/**
 * Set the default values of the package creation types and the package creation groups
 */
$this->startSetup();

/** @var Varien_Db_Adapter_Interface $connection */
$connection = $this->getConnection();

/** @var Dyna_Package_Model_PackageCreationTypes $packageCreationTypes */
$packageCreationTypes = Mage::getModel('dyna_package/packageCreationTypes');
$packageCreationGroupsValues = [ "" ,  " ", "", " "];

$sql = "INSERT INTO package_creation_groups (`name`, `gui_name`, `sorting`) VALUES ('Mobile', 'Mobilfunk', 1) ;" ;
$connection->query($sql);
$packageCreationMobileGroupId = $connection->lastInsertId();

$sql = "INSERT INTO package_creation_groups (`name`, `gui_name`, `sorting`) VALUES ('Internet & Phone', 'Internet & Phone', 2) ;" ;
$connection->query($sql);
$packageCreationInternetPhoneGroupId = $connection->lastInsertId();

$sql = "INSERT INTO package_creation_groups (`name`, `gui_name`, `sorting`) VALUES('TV', 'TV', 3) ;" ;
$connection->query($sql);
$packageCreationTVGroupId = $connection->lastInsertId();

$sql = "INSERT INTO package_creation_groups (`name`, `gui_name`, `sorting`) VALUES('CableIndependentServices', 'Cable Independent Services', 4) ;" ;
$connection->query($sql);
$packageCreationCableIndependentGroupId = $connection->lastInsertId();

$sql = "INSERT INTO package_creation_types (`name`, `gui_name`, `sorting`, `filter_attributes`, `package_type_code`, `package_creation_group_id`) VALUES " .
    "('Mobile Postpaid', 'Deals with contract', 1, '', '".Dyna_Catalog_Model_Type::TYPE_MOBILE."', $packageCreationMobileGroupId), " .
    "('Mobile Prepaid', 'Prepaid / Callya', 2,'',  '".Dyna_Catalog_Model_Type::TYPE_PREPAID."', $packageCreationMobileGroupId)," .
    "('Mobile RedPlus', 'Red+ Partner Card', 3,'',  '".Dyna_Catalog_Model_Type::TYPE_REDPLUS."', $packageCreationMobileGroupId)," .
    "('Cable Internet & Phone', 'Cable Internet & Phone', 4,'',  '".Dyna_Catalog_Model_Type::TYPE_CABLE_INTERNET_PHONE."', $packageCreationInternetPhoneGroupId)," .
    "('DSL', 'DSL', 5,'',  '".Dyna_Catalog_Model_Type::TYPE_FIXED_DSL."', $packageCreationInternetPhoneGroupId),".
    "('LTE', 'LTE', 6,'',  '".Dyna_Catalog_Model_Type::TYPE_LTE."', $packageCreationInternetPhoneGroupId),".
    "('Cable TV', 'Cable TV', 7,'',  '".Dyna_Catalog_Model_Type::TYPE_CABLE_TV."', $packageCreationTVGroupId),".
    "('Cable Independent services', 'Cable Independent Services', 8,'',  '".Dyna_Catalog_Model_Type::TYPE_CABLE_INDEPENDENT."', $packageCreationCableIndependentGroupId);";

$connection->query($sql);

$this->endSetup();

