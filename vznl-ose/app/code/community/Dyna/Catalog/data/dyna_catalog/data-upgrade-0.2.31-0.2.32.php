<?php
$installer = $this;
/* @var $installer Dyna_Cable_Model_Resource_Setup */

$installer->startSetup();

// Get catalog_product entity id
$entityTypeId = Mage_Catalog_Model_Product::ENTITY;
$targetProperty = 'is_required';
$targetPropertyValue = 0;

$newAttributes = [
    'display_name_inventory',
    'display_name_configurator',
    'display_name_short',
    'display_name_communication',
];

foreach ($newAttributes as $attributeCode) {
    $attributeId = $installer->getAttributeId($entityTypeId, $attributeCode);
    if ($attributeId) {
        $installer->updateAttribute($entityTypeId, $attributeCode, $targetProperty, $targetPropertyValue);
    }
}

$installer->endSetup();
