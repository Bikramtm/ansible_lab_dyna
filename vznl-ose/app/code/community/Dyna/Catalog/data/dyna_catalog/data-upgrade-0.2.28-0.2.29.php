<?php

$installer = $this;

// add process context entries
$processContextEntries = array(
    'ACQ' => [
        'name' => 'Acquisition',
        'description' => 'Buying a new contract without context of an existing contract. There might be rules related to the fact that is it an existing customer but there is no relation to an existing contract. 
        Use: Customer wants to buy its first contract.',
    ],
    'REO' => [
        'name' => 'ReOrder',
        'description' => 'Buying a new contract taking into account the fact that customer already has a contract and this needs to be taken into account. ReOrder can only be selected from the context of an existing contract. This contract will then determine rules that allow (for instance) old products to still be purchased which is not possible in regular acquisition as there only the latest version of a product can be purchased.
        Use: Customer wants to buy exactly the same as he already has. This due to compatibility of desired features (like call with friends which might be discontinued in a newer version of product) or because it was contractually agreed (as part of a framework agreement).',
    ],
    'RETBLU' => [
        'name' => 'Retention with BaseLine Update',
        'description' => 'This is retaining an existing contract (if eligible / retainable) and will directly change the product to the new version without waiting for the end of commitment period (end of contract date). So in principle this is an In Life Sale up until end of contract combined with a RETNOBLU. For Retention you always stay in the same ProductLine.
        Use: Customer wants immediate benefit a new product. Based on Hierarchy value and Contractual Value the customer might have to pay a penalty (in case of a downgrade for instance).',
    ],
    'RETNOBLU' => [
        'name' => 'Retention without Baseline Update',
        'description' => 'This is retaining an existing contract (if eligible / Retainable) but will not change the products up until the contract has been served out. This means that activation of products will be done after the end of commitment of the old contract. For Retention you always stay in the same ProductLine.
        Use: Customer wants to serve out his current contract first and then start his new product. This to facilitate customers that do not want to pay the penalty in case of downgrade (determined by HV and CV) but do want to retain their contract.',
    ],
    'RETTER' => [
        'name' => 'Retention with Direct Terminate of old contract',
        'description' => 'The new configuration will immediately be active and his old contract will be terminated.',
    ],
    'ILSBLU' => [
        'name' => 'In Life Sale with Baseline Update',
        'description' => 'Customer wants to change products (tarif and or options) within the runtime of his contract (so no change to commitment period) without changing the ProductLine. The new configuration will be considered the new Contractual Agreement made up until the end of the runtime of the contract.
        Use: In case system is not able to handle temporary changes to contracts or that customer is getting benefit / penalty due to changing his contract up until the end of its commitment period.',
    ],
    'ILSNOBLU' => [
        'name' => 'In Life Sale without Baseline Update',
        'description' => 'Customer wants to change products (tarif and or options) within the runtime of his contract (so no change to commitment period) without changing the ProductLine. The new configuration will be considered a temporary change  and not a change to the contractual agreement. This means that a customer can always go back to the initial contractual agreement without a penalty or benefit.
        Use: Customer goes on holiday and wants for duration of his holiday to upgrade to higher priceplan. As he returns, he wants to go back to his original contractual agreement.',
    ],
    'MIGCOC' => [
        'name' => 'Migration with Change Of ContractPeriod',
        'description' => 'Customer wants to migrate to a new ProductLine (=PackageType) and based on this also his contract period needs to be changed. This will always be done directly.
        Use: Customer wants to move to completely new Productline.',
    ],
    'MIGNOCOC' => [
        'name' => 'Migration without Change of ContractPeriod',
        'description' => 'Customer wants to migrate to a new ProductLine (=packageType) and does not want to change his commitment period as contractually agreed. Possibly penalties can be issued which are done by means of rules in the configurator. Migration is always instant and always a BaseLine Update.
        Use: Customer wants to move to completely new Productline.',
    ],
    'MOVE' => [
        'name' => 'Move',
        'description' => 'Customer wants to move and this triggers also a change or product but no changes are made to his contract end-date. If that would be the case , it would be considered a combination of Move and Retention which is currently not a supported processcontext.
        Use: A customer moved to a different house and now needs to technically terminate his current connection and setup a new connection. Based on the location of his new house it might be the case his existing product cannot be services anymore (for instance cable now needs to be come VDSL).',
    ],
    'TOU' => [
        'name' => 'Transfer of User',
        'description' => 'Transfering from one legal entity to another.',
    ],

);

$conn = Mage::getSingleton('core/resource')->getConnection('core_write');
/**
 * @var $processContextModel Dyna_Catalog_Model_ProcessContext
 */
$processContextModel = Mage::getModel('dyna_catalog/processContext');

foreach ($processContextEntries as $code => $details) {
    if(!$processContextModel->getProcessContextIdByCode($code)) {
        $conn->insert('process_context', array('code' => $code, 'name' => $details['name'], 'description' => $details['description']));
    }
}

$installer->endSetup();