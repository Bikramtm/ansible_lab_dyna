<?php

/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */
class Dyna_Catalog_Block_Adminhtml_ProductVersions_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId("productVersionsGrid");
        $this->setDefaultSort("entity_id");
        $this->setDefaultDir("DESC");
        $this->setSaveParametersInSession(true);
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel("dyna_catalog/productVersion")->getCollection();
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * @return self|Mage_Adminhtml_Block_Widget_Grid
     * @throws Exception
     */
    protected function _prepareColumns()
    {
        $this->addColumn("entity_id", array(
            "header" => Mage::helper("dyna_catalog")->__("ID"),
            "type" => "number",
            "index" => "entity_id",
        ));
        $this->addColumn("product_version_id", array(
            "header" => Mage::helper("dyna_catalog")->__("Code"),
            "index" => "product_version_id",
        ));
        $this->addColumn("product_version_name", array(
            "header" => Mage::helper("dyna_catalog")->__("Name"),
            "index" => "product_version_name",
        ));
        $this->addColumn("lifecycle_status", array(
            "header" => Mage::helper("dyna_catalog")->__("Lifecycle"),
            "index" => "lifecycle_status",
            "searchable" => false,
            "renderer" => "dyna_catalog/adminhtml_lifecycleRenderer",
            'type' => 'options',
            'options' => self::getLifecycleStatuses(),
        ));
        $this->addColumn("product_family_id", array(
            "header" => Mage::helper("dyna_catalog")->__("Family ID"),
            "index" => "product_family_id",
        ));
        $this->addColumn("package_type_id", array(
            "header" => Mage::helper("dyna_catalog")->__("Package type"),
            "index" => "package_type_id",
            "searchable" => false,
            "type" => "options",
            "options" => Mage::helper('dyna_package')->getPackageTypesId()
        ));

        return parent::_prepareColumns();
    }

    public static function getLifecycleStatuses()
    {
        return Dyna_Catalog_Model_Lifecycle::getLifecycleTypes();
    }

    /**
     * @param $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl("*/*/edit", array("id" => $row->getId()));
    }

    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('entity_ids');
        $this->getMassactionBlock()->setUseSelectAll(true);

        return $this;
    }
}
