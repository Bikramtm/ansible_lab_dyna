<?php
class Dyna_Catalog_VersionController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $url = $this->getRequest()->get('url');

        /** @var Mage_Core_Model_Resource $resource */
        $resource = Mage::getSingleton('core/resource');
        $readerAdapter   = $resource->getConnection('core_read');
        $sql = 'SELECT * FROM `catalog_import_log` ORDER BY `date` DESC LIMIT 1';
        $catalogVersion = $readerAdapter->fetchRow($sql);

        $result['Browser version'] = $_SERVER['HTTP_USER_AGENT'];
        $result['URL'] = $url;
        $result['SW'] = Mage::helper("dyna_cache")->getAppVersion();

        array_shift($catalogVersion);
        $result['CatalogVersion'] = $catalogVersion;


        $packages = [];

        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();

        foreach ($quote->getCartPackages() as $package) {
            $packageInfo = [];
            $packageInfo['PackageID'] = $package->getPackageId();
            $packageInfo['Name'] = $package->getTitle();

            foreach ($package->getItems() as $item) {
                $productInfo['ID'] = $item->getProductId();
                $productInfo['SKU'] = $item->getProduct()->getSku();
                $productInfo['Name'] = $item->getProduct()->getName();

                $packageInfo['Products'][] = $productInfo;
            }

            $packages[] = $packageInfo;
        }

        $result['Packages'] = $packages;

        $this->getResponse()->setHeader('Content-Type', 'application/json');
        $this->getResponse()->setBody(json_encode($result));
    }
}