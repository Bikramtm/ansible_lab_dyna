<?php

/**
 * Class Dyna_Catalog_Helper_ProcessContext
 */
class Dyna_Catalog_Helper_ProcessContext extends Mage_Core_Helper_Abstract
{
    protected $processContextIds = array();
    protected $processContextCode = false;
    protected $processContextId = [];

    /**
     * @return mixed
     */
    public function getProcessContextsByCodes()
    {
        /**
         * @var $processContextModel Dyna_Catalog_Model_ProcessContext
         */
        $processContextModel = Mage::getModel('dyna_catalog/processContext');
        return $processContextModel->getAllProcessContextCodeIdPairs();
    }

    /**
     * @return array|mixed
     */
    public function getProcessContextsByNamesForForm()
    {
        /**
         * @var $processContextModel Dyna_Catalog_Model_ProcessContext
         */
        $processContextModel = Mage::getModel('dyna_catalog/processContext');
        return $processContextModel->getAllProcessContextByNamesForForm();
    }

    /**
     * Get the active Process Context code from the session
     * @return mixed
     */
    public function isILSContext()
    {
        return in_array($this->getProcessContextCode(), Dyna_Catalog_Model_ProcessContext::ILSProcessContexts());
    }

    /**
     * Get the active Process Context code from the session
     * @return string
     */
    public function getProcessContextCode()
    {
        if ($this->processContextCode === false) {
            /** @var Dyna_Checkout_Model_Sales_Quote $quote */
            $quote = Mage::getSingleton('checkout/cart')->getQuote();
            $activePackage = $quote->getCartPackage();

            $this->processContextCode = $activePackage ? $activePackage->getSaleType() : "";
        }

        return $this->processContextCode;
    }

    /**
     * Get the current Process Context id (associated to the code) from the cart session;
     * If current package is not provided, we get the process context from the active package on quote
     * @param bool | Dyna_Package_Model_Package $currentPackage
     * @return mixed
     */
    public function getProcessContextId($currentPackage = false)
    {
        // if there are multiple packages in cart, and a package is specified, use that instead of active package
        if ($currentPackage && $currentPackage instanceof Dyna_Package_Model_Package) {
            $processContextPackage = $currentPackage;
        } else {
            /** @var Dyna_Checkout_Model_Sales_Quote $quote */
            $quote = Mage::getSingleton('checkout/cart')->getQuote();
            $processContextPackage = $quote->getCartPackage();
        }
        if (!$processContextPackage) {
            return null;
        }

        if (!isset($this->processContextId[$processContextPackage->getEntityId()])) {
            /** @var $processContextModel Dyna_Catalog_Model_ProcessContext */
            $processContextModel = Mage::getModel('dyna_catalog/processContext');
            $code = $processContextPackage && $processContextPackage->getSaleType() ? $processContextPackage->getSaleType() : Dyna_Catalog_Model_ProcessContext::ACQ;

            $this->processContextId[$processContextPackage->getEntityId()] = $processContextModel->getProcessContextIdByCode($code);
        }

        return $this->processContextId[$processContextPackage->getEntityId()];
    }

    public function getProcessContextSetIds($currentPackage = false)
    {
        // if there are multiple packages in cart, and a package is specified, use that instead of active package
        if ($currentPackage && $currentPackage instanceof Dyna_Package_Model_Package) {
            $processContextPackage = $currentPackage;
        } else {
            /** @var Dyna_Checkout_Model_Sales_Quote $quote */
            $quote = Mage::getSingleton('checkout/cart')->getQuote();
            $processContextPackage = $quote->getCartPackage();
        }

        if (!$processContextPackage) {
            return null;
        }

        if (!isset($this->processContextIds[$processContextPackage->getEntityId()])) {
            $connection = Mage::getSingleton('core/resource')->getConnection('core/read');
            $processContextId = $this->getProcessContextId($processContextPackage);
            $this->processContextIds[$processContextPackage->getEntityId()] = $connection->query("select entity_id from product_match_whitelist_index_process_context_set where find_in_set({$processContextId}, process_context_ids)")->fetchAll(\PDO::FETCH_COLUMN);
        }

        return $this->processContextIds[$processContextPackage->getEntityId()];
    }

    public function getNameByCode($code){
        $processContextModel = Mage::getModel('dyna_catalog/processContext');
        $processContextName = $processContextModel->getProcessContextNameByCode($code);
        return $processContextName;
    }
}
