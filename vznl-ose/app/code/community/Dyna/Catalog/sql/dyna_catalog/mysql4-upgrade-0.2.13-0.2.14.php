<?php
$installer = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;
$targetAttributeCode = 'product_visibility';

$attr = Mage::getModel('eav/config')->getAttribute($entityTypeId, $targetAttributeCode);
$attrId = $attr->getId();
$attrResModel = Mage::getModel('catalog/resource_eav_attribute');
$attrResModel->load($attrId);
$attrOptions = $attr->getSource()->getAllOptions();

$deleteOptions = [];
$deleteOptions['attribute_id'] = $attrId;

$newOptions = [
    'attribute_id' => $attrId,
    'values' => [
        Dyna_Catalog_Model_Product::PRODUCT_VISIBILITY_INVENTORY,
        Dyna_Catalog_Model_Product::PRODUCT_VISIBILITY_CONFIGURATOR,
        Dyna_Catalog_Model_Product::PRODUCT_VISIBILITY_SHOPPING_CART,
        Dyna_Catalog_Model_Product::PRODUCT_VISIBILITY_EXTENDED_SHOPPING_CART,
        Dyna_Catalog_Model_Product::PRODUCT_VISIBILITY_ORDER_OVERVIEW,
        Dyna_Catalog_Model_Product::PRODUCT_VISIBILITY_VOICELOG,
        Dyna_Catalog_Model_Product::PRODUCT_VISIBILITY_OFFER_PDF,
        Dyna_Catalog_Model_Product::PRODUCT_VISIBILITY_CALL_SUMMARY,
    ]
];

$first = true;
foreach ($attrOptions as $key => $details) {
    if (!$first) {
        $deleteOptions['delete'][$details['value']] = true;
        $deleteOptions['value'][$details['value']] = true;
    }
    $first = false;
}
$installer->addAttributeOption($deleteOptions);
$installer->addAttributeOption($newOptions);

$installer->endSetup();
?>
