<?php

/**
 * Cable product General attributes
 */

/* @var $installer Dyna_Cable_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

// Create attributes
$attributes = [
    'hide_in_configurator',
    'hide_in_shopping_cart',
    'hide_in_order_overview'
];

foreach ($attributes as $attribute) {
    $installer->removeAttribute(Mage_Catalog_Model_Product::ENTITY, $attribute);
}
$installer->endSetup();
