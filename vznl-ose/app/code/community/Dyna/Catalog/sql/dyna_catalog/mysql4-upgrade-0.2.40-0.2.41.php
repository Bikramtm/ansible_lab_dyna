<?php
/**
 * Installer that adds the contract_value attribute to product entity and assigns this attribute to all attribute sets (see @VFDED1W3S-913)
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $this Mage_Sales_Model_Entity_Setup */
$this->startSetup();

$attributeCode = 'contract_value';
$entityTypeId = $this->getEntityTypeId(Dyna_Catalog_Model_Product::ENTITY);

// add the attribute to db
$this->addAttribute($entityTypeId, $attributeCode, array(
    'label' => 'Contract value',
    'input' => 'text',
    'required' => false,
    'type' => 'int',
    'note' => 'Product contract value',
    'user_defined' => 1,
));

/** @var Mage_Eav_Model_Resource_Entity_Attribute_Set_Collection $attributeSetCollection */
$attributeSetCollection = Mage::getResourceModel('eav/entity_attribute_set_collection')
    ->addFieldToFilter('entity_type_id', $entityTypeId);

$attributeSetNames = array();
/** @var Mage_Eav_Model_Entity_Attribute_Set $attribute */
foreach ($attributeSetCollection as $attribute) {
    // add it to the list
    $attributeSetNames[$attribute->getId()] = $attribute->getAttributeSetName();
}

foreach ($attributeSetNames as $attributeSetId => $attributeSetCode) {
    $this->addAttributeToSet($entityTypeId, $attributeSetId, "General", $attributeCode);
}

$this->endSetup();
