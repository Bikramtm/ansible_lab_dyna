<?php
/**
 * Copyright (c) 2017. Dynacommerce B.V.
 */

/** @var Dyna_Cable_Model_Resource_Setup $this */
$this->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

// Update attributes
$updateAttributes = array(
    'dtc_restrict_ind',
);

foreach ($updateAttributes as $attrCode) {
    $attributeId = $this->getAttributeId($entityTypeId, $attrCode);
    if ($attributeId) {
        $this->updateAttribute($entityTypeId, $attributeId, 'is_required', false);
    }
}

$this->endSetup();
