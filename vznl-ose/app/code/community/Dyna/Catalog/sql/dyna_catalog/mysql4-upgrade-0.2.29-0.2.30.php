<?php
$installer = $this;
/* @var $installer Dyna_Cable_Model_Resource_Setup */

$installer->startSetup();

// Get catalog_product entity id
$entityTypeId = Mage_Catalog_Model_Product::ENTITY;
$groupName = 'General';

$attributesToDelete = [
    'display_name_configurator',
    'display_name_basket',
    'display_name_overview',
    'display_name_printout',
    'display_name_inventory',
];

$newAttributes = [
    'display_name_inventory' => [
        'label' => 'Display name in inventory',
        'input' => 'text',
        'type' => 'varchar',
        'note' => 'The display name used in inventory for this product',
    ],
    'display_name_configurator' => [
        'label' => 'Display name in configurator',
        'input' => 'text',
        'type' => 'varchar',
        'note' => 'The display name used in configurator for this product',
    ],
    'display_name_short' => [
        'label' => 'Display name in shopping cart',
        'input' => 'text',
        'type' => 'varchar',
        'note' => 'The display name used in shopping cart for this product',
    ],
    'display_name_communication' => [
        'label' => 'Display name in communication',
        'input' => 'text',
        'type' => 'varchar',
        'note' => 'The display name used in extended shopping cart, checkout overview & voicelog, call summary, offer summary and info summary sections for this product',
    ],
];

// Out with the old
foreach ($attributesToDelete as $attributeCode) {
    $attributeId = $installer->getAttributeId($entityTypeId, $attributeCode);

    if ($attributeId) {
        $installer->removeAttribute($entityTypeId, $attributeCode);
    }
}

// In with the new
$sortOrder = $attributeToSetSortOrder = 200;
$allAttributeSetIds = $installer->getAllAttributeSetIds($entityTypeId);

foreach ($newAttributes as $attributeCode => $options) {
    $attributeId = $installer->getAttributeId($entityTypeId, $attributeCode);

    if (!$attributeId) {
        $options['user_defined'] = 1;
        $installer->addAttribute($entityTypeId, $attributeCode, $options);
        $sortOrder++;
    }

    foreach ($allAttributeSetIds as $attributeSetId) {
        $installer->addAttributeToSet($entityTypeId, $attributeSetId, $groupName, $attributeCode, $attributeToSetSortOrder);
    }
}

$installer->endSetup();
