<?php
/**
 * Add attribute debit_to_credit for mobile products. This attribute is used to mark products that will be
 * available in the configurator for debit_to_credit flows (Prepaid to Postpaid)
 */
/* @var $installer Mage_Sales_Model_Entity_Setup */

$installer = $this;
$installer->startSetup();

// Create attributes
$attributes = ['debit_to_credit' => [
    'label' => 'Debit to Credit',
    'input' => 'boolean',
    'type' => 'int',
    'backend' => 'eav/entity_attribute_backend_array',
    'note' => 'Marks this product to be available in debit to credit flows',
    'required' => false,
]];

foreach ($attributes as $code => $options) {
    $attributeId = $installer->getAttributeId(Mage_Catalog_Model_Product::ENTITY, $code);
    if (!$attributeId) {
        $options['user_defined'] = 1;
        $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, $code, $options);
    }
}
unset($attributes);

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;
$attributeCode = "debit_to_credit";

$attributeSets = [
    "Mobile_Accessory",
    "Mobile_Additional_Service",
    "Mobile_Data_Tariff",
    "Mobile_Device",
    "Mobile_Footnote",
    "Mobile_Prepaid_Tariff",
    "Mobile_Simcard",
    "Mobile_Voice_Data_Tariff"
];

foreach ($attributeSets as $attributeSetCode) {
    $attributeSetId = $installer->getAttributeSet($entityTypeId, $attributeSetCode, "attribute_set_id");
    $installer->addAttributeToSet($entityTypeId, $attributeSetId, "Mobile", $attributeCode);
}

$installer->endSetup();
