<?php
$installer = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();

// update custom attribute "position" to "product_position"
$installer->updateAttribute('catalog_product', 'position', array('attribute_code' => 'product_position'));

// add "product_position" to every product set
$attributeSet = Mage::getModel('eav/entity_type')
    ->getCollection()
    ->addFieldToFilter('entity_type_code', 'catalog_product')
    ->getFirstItem();

$attributeSetCollection = Mage::getModel('eav/entity_type')
    ->load($attributeSet->getId())
    ->getAttributeSetCollection();

foreach ($attributeSetCollection as $item) {
    $attributeSetId    = $installer->getAttributeSetId('catalog_product', $item->getAttributeSetName());
    $attributeGroupId  = $installer->getAttributeGroupId('catalog_product', $attributeSetId, "General");
    $attributeId       = $installer->getAttributeId('catalog_product', "product_position");

    $installer->addAttributeToSet('catalog_product', $attributeSetId, $attributeGroupId, $attributeId);
}

$installer->endSetup();
