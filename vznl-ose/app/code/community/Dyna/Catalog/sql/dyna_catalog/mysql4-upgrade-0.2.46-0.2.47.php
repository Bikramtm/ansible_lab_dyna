<?php
/**
 * Copyright (c) 2017. Dynacommerce B.V.
 */

/** @var Dyna_Cable_Model_Resource_Setup $this */

$this->getConnection()
    ->addIndex(
        $this->getTable('catalog_product_entity_varchar'),
        $this->getIdxName('catalog_product_entity_varchar', array('value'), Varien_Db_Adapter_Interface::INDEX_TYPE_INDEX),
        array('value'), Varien_Db_Adapter_Interface::INDEX_TYPE_INDEX
    );

$this->endSetup();
