<?php
/**
 * Copyright (c) 2017. Dynacommerce B.V.
 */

/* @var $this Mage_Sales_Model_Entity_Setup */
$this->startSetup();

$this->addAttribute(Mage_Catalog_Model_Product::ENTITY, "product_family", array(
    'type' => 'text',
    'group' => 'General',
    'sort_order' => 100,
    'input' => 'multiselect',
    'label' => 'Product families',
    'backend' => 'eav/entity_attribute_backend_array',
    'visible' => true,
    'source' => 'dyna_catalog/productFamilyAttribute',
    'user_defined' => 1,
    'required' => false,
    'visible_on_front' => true,
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
));

$this->addAttribute(Mage_Catalog_Model_Product::ENTITY, "product_version", array(
    'type' => 'text',
    'group' => 'General',
    'sort_order' => 110,
    'input' => 'multiselect',
    'label' => 'Product version',
    'backend' => 'eav/entity_attribute_backend_array',
    'visible' => true,
    'source' => 'dyna_catalog/productVersionAttribute',
    'user_defined' => 1,
    'required' => false,
    'visible_on_front' => true,
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
));

$this->addAttribute(Mage_Catalog_Model_Product::ENTITY, "lifecycle_status", array(
    'type' => 'int',
    'group' => 'General',
    'sort_order' => 90,
    'input' => 'select',
    'label' => 'Lifecycle status',
    'backend' => 'eav/entity_attribute_backend_array',
    'visible' => true,
    'source' => 'dyna_catalog/productLifecycleStatusAttribute',
    'user_defined' => 1,
    'required' => false,
    'visible_on_front' => true,
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
));

$this->endSetup();
