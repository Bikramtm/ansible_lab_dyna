<?php
/**
 * OMNVFDE-885
 * Remove old attributes in attributeSets
 */

/** @var Mage_Eav_Model_Entity_Setup $installer */
$installer = $this;
$installer->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;
// Old attributes
$oldAttributes = [
    'account_sub_type',
    'account_type',
    'acquisition_numbers',
    'acs_enabled',
    'addon_type',
    'adsl2_plus',
    'agent_groups_visibility',
    'allowed_for_belated_porting',
    'allowed_for_debit_credit_swap',
    'annex_j_enabled',
    'assignment',
    'backend_technology',
    'bookable_in_sales_channel',
    'bookable_sales_force_id',
    'ccb_package_code',
    'condition_service_name',
    'condition_service_price',
    'connection_type',
    'contract_duration',
    'cost',
    'country_codes',
    'country_of_manufacture',
    'current_telephone_company',
    'customer_class',
    'custom_design',
    'custom_design_from',
    'custom_design_to',
    'custom_layout_update',
    'directory_entry_amount',
    'directory_entry_type',
    'directory_usage_type',
    'direct_debit',
    'display_frontend_category',
    'downstream',
    'dsl',
    'dsl_realized',
    'dsl_resale_tariff_allowed',
    'fax_entry_allowed',
    'fee_hints_priority',
    'fee_name',
    'fee_price_once',
//    'fee_priority', // => app/code/local/Dyna/Checkout/Model/Sales/Quote.php:289, Still needed for getting product priority in quote
    'fee_subtype',
    'fn_change_end_date_bc',
    'fn_change_ex_compat_target_tat',
    'fn_change_ex_restricted_tsp',
    'fn_change_ex_restricted_vat',
    'fn_change_new_allowed_tat',
    'fn_change_new_restricted_tsp',
    'fn_change_new_restricted_vat',
    'fn_change_start_date_bc',
    'fn_new_device_allowed_tat',
    'fn_new_device_restricted_vat',
    'fn_new_end_date_nc',
    'fn_new_restricted_tsp',
    'fn_new_start_date_nc',
    'free_tv_package',
    'fttx',
//    'gallery', Magento default attribute, it might cause problems in magento core if removed
    'gift_message_available',
    'gimli_id',
    'gross_package_price_brutto',
//    'group_price', // => app/code/core/Mage/Bundle/Model/Product/Price.php:575, Magento default attribute, it might cause problems in magento core if removed
    'gui_hint_text',
    'hardware_gui_name',
    'hw_materianumer',
    'hw_sub_type',
//    'hw_type', // => app/code/local/Dyna/Fixed/Model/Import/FixedProduct.php:299, Still used to determine the attribute set on fixed products import
    'iad_hardware_class',
    'identifier_simcard_allowed',
    'identifier_simcard_formfactor',
    'identifier_simcard_selector',
//    'image', // => app/code/core/Mage/Catalog/Model/Resource/Product.php:684, Magento default attribute, it might cause problems in magento core if removed
    'imei_mandatory',
    'instant_access',
    'inventory_replacement_product',
    'inverse_search_indicator',
    'iptv',
    'iptv_service_name',
    'ipv6_enabled',
    'isdn_capable_add_hw_device',
    'isdn_enabled',
    'isdn_network_capable_nt_spi',
//    'is_deleted', // => app/code/local/Dyna/Catalog/Helper/Data.php:28, app/code/local/Dyna/Configurator/Model/Catalog.php:231, as well in community - Used in omnius core to flag product as deleted
    'is_recurring',
    'license_type',
    'lte_capable_add_hw_device',
    'main_access_service_code',
    'main_access_technology',
    'manufacturer',
//    'media_gallery', // => app/code/core/Mage/Catalog/Model/Product.php:1039 , app/code/community/Omnius/Import/Model/Import/Product/Abstract.php:374 , Magento default attribute, it might cause problems in magento core if removed
    'member_type',
    'meta_description',
    'meta_keyword',
    'meta_title',
    'monthly_price',
//    'msrp', // => app/code/core/Mage/Catalog/Model/Api2/Product/Validator/Product.php:154, Magento default attribute, it might cause problems in magento core if removed
//    'msrp_display_actual_price_type', // => app/code/core/Mage/Catalog/Model/Api2/Product/Validator/Product.php:150, Magento default attribute, it might cause problems in magento core if removed
    'msrp_enabled',
//    'news_from_date', // => app/code/core/Mage/Catalog/Block/Product/New.php:102 , app/code/core/Mage/Adminhtml/controllers/Catalog/ProductController.php:519 , Magento default attribute, it might cause problems in magento core if removed
//    'news_to_date', // => same as above, Magento default attribute, it might cause problems in magento core if removed
//    'new_price', Magento default attribute, it might cause problems in magento core if removed
//    'new_price_date', Magento default attribute, it might cause problems in magento core if removed
    'next_best_offer',
    'ngn_dsl_tariff_allowed',
    'notice_id',
    'not_compatible_hint',
    'no_of_new_telephone_numbers',
    'number_s0_lines',
    'one_time_charge',
    'one_time_condition_code',
    'onkz_allowed',
    'online_tariff_allowed',
    'options_container',
    'option_category',
    'option_group',
    'ordered_connection',
    'order_line_title',
    'package_allowed',
//    'page_layout', // => app/code/core/Mage/Catalog/Model/Resource/Category.php:618, Magento default attribute, it might cause problems in magento core if removed
    'payment_mode',
    'portal_group',
    'portal_hide_tw_summary',
    'portal_variable_billing_freq',
    'portal_variable_price',
    'price_once',
//    'price_repeated',
//    'price_sliding', // => app/code/local/Dyna/Package/Model/Package.php:486, Still used in cable products
    'price_subsidized',
    'price_unsubsidized',
    'price_view',
    'print_additional_text',
    'print_name1',
    'print_name2',
    'print_name3',
    'print_text',
    'product_allows_inventory',
    'product_combination',
    'product_hint',
//    'promotion_code', // => app/code/local/Dyna/Cable/Model/Import/CableProduct.php:124, Still used in cable products to form the sku
    'promo_amount',
    'promo_percent_activation_fee',
    'promo_percent_onetime_payment',
    'promo_percent_recurring_charge',
    'recurring_condition_code',
//    'recurring_profile', // => app/code/core/Mage/Adminhtml/Block/Catalog/Product/Edit/Tab/Attributes.php:93, Magento default attribute, it might cause problems in magento core if removed
    'related_pricing_structure_code',
    'relation_radio_group',
    'relation_services',
//    'req_rate_max_bound', Used by GetNonStandardRates service call for cable products
//    'req_rate_min_bound', Used by GetNonStandardRates service call for cable products
//    'req_rate_prod_for_contracts', // => app/code/local/Dyna/Configurator/Helper/Services.php:137, Used by GetNonStandardRates service call for cable products
    'restricted_to',
    'sales_package_id',
    'selection_replacement_product',
//    'small_image', // => app/code/core/Mage/Customer/Block/Account/Dashboard/Sidebar.php:73, Magento default attribute, it might cause problems in magento core if removed
    'som_order_reason',
//    'special_from_date', // => app/code/core/Mage/Adminhtml/controllers/Catalog/ProductController.php:517, Magento default attribute, it might cause problems in magento core if removed
    'special_legal_text',
    'special_legal_text_for_redone',
    'special_legal_text_nr_porting',
//    'special_price', // => app/code/core/Mage/Bundle/Model/Resource/Indexer/Price.php:205, Magento default attribute, it might cause problems in magento core if removed
//    'special_to_date', // => app/code/core/Mage/Catalog/Model/Product/Api.php:365 , Magento default attribute, it might cause problems in magento core if removed
    'subsidy_contract_duration',
    'subsidy_level',
    'subvention_code',
    'tariff_change_keep_on',
    'tariff_change_replace_by',
    'tariff_code',
    'tariff_level',
    'tariff_option_allowed',
    'technology_category',
    'technology_product',
//    'thumbnail', // => app/code/core/Mage/Rss/Block/Catalog/New.php:95, Magento default attribute, it might cause problems in magento core if removed
    'tv',
    'tv_access_type',
    'tv_center_inkl_qos',
    'tv_product_name',
    'tv_service_code',
    'tv_subscriptions_allowed',
    'tv_tariff_allowed',
    'upstream',
//    'url_key', // => app/code/local/Dyna/Catalog/Model/Category.php:33, Magento default attribute, it might cause problems in magento core if removed
    'vdsl2_legacy',
    'vdsl2_vectoring',
    'vdsl_capable_add_vsdl_modem',
    'version',
    'voice_access',
    'voip_tariff_allowed',
//    'weight' // => app/code/local/Dyna/Catalog/Helper/Data.php:268, Magento default attribute, it might cause problems in magento core if removed
];

foreach ($oldAttributes as $attribute) {
    $attributeId = $installer->getAttribute($entityTypeId, $attribute, 'attribute_id');
    if($attributeId){
        $installer->removeAttribute($entityTypeId, $attribute);
    }
}

$installer->endSetup();
