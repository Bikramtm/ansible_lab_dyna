<?php

/** @var Mage_Eav_Model_Entity_Setup $installer */
$installer = $this;
$installer->startSetup();

Mage::getConfig()->saveConfig('cataloginventory/item_options/manage_stock', '0', 'default', 0);

$installer->endSetup();
