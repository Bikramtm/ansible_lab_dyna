<?php
/**
 * Rename column:
 * kias_linked_account => linked_account_number
 */

/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();
/** @var Varien_Db_Adapter_Interface $connection */
$connection = $installer->getConnection();

$table = $this->getTable('catalog_package');

if ($connection->tableColumnExists($table, 'kias_linked_account')) {
    $connection->changeColumn($table, 'kias_linked_account', 'linked_account_number', 'varchar(255) null');
} else {
    $connection
        ->addColumn($table, 'linked_account_number',
            array(
                'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
                'comment' => 'Linked account number',
                'nullable' => true,
                'default' => null,
                'length' => 255
            )
        );
}

$installer->endSetup();
