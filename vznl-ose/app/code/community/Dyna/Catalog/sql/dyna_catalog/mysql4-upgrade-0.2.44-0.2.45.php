<?php
/**
 * Copyright (c) 2017. Dynacommerce B.V.
 */

/** @var Dyna_Cable_Model_Resource_Setup $this */
$this->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

// Remove duplicate attributes
$attrCodesToRemove = array(
    'product_family_id',
    'product_version_id',
);
foreach ($attrCodesToRemove as $attrCode) {
    $this->removeAttribute($entityTypeId, $attrCode);
}

// Update attributes
$updateAttributes = array(
    'ask_confirmation_first',
    'confirmation_text',
    'service_xpath',
);

foreach ($updateAttributes as $attrCode) {
    $attributeId = $this->getAttributeId($entityTypeId, $attrCode);
    if ($attributeId) {
        $this->updateAttribute($entityTypeId, $attributeId, 'is_required', false);
    }
}

$this->endSetup();
