<?php /* @var $installer Dyna_Cable_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();
$entityTypeId = Mage_Catalog_Model_Product::ENTITY;
$groupName = 'General';
$attributeCode = 'preselect';
$createAttributeSets = [];
$attributeId = $installer->getAttributeId($entityTypeId, $attributeCode);
if ($attributeId) {
    $installer->removeAttribute($entityTypeId, $attributeCode);
}
$installer->addAttribute($entityTypeId, $attributeCode, array(
        'label' => 'Preselect',
        'input' => 'boolean',
        'type' => 'int',
        'visible' => true,
        'visible_on_front' => true,
        'required' => false,
        'default' => 1,
        'user_defined' => 1,
        'source' => 'eav/entity_attribute_source_boolean',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    )
);
$attributeSets = ["Mobile_Additional_Service", "Mobile_Data_Tariff", "Mobile_Prepaid_Tariff", "Mobile_Voice_Data_Tariff"];
foreach ($attributeSets as $attributeSetCode) {
    $attributeSetId = $installer->getAttributeSet($entityTypeId, $attributeSetCode, "attribute_set_id");
    $installer->addAttributeToSet($entityTypeId, $attributeSetId, "Mobile", $attributeCode);
}
$installer->endSetup();