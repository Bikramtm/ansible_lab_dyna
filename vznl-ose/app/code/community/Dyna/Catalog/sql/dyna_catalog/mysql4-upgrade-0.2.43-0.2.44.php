<?php
/** @var Dyna_Cable_Model_Resource_Setup $this */
$this->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;
$targetAttributeCode = 'product_visibility';

$attribute = Mage::getModel('eav/config')->getAttribute($entityTypeId, $targetAttributeCode);
if ($attribute->getId())
{
    $newOptions = [
        'attribute_id' => $attribute->getId(),
        'values' => [
            8 => Dyna_Catalog_Model_Product::CATALOG_PRODUCT_VISIBILITY_OPTION_NOT_SHOWN
        ]
    ];
}
$this->addAttributeOption($newOptions);

$this->endSetup();
