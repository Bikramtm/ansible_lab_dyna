<?php
/* @var $this Mage_Sales_Model_Entity_Setup */
$this->startSetup();

$this->addAttribute(Mage_Catalog_Model_Category::ENTITY, 'external_identifier', array(
    'group' => 'General',
    'sort_order' => 5,
    'input' => 'text',
    'label' => 'Unique identifier in the external system.',
    'backend' => '',
    'visible' => true,
    'required' => false,
    'visible_on_front' => true,
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
));

$this->endSetup();
