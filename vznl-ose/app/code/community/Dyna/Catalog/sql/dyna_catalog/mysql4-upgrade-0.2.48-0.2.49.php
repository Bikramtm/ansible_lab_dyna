<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$packageSubTypesTable = 'catalog_package_subtypes';
if ($installer->getConnection()->tableColumnExists($packageSubTypesTable, 'manual_override_allowed')) {
    $installer->getConnection()
        ->changeColumn($packageSubTypesTable,
            'manual_override_allowed',
            'manual_override_allowed',
            array(
                'nullable' => false,
                'type' => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
                'default' => 1,
                'comment' => 'Determines whether products are allowed to be manually added after they have been automatically removed.',
            )
        );
}