<?php

/** @var Mage_Eav_Model_Entity_Setup $installer */
$installer = $this;
$installer->startSetup();

Mage::getConfig()->saveConfig('catalog/seo/product_use_categories', '0', 'default', 0);
Mage::getConfig()->saveConfig('catalog/seo/save_rewrites_history', '0', 'default', 0);
Mage::getConfig()->saveConfig('catalog/seo/search_terms', '0', 'default', 0);
Mage::getConfig()->saveConfig('catalog/seo/site_map', '0', 'default', 0);

$installer->endSetup();
