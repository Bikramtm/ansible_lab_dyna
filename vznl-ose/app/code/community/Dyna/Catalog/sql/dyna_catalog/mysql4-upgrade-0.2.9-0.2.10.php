<?php
$installer = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;
$oldAttributeCode = 'hide_in_config';
$newAttributeCode = 'hide_in_configurator';

$attributeId = $installer->getAttributeId($entityTypeId, $oldAttributeCode);
if ($attributeId) {
    $installer->updateAttribute($entityTypeId, $oldAttributeCode, array('attribute_code' => $newAttributeCode));
}

$installer->endSetup();
?>
