<?php
/**
 * Add attribute redplus_role with possible values (owner/member) to all the mobile products (it is added to all mobile
 * attribute sets in the Mobile group)
 */
/* @var $installer Dyna_Cable_Model_Resource_Setup */

$installer = $this;
$installer->startSetup();
$attributeCode = "redplus_role";

$installer->removeAttribute(Mage_Catalog_Model_Product::ENTITY,$attributeCode);
// Create attributes
$attributes = ['redplus_role' => [
    'label' => 'Red Plus Role',
    'input' => 'select',
    'type' => 'int',
    'backend' => 'eav/entity_attribute_backend_array',
    'option' => [
        'values' => Dyna_Mobile_Model_Product_Attribute_Option::$redPlusRoles,
    ],
    'note' => 'Marks this product to be an owner or member of a RedPlus.',
]];

foreach ($attributes as $code => $options) {
    $attributeId = $installer->getAttributeId(Mage_Catalog_Model_Product::ENTITY, $code);
    if (!$attributeId) {
        $options['user_defined'] = 1;
        $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, $code, $options);
    }
}
unset($attributes);
$entityTypeId = Mage_Catalog_Model_Product::ENTITY;
$attributeCode = "redplus_role";

$attributeSets = [
    "Mobile_Accessory",
    "Mobile_Additional_Service",
    "Mobile_Data_Tariff",
    "Mobile_Device",
    "Mobile_Footnote",
    "Mobile_Prepaid_Tariff",
    "Mobile_Simcard",
    "Mobile_Voice_Data_Tariff"
];

foreach ($attributeSets as $attributeSetCode) {
    $attributeSetId = $installer->getAttributeSet($entityTypeId, $attributeSetCode, "attribute_set_id");
    $installer->addAttributeToSet($entityTypeId, $attributeSetId, "Mobile", $attributeCode);
}

$installer->endSetup();