<?php
/**
 * Installer that assigns the package_subtype attribute the CheckoutOption attribute set
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $this Dyna_Cable_Model_Resource_Setup */

$this->startSetup();

$attrSetId = $this->getAttributeSetId(Dyna_Catalog_Model_Product::ENTITY, "CheckoutOption");
$this->addAttributeToSet(Dyna_Catalog_Model_Product::ENTITY, $attrSetId, "General", Dyna_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR, 10);

$this->endSetup();