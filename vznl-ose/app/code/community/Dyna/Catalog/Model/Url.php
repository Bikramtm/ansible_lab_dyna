<?php

class Dyna_Catalog_Model_Url extends Mage_Catalog_Model_Url
{
    /**
     * @inheritdoc
     */
    protected function _refreshProductRewrite(Varien_Object $product, Varien_Object $category)
    {
        $categoryStoreId = $category->getStoreId();
        if (Mage::getStoreConfigFlag(Mage_Catalog_Helper_Product::XML_PATH_PRODUCT_URL_USE_CATEGORY, $categoryStoreId) ||
            $this->getStoreRootCategory($categoryStoreId)->getId() == $category->getId()
        ) {
            return parent::_refreshProductRewrite($product, $category);
        }

        return $this;
    }
}
