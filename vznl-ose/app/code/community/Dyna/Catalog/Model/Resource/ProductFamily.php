<?php

/**
 * Class Dyna_Catalog_Model_Resource_ProductFamily
 */
class Dyna_Catalog_Model_Resource_ProductFamily extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {
        $this->_init('dyna_catalog/productFamily', 'entity_id');
    }
}
