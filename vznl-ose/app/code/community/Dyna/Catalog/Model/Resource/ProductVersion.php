<?php

/**
 * Class Dyna_Catalog_Model_Resource_ProductVersion
 */
class Dyna_Catalog_Model_Resource_ProductVersion extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {
        $this->_init('dyna_catalog/productVersion', 'entity_id');
    }
}
