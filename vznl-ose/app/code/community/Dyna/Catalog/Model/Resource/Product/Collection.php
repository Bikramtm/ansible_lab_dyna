<?php

class Dyna_Catalog_Model_Resource_Product_Collection extends Omnius_Catalog_Model_Resource_Product_Collection
{
    protected static $sectionProductIds = array();
    protected static $discontinuedProductIds = array();

    /**
     * @return $this
     */
    protected function _initSelect()
    {
        Mage_Catalog_Model_Resource_Product_Collection::_initSelect();
        /**
         * Assure attributes for package type and subtype load by default
         */

        $this->addAttributeToSelect(Dyna_Catalog_Model_Product::CATALOG_PACKAGE_TYPE_ATTR)
            ->addAttributeToSelect(Dyna_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR);

        return $this;
    }


    /**
     * Return a list of inactive products' ids (these have either a future activation date or are expired)
     * @return int[]
     */
    public function getInactiveProducts()
    {
        return $this
            ->addAttributeToFilter(array(
                    array('attribute' => 'effective_date', 'gt' => date("Y-m-d")),
                    array('attribute' => 'expiration_date', 'lt' => date("Y-m-d"))
                )
            )
            ->getSelect()
            ->reset(Varien_Db_Select::COLUMNS)
            ->columns("entity_id")
            ->query()
            ->fetchAll(Zend_Db::FETCH_COLUMN);
    }

    /**
     * Return all product ids for a certain package type
     * @param string $forPackageType
     * @return int[]
     */
    public function getPackageTypeProductIds(string $forPackageType, $websiteId = null)
    {
        if (!$websiteId) {
            $websiteId = Mage::app()->getStore()->getWebsiteId();
        }

        // try loading ids from cache instance
        if (empty(self::$sectionProductIds[$forPackageType])) {
            // if not cached at instance level, try loading these from redis
            $key = "package_type_product_ids_" . $forPackageType . "_" . $websiteId;
            /** @var Dyna_Cache_Model_Cache $cache */
            $cache = Mage::getSingleton('dyna_cache/cache');
            if (!(self::$sectionProductIds[$forPackageType] = unserialize($cache->load($key)))) {
                // Adding package type attribute to filtering
                $attribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', Dyna_Catalog_Model_Product::CATALOG_PACKAGE_TYPE_ATTR);
                $typeValue = Mage::helper('omnius_catalog')->getAttributeAdminLabel($attribute, null, $forPackageType);
                // Adding product visibility attribute to filtering
                $visibilityAttribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', Dyna_Catalog_Model_Product::CATALOG_PACKAGE_PRODUCT_VISIBILITY_ATTR);
                $visibilityTypeValue = Mage::helper('omnius_catalog')->getAttributeAdminLabel($visibilityAttribute, null, Dyna_Catalog_Model_Product::PRODUCT_VISIBILITY_CONFIGURATOR);

                self::$sectionProductIds[$forPackageType] = $this
                    ->addAttributeToFilter(Dyna_Catalog_Model_Product::CATALOG_PACKAGE_TYPE_ATTR, array("finset" => $typeValue))
                    ->addAttributeToFilter(Dyna_Catalog_Model_Product::CATALOG_PACKAGE_PRODUCT_VISIBILITY_ATTR, array("finset" => $visibilityTypeValue))
                    ->addWebsiteFilter($websiteId)
                    ->getSelect()
                    ->reset(Varien_Db_Select::COLUMNS)
                    ->columns("entity_id")
                    ->query()
                    ->fetchAll(Zend_Db::FETCH_COLUMN);
            }

            $cache->save(serialize(self::$sectionProductIds[$forPackageType]), $key, array(Dyna_Cache_Model_Cache::PRODUCT_TAG), $cache->getTtl());
        }

        return self::$sectionProductIds[$forPackageType];
    }

    public function getPackageDiscontinuedProductIds(string $forPackageType, $websiteId = null)
    {
        if (!$websiteId) {
            $websiteId = Mage::app()->getWebsite()->getId();
        }
        $validProductIds = $this->getPackageTypeProductIds($forPackageType, $websiteId);
        /** @var Dyna_Catalog_Model_Lifecycle $lifeCycle */
        $lifeCycle = Mage::getModel('dyna_catalog/lifecycle');
        return (new self())
            ->addFieldToFilter('entity_id', array('in' => $validProductIds))
            ->addAttributeToFilter('lifecycle_status', array('eq' => $lifeCycle::getLifecycleStatusIdByCode(Dyna_Catalog_Model_Lifecycle::TYPE_DISCONTINUED)))
            ->getAllIds();

    }
}
