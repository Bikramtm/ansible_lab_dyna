<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Catalog_Model_Resource_Product
 */
class Dyna_Catalog_Model_Resource_Product extends Omnius_Catalog_Model_Resource_Product
{
    /** @var array */
    protected static $allSkus = false;

    /**
     * Get a list of skus (as keys) and product ids for a specified website
     *
     * @return array
     */
    public function getAllProductSKUs()
    {
        self::loadSkusIdsPairs();
        return static::$allSkus;
    }

    public static function loadSkusIdsPairs()
    {
        if (static::$allSkus === false) {
            $cacheKey = "ALL_SKUS_TO_IDS_CACHE";
            /** @var Dyna_Cache_Model_Cache $cacheModel */
            $cacheModel = Mage::getModel('dyna_cache/cache');
            if (!$serializedResult = $cacheModel->load($cacheKey)) {
                $resource = Mage::getSingleton('core/resource');
                $readConnection = $resource->getConnection('core_read');
                $query = "select UPPER(sku), entity_id from catalog_product_entity";
                self::$allSkus = $readConnection->fetchPairs($query);
                $cacheModel->save(serialize(self::$allSkus), $cacheKey, array($cacheModel::PRODUCT_TAG), $cacheModel->getTtl());
            } else {
                self::$allSkus = unserialize($serializedResult);
            }
        }
    }

    /**
     * Retrieve product id by sku
     * @param   string $sku
     * @return  integer
     */
    public function getIdBySku($sku)
    {
        self::loadSkusIdsPairs();
        return static::$allSkus[strtoupper($sku)] ?? null;
    }

    public function getSkuById($id) {
        self::loadSkusIdsPairs();
        $allIds = array_flip(self::$allSkus);
        return $allIds[$id] ?? null;
    }

    /**
     * Save data related with product
     * Add newly added product to $allSkus cache
     *
     * @param Varien_Object $product
     * @return Mage_Catalog_Model_Resource_Product
     */
    protected function _afterSave(Varien_Object $product)
    {
        self::$allSkus[strtoupper($product->getSku())] = $product->getId();
        return parent::_afterSave($product);
    }

    public function getCategoryIdsWithAnchors($object)
    {
        $cacheKey = "PRODUCT_CATEGORIES_WITH_ANCHOR_" . $object->getId();
        /** @var Dyna_Cache_Model_Cache $cacheModel */
        $cacheModel = Mage::getModel('dyna_cache/cache');
        if (!$serializedResult = $cacheModel->load($cacheKey)) {
            $result = parent::getCategoryIdsWithAnchors($object);
            $cacheModel->save(serialize($result), $cacheKey, array($cacheModel::PRODUCT_TAG), $cacheModel->getTtl());
        } else {
            $result = unserialize($serializedResult);
        }

        return $result;
    }
}
