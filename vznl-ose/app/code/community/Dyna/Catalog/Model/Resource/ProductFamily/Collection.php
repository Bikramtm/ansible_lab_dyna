<?php

/**
 * Class Dyna_Catalog_Model_Resource_ProductFamily_Collection
 */
class Dyna_Catalog_Model_Resource_ProductFamily_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    /**
     * Dyna_Catalog_Model_Resource_ProductFamily_Collection constructor
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('dyna_catalog/productFamily');
    }
}
