<?php

class Dyna_Catalog_Model_Resource_ProcessContext extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init("dyna_catalog/processcontext", "entity_id");
    }
}
