<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Product
 * @method string getProductFamily() Returns a list separated by comma of product families to which current product belongs
 */
class Dyna_Catalog_Model_Product extends Omnius_Catalog_Model_Product
{
    CONST CATALOG_CONSUMER_TYPE = 'product_segment';
    CONST IDENTIFIER_BUSINESS = 'Business';
    CONST IDENTIFIER_CONSUMER = 'Privat';

    CONST CATALOG_PACKAGE_TYPE_ATTR = 'package_type';
    CONST CATALOG_PACKAGE_SUBTYPE_ATTR = 'package_subtype';
    CONST CATALOG_PACKAGE_TYPE_ID = 'package_type_id';
    CONST CATALOG_PACKAGE_SUBTYPE_ID = 'package_subtype_id';
    CONST CATALOG_PACKAGE_PRODUCT_VISIBILITY_ATTR = 'product_visibility';
    CONST CATALOG_PACKAGE_PRODUCT_SERVICE_CATEGORY_ATTR = 'service_category';
    CONST CATALOG_PACKAGE_PRODUCT_OPTION_TYPE_ATTR = 'option_type';

    CONST RELATED_SLAVE_SKU_ATTR = 'related_slave_sku';

    CONST PRODUCT_VISIBILITY_BASKET = 'Basket';
    CONST PRODUCT_VISIBILITY_PRINTOUT = 'Printout';
    CONST PRODUCT_VISIBILITY_OVERVIEW = 'Overview';

    CONST PRODUCT_VISIBILITY_INVENTORY = 'Inventory';
    CONST PRODUCT_VISIBILITY_CONFIGURATOR = 'Configurator';
    CONST PRODUCT_VISIBILITY_SHOPPING_CART = 'Shopping cart';
    CONST PRODUCT_VISIBILITY_EXTENDED_SHOPPING_CART = 'Extended shopping cart';
    CONST PRODUCT_VISIBILITY_ORDER_OVERVIEW = 'Order overview';
    CONST PRODUCT_VISIBILITY_VOICELOG = 'Voice log';
    CONST PRODUCT_VISIBILITY_OFFER_PDF = 'Offer PDF';
    CONST PRODUCT_VISIBILITY_CALL_SUMMARY = 'Call summary';

    // VFDED1W3S-383 - different names should be displayed depending on the gui application section where a product is displayed
    const GUI_CART = "cart";
    const GUI_INVENTORY = "inventory";
    const GUI_CONFIGURATOR = "configurator";
    const GUI_COMMUNICATION = "communication";

    CONST PRODUCT_SERVICE_CATEGORY_KAA = 'KAA';

    CONST CATALOG_PRODUCT_VISIBILITY_OPTION_NOT_SHOWN = 'not shown';

    public static $sections = [
        Dyna_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION => [Dyna_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION],
        Dyna_Catalog_Model_Type::SUBTYPE_DEVICE => [Dyna_Catalog_Model_Type::SUBTYPE_DEVICE],
        Dyna_Catalog_Model_Type::SUBTYPE_ADDON => [Dyna_Catalog_Model_Type::SUBTYPE_ADDON],
        Dyna_Catalog_Model_Type::SUBTYPE_ACCESSORY => [Dyna_Catalog_Model_Type::SUBTYPE_ACCESSORY],
        Dyna_Catalog_Model_Type::SUBTYPE_GOODY => [Dyna_Catalog_Model_Type::SUBTYPE_GOODY],
        Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_DEVICE => [Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_DEVICE],
        Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_SUBSCRIPTION => [Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_SUBSCRIPTION],
        Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_PROMOTION => [Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_PROMOTION],
        Dyna_Catalog_Model_Type::SUBTYPE_PROMOTION => [Dyna_Catalog_Model_Type::SUBTYPE_PROMOTION],
        Dyna_Catalog_Model_Type::MOBILE_PREPAID_SUBSCRIPTION => [Dyna_Catalog_Model_Type::MOBILE_PREPAID_SUBSCRIPTION],
    ];
    /** @var string */
    protected $_packageType = null;

    protected $selectableAttributeData = [];

    protected static $instances = array();
    protected static $skus2ids = array();

    /**
     * Go-to method for getProducts action
     */
    public function getCatalogProducts($params) {
        $websiteId = $params['websiteId'];
        $processContext = $params['processContext'];
        $packageCreationTypeId = $params['packageCreationTypeId'];
        $customerType = $params['customerType'];
        $showPriceWithBtw = $params['showPriceWithBtw'];

        $response = array();
        /** @var Dyna_Configurator_Helper_Data $helper */
        $helper = Mage::helper('dyna_configurator');
        /** @var Dyna_Catalog_Helper_Data $catalogHelper */
        $catalogHelper = Mage::helper('dyna_catalog');

        $generalFilters = $additionalFilters = $permanentFiltersAttributePairs = [];

        /** @var Dyna_Package_Model_PackageCreationTypes $packageCreationType */
        $packageCreationType = Mage::getModel('dyna_package/packageCreationTypes')->load($packageCreationTypeId);
        if ($packageCreationType) {
            /** @var Dyna_Package_Model_PackageType $packageType */
            $packageType = Mage::getModel('dyna_package/packageType')->load($packageCreationType->getPackageTypeId());
            $type = strtolower($packageType->getPackageCode());

            $generalFilters = $packageCreationType->getFilterAttributes($processContext);

            $tmpFilters = $packageCreationType->getPackageSubtypeFilterAttributes();
            if (!empty($tmpFilters)) {
                $additionalFilters = Mage::helper('dyna_configurator')->getSubtypeFilterCondition($tmpFilters);
            }

            foreach ($packageType->getPackageSubTypes() as $packageSubType) {
                if (!$packageSubType->getVisibleConfigurator()) {
                    continue;
                }

                $permanentFiltersAttributePairs[$packageSubType->getPackageCode(true)] = $helper->getAttributePairsPermanentFilters($packageSubType->getPackageCode(true), $generalFilters);
            }
        }

        foreach ($additionalFilters as $section => $filter) {

            if (isset($permanentFiltersAttributePairs[strtolower($section)])) {
                $permanentFiltersAttributePairs[strtolower($section)] = array_merge($permanentFiltersAttributePairs[strtolower($section)], $helper->getAttributePairsPermanentFilters(strtolower($section), $filter));
            } else {
                $permanentFiltersAttributePairs[strtolower($section)] = $helper->getAttributePairsPermanentFilters(strtolower($section), $filter);
            }
        }

        $permanentFiltersSearchText = $helper->getSearchTxtPermanentFilters($generalFilters);

        Mage::unregister('products_visibility_package_type');
        Mage::register('products_visibility_package_type', $type);

        /** @var Dyna_Configurator_Model_Catalog $configuratorTypeModel */
        $configuratorTypeModel = @Mage::getModel(sprintf('dyna_configurator/%s', $type));
        // fall back to catalog model if package type is not extended
        if (!$configuratorTypeModel) {
            $configuratorTypeModel = Mage::getModel('dyna_configurator/catalog');
        }
        // let configuration model know what package type it configures
        $configuratorTypeModel->setPackageType($type);
        $configuratorTypeModel->setCustomerType($customerType);
        $configuratorTypeModel->setShowPriceWithBtw($showPriceWithBtw);
        $products = $configuratorTypeModel->getAllConfiguratorItems($websiteId, $permanentFiltersAttributePairs, $processContext);

        $productFilters = $products['filters'] ?? null;
        $productFilters = isset($productFilters) && is_array($productFilters) && count($productFilters) ? $helper->toArray($productFilters) : [];
        unset($products['filters']);
        $allProducts = count($products) ? $helper->toArray($products) : [];

        // determine if a product should be visible or not
        foreach ($allProducts as $key => &$currentProducts) {
            if ($key != 'consumer_filters') {
                foreach ($currentProducts as &$currentProduct) {
                    /** @var Dyna_Catalog_Model_Product $product */
                    $product = $products[$key]->getItemById($currentProduct['entity_id']);
                    $currentProduct['product_visibility'] = $catalogHelper->getVisibility(
                        Dyna_Catalog_Model_Product::PRODUCT_VISIBILITY_CONFIGURATOR,
                        $currentProduct['sku'],
                        $product,
                        $product->getData('product_visibility')
                    );
                }
                unset($currentProduct);
            }
        }
        unset($currentProducts);

        $response[sprintf('prod_%s', $type)] = $allProducts;
        $response[sprintf('filter_%s', $type)] = $productFilters;

        $processedFamilies = $configuratorTypeModel->processFamilies($type, $allProducts);
        foreach ($processedFamilies['families']['items'] as &$family) {
            // If no family position set, moving it to the end of the list
            $family['family_position'] = isset($family['family_position']) ? (int)$family['family_position'] : 9999;
        }
        $response[sprintf('fam_%s', $type)] = $processedFamilies['families'];

        foreach ($permanentFiltersAttributePairs as $section => $filters) {
            $sectionFilters = array_map(
                function ($filter) {
                    return $filter['string_filter'];
                }, $filters);

            if($sectionFilters) {
                $response[sprintf('permanent_filters_%s', $type)][mb_strtolower($section)] = [
                    'all_permanent_filters' => $sectionFilters,
                    'permanent_search_filters' => $permanentFiltersSearchText
                ];
            }
        }

        if (isset($products['consumer_filters'])) {
            $consumerProductFilters = $products['consumer_filters'];
            $consumerProductFilters = isset($consumerProductFilters) && is_array($consumerProductFilters) && count($consumerProductFilters) ? $helper->toArray($consumerProductFilters) : new stdClass();
            unset($products['consumer_filters']);
            $response[sprintf('filter_consumer_%s', $type)] = $consumerProductFilters;
        }

        return $response;
    }

    /**
     * @param bool $ids
     * @return array Returns an array with the product ids of the special mobile discount products
     */
    public static function getMobileDiscountsIds($ids = true)
    {
        $allMobileDiscount = Dyna_Catalog_Model_Type::getMobileDiscounts();
        $discounts = $allMobileDiscount;
        if (!$ids) {
            return $discounts;
        }

        $discounts = [];
        foreach ($allMobileDiscount as $sku) {
            $discounts[] = Mage::getModel('catalog/product')->getResource()->getIdBySku($sku);
        }

        return $discounts;
    }

    /**
     * Check if the product is a simonly product
     */
    public function getSimOnly()
    {
        return $this->getData('sim_only');
    }

    /**
     * Escape invalid characters from product name
     * @return mixed|string
     */
    public function getName()
    {
        return mb_convert_encoding($this->getData('name'), "UTF-8");
    }

    /**
     * Get regular maf price or maf price if exists
     */
    public function getMaf()
    {
        // Moved to singleton because it queries db or redis about 6000 times... better to keep it on instance
        $attrSet = Mage::helper('dyna_catalog')->getAttributeSet($this->getAttributeSetId());
        // Special case for Cable products
        if ($attrSet == 'KD_Cable_Products') {
            $mafPrice = $this->getData('price_repeated');

            if ($mafPrice) {
                $mafParts = array_map(function ($maf) {
                        return str_replace(',', '.', $maf);
                    }, explode(':', $mafPrice)
                );

                $maf = end($mafParts);

                return $maf ?: 0;
            }
        }

        if (($maf = $this->_getData('maf')) !== null) {
            return $maf;
        }

        return $this->getPromotionalMaf() ?: ($this->getRegularMaf() ?: false);
    }

    /**
     * todo: check if this returns the correct contract duration for each product
     * @return mixed
     */
    public function getContractDuration()
    {
        return $this->getData('contract_duration');
    }


    public function isDevice()
    {
        $is = false;
        foreach (Dyna_Catalog_Model_Type::$devices as $subType) {
            if (!$is && in_array($subType, $this->getType())) {
                $is = true;
                break;
            }
        }

        return $is;
    }

    public function hasMafPrice()
    {
        $has = false;
        foreach (Dyna_Catalog_Model_Type::$hasMafPrices as $subType) {
            if (!$has && in_array($subType, $this->getType())) {
                $has = true;
                break;
            }
        }
        if (!$has && $this->isMobileSpecialDiscount()) {
            $has = true;
        }

        return $has;
    }

    public function isMobileSpecialDiscount()
    {
        $is = false;
        foreach (Dyna_Catalog_Model_Type::getMobileDiscounts() as $sku) {
            if (!$is && $sku == $this->getSku()) {
                $is = true;
                break;
            }
        }

        return $is;
    }

    public function hasOTCPrice()
    {
        return $this->is(Dyna_Catalog_Model_Type::$hasOTCPrices);
    }

    public function isPromotion()
    {
        return $this->is(Dyna_Catalog_Model_Type::$promotions);
    }

    public function isFee()
    {
        if ($this->is(Dyna_Catalog_Model_Type::$fees)) {
            return true;
        }

        if ($this->getSku() == Dyna_Catalog_Model_Type::MOBILE_SHIPPING_SKU) {
            return true;
        }

        return false;
    }

    public function isSubscription()
    {
        return $this->is(Dyna_Catalog_Model_Type::$subscriptions);
    }

    public function isAccessory()
    {
        return $this->is(Dyna_Catalog_Model_Type::$accessories);
    }

    public function isOption()
    {
        return $this->is(Dyna_Catalog_Model_Type::$options);
    }

    public function isDiscount()
    {
        return $this->is(Dyna_Catalog_Model_Type::$discount);
    }

    public function isFootnote()
    {
        return $this->is(Dyna_Catalog_Model_Type::$footnotes);
    }

    /**
     * Checks whether or not current product is migration charge
     * @return bool
     */
    public function isMigrationCharge()
    {
        return in_array($this->getSku(), Dyna_Catalog_Model_Type::getMigrationCharges());
    }

    /**
     * Retrieve assigned category Ids
     * @param int $websiteId
     * @param $subtype string Subtype of the product
     * @return array
     */
    public function getStockCall($websiteId, $subtype, $axiStoreId)
    {
        /** @var Dyna_Cache_Model_Cache $cache */
        $cache = Mage::getSingleton('dyna_cache/cache');
        $product = [];
        $products = [];
        if (is_array($subtype)) {
            foreach ($subtype as $identifier) {
                $productStock = Mage::helper('omnius_catalog')->getProductForStock($websiteId, $identifier, $cache);
                if ($productStock && is_array($productStock)) {
                    $product = array_merge($product, $productStock);
                }
                $productsStock = Mage::helper('omnius_catalog')->getProductsForStock($websiteId, $identifier, $cache, $product);
                if ($productsStock && is_array($productsStock)) {
                    $products = array_merge($products, $productsStock);
                }
            }
        } else {
            $product = Mage::helper('omnius_catalog')->getProductForStock($websiteId, $subtype, $cache);
            $products = Mage::helper('omnius_catalog')->getProductsForStock($websiteId, $subtype, $cache, $product);
        }
        $result = [];

        foreach ($products as $id => $item) {
            $result = Mage::helper('dyna_catalog')->buildStockItem($result, $id, $item, []);
        }

        return $result;
    }

    public function isOwnReceiver()
    {
        return in_array($this->getSku(), Dyna_Catalog_Model_Type::getOwnReceiverSkus());
    }

    public function isOwnSmartcard()
    {
        return (bool)($this->getSku() == Dyna_Catalog_Model_Type::OWN_SMARTCARD_SKU);
    }


    public function hasTagByName($tagName)
    {
        $tags = $this->getTags();

        if (!empty($tags)) {
            $tagsArray = explode(',', $tags);

            foreach ($tagsArray as $tag) {
                if ($tag == $tagName) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Return the extra indicator (if product has any)
     */
    public function getExtraIndicator($indicator)
    {
        $cache = Mage::getSingleton('dyna_cache/cache');
        $cacheKey = 'extra_indicator_id_' . $indicator;

        if (!($optionId = $cache->load($cacheKey))) {
            $attr = Mage::getModel('catalog/product')->getResource()->getAttribute('extra_handle_indicator');
            $optionId = $attr->getSource()->getOptionId($indicator);
            $cache->save($optionId, $cacheKey, array(Dyna_Cache_Model_Cache::PRODUCT_TAG), $cache->getTtl());
        }
        return $this->getData('extra_handle_indicator') == $optionId ? $indicator : null;
    }

    /**
     * Identify the shipping fee product
     */
    public function getShippingFeeProduct()
    {
        $product = $this->loadByAttribute('sku', Dyna_Catalog_Model_Type::MOBILE_SHIPPING_SKU);

        if (!$product || !$product->getId()) {
            $storeId = Mage::helper('core')->getStoreId('admin');
            $taxClassId = Mage::helper('dyna_checkout/tax')->getTaxClassIdByName('TAX_19');
            $allWebsites = Mage::app()->getWebsites();
            $websiteIds = array();
            foreach ($allWebsites as $website) {
                $websiteIds[] = $website->getId();
            }
            $attributeSetId = Mage::getModel("eav/entity_attribute_set")->getCollection()
                ->addFieldToFilter("attribute_set_name", 'Default_Set')
                ->getFirstItem()
                ->getAttributeSetId();

            //get the product visibility ids and package types
            $options = Mage::getSingleton('eav/config')
                ->getAttribute(Mage_Catalog_Model_Product::ENTITY, self::CATALOG_PACKAGE_PRODUCT_VISIBILITY_ATTR)
                ->getSource()->getAllOptions(false);

            $packageTypes = Mage::getSingleton('eav/config')
                ->getAttribute(Mage_Catalog_Model_Product::ENTITY, self::CATALOG_PACKAGE_TYPE_ATTR)
                ->getSource()->getAllOptions(false);

            $visibilityIds = [];
            $packageTypeIds = [];
            $visibilityArray = [
                self::PRODUCT_VISIBILITY_ORDER_OVERVIEW,
                self::PRODUCT_VISIBILITY_VOICELOG,
                self::PRODUCT_VISIBILITY_OFFER_PDF,
                self::PRODUCT_VISIBILITY_CALL_SUMMARY
            ];

            foreach ($options as $option) {
                if (in_array($option['label'], $visibilityArray)) {
                    $visibilityIds[] = $option['value'];
                }
            }

            foreach ($packageTypes as $packageType) {
                $packageTypeIds[] = $packageType['value'];
            }
            $subtype = Mage::helper('omnius_catalog')
                ->getAttributeAdminLabel(
                    Mage::getSingleton('eav/config')->getAttribute(self::ENTITY, self::CATALOG_PACKAGE_SUBTYPE_ATTR),
                    null,
                    Dyna_Catalog_Model_Type::SUBTYPE_FEE
                );

            $product = $this
                ->setWebsiteIds($websiteIds)
                ->setStoreId($storeId)
                ->setAttributeSetId($attributeSetId)
                ->setTypeId(Mage_Catalog_Model_Product_Type::TYPE_SIMPLE)
                ->setCreatedAt(strtotime('now'))
                ->setSku(Dyna_Catalog_Model_Type::MOBILE_SHIPPING_SKU)
                ->setName(Mage::helper('catalog')->__(ucfirst(Dyna_Catalog_Model_Type::MOBILE_SHIPPING_SKU)))
                ->setWeight(1.0000)
                ->setStatus(Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
                ->setPackageSubtype($subtype)
                ->setPackageType($packageTypeIds)
                ->setCheckoutProduct(1)
                ->setTaxClassId($taxClassId)
                ->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH)
                ->setPrice(0)
                ->setProductVisibility($visibilityIds)
                ->setData('stock_data',
                    array(
                        'use_config_min_sale_qty' => 1,
                        'use_config_max_sale_qty' => 1,
                        'use_config_manage_stock' => 1,
                        'use_config_enable_qty_increments' => 1
                    )
                )
                ->save();
        }

        return $product;
    }

    /**
     * Returns all package types as string. For example: "dsl,mobile,lte"
     * @return string
     */
    public function getPackageType()
    {
        // if no id, then product is deleted, than this is a fake product so no attribute value to search for
        if (!$this->getId()) {
            return $this->getData('package_type');
        } else {
            $packageTypeString = $this->getResource()
                ->getAttribute(Dyna_Catalog_Model_Product::CATALOG_PACKAGE_TYPE_ATTR)
                ->getFrontend()
                ->getValue($this);

            $multipleTypes = strpos(",", $packageTypeString);
            $packageType = trim(strtolower($packageTypeString));
            if ($multipleTypes) {
                $packageTypeArray = explode(",", $packageTypeString);
                $packageTypeArrayTrimmed = [];
                foreach ($packageTypeArray as $packageTypeStr) {
                    $packageTypeArrayTrimmed = trim(strtolower($packageTypeStr));
                }
                $packageType = implode(",", $packageTypeArrayTrimmed);
            }
        }

        return $packageType;
    }

    /**
     * Returns package subtype as string. For example: "device"
     * @return string
     */
    public function getPackageSubtypeName()
    {
        return strtolower(current($this->getType()));
    }

    /**
     * Determine whether or not a product is of a certain type
     * @param $type array|string
     * @return bool
     */
    public function is($type)
    {
        if (is_array($type)) {
            foreach ($type as $value) {
                if (in_array(strtolower($value), array_map("strtolower", $this->getType()))) {
                    return true;
                }
            }
            return false;
        } else {
            return in_array(strtolower($type), array_map("strtolower", $this->getType()));
        }
    }

    /**
     * Get product subtype (addon, device, subscription, etc)
     */
    public function getType()
    {
        if ($this->getId() !== null) {
            if ($this->_type === null) {
                $attribute = $this->getResource()->getAttribute(static::CATALOG_PACKAGE_SUBTYPE_ATTR);
                $type = Mage::helper('dyna_catalog')->getAttributeAdminLabel($attribute, $this->getData(static::CATALOG_PACKAGE_SUBTYPE_ATTR));
                $this->_type = json_decode($type, true);
            }
        } else {
            // This is a fake product, deleted from our database
            if ($this->_type === null) {
                $this->_type = $this->getData('type');
            }
        }

        return $this->_type ?: [];
    }

    /**
     * Get true if the product service category: KAA
     */
    public function hasServiceCategoryKAA()
    {
        if ($this->getId() !== null) {
            $product = Mage::getModel('catalog/product')->load($this->getId());
            $seviceCategoryAttribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', 'service_category');
            $serviceCategoryValue = Mage::helper('dyna_catalog')->getAttributeAdminLabel($seviceCategoryAttribute, null, Dyna_Catalog_Model_Type::SERVICE_CATEGORY_VALUE_KAA);
            return ($product->getData('service_category') == $serviceCategoryValue);
        }

        return false;
    }

    /**
     * Get true if the product service category: KAD
     */
    public function hasServiceCategoryKAD()
    {
        if ($this->getId() !== null) {
            $product = Mage::getModel('catalog/product')->load($this->getId());
            $seviceCategoryAttribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', 'service_category');
            $serviceCategoryValue = Mage::helper('dyna_catalog')->getAttributeAdminLabel($seviceCategoryAttribute, null, Dyna_Catalog_Model_Type::SERVICE_CATEGORY_VALUE_KAD);
            return ($product->getData('service_category') == $serviceCategoryValue);
        }

        return false;
    }

    /**
     * Get available filters loading only filterable attributes
     * @return array|mixed
     */
    public function getAvailableFilters()
    {
        $key = sprintf('%s_available_filters', $this->getId());
        if (!($filters = unserialize($this->getCache()->load($key)))) {
            $filters = [];
            $store = Mage::app()->getStore();
            $localeCode = Mage::app()->getLocale()->getLocaleCode();
            $attributeSetId = $this->getAttributeSetId();

            $attributes = Mage::getResourceModel('catalog/product_attribute_collection');
            $attributes->setItemObjectClass('catalog/resource_eav_attribute')
                ->setAttributeSetFilter($attributeSetId)
                ->addStoreLabel($store->getId())
                ->setOrder('position', 'ASC')
                ->addFieldToFilter('additional_table.is_filterable', array('gt' => 0))
                ->load();

            /** @var Mage_Catalog_Model_Resource_Eav_Attribute $attr */
            foreach ($attributes as $attr) {
                Mage::helper('omnius_catalog')->addFilter($attr, $filters, $store, $localeCode, $this);
            }
            $this->getCache()->save(serialize($filters), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
        }

        return $filters;
    }

    /**
     * @param bool $onlyFamily
     * @return array|mixed
     */
    public function getCategoryIds($onlyFamily = false)
    {
        $key = md5(serialize([strtolower(__METHOD__), $this->getId(), $onlyFamily]));
        if (!($result = unserialize($this->getCache()->load($key)))) {
            if ($onlyFamily === false) {
                $result = Mage_Catalog_Model_Product::getCategoryIds();
            } else {
                $all = Mage_Catalog_Model_Product::getCategoryIds();
                $collection = Mage::getModel('catalog/category')
                    ->getCollection()
                    ->addAttributeToFilter('is_family', 1)
                    ->addFieldToFilter('entity_id', ['in' => $all]);
                $result = [];
                foreach ($collection as $category) {
                    /** @var Dyna_Catalog_Model_Category $category */
                    $productsPositions = $category->getProductsPosition();
                    if (isset($productsPositions[$this->getId()])) {
                        $result[$productsPositions[$this->getId()]] = $category->getId();
                    }
                }
            }
            $this->getCache()->save(serialize($result), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
        }

        return $result;
    }

    public function getCategoryNames($onlyFamily = false)
    {
        $key = md5(serialize([strtolower(__METHOD__), $this->getId(), $onlyFamily]));
        if (!($result = unserialize($this->getCache()->load($key)))) {
            $categoryCollection = Mage::getModel('catalog/category')
                ->getCollection()
                ->addNameToResult()
                ->addIdFilter($this->getCategoryIds());
            $result = array();
            foreach ($categoryCollection as $category) {
                $result[] = $category->getName();
            }
            $this->getCache()->save(serialize($result), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
        }

        return $result;
    }

    /**
     * Determine if a product is initially selectable in the configurator
     * @return bool|mixed
     */
    public function getInitialSelectable()
    {
        // Moved to singleton because it queries db or redis about 6000 times... better to keep it on instance
        $attrSet = Mage::helper('dyna_catalog')->getAttributeSet($this->getAttributeSetId());
        // Special case for Cable products
        if ($attrSet == 'KD_Cable_Products') {
            return $this->getData('selectable');
        } else {
            return $this->getData('initial_selectable');
        }
    }

    /**
     * Callback function which called after transaction commit in resource model
     *
     * @return Mage_Catalog_Model_Product
     */
    public function afterCommitCallback()
    {
        $skip_reindex = Mage::registry('import-skip-reindex-on-save') ?: 0;
        if (!$skip_reindex) {
            Mage_Catalog_Model_Product::afterCommitCallback();
        }
    }

    /**
     * If is red plus owner => is eligible for red
     * @return bool
     */
    public function isRedPlusOwner()
    {
        return $this->getAttributeText('redplus_role') == Dyna_Mobile_Model_Product_Attribute_Option::RED_PLUS_ROLE_OWNER;
    }

    /**
     * Check if product is eligible to be member for a red+ group
     * @return bool
     */
    public function isRedPlusMember()
    {
        return $this->getAttributeText('redplus_role') == Dyna_Mobile_Model_Product_Attribute_Option::RED_PLUS_ROLE_MEMBER;
    }

    /**
     * Get product ids by a set of skus
     *
     * @param array $skus
     * @return array of skus keys and entity_id values
     */
    public function getIdsBySkus(array $skus)
    {
        $result = array();
        foreach ($skus as $sku) {
            $result[$sku] = $this->getIdBySku($sku);
        }

        return $result;
    }

    /**
     * Get product skus by a set of ids
     *
     * @param string $ids
     * @return array of entity_id keys and sku values
     */
    public function getSkusByIds(array $ids)
    {
        $result = array();
        foreach ($ids as $id) {
            $result[$id] = $this->getSkuById($id);
        }

        return $result;
    }

    /**
     * Retrive product sku by id
     *
     * @param   integer $id
     * @return  string
     */
    public function getSkuById($id)
    {
        return $this->_getResource()->getSkuById($id);
    }

    protected function _beforeSave()
    {
        // If "Not shown" option for product_visibility exists, remove the other options

        /** @var Mage_Eav_Model_Entity_Attribute $attribute */
        $attribute = Mage::getSingleton('eav/config')
            ->getAttribute(Mage_Catalog_Model_Product::ENTITY, Dyna_Catalog_Model_Product::CATALOG_PACKAGE_PRODUCT_VISIBILITY_ATTR);
        $notShownOptionId = $attribute->getSource()->getOptionId(Dyna_Catalog_Model_Product::CATALOG_PRODUCT_VISIBILITY_OPTION_NOT_SHOWN);

        $product_visibility = $this->getData('product_visibility') ?? [];
        if (!is_array($product_visibility)) {
            $product_visibility = [$product_visibility];
        }
        if (in_array($notShownOptionId, $product_visibility)) {
            $this->setData('product_visibility', [$notShownOptionId]);
        }

        parent::_beforeSave();
    }

    /**
     * Return PackageType id for current product
     * @return mixed|null
     */
    public function getPackageTypeId()
    {
        /** @var Dyna_Package_Model_PackageType $packageModel */
        $packageModel = Mage::getModel('dyna_package/packageType');

        return $packageModel->getPackageTypeIdByCode($this->getPackageType());
    }

    /**
     * Return PackageSubtype for current product
     * @return mixed
     */
    public function getPackageSubtypeModel()
    {
        /** @var Dyna_Package_Model_PackageSubtype $packageSubtypeModel */
        $packageSubtypeModel = Mage::getModel('dyna_package/packageSubtype');

        return $packageSubtypeModel->loadByPackageIdAndCode(
            $this->getPackageTypeId(),
            $this->getPackageSubtypeName()
        );
    }

    /**
     * Returns an array of the slave SKUs of this product
     * Attribute: related_slave_sku
     *
     * @return array
     */
    public function getSlaveSkus()
    {
        $cacheKey = md5(serialize([strtolower(__METHOD__), strtolower($this->getId())]));

        if ($data = $this->getCache()->load($cacheKey)) {
            return unserialize($data);
        } else {
            $attributeValue = $this->getResource()
                ->getAttribute(self::RELATED_SLAVE_SKU_ATTR)
                ->getFrontend()
                ->getValue($this);

            $slaveSkus = array_filter(explode(',', $attributeValue));

            $this->getCache()->save(serialize($slaveSkus), $cacheKey, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
            return $slaveSkus;
        }
    }

    /**
     * Returns the first product version text/code
     * @return string
     */
    public function getProductVersionCode()
    {
        $internalVersionIds = ($versionData = $this->getProductVersion()) ? explode(",", $versionData) : array();

        return Dyna_Catalog_Model_ProductVersion::getProductVersionCodeById(current($internalVersionIds));
    }
}
