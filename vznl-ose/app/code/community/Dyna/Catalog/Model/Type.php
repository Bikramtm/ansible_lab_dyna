<?php

/**
 * Class Dyna_Catalog_Model_Type
 */
class Dyna_Catalog_Model_Type extends Omnius_Catalog_Model_Type
{
    // general type used in the application to group types that are not relevant to a section; this is not imported and is not present in the db.
    const TYPE_OTHER = 'Other';

    // FN package types
    const TYPE_FIXED_DSL = 'DSL';
    const TYPE_LTE = 'LTE';

    // Cable package types
    const TYPE_CABLE_TV = 'CABLE_TV';
    const TYPE_CABLE_INTERNET_PHONE = 'CABLE_INTERNET_PHONE';
    const TYPE_CABLE_INDEPENDENT = 'CABLE_INDEPENDENT';

    // Mobile package types
    const TYPE_MOBILE = 'Mobile';
    const TYPE_PREPAID = 'Prepaid';

    const TYPE_HARDWARE = 'Hardware';
    const TYPE_REDPLUS = 'Redplus';

    // Package creation types
    const CREATION_TYPE_HARDWARE = 'Hardware';
    const CREATION_TYPE_REDPLUS = 'Postpaid_RedPlus';
    const CREATION_TYPE_MOBILE = 'Postpaid';
    const CREATION_TYPE_PREPAID = 'Prepaid';
    const CREATION_TYPE_DSL = 'DSL';
    const CREATION_TYPE_LTE = 'LTE';
    const CREATION_TYPE_CABLE_INTERNET_PHONE = 'Cable_internet_phone';
    const CREATION_TYPE_CABLE_TV = 'Cable_tv';
    const CREATION_TYPE_CABLE_INDEPENDENT = 'Cable_independent';

    const SUBTYPE_SUBSCRIPTION = 'Tariff';
    const SUBTYPE_DEVICE_SUBSCRIPTION = 'DeviceSubscription';
    const SUBTYPE_ADDON = 'Option';
    const SUBTYPE_GOODY = 'Goody';
    const SUBTYPE_SETTING = 'Setting';

    // Configurable Checkout product package subtype
    const SUBTYPE_CONFIGURABLE_CHECKOUT_OPTIONS = 'ConfigurableCheckoutOptions';

    // Unknown package subtypes
    const SUBTYPE_CHECKOUTOPTION = 'CheckoutOption';
    const SUBTYPE_HIDDEN = 'hidden';
    const SUBTYPE_ADDITIONAL_SERVICES = 'Additional_services';
    const SUBTYPE_FOOTNOTE = 'Footnote';

    const SUBTYPE_FEE = 'Fee';
    const SUBTYPE_ACCESSORY = 'Accessory';
    const SUBTYPE_DEVICE = 'Hardware';

    const MOBILE_PREPAID_SUBSCRIPTION = 'Tariff';
    const MOBILE_PREPAIID_SUBTYPE_DEVICE = 'Device';

    const MOBILE_SUBTYPE_SUBSCRIPTION = 'Tariff';
    const MOBILE_SUBTYPE_DEVICE = 'Device';
    const MOBILE_SUBTYPE_ADDON = "Option";
    const MOBILE_SUBTYPE_PROMOTION = 'Discount';
    const MOBILE_SUBTYPE_DISCOUNTS = 'Discounts';

    const DSL_SUBTYPE_SUBSCRIPTION = 'Tariff';
    const DSL_SUBTYPE_PROMOTION = 'Promotion';
    const DSL_SUBTYPE_DEVICE = 'Hardware';
    const DSL_SUBTYPE_ADDON = 'Option';

    const LTE_SUBTYPE_SUBSCRIPTION = 'Tariff';
    const LTE_SUBTYPE_PROMOTION = 'Promotion';
    const LTE_SUBTYPE_DEVICE = 'Hardware';
    const LTE_SUBTYPE_ADDON = 'Option';

    const CABLE_SUBTYPE_RECEIVER = "RC";
    const CABLE_SUBTYPE_SMARTCARD = "SC";

    const COMFORT_CONNECTION_CATEGORY = "Comfort Access";
    const BASIC_CONNECTION_CATEGORY = "Basic Access";

    const SERVICE_CATEGORY_VALUE_KAV = 'KAV';
    const SERVICE_CATEGORY_VALUE_KAA = 'KAA';
    const SERVICE_CATEGORY_VALUE_KAD = 'KAD';

    const INSTALL_INSTRUCTIONS_CABLE_SKU = "6P0:DG1";

    // The SKU of the cable installation product
    const INSTALLATION_SERVICE_CABLE_SKU = "ES1";

    // The skus that require serial number input OMNVFDE-2634
    const OWN_RECEIVER_PRODUCT_TAG = "own_rec_device";
    const OWN_SMARTCARD_SKU = "SCOWN";

    // todo add real products sku if these producuts should be hardcoded also in the future;
    // else remove these and add the functionality that will create them automatically
    const MIGRATION = "migration"; //DSL to Cable
    const MOVE_OFFNET = "moveOffnet"; //Cable to DSL
    const CABLE_INSTALLATION_TAX = "CABLE_TAX";

    const CABLE_MODEM_SUBTYPE = "CM";

    const MOBILE_SHIPPING_SKU = 'shippingfee';

    const IBAN_LENGTH = 22;

    const DEFAULT_CARRIER_CODE = 'DEU.VFDE';

    const CABLE_DEFAULT_CARRIER_LABEL = 'Kabel Deutschland';

    const TELEPHONE_NUMBER_NG1 = "NG1";
    const TELEPHONE_NUMBER_TNF = "TNF";

    public static $creationTypesMapping = [
        self::TYPE_MOBILE => self::CREATION_TYPE_MOBILE,
        self::TYPE_PREPAID => self::CREATION_TYPE_PREPAID,
        self::TYPE_CABLE_INDEPENDENT => self::CREATION_TYPE_CABLE_INDEPENDENT,
        self::TYPE_CABLE_INTERNET_PHONE => self::CREATION_TYPE_CABLE_INTERNET_PHONE,
        self::TYPE_CABLE_TV => self::CREATION_TYPE_CABLE_TV,
        self::TYPE_FIXED_DSL => self::CREATION_TYPE_DSL,
        self::TYPE_LTE => self::CREATION_TYPE_LTE
    ];

    public static $unknown = [
        self::SUBTYPE_CHECKOUTOPTION,
        self::SUBTYPE_HIDDEN,
        self::SUBTYPE_ADDITIONAL_SERVICES,
        self::SUBTYPE_FOOTNOTE
    ];
    public static $devices = [
        self::MOBILE_SUBTYPE_DEVICE,
        self::SUBTYPE_DEVICE,
    ];
    public static $subscriptions = [
        self::MOBILE_SUBTYPE_SUBSCRIPTION,
        self::SUBTYPE_SUBSCRIPTION,
        self::MOBILE_PREPAID_SUBSCRIPTION,
        self::DSL_SUBTYPE_SUBSCRIPTION,
        self::LTE_SUBTYPE_SUBSCRIPTION,
    ];
    public static $accessories = [
        self::SUBTYPE_ACCESSORY,
    ];
    public static $options = [
        self::MOBILE_SUBTYPE_ADDON,
        self::DSL_SUBTYPE_ADDON,
        self::SUBTYPE_ADDON,
    ];
    public static $promotions = [
        //self::MOBILE_SUBTYPE_PROMOTION,
        self::SUBTYPE_GOODY,
        self::DSL_SUBTYPE_PROMOTION,
    ];
    public static $discount = [
        self::MOBILE_SUBTYPE_PROMOTION,
        self::MOBILE_SUBTYPE_DISCOUNTS,
    ];
    public static $fees = [
        self::SUBTYPE_FEE,
    ];
    public static $configurableCheckoutOptions = [
        self::SUBTYPE_CONFIGURABLE_CHECKOUT_OPTIONS,
    ];
    public static $footnotes = [
        self::SUBTYPE_FOOTNOTE,
    ];
    public static $hasMafPrices = [
        self::MOBILE_SUBTYPE_ADDON,
        self::DSL_SUBTYPE_ADDON,
        self::SUBTYPE_ADDON,
        self::SUBTYPE_GOODY,
        self::MOBILE_SUBTYPE_PROMOTION,
        self::MOBILE_SUBTYPE_SUBSCRIPTION,
        self::MOBILE_PREPAID_SUBSCRIPTION,
        self::SUBTYPE_SUBSCRIPTION,
        self::DSL_SUBTYPE_PROMOTION,
        self::DSL_SUBTYPE_DEVICE,
        self::SUBTYPE_FEE,
    ];
    public static $hasOTCPrices = [
        self::MOBILE_SUBTYPE_DEVICE,
        self::SUBTYPE_DEVICE,
        self::MOBILE_SUBTYPE_SUBSCRIPTION,
        self::SUBTYPE_SUBSCRIPTION,
        self::MOBILE_PREPAID_SUBSCRIPTION,
        self::SUBTYPE_FEE,
        self::MOBILE_SUBTYPE_PROMOTION,
        self::SUBTYPE_GOODY,
        self::DSL_SUBTYPE_PROMOTION,
        self::MOBILE_SUBTYPE_ADDON,
    ];

    protected static $cache = null;
    protected static $ownReceiverSkus = array();
    protected static $ownReceiverIds = array();

    /**
     * Method that returns product skus marked as own receivers
     * @return bool|Dyna_Cache_Model_Cache|false|Mage_Core_Model_Abstract|mixed|null
     */
    public static function getOwnReceiverSkus()
    {
        if (empty(self::$ownReceiverSkus)) {
            $productsWithOwnReceiverTag = Mage::getModel('dyna_configurator/catalog')->getProductsByTag(self::OWN_RECEIVER_PRODUCT_TAG);
            $ownReceiverSkus = [];
            foreach ($productsWithOwnReceiverTag as $product) {
                $ownReceiverSkus[] = $product->getSku();
            }

            self::$ownReceiverSkus = $ownReceiverSkus;
        }

        return self::$ownReceiverSkus;
    }

    public static function getOwnReceiverIds()
    {
        if (empty($ownReceiverIds)) {
            $productsWithOwnReceiverTag = Mage::getModel('dyna_configurator/catalog')->getProductsByTag(self::OWN_RECEIVER_PRODUCT_TAG);
            $ownReceiverIds = [];
            foreach ($productsWithOwnReceiverTag as $product) {
                $ownReceiverIds[] = $product->getId();
            }

            self::$ownReceiverIds = $ownReceiverIds;
        }
        //product ids
        return self::$ownReceiverIds;
    }

    public static function getMobileDiscounts()
    {
        return [
            'student' => self::getMobileStudentDiscount(),
            'disabled' => self::getMobileDisabledDiscount(),
            'young' => self::getMobileYoungDiscount(),
        ];

    }

    //CHANGE PROVIDER FILTER
    public static function getCarrierCodeFilter()
    {
        return Mage::getStoreConfig('checkout/cart/carrier_exclusions');
    }

    public static function getMobileInsuranceCodeCategories()
    {
        return Mage::getStoreConfig('checkout/cart/mobile_insurance_categories');
    }

    public static function getMobileStudentDiscount()
    {
        return Mage::getStoreConfig('checkout/cart/student_discount');
    }

    public static function getMobileDisabledDiscount()
    {
        return Mage::getStoreConfig('checkout/cart/disabled_discount');
    }

    public static function getMobileYoungDiscount()
    {
        return Mage::getStoreConfig('checkout/cart/young_discount');
    }

    public static function getStudentDiscountMaxAge()
    {
        return Mage::getStoreConfig('checkout/cart/student_discount_max_age');
    }

    public static function getYoungDiscountMaxAge()
    {
        return Mage::getStoreConfig('checkout/cart/young_discount_max_age');
    }

    // YOUBIAGE
    public static function getYouBiageCategory()
    {
        return Mage::getStoreConfig('checkout/cart/youbiage_birthday_check_category');
    }

    public static function getYouBiageOption()
    {
        return Mage::getStoreConfig('checkout/cart/youbiage_birthday_check_sku');
    }

    public static function getYouBiageMinAge()
    {
        return Mage::getStoreConfig('checkout/cart/youbiage_birthday_check_min_age');
    }

    public static function getYouBiageMaxAge()
    {
        return Mage::getStoreConfig('checkout/cart/youbiage_birthday_check_max_age');
    }

    /**
     * Category containing Young tariffs
     */
    public static function getMobileYoungCategory()
    {
        return Mage::getStoreConfig('checkout/cart/young_birthday_check_category');
    }

    /**
     * @return mixed
     */
    public static function getMobileYoungMaxAge()
    {
        // If it has young discount override the max age
        if (true == Mage::helper("dyna_validators/checkout")->hasYoungDiscount()
            && Mage::getStoreConfig('checkout/cart/young_discount_max_age') < Mage::getStoreConfig('checkout/cart/young_birthday_check_max_age')
        ) {
            return Mage::getStoreConfig('checkout/cart/young_discount_max_age');
        }
        return Mage::getStoreConfig('checkout/cart/young_birthday_check_max_age');
    }

    /**
     * @return mixed
     */
    public static function getMobileYoungMinAge()
    {
        return Mage::getStoreConfig('checkout/cart/young_birthday_check_min_age');
    }


    /**
     * New Customer min age
     */
    public static function getNewCustomerMinimumAge()
    {
        return Mage::getStoreConfig('checkout/cart/new_customer_min_age');
    }

    public static function getNewPrepaidCustomerMinimumAge()
    {
        return Mage::getStoreConfig('checkout/cart/new_customer_prepaid_min_age');
    }

    public static function getPhonebookNoInversion()
    {
        return Mage::getStoreConfig('checkout/cart/telephone_book_no_inversion');
    }

    public static function getPhonebookWithAddress()
    {
        return Mage::getStoreConfig('checkout/cart/telephone_book_with_address');
    }

    public static function getPhonebookNewNumber()
    {
        return Mage::getStoreConfig('checkout/cart/telephone_book_new_number');
    }

    public static function getPhonebookOneNumber()
    {
        return Mage::getStoreConfig('checkout/cart/telephone_book_one_number');
    }

    public static function getPhonebookAbreviatedFirstname()
    {
        return Mage::getStoreConfig('checkout/cart/telephone_book_abreviated_firstname');
    }

    public static function getPhonebookPrinted()
    {
        return Mage::getStoreConfig('checkout/cart/telephone_book_printed');
    }

    public static function getPhonebookSecondNoPorting()
    {
        return Mage::getStoreConfig('checkout/cart/telephone_book_second_no_porting');
    }

    public static function getPhonebookPortPickupSecondNoPorting()
    {
        return Mage::getStoreConfig('checkout/cart/telephone_book_port_pickup_second_no_porting');
    }

    public static function getPhonebookNoIndividualConnection()
    {
        return Mage::getStoreConfig('checkout/cart/telephone_book_no_individual_connection');
    }

    public static function getPhonebookDeleteConnectionData()
    {
        return Mage::getStoreConfig('checkout/cart/telephone_book_delete_connection_data');
    }

    public static function getPhonebookSaveConnectionData()
    {
        return Mage::getStoreConfig('checkout/cart/telephone_book_save_connection_data');
    }

    public static function getSendInvoiceByPostSkus()
    {
        return Mage::getStoreConfig('checkout/cart/send_invoice_by_post');
    }

    public static function getCheckoutSelfReferrerSkus()
    {
        return Mage::getStoreConfig('checkout/cart/checkout_self_referrer');
    }

    public static function getFixedTelephoneBookOneEntrySkus()
    {
        return Mage::getStoreConfig('checkout/cart/fixed_telephone_book_one_entry');
    }

    public static function getFixedTelephoneBookSecondEntrySkus()
    {
        return Mage::getStoreConfig('checkout/cart/fixed_telephone_book_second_entry');
    }

    public static function getFixedTelephoneBookThirdEntrySkus()
    {
        return Mage::getStoreConfig('checkout/cart/fixed_telephone_book_third_entry');
    }

    public static function getMobileSendInvoiceByPostSkus()
    {
        return Mage::getStoreConfig('checkout/cart/mobile_send_invoice_by_post');
    }

    public static function getMobileNoConnectionOverviewSkus()
    {
        return Mage::getStoreConfig('checkout/cart/mobile_no_connection_overview');
    }

    public static function getMobileShortenedDestinationNumberSkus()
    {
        return Mage::getStoreConfig('checkout/cart/mobile_shortened_destination_number');
    }

    public static function getMobileFullDestinationNumberSkus()
    {
        return Mage::getStoreConfig('checkout/cart/mobile_full_destination_number');
    }

    public static function getPhonebookStandard()
    {
        return [
            self::getPhonebookNoEntries(),
            self::getPhonebookWithoutAddress(),
            self::getPhonebookPrintedElectronic(),
            self::getPhonebookOneEntry(),
        ];
    }

    public static function getPhonebookNoEntries()
    {
        return Mage::getStoreConfig('checkout/cart/telephone_book_no_entries');
    }

    public static function getPhonebookWithoutAddress()
    {
        return Mage::getStoreConfig('checkout/cart/telephone_book_without_address');
    }

    public static function getPhonebookPrintedElectronic()
    {
        return Mage::getStoreConfig('checkout/cart/telephone_book_printed_electronic');
    }

    public static function getPhonebookOneEntry()
    {
        return Mage::getStoreConfig('checkout/cart/telephone_book_one_entry');
    }

    public static function getOptionOfOptionRemovalPackages()
    {
        return explode(',', Mage::getStoreConfig('catalog/product_dependencies/option_of_option_removal'));
    }

    public static function getOwnSmartcardId()
    {
        return Mage::getModel('catalog/product')->getResource()->getIdBySku(self::OWN_SMARTCARD_SKU);
    }

    public static function getMigrationCharges()
    {
        return [
            self::MIGRATION,
            self::MOVE_OFFNET,
            self::CABLE_INSTALLATION_TAX,
        ];
    }

    public static function getFeesSubtypes()
    {
        return array(
            "Prio KAA",
            "Prio KAD",
            "Prio KAI",
            "Prio Default",
        );
    }

    public static function getSections()
    {
        $packageTypes = Mage::getModel('dyna_package/packageType')
            ->getCollection()
            ->getPackageTypesToArray(true);

        return array_keys($packageTypes);
    }

    public static function getCablePackages()
    {
        return [
            strtolower(self::TYPE_CABLE_TV),
            strtolower(self::TYPE_CABLE_INTERNET_PHONE),
            strtolower(self::TYPE_CABLE_INDEPENDENT),
        ];
    }

    public static function getPackagesThatNeedServiceability()
    {
        $key = md5(serialize([strtolower(__METHOD__)]));
        if (!($data = unserialize(self::getCache()->load($key)))) {
            $data = [];
            $types = Mage::getModel('dyna_package/packageCreationTypes')->getCollection()->addFieldToFilter('requires_serviceability', 1);
            foreach($types as $type) {
                $data[] = strtolower($type->getPackageTypeCode());
            }
            self::getCache()->save(serialize($data), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), self::getCache()->getTtl());
        }
        return $data;
    }

    public static function getMobilePackages()
    {
        return [
            strtolower(self::TYPE_MOBILE),
            strtolower(self::TYPE_PREPAID),
            strtolower(self::TYPE_REDPLUS),
        ];
    }

    public static function getPostPaidMobilePackages()
    {
        return [
            strtolower(self::TYPE_MOBILE),
        ];
    }

    public static function getPrePaidMobilePackages()
    {
        return [
            strtolower(self::TYPE_PREPAID),
            strtolower(self::TYPE_REDPLUS),
        ];
    }

    public static function getFixedPackages()
    {
        return [
            strtolower(self::TYPE_FIXED_DSL),
            strtolower(self::TYPE_LTE),
        ];
    }

    public static function getLtePackages()
    {
        return [
            strtolower(self::TYPE_LTE),
        ];
    }

    public static function getDslPackages()
    {
        return [
            strtolower(self::TYPE_FIXED_DSL),
        ];
    }

    public static function getPackageTypes($lowercase = true)
    {
        $values = [
            self::TYPE_FIXED_DSL,
            self::TYPE_LTE,
            self::TYPE_CABLE_INTERNET_PHONE,
            self::TYPE_CABLE_TV,
            self::TYPE_CABLE_INDEPENDENT,
            self::TYPE_PREPAID,
            self::TYPE_MOBILE,
            self::TYPE_REDPLUS,
        ];

        if ($lowercase) {
            return array_map('strtolower', $values);
        } else {
            return $values;
        }
    }

    /**
     * Return sub_type attribute value for identifying cable receiver products
     * @return string
     */
    public static function getCableReceiverSubtype()
    {
        return self::CABLE_SUBTYPE_RECEIVER;
    }

    /**
     * Return sub_type attribute value for identifying cable smartcard products
     * @return string
     */
    public static function getCableSmartcardSubtype()
    {
        return self::CABLE_SUBTYPE_SMARTCARD;
    }

    public static function getCreationTypesMapping()
    {
        $creationTypesMapping = [
            strtolower(self::TYPE_MOBILE) => self::CREATION_TYPE_MOBILE,
            strtolower(self::TYPE_PREPAID) => self::CREATION_TYPE_PREPAID,
            strtolower(self::TYPE_CABLE_INDEPENDENT) => self::CREATION_TYPE_CABLE_INDEPENDENT,
            strtolower(self::TYPE_CABLE_INTERNET_PHONE) => self::CREATION_TYPE_CABLE_INTERNET_PHONE,
            strtolower(self::TYPE_CABLE_TV) => self::CREATION_TYPE_CABLE_TV,
            strtolower(self::TYPE_FIXED_DSL) => self::CREATION_TYPE_DSL,
            strtolower(self::TYPE_LTE) => self::CREATION_TYPE_LTE
        ];

        return array_map('strtolower', $creationTypesMapping);
    }

    public static function getCableCreationTypes()
    {
        return [
            Mage::helper('dyna_configurator/cart')->getCreationTypeId(Dyna_Catalog_Model_Type::CREATION_TYPE_CABLE_INTERNET_PHONE),
            Mage::helper('dyna_configurator/cart')->getCreationTypeId(Dyna_Catalog_Model_Type::CREATION_TYPE_CABLE_TV),
            Mage::helper('dyna_configurator/cart')->getCreationTypeId(Dyna_Catalog_Model_Type::CREATION_TYPE_CABLE_INDEPENDENT)
        ];
    }

    public static function getMobileCreationTypes()
    {
        return [
            Mage::helper('dyna_configurator/cart')->getCreationTypeId(Dyna_Catalog_Model_Type::CREATION_TYPE_MOBILE),
            Mage::helper('dyna_configurator/cart')->getCreationTypeId(Dyna_Catalog_Model_Type::CREATION_TYPE_PREPAID)
        ];
    }

    /**
     * @return Dyna_Cache_Model_Cache
     */
    protected static function getCache()
    {
        if (!self::$cache) {
            return self::$cache = Mage::getSingleton('dyna_cache/cache');
        }

        return self::$cache;
    }
}
