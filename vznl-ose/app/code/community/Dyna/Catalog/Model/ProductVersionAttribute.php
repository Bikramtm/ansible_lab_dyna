<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Dyna_Catalog_Model_ProductVersionAttribute extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
    protected static $options = false;

    /**
     * Retrieve all product family attributes
     * @return array
     */
    public function getAllOptions()
    {
        if (self::$options === false) {
            self::$options = array();
            $versions = Mage::getModel('dyna_catalog/productVersion')
                ->getCollection();
            /** @var Dyna_Catalog_Model_ProductVersion $version */
            foreach ($versions as $version) {
                self::$options[] = array(
                    "label" => $version->getProductVersionId(),
                    "value" => $version->getId(),
                );
            }
        }

        return self::$options;
    }
}
