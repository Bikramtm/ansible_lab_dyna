<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */


/** Class Dyna_Catalog_Model_CheckoutOptionTypeReferencetable */
class Dyna_Catalog_Model_CheckoutOptionTypeReferencetable extends Mage_Core_Model_Abstract
{
    /**
     * Dyna_Catalog_Model_Checkout_Option_Type_Referencetable constructor.
     */
    protected function _construct() {
        $this->_init('dyna_catalog/checkoutOptionTypeReferencetable');
    }
}
