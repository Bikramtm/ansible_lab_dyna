<?php

/**
 * Class for own smartcard types
 * Class Dyna_Catalog_Model_Smartcard
 */

class Dyna_Catalog_Model_Smartcard extends Mage_Core_Model_Abstract
{
    /**
     * Return current smartcard type
     * @return string
     */
    public function getType()
    {
        return $this->getData("Type");
    }

    /**
     * Return true if current smartcard supports HD
     * @return bool
     */
    public function supportsHd()
    {
        return $this->getData("HD") == "true";
    }

    /**
     * Return current smartcard serial number
     * Using camelcase data keys for aligning with own hardware service validation response which will be added as is to models
     * @return string
     */
    public function getSerialNumber()
    {
        return $this->getData("SmartCardSerialNumber");
    }

    /**
     * Set serial number on current instance
     * Using camelcase data keys for aligning with own hardware service validation response which will be added as is to models
     * @param string $serialNumber
     * @return $this
     */
    public function setSerialNumber(string $serialNumber)
    {
        $this->setData("SmartCardSerialNumber", $serialNumber);

        return $this;
    }
}
