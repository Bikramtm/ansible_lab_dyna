<?php
/**
 * Copyright (c) 2017. Dynacommerce B.V.
 */

class Dyna_Catalog_Model_ProductLifecycleStatusAttribute extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
    /** @var array */
    protected static $options = false;

    /**
     * Retrieve all product family attributes
     * @return array
     */
    public function getAllOptions()
    {
        if (self::$options === false) {
            // reset keys, should be numeric
            $lifeCycles = Dyna_Catalog_Model_Lifecycle::getLifecycleTypes();
            foreach($lifeCycles as $value => $label) {
                self::$options[] = [ 'label' => $label, 'value' => $value ];
            }
        }

        return self::$options;
    }
}
