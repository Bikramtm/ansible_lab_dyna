<?php

/**
 * Class Dyna_Catalog_Model_ProcessContext
 */
class Dyna_Catalog_Model_ProcessContext extends Mage_Core_Model_Abstract
{
    // Acquisition
    const ACQ = "ACQ";
    //ReOrder
    const REO = "REO";
    //Retention / prolongation with BaseLine Update
    const RETBLU = "RETBLU";
    // Retention / prolongation without Baseline Update
    const RETNOBLU = "RETNOBLU";
    // Retention / prolongation with Direct Terminate of old contract
    const RETTER = "RETTER";
    // In Life Sale with Baseline Update
    const ILSBLU = "ILSBLU";
    // In Life Sale without Baseline Update
    const ILSNOBLU = "ILSNOBLU";
    // Migration with Change Of ContractPeriod
    const MIGCOC = "MIGCOC";
    // Migration without Change of ContractPeriod
    const MIGNOCOC = "MIGNOCOC";
    // Move
    const MOVE = "MOVE";
    // Transfer of User
    const TOU = "TOU";

    const INLIFE_START_FLAG = "just-started-inlife-action-flow";

    protected $cache;

    protected function _construct()
    {
        $this->_init("dyna_catalog/processContext");
    }

    /**
     * ILS process contexts
     * @return array
     */
    public static function ILSProcessContexts()
    {
        return [self::ILSBLU, self::ILSNOBLU];
    }

    /**
     * Get cache instance
     * @return Dyna_Cache_Model_Cache
     */
    protected function getCache()
    {
        if (!$this->cache) {
            $this->cache = Mage::getSingleton('dyna_cache/cache');
        }
        return $this->cache;
    }

    /**
     * @return mixed
     */
    public function getAllProcessContextCodeIdPairs()
    {
        // cache key
        $key = 'process_contexts_codes_' . md5(serialize(array(__CLASS__, __METHOD__)));

        if ($result = unserialize($this->getCache()->load($key))) {
            return $result;
        } else {
            $processContexts = $this->getCollection();
            foreach ($processContexts as $context) {
                $result[$context->getCode()] = $context->getId();
            }

            $this->getCache()->save(
                serialize($result),
                $key,
                array(Dyna_Cache_Model_Cache::PRODUCT_TAG),
                $this->getCache()->getTtl()
            );

            return $result;
        }
    }

    /**
     * @return mixed
     */
    public function getAllProcessContextIdCodePairs()
    {
        // cache key
        $key = 'process_contexts_names_' . md5(serialize(array(__CLASS__, __METHOD__)));

        if ($result = unserialize($this->getCache()->load($key))) {
            return $result;
        } else {
            $processContexts = $this->getCollection();
            foreach ($processContexts as $context) {
                $result[$context->getId()] = $context->getCode();
            }

            $this->getCache()->save(
                serialize($result),
                $key,
                array(Dyna_Cache_Model_Cache::PRODUCT_TAG),
                $this->getCache()->getTtl()
            );

            return $result;
        }
    }

    /**
     * @return array|mixed
     */
    public function getAllProcessContextByNamesForForm()
    {
        $processContextCodes = $this->getAllProcessContextIdCodePairs();

        // cache key
        $key = 'process_contexts_form_names_' . md5(serialize(array(__CLASS__, __METHOD__)));
        if ($result = unserialize($this->getCache()->load($key))) {
            return $result;
        } else {
            foreach ($processContextCodes as $contextId => $contextCode) {
                $result[] = array(
                    'label' => $contextCode,
                    'value' => $contextId
                );
            }

            $this->getCache()->save(
                serialize($result),
                $key,
                array(Dyna_Cache_Model_Cache::PRODUCT_TAG),
                $this->getCache()->getTtl()
            );

            return $result;
        }
    }

    /**
     * @param $processContextCode
     * @return mixed
     */
    public function getProcessContextIdByCode($processContextCode)
    {
        // cache key
        $key = 'process_contexts_idByCode_' . md5(serialize(array(__CLASS__, __METHOD__, $processContextCode)));

        if ($result = unserialize($this->getCache()->load($key))) {
            return $result;
        } else {

            $processContextId = $this->getCollection()
                ->addFieldToFilter('code', $processContextCode)
                ->getFirstItem()
                ->getId();

            $this->getCache()->save(
                serialize($processContextId),
                $key,
                array(Dyna_Cache_Model_Cache::PRODUCT_TAG),
                $this->getCache()->getTtl()
            );

            return $processContextId;
        }
    }

    /**
     * Get all process context of prolongation flows
     */
    public static function getProlongationProcessContexts()
    {
        return [self::RETBLU, self::RETNOBLU, self::RETTER];
    }

    /**
     * Get all migration process contexts
     *
     * @return array
     */
    public static function getMigrationProcessContexts()
    {
        return [self::MIGCOC, self::MIGNOCOC];
    }

    public function getProcessContextNameByCode($processContextCode)
    {
        // cache key
        $key = 'process_contexts_nameByCode_' . md5(serialize(array(__CLASS__, __METHOD__, $processContextCode)));

        if ($result = unserialize($this->getCache()->load($key))) {
            return $result;
        } else {

            $processContextId = $this->getCollection()
                ->addFieldToFilter('code', $processContextCode)
                ->getFirstItem()
                ->getName();

            $this->getCache()->save(
                serialize($processContextId),
                $key,
                array(Dyna_Cache_Model_Cache::PRODUCT_TAG),
                $this->getCache()->getTtl()
            );

            return $processContextId;
        }
    }
}
