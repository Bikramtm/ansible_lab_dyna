<?php

/**
 * Class Dyna_Catalog_Model_Import_MnpFreeDays
 */
class Dyna_Catalog_Model_Import_MnpFreeDays extends Dyna_Bundles_Model_Import_Abstract
{
    /** @var string $_logFileName */
    protected $_logFileName = "mnp_free_days";

    /**
     * Import mnp_free_days
     *
     * @param array $data
     *
     * @return void
     */
    public function import($data)
    {
        
        $skip = false;
        //for logging purposes
        $this->_totalFileRows++;
        $data = $this->_MNPsetDataMapping($data);
        
        foreach ($data as $attribute => $value) {
            if (!isset($data[$attribute])) {
                $this->_logError('Skipping line without ' . $attribute);
                $skip = true;
                return;
            }
        }
        
        $skip = $this->_dateFormat($data);

        if ($skip) {
            $this->_skippedFileRows++;
            return;
        }

        $this->_log('Start importing ' . $data['free_day'], false);

        try {
            /** @var Dyna_Bundles_Model_Campaign $campaign */
            $free_date = date('Y-m-d', strtotime($data['free_day']));
            $mnpFreeDays = Mage::getModel('dyna_checkout/mobilePortingFreeDays');
            $existingData = $mnpFreeDays->load($free_date, 'free_day');
            if ($existingData->getEntityId()) {
                $mnpFreeDays = $existingData;
                $this->_log('Loading existing Date "' . $data['free_day'] . '" with ID ' . $mnpFreeDays->getEntityId(), false);
            }

            $params = [
              'free_day',
              'reason',
            ];
            // all parameters are default null
            $defaultParams = array_combine($params, array_fill(0, count($params), null));
            $this->_setDataWithDefaults($mnpFreeDays, $data, $defaultParams);

            if (!$this->getDebug()) {
                $mnpFreeDays->save();
            }

            unset($mnpFreeDays);
            $this->_qties++;

            $this->_log('Finished importing ' . $data['free_day'], false);
        } catch (Exception $ex) {
            echo $ex->getMessage();
            $this->_logEx($ex);
            fwrite(STDERR, $ex->getMessage());
        }
    }

    /**
     * Custom mapping
     *
     * @param array $data
     * @return array
     */
    protected static function _MNPsetDataMapping($data)
    {
        if (is_object($data)) {
            $data = (array) $data;
        }
        
        foreach ($data as &$value) {
            if (is_array($value) && empty($value)) {
                $value = null;
            }
        }
        $result = array(
            'free_day' => $data['SwitchFreeDays'],
            'reason' => $data['Reason']
        );
        return $result;
    }
    /**
     * Custom date format
     *
     * @param array $data
     * @return array
     */
    protected function _dateFormat($data)
    {
        $date = $data['free_day'];
        $format = "d.m.Y";

        if(date($format, strtotime($date)) != date($date)) {
            $status = true;
        }

        return $status;
    }
}
/**
* php import.php --type mnpFreeDays --stack MNPfreedays --file MNPfreedays_mod.xml
*/