<?php

/**
 * Class Dyna_Catalog_Model_Import_MobileProvidersList
 */

 class Dyna_Catalog_Model_Import_MobileProvidersList extends Dyna_Bundles_Model_Import_Abstract
 {
 	
 	/** @var string $_logFileName */
    protected $_logFileName = "msproviders_list";

    /**
     * Import mobile_Operator_List
     *
     * @param array $data
     *
     * @return void
     */
    public function import($data)
    {	
        $skip = false;
        //for logging purposes
        $this->_totalFileRows++;
        $data = $this->_MSPlsetDataMapping($data);
        
        foreach ($data as $attribute => $value) {
            if (!isset($data[$attribute])) {
                $this->_logError('Skipping line without ' . $attribute);
                $skip = true;
                return;
            }
        }
        
        if ($skip) {
            $this->_skippedFileRows++;
            return;
        }

        $this->_log('Start importing ' . $data['code'], false);

        try {
            /** @var Dyna_Bundles_Model_Campaign $campaign */
            $mobileOperatorList = Mage::getModel('dyna_checkout/mobileOperatorServiceProviders');
            $existingData = $mobileOperatorList->load($data['code'], 'code');
            if ($existingData->getEntityId()) {
                $mobileOperatorList = $existingData;
                $this->_log('Loading existing provider "' . $data['code'] . '" with ID ' . $mobileOperatorList->getEntityId(), false);
            }

            $params = [
              'code',
              'description',
              'npind',
              'effective_date',
              'expiration_date',
            ];
            // all parameters are default null
            $defaultParams = array_combine($params, array_fill(0, count($params), null));
            $this->_setDataWithDefaults($mobileOperatorList, $data, $defaultParams);

            if (!$this->getDebug()) {
                $mobileOperatorList->save();
            }

            unset($mobileOperatorList);
            $this->_qties++;

            $this->_log('Finished importing ' . $data['code'], false);
        } catch (Exception $ex) {
            echo $ex->getMessage();
            $this->_logEx($ex);
            fwrite(STDERR, $ex->getMessage());
        }
    }

    /**
     * Custom mapping
     *
     * @param array $data
     * @return array
     */
    protected static function _MSPlsetDataMapping($data)
    {
        if (is_object($data)) {
            $data = (array) $data;
        }
        
        foreach ($data as &$value) {
            if (is_array($value) && empty($value)) {
                $value = null;
            }
        }

        $data['NpInd'] = (int)($data['NpInd'] === 'true');

        $result = array(
            'code' 				=> $data['SpCode'],
            'description' 		=> $data['SpDescription'],
            'npind' 			=> $data['NpInd'],
            'effective_date' 	=> $data['EffectiveDate'],
            'expiration_date' 	=> $data['ExpirationDate']
        );
        return $result;
    }
 }
 /**
* php import.php --type mobileProvidersList --stack MobileProvidersList --file Mobileproviderlist_mod.xml
*/