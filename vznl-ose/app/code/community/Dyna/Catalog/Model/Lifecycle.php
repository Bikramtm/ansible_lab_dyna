<?php

/**
 * Class Dyna_Catalog_Model_Lifecycle
 */
class Dyna_Catalog_Model_Lifecycle extends Mage_Core_Model_Abstract
{
    const TYPE_LIVE = "live";
    const TYPE_LEGACY = "legacy";
    const TYPE_DISCONTINUED = "discontinued";
    const TYPE_FUTURE = "future";

    /**
     * Return available product lifecycle statuses
     * DO NOT CHANGE VALUES ORDER in this array
     * @return string[]
     */
    public static function getLifecycleTypes()
    {
        $response = array(
            0 => static::TYPE_LIVE,
            1 => static::TYPE_LEGACY,
            2 => static::TYPE_DISCONTINUED,
            3 => static::TYPE_FUTURE,
        );

        return $response;
    }

    /**
     * Return lifecycle status id by code
     * @param $code
     * @return null
     */
    public static function getLifecycleStatusIdByCode($code)
    {
        $found = array_search($code, self::getLifecycleTypes());

        return $found;
    }
}
