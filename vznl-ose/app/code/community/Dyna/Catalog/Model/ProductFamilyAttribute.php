<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Dyna_Catalog_Model_ProductFamilyAttribute extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
    /** @var array */
    protected static $options = false;

    /**
     * Retrieve all product family attributes
     * @return array
     */
    public function getAllOptions()
    {
        if (self::$options === false) {
            self::$options = array();
            $families = Mage::getModel('dyna_catalog/productFamily')
                ->getCollection();
            /** @var Dyna_Catalog_Model_ProductFamily $family */
            foreach ($families as $family) {
                self::$options[] = array(
                    "label" => $family->getProductFamilyId(),
                    "value" => $family->getId(),
                );
            }
        }

        return self::$options;
    }
}
