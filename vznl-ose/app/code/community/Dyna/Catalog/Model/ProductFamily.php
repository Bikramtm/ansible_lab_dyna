<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */


/** Class Dyna_Catalog_Model_ProductFamily */
class Dyna_Catalog_Model_ProductFamily extends Mage_Core_Model_Abstract
{
    const LIFECYCLE_LIFE = "Live";
    const LIFECYCLE_LEGACY = "Legacy";
    const LIFECYCLE_DISCONTINUE = "Discontinued";
    const LIFECYCLE_FUTURE = "Future";

    protected static $productFamilies = false;

    /**
     * Dyna_Catalog_Model_ProductFamily constructor.
     */
    public function _construct()
    {
        $this->_init('dyna_catalog/productFamily');
    }

    /*
    * Return product version id based on version code
    * @return int
    */
    public static function getProductFamilyIdByCode(string $familyCode)
    {
        if (self::$productFamilies === false) {
            /** @var Dyna_Catalog_Model_Resource_ProductFamily_Collection $collection */
            $collection = (new self())
                ->getCollection();
            $collection->getSelect()
                ->reset(Varien_Db_Select::COLUMNS)
                ->columns(array('product_family_id', 'entity_id'));
            self::$productFamilies = $collection->getConnection()->fetchPairs($collection->getSelect()->assemble());
        }

        return self::$productFamilies[$familyCode] ?? null;
    }
}
