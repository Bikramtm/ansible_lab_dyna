<?php

/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Dyna_DynaSecurity_Model_Api_User extends Mage_Api_Model_User
{
    /**
     * Login
     *
     * @param string $username
     * @param string $apiKey
     * @return $this
     */
    public function login($username, $apiKey)
    {
        if (Mage::helper('core')->isModuleEnabled('Dyna_LoginSecurity')) {
            $sessId = $this->getSessid();

            Mage::dispatchEvent('authenticate_before', array(
                'model' => $this,
                'login' => $username,
                'section' => 'apiUser'
            ));

            if ($this->authenticate($username, $apiKey)) {
                $this->setSessid($sessId);
                $this->getResource()->cleanOldSessions($this)
                    ->recordLogin($this)
                    ->recordSession($this);

                Mage::dispatchEvent('authenticate_success', array(
                    'login' => $username,
                    'section' => 'apiUser'
                ));

                Mage::dispatchEvent('api_user_authenticated', array(
                    'model' => $this,
                    'api_key' => $apiKey,
                ));
            } else {
                Mage::dispatchEvent('authenticate_failed', array(
                    'model' => $this,
                    'login' => $username,
                    'section' => 'apiUser'
                ));
            }

            return $this;
        } else {
            return parent::login($username, $apiKey);
        }
    }

    /**
     * Authenticate user name and api key and save loaded record
     *
     * @param string $username
     * @param string $apiKey
     * @return boolean
     */
    public function authenticate($username, $apiKey)
    {
        if (Mage::helper('core')->isModuleEnabled('Dyna_PasswordHash')) {
            $this->loadByUsername($username);
            if (!$this->getId()) {
                return false;
            }

            Mage::dispatchEvent('authenticate_before_validate', array(
                'model' => $this
            ));

            $auth = Mage::helper('core')->validateHash($apiKey, $this->getApiKey());
            if ($auth) {
                return true;
            } else {
                $this->unsetData();

                return false;
            }
        } else {
            return parent::authenticate($username, $apiKey);
        }
    }
}
