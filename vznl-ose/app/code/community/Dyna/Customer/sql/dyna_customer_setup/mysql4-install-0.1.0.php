<?php
/**
 * Installer that creates the checkout fields resource table
 */

/** @var Mage_Core_Model_Resource_Setup $this */
$installer = $this;

$installer->startSetup();

// Add customer number on sales order resource
$this->getConnection()->addColumn($this->getTable("customer/entity"), "customer_number", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 20,
    "after" => "ban",
    "comment" => "External customer number to which the current customer maps to",
]);

$installer->endSetup();
