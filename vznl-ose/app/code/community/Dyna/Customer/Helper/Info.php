<?php

/**
 * Get customer data from customer model and prepare it to be sent to frontend templates
 * Class Dyna_Customer_Helper_Info
 */

class Dyna_Customer_Helper_Info extends Mage_Core_Helper_Abstract
{
    /**
     * Return an array containing customer data needed for left_details.phtml template
     * Called from Customer/DetailController loadCustomerOnSession()
     */
    public function buildCustomerDetails(Dyna_Customer_Model_Customer $customer = null, $mode = null, $prospectCustomer = false)
    {
        /**
         * @var $panelHelper Dyna_Customer_Helper_Panel
         */
        $panelHelper = Mage::helper('dyna_customer/panel');
        $customerSession = Mage::getSingleton('customer/session');

        // If not customer sent as parameter, load the one from session
        // If it's passed, set it on the session as the current one that's
        // present on the session does not have all the data assigned to it
        // and it's used further in the execution to make the service calls
        if (!$customer) {
            $customer = $customerSession->getCustomer();
        } else {
            $customerSession->setCustomer($customer);
        }

        // Using Varien_Object data array
        $customerData = new Varien_Object();

        if (($mode != 1) && (!$prospectCustomer)) {
            $customerData->setData('orders_count', $panelHelper->getOrdersCount($customer));
            $customerData->setData('products_count', $panelHelper->getProductsCount());
            $customerData->setData('permissionMessages', $panelHelper->getStackPermissionMessages());

            if ($mode == 2) {
                return $customerData->getData();
            }
        } else {
            $customerData->setData('orders_count', 0);
            $customerData->setData('products_count', 0);
        }

        $customerData->setData('hasMobile', $this->currentCustomerHasMobile());
        $customerData->setData('hasCable', $this->currentCustomerHasCable());
        $customerData->setData('hasFixed', $this->currentCustomerHasFixed());
        $customerData->setData("isSoho", $customer->getIsSoho());
        $customerData->setData("title", $customer->getTitle());
        $customerData->setData("isLinked", $customer->isLinked());
        $customerData->setData("isProspect", $customer->isProspect());
        $customerData->setData("first_name", $customer->getFirstName());
        $customerData->setData("last_name", $customer->getLastName());
        $customerData->setData("street", $customer->getStreet());
        $customerData->setData("no", $customer->getHouseNo());
        $customerData->setData("house_addition", $customer->getHouseAddition());
        $customerData->setData("postal_code", $customer->getPostalCode());
        $customerData->setData("city", $customer->getCity());
        $customerData->setData("state", $customer->getState());
        $customerData->setData("contact_number_prefix", $customer->getLocalAreaCode());
        $customerData->setData("contact_number", $customer->getPhoneNumber());
        $customerData->setData("email", $customer->getEmailAddress());
        if($customer->getDob()){
            $customerData->setData("birth_date", date('d.m.Y', strtotime($customer->getDob())));
        }
        $customerData->setData("kvk_number", $customer->getCompanyRegistrationNumber());
        $customerData->setData("customer_number", $customer->getCustomerNumber());
        $customerData->setData("global_id", $customer->getCustomerGlobalId());
        $customerData->setData("service_address_id", $customer->getServiceAddressId());
        $customerData->setData("company_name", $customer->getCompanyName());
        $customerData->setData("contactPerson", [
            "title" => $customer->getTitle(),
            "first_name" => $customer->getFirstName(),
            "last_name" => $customer->getLastName(),
        ]);


        $customerData->setData('carts_count', $panelHelper->getCartsCount($customer));
        $customerData->setData('in_dunning', $customer->getDunningStatus());
        if ($legalAddress = $customer->getAddress()) {
            $customerData->setData('legal_address_id', $customer->getLegalAddressId());
        }

        $customerType = $customer->getAccountCategory();
        $customerData->setData('customer_type', $customerType);
        // Legal address should be used
        if($legalAddress) {
            $customerData->setData("legal_address", [
                "id" => $customer->getLegalAddressId(),
                "street" => $legalAddress->getData('street'),
                "no" => $legalAddress->getHouseNumber(),
                "postal_code" => $legalAddress->getPostcode(),
                "city" => $legalAddress->getCity(),
                "house_addition" => $legalAddress->getHouseAddition(),
            ]);
        } else {
            // If no real legal address was found fall back to customer address (prevents the usage of service address)
            $customerData->setData("legal_address", [
                "id" => '',
                "street" => $customerData->getData('street'),
                "no" => $customerData->getData('no'),
                "postal_code" => $customerData->getData('postal_code'),
                "city" => $customerData->getData('city'),
                "house_addition" => $customerData->getData('house_addition'),
            ]);
        }

        $customerData->setData('has_excluded_cable_account', Mage::getSingleton('dyna_customer/session')->getHasExcludedCableAccount());
        Mage::getSingleton('dyna_customer/session')->unsetData('has_excluded_cable_account');
        $customerData->setData('has_excluded_mobile_account', Mage::getSingleton('dyna_customer/session')->getHasExcludedMobileAccount());
        Mage::getSingleton('dyna_customer/session')->unsetData('has_excluded_mobile_account');

        // set counter for displaying in left panel
        $customerData->setData('household_members_count', $customerSession->getData('household_members_count')?? 0);

        $serviceAddress = $customer->getServiceAddress();
        if ($serviceAddress) {
            $customerData->setData("service_address", [
                "id" => $customer->getServiceAddressId(),
                "street" => $serviceAddress->getData('street'),
                "no" => $serviceAddress->getHouseNumber(),
                "postal_code" => $serviceAddress->getPostcode(),
                "city" => $serviceAddress->getCity(),
                "house_addition" => $serviceAddress->getHouseAddition(),
            ]);
        }


        return $customerData->getData();
    }

    
    /**
     * @return Dyna_Customer_Model_Session
     */
    protected function getCustomerSession()
    {
         return Mage::getSingleton('dyna_customer/session');
    }

    /**
     * Get whether the current customer has an installed base of mobile type
     * @return boolean
     */
    public function currentCustomerHasMobile()
    {
        $customerStorage = $this->getCustomerSession();
        $customerProducts = $customerStorage->getServiceCustomers();
        $status = isset($customerProducts[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS]) ? true : false;

        return $status;
    }

    /**
     * Get whether the current customer has an installed base of cable type
     * @return boolean
     */
    public function currentCustomerHasCable()
    {
        $customerStorage = $this->getCustomerSession();
        $customerProducts = $customerStorage->getServiceCustomers();
        $status = isset($customerProducts[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD]) ? true : false;

        return $status;
    }

    /**
     * Get whether the current customer has an installed base of fixed/dsl type
     * @return boolean
     */
    public function currentCustomerHasFixed()
    {
        $customerStorage = $this->getCustomerSession();
        $customerProducts = $customerStorage->getServiceCustomers();
        $status = isset($customerProducts[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN]) ? true : false;

        return $status;
    }
}
