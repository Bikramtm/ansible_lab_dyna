<?php

/**
 * Class Dyna_Customer_Helper_Search Helper
 */
class Dyna_Customer_Helper_Search extends Mage_Core_Helper_Abstract
{
    private $legacySearch = false;

    public function parseCustomerSearchLegacyParams($params)
    {
        $searchParams = [];
        foreach ($params['data'] as $parameter) {
            if (!empty(trim($parameter['value']))) {
                $searchParams[$parameter['name']] = $parameter['value'];
            }
        }

        /**
         * @var $validationHelper Dyna_Customer_Helper_Validation
         */
        $validationHelper = Mage::helper('dyna_customer/validation');
        return $validationHelper->validateCustomerLegacySearchParameters($searchParams);
    }

    /**
     * Parsing the form input search fields and validating them;
     * If the fields are not valid => the error messages will be returned
     *
     * @param array $params
     * @return array
     */
    public function parseCustomerSearchParams($params)
    {
        $searchParams = [];
        foreach ($params['data'] as $parameter) {
            if (!empty(trim($parameter['value']))) {
                $searchParams[$parameter['name']] = trim($parameter['value']);
            }
        }

        if (!$searchParams) {
            return [
                'error' => 1,
                "message" => $this->__('Please refine your search. No parameters given.')
            ];
        }

        /**
         * @var $validationHelper Dyna_Customer_Helper_Validation
         */
        $validationHelper = Mage::helper('dyna_customer/validation');
        // todo validate the parameters
        return $validationHelper->validateCustomerSearchParameters($searchParams);
    }

    /**
     * Parse the search customer results from the service and return only an array that contains the customer accounts
     * @param array $customersResult
     * @return array
     */
    public function parseCustomerSearchResults($customersResult)
    {
        $parsedCustomerResults = [];
        // even if the results is with a single account -> add it to an array for consistency
        if (!empty($customersResult['Success']) && $customersResult['Success']) {
            if (!empty($customersResult['Account']['AccountClass'])) {
                $parsedCustomerResults[] = $customersResult['Account'];
            } else {
                $parsedCustomerResults = isset($customersResult['Account'][0]) ? $customersResult['Account'] : (isset($customersResult['Account']) ? array($customersResult['Account']) : '');
            }
        }

        return $parsedCustomerResults;
    }

    /**
     * @param $customerResultFromService
     * @param string|bool $legacySearch
     * @return array
     */
    public function parseCustomerSearchAndGroupLinkedAccounts($customerResultFromService, $legacySearch)
    {
        $this->legacySearch = $legacySearch;
        $parsedCustomerResults = $this->parseCustomerSearchResults($customerResultFromService);
        $linkedAccounts = [];

        /**
         * set the linked accounts as a set of arrays in key ['linkedAccounts'] on the first array that was found with a certain contact.id
         *
         * the first account that is found with a certain contact.id is considered as a "parent" account and we will set isLinkedChild = false;
         * the rest of the accounts that are found with the same contact.id we will consider them as children and we weill set isLinkedChild = true;
         *
         * we will also set on the "parent" account the KG, KIAS and FN options based on all the linked children options. meaning if 1 child has FN and the parent account
         * has KG => the "parent" account will have stacks[FN] = true; stacks[KG] = true; stacks[KIAS] = false;
         */
        if(count($parsedCustomerResults) > 0) {
            foreach ($parsedCustomerResults as $key => $customer) {
                $parentAccountId = $customer['ParentAccountNumber']['ID'];
                $parsedCustomerResults[$key]['isTemporary'] = false !== strpos($parentAccountId, Dyna_Customer_Helper_Customer::TEMPORARY_CUSTOMER_PREFIX) ? 1 : 0;
                // the customers from service are not prospect
                $parsedCustomerResults[$key]['isProspect'] = 0;
                $parsedCustomerResults[$key]['legacyCustomer'] = $this->legacySearch;
                if (array_key_exists('Role', $customer) && array_key_exists('Person', $customer['Role']) && array_key_exists('BirthDate', $customer['Role']['Person'])) {
                    $parsedCustomerResults[$key]['Role']['Person']['BirthDate'] = !empty($customer['Role']['Person']['BirthDate']) ? date('d.m.Y', strtotime($customer['Role']['Person']['BirthDate'])) : '';
                }

                // potential links are available only for non legacy search
                if (!$this->legacySearch) {
                    $contactId = $customer['Contact']['ID'];
                    $this->setDefaultsForNotFoundLinkedStacks($parsedCustomerResults[$key]['stacks']);
                    if (!isset($linkedAccounts[$contactId])) {
                        $this->setParentLinkedAccount($contactId, $key, $parsedCustomerResults, $linkedAccounts);
                    } else {
                        $this->setChildLinkedAccount($contactId, $key, $parsedCustomerResults, $linkedAccounts);
                    }
                } else {
                    // Deducts the ParentAccountID by removing first 3 chars. Because legacy search will never return a link.
                    $parsedCustomerResults[$key]['ParentAccountNumber'] = array('ID' => substr($parsedCustomerResults[$key]['Contact']['ID'], 3));
                }
            }

            // potential links are available only for non legacy search
            if (!$this->legacySearch) {
                // sort the customers to have the linked customers ordered and grouped
                usort($parsedCustomerResults, function ($a, $b) {
                    return $a['Contact']['ID'] > $b['Contact']['ID'];
                });
                $this->handleLinkedAccountsTemporaryStatus($parsedCustomerResults);
            }
        }

        // legacy search does not have links available therefore for legacy search we count $parsedCustomerResults
        return ['customers' => $parsedCustomerResults,
            'countUnlinkedCustomers' => $this->legacySearch ? count($parsedCustomerResults) : count($linkedAccounts),
        ];
    }

    /**
     * @param $contactId // the ID which is used to identify the linked accounts
     * @param $key // the key from the parsedCustomersArray that gives us the current customer
     * @param $parsedCustomerResults // all customers
     * @param $linkedAccounts //  an array with linked accounts as a dictionary with the keys = contact.id to fast determine if exists linked accounts
     */
    private function setParentLinkedAccount($contactId, $key, &$parsedCustomerResults, &$linkedAccounts)
    {
        // we create an array linkedAccounts accounts as a dictionary with the keys = contact.id to fast determine if exists linked accounts
        $linkedAccounts[$contactId] = ['linkedId' => $parsedCustomerResults[$key]['ParentAccountNumber']['ID'], 'accountKey' => $key];
        // set this account in the "linkedAccounts" array
        $parsedCustomerResults[$key]['linkedAccounts'][$key] = $parsedCustomerResults[$key];
        // this is not a linked child
        $parsedCustomerResults[$key]['isLinkedChild'] = false;
        // set the stacks for all linked accounts in stacks key
        $this->setAllLinkedAccountsStacks($parsedCustomerResults[$key]['AccountCategory'], $parsedCustomerResults[$key]);
    }

    private function setChildLinkedAccount($contactId, $key, &$parsedCustomerResults, &$linkedAccounts)
    {
        $keyLinkedAccounts = count($parsedCustomerResults[$linkedAccounts[$contactId]['accountKey']]['linkedAccounts']);
        // set this account in the "linkedAccounts" array
        $parsedCustomerResults[$linkedAccounts[$contactId]['accountKey']]['linkedAccounts'][$keyLinkedAccounts] = $parsedCustomerResults[$key];
        // this is a linked child
        $parsedCustomerResults[$key]['isLinkedChild'] = true;
        // set the stacks for all linked accounts in stacks key
        $this->setAllLinkedAccountsStacks($parsedCustomerResults[$key]['AccountCategory'], $parsedCustomerResults[$linkedAccounts[$contactId]['accountKey']]);
    }

    private function setDefaultsForNotFoundLinkedStacks(&$parsedCustomer)
    {
        // set each stack to false if the stack has not been already found as present
        if (!isset($parsedCustomer[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS])) {
            $parsedCustomer[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS] = false;
        }
        if (!isset($parsedCustomer[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD])) {
            $parsedCustomer[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD] = false;
        }
        if (!isset($parsedCustomer[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN])) {
            $parsedCustomer[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN] = false;
        }
    }

    private function setAllLinkedAccountsStacks($accountCategory, &$parsedCustomer)
    {
        if ($accountCategory == Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_ALL) {
            $parsedCustomer['stacks'][Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS] = true;
            $parsedCustomer['stacks'][Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD] = true;
            $parsedCustomer['stacks'][Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN] = true;
        }
        if (in_array($accountCategory, Dyna_Customer_Helper_Services::getMobileStack())) {
            $parsedCustomer['stacks'][Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS] = true;
        }
        if (in_array($accountCategory, Dyna_Customer_Helper_Services::getCableStack())) {
            $parsedCustomer['stacks'][Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD] = true;
        }
        if (in_array($accountCategory, Dyna_Customer_Helper_Services::getFixedStack())) {
            $parsedCustomer['stacks'][Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN] = true;
        }
    }

    public function parseCustomerResultsFromLocalDb($customerResultsFromLocalDb)
    {
        $parsedCustomerResults = [];
        foreach ($customerResultsFromLocalDb as $key => $customer) {
            /** @var Dyna_Customer_Model_Customer $customerModel */
            $customerModel = Mage::getModel('customer/customer');
            $dbShoppingCarts = $customerModel->getShoppingCartsForCustomer($customer['entity_id'], null);

            $hasCable = $hasMobile = $hasFixed = false;
            foreach ($dbShoppingCarts as $cartAndPackages) {
                /** @var Dyna_Package_Model_Package $package */
                foreach ($cartAndPackages['packages'] as $packages) {
                    foreach ($packages as $package) {
                        $hasCable = $package->isCable();
                        $hasMobile = $package->isMobile();
                        $hasFixed = $package->isFixed();
                    }
                }
            }

            $parsedCustomerResults[$key]['AccountClass'] = ($customer['is_business']) ? Dyna_Customer_Helper_Services::ACCOUNT_CLASS_SOHO : Dyna_Customer_Helper_Services::ACCOUNT_CLASS_PRIVATE;
            $parsedCustomerResults[$key]['PartyName']['Name'] = $customer['company_name'];
            $parsedCustomerResults[$key]['Contact']['ID'] = null; // this stores the linked number
            $parsedCustomerResults[$key]['Contact']['ElectronicMail'] = $customer['additional_email'];
            $parsedCustomerResults[$key]['PostalAddress']['StreetName'] = null;
            $parsedCustomerResults[$key]['PostalAddress']['BuildingNumber'] = null;
            $parsedCustomerResults[$key]['PostalAddress']['CityName'] = null;
            $parsedCustomerResults[$key]['PostalAddress']['PostalZone'] = null;
            $parsedCustomerResults[$key][Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS] = $hasMobile;
            $parsedCustomerResults[$key][Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD] = $hasCable;
            $parsedCustomerResults[$key][Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN] = $hasFixed;
            $parsedCustomerResults[$key]['ParentAccountNumber']['ID'] = $customer['entity_id']; // this stores the customer ID
            $parsedCustomerResults[$key]['phoneNumber'] = $customer['phone_number'];
            $parsedCustomerResults[$key]['phoneLocalAreaCode'] = $customer['phone_local_area_code'];
            $parsedCustomerResults[$key]['BankAccountNumber'] = null;
            $parsedCustomerResults[$key]['Role']['RoleID'] = null;
            $parsedCustomerResults[$key]['Role']['Person']['ID'] = null;
            $parsedCustomerResults[$key]['Role']['Person']['FirstName'] = $customer['firstname'];
            $parsedCustomerResults[$key]['Role']['Person']['FamilyName'] = $customer['lastname'];
            $parsedCustomerResults[$key]['Role']['Person']['BirthDate'] = !empty($customer['dob']) ? date('d.m.Y', strtotime($customer['dob'])) : '';
            $parsedCustomerResults[$key]['isLinkedChild'] = null;
            $parsedCustomerResults[$key]['isProspect'] = 1;
        }

        return $parsedCustomerResults;
    }

    /**
     * Sets temporary flags for linked accounts depending on what type of accounts are linked
     * We must know if a TEMP customer is only linked to TEMP customers in order to display it in the list
     *
     * @param $customersData
     */
    public function handleLinkedAccountsTemporaryStatus(&$customersData)
    {
        foreach ($customersData as $key => $customerDetails) {
            $customersData[$key]['allTemporaryLink'] = $customerDetails['isTemporary'];
            if (!$customerDetails['isLinkedChild'] && $customerDetails['isTemporary'] && count($customerDetails['linkedAccounts']) > 1) {
                foreach ($customerDetails['linkedAccounts'] as $linkedAccountDetails) {
                    if (!$linkedAccountDetails['isTemporary']) {
                        $customersData[$key]['allTemporaryLink'] = false;
                        break;
                    }
                }
            }
        }
    }

    /**
     * @param Mage_Sales_Model_Quote $quote
     * @return void
     */
    public function saveDataForExternalServices($quote)
    {
        // Placeholder method to handle some additional operations on quote object
    }
}
