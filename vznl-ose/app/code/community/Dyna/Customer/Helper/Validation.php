<?php

/**
 * Class Dyna_Customer_Helper_Validation Helper
 */
class Dyna_Customer_Helper_Validation extends Mage_Core_Helper_Abstract
{
    protected $errorMessage = [];
    protected $error = 0;
    protected $response = [];
    protected $searchParams = [];

    const FIRST_NAME_MAX_LENGTH = 8;
    const LAST_NAME_MAX_LENGTH = 30;
    const EMAIL_REGEX = '/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/';
    /**
     * Validation fields for myProducts buttons
     * @var array
     */
    public static $myProductsActionButtons = [
        'modify_package' => [
            'validation' => [
                'mapped_skus',
                'tariff',
                'open_order'
            ]
        ],
        'renew_package' => [
            'validation' => [
                'mapped_skus',
                'tariff',
                'open_order'
            ]
        ],
        'change_service' => [
            'validation' => [
                'address_id',
                'open_order'
            ]
        ],
        "add_red" => [
            'validation' => [
                'tariff',
            ]
        ],
        "create_red" => [
            'validation' => [
                'tariff',
            ]
        ],
        "relocation" => [
            'validation' => [
                'tariff',
                'open_order'
            ]
        ],
        "convert_to_member" => [
            'validation' => [
                'tariff',
                'open_order'
            ]
        ],
        "pre_post" => [
            'validation' => [
                'open_order',
            ],
        ]
    ];

    /**
     * @param $params
     * @return array
     */
    public function validateCustomerLegacySearchParameters($params)
    {
        $this->searchParams = $params;
        $this->validateLegacySearchType();
        $this->validateLegacySearchParams();

        ($this->error) ? $this->setValidationErrorResponse() : $this->setSuccessResponse($params);
        return $this->response;
    }

    private function validateLegacySearchType()
    {
        if (!isset($this->searchParams['legacy_type'])) {
            $this->errorMessage['legacy_type'] = $this->__("Please select a legacy type!");
            $this->error = true;
            return;
        }

        if (!in_array(strtolower($this->searchParams['legacy_type']),
            [strtolower(Dyna_Customer_Helper_Services::CODE_CABLE),
                strtolower(Dyna_Customer_Helper_Services::CODE_DSL),
                strtolower(Dyna_Customer_Helper_Services::CODE_MOBILE)])
        ) {
            $this->errorMessage['legacy_type'] = $this->__("Legacy type selected is invalid!");
            $this->error = true;
            return;
        }
    }

    private function validateLegacySearchParams()
    {
        switch (strtolower($this->searchParams['legacy_type'])) {
            case strtolower(Dyna_Customer_Helper_Services::CODE_MOBILE):
                $this->validateLegacyMobileSearchParams();
                break;
            case strtolower(Dyna_Customer_Helper_Services::CODE_DSL):
                $this->validateLegacyDslSearchParams();
                break;
            case strtolower(Dyna_Customer_Helper_Services::CODE_CABLE):
                $this->validateLegacyCableSearchParams();
                break;
        }
    }

    /**
     * @param $params
     * @return array
     */
    public function validateCustomerSearchParameters($params)
    {
        if (!$this->validateAtLeastOneSearchFieldIsPresent($params)) {
            return $this->response;
        }
        $this->validateCustomerContractOrderNumber();
        $this->validatePhoneNumber($params);
        $this->validateCompanyName($params);
        $this->validateFirstName($params);
        $this->validateLastName($params);
        $this->validateBirthDate($params);
        $this->validateStreet($params);
        $this->validateHouseNo($params);
        $this->validateZipCode($params);
        $this->validateCity($params);
        $this->validateAddress($params);
        $this->validateDeviceId($params);
        $this->validateEmail($params);

        ($this->error) ? $this->setValidationErrorResponse() : $this->setSuccessResponse($params);
        return $this->response;
    }

    /**
     * Validate the customer id
     * @param array $param
     * @return array
     */
    public function validateContactId($param)
    {
        if (empty($param['contactId'])) {
            $this->errorMessage['contactId'] = $this->__("Contact Id could not be found");
            $this->setValidationErrorResponse();
        } else {
            $this->setSuccessResponse($param);
        }
        return $this->response;
    }

    /**
     * Validate the customer id
     * @param array $param
     * @return array
     */
    public function validateParentAccountNumberId($param)
    {
        if (empty($param['parentAccountNumberId'])) {
            $this->errorMessage['parentAccountNumberId'] = $this->__("\"Parent account number id could not be found");
            $this->setValidationErrorResponse();
        } else {
            $this->setSuccessResponse($param);
        }
        return $this->response;
    }

    public function validateAuthParams($param)
    {
        if (empty($param['contactId'])) {
            $this->errorMessage['contactId'] = $this->__("Contact Id could not be found");
        }
        if (empty($param['parentAccountNumberId'])) {
            $this->errorMessage['parentAccountNumberId'] = $this->__("Parent account number id could not be found");
        }
        if ($this->errorMessage) {
            $this->setValidationErrorResponse();
        } else {
            $this->setSuccessResponse($param);
        }
        return $this->response;
    }

    /**
     * Validate the BAN of a mobile product
     * @param $param
     * @return array
     */
    public function validateBan($param)
    {
        if (empty($param['ban'])) {
            $this->errorMessage['ban'] = $this->__("Please provide the BAN!");
            $this->setValidationErrorResponse();
        } else {
            $this->setSuccessResponse($param);
        }
        return $this->response;
    }


    /**
     * Validate the accounts to be active and to exist on the customer with the same kias
     * @param array $accounts
     * @return array
     */
    public function validateAccountStatus($accounts)
    {
        if (empty($accounts)) {
            $this->errorMessage['accounts'] = $this->__("Please provide accounts for retention check!");
            $this->setValidationErrorResponse();
            return $this->response;
        }

        /** @var Dyna_Customer_Model_Session $customerStorage */
        $customerStorage = Mage::getSingleton('dyna_customer/session');
        $customerProducts = $customerStorage->getCustomerProducts();

        if (!empty($customerProducts) && !empty($customerProducts[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS])) {
            foreach ($accounts as $contractNo => $data) {
                $kias = $data['kiasNo'];
                if (empty($customerProducts[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS][$kias])) {
                    $this->errorMessage['accounts'][$contractNo] = $kias . " " . $this->__("KIAS number does not exist on this customer!");
                    $this->setValidationErrorResponse();
                    return $this->response;
                } else {
                    foreach ($customerProducts[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS][$kias] as $mobileAccount) {
                        if ($mobileAccount['contract_nr'] == $contractNo &&
                            $mobileAccount['product_status'] != Dyna_Customer_Helper_Customer::MOBILE_PRODUCT_STATUS_ACTIVE
                        ) {
                            $this->errorMessage['accounts'][$contractNo] = $this->__("Not possible: account suspended.");
                        }
                    }
                }
            }

            if (!empty($this->errorMessage)) {
                $this->setErrorResponse();
                return $this->response;
            }
            $this->setSuccessResponse($accounts);
            return $this->response;
        } else {
            $this->errorMessage['accounts'] = $this->__("No mobile accounts were found for this customer!");
            $this->setValidationErrorResponse();
            return $this->response;
        }
    }

    /**
     * At least one of the SearchAccount, SearchContact or SearchedEquipment is mandatory
     *
     * @param $params
     * @return bool
     */
    private function validateAtLeastOneSearchFieldIsPresent($params)
    {
        if (count($params) == 0) {
            $this->errorMessage['required'] = $this->__("0 matching results found. Please check the spelling or use more search criteria.");
            $this->setValidationErrorResponse();
            return false;
        }
        return true;
    }

    private function validateCustomerContractOrderNumber()
    {
        $this->error = false;
    }

    private function validateCompanyName($params)
    {
        $this->error = false;
    }

    private function validatePhoneNumber($params)
    {
        $this->error = false;
    }

    /**
     * Validate first name
     * - max length 8
     *
     * @param array $params
     */
    private function validateFirstName($params)
    {
        if (!empty($params['first_name']) && strlen($params['first_name']) > self::FIRST_NAME_MAX_LENGTH) {
            $this->errorMessage['first_name'] = $this->__("First name must not exceed 8 characters");
            $this->error = true;
        }
    }

    /**
     * Validate last name
     * - max length 30
     * s
     * @param array $params
     */
    private function validateLastName($params)
    {
        if (!empty($params['last_name']) && strlen($params['last_name']) > self::LAST_NAME_MAX_LENGTH) {
            $this->errorMessage['last_name'] = $this->__("Last name must not exceed 30 characters");
            $this->error = true;
        }
    }

    private function validateBirthDate($params)
    {
        $this->error = false;
    }

    private function validateStreet($params)
    {
        $this->error = false;
    }

    private function validateHouseNo($params)
    {
        $this->error = false;
    }

    private function validateZipCode($params)
    {
        $this->error = false;
    }

    private function validateCity($params)
    {
        $this->error = false;
    }

    private function validateAddress($params)
    {
        $this->error = false;
    }

    private function validateDeviceId($params)
    {
        $this->error = false;
    }

    private function validateEmail($params)
    {
        if (!empty($params['email']) && !preg_match(self::EMAIL_REGEX, $params['email'])) {
            $this->errorMessage['email'] = $this->__("Email address must be valid");
            $this->error = true;
        }
        $this->error = false;

        // todo validate domain
    }

    /**
     *  Mandatory
     *  - Kunden-/Ordenrummer
     *  OR
     *  - SubscriberNumber
     *  OR
     *  - SIM/IMEI
     *  OR
     *  - LastName AND (FirstName OR BirthDate OR ZipCode OR City OR Street)
     */
    private function validateLegacyMobileSearchParams()
    {
        // ban is found => validation has passed
        if (isset($this->searchParams['customer']) && trim($this->searchParams['customer'])) {
            return;
        }

        // SubscriberNumber is found => validation has passed
        if (isset($this->searchParams['subscriber_number']) && trim($this->searchParams['subscriber_number'])) {
            return;
        }

        // SIM/IMEI is found => validation has passed
        if (isset($this->searchParams['sim_imei']) && trim($this->searchParams['sim_imei'])) {
            return;
        }

        // no ban, subscriberNo, sim/imei, nor lastName is found => validation has failed
        if (!isset($this->searchParams['last_name']) || !trim($this->searchParams['last_name'])) {
            $this->errorMessage['customer'] = $this->__("At least BAN or SubscriberNumber or EquipmentNumber or LastName and at least one field of First Name, Birth Date, ZIP, City or Street should be populated");
            $this->error = true;
        }

        // lastName is found => we must check if one of FirstName OR BirthDate OR ZipCode OR City OR Street is present
        // firstName is found => validation has passed
        if (isset($this->searchParams['first_name']) && trim($this->searchParams['first_name'])) {
            return;
        }

        // birthday is found => validation has passed
        if (isset($this->searchParams['birthday']) && trim($this->searchParams['birthday'])) {
            return;
        }


        // city is found => validation has passed
        if (isset($this->searchParams['city']) && trim($this->searchParams['city'])) {
            return;
        }

        // street is found => validation has passed
        if (isset($this->searchParams['street']) && trim($this->searchParams['street'])) {
            return;
        }

        // FirstName NOR BirthDate NOR ZipCode NOR City NOR Street is found
        $this->errorMessage['customer'] = $this->__("LastName and at least one field of First Name, Birth Date, ZIP, City or Street should be populated");
        $this->error = true;
    }

    /**
     * Mandatory:
     * - Ban
     * OR
     * the following:
     * LastName AND ZipCode AND BirthDate
     */
    private function validateLegacyDslSearchParams()
    {
        // ban is present => validation has passed
        if (
            (isset($this->searchParams['telephone_number']) && trim($this->searchParams['telephone_number'])) ||
            (isset($this->searchParams['customer']) && trim($this->searchParams['customer']))
        ) {
            return;
        }

        // When an input is present in the personal data fields: LastName AND ZipCode AND BirthDate are mandatory
        if (
            (isset($this->searchParams['first_name']) && trim($this->searchParams['first_name'])) ||
            (isset($this->searchParams['last_name']) && trim($this->searchParams['last_name'])) ||
            (isset($this->searchParams['birthday']) && trim($this->searchParams['birthday'])) ||
            (isset($this->searchParams['zipcode']) && trim($this->searchParams['zipcode'])) ||
            (isset($this->searchParams['city']) && trim($this->searchParams['city'])) ||
            (isset($this->searchParams['street']) && trim($this->searchParams['street'])) ||
            (isset($this->searchParams['no']) && trim($this->searchParams['no'])) ||
            (isset($this->searchParams['addNo']) && trim($this->searchParams['addNo']))
        ) {
            // LastName AND ZipCode AND BirthDate are not present => error
            if (
                (!isset($this->searchParams['last_name']) || !trim($this->searchParams['last_name'])) ||
                (!isset($this->searchParams['birthday']) || !trim($this->searchParams['birthday']))

            ) {
                $this->error = true;
                $this->errorMessage['last_name'] = $this->__("When searching by Personal data, Last Name, Zip Code and Birth Date are mandatory");
                return;
            }
            return;
        }

        // no field is given => error
        $this->errorMessage['customer'] = $this->__("Please enter either surname with date of birth and with zipcode OR customer number");
        $this->error = true;
    }

    /**
     * Mandatory:
     * - Kundennummer
     * OR
     * - Geräte ID
     *  OR
     * - PLZ AND Ort AND Straße AND Hausnr
     */
    private function validateLegacyCableSearchParams()
    {
        // ban is found => validation has passed
        if (isset($this->searchParams['customer']) && trim($this->searchParams['customer'])) {
            return;
        }

        // device_id is found => validation has passed
        if (isset($this->searchParams['device_id']) && trim($this->searchParams['device_id'])) {
            return;
        }

        // When an input is present in one of the fields PLZ AND Ort AND Straße AND Hausnr => all fields PLZ AND Ort AND Straße AND Hausnr are mandatory
        if (
            (isset($this->searchParams['zipcode']) && trim($this->searchParams['zipcode'])) ||
            (isset($this->searchParams['city']) && trim($this->searchParams['city'])) ||
            (isset($this->searchParams['no']) && trim($this->searchParams['no'])) ||
            (isset($this->searchParams['street']) && trim($this->searchParams['street']))
        ) {
            // LastName AND ZipCode AND BirthDate are not present => error
            if (
                (!isset($this->searchParams['zipcode']) || !trim($this->searchParams['zipcode'])) ||
                (!isset($this->searchParams['city']) || !trim($this->searchParams['city'])) ||
                (!isset($this->searchParams['street']) || !trim($this->searchParams['street'])) ||
                (!isset($this->searchParams['no']) || !trim($this->searchParams['no']))

            ) {
                $this->error = true;
                $this->errorMessage['zipcode'] = $this->__("When searching by address data: PostalCode, City, Street and HouseNumber are mandatory");
                return;
            }
            return;
        }

        // FirstName NOR BirthDate NOR ZipCode NOR City NOR Street is found
        $this->errorMessage['customer'] = $this->__("At least BAN or DeviceId or the fields PostalCode and City and Street and HouseNumber should be populated.");
        $this->error = true;
    }

    private function setErrorResponse()
    {
        $this->response = ['validationError' => 0,
            'error' => 1,
            'message' => $this->errorMessage];
    }

    protected function setValidationErrorResponse()
    {
        $this->response = ['validationError' => 1,
            'error' => 1,
            'message' => $this->errorMessage];
    }

    protected function setSuccessResponse($params)
    {
        $this->response = ['validationError' => 0, 'error' => 0, 'params' => $params];
    }

    /**
     * Check if we have restrictions for myProducts buttons
     * @param $data
     * @return mixed
     */
    public function checkRestrictionsForButtons($data)
    {
        // KD pending orders
        $pendingOrdersStatus = $this->extractPendingOrders();

        /** @var Dyna_Customer_Model_TypeSubtypeCombinations $model */
        $model = Mage::getModel("dyna_customer/typeSubtypeCombinations");

        // agent permissions
        $customerSession = Mage::getSingleton('dyna_customer/session');
        $agent = $customerSession->getAgent();
        $agentHasDebitToCredit = $agent->isGranted('DEBIT_TO_CREDIT');
        $agentHasChangeToDslBase = $agent->isGranted('CHANGE_TO_DSL_BASE');
        $agentHasChangeToCabBase = $agent->isGranted('CHANGE_TO_CAB_BASE');
        $agentHasAllowCab = $agent->isGranted('ALLOW_CAB');
        $agentHasAllowDsl = $agent->isGranted('ALLOW_DSL');
        $agentHasChangeToMobBase = $agent->isGranted('CHANGE_TO_MOB_BASE');

        foreach ($data as $stack_name => &$stack_data) {
            foreach ($stack_data as &$stack_contracts) {
                foreach ($stack_contracts['contracts'] as &$contract) {
                    foreach ($contract['subscriptions'] as &$subscription) {
                        //validate mandatory data for buttons
                        $this->validateDataForButtons($subscription, $stack_name);
                        // disable Modify package if package is already in cart
                        $this->checkILSPackageExistence($subscription);
                        //validate custom data

                        // If customer is suspended then NO package change is allowed (regardless of stack)
                        if (isset($contract['customer_status']) && strtoupper($contract['customer_status']) == Dyna_Customer_Helper_Customer::CUSTOMER_STATUS_SUSPENDED) {
                            $subscription['disableButtons']['modify_package']['class'] = true;
                            $subscription['disableButtons']['change_service']['class'] = true;

                            $subscription['disableButtons']['perform_serviceability'] = [
                                'enabled' => 0
                            ];
                        }

                        switch ($stack_name) {
                            case Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS:
                            case Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_MOBILE:
                                /**
                                 * acc_type & acc_subtype check
                                 */
                                if (!empty($subscription['validationFields']['party_type']) &&
                                    !empty($subscription['validationFields']['account_class'])
                                ) {
                                    $disableButtons = $model->checkIfDisable(
                                        $subscription['validationFields']['party_type'],
                                        $subscription['validationFields']['account_class']
                                    );

                                    $subscription['disableButtons']['modify_package']['class'] = $subscription['disableButtons']['modify_package']['class'] ?? $disableButtons;
                                    $subscription['disableButtons']['renew_package']['class'] = $subscription['disableButtons']['renew_package']['class'] ?? $disableButtons;
                                    $subscription['disableButtons']['create_red']['class'] = $subscription['disableButtons']['create_red']['class'] ?? $disableButtons;
                                    $subscription['disableButtons']['add_red']['class'] = $subscription['disableButtons']['add_red']['class'] ?? $disableButtons;

                                    if ($disableButtons) {
                                        $subscription['disableButtons']['modify_package']['message'] = $this->__("Changing this customer is not possible because of the type (%s) and subtype (%s).",
                                            $model->mapType($subscription['validationFields']['party_type']),
                                            $subscription['validationFields']['account_class']);
                                        $subscription['disableButtonsMessages'] = true;
                                    }

                                }

                                if ($subscription['contract_type'] == Dyna_Customer_Helper_Customer::MOBILE_CONTRACT_TYPE_POSTPAID) {
                                    if (isset($disableButtons)) {
                                        $subscription['disableButtons']['modify_package']['class'] = $subscription['disableButtons']['modify_package']['class'] ?? $disableButtons;
                                        $subscription['disableButtons']['renew_package']['class'] = $subscription['disableButtons']['renew_package']['class'] ?? $disableButtons;
                                        $subscription['disableButtons']['create_red']['class'] = $subscription['disableButtons']['create_red']['class'] ?? $disableButtons;
                                        $subscription['disableButtons']['add_red']['class'] = $subscription['disableButtons']['add_red']['class'] ?? $disableButtons;
                                    }

                                    if ((!isset($subscription['disableButtons']['modify_package']['class']) ||
                                            $subscription['disableButtons']['modify_package']['class'] == false) &&
                                        isset($subscription['customer_number']) &&
                                        isset($pendingOrdersStatus['activity'][strtolower(Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_MOBILE)][$subscription['customer_number']]) &&
                                        isset($subscription['ctn_phoneNumber']) &&
                                        $subscription['ctn_phoneNumber'] != '' &&
                                        isset($pendingOrdersStatus['activity'][strtolower(Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_MOBILE)][$subscription['customer_number']][$subscription['ctn_phoneNumber']])
                                    ) {
                                        $subscription['disableButtons']['modify_package']['class'] =
                                            $model->checkFutureTransactions($pendingOrdersStatus['activity'][strtolower(Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_MOBILE)][$subscription['customer_number']][$subscription['ctn_phoneNumber']]);
                                    }

                                    // if in dunning, don't allow prolongation
                                    if (strtoupper($contract['dunning_status']) == Dyna_Customer_Helper_Services::DUNNING_IN) {
                                        $subscription['disableButtons']['modify_package']['class'] = true;
                                        $subscription['disableButtons']['renew_package']['class'] = true;
                                        $subscription['disableButtons']['create_red']['class'] = true;
                                        $subscription['disableButtons']['add_red']['class'] = true;
                                    }

                                    //Scenario 5 (confluence CUST_001) Subscriber Status is not Active
                                    if ($subscription['product_status'] !== Dyna_Customer_Helper_Customer::MOBILE_CONTRACT_PRODUCT_STATUS_A) {
                                        $subscription['disableButtons']['renew_package']['class'] = true;
                                        $subscription['disableButtons']['modify_package']['class'] = true;
                                        $subscription['disableButtons']['convert_to_member']['class'] = true;
                                    }

                                    // if permissions on agent are missing, do not allow tariff change
                                    if ((!isset($subscription['disableButtons']['modify_package']['class']) ||
                                            $subscription['disableButtons']['modify_package']['class'] == false) &&
                                        (!$agentHasChangeToMobBase)
                                    ) {
                                        $subscription['disableButtons']['modify_package']['class'] = true;
                                    }

                                    if ((!isset($subscription['disableButtons']['renew_package']['class']) ||
                                            $subscription['disableButtons']['renew_package']['class'] == false) && !$agentHasChangeToMobBase
                                    ) {
                                        $subscription['disableButtons']['renew_package']['class'] = true;
                                    }
                                } elseif ($subscription['contract_type'] == Dyna_Customer_Helper_Customer::MOBILE_CONTRACT_TYPE_PREPAID) {
                                    if (isset($disableButtons)) {
                                        $subscription['disableButtons']['pre_post']['class'] = $subscription['disableButtons']['pre_post']['class'] ?? $disableButtons;
                                        $subscription['disableButtons']['convert_to_member']['class'] = $subscription['disableButtons']['convert_to_member']['class'] ?? $disableButtons;
                                    }

                                    if ((!isset($subscription['disableButtons']['pre_post']['class']) ||
                                            $subscription['disableButtons']['pre_post']['class'] == false) &&
                                        isset($subscription['customer_number']) &&
                                        isset($pendingOrdersStatus['activity'][strtolower(Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_MOBILE)][$subscription['customer_number']]) &&
                                        isset($subscription['ctn_phoneNumber']) &&
                                        $subscription['ctn_phoneNumber'] != '' &&
                                        isset($pendingOrdersStatus['activity'][strtolower(Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_MOBILE)][$subscription['customer_number']][$subscription['ctn_phoneNumber']])
                                    ) {
                                        $subscription['disableButtons']['pre_post']['class'] =
                                            $model->checkFutureTransactions($pendingOrdersStatus['activity'][strtolower(Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_MOBILE)][$subscription['customer_number']][$subscription['ctn_phoneNumber']]);
                                    }
                                    // if permissions on agent are missing, do not allow tariff change
                                    if ((!isset($subscription['disableButtons']['pre_post']['class']) ||
                                            $subscription['disableButtons']['pre_post']['class'] === false) &&
                                        !$agentHasDebitToCredit) {
                                        $subscription['disableButtons']['pre_post']['class'] = true;
                                    }

                                    if (empty($subscription['tariff']['sku'])) {
                                        $subscription['disableButtons']['pre_post']['class'] = true;
                                    } else {
                                        // VFDED1W3S-2118: Disable pre2post if dtc_restrict_ind is true

                                        /** @var Dyna_Catalog_Model_Product $tariff */
                                        $tariff = Mage::getModel('dyna_catalog/product')->loadByAttribute('sku', $subscription['tariff']['sku']);
                                        if (strtolower($tariff->getAttributeText('dtc_restrict_ind')) == 'true') {
                                            $subscription['disableButtons']['pre_post']['class'] = true;
                                        }
                                    }

                                    // VFDEW3E2E-789: If in dunning, don't allow pre2post and convert_to_member
                                    if (strtoupper($contract['dunning_status']) == Dyna_Customer_Helper_Services::DUNNING_IN) {
                                        $subscription['disableButtons']['pre_post']['class'] = true;
                                        $subscription['disableButtons']['convert_to_member']['class'] = true;
                                    }

                                    // VFDEW3E2E-789: If ServiceLineStatus = Suspended/Reserved no actions allowed
                                    if ($subscription['product_status'] !== Dyna_Customer_Helper_Customer::MOBILE_CONTRACT_PRODUCT_STATUS_A) {
                                        $subscription['disableButtons']['pre_post']['class'] = true;
                                        $subscription['disableButtons']['convert_to_member']['class'] = true;
                                    }
                                }
                                break;
                            case Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_DSL:
                            case Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN:
                                /** @var Dyna_Address_Model_Storage $sessionAddress */
                                $sessionAddress = Mage::getSingleton('dyna_address/storage');
                                $serviceAddress = $subscription['service_address'] ?? null;
                                $serviceAddress['service_line_id'] = !empty($subscription['service_line_id']) ? $subscription['service_line_id'] : null;
                                $serviceAddress['component_ctn'] = !empty($subscription['ctn']) ? $subscription['ctn'] : null;
                                $needChangeServiceAddress = false;
                                if (isset($userAddress) && isset($userAddress['address_id']) && !empty($userAddress['address_id'])) {
                                    $userAddressId = is_array($userAddress['address_id']) ? reset($userAddress['address_id']) : $userAddress['address_id'];
                                    if ($userAddressId != $sessionAddress->getAddressId()) {
                                        $needChangeServiceAddress = true;
                                    }
                                } else {
                                    $sessionAddressData = $sessionAddress->getServiceAddressArray();
                                    foreach ($sessionAddressData as $key => $value) {
                                        if (isset($serviceAddress[$key]) &&
                                            $serviceAddress[$key] != null &&
                                            $value != null &&
                                            strtolower(trim($serviceAddress[$key])) !== strtolower(trim($value)) &&
                                            !$needChangeServiceAddress
                                        ) {
                                            $needChangeServiceAddress = true;
                                        }
                                    }

                                    // remove empty values to see if the entire array is empty
                                    foreach ($sessionAddressData as $key => $value) {
                                        if (empty($sessionAddressData[$key])) {
                                            unset($sessionAddressData[$key]);
                                        }
                                    }
                                }

                                $subscription['disableButtons']['perform_serviceability'] = [
                                    'enabled' => empty($sessionAddressData) || $needChangeServiceAddress ? 1 : 0,
                                    'serviceAddress' => $serviceAddress,
                                    'serviceAddressID' => md5(serialize($serviceAddress))
                                ];

                                // old products
                                $hasOldProducts = false;
                                foreach ($subscription['components'] as $component) {
                                    if (isset($component['has_old_products']) && $component['has_old_products'] == true) {
                                        $hasOldProducts = true;
                                        break;
                                    }
                                }
                                if ($hasOldProducts) {
                                    $subscription['disableButtons']['modify_package']['class'] = true;
                                }

                                // if permissions on agent are missing, do not allow tariff change
                                if ((!isset($subscription['disableButtons']['modify_package']['class']) ||
                                        $subscription['disableButtons']['modify_package']['class'] == false) && !$agentHasChangeToDslBase) {
                                    $subscription['disableButtons']['modify_package']['class'] = true;
                                }


                                if ((!isset($subscription['disableButtons']['change_service']['class']) ||
                                        $subscription['disableButtons']['change_service']['class'] == false) &&
                                    (!$agentHasAllowCab)) {
                                    $subscription['disableButtons']['change_service']['class'] = true;
                                }

                                // If customer is IN_DUNNING, then NO package change is allowed
                                if (isset($contract['dunning_status']) && strtoupper($contract['dunning_status']) == Dyna_Customer_Helper_Services::DUNNING_IN) {
                                    $subscription['disableButtons']['modify_package']['class'] = true;
                                    $subscription['disableButtons']['change_service']['class'] = true;

                                    $subscription['disableButtons']['perform_serviceability'] = [
                                        'enabled' => 0
                                    ];
                                }
                                break;
                            case Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD:
                                /** @var Dyna_Address_Model_Storage $sessionAddress */
                                $sessionAddress = Mage::getSingleton('dyna_address/storage');
                                $sessionAddressData = $sessionAddress->getServiceAddressArray();

                                /** @var Dyna_Address_Model_Storage $sessionAddress */
                                $sessionAddress = Mage::getSingleton('dyna_address/storage');
                                $serviceAbility = $sessionAddress->getServiceAbility();

                                foreach ($subscription['products'] as &$KDsubscriptions) {
                                    foreach ($KDsubscriptions as &$KDsubscription) {
                                        $serviceAddress = $KDsubscription['service_address'] ?? null;
                                        if (!empty($sessionAddressData)) {
                                            $needChangeServiceAddress = false;
                                            if (isset($userAddress) && isset($userAddress['address_id']) && !empty($userAddress['address_id'])) {
                                                $userAddressId = is_array($userAddress['address_id']) ? reset($userAddress['address_id']) : $userAddress['address_id'];
                                                if ($userAddressId != $sessionAddress->getAddressId()) {
                                                    $needChangeServiceAddress = true;
                                                }
                                            } else {
                                                foreach ($sessionAddressData as $key => $value) {
                                                    if (isset($serviceAddress[$key]) &&
                                                        $serviceAddress[$key] != null &&
                                                        $value != null &&
                                                        strtolower(trim($serviceAddress[$key])) !== strtolower(trim($value)) &&
                                                        !$needChangeServiceAddress
                                                    ) {
                                                        $needChangeServiceAddress = true;
                                                    }
                                                }
                                            }

                                            // cable_internet_phone
                                            if (($needChangeServiceAddress == false) && ($KDsubscription['package_type'] == strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_INTERNET_PHONE)) && ($serviceAbility['cableAvailability']['KIP'] == false)) {
                                                $KDsubscription['disableButtons']['modify_package']['class'] = true;
                                            }

                                            // cable_tv
                                            if (($needChangeServiceAddress == false) && ($KDsubscription['package_type'] == strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_TV)) && ($serviceAbility['cableAvailability']['KAA'] == false) && ($serviceAbility['cableAvailability']['KAD'] == false)) {
                                                $KDsubscription['disableButtons']['modify_package']['class'] = true;
                                            }
                                        }

                                        $KDsubscription['disableButtons']['perform_serviceability'] = [
                                            'enabled' => empty($sessionAddressData) || $needChangeServiceAddress ? 1 : 0,
                                            'serviceAddress' => $serviceAddress,
                                            'serviceAddressID' => md5(serialize($serviceAddress))
                                        ];

                                        // if permissions on agent are missing, do not allow tariff change
                                        if ((!isset($KDsubscription['disableButtons']['modify_package']['class']) ||
                                                $KDsubscription['disableButtons']['modify_package']['class'] == false) &&
                                            (!$agentHasChangeToCabBase)
                                        ) {
                                            $KDsubscription['disableButtons']['modify_package']['class'] = true;
                                        }

                                        if ((!isset($KDsubscription['disableButtons']['change_service']['class']) ||
                                                $KDsubscription['disableButtons']['change_service']['class'] == false) &&
                                            (!$agentHasAllowDsl)
                                        ) {
                                            $KDsubscription['disableButtons']['change_service']['class'] = true;
                                        }

                                        // If customer is IN_DUNNING, then NO package change is allowed
                                        if (isset($contract['dunning_status']) && strtoupper($contract['dunning_status']) == Dyna_Customer_Helper_Services::DUNNING_IN) {
                                            $KDsubscription['disableButtons']['modify_package']['class'] = true;
                                            $KDsubscription['disableButtons']['change_service']['class'] = true;

                                            $KDsubscription['disableButtons']['perform_serviceability'] = [
                                                'enabled' => 0
                                            ];
                                        }

                                        if (isset($contract['no_ordering_allowed']) && $contract['no_ordering_allowed'] == "true") {
                                            $KDsubscription['disableButtons']['modify_package']['class'] = true;
                                            $KDsubscription['disableButtons']['change_service']['class'] = true;
                                            $KDsubscription['disableButtons']['perform_serviceability'] = [
                                                'enabled' => 0
                                            ];

                                            $KDsubscription['disableButtons']['renew_package']['message'] = $this->__("Customer is not allowed to order");
                                            $KDsubscription['disableButtonsMessages'] = true;
                                        }
                                    }
                                }

                                break;
                            default:
                                //nothing
                        }
                    }
                }
            }
        }

        return $data;
    }

    /**
     * We'll enable all the restrictions for household products
     * since household products is for view purpose only
     * @param $data
     * @return mixed
     */
    public function checkHouseholdRestrictionsForButtons($data)
    {
        foreach ($data as $stack_name => &$stack_data) {
            foreach ($stack_data as &$stack_contracts) {
                foreach ($stack_contracts['contracts'] as &$contract) {
                    foreach ($contract['subscriptions'] as &$subscription) {
                        //validate mandatory data for buttons
                        $this->validateHouseholdDataForButtons($subscription, $stack_name);

                        //validate custom data
                        switch ($stack_name) {
                            case Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS:
                            case Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_MOBILE:
                                if ($subscription['contract_type'] == Dyna_Customer_Helper_Customer::MOBILE_CONTRACT_TYPE_POSTPAID) {
                                    $subscription['disableButtons']['modify_package']['class'] = true;
                                    $subscription['disableButtons']['renew_package']['class'] = true;
                                    $subscription['disableButtons']['create_red']['class'] = true;
                                    $subscription['disableButtons']['add_red']['class'] = true;

                                } elseif ($subscription['contract_type'] == Dyna_Customer_Helper_Customer::MOBILE_CONTRACT_TYPE_PREPAID) {
                                    $subscription['disableButtons']['pre_post']['class'] = true;
                                    $subscription['disableButtons']['create_red']['class'] = true;
                                    $subscription['disableButtons']['add_red']['class'] = true;
                                }
                                break;
                            case Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_DSL:
                            case Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN:

                                if (!isset($subscription['disableButtons'])) {
                                    $subscription['disableButtons']['modify_package']['class'] = true;
                                }

                                $serviceAddress = $subscription['service_address'] ?? null;
                                $subscription['disableButtons']['perform_serviceability'] = [
                                    'enabled' => 0,
                                    'serviceAddress' => $serviceAddress,
                                    'serviceAddressID' => md5(serialize($serviceAddress))
                                ];

                                $subscription['disableButtons']['modify_package']['class'] = true;
                                $subscription['disableButtons']['change_service']['class'] = true;
                                $subscription['disableButtons']['relocation']['class'] = true;
                                $subscription['disableButtons']['change_service']['class'] = true;
                                break;
                            case Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD:

                                foreach ($subscription['products'] as &$KDsubscriptions) {
                                    foreach ($KDsubscriptions as &$KDsubscription) {

                                        $KDsubscription['disableButtons']['modify_package']['class'] = true;
                                        $KDsubscription['disableButtons']['renew_package']['class'] = true;
                                        $serviceAddress = $KDsubscription['service_address'] ?? null;
                                        $KDsubscription['disableButtons']['perform_serviceability'] = [
                                            'enabled' => 0,
                                            'serviceAddress' => $serviceAddress,
                                            'serviceAddressID' => md5(serialize($serviceAddress))
                                        ];

                                        $KDsubscription['disableButtons']['change_service']['class'] = true;
                                        $KDsubscription['disableButtons']['relocation']['class'] = true;
                                    }
                                }

                                break;
                            default:
                                //nothing
                        }
                    }
                }
            }
        }

        return $data;
    }

    /**
     * @param $subscription
     * @return mixed
     */
    private function validateDataForButtons(&$subscription, $stack_name)
    {
        foreach (self::$myProductsActionButtons as $buttonName => $buttonData) {
            if (isset($buttonData['validation'])) {
                foreach ($buttonData['validation'] as $validation) {
                    if ($stack_name == Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD) {
                        foreach ($subscription['products'] as $type => &$products) {
                            foreach ($products as &$product) {
                                if ($validation == 'open_order') {
                                    if ($product['validationFields'][$validation] !== false) {
                                        $product['disableButtons'][$buttonName] = [
                                            'class' => true
                                        ];
                                        $product['disableButtons']['renew_package']['message'] = $this->__("You can not perform any actions on this package due to an open order");
                                        $product['disableButtonsMessages'] = true;
                                    }
                                } else {
                                    if (in_array($validation, array_keys($product['validationFields'])) &&
                                        ($product['validationFields'][$validation] == null || trim($product['validationFields'][$validation]) == '')) {

                                        $product['disableButtons'][$buttonName] = [
                                            'class' => true
                                        ];

                                        // VFDED1W3S-1330
                                        if ($type == Dyna_Customer_Model_Client_RetrieveCustomerInfo::CABLE_PRODUCT_CATEGORY_TV) {
                                            $product['disableButtons']['modify_package'] = [
                                                'class' => false
                                            ];
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        if ($validation == 'open_order') {
                            // Has OSF Open Order
                            if ($subscription['validationFields'][$validation] === true) {
                                $subscription['disableButtons'][$buttonName] = [
                                    'class' => true
                                ];
                                $subscription['disableButtons']['renew_package']['message'] = $this->__("You can not perform any actions on this package due to an open order");
                                $subscription['disableButtonsMessages'] = true;
                                // Has non-OSF Open Order
                            } else if ($subscription['validationFields'][$validation] !== false) {
                                if (in_array($subscription['validationFields'][$validation], Mage::getSingleton('dyna_customer/activityCodes')->getNotAllowedActivityCodes()) && $buttonName != 'modify_package') {
                                    $subscription['disableButtons'][$buttonName] = [
                                        'class' => true
                                    ];
                                    $subscription['disableButtons']['renew_package']['message'] = $this->__("Exists a future dated transaction and prolongation can not be done");
                                    $subscription['disableButtonsMessages'] = true;
                                }
                            }
                        } else {
                            if (in_array($validation, array_keys($subscription['validationFields'])) &&
                                ($subscription['validationFields'][$validation] == null || trim($subscription['validationFields'][$validation]) == '')) {

                                $subscription['disableButtons'][$buttonName] = [
                                    'class' => true
                                ];
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * @param $subscription
     * @return mixed
     */
    private function validateHouseholdDataForButtons(&$subscription, $stack_name)
    {
        foreach (self::$myProductsActionButtons as $buttonName => $buttonData) {
            if (isset($buttonData['validation'])) {
                foreach ($buttonData['validation'] as $validation) {
                    $subscription['disableButtons'][$buttonName] = ['class' => true];

                    if ($stack_name == Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD) {
                        foreach ($subscription['products'] as &$products) {
                            foreach ($products as &$product) {
                                $product['disableButtons'][$buttonName] = ['class' => true];
                                $product['disableButtons']['modify_package'] = ['class' => false];
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * @return array
     */
    protected function extractPendingOrders(): array
    {
        $pendingOrders = Mage::getSingleton('customer/session')->getCustomerOrders();
        $ordersPending = $ordersActivity = $futureActivityCodes = [];
        $disabledForStatuses = array(
            Dyna_Checkout_Model_Order::PENDING_ORDER_STATUS_PENDING,
            Dyna_Checkout_Model_Order::PENDING_ORDER_STATUS_IN_PROGRESS
        );

        if ($pendingOrders) {
            foreach ($pendingOrders as $stack => $orders) {
                foreach ($orders as $order) {
                    switch ($stack) {
                        case strtolower(Dyna_Customer_Model_Client_RetrieveCustomerInfo::NON_OSF_ORDER_LINE_OF_BUSINESS_CABLE):
                            if (in_array($order['status'], $disabledForStatuses)) {
                                if ($order['kip_category'] == true) {
                                    $ordersPending[$stack]['kip_category'][$order['legacy_customer_id']] = $order['order_id'];
                                }

                                if ($order['tv_category'] == true) {
                                    $ordersPending[$stack]['tv_category'][$order['legacy_customer_id']] = $order['order_id'];
                                }

                                if ($order['cls_category'] == true) {
                                    $ordersPending[$stack]['cls_category'][$order['legacy_customer_id']] = $order['order_id'];
                                }
                            }
                            break;
                        case strtolower(Dyna_Customer_Model_Client_RetrieveCustomerInfo::NON_OSF_ORDER_LINE_OF_BUSINESS_DSL):
                            if (in_array($order['status'], $disabledForStatuses)) {
                                $ordersPending[$stack][$order['legacy_customer_id']] = $order['order_id'];
                            }
                            break;
                        case strtolower(Dyna_Customer_Model_Client_RetrieveCustomerInfo::NON_OSF_ORDER_LINE_OF_BUSINESS_MOBILE):
                            if ($order['subscriber_number'] != '') {
                                $ordersActivity[$stack][$order['legacy_customer_id']][$order['subscriber_number']] = $order['activity_code'];
                            }
                            // VFDED1W3S-19 && VFDED1W3S-1067: codes  CCN, CIM, MCN, SPC, TRC do not allow prolongation
                            $futureActivityCodes[$stack][$order['legacy_customer_id']][] = $order['future_activity_code'];
                            break;
                    }
                }
            }
        }

        return [
            'pending' => $ordersPending,
            'activity' => $ordersActivity,
            'futureActivityCodes' => $futureActivityCodes
        ];
    }

    /**
     * If package is already in an ILS flow, disable its Modify package button from My products
     * @param $subscription
     */
    protected function checkILSPackageExistence(&$subscription)
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $customerSession = Mage::getSingleton('customer/session');
        $cartPackages = $quote->getCartPackages();
        // firstly check if there is already in cart a package which requires serviceability
        // and so, disable Modify package for all those on the same package type
        $restrictedPackageTypes = array(
            Dyna_Catalog_Model_Type::TYPE_CABLE_INTERNET_PHONE,
            Dyna_Catalog_Model_Type::TYPE_CABLE_TV,
            Dyna_Catalog_Model_Type::TYPE_CABLE_INDEPENDENT,
            Dyna_Catalog_Model_Type::TYPE_FIXED_DSL,
            Dyna_Catalog_Model_Type::TYPE_LTE
        );

        /** @var Dyna_Package_Model_Package[] $cartPackages */
        if ($cartPackages) {
            foreach ($cartPackages as $package) {

                if (($package->getType() == $subscription['package_type']) && in_array(strtoupper($package->getType()), $restrictedPackageTypes)) {
                    if (in_array($package->getSaleType(), Dyna_Catalog_Model_ProcessContext::ILSProcessContexts())) {
                        $subscription['disableButtons']['modify_package']['class'] = true;
                        $subscription['disableButtons']['change_service']['class'] = true;
                        continue;
                    } elseif ($package->getSaleType() == Dyna_Catalog_Model_ProcessContext::ACQ) {
                        $subscription['disableButtons']['modify_package']['class'] = true;
                        continue;
                    }
                }

                if ($package->getParentAccountNumber() != $subscription['customer_number'] && in_array($package->getSaleType(), Dyna_Catalog_Model_ProcessContext::getProlongationProcessContexts())) {
                    $subscription['disableButtons']['renew_package']['class'] = true;
                }

                $preToPostPackage = false;
                if (($package->getSaleType() == Dyna_Catalog_Model_ProcessContext::MIGCOC)
                    && ($subscription['package_type'] == strtolower(Dyna_Catalog_Model_Type::TYPE_PREPAID))
                    && ($package->getType() == strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE))) {
                    $preToPostPackage = true;

                    // VFDED1W3S-3059: Can NOT Convert to member or Pre to Post, if subscription package has been already converted or migrated to postpaid.
                    if ($package->getCtn() == $subscription['ctn']) {
                        if (count($quote->getCartPackagesWithParent($package->getParentId())) > 0) {
                            $subscription['disableButtons']['pre_post']['class'] = true;
                            $subscription['disableButtons']['convert_to_member']['class'] = true;
                            continue;
                        }
                    }
                }

                if (in_array($subscription['package_type'], Dyna_Catalog_Model_Type::getMobilePackages())) {
                    if ($customerSession->getKiasLinkedAccountNumber()) {
                        if ($customerSession->getKiasLinkedAccountNumber() != $subscription['customer_number']) {
                            if (!($subscription['contract_type'] == Dyna_Customer_Helper_Customer::MOBILE_CONTRACT_TYPE_PREPAID)) {
                                $subscription['disableButtons']['modify_package']['class'] = true;
                                $subscription['disableButtons']['renew_package']['class'] = true;
                                $subscription['disableButtons']['change_service']['class'] = true;
                                $subscription['disableButtons']['convert_to_member']['class'] = true;
                                $subscription['disableButtons']['create_red']['class'] = true;
                                $subscription['disableButtons']['add_red']['class'] = true;
                            }
                            continue;
                        } elseif ($customerSession->getKiasLinkedAccountNumber() == $subscription['customer_number'] &&
                            $package->getParentAccountNumber() == $subscription['customer_number'] &&
                            $package->getServiceLineId() == $subscription['service_line_id']) {

                            //we know that the installed base subscription was prepaid
                            if ($subscription['contract_type'] == Dyna_Customer_Helper_Customer::MOBILE_CONTRACT_TYPE_PREPAID) {
                                $subscription['disableButtons']['pre_post']['class'] = true;
                                $subscription['disableButtons']['convert_to_member']['class'] = true;
                                continue;
                            }

                            //new red+ group from eligible owner
                            if ($package->isPartOfRedPlus()) {
                                //owner
                                if (!$package->getParentId()) {
                                    $subscription['disableButtons']['renew_package']['class'] = true;
                                    //owner dummy
                                    if ($package->getEditingDisabled() == null) {
                                        $subscription['disableButtons']['modify_package']['class'] = true;
                                        $subscription['disableButtons']['renew_package']['class'] = true;
                                    }
                                } else {
                                    //member
                                    $subscription['disableButtons']['modify_package']['class'] = true;
                                    $subscription['disableButtons']['renew_package']['class'] = true;
                                    $subscription['disableButtons']['convert_to_member']['class'] = true;
                                    $subscription['disableButtons']['create_red']['class'] = true;
                                    $subscription['disableButtons']['add_red']['class'] = true;
                                }
                            } else {
                                $subscription['disableButtons']['modify_package']['class'] = true;
                                $subscription['disableButtons']['renew_package']['class'] = true;
                                $subscription['disableButtons']['convert_to_member']['class'] = true;
                            }
                            continue;
                        }
                    }
                } else {
                    if ((($package->getType() == $subscription['package_type']) || $preToPostPackage)
                        && ($package->getParentAccountNumber() == $subscription['customer_number'])
                        && ($package->getServiceLineId() == $subscription['service_line_id'])
                    ) {
                        $subscription['disableButtons']['modify_package']['class'] = true;
                        $subscription['disableButtons']['change_service']['class'] = true;
                        continue;
                    }
                }

                // check the above on KD (take package_type and service_line_id from products level)
                $kdStacks = array(Dyna_Customer_Model_Client_RetrieveCustomerInfo::CABLE_PRODUCT_CATEGORY_KIP,
                    Dyna_Customer_Model_Client_RetrieveCustomerInfo::CABLE_PRODUCT_CATEGORY_TV,
                    Dyna_Customer_Model_Client_RetrieveCustomerInfo::CABLE_PRODUCT_CATEGORY_CLS);
                if (!empty($subscription['products'])) {
                    foreach ($subscription['products'] as $kdStack => $kdProducts) {
                        if (in_array($kdStack, $kdStacks)) {
                            foreach ($kdProducts as $key => $product) {
                                // check again, now on KD's product level, if this package's type requires serviceability and is matches the one's from cart
                                if (isset($product['package_type']) &&
                                    ($package->getType() == $product['package_type'])
                                    && in_array(strtoupper($package->getType()), $restrictedPackageTypes)
                                ) {
                                    $subscription['products'][$kdStack][$key]['disableButtons']['modify_package']['class'] = true;
                                    $subscription['products'][$kdStack][$key]['disableButtons']['change_service']['class'] = true;
                                    continue;
                                }
                                // now, for ILS flow
                                if (isset($product['package_type'])
                                    && ($package->getType() == $product['package_type'])
                                    && ($package->getParentAccountNumber() == $product['customer_number'])
                                    && ($package->getServiceLineId() == $product['service_line_id'])
                                ) {
                                    $subscription['products'][$kdStack][$key]['disableButtons']['modify_package']['class'] = true;
                                    $subscription['products'][$kdStack][$key]['disableButtons']['change_service']['class'] = true;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Verify if agent has right permissions, and remove part of records if not
     * @param $data
     * @return mixed
     */
    public function checkAgentPermissions($data)
    {
        $customerSession = Mage::getSingleton('dyna_customer/session');
        $agent = $customerSession->getAgent();
        $agentHasAllowMob = $agent->isGranted('ALLOW_MOB');
        $agentHasAllowCab = $agent->isGranted('ALLOW_CAB');
        $agentHasAllowDsl = $agent->isGranted('ALLOW_DSL');
        $agentHasUnknownProducts = $agent->isGranted('UNKNOWN_PRODUCTS');

        // general permissions
        if (!$agentHasAllowCab) {
            unset($data[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD]);
        }

        if (!$agentHasAllowDsl) {
            unset($data[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN]);
        }

        if (!$agentHasAllowMob) {
            unset($data[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS]);
        }

        // parse and remove unknown products if permission is not granted
        if (!$agentHasUnknownProducts) {
            foreach ($data as $stack_name => $stack_data) {
                foreach ($stack_data as $customer_number => $stack_contracts) {
                    foreach ($stack_contracts['contracts'] as $key => $contract) {
                        foreach ($contract['subscriptions'] as $keySubscription => $subscription) {
                            // mobile and dsl
                            if (in_array($stack_name, [
                                Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS,
                                Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN,])) {
                                if (empty($subscription['validationFields']['tariff'])) {
                                    unset($data[$stack_name][$customer_number]['contracts'][$key]);
                                }
                            } else {
                                // cable
                                foreach ($subscription['products'] as $keysKD => $KDsubscriptions) {
                                    foreach ($KDsubscriptions as $keyKD => $KDsubscription) {
                                        if (empty($KDsubscription['validationFields']['tariff'])) {
                                            unset($data[$stack_name][$customer_number]['contracts'][$key]['subscriptions'][$keySubscription]['products'][$keysKD][$keyKD]);
                                        }
                                    }

                                    if (count($data[$stack_name][$customer_number]['contracts'][$key]['subscriptions'][$keySubscription]['products'][$keysKD]) == 0) {
                                        unset($data[$stack_name][$customer_number]['contracts'][$key]['subscriptions'][$keySubscription]['products'][$keysKD]);
                                    }
                                }

                                // delete if empty
                                if (count($data[$stack_name][$customer_number]['contracts'][$key]['subscriptions'][$keySubscription]['products']) == 0) {
                                    unset($data[$stack_name][$customer_number]['contracts'][$key]);
                                }
                            }

                            // delete record if empty
                            if (count($data[$stack_name][$customer_number]['contracts']) == 0) {
                                unset($data[$stack_name][$customer_number]);
                            }

                        }
                    }
                }
            }
        }
        return $data;
    }
}
