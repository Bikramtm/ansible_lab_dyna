<?php

/**
 * Class Dyna_Customer_Helper_Panel
 */
class Dyna_Customer_Helper_Panel extends Mage_Core_Helper_Abstract
{
    const FN_VOICE_COMPONENT_TYPE = 'voice';
    const FN_INTERNET_COMPONENT_TYPE = 'internet';
    const FN_SECURITY_COMPONENT_TYPE = 'safetyPackage';
    const FN_TV_COMPONENT_TYPE = 'tvCenter';
    const FN_HARDWARE_COMPONENT_TYPE = 'hardware';

    /** @var Dyna_Cache_Model_Cache */
    protected $_cache;

    protected $revMMDebug = false;
    /**
     * @return array
     */
    public static function getFnComponentTypes()
    {
        return [
            self::FN_VOICE_COMPONENT_TYPE,
            self::FN_INTERNET_COMPONENT_TYPE,
            self::FN_SECURITY_COMPONENT_TYPE,
            self::FN_TV_COMPONENT_TYPE,
            self::FN_HARDWARE_COMPONENT_TYPE,
        ];
    }

    /**
     * @return array
     */
    public static function getFnAccessTypes()
    {
        return [
            strtolower(Dyna_Catalog_Model_Type::TYPE_LTE),
            strtolower(Dyna_Catalog_Model_Type::TYPE_FIXED_DSL),
        ];
    }

    /**
     * @var $catalogHelper Dyna_Catalog_Helper_Data
     */
    protected $catalogHelper = null;

    const COMPLETED_SUCCESS = 'Completed-Success';
    const COMPLETED_FAILED = 'Completed-Failed';
    const PENDING = 'Pending';
    const PARTIAL_COMPLETED = 'Partial Completed';

    /**
     * Parses response for requested customer section
     * @param $section
     * @param $household
     * @return array
     */
    public function getPanelData($section, $household = false)
    {
        $panelData = null;

        switch ($section) {
            case "link-details" :
                $panelData = $this->parseLinkDetails();
                break;
            case "orders-content" :
                $panelData = $this->getCustomerOrders();
                break;
            case "carts-content" :
                $panelData = $this->parseCartsContent();
                break;
            case "products-content" :
                $panelData = $this->getCustomerProducts($household);
                break;
            case "household" :
                $panelData = $this->getHouseholdMembers();
                break;
            default :
                $panelData = [
                    "error" => true,
                    "message" => $this->__("No valid section requested for customer panel"),
                ];
                break;
        }

        return $panelData;
    }

    /**
    * Checks if the legacy system belongs to mobile, cable or fixed line stack
    * @param $legacySystem
    * @param $stack
    * @return bool
    */
    protected function hasStack($legacySystem, $stack)
    {
        return in_array($legacySystem, $stack);
    }

    /**
     * Parse left sidebar link details for a selected customer
     * No customer exists on session check as it is expected to be checked in controller
     * We assume the already linked accounts are part of the getCustomerDetails mode1
     */
    public function parseLinkDetails()
    {
        /** @var Dyna_Customer_Model_Customer $customer */
        $customer = Mage::getSingleton('dyna_customer/session')->getCustomer();
        // Calling helper to map services data to frontend data
        $customerData = Mage::helper('dyna_customer/info')->buildCustomerDetails($customer);
        $response = $customerData;
        //todo when services are available check if the already linked accounts are part of the getCustomerDetails mode1
        $linkedCustomers = Mage::getSingleton('dyna_customer/session')->getServiceCustomers();
        $hasDsl = $hasMobile = $hasCable = false;
        $noLinkedAccounts = 0;
        $parsedLinkedCustomers = [];
        foreach ($linkedCustomers as $legacySystem => $customers) {
            foreach ($customers as $customer) {
                if (!isset($customer['customer_number']))
                    continue;
                $customerNumber = $customer['customer_number'];
                if (isset($customer['contracts']) && isset($customer['contracts'][0]) &&
                    isset($customer['contracts'][0]['subscriptions']) && isset($customer['contracts'][0]['subscriptions'][0]) &&
                    isset($customer['contracts'][0]['subscriptions'][0]['addresses']) && $customerNumber
                ) {
                    $noLinkedAccounts++;
                    $parsedLinkedCustomers[$legacySystem][$customerNumber] = $customer;
                    foreach ($customer['contracts'][0]['subscriptions'][0]['addresses'] as $address) {
                        if (($legacySystem == Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN ||
                                $legacySystem == Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD) &&
                            $address['address_type'] == Dyna_Customer_Helper_Services::ADDRESS_TYPE_SERVICE_LONG
                        ) {
                            $parsedLinkedCustomers[$legacySystem][$customerNumber]['address'] = $address;
                        }
                        if ($legacySystem == Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS &&
                            $address['address_type'] == Dyna_Customer_Helper_Services::ADDRESS_TYPE_LEGAL_LONG
                        ) {
                            $parsedLinkedCustomers[$legacySystem][$customerNumber]['address'] = $address;
                        }
                    }

                    $hasCable = !$hasCable ? $this->hasStack($legacySystem, Dyna_Customer_Helper_Services::getCableStack()) : $hasCable;
                    $hasMobile = !$hasMobile ? $this->hasStack($legacySystem, Dyna_Customer_Helper_Services::getMobileStack()) : $hasMobile;
                    $hasDsl = !$hasDsl ? $this->hasStack($legacySystem, Dyna_Customer_Helper_Services::getFixedStack()) : $hasDsl;
                }
            }
        }

        $response['linkedAccounts'] = $parsedLinkedCustomers;
        $response['noLinkedAccounts'] = $noLinkedAccounts;
        $response['stacks'][Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD] = $hasCable;
        $response['stacks'][Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS] = $hasMobile;
        $response['stacks'][Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN] = $hasDsl;

        Mage::getSingleton('dyna_customer/session')->setLinkedCustomersForLoggedInCustomer($response);
        return $response;
    }

    /**
     * @param Dyna_Customer_Model_Customer|null $customer
     * @return array
     */
    public function getCustomerOrders(Dyna_Customer_Model_Customer $customer = null)
    {
        try {
            $nonOsfOrders = $this->getServiceCustomerOrders();
            $dbOrders = $this->getDbOrders($customer);
            $result = [
                'nonOsfOrders' => $nonOsfOrders,
                'dbOrders' => $dbOrders,
            ];

            if (isset($nonOsfOrders['error']) && $nonOsfOrders['error'] && isset($dbOrders['error']) && $dbOrders['error']) {
                $result['error'] = false;
                $result['message'] = $this->__("No orders found for this customer.");
            }
        } catch (\Exception $e) {
            $result['error'] = true;
            $result['message'] = $this->__("An error occurred and customer orders could not be loaded.");
        }

        return $result;
    }

    /**
     * @param Dyna_Customer_Model_Customer|null $customer
     * @return array
     * @throws Exception
     */
    public function getServiceCustomerOrders()
    {
        /** @var Dyna_Customer_Model_Session $customerStorage */
        $customerStorage = Mage::getSingleton('dyna_customer/session');
        $customerSession = Mage::getSingleton('customer/session');

        try {
            $customerOrdersStorage = $customerStorage->getCustomerOrders();
            if ($customerOrdersStorage !== null) {
                $data = $customerOrdersStorage;
            } else {
                if (Mage::getStoreConfig('omnius_service/customer_search_implementation_configuration/customer_search')) {
                    if ($customerData = $customerSession->getCustomerMode2()) {
                        $dataCustomerFromMode2 = $customerData;
                    } else {
                        $customer = Mage::getSingleton('dyna_customer/session')->getCustomer();
                        $adapterFactoryName = Mage::getStoreConfig('omnius_service/customer_search_implementation_configuration/customer_search_adapter');
                        $adapter = $adapterFactoryName::create();
                        $dataCustomerFromMode2 = $adapter->getInstalledBase($customer);
                        $customerSession->setCustomerMode2($dataCustomerFromMode2);
                    }
                } else {
                    /** @var Dyna_Customer_Model_Client_RetrieveCustomerInfo $customerInfoClient */
                    $customerInfoClient = Mage::getModel('dyna_customer/client_retrieveCustomerInfo');
                    $dataCustomerFromMode2 = $customerInfoClient->executeRetrieveCustomerInfo([], 2);
                }

                // if the service did not respond with success -> display a message
                $allCustomers = $customerStorage->getServiceCustomers();
                $data = [];
                foreach ($allCustomers as $customersPerStack) {
                    foreach ($customersPerStack as $customerData) {
                        if (isset($customerData['orders'])) {
                            foreach ($customerData['orders'] as $stackType => $stackOrders) {
                                $data[strtolower($stackType)] = $stackOrders;
                            }
                        }
                    }
                }
                if (!$dataCustomerFromMode2['Success']) {
                    return [
                        "error" => true,
                        "message" => $this->__("There was a problem with the loading of customer orders . Please try again!"),
                    ];
                }
                $customerStorage->setCustomerOrders($data);
            }

            $result = $data;
            if (!count($data)) {
                $result['error'] = true;
                $result['message'] = $this->__("No non-osf orders found");
            }


            return $result;

        } catch (\Exception $e) {
            $customerStorage->clearCustomerOrders();
            Mage::logException($e);
            throw $e;
        }
    }

    /**
     * Parse left sidebar orders for a selected customer
     * No customer exists on session check as it is expected to be checked in controller
     *
     * @param Dyna_Customer_Model_Customer|null $customer
     * @return mixed
     */
    public function getDbOrders(Dyna_Customer_Model_Customer $customer = null)
    {
        if (!$customer) {
            /** @var Mage_Customer_Model_Customer $customer */
            $customer = Mage::getSingleton('dyna_customer/session')->getCustomer();
        }

        $magentoOrders = Mage::getModel('superorder/superorder')
            ->getCollection()
            ->addFieldToFilter('customer_number', $customer->getCustomerNumber())
            ->load();

        $result = [];
        /** @var Dyna_Superorder_Model_Superorder $order */
        foreach ($magentoOrders as $order) {
            $orderData = [];

            // Order creation date converted to local time format
            $orderData["creationDate"] = Mage::app()
                ->getLocale()
                ->date(
                    $order->getCreatedAt(),
                    Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_MEDIUM),
                    null,
                    false
                )
                ->toString('dd-MM-yyyy');
            $orderData["orderNumber"] = $order->getOrderNumber();
            $orderData["orderStatus"] = $order->getOrderStatusTranslate();
            $orderData["salesChannel"] = Mage::getModel('core/website')
                ->load($order->getWebsiteId())
                ->getName();

            $orderPackages = $order->getPackages();
            $orderData["packages"] = count($orderPackages);
            $orderData["packageTypes"] = array();
            $orderData[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD] = $orderData[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS] = $orderData[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN] = 0;
            /** @var Dyna_Package_Model_Package $package */
            foreach ($orderPackages as $package) {
                $orderData["packageTypes"][$package->getType()] = 1;
                $package->isCable() ? $orderData[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD] = "1" : $orderData[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD];
                $package->isMobile() ? $orderData[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS] = "1" : $orderData[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS];
                $package->isFixed() ? $orderData[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN] = "1" : $orderData[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN];
            }

            $result[] = $orderData;
        }

        if (!count($result)) {
            $result['error'] = true;
            $result['message'] = $this->__("No local orders found");
        }

        return $result;
    }

    /**
     * Parse customer panel shopping carts
     *
     * @param Dyna_Customer_Model_Customer|null $customer
     * @return array
     */
    public function parseCartsContent(Dyna_Customer_Model_Customer $customer = null)
    {
        // If not customer sent as parameter, load the one from session
        if (!$customer) {
            $customer = Mage::getSingleton('customer/session')->getCustomer();
        }

        $dbShoppingCarts = $customer->getShoppingCartsForCustomer($customer->getId(), $customer->getCustomerNumber());
        $dbCartsParsed = [];

        $currentAgent = Mage::getSingleton('customer/session')->getAgent();

        foreach ($dbShoppingCarts as $cartId => &$cartAndPackages) {
            $cart = &$cartAndPackages['cart'];
            $hasMobile = $hasCable = $hasFixed = false;
            foreach ($cartAndPackages['packages'] as &$packages) {
                foreach ($packages as $keyPack => &$package) {
                    /** @var Dyna_Package_Model_Package $package */
                    if ($package->getPackageId()) {
                        $packageItemsBySections = $package->getPackagesProductsForSidebarSection($package->getData('items'), Dyna_Catalog_Model_Product::PRODUCT_VISIBILITY_EXTENDED_SHOPPING_CART);
                        $packageItemsBySections['package_id'] = $package->getPackageId();
                        $dbCartsParsed[$cartId]['packages'][$keyPack]['packageItemsBySections'] = $packageItemsBySections;
                        $dbCartsParsed[$cartId]['packages'][$keyPack]['title'] = $package->getTitle();
                        $packageTotals = $cart[0]->calculateTotalsForPackage($package->getPackageId());
                        $dbCartsParsed[$cartId]['packages'][$keyPack]['totals'] = $packageTotals;
                    }
                    if ($package->isCable()) {
                        $hasCable = true;
                    }
                    if ($package->isMobile()) {
                        $hasMobile = true;
                    }
                    if ($package->isFixed()) {
                        $hasFixed = true;
                    }
                }
            }

            // if permissions are missing, do not allow agent to continue the quote
            if (($hasMobile && !$currentAgent->isGranted('ALLOW_MOB')) ||
                ($hasCable && !$currentAgent->isGranted('ALLOW_CAB')) ||
                ($hasFixed && !$currentAgent->isGranted('ALLOW_DSL'))
            ) {
                $dbCartsParsed[$cartId]['disabled_continue'] = true;
            }
            /**
             * @var $packageHelper Dyna_Package_Helper_Data
             */
            $packageHasNotes = false;
            foreach ($cart[0]->getPackages() as $cartPackage) {
                $packageNotes = Mage::getModel('dyna_checkout/packageNotes')
                    ->getCollection()
                    ->addFieldToFilter('package_id', $cartPackage['entity_id']);

                if ($packageNotes->count() > 0) {
                    $packageHasNotes = true;
                    foreach ($packageNotes as $packageNote) {
                        $packageNoteData = $packageNote->getData();
                        // decoded it first because we send this whole "notes" key json_encoded
                        // and jQuery.parseJSON returns error otherwise
                        $packageNoteData['created_agent'] = json_decode($packageNoteData['created_agent']);
                        $packageNoteData['last_update_agent'] = json_decode($packageNoteData['last_update_agent']);

                        $dbCartsParsed[$cartId]['notes'][] = $packageNoteData;
                    }
                }
            }

            // if submit order permission is missing, allow only view
            if (!$currentAgent->isGranted('SUBMIT_ORDER')) {
                $dbCartsParsed[$cartId]['show_only'] = true;
            }

            $dbCartsParsed[$cartId]['created_at'] = date('d.m.Y', strtotime($cart[0]->getCreatedAt()));
            $dbCartsParsed[$cartId]['created_at_datetime'] = $cart[0]->getCreatedAt();
            if ($cart[0]->getIsOffer()) {
                $dbCartsParsed[$cartId]['is_expired'] = Mage::helper('dyna_checkout')->isOfferExpired($cart[0]->getCreatedAt());
                $dbCartsParsed[$cartId]['is_readonly'] = !$cartAndPackages['valid'] ?: Mage::helper('dyna_checkout')->isOfferReadonly($cart[0]->getCreatedAt());

                $dbCartsParsed[$cartId]['validationMessages'] = $cartAndPackages['validationMessages'];
                $dbCartsParsed[$cartId]['showOfferMessage'] = count($cartAndPackages['validationMessages']) > 0;
            } else {
                $dbCartsParsed[$cartId]['is_expired'] = Mage::helper('dyna_checkout')->isCartExpired($cart[0]->getCreatedAt());
                $dbCartsParsed[$cartId]['is_readonly'] = !$cartAndPackages['valid'] ?: Mage::helper('dyna_checkout')->isCartReadonly($cart[0]->getCreatedAt());
            }
            $dbCartsParsed[$cartId][Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD] = $hasCable;
            $dbCartsParsed[$cartId][Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN] = $hasFixed;
            $dbCartsParsed[$cartId][Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS] = $hasMobile;
            $dbCartsParsed[$cartId]['is_offer'] = $cart[0]->getIsOffer();
            $dbCartsParsed[$cartId]['channel'] = $cart[0]->getChannel();
            $agent = Mage::getModel('agent/agent')->load($cart[0]->getAgentId());
            $dbCartsParsed[$cartId]['agent'] = $agent->getUsername();
            $dbCartsParsed[$cartId]['agent_id'] = $agent->getId();
            $continueEdit = $cart[0]->getIsOffer() ? (int)($agent->getDealerId() == $currentAgent->getDealerId()) : 1;
            $dbCartsParsed[$cartId]['continue_edit'] = $continueEdit;
            /** @var Dyna_Customer_Helper_Data $customerHelper */
            $customerHelper = Mage::helper('dyna_customer');
            $dbCartsParsed[$cartId]['hasPermissionLoadOffer'] = $customerHelper->hasPermissionLoadOffer($agent);

            //created sales id validation
            $salesId = $cart[0]->getData('sales_id') ?? $cart[0]->getRedSalesId();
            $bundlesHelper = Mage::helper('bundles');
            $salesIdsTriple = $bundlesHelper->getProvisDataByRedSalesId($salesId);
            $saleIdValidation = 0;

            if (!isset($salesIdsTriple['error'])) {
                $saleIdValidation = 1;
            }

            $dbCartsParsed[$cartId]['saleidvalidation'] = $saleIdValidation;
            $dbCartsParsed[$cartId]['packageHasNotes'] = $packageHasNotes;
            $dbCartsParsed[$cartId]['notes'] = !empty($dbCartsParsed[$cartId]['notes']) ? json_encode($dbCartsParsed[$cartId]['notes']) : false;
        }

        return [
            "shoppingCarts" => $dbCartsParsed
        ];
    }

    /**
     * Return the customer products after a service call to getCustomerDetails Mode2
     *
     * @return array|mixed
     */
    protected function getCustomerProducts($household = false)
    {
        /** @var Dyna_Customer_Model_Session $customerStorage */
        $customerStorage = Mage::getSingleton('dyna_customer/session');

        $this->catalogHelper = Mage::helper('dyna_catalog');

        try {
            $data = $this->parseProductsResults($household);
            /** @var $validationHelper Dyna_Customer_Helper_Validation */
            $validationHelper = Mage::helper('dyna_customer/validation');
            $data = $validationHelper->checkRestrictionsForButtons($data);
            $customerStorage->setCustomerProducts($data);
            $this->setOnSessionIfCustomerIsBundleEligible($data);

            return $validationHelper->checkAgentPermissions($data);

        } catch (\Exception $e) {
            $customerStorage->clearCustomerProducts();
            Mage::getSingleton('customer/session')->setCustomerIsBundleEligible(false);
            Mage::logException($e);
            return [
                "error" => true,
                "message" => $e->getMessage(),
            ];
        }
    }

    /**
     * Get members of a household
     * @return array|mixed
     */
    public function getHouseholdMembers()
    {
        /** @var Dyna_Customer_Model_Session $customerStorage */
        $customerStorage = Mage::getSingleton('dyna_customer/session');
        try {

            // Getting household members from session
            $householdMembers = $customerStorage->getHouseHoldMembers();

            if (empty($householdMembers)) {
                /** @var Dyna_Customer_Model_Client_HouseholdMembersClient $householdClient */
                $householdClient = Mage::getModel('dyna_customer/client_HouseholdMembersClient');
                $customer = $customerStorage->getCustomer();
                $searchParams = [
                    'CUSTOMER_ID' => $customer->getData('global_id')
                ];
                $householdRequest = $householdClient->getMembers($searchParams);

                // if the service did not respond with success -> display a message
                if (!$householdRequest['Success']) {
                    return [
                        "error" => true,
                        "message" => $this->__("There was a problem with the loading of the household members . Please try again!"),
                    ];
                }

                // parse accounts
                $householdArray = [];
                $householdServices = [];

                if ($householdRequest['ReturnedNumberOfRecords'] == 1) {
                    $householdRequest['Account'] = array($householdRequest['Account']);
                }

                foreach ($householdRequest['Account'] as $account) {

                    // fix postal address
                    if (!isset($account['PostalAddress'][0])) {
                        $account['PostalAddress'][] = $account['PostalAddress'];
                    }
                    $accountID = $account['Contact']['ID'];
                    $householdServices[$accountID][] = $account['AccountCategory'];
                    $householdArray[$accountID][$account['AccountCategory']] = $account;

                    // Checking if Account is linked account and LK_ is present, since we have to change the color of linked account while displaying
                    $householdArray[$accountID][$account['AccountCategory']]['LinkedAccount'] = false;
                    if (strpos($accountID, 'LK_') !== false) {
                        $householdArray[$accountID][$account['AccountCategory']]['LinkedAccount'] = true;
                    }
                }

                // return only one element, in this specific order: Cable[KD] / DSL[FN] / Mobile[KS]
                foreach ($householdArray as &$household) {
                    if (array_key_exists(Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD, $household)) {
                        $household = $household[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD];
                    } elseif (array_key_exists(Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_CABLE, $household)) {
                        $household = $household[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_CABLE];
                    } elseif (array_key_exists(Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN, $household)) {
                        $household = $household[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN];
                    } elseif (array_key_exists(Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_DSL, $household)) {
                        $household = $household[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_DSL];
                    } elseif (array_key_exists(Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KS, $household)) {
                        $household = $household[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KS];
                    } elseif (array_key_exists(Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_MOBILE, $household)) {
                        $household = $household[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_MOBILE];
                    } elseif (array_key_exists(Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS, $household)) {
                        $household = $household[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS];
                    }

                    $household['member_services'] = $householdServices[$household['Contact']['ID']];
                }

                // Will get the customer name to show in household header
                $customer = $customerStorage->getCustomer();
                $customerName = $customer->getFirstname() . " " . $customer->getLastname();

                $householdMembers = [
                    'error' => false,
                    'members' => $householdArray,
                    'members_count' => count($householdArray),
                    'customer_details' => $customerName,
                    'breadcrumb' => $this->__('Household of') . ' ' . $customerName
                ];

                if (isset($customer->getData('contracts')[0]['subscriptions'][0]['addresses'][0])) {
                    $address = $customer->getData('contracts')[0]['subscriptions'][0]['addresses'][0];
                    $householdMembers['customer_address'] = $address['street'] . ' ' . $address['house_number'] . ', ' . $address['postcode'] . ' ' . $address['city'];
                }

                //save request data on customer session
                $customerStorage->setHouseholdMembers($householdMembers);
            }

            return $householdMembers;

        } catch (\Exception $e) {
            $customerStorage->clearHouseholdMembers();
            Mage::logException($e);
            return [
                "error" => true,
                "message" => $e->getMessage(),
            ];
        }
    }

    /**
     * todo: move logic to client and remove hardcodes
     * @param $mode2Data
     * @return mixed
     */
    protected function parseProductsResults($household = false)
    {
        /**
         * @var $categoryModel Dyna_Catalog_Model_Category
         */
        $categoryModel = Mage::getModel('catalog/category');
        // get all the product skus that are eligible for bundle for later use (OMNVFDE-1843)
        $productSkusEligibleForBundle = $categoryModel->getProductSkusForCategoryNames(Dyna_Customer_Model_Client_RetrieveCustomerInfo::getEligibleBundleCategoriesNames());

        $parsedCustomerProducts = [];
        if ($household) {
            $allCustomers = Mage::getSingleton('dyna_customer/session')->getHouseholdServiceCustomers();
        } else {
            $allCustomers = Mage::getSingleton('dyna_customer/session')->getServiceCustomers();
        }
        // used for indexing cable subscriptions for an unique combination of customer number - product id
        $productIdCounter = 1;

        /** @var Dyna_Package_Model_PackageCreationTypes $typeModel */
        $typeModel = Mage::getModel('dyna_package/packageCreationTypes');

        /** @var Dyna_Package_Model_PackageCreationTypes $dslPackageCreationTypeId */
        $dslPackageCreationTypeId = $typeModel->getTypeByPackageCode(Dyna_Catalog_Model_Type::CREATION_TYPE_DSL);

        if ($allCustomers) {
            foreach ($allCustomers as $accountType => $serviceCustomers) {
                foreach ($serviceCustomers as $serviceCustomer) {
                    if (isset($serviceCustomer['customer_number'])) {
                        $customerNumber = $serviceCustomer['customer_number'];
                        $pc = [
                            'customer_number' => $customerNumber,
                            'dunning_status' => $serviceCustomer['dunning_status'],
                            'dunning_status_text' => ($serviceCustomer['dunning_status'] == 'IN_DUNNING') ? $this->__('IN DUNNING') : null,
                            'customer_status' => $serviceCustomer['customer_status'],
                            'last_billing_amount' => $serviceCustomer['last_billing_amount'] ? $serviceCustomer['last_billing_amount'] : 0,
                            'last_billing_due_amount' => $serviceCustomer['last_billing_due_amount'] ? $serviceCustomer['last_billing_due_amount'] : 0,
                            'last_back_out_amount' => $serviceCustomer['last_back_out_amount'] ? $serviceCustomer['last_back_out_amount'] : 0,
                            'show_dunning_amount' => isset($serviceCustomer['last_back_out_amount']),
                            'customer_status_text' => $this->mapStatus($serviceCustomer['customer_status']),
                            'legacy_dunning_amount' => $serviceCustomer['legacy_dunning_amount'] ? $serviceCustomer['legacy_dunning_amount'] : 0,
                            'display_dunning_amount' => $serviceCustomer['display_dunning_amount'] ? $serviceCustomer['display_dunning_amount'] : 0,
                            'no_ordering_allowed' => $serviceCustomer['no_ordering_allowed']
                        ];

                        $serviceLineId = null;
                        $contracts = isset($serviceCustomer['contracts']) ? $serviceCustomer['contracts'] : [];

                        foreach ($contracts as $contract) {
                            $ps = [];
                            $initialContract = $contract;
                            $initialContractProducts = isset($initialContract['products']) && $initialContract['products'] ? $initialContract['products'] : null;
                            unset($initialContract['subscriptions'], $initialContract['products']);
                            $contractMappedSkusFN = [];
                            $contractMappedSocsFN = [];
                            // OMNVFDE-3013 FN case only - Tariff name is in Contract -> Products
                            if ($initialContractProducts && !empty($initialContractProducts[0]['components'])) {
                                $reversedComponents = [];
                                $tariffName = '';
                                foreach ($initialContractProducts as $product) {
                                    foreach ($product['components'] as $componentKey => $component) {
                                        $productForComponent = $this->getProductsForComponent($accountType, $product['components'], $componentKey);
                                        $reverseMapped = $this->reverseMultiMap($accountType, $component, $productForComponent);
                                        $tariffName = '';
                                        if (strtolower($reverseMapped['type']) == strtolower(Dyna_Catalog_Model_Type::DSL_SUBTYPE_SUBSCRIPTION)) {
                                            $pc['tariff'] = $reverseMapped;
                                        }

                                        $reversedComponents[] = $reverseMapped;
                                        if (isset($component['ComponentAdditionalData']) && !isset($component['ComponentAdditionalData'][0])) {
                                            $component['ComponentAdditionalData'] = [$component['ComponentAdditionalData']];
                                        }
                                        foreach ($component['ComponentAdditionalData'] as $additionalData) {
                                            if ($additionalData['Code'] == 'salesPackageName') {
                                                $tariffName = trim($additionalData['Value']);
                                                $tariffSKU = $reverseMapped['sku'];
                                                break;
                                            }
                                        }
                                    }
                                }
                                $pc['products'] = $reversedComponents;
                                foreach ($reversedComponents as $productComponentFn) {
                                    if (!empty($productComponentFn['sku'])) {
                                        $contractMappedSkusFN[] = $productComponentFn['sku'];
                                    }
                                }
                                unset($productComponentFn);
                                $pc['tariff'] = ['name' => $tariffName, 'sku' => $tariffSKU];
                            }

                            $contractBundleMarkers = [];

                            // Iterate through Contract > Subscription nodes
                            foreach ($contract['subscriptions'] as $subscription) {
                                $allSubscriptionPhoneNo = [];
                                $fnAddons = [];
                                $serviceAddress = $userAddress = null;
                                if (!empty($subscription['addresses'])) {
                                    foreach ($subscription['addresses'] as $address) {
                                        if ($address['address_type'] == Dyna_Customer_Model_Customer::SERVICE_ADDRESS) {
                                            $serviceAddress = $address;
                                                if ($accountType == 'FN' || (!isset($pc['service_address']))) {
                                                    $pc['service_address'] = $serviceAddress;
                                                }
                                        } elseif ($address['address_type'] == Dyna_Customer_Model_Customer::USER_ADDRESS) {
                                            $userAddress = $address;
                                        }
                                    }
                                }

                                $countryCode = !empty($subscription['ctn']['CountryCode']) ? trim($subscription['ctn']['CountryCode']) : '';
                                $areaCode = !empty($subscription['ctn']['LocalAreaCode']) ? trim($subscription['ctn']['LocalAreaCode']) : '';
                                $phoneNumber = !empty($subscription['ctn']['PhoneNumber']) ? trim($subscription['ctn']['PhoneNumber']) : '';
                                $ctnType = !empty($subscription['ctn']['Type']) ? trim($subscription['ctn']['Type']) : '';

                                //There's also Type in the Ctn node as per OMNVFDE-2364 which we do not handle as of yet
                                $ctn = "";
                                if (!empty($subscription['ctn'])) {
                                    if (is_array($subscription['ctn'])) {
                                        $ctn = $countryCode . $areaCode . $phoneNumber;
                                    } else {
                                        $ctn = $subscription['ctn'];
                                    }
                                }

                                // fallback contract type to Postpaid
                                $contractType = empty($contract['contract_type']) ? Dyna_Customer_Helper_Customer::MOBILE_CONTRACT_TYPE_POSTPAID : $contract['contract_type'];
                                $packageType = null;

                                if ($accountType == Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS) {
                                    $packageType = ($contractType == Dyna_Customer_Helper_Customer::MOBILE_CONTRACT_TYPE_POSTPAID)
                                        ? Dyna_Catalog_Model_Type::TYPE_MOBILE : Dyna_Catalog_Model_Type::TYPE_PREPAID;
                                } elseif ($accountType == Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN) {
                                    $packageType = Dyna_Catalog_Model_Type::TYPE_FIXED_DSL;
                                }
                                // there should always exist a product id because installBase bundles rely on combination of customer_number - product_id
                                // can be overridden bellow by array merges

                                $validationFields = [
                                    'party_type' => $serviceCustomer['party_type'],
                                    'account_class' => $serviceCustomer['account_class']
                                ];

                                $subs = [
                                    'id' => $subscription['id'],
                                    'contract_type' => $contractType,
                                    'package_type' => $packageType ? strtolower($packageType) : "",
                                    'package_creation_type' => $packageType ? strtolower($packageType) : "",
                                    'customer_number' => $customerNumber,
                                    'value_indicator' => $this->mapValueIndicator($subscription['value_indicator']),
                                    'last_subsidy_date' => $subscription['last_subsidy_date'],
                                    'ctn' => $ctn,
                                    'in_minimum_duration' => $subscription['in_minimum_duration'],
                                    'ctn_country_code' => $countryCode,
                                    'ctn_localAreaCode' => $areaCode,
                                    'ctn_type' => $ctnType,
                                    'ctn_phoneNumber' => $phoneNumber,
                                    'contract_possible_cancellation_date' => $contract['contract_possible_cancellation_date'] ?? null,
                                    'service_address' => $serviceAddress,
                                    'user_address' => $userAddress,
                                    'contact_first_name' => isset($subscription['contact']) && isset($subscription['contact']['Role'])
                                    && isset($subscription['contact']['Role']['Person']) && isset($subscription['contact']['Role']['Person']['FirstName']) ?
                                        $subscription['contact']['Role']['Person']['FirstName'] : '',
                                    'contact_last_name' => isset($subscription['contact']) && isset($subscription['contact']['Role'])
                                    && isset($subscription['contact']['Role']['Person']) && isset($subscription['contact']['Role']['Person']['FamilyName']) ?
                                        $subscription['contact']['Role']['Person']['FamilyName'] : '',
                                    'contact_birth_date' => isset($subscription['contact']['Role']['Person']['BirthDate']) ? $subscription['contact']['Role']['Person']['BirthDate'] : '',
                                    /**
                                     * OMNVFDE-1843
                                     * we assume initially that the subscription is not part of bundle nor eligible for bundle;
                                     */
                                    'bundle_class' => '',
                                    'part_of_bundle' => false,
                                    'bundle_eligible' => false,
                                    'validationFields' => $validationFields,
                                    'actual_month_of_contract' => $subscription['actual_month_of_contract'],
                                    'serial_number' => $subscription['serial_number'],
                                    'sim_number' => $subscription['sim_number']
                                ];

                                // this will store the value true if at least one of the TV products has env contract
                                $generalTVHasEnvContract = false;
                                $prods = [];
                                if (isset($subscription['products'])) {
                                    $index = [];
                                    foreach ($subscription['products'] as $product) {
                                        $productCategory = !empty($product['product_category']) ? $product['product_category'] : null;
                                        $bundleClass = '';
                                        $bundleTooltip = '';
                                        $partOfBundle = false;
                                        // this will store the value true if at least one of the components of a kip product has triple play
                                        $kipProductHasTriplePlay = false;
                                        // this will store the value true if at least one of the components of a TV product has env contract
                                        $tvProductHasEnvContract = false;

                                        if (!isset($index[$productCategory])) {
                                            $index[$productCategory] = 0;
                                        } else {
                                            $index[$productCategory]++;
                                        }

                                        $productId = $index[$productCategory];
                                        $installProductId = $product['product_id'];
                                        $productIdCounter++;
                                        $packageType = null;

                                        if ($accountType == Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD) {
                                            switch ($productCategory) {
                                                case Dyna_Customer_Model_Client_RetrieveCustomerInfo::CABLE_PRODUCT_CATEGORY_KIP:
                                                    $packageType = strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_INTERNET_PHONE);
                                                    break;
                                                case Dyna_Customer_Model_Client_RetrieveCustomerInfo::CABLE_PRODUCT_CATEGORY_TV:
                                                    $packageType = strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_TV);
                                                    break;
                                                case Dyna_Customer_Model_Client_RetrieveCustomerInfo::CABLE_PRODUCT_CATEGORY_CLS:
                                                    $packageType = strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_INDEPENDENT);
                                                    break;
                                            }

                                        }

                                        $devices = [];
                                        if (!empty($product['devices'])) {
                                            $devices = $this->mapDevicesByCategory($product['devices']);
                                        }
                                        $feProductData = [
                                            'customer_number' => $customerNumber,
                                            'product_id' => !empty($product['product_id']) ? $product['product_id'] : null,
                                            'package_type' => $packageType,
                                            'product_category' => $productCategory,
                                            'product_status' => !empty($product['product_status']) ? $product['product_status'] : null,
                                            'product_status_display' => $this->getProductStatusDisplay($product['product_status']),
                                            'commodity_classification_nature_code' => !empty($product['commodity_classification_nature_code']) ? $product['commodity_classification_nature_code'] : null,
                                            'product_status_text' => $this->mapStatus($product['product_status']),
                                            'devices' => $devices,
                                            'service_address' => $serviceAddress,
                                            'user_address' => $userAddress,
                                            'contract_type' => $contractType,
                                            'contract_start_date' => $initialContract['contract_start_date'] ?? $subscription['contract_start_date'],
                                            'contract_end_date' => $initialContract['contract_end_date'] ?? $subscription['contract_end_date'],
                                            'contract_activation_date' => $initialContract['contract_activation_date'],
                                            'contract_last_notification_date' => $initialContract['contract_last_notification_date'],
                                            'sharing_group_details' => $subscription['sharing_group'] ?? null
                                        ];

                                        if ($accountType == Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD) {
                                            $feProductData['move_offnet_to_dsl_package_creation_type_id'] = $dslPackageCreationTypeId ? $dslPackageCreationTypeId->getPackageTypeId() : null;
                                        }

                                        // Map products
                                        $components = $product['components'] ?? [];

                                        $reversedMappedProducts = [];
                                        $tariffs = $discounts = $options = [];
                                        // $allSocs has the socs for all products
                                        // $mappedSocs has the socs only for the products that could not be reversed mapped
                                        // $mappedSkus has all the skus that could be reversed mapped
                                        $allSocs = $mappedSocs = $mappedSkus = [];

                                        if ($accountType == Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN) {
                                            $subs['package_creation_type'] = $this->getFnAccessType($components);
                                            $subs['technical_service_id'] = $this->getFnTechnicalServiceId($components);
                                        }

                                        foreach ($components as $componentKey => $component) {
                                            $productForComponent = $this->getProductsForComponent($accountType, $components, $componentKey);
                                            $reversedComponent = $this->reverseMultiMap($accountType, $component, $productForComponent);

                                            if ($accountType == Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN) {
                                                // Populate values for the Tariff tab from various options
                                                if ($reversedComponent['component_type'] == self::FN_INTERNET_COMPONENT_TYPE) {
                                                    $pc['tariff']['start_date'] = $reversedComponent['start_date'];
                                                    $pc['tariff']['end_date'] = $reversedComponent['end_date'];
                                                    $pc['tariff']['termination_to'] = $reversedComponent['termination_to'];
                                                }
                                                if (isset($reversedComponent['phone_numbers'])) {
                                                    $pc['tariff']['phone_numbers'] = $reversedComponent['phone_numbers'];
                                                }
                                            }
                                            $serviceLineId = $reversedComponent["install_base_product_id"] = $installProductId;
                                            switch (strtolower($reversedComponent['type'])) {
                                                case strtolower(Dyna_Catalog_Model_Type::MOBILE_PREPAID_SUBSCRIPTION):
                                                case strtolower(Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_SUBSCRIPTION):
                                                case strtolower(Dyna_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION):
                                                case strtolower(Dyna_Catalog_Model_Type::DSL_SUBTYPE_SUBSCRIPTION):
                                                case strtolower(Dyna_Catalog_Model_Type::LTE_SUBTYPE_SUBSCRIPTION):
                                                    if (!isset($tariff)) {
                                                        $tariff = $reversedComponent;
                                                    }
                                                    $tariffs[] = $reversedComponent;
                                                    break;
                                                case strtolower(Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_ADDON):
                                                case strtolower(Dyna_Catalog_Model_Type::SUBTYPE_ADDON):
                                                case strtolower(Dyna_Catalog_Model_Type::DSL_SUBTYPE_ADDON):
                                                case strtolower(Dyna_Catalog_Model_Type::LTE_SUBTYPE_ADDON):
                                                    $options[] = $reversedComponent;
                                                    break;
                                                case strtolower(Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_PROMOTION):
                                                case strtolower(Dyna_Catalog_Model_Type::SUBTYPE_PROMOTION):
                                                case strtolower(Dyna_Catalog_Model_Type::DSL_SUBTYPE_PROMOTION):
                                                case strtolower(Dyna_Catalog_Model_Type::LTE_SUBTYPE_PROMOTION):
                                                    $discounts[] = $reversedComponent;
                                                    break;
                                                case strtolower('KDPhoneNumber'):
                                                    $phoneNumbers[] = $reversedComponent;
                                                    if (!isset($productCtn)) {
                                                        $productCtn = $reversedComponent['name'];
                                                    }
                                                    break;
                                                case strtolower(Dyna_Catalog_Model_Type::SUBTYPE_HIDDEN):
                                                    $hidden[] = $reversedComponent;
                                                    break;
                                                default:
                                                    $options[] = $reversedComponent;
                                                    break;
                                            }

                                            if (isset($component['LegacyID']) && $component['LegacyID'] && $accountType == Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD) {
                                                $allSocs[] = $component['LegacyID'];
                                            }

                                            /**
                                             * OMNVFDE-1843
                                             * a.installed base product is part of bundle if the SOC from legacyID(for kd and fn)/AgreementSoc(for kias) is one of the provided SOC-s
                                             */

                                            $legacyIdIsset = isset($component['LegacyID']) && $component['LegacyID'];
                                            $agreementIsset = isset($component['Agreement']) && isset($component['Agreement']['AgreementSOC']);

                                            if (($legacyIdIsset && in_array($component['LegacyID'], Dyna_Customer_Model_Client_RetrieveCustomerInfo::getPartOfBundleSocs()))
                                                || ($agreementIsset && in_array($component['Agreement']['AgreementSOC'], Dyna_Customer_Model_Client_RetrieveCustomerInfo::getPartOfBundleSocs()))
                                            ) {
                                                $bundleClass = 'part-of-bundle';
                                                $bundleTooltip = $this->__("Part of Bundle");
                                                $partOfBundle = true;
                                            }

                                            // triple play is needed only for kip
                                            if ($productCategory == Dyna_Customer_Model_Client_RetrieveCustomerInfo::CABLE_PRODUCT_CATEGORY_KIP) {
                                                $this->hasKipTriplePlay($kipProductHasTriplePlay, $component);
                                            }
                                            // env contract is needed only for tv
                                            if ($productCategory == Dyna_Customer_Model_Client_RetrieveCustomerInfo::CABLE_PRODUCT_CATEGORY_TV) {
                                                $this->hasTvProductEnvContract($generalTVHasEnvContract, $tvProductHasEnvContract, $reversedComponent);
                                            }
                                            $reversedMappedProducts[] = $reversedComponent;
                                            if (isset($reversedComponent['addons']) && count($reversedComponent['addons'])) {
                                                $fnAddons = array_merge($fnAddons, $reversedComponent['addons']);
                                            }

                                            // get all the TN phone numbers from all the components and set them at the subscription level later
                                            if (($accountType == Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN ||
                                                    $accountType == Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD) && isset($component['TN'])
                                            ) {
                                                $this->getComponentsPhoneNumbers($component['TN'], $allSubscriptionPhoneNo);
                                            }
                                        }

                                        $options = $this->mapByCategory($options);
                                        $discounts = $this->mapDiscountByAgreementType($discounts);
                                        $tariffsByCategory = isset($tariffs) ? $this->mapByCategory($tariffs) : null;

                                        if (!isset($tariff) && isset($hidden)) {
                                            $tariff = array_shift($hidden);
                                        } elseif (!isset($tariff) && isset($pc['tariff'])) {
                                            // Get tariff from main Contract
                                            $tariff = $pc['tariff'];
                                        }

                                        $feProductData['tariff'] = isset($tariff) ? $tariff : null;
                                        $feProductData['tariffs'] = isset($tariffs) ? $tariffs : null;
                                        $feProductData['tariffsByCategory'] = $tariffsByCategory;
                                        $feProductData['options'] = isset($options) ? $options : null;
                                        $feProductData['discounts'] = isset($discounts) ? $discounts : null;
                                        $feProductData['phone_numbers'] = isset($phoneNumbers) ? $phoneNumbers : null;
                                        $feProductData['ctn'] = isset($productCtn) ? $productCtn : null;
                                        $feProductData['bundle_class'] = $bundleClass;
                                        $feProductData['bundle_tooltip'] = $bundleTooltip;
                                        $feProductData['part_of_bundle'] = $partOfBundle;
                                        $feProductData['options_visibility'] = $this->isOneProductVisible($feProductData['options']);
                                        $feProductData['discounts_visibility'] = $this->isOneProductVisible($feProductData['discounts']);
                                        $feProductData['install_base_product_id'] = $installProductId;

                                        // triple play is needed only for kip
                                        if ($productCategory == Dyna_Customer_Model_Client_RetrieveCustomerInfo::CABLE_PRODUCT_CATEGORY_KIP) {
                                            $feProductData['triple_play'] = $kipProductHasTriplePlay;
                                        }
                                        // env contract is needed only for tv
                                        if ($productCategory == Dyna_Customer_Model_Client_RetrieveCustomerInfo::CABLE_PRODUCT_CATEGORY_TV) {
                                            $feProductData['env_contract'] = $tvProductHasEnvContract;
                                        }

                                        $feProductData['addons'] = $fnAddons;
                                        $feProductData['components'] = $reversedMappedProducts;

                                        foreach ($reversedMappedProducts as $reversedMappedProduct) {
                                            if (empty($reversedMappedProduct['sku']) && !empty($reversedMappedProduct['soc'])) {
                                                $mappedSocs[] = trim($reversedMappedProduct['soc']);
                                            } else {
                                                if (!empty($reversedMappedProduct['sku'])) {
                                                    $mappedSkus[] = $reversedMappedProduct['sku'];
                                                }
                                                if (!empty($reversedMappedProduct['soc'])) {
                                                    $allSocs[] = trim($reversedMappedProduct['soc']);
                                                }
                                            }
                                        }

                                        foreach ($fnAddons as $reversedFnAddon) {
                                            if (!empty($reversedFnAddon['sku'])) {
                                                $mappedSkus[] = $reversedFnAddon['sku'];
                                            }
                                        }

                                        $feProductData['mapped_socs'] = implode(',', $mappedSocs);
                                        $feProductData['mapped_skus'] = implode(',', $mappedSkus);
                                        $feProductData['all_socs'] = implode(',', $allSocs);
                                        $feProductData['service_line_id'] = $product['service_line_id'] ?: $subscription['standard_item_identification'];

                                        /** @var Dyna_Customer_Helper_Data $customerHelper */
                                        $customerHelper = Mage::helper('dyna_customer');

                                        //validation fields per stack
                                        switch ($accountType) {
                                            case Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN:
                                                $subs['validationFields']['tariff'] = $pc['tariff']['sku'];
                                                $contractMappedSkusFN = array_merge($contractMappedSkusFN, $mappedSkus);
                                                $contractMappedSocsFN = array_merge($contractMappedSocsFN, $mappedSocs);

                                                if ($feProductData['devices']) {
                                                    foreach ($feProductData['devices'] as $devices) {
                                                        foreach ($devices as $device) {
                                                            if (isset($device['sku']) && $device['sku'] != null) {
                                                                $contractMappedSkusFN[] = $device['sku'];
                                                            }
                                                        }
                                                    }
                                                }
                                                $subs['validationFields']['service_line_id'] = $feProductData['service_line_id'];
                                                $subs['validationFields']['mapped_skus'] = implode(',', $contractMappedSkusFN);
                                                $subs['validationFields']['open_order'] = $customerHelper->hasOpenOrders(null, Dyna_Catalog_Model_Type::getFixedPackages());
                                                break;
                                            case Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD:
                                                $feProductData['validationFields']['tariff'] = $feProductData['tariff']['sku'];
                                                $feProductData['validationFields']['mapped_skus'] = $feProductData['mapped_skus'];
                                                $feProductData['validationFields']['open_order'] = $customerHelper->hasOpenOrders($feProductData['customer_number'], $feProductData['package_type']);
                                                $subs['validationFields']['service_line_id'] = $feProductData['service_line_id'];
                                                break;
                                            case Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS:
                                                $subs['validationFields']['tariff'] = $feProductData['tariff']['sku'];
                                                $subs['validationFields']['mapped_skus'] = $feProductData['mapped_skus'];
                                                $subs['validationFields']['service_line_id'] = $feProductData['service_line_id'];
                                                $subs['validationFields']['open_order'] = $customerHelper->hasOpenOrders($feProductData['customer_number'], Dyna_Catalog_Model_Type::getMobilePackages(), $subs['ctn'], $feProductData['service_line_id']);
                                                break;
                                        }


                                        if ($accountType == Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD) {

                                            if (count($reversedMappedProducts)) {
                                                /**
                                                 * OMNVFDE-1843
                                                 * b. if is not part of bundle we will try to find if is eligible for bundle:
                                                 * the product sku must be part of the bundle categories provided
                                                 */
                                                foreach ($reversedMappedProducts as $reversedMappedProduct) {
                                                    if (!$bundleClass && isset($reversedMappedProduct['sku']) && $reversedMappedProduct['sku']) {
                                                        if (in_array($reversedMappedProduct['sku'], $productSkusEligibleForBundle)) {
                                                            $feProductData['bundle_class'] = 'eligible-for-bundle';
                                                            $feProductData['bundle_tooltip'] = $this->__("Bundle Eligible");
                                                            $feProductData['bundle_eligible'] = true;
                                                        }
                                                    }
                                                }
                                            }

                                            $prods[$productCategory][$productId] = $feProductData;
                                        } else {
                                            $prods[$productId] = $feProductData;
                                            $subs = $subs + $feProductData;
                                        }

                                        unset($tariff, $options, $discounts, $hidden, $phoneNumbers, $kipProductHasTriplePlay, $tvProductHasEnvContract, $productCtn, $reversedMappedProducts);
                                    }
                                }

                                // if this subscription is a red+ owner, loop through all other subscriptions and check if any of its children are reserved
                                if (isset($subscription['sharing_group'])
                                    && $subscription['sharing_group']['member_type'] == Dyna_Customer_Helper_Customer::SUBSCRIPTION_TYPE_OWNER
                                ) {
                                    foreach ($contracts as $contractDetails) {
                                        foreach ($contractDetails['subscriptions'] as $subscriptionDetails) {
                                            if (isset($subscriptionDetails['sharing_group'])) {
                                                if ($subscriptionDetails['sharing_group']['id'] == $subscription['sharing_group']['id']) {
                                                    // according to VFDED1W3S-1311, update number of members just for subscriptions whom member type is just Member
                                                    if (($subscriptionDetails['products'][0]['product_status'] == Dyna_Customer_Helper_Customer::MOBILE_CONTRACT_PRODUCT_STATUS_R)
                                                        && ($subscriptionDetails['sharing_group']['member_type'] != Dyna_Customer_Helper_Customer::SUBSCRIPTION_TYPE_OWNER)) {
                                                        $subs['has_reserved_member'] = true;
                                                        // Number of members in a group should not count the reserved members
                                                        $subs['sharing_group_details']['number_of_members'] -= 1;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                if ($accountType != Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD) {

                                    if ($accountType == Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN) {
                                        $prods = $pc['products'];
                                    }

                                    $subscriptionBundleMarks = $this->determineSubscriptionBundleMarks($subscription, $prods, $productSkusEligibleForBundle, $accountType);

                                    $bundleMarkers = [
                                        'bundle_class' => $subscriptionBundleMarks['bundle_class'],
                                        'part_of_bundle' => $subscriptionBundleMarks['part_of_bundle'],
                                        'bundle_eligible' => $subscriptionBundleMarks['bundle_eligible'],
                                        'bundle_tooltip' => $subscriptionBundleMarks['bundle_tooltip'],
                                        'bundle_product_ids' => implode(",", $subscriptionBundleMarks['bundle_product_ids'])
                                    ];

                                    // Attach bundle markers to the subscription
                                    $subs = array_merge($subs, $bundleMarkers, array(
                                        'install_base_product_id' => $installProductId,
                                    ));

                                    if ($accountType == Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN) {
                                        if ($bundleMarkers['part_of_bundle'] == true
                                            && (!isset($contractBundleMarkers['part_of_bundle']) || !$contractBundleMarkers['part_of_bundle'])
                                        ) {
                                            $contractBundleMarkers = $bundleMarkers;
                                        } elseif (!isset($contractBundleMarkers['part_of_bundle'])) {
                                            $contractBundleMarkers = $bundleMarkers;
                                        }
                                    }

                                } // triple play is only for kip
                                else {
                                    $subs['env_contract'] = $generalTVHasEnvContract;
                                }

                                if (count($allSubscriptionPhoneNo)) {
                                    $subs['all_phone_no'] = $allSubscriptionPhoneNo;
                                }

                                $ps[] = $subs + $initialContract + ['products' => $prods];

                                unset($tariff, $options, $hidden, $discounts, $phoneNumbers, $allSubscriptionPhoneNo);
                            }
                            if ($accountType == Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN) {
                                $pc['mapped_skus'] = implode(',', $contractMappedSkusFN);
                                $pc['mapped_socs'] = implode(',', $contractMappedSocsFN);
                                // map the options, discounts for all subscriptions by category
                                if (!empty($pc['subscriptions'])) {
                                    foreach ($pc['subscriptions'] as &$subscription) {
                                        $subscription['mapped_skus'] = $pc['mapped_skus'];
                                        $subscription['mapped_socs'] = $pc['mapped_socs'];
                                        $subscription['validationFields']['mapped_skus'] = $pc['mapped_skus'];
                                    }
                                }
                                unset($contractMappedSkusFN, $contractMappedSocsFN);
                            }

                            if (count($ps)) {
                                if ($accountType == Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN) {
                                    $pc['discounts'] = $this->mapDiscountsByCategoryForFN($ps);
                                    $pc['options'] = $this->mapOptionsByCategoryForFN($ps);
                                    $pc['devices'] = $this->mapDevicesByCategoryForFN($ps);
                                    $pc['subscriptionsByCategory'] = $this->mapSubscriptionsByCategoryForFN($ps);
                                    $pc['service_line_id'] = $serviceLineId;
                                }
                                $pc['subscriptions'] = $ps;
                                $pc += $contractBundleMarkers;
                            }

                            $parsedCustomerProducts[$accountType][$pc['customer_number']]['contracts'][] = $pc;
                        }
                    }
                }
            }
        }

        // sort Kias subscriptions by sharing group id
        if (isset($parsedCustomerProducts[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS])) {
            $this->sortKiasBySharingGroupId($parsedCustomerProducts);
            $this->setRedPlusBadges($parsedCustomerProducts);
        }
        return $parsedCustomerProducts;
    }

    protected function setRedPlusBadges(&$customerProducts)
    {
        $badgeColors = ['darkorchid', 'darkcyan', 'darkslategray'];

        $prevGroupId = null;
        foreach ($customerProducts[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS] as &$stackData) {
            $groupCounter = 0;
            foreach ($stackData['contracts'] as &$contract) {
                if (isset($contract['subscriptions'][0]['sharing_group_details'])) {
                    if ($contract['subscriptions'][0]['sharing_group_details']['id'] != $prevGroupId) {
                        $prevGroupId = $contract['subscriptions'][0]['sharing_group_details']['id'];
                        $groupCounter++;
                    }
                    $contract['subscriptions'][0]['sharing_group_details']['badge_color'] = $badgeColors[$groupCounter % count($badgeColors)];
                }
            }
        }
    }

    /**
     * @param $accountType
     * @param $components
     * @param $componentKey
     * @return null
     * @todo Implement sup
     */
    protected function getProductsForComponent($accountType, $components, $componentKey)
    {
        $result = $this->getProductsForAllComponents($accountType, $components);

        if (isset($result[$componentKey])) {
            return current($result[$componentKey]);
        } else {
            return null;
        }
    }

    /**
     * @param $accountType
     * @param $components
     * @return mixed
     */
    protected function getProductsForAllComponents($accountType, $components)
    {
        $cache_key = sprintf('%s_products_for_components_%s', $accountType, md5(serialize($components))) . time();
        if (!($result = unserialize($this->getCache()->load($cache_key)))) {
            $componentsForSKUs = [];
            $additionalAttributes = '*';

            foreach ($components as $key => $component) {
                // Get all the SKUs for this component using reverse multi map logic
                $parsedComponent = $this->parseComponent($accountType, $component);

                if (!empty($parsedComponent['sku'])) {
                    // multiple skus were returned for one component (FN case)
                    if (is_array($parsedComponent['sku'])) {
                        foreach ($parsedComponent['sku'] as $sku) {
                            $componentsForSKUs[$key][] = $sku;
                        }
                    } else {
                        // one sku is returned for one component (KIAS, KD)
                        $componentsForSKUs[$key][] = $parsedComponent['sku'];
                    }
                }

                if (!empty($componentsForSKUs[$key])) {
                    $productsForComponents = Mage::getResourceModel('catalog/product_collection')
                        ->addAttributeToFilter('sku', array_unique($componentsForSKUs[$key]))
                        ->addAttributeToSelect($additionalAttributes)
                        ->getItems();


                    $result[$key] = $productsForComponents;
                }
            }

            $this->getCache()->save(
                serialize($result),
                $cache_key,
                array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl()
            );
        }

        return $result;
    }

    /**
     * @param $componentTn
     * @param $allSubscriptionPhoneNo
     */
    protected function getComponentsPhoneNumbers($componentTn, &$allSubscriptionPhoneNo)
    {
        // single TN as array
        if (isset($componentTn['PhoneNumber'])) {
            $allSubscriptionPhoneNo[] = $componentTn;
        } // multiple TN as array
        else if (is_array($componentTn)) {
            foreach ($componentTn as $tn) {
                if (isset($tn['PhoneNumber'])) {
                    $allSubscriptionPhoneNo[] = $tn;
                } else {
                    $allSubscriptionPhoneNo[] = $this->getParsedTn($tn);
                }
            }
        } // single TN as string
        else {
            $allSubscriptionPhoneNo[] = $this->getParsedTn($componentTn);
        }
    }

    /**
     * @param $tn
     * @return array
     */
    protected function getParsedTn($tn)
    {
        $parsedTn = [];
        if (strpos($tn, "-")) {
            $explodedTn = explode("-", $tn);
            $parsedTn['LocalAreaCode'] = trim($explodedTn[0]);
            $parsedTn['PhoneNumber'] = trim($explodedTn[1]);
        } else {
            $parsedTn['PhoneNumber'] = $tn;
        }

        return $parsedTn;
    }

    /**
     * Find if at least 1 product from an associative array of products, grouped on categories, has product_visibility = true;
     * @param array $products
     * @return bool
     */
    protected function isOneProductVisible($products)
    {
        if (!$products) {
            return false;
        }

        $isVisible = false;
        foreach ($products as $productsArray) {
            foreach ($productsArray as $product) {
                if (!empty($product['product_visibility']) && $product['product_visibility']) {
                    return true;
                }
            }
        }

        return $isVisible;
    }

    /**
     * @param bool $kipProductHasTriplePlay
     * @param array $component
     */
    protected function hasKipTriplePlay(&$kipProductHasTriplePlay, $component)
    {
        if (!$kipProductHasTriplePlay && strtolower($component['LegacyID']) == strtolower(Dyna_Customer_Helper_Services::KIP_LEGACY_ID_TRIPLE_PLAY)) {
            $kipProductHasTriplePlay = true;
        }
    }

    /**
     * If at least one product in the costumers inventory on that service line has the product type KAA, it’s an ENV contract
     * If not => MNV (OMNVFDE-2741)
     * This is needed only for TV
     * @param bool $generalTvHasEnvContract
     * @param bool $tvProductHasEnvContract
     * @param array $reversedComponent
     */
    protected function hasTvProductEnvContract(&$generalTvHasEnvContract, &$tvProductHasEnvContract, $reversedComponent)
    {
        if (!$tvProductHasEnvContract && $reversedComponent['service_category_KAA']) {
            $generalTvHasEnvContract = $tvProductHasEnvContract = true;
        }
    }

    /**
     * Used for customer and product
     * @param $value
     * @return mixed
     */
    protected function mapStatus($value)
    {
        $map = [
            "A" => $this->__("Active"),
            "C" => $this->__("Cancelled"),
            "R" => $this->__("Reserved"),
            "S" => $this->__("Suspended"),
            "N" => $this->__("Cancelled"),
            "O" => $this->__("Open"),
            "T" => $this->__("Tentative"),
        ];

        return ($value && isset($map[$value])) ? $map[$value] : $value;
    }

    /**
     * @param $value
     * @return mixed
     */
    protected function mapValueIndicator($value)
    {
        $map = [
            "R" => "Red",
            "S" => "Silver",
            "G" => "Gold",
            "P" => "Platin",
            "T" => "Rot",
            "B" => "Bronze",
        ];

        return $map[$value] ?? $value;
    }

    /**
     * @param $accountType
     * @param $component
     * @return array
     */
    protected function parseComponent($accountType, $component)
    {
        switch ($accountType) {
            case Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS:
                $result = $this->parseKiasComponent($component);
                break;
            case Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD:
                $result = $this->parseKdComponent($component);
                break;
            case Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN:
                $result = $this->parseFnComponent($component);
                break;
            default:
                $result = [];
                break;
        }

        return $result;
    }

    /**
     * @param $component
     * @return array
     */
    protected function parseKiasComponent($component)
    {
        $sku = null;
        /** @var Dyna_MultiMapper_Helper_Data $h */
        $h = Mage::helper('dyna_multimapper');
        $soc = $component['Agreement']['AgreementSOC'];

        $sku = $h->getSkuBySoc($soc);
        return array('sku' => $sku, 'additionalAttributes' => ['maf', 'name', 'display_name_inventory']);
    }

    /**
     * @param $component
     * @return array
     */
    protected function parseKdComponent($component)
    {
        $sku = null;
        /** @var Dyna_MultiMapper_Helper_Data $h */
        $h = Mage::helper('dyna_multimapper');
        if (!empty($component['TN'])) {
            // For cable, SKU = SOC (LegacyID), no reverse mapping needed
            $sku = !empty($component['LegacyID']) ? trim($component['LegacyID']) : $sku;
        } else {
            // Product name should be reverse mapped using LegacyID to Commercial product name, and displayed using display_inventory attribute value
            // @see OMNVFDE-2710
            if (isset($component['LegacyID'])) {
                $legacyId = trim($component['LegacyID']);
                // Try getting sku from multimapper
                $sku = $h->getSkuBySoc($legacyId);
            }
        }
        return array('sku' => $sku, 'additionalAttributes' => ['display_name_inventory', 'name', 'product_visibility', 'display_price_repeated', 'product_version']);
    }

    /**
     * @param $component
     * @return array
     */
    protected function parseFnComponent($component)
    {
        $skus = [];
        $mapperIds = $this->getMMByNodes($component['ComponentAdditionalData']);

        if ($mapperIds) {
            $mappers = $this->getFnMappers($mapperIds);
            foreach ($mappers as $mapper) {
                if ($mappers && $mapper->getId()) {
                    $skus[] = $mapper->getSku();
                }
            }
        }

        return array('sku' => $skus, 'additionalAttributes' => ['maf', 'product_version']);
    }

    /**
     * @param $accountType
     * @param $component
     * @param $product
     * @return array
     */
    protected function reverseMultiMap($accountType, $component, $products = null)
    {
        switch ($accountType) {
            case Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS:
                $result = $this->mapKiasComponent($component, $products);
                break;
            case Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD:
                $result = $this->mapKdComponent($component, $products);
                break;
            case Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN:
                $result = $this->mapFnComponent($component, $products);
                break;
            default:
                $result = [];
                break;
        }

        return $result;
    }

    /**
     * @param $component
     * @param null|array $product
     * @return array
     */
    protected function mapKiasComponent($component, $product = null)
    {
        /**
         * @var $helper Dyna_Checkout_Helper_Data
         */
        $helper = Mage::helper('dyna_checkout');
        $name = null;
        $type = null;
        $sku = null;
        $soc = $component['Agreement']['AgreementSOC'];
        $fields = [];
        $productVisibility = true;
        $isRedPlusOwner = false;
        $isRedPlusMember = false;
        $maf = false;
        if ($product && $product->getId()) {
            $sku = $product->getSku();
            $maf = $helper->showNewFormattedPriced($product->getMaf(), true);
            $productFirstCategoryName = $this->getNameOfFirstFamilyCategory($product->getId());
            $isRedPlusMember = $product->isRedPlusMember();
        }

        $agreementServiceType = isset($component['Agreement']['AgreementServiceType']) ? $component['Agreement']['AgreementServiceType'] : null;

        switch ($agreementServiceType) {
            case 'P':
                if ($product && $product->getId()) {
                    $name = $product->getDisplayNameInventory() ?: $product->getName();
                    $type = $product->getType();
                    $type = array_shift($type);
                }
                $name = $name ?? ($component['TariffOptionName'] ?? '');
                $type = $type ?? Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_SUBSCRIPTION;
                break;
            case 'O':
            case 'R':
                // Options
                $fields = [
                    'active_from' => isset($component['Agreement']['AgreementEffectiveDate']) ? date_format(date_create($component['Agreement']['AgreementEffectiveDate']), 'Y-m-d') : null,
                    'active_until' => isset($component['CommitEndDate']) ? date_format(date_create($component['CommitEndDate']), 'Y-m-d') : null,
                ];
                if ($product && $product->getId()) {
                    $name = $product->getDisplayNameInventory() ?: $product->getName();
                    $type = $product->getType();
                    $type = array_shift($type);
                }
                if ($agreementServiceType == 'R') {
                    // OMNVFDE-2806 : the Option name needs to be derived from: AgreementSOCDescription
                    $name = $component['Agreement']['AgreementSOCDescription'] ?? '';
                } else {
                    $name = $name ?? ($component['Agreement']['AgreementSOCDescription'] ?? '');
                }
                $type = $type ?? Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_ADDON;
                break;
            case 'M':
                // Goodies
                if ($product && $product->getId()) {
                    $name = $product->getDisplayNameInventory() ?: $product->getName();
                    $type = $product->getType();
                    $type = array_shift($type);
                }
                $name = $name ?? ($component['Agreement']['AgreementSOCDescription'] ?? '');
                $type = $type ?? Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_PROMOTION;
                break;
        }

        if ($product && $product->getId()) {
            $productVisibility = $this->catalogHelper->getVisibility(Dyna_Catalog_Model_Product::PRODUCT_VISIBILITY_INVENTORY, $sku, $product);
        }

        $componentData = array_merge(
            [
                'name' => $name,
                'type' => $type,
                'sku' => $sku,
                'maf' => $maf,
                'product_visibility' => $productVisibility,
                'soc' => $soc,
                'is_red_plus_owner' => $isRedPlusOwner,
                'is_red_plus_member' => $isRedPlusMember,
                'category_name' => $productFirstCategoryName ?? '',
                'agreement_service_type' => $agreementServiceType,
            ],
            $fields
        );


        return $componentData;
    }

    /**
     * @param $component
     * @param null|Dyna_Catalog_Model_Product $product
     * @return array
     */
    protected function mapKdComponent($component, $product = null)
    {
        /**
         * @var $helper Dyna_Checkout_Helper_Data
         */
        $helper = Mage::helper('dyna_checkout');
        $name = null;
        $serviceCategoryKAA = null;
        $type = null;
        $sku = null;
        $productVisibility = true;
        $productVersion = false;
        $displayPriceRepeated = null;
        if ($product && $product->getId()) {
            $sku = $product->getSku();
            $productFirstCategoryName = $this->getNameOfFirstFamilyCategory($product->getId());
            $displayPriceRepeated = $helper->showNewFormattedPriced($product->getDisplayPriceRepeated(), true);
            $productVersion = $product->getProductVersionCode();
        }

        $fields = [
            'legacy_code' => $component['LegacyID'] ?? null,
        ];

        // Special case for phone numbers
        if (!empty($component['TN'])) {
            $name = is_array($component['TN']) ? ltrim(implode(' ', $component['TN'])) : trim($component['TN']);
            $fields['active_from'] = $component['ProductStartDate'] ?? null;
            $type = 'KDPhoneNumber';
        } else {
            if ($product && $product->getId()) {
                // Display name has changed to the value of display_inventory attribute @see OMNVFDE-2710
                $name = $product->getDisplayNameInventory() ?: $product->getName();
                $serviceCategoryKAA = $product->hasServiceCategoryKAA();
                $type = $product->getType();
                $type = array_shift($type);

                $productVisibility = $this->catalogHelper->getVisibility(Dyna_Catalog_Model_Product::PRODUCT_VISIBILITY_INVENTORY, $sku, $product);
            }

            $name = $name ?? ($component['ProductDescription'] ?? '');
            $type = $type ?? null;

            $fields['contract_start_date'] = $component['ProductStartDate'] ?? null;
        }

        $componentData = array_merge(
            [
                'name' => $name,
                'type' => $type,
                'service_category_KAA' => $serviceCategoryKAA,
                'sku' => $sku,
                'display_price_repeated' => $displayPriceRepeated,
                'product_version' => $productVersion,
                'product_visibility' => $productVisibility,
                'category_name' => $productFirstCategoryName ?? '',
            ],
            $fields
        );

        return $componentData;
    }

    /**
     * @param $component
     * @param $product Dyna_Catalog_Model_Product
     * @return array
     */
    protected function mapFnComponent($component, $product = null)
    {
        $fields = [];

        if (isset($component['ComponentAdditionalData']) && !isset($component['ComponentAdditionalData'][0])) {
            $component['ComponentAdditionalData'] = [$component['ComponentAdditionalData']];
        }

        $componentType = $this->getFnComponentType($component['ComponentAdditionalData']);

        $fields['component_type'] = $componentType;

        if (in_array($componentType, [self::FN_VOICE_COMPONENT_TYPE, self::FN_INTERNET_COMPONENT_TYPE])) {
            $fields['start_date'] = $this->getFnComponentStartDate($component['ComponentAdditionalData'], $componentType);
            $fields['start_date'] = $this->getFnContractStartDate($component['ComponentAdditionalData'], $componentType);
            $fields['end_date'] = $this->getFnContractEndDate($component['ComponentAdditionalData'], $componentType);
            $fields['termination_to'] = $this->getFnComponentTerminationTo($component['ComponentAdditionalData'], $componentType, $fields['end_date']);
            $fields['discount'] = $this->getFnComponentDiscount($component['ComponentAdditionalData'], $componentType);
        }

        if ($componentType == self::FN_INTERNET_COMPONENT_TYPE) {
            $fields['addons'] = $this->getFnComponentAddon($component['ComponentAdditionalData'], $componentType);
        }

        if ($componentType == self::FN_VOICE_COMPONENT_TYPE) {
            $fields['connection_type'] = $this->getFnComponentConnectionType($component['ComponentAdditionalData'], $componentType);
            $fields['addons'] = $this->getFnComponentAddon($component['ComponentAdditionalData'], $componentType);
        }

        if ($componentType == self::FN_SECURITY_COMPONENT_TYPE) {
            $fields['start_date'] = $this->getFnContractStartDate($component['ComponentAdditionalData'], $componentType);
            $fields['end_date'] = $this->getFnContractEndDate($component['ComponentAdditionalData'], $componentType);
            $fields['termination_to'] = $this->getFnComponentTerminationTo($component['ComponentAdditionalData'], $componentType, $fields['end_date']);;
        }

        if ($componentType == self::FN_TV_COMPONENT_TYPE) {
            $fields['start_date'] = $this->getFnComponentStartDate($component['ComponentAdditionalData'], $componentType);
            $fields['start_date'] = $this->getFnContractStartDate($component['ComponentAdditionalData'], $componentType);
            $fields['end_date'] = $this->getFnContractEndDate($component['ComponentAdditionalData'], $componentType);
            $fields['termination_to'] = $this->getFnComponentTerminationTo($component['ComponentAdditionalData'], $componentType, $fields['end_date']);;
            $fields['connection_type'] = $this->getTvComponentConnectionType($component['ComponentAdditionalData'], $componentType);
            $fields['discount'] = $this->getFnComponentDiscount($component['ComponentAdditionalData'], $componentType);
            $fields['addons'] = $this->getFnComponentAddon($component['ComponentAdditionalData'], $componentType);
        }

        if (isset($component['TN'])) {
            $componentTns = $component['TN'];

            if (is_array($componentTns) && !isset($componentTns[0])) {
                $componentTns = [$componentTns];
            }

            foreach ($componentTns as $componentTn) {
                if (is_array($componentTn)) {
                    // Remove leading 0s for local area code if country code is also present OMNVFDE-3989
                    $componentTn['LocalAreaCode'] = isset($componentTn['CountryCode']) && $componentTn['CountryCode'] !== '' ? ltrim($componentTn['LocalAreaCode'], '0') : $componentTn['LocalAreaCode'];
                    $fields['phone_numbers'][] = ltrim(implode(' ', $componentTn));
                } else {
                    $fields['phone_numbers'][] = trim($componentTn);
                }
            }

            $componentTn = $componentTns[0];
        } else {
            $componentTn = '';
        }
        $fields['active'] = isset($fields['end_date']) ? ($fields['end_date'] > date('Y-m-d') ? true : false) : false;

        if (is_array($componentTn)) {
            // Remove leading 0s for local area code if country code is also present OMNVFDE-3989
            $componentTn['LocalAreaCode'] = isset($componentTn['CountryCode']) && $componentTn['CountryCode'] !== '' ? ltrim($componentTn['LocalAreaCode'], '0') : $componentTn['LocalAreaCode'];
            $tn = ltrim(implode(' ', $componentTn));
        } else {
            $tn = trim($componentTn);
        }
        $fields['technical_code'] = is_array($tn) ? ltrim(implode(' ', $tn)) : trim($tn) ?? null;
        $fields['has_old_products'] = $this->getFnOldProducts($component['ComponentAdditionalData'], $componentType);

        $productInfo = $this->extractComponentFromProduct($product);
        $result = array_merge($productInfo, $fields);

        return $result;
    }

    protected function extractComponentFromProduct($product)
    {
        /**
         * @var $helper Dyna_Checkout_Helper_Data
         */
        $helper = Mage::helper('dyna_checkout');
        $name = null;
        $internalName = null;
        $type = null;
        $sku = null;
        $maf = false;
        $productVersion = false;
        $productVisibility = true;

        if ($product && $product->getId()) {
            $sku = $product->getSku();
            $productFirstCategoryName = $this->getNameOfFirstFamilyCategory($product->getId());
            $maf = $helper->showNewFormattedPriced($product->getMaf(), true);
            $productVersion = $product->getProductVersionCode();
            $name = $product->getDisplayNameInventory() ?: $product->getName();
            $internalName = $product->getInternalName();
            $type = $product->getType();
            $type = array_shift($type);

            // Determine product visibility
            $productVisibility = $this->catalogHelper->getVisibility(Dyna_Catalog_Model_Product::PRODUCT_VISIBILITY_INVENTORY, $sku, $product);
        }

        return [
            'name' => $name,
            'type' => $type,
            'sku' => $sku,
            'maf' => $maf,
            'product_visibility' => $productVisibility,
            'internal_name' => $internalName,
            'category_name' => $productFirstCategoryName ?? 'UNCATEGORIZED',
            'product_version' => $productVersion,
        ];
    }

    /**
     * Get name of product's first category
     * @param $productId
     * @return string
     */
    protected function getNameOfFirstFamilyCategory($productId)
    {
        $categoryFamilyName = '';
        $categoryModel = Mage::getModel('catalog/category');
        /** @var Mage_Catalog_Model_Product $catalogProduct */
        $catalogProduct = Mage::getModel('catalog/product')->load($productId);
        $productVisibility = $catalogProduct->getProductVisibility();
        $productCategories = $catalogProduct->getCategoryIds();
        foreach ($productCategories as $categoryId) {
            $category = Mage::getModel('catalog/category')->load($categoryId);
            $categoryFamilyName = $categoryModel->getCategoryFamilyByName($category->getName());
            if ($categoryModel->getCategoryFamilyByName($category->getName()) != '' && (strpos($productVisibility, '142') !== false)) {
                $categoryFamilyName = $categoryModel->getCategoryFamilyByName($category->getName());
                break;
            }
        }
        return $categoryFamilyName;
    }

    /**
     * Group products by family categories
     * @param $items
     * @return array
     */
    protected function mapByCategory($items)
    {
        $itemsByCategory = array();
        $categories = array();
        $counter = 0;
        if ($items) {
            /** @var Dyna_Catalog_Model_Category $categoryModel */
            $categoryModel = Mage::getModel('catalog/category');
            foreach ($items as $item) {
                if ($item['sku'] && empty($item['product_visibility'])) {
                    continue;
                }
                if (isset($item['category_name']) && $item['category_name'] != '') {
                    $categoryFamilyName = $categoryModel->getCategoryFamilyByName($item['category_name']);
                    $categoryName = (strtolower($item['category_name']) == strtolower($categoryFamilyName)) ? $item['category_name'] : 'Uncategorized';
                } else {
                    $categoryName = 'Uncategorized';
                }
                $itemsByCategory[$categoryName][$counter] = $item;
                if (!in_array(strtoupper($categoryName), $categories)) {
                    array_push($categories, strtoupper($categoryName));
                }
                $counter++;
            }
        }
        if (isset($itemsByCategory['Uncategorized'])) {
            $uncategorizedItems = $itemsByCategory['Uncategorized'];
            unset($itemsByCategory['Uncategorized']);
            $itemsByCategory['Uncategorized'] = $uncategorizedItems;
        }

        return $itemsByCategory;
    }

    /**
     * @param $devices
     * @return array
     */
    protected function mapDevicesByCategory($devices)
    {
        foreach ($devices as &$device) {
            if (isset($device['sku']) && $device['sku']) {
                /** @var Dyna_Catalog_Model_Product $product */
                $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $device['sku']);
                if ($product->getId()) {
                    $productFirstCategoryName = $this->getNameOfFirstFamilyCategory($product->getId());
                    $device['category_name'] = $productFirstCategoryName;
                }
            }
        }

        return $this->mapByCategory($devices);
    }

    /**
     * @param $subscriptions
     * @return array
     */
    protected function mapDevicesByCategoryForFN($subscriptions)
    {
        $allDevices = [];
        foreach ($subscriptions as $subscription) {
            if ($subscription['devices']) {
                foreach ($subscription['devices'] as $device) {
                    $allDevices[] = isset($device['sku']) ? $device : array_shift($device);
                }
            }
        }

        return ($allDevices) ? $this->mapDevicesByCategory($allDevices) : [];
    }

    protected function mapSubscriptionsByCategoryForFN($subscriptions)
    {
        $allSubscriptions = [];
        foreach ($subscriptions as $subscription) {
            if (!empty($subscription['options'])) {
                foreach ($subscription['options'] as $optionPerCategory) {
                    foreach ($optionPerCategory as $option) {
                        $option['contract_possible_cancellation_date'] = $subscription['contract_possible_cancellation_date'];
                        $allSubscriptions[] = $option;
                    }
                }
            }
        }

        return ($allSubscriptions) ? $this->mapByCategory($allSubscriptions) : [];
    }

    protected function mapOptionsByCategoryForFN($subscriptions)
    {
        $allOptions = [];
        foreach ($subscriptions as $subscription) {
            if (!empty($subscription['components'])) {
                foreach ($subscription['components'] as $component) {
                    if (!empty($component['addons'])) {
                        foreach ($component['addons'] as $addon) {
                            $allOptions[] = $addon;
                        }
                        unset($component['addons']);
                    }
                    if ($component['type'] != Dyna_Catalog_Model_Type::SUBTYPE_GOODY) {
                        $allOptions[] = $component;
                    }
                }
            }
        }

        return ($allOptions) ? $this->mapByCategory($allOptions) : [];
    }

    protected function mapDiscountsByCategoryForFn($subscriptions)
    {
        $allDiscounts = [];
        foreach ($subscriptions as $subscription) {
            if ($subscription['discounts']) {
                foreach ($subscription['discounts'] as $discount) {
                    $allDiscounts[] = $discount;
                }
            }
        }

        return ($allDiscounts) ? $this->mapByCategory($allDiscounts) : [];
    }

    protected function mapDiscountByAgreementType($discounts)
    {
        $counter = 0;
        $index = 0;
        $updatedDiscounts = array();
        $discountsByAgreementType = array();
        if ($discounts) {
            foreach ($discounts as $discount) {
                $agreementServiceType = isset($discount['agreement_service_type']) && $discount['agreement_service_type'] != '' ? $discount['agreement_service_type'] : 'Not Agreement Service Type';
                $updatedDiscounts[$agreementServiceType][$counter] = $discount;
                switch ($agreementServiceType) {
                    case 'M':
                        $updatedDiscounts[$agreementServiceType][$counter] = array_merge($discount, array('agreement_type_name' => 'GOODIES'));
                        break;
                    default:
                        $updatedDiscounts[$agreementServiceType][$counter] = array_merge($discount, array('agreement_type_name' => 'PROMOTIONS'));
                        break;
                }
                $counter++;
            }
        }

        if ($updatedDiscounts) {
            foreach ($updatedDiscounts as $updatedDiscountsChild) {
                foreach ($updatedDiscountsChild as $updatedDiscount) {
                    $updatedDiscountAgreementName = $updatedDiscount['agreement_type_name'];
                    $discountsByAgreementType[$updatedDiscountAgreementName][$index] = $updatedDiscount;
                    $index++;
                }
            }
        }
        return $discountsByAgreementType;
    }

    /**
     * @param $mapperIds
     * @return Dyna_MultiMapper_Model_Mapper|array
     */
    protected function getFnMappers($mapperIds)
    {
        $fnMappers = [];
        /**
         * @var $mapperModel Dyna_MultiMapper_Model_Mapper
         */
        $mapperModel = Mage::getModel("dyna_multimapper/mapper");

        if (!$mapperIds) {
            return $fnMappers;
        }
        $mappers = $mapperModel->getMappersWithInDirection($mapperIds);

        if ($mappers) {

            /** @var Dyna_MultiMapper_Model_Mapper $mapper */
            foreach ($mappers as $mapper) {
                // FN options mapping OMNVFDE-2928
                if ($mapper->getServiceExpression()) {
                    /** @var $dynaCoreHelper Dyna_Core_Helper_Data */
                    $dynaCoreHelper = Mage::helper('dyna_core');

                    $passed = $dynaCoreHelper->evaluateExpressionLanguage(
                        $mapper->getServiceExpression(),
                        array('customer' => Mage::getModel('dyna_configurator/expression_customer'))
                    );

                    if ($passed && $mapper->getId()) {
                        $fnMappers[] = $mapper;
                        break;
                    }
                } else {
                    // Get the first match item
                    if ($mapper->getId()) {
                        $fnMappers[] = $mapper;
                    }
                }
            }
        }

        return $fnMappers;
    }

    /**
     * @param $componentsAdditionalData
     * @param $componentType
     * @return array
     */
    protected function getFnComponentAddon($componentsAdditionalData, $componentType)
    {
        /**
         * @var $helper Dyna_Checkout_Helper_Data
         */
        $helper = Mage::helper('dyna_checkout');
        $productName = $fallbackName = '';
        $productType = 'option';
        $startDate = null;
        $endDate = null;
        $TVOptions = [];
        $VoiceOptions = [];
        $internetOptions = [];
        $hasAccessTechnology = $hasProductCode = false;
        $addons = [];
        $categoryName = 'Uncategorized';
        $maf = false;
        $terminationTo = null; // prolongation date
        $productVisibility = null;

        foreach ($componentsAdditionalData as $additional) {
            if (isset($additional['AdditionalValue']) && strpos($additional['AdditionalValue'], '/functions/tvCenter/tvCenterBundledConfiguration/tariffOptionsList/existing/tariffOption[') !== false) {
                if ($additional['Code'] == 'serviceCode') {
                    $TVOptions['tariffOptionsList'][$additional['Code']][] = $additional;
                } else {
                    $TVOptions['tariffOptionsList'][$additional['Code']] = $additional;
                }
            } elseif (isset($additional['AdditionalValue']) && strpos($additional['AdditionalValue'], '/functions/tvCenter/tvCenterBundledConfiguration/tvCenterOptionsList/existing/tvCenterOption[') !== false) {
                if ($additional['Code'] == 'serviceCode') {
                    $TVOptions['tvCenterOptionsList'][$additional['Code']][] = $additional;
                } else {
                    $TVOptions['tvCenterOptionsList'][$additional['Code']] = $additional;
                }
            } elseif (isset($additional['AdditionalValue']) && strpos($additional['AdditionalValue'], '/functions/voice/voiceBasisConfiguration/tariffOptionsList/existing/tariffOption[') !== false) {
                if ($additional['Code'] == 'serviceCode') {
                    $VoiceOptions['voiceBasisConfiguration'][$additional['Code']][] = $additional;
                } else {
                    $VoiceOptions['voiceBasisConfiguration'][$additional['Code']] = $additional;
                }
            } elseif (isset($additional['AdditionalValue']) && strpos($additional['AdditionalValue'], '/functions/voice/voicePremiumConfiguration/tariffOptionsList/existing/tariffOption[') !== false) {
                if ($additional['Code'] == 'serviceCode') {
                    $VoiceOptions['voicePremiumConfiguration'][$additional['Code']][] = $additional;
                } else {
                    $VoiceOptions['voicePremiumConfiguration'][$additional['Code']] = $additional;
                }
            } elseif (isset($additional['Code']) && $additional['Code'] == 'accessTechnology') {
                $internetOptions[] = $additional;
                $hasAccessTechnology = true;
            } elseif (isset($additional['Code']) && $additional['Code'] == 'productCode') {
                $internetOptions[] = $additional;
                $hasProductCode = true;
            }

            if ($additional['Code'] == 'serviceCode') {
                continue;
            }
        }

        switch ($componentType) {
            case self::FN_INTERNET_COMPONENT_TYPE:
                if (count($internetOptions > 1) && $hasAccessTechnology && $hasProductCode) {
                    $searchBy = $internetOptions;

                    $mapperIds = $this->getMMByCodes($searchBy);
                    $mappers = $this->getFnMappers($mapperIds);

                    $mapper = null;
                    if (count($mappers)) {
                        $mapper = current($mappers);
                    }
                    if ($mapper && $mapper->getId() && ($sku = $mapper->getSku())) {

                        /** @var Dyna_Catalog_Model_Product $product */
                        $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
                        if ($product && $product->getId()) {
                            $categoryName = $this->getNameOfFirstFamilyCategory($product->getId());
                            $productName = $product->getName();
                            $maf = $helper->showNewFormattedPriced($product->getMaf(), true);
                            $type = $product->getType();
                            $productType = array_shift($type);
                            $productVisibility = $this->catalogHelper->getVisibility(Dyna_Catalog_Model_Product::PRODUCT_VISIBILITY_INVENTORY, $sku, $product);
                        }
                    }

                    foreach ($componentsAdditionalData as $additional) {
                        if ($additional['Code'] == "startDate"
                            && $additional['AdditionalValue'] == "/functions/internet/adslInternetConfiguration/minimumDurationPeriod/startDate/existing"
                        ) {
                            $startDate = $additional['Value'];
                        }

                        if ($additional['Code'] == "endDate"
                            && $additional['AdditionalValue'] == "/functions/internet/adslInternetConfiguration/minimumDurationPeriod/endDate/existing"
                        ) {
                            $endDate = $additional['Value'];
                        }
                    }

                    if ($endDate) {
                        $terminationTo = $this->getFnComponentTerminationTo($componentsAdditionalData, $componentType, $endDate);
                    }

                    $addons[] = [
                        'name' => !empty($productName) ? $productName : $fallbackName,
                        'type' => $productType,
                        'start_date' => $startDate,
                        'end_date' => $endDate,
                        'termination_to' => $terminationTo,
                        'category_name' => $categoryName ? $categoryName : 'Uncategorized',
                        'sku' => $sku ?? null,
                        'maf' => $maf,
                        'product_visibility' => $productVisibility,
                    ];
                }
                break;
            case self::FN_VOICE_COMPONENT_TYPE:
                foreach ($VoiceOptions as $VoiceOption) {
                    if (count($VoiceOption['serviceCode']) > 0) {
                        foreach ($VoiceOption['serviceCode'] as $serviceCodeOption) {
                            $searchBy = [
                                $serviceCodeOption
                            ];

                            $mapperIds = $this->getMMByCodes($searchBy);
                            $mappers = $this->getFnMappers($mapperIds);

                            $mapper = null;
                            if (count($mappers)) {
                                $mapper = current($mappers);
                            }

                            if ($mapper && $mapper->getId() && ($sku = $mapper->getSku())) {

                                /** @var Dyna_Catalog_Model_Product $product */
                                $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
                                if ($product && $product->getId()) {
                                    $categoryName = $this->getNameOfFirstFamilyCategory($product->getId());
                                    $productName = $product->getName();
                                    $maf = $helper->showNewFormattedPriced($product->getMaf(), true);
                                    $type = $product->getType();
                                    $productType = array_shift($type);
                                    $productVisibility = $this->catalogHelper->getVisibility(Dyna_Catalog_Model_Product::PRODUCT_VISIBILITY_INVENTORY, $sku, $product);
                                }
                            }

                            $addonId = $serviceCodeOption['Value'];
                            foreach ($componentsAdditionalData as $additional) {
                                // todo: check if fallback names are correctly mapped or provided
                                if ($additional['Code'] == "serviceBillingName"
                                    && ($additional['AdditionalValue'] == "/functions/voice/voiceBasisConfiguration/tariffOptionsList/existing/tariffOption[@ID=$addonId]/serviceBillingName"
                                        || $additional['AdditionalValue'] == "/functions/voice/voicePremiumConfiguration/tariffOptionsList/existing/tariffOption[@ID=$addonId]/serviceBillingName")
                                ) {
                                    $fallbackName = $additional['Value'];
                                }

                                if ($additional['Code'] == "startDate"
                                    && ($additional['AdditionalValue'] == "/functions/voice/voiceBasisConfiguration/tariffOptionsList/existing/tariffOption[@ID=$addonId]/minimumDurationPeriod/startDate"
                                        || $additional['AdditionalValue'] == "/functions/voice/voicePremiumConfiguration/tariffOptionsList/existing/tariffOption[@ID=$addonId]/minimumDurationPeriod/startDate")
                                ) {
                                    $startDate = $additional['Value'];
                                }

                                if ($additional['Code'] == "endDate"
                                    && ($additional['AdditionalValue'] == "/functions/voice/voiceBasisConfiguration/tariffOptionsList/existing/tariffOption[@ID=$addonId]/minimumDurationPeriod/endDate"
                                        || $additional['AdditionalValue'] == "/functions/voice/voicePremiumConfiguration/tariffOptionsList/existing/tariffOption[@ID=$addonId]/minimumDurationPeriod/endDate")
                                ) {
                                    $endDate = $additional['Value'];
                                }
                            }

                            if ($endDate) {
                                $terminationTo = $this->getFnComponentTerminationTo($componentsAdditionalData, $componentType, $endDate);
                            }

                            $addons[] = [
                                'name' => !empty($productName) ? $productName : $fallbackName,
                                'type' => $productType,
                                'start_date' => $startDate,
                                'end_date' => $endDate,
                                'termination_to' => $terminationTo,
                                'category_name' => $categoryName ? $categoryName : 'Uncategorized',
                                'sku' => $sku ?? null,
                                'maf' => $maf,
                                'product_visibility' => $productVisibility,
                            ];
                        }
                    }
                }
                break;
            case
            self::FN_TV_COMPONENT_TYPE:
                // Try to reverse map
                foreach ($TVOptions as $TVOption) {
                    if (count($TVOption['serviceCode']) > 0) {
                        foreach ($TVOption['serviceCode'] as $serviceCodeOption) {
                            $searchBy = [
                                $serviceCodeOption
                            ];

                            $mapperIds = $this->getMMByCodes($searchBy);
                            $mappers = $this->getFnMappers($mapperIds);

                            $mapper = null;
                            if (count($mappers)) {
                                $mapper = current($mappers);
                            }

                            if ($mapper && $mapper->getId() && ($sku = $mapper->getSku())) {

                                /** @var Dyna_Catalog_Model_Product $product */
                                $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
                                if ($product && $product->getId()) {
                                    $productName = $product->getName();
                                    $categoryName = $this->getNameOfFirstFamilyCategory($product->getId());
                                    $type = $product->getType();
                                    $productType = array_shift($type);
                                    $maf = $helper->showNewFormattedPriced($product->getMaf(), true);
                                    $productVisibility = $this->catalogHelper->getVisibility(Dyna_Catalog_Model_Product::PRODUCT_VISIBILITY_INVENTORY, $sku, $product);
                                }
                            }

                            $addonId = $serviceCodeOption['Value'];
                            foreach ($componentsAdditionalData as $additional) {
                                // todo: check if fallback names are correctly mapped or provided
                                if ($additional['Code'] == "serviceBillingName"
                                    && $additional['AdditionalValue'] == "/functions/tvCenter/tvCenterBundledConfiguration/tvCenterOptionsList/existing/tvCenterOption[@ID=$addonId]/serviceBillingName"
                                ) {
                                    $fallbackName = $additional['Value'];
                                }

                                if ($additional['Code'] == "startDate"
                                    && $additional['AdditionalValue'] == "/functions/tvCenter/tvCenterBundledConfiguration/tvCenterOptionsList/existing/tvCenterOption[@ID=$addonId]/minimumDurationPeriod/startDate"
                                ) {
                                    $startDate = $additional['Value'];
                                }

                                if ($additional['Code'] == "endDate"
                                    && $additional['AdditionalValue'] == "/functions/tvCenter/tvCenterBundledConfiguration/tvCenterOptionsList/existing/tvCenterOption[@ID=$addonId]/minimumDurationPeriod/endDate"
                                ) {
                                    $endDate = $additional['Value'];
                                }
                            }

                            if ($endDate) {
                                $terminationTo = $this->getFnComponentTerminationTo($componentsAdditionalData, $componentType, $endDate);
                            }
                            $addons[] = [
                                'name' => !empty($productName) ? $productName : $fallbackName,
                                'type' => !empty($productType) ? $productType : Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_ADDON,
                                'start_date' => $startDate,
                                'end_date' => $endDate,
                                'termination_to' => $terminationTo,
                                'category_name' => $categoryName ? $categoryName : 'Uncategorized',
                                'sku' => $sku ?? null,
                                'AdditionalValue' => $addonId ?? null, // used to identify if it is part_of_bundle
                                'maf' => $maf,
                                'product_visibility' => $productVisibility,
                            ];

                            unset($sku, $product, $productName, $productType);
                        }
                    }
                }
                break;
        }

        return $addons;
    }

    /**
     * @param $searchAdditional
     * @return bool|Dyna_Catalog_Model_Product
     */
    protected function getMMProductByCodes($searchAdditional)
    {
        $searchBy = [
            $searchAdditional
        ];

        $mapperIds = $this->getMMByCodes($searchBy);
        $mappers = $this->getFnMappers($mapperIds);

        $mapper = null;
        if (count($mappers)) {
            $mapper = current($mappers);
        }

        if ($mapper && $mapper->getId() && ($sku = $mapper->getSku())) {
            /** @var Dyna_Catalog_Model_Product $product */
            $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
            if ($product->getId()) {
                return $product;
            } else {
                return false;
            }
        }

        return false;
    }

    /**
     * @todo: Check if this is still needed
     * @param $componentsAdditionalData
     * @param $componentType
     * @return null|string
     */
    protected function getFnComponentDiscount($componentsAdditionalData, $componentType)
    {
        $value = null;

        foreach ($componentsAdditionalData as $additional) {
            switch ($componentType) {
                case self::FN_VOICE_COMPONENT_TYPE:
                    if ($additional['Code'] == "conditionServiceName"
                        && $additional['AdditionalValue'] == "/functions/voice/voiceBasisConfiguration/conditionsList/existing/conditionService/conditionServiceName"
                    ) {
                        $productObject = $this->getMMProductByCodes($additional);
                        $value = ($productObject) ? $productObject->getName() : 'N/A';
                    }
                    break;
                case self::FN_INTERNET_COMPONENT_TYPE:
                    if ($additional['Code'] == "conditionServiceName"
                        && $additional['AdditionalValue'] == "/functions/internet/adslInternetConfiguration/conditionsList/existing/conditionService/conditionServiceName"
                    ) {
                        $productObject = $this->getMMProductByCodes($additional);
                        $value = ($productObject) ? $productObject->getName() : 'N/A';
                    }
                    break;
                case self::FN_TV_COMPONENT_TYPE:
                    if ($additional['Code'] == "conditionServiceName"
                        && $additional['AdditionalValue'] == "/functions/tvCenter/tvCenterBundleConfiguration/conditionsList/existing/conditionService/conditionServiceName"
                    ) {
                        $value = trim($additional['Value']);
                    }
                    break;
            }
        }

        return $value;
    }

    /**
     * OMNVFDE-3013
     * Extract the connection type from components
     * @param $componentsAdditionalData
     * @param $componentType
     * @param $contractEndDate
     * @return string|null
     */
    protected function getFnComponentTerminationTo($componentsAdditionalData, $componentType, $contractEndDate)
    {
        $noticePeriod = '';
        $noticePeriodUnit = '';

        foreach ($componentsAdditionalData as $additional) {
            switch ($componentType) {
                case self::FN_VOICE_COMPONENT_TYPE:
                    if ($additional['Code'] == "value"
                        && $additional['AdditionalValue'] == "/functions/voice/voiceBasisConfiguration/noticePeriod/value/existing"
                    ) {
                        $noticePeriod = trim($additional['Value']);
                    } elseif ($additional['Code'] == "unit"
                        && $additional['AdditionalValue'] == "/functions/voice/voiceBasisConfiguration/noticePeriod/unit/existing"
                    ) {
                        $noticePeriodUnit = trim($additional['Value']);
                    }

                    break;
                case self::FN_INTERNET_COMPONENT_TYPE:
                    if ($additional['Code'] == "value"
                        && $additional['AdditionalValue'] == "/functions/internet/adslInternetConfiguration/noticePeriod/value/existing"
                    ) {
                        $noticePeriod = trim($additional['Value']);
                    } elseif ($additional['Code'] == "unit"
                        && $additional['AdditionalValue'] == "/functions/internet/adslInternetConfiguration/noticePeriod/unit/existing"
                    ) {
                        $noticePeriodUnit = trim($additional['Value']);
                    }

                    break;
                case self::FN_TV_COMPONENT_TYPE:
                    if ($additional['Code'] == "value"
                        && $additional['AdditionalValue'] == "/functions/tvCenter/tvCenterBundledConfiguration/noticePeriod/value/existing"
                    ) {
                        $noticePeriod = trim($additional['Value']);
                    } elseif ($additional['Code'] == "unit"
                        && $additional['AdditionalValue'] == "/functions/tvCenter/tvCenterBundledConfiguration/noticePeriod/unit/existing"
                    ) {
                        $noticePeriodUnit = trim($additional['Value']);
                    }

                    break;
                case self::FN_SECURITY_COMPONENT_TYPE:
                    if ($additional['Code'] == "value"
                        && $additional['AdditionalValue'] == "/functions/safetyPackage/safetyPackageConfiguration/noticePeriod/value/existing"
                    ) {
                        $noticePeriod = trim($additional['Value']);
                    } elseif ($additional['Code'] == "unit"
                        && $additional['AdditionalValue'] == "/functions/safetyPackage/safetyPackageConfiguration/noticePeriod/unit/existing"
                    ) {
                        $noticePeriodUnit = trim($additional['Value']);
                    }

                    break;
            }
        }

        if ($contractEndDate && $noticePeriod !== '') {
            $contractEndDate = date("Y-m-d", strtotime($contractEndDate));
            return date("Y-m-d", strtotime($contractEndDate . ' -' . $noticePeriod . ' ' . strtolower($noticePeriodUnit)));
        } else {
            return null;
        }

    }

    /**
     * @param $componentsAdditionalData
     * @return string
     */
    protected function getTvComponentConnectionType($componentsAdditionalData)
    {
        $name = 'N/A';
        $mapperIds = null;
        $uniqueCodes = [];

        foreach ($componentsAdditionalData as $additional) {
            $uniqueCodes[$additional['Code']] = $additional;
        }

        if (isset($uniqueCodes['productCode']) && isset($uniqueCodes['pricingStructureCode'])) {
            $searchBy = [
                $uniqueCodes['productCode'],
                $uniqueCodes['pricingStructureCode']
            ];

            $mapperIds = $this->getMMByCodes($searchBy);
        }

        if ($mapperIds) {
            $mappers = $this->getFnMappers($mapperIds);

            $mapper = null;
            if (count($mappers)) {
                $mapper = current($mappers);
            }

            if ($mapper && $mapper->getId()) {
                /** @var Dyna_MultiMapper_Model_Addon $addonModel */
                /** @var Dyna_MultiMapper_Model_Addon $addon */
                $addonModel = Mage::getModel('dyna_multimapper/addon');
                $addon = $addonModel->getAddonByMapperAndName($mapper->getId(), 'TivanoOptionValue');
                $name = $addon->getAddonAdditionalValue();
            }
        }

        return $name;
    }

    /**
     * OMNVFDE-3013
     * Extract the connection type from components
     * @param $componentsAdditionalData
     * @param $componentType
     * @return string|null
     */
    protected function getFnComponentConnectionType($componentsAdditionalData, $componentType)
    {
        $value = null;

        foreach ($componentsAdditionalData as $additional) {
            switch ($componentType) {
                case self::FN_VOICE_COMPONENT_TYPE:
                    if ($additional['Code'] == "connectionType"
                        && $additional['AdditionalValue'] == "/functions/voice/voiceBasisConfiguration/connectionType/existing"
                    ) {
                        $value = trim($additional['Value']);
                    }
                    break;
            }
        }

        return $this->__("Connection type $value");
    }

    /**
     * OMNVFDE-3013
     * Extract the subscription end date from components
     * @param $componentsAdditionalData
     * @param $componentType
     * @return string|null
     */
    protected function getFnContractEndDate($componentsAdditionalData, $componentType)
    {
        $value = null;

        foreach ($componentsAdditionalData as $additional) {
            switch ($componentType) {
                case self::FN_VOICE_COMPONENT_TYPE:
                    if ($additional['Code'] == "contractExpirationDate"
                        && $additional['AdditionalValue'] == "/functions/voice/voiceBasisConfiguration/contractExpirationDate/existing"
                    ) {
                        $value = trim($additional['Value']);
                    }
                    break;
                case self::FN_INTERNET_COMPONENT_TYPE:
                    if ($additional['Code'] == "contractExpirationDate"
                        && $additional['AdditionalValue'] == "/functions/internet/adslInternetConfiguration/contractExpirationDate/existing"
                    ) {
                        $value = trim($additional['Value']);
                    }
                    break;
                case self::FN_SECURITY_COMPONENT_TYPE:
                    if ($additional['Code'] == "contractExpirationDate"
                        && $additional['AdditionalValue'] == "/functions/safetyPackage/safetyPackageConfiguration/contractExpirationDate/existing"
                    ) {
                        $value = trim($additional['Value']);
                    }
                    break;
                case self::FN_TV_COMPONENT_TYPE:
                    if ($additional['Code'] == "contractExpirationDate"
                        && $additional['AdditionalValue'] == "/functions/tvCenter/tvCenterBundledConfiguration/contractExpirationDate/existing"
                    ) {
                        $value = trim($additional['Value']);
                    }
                    break;
            }
        }

        return $value;
    }

    /**
     * OMNVFDE-3013
     * Extract the subscription start date from components
     * @param $componentsAdditionalData
     * @param $componentType
     * @return string|null
     */
    protected function getFnContractStartDate($componentsAdditionalData, $componentType)
    {
        $value = null;

        foreach ($componentsAdditionalData as $additional) {
            switch ($componentType) {
                case self::FN_VOICE_COMPONENT_TYPE:
                    if ($additional['Code'] == "contractStartDate"
                        && $additional['AdditionalValue'] == "/functions/voice/contractStartDate/existing"
                    ) {
                        $value = trim($additional['Value']);
                    }
                    break;
                case self::FN_INTERNET_COMPONENT_TYPE:
                    if ($additional['Code'] == "contractStartDate"
                        && $additional['AdditionalValue'] == "/functions/internet/contractStartDate/existing"
                    ) {
                        $value = trim($additional['Value']);
                    }
                    break;
                case self::FN_SECURITY_COMPONENT_TYPE:
                    if ($additional['Code'] == "startDate"
                        && $additional['AdditionalValue'] == "/functions/safetyPackage/safetyPackageConfiguration/minimumDurationPeriod/startDate/existing"
                    ) {
                        $value = trim($additional['Value']);
                    }
                    break;
                case self::FN_TV_COMPONENT_TYPE:
                    if ($additional['Code'] == "contractStartDate"
                        && $additional['AdditionalValue'] == "/functions/tvCenter/contractStartDate/existing"
                    ) {
                        $value = trim($additional['Value']);
                    }
                    break;
            }
        }

        return $value;
    }

    /**
     * OMNVFDE-3013
     * Extract the component start date
     * @param $componentsAdditionalData
     * @param $componentType
     * @return string|null
     */
    protected function getFnComponentStartDate($componentsAdditionalData, $componentType)
    {
        $value = null;

        foreach ($componentsAdditionalData as $additional) {
            switch ($componentType) {
                case self::FN_VOICE_COMPONENT_TYPE:
                    if ($additional['Code'] == "startDate"
                        && $additional['AdditionalValue'] == "/functions/voice/voiceBasisConfiguration/minimumDurationPeriod/startDate/existing"
                    ) {
                        $value = trim($additional['Value']);
                    }
                    break;
                case self::FN_INTERNET_COMPONENT_TYPE:
                    if ($additional['Code'] == "contractStartDate"
                        && $additional['AdditionalValue'] == "/functions/internet/contractStartDate/existing"
                    ) {
                        $value = trim($additional['Value']);
                    }
                    break;
                case self::FN_TV_COMPONENT_TYPE:
                    if ($additional['Code'] == "contractStartDate"
                        && $additional['AdditionalValue'] == "/functions/tvCenter/contractStartDate/existing"
                    ) {
                        $value = trim($additional['Value']);
                    }
                    break;
            }
        }

        return $value;
    }

    /**
     * OMNVFDE-3013
     * Determine if there are old products from components
     * @param $componentsAdditionalData
     * @param $componentType
     * @return string|null
     */
    protected function getFnOldProducts($componentsAdditionalData, $componentType)
    {
        $value = false;

        foreach ($componentsAdditionalData as $additional) {
            switch ($componentType) {
                case self::FN_INTERNET_COMPONENT_TYPE:
                    if (($additional['Code'] == "access@ID" && $additional['AdditionalValue'] == "/accesses/dslr")
                        || ($additional['Code'] == "access@IDAdditional" && $additional['AdditionalValue'] == "/accesses/preselect")) {
                        $value = true;
                    }
                    break;
            }
        }

        return $value;
    }

    /**
     * OMNVFDE-3013
     * Extract the component type (voice, internet, hardware, data, ... )
     * @param $componentsAdditionalData
     * @return string|null
     */
    protected function getFnComponentType($componentsAdditionalData)
    {
        $componentType = null;
        foreach ($componentsAdditionalData as $additional) {
            if ($additional['Code'] == "function@ID") {
                $componentType = str_replace('/functions/', '', $additional['AdditionalValue']);
                // if a valid component type is found -> end the search (currently there is no other way to properly identify a component type)
                if (in_array($componentType, self::getFnComponentTypes())) {
                    break;
                }
            }
        }

        return $componentType;
    }

    /**
     * Method used to identify LTE subscriptions package types
     * @param $components
     * @return mixed|null
     */
    protected function getFnAccessType($components)
    {
        $componentType = strtolower(Dyna_Catalog_Model_Type::TYPE_FIXED_DSL);

        foreach ($components[0]['ComponentAdditionalData'] as $additional) {
            if ($additional['Code'] == "access@ID") {
                $foundComponentType = str_replace('/accesses/', '', $additional['AdditionalValue']);
                // if a valid component type is found -> end the search (currently there is no other way to properly identify a component type)
                if (in_array($foundComponentType, self::getFnAccessTypes())) {
                    $componentType = $foundComponentType;
                    break;
                }
            }
        }

        return $componentType;
    }

    /**
     * Method used to identify TechnicalServiceId that is used for TASI in CheckServiceAbilityClient
     * @param $components
     * @return mixed|null
     */
    protected function getFnTechnicalServiceId($components)
    {
        $value = null;

        foreach ($components[0]['ComponentAdditionalData'] as $additional) {
            if ($additional['Code'] == "technicalServiceId") {
                return $additional['Value'];
            }
        }

        return $value;
    }

    /**
     * DSL reverse multimapper logic
     * Described in OMNVFDE-2518
     * @param $componentsAdditionalData
     * @return bool|mixed
     */
    protected function getMMByNodes($componentsAdditionalData)
    {
        /*
         * todo: ?what happens if the $componentsAdditionalData contains multiple nodes with the same codes; currently the last one in used
         */
        foreach ($componentsAdditionalData as $additional) {
            $uniqueCodes[$additional['Code']] = $additional;
        }

        $mapperIds = [];
        $searchNodes = [];

        // ****** Tariff Tab Mapping ***** //
        if (isset($uniqueCodes['pkatPacketId'])) {
            $this->reverseDebug('tariff');
            // Multimapper_SalesPackage
            $searchBy = [
                $uniqueCodes['pkatPacketId']
            ];

            $mapperIds = $this->getMMByCodes($searchBy);
        }

        if (!$mapperIds) {
            // ****** Options Tab Mapping ***** //
            // ****** Hardware Tab Mapping ***** //
            $componentType = $this->getFnComponentType($componentsAdditionalData);
            $this->reverseDebug($componentType);
            switch ($componentType) {
                case self::FN_INTERNET_COMPONENT_TYPE:
                    //1. Internet bandwidth
                    $searchNodes[0] = ['pricingStructureCode' => [], 'DSLBandwidth' => []];
                    break;

                case self::FN_VOICE_COMPONENT_TYPE:
                    //2. Voice access
                    $searchNodes[0] = ['mainAccessServiceCode' => [], 'pricingStructureCode' => []];
                    //3. Voice tariff options are mapped as addons in getFnComponentAddon
                    break;

                case self::FN_TV_COMPONENT_TYPE:
                    $searchNodes[0] = ['pricingStructureCode' => []];
                    //TV options are mapped as addons in getFnComponentAddon
                    break;

                case self::FN_SECURITY_COMPONENT_TYPE:
                    //6. Safety service
                    $searchNodes[0] = ['productCode' => [], 'pricingStructureCode' => []];
                    break;

                case self::FN_HARDWARE_COMPONENT_TYPE:
                    // ****** Hardware Tab Mapping ***** //
                    $searchNodes[0] = ['articleNumber' => [], 'subventionCode' => []];
                    break;
            }

            foreach ($searchNodes as $searchBy) {
                $foundKey = 0;
                foreach ($componentsAdditionalData as $additional) {
                    $additionalCode = $additional['Code'];
                    if (isset($searchBy[$additionalCode]) && empty($searchBy[$additionalCode])) {
                        $searchBy[$additionalCode] = $additional;
                        $foundKey++;
                    }
                }

                if ($foundKey == count($searchBy)) {
                    $this->reverseDebug('SEARCH BY ' . var_export($searchBy, true));

                    $foundMappers = $this->getMMByCodes($searchBy);
                    $mapperIds = array_merge($mapperIds, $foundMappers);

                    if ($mapperIds) {
                        $this->reverseDebug('FOUND MAPPER: ' . var_export($foundMappers, true));
                    } else {
                        $this->reverseDebug('NO MAPPER FOUND');
                    }
                } else {
                    $this->reverseDebug('NOT FOUND KEYS ' . var_export($searchBy, true));
                }
            }

            // ****** Promotion Tab Mapping ***** //
            $promoSearchNodes[0] = ['conditionServiceName' => []];

            foreach ($promoSearchNodes as $searchBy) {
                $foundKey = 0;
                foreach ($componentsAdditionalData as $additional) {
                    $additionalCode = $additional['Code'];
                    if (isset($searchBy[$additionalCode]) && empty($searchBy[$additionalCode])) {
                        $searchBy[$additionalCode] = $additional;
                        $foundKey++;
                    }
                }

                if ($foundKey == count($searchBy)) {
                    $this->reverseDebug('PROMO SEARCH BY ' . var_export($searchBy, true));

                    $foundMappers = $this->getMMByCodes($searchBy);
                    $mapperIds = array_merge($mapperIds, $foundMappers);

                    if ($foundMappers) {
                        $this->reverseDebug('PROMO  FOUND MAPPER: ' . var_export($foundMappers, true));
                    } else {
                        $this->reverseDebug('NO PROMO MAPPER FOUND');
                    }
                } else {
                    $this->reverseDebug('NOT FOUND PROMO KEYS: ' . var_export($searchBy, true));
                }
            }
        }

        // Directory display either on option tab or like the access number
        // todo: Currently no IN Mapping in Multimapper

        //Blocking service
        // todo: there is no IN mapping for Blocking Services

        $this->reverseDebug('FOUND MAPPERS: ' . var_export(array_unique($mapperIds), true));

        return array_unique($mapperIds);
    }

    /**
     * Method used only in debug mode
     * @param $message
     */
    private function reverseDebug($message)
    {
        if ($this->revMMDebug) {
            Mage::log($message, null, 'reverseMultiMapperDebug.log');
        }
    }

    /**
     * Search Mapper entry by specific components
     * @param $components
     * @return bool|mixed Mapper ID
     */
    protected function getMMByCodes($components)
    {
        /** @var Dyna_MultiMapper_Helper_Data $h */
        $h = Mage::helper('dyna_multimapper');

        $mapperIds = null;

        foreach ($components as $data) {
            $dbResult = $h->getMapperIdByFnData($data['Code'], $data['Value'], $data['AdditionalValue']);
            if ($mapperIds === null) {
                $mapperIds = $dbResult;
            } else {
                $mapperIds = array_intersect($mapperIds, $dbResult);
            }
        }

        // if none mappers are found we continue
        if (count($mapperIds) == 0) {
            return [];
        } else {
            // return all mappers
            return array_unique($mapperIds);
        }
    }

    /**
     * @param $subscription
     * @param $reverseMapperProducts
     * @param $productSkusEligibleForBundle
     * @param $accountType
     * @return mixed
     */
    protected function determineSubscriptionBundleMarks($subscription, $reverseMapperProducts, $productSkusEligibleForBundle, $accountType)
    {
        $subs['bundle_class'] = '';
        $subs['bundle_tooltip'] = $this->__("Not Bundle Eligible");
        $subs['part_of_bundle'] = false;
        $subs['bundle_eligible'] = false;
        $subs['bundle_product_ids'] = [];

        $existingBundleComponents = [];
        foreach ($reverseMapperProducts as $mappedProduct) {
            if (isset($mappedProduct['components'])) {
                foreach ($mappedProduct['components'] as $component) {
                    switch ($accountType) {
                        case Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN:
                            // FN Addons
                            if (isset($component['addons']) && $component['addons']) {
                                foreach ($component['addons'] as $addon) {
                                    if (in_array($addon['AdditionalValue'],
                                            Dyna_Customer_Model_Client_RetrieveCustomerInfo::getPartOfBundleSocs())
                                        && !isset($existingBundleComponents[$addon['AdditionalValue']])) {
                                        $existingBundleComponents[$addon['AdditionalValue']] = $addon['sku'];
                                    }
                                }
                            }
                            // FN Discounts
                            if (isset($component['discounts']) && $component['discounts']) {
                                foreach ($component['discounts'] as $addon) {
                                    if (in_array($addon['AdditionalValue'],
                                            Dyna_Customer_Model_Client_RetrieveCustomerInfo::getPartOfBundleSocs())
                                        && !isset($existingBundleComponents[$addon['AdditionalValue']])) {
                                        $existingBundleComponents[$addon['AdditionalValue']] = $addon['sku'];
                                    }
                                }
                            }
                            break;
                        case Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS:
                            if (in_array($component['soc'],
                                    Dyna_Customer_Model_Client_RetrieveCustomerInfo::getPartOfBundleSocs())
                                && !isset($existingBundleComponents[$component['soc']])) {
                                $existingBundleComponents[$component['soc']] = $component['sku'];
                            }
                            break;
                        case Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD:
                            if (in_array($component['legacy_code'],
                                    Dyna_Customer_Model_Client_RetrieveCustomerInfo::getPartOfBundleSocs())
                                && !isset($existingBundleComponents[$component['legacy_code']])) {
                                $existingBundleComponents[$component['legacy_code']] = $component['sku'];
                            }
                            break;
                    }
                }
            }
        }

        if (isset($subscription['products'])) {
            foreach ($subscription['products'] as $product) {
                $components = $product['components'] ?? [];
                foreach ($components as $component) {
                    if (isset($component['ComponentAdditionalData'])) {
                        /**
                         * OMNVFDE-1843
                         * a.installed base product is part of bundle if the SOC from ComponentAddtionalData.Value (for FN) is one of the provided SOC-s
                         */
                        foreach ($component['ComponentAdditionalData'] as $additionalData) {
                            if ($additionalData['Code'] == 'serviceCode'
                                && in_array($additionalData['Value'], Dyna_Customer_Model_Client_RetrieveCustomerInfo::getPartOfBundleSocs())
                            ) {
                                if (isset($existingBundleComponents[$additionalData['Value']])) {
                                    /** @var Dyna_Catalog_Model_Product $product */
                                    $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $existingBundleComponents[$additionalData['Value']]);

                                    $subs['bundle_class'] = 'part-of-bundle';
                                    $subs['bundle_tooltip'] = $this->__("Part of Bundle");
                                    $subs['part_of_bundle'] = true;

                                    if ($product && $product->getId()) {
                                        $subs['bundle_product_ids'][] = $product->getId();
                                    }
                                }
                            }
                        }
                    }

                    /**
                     * OMNVFDE-1843
                     * a.installed base product is part of bundle if AgreementSoc (for kias) is one of the provided SOC-s
                     */
                    if (isset($component['Agreement']) && isset($component['Agreement']['AgreementSOC']) &&
                        in_array($component['Agreement']['AgreementSOC'], Dyna_Customer_Model_Client_RetrieveCustomerInfo::getPartOfBundleSocs())
                    ) {
                        /** @var Dyna_Catalog_Model_Product $product */
                        $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $existingBundleComponents[$component['Agreement']['AgreementSOC']]);
                        // Mobile scenario
                        $subs['bundle_class'] = 'part-of-bundle';
                        $subs['bundle_tooltip'] = $this->__("Part of Bundle");
                        $subs['part_of_bundle'] = true;

                        if ($product && $product->getId()) {
                            $subs['bundle_product_ids'][] = $product->getId();
                        }
                    }

                    /**
                     * OMNVFDE-1843
                     * a.installed base product is part of bundle if the SOC from legacyID(for kd) is one of the provided SOC-s
                     */
                    if (isset($component['LegacyID']) && in_array($component['LegacyID'], Dyna_Customer_Model_Client_RetrieveCustomerInfo::getPartOfBundleSocs())) {
                        // KD scenario
                        $reverseMapperProducts['part_of_bundle'] = true;
                    }
                }
            }
        }
        if ($reverseMapperProducts) {
            if ($accountType == Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN) {
                /**
                 * OMNVFDE-3349 - FN
                 * b. if is not part of bundle we will try to find if is eligible for bundle: the product sku must be part of the bundle categories provided
                 */
                foreach ($reverseMapperProducts as $product) {
                    if (!$subs['part_of_bundle'] && isset($product['sku']) && $product['sku']) {
                        if (in_array($product['sku'], $productSkusEligibleForBundle)) {
                            $subs['bundle_class'] = 'eligible-for-bundle';
                            $subs['bundle_tooltip'] = $this->__("Bundle Eligible");
                            $subs['bundle_eligible'] = true;
                        }
                    }
                }
            } else {
                /**
                 * OMNVFDE-1843 - Mobile
                 * b. if is not part of bundle we will try to find if is eligible for bundle: the product sku must be part of the bundle categories provided
                 */
                foreach ($reverseMapperProducts as $product) {
                    foreach ($product['components'] as $component) {
                        if (!$subs['bundle_class'] && isset($component['sku']) && $component['sku']) {
                            if (in_array($component['sku'], $productSkusEligibleForBundle)) {
                                $subs['bundle_class'] = 'eligible-for-bundle';
                                $subs['bundle_tooltip'] = $this->__("Bundle Eligible");
                                $subs['bundle_eligible'] = true;
                            }
                        }
                    }
                }
            }
        }

        return $subs;
    }

    /**
     * @param $data
     */
    protected function setOnSessionIfCustomerIsBundleEligible($data)
    {
        foreach ($data as $productType) {
            if (isset($productType['contracts']) && count($productType['contracts'])) {
                foreach ($productType['contracts'] as $contract) {
                    if (isset($contract['subscriptions']) && count($contract['subscriptions'])) {
                        foreach ($contract['subscriptions'] as $subscription) {
                            if (isset($subscription['bundle_eligible']) && $subscription['bundle_eligible']) {
                                Mage::getSingleton('customer/session')->setCustomerIsBundleEligible(true);
                                return;
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Returns the total amount of orders for the current logged in customer
     *
     * @param Dyna_Customer_Model_Customer|null $customer
     * @return int|string
     */
    public function getOrdersCount(Dyna_Customer_Model_Customer $customer = null)
    {
        $totalOrdersCount = 0;
        $data = $this->getCustomerOrders($customer);

        $orderCountFlag = true;
        if (isset($data['nonOsfOrders']['error']) && $data['nonOsfOrders']['error']) {
            $orderCountFlag = false;
        }

        if ($orderCountFlag) {
            unset($data['nonOsfOrders']['error']);
            unset($data['nonOsfOrders']['message']);

            //external orders
            $totalOrdersCount += count($data['nonOsfOrders'] ?? array());
        }

        //local database orders
        if (!(isset($data['dbOrders']['error']) && $data['dbOrders']['error'])) {
            $totalOrdersCount += count($data['dbOrders'] ?? array());
        }

        return $totalOrdersCount;
    }

    /**
     * Returns the total amount of saved carts for the current logged in customer or the passed customer
     *
     * @param Dyna_Customer_Model_Customer|null $customer
     * @return int
     */
    public function getCartsCount(Dyna_Customer_Model_Customer $customer = null)
    {
        return count($this->parseCartsContent($customer)['shoppingCarts']);
    }

    public function getStackPermissionMessages()
    {
        $data = $this->parseProductsResults();
        $customerSession = Mage::getSingleton('dyna_customer/session');
        $agent = $customerSession->getAgent();

        $response = [
            'show_kias' => (!$agent->isGranted('ALLOW_MOB')) ? ((isset($data[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS])) ? true : false) : false,
            'show_kd' => (!$agent->isGranted('ALLOW_CAB')) ? ((isset($data[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD])) ? true : false) : false,
            'show_fn' => (!$agent->isGranted('ALLOW_DSL')) ? ((empty($data[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN])) ? false : true) : false,
        ];

        return $response;
    }

    /**
     * Returns the total amount of products for the current logged in customer
     *
     * @return int
     */
    public function getProductsCount()
    {
        $productsCount = 0;
        $data = $this->getCustomerProducts();
        $types = array_keys($data);
        foreach ($types as $type) {
            foreach ($data[$type] as $customer) {
                foreach ($customer['contracts'] as $contract) {
                    if ($type == Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN) {
                        $productsCount += count($contract['products']);
                    } else {
                        foreach ($contract['subscriptions'] as $subscription) {
                            if (isset($subscription['products']) && count($subscription['products'])) {
                                if ($type == Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD) {
                                    foreach ($subscription['products'] as $product) {
                                        $productsCount += count($product);
                                    }
                                } else {
                                    $productsCount += count($subscription['products']);
                                }
                            }
                        }
                    }
                }
            }
        }
        return $productsCount;
    }

    /**
     * Get order line statuses
     * @return array
     */
    public function getOrderLineStatuses()
    {
        return array(
            'Completed-Success' => $this::COMPLETED_SUCCESS,
            'Completed-Failed' => $this::COMPLETED_FAILED,
            'Completed-OrderRejectedByDSLShop' => $this::COMPLETED_FAILED,
            'Completed-RejectedByFRIDA' => $this::COMPLETED_FAILED,
            'Completed-RejectedByRiskEditor' => $this::COMPLETED_FAILED,
            'InProgress–New' => $this::PENDING,
            'InProgress-SubmittedToBackend' => $this::PENDING,
            'InProgress-FulfillmentStarted' => $this::PENDING,
            'InProgress-Failed-InternalTicket' => $this::PENDING,
            'InProgress-Failed-OpenExternalTicket' => $this::PENDING,
            'InProgress-OrderConfirmationByDSLShop' => $this::PENDING,
            'InProgress-HWFulfilmentReady' => $this::PENDING,
            'InProgress-RiskScoreByFRIDA' => $this::PENDING,
            'InProgress-ApprovedByFRIDA' => $this::PENDING,
            'InProgress-ApprovedByRiskEditor' => $this::PENDING,
            'OnHold-ByBE' => $this::PENDING,
            'OnHold–PendingDSLShop' => $this::PENDING,
            'OnHold–PendingOrchestration' => $this::PENDING,
            'OnHold–PendingHWFulfillment' => $this::PENDING,
            'OnHold-RiskAssessment' => $this::PENDING
        );
    }

    /**
     * Get order line status
     * @param $status
     * @return int|string
     */
    public function getOrderLineStatus($status)
    {
        $updatedStatus = '';
        foreach ($this->getOrderLineStatuses() as $key => $value) {
            if ($status == $key) {
                $updatedStatus = $value;
                break;
            }
        }
        return $updatedStatus;
    }

    /**
     * Get order status based on packages statuses
     * @param $packagesStatuses
     * @return string
     */
    public function getOrderStatus($packagesStatuses)
    {
        $pendingOccurence = strpos($packagesStatuses, $this::PENDING);
        $completedSuccessOccurence = strpos($packagesStatuses, $this::COMPLETED_SUCCESS);
        $completedFailedOccurence = strpos($packagesStatuses, $this::COMPLETED_FAILED);
        if ($pendingOccurence !== false) {
            $orderStatus = $this::PENDING;
        } elseif (($completedSuccessOccurence !== false) && ($completedFailedOccurence !== false) && ($pendingOccurence === false)) {
            $orderStatus = $this::PARTIAL_COMPLETED;
        } elseif (($completedSuccessOccurence !== false) && ($completedFailedOccurence === false) && ($pendingOccurence === false)) {
            $orderStatus = $this::COMPLETED_SUCCESS;
        } elseif (($completedSuccessOccurence === false) && ($completedFailedOccurence !== false) && ($pendingOccurence === false)) {
            $orderStatus = $this::COMPLETED_FAILED;
        }
        return $orderStatus;
    }

    /**
     * Loops through customer subscriptions and checks if
     * they are eligible for Red+ bundling to enable the
     * add member button on frontend
     * @param $customers
     * @return mixed
     */
    public function updateKiasSubscriptionsWithRedplusBundles($customers)
    {
        /** @var Dyna_Bundles_Helper_Data $bundlesHelper */
        $bundlesHelper = Mage::helper('bundles');
        $eligibleBundles = $bundlesHelper->getFrontendEligibleBundles(true);
        $activeBundles = $bundlesHelper->getActiveBundles();
        $bundlesHelper->filterBundlesBySubscriptionStatus($eligibleBundles);
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();

        // Build a list of owner subscriptions per customer number
        $ownerProductIds = [];
        foreach ($customers as &$customer) {
            foreach ($customer['contracts'] as $contract) {
                $customerNumber = $contract['customer_number'];
                $productId = $contract['subscriptions'][0]['product_id'];
                foreach ($eligibleBundles as $bundle) {
                    if ($bundle['redPlusBundle']) {
                        foreach ($bundle['targetedSubscriptions'] as $targetedSubscription) {
                            if ($customerNumber == $targetedSubscription['customerNumber']
                                && $productId == $targetedSubscription['productId']) {
                                $ownerProductIds[$customerNumber][] = $productId;
                            }
                        }
                    }
                }
            }
        }
        foreach ($customers as &$customer) {
            foreach ($customer['contracts'] as &$contract) {
                $customerNumber = $contract['customer_number'];
                $productId = $contract['subscriptions'][0]['product_id'];
                $subscription = &$contract['subscriptions'][0];

                if ($subscription['contract_type'] == Dyna_Customer_Helper_Customer::MOBILE_CONTRACT_TYPE_POSTPAID) {
                    $addMemberToExistingGroup = isset($subscription['sharing_group_details'])
                        && $subscription['sharing_group_details']['member_type'] == Dyna_Customer_Helper_Customer::SUBSCRIPTION_TYPE_OWNER
                        && $subscription['product_status'] != Dyna_Customer_Helper_Customer::MOBILE_CONTRACT_PRODUCT_STATUS_R
                        && $subscription['sharing_group_details']['status'] != Dyna_Customer_Helper_Customer::MOBILE_CONTRACT_PRODUCT_STATUS_R;

                    $allowCreateNewGroup = !isset($subscription['sharing_group_details'])
                        && $subscription['product_status'] != Dyna_Customer_Helper_Customer::MOBILE_CONTRACT_PRODUCT_STATUS_R;

                    $subscription['allow_add_member'] = $addMemberToExistingGroup;
                    $subscription['allow_create_new_group'] = $allowCreateNewGroup;

                    foreach ($activeBundles as $activeBundle) {
                        if ($activeBundle['redPlusBundle']) {
                            $subscription['ib_package_exists'] = $quote->getGhostPackageForIBSubscription($customerNumber, $productId) != null;
                            $subscription['redplus_packages_in_cart'] = $quote->getNumberOfChildPackagesForIBSubscription($activeBundle['bundleId'], $customerNumber, $productId);
                        }
                    }

                    foreach ($eligibleBundles as $bundle) {
                        if ($bundle['redPlusBundle']) {
                            foreach ($bundle['targetedSubscriptions'] as $targetSubscription) {
                                if ($customerNumber === $targetSubscription['customerNumber']
                                    && $productId === $targetSubscription['productId']) {
                                    $subscription['redplus_bundle_eligible'] = true;
                                    $subscription['redplus_bundle_id'] = $bundle['bundleId'];
                                    $subscription['redplus_packages_in_cart'] = $quote->getNumberOfChildPackagesForIBSubscription($bundle['bundleId'], $targetSubscription['customerNumber'], $targetSubscription['productId']);
                                    // get the first eligible bundle

                                    break;
                                } elseif ($customerNumber === $targetSubscription['customerNumber']) {
                                    $subscription['redplus_packages_in_cart'] = $quote->getNumberOfChildPackagesForIBSubscription($bundle['bundleId'], $targetSubscription['customerNumber'], $targetSubscription['productId']);
                                    if ($quote->getGhostPackageForIBSubscription($targetSubscription['customerNumber'], $targetSubscription['productId']) !== null) {
                                        $subscription['disableButtons']['convert_to_member']['class'] = $subscription['disableButtons']['convert_to_member']['class'] ?? false;
                                    }
                                }
                            }
                        }
                    }

                    $activeBundleExists = true;
                    if (!isset($subscription['redplus_bundle_eligible'])) {
                        foreach ($activeBundles as $activeBundle) {
                            if ($activeBundle['redPlusBundle']) {
                                foreach ($activeBundle['packages'] as $activeBundledPackage) {
                                    if (
                                        $activeBundledPackage['targetedSubscriptions']['customerNumber'] != null
                                        && $activeBundledPackage['targetedSubscriptions']['productId'] != null
                                        && $activeBundledPackage['targetedSubscriptions']['customerNumber'] == $customerNumber
                                        && $activeBundledPackage['targetedSubscriptions']['productId'] == $productId
                                    ) {
                                        $activeBundleExists = false;
                                        break;
                                    }
                                }
                            }
                        }
                    }

                    /**
                     * The current (iterated) subscription can be converted to member if ANOTHER owner subscription
                     * is found under the same BAN ($customerNumber)
                     */
                    $eligibleBundleForConvertToMember = false;
                    if (isset($ownerProductIds[$customerNumber])) {
                        if (count($ownerProductIds[$customerNumber]) == 1) {
                            if (!in_array($productId, $ownerProductIds[$customerNumber])) {
                                $eligibleBundleForConvertToMember = true;
                            }
                        } elseif (count($ownerProductIds[$customerNumber] > 1)) {
                            $eligibleBundleForConvertToMember = true;
                        }
                    }
                    if (!$eligibleBundleForConvertToMember) {
                        foreach ($eligibleBundles as $bundle) {
                            if ($bundle['redPlusBundle']) {
                                foreach ($bundle['targetedPackagesInCart'] as $targetPackage) {
                                    $package = $quote->getPackageById($targetPackage['packageId']);
                                    if ($package->getParentAccountNumber() == $customerNumber) {
                                        $eligibleBundleForConvertToMember = true;
                                    }
                                }
                            }
                        }
                    }
                    foreach ($eligibleBundles as $bundle) {
                        if ($bundle['redPlusBundle']) {
                            if ($quote->getNumberOfChildPackagesForIBSubscription($bundle['bundleId'], $customerNumber, $productId)) {
                                // the current subscription is already a cart owner, can't be converted to member
                                $eligibleBundleForConvertToMember = false;
                            }
                        }
                    }

                    if (!$eligibleBundleForConvertToMember) {
                        $subscription['disableButtons']['convert_to_member']['class'] = true;
                    }
                    $subscription['allow_add_member'] = $addMemberToExistingGroup && $activeBundleExists;
                    $subscription['allow_create_new_group'] = $allowCreateNewGroup && $activeBundleExists;
                } elseif ($subscription['contract_type'] == Dyna_Customer_Helper_Customer::MOBILE_CONTRACT_TYPE_PREPAID) {
                    //check to see if convert_to_member button is not already disabled
                    $countEligibleBundles = 0;
                    if (isset($subscription['disableButtons']['convert_to_member']) && !$subscription['disableButtons']['convert_to_member']['class'] || !isset($subscription['disableButtons']['convert_to_member'])) {
                        foreach ($eligibleBundles as $bundle) {
                            if ($bundle['redPlusBundle']) {
                                if (!empty($bundle['targetedPackagesInCart'])) {
                                    $countEligibleBundles++;
                                } elseif (!empty($bundle['targetedSubscriptions'])) {
                                    /**
                                     * When counting eligible bundles with targeted subscriptions, only enable convert
                                     * to member if we find at least one subscription that is NOT reserved
                                     */
                                    foreach ($bundle['targetedSubscriptions'] as $targetedSubscription) {
                                        if ($targetedSubscription['subscriptionProductStatus'] != Dyna_Customer_Helper_Customer::MOBILE_CONTRACT_PRODUCT_STATUS_R) {
                                            $countEligibleBundles++;
                                            break;
                                        }
                                    }
                                }
                                /** @var Dyna_Bundles_Model_BundleRule $bundleModel */
                                $bundleModel = Mage::getModel("dyna_bundles/bundleRule")->load($bundle['bundleId']);
                                //check if bundle has process context for red+ convert to member action
                                if ($bundleModel->hasProcessContext(Dyna_Catalog_Model_ProcessContext::MIGCOC)) {
                                    $subscription['disableButtons']['convert_to_member']['class'] = false;
                                }
                            }
                        }
                        //if we don't find any eligible bundle with targetedSubscriptions or targetedPackagesInCart we disable the button
                        if ($countEligibleBundles == 0) {
                            $subscription['disableButtons']['convert_to_member']['class'] = true;
                        }
                    }

                    // block the user from using Convert to member or Pre-Post if the package is already modified
                    if ($quote->getGhostPackageForIBSubscription($customerNumber, $productId) !== null) {
                        $subscription['disableButtons']['convert_to_member']['class'] = true;
                        $subscription['disableButtons']['pre_post']['class'] = true;
                    }

                    // VFDED1W3S-1912 if the tariff cannot be mapped => the button "pre-post" should be disabled
                    if (!$productId) {
                        $subscription['disableButtons']['pre_post']['class'] = true;
                    }
                }
            }
        }

        return $customers;
    }

    /**
     * @return Dyna_Cache_Model_Cache
     */
    protected
    function getCache()
    {
        if (!$this->_cache) {
            $this->_cache = Mage::getSingleton('dyna_cache/cache');
        }

        return $this->_cache;
    }

    /**
     * Sort Kias subscriptions by sharing group id in order to determine the groups which don't have an owner
     * @param $parsedCustomerProducts
     */
    protected function sortKiasBySharingGroupId(&$parsedCustomerProducts)
    {
        $sortedKiasContracts = array();
        $sharingGroupContracts = array();

        $this->getKiasBySharingGroupId($parsedCustomerProducts, $sharingGroupContracts);

        $lastCustomerId = '';
        $customerContractsCounter = 0;
        foreach ($sharingGroupContracts as $sharingGroupId => $sharingGroupDetails) {
            $counter = 0;
            foreach ($sharingGroupDetails['contracts'] as $contract) {
                $customerContractsCounter = $contract['customerId'] != $lastCustomerId ? 0 : $customerContractsCounter;
                $sortedKiasContracts[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS][$contract['customerId']]['contracts'][$customerContractsCounter] =
                    $parsedCustomerProducts[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS][$contract['customerId']]['contracts'][$contract['contractId']];
                // if no owner on the current group, set a flag per subscription which will trigger an error block on the template
                if (isset($sharingGroupDetails['ownership']) && (is_array($sharingGroupDetails['ownership']) &&
                        !in_array('O', $sharingGroupDetails['ownership'])) && ($sharingGroupId != 'no_group')
                ) {
                    if ($counter == 0) {
                        $sortedKiasContracts[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS][$contract['customerId']]['contracts'][$customerContractsCounter]['subscriptions'][0]['sharing_group_details']['no_owner'] = true;
                    }
                }
                $lastCustomerId = $contract['customerId'];
                $counter++;
                $customerContractsCounter++;
            }
        }
        if (isset($sortedKiasContracts[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS])) {
            $parsedCustomerProducts[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS] = $sortedKiasContracts[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS];
        }
    }

    /**
     * Get Kias by sharing group id
     * @param $parsedCustomerProducts
     * @param $sharingGroupContracts
     */
    protected function getKiasBySharingGroupId($parsedCustomerProducts, &$sharingGroupContracts)
    {
        foreach ($parsedCustomerProducts as $stack => $customer) {
            if ($stack == Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS) {
                foreach ($customer as $customerId => $customerData) {
                    foreach ($customerData['contracts'] as $contractId => $contract) {
                        $this->sortContractsBySharingGroupId($contract, $contractId, $sharingGroupContracts, $customerId);
                    }
                }
            }
        }
    }

    /**
     * Sort contracts by sharing group id
     * @param $contract
     * @param $contractId
     * @param $sharingGroupContracts
     * @param $customerId
     */
    protected function sortContractsBySharingGroupId($contract, $contractId, &$sharingGroupContracts, $customerId)
    {
        $sharingGroupDetails = $contract['subscriptions'][0]['sharing_group_details'];
        if (is_array($sharingGroupDetails)) {
            $sharingGroupContracts[$sharingGroupDetails['id']]['contracts'][] = array(
                'customerId' => $customerId,
                'contractId' => $contractId
            );
            $sharingGroupContracts[$sharingGroupDetails['id']]['ownership'][] = $sharingGroupDetails['member_type'];
        } else {
            $sharingGroupContracts['no_group']['contracts'][] = array(
                'customerId' => $customerId,
                'contractId' => $contractId
            );
        }
    }

    /**
     * Returns the display status text for product labels based on the product status
     *
     * @param $status
     * @return null|string
     */
    public function getProductStatusDisplay($status)
    {
        if (!isset($status)) {
            return null;
        }

        $displayStatus = null;

        switch ($status) {
            case Dyna_Customer_Helper_Customer::MOBILE_CONTRACT_PRODUCT_STATUS_A:
                $displayStatus = Dyna_Customer_Helper_Customer::MOBILE_PRODUCT_STATUS_ACTIVE;
                break;
            case Dyna_Customer_Helper_Customer::MOBILE_CONTRACT_PRODUCT_STATUS_S:
                $displayStatus = Dyna_Customer_Helper_Customer::MOBILE_PRODUCT_STATUS_SUSPENDED;
                break;
            case Dyna_Customer_Helper_Customer::MOBILE_CONTRACT_PRODUCT_STATUS_R:
                $displayStatus = Dyna_Customer_Helper_Customer::MOBILE_PRODUCT_STATUS_RESERVED;
                break;
            case Dyna_Customer_Helper_Customer::MOBILE_CONTRACT_PRODUCT_STATUS_C:
                $displayStatus = Dyna_Customer_Helper_Customer::MOBILE_PRODUCT_STATUS_CANCELLED;
                break;
            default:
                $displayStatus = $status;
                break;
        }

        return $displayStatus;
    }

    /**
     * Return Line Of Business (for pending orders) for package type
     * @param string $packageType
     * @return null|string
     */
    public function getLineOfBusinessForPackageType($packageType)
    {
        switch (strtolower($packageType)) {
            case strtolower(Dyna_Catalog_Model_Type::TYPE_FIXED_DSL):
            case strtolower(Dyna_Catalog_Model_Type::TYPE_LTE):
                return Dyna_Customer_Model_Client_RetrieveCustomerInfo::NON_OSF_ORDER_LINE_OF_BUSINESS_DSL;
            case strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_INDEPENDENT):
            case strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_TV):
            case strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_INTERNET_PHONE):
                return Dyna_Customer_Model_Client_RetrieveCustomerInfo::NON_OSF_ORDER_LINE_OF_BUSINESS_CABLE;
            case strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE):
            case strtolower(Dyna_Catalog_Model_Type::TYPE_PREPAID):
                return Dyna_Customer_Model_Client_RetrieveCustomerInfo::NON_OSF_ORDER_LINE_OF_BUSINESS_MOBILE;
            default :
                return null;
        }
    }
}
