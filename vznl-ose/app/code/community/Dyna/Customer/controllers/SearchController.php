<?php

/**
 * Class Dyna_Customer_SearchController
 */
class Dyna_Customer_SearchController extends Mage_Core_Controller_Front_Action
{
    const MAX_SEARCH_RESULTS = 40;
    const LIMIT_PATH = 'vodafone_customer/search/limit';
    // accepted search fields
    /** @var Dyna_Customer_Model_Session|null */
    private $storage = null;
    private $acceptedFields = array('telephone_number', 'first_name', 'last_name', 'birthday', 'email');
    /** @var Dyna_Cache_Model_Cache */
    private $_attributeIds = array();
    /** @var Dyna_Cache_Model_Cache */
    private $_cache = null;
    /**
     * @var $searchHelper Dyna_Customer_Helper_Search
     */
    protected $searchHelper;
    protected $allCustomers = [];
    protected $legacySearch = false;
    protected $exactSearch = false;
    private $_limit = null;

    /*
     * Get the customer results filtered:
     * - from legacy service and from local db
     * @IsGranted({"permissions":["SEARCH_CUSTOMER"]})
     */
    public function getCustomerResultsLegacyAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }
        $this->legacySearch = true;
        $this->searchCustomer();
    }

    public function getCustomerResultsExactAction()
    {
        $this->exactSearch = true;
        $this->searchCustomer();
    }

    protected function searchCustomer()
    {
        if ($postData = $this->getRequest()->getPost()) {
            try {
                $this->searchHelper = Mage::helper('dyna_customer/search');
                $this->allCustomers = [];

                // parse and validate the filters
                if ($this->legacySearch) {
                    $response = $this->searchHelper->parseCustomerSearchLegacyParams($postData);
                }
                else {
                    $response = $this->searchHelper->parseCustomerSearchParams($postData);
                }

                // if there is a validation error or no parameters are given => skip search and display the error
                if (!empty($response['error'])) {
                    $this->setJsonResponse($response);
                    return;
                }

                if ($this->legacySearch) {
                    $this->legacySearch = $response['params']['legacy_type'];
                }
                $response = $this->getDbAndServiceCustomers($response['params']);
                $this->setJsonResponse($response);

                return;
            } catch (\Exception $e) {
                $result = [
                    "error" => true,
                    "message" => $e->getMessage()
                ];

                $this->setJsonResponse($result);
            }
        }
    }

    /**
     * @param $result
     */
    protected function setJsonResponse($result)
    {
        $this->getResponse()->setHeader('Content-Type', 'application/json');
        $this->getResponse()->setBody(json_encode($result));
        $this->getResponse()->setHttpResponseCode(200);
    }

    protected function getDbAndServiceCustomers($params)
    {
        $allCustomersCounter = 0;
        $serviceCustomersResponse = $this->getFilteredCustomersFromService($params);
        $localDbCustomersResponse = $this->getCustomerResultsFromDbCounter($params);
        if (isset($serviceCustomersResponse['customers'])) {
            $allCustomersCounter = $serviceCustomersResponse['countUnlinkedCustomers'];
        }
        if ($localDbCustomersResponse > 0) {
            $allCustomersCounter += $localDbCustomersResponse;
        }else{
            $localDbCustomersResponse = ['error' => true, 'message' => $this->__('There are no search results. Please try again with new search criteria.')];
        }

        if (isset($serviceCustomersResponse['customers'])) {
            // Save customers to session
            $this->getStorage()->setCustomers($serviceCustomersResponse['customers']);
        } else {
            $this->getStorage()->unsCustomers();
        }

        $response =  [
            'allCustomersCounter' => $allCustomersCounter,
            'serviceCustomers' => $serviceCustomersResponse,
            'localCustomersCounter' => $localDbCustomersResponse
        ];
        if (isset($serviceCustomersResponse['customers']) &&
            (!isset($localDbCustomersResponse['customers']) && !(is_numeric($localDbCustomersResponse) && $localDbCustomersResponse > 1)) &&
            $serviceCustomersResponse['countUnlinkedCustomers'] == 1 &&
            !$this->legacySearch
        ) {
            $response["one_result"] = true;
        }

        return $response;
    }

    /**
     * The list with accounts is parsed as follows in order to group the accounts that are linked together.
     * Foreach account we set 2 properties:
     * "linkedAccounts" property will contain an array with all the accounts that are linked to this one
     * "isLinkedChild" property is a boolean that will be set to "true" if the account is a linked account not a parent account.
     * (in business logic all linked accounts are on the same level but we have decided that the first found will be considered as a "parent")
     * The accounts that have the same "Contact"."ID" are considered to be linked.
     *
     * @param $params
     * @return array
     */
    protected function getFilteredCustomersFromService($params)
    {
        if ($this->legacySearch) {
            $postData = $this->getRequest()->getPost();

            $searchParams = [];
            foreach ($postData['data'] as $parameter) {
                $searchParams[$parameter['name']] = trim($parameter['value']);
            }
            /** @var Dyna_Customer_Model_Client_SearchCustomerFromLegacyClient $customerLegacyClient */
            $customerLegacyClient = Mage::getModel('dyna_customer/client_searchCustomerFromLegacyClient');
            // call the service to filter customers
            $customersResponseService = $customerLegacyClient->executeSearchCustomerFromLegacy($searchParams);
        } else if ($this->exactSearch) {
            /** @var Dyna_Customer_Model_Client_ObjectSearchClient $customerClient */
            $customerClient = Mage::getModel('dyna_customer/client_objectSearchClient');
            // call the service to filter customers
            $customersResponseService = $customerClient->executeObjectSearch($params);
        } else {
            /** @var Dyna_Customer_Model_Client_SearchCustomerClient $customerClient */
            $customerClient = Mage::getModel('dyna_customer/client_searchCustomerClient');
            // call the service to filter customers
            $customersResponseService = $customerClient->executeSearchCustomer($params);
        }

        // if the service did not respond with success -> display a message
        if (!$customersResponseService['Success'] || strtoupper($customersResponseService['Status']['StatusReasonCode']) !== Dyna_Service_Model_Client::NO_ERROR_CODE) {
            return array (
                "error" => true,
                "message" => $this->__("There was a problem with the service search customers. Please try again"),
            );
        }

        // parse customer search results
        $customersResult = $this->searchHelper->parseCustomerSearchAndGroupLinkedAccounts($customersResponseService, $this->legacySearch);

        $customers = $customersResult['customers'];
        $countUnlinkedCustomers = $customersResult['countUnlinkedCustomers'];

        if (!empty($customers) && $countUnlinkedCustomers <= $this->_getLimit() && count($customers) <= $this->_getLimit()) {

            // OMNVFDE-2718: Date of Birth is not correctly displayed for individual accounts that are part of a linked account
            foreach ($customersResult['customers'] as &$customer) {
                if (!empty($customer["Role"]["Person"]["BirthDate"])) {
                    $customer["Role"]["Person"]["BirthDate"] = date("d.m.Y", strtotime($customer["Role"]["Person"]["BirthDate"]));
                }
                if (!empty($customer['linkedAccounts'])) {
                    foreach ($customer['linkedAccounts'] as &$customerLink) {
                        if (!empty($customerLink["Role"]["Person"]["BirthDate"])) {
                            $customerLink["Role"]["Person"]["BirthDate"] = date("d.m.Y", strtotime($customerLink["Role"]["Person"]["BirthDate"]));
                        }
                    }
                }
            }
            // OMNVFDE-2718

            $serviceCustomerResult = [
                "error" => false,
                "customers" => $customersResult['customers'],
                "countUnlinkedCustomers" => $customersResult['countUnlinkedCustomers'],
                'legacyResult' => ($this->legacySearch) ? true : false
            ];
        } elseif ($countUnlinkedCustomers > $this->_getLimit() || count($customers) > $this->_getLimit()) {
            // More than 40 customers found
            // todo: determine if error is received from service or not
            $serviceCustomerResult = [
                "error" => true,
                "message" => sprintf($this->__('There are %d+ search results. Please try again with new search criteria.'), $this->_getLimit())
            ];
        } else {
            // No customers found
            $serviceCustomerResult = [
                "error" => true,
                "message" => $this->__("There are no search results. Please try again with new search criteria.")
            ];
        }

        return $serviceCustomerResult;
    }

    protected function getCustomerResultsFromDbCounter($requestParameters)
    {
        $countResults = 0;
        $parameters = new Varien_Object();
        $parameters->addData($requestParameters);
        $searchFields = $this->getSearchFieldsData($parameters);

        if (!count($searchFields)) {
            return $countResults;
        }

        /** @var Omnius_Customer_Model_Customer_Resource_Customer_Collection $customerCollection */
        $customerCollection = Mage::getResourceSingleton('customer/customer_collection')
            //display only prospect customers
            ->addAttributeToFilter('is_prospect', 1);

        $this->_getHits($customerCollection, $searchFields, false);

        return $customerCollection->getSize();
    }

    /**
     * @param $parameters
     * @return array
     */
    protected function getSearchFieldsData($parameters)
    {
        //filter through each provided parameters
        $searchFields = [];
        foreach ($this->acceptedFields as $field) {
            if ($value = $parameters->getData($field)) {
                $searchFields[$field] = $value;
            }
        }

        return $searchFields;
    }

    /*
     * Get the customer results filtered:
     * - from service and from local db
     */

    /**
     * @param $collection Omnius_Customer_Model_Customer_Resource_Customer_Collection
     * @param $fields
     * @param $addGroupBy
     */
    protected function _getHits(&$collection, $fields, $addGroupBy = true)
    {
        /** @var Dyna_Customer_Model_Customer $customer */
        $customer = Mage::getModel('customer/customer');
        $select = $collection->getSelect();
        $adapter = $select->getAdapter();
        $select->joinLeft(['cae' => 'customer_address_entity'], 'cae.parent_id = e.entity_id', '');
        foreach ($fields as $field => $value) {
            switch ($field) {
                case 'first_name':
                    $select->joinLeft(['cev2' => 'customer_entity_varchar'], '(cev2.entity_id = e.entity_id) AND (cev2.attribute_id=' . $this->_getAttributeId('firstname', 1) . ')', "");
                    $select->joinLeft(['cev1' => 'customer_entity_varchar'], '(cev1.entity_id = e.entity_id) AND (cev1.attribute_id=' . $this->_getAttributeId('company_name', 1) . ')', "");
                    // first name is used also for the company name query OMNVFDE-164
                    $select->where(
                        sprintf(
                            "cev2.value = %s OR cev1.value = %s",
                            $adapter->quote($value),
                            $adapter->quote($value)
                        )
                    );
                    break;
                case 'last_name':
                    $select->joinLeft(['cev3' => 'customer_entity_varchar'], '(cev3.entity_id = e.entity_id) AND (cev3.attribute_id=' . $this->_getAttributeId('lastname', 1) . ')', "");
                    $select->where('cev3.value = ?', $value);
                    break;
                case 'email':
                    $select->joinLeft(['cev4' => 'customer_entity_varchar'], '(cev4.entity_id = e.entity_id) AND (cev4.attribute_id=' . $this->_getAttributeId('additional_email', 1) . ')', "");
                    $select->where('cev4.value = ?', $value);
                    break;
                //todo how should we search by prefix/phone since the input contains an entire number?
                case 'telephone_number':
                    // OMNVFDE-2160: exclude spaces and + prefix before searching since the UCT will retrieve numbers in this format
                    $value =  preg_replace('/\s+/', '', $value);
                    if(strpos($value,"+") === 0) {
                        $value = substr($value, 1);
                    }
                    if (strpos($value,$customer::COUNTRY_CODE) === 0) {
                        $value = substr($value, 4);
                    }
                    if (strpos($value,$customer::SHORT_COUNTRY_CODE) === 0) {
                        $value = substr($value, 2);
                    }
                    if(strpos($value,"0") === 0) {
                        $value = substr($value, 1);
                    }
                    $select->joinLeft(['cev5' => 'customer_entity_varchar'], '(cev5.entity_id = e.entity_id) AND (cev5.attribute_id=' . $this->_getAttributeId('additional_telephone', 1) . ')', "");
                    $select->where(
                        sprintf(
                            'cev5.value = %s OR cev5.value = %s OR cev5.value = %s',
                            $adapter->quote($value),
                            $adapter->quote($customer::SHORT_COUNTRY_CODE . $value),
                            $adapter->quote($customer::COUNTRY_CODE . $value)
                        )
                    );
                    break;
                case 'birthday':
                    $select->joinLeft(['ced' => 'customer_entity_datetime'], '(ced.entity_id = e.entity_id) AND (ced.attribute_id=' . $this->_getAttributeId('dob', 1) . ')', "");
                    $select->where('ced.value = ?', date('Y-m-d H:i:s', strtotime($value)));
                    break;
                default:
                    //do nothing
                    break;
            }
        }

        if($addGroupBy){
            $select->group('e.entity_id');
        }
        if ($this->_getLimit()) {
            $select->limit($this->_getLimit() + 1);
        }
    }

    /**
     * @param $handle
     * @param $typeId
     * @return bool|false|Mage_Core_Model_Abstract|mixed
     */
    private function _getAttributeId($handle, $typeId)
    {
        $readConnection = Mage::getSingleton('core/resource')->getConnection('core_read');

        if (isset($this->_attributeIds[$typeId . '_' . $handle])) {
            return $this->_attributeIds[$typeId . '_' . $handle];
        }

        $key = serialize([__METHOD__, $handle, $typeId]);
        if (!$attributeId = $this->getCache()->load($key)) {

            $attributeId = $readConnection
                ->fetchOne("SELECT attribute_id FROM eav_attribute WHERE entity_type_id = :type AND attribute_code = :attr",
                    array(
                        ':type' => $typeId,
                        ':attr' => $handle
                    ));
            $this->getCache()->save($attributeId, $key, [Dyna_Cache_Model_Cache::CACHE_TAG], $this->getCache()->getTtl());
        }
        $this->_attributeIds[$typeId . '_' . $handle] = $attributeId;

        return $attributeId;
    }

    /**
     * @return Dyna_Cache_Model_Cache
     */
    protected function getCache()
    {

        if (!$this->_cache) {
            $this->_cache = Mage::getSingleton('dyna_cache/cache');
        }

        return $this->_cache;
    }

    /**
     * @return Dyna_Customer_Model_Session
     */
    protected function getStorage()
    {
        if (!$this->storage) {
            $this->storage = Mage::getSingleton('dyna_customer/session');
        }

        return $this->storage;
    }

    /**
     * @IsGranted({"permissions":["SEARCH_CUSTOMER"]})
     */
    public function getCustomerResultsAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }
        $this->legacySearch = false;
        $this->searchCustomer();
    }

    /**
     * Get the customer results list with a ctn received from UCT that makes an URL ReceiveUCTLinkAPI call with parameter
     * @IsGranted({"permissions":["SEARCH_CUSTOMER"]})
     */
    public function getCustomerResultsWithUctAction()
    {
        if ($data = $this->getRequest()->getParams()) {
            /** @var Dyna_Customer_Helper_Data $customerHelper */
            $customerHelper = Mage::helper('dyna_customer');
            $customerHelper->unloadCustomer();
            $customerSession = Mage::getSingleton("dyna_customer/session");
            // Save uct params to session
            $transaction = Mage::getModel('dyna_checkout/uct')
                ->getCollection()
                ->addFieldToFilter('transaction_id', $data['transactionId'])
                ->getFirstItem();
            if($transaction->getId()){
                $data['campaignId'] = $transaction->getCampaignId();
                $data['callingNumber'] = $transaction->getCallerId();
                $data['Idwithtrans'] = $transaction->getCampaignId(); 
                $data['success'] = true;
            }
            else {
                $data['success'] = false;
            }
            $customerSession->setData('new_search_customer',false);
            $this->getStorage()->setUctParams($data);
        }

        $this->_redirect('/');
    }

    /**
     * @IsGranted({"permissions":["SEARCH_CUSTOMER"]})
     */
    public function getLocalCustomersAction()
    {
        if ($postData = $this->getRequest()->getPost()) {
            try {
                $this->searchHelper = Mage::helper('dyna_customer/search');
                $this->allCustomers = [];

                // parse and validate the filters
                $response = $this->searchHelper->parseCustomerSearchParams($postData);
                // if there is a validation error or no parameters are given => skip search and display the error
                if (!empty($response['error'])) {
                    $this->setJsonResponse($response);
                    return;
                }

                $this->getResponse()
                    ->setHeader('Content-Type', 'application/json')
                    ->setBody(Mage::helper('core')->jsonEncode(['localCustomers' => $this->geParsedCustomersFromDb($response['params'])]));

                return;
            } catch (\Exception $e) {
                $result = [
                    "error" => true,
                    "message" => $e->getMessage()
                ];

                $this->setJsonResponse($result);
            }
        }
    }

    /*
    *
    */

    private function geParsedCustomersFromDb($requestParameters)
    {
        $parameters = new Varien_Object($requestParameters);
        $searchFields = $this->getSearchFieldsData($parameters);

        if (!count($searchFields)) {
            // No search parameter given for prospect search
            return [
                "error" => true,
                "message" => $this->__("There are no search results. Please try again with new search criteria.")
            ];
        }
        $customers = $this->getCustomerResultsFromDb($searchFields);
        $customersResult = $this->searchHelper->parseCustomerResultsFromLocalDb($customers);

        if (!empty($customers)) {
            // parse customer search results
            $dbCustomerResult = [
                "error" => false,
                "customers" => $customersResult,
            ];
        } elseif (count($customersResult) > $this->_getLimit()) {
            $dbCustomerResult = [
                "error" => true,
                "message" => sprintf($this->__('There are %d+ search results. Please try again with new search criteria.'), $this->_getLimit())
            ];
        } else {
            $dbCustomerResult = [
                "error" => true,
                "message" => $this->__("There are no search results. Please try again with new search criteria.")
            ];
        }
        return $dbCustomerResult;
    }

    /**
     * @param $requestParameters
     * @return array
     */
    private function getCustomerResultsFromDb($requestParameters)
    {
        $dataToCollect = array('entity_id', 'is_prospect', 'customer_type', 'additional_email', 'additional_telephone', 'firstname', 'lastname',
            'middlename', 'customer_ctn', 'ban', 'company_coc', 'billing_address', 'billing_street', 'billing_city', 'dob',
            'billing_postcode', 'company_name', 'is_business', 'gender', 'customer_label', 'prefix', 'phone_local_area_code', 'phone_number');

        /** @var Omnius_Customer_Model_Customer_Resource_Customer_Collection $customerCollection */
        $customerCollection = Mage::getResourceSingleton('customer/customer_collection')
            ->addAttributeToSelect($dataToCollect)
            ->joinAttribute('is_prospect_3', 'customer/is_prospect', 'is_prospect', null, 'left')
            ->joinAttribute('billing_street', 'customer_address/street', 'default_billing', null, 'left')
            ->joinAttribute('billing_postcode', 'customer_address/postcode', 'default_billing', null, 'left')
            ->joinAttribute('billing_city', 'customer_address/city', 'default_billing', null, 'left')
            ->addAttributeToFilter('is_prospect', 1); //display only prospect customers

        $this->_getHits($customerCollection, $requestParameters);


        $result = $customerCollection
            ->load()
            ->toArray($dataToCollect);


        //serialize result or show No results found
        if (!count($result)) {
            return $result;
        }

        return array_values($result);
    }

    /**
     * Get limit for customer search results
     * @return int|null
     */
    protected function _getLimit(){
        if ($this->_limit === null) {
            $storeConfig = (int) trim(Mage::getStoreConfig(self::LIMIT_PATH));
            $this->_limit =  $storeConfig ?: self::MAX_SEARCH_RESULTS;
        }

        return $this->_limit;
    }
}
