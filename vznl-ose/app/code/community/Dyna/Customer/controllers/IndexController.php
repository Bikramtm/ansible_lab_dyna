<?php

/**
 * Class Dyna_Customer_IndexController
 * CustomerDE actions
 */
class Dyna_Customer_IndexController extends Mage_Core_Controller_Front_Action
{
    /** @var Dyna_Customer_Model_Session|null */
    protected $storage = null;

    /**
     * Get the potential links for a customer
     */
    public function getCustomerPotentialLinksAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }
        /**
         * OMNVFDE-2306
         * If the Agent does not have the permission LINKED_ACCOUNTS_RESULTS for a logged in customer => the Agent cannot search the potential links.
         */
        $customerSession = Mage::getSingleton('customer/session');
        $customer = Mage::getSingleton('dyna_customer/session')->getCustomer();
        $customerLoggedIn = $customer->isCustomerLoggedIn();
        if (!$customerSession->getAgent()->isGranted(array('LINKED_ACCOUNTS_RESULTS')) && $customerLoggedIn) {
            $this->setJsonResponse([
                'error' => true,
                'message' => $this->__("You do not have the permission for this action")
            ]);
            return;
        }
        if ($postData = $this->getRequest()->getPost()) {
            /**
             * @var $validationHelper Dyna_Customer_Helper_Validation
             */
            $validationHelper = Mage::helper('dyna_customer/validation');
            // parse and validate the filters
            $response = $validationHelper->validateContactId($postData);
            if (!empty($response['validationError'])) {
                $this->setJsonResponse($response);
                return;
            }
            try {
                /**
                 * @var $helper Dyna_Customer_Helper_Search
                 */
                $helper = Mage::helper('dyna_customer/search');
                /** @var Dyna_Customer_Model_Client_PotentialLinksClient $customerClient */
                $customerClient = Mage::getModel('dyna_customer/client_potentialLinksClient');
                $linkDataResponse = $customerClient->executeGetPotentialLink($postData);
                $sessionCustomer = $this->getCustomerSession()->getSessionCustomer($postData['contactId']);
                $customer = ($customerLoggedIn) ?  Mage::getSingleton('dyna_customer/session')->getLinkedCustomersForLoggedInCustomer() : $sessionCustomer;

                // if the service did not respond with success -> display a message
                if (!$linkDataResponse['Success']) {
                    $result = [
                        "error" => true,
                        "message" => $this->__("There was a problem with the potential links search. Please try again"),
                        "customer" => $customer
                    ];

                    $this->setJsonResponse($result);
                    return;
                }
                // parse customer search results
                $linkData = $helper->parseCustomerSearchResults($linkDataResponse);
                $result = [
                    "error" => false,
                    "customer" => $customer,
                ];
                $result['potential_links'] = count($linkData) ? $linkData : '';
                $this->setJsonResponse($result);
                return;

            } catch (\Exception $e) {
                Mage::logException($e);
                // Send the loaded customer even if an error occurred during search potential links
                $sessionCustomer = $this->getCustomerSession()->getSessionCustomer($postData['contactId']);
                $this->setJsonResponse([
                    'error' => true,
                    'message' => $e->getMessage(),
                    'customer' => $customer
                ]);
            }
        }
    }

    public function createPotentialLinkAction()
    {
        if ($postData = $this->getRequest()->getPost()) {
            try {
                $customerClient = Mage::getModel('dyna_customer/client_createOrAddLinkClient');

                $requestData["targetId"] = $postData['targetCustomerLinkedId'] ?? '';
                foreach ($postData['potentialCustomersLinks'] as $linkId) {
                    $requestData["customers"][] = $linkId;
                }

                /** @var Dyna_Customer_Model_Client_CreateOrAddLinkClient $customerClient */
                $data = $customerClient->executeCreateOrAddLink($requestData);

                $response = array(
                    "error" => $data["Success"] == true ? false : true,
                    "message" => $data["Status"]["StatusReason"],
                    "data" => $data["Account"]
                );

                $this->setJsonResponse($response);

                return;
            } catch (\Exception $e) {
                Mage::logException($e);
                $this->setJsonResponse([
                    'error' => true,
                    'message' => $e->getMessage()
                ]);
            }
        }
    }

    /**
     * @return Dyna_Customer_Model_Session
     */
    protected function getCustomerSession()
    {
        if (!$this->storage) {
            $this->storage = Mage::getSingleton('dyna_customer/session');
        }

        return $this->storage;
    }

    /**
     * @param $result
     */
    private function setJsonResponse($result)
    {
        $this->getResponse()->setHeader('Content-Type', 'application/json');
        $this->getResponse()->setBody(json_encode($result));
        $this->getResponse()->setHttpResponseCode(200);
    }
}
