<?php

class Dyna_Customer_CallController extends Mage_Core_Controller_Front_Action
{
    /**
     * Frontend will continuously interrogate this action to see if there are any customer ids available
     */
    public function incomingCallAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }

        // @todo add customer id and call services
        $this->setJsonResponse([
            'customerId' => ''
        ]);
    }

    /**
     * @param $result
     */
    private function setJsonResponse($result)
    {
        $this->getResponse()->setHeader('Content-Type', 'application/json');
        $this->getResponse()->setBody(json_encode($result));
        $this->getResponse()->setHttpResponseCode(200);
    }
}
