<?php
require_once Mage::getModuleDir('controllers', 'Omnius_Customer') . DS . '/Customer/DetailsController.php';

/**
 * Class Dyna_Customer_DetailsController
 */
class Dyna_Customer_DetailsController extends Omnius_Customer_Customer_DetailsController
{
    /**
     *
     */
    public function authenticateCustomerAction()
    {
        if ($postData = $this->getRequest()->getPost()) {
            /**
             * @var $validationHelper Dyna_Customer_Helper_Validation
             */
            $validationHelper = Mage::helper('dyna_customer/validation');
            // parse and validate the params
            $response = $validationHelper->validateAuthParams($postData);
            if (!empty($response['validationError'])) {
                $this->setJsonResponse($response);
                return;
            }

            /** @var Dyna_Customer_Model_Client_RetrieveCustomerInfo $retrieveCustomerInfoClient */
            $retrieveCustomerInfoClient = Mage::getModel('dyna_customer/client_retrieveCustomerInfo');
            try {
                $data = $retrieveCustomerInfoClient->executeRetrieveCustomerInfo($postData, 1);
                $customerObject = $retrieveCustomerInfoClient->mapBillingDetails($data, $postData['parentAccountNumberId']);

                if (!$customerObject) {
                    $this->getResponse()
                        ->setHeader('Content-Type', 'application/json')
                        ->setBody(Mage::helper('core')->jsonEncode([
                            'error' => true,
                            'message' => $this->__("Retrieving the authentication data for this customer failed"),
                            'data' => $customerObject
                        ]));
                } else {
                    $this->getResponse()
                        ->setHeader('Content-Type', 'application/json')
                        ->setBody(Mage::helper('core')->jsonEncode([
                            'error' => false,
                            'data' => $customerObject
                        ]));
                }

                return;
            } catch (\Exception $e) {
                Mage::logException($e);
                $this->getResponse()
                    ->setHeader('Content-Type', 'application/json')
                    ->setBody(Mage::helper('core')->jsonEncode([
                        'error' => true,
                        'message' => $e->getMessage()
                    ]));
            }

        }
    }

    /**
     * Authenticate household members by calling RetrieveCustomerInfo mode1
     */
    public function authenticateHouseholdCustomerAction()
    {
        if ($postData = $this->getRequest()->getPost()) {
            /**
             * @var $validationHelper Dyna_Customer_Helper_Validation
             */
            $validationHelper = Mage::helper('dyna_customer/validation');
            // parse and validate the params
            $response = $validationHelper->validateAuthParams($postData);
            if (!empty($response['validationError'])) {
                $this->setJsonResponse($response);
                return;
            }

            /** @var Dyna_Customer_Model_Client_RetrieveCustomerInfo $retrieveCustomerInfoClient */
            $retrieveCustomerInfoClient = Mage::getModel('dyna_customer/client_retrieveCustomerInfo');
            try {
                $data = $retrieveCustomerInfoClient->executeRetrieveCustomerInfo($postData, 1);
                $customerObject = $retrieveCustomerInfoClient->mapHouseholdBillingDetails($data);

                if (!$customerObject) {
                    $this->getResponse()
                        ->setHeader('Content-Type', 'application/json')
                        ->setBody(Mage::helper('core')->jsonEncode([
                            'error' => true,
                            'message' => $this->__("Retrieving the authentication data for this customer failed"),
                            'data' => $customerObject
                        ]));
                } else {
                    $this->getResponse()
                        ->setHeader('Content-Type', 'application/json')
                        ->setBody(Mage::helper('core')->jsonEncode([
                            'error' => false,
                            'data' => $customerObject
                        ]));
                }

                return;
            } catch (\Exception $e) {
                Mage::logException($e);
                $this->getResponse()
                    ->setHeader('Content-Type', 'application/json')
                    ->setBody(Mage::helper('core')->jsonEncode([
                        'error' => true,
                        'message' => $e->getMessage()
                    ]));
            }

        }
    }

    /**
     * Returns the customer details (address, name, id, phone) that are displayed in the left side of the page
     * This is the getCustomerDetailsMode1 request
     *
     * @IsGranted({"permissions":["SEARCH_CUSTOMER"]})
     */
    public function loadCustomerAction()
    {
        if (!$postData = $this->getRequest()->getPost()) {
            return;
        }

        /**
         * @var $validationHelper Dyna_Customer_Helper_Validation
         */
        $validationHelper = Mage::helper('dyna_customer/validation');
        // parse and validate the filters
        if (isset($postData['isProspect']) && $postData['isProspect'] == 'true') {
            $response = $validationHelper->validateParentAccountNumberId($postData);
        } else {
            $response = $validationHelper->validateContactId($postData);
        }

        if (!empty($response['validationError'])) {
            $this->setJsonResponse($response);
            return;
        }

        try {
            if (isset($postData['isProspect']) && $postData['isProspect'] == "true") {
                /** @var Dyna_Customer_Model_Customer $customerObject */
                $customerObject = Mage::getModel('customer/customer')->load($postData["parentAccountNumberId"]);
                $response = $this->loadCustomerOnSession($customerObject, $postData['mode'], $postData['isProspect']);
            } else {
                if ($postData['mode'] == 1) {
                    /** @var Dyna_Customer_Model_Client_RetrieveCustomerInfo $retrieveCustomerInfoClient */
                    $retrieveCustomerInfoClient = Mage::getModel('dyna_customer/client_retrieveCustomerInfo');
                    $data = $retrieveCustomerInfoClient->executeRetrieveCustomerInfo($postData, 1);

                    // if the service did not respond with success -> display a message
                    if (!$data['Success']) {
                        $result = [
                            "error" => true,
                            "message" => $this->__("There was a problem with the customer loading. Please try again!"),
                        ];

                        $this->setJsonResponse($result);
                        return;
                    }

                    $addressStorage = Mage::getSingleton('dyna_address/storage');
                    // Go further with renewing service address
                    $addressStorage->unsServiceAbility();
                    $addressStorage->unsAddress();

                    $customerObject = $retrieveCustomerInfoClient->mapCustomer($data, 1);
                    if ($this->hasOnlyExcludedAccount()) {
                        $result = [
                            "error" => false,
                            "warning" => true,
                            "message" => $this->__("The creation of the customer has not yet been completed, so further processing is not possible. Please try again shortly"),
                        ];
                        $this->setJsonResponse($result);
                        return;
                    }
                } else {
                    $customerObject = Mage::getSingleton('customer/session')->getCustomer();
                }
                $response = $this->loadCustomerOnSession($customerObject, $postData['mode']);
            }

            if (isset($postData['cartWasCleared']) && $postData['cartWasCleared'] == "true") {
                $response['totals'] = Mage::getSingleton('core/layout')->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/totals.phtml')->toHtml();
            }

            // send eligible bundles to frontend so we can enable add red+ package in cart after a customer is loaded
            /** @var Dyna_Bundles_Helper_Data $bundlesHelper */
            if ($postData['mode'] == 2) {
                $bundlesHelper = Mage::helper('dyna_bundles');
                $eligibleBundles = $bundlesHelper->getFrontEndEligibleBundles(true);
                $bundlesHelper->filterBundlesBySubscriptionStatus($eligibleBundles);
                $response['eligibleBundles'] = $eligibleBundles;

                /** @var Dyna_Customer_Helper_Data $customerHelper */
                $customerHelper = Mage::helper('dyna_customer');

                $response['has_open_orders'] = [
                    strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE) => $customerHelper->hasOpenOrders(null, Dyna_Catalog_Model_Type::getMobilePackages()),
                    strtolower(Dyna_Catalog_Model_Type::TYPE_PREPAID) => $customerHelper->hasOpenOrders(null, Dyna_Catalog_Model_Type::getMobilePackages()),
                    strtolower(Dyna_Catalog_Model_Type::TYPE_FIXED_DSL) => $customerHelper->hasOpenOrders(null, Dyna_Catalog_Model_Type::getFixedPackages()),
                    strtolower(Dyna_Catalog_Model_Type::TYPE_LTE) => $customerHelper->hasOpenOrders(null, Dyna_Catalog_Model_Type::getFixedPackages()),
                    strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_INTERNET_PHONE) => $customerHelper->hasOpenOrders(null, Dyna_Catalog_Model_Type::TYPE_CABLE_INTERNET_PHONE),
                    strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_INDEPENDENT) => $customerHelper->hasOpenOrders(null, Dyna_Catalog_Model_Type::TYPE_CABLE_INDEPENDENT),
                    strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_TV) => $customerHelper->hasOpenOrders(null, Dyna_Catalog_Model_Type::TYPE_CABLE_TV),

                ];
            }

            $this->getResponse()
                ->setHeader('Content-Type', 'application/json')
                ->setBody(Mage::helper('core')->jsonEncode($response));
            return;
        } catch (\Exception $e) {
            Mage::logException($e);
            $this->getResponse()
                ->setHeader('Content-Type', 'application/json')
                ->setBody(Mage::helper('core')->jsonEncode([
                    'error' => true,
                    'message' => $e->getMessage()
                ]));
        }
    }

    /**
     * Check if there is just a mobile / cable excluded account on current customer
     * @return bool
     */
    public function hasOnlyExcludedAccount()
    {
        if (Mage::getSingleton('dyna_customer/session')->getHasOnlyExcludedCable()) {
            $hasExcludedAccount = true;
            Mage::getSingleton('dyna_customer/session')->unsetData('has_only_excluded_cable');
        } elseif (Mage::getSingleton('dyna_customer/session')->getHasOnlyExcludedMobile()) {
            $hasExcludedAccount = true;
            Mage::getSingleton('dyna_customer/session')->unsetData('has_only_excluded_mobile');
        }
        return isset($hasExcludedAccount);
    }

    public function loadHouseholdAction()
    {
        try {
            /** @var Dyna_Customer_Helper_Panel $panelHelper */
            $panelHelper = Mage::helper('dyna_customer/panel');
            $customerSession = Mage::getSingleton('customer/session');
            $householdResponse = $panelHelper->getHouseholdMembers();

            if (!$householdResponse['error'] && isset($householdResponse['members_count'])) {
                $customerSession->setData('household_members_count', $householdResponse['members_count']);

                $response = [
                    'household' => $householdResponse
                ];
            } else {
                $response = $householdResponse;
            }

            $this->getResponse()
                ->setHeader('Content-Type', 'application/json')
                ->setBody(Mage::helper('core')->jsonEncode($response));

        } catch (\Exception $e) {
            Mage::logException($e);
            $this->getResponse()
                ->setHeader('Content-Type', 'application/json')
                ->setBody(Mage::helper('core')->jsonEncode([
                    'error' => true,
                    'message' => $e->getMessage()
                ]));
        }
    }

    /**
     * Returns the UCT caller & campaign data
     */
    public function getCallerCampaignDataAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }

        $postData = $this->getRequest()->getPost();
        if (array_key_exists('page', $postData)) {
            $page = $postData['page'];
        } else {
            $page = 1;
        }
        try {
            $uctParams = Mage::getSingleton('dyna_customer/session')->getUctParams();

            $result = [];
            $result['salesIds'] = Mage::helper('bundles')->getProvisDataByRedSalesId();
            $result['callerData'] = Mage::helper('bundles')->getCallerData($uctParams['transactionId']);
            $result['campaignData'] = Mage::helper('bundles')->getCampaignData($uctParams['campaignId'], $uctParams['callingNumber'], $page);
            $result['success'] = true;
        } catch (Exception $e) {
            $result = [
                'success' => false,
                'message' => $e->getMessage()
            ];
        }

        $this->setJsonResponse($result);
    }
    /**
    * Function to get the default campaigns available
    */
    public function getDefaultCampaignDataAction()
    {
        $post = $this->getRequest()->getPost();

        $defaultCampaignData= Mage::helper('bundles')->getDefaultCampaignData($post);

        $this->setJsonResponse($defaultCampaignData);
    }
    /**
    * Function to set the UCT session variables with the selcted campaign
    */
    public function setDefaultCampaignDataAction()
    {
        $post = $this->getRequest()->getPost();
        $result= Mage::helper('bundles')->setDefaultCampaignData($post);
        $this->setJsonResponse($result);
    }

    /**
     * Unload customer data from session (customer, quotes, orders, address ...)
     */
    public function unloadCustomerAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }
        /** @var Dyna_Customer_Helper_Data $customerHelper */
        $customerHelper = Mage::helper('dyna_customer');
        $customerHelper->unloadCustomer();
        $response = array(
            'sticker' => $this->getLayout()->createBlock('core/template')->setTemplate('customer/left_search.phtml')->toHtml(),
            'saveCartModal' => $this->getLayout()->createBlock('core/template')->setTemplate('page/html/save_cart_modal.phtml')->toHtml(),
        );
        $this->getResponse()
            ->setHeader('Content-Type', 'application/json')
            ->setBody(Mage::helper('core')->jsonEncode($response));
    }

    /**
     * Returns json response for customer left screen depending on the requested section (orders-content, products-content ... etc)
     */
    public function showCustomerPanelAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }

        if (!$customer = Mage::getSingleton("dyna_customer/session")->getCustomer()) {
            $response = $this->parseError("Please select a customer.");

            $this->setJsonResponse($response);
            return;
        }

        if (!$section = $this->getRequest()->getParam('section')) {
            $response = $this->parseError("No valid section requested.");

            $this->setJsonResponse($response);
            return;
        }

        /**
         * @var $panelHelper Dyna_Customer_Helper_Panel
         */
        $panelHelper = Mage::helper('dyna_customer/panel');
        $response = $panelHelper->getPanelData($section);

        $customerName = $customer->getFirstname()." ".$customer->getLastname();
        // Creating breadcrumbs for each links clicked
        if( $section == "link-details") {
            $response['breadcrumb'] = $this->__('Linked Account');
        }elseif( $section == "orders-content") {
            $response['breadcrumb'] = $this->__('Orders of').' ' .$customerName;
        }elseif( $section == "carts-content") {
            $response['breadcrumb'] = $this->__('Shopping cart of').' ' .$customerName;
        }elseif( $section == "products-content") {
            $response['breadcrumb'] = $this->__('Products');
            $response['customer_details'] = $customerName;
        }

        /**
         * Check if customer products are eligible for Red+ bundling and mark them as such
         * so we can enable the Add Red+ member button in frontend
         */
        if (isset($response['KIAS']) && $response['KIAS']) {
            $response['KIAS'] = $panelHelper->updateKiasSubscriptionsWithRedplusBundles($response['KIAS']);
        }

        $this->setJsonResponse($response);
    }

    /**
     *  Make a retention check for multiple mobile accounts
     * @IsGranted({"permissions":["PROLONGATION_CHECK"]})
     */
    public function prolongationCheckAction()
    {
        if ($postData = $this->getRequest()->getPost()) {
            $customerSession = Mage::getSingleton('dyna_customer/session');
            $agent = $customerSession->getAgent();
            $agentHasProlongation = $agent->isGranted('NPH_PROLONGATION');
            $currentId = $postData['currentId'];

            if (!Mage::getSingleton("dyna_customer/session")->getCustomer()) {
                $response = $this->parseError("Customer not found on session.");

                $this->setJsonResponse($response);
                return;
            }

            try {
                /** @var Dyna_Customer_Model_Client_RetrieveCustomerCreditProfileClient $creditProfileClient */
                $creditProfileClient = Mage::getModel('dyna_customer/client_retrieveCustomerCreditProfileClient');
                $results = $creditProfileClient->executeRetrieveCustomerCreditProfile($postData);
                if (in_array($currentId, array_keys($results['options']))) {
                    $options = $results['options'][$currentId];
                    foreach ($options as $key => $value) {
                        if ($value['type']['key'] == 'NPH' && !$agentHasProlongation) {
                            $results['options'][$currentId][$key]['disabledButton'] = 'true';
                        }
                    }

                }
                // if the service did not respond with success -> display a message
                if (!$results['Success'] || strtoupper($results['Status']['StatusReasonCode']) !== Dyna_Service_Model_Client::NO_ERROR_CODE) {
                    $result = [
                        "error" => true,
                        "message" => $this->__("There was a problem with the retention check. Please try again!"),
                    ];

                    $this->setJsonResponse($result);
                    return;
                }

                $this->getResponse()
                    ->setHeader('Content-Type', 'application/json')
                    ->setBody(Mage::helper('core')->jsonEncode($results));
                return;
            } catch (\Exception $e) {
                Mage::logException($e);
                $this->getResponse()
                    ->setHeader('Content-Type', 'application/json')
                    ->setBody(Mage::helper('core')->jsonEncode([
                        'error' => true,
                        'message' => $e->getMessage()
                    ]));
            }

        }
    }

    /**
     * Set the show BTW
     */
    public function setCustomerTypeSohoAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }

        /** @var Omnius_Customer_Model_Session $session */
        $customer = Mage::getSingleton('customer/session')
            ->getCustomer();

        $state = (bool)$this->getRequest()->getParam('state');
        $customer->setData('party_type', $state ? Dyna_Customer_Helper_Services::PARTY_TYPE_SOHO : Dyna_Customer_Helper_Services::PARTY_TYPE_PRIVATE);
        Mage::getSingleton('checkout/cart')->getQuote()->setCustomerIsBusiness($state);
    }

    /**
     * Set the show BTW
     */
    public function getCustomerTypeSohoAction()
    {
        /** @var Dyna_Customer_Model_Session $session */
        $session = Mage::getSingleton('customer/session');

        $this->getResponse()->setHeader('Content-Type', 'application/json')
            ->setBody(
                Mage::helper('core')->jsonEncode(
                    array(
                        'isSoho' => $session->getCustomer()->getIsSoho(),
                    )
                )
            );
    }

    /**
     * @return Dyna_Customer_Model_Session
     */
    protected function getCustomerSession()
    {
        return Mage::getSingleton('customer/session');
    }

    /**
     * Get checkout session model instance
     *
     * @return Mage_Checkout_Model_Session
     */
    protected function getCheckoutSession()
    {
        return Mage::getSingleton('checkout/session');
    }

    /**
     * @param $customer
     * @return array
     */
    protected function loadCustomerOnSession($customer, $mode = 1, $isProspect = false)
    {
        // Calling helper to map services data to frontend data
        $customerData = Mage::helper('dyna_customer/info')->buildCustomerDetails($customer, $mode, $isProspect);
        if ($mode == 1) {
            //if customer exists, load the entity to retrieve the campaigns, ctn records and other data
            /** @var Dyna_Customer_Model_Session $session */
            $session = $this->_getCustomerSession();
            $session->setCustomer($customer);
            $session->unsBtwState();

            if(!$isProspect){
                $this->setKiasLinkedAccountOnSession($session);
                $this->setKdLinkedAccountOnSession($session);
            }

            $response = array(
                'customerData' => $customerData,
            );
        } else {
            //remember current quote as this will need to be either removed or associated to the loaded customer
            /** @var Omnius_Checkout_Model_Sales_Quote $currentQuote */
            $currentQuote = Mage::getSingleton('checkout/cart')->getQuote();
            $customerId = $customer->getId();
            $currentQuote->setCustomerId($customerId);
            $currentQuote->setTotalsCollectedFlag(true);
            $currentQuote->save();
            // TODO: check if below actions are needed

            /** @var Omnius_PriceRules_Helper_Data $priceRulesHelper */
            $priceRulesHelper = Mage::helper('pricerules');
            $priceRulesHelper->decrementAll($currentQuote);
            $priceRulesHelper->reinitSalesRules($currentQuote);

            $response = array(
                'sticker' => $this->loadLayout('page_three_columns')->getLayout()->getBlock('customer.details')->toHtml(),
                'customerData' => $customerData,
                'saveCartModal' => $this->getLayout()->createBlock('core/template')->setTemplate('page/html/save_cart_modal.phtml')->toHtml(),
                'createWorkItemModal' => $this->getLayout()->createBlock('core/template')->setTemplate('page/modals/work-item-modal.phtml')->toHtml(),
            );
        }

        return $response;
    }

    /**
     * @param $result
     */
    protected function setJsonResponse($result)
    {
        $this->getResponse()->setHeader('Content-Type', 'application/json');
        $this->getResponse()->setBody(json_encode($result));
        $this->getResponse()->setHttpResponseCode(200);
    }

    protected function parseError($message)
    {
        return [
            "error" => true,
            "message" => $this->__($message),
        ];
    }

    /**
     * Set KIAS linked account on session if there is just one linked account
     * @param $session
     */
    protected function setKiasLinkedAccountOnSession(&$session)
    {
        $serviceCustomers = Mage::getSingleton('dyna_customer/session');
        $customerProducts = $serviceCustomers->getCustomerProducts();
        $kiasProducts     = !empty($customerProducts[Dyna_Superorder_Helper_Client::BACKEND_KIAS]) ? $customerProducts[Dyna_Superorder_Helper_Client::BACKEND_KIAS] : [];
        if(count($kiasProducts) == 1) {
            foreach($kiasProducts as $kiasCustomerId => $kiasProduct){
                $session->setKiasLinkedAccountNumber($kiasCustomerId);
            }
        }
    }

    /**
     * Set KD linked account on session if there is just one linked account
     * @param $session
     */
    protected function setKdLinkedAccountOnSession(&$session)
    {
        $serviceCustomers = Mage::getSingleton('dyna_customer/session');
        $customerProducts = $serviceCustomers->getCustomerProducts();
        $kdProducts = $customerProducts[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD] ?? [];
        if (count($kdProducts) == 1) {
            foreach ($kdProducts as $kdCustomerId => $kdProduct) {
                $session->setKdLinkedAccountNumber($kdCustomerId);
            }
        }
    }

    /**
     * RetrieveCustomerInfo mode1 and mode2 will be called to set the household products into session
     */
    public function loadHouseholdProductsAction()
    {
        if (!$postData = $this->getRequest()->getPost()) {
            return;
        }

        try {
            $global_id = $postData['contactId'];

            // If household products already set in session then we'll load the products from session
            /** @var Dyna_Customer_Model_Customer_Session $customerSession */
            $customerSession = Mage::getSingleton('dyna_customer/session');
            
            $householdProducts = $customerSession->getHouseholdProducts();
            if(isset($householdProducts[$global_id])){
                // Loading the products session for the global_id selected
                $response = $householdProducts[$global_id];

                $this->getResponse()
                    ->setHeader('Content-Type', 'application/json')
                    ->setBody(Mage::helper('core')->jsonEncode($response));
                return;

            }else {
                /** @var Dyna_Customer_Model_Client_RetrieveCustomerInfo $retrieveCustomerInfoClient */
                $retrieveCustomerInfoClient = Mage::getModel('dyna_customer/client_retrieveCustomerInfo');
                $data = $retrieveCustomerInfoClient->executeHouseholdRetrieveCustomerInfo($postData, 1);

                /** if the service did not respond with success -> display a message */
                if (!$data['Success']) {
                    $result = [
                        "error" => true,
                        "message" => $this->__("There was a problem with the customer loading. Please try again!"),
                    ];

                    $this->setJsonResponse($result);
                    return;
                }

                /** It will map the data and will save into householdServiceCustomer session */
                $customerData = $retrieveCustomerInfoClient->mapHouseholdCustomer($data, 1);

                /** Mode 2 call for getting more details */

                /** We'll set global id and customer id as returned by mapping customer data after receiving from mode1 call */
                $postData = array(
                    'customerNumber' => $customerData['customer_number'],
                    'global_id' => $customerData['global_id']
                );

                $mode2_data = $retrieveCustomerInfoClient->executeHouseholdRetrieveCustomerInfo($postData, 2);

                if (!$mode2_data['Success']) {
                    $result = [
                        "error" => true,
                        "message" => $this->__("There was a problem with the customer loading. Please try again!"),
                    ];

                    $this->setJsonResponse($result);
                    return;
                }

                // Getting the products for selected household member
                /** @var Dyna_Customer_Helper_Panel $response */
                $response = Mage::helper('dyna_customer/panel')->getPanelData('products-content', true);

                if (!empty($response['error'])) {
                    $result = [
                        "error" => true,
                        "message" => $this->__("There was a problem with the customer loading. Please try again!"),
                    ];

                    $this->setJsonResponse($result);
                    return;
                }

                // We'll disable all the buttons so that agent can't modify packages etc. Since this is only for viewing the product details for household members
                $validationHelper = Mage::helper('dyna_customer/validation');
                $response = $validationHelper->checkHouseholdRestrictionsForButtons($response);

                // Adding breadcrumb
                $response['breadcrumb'] = $this->__('Products of').' '.$customerData['firstname']." ".$customerData['lastname'];

                $response['customer_details'] = $customerData['firstname']." ".$customerData['lastname'];
                $response['household'] = 1;

                // Setting the product details inside customer session so that we don't need to call the service again for household products
                $customerSession->setHouseholdProducts(array($global_id => $response));

                $this->getResponse()
                    ->setHeader('Content-Type', 'application/json')
                    ->setBody(Mage::helper('core')->jsonEncode($response));
                return;
            }
        } catch (\Exception $e) {

            Mage::logException($e);
            $this->getResponse()
                ->setHeader('Content-Type', 'application/json')
                ->setBody(Mage::helper('core')->jsonEncode([
                    'error' => true,
                    'message' => $e->getMessage()
                ]));
        }
    }
}
