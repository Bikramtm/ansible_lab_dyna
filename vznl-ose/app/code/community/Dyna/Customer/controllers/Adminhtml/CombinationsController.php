<?php


class Dyna_Customer_Adminhtml_CombinationsController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()->_setActiveMenu("customer/combinations")
            ->_addBreadcrumb(
                Mage::helper("adminhtml")->__("Account type/subtype combinations"),
                Mage::helper("adminhtml")->__("Account type/subtype combinations")
            );

        return $this;
    }

    public function indexAction()
    {
        $this->_title($this->__("Customer"));
        $this->_title($this->__("Account type/subtype combinations"));
        $this->_initAction();
        $this->_addContent($this->getLayout()->createBlock("dyna_customer/adminhtml_combinations"));
        $this->renderLayout();
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function editAction()
    {


        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("dyna_customer/typeSubtypeCombinations")->load($id);

        if ($model->getId()) {
            Mage::register("dyna_customer_typeSubtypeCombinations", $model);

            $this->_title($this->__("Non suported combinations"));
            $this->_title($this->__("Edit combination"));
        }

        $this->loadLayout();
        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
        $this->_addContent($this->getLayout()->createBlock('dyna_customer/adminhtml_combinations_edit'));
        $this->_addLeft($this->getLayout()->createBlock('dyna_customer/adminhtml_combinations_edit_tabs'));

        $this->renderLayout();
    }

    public function saveAction()
    {
        $postData = $this->getRequest()->getPost();

        if ($postData) {
            $model = Mage::getModel("dyna_customer/typeSubtypeCombinations");

            if ($this->getRequest()->getParam("id")) {
                $existingOption = $model->load($this->getRequest()->getParam("id"));
                $existingOption->addData($postData);

                $existingOption->save();


                if ($this->getRequest()->getParam("back")) {
                    $this->_redirect("*/*/edit", array("id" => $model->getId()));
                    return;
                }
                $this->_redirect("*/*/");
            } else {
                $model->setData($postData);
                $model->save();
            }
        }

        $this->_redirect("*/*/");
    }

    public function deleteAction()
    {
        if ($this->getRequest()->getParam("id")) {
            $option = Mage::getModel("dyna_customer/typeSubtypeCombinations")->load($this->getRequest()->getParam("id"));
            $option->delete();

            Mage::unregister("dyna_customer_typeSubtypeCombinations");
        }

        $this->_redirect("*/*/");
    }

    /**
     * To check if the action is allowed or not
     * @return boolean
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('admin');
    }
}
