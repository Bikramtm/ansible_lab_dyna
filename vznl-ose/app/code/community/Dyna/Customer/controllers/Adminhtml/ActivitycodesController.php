<?php


class Dyna_Customer_Adminhtml_ActivitycodesController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()->_setActiveMenu("customer/activitycodes")->_addBreadcrumb(Mage::helper("adminhtml")->__("Activity codes"), Mage::helper("adminhtml")->__("Activity codes"));

        return $this;
    }

    public function indexAction()
    {
        $this->_title($this->__("Customer"));
        $this->_title($this->__("Activity codes"));
        $this->_initAction();
        $this->_addContent($this->getLayout()->createBlock("dyna_customer/adminhtml_activitycodes"));
        $this->renderLayout();
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function editAction()
    {


        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("dyna_customer/activityCodes")->load($id);

        if ($model->getId()) {
            Mage::register("dyna_customer_activitycodes", $model);

            $this->_title($this->__("Activity codes"));
            $this->_title($this->__("Edit activity codes"));
        }

        $this->loadLayout();
        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
        $this->_addContent($this->getLayout()->createBlock('dyna_customer/adminhtml_activitycodes_edit'));
        $this->_addLeft($this->getLayout()->createBlock('dyna_customer/adminhtml_activitycodes_edit_tabs'));

        $this->renderLayout();
    }

    public function saveAction()
    {
        $postData = $this->getRequest()->getPost();

        if ($postData) {
            $model = Mage::getModel("dyna_customer/activityCodes");

            if ($this->getRequest()->getParam("id")) {
                $existingOption = $model->load($this->getRequest()->getParam("id"));
                $existingOption->addData($postData);

                $existingOption->save();


                if ($this->getRequest()->getParam("back")) {
                    $this->_redirect("*/*/edit", array("id" => $model->getId()));
                    return;
                }
                $this->_redirect("*/*/");
            } else {
                $model->setData($postData);
                $model->save();
            }
        }

        $this->_redirect("*/*/");
    }

    public function deleteAction()
    {
        if ($this->getRequest()->getParam("id")) {
            $option = Mage::getModel("dyna_customer/activityCodes")->load($this->getRequest()->getParam("id"));
            $option->delete();

            Mage::unregister("dyna_customer/activityCodes");
        }

        $this->_redirect("*/*/");
    }

    /**
     * To check if the action is allowed or not
     * @return boolean
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('admin');
    }
}