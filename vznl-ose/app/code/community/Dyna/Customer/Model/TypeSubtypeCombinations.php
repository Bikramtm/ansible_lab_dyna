<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Dyna_Customer_Model_TypeSubtypeCombinations extends Mage_Core_Model_Abstract
{
    /**
     * Dyna_Customer_Model_TypeSubtypeCombinations constructor.
     */
    public function _construct()
    {
        $this->_init('dyna_customer/typeSubtypeCombinations');
    }

    public function checkIfDisable($type = null, $subType = null)
    {
        if (isset($type) && isset($subType)) {
            $collection = $this->getCollection()
                ->addFieldToFilter('acc_type', $this->mapType($type))
                ->addFieldToFilter('acc_sub_type', $subType)
                ->addFieldToFilter('website_id', Mage::app()->getWebsite()->getId());
             return $collection->getSize() > 0;

        }
        return true;
    }

    public function mapType($type)
    {
        $types = [
            'PRIVATE' => 'I',
            'AUTHORITY' => 'A',
            'BUSINESS' => 'B',
            'SOHO' => 'S'
        ];

        return $types[$type];
    }

    public function checkFutureTransactions($code)
    {
        /** @var $activityCodesModel Dyna_Customer_Model_ActivityCodes */
        $activityCodesModel = Mage::getModel('dyna_customer/activityCodes');
        $currentCodes = $activityCodesModel->getNotAllowedActivityCodes();

        if (in_array($code, $currentCodes)) {
            return true;
        }

        return false;
    }
}
