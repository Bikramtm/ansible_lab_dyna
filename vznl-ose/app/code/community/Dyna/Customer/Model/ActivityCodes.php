<?php

/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */
class Dyna_Customer_Model_ActivityCodes extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        $this->_init('dyna_customer/activityCodes');
    }

    public function getNotAllowedActivityCodes()
    {
        $activityCodesItems = $this->getCollection()->addFieldToFilter('allowed', false)->getItems();
        $activityCodes = array();
        foreach ($activityCodesItems as $item) {
            $activityCodes[] = $item->getActivityCode();
        }
        return $activityCodes;
    }

    public function getAllowedActivityCodes()
    {
        $activityCodesItems = $this->getCollection()->addFieldToFilter('allowed', true)->getItems();
        foreach ($activityCodesItems as $item) {
            $activityCodes[] = $item->getActivityCode();
        }
        return $activityCodes;
    }
}
