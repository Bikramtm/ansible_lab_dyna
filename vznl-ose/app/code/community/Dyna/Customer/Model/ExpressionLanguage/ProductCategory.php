<?php

/**
 *
 * Class Dyna_Customer_Model_ExpressionLanguage_ProductCategory
 */
class Dyna_Customer_Model_ExpressionLanguage_ProductCategory
{
    /**
     * @var
     */
    protected $categoryModel;

    /**
     * @var array
     */
    protected $allowedCategories = [];

    /**
     * @var array
     */
    protected $notAllowedCategories = [];


    public function __construct()
    {
        $this->categoryModel = Mage::getModel('catalog/category');
    }

    /**
     *
     * $categories should be an array of full category name paths
     * @param array $categories
     * @internal param string $category
     */
    public function allowed(...$categories)
    {
        foreach($categories as $category) {
            $category = $this->categoryModel->getCategoryByNamePath($category);

            if($category) {
                $categoryId = $category->getId();

                if (!in_array($categoryId, $this->allowedCategories)) {
                    $this->allowedCategories[] = $categoryId;
                }
            }

        }

        return true;
    }

    public function notAllowed(...$categories)
    {
        foreach($categories as $category) {
            $category = $this->categoryModel->getCategoryByNamePath($category);

            if($category) {
                $categoryId = $category->getId();

                if (!in_array($categoryId, $this->notAllowedCategories)) {
                    $this->notAllowedCategories[] = $categoryId;
                }
            }
        }

        return true;
    }

    /**
     * @return array
     */
    public function getAllowedCategories(): array
    {
        return $this->allowedCategories;
    }

    /**
     * @return array
     */
    public function getNotAllowedCategories(): array
    {
        return $this->notAllowedCategories;
    }

    public function reset()
    {
        $this->allowedCategories = [];
        $this->notAllowedCategories = [];
    }

}