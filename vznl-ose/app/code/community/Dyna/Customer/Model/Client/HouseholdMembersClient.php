<?php

/**
 * Class Dyna_Customer_Model_Client_HouseholdMembersClient
 */
class Dyna_Customer_Model_Client_HouseholdMembersClient extends Dyna_Service_Model_Client
{
    const WSDL_CONFIG_KEY = "household_members/wsdl_household_members";
    const ENDPOINT_CONFIG_KEY = "household_members/endpoint_household_members";
    const CONFIG_STUB_PATH = 'omnius_service/household_members/use_stubs';

    /**
     * @param $params
     * @return mixed
     */
    public function getMembers($params)
    {
        $searchParams = $this->mapMembersSearchParametersToXMLNodes($params);
        //GetHouseHoldMembers
        return $this->GetHouseHoldMembers($searchParams);
    }

    /**
     * Map the params with the structure needed for the XML that will be send to the service
     *
     * @access private
     * @param array $params
     * @return array
     */
    private function mapMembersSearchParametersToXMLNodes($params)
    {
        /*
         *
         GetHouseHoldMembersRequest	Root	    1
            HeaderInfo              Record	    1
            TransactionId           String	    1	    Unique Id for this transaction
            UserId                  String	    0..1	The frontend user id
            ApplicationId           String	    0..1	The application. Always "OMNIUS"
            PartyName               String	    1	    "OGW" for Order Gateway, "SAP" for SAP, "OSE" for frontend
            MessageSequenceNumber   String	    0..1	For diagnostics purposes
            Timestamp               DateTime	0..1	For diagnostics purposes
            CorrelationId           String	    1	    Unique ID to correlate messages over several calls (e.g. same customer or order flow)
            Customer                Record	    1
            ID                      String	    1	    The GlobalID of the customer of the household to be searched
         */
        $parametersMapping = [];
        // Set additional headerInfo data (For diagnostics purposes)
        $parametersMapping['HeaderInfo']['MessageSequenceNumber'] = null;
        $parametersMapping['HeaderInfo']['Timestamp'] = null;
        $this->setRequestHeaderInfo($parametersMapping);
        //set body request
        $parametersMapping['Customer']['ID'] = $params['CUSTOMER_ID'];

        return $parametersMapping;
    }
}
