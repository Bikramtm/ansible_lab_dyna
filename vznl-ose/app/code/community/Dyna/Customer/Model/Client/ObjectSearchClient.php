<?php

/**
 * Class Dyna_Customer_Model_Client_ObjectSearchClient
 */
class Dyna_Customer_Model_Client_ObjectSearchClient extends Dyna_Service_Model_Client
{
    const WSDL_CONFIG_KEY = "object_search/wsdl";
    const ENDPOINT_CONFIG_KEY = "object_search/usage_url";
    const CONFIG_STUB_PATH = 'omnius_service/object_search/use_stubs';

    /**
     * @param $params
     * @return mixed
     */
    public function executeObjectSearch($params)
    {
        $searchParams = $this->mapCustomerSearchParametersToXMLNodes($params);
        $this->setRequestHeaderInfo($searchParams);

        return $this->ObjectSearch($searchParams);
    }

    /**
     * Map the form search parameters for customer to an array with the structure needed for the XML that will be send to the service
     *
     * @access private
     * @param array $searchParams
     * @return array
     */
    private function mapCustomerSearchParametersToXMLNodes($searchParams)
    {
        $parametersMapping = [];
        // Set birthday default to null.

        foreach ($searchParams as $key => $value) {
            switch ($key) {
                case 'street':
                    $parametersMapping['Account']['Role']['Person']['ResidenceAddress']['StreetName'] = $value;
                    break;
                case 'no':
                    $parametersMapping['Account']['Role']['Person']['ResidenceAddress']['BuildingNumber'] = $value;
                    break;
                case 'city':
                    $parametersMapping['Account']['Role']['Person']['ResidenceAddress']['CityName'] = $value;
                    break;
                case 'zipcode':
                    $parametersMapping['Account']['Role']['Person']['ResidenceAddress']['PostalZone'] = $value;
                    break;
                case 'addNo':
                    $parametersMapping['Account']['Role']['Person']['ResidenceAddress']['HouseNumberAddition'] = $value;
                    break;
                default:
                    break;
            }
        }

        $parametersMapping['HeaderInfo']['Timestamp'] = date("Y-m-d\TH:i:s", time());

        return $parametersMapping;
    }
}
