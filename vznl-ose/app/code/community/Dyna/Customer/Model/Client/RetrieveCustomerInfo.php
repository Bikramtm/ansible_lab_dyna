<?php

/**
 * Class Dyna_Customer_Model_Client_RetrieveCustomerInfo
 */
class Dyna_Customer_Model_Client_RetrieveCustomerInfo extends Dyna_Service_Model_Client
{
    CONST CONFIG_STUB_PATH = 'customer_info/use_stubs'; // KUD
    CONST WSDL_CONFIG_KEY = 'customer_info/wsdl_customer_info';
    CONST ENDPOINT_CONFIG_KEY = 'customer_info/endpoint_customer_info';

    CONST STATUS_REASON_CODE = "OK";
    CONST PHONE_PRIVATE = "PRIVATE";
    CONST ADDRESS_TYPE_C = "C";
    CONST ADDRESS_TYPE_LO = "LO";
    CONST ADDRESS_TYPE_DL = "DL";
    CONST ADDRESS_TYPE_LEGAL = "L"; //@todo clarify what type of adress is this
    CONST ADDRESS_TYPE_USER = "U"; //@todo clarify what type of adress is this
    CONST ADDRESS_TYPE_BILLING = "B"; //@todo clarify what type of adress is this
    CONST ADDRESS_TYPE_SERVICE = "S";
    CONST ADDRESS_TYPE_SHIPPING = "SH";
    CONST ALL = 'ALL';
    CONST PARTY_TYPE_PRIVATE = "PRIVATE";
    CONST PARTY_TYPE_SOHO = "SOHO"; //@todo clarify what address type return the services for customer shipping address
    CONST PARTY_TYPE_SHORT_PRIVATE = "P";
    CONST PARTY_TYPE_SHORT_SOHO = "S";
    CONST CUSTOMER_SALUTATION_MR = "R";
    CONST CUSTOMER_SALUTATION_MRS = "S";
    CONST CUSTOMER_SALUTATION_NONE = "N";
    CONST CUSTOMER_SALUTATION_FIRMA = "F";
    CONST NON_OSF_ORDER_LINE_OF_BUSINESS_MOBILE = "Mobile";
    CONST NON_OSF_ORDER_LINE_OF_BUSINESS_MOBILE_POSTPAID = "Mobile_Postpaid";
    CONST NON_OSF_ORDER_LINE_OF_BUSINESS_MOBILE_PREPAID = "Mobile_Prepaid";
    CONST NON_OSF_ORDER_LINE_OF_BUSINESS_CABLE = "Cable";
    CONST NON_OSF_ORDER_LINE_OF_BUSINESS_DSL = "DSL";
    CONST NON_OSF_ORDER_TYPE_BAN = "BAN";
    CONST NON_OSF_ORDER_TYPE_SUBSCRIBER = "SUBSCRIBER";
    CONST NON_OSF_ORDER_ACTIVITY_SUSPENDED = "SUS";
    CONST NON_OSF_ORDER_ACTIVITY_CANCELLATION = "CAN";
    CONST NON_OSF_ORDER_ACTIVITY_RESTORE_FROM_SUSPENDED = "RSP";
    CONST KD_DEVICE_ENABLED_FEATURES = "Enabled features";
    CONST KD_DEVICE_SUPPORTED_FEATURES = "Supported features";

    // contains this code but the value is like below according to OMNVFDE-1512
    /*BAN Level
    CAN : Cancelation
    RCB : Resume from cancellation
    SUS : Suspension
    RSP : Restore from Suspend
    Subcriber level:
    SCH : Service Change
    RPS : Replace SOC
    CAN : Cancelation
    RCL : Resume from Cancel
    SUS : Suspension
    RSP : Restore Suspend*/
    CONST NON_OSF_ORDER_ACTIVITY_RESUME_FROM_CANCELLATION = "RCB";
    CONST NON_OSF_ORDER_ACTIVITY_SERVICE_CHANGE = "SCH";
    CONST NON_OSF_ORDER_ACTIVITY_RESTORE_SUSPEND = "RPS";
    CONST NON_OSF_ORDER_ACTIVITY_RESUME_FROM_CANCEL = "RCL"; //ban
    CONST NON_OSF_ORDER_STATUS_NEW = "NEW"; //subscriber
    CONST NON_OSF_ORDER_STATUS_IN_PROGRESS = "IN_PROGRESS"; //subscriber
    CONST NON_OSF_ORDER_STATUS_WAITING = "WAITING"; //subscriber

    /* according to OMNVFDE-1512
     * KD : NEW, IN_PROGRESS, WAITING , MANUAL WAITING
     * FN: ORDER_POSITION_IN_PROGRESS*/
    CONST NON_OSF_ORDER_STATUS_ORDER_POSITION_IN_PROGRESS = "ORDER_POSITION_IN_PROGRESS";
    CONST NON_OSF_ORDER_STATUS_MANUAL_WAITING = "MANUAL_WAITING";
    CONST CABLE_PRODUCT_CATEGORY_CLS = "CLS";
    CONST CABLE_PRODUCT_CATEGORY_KIP = "KIP";
    CONST CABLE_PRODUCT_CATEGORY_TV = "TV";
    // OVG-2632 part of bundle SOC should be VFGSAIOHK
    const KIAS_PART_OF_BUNDLE_SOC = ['VFGSAIOHK']; // KID
    const KIAS_ELIGIBLE_BUNDLE_SOC = 'VFAIO2MRK'; // KAA, KAD
    const KIAS_ELIGIBLE_BUNDLE_CATEGORY_NAME = 'Mobile_total';
    const KD_PART_OF_BUNDLE_SOC = ['VA1', 'VC1'];
    const KD_ELIGIBLE_BUNDLE_CATEGORY_NAME = 'Cable_total';

    // OMNVFDE-1843
    const FN_PART_OF_BUNDLE_SOC = ['V0987'];
    const FN_ELIGIBLE_BUNDLE_CATEGORY_NAME = 'DSL_total';
    protected $accountCategory = null;
    protected $customerDevices = null;
    protected $subscriptionCtn = null;
    protected $customersPriority = [
        Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD,
        Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN,
        Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS,
        self::ALL
    ];
    protected $serviceResponse = null;

    /**
     * OVG-703 keep the sharing group details from customer level in this array
     * so we can access them inside each subscription node
     */
    protected $sharingGroupsDetails = null;

    private $hasOnlyExcludedCable = false;
    private $hasExcludedCable = false;
    private $hasOnlyExcludedMobile = false;
    private $hasExcludedMobile = false;
    protected $xmlStubs = false; //it true, the local stubs can be provided in XML format instead on json for this client

    public static function getPartOfBundleSocs()
    {
        return array_merge(
            self::KIAS_PART_OF_BUNDLE_SOC,
            self::KD_PART_OF_BUNDLE_SOC,
            self::FN_PART_OF_BUNDLE_SOC
        );
    }

    public static function getEligibleBundleCategoriesNames()
    {
        return array(
            self::KIAS_ELIGIBLE_BUNDLE_CATEGORY_NAME,
            self::KD_ELIGIBLE_BUNDLE_CATEGORY_NAME,
            self::FN_ELIGIBLE_BUNDLE_CATEGORY_NAME
        );
    }

    /**
     * @param $params
     * @param $mode
     * @return mixed
     */
    public function executeRetrieveCustomerInfo($params = [], $mode = 1)
    {
        $dot = $this->getAccessor();
        $customerSession = Mage::getSingleton('customer/session');
        // In mode 1 the customer data is retrieved, other modes retrieve additional customer info, so customer should already be set on session
        $params['BANDetails'] = null;
        if ($mode == 2) {
            /** @var Dyna_Customer_Model_Customer $customer */
            $customer = $customerSession->getCustomer();
            $params["customerNumber"] = $customer->getCustomerNumber();
            $params["global_id"] = $customer->getCustomerGlobalId();

            $serviceCustomers = Mage::getSingleton('dyna_customer/session')->getServiceCustomers();
            $kiasCustomers = $dot->getValue($serviceCustomers, 'KIAS') ?: [];
            $marketCode = "";

            foreach ($kiasCustomers as $kiasCustomer) {
                $customerNumber = $dot->getValue($kiasCustomer, 'customer_number');
                foreach ($kiasCustomer['bans'] as $key => $ban) {
                    if ($ban == $customerNumber) {
                        $marketCode = $kiasCustomer['contract_types'][$key];
                    }
                }

                $params['BANDetails'][] = array(
                    'BAN' => $customerNumber,
                    'MarketCode' => $marketCode
                );
            }
        }

        $this->mapRequestParams($params, $mode);

        $this->setRequestHeaderInfo($params);
        $params['Mode'] = $mode;

        if ($mode == 2 && ($customerData = $customerSession->getCustomerMode2())) {
            return $customerData;
        }
        /**
         * call service for $mode (1 or 2)
         */
        $customerData = $this->RetrieveCustomerInfo($params, 'mode' . $mode);
        if ($mode == 2) {
            $this->mapCustomer($customerData, 2);
            //save mode2 data for feature usage if we need it
            $customerSession->setCustomerMode2($customerData);
        }

        return $customerData;
    }

    /**
     * @param $params
     * @param $mode
     * @return mixed
     */
    public function executeHouseholdRetrieveCustomerInfo($params = [], $mode = 1)
    {
        $dot = $this->getAccessor();
        $params['BANDetails'] = null;
        if ($mode == 2) {
            $serviceCustomers = Mage::getSingleton('dyna_customer/session')->getHouseholdServiceCustomers();
            $kiasCustomers = $dot->getValue($serviceCustomers, 'KIAS') ?: [];
            $marketCode = "";

            foreach ($kiasCustomers as $kiasCustomer) {
                $customerNumber = $dot->getValue($kiasCustomer, 'customer_number');
                foreach ($kiasCustomer['bans'] as $key => $ban) {
                    if ($ban == $customerNumber) {
                        $marketCode = $kiasCustomer['contract_types'][$key];
                    }
                }
                $params['BANDetails'][] = array(
                    'BAN' => $customerNumber,
                    'MarketCode' => $marketCode
                );
            }
        }

        $this->mapRequestParams($params, $mode);

        $this->setRequestHeaderInfo($params);
        $params['Mode'] = $mode;

        /** call service for $mode (1 or 2) */
        $customerData = $this->RetrieveCustomerInfo($params, 'mode' . $mode);
        if ($mode == 2) {
            $this->mapHouseholdCustomer($customerData, 2);
        }

        return $customerData;
    }

    /**
     * @return Omnius_Service_Model_DotAccessor
     */
    protected function getAccessor()
    {
        return Mage::getSingleton('omnius_service/dotAccessor');
    }

    /**
     * @param $params
     * @param $mode
     */
    protected function mapRequestParams(&$params, $mode)
    {
        // the non unique value for retrieveCustomerInfo and for searchCustomer response
        $params["Customer"]["ID"] = ($mode == 2) ? $params["global_id"] : $params["contactId"];
        // the unique value of the search customer response
        if ($mode == 1) {
            $params["BAN"] = $params['parentAccountNumberId'] ?? null;
        }

        unset($params["contactId"], $params['parentAccountNumberId']);
    }

    /**
     * @param $data
     * @param int $mode
     * @return Dyna_Customer_Model_Customer
     * @throws Exception
     */
    public function mapCustomer($data, $mode = 1)
    {
        /**
         * must exist both in mode1 and mode2
         */
        if (!isset($data["Customer"][0]["ParentAccountNumber"]["ID"])) {
            $data["customer_number"] = $data["Customer"]["ParentAccountNumber"]["ID"];
        } else {
            $data["customer_number"] = ($data["Customer"][0]["ParentAccountNumber"]["ID"]) ?? null;
        }

        if (!$data["customer_number"]) {
            $message = "No customer id received after executing RetrieveCustomerInfo service call (['Customer'][0]['ParentAccountNumber']['ID'] on mode " . $mode . ")";
            if ($mode == 1) {
                Mage::logException(new Exception($message));
            } else {
                Mage::log($message, null, "exception.log", true);
                return false;
            }
        }

        /** @var Dyna_Customer_Model_Customer $customerObject */
        $customerObject = Mage::getModel("customer/customer");
        if ($mode == 1) {
            $parsedData = $this->mapMode1($data);
        } else {
            $parsedData = $this->mapMode2($data);
        }
        //OMNVFDE-3315: All further activity for this account should be blocked if there is only a Cable account with status EN/NE
        // or a Mobile one with status T (VFDED1W3S-3657)
        if ($this->hasOnlyExcludedCable) {
            Mage::getSingleton('dyna_customer/session')->setHasOnlyExcludedCable($this->hasOnlyExcludedCable);
            return $customerObject;
        } elseif ($this->hasOnlyExcludedMobile) {
            Mage::getSingleton('dyna_customer/session')->setHasOnlyExcludedMobile($this->hasOnlyExcludedMobile);
            return $customerObject;
        }

        // Save all returned results to session
        Mage::getSingleton('dyna_customer/session')->setServiceCustomers($parsedData);

        $firstContract = null;
        $firstCustomer = [];

        foreach ($this->customersPriority as $priority) {
            if (isset($parsedData[$priority])) {
                $firstContract = array_shift($parsedData[$priority]);
                if (isset($parsedData[$priority]) && $firstContract['is_temporary'] == 0) {
                    $firstCustomer = $firstContract;
                    break;
                }
            }
        }

        if ($firstContract && $firstContract['is_temporary'] == 1) {
            // Verify if response contains only one customer and that customer is a temporary customer
            throw new Exception('Customer cannot be loaded as it is a temporary account: ' . $firstContract['customer_number']);
        }

        // by now $customerObject should be empty, so we'll try to load it from magento
        $customerNumber = isset($firstCustomer['customer_number']) ? $firstCustomer['customer_number'] : null;
        // check if current customer is prospect; if so, dont't retrieve it from magento
        $customerSession = Mage::getSingleton('customer/session');
        $prospectCustomer = $customerSession->getCustomer()->getIsProspect();

        $customerId = ($prospectCustomer || is_null($customerNumber)) ? '' : $customerObject->getIdByCustomerNumber($customerNumber);

        // If there is a customer id load customer from db and overwrite its data
        !$customerId ?: $customerObject = Mage::getModel('customer/customer')->load($customerId);

        $customerObject->addData($firstCustomer);


        if ($mode == 1 && !empty($addresses = $firstCustomer['postal_addresses'])) {
            foreach ($addresses as $addressData) {
                $address = Mage::getModel('customer/address');
                // Address collection will be updated by appending another address
                $address->addData($addressData->getData());
                // Add address to customer
                $customerObject->addSoapAddress($address);
            }
        }

        return $customerObject;
    }

    /**
     * @param $data
     * @param int $mode
     * @return Dyna_Customer_Model_Customer
     * @throws Exception
     */
    public function mapHouseholdCustomer($data, $mode = 1)
    {
        /**
         * must exist both in mode1 and mode2
         */
        if (!isset($data["Customer"][0]["ParentAccountNumber"]["ID"])) {
            $data["customer_number"] = $data["Customer"]["ParentAccountNumber"]["ID"];
        } else {
            $data["customer_number"] = ($data["Customer"][0]["ParentAccountNumber"]["ID"]) ?? null;
        }

        if (!$data["customer_number"]) {
            $message = "No customer id received after executing RetrieveCustomerInfo service call (['Customer'][0]['ParentAccountNumber']['ID'] on mode " . $mode . ")";
            if ($mode == 1) {
                Mage::logException(new Exception($message));
            } else {
                Mage::log($message, null, "exception.log", true);
                return false;
            }
        }
        if ($mode == 1) {
            $parsedData = $this->mapMode1($data);
        } else {
            $parsedData = $this->mapMode2($data, true);
        }

        // Save all returned results to session
        Mage::getSingleton('dyna_customer/session')->setHouseholdServiceCustomers($parsedData);


        $firstContract = null;
        $firstCustomer = [];

        foreach ($this->customersPriority as $priority) {
            if (isset($parsedData[$priority])) {
                $firstContract = array_shift($parsedData[$priority]);
                if (isset($parsedData[$priority]) && $firstContract['is_temporary'] == 0) {
                    $firstCustomer = $firstContract;
                    break;
                }
            }
        }

        if ($firstContract && $firstContract['is_temporary'] == 1) {
            // Verify if response contains only one customer and that customer is a temporary customer
            throw new Exception('Customer cannot be loaded as it is a temporary account: ' . $firstContract['customer_number']);
        }

        return $firstCustomer;
    }

    /**
     * @param $data
     * @return array
     */
    public function mapMode1($data)
    {
        // In case only one result is returned, change it to an array with one item
        if (empty($data['Customer'][0])) {
            $data['Customer'] = [$data['Customer']];
        }

        $serviceCustomers = $data['Customer'];

        $customers = [];
        $bans = $contractTypes = array();
        // TODO: if linked (more 'lc:RetrieveCustomerInfoResponse/lc:Customer' elements), then First Cable, then FN then Mobile Data
        foreach ($serviceCustomers as $customerData) {
            $this->accountCategory = $customerData['AccountCategory'];
            $this->checkExcludedCableAccounts($customerData['CustomerLifeCycleStatus'], count($serviceCustomers));
            $this->checkExcludedMobileAccounts($customerData['CustomerLifeCycleStatus'], count($serviceCustomers));
            // OMNVFDE-3315: If Cable account in status EN/NE and is part of a legal link. Message should be displayed. Only Active accounts in the link should be loaded
            if (($this->accountCategory == Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD && $this->hasExcludedCable) ||
                ($this->accountCategory == Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS && $this->hasExcludedMobile)) {
                continue;
            }

            $customers[$customerData['AccountCategory']][$customerData['ParentAccountNumber']['ID']] = array_merge(
                $this->mapCustomerData($customerData, $bans, $contractTypes),
                $this->mapBillingData($customerData),
                $this->mapPostalAddressData($customerData),
                $this->mapContactData($customerData)
            );
        }

        return $customers;
    }

    public function mapMode2($data, $household=false)
    {
        $dot = $this->getAccessor();
        // In case only one result is returned, change it to an array with one item
        if (empty($data['Customer'][0])) {
            $data['Customer'] = [$data['Customer']];
        }
        /** get customers from service */
        $serviceCustomers = $data['Customer'];
        /** get saved customers from session */
        if($household)
            $sessionCustomers = Mage::getSingleton('dyna_customer/session')->getHouseholdServiceCustomers() ?? [];
        else
            $sessionCustomers = Mage::getSingleton('dyna_customer/session')->getServiceCustomers() ?? [];

        /** parce service customers */
        foreach ($serviceCustomers as $customerData) {
            $this->accountCategory = $customerData['AccountCategory'];

            if (isset($customerData['SharingGroupDetails'])) {
                if (!is_array($customerData['SharingGroupDetails'][0])) {
                    $sharingGroupDetails = $customerData['SharingGroupDetails'];
                    unset($customerData['SharingGroupDetails']);
                    $customerData['SharingGroupDetails'][0] = $sharingGroupDetails;
                }

                $this->sharingGroupsDetails = $customerData['SharingGroupDetails'];
            }
            /**********************************************************
             * MAPPING CUSTOMER DEVICE
             *********************************************************/
            $devices = $dot->getValue($customerData, "Device");
            if ($devices) {
                $this->mapCustomerDevices($devices);
            }
            /**********************************************************
             * EOF MAPPING CUSTOMER DEVICE
             *********************************************************/
            if (isset($sessionCustomers[$customerData['AccountCategory']])) {
                if (empty($sessionCustomers[$customerData['AccountCategory']][$customerData['ParentAccountNumber']['ID']])) {
                    $sessionCustomers[$customerData['AccountCategory']][$customerData['ParentAccountNumber']['ID']] = array_merge(
                        $this->mapCustomerOrdersData($customerData),
                        $this->mapContracts($customerData)
                    );
                } else {
                    $sessionCustomers[$customerData['AccountCategory']][$customerData['ParentAccountNumber']['ID']] += array_merge(
                        $this->mapCustomerOrdersData($customerData),
                        $this->mapContracts($customerData)
                    );
                }
            }


            if ($customerData['AccountCategory'] == Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS) {
                $this->mapContractType($sessionCustomers[$customerData['AccountCategory']][$customerData['ParentAccountNumber']['ID']]);
            }

        }

        return $sessionCustomers;
    }

    /**
     * Map contract type (from mode1) to mode2 for KIAS
     * @param $customerData
     * @return mixed
     */
    function mapContractType(&$customerData)
    {
        $bans = $customerData['bans'] ?? array();
        foreach ($customerData['contracts'] as &$contract) {
            foreach ($contract['subscriptions'] as &$subscription) {
                foreach ($bans as $banKey => $ban) {
                    if ($subscription['id'] == $ban) {
                        $subscription['contract_type'] = $customerData['contract_types'][$banKey];
                        $contract['contract_type'] = $customerData['contract_types'][$banKey];
                    }
                }
            }
        }
    }

    /**
     * 1.Check if the customer has excluded cable account
     * 2.Check if the excluded cable account is the only account for this customer
     * If 2. is true => the customer will not be loaded. Notification will appear.
     * If only 1. is true => the customer will be loaded. Notification will appear.
     * @param $customerLifeCycleStatus
     */
    private function checkExcludedCableAccounts($customerLifeCycleStatus, $accountsCounter)
    {
        if ($this->accountCategory == Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD &&
            in_array($customerLifeCycleStatus, Dyna_Customer_Helper_Services::excludedCableAccountsLifeCycleStatuses())) {
            $this->hasExcludedCable = true;
            if ($accountsCounter == 1) {
                $this->hasOnlyExcludedCable = true;
            }
            Mage::getSingleton('dyna_customer/session')->setHasExcludedCableAccount(true);
        } else {
            $this->hasExcludedCable = false;
        }
    }

    /**
     * Same conditions from the above checkExcludedCableAccounts method apply
     * @param $customerLifeCycleStatus
     */
    private function checkExcludedMobileAccounts($customerLifeCycleStatus, $accountsCounter)
    {
        if ($this->accountCategory == Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS &&
            in_array($customerLifeCycleStatus, Dyna_Customer_Helper_Services::excludedMobileAccountsLifeCycleStatuses())) {
            $this->hasExcludedMobile = true;
            if ($accountsCounter == 1) {
                $this->hasOnlyExcludedMobile = true;
            }
            Mage::getSingleton('dyna_customer/session')->setHasExcludedMobileAccount(true);
        } else {
            $this->hasExcludedMobile = false;
        }
    }

    protected function mapCustomerData($customerData, &$bans, &$contractTypes)
    {
        $dot = $this->getAccessor();
        $bans[$customerData['AccountCategory']][] = $dot->getValue($customerData, 'ParentAccountNumber.ID');
        $contractTypes[$customerData['AccountCategory']][] = $dot->getValue($customerData, 'Contract.ContractType');

        $customerMapping = array(
            'bans' => $bans[$customerData['AccountCategory']],
            'contract_types' => $contractTypes[$customerData['AccountCategory']]
        );

        $customerType = $dot->getValue($customerData, 'PartyType');
        if ($customerType == static::PARTY_TYPE_SOHO) {
            /**********************************************************
             * PARSING COMPANY DATA
             *********************************************************/
            $customerMapping += [
                "company_name" => $dot->getValue($customerData, 'PartyLegalEntity.RegistrationName'),
                "company_id" => $dot->getValue($customerData, 'PartyLegalEntity.CompanyID'),
                "company_legal_form" => $dot->getValue($customerData, 'PartyLegalEntity.CompanyLegalForm'),
                // @todo check if this node really exists, no details provided in OMNVFDE-1297
                "company_additional_name" => $dot->getValue($customerData, 'PartyLegalEntity.CompanyAdditionalName'),
                "company_city" => $dot->getValue($customerData, 'PartyLegalEntity.RegistrationAddress.CityName'),
            ];
            /**********************************************************
             * EOF PARSING COMPANY DATA
             *********************************************************/
        }

        /**********************************************************
         * PARSING CUSTOMER DATA
         *********************************************************/


        $customerMapping += [
            "party_type" => $customerType,
            "account_class" => $dot->getValue($customerData, 'AccountClass'),
            "title" => $customerType == static::PARTY_TYPE_SOHO ? null : $dot->getValue($customerData, 'Role.Person.Title'),
            //First Name change for SOHO Customer OMNVFDE-3064
            "firstname" => $customerType == static::PARTY_TYPE_SOHO ? $dot->getValue($customerData, 'Contact.Name') : $dot->getValue($customerData, 'Role.Person.FirstName'),
            "lastname" => $customerType == static::PARTY_TYPE_SOHO ? '' : $dot->getValue($customerData, 'Role.Person.FamilyName'),
            "mobile_email" => $dot->getValue($customerData, 'Contact.ElectronicMail'),
            "mobile_telephone" => $dot->getValue($customerData, 'Contact.TelephoneNumber'),
            "dob" => $dot->getValue($customerData, 'Role.Person.BirthDate'),
            "global_id" => $dot->getValue($customerData, 'PartyIdentification.ID'),
            "customer_number" => $dot->getValue($customerData, 'ParentAccountNumber.ID'),
            "customer_password" => $dot->getValue($customerData, 'CustomerPassword'),
            "life_cycle_status" => $dot->getValue($customerData, 'CustomerLifeCycleStatus'),
            "customer_status" => !empty($dot->getValue($customerData, 'CustomerLifeCycleStatus')) ? $dot->getValue($customerData, 'CustomerLifeCycleStatus') : "",
            "dunning_status" => str_replace(array(' ', "\n"), array('', ''), $dot->getValue($customerData, 'DunningStatus')),
            "no_ordering_allowed" => $dot->getValue($customerData, 'NoOrderingAllowed'),
            "account_category" => $dot->getValue($customerData, 'AccountCategory'),
            "nationality_id" => $dot->getValue($customerData, 'Role.Person.NationalityID'),
            "identification_number" => $dot->getValue($customerData, 'Role.Person.IdentificationTypeNumber'),
            "salutation" => $dot->getValue($customerData, 'Role.Person.Salutation'),
            'last_billing_amount' => $dot->getValue($customerData, 'BillingDetails.LastBillAmount'),
            'last_billing_due_amount' => $dot->getValue($customerData, 'BillingDetails.LastBillDueAmount'),
            'last_back_out_amount' => $dot->getValue($customerData, 'BillingDetails.LastBackOutAmount'),
            'legacy_dunning_amount' => $dot->getValue($customerData, 'DunningAmountSystem.LegacyDunningAmount'),
            'display_dunning_amount' => $dot->getValue($customerData, 'DunningAmountSystem.DisplayDunningAmount')
        ];

        $customerMapping['is_temporary'] =
            false !== strpos($customerMapping['customer_number'], Dyna_Customer_Helper_Customer::TEMPORARY_CUSTOMER_PREFIX)
                ? 1 : 0;

        /**********************************************************
         * EOF PARSING CUSTOMER DATA
         *********************************************************/

        return $customerMapping;
    }

    /**
     * PARSING POSTAL ADDRESS
     * Used for check-serviceablity and checkout
     */
    protected function mapPostalAddressData($customerData)
    {
        $dot = $this->getAccessor();

        $postalAddresses = $dot->getValue($customerData, "PostalAddresses");
        $customerMappedAddresses = [];
        isset($postalAddresses[0]) ?: $postalAddresses = [$postalAddresses];
        foreach ($postalAddresses as $arrayData) {
            $postalAddress = new Varien_Object();
            $postalAddress->addData($this->mapAddress($arrayData));
            $customerMappedAddresses[] = $postalAddress;
        }

        return [
            "postal_addresses" => $customerMappedAddresses,
        ];
    }

    /**
     * Map contact data
     * @param $customerData
     * @return mixed
     */
    protected function mapContactData($customerData)
    {
        $dot = $this->getAccessor();
        // Customer phone numbers
        $customerMapping["contact_phone_numbers"] = [];
        $contactPhoneNumbers = $dot->getValue($customerData, "ContactPhoneNumber");

        if (!empty($contactPhoneNumbers)) {
            $contactPhoneNumbers = isset($contactPhoneNumbers[0]) ? $contactPhoneNumbers : [$contactPhoneNumbers];

            foreach ($contactPhoneNumbers as $phoneNumber) {
                $phoneData["country_code"] = isset($phoneNumber["CountryCode"]) ? $phoneNumber["CountryCode"] : null;
                $phoneData["local_area_code"] = !empty($phoneNumber["LocalAreaCode"]) ? $phoneNumber["LocalAreaCode"] : null;
                // In case when CountryCode and AreaCode fields are empty this field contains FullPhoneNumber.
                $phoneData["phone_number"] = !empty($phoneNumber["PhoneNumber"]) ? $phoneNumber["PhoneNumber"] : null;
                $phoneData["type"] = !empty($phoneNumber["Type"]) ? $phoneNumber["Type"] : null;

                $customerMapping["contact_phone_numbers"][] = $phoneData;

                // Set omnius customer phone number
                if (!empty($phoneData["phone_number"])) {
                    $customerMapping["phone_number"] = $phoneData["phone_number"];
                }
                if (!empty($phoneData["local_area_code"])) {
                    $customerMapping["local_area_code"] = $phoneData["local_area_code"];
                }
                if (!empty($phoneData["type"])) {
                    $customerMapping["phone_type"] = $phoneData["type"];
                }
            }
        }

        return $customerMapping;
    }

    /**
     * Map billing information Account & Billing list
     * @param $customerData
     * @return array
     */
    protected function mapBillingData($customerData)
    {
        $dot = $this->getAccessor();

        $billingDetails = new Varien_Object();
        $billingDetails->setData($this->mapBilling($customerData));
        return [
            "billing_account" => $billingDetails,
            'last_billing_amount' => $dot->getValue($customerData, 'BillingDetails.LastBillAmount'),
            'last_billing_due_amount' => $dot->getValue($customerData, 'BillingDetails.LastBillDueAmount'),
            'last_back_out_amount' => $dot->getValue($customerData, 'BillingDetails.LastBackOutAmount'),
            'legacy_dunning_amount' => $dot->getValue($customerData, 'DunningAmountSystem.LegacyDunningAmount'),
            'display_dunning_amount' => $dot->getValue($customerData, 'DunningAmountSystem.DisplayDunningAmount')
        ];
    }

    protected function mapAddress($address)
    {
        $dot = $this->getAccessor();
        $addressTypes = self::getAddressMapping();

        $addressType = null;
        if (isset($addressTypes[$dot->getValue($address, 'AddressTypeCode')])) {
            $addressType = $addressTypes[$dot->getValue($address, 'AddressTypeCode')];
        }

        $addressData = [
            // Unique address ID
            "address_id" => $dot->getValue($address, 'ID'),
            // Type of address (Billing, Legal etc)
            "address_type" => $addressType,
            "postcode" => $dot->getValue($address, 'PostalZone'),
            "post_box" => $dot->getValue($address, 'Postbox'),
            "street" => $dot->getValue($address, 'StreetName'),
            "house_number" => $dot->getValue($address, 'BuildingNumber'),
            "house_addition" => $dot->getValue($address, 'HouseNumberAddition'),
            "city" => $dot->getValue($address, 'CityName'),
            "district" => $dot->getValue($address, 'District') ?: $dot->getValue($address, 'Region'),
            "state" => $dot->getValue($address, 'CountrySubentity'),
            "country" => $dot->getValue($address, 'Country'),
        ];

        if (!array_filter($addressData)) {
            return [];
        }
        return $addressData;
    }

    /**
     * @return array
     */
    public static function getAddressMapping()
    {
        return array(
            static::ADDRESS_TYPE_LEGAL => Dyna_Customer_Model_Customer::DEFAULT_ADDRESS,
            static::ADDRESS_TYPE_C => Dyna_Customer_Model_Customer::DEFAULT_ADDRESS, //@todo determine what does this address types mean
            static::ADDRESS_TYPE_LO => Dyna_Customer_Model_Customer::DEFAULT_ADDRESS, //@todo determine what does this address types mean
            static::ADDRESS_TYPE_USER => Dyna_Customer_Model_Customer::USER_ADDRESS,
            static::ADDRESS_TYPE_BILLING => Mage_Customer_Model_Address_Abstract::TYPE_BILLING,
            static::ADDRESS_TYPE_SERVICE => Dyna_Customer_Model_Customer::SERVICE_ADDRESS,
            static::ADDRESS_TYPE_SHIPPING => Mage_Customer_Model_Address_Abstract::TYPE_SHIPPING,
            static::ADDRESS_TYPE_DL => Mage_Customer_Model_Address_Abstract::TYPE_SHIPPING,
        );
    }

    protected function mapBilling($customerData)
    {
        $dot = $this->getAccessor();

        $billingAccountData = $dot->getValue($customerData, 'BillingAccount');
        $iban = $dot->getValue($billingAccountData, 'ID');
        $bic = $dot->getValue($billingAccountData, 'BIC');
        $bankName = $dot->getValue($billingAccountData, 'FinancialInstitutionBranch.Name');
        $bankID = $dot->getValue($billingAccountData, 'FinancialInstitutionBranch.ID');
        $bankNumber = $dot->getValue($billingAccountData, 'FinancialInstitutionBranch.FinancialInstitution.ID');
        $password = $dot->getValue($customerData, 'CustomerPassword');
        $paymentMethod = $dot->getValue($billingAccountData, 'PaymentMethod');
        $partyIdentyfication = $dot->getValue($billingAccountData, 'PartyIdentyfication.ID');
        $nextBillCycleStartDate = $dot->getValue($billingAccountData, 'NextBillCycleStartDate');
        return [
            'bic' => $bic,
            'iban' => $iban,
            'bank_name' => $bankName,
            'bank_id' => $bankID,
            'bank_number' => $bankNumber,
            'password' => $password,
            'payment_method' => $paymentMethod,
            'party_identyfication' => $partyIdentyfication,
            'next_bill_cycle_start_date' => $nextBillCycleStartDate
        ];
    }

    protected function mapCustomerDevices($deviceData)
    {
        $dot = $this->getAccessor();
        $deviceDeliveryDate = false;
        /**
         * @var $helper Dyna_Checkout_Helper_Data
         */
        $helper = Mage::helper('dyna_checkout');

        //Expecting an array of contracts
        if (!isset($deviceData[0])) {
            $deviceData = [$deviceData];
        }
        $customerDevices = [];

        foreach ($deviceData as $device) {
            $deviceType = $dot->getValue($device, 'CommodityClassification.NatureCode');
            $deviceName = null;
            $sku = null;
            $maf = false;
            $deviceCode = $dot->getValue($device, 'StandardItemIdentification.ID');
            $subventionCode = '';

            if (($components = $dot->getValue($device, 'Component'))
                && ($this->accountCategory == Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN)
            ) {
                // Get first component as there should not be multiple Component nodes for one Device
                if (isset($components[0])) {
                    $components = $components[0];
                }
                foreach ($components['ComponentAdditionalData'] as $additional) {
                    if ($additional['Code'] === 'articleName' && ($additional['AdditionalValue'] === '/functions/hardware/rentedHardwareConfiguration/articleName/existing'
                        || $additional['AdditionalValue'] === '/functions/hardware/hardwareConfiguration/articleName/existing')) {
                        $deviceName = $additional['Value'];
                    } elseif ($additional['Code'] === 'articleNumber' && ($additional['AdditionalValue'] === '/functions/hardware/rentedHardwareConfiguration/articleNumber/existing'
                            || $additional['AdditionalValue'] === '/functions/hardware/hardwareConfiguration/articleNumber/existing')) {
                        $deviceCode = $additional['Value'];
                    } elseif ($additional['Code'] === 'subventionCode' && ($additional['AdditionalValue'] === '/functions/hardware/rentedHardwareConfiguration/subventionCode/existing'
                            || $additional['AdditionalValue'] === '/functions/hardware/hardwareConfiguration/subventionCode/existing')) {
                        $subventionCode = $additional['Value'];
                    }

                    if (!empty($additional['Code']) && $additional['Code'] === 'deliveryDate') {
                        $deviceDeliveryDate = $additional['Value'];
                    }

                    if ($additional['Code'] === 'hardwareType' && $additional['AdditionalValue'] === '/functions/hardware/rentedHardwareConfiguration/hardwareType/existing') {
                        $deviceType = $additional['Value'];
                    }
                }

                $articleNumberAddons = Mage::getModel('dyna_multimapper/addon')->getCollection()
                    ->addFieldToFilter(
                        [
                            'addon_additional_value',
                            'addon_additional_value'
                        ],
                        [
                            ['eq' => "/functions/hardware/rentedHardwareConfiguration/articleNumber/existing"],
                            ['eq' => "/functions/hardware/hardwareConfiguration/articleNumber/existing"]
                        ])
                    ->addFieldToFilter('addon_name', ['eq' => "articleNumber"])
                    ->addFieldToFilter('addon_value', ['eq' => $deviceCode])
                    ->addFieldToFilter('direction', ['in' => [Dyna_MultiMapper_Model_Mapper::DIRECTION_IN, Dyna_MultiMapper_Model_Mapper::DIRECTION_BOTH]]);

                $articleNumberMapperIds = [];
                foreach ($articleNumberAddons as $articleNumberAddon) {
                    $articleNumberMapperIds[] = $articleNumberAddon->getMapperId();
                }

                $subventionCodeAddons = Mage::getModel('dyna_multimapper/addon')->getCollection()
                    ->addFieldToFilter(
                        [
                            'addon_additional_value',
                            'addon_additional_value'
                        ],
                        [
                            ['eq' => "/functions/hardware/rentedHardwareConfiguration/subventionCode/existing"],
                            ['eq' => "/functions/hardware/hardwareConfiguration/subventionCode/existing"]
                        ])
                    ->addFieldToFilter('addon_name', ['eq' => "subventionCode"])
                    ->addFieldToFilter('addon_value', ['eq' => $subventionCode])
                    ->addFieldToFilter('direction', ['in' => [Dyna_MultiMapper_Model_Mapper::DIRECTION_IN, Dyna_MultiMapper_Model_Mapper::DIRECTION_BOTH]]);
                $subventionCodeMapperIds = [];
                foreach ($subventionCodeAddons as $subventionCodeAddon) {
                    $subventionCodeMapperIds[] = $subventionCodeAddon->getMapperId();
                }

                $mapperIds = array_intersect($articleNumberMapperIds, $subventionCodeMapperIds);
                if (empty($mapperIds)) {
                    continue;
                } else {
                    $mapperID = $mapperIds[0];
                }

                /** @var Dyna_MultiMapper_Helper_Data $h */
                $mapper = Mage::getModel('dyna_multimapper/mapper')->load($mapperID);
                if($mapper){
                    $sku = $mapper->getSku();
                    if($sku) {
                        /** @var Dyna_Catalog_Model_Product $product */
                        $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
                        if($product && $product->getId()) {
                            $maf = $helper->showNewFormattedPriced($product->getMaf(), true);
                            $deviceName = $product->getDisplayNameInventory() ?: $product->getName();
                            $productVisibility = Mage::helper('dyna_catalog')->getVisibility(Dyna_Catalog_Model_Product::PRODUCT_VISIBILITY_INVENTORY, $sku, $product);
                        }
                    }
                }

            } else {
                $deviceCode = $dot->getValue($device, 'StandardItemIdentification.ID');
            }

            $featureListType = $dot->getValue($device, 'FeatureListType');
            if ($featureListType && !isset($featureListType[0])) {
                $featureListType = [$featureListType];
            }

            if ($featureListType) {
                foreach ($featureListType as $feature) {
                    if ($feature['FeatureType'] == self::KD_DEVICE_SUPPORTED_FEATURES) {
                        $supportedFeatures = $feature['FeatureList'];
                        break;
                    }
                }

                foreach ($supportedFeatures as $supportedFeature) {
                    if ($supportedFeature['FeatureName'] === 'wlan') {
                        $wlanEnabled = $supportedFeature['FeatureValue'];
                    }
                    if ($supportedFeature['FeatureName'] === 'maxBandwidth') {
                        $maxBandwidth = $supportedFeature['FeatureValue'];
                    }
                }
            }
            $customerDevices[] = [
                'device_name' => $deviceName ?? $dot->getValue($device, 'Model'),
                'device_type' => $deviceType,
                'device_code' => $deviceCode,
                'device_delivery_date' => $deviceDeliveryDate,
                'customer_id' => $dot->getValue($device, 'BackEndCustomerID'),
                'service_line_id' => $dot->getValue($device, 'ServiceLineID'),
                'sku' => $sku,
                'maf' => $maf,
                'manufacturer' =>  $dot->getValue($device, 'Manufacturer'),
                'mac_address' =>  $dot->getValue($device, 'MacAddress'),
                'wlan_enabled' =>  $wlanEnabled ?? null,
                'max_bandwidth' =>  $maxBandwidth ?? null,
                'equipment_type' => $dot->getValue($device, 'EquipmentType'),
                'serial_number' => $dot->getValue($device, 'SerialNumber'),
                'ownership' => $dot->getValue($device, 'Ownership'),
                'feature_list_type' => $featureListType,
                'product_visibility' => $productVisibility ?? null,
            ];
        }
        $this->customerDevices = $customerDevices;
    }

    /**
     * Map contracts of customer
     * (mode1 is used to retrieve for KIAS the contractType per contract and later use it to make the request for mode2)
     * (mode2 is used to get all contracts an merge them with the ones from mode1)
     * @param $customerData
     * @param int $mode
     * @return array
     */
    public function mapContracts($customerData)
    {
        $dot = $this->getAccessor();

        /**********************************************************
         * PARSING CUSTOMER CONTRACTS
         *********************************************************/
        $customerContracts = [];
        if (isset($customerData['Contract'])
            && true === array_key_exists("ID", $customerData['Contract'])
        ) {
            $contractData = $dot->getValue($customerData, 'Contract');
        } elseif (isset($customerData['Contract']['Subscription'])
            && true === array_key_exists("ID", $customerData['Contract']['Subscription'])
        ) {
            $contractData = $dot->getValue($customerData['Contract'], 'Subscription');
        } else {
            $contractData = $dot->getValue($customerData, 'Contract');
        }

        //Expecting an array of contracts
        if (!isset($contractData[0])) {
            $contractData = [$contractData];
        }

        foreach ($contractData as $contract) {
            $contractInfo = $this->mapContract($contract);
            $customerContracts['contracts'][] = $contractInfo;
        }

        /**********************************************************
         * EOF PARSING CUSTOMER CONTRACTS
         *********************************************************/

        return $customerContracts;
    }

    /**
     * Map contract for customer
     * @param $contract
     * @param $mode
     * @return array
     */
    protected function mapContract($contract)
    {
        $dot = $this->getAccessor();

        $parsedContractData = [
            "contract_id" => $dot->getValue($contract, 'ID'),
            //Contract Type
            "contract_type" => $dot->getValue($contract, 'ContractType'),
            // Contract Start Date
            "contract_start_date" => $dot->getValue($contract, 'ValidityPeriod.StartDate') ? date_format(date_create($dot->getValue($contract, 'ValidityPeriod.StartDate')), 'Y-m-d') : null,
            // Contract End Date
            "contract_end_date" => $dot->getValue($contract, 'ValidityPeriod.EndDate') ? date_format(date_create($dot->getValue($contract, 'ValidityPeriod.EndDate')), 'Y-m-d') : null,
            // First Activation Date
            "contract_activation_date" => $dot->getValue($contract, 'ActivationDate') ? date_format(date_create($dot->getValue($contract, 'ActivationDate')), 'Y-m-d') : null,
            // Next Possible cancellation date
            "contract_possible_cancellation_date" => $dot->getValue($contract, 'PossibleCancellationDate') ? date_format(date_create($dot->getValue($contract, 'PossibleCancellationDate')), 'Y-m-d') : null,
            // Cancellation Notice Period
            "contract_cancellation_notice_period" => $dot->getValue($contract, 'CancellationNoticePeriod'),
            // Cancellation Notice Unit (D,W,M,Y)
            "contract_cancellation_notice_unit" => $dot->getValue($contract, 'CancellationNoticeUnit'),
            "contract_last_notification_date" => $dot->getValue($contract, 'LastNotificationDate'),
        ];

        // Adding subscription to current contract
        // @todo Example contains two subscription nodes - which still needs to be clarified as for a contract there should be only one subscription
        $subscriptions = $dot->getValue($contract, 'Subscription');
        $parsedContractData['subscriptions'] = [];

        if ($subscriptions) {
            if (!isset($subscriptions[0])) {
                $subscriptions = [$subscriptions];
            }
            foreach ($subscriptions as $subscription) {
                $this->subscriptionCtn = null;
                $parsedContractData['subscriptions'][] = $this->mapSubscription($subscription);
            }
        }

        // OMNVFDE-3013: Map FN Contract->Product nodes that contain the FN subscriptions
        if ($this->accountCategory == Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN) {
            $products = $dot->getValue($contract, 'Product');

            if ($products) {
                if (!isset($products[0])) {
                    $products = [$products];
                }
                foreach ($products as $product) {
                    $parsedContractData['products'][] = $this->mapProduct($product);
                }
            }
        }

        return $parsedContractData;
    }

    protected function mapSubscription($subscription)
    {
        $dot = $this->getAccessor();

        $subscriptionData = [
            // Reference of the Customer ID in the backend.
            "id" => $dot->getValue($subscription, 'ID'),
            // Reference of the CTN
            // Subscriber Value (R,S,G,P,T,B)
            "value_indicator" => $dot->getValue($subscription, 'Valueindicator'),
            "puk" => $dot->getValue($subscription, 'PUK'),
            "product_name" => $dot->getValue($subscription, 'Product.Name'),
            "contact" => $dot->getValue($subscription, 'Contact'),
            // Contract Start Date
            "contract_start_date" => $dot->getValue($subscription, 'ValidityPeriod.StartDate'),
            // Contract End Date
            "contract_end_date" => $dot->getValue($subscription, 'ValidityPeriod.EndDate'),
            "last_subsidy_date" => $dot->getValue($subscription, 'LastSubsidyDate'),
            "actual_month_of_contract" => $dot->getValue($subscription, 'ActualMonthOfContract'),
            "sim_number" => $dot->getValue($subscription, 'SimNumber'),
            "serial_number" => $dot->getValue($subscription, 'SerialNumber'),
            "in_minimum_duration" => (int)(strtolower($dot->getValue($subscription, 'InMinimumDurationPeriod')) === "y"),
        ];

        if (!empty($subscriptionData['contact']) && !array_filter($subscriptionData['contact'])) {
            unset($subscriptionData['contact']);
        }

        $products = $dot->getValue($subscription, 'Product');
        if ($products) {
            // Make sure that we are parsing an array of products
            if (!isset($products[0])) {
                $products = [$products];
            }
            $subscriptionProducts = array();

            foreach ($products as $product) {
                if ($mappedProduct = $this->mapProduct($product)) {
                    $subscriptionProducts[] = $mappedProduct;
                }
                $subscriptionData['standard_item_identification'] = $dot->getValue($product, 'StandardItemIdentification.ID');
            }

            $subscriptionData['products'] = $subscriptionProducts;
        }

        $sharingGroup = $dot->getValue($subscription, 'SharingGroupSubscriberDetails');
        if ($sharingGroup) {
            $subscriptionData['sharing_group'] = $this->mapSharingGroup($sharingGroup);
        }

        // For Cable (KD) and Fixed (FN) er get the addresses from Customer -> Contract -> Subscription -> Address
        $subscriptionAddresses = $dot->getValue($subscription, 'Address');

        if (!isset($subscriptionAddresses[0])) {
            $subscriptionAddresses = [$subscriptionAddresses];
        }

        // Building addresses for current subscription
        if (isset($subscriptionAddresses) && count($subscriptionAddresses)) {
            // Parse each addresses
            foreach ($subscriptionAddresses as $addressArray) {
                $subscriptionData["addresses"][] = $this->mapAddress($addressArray);
            }

            if (!array_filter($subscriptionData["addresses"])) {
                unset($subscriptionData["addresses"]);
            }
        }
        // EOF building addresses
        // get the ctn: for cable & FN is the first TN that will be found on a Subscription/Product/Component/TN node (in mode 1 like all the other details);
        // for KIAS is the Subscription/Ctn node (in mode2 - like the majority of details)
        if (!$this->subscriptionCtn) {
            $this->subscriptionCtn = $dot->getValue($subscription, 'Ctn');
        }
        $subscriptionData['ctn'] = $this->subscriptionCtn;

        return $subscriptionData;
    }


    protected function mapSharingGroup($sharingGroupDetails)
    {
        $dot = $this->getAccessor();

        // build sharing group data from the SharingGroupSubscriberDetails nodes
        $sharingGroupData = [
            'id' => $dot->getValue($sharingGroupDetails, 'GroupID'),
            'legacy_customer_id' => $dot->getValue($sharingGroupDetails, 'LegacyCustomerID'),
            'skeleton_contract_number' => $dot->getValue($sharingGroupDetails, 'SkeletonContractNumber'),
            'ban' => $dot->getValue($sharingGroupDetails, 'Ban'),
            'group_tariff_type' => $dot->getValue($sharingGroupDetails, 'GroupTariffType'),
            'member_type' => $dot->getValue($sharingGroupDetails, 'MemberType'),
            'tariff_option' => $dot->getValue($sharingGroupDetails, 'TariffOption'),
            'effective_date' => date('Y-m-d', strtotime($dot->getValue($sharingGroupDetails, 'EffectiveDate'))),
            'expiration_date' => date('Y-m-d', strtotime($dot->getValue($sharingGroupDetails, 'ExpirationDate')))
        ];

        // Get more info from Customer -> SharingGroupDetails saved before
        if ($this->sharingGroupsDetails) {
            foreach ($this->sharingGroupsDetails as $group) {
                if ($group['GroupID'] == $sharingGroupData['id']) {
                    $sharingGroupData['status'] = $group['GroupStatus'];
                    $sharingGroupData['group_type'] = $group['GroupType'];
                    $sharingGroupData['number_of_owners'] = (int)$group['NumberOfOwner'];
                    $sharingGroupData['number_of_members'] = (int)$group['NumberOfMember'];
                    $sharingGroupData['group_effective_date'] = date('Y-m-d', strtotime($group['EffectiveDate']));
                    $sharingGroupData['group_expiration_date'] = date('Y-m-d', strtotime($group['ExpirationDate']));

                    break;
                }
            }
        }

        return $sharingGroupData;
    }

    protected function mapProduct($product)
    {
        $dot = $this->getAccessor();
        $components = $dot->getValue($product, 'Component');
        $components = (isset($components[0])) ? $components : [$components];
        // Parse contact data for every subscription as it is needed in checkout flow
        $contact = [];
        $rawContactData = $dot->getValue($product, 'Contact');
        isset($rawContactData[0]) ?: $rawContactData = [$rawContactData];
        foreach ($rawContactData as $contactData) {
            $contact[] = $this->mapPerson($dot->getValue($contactData, 'Role.Person'));
        }

        $productId = $dot->getValue($product, 'StandardItemIdentification.ID');
        $productData = [
            'product_id' => $productId,
            'product_status' => $dot->getValue($product, 'Status.Text'),
            'product_category' => $dot->getValue($product, 'Category'),
            'commodity_classification_nature_code' => $dot->getValue($product, 'CommodityClassification.NatureCode'),
            'components' => $components,
            'contact' => $contact,
            'service_line_id' => $dot->getValue($product, 'StandardItemIdentification.ID')
        ];

        if (!array_filter($productData['contact'])) {
            unset($productData['contact']);
        }

        if ($this->accountCategory == Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD
            || $this->accountCategory == Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN
        ) {
            // get the ctn: for cable is the first TN that will be found on a Subscription/Product/Component/TN node (in mode 1 like all the other details);
            // for KIAS is the Subscription/Ctn node (in mode2 - like the majority of details)
            if (count($components)) {
                foreach ($components as $component) {
                    if (!empty($component['TN'])) {
                        if (is_array($component['TN']) && isset($component['TN'][0])) {
                            $component['TN'] = $component['TN'][0];
                        }
                        $this->subscriptionCtn = $component['TN'];
                        break;
                    }
                }
            }

            // map devices to products
            $deviceId = $dot->getValue($product, 'StandardItemIdentification.ID');
            $backEndCustomerId = $dot->getValue($product, 'BackEndCustomerID');

            $productData['devices'] = [];

            if ($deviceId && $this->customerDevices) {
                foreach ($this->customerDevices as $customerDevice) {
                    if ($this->accountCategory == Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN) {
                        /**
                         * according to the mapping and OMNVFDE-3013 we have to map:
                         * Device->ServiceLineID points to Component->ComponentAdditionalData->Value (with code == 'function@ID')
                         */
                        if (isset($components[0])) {
                            $components = $components[0];
                        }
                        foreach ($components as $component) {
                            foreach ($component as $additional) {
                                if (isset($additional['Code']) && $additional['Code'] == "function@ID") {
                                    if ($additional['Value'] == $customerDevice['service_line_id']) {
                                        $productData['devices'][] = $customerDevice;
                                        break(2);
                                    }
                                }
                            }
                        }
                    } else {
                        /**
                         * according to the mapping and OMNVFDE-1976 we have to map:
                         * "Device Element has 2 pointers!
                         * 1. BackendCustomerID points to Customer/ParentAccountNumber
                         * 2. ServiceLineID points to lc:retrievecustomerinforesponse>customer>Contract>subscription>product>standarditemidentification>id, withing Pointer 1"
                         */
                        if ($customerDevice['service_line_id'] == $deviceId &&
                            $customerDevice['customer_id'] == $backEndCustomerId
                        ) {
                            $productData['devices'][] = $customerDevice;
                        }
                    }

                }
            }
        } else {
            $devicesData = $dot->getValue($product, 'Device');
            $productData['devices'] = $this->mapDevices($devicesData);
        }

        if (!array_filter($productData['devices'])) {
            unset($productData['devices']);
        }

        return $productData;
    }

    protected function mapPerson($person)
    {
        $dot = $this->getAccessor();
        $mappedPerson = [
            "firstname" => $dot->getValue($person, "FirstName"),
            "lastname" => $dot->getValue($person, "FamilyName"),
            "nationality_id" => $dot->getValue($person, "NationalityID"),
            "dob" => $dot->getValue($person, "BirthDate"),
            "phone_number" => $dot->getValue($person, "ContactPhoneNumber"),
            "title" => $dot->getValue($person, "Title"),
            "salutation" => $dot->getValue($person, "Salutation"),
            "status" => [
                "text" => $dot->getValue($person, "Status.Text"),
            ],

        ];

        if (!array_filter($mappedPerson['status'])) {
            unset($mappedPerson['status']);
        }
        if (!array_filter($mappedPerson)) {
            return [];
        }

        return $mappedPerson;
    }

    protected function mapDevices($deviceData)
    {
        $dot = $this->getAccessor();

        /**********************************************************
         * PARSING PRODUCTS DEVICES
         *********************************************************/
        $customerDevices = [];
        if (!$deviceData) {
            return $customerDevices;
        }
        //Expecting an array of contracts
        if (!isset($deviceData[0])) {
            $deviceData = [$deviceData];
        }

        foreach ($deviceData as $device) {
            $customerDevices[] = [
                'device_type' => $dot->getValue($device, 'CommodityClassification.NatureCode'),
                'device_code' => $dot->getValue($device, 'StandardItemIdentification.ID'),
            ];
        }
        /**********************************************************
         * EOF PARSING PRODUCTS DEVICES
         *********************************************************/

        return $customerDevices;
    }


    public function mapBillingDetails($data, $parentAccountNumberId)
    {
        $customers = [];

        // In case only one result is returned do not verify the ID just return the billing data of the customer
        if (true === array_key_exists("PartyIdentification", $data["Customer"])) {
            return $this->mapBilling($data["Customer"]);
            // else make sure to return the information for the given customer ID
        } else {
            if (count($data['Customer']) > 1) {
                foreach ($data['Customer'] as $customerData) {
                    if ($customerData["ParentAccountNumber"]["ID"] == $parentAccountNumberId) {
                        $customers = $this->mapBilling($customerData);
                        break;
                    }
                }
            }
        }

        return $customers;
    }

    /**
     * @param $data
     * @return array
     */
    public function mapHouseholdBillingDetails($data)
    {
        $customers = [];

        // In case only one result is returned do not verify the ID just return the billing data of the customer
        if (array_key_exists("PartyIdentification", $data["Customer"])) {
            $results = $this->mapBilling($data["Customer"]);
            $results['account_category'] = $data['Customer']['AccountCategory'];
            return array($results);
        } else {
            if (count($data['Customer']) > 1) {
                $i = 0;
                foreach ($data['Customer'] as $customerData) {
                    $customers[$i] = $this->mapBilling($customerData);
                    $customers[$i]['account_category'] = $customerData['AccountCategory'];
                    $i++;
                }
            }
        }

        return $customers;
    }

    /**
     * Map customer orders for mobile/cable/dsl from mode2
     *
     * @param $customerData
     * @return array
     */
    public function mapCustomerOrdersData($customerData)
    {
        $dot = $this->getAccessor();
        $data = [];

        if (array_key_exists("PendingOrder", $customerData)) {
            $pendingOrders = $customerData['PendingOrder'];

            // if is a single result make it an array with 1 element
            if ($dot->getValue($pendingOrders, 'LineOfBusiness')) {
                $pendingOrders = [0 => $pendingOrders];
            }

            foreach ($pendingOrders as $order) {
                switch (strtolower($dot->getValue($order, 'LineOfBusiness'))) {
                    case strtolower(self::NON_OSF_ORDER_LINE_OF_BUSINESS_MOBILE):
                    case strtolower(self::NON_OSF_ORDER_LINE_OF_BUSINESS_MOBILE_POSTPAID);
                    case strtolower(self::NON_OSF_ORDER_LINE_OF_BUSINESS_MOBILE_PREPAID):
                        $data['orders'][strtolower(self::NON_OSF_ORDER_LINE_OF_BUSINESS_MOBILE)][] = [
                            'created_date' => date("d.m.Y", strtotime($dot->getValue($order, 'CreationDate'))),
                            'planned_date' => date("d.m.Y", strtotime($dot->getValue($order, 'ExecutionPlannedDate'))),
                            'order_id' => $dot->getValue($order, 'OrderID'),
                            'type_request_level' => $dot->getValue($order, 'Detail.FutureRequestLevel'),
                            'subscriber_number' => $dot->getValue($order, 'Detail.SubscriberNumber'),
                            'activity_code' => $dot->getValue($order, 'OrderActivity'),
                            'activity_param' => $dot->getValue($order, 'Detail.FutureRequestAdditionalParam'),
                            'future_activity_code' => $dot->getValue($order, 'Detail.FutureRequestActivityCode'),
                            'future_reason_code' => $dot->getValue($order, 'Detail.FutureRequestReasonCode'),
                            'line_of_business' => $dot->getValue($order, 'LineOfBusiness'),
                            'legacy_customer_id' => $dot->getValue($order, 'LegacyCustomerID'),
                        ];
                        break;
                    case strtolower(self::NON_OSF_ORDER_LINE_OF_BUSINESS_CABLE):
                        $data['orders'][strtolower(self::NON_OSF_ORDER_LINE_OF_BUSINESS_CABLE)][] = [
                            'created_date' => date("d.m.Y", strtotime($dot->getValue($order, 'CreationDate'))),
                            'creator' => $dot->getValue($order, 'Creator'),
                            'order_id' => $dot->getValue($order, 'OrderID'),
                            'status' => $dot->getValue($order, 'Status'),
                            'kip_category' => $dot->getValue($order, 'Detail.KipCategory'),
                            'tv_category' => $dot->getValue($order, 'Detail.TvCategory'),
                            'cls_category' => $dot->getValue($order, 'Detail.ClsCategory'),
                            'line_of_business' => $dot->getValue($order, 'LineOfBusiness'),
                            'legacy_customer_id' => $dot->getValue($order, 'LegacyCustomerID'),
                            'subscriber_number' => $dot->getValue($order, 'Detail.SubscriberNumber'),
                        ];
                        break;
                    case strtolower(strtolower(self::NON_OSF_ORDER_LINE_OF_BUSINESS_DSL)):
                        $data['orders'][strtolower(self::NON_OSF_ORDER_LINE_OF_BUSINESS_DSL)][] = [
                            'created_date' => date("d.m.Y", strtotime($dot->getValue($order, 'CreationDate'))),
                            'planned_date' => date("d.m.Y", strtotime($dot->getValue($order, 'ExecutionPlannedDate'))),
                            'order_id' => $dot->getValue($order, 'OrderID'),
                            'status' => $dot->getValue($order, 'Status'),
                            'order_activity' => $dot->getValue($order, 'OrderActivity'),
                            'line_of_business' => $dot->getValue($order, 'LineOfBusiness'),
                            'subscriber_number' => $dot->getValue($order, 'Detail.SubscriberNumber'),
                            'legacy_customer_id' => $dot->getValue($order, 'LegacyCustomerID'),
                        ];
                        break;
                }
            }
        }

        return $data;
    }
}
