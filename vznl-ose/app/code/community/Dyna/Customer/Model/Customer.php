<?php

use Symfony\Component\ExpressionLanguage\ExpressionLanguage;

class Dyna_Customer_Model_Customer extends Omnius_Customer_Model_Customer_Customer
{
    const ACCOUNT_CATEGORY_MOBILE_POSTPAID = "Mobile_Postpaid";
    const ACCOUNT_CATEGORY_MOBILE_PREPAID = "Mobile_Prepaid";
    const ACCOUNT_CATEGORY_FIXED = "DSL";
    const ACCOUNT_CATEGORY_CABLE = "Cable";

    const DEFAULT_ADDRESS = "legal";
    const SERVICE_ADDRESS = "service";
    const POSTAL_ADDRESS = "P";
    const SUBSCRIPTION_ADDRESS = "S";
    const USER_ADDRESS = 'user';
    // DE country code
    const COUNTRY_CODE = '0049';
    const SHORT_COUNTRY_CODE = '49';
    protected static $productCategoryToPackageType = array(
        "kip" => Dyna_Catalog_Model_Type::TYPE_CABLE_INTERNET_PHONE,
        "kd" => Dyna_Catalog_Model_Type::TYPE_CABLE_TV,
        "tv" => Dyna_Catalog_Model_Type::TYPE_CABLE_TV,
        "cls" => Dyna_Catalog_Model_Type::TYPE_CABLE_INDEPENDENT,
        "dsl" => Dyna_Catalog_Model_Type::TYPE_FIXED_DSL,
        "fn" => Dyna_Catalog_Model_Type::TYPE_FIXED_DSL,
        "lte" => Dyna_Catalog_Model_Type::TYPE_LTE,
        "kias" => Dyna_Catalog_Model_Type::TYPE_MOBILE,
    );
    /** @var array of Varien_Object */
    protected $soapAddresses = [];
    protected $hardwareSerials = [];
    /** @var  Dyna_Catalog_Model_Smartcard */
    protected $ownSmartcard = null;
    /** @var  Dyna_Catalog_Model_Receiver */
    protected $ownReceiver = null;

    /** @var array  */
    // This is where we keep the data retrieved from services for current customer    /** @var null Varien_Object */
    protected $customerModem = null;

    protected $retrievedCustomerInfoData = null;
    /** @var Varien_Object */
    protected $addresses = null;
    /** @var string */
    protected $phoneNumber = null;
    /** @var string */
    protected $phoneLocalAreaCode = null;
    /** @var string */
    protected $emailAddress = null;
    /** @var string */
    protected $serviceAddressId = null;
    /** @var array  */
    protected $eligibleBundles = [];

    /**
     * Set the addresses received from RetrieveCustomerInfo service call
     * @param $address
     * @return $this
     */
    public function addSoapAddress($address)
    {
        $this->soapAddresses[] = $address;

        return $this;
    }

    /**
     * Get services customer id (customer number)
     * If account category is sent (see above account category constants) will send only if customer is known to this stack
     * @return mixed
     */
    public function getCustomerGlobalId($accountCategory = null)
    {
        $globalId = $this->getData('global_id');

        // if account category is sent, check against that category
        if ($accountCategory) {
            return $accountCategory == Dyna_Superorder_Helper_Client::getPackageType(Mage::helper('dyna_customer/customer')->getCustomerDataAccountCategory()) ? $globalId : null;
        }

        return $globalId;
    }

    /**
     * Determines whether or not customer belongs to a certain account category
     * @param $accountCategory
     * @return bool
     */
    public function belongsToAccountCategory($accountCategory)
    {
        /** @var Dyna_Customer_Model_Session $serviceCustomers */
        $serviceCustomers = Mage::getSingleton('customer/session')->getServiceCustomers();
        $belongs = false;

        if (!$this->isCustomerLoggedIn() || !$serviceCustomers) {
            return $belongs;
        }

        switch (strtolower($accountCategory)) {
            case strtolower(static::ACCOUNT_CATEGORY_FIXED) :
                if (isset($serviceCustomers[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN])) {
                    $belongs = true;
                }
                break;
            case strtolower(static::ACCOUNT_CATEGORY_MOBILE_POSTPAID) :
                $belongs = $this->hasContractType(Dyna_Mobile_Model_Product_Attribute_Option::MARKET_CODE_MMC);
                break;
            case strtolower(static::ACCOUNT_CATEGORY_MOBILE_PREPAID) :
                $belongs = $this->hasContractType(Dyna_Mobile_Model_Product_Attribute_Option::MARKET_CODE_MMO);
                break;
            case strtolower(static::ACCOUNT_CATEGORY_CABLE):
                if (isset($serviceCustomers[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD])) {
                    $belongs = true;
                }
                break;
            default:
                break;
        }

        return $belongs;
    }

    /**
     * Determine whether or not current customer has a certain contract type in a certain stack
     * @param $contractType
     * @return bool
     */
    public function hasContractType($contractType, $inStack = null)
    {
        /** @var Dyna_Customer_Model_Session $serviceCustomers */
        $serviceCustomers = Mage::getSingleton('customer/session')->getServiceCustomers();

        if (!$serviceCustomers) {
            return false;
        }

        foreach ($serviceCustomers as $accountType => $subscriptions) {
            // skip customers that are not KIAS
            if ($inStack && $accountType !== Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS) {
                continue;
            }

            foreach ($subscriptions as $subscription) {
                if (isset($subscription['contract_types'])) {
                    if (in_array($contractType, $subscription['contract_types'])) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * Retrieve the list of saved shopping carts
     *
     * @param int $customerId
     * @param string $customerNumber
     * @return array
     */
    public function getShoppingCartsForCustomer($customerId = null, $customerNumber = null)
    {
        /** @var Mage_Sales_Model_Resource_Quote_Collection $carts */
        $carts = Mage::getModel('sales/quote')->getCollection();
        if($customerId) {
            $carts->addFieldToFilter('main_table.customer_id',$customerId);
        } else if($customerNumber) {
            $carts->addFieldToFilter('main_table.customer_number',$customerNumber);
        }
        $carts->getSelect()->where(
            '(
                -- saved shopping cart
                main_table.cart_status = \'' . Omnius_Checkout_Model_Sales_Quote::CART_STATUS_SAVED . '\'
                  AND main_table.is_offer = \'0\'
                )
                OR (
                  -- offer
                  main_table.cart_status IS NULL
                  AND main_table.is_offer = \'1\'
                  AND main_table.store_id = ' . Mage::app()->getStore()->getId() . '
                  AND main_table.quote_parent_id IS NULL
                )'
        );

        $carts->setOrder('main_table.created_at', 'desc');

        $carts->getSelect()->joinLeft(array('orders' => 'sales_flat_order'), 'main_table.entity_id = orders.quote_id', array('orders.entity_id as order_id'));
        $carts->getSelect()->group('main_table.entity_id');

        $result = [];
        $offerLifetime = Mage::helper('omnius_checkout')->getOfferLifetime();
        $nowTimestamp = Mage::getModel('core/date')->timestamp();
        foreach ($carts as $cart) {
            $validationMessages = [];
            /** @var Dyna_Checkout_Model_Sales_Quote $cart */
            if ($cart->getOrderId()) {
                continue;
            }
            $tempPackages = $cart->getPackages();
            $packages = [];
            if (count($tempPackages) == 0) {
                continue;
            }

            $cartIsValid = true;
            foreach ($tempPackages as $package) {
                /** @var Dyna_Package_Model_Package $packageModel */
                $packageModel = $cart->getCartPackage($package['package_id']);
                $packageModel->setData('items', $package['items']);
                $packages[$packageModel->getPackageId()] = $packageModel;
            }


            if ($cart->getIsOffer()) {
                // Ensure no offer with risk is shown
                if ($cart->getCartStatus() == Omnius_Checkout_Model_Sales_Quote::CART_STATUS_RISK) {
                    continue;
                }
                $quoteTimestamp = Mage::getModel('core/date')->timestamp(strtotime($cart->getCreatedAt()));
                $quoteTimestamp = strtotime("+$offerLifetime days", $quoteTimestamp);
                $quoteTimestamp = strtotime(date("Y-m-d 23:59:59", $quoteTimestamp));
                $days_between = ceil(($quoteTimestamp - $nowTimestamp) / 86400);
                $cart->setOfferExpireDays($days_between);
            }

            if ($cartIsValid && $cart->getIsOffer()) {
                /** @var Dyna_Validators_Helper_Offer $offerValidatorHelper */
                $offerValidatorHelper = Mage::helper('dyna_validators/offer');
                $validationResults = $offerValidatorHelper->validate($cart);

                foreach ($validationResults as $validationResult) {
                    if (!$validationResult->getIsValid()) {
                        // found one error in the offer, stopping
                        $cartIsValid = false;

                        if ($validationResult->getShowMessage()) {
                            $validationMessages[] = $validationResult->getMessage();
                        }
                    }
                }
            }

            $result[$cart->getId()]['cart'][] = $cart;
            $result[$cart->getId()]['packages'][] = $packages;
            $result[$cart->getId()]['valid'] = $cartIsValid;
            $result[$cart->getId()]['validationMessages'] = $validationMessages;
        }

        return $result;

    }

    /**
     * Get customer salutation
     *
     * @return int
     */
    public function getSalutation($returnNumberic = false)
    {
        if ($this->isProspect()) {
            $gender = ($this->getIsBusiness() && $this->getContractantGender() != null) ? (int)$this->getContractantGender() : $this->getGender();
            switch ($gender) {
                case 'madam' :
                    $response = 2;
                    break;
                default:
                    $response = 1;
                    break;
            }
        } else {
            $salutation = $this->getData("salutation");
            switch ($salutation) {
                case Dyna_Customer_Model_Client_RetrieveCustomerInfo::CUSTOMER_SALUTATION_MR:
                    $response = 1;
                    break;
                case Dyna_Customer_Model_Client_RetrieveCustomerInfo::CUSTOMER_SALUTATION_MRS:
                    $response = 2;
                    break;
                case Dyna_Customer_Model_Client_RetrieveCustomerInfo::CUSTOMER_SALUTATION_FIRMA:
                    $response = 3;
                    break;
                case Dyna_Customer_Model_Client_RetrieveCustomerInfo::CUSTOMER_SALUTATION_NONE:
                default:
                    $response = 0;
                    break;
            }
        }

        // If this return is changed to other than 0, 1, 2 or three, consider updating checkout steps
        return $response;
    }

    /**
     * Get customer salutation - string version
     *
     * @return string
     */
    public function getSalutationString()
    {
        switch( $this->getSalutation() ) {
            case 1:
                return Mage::helper('core')->__('Mr.');
            case 2:
                return Mage::helper('core')->__('Mrs.');
        }
        return "";
    }

    /**
     * Check whether or not a customer is prospect (has saved shopping cart)
     * @todo implement it - customer number needs to be mapped to a customer id
     * @return bool
     */
    public function isProspect()
    {
        return (bool)$this->getData('is_prospect');
    }

    /**
     * Return company legal form
     *
     * @return string
     */
    public function getCompanyLegalForm()
    {
        return $this->getData("company_legal_form");
    }

    /**
     * Return company registration number
     *
     * @return string
     */
    public function getCompanyRegistrationNumber()
    {
        return $this->getData("company_id");
    }

    /**
     * Return true if customer has company_id
     *
     * @return string
     */
    public function getCommercialRegister()
    {
        if (false === empty($this->getData("company_id"))) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Return company registration city received through services
     * @return mixed
     */
    public function getCompanyRegistrationCity()
    {
        return $this->getData("company_city");
    }

    /**
     * Return customer address house addition from customer address
     */
    public function getHouseAddition($type = self::SUBSCRIPTION_ADDRESS)
    {
        return $this->handleAddress($type)
            ? htmlspecialchars($this->handleAddress($type)->getHouseAddition())
            : "";
    }

    /**
     * Retrieving legal address from first of type legal found in subscription list
     * @return Mage_Customer_Model_Address|null
     */
    public function getAddress($type = self::DEFAULT_ADDRESS)
    {
        foreach ($this->getSoapAddresses() as $address) {
            if ($address->getAddressType() == $type) {
                return $address;
            }
        }

        return null;
    }

    /**
     * Gets the specified customer address
     * @return Mage_Customer_Model_Address|null
     */
    public function handleAddress($type = self::SUBSCRIPTION_ADDRESS)
    {
        if ($type == self::SUBSCRIPTION_ADDRESS) {
            return $this->getAddress();
        } else if ($type == self::POSTAL_ADDRESS) {
            return !$this->getPostalAddress()->getData() ? $this->getAddress() : $this->getPostalAddress();
        }
        return null;
    }

    /**
     * Get the addresses received from RetrieveCustomerInfo service call
     * @return array of Varien_Object
     */
    public function getSoapAddresses()
    {
        return $this->soapAddresses;
    }

    /**
     * Get district from customer address
     * @return mixed
     */
    public function getDistrict($type = self::SUBSCRIPTION_ADDRESS)
    {
        return $this->handleAddress($type)
            ? $this->handleAddress($type)->getDistrict()
            : "";
    }

    /**
     * Return the current customer country
     * @return string
     */
    public function getCountry()
    {
        return $this->getData('country') ?: "Deutschland";
    }

    /**
     * Get full address info for current customer in the following format:
     * Mr. Full Customer Name,
     * Street, HouseNo
     * Postal Code, City
     * Country
     * @param boolean $isSohoCustomer
     * @param boolean $addState
     * @return string
     */
    public function getFullAddressInfo($isSohoCustomer = false, $addState = false)
    {
        if (!$this->isCustomerLoggedIn()) {
            return "";
        }

        $fullAddressInfo =  "";
        $fullAddressInfo = $fullAddressInfo
            . ( $isSohoCustomer ? $this->getCompanyName() . " " . $this->getCompanyAdditionalName() . "<br>" : "" )
            . ( $this->getSalutationString() != "" ? $this->getSalutationString() . " " : "" )
            . ( $this->getTitle() != "" ? $this->getTitle() . " " : "" )
            . $this->getName() . "<br>"
            . $this->getStreet(Dyna_Customer_Model_Customer::POSTAL_ADDRESS)
            . " " . $this->getHouseNo(Dyna_Customer_Model_Customer::POSTAL_ADDRESS) . "<br>"
            . $this->getPostalCode(Dyna_Customer_Model_Customer::POSTAL_ADDRESS)
            . " " . $this->getCity(Dyna_Customer_Model_Customer::POSTAL_ADDRESS)
            . ($addState ? " " . $this->getState(Dyna_Customer_Model_Customer::POSTAL_ADDRESS) : "")
        ;

        return strip_tags($fullAddressInfo,'<br>');
    }

    /**
     * Check if customer is logged in
     */
    public function isCustomerLoggedIn()
    {
        if($this->isProspect()) {
            return $this->getId();
        }

        return $this->getCustomerNumber();
    }

    /**
     * Get services customer id (customer number)
     * @return mixed
     */
    public function getCustomerNumber()
    {
        return $this->getData('customer_number');
    }

    /**
     * Get company name for business type customer
     * @return string
     */
    public function getCompanyName()
    {
        return $this->getData("company_name");
    }

    /**
     * Get company additional name for business type customer
     * @return mixed
     */
    public function getCompanyAdditionalName()
    {
        return $this->getData('company_additional_name');
    }

    /**
     * Return translated salutation for customer
     * @return string
     */
    public function getTitle()
    {
        return htmlspecialchars($this->getData("title"));
    }

    /**
     * Return customer full name
     * @return string|null
     */
    public function getName()
    {
        if ($this->getFirstName() || $this->getLastName()) {
            return $this->getFirstName() . " " . $this->getLastName();
        }

        return null;
    }

    /**
     * Get customer (if exists) first name
     * @return string
     */
    public function getFirstName()
    {
        return htmlspecialchars($this->getData('firstname')) ?: htmlspecialchars($this->getData('first_name'));
    }

    /**
     * Get customer (if exists) last name
     * @return string
     */
    public function getLastName()
    {
        return  htmlspecialchars($this->getData('lastname')) ?: htmlspecialchars($this->getData('last_name'));
    }

    /**
     * Get street from customer address
     * @return mixed
     */
    public function getStreet($type = self::SUBSCRIPTION_ADDRESS)
    {
        return $this->handleAddress($type)
            ? htmlspecialchars($this->handleAddress($type)->getData('street'))
            : "";
    }

    /**
     * Get house number from customer address
     * @return mixed
     */
    public function getHouseNo($type = self::SUBSCRIPTION_ADDRESS)
    {
        return $this->handleAddress($type)
            ? htmlspecialchars($this->handleAddress($type)->getHouseNumber())
            : "";
    }

    /**
     * Get postal code from customer address
     * @return mixed
     */
    public function getPostalCode($type = self::SUBSCRIPTION_ADDRESS)
    {
        return $this->handleAddress($type)
            ? htmlspecialchars($this->handleAddress($type)->getPostcode())
            : "";
    }

    /**
     * Get city from customer address
     * @return mixed
     */
    public function getCity($type = self::SUBSCRIPTION_ADDRESS)
    {
        return $this->handleAddress($type)
            ? htmlspecialchars($this->handleAddress($type)->getCity())
            : "";
    }

    /**
     * Get state from customer address
     * @return string
     */
    public function getState($type = self::SUBSCRIPTION_ADDRESS)
    {
        return $this->handleAddress($type)
            ? $this->handleAddress($type)->getState()
            : "";
    }

    /**
     * Get local area code
     * @return mixed
     */
    public function getLocalAreaCode( $index = 0 )
    {
        return $this->isProspect() ? htmlspecialchars($this->getData('phone_local_area_code', $index)) :  htmlspecialchars($this->getData('local_area_code', $index));
    }

    /**
     * Retrieving customer private number from contact phone number list
     * @from customer attributes
     * @return string
     */
    public function getPhoneNumber( $index = 0 )
    {
        return htmlspecialchars($this->getData("phone_number", $index));
    }


    /**
     * Retrieving customer telephone type from contact phone number list
     * @from customer attributes
     * @return string
     */
    public function getTelephoneType( $index = 0 )
    {
        return $this->getData("phone_type", $index);
    }

    /**
     * Return the local area phone code from contact phone number list
     * @from customer attributes
     * @return string
     */
    public function getServiceAreaCode()
    {
        // Try to load area phone code from serviceability (OMNVFDE-825)
        return  htmlspecialchars(Mage::getSingleton('dyna_address/storage')->getAreaCode());
    }

    /**
     * Return customer e-mail address as the first found contact_person from the contacts list of subscriptions
     * Do not create dummy one
     * @return string
     */
    public function getEmailAddressWithoutCreatingDummyOne()
    {
        if ($this->isProspect()) {
            return $this->getData("additional_email");
        }

        // If no customer
        if ($this->getCustomerNumber()) {
            if ($this->getIsSoho()) {
                return $this->getCompanyContactEmail();
            } else {
                return $this->getData('mobile_email');
            }
        }

        return "";
    }

    /**
     * Return customer e-mail address as the first found contact_person from the contacts list of subscriptions
     * No reference to private person e-mail address found
     * @todo clarify if this is the correct e-mail address or additional info will be appended to service response for private customers
     * @return mixed
     */
    public function getEmailAddress()
    {
        if($this->isProspect()) {
            return  htmlspecialchars($this->getData("additional_email"));
        }

        // If no customer
        if (!$this->getCustomerNumber()) {
            $this->email = "";
            return $this->email;
        }

        if (!$this->email) {
            if (!$this->getId()) {
                if ($this->getIsSoho()) {
                    $this->email = $this->getCompanyContactEmail();
                }
            }
        }

        return htmlspecialchars($this->getContactEmail());
    }

    /**
     * Get customer SOHO status
     * @from customer attributes
     * @return boolean
     */
    public function getIsSoho()
    {
        if($this->isProspect()) {
            return (bool)$this->getData("is_business");
        }
        return $this->getData("party_type") === Dyna_Customer_Model_Client_RetrieveCustomerInfo::PARTY_TYPE_SOHO;
    }

    /**
     * Get email address for current customer
     * If no e-mail address is set, generate dummy one - Magento throws exception when saving order
     * @return string
     */
    public function getEmail()
    {
        if(!$this->email) {
            $contracts = $this->getContracts();
            $contacts = isset($contracts[0]) ? isset($contracts[0]["subscriptions"][0]) ? $contracts[0]["subscriptions"][0]["contact"] : null : null;
            if (!empty($contacts[0]["Contact"]["ElectronicMail"])) {
                $this->email = $contacts[0]["Contact"]["ElectronicMail"];
            }elseif(!empty($contacts["Contact"]["ElectronicMail"])){
                $this->email = $contacts["Contact"]["ElectronicMail"];
            }else{
                $this->email = Mage::helper('omnius_customer')->generateDummyEmail();
            }
            $this->setEmail($this->email);
        }

        return htmlspecialchars($this->email);
    }

    /**
     * Return all contracts current customer has
     * @return mixed
     */
    public function getContracts()
    {
        return $this->getData('contracts');
    }

    /**
     * Get current customer service address id
     * @from customer address collection
     * @return string
     */
    public function getServiceAddressId()
    {
        /** @var Mage_Customer_Model_Address $address */
        $serviceAddress = $this->getServiceAddress();

        if($serviceAddress && $serviceAddress->getAddressId()) {
            return htmlspecialchars($serviceAddress->getAddressId());
        }

        return null;
    }

    /**
     * Get current customer legal address id
     * @from customer address collection
     * @return string
     */
    public function getLegalAddressId()
    {
        /** @var Mage_Customer_Model_Address $address */
        $legalAddress = $this->getAddress();

        if($legalAddress && !empty($legalAddress->getAddressId())) {
            return htmlspecialchars(trim($legalAddress->getAddressId()));
        }

        return null;
    }

    /**
     * @return Mage_Customer_Model_Address|null|Varien_Object
     */
    public function getServiceAddress()
    {
        $address = null;

        switch ($this->getData('account_category')) {
            case Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS:
                    $address = $this->getPostalAddress(static::DEFAULT_ADDRESS);
                break;
            case Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD:
            case Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN:
                    $address = $this->getAddress(static::SERVICE_ADDRESS);
                break;
        }

        return $address;
    }

    /**
     * Return postal set on RetrieveCustomerInfo service call for soho customer
     * Used in checkout for soho customer with cable or fixed package
     * For DSL package, the Orsteil will not be disabled if retrieved empty
     * Set from Dyna_Customer_Model_Client_RetrieveCustomerInfo
     *
     * @return Varien_Object
     * @method getAddressId
     * @method getAddressType
     * @method getPostcode
     * @method getPostBox
     * @method getStreet
     * @method getHouseNumber
     * @method getHouseAddition
     * @method getCity
     * @method getDistrict
     * @method getState
     * @method getCountry
     */
    public function getPostalAddress($type = null)
    {
        $addressNode = "postal_addresses";
        if (!$this->getData($addressNode)) {
            return new Varien_Object();
        }

        if (!$type) {
            // Loop through all address types
            foreach ($this->getDefaultPostalAddressTypes() as $type) {
                // This loop should not be reached
                foreach ($this->getData($addressNode) as $address) {
                    if (strtolower($address->getAddressType()) === strtolower($type)) {
                        return $address;
                    }
                }
            }
        } else {
            // This loop should not be reached
            foreach ($this->getData($addressNode) as $address) {
                if (strtolower($address->getAddressType()) === strtolower($type)) {
                    return $address;
                }
            }
        }

        return new Varien_Object();
    }

    /**
     * Returning an ordered array of address types that need to be searched through postal addresses
     * Needed in checkout / save_customer step to build customer address
     * @return array
     */
    public function getDefaultPostalAddressTypes()
    {
        return [
            Dyna_Customer_Model_Client_RetrieveCustomerInfo::ADDRESS_TYPE_C,
            Dyna_Customer_Model_Client_RetrieveCustomerInfo::ADDRESS_TYPE_LO,
            static::DEFAULT_ADDRESS,
        ];
    }

    /**
     * Get current customer billing bank account number
     * @from customer attributes
     * @return string
     */
    public function getBankAccountNumber()
    {
        return $this->getBillingAccount() ? $this->getBillingAccount()->getIban() : "";
    }

    /**
     * Get current customer billing bank name
     * @from customer attributes
     * @return string
     */
    public function getBankName()
    {
        return $this->getBillingAccount() ? $this->getBillingAccount()->getBankName() : "";
    }

    /**
     * Get the last saved billing method for current customer. If none set in Omnius then it will be retrieved through RetrieveCustomerInfo service call
     * @return string
     */
    public function getBillingMethod()
    {
        if ($billing = $this->getBillingAccount()) {
            return $billing->getBillingMethod();
        }

        return "";
    }

    /**
     * Get customer birth data
     * @return string
     */
    public function getDob()
    {
        return $this->getData("dob");
    }

    /**
     * Set serial number on current customer for an owned product
     * @param $productId
     * @param $productSerialNumber
     */
    public function setHardwareSerial($productId, $productSerialNumber)
    {
        $this->hardwareSerials[$productId] = $productSerialNumber;
    }

    /**
     * Retrieve previously set serial hardware for an owned product
     * @param $productId
     * @return string|null
     */
    public function getHardwareSerial($productId)
    {
        return !empty($this->hardwareSerials[$productId]) ? $this->hardwareSerials[$productId] : null;
    }

    /**
     * Retrieve previously set owned smartcard
     * @return Dyna_Catalog_Model_Smartcard
     */
    public function getOwnSmartCard()
    {
        return $this->ownSmartcard;
    }

    /**
     * Set owned smart card on current customer
     * @return $this
     */
    public function setOwnSmartCard(Varien_Object $smartcard)
    {
        $this->ownSmartcard = $smartcard;

        return $this;
    }

    /**
     * Retrieve previously set owned receiver
     * @return Dyna_Catalog_Model_Receiver
     */
    public function getOwnReceiver()
    {
        return $this->ownReceiver;
    }

    /**
     * Set owned receiver on current customer
     * @return $this
     */
    public function setOwnReceiver(Varien_Object $receiver)
    {
        $this->ownReceiver = $receiver;

        return $this;
    }

    /**
     * Clear own receiver data from customer
     * @return $this
     */
    public function clearOwnReceiver() {
        $this->ownReceiver = null;
        return $this;
    }

    /**
     * Clear own smartcard data from customer
     * @return $this
     */
    public function clearOwnSmartcard() {
        $this->ownSmartcard = null;
        return $this;
    }

    public function hasOwnHardware()
    {
        return (bool)(!empty($this->ownReceiver) || !empty($this->ownSmartcard));
    }

    /**
     * Show customer link icon in customer panel
     */
    public function isLinked()
    {
        if(count(Mage::getSingleton('dyna_customer/session')->getServiceCustomers()) > 1) {
            return true;
        }
       return false;
    }

    /**
     * Retrieve customer ban / legacy id / ccbId as stated in RetrieveCustomerInfo webservice definition
     * @return string
     */
    public function getBan()
    {
        return $this->getCustomerNumber();
    }

    /**
     * Return customer password set from retrieve customer info service call or through checkout
     * @return string
     */
    public function getCustomerPassword()
    {
        return $this->getData("customer_password");
    }

    /**
     * Retrieve customer fax number set on checkout
     * @return string
     */
    public function getFaxNumber()
    {
        return $this->getData("fax_number");
    }

    /**
     * Get customer modem (if customer has one)
     * @return Varien_Object|null
     */
    public function getCustomerModem()
    {
        return $this->customerModem;
    }

    /**
     * Set modem on current customer
     * Trigger from configurator
     * @todo implement modem setter after retrieveCustomerInfo service call
     * @return $this
     */
    public function setCustomerModem(Varien_Object $modem = null)
    {
        $this->customerModem = $modem;
        return $this;
    }

    /**
     * Get customer identification number from Customer.Role.Person.IdentificationTypeNumber
     * Set on Dyna_Customer_Model_Client_RetrieveCustomerInfo
     * @notice No change allowed for this field
     * @return string
     */
    public function getIdentificationId()
    {
        return $this->getData("identification_number");
    }

    /**
     * Return nationality country code
     * Set on Dyna_Customer_Model_Client_RetrieveCustomerInfo
     * @notice No change allowed for this field
     * @return string
     */
    public function getNationalityId()
    {
        return $this->getData("nationality_id");
    }

    /**
     * Return subscription product contact first name used in checkout
     */
    public function getSubscriptionContactFirstname($alsoCheckAlternatePaths = false)
    {
        if ($this->isCustomerLoggedIn() && ($contracts = $this->getContracts())) {
            $firstName = !empty($contracts[0]["subscriptions"][0]["products"]["contact"][0]["firstname"]) ? $contracts[0]["subscriptions"][0]["products"]["contact"][0]["firstname"] : "";

            if ($firstName === "" && $alsoCheckAlternatePaths) {
                if (!isset($contracts[0]["subscriptions"][0]["contact"][0])) {
                    $contracts[0]["subscriptions"][0]["contact"] = array($contracts[0]["subscriptions"][0]["contact"]);
                }
                $firstName = !empty($contracts[0]["subscriptions"][0]["contact"][0]["Role"]["Person"]["FirstName"]) ? $contracts[0]["subscriptions"][0]["contact"][0]["Role"]["Person"]["FirstName"] : "";
            }
        } else {
            $firstName = "";
        }

        return $firstName;
    }

    /**
     * Return subscription product contact last name used in checkout
     */
    public function getSubscriptionContactLastname($alsoCheckAlternatePaths = false)
    {
        if ($this->isCustomerLoggedIn() && ($contracts = $this->getContracts())) {
            $lastName = !empty($contracts[0]["subscriptions"][0]["products"]["contact"][0]["lastname"]) ? $contracts[0]["subscriptions"][0]["products"]["contact"][0]["lastname"] : "";
            if ($lastName === "" && $alsoCheckAlternatePaths) {
                if (!isset($contracts[0]["subscriptions"][0]["contact"][0])) {
                    $contracts[0]["subscriptions"][0]["contact"] = array($contracts[0]["subscriptions"][0]["contact"]);
                }
                $lastName = !empty($contracts[0]["subscriptions"][0]["contact"][0]["Role"]["Person"]["FamilyName"]) ? $contracts[0]["subscriptions"][0]["contact"][0]["Role"]["Person"]["FamilyName"] : "";
            }
        } else {
            $lastName = "";
        }

        return $lastName;
    }

    /**
     * Return subscription product contact salutation used in checkout
     */
    public function getSubscriptionContactSalutation($alsoCheckAlternatePaths = false)
    {
        $salutation = "";
        if ($this->isCustomerLoggedIn()) {
            if ($contracts = $this->getContracts()) {
                $salutation = !empty($contracts[0]["subscriptions"][0]["products"]["contact"][0]["salutation"]) ? $contracts[0]["subscriptions"][0]["products"]["contact"][0]["salutation"] : $salutation;

                if ($salutation === "" && $alsoCheckAlternatePaths) {
                    if (!isset($contracts[0]["subscriptions"][0]["contact"][0])) {
                        $contracts[0]["subscriptions"][0]["contact"] = array($contracts[0]["subscriptions"][0]["contact"]);
                    }
                    $salutation = !empty($contracts[0]["subscriptions"][0]["contact"][0]["Role"]["Person"]["Salutation"]) ? $contracts[0]["subscriptions"][0]["contact"][0]["Role"]["Person"]["Salutation"] : $salutation;
                }
            }
        }

        switch ($salutation) {
            case Dyna_Customer_Model_Client_RetrieveCustomerInfo::CUSTOMER_SALUTATION_MR:
                $response = 1;
                break;
            case Dyna_Customer_Model_Client_RetrieveCustomerInfo::CUSTOMER_SALUTATION_MRS:
                $response = 2;
                break;
            case Dyna_Customer_Model_Client_RetrieveCustomerInfo::CUSTOMER_SALUTATION_FIRMA:
                $response = 3;
                break;
            case Dyna_Customer_Model_Client_RetrieveCustomerInfo::CUSTOMER_SALUTATION_NONE:
                $response = 0;
                break;
            default:
                $response = null;
                break;
        }

        return $response;
    }

    /**
     * Return subscription product contact telephone used in checkout
     */
    public function getSubscriptionContactTelephone()
    {
        if ($this->isCustomerLoggedIn()) {
            $firstName = ($contracts = $this->getContracts())
                ? !empty($contracts[0]["subscriptions"][0]["products"][0]["contact"][0]["phone_number"]) ? $contracts[0]["subscriptions"][0]["products"][0]["contact"][0]["phone_number"] : ""
                : "";
        } else {
            $firstName = "";
        }

        return $firstName;
    }

    /**
     * Return subscription product contact title used in checkout
     */
    public function getSubscriptionContactTitle()
    {
        if ($this->isCustomerLoggedIn()) {
            $firstName = ($contracts = $this->getContracts())
                ? !empty($contracts[0]["subscriptions"][0]["products"]["contact"][0]["title"]) ? $contracts[0]["subscriptions"][0]["products"]["contact"][0]["title"] : ""
                : "";
        } else {
            $firstName = "";
        }

        return $firstName;
    }

    /*
     * Get current customer billing method of payment
     * Set on Dyna_Customer_Model_Client_RetrieveCustomerInfo
     * @notice Change allowed for this field
     * @return string
     */

    public function getPaymentMethod()
    {
        return $this->getBillingAccount() ? $this->getBillingAccount()->getPaymentMethod() : "";
    }

    /*
     * Get current customer party identification
     * Set on Dyna_Customer_Model_Client_RetrieveCustomerInfo
     * @notice Change allowed for this field
     * @return string
     */
    public function getPartyIdentyfication()
    {
        return $this->getBillingAccount() ? $this->getBillingAccount()->getPartyIdentyfication() : "";
    }

    /*
     * Get current customer billing bank no.
     * Set on Dyna_Customer_Model_Client_RetrieveCustomerInfo
     * @notice Change allowed for this field
     * @return string
     */
    public function getBankId()
    {
        return $this->getBillingAccount() ? $this->getBillingAccount()->getBankId() : "";
    }

    /**
     * Method that returns an array of product skus that customer has in its installed base based on a given subscription number and product id with which bundle has been created
     * @param $parentAccountNumber string
     * @param $serviceLineId string|null
     * @param $justSubscription bool|false
     * @return array[string]
     */
    public function getInstalledBaseProducts($parentAccountNumber, $serviceLineId = null, $justSubscription = false)
    {
        $serviceLineId = (string)$serviceLineId;
        $response = array();
        foreach ($this->getAllInstallBaseProducts() as $subscription) {
            if ($subscription['contract_id'] === $parentAccountNumber && $subscription['product_id'] === $serviceLineId) {
                foreach ($subscription['products'] as $product) {
                    if ($justSubscription && $product['product_package_subtype'] !== strtolower(Dyna_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION)) {
                        continue;
                    }
                    $response[] = $product['product_sku'];
                }
            }
        }

        return $response;
    }

    public function getParentAccountNumberByServicelineId($serviceLineId){

        $serviceLineId = (string)$serviceLineId;
        foreach ($this->getAllInstallBaseProducts() as $subscription) {
            if ($subscription['product_id'] === $serviceLineId ) {
                return $subscription['contract_id'];
            }
        }
        return null;
    }
    /**
     * Method that returns the entire contract data for an install base account and subscription combination
     * @param $parentAccountNumber string
     * @param $serviceLineId string|null
     * @return array[string]
     */
    public function getInstalledBaseProduct($parentAccountNumber, $serviceLineId = null)
    {
        foreach ($this->getAllInstallBaseProducts() as $subscription) {
            if ($subscription['contract_id'] === $parentAccountNumber && $subscription['product_id'] === $serviceLineId) {
                return $subscription;
            }
        }

        return null;
    }

    /**
     * Method that returns an array of products in install base by parsing components
     * @param $product
     * @return array
     */
    protected function parseComponents($product)
    {
        $subscriptionProducts = array();

        if (isset($product['components'])) {
            foreach ($product['components'] as $component) {
                /** @var Dyna_Catalog_Model_Product $productModel */
                $productModel = Mage::getModel('catalog/product');

                /** @var Dyna_Catalog_Model_Category $categoryModel */
                $categoryModel = Mage::getModel('catalog/category');

                $productSku = isset($component['sku']) ? trim($component['sku']) : null;
                $productId = $productSku ? $productModel->getIdBySku($productSku) : null;
                $productType = isset($component['type']) ? strtolower($component['type']) : null;

                if ($productId) {
                    //Loading product to get the familyId
                    $productObject = $productModel->load($productId);
                    $productFamily = $productObject->getProductFamily();

                    $categories = $categoryModel->getProductCategoryIds($productId);
                    // return subscription product with it's associated categories
                    $subscriptionProducts[] = array(
                        'product_id' => $productId,
                        'product_sku' => $productSku,
                        'product_package_subtype' => $productType,
                        'categories' => $categories,
                        'product_family' => $productFamily,
                        'title' => $component['name'],
                        'description' => '', // @todo previously this was the value of attribute label from product
                        // @todo determine what to do with this key and if it really is necessarily
                    );
                }
            }
        }

        return $subscriptionProducts;
    }

    /**
     * Return all install base products with index and subscription contract reference
     * @return array
     */
    public function getAllInstallBaseProducts($filterLinkedAccounts = true)
    {
        // caching response on customer session
        $customerSession = Mage::getSingleton('customer/session');
        $installBaseProducts = $customerSession->getAllInstalledBaseProducts();
        if ($installBaseProducts !== [] && ($installBaseProducts === null || empty($installBaseProducts))) {
            //don't check if no customer is logged in
            if(!$this->isCustomerLoggedIn() || $this->isProspect()){
                return array();
            }

            $installBaseProducts = array();
            $index = 1;

            /** @var $panelHelper Dyna_Customer_Helper_Panel */
            $panelHelper = Mage::helper('dyna_customer/panel');
            $customerData = $panelHelper->getPanelData("products-content");

            foreach ($customerData as $type => $customers) {
                if (!is_array($customers)) continue;
                foreach ($customers as $customer) {
                    if (!isset($customer['contracts']) ||
                        (!is_array($customer['contracts']) && (!$customer['contracts'] instanceof Traversable))
                    ) {
                        $contracts = [];
                    } else {
                        $contracts = $customer['contracts'];
                    }

                    foreach ($contracts as $contract) {
                        // Skip if customer status in not active (see OMNVFDE-2733)
                        if (in_array($contract['customer_status'], Dyna_Customer_Helper_Customer::getCustomerInactiveStatusList()) ||
                            $contract['dunning_status'] == Dyna_Customer_Helper_Services::DUNNING_IN
                        ) {
                            continue;
                        }
                        switch ($type) {
                            case Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD:
                                foreach ($contract['subscriptions'] as $subscriptionData) {
                                    foreach ($subscriptionData['products'] as $productCategory => $products) {
                                        foreach ($products as $product) {
                                            // Skip if product status is not active (see OMNVFDE-2733)
                                            if ((isset($product['product_status']) && in_array($product['product_status'], Dyna_Customer_Helper_Customer::getContractProductInactiveStatusList()))) {
                                                continue;
                                            }
                                            $hisProducts = $this->parseComponents($product);

                                            if (!empty($products)) {
                                                $installBaseProducts[] = array(
                                                    'index_id' => $index,
                                                    'part_of_bundle' => $product['part_of_bundle'] ?? false,
                                                    'contract_id' => $contract['customer_number'],
                                                    'in_minimum_duration' => $subscriptionData['in_minimum_duration'] ?? false,
                                                    'sharing_group_id' => $subscriptionData['sharing_group_details']['id'] ?? null,
                                                    'sharing_group_member_type' => $subscriptionData['sharing_group_details']['member_type'] ?? null,
                                                    'sharing_group_number_of_members' => $subscriptionData['sharing_group_details']['number_of_members'] ?? null,
                                                    'product_id' => $product['install_base_product_id'],
                                                    'contract_category' => Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD,
                                                    'contract_activation_date' => $subscriptionData['contract_activation_date'],
                                                    'package_type' => self::$productCategoryToPackageType[strtolower($productCategory)] ?? "",
                                                    'package_type_icon' => self::$productCategoryToPackageType[strtolower($productCategory)] ?? "",
                                                    'ctn' => $subscriptionData['ctn'] ?? "",
                                                    'products' => $hisProducts,
                                                    'service_address' => $subscriptionData['service_address'],
                                                    "actual_month_of_contract" => $subscriptionData['actual_month_of_contract'],
                                                    "serial_number" => $subscriptionData['serial_number']
                                                );

                                                $index++;
                                            }
                                        }
                                    }
                                }
                                break;
                            case Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN:
                                $subscriptionsComponents = [];
                                // todo: determine if for FN we need to check the components from subscriptions as well
                                foreach ($contract['subscriptions'] as $subscriptionData) {
                                    foreach ($subscriptionData['components'] as $product) {
                                        $subscriptionsComponents[] = $product;
                                    }
                                }
                                $components = array_merge($subscriptionsComponents, $contract['products']);
                                $products = $this->parseComponents(['components' => $components]);

                                if (!empty($products)) {
                                    // Found an eligible subscription? add it to the list
                                    $installBaseProducts[] = array(
                                        'index_id' => $index,
                                        'part_of_bundle' => $subscriptionData['part_of_bundle'] ?? false,
                                        'contract_id' => $contract['customer_number'],
                                        'in_minimum_duration' => $subscriptionData['in_minimum_duration'] ?? false,
                                        'sharing_group_id' => $subscriptionData['sharing_group_details']['id'] ?? null,
                                        'sharing_group_member_type' => $subscriptionData['sharing_group_details']['member_type'] ?? null,
                                        'sharing_group_number_of_members' => $subscriptionData['sharing_group_details']['number_of_members'] ?? null,
                                        'product_id' => $subscriptionData['install_base_product_id'],
                                        'contract_category' => Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN,
                                        'contract_activation_date' => $subscriptionData['contract_activation_date'],
                                        'package_type' => self::$productCategoryToPackageType[strtolower(Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN)] ?? "",
                                        'package_type_icon' => self::$productCategoryToPackageType[strtolower($subscriptionData['product_category'])] ?? "",
                                        'ctn' => $subscriptionData['ctn'] ?? "",
                                        'products' => $products,
                                        'service_address' => $subscriptionData['service_address'],
                                        "actual_month_of_contract" => $subscriptionData['actual_month_of_contract'],
                                        "serial_number" => $subscriptionData['serial_number']
                                    );
                                    $index++;
                                }
                                break;
                            case Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS:
                                foreach ($contract['subscriptions'] as $subscriptionData) {
                                    foreach ($subscriptionData['products'] as $product) {
                                        // Skip if product status is not active (see OMNVFDE-2733)
                                        if (isset($product['product_status']) &&
                                            in_array($product['product_status'], Dyna_Customer_Helper_Customer::getContractProductInactiveStatusList())
                                        ) {
                                            continue;
                                        }

                                        $products = $this->parseComponents($subscriptionData);

                                        if (!empty($products)) {
                                            $installBaseProducts[] = array(
                                                'index_id' => $index,
                                                'part_of_bundle' => $subscriptionData['part_of_bundle'] ?? false,
                                                'contract_id' => $contract['customer_number'],
                                                'in_minimum_duration' => $subscriptionData['in_minimum_duration'] ?? false,
                                                'sharing_group_id' => $subscriptionData['sharing_group_details']['id'] ?? null,
                                                'sharing_group_member_type' => $subscriptionData['sharing_group_details']['member_type'] ?? null,
                                                'sharing_group_number_of_members' => $subscriptionData['sharing_group_details']['number_of_members'] ?? null,
                                                'sharing_group_reserved_status' => $subscriptionData['has_reserved_member'] ?? null,
                                                'product_id' => $subscriptionData['product_id'] ?? $product['install_base_product_id'],
                                                'contract_category' => Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS,
                                                'contract_activation_date' => $subscriptionData['contract_activation_date'],
                                                'contact_birth_date' =>  $subscriptionData['contact_birth_date'],
                                                'package_type' => self::$productCategoryToPackageType[strtolower(Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS)] ?? "",
                                                'package_type_icon' => self::$productCategoryToPackageType[strtolower($product['product_category'])] ?? "",
                                                'ctn' => $subscriptionData['ctn'] ?? "",
                                                'products' => $products,
                                                'service_address' => $subscriptionData['service_address'],
                                                'product_status' => $subscriptionData['product_status'],
                                                "actual_month_of_contract" => $subscriptionData['actual_month_of_contract'],
                                                "serial_number" => $subscriptionData['serial_number'],
                                                "sim_number" => $subscriptionData['sim_number']
                                            );
                                            $index++;
                                        }
                                    }
                                }
                                break;
                            default:
                                // Do nothing
                                break;
                        }
                    }
                }
            }

            $customerSession->setAllInstalledBaseProducts($installBaseProducts);
        }

        $this->alignInstallBaseProductsWithLinkedAccounts($installBaseProducts, $filterLinkedAccounts);

        return $installBaseProducts;
    }

    protected function alignInstallBaseProductsWithLinkedAccounts(&$installBaseProducts, $filterLinkedAccounts)
    {
        $customerSession = Mage::getSingleton('customer/session');
        // OVG-1890, OVG-2538 - If no mobile postpaid is in cart and is not dummy, or is dummy and has the added from my products flag filter panel
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $filterKias = $filterKd = false;
        $accountsData = array();
        foreach ($quote->getCartPackages() as $cartPackage) {
            // if a mobile postpaid has been found in cart, filter the install base products
            !$cartPackage->isMobilePostpaid() ?: $filterKias = true;

            // if no leading mobile postpaid is found, the Red+ becomes leading for parent account selection (if not added from my products)
            !$cartPackage->isPartOfRedPlus() ?: $filterKias = true;

            // if there is a KD linked account, filter KD products
            !$customerSession->getKdLinkedAccountNumber() ?: $filterKd = true;

            // Gather linked accounts data in order to override $filterLinkedAccounts in case there are more than two packages with the same linked account
            if (($accountNumber = $cartPackage->getParentAccountNumber())
                || ($cartPackage->isPartOfGigaKombi() && ($accountNumber = $cartPackage->getGigaKombiAccountNumber())))
            {
                isset($accountsData[$accountNumber]) ?: $accountsData[$accountNumber] = 0;
                $accountsData[$accountNumber]++;
                if ($accountsData[$accountNumber] > 2) {
                    $filterLinkedAccounts = true;
                    if ($cartPackage->isMobilePostpaid()) {
                        $filterKias = true;
                    }
                    if ($cartPackage->isCable()) {
                        $filterKd = true;
                    }
                }
            }
        }

        if (($filterKd || $filterKias) && $filterLinkedAccounts === true) {
            $kiasLinkedAccount = $customerSession->getKiasLinkedAccountNumber();
            foreach ($installBaseProducts as $key => $baseProduct) {
                // Filter KIAS linked accounts
                if ($filterKias && $kiasLinkedAccount
                    && $baseProduct['contract_category'] == Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS
                    && $baseProduct['contract_id'] != $kiasLinkedAccount
                    && !$quote->getCartPackage()->isPrePost()
                ) {
                    unset($installBaseProducts[$key]);
                }
            }
        }
    }

    /**
     * @return array
     */
    public function getInstalledBasePackageTypes()
    {
        //don't check if no customer is logged in
        if (!$this->isCustomerLoggedIn()) {
            return array();
        }
        /** @var $panelHelper Dyna_Customer_Helper_Panel */
        $panelHelper = Mage::helper('dyna_customer/panel');
        $customerData = $panelHelper->getPanelData("products-content");
        $installedBasePackageTypes = array();

        foreach ($customerData as $customers) {
            foreach ($customers as $customer) {

                if (!isset($customer['contracts']) ||
                    (!is_array($customer['contracts']) && (!$customer['contracts'] instanceof Traversable))
                ) {
                    $contracts = [];
                } else {
                    $contracts = $customer['contracts'];
                }

                foreach ($contracts as $contract) {
                    foreach ($contract['subscriptions'] as $subscriptionData) {
                        if (!empty($subscriptionData['product_category']) && !empty(self::$productCategoryToPackageType[strtolower($subscriptionData['product_category'])])) {
                            $installedBasePackageTypes[] = strtolower(self::$productCategoryToPackageType[strtolower($subscriptionData['product_category'])]);
                        }
                    }
                }
            }
        }
        return array_unique(array_filter($installedBasePackageTypes));
    }

    // customer number / subscription number should arrive in bundle create action

    /**
     * Return package type for a certain combination of subscription number with a product id (for cable is null)
     * @param $indexId string
     * @return string
     */
    public function getInstalledBaseContractType($subscriptionNumber, $productId = null)
    {
        $contractType = "";
        foreach ($this->getAllInstallBaseProducts() as $productData) {
            if ($productData['contract_id'] == $subscriptionNumber && $productData['product_id'] == $productId) {
                $contractType = $productData['package_type'];
                break;
            }
        }

        return $contractType;
    }

    public function getDunningStatus()
    {
        return $this->getData('dunning_status') ? $this->getData('dunning_status') : "";
    }

    /**
     * Returns current dunning status for package customer number in same stack
     *
     * @param Dyna_Package_Model_Package $package
     * @return null
     */
    public function getDunningStatusForPackage($package)
    {
        /** @var Dyna_Customer_Model_Session $customerSession */
        $customerSession = Mage::getSingleton('customer/session');
        $serviceCustomers = $customerSession->getServiceCustomers() ?? array();
        foreach ($serviceCustomers as $customers) {
            if (isset($customers[$package->getParentAccountNumber()]) && $customers[$package->getParentAccountNumber()]) {
                return $customers[$package->getParentAccountNumber()]['dunning_status'];
            }
        }

        return null;
    }

    /**
     * Return customer id by a given customer number
     * @return int
     */
    public function getIdByCustomerNumber(string $customerNumber)
    {
        /** @var Varien_Db_Adapter_Interface $adapter */
        $adapter = Mage::getSingleton('core/resource')->getConnection('core_read');
        $select = $adapter->select()->from('customer_entity', array('entity_id'))->where('customer_number = ?', $customerNumber)->order('entity_id desc');
        $results = $adapter->fetchCol($select);
        return (int)current($results);
    }

    /**
     * Return the account category / line of business that customer is loaded from
     * @return string
     */
    public function getLineOfBusiness()
    {
        /** @var $panelHelper Dyna_Customer_Helper_Panel */
        $panelHelper = Mage::helper('dyna_customer/panel');

        return implode(",", array_keys($panelHelper->getPanelData("products-content")));
    }

    public function getCtn($listOfCtns = false, $onlyMobile = false)
    {
        $ctns = array();

        /** @var $panelHelper Dyna_Customer_Helper_Panel */
        $panelHelper = Mage::helper('dyna_customer/panel');
        $customerData = $panelHelper->getPanelData("products-content");

        foreach ($customerData as $customers) {
            if ($customers && is_array($customers)) {
                foreach ($customers as $accountData) {
                    foreach ($accountData['contracts'] as $contract) {
                        foreach ($contract['subscriptions'] as $subscription) {
                            if (!empty($ctn = $subscription['ctn'])) {
                                if (!$onlyMobile || (isset($subscription['ctn_type']) && $subscription['ctn_type'] == Dyna_Checkout_Block_Cart_Steps_SaveCustomer::PRIVATE_TELEPHONE_TYPE)) {
                                    $ctns[] = $ctn;
                                }
                                if (!$listOfCtns) {
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        return $listOfCtns ? $ctns : current($ctns);
    }

    /**
     * Return the sibling of an install base part of bundle product
     * Searching through install base and returning the first found combination of subscription number and product id different from combination of arguments found in install base
     * @see OVG-1918
     * @param $subscriptionNumber
     * @param $productId
     * @return array(contract_id, product_id)
     */
    public function getIBBundleSibling($subscriptionNumber, $productId)
    {
        $result = array();
        foreach ($this->getAllInstallBaseProducts() as $baseProduct) {
            if (($baseProduct['contract_id'] !== $subscriptionNumber ||  $baseProduct['product_id'] !== $productId) && $baseProduct['part_of_bundle']) {
                $result = array(
                    'contract_id' => $baseProduct['contract_id'],
                    'product_id' => $baseProduct['product_id'],
                );
            }
        }

        return $result;
    }

    /**
     * Determine whether or not this combination of subscription number product id represents an install base product that is part of bundle
     * @param $subscriptionNumber
     * @param $productId
     * @return bool
     */
    public function isIBProductPartOfBundle($subscriptionNumber, $productId)
    {
        foreach ($this->getAllInstallBaseProducts() as $product) {
            if (($product['contract_id'] === $subscriptionNumber) && ($product['product_id'] === $productId) && $product['part_of_bundle']) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param $accountNumber
     * @param null $node
     * @return mixed|null
     */
    public function getInstallBaseDataBy($accountNumber, $node = null)
    {
        if (!$accountNumber) {
            return null;
        }

        foreach ($this->getAllInstallBaseProducts() as $baseProduct) {
            if ($baseProduct['contract_id'] === $accountNumber) {
                return !empty($node)?  $baseProduct[$node] : $baseProduct;
            }
        }

        return null;
    }

    /**
     * @param $byStack string one of the following:
     * Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN
     * Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD
     */
    public function getServiceLineId($byStack)
    {
        $serviceCustomers = Mage::getSingleton('customer/session')->getServiceCustomers();
        $customerData = $serviceCustomers[$byStack] ?? null;

        if (!$customerData) {
            return null;
        }

        foreach ($customerData as $banData) {
            foreach ($banData['contracts'] as $contractData) {
                foreach ($contractData['subscriptions'] as $subscriptionData) {
                    foreach ($subscriptionData['products'] as $product) {
                        return $product['service_line_id'];
                    }
                }
            }
        }

        return null;
    }

    /**
     * Determine whether contract is in minimum duration (see VFDED1W3S-895)
     * @param $contractId
     * @return bool
     */
    public function getIsInMinimumDuration($contractId)
    {
        foreach ($this->getAllInstallBaseProducts() as $productData) {
            if ($productData['contract_id'] == $contractId) {
                return (bool)$productData['in_minimum_duration'];
            }
        }

        return false;
    }
}
