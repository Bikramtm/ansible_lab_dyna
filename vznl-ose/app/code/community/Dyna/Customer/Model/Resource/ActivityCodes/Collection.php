<?php

class Dyna_Customer_Model_Resource_ActivityCodes_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{

    public function _construct()
    {
        parent::_construct();
        $this->_init('dyna_customer/activityCodes');
    }
}
