<?php

class Dyna_Customer_Model_TypeSubtypeImporterXML extends Dyna_Import_Model_Adapter_Xml
{
    const ALL_WEBSITES = "*";

    protected $lineNo = 0;
    protected $websites = [];
    protected $websiteIds = [];

    /**
     * Dyna_MultiMapper_Model_ImporterXML constructor.
     * @param $args
     */
    public function __construct($args)
    {
        $this->logFile = "import_type_subtype_combinations.log";
        parent::__construct($args);
    }

    public function import()
    {
        $this->removePreviousData(Mage::getModel('dyna_customer/typeSubtypeCombinations'));
        $this->setAllDefaultWebsites();

        // retrieve data
        $combinations = $this->getXmlData(true, false);

        // parse components
        foreach ($combinations->TypeSubtype as $typeSubtype) {
            // set current line
            $this->lineNo++;

            if (trim((string) $typeSubtype->CustomerType) == '') {
                $this->_skippedFileRows++;
                $this->_logError('[ERROR] Skipping element no. '.$this->lineNo.' because CustomerType is missing.');
                continue;
            }

            if (trim((string) $typeSubtype->CustomerSubType) == '') {
                $this->_skippedFileRows++;
                $this->_logError('[ERROR] Skipping element no. '.$this->lineNo.' because CustomerSubType is missing.');
                continue;
            }

            // determine the website ids (if is provided a code it must be mapped to a possible id;
            // if is an id it must be a valid one, else -> error)
            if (isset($typeSubtype->Channel)) {
                if (!$this->setWebsiteIds(trim((string) $typeSubtype->Channel))) {
                    $this->_skippedFileRows++;
                    $this->_logError('[ERROR] Skipping element no. ' . $this->lineNo . ' because Channel was not found.');
                    continue;
                }
            } else {
                foreach (Mage::app()->getWebsites() as $website) {
                    /** @var $website Mage_Core_Model_Website */
                    $this->websiteIds[] = $website->getId();
                }
            }

            foreach ($this->websiteIds as $websiteId) {
                $model = Mage::getModel('dyna_customer/typeSubtypeCombinations');
                $model->setAccType(trim((string) $typeSubtype->CustomerType));
                $model->setAccSubType(trim((string) $typeSubtype->CustomerSubType));
                $model->setWebsiteId($websiteId);
                $model->save();
                $model->unsetData();
            }
        }

        $this->_totalFileRows = $this->lineNo;
    }

    /**
     * Set the websites that are available as an array of [websiteID => websiteCode]
     */
    protected function setAllDefaultWebsites()
    {
        if (!$this->websites) {
            $presentWebsites = Mage::app()->getWebsites();

            /** @var Mage_Core_Model_Website $website */
            foreach ($presentWebsites as $website) {
                $this->websites[$website->getId()] = strtolower($website->getCode());
            }
        }
    }

    /**
     * Try to set the websites for the rule;
     * Go through each website id or website code provided (the list provided in the csv can be of the form "id1,id2" or "code1,code2"
     * and see if the code/id provided is in the list of websites codes/ids that are possible.
     * If one id/code is not found in the possible list => skip the row, error
     *
     * @param $channel
     * @return bool
     */
    protected function setWebsiteIds($channel)
    {
        $this->websiteIds = [];
        $websitesData = explode(',', strtolower($channel));
        foreach ($websitesData as $key => $possibleWebsite) {
            // if one of the possible websites provided in the csv is "*" the rule will be available for all websites;
            // skip the rest of the code
            if ($possibleWebsite == self::ALL_WEBSITES) {
                $this->websiteIds = array_keys($this->websites);
                return true;
            }

            // the website provided is the code and we should store the id
            if ($validWebsite = array_search($possibleWebsite, $this->websites)) {
                $this->websiteIds[$key] = $validWebsite;
            } // the website provided is id and we should store it
            elseif (array_key_exists($possibleWebsite, $this->websites)) {
                $this->websiteIds[$key] = $possibleWebsite;
            } // not match can be found between the given website and a code or an id
            else {
                $this->_logError('[ERROR] The website_id '.$channel.' is not present/valid. Skipping element.');
                return false;
            }
        }

        return true;
    }
}
