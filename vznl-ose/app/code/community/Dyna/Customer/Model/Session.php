<?php

/**
 * Class Dyna_Customer_Model_Session
 * @method setCustomers($data)
 * @method getCustomers()
 * @method unsCustomers()
 * @method setUctParams($data)
 * @method getUctParams()
 * @method unsUctParams()
 * @method setServiceCustomers($data) //is set in Dyna_Customer_Model_Client_RetrieveCustomerInfo
 * @method setKiasLinkedAccountNumber($linkedAccountNumber)
 * @method setKdLinkedAccountNumber($linkedAccountNumber)
 */
class Dyna_Customer_Model_Session extends Omnius_Customer_Model_Session
{
    CONST KEY_HINT_MESSAGE = "expression-injected-message";
    protected $bundleChoices = array();

    /** @var Dyna_Customer_Model_Customer */
    protected $customer;

    /**
     * Get all the customer that were returned by service call
     * @return array|mixed
     */
    public function getServiceCustomers()
    {
        return $this->getData('service_customers') ?? array();
    }

    /**
     * Clear customer from session
     * @return bool
     */
    public function unsetCustomer()
    {
        $this->customer = null;
        $this->setData('customer', null);
        $this->setData('customer_mode2', null);
        $this->setData('all_installed_base_products', null);
        $this->setData('new_search_customer',true);
        $this->clearHouseholdMembers();
        $this->clearServiceResponse();

        return true;
    }

    /**
     * Search a specific customer in the ones returned by the SearchCustomer service call
     * @param $id
     * @return mixed
     */
    public function getSessionCustomer($id)
    {
        $customers = $this->getCustomers();
        if (!empty($customers)) {
            return current(array_filter($customers, function ($item) use ($id) {
                return ($item['Contact']['ID'] == $id);
            }));
        }
    }

    /**
     * @param $number Customer number (Global ID not Legacy ID)
     * @return null
     */
    public function getCustomerByNumber($number)
    {
        $serviceCustomers = $this->getServiceCustomers();
        $customerObject = Mage::getModel('customer/customer');
        foreach ($serviceCustomers as $customers) {
            foreach ($customers as $serviceCustomer) {
                if (isset($serviceCustomer['customer_number']) && $serviceCustomer['customer_number'] == $number) {
                    $customerObject->addData($serviceCustomer);
                    break;
                }
            }
        }

        return $customerObject;
    }

    public function setCustomerProducts($customerProducts)
    {
        $this->setData('customerProducts', $customerProducts);

        return $this;
    }

    public function getCustomerProducts()
    {
        return $this->getData('customerProducts');
    }

    public function clearCustomerProducts()
    {
        $this->setData('customerProducts', null);
    }

    public function setCustomerOrders($customerOrders)
    {
        $this->setData('customerOrders', $customerOrders);

        return $this;
    }

    public function getCustomerOrders()
    {
        return $this->getData('customerOrders');
    }

    public function clearCustomerOrders()
    {
        $this->setData('customerOrders', null);
    }

    /**
     * Overrides Magento getCustomer method loading it only from session
     * @return Dyna_Customer_Model_Customer
     */
    public function getCustomer()
    {
        if (!$this->customer) {
            if (!$this->customer = $this->getData('customer')) {
                $this->customer = Mage::getModel('dyna_customer/customer');
                $this->setData('customer', $this->customer);
            }
        }

        return $this->customer;
    }

    public function setCustomer(Mage_Customer_Model_Customer $customer)
    {
        $this->customer = $customer;
        $this->setData('new_search_customer',false);
        $this->setData('customer', $customer);

        return $this;
    }

    /**
     * Returns customer session id
     * @return mixed|null
     */
    public function getCustomerId()
    {
        return $this->getCustomer()->getId();
    }

    /**
     * Check if a customer is logged in
     * @return mixed
     */
    public function isLoggedIn()
    {
        return $this->getCustomer()->isCustomerLoggedIn();
    }

    /**
     * Set household data for customer
     * @param $data
     */
    public function setHouseholdMembers($data)
    {
        $this->setData('householdMembers', $data);
    }

    /**
     * Get household data of customer
     * @return mixed
     */
    public function getHouseHoldMembers()
    {
        return $this->getData('householdMembers');
    }

    /**
     * Remove household data of customer
     */
    public function clearHouseholdMembers()
    {
        $this->setData('householdMembers', null);
    }

    public function updateLinkedAccountNumber() {
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $quotePackages = $quote->getCartPackages();

        $kiasLinkedAccountNumber = $this->getKiasLinkedAccountNumber();
        $kdLinkedAccountNumber = $this->getKdLinkedAccountNumber();
        $kiasPackages = 0;
        $kdPackages = 0;

        foreach($quotePackages as $package){
            if(strtolower($package->getType()) == strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE)){
                $kiasPackages++;
            } elseif(in_array(strtolower($package->getType()),
                array(
                    strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_TV),
                    strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_INTERNET_PHONE),
                    strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_INDEPENDENT)
                )
            )){
                $kdPackages++;
            }
        }

        if ($kiasPackages == 0 && $kiasLinkedAccountNumber) {
            $this->setKiasLinkedAccountNumber(null);
        }
        if ($kdPackages == 0 && $kdLinkedAccountNumber) {
            $this->setKdLinkedAccountNumber(null);
        }
    }

    /**
     * Once requested, messages are removed from session
     * @return array
     */
    public function getHints()
    {
        $hints = $this->getData(static::KEY_HINT_MESSAGE) ?: array();
        $this->setData(static::KEY_HINT_MESSAGE, null);

        return $hints;
    }

    /**
     * Add a hint to the list of hints
     * @return $this
     */
    public function addHint($hintMessage, $onKey = false)
    {
        $hints = $this->getHints();
        $onKey === false ? $hints[] = $hintMessage : $hints[$onKey] = $hintMessage;
        $this->setData(static::KEY_HINT_MESSAGE, $hints);

        return $this;
    }

    /**
     * Method that stores service response as string on session
     * @param string $requestName
     * @param string $stubResponse
     */
    public function addServiceResponse(string $requestName, string $stubResponse, $hash)
    {
        if (!$servicesData = $this->getData('services_data')) {
            $servicesData = array();
        }
        $servicesData[$requestName][$hash] = $stubResponse;
        $this->setData('services_data', $servicesData);

        return $this;
    }

    /**
     * Method that clear session cached services response on this session namespace
     * @return $this
     */
    public function clearServiceResponse()
    {
        $this->unsetData('services_data');

        return $this;
    }

    /**
     * Return cached services responses stored on current session
     * @param string|null $requestName
     * @return array|mixed|null
     */
    public function getServicesResponse(string $requestName = null)
    {
        $servicesData = $this->getData('services_data');

        return $requestName ? ($servicesData[$requestName] ?? null) : $servicesData;
    }

    /**
     * Return service customer numbers per stack
     * @param string $stack
     * @return array
     */
    public function getServiceCustomerNumbersPerStack($stack)
    {
        $serviceCustomers = $this->getServiceCustomers();

        if (isset($serviceCustomers[$stack])) {
            return array_unique(array_keys($serviceCustomers[$stack]));
        } else {
            return [];
        }
    }

    /**
     * Returns the unmapped customers of the specified stack/account category
     *
     * @param $stack
     * @return array
     */
    public function getCustomersForStack($stack)
    {
        $stackCustomers = [];
        if ($customerData = $this->getCustomerMode2()) {
            if (!isset($customerData['Customer'][0])) {
                $customerData['Customer'] = [$customerData['Customer']];
            }
            foreach ($customerData['Customer'] as $customer) {
                if ($customer['AccountCategory'] == $stack) {
                    $stackCustomers[] = $customer;
                }
            }
        }

        return $stackCustomers;
    }
}
