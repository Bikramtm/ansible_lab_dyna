<?php

class Dyna_Customer_Block_Adminhtml_Combinations extends Mage_Adminhtml_Block_Widget_Grid_Container{

    /**
     * Dyna_Mobile_Block_Adminhtml_Account constructor.
     */
    public function __construct()
    {
        $this->_controller = "adminhtml_combinations";
        $this->_blockGroup = "dyna_customer";
        $this->_headerText = Mage::helper("dyna_customer/data")->__("Account type/subtype combination");
        $this->_addButtonLabel = Mage::helper("dyna_customer/data")->__("Add New Item");
        parent::__construct();
    }
}
