<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Dyna_Customer_Block_Adminhtml_Activitycodes_Edit extends Mage_Adminhtml_Block_Widget_Form_Container

{
    /**
     *  constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->_objectId = "entity_id";
        $this->_blockGroup = "dyna_customer";
        $this->_controller = "adminhtml_activitycodes";
        $this->_updateButton("save", "label", Mage::helper("customer")->__("Save Item"));
        $this->_updateButton("delete", "label", Mage::helper("customer")->__("Delete Item"));

        $this->_addButton("saveandcontinue", array(
            "label" => Mage::helper("customer")->__("Save And Continue Edit"),
            "onclick" => "saveAndContinueEdit()",
            "class" => "save",
        ), -100);


        $this->_formScripts[] = "function saveAndContinueEdit()" .
            "{editForm.submit($('edit_form').action + 'back/edit/')}";
    }

    /**
     * @return string
     */
    public function getHeaderText()
    {
        if (Mage::registry("dyna_customer_activitycodes") && Mage::registry("dyna_customer_activitycodes")->getId()) {

            return Mage::helper("customer")->__("Edit Item '%s'", $this->htmlEscape(Mage::registry("dyna_customer_activitycodes")->getId()));

        } else {

            return Mage::helper("customer")->__("Add Item");

        }
    }
}