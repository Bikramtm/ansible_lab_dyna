<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Dyna_Customer_Block_Adminhtml_Combinations_Edit extends Mage_Adminhtml_Block_Widget_Form_Container

{
    /**
     *  constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->_objectId = "entity_id";
        $this->_blockGroup = "dyna_customer";
        $this->_controller = "adminhtml_combinations";
        $this->_updateButton("save", "label", Mage::helper("dyna_customer/data")->__("Save Item"));
        $this->_updateButton("delete", "label", Mage::helper("dyna_customer/data")->__("Delete Item"));

        $this->_addButton("saveandcontinue", array(
            "label" => Mage::helper("dyna_customer/data")->__("Save And Continue Edit"),
            "onclick" => "saveAndContinueEdit()",
            "class" => "save",
        ), -100);


        $this->_formScripts[] = "function saveAndContinueEdit()" .
            "{editForm.submit($('edit_form').action + 'back/edit/')}";
    }

    /**
     * @return string
     */
    public function getHeaderText()
    {
        if (Mage::registry("dyna_customer_typeSubtypeCombinations") && Mage::registry("dyna_customer_typeSubtypeCombinations")->getId()) {

            return Mage::helper("dyna_customer/data")->__("Edit Item '%s'", $this->htmlEscape(Mage::registry("dyna_customer_typeSubtypeCombinations")->getId()));

        } else {

            return Mage::helper("dyna_customer/data")->__("Add Item");

        }
    }
}
