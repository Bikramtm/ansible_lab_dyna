<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */
class Dyna_Customer_Block_Adminhtml_Activitycodes_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{

    /**
     * Class constructor
     *
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setDestElementId('activitycodes_form');
    }


    /**
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {

        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset("activitycodes_form", array("legend" => Mage::helper("customer")->__("Item information")));

        $fieldset->addField("activity_code", "text", array(
            "label" => Mage::helper("customer")->__("Activity codes"),
            "name" => "activity_code",
        ));

        $fieldset->addField("allowed", "select", array(
            "label" => Mage::helper("customer")->__("Allowed"),
            "name" => "allowed",
            'values' => array('False','True')
        ));

        if (Mage::getSingleton("adminhtml/session")->getAccountData()) {
            $form->setValues(Mage::getSingleton("adminhtml/session")->getAccountData());
            Mage::getSingleton("adminhtml/session")->setAccountData(null);
        } elseif (Mage::registry("dyna_customer_activitycodes")) {
            $form->setValues(Mage::registry("dyna_customer_activitycodes")->getData());
        }
        return parent::_prepareForm();
    }
}
