<?php

/**
 * CustomerDE block
 */
class Dyna_Customer_Block_CustomerSearch extends Mage_Core_Block_Template
{
    /** @var Dyna_Customer_Model_Session|null */
    protected $storage = null;

    /**
     * Get the uct params from session
     *
     * @return array
     */
    public function getUctParams()
    {
        return $this->getStorage()->getUctParams();
    }

    /**
     * @return mixed|null
     */
    public function getCustomerId()
    {
        return $this->getStorage()->getCustomerId();
    }

    /**
     * @return Dyna_Customer_Model_Session
     */
    protected function getStorage()
    {
        if (!$this->storage) {
            $this->storage = Mage::getSingleton('dyna_customer/session');
        }

        return $this->storage;
    }
}
