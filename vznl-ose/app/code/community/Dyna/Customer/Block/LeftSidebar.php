<?php

/**
 * CustomerDE block
 */
class Dyna_Customer_Block_LeftSidebar extends Mage_Core_Block_Template
{
    /** @var Dyna_Customer_Model_Session|null */
    protected $storage = null;

    /**
     * @return Dyna_Customer_Model_Session
     */
    protected function getCustomerSession()
    {
        if (!$this->storage) {
            $this->storage = Mage::getSingleton('dyna_customer/session');
        }

        return $this->storage;
    }

    public function isLoggedIn()
    {

        return $this->getCustomerSession()->isLoggedIn();
    }

}
