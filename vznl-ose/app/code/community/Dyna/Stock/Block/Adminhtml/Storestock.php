<?php


class Dyna_Stock_Block_Adminhtml_Storestock extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

	$this->_controller = "adminhtml_storestock";
	$this->_blockGroup = "stock";
	$this->_headerText = Mage::helper("stock")->__("Storestock Manager");
	$this->_addButtonLabel = Mage::helper("stock")->__("Add New Item");
	parent::__construct();
	
	}

}