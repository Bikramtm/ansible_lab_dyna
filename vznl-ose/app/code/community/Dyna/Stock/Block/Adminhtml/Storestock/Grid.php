<?php

class Dyna_Stock_Block_Adminhtml_Storestock_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("storestockGrid");
				$this->setDefaultSort("entity_id");
				$this->setDefaultDir("DESC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("stock/storestock")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("entity_id", array(
				"header" => Mage::helper("stock")->__("ID"),
				"align" =>"right",
				"width" => "50px",
			    "type" => "number",
				"index" => "entity_id",
				));
                
				$this->addColumn("product_id", array(
				"header" => Mage::helper("stock")->__("Magento product ID"),
				"index" => "product_id",
				));
				$this->addColumn("store_id", array(
				"header" => Mage::helper("stock")->__("Store ID"),
				"index" => "store_id",
				));
				$this->addColumn("qty", array(
				"header" => Mage::helper("stock")->__("Qty in stock"),
				"index" => "qty",
				));
				$this->addColumn("last_modified", array(
				"header" => Mage::helper("stock")->__("Last modified"),
				"index" => "last_modified",
				));
			$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV')); 
			$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}


		
		protected function _prepareMassaction()
		{
			$this->setMassactionIdField('entity_id');
			$this->getMassactionBlock()->setFormFieldName('entity_ids');
			$this->getMassactionBlock()->setUseSelectAll(true);
			$this->getMassactionBlock()->addItem('remove_storestock', array(
					 'label'=> Mage::helper('stock')->__('Remove Storestock'),
					 'url'  => $this->getUrl('*/adminhtml_storestock/massRemove'),
					 'confirm' => Mage::helper('stock')->__('Are you sure?')
				));
			return $this;
		}
			

}