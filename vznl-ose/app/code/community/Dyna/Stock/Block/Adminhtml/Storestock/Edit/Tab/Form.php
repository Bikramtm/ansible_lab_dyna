<?php
class Dyna_Stock_Block_Adminhtml_Storestock_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("stock_form", array("legend"=>Mage::helper("stock")->__("Item information")));

				
						$fieldset->addField("product_id", "text", array(
						"label" => Mage::helper("stock")->__("Magento product ID"),
						"name" => "product_id",
						));
					
						$fieldset->addField("store_id", "text", array(
						"label" => Mage::helper("stock")->__("Store ID"),
						"name" => "store_id",
						));
					
						$fieldset->addField("qty", "text", array(
						"label" => Mage::helper("stock")->__("Qty in stock"),
						"name" => "qty",
						));
					
						$fieldset->addField("last_modified", "text", array(
						"label" => Mage::helper("stock")->__("Last modified"),
						"name" => "last_modified",
						));
					

				if (Mage::getSingleton("adminhtml/session")->getStorestockData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getStorestockData());
					Mage::getSingleton("adminhtml/session")->setStorestockData(null);
				} 
				elseif(Mage::registry("storestock_data")) {
				    $form->setValues(Mage::registry("storestock_data")->getData());
				}
				return parent::_prepareForm();
		}
}
