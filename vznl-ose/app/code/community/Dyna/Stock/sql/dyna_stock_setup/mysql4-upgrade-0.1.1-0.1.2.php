<?php
$installer = $this;

$installer->startSetup();

$installer->getConnection()->addColumn($this->getTable('stock/stock'), 'warehouse_count', Varien_Db_Ddl_Table::TYPE_INTEGER);

$installer->endSetup();
