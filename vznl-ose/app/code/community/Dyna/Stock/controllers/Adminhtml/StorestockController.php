<?php

class Dyna_Stock_Adminhtml_StorestockController extends Mage_Adminhtml_Controller_Action
{
		protected function _initAction()
		{
				$this->loadLayout()->_setActiveMenu("stock/storestock")->_addBreadcrumb(Mage::helper("adminhtml")->__("Storestock  Manager"),Mage::helper("adminhtml")->__("Storestock Manager"));
				return $this;
		}
		public function indexAction() 
		{
			    $this->_title($this->__("Stock"));
			    $this->_title($this->__("Manager Storestock"));

				$this->_initAction();
				$this->renderLayout();
		}
		public function editAction()
		{			    
			    $this->_title($this->__("Stock"));
				$this->_title($this->__("Storestock"));
			    $this->_title($this->__("Edit Item"));
				
				$id = $this->getRequest()->getParam("id");
				$model = Mage::getModel("stock/storestock")->load($id);
				if ($model->getId()) {
					Mage::register("storestock_data", $model);
					$this->loadLayout();
					$this->_setActiveMenu("stock/storestock");
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Storestock Manager"), Mage::helper("adminhtml")->__("Storestock Manager"));
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Storestock Description"), Mage::helper("adminhtml")->__("Storestock Description"));
					$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
					$this->_addContent($this->getLayout()->createBlock("stock/adminhtml_storestock_edit"))->_addLeft($this->getLayout()->createBlock("stock/adminhtml_storestock_edit_tabs"));
					$this->renderLayout();
				} 
				else {
					Mage::getSingleton("adminhtml/session")->addError(Mage::helper("stock")->__("Item does not exist."));
					$this->_redirect("*/*/");
				}
		}

		public function newAction()
		{

		$this->_title($this->__("Stock"));
		$this->_title($this->__("Storestock"));
		$this->_title($this->__("New Item"));

        $id   = $this->getRequest()->getParam("id");
		$model  = Mage::getModel("stock/storestock")->load($id);

		$data = Mage::getSingleton("adminhtml/session")->getFormData(true);
		if (!empty($data)) {
			$model->setData($data);
		}

		Mage::register("storestock_data", $model);

		$this->loadLayout();
		$this->_setActiveMenu("stock/storestock");

		$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);

		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Storestock Manager"), Mage::helper("adminhtml")->__("Storestock Manager"));
		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Storestock Description"), Mage::helper("adminhtml")->__("Storestock Description"));


		$this->_addContent($this->getLayout()->createBlock("stock/adminhtml_storestock_edit"))->_addLeft($this->getLayout()->createBlock("stock/adminhtml_storestock_edit_tabs"));

		$this->renderLayout();

		}
		public function saveAction()
		{

			$post_data=$this->getRequest()->getPost();


				if ($post_data) {

					try {

						

						$model = Mage::getModel("stock/storestock")
						->addData($post_data)
						->setId($this->getRequest()->getParam("id"))
						->save();

						Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Storestock was successfully saved"));
						Mage::getSingleton("adminhtml/session")->setStorestockData(false);

						if ($this->getRequest()->getParam("back")) {
							$this->_redirect("*/*/edit", array("id" => $model->getId()));
							return;
						}
						$this->_redirect("*/*/");
						return;
					} 
					catch (Exception $e) {
						Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
						Mage::getSingleton("adminhtml/session")->setStorestockData($this->getRequest()->getPost());
						$this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
					return;
					}

				}
				$this->_redirect("*/*/");
		}



		public function deleteAction()
		{
				if( $this->getRequest()->getParam("id") > 0 ) {
					try {
						$model = Mage::getModel("stock/storestock");
						$model->setId($this->getRequest()->getParam("id"))->delete();
						Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item was successfully deleted"));
						$this->_redirect("*/*/");
					} 
					catch (Exception $e) {
						Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
						$this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
					}
				}
				$this->_redirect("*/*/");
		}

		
		public function massRemoveAction()
		{
			try {
				$ids = $this->getRequest()->getPost('entity_ids', array());
				foreach ($ids as $id) {
                      $model = Mage::getModel("stock/storestock");
					  $model->setId($id)->delete();
				}
				Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item(s) was successfully removed"));
			}
			catch (Exception $e) {
				Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
			}
			$this->_redirect('*/*/');
		}
			
		/**
		 * Export order grid to CSV format
		 */
		public function exportCsvAction()
		{
			$fileName   = 'storestock.csv';
			$grid       = $this->getLayout()->createBlock('stock/adminhtml_storestock_grid');
			$this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
		} 
		/**
		 *  Export order grid to Excel XML format
		 */
		public function exportExcelAction()
		{
			$fileName   = 'storestock.xml';
			$grid       = $this->getLayout()->createBlock('stock/adminhtml_storestock_grid');
			$this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
		}

		//Patch for SUPEE-6285
		protected function _isAllowed()
		{
			return true;
		}
}
