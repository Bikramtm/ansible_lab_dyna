<?php

/**
 * Class Dyna_Stock_Model_Stock
 */
class Dyna_Stock_Model_Stock extends Mage_Core_Model_Abstract
{
    const STATUS_IN_STOCK = 'in stock';
    const STATUS_OUT_OF_STOCK = 'out of stock';
	const LOCAL_STOCK_THRESHOLD_ATTRIBUTE_CODE = 'axi_safety_stock';
	const BACKORDER_THRESHOLD_ATTRIBUTE_CODE = 'axi_threshold';
	const STOCK_STATUS_ATTRIBUTE_CODE = 'stock_status';

    /** @var Dyna_Configurator_Model_Cache */
    protected $_cache;

    /**
     * Init entity
     */
    protected function _construct()
    {
        $this->_init('stock/stock');
    }

    /** @var Dyna_Service_Model_Client_AxiClient */
    protected static $client;
	
    protected static function getAxiClient()
    {
        if ( ! static::$client) {
            static::$client = Mage::helper('dyna_service')->getClient('axi');
        }
        return static::$client;
    }
	
    /**
     * @param Mage_Catalog_Model_Product $product
     * @return mixed
     */
    public function getLiveStockForItem($productCollection, $storeCode = null, $includeWarehouseStock=true, $maxDistance = 200)
    {
        if(is_null($storeCode))
			$storeCode = Mage::helper('agent')->getAxiStore();

        $productSkus = array();
        $stockCollection = new Varien_Data_Collection();
		$accessor = Mage::getModel('dyna_service/dotAccessor');
        if(!is_array($productCollection))
        {
            $productCollection = array($productCollection);
        }
        foreach($productCollection as $product)
        {
            $productSkus[] = $product->getSku();
        }

        // Make the call
        $resultsIncludingWarehouse = static::getAxiClient()->getStock($productSkus, $storeCode, $maxDistance, $includeWarehouseStock);
		if (array_key_exists('product_i_d', $resultsIncludingWarehouse)) { // Only one result
			// Check other store list only one result
			$otherStores = $accessor->getValue($resultsIncludingWarehouse, 'other_store_stock_list.item');
			
			if (array_key_exists('store', $otherStores))
				$otherStoreArr[] = $otherStores;
			else 
				$otherStoreArr = $otherStores;
				
			$stock = Mage::getModel('stock/stock')
				->setProductId($accessor->getValue($resultsIncludingWarehouse, 'product_i_d'))
				->setStoreId($storeCode)
				->setItemCount($accessor->getValue($resultsIncludingWarehouse, 'quantity_in_store'))
				->setWarehouseCount($accessor->getValue($resultsIncludingWarehouse, 'quantity_in_warehouse'))
				->setBackorderCount($accessor->getValue($resultsIncludingWarehouse, 'quantity_in_backorder'))
				->setOtherStoreList($otherStoreArr);
			$stockCollection->addItem($stock);
		} else {
			// Put the results in collection object
			foreach($resultsIncludingWarehouse as $resultIncludingWarehouse)
			{
				// Check other store list only one result
				$otherStores = $accessor->getValue($resultIncludingWarehouse, 'other_store_stock_list.item');
				
				if (array_key_exists('store', $otherStores))
					$otherStoreArr[] = $otherStores;
				else 
					$otherStoreArr = $otherStores;
			
				$stock = Mage::getModel('stock/stock')
					->setProductId($accessor->getValue($resultIncludingWarehouse, 'product_i_d'))
					->setStoreId($storeCode)
					->setItemCount($accessor->getValue($resultIncludingWarehouse, 'quantity_in_store'))
					->setWarehouseCount($accessor->getValue($resultIncludingWarehouse, 'quantity_in_warehouse'))
					->setOtherStoreList($otherStoreArr);
				$stockCollection->addItem($stock);
			}
		}

        return $stockCollection;
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @return mixed
     */
    public function getStockForItem(Mage_Catalog_Model_Product $product)
    {
        // TODO: Should filter by store ID and add this to teh cache key as well
        $key = serialize(array(__METHOD__, $product->getSku()));
        if ($result = unserialize($this->getCache()->load($key))) {
            return $result;
        } else {
            $stock = Mage::getResourceModel('stock/stock_collection')
                ->addFieldToFilter('product_id', $product->getSku())
                ->getFirstItem();

            $this->getCache()->save(serialize($stock), $key, array(Dyna_Configurator_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());

            return $stock;
        }
    }

    /**
     * @param array $productSkus
     * @return mixed
     */
    public function getStockForProducts(array $productSkus = array())
    {
        // TODO: Should filter by store ID and add this to teh cache key as well
        $key = serialize(array(__METHOD__, $productSkus));
        if ($result = unserialize($this->getCache()->load($key))) {
		     return $result;
        } else {
            /** @var Dyna_Stock_Model_Mysql4_Stock_Collection $stocks */
            $stocks = Mage::getResourceModel('stock/stock_collection')
                ->addFieldToFilter('product_id', array('in' => $productSkus));

            $stockCollection = new Varien_Data_Collection();
            foreach ($stocks->getItems() as $item) {
                $stockCollection->addItem($item);
            }
            $this->getCache()->save(serialize($stockCollection), $key, array(Dyna_Configurator_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());

            return $stockCollection;
       }
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @return int
     */
    public function getItemCount(Mage_Catalog_Model_Product $product = null)
    {
        return $product && $product->getSku() != $this->getProductId()
            ? (int) $this->getItemCountForProduct($product)
            : (int) $this->getData('item_count');
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @return string|null
     */
    protected function getItemCountForProduct(Mage_Catalog_Model_Product $product)
    {
        // TODO: Should filter by store ID and add this to teh cache key as well
        $key = serialize(array(__METHOD__, $product->getSku()));
        if (($result = unserialize($this->getCache()->load($key))) instanceof Dyna_Stock_Model_Stock) {
            return $result->getItemCount();
        } else {
            $stock = $this->getResourceCollection()
                ->addFieldToFilter('product_id', $product->getSku())
                ->load()
                ->getFirstItem();

            if ($stock) {
                $this->getCache()->save(serialize($stock), $key, array(Dyna_Configurator_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
                return $stock->getItemCount();
            }
            return null;
        }
    }

    /**
     * @param array $storeList
     * @return $this
     */
    public function setOtherStoreList(array $storeList = null)
    {
        $this->setData('other_store_list', json_encode($storeList));
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOtherStoreList()
    {
        $result = json_decode($this->getData('other_store_list'), true);
        if ( ! is_array($result)) {
            return array();
        }
        return $result;
    }

    /**
     * @return Dyna_Configurator_Model_Cache
     */
    protected function getCache()
    {
        if ( ! $this->_cache) {
            $this->_cache = Mage::getSingleton('dyna_configurator/cache');
        }
        return $this->_cache;
    }
}