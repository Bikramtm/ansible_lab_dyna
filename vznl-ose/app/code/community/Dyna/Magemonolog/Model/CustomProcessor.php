<?php

/**
 * Class Dyna_Magemonolog_Model_CustomProcessor
 */
class Dyna_Magemonolog_Model_CustomProcessor
{
    /**
     * Dyna_Magemonolog_Model_CustomProcessor constructor.
     *
     * @param array $defaultArgs
     */
    public function __construct(array $defaultArgs = array())
    {
        $this->defaultArgs = $defaultArgs;
    }

    /**
     * @param array $record
     *
     * @return array
     */
    public function __invoke(array $record)
    {
        foreach ($this->defaultArgs as $argName => $val) {
            if (!isset($record['extra'][$argName])) {
                $record['extra'][$argName] = $val;
            }
        }
        $record['extra']['system'] = gethostname();
        $record['extra']['timestamp'] = $record['datetime']->getTimestamp();
        $record['message'] = preg_replace('/\|/', ' ', $record['message']);

        return $record;
    }
}
