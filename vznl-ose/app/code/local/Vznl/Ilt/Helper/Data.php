<?php

use Omnius\InkomensLastenToets\ExternalAdapter\Bsl\Factory as IltFactory;
use Omnius\InkomensLastenToets\ExternalAdapter\Stub\Factory as IltStubFactory;
use Ramsey\Uuid\Uuid;

class Vznl_Ilt_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * @var array
     */
    protected $allowedFamilyTypes = [
        'Single',
        'Single with Children',
        'Couple',
        'Couple with Children',
    ];

    /**
     * Validate all the data keys
     * @param array $data
     * @return bool
     */
    public function validateData(array $data) : array
    {
        $response = [];
        if(!isset($data['family_type']) || !$this->isValidFamilyType($data['family_type'])) {
            $response['family_type'] = Mage::helper('dyna_checkout')->__('Given family type is not valid');
        }
        if(!isset($data['income']) || !ctype_digit($data['income'])) {
            $response['income'] = Mage::helper('dyna_checkout')->__('Given income is not a valid value');
        }
        if(!isset($data['housing_costs']) || !ctype_digit($data['housing_costs'])) {
            $response['housing_costs'] = Mage::helper('dyna_checkout')->__('Given housing costs are invalid');
        }
        if(!isset($data['birth_lastname']) || !is_string($data['birth_lastname'])) {
            $response['birth_lastname'] = Mage::helper('dyna_checkout')->__('Lastname is invalid');
        }
        return $response;
    }

    /**
     * Return all the allowed family types
     * @return array
     */
    public function getAllowedFamilyTypes() : array
    {
        return $this->allowedFamilyTypes;
    }

    /**
     * Check if the given family type is valid or not
     * @param $familyType
     * @return bool
     */
    public function isValidFamilyType($familyType) : bool
    {
        if(in_array($familyType, $this->allowedFamilyTypes)){
            return true;
        }
        return false;
    }

    /**
     * Store the ilt data in the db
     * @param Vznl_Superorder_Model_Superorder $superOrder
     * @param array $data The ilt data.
     * @return bool Whether the data was saved successful
     */
    public function storeIltData(Vznl_Superorder_Model_Superorder $superOrder, array $data) : bool
    {
        // @todo communicate with Repository
        if(count($this->validateData($data)) == 0) {
            $this->removeIltData($superOrder);

            $ilt = Mage::getModel('ilt/ilt')->load($superOrder->getId(), 'superorder_id');
            $ilt->setData($data);
            $ilt->setSuperorderId($superOrder->getId());
            $ilt->setCreatedAt(date('Y-m-d H:i:s'));
            $ilt->save();
            return true;
        }
        return false;
    }

    /**
     * Remove ilt data from the db for the given superorder.
     * @param Vznl_Superorder_Model_Superorder $superOrder The super order to delete the ilt data from.
     * @return bool Whether the data was removed successful.
     */
    public function removeIltData(Vznl_Superorder_Model_Superorder $superOrder){
        if(!$superOrder || !$superOrder->getOrderNumber()){
            return false;
        }

        try{
            /** @var Vznl_Ilt_Model_Ilt $iltData */
            $iltData = Mage::getModel('ilt/ilt')->load($superOrder->getId(), 'superorder_id');
            if(!$iltData->getEntityId()){
                return false;
            }

            $iltData->delete();
        }
        catch(Exception $e){
            return false;
        }

        return true;
    }

    /**
     * @param Vznl_Superorder_Model_Superorder $superOrder
     * @param Mage_Customer_Model_Customer $customer
     */
    public function sendParsedIltData($superOrder, $customer)
    {
        // Check for ILT
        if ($superOrder->isIltRequired()) {
            $additionalAttributes = Mage::getModel('omnius_checkout/additionalAttributes')
                ->getAdditionalAttributes($customer->getId(), 'customer');
            if (!empty($additionalAttributes)) {
                $valuesToStore = [
                    'ILT_FAMILY_TYPE' => 'family_type',
                    'ILT_INCOME' => 'income',
                    'ILT_HOUSING_COSTS' => 'housing_costs',
                    'ILT_BIRTH_MIDDLENAME' => 'birth_middlename',
                    'ILT_BIRTH_LASTNAME' => 'birth_lastname'
                ];
                /** @var Vznl_Ilt_Helper_Data $iltHelper */
                $iltHelper = Mage::helper('ilt');

                foreach ($additionalAttributes as $key => $value) {
                    $keyName = $valuesToStore[$key];
                    if (isset($keyName)) {
                        $iltData[$keyName] =
                            $keyName === 'family_type' ?
                                $iltHelper->getAllowedFamilyTypes()[$value - 1] ?? $value
                                : $value;
                    }
                }

                if (!empty($iltData)) {
                    if (empty($iltData['birth_lastname'])) {
                        $iltData['birth_lastname'] = $customer->getLastNname();
                    }
                    if (empty($iltData['birth_middlename'])) {
                        $iltData['birth_middlename'] = $customer->getMiddlename();
                    }

                    $this->storeIltData($superOrder, $iltData);

                    // Trigger ILT call
                    $iltRepository = new Vznl_Ilt_Repository_SuperorderIlt();
                    $iltModel = $iltRepository->get($superOrder->getOrderNumber());
                    if ($iltModel) {
                        $this->sendIltData($superOrder);
                    }
                }
            }
        }
    }

    /**
     * @param Vznl_Superorder_Model_Superorder $superOrder
     * @return bool
     */
    protected function sendIltData(Vznl_Superorder_Model_Superorder $superOrder)
    {
        try {
            $iltRepository = new Vznl_Ilt_Repository_SuperorderIlt();
            $loggerWriter = new Aleron75_Magemonolog_Model_Logwriter('services/Ilt/' . Uuid::uuid4() . '.log');
            if (Mage::getStoreConfig('vodafone_service/ilt/use_stubs')) {
                $iltAdapter = IltStubFactory::create($iltRepository, $loggerWriter->getLogger());
            } else {
                $agent = Mage::getSingleton('customer/session')->getAgent();
                $dealer = Mage::getModel('agent/dealer')->load(
                    $superOrder->getOriginalQuote()->getDealerId()
                );

                $iltAdapter = IltFactory::create(
                    Mage::getStoreConfig('vodafone_service/ilt/wsdl'),
                    $iltRepository,
                    $loggerWriter->getLogger(),
                    [
                        'username' => Mage::getStoreConfig('vodafone_service/ilt/bsl_username'),
                        'password' => Mage::getStoreConfig('vodafone_service/ilt/bsl_password'),
                        'agent' => $agent->getUsername(),
                        'dealercode' => $dealer->getVfDealerCode(),
                        'service_endpoint' => Mage::getStoreConfig('vodafone_service/ilt/usage_url'),
                        'client_options' => Mage::helper('vznl_core/service')->getZendSoapClientOptions('ilt') ?: []
                    ]
                );
            }
            $data = $iltRepository->get($superOrder->getOrderNumber());
            if (!$iltAdapter->send($data)) {
                return false;
            }
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
}
