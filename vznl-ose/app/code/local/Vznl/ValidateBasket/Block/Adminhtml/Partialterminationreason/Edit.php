<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */

/**
 * Class Vznl_ValidateBasket_Block_Adminhtml_Partialterminationreason_Edit
 */
class Vznl_ValidateBasket_Block_Adminhtml_Partialterminationreason_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * Vznl_ValidateBasket_Block_Adminhtml_Partialterminationreason_Edit constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_objectId = 'entity_id';
        $this->_blockGroup = 'validatebasket';
        $this->_controller = 'adminhtml_partialterminationreason';

        $this->_updateButton('save', 'label', Mage::helper('vznl_validatebasket')->__('Save Partial Termination Reason'));

        $objId = $this->getRequest()->getParam($this->_objectId);

        if (!empty($objId)) {
            $this->_addButton('delete', array(
                'label' => Mage::helper('vznl_validatebasket')->__('Delete'),
                'class' => 'delete',
                'onclick' => 'deleteConfirm(\''
                    . Mage::helper('core')->jsQuoteEscape(
                        Mage::helper('vznl_validatebasket')->__('Are you sure you want to do this?')
                    )
                    . '\', \''
                    . $this->getDeleteUrl()
                    . '\')',
            ));
        }
    }

    /**
     * @return string
     */
    public function getHeaderText()
    {
        if (Mage::registry('partial_termination_reason_data') && Mage::registry('partial_termination_reason_data')->getEntityId()) {
            return Mage::helper('vznl_validatebasket')->__("Edit partial termination reason with id '%s'", $this->escapeHtml(Mage::registry('partial_termination_reason_data')->getEntityId()));
        } else {
            return Mage::helper('vznl_validatebasket')->__('Add new partial termination reason');
        }
    }
}
