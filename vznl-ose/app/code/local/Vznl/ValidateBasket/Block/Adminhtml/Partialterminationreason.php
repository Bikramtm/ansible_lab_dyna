<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */

/**
 * Class Vznl_ValidateBasket_Block_Adminhtml_Partialterminationreason
 */
class Vznl_ValidateBasket_Block_Adminhtml_Partialterminationreason extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_partialterminationreason';
        $this->_blockGroup = 'validatebasket';
        $this->_headerText = 'Partial Termination Reasons';
        $this->_addButtonLabel = 'Add new partial termination reason';
        parent::__construct();
    }
}
