<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */

/**
 * Class Vznl_ValidateBasket_Block_Adminhtml_Partialterminationsubreason
 */
class Vznl_ValidateBasket_Block_Adminhtml_Partialterminationsubreason extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_partialterminationsubreason';
        $this->_blockGroup = 'validatebasket';
        $this->_headerText = 'Partial Termination Subreasons';
        $this->_addButtonLabel = 'Add new partial termination subreason';
        parent::__construct();
    }
}
