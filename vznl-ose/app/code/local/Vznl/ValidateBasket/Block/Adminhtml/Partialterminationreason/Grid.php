<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */

/**
 * Class Vznl_ValidateBasket_Block_Adminhtml_Partialterminationreason_Grid
 */
class Vznl_ValidateBasket_Block_Adminhtml_Partialterminationreason_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('partialTerminationreason');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('validateBasket/partialterminationreason_collection');
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * Create partial termination reason list
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareColumns()
    {
        $this->addColumn('entity_id', array(
            'header'    => Mage::helper('vznl_validatebasket')->__('ID'),
            'align'     =>'right',
            'width'     => '30px',
            'index'     => 'entity_id',
        ));

        $this->addColumn('code', array(
            'header'        => Mage::helper('vznl_validatebasket')->__('Code'),
            'align'         => 'left',
            'index'         => 'code',
        ));

        $this->addColumn('en_value', array(
            'header'    => Mage::helper('vznl_validatebasket')->__('English Value'),
            'align'     => 'left',
            'index'     => 'en_value',
            
        ));

        $this->addColumn('nl_value', array(
            'header'    => Mage::helper('vznl_validatebasket')->__('Dutch Value'),
            'align'     => 'left',
            'index'     => 'nl_value',
            
        ));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('entity_id' => $row->getEntityId()));
    }
}
