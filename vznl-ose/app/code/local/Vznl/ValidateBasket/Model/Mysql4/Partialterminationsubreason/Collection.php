<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */

/**
 * Class Vznl_ValidateBasket_Model_Mysql4_Partialterminationsubreason_Collection
 */
class Vznl_ValidateBasket_Model_Mysql4_Partialterminationsubreason_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        $this->_init("validateBasket/partialterminationsubreason");
    }
}
