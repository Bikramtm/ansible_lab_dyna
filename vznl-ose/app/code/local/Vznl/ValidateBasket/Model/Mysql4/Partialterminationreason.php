<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */

/**
 * Class Vznl_ValidateBasket_Model_Mysql4_Partialterminationreason
 */
class Vznl_ValidateBasket_Model_Mysql4_Partialterminationreason extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init("validateBasket/partialterminationreason", "entity_id");
    }
}
