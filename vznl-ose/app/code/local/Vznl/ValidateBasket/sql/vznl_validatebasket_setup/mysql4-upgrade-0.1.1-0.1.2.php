<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */

/**
 * @category    Vznl
 * @package     Vznl_ValidateBasket
 */

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();

/**
 * Create table
 */
if (!$installer->tableExists($installer->getTable('validateBasket/partialterminationreason'))) {
    $table = $installer->getConnection()
        ->newTable($installer->getTable('validateBasket/partialterminationreason'))
        ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity'  => true,
            'unsigned'  => true,
            'nullable'  => false,
            'primary'   => true,
        ), 'ID')
        ->addColumn('code', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        ), 'Partial termination main reason code')
        ->addColumn('en_value', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        ), 'English Translation')
        ->addColumn('nl_value', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        ), 'Dutch Translation')
        ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        ), 'Created')
        ->addColumn('updated_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        ), 'Updated')

        ->addIndex($installer->getIdxName($installer->getTable('validateBasket/partialterminationreason'), array('code'), Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE),
            array('code'), array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE))
        ->setComment('Partial termination reasons');

    $installer->getConnection()->createTable($table);
}

$installer->endSetup();
