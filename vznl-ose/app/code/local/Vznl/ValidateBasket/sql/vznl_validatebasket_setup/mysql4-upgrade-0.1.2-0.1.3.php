<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */

/**
 * @category    Vznl
 * @package     Vznl_ValidateBasket
 */

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();

/**
 * Create table
 */
if (!$installer->tableExists($installer->getTable('validateBasket/partialterminationsubreason'))) {
    $table = $installer->getConnection()
        ->newTable($installer->getTable('validateBasket/partialterminationsubreason'))
        ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity'  => true,
            'unsigned'  => true,
            'nullable'  => false,
            'primary'   => true,
        ), 'ID')
        ->addColumn('main_reason_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'unsigned'  => true,
            'nullable'  => false,
        ), 'Partial Termination Main Reason Id')
        ->addColumn('code', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        ), 'Partial termination subreason code')
        ->addColumn('en_value', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        ), 'English Translation')
        ->addColumn('nl_value', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        ), 'Dutch Translation')
        ->addColumn('include_competitor', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        ), 'Include or Exclude Competitor')
        ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        ), 'Created')
        ->addColumn('updated_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        ), 'Updated')

        ->addIndex($installer->getIdxName($installer->getTable('validateBasket/partialterminationsubreason'), array('code'), Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE),
            array('code'), array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE))
        ->setComment('Partial termination subreasons');

    $installer->getConnection()->createTable($table);
}

$installer->endSetup();
