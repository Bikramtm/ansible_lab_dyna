<?php

/**
 * Class Vznl_Configurator_Model_Cache
 */
class Vznl_Configurator_Model_Cache extends Varien_Object
{
    const CACHE_TAG = 'APPLICATION_CACHE';
    const AGENT_TAG = 'AGENT_DEALER_CACHE';
    const PRODUCT_TAG = 'DYNA_PRODUCT_CACHE';
    const REASON_TAG = 'DYNA_REASON_CACHE';

    /** @var string */
    protected $_prefix = 'dyna_configurator';

    /** @var Varien_Cache_Core */
    protected $_cache;

    /** @var int|mixed */
    protected $_cacheTTL = 86400; //default in case it's not set in the backend

    /** @var bool */
    protected $_isDev = false;

    /** @var array */
    protected $_registry = array();

    /** @var null */
    protected $_currMemoryLimit = -1;

    /**
     * Minimum amount of memory allowed for PHP to use
     * If limit is greater or equal to this, values saved and
     * retrieved from cache will be also kept and updated in
     * memory to reduce calls to the cache backend
     * This limit is present to prevent "Out of memory" issues that may appear
     *
     * @var int
     */
    protected $_minAllocatedMemory = 2048000000; //bytes

    /**
     * Prepare cache instance and config
     */
    public function __construct()
    {
        $appVersion = Mage::helper('dyna_cache')->getAppVersion();
        $this->_prefix = $appVersion ? md5($appVersion) : $this->_prefix;
        $this->_cache = Mage::app()->getCache();
        $this->_cacheTTL = (int) Mage::getStoreConfig(Vznl_Core_Helper_Service::ENVIRONMENT_CACHE_TTL);
        $this->_isDev = (bool) Mage::getStoreConfigFlag(Vznl_Core_Helper_Service::ENVIRONMENT_MODE_CONFIG_PATH);

        if (function_exists('ini_get')) {
            $value = trim(ini_get('memory_limit'));
            $unit = strtolower(substr($value, -1, 1));
            $value = (int) $value;
            switch ($unit) {
                case 'g':
                    $value *= 1024;
                // no break (cumulative multiplier)
                case 'm':
                    $value *= 1024;
                // no break (cumulative multiplier)
                case 'k':
                    $value *= 1024;
            }

            $this->_currMemoryLimit = $value;

            unset($value);
            unset($unit);
        }
    }

    /**
     * @return mixed
     */
    public function isTestMode()
    {
        return $this->_isDev;
    }

    /**
     * @return int|mixed
     */
    public function getTtl()
    {
        return $this->_cacheTTL;
    }

    /**
     * @param bool $forceNewPrefix
     * @return string
     */
    public function getFrontendCachePrefix($forceNewPrefix = false)
    {
        if ((!$forceNewPrefix) && strlen($prefix = trim(Mage::getStoreConfig(Vznl_Core_Helper_Service::ENVIRONMENT_FRONTEND_CACHE_PREFIX)))) {
            return md5($prefix);
        }
        Mage::getConfig()->saveConfig(Vznl_Core_Helper_Service::ENVIRONMENT_FRONTEND_CACHE_PREFIX, $prefix = md5(time() . rand(0, 1000)));
        Mage::getConfig()->cleanCache();

        return md5($prefix);
    }

    /**
     * @return int|mixed
     */
    public function getFrontendCacheTtl()
    {
        if (0 === ($ttl = (int) Mage::getStoreConfig(Vznl_Core_Helper_Service::ENVIRONMENT_FRONTEND_CACHE_TTL))) {
            return $this->_cacheTTL;
        }

        return $ttl;
    }

    /**
     * @param $data
     * @param null $cacheId
     * @param array $tags
     * @param bool $specificLifetime
     * @param int $priority
     * @return Mage_Core_Model_Abstract|void
     */
    public function save($data, $cacheId = null, $tags = array(), $specificLifetime = false, $priority = 8)
    {
        //$this->preserve($cacheId, $data);
        $this->_cache->save($data, $this->prepareId($cacheId), $this->prepareTags($tags), $specificLifetime, $priority);
    }

    /**
     * @param int $cacheId
     * @param bool $skipCacheValidity
     * @param bool $doNotUnserialize
     * @return bool|false|Mage_Core_Model_Abstract|mixed
     */
    public function load($cacheId, $skipCacheValidity = false, $doNotUnserialize = false)
    {
        //on admin, no cache should be used, to prevent confusion (if a product is change, the change will work but will not be visible)
        if (Mage::app()->getStore()->isAdmin() || Mage::getDesign()->getArea() == 'adminhtml') {
            return false;
        }

        if ($this->isTestMode()) {
            return false;
        }

        /*if (false !== ($result = $this->alreadyRequested($cacheId))) {
            return $result;
        }*/

        $result = $this->_cache->load($this->prepareId($cacheId), $skipCacheValidity, $doNotUnserialize);
        /*if (false !== ($result = $this->_cache->load($this->prepareId($cacheId), $skipCacheValidity, $doNotUnserialize))) {
            $this->preserve($cacheId, $result);
        }*/

        return $result;
    }

    /**
     * @param array $tags
     * @param string $mode
     */
    public function clean($tags = array(), $mode = 'all')
    {
        $this->_registry = array();
        $this->_cache->clean($mode, $tags);
    }

    /**
     * @param $cacheId
     * @return string
     */
    protected function prepareId($cacheId)
    {
        return $cacheId ? sprintf('%s_%s', $this->_prefix, (string) $cacheId) : $cacheId;
    }

    /**
     * @param array $tags
     * @return array
     */
    protected function prepareTags(array $tags)
    {
        return $tags;
    }

    /**
     * @param $key
     * @param $value
     */
    protected function preserve($key, $value)
    {
        if ($this->_currMemoryLimit != -1 && $this->_currMemoryLimit >= $this->_minAllocatedMemory) {
            $this->_registry[$key] = $value;
        }
    }

    /**
     * @param $key
     * @return bool
     */
    protected function alreadyRequested($key)
    {
        return isset($this->_registry[$key])
            ? $this->_registry[$key]
            : false;
    }
}
