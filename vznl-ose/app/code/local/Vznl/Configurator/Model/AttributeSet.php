<?php

/**
 * Class Vznl_Configurator_Model_AttributeSet
 */
class Vznl_Configurator_Model_AttributeSet extends Omnius_Configurator_Model_AttributeSet
{
    const PROMOTION = 'promotion';

    const PACKAGE_STATUS_OPEN = 'open';
    const PACKAGE_STATUS_COMPLETED = 'completed';

	const ATTRIBUTE_NETWORK = 'identifier_subscr_brand';
	const ATTRIBUTE_NETWORK_VODAFONE = 'Vodafone';
	const ATTRIBUTE_NETWORK_ZIGGO = 'Ziggo';
	const ATTRIBUTE_NETWORK_HOLLANDSNIEUWE = 'hollandsnieuwe';
	const ATTRIBUTE_NETWORK_SIZZE = 'Sizz';
    const ATTR_GROUP_FIXED_PROMOTIONS = 'Fixed_Promotions';
}
