<?php

/**
 * Class Vznl_Configurator_Model_PackageType
 */
class Vznl_Configurator_Model_PackageType
{
    const PACKAGE_TYPE_MOBILE = 'mobile';
    const PACKAGE_TYPE_INTERNET = 'internet';
    const PACKAGE_TYPE_PREPAID = 'prepaid';
    const PACKAGE_TYPE_HARDWARE = 'hardware';
    const PACKAGE_TYPE_FIXED = 'int_tv_fixtel';

    const TELEPHONY_FIRST_LINE = 'First';
    const TELEPHONY_SECOND_LINE = 'Second';
    const TELEPHONY_BOTH_LINE = 'Both';

    const ORDER_TYPE_PRODUCT = 'PRODUCT_ORDER';
    const ORDER_TYPE_MOVE = 'MOVE_ORDER';
}
