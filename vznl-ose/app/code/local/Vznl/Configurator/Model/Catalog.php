<?php

/**
 * Class Vznl_Configurator_Model_Catalog
 */
class Vznl_Configurator_Model_Catalog extends Dyna_Configurator_Model_Catalog
{
    /**
     * These are the only product attributes available in the Configurator Frontend
     *
     * @var array
     */
    protected $frontendAttributes = [
        'aikido',
        'sku',
        'thumbnail',
        'image',
        'entity_id',
        'name',
        'display_name_configurator',
        'stock_status_class',
        'stock_status_text',
        'stock_status',
        'rel_promotional_maf',
        'rel_promotional_maf_with_tax',
        'rel_regular_maf',
        'rel_regular_maf_with_tax',
        'regular_maf',
        'regular_maf_with_tax',
        'price',
        'price_with_tax',
        'sim_type',
        'maf',
        'group_type',
        'is_discontinued',
        'sim_only',
        'business_product',
        'include_btw',
        'contract_period',
        'available_stock_ind',
        'initial_period',
        'bandwidth',
        'minimum_contract_duration',
        'available_stock_ind',
        'additional_text',
        'has_maf',
        'has_price',
        'relation_components',
        'service_description',
        'product_visibility',
        'initial_selectable',
        Vznl_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR,
        Vznl_Catalog_Model_Product::CATALOG_PACKAGE_TYPE_ATTR,
        Vznl_Catalog_Model_Product::CATALOG_PACKAGE_TYPE_ID,
        Vznl_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ID,
        'display_price',
        'display_price_repeated',
        'display_price_sliding',
        'type',
        'hierarchy_value',
        'lifecycle_status',
        'product_version',
        'product_family',
        'contract_value',
        'redplus_role',
        'product_type',
        'use_service_value',
        'service_xpath',
        'prodspecs_network',
        'prodspecs_brand',
        'prodspecs_kleur',
        'has_volte',
        'loan_indicator',
        'is_ilt_required',
        'identifier_device_post_prepaid',
        'product_segment',
        'identifier_subscr_type',
        'identifier_subscr_type_data',
        'identifier_subsc_postp_simonly',
        'identifier_commitment_months',
        'prodspecs_besturingssysteem',
        'prodspecs_afmetingen_lengte',
        'prodspecs_afmetingen_breedte',
        'prodspecs_afmetingen_dikte',
        'prodspecs_afmetingen_unit',
        'prodspecs_weight_gram',
        'prodspecs_batterij',
        'prodspecs_schermdiagonaal_inch',
        'prodspecs_schermresolutie',
        'prodspecs_touchscreen',
        'prodspecs_camera',
        'prodspecs_megapixels_1e_cam',
        'prodspecs_processor',
        'prodspecs_werkgeheugen_mb',
        'prodspecs_standby_tijd',
        'prodspecs_spreektijd',
        'prodspecs_network',
        'prodspecs_filmen_in_hd',
        'prodspecs_audio_out',
        'prodspecs_hdmi',
        'prodspecs_2e_camera',
        'prodspecs_4g_lte',
        'prodspecs_3g',
        'prodspecs_gprs_edge',
        'prodspecs_wifi',
        'prodspecs_wifi_frequenties',
        'prodspecs_bluetooth',
        'prodspecs_bluetooth_version',
        'prodspecs_nfc',
        'prodspecs_gps',
        'prodspecs_lichtsensor',
        'prodspecs_opslag_uitbreidbaar',
        'prodspecs_flitser',
        'prodspecs_voice_dialing',
        'prodspecs_spraakbesturing',
        'prodspecs_accu_verwisselb',
        'axi_threshold',
        'serial_number',
        'hardware_name',
        'contract_start_date'
    ];

    private $axiStoreCode;

    /**
     * @return string
     */
    public function getAxiStoreCode()
    {
        return $this->axiStoreCode;
    }

    /**
     * @param string $axiStoreCode
     * @return Vznl_Configurator_Model_Catalog
     */
    public function setAxiStoreCode($axiStoreCode)
    {
        $this->axiStoreCode = $axiStoreCode;

        return $this;
    }

    /**
     * @return array
     */
    public function getFrontendAttributes(): array
    {
        return $this->frontendAttributes;
    }

    /**
     * @param array $frontendAttributes
     * @return $this
     */
    public function setFrontendAttributes(array $frontendAttributes)
    {
        $this->frontendAttributes = $frontendAttributes;

        return $this;
    }



    /**
     * Gets the frontend product, removing data with keys that are not set inside $keys array.
     * @param Dyna_Catalog_Model_Product $item The item to use.
     * @param array $keys The keys to keep.
     * @param array $mandatoryCategories The mandatory categories.
     * @param string $productTypeValue The product type value to check.
     * @return Dyna_Catalog_Model_Product The adjusted product.
     */
    public function getFrontendProduct($item, $keys, $mandatoryCategories, $productTypeValue)
    {
        /** @var Mage_Catalog_Model_Resource_Product $productResource */
        $productResource = Mage::getResourceModel("catalog/product");
        $attrHelper = Mage::helper('omnius_configurator/attribute');

        $data = $this->parseProductDataForType($item, $attrHelper, $keys);

        // Building family information
        $familyIds = $item->getCategoryIds(true);
        $mandatory = array_intersect($familyIds, $mandatoryCategories);
        // don't touch the following line, will break the entire configurator
        $data['families'] = array_combine(array_map("intval", array_values($mandatory)), array_values($mandatory));
        $data['category_ids'] = $productResource->getCategoryIdsWithAnchors($item);

        // check if the option cable type is "container" (the display prices are used only for containers)
        if (isset($data['type']) && $data['type'] == $productTypeValue) {
            $data['isContainer'] = true;
            $data['display_price'] = is_null($data['display_price']) ? 0.00 : (float)str_replace(",", ".", $data['display_price']);
            $data['display_price_repeated'] = is_null($data['display_price_repeated']) ? 0.00 : (float)str_replace(",", ".", $data['display_price_repeated']);
        } else {
            $data['isContainer'] = false;
            $data['display_price'] = 0.00;
            $data['display_price_repeated'] = 0.00;
        }

        try {
            $thumbnailImage = Mage::helper('vznl_catalog')->getThumbnailBySku((string)$item->getSku());
        } catch (Exception $exception) {
            $thumbnailImage = false;
        }
        $item->setData($data);
        $item->setData("thumbnailImage", $thumbnailImage);
        $item->setData("thumbnailImageRetina", Mage::helper('vznl_catalog')->getThumbnailBySku((string)$item->getSku(), true));
        $item->setData("maf", $item->getMaf());
        $item->setData("bandwidth", $item->getBandwidth() / 1000);
        $item->setData("available_stock_ind", (int)$item->getAvailableStockInd());
        $item->setData("has_maf", $item->getMaf() && (bool)$item->getMaf() >= 0);
        $item->setData("has_price", (bool)$item->getPrice());
        $item->setData("requires_serial", $item->isOwnReceiver() || $item->isOwnSmartcard());
        $item->setData("is_own_receiver", $item->isOwnReceiver());
        $item->setData("is_own_smartcard", $item->isOwnSmartcard());
        $item->setData("name", $item->getDisplayNameConfigurator() ?: $item->getName());
        $item->setData("product_visibility", explode(",", $item->getProductVisibility()));
        $item->setData("sim_only", $item->getSimOnly());

        // Handle some custom vznl stuff
        $item->setData('is_ilt_required', $item->isIltRequired());
        $this->getFrontendProductVolte($item);
        $this->getFrontendProductInformation($item);
        $this->getFrontendProductStockInfo($item);
        $item->setData("prodspecs_network", explode(",", $item->getProdspecsNetwork()));
        $item->setData("product_segment", explode(",", $item->getProductSegment()));

        $customerSession = Mage::getSingleton('customer/session');
        $hardwareDetailsIls = $customerSession->getHardwareDetailsIls();
        $item->setData("serial_number", $hardwareDetailsIls[$item->getSku()]['serialNumber']);
        $item->setData("hardware_name", $hardwareDetailsIls[$item->getSku()]['hardwareName']);
        $item->setData("package_type_name", strtoupper($item->getData('package_type_id')));

        return $item;
    }

    /**
     * Gets the frontend product's volte setting
     * @param Dyna_Catalog_Model_Product $item The item to use.
     */
    protected function getFrontendProductVolte($item)
    {
        if ($item->isDevice()) {
            $item->setData('has_volte', $item->hasVoLTE());
        }
    }

    /**
     * Gets the frontend product's product information
     * @param Dyna_Catalog_Model_Product $item The item to use.
     */
    protected function getFrontendProductInformation($item)
    {
        /** @var Dyna_Configurator_Helper_Data $configuratorHelper */
        $configuratorHelper = Mage::helper('dyna_configurator');
        $productInformation = $configuratorHelper->getProductInformation($item);
        if ($productInformation) {
            $item->setData('product_information', $productInformation);
        }
    }

    /**
     * Gets the frontend product's stock information
     * @param Dyna_Catalog_Model_Product $item The item to use.
     */
    protected function getFrontendProductStockInfo($item)
    {
        if ($item->isOfHardwareType(true)) {
            $item->setData('is_device', '1');

            $storeCode = $this->getCurrentAxiStoreCode();

            $productId = $item->getData('entity_id');
            $stocks = Mage::helper('stock')->getStockForProducts(array($productId), $storeCode);

            if (!empty($stocks[$productId])) {
                //check if sellable based on the product's stock
                $item->isSellable($stocks[$productId]['qty'], $stocks[$productId]['backorder_qty']);
            } else {
                //if the system doesn't find any stock for the current product => fallback with no stock
                $item->isSellable(0, 0);
            }
        }
    }

    /**
     * Retrieve axi store code from session if current store is retail
     * @return string|null
     */
    private function getCurrentAxiStoreCode()
    {
        if (!$this->axiStoreCode) {
            $this->axiStoreCode = Mage::helper('agent')->getCurrentStoreId();
        }

        return $this->axiStoreCode;
    }

    /**
     * @param $packageSubType
     * @param string $type
     * @param $websiteId
     * @param $packageType
     * @return Omnius_Catalog_Model_Resource_Product_Collection
     */
    public function getProductCollection($packageSubType, $type, $websiteId, $packageType, $consumerType = null, $filters = [])
    {
        // Bringing up the memory limit for proper catalog retrieval
        ini_set('memory_limit', '1G' );

        if ($packageType) {
            $attribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', Dyna_Catalog_Model_Product::CATALOG_PACKAGE_TYPE_ATTR);
            $typeValue = Mage::helper('omnius_catalog')->getAttributeAdminLabel($attribute, null, $packageType);
        }

        $attribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', Dyna_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR);
        $subTypeValue = Mage::helper('dyna_catalog')->getAttributeAdminLabel($attribute, null, $packageSubType);

        //if website_id is provided in request param, overwrite it
        $websiteId = Mage::app()->getRequest()->getParam('website_id', $websiteId);
        /** @var Omnius_Catalog_Model_Resource_Product_Collection $collection */
        $collection = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToSelect('*')
            ->setStoreId(Mage::app()->getStore())
            ->addWebsiteFilter($websiteId)
            ->addTaxPercents()
            ->addPriceData(null, $websiteId)
            ->addAttributeToFilter('type_id', $type)
            ->addAttributeToFilter('is_deleted', array('neq' => 1))
            ->addAttributeToFilter(Dyna_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR, $subTypeValue);

        $cablePackages = Dyna_Catalog_Model_Type::getCablePackages();
        $cablePackages = array_map('strtolower', $cablePackages);

        if (!in_array(strtolower($packageType),$cablePackages)){
            if ($packageSubType == Vznl_Catalog_Model_Type::SUBTYPE_DEVICE_SUBSCRIPTION) {
                $collection
                    // Add attribute to sorting
                    ->addAttributeToSort('sorting', Mage_Eav_Model_Entity_Collection_Abstract::SORT_ORDER_DESC)
                    ->getSelect()
                    // Resetting magento's way of doing it because it adds null sorting values at the top of the list
                    ->reset(Zend_Db_Select::ORDER)
                    ->order("ISNULL(sorting), CAST(sorting AS UNSIGNED) DESC ", Mage_Eav_Model_Entity_Collection_Abstract::SORT_ORDER_DESC);
            } else {
                $collection
                    // Add attribute to sorting
                    ->addAttributeToSort('sorting', Mage_Eav_Model_Entity_Collection_Abstract::SORT_ORDER_ASC)
                    ->addAttributeToSort('name', Mage_Eav_Model_Entity_Collection_Abstract::SORT_ORDER_ASC)
                    ->getSelect()
                    // Resetting magento's way of doing it because it adds null sorting values at the top of the list
                    ->reset(Zend_Db_Select::ORDER)
                    ->order("ISNULL(sorting), CAST(sorting AS UNSIGNED) ASC, name ASC", Mage_Eav_Model_Entity_Collection_Abstract::SORT_ORDER_ASC);
            }
        }else{
            // @todo Waiting for feedback regarding the attribute by which we should sort cable products
            // If none of the above attributes are present sort by name
            $collection->addAttributeToSort('name', Mage_Eav_Model_Entity_Collection_Abstract::SORT_ORDER_ASC);
        }

        if (count($filters)) {
            $collection = $this->applyFilters($collection, $filters);
        }

        $collection->addAttributeToFilter(
            array(
                array('attribute' => 'expiration_date', 'gteq' => date('Y-m-d')),
                array('attribute' => 'expiration_date', array('null' => true)),
            )
        );

        $collection->addAttributeToFilter(
            array(
                array('attribute' => 'effective_date', 'lteq' => date('Y-m-d')),
                array('attribute' => 'effective_date', array('null' => true)),
            )
        );

        // Don't get the price for mixmatch as we also need disabled products
        if (!Mage::registry('mix_match_with_disabled_products')) {
            $collection
                ->addTaxPercents()
                ->addPriceData(null, $websiteId);
        }

        if ($consumerType !== null) {
            $consValue = Mage::helper('dyna_configurator/attribute')->getSubscriptionIdentifier($consumerType);
            $collection->addAttributeToFilter(Dyna_Catalog_Model_Product::CATALOG_CONSUMER_TYPE, array('finset' => $consValue));
        }

        if ($packageType) {
            $collection->addAttributeToFilter(Dyna_Catalog_Model_Product::CATALOG_PACKAGE_TYPE_ATTR, array("finset" => $typeValue));
        }

        /**
         * Apply agent groups visibility filters
         * OMNVFNL-5572 Agent/dealer filters create issue with product visibility, disabled until fixed on pcat side
         */
        // $collection = Mage::getModel('productfilter/filter')->addAllowedProductFilters($collection);

        $customerTypeAttribute = Mage::getResourceModel('catalog/product')->getAttribute(Dyna_Catalog_Model_Product::CATALOG_CONSUMER_TYPE);
        $customerTypeOptions = $customerTypeAttribute->getSource()->getAllOptions(true, true);
        $customerTypes = array();
        foreach ($customerTypeOptions as $ct) {
            if ($ct['value']) {
                $customerTypes[$ct['value']] = ($ct['label'] == Dyna_Catalog_Model_Product::IDENTIFIER_BUSINESS) ? true : false;
            }
        }

        $showPriceWithBtw = $this->showPriceWithBtw;
        $isBusiness = $this->customerType;

        /** @var Dyna_Catalog_Model_Product $item */
        foreach ($collection as $item) {
            /**
             * Save if customer is business or not
             */
            $name = $item->getName();
            $display_name_configurator = $item->getDisplayNameConfigurator();
            $item->setData('is_business', $isBusiness);
            $item->setData('maf', $item->getMaf());
            $item->setData('regular_maf', $item->getMaf());
            $item->setData('group_type', $item->getType());
            $item->updateSimType();

            $customerTypeAttributeId = $item->getData(Dyna_Catalog_Model_Product::CATALOG_CONSUMER_TYPE);
            $item->setData('business_product', isset($customerTypes[$customerTypeAttributeId]) ? $customerTypes[$customerTypeAttributeId] : false);
            $item->escapeData();
            $item->setData('name', htmlentities(str_replace('?','€',$name), ENT_QUOTES));
            $item->setData('display_name_configurator', htmlentities(str_replace('?','€',$display_name_configurator), ENT_QUOTES));
            /**
             * Prepare prices
             */
            $this->preparePrice($item);
        }
        
        return $collection;
    }

    /**
     * Return section restrictions for order
     * @param string $websiteCode
     * @return array
     */
    public static function getOrderRestrictions($websiteCode)
    {
        //TODO: Refactor getOrderRestrictions with getWebsiteRestrictions into one function
        // no restrictions on edit order unless on indirect
        if (Mage::getSingleton('customer/session')->getOrderEditMode() && Mage::app()->getWebsite()->getCode() != Vznl_Agent_Model_Website::WEBSITE_INDIRECT_CODE) {
            return array();
        }
        // normal restrictions otherwise
        return static::getWebsiteRestrictions($websiteCode);
    }

    /**
     * @param $websiteCode
     * @return array
     *
     * Define possible device type restrictions for certain website
     */
    public static function getWebsiteRestrictions($websiteCode)
    {
        $restrictions = array();
        switch ($websiteCode) {
            case Vznl_Agent_Model_Website::WEBSITE_INDIRECT_CODE:
                $restrictions[] = Vznl_Catalog_Model_Type::SUBTYPE_ACCESSORY;
                $restrictions[] = Vznl_Catalog_Model_Type::SUBTYPE_DEVICE;
                break;
        }

        return $restrictions;
    }

    /**
     * Applies tax on the prices if the current customer is business
     *
     * @param Mage_Catalog_Model_Product $product
     */
    protected function preparePrice(Mage_Catalog_Model_Product $product)
    {
        $_taxHelper = Mage::helper('tax');
        $_store = $product->getStore();

        $allMafTypes = Vznl_Catalog_Model_Type::$hasMafPrices;

        if ($product->is($allMafTypes)) {
            $priceValues = array(
                'promotional_maf',
                'regular_maf',
            );
            foreach ($priceValues as $priceValue) {
                if ($product->hasData($priceValue)) {
                    $_convertedPrice = $_store->roundPrice($_store->convertPrice($product->getData($priceValue)));
                    $priceWithTax = $_taxHelper->getPrice($product, $_convertedPrice, true);
                    $price = $_taxHelper->getPrice($product, $_convertedPrice, false);
                    /**
                     * Save both prices (with and without tax)
                     */
                    $product->setData(sprintf('%s_with_tax', $priceValue), $priceWithTax);
                    $product->setData($priceValue, $price);
                }
            }
        }

        /**
         * Save both prices (with and without tax)
         */
        $_convertedFinalPrice = $_store->roundPrice($_store->convertPrice($product->getPrice()));
        $priceWithTax = $_taxHelper->getPrice($product, $_convertedFinalPrice, true);
        $price = $_taxHelper->getPrice($product, $_convertedFinalPrice, false);
        $product->setPriceWithTax($priceWithTax);
        $product->setPrice($price);
    }

    /**
     * @param assocArray
     * @param array $key
     * @param array $value
     * @return array
     */
    protected function array_push_assoc($array, $key, $value)
    {
        $array[$key] = $value;
        return $array;
    }

    /**
     * Get all items that are visible in configurator for a specific package type and filters
     *
     * @param null $websiteId
     * @param array $filters
     * @param null $processContext
     * @return array|mixed
     */
    public function getAllConfiguratorItems($websiteId = null, array $filters = array(), $processContext = null)
    {
        $websiteId = $websiteId ?: Mage::app()->getWebsite()->getId();

        /**
         * @var Dyna_Configurator_Helper_Data $helper
         */
        $helper = Mage::helper('dyna_configurator');

        $key = serialize(array(__METHOD__, $websiteId, $filters, $this->packageType, $processContext));

        // store the current process context on the object
        $this->processContext = $processContext;

        if ($result = unserialize($this->getCache()->load($key))) {
            return $result;
        } else {
            /** @var Dyna_Package_Model_PackageType $packageTypeModel */
            $packageTypeModel = Mage::getModel('dyna_package/packageType')->loadByCode($this->packageType);
            $result = array();
            $resultDevice = array();
            $filterableAttributes = $this->getFilterableAttributeCodes();
            foreach ($packageTypeModel->getPackageSubTypes() as $packageSubType) {
                if (!$packageSubType->getVisibleConfigurator()) {
                    continue;
                }
                $result[$packageSubType->getPackageCode(true)] = $this->getProductsOfType($packageSubType->getPackageCode(), $websiteId,
                    $helper->getFiltersPerProductType($packageSubType->getPackageCode(true), $filters),
                    $this->packageType);

                $customFilters = $this->getAttrHelper()->getAvailableFilters(
                    Mage::getResourceModel('catalog/product_collection')
                        ->addAttributeToSelect($filterableAttributes)
                        ->addAttributeToFilter('entity_id', array('in' => $result[$packageSubType->getPackageCode(true)]->getColumnValues('entity_id')))
                );

                $result['filters'][$packageSubType->getPackageCode(true)] = $customFilters;
                $result['consumer_filters'][$packageSubType->getPackageCode(true)] = $customFilters;

                if (($packageSubType->getPackageCode(true) == strtolower(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE)) && (!Mage::helper('vznl_agent')->isIndirectStore())) {
                    foreach ($result[strtolower(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE)] as $id => $item) {
                        if (!empty($item['stock_status_text'])) {
                            $resultDevice = $this->array_push_assoc($resultDevice, $item[Vznl_Superorder_Model_StatusHistory::PACKAGE_STOCK_STATUS], $item['stock_status_text']);
                        }
                    }

                    $deviceFilterOption = $result['filters'][strtolower(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE)][Vznl_Superorder_Model_StatusHistory::PACKAGE_STOCK_STATUS]['options'];
                    $mergedOptions = array_merge(array_flip($deviceFilterOption), array_flip($resultDevice));
                    $mergedOptions = array_flip($mergedOptions);
                    $result['filters'][strtolower(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE)][Vznl_Superorder_Model_StatusHistory::PACKAGE_STOCK_STATUS]['options'] = $mergedOptions;
                    $result['consumer_filters'][strtolower(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE)][Vznl_Superorder_Model_StatusHistory::PACKAGE_STOCK_STATUS]['options'] = $mergedOptions;
                }

                if (($packageSubType->getPackageCode(true) == strtolower(Dyna_Catalog_Model_Type::SUBTYPE_ACCESSORY)) && (!Mage::helper('vznl_agent')->isIndirectStore())) {
                    foreach ($result[strtolower(Dyna_Catalog_Model_Type::SUBTYPE_ACCESSORY)] as $id => $item) {
                        if (!empty($item['stock_status_text'])) {
                            $resultDevice = $this->array_push_assoc($resultDevice, $item[Vznl_Superorder_Model_StatusHistory::PACKAGE_STOCK_STATUS], $item['stock_status_text']);
                        }
                    }

                    $deviceFilterOption = $result['filters'][strtolower(Dyna_Catalog_Model_Type::SUBTYPE_ACCESSORY)][Vznl_Superorder_Model_StatusHistory::PACKAGE_STOCK_STATUS]['options'];
                    $mergedOptions = array_merge(array_flip($deviceFilterOption), array_flip($resultDevice));
                    $mergedOptions = array_flip($mergedOptions);
                    $result['filters'][strtolower(Dyna_Catalog_Model_Type::SUBTYPE_ACCESSORY)][Vznl_Superorder_Model_StatusHistory::PACKAGE_STOCK_STATUS]['options'] = $mergedOptions;
                    $result['consumer_filters'][strtolower(Dyna_Catalog_Model_Type::SUBTYPE_ACCESSORY)][Vznl_Superorder_Model_StatusHistory::PACKAGE_STOCK_STATUS]['options'] = $mergedOptions;
                }
            }

            $this->getCache()->save(serialize($result), $key, array(Dyna_Cache_Model_Cache::PRODUCT_TAG), $this->getCache()->getTtl());

            return $result;
        }
    }

    /**
     * @return Vznl_Configurator_Helper_Attribute
     */
    protected function getAttrHelper()
    {
        if ( ! $this->_attrHelper) {
            $this->_attrHelper = Mage::helper('vznl_configurator/attribute');
        }

        return $this->_attrHelper;
    }

    /**
     * @param $productType
     * @param null $websiteId
     * @param array $filters
     * @return Mage_Eav_Model_Entity_Collection_Abstract|mixed|Varien_Data_Collection
     *
     * Get list of products of type device, subscription, addon or accessory
     */
    public function getProductsOfType($productType, $websiteId = null, array $filters = array(), $packageType = null, $consumerType = null)
    {
        $type = Mage_Catalog_Model_Product_Type::TYPE_SIMPLE;
        // Date key added when implemented expiration date for products
        // This cache will be refreshed every 24h
        // Use short keys for redis, because it affects read time
        $key = md5(serialize(array($packageType, $filters, $productType, $type, $websiteId, $consumerType, date('Y-m-d'), $this->processContext)));


        $productTypeAttribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', 'type');
        $productTypeValue = Mage::helper('dyna_catalog')->getAttributeAdminLabel($productTypeAttribute, null, Dyna_Cable_Model_Product_Attribute_Option::TYPE_OPTION_CONTAINER);

        // parse and set products data
        if (!($products = unserialize($this->getCache()->load($key)))) {
            $collection = $this->getProductCollection($productType, $type, $websiteId, $packageType, $consumerType, $filters)->load();
            $products = new Varien_Data_Collection();
            // Only return the following properties to avoid large json objects with properties that are not used
            $keys = array_merge($this->frontendAttributes, Mage::getModel('omnius_configurator/catalog')->getFilterableAttributeCodes());
            $mandatoryCategories = Mage::helper('omnius_configurator')->getMandatoryCategories();
            /** @var Dyna_Catalog_Model_Product $item */
            $flippedMobileDiscountsIds = array_flip(Dyna_Catalog_Model_Product::getMobileDiscountsIds(false));
            foreach ($collection->getItems() as $item) {
                // Skip Special Mobile discounts from promotions list
                $itemSku = $item->getSku();
                if ($productType == Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_PROMOTION
                    && isset($flippedMobileDiscountsIds[$itemSku])
                ) {
                    continue;
                }
                $item = $this->getFrontendProduct($item, $keys, $mandatoryCategories, $productTypeValue);
                $products->addItem($item);
            }
            $this->getCache()->save(serialize($products), $key, array(Dyna_Cache_Model_Cache::PRODUCT_TAG), $this->getCache()->getTtl());
        }

        return $products;
    }
}
