<?php

/**
 * Class Vznl_Configurator_Model_Expression_Customer
 */
class Vznl_Configurator_Model_Expression_Customer extends Dyna_Configurator_Model_Expression_Customer
{
    CONST CUSTOMER_SOHO_TYPE = 'SoHo';
    CONST CUSTOMER_PRIVATE_TYPE = 'Private';

    protected $customerData = null;
    protected $customerOfferIds = null;
    protected $customerBundleOfferIds = null;

    /**
     * Overwrite constructor to get service customer data during initialization
     * Vznl_Configurator_Model_Expression_Customer constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->customerData = Mage::getSingleton('dyna_customer/session')->getServiceCustomers();
    }

    /**
     * This expression should check if the existing customer has Private OR SoHo type
     * By default if the party_type is not set, fallback to PRIVATE type
     * @param string $customerTypeToCheck
     * @return bool
     */
    public function customerType(string $customerTypeToCheck)
    {
        // PRIVATE as type by default
        $customerType = $this->customer->getData('party_type') ?? Dyna_Customer_Helper_Services::PARTY_TYPE_PRIVATE;
        switch ($customerType) {
            case Dyna_Customer_Helper_Services::PARTY_TYPE_SOHO:
                $customerType = self::CUSTOMER_SOHO_TYPE;
                break;
            case Dyna_Customer_Helper_Services::PARTY_TYPE_PRIVATE:
                $customerType = self::CUSTOMER_PRIVATE_TYPE;
                break;
        }

        return $customerTypeToCheck == $customerType;
    }

    /**
     * Compare max contractEndDate node value with a given date using a given operator
     * @param $operator
     * @param $comparingDate
     * @return bool
     */
    public function bindingDate($operator, $comparingDate)
    {
        $customers = Mage::helper('vznl_configurator')->getCustomersForCurrentStack();
        $maxContractEndDate = $givenDay = $numberOfMonths = false;
        // getting max value of contract_end_date from customer response
        foreach ($customers as $customer) {
            foreach ($customer['contracts'] as $contract) {
                if ($maxContractEndDate) {
                    if (empty($contract['contract_end_date'])) {
                        continue;
                    }
                    $previousDate = new DateTime($maxContractEndDate);
                    $currentDate = new DateTime($contract['contract_end_date']);

                    if ($currentDate > $previousDate) {
                        $maxContractEndDate = $contract['contract_end_date'];
                    }
                } else {
                    $maxContractEndDate = $contract['contract_end_date'];
                }
            }
        }
        $plusPos = strpos($comparingDate, '+');
        if ($plusPos) {
            $givenDay = substr($comparingDate, 0, $plusPos);
            $numberOfMonths = substr($comparingDate, $plusPos + 1, 1);

        } elseif ($minusPos = strpos($comparingDate, '-')) {
            $givenDay = substr($comparingDate, 0, $minusPos);
            $numberOfMonths = substr($comparingDate, $minusPos + 1, 1);
        }

        if ($givenDay) {
            return $this->checkGivenData($givenDay, $numberOfMonths, $plusPos, $operator, $maxContractEndDate);
        } else {
            return false;
        }
    }

    /**
     * Compare two given days to evaluate the binding expression
     * @param $givenDay
     * @param $numberOfMonths
     * @param $plusPos
     * @param $operator
     * @param $maxContractEndDate
     * @return bool
     */
    private function checkGivenData($givenDay, $numberOfMonths, $plusPos, $operator, $maxContractEndDate)
    {
        if (in_array(strtolower($givenDay), array('yesterday', 'today', 'tomorrow'))) {
            $givenDate = new DateTime(strtolower($givenDay));
            $interval = new DateInterval('P1M');
            // add/substract given number of months from the given day
            for ($i = 1; $i <= $numberOfMonths; $i++) {
                if ($plusPos) {
                    $givenDate->add($interval);
                } else {
                    $givenDate->sub($interval);
                }
            }
            // check all possible operators
            switch ($operator) {
                case '<':
                    $comparison = new DateTime($maxContractEndDate) < $givenDate;
                    break;
                case '>':
                    $comparison = new DateTime($maxContractEndDate) > $givenDate;
                    break;
                case '>=':
                    $comparison = new DateTime($maxContractEndDate) >= $givenDate;
                    break;
                case '<=':
                    $comparison = new DateTime($maxContractEndDate) <= $givenDate;
                    break;
                case '=':
                    $comparison = new DateTime($maxContractEndDate) == $givenDate;
                    break;
                default:
                    $comparison = false;
            }
            return $comparison;

        } else {
            return false;
        }
    }

    /**
     * @param string $techIds
     * @param null $bundleId
     */
    public function productsArePartOfBundle($techIds, $bundleId = null)
    {
        if (!$techIds) {
            return false;
        }

        if (!$this->customerData || !($FNCustomer = $this->customerData[Vznl_Customer_Helper_Services::ACCOUNT_CATEGORY_PEAL])) {
            return false;
        }

        if(strcasecmp($bundleId, 'null') === 0){
            $bundleId = null;
        }

        $techIds = explode(',', $techIds);

        if (!$this->customerOfferIds || !$this->customerBundleOfferIds) {
            $customerOfferIds = $customerBundleOfferIds = [];
            foreach ($FNCustomer as $customer) {
                foreach ($customer['contracts'] as $contract) {
                    foreach ($contract['subscriptions'] as $subscription) {
                        foreach ($subscription['products'] as $product) {
                            $customerOfferIds[] = isset($product['product_offer_id']) ? $product['product_offer_id'] : null;
                            $customerBundleOfferIds[] = isset($product['product_parent_bundle_offer_id']) ? $product['product_parent_bundle_offer_id'] : null;
                        }
                    }
                }
            }
            $this->customerOfferIds = $customerOfferIds;
        }
        return $this->checkTechId($techIds, $customerOfferIds, $customerBundleOfferIds, $bundleId);
    }

    /**
     * Search a given tech id in the customer offers
     * @param $techIds
     * @param $customerOfferIds
     * @param $customerBundleOfferIds
     * @param $bundleId
     * @return bool
     */
    public function checkTechId($techIds, $customerOfferIds, $customerBundleOfferIds, $bundleId)
    {
        foreach ($techIds as $techId) {
            if (($key = array_search(trim($techId), $customerOfferIds)) === false) {
                return false;
            }

            if ($customerBundleOfferIds[$key] != $bundleId) {
                return false;
            }
        }
        return true;
    }

    /**
     * Check if a product has a specific price id set
     * @param $priceId
     * @return bool
     */
    public function hasAssignedPrice($priceId)
    {
        if (!$this->customerData || !($PEALCustomer = $this->customerData[Vznl_Customer_Helper_Services::ACCOUNT_CATEGORY_PEAL])) {
            return false;
        }

        foreach ($PEALCustomer as $customer) {
            foreach ($customer['contracts'] as $contract) {
                foreach ($contract['subscriptions'] as $subscription) {
                    foreach ($subscription['products'] as $product) {
                        if (!empty($product['product_prices'])
                            && isset($product['product_prices']['price_id'])
                            && $product['product_prices']['price_id'] == $priceId
                        ) {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    /**
     * Check if a product with a specific offer type has a cpe hardware with a given name
     * @param $hardwareName
     * @param $offerType
     * @return bool
     */
    public function hasCPEAssignedToProductOfferType($hardwareName, $offerType)
    {
        if (!$this->customerData || !($PEALCustomer = $this->customerData[Vznl_Customer_Helper_Services::ACCOUNT_CATEGORY_PEAL])) {
            return false;
        }

        foreach ($PEALCustomer as $customer) {
            foreach ($customer['contracts'] as $contract) {
                foreach ($contract['subscriptions'] as $subscription) {
                    foreach ($subscription['products'] as $product) {
                        if (strcasecmp($product['product_offer_type'], $offerType) != 0) {
                            continue;
                        }

                        if (!empty($product['hardware'])) {
                            return $this->evaluateHasCPE($product, $hardwareName);
                        }
                    }
                }
            }
        }

        return false;
    }

    /**
     * Check if a product with a specific offer id has a cpe hardware with a given name
     * @param $hardwareName
     * @param $offerId
     * @return bool
     */
    public function hasCPEAssignedToProductOfferId($hardwareName, $offerId)
    {
        if (!$this->customerData || !($PEALCustomer = $this->customerData[Vznl_Customer_Helper_Services::ACCOUNT_CATEGORY_PEAL])) {
            return false;
        }

        foreach ($PEALCustomer as $customer) {
            foreach ($customer['contracts'] as $contract) {
                foreach ($contract['subscriptions'] as $subscription) {
                    foreach ($subscription['products'] as $product) {
                        if (strcasecmp($product['product_offer_id'], $offerId) != 0) {
                            continue;
                        }

                        if (!empty($product['hardware'])) {
                            return $this->evaluateHasCPE($product, $hardwareName);
                        }
                    }
                }
            }
        }

        return false;
    }

    /**
     * Check in the customer hardware if a hardware of type cpe with a given name
     * @param $hardwareName
     * @return bool
     */
    public function hasCPE($hardwareName)
    {
        if (!$this->customerData || !($PEALCustomer = $this->customerData[Vznl_Customer_Helper_Services::ACCOUNT_CATEGORY_PEAL])) {
            return false;
        }

        foreach ($PEALCustomer as $customer) {
            foreach ($customer['contracts'] as $contract) {
                foreach ($contract['subscriptions'] as $subscription) {
                    foreach ($subscription['products'] as $product) {
                        if (!empty($product['hardware'])) {
                            if($this->evaluateHasCPE($product, $hardwareName)){
                                return true;
                            }
                        }
                    }
                }
            }
        }

        return false;
    }

    /**
     * Check the hardware of a given product to determine if it has type cpe with a given name
     * @param $product
     * @param $hardwareName
     * @return bool
     */
    public function evaluateHasCPE($product, $hardwareName)
    {
        if (isset($product['hardware']['hardware_type'])
            && strcasecmp($product['hardware']['hardware_type'], 'CPE') == 0
            && isset($product['hardware']['hardware_name'])
            && strcasecmp($product['hardware']['hardware_name'], $hardwareName) == 0
        ) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether or not current customer is of type soho or not
     * @return bool
     */
    public function isSoho()
    {
        return !empty($this->getData()) ? (bool)$this->getData("is_business") : (bool)$this->customer->getData('is_business');
    }
}
