<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 *
 * Used in service source expression for evaluating customer's installed base
 * Class Vznl_Configurator_Model_Expression_InstallBase
 */
class Vznl_Configurator_Model_Expression_InstallBase extends Dyna_Configurator_Model_Expression_InstallBase
{
    /**
     * Constructor method
     */
    public function _construct()
    {
        /** @var Dyna_Customer_Model_Customer $currentCustomer */
        $currentCustomer = Mage::getSingleton('customer/session')
            ->getCustomer();

        // used for getting static constant (and not directly through class call for magento overriding purposes)
        $this->bundlesHelper = Mage::helper('dyna_bundles');

        // get reference to quote
        $this->quote = Mage::getSingleton('checkout/cart')->getQuote();

        // setting migration data on current instance
        $this->inMigrationState = Mage::getSingleton('checkout/cart')->getCheckoutSession()->getInMigrationOrMoveOffnetState();
        // setting customer install base product on current instance
        $migrationData = explode(",", Mage::getSingleton('checkout/cart')->getCheckoutSession()->getMigrationOrMoveOffnetCustomerNo());
        $this->migrationCustomerNumber = $migrationData[0] ?? null;
        $this->migrationProductId = $migrationData[1] ?? null;

        if ($currentCustomer->isCustomerLoggedIn()) {
            $this->customerLoggedIn = true;
            $allInstallBaseProducts = Mage::getModel('vznl_customer/customer')->getAllInstallBaseProducts();
            foreach ($allInstallBaseProducts as $customerProduct) {
                if ($this->canBeBundled($customerProduct, $allInstallBaseProducts)) {
                    $this->installBaseProducts[] = $customerProduct;
                }
                $this->installBaseProductsForRedPlus[] = $customerProduct;
            }
        }

        $this->expressionHelper = Mage::helper('dyna_bundles/expression');
    }

    /**
     * Checks for products that contains the given attribute.
     * If there is a product with this attribute and the value of the attribute as provided return true.
     * @param $packageSubtype
     * @param $attributeId
     * @param $operator
     * @param $attributeValue
     * @return bool
     */
    public function hasProductAttributeValue($packageSubtype, $attributeId, $operator, $attributeValue)
    {
        $ibSkus = [];

        //also check the quote items with preselect flag
        foreach(Mage::getSingleton('checkout/cart')->getQuote()->getAllItems(true) as $item){
            if($item->getPreselectProduct() ){
                $ibSkus[]=$item->getSku();
            }
        }

        foreach($this->installBaseProducts as $ibProduct){
            if(isset($ibProduct['products']) && !empty($ibProduct['products'])){
                foreach($ibProduct['products'] as $product){
                    $ibSkus[] = $product['product_sku'];
                }
            }
        }

        //if IB is apparently empty, return false
        if(empty($ibSkus)){
            return false;
        }
        /** @var Vznl_Configurator_Helper_Expression $evaluatorHelper */
        $evaluatorHelper = Mage::helper('dyna_configurator/expression');
        $productsCollection = $evaluatorHelper->getProductsCollectionByAttr($packageSubtype, $attributeId, $operator, $attributeValue, $ibSkus);

        return $productsCollection->load()->getSize() > 0;
    }

    public function getInstallBaseProducts()
    {
        return $this->installBaseProducts;
    }

    public function setInstallBaseProducts($products)
    {
        $this->installBaseProducts = $products;

        return $this;
    }

    /**
     * Check if customer has fixed package in install base (which he has not yet modified/moved)
     */
    public function hasIBFixedPackage()
    {
        //if register key is set, return true as ILS already created package and it will cause method to now return false
        if(Mage::registry('simulate_has_fixed_package')){
            return true;
        }
        //if quote already has a fixed package, you can't use the fixed IB package
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        if($quote->hasFixed()){
            return false;
        }

        $customerProducts = Mage::getSingleton('dyna_customer/session')->getCustomerProducts();
        if (isset($customerProducts[Vznl_Customer_Model_System_Config_Stack::ZIGGO_STACK])) {

            $fixPackage = current($customerProducts[Vznl_Customer_Model_System_Config_Stack::ZIGGO_STACK]);
            $fixedProducts = isset($fixPackage['contracts'][0]['subscriptions'][0]['products']) ? $fixPackage['contracts'][0]['subscriptions'] : array();
            $fixedProductId = current($fixedProducts);

            $installProduct = Mage::registry('install_base_expression_result')?: array();
            $installProduct[($this->bundlesHelper)::EXPRESSION_INSTALL_BASE_PRODUCT][] = array(
                ($this->bundlesHelper)::INSTALL_BASE_CUSTOMER_NUMBER => $fixedProductId['customer_number'] ?? null,
                ($this->bundlesHelper)::INSTALL_BASE_PRODUCT_ID => $fixedProductId['product_offer_id'] ?? null,
            );

            if(Mage::registry('install_base_expression_result')){
                Mage::unregister('install_base_expression_result');
            }
            Mage::register('install_base_expression_result', $installProduct);

            return count($customerProducts[Vznl_Customer_Model_System_Config_Stack::ZIGGO_STACK]) > 0;
        }

        return false;
    }


    /**
     * Method that determines whether or not current customer has in his install base a product from a certain category
     * Returns install base subscription (customer number) number with it's associated product id, if found
     * @param $categoryPath string
     * @param $notExpression bool
     * @return bool
     */
    public function containsProductInCategory($categoryPath, $notExpression = false)
    {
        $contains = false;
        $categoryPath = str_replace('->', self::CATEGORY_PATH_SEPPARTOR, $categoryPath);
        if (!isset($this->categoryCache[$categoryPath])) {
            $categoryModel = Mage::getModel('catalog/category');
            $this->categoryCache[$categoryPath] = $categoryModel->getCategoryByNamePath($categoryPath, self::CATEGORY_PATH_SEPPARTOR);
        }
        $category = $this->categoryCache[$categoryPath];

        // if no customer logged in, no products in install base
        // same for category, if it doesn't exist condition will be evaluated to false
        // nothing to store in magento registry
        if (!$this->customerLoggedIn || !$category || !$categoryId = $category->getId()) {
            return $contains;
        }

        //first check the quote if we have contract products and check for category
        $productResourceModel = Mage::getResourceModel("catalog/product");
        foreach(Mage::getSingleton('checkout/cart')->getQuote()->getAllItems(true) as $item){
            $categoriesWithAnchors = $productResourceModel->getCategoryIdsWithAnchors($item->getProduct());
            if($item->getPreselectProduct() && in_array($categoryId, $categoriesWithAnchors)){
                return true;
            }
        }

        foreach ($this->installBaseProducts as $installBaseProduct) {
            // each product has an associated list of category ids to which it belongs
            foreach ($installBaseProduct['products'] as $product) {
                if (in_array($categoryId, $product['categories'])) {
                    // exit loop
                    $contains = true;
                    break(2);
                }
            }
        }

        return $contains;
    }

    /**
     * Determine whether or not install base contains a certain product
     * @param string $productSku
     * @param $notExpression bool
     * @return bool
     */
    public function containsProduct($productSku, $notExpression = false)
    {
        $contains = false;

        // if no customer logged in, no products in install base
        if (!$this->customerLoggedIn) {
            return $contains;
        }

        //first check the quote if we have contract products
        foreach(Mage::getSingleton('checkout/cart')->getQuote()->getAllItems(true) as $item){
            if($item->getPreselectProduct() && ($item->getSku() == $productSku)) {
                return true;
            }
        }

        foreach ($this->installBaseProducts as $installBaseProduct) {
            foreach ($installBaseProduct['products'] as $product) {
                if ($product['product_sku'] == $productSku) {
                    // exit this loop
                    $contains = true;
                    break;
                }
            }
        }

        return $contains;
    }

    /**
     * Check if FamilyId exists in install base products
     * @param string $familyCode
     * @return bool
     */
    public function containsProductInProductFamily($familyCode)
    {

        $contains = false;

        $familyId = Mage::getSingleton('dyna_catalog/productFamily')->getProductFamilyIdByCode($familyCode);

        // Return if no customer loaded into session or no family id found
        if (!$this->customerLoggedIn || !$familyId) {
            return $contains;
        }

        foreach ($this->installBaseProducts as $installBaseProduct) {
            foreach ($installBaseProduct['products'] as $product) {

                $productFamilies = $product['product_family'] ?? [];
                if (!is_array($productFamilies)) {
                    $productFamilies = explode(",", $product['product_family']);
                }

                if (isset($product['product_family'])
                    && $product['product_family']
                    && in_array($familyId, $productFamilies)
                ) {
                    $contains = true;
                    break;
                }
            }
        }

        return $contains;
    }

    /**
     * Method that determines for current customer  how many products has in his install base from a certain category
     * @param $categoryPath
     * @return int
     */
    public function numberOfProductsInCategory($categoryPath)
    {
        if(strpos($categoryPath, '->') === false){
            $categoryPath = str_replace('/', '->', $categoryPath);
        }

        return parent::numberOfProductsInCategory($categoryPath);
    }
}
