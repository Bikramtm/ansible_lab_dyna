<?php

class Vznl_Configurator_Model_Expression_Package extends Dyna_Configurator_Model_Expression_Package
{
    /**
     * Get the product in the active package by sku
     *
     * @param $productSku
     * @return bool|Dyna_Checkout_Model_Sales_Quote_Item
     */
    public function getProductInActivePackageBySku($productSku)
    {
        foreach ($this->packageItems as $item) {
            if ($item->getSku() === $productSku) {
                return $item;
            }
        }

        return false;
    }

    /**
     * Check if the given product exists and is new within the active package.
     *
     * @param $productSku
     * @return bool
     */
    public function containsNewProduct($productSku)
    {
        $item = $this->getProductInActivePackageBySku($productSku);

        return $item && !$item->getPreselectProduct();
    }

    /**
     * Checks for products that contains the given attribute
     * @param null $packageSubtype
     * @param $attributeId
     * @param $operator
     * @param $attributeValue
     * @return bool
     */
    public function hasProductAttributeValue($packageSubtype = null, $attributeId, $operator, $attributeValue = null)
    {
        if (($packageSubtype == null && (($attributeValue == null) || ($attributeId == null) || ($operator == null)))) {
            return false;
        }

        $productsSkus = array_keys($this->package->getAllProductIs());
        /** @var Vznl_Configurator_Helper_Expression $evaluatorHelper */
        $evaluatorHelper = Mage::helper('dyna_configurator/expression');

        $productsCollection = $evaluatorHelper->getProductsCollectionByAttr($packageSubtype, $attributeId, $operator, $attributeValue, $productsSkus);

        return $productsCollection->load()->getSize() > 0;
    }

    /**
     * Check if the active package contains products in the given category that are also not in install base.
     *
     * @param $categoryPath
     * @return bool
     */
    public function containsNewProductInCategory($categoryPath)
    {
        $items = $this->getProductsInCategory($categoryPath);

        foreach ($items as $item) {
            if (!$item->getPreselectProduct()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Get an array of products from the active package based on a given category path.
     *
     * @param $categoryPath
     * @return array
     */
    protected function getProductsInCategory($categoryPath)
    {
        $productResource = Mage::getResourceModel('catalog/product');
        $category = Mage::getModel('catalog/category')->getCategoryByNamePath($categoryPath, '->');

        if (!$category) {
            return [];
        }

        $items = [];

        $allItems = array_merge($this->packageItems, $this->getInjectedItems());
        foreach ($allItems as $item) {
            if (in_array($category->getId(), $productResource->getCategoryIdsWithAnchors($item->getProduct()))) {
                $items[] = $item;
            }
        }

        return $items;
	}

	/*
     * @param $status
     * @param $categoryPath
     * @return int
     *
     * Count number of products in the package having a specific status and belonging to a specific category
     */
    public function countProductsWithStatusInCategory($status, $categoryPath)
    {
        $helper = Mage::helper('dyna_configurator/expression');
        $productResource = Mage::getResourceModel("catalog/product");
        $count = 0;
        foreach ($this->packageItems as $item) {

            //if item status is not the one we're checking, skip item
            if(strtolower($item->getSystemAction()) != strtolower($status)){
                continue;
            }

            //loop through the categories and if we find a match, increment count and break loop
            $productCategoryIds = $productResource->getCategoryIdsWithAnchors($item->getProduct());
            foreach($productCategoryIds as $productCategoryId){
                $productCategoryPath = $helper->getCategoryPath($productCategoryId);
                if(strtolower($productCategoryPath) == strtolower($categoryPath)){
                    $count++;
                    break;
                }
            }
        }

        return $count;
    }

    /*
     * Apply package filters for products in cart
     * @return int
     */
    public function filterOutOwnerSubscription()
    {
        $response = false;
        /** @var Dyna_Package_Model_Package $package */
        foreach ($this->package->getQuote()->getCartPackages() as $package) {
            if (!$package->getConvertedToMember()) {
                continue;
            }

            foreach ($package->getAllItems() as $item) {
                $item->getProduct()->isSubscription() && $item->isDeleted(true) && $response = true;
            }
        }

        return $response;
    }

    /*
     * Determine whether the package was created by a offer
     * @return bool
     */
    public function isOfferPackage()
    {
        /** @var Dyna_Package_Model_Package $package */
        if ($this->package->getData('is_offer')) {
            $offerProducts = Mage::getSingleton('core/session')->getSpecialOfferPromoProducts();
            $packageId = $this->package->getPackageId();
            if(isset($offerProducts[$packageId]['offerTargetProducts'])){
                $offerTarget = $offerProducts[$packageId]['offerTargetProducts'];

                //now also check if target is still in package, only if target was stored in session
                if(!empty($offerTarget)) {
                    $targetStillInCart = false;
                    foreach ($this->packageItems as $item) {
                        if (in_array($item->getSku(), $offerTarget)) {
                            $targetStillInCart = true;
                            break;
                        }
                    }
                    return $targetStillInCart;
                }
            }
            return true;
        }

        return false;
    }

    /**
     * Returns injected items
     * @return $this
     */
    public function getInjectedItems()
    {
        $ibSkus = Mage::getSingleton('customer/session')->getAllInstalledBaseProductSkus();
        $packageInjectedProducts = Mage::helper('dyna_configurator/expression')->getInjectedProducts();
        $skusPresentInPackage = !empty($this->packageItems)
            ? array_map(
                function ($e) {
                    return $e->getProduct()->getSku();
                },
                $this->packageItems)
            : array();

        /** @var Dyna_Checkout_Model_Sales_Quote_Item $item */
        foreach ($this->injectedItems as $item) {
            if (isset($packageInjectedProducts[$item->getProduct()->getSku()])) {
                unset($packageInjectedProducts[$item->getProduct()->getSku()]);
            }
        }

        /** @var Vfde_Catalog_Model_Product $product */
        foreach ($packageInjectedProducts as $sku => $product) {
            if (!empty($this->package->getData()) && !in_array($product->getSku(), $skusPresentInPackage)) {
                $injItem = Mage::getModel('sales/quote_item')->setProduct($product);
                if(isset($ibSkus[$product->getSku()])){
                    $injItem->setPreselectProduct(1);
                }
                $this->injectedItems[] = $injItem;
            }
        }

        return $this->injectedItems;
    }
}
