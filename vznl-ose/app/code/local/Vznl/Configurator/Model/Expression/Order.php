<?php

class Vznl_Configurator_Model_Expression_Order extends Dyna_Configurator_Model_Expression_Order
{
    /**
     * Checks if the order contains a specific product
     * @param string $sku
     * @return bool
     */
    public function packageContainsProduct($sku)
    {
        if($sku) {
            if (in_array($sku, $this->getAllQuoteSku())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get all the Sku form the current quote
     * @return array
     */
    public function getAllQuoteSku()
    {
        $quoteId = Mage::getSingleton('checkout/cart')->getQuote()->getId();
        $quote = Mage::getModel('sales/quote')->load($quoteId);
        $allSku = array();
        $allSkuKeys = array();

        if ($quote) {
            foreach ($quote->getAllItems() as $item) {
                $sku = $item->getData('sku');
                if (!isset($allSkuKeys[$sku])) {
                    $allSku[] = $sku;
                    $allSkuKeys[$sku] = $sku;
                }
            }
        }
        return $allSku;
    }
}