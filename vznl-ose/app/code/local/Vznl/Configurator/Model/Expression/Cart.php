<?php

class Vznl_Configurator_Model_Expression_Cart extends Dyna_Configurator_Model_Expression_Cart
{
    public function __construct()
    {
        parent::__construct();
        $this->helper = Mage::helper('dyna_configurator/expression');
    }

    /**
     * Check if the given product exists and is new within the entire cart.
     *
     * @param $productSku
     * @return bool
     */
    public function containsNewProduct($productSku)
    {
        $items = $this->helper->getProductsInQuoteBySku($productSku);

        foreach ($items as $item) {
            if ($item && !$item->getPreselectProduct()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check if the quote contains products in the given category that are also not in install base.
     *
     * @param $categoryPath
     * @return bool
     */
    public function containsNewProductInCategory($categoryPath)
    {
        $items = $this->helper->getProductsInQuoteByCategory($categoryPath);
        foreach ($items as $item) {
            if (!$item->getPreselectProduct()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Checks for products that contains the given attribute
     * And return true if the attribute has the value that is being checked
     * @param $packageSubtype
     * @param $attributeId
     * @param $operator
     * @param $attributeValue
     * @return bool
     */
    public function hasProductAttributeValue($packageSubtype, $attributeId, $operator, $attributeValue)
    {
        return $this->helper->hasProductAttributeValue($packageSubtype, $attributeId, $operator, $attributeValue);
    }

    /**
     * @param $status
     * @param $categoryPath
     * @return int
     *
     * Count number of products in the cart having a specific status and belonging to a specific category
     */
    public function countProductsWithStatusInCategory($status, $categoryPath)
    {
        $count = 0;
        foreach(Mage::getSingleton('checkout/cart')->getQuote()->getCartPackages() as $package){
            $packageExpression = Mage::getModel('dyna_configurator/expression_package');
            $packageExpression->setPackage($package);
            $count += $packageExpression->countProductsWithStatusInCategory($status, $categoryPath);
        }

        return $count;
    }

    /**
     * Checks if the package contains a package with a specific type and process context which is not part of a bundle
     *
     * @param $packageType
     * @param $processContext
     * @return bool
     */
    public function containsEligibleBundlePackage($packageType, $processContext)
    {
        $packageType = strtoupper($packageType);
        $tempResponse['package_types'] = [];
        $configHelper = Mage::helper('dyna_configurator/expression');
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        if(!$quote->getCartPackages()){
            return false;
        }
        $nonBundlePackages = Mage::helper('dyna_configurator/expression')->getNonBundledPackages();
        $activePackageId = $quote->getActivePackage() ? $quote->getActivePackage()->getEntityId() : 0;
        $packagesToCheck = $quote->getCartPackages();

        if ($configHelper->shouldSkipBundleProcessing($quote->getActivePackage(), $packageType, null)){
            $packagesToCheck = [$quote->getActivePackage()];
        }

        foreach($packagesToCheck as $package){

            if (is_null($package)){
                continue;
            }

            $isActivePackage = $activePackageId == $package->getId();

            //if package is already part of bundle, skip it
            if (!isset($nonBundlePackages[$package->getId()])){
                continue;
            }

            //if package doesn't contain tariff, skip it
            if (!$isActivePackage && !$package->hasTariff()){
                continue;
            }

            //if a package is found with same type and process context, condition is met
            if (strcasecmp($package->getSaleType(), $processContext) === 0 && strcasecmp($package->getType(), $packageType) === 0){
                $tempResponse['package_types'][$packageType][] = (int)$package->getPackageId();
                $tempResponse['package_types'][$packageType] = array_unique($tempResponse['package_types'][$packageType]);
            }
        }

        //intersect with other findings
        $response = $configHelper->updateBundleResult($tempResponse);

        return (isset($tempResponse['package_types']) && !empty($tempResponse['package_types']))?true:false;
    }

    /**
     * Check if current package is from an order with certain statuses
     * @return bool
     */
    public function hasSalesorderstatusValidationApproved()
    {
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        if(!$quote->getSuperQuoteId()){
            return false;
        }
        $parentQuote = Mage::getModel('sales/quote')->load($quote->getSuperQuoteId());
        if(!$parentQuote->getId()){
            return false;
        }
        $superorder = Mage::getModel('superorder/superorder')->load($parentQuote->getSuperOrderEditId());
        if(!$superorder->getId()){
            return false;
        }

        if(in_array($superorder->getOrderStatus(), array(Vznl_Superorder_Model_Superorder::SO_VALIDATED, Vznl_Superorder_Model_Superorder::SO_FULFILLMENT_IN_PROGRESS , Vznl_Superorder_Model_Superorder::SO_FULFILLED))){
            return true;
        }

        return false;
    }
}
