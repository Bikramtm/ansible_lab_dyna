<?php
/**
 * Class Vznl_Configurator_Helper_Cart
 */
class Vznl_Configurator_Helper_Cart extends Dyna_Configurator_Helper_Cart
{
     public $catalogObject = null;
     public $cartObject = null;
     public $customerObject = null;
     public $availabilityObject = null;
     public $installBaseObject = null;
     public $activeInstallBaseObject = null;
     public $agentObject = null;
     public $servicabilityObject = null;

    /**
     * @param $productIds
     * @param array $allProducts
     * @param null $websiteId
     * @param bool $cacheCollections
     * @param null $packageType
     * @param bool $skipServiceRules
     * @return array
     */
    public function getCartRules($productIds, $allProducts = array(), $websiteId = null, $cacheCollections = false, $packageType = null, $skipServiceRules = false)
    {
        sort($productIds);
        sort($allProducts);
        $debugRules = Mage::helper('dyna_productmatchrule')->isDebugMode();

        if ($debugRules) {
            Mage::log("getCartRules called on action: " . Mage::app()->getFrontController()->getAction()->getFullActionName(), null, 'ServiceExpressionRules.log');
            Mage::log("Current source collection found in registry: " . Mage::registry('rules_source_collection'), null, 'ServiceExpressionRules.log');
        }

        $websiteId = $websiteId ?: Mage::app()->getStore()->getWebsiteId();
        /** @var Dyna_Catalog_Helper_ProcessContext $processContextHelper */
        $processContextHelper = Mage::helper("dyna_catalog/processContext");
        $processContextId = $processContextHelper->getProcessContextId();
        if(!$processContextId){
            $processContextModel = Mage::getModel('dyna_catalog/processContext');
            $processContextId = $processContextModel->getProcessContextIdByCode(Dyna_Catalog_Model_ProcessContext::ACQ);
        }

        $sourceCollectionId = $this->getCurrentSourceCollection();

        $cache_key = sprintf('get_cart_new_rules_%s_%s_%s_%s_%s_%s',
            md5(serialize($productIds)),
            md5(serialize($allProducts)),
            $websiteId,
            $packageType,
            $processContextId,
            $sourceCollectionId);
        $notInitiallySelectable = $this->getNotInitiallySelectableProducts($packageType);

        if ($debugRules || !$cacheCollections) {
            $result = false;
        } else {
            $result = unserialize($this->getCache()->load($cache_key));
            if ($debugRules) {
                Mage::log("[CACHED] Normal allowed rules: ", null, 'ServiceExpressionRules.log');
                Mage::log(json_encode($result), null, 'ServiceExpressionRules.log');
            }
        }
        if (!Mage::registry("no_updated_for_expression_helper_needed")) {
            /** @var Dyna_Configurator_Helper_Expression $serviceHelper */
            $serviceHelper = Mage::helper('dyna_configurator/expression');
            $serviceHelper->updateInstance(Mage::getSingleton('checkout/cart')->getQuote());
        }

        if (!$result) {
            $skippedProducts = [];
            if ($packageType) {
                $productIds = $this->handleSkippedSubtypes($packageType, $productIds, $skippedProducts);
            }

            if (count($productIds)) {
                if ($debugRules) {
                    Mage::log("Getting Omnius allowed rules for the following input ids (after removing the ones with skipped package subtype): ", null, 'ServiceExpressionRules.log');
                    Mage::log(json_encode($productIds), null, 'ServiceExpressionRules.log');
                }
                $available = [];
                $storeId = $websiteId ? Mage::app()->getWebsite($websiteId)->getDefaultStore()->getId() : Mage::app()->getStore()->getId();
                $catalogResource = Mage::getResourceModel('catalog/product');
                $subtypeAttribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', Dyna_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR);
                $rawAttributesValues = [];
                $attribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', Dyna_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR);
                $typeDevice = Mage::helper('dyna_catalog')->getAttributeAdminLabel($attribute, null, Vznl_Catalog_Model_Type::SUBTYPE_DEVICE);
                $typeSubscription = Mage::helper('dyna_catalog')->getAttributeAdminLabel($attribute, null, Dyna_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION);
                $typeDeviceSubscription = Mage::helper('dyna_catalog')->getAttributeAdminLabel($attribute, null, Vznl_Catalog_Model_Type::SUBTYPE_DEVICE_SUBSCRIPTION);
                $adapter = Mage::getSingleton('core/resource')->getConnection('core_read');

                $products = Mage::getModel('catalog/product')->getCollection()
                    ->addAttributeToSelect('aikido')
                    ->addAttributeToSelect('identifier_commitment_months')
                    ->addAttributeToFilter('entity_id', ['in' => $productIds]);

                $cache_key_collection = 'getRules_collections_'.$websiteId;
                if($resultCollection = unserialize($this->getCache()->load($cache_key_collection))){
                    list($deviceCollection, $subsCollection, $deviceSubsCollection) = $resultCollection;
                }
                else{
                    $deviceCollection = $adapter->fetchCol(Mage::getModel('catalog/product')->getCollection()
                        ->addAttributeToFilter(Dyna_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR, $typeDevice)->getSelectSql(true));
                    $subsCollection = $adapter->fetchCol(Mage::getModel('catalog/product')->getCollection()
                        ->addAttributeToFilter(Dyna_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR, $typeSubscription)->getSelectSql(true));
                    $deviceSubsCollection = $adapter->fetchCol(Mage::getModel('catalog/product')->getCollection()
                        ->addAttributeToFilter(Dyna_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR, $typeDeviceSubscription)->getSelectSql(true));
                    $resultCollection = [$deviceCollection, $subsCollection, $deviceSubsCollection];

                    $this->getCache()->save(serialize($resultCollection), $cache_key_collection, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
                }

                foreach ($productIds as $id) {

                    /**
                     * When choosing a device or subscription, normally the application would hide the rest of the devices or subscriptions
                     * if there is no allowed rule between them. However, for devices and subscriptions, we must allow users to switch
                     * between the allowed ones without hiding the other options.
                     */
                    $valueId = $catalogResource->getAttributeRawValue($id, Dyna_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR, $storeId);
                    if (isset($rawAttributesValues[$valueId])) {
                        $productType = $rawAttributesValues[$valueId];
                    } else {
                        $productType = json_decode(Mage::helper('dyna_catalog')->getAttributeAdminLabel($attribute, $valueId), true);
                        $rawAttributesValues[$valueId] = $productType;
                    }

                    $similarProductIds = array();

                    //if device, also retrieve all other devices to simulate that subscriptions are allowed between themselves
                    if (in_array(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE, $productType)) {
                        $similarProductIds = $deviceCollection;
                    }

                    //if subscription, also retrieve all other subscriptions to simulate that subscriptions are allowed between themselves
                    if (in_array(Dyna_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION, $productType)) {
                        $similarProductIds = array_merge($similarProductIds, $subsCollection);
                    }
                    $available[$id] = [];
                    $rule = $this->getRulesForId($websiteId, $id, $catalogResource, $storeId, $rawAttributesValues, $subtypeAttribute, null, null, $packageType);
                    $rule['whitelist'] = array_unique(array_merge($rule['whitelist'], $similarProductIds));
                    if ($debugRules) {
                        Mage::log("Got the following compatible products for [" . $id . "]: ", null, 'ServiceExpressionRules.log');
                        Mage::log(json_encode($rule), null, 'ServiceExpressionRules.log');
                    }
                    if (!empty($rule['whitelist'])) {
                        $available[$id] = $rule['whitelist'];
                    }
                    else
                        unset($available[$id]);

                    if (!empty($rule['options'])) {
                        $notInitiallySelectable = array_diff($notInitiallySelectable, $rule['options']);
                    }
                }

                $result_new = [];
                if (!empty($available)) {
                    $result_new = array_pop($available);
                    foreach ($available as $items) {
                        $result_new = array_intersect($result_new, $items);
                    }
                }

                if (!$result_new) {
                    $result = [];
                } else {
                    $result = array_values($result_new);
                    $result = array_diff($result, $notInitiallySelectable);
                }

                /** @var Vznl_MixMatch_Model_Pricing $priceModel */
                $priceModel = Mage::getModel('dyna_mixmatch/pricing');
                $updatedResults = $priceModel->checkMixMatchAvailable($result, $products, $deviceCollection, $subsCollection, $deviceSubsCollection);

                $result = array_merge($updatedResults, $skippedProducts);
                if ($debugRules) {
                    Mage::log("[NOT CACHED] Normal allowed rules: ", null, 'ServiceExpressionRules.log');
                    Mage::log(json_encode($result), null, 'ServiceExpressionRules.log');
                    Mage::log("No initial selectable: ", null, 'ServiceExpressionRules.log');
                    Mage::log(json_encode($notInitiallySelectable), null, 'ServiceExpressionRules.log');
                }
            } else {
                if (in_array(strtolower($packageType), Dyna_Catalog_Model_Type::getMobilePackages())
                    || in_array(strtolower($packageType), Vznl_Catalog_Model_Type::getHardwarePackages())) {
                    // not forwarding all products because this is Omnius legacy logic for calling rules without session (in order to be cached in varnish) which is not suitable for DE
                    $result = $this->getAllRulesForProducts(array(), $websiteId, $packageType);
                    // add also skipped products for comp rules allowing
                    // (there could be promos which are not defaulted (skipping removeItemAndParseDefault) and so there are still in cart in this moment
                    // and their subtype is also marked for skipping comp rules checking)
                    $result = array_merge(array_diff($result, $notInitiallySelectable), $skippedProducts);
                } else {
                    $result = [];
                }
            }

            $this->getCache()->save(serialize($result), $cache_key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
        }

        // Appending service expression rules to result (not cached because these depend on customer and serviceability conditions)
        if (!$skipServiceRules) {
            /** @var Vznl_ProductMatchRule_Model_Rule $ruleModel */
            $ruleModel = Mage::getSingleton('vznl_productmatchrule/rule');
            $allowedServiceRules = $ruleModel->loadAllowedServiceRules($productIds, $websiteId, $packageType);
            $notAllowedServiceRules = $ruleModel->loadAllowedServiceRules($productIds, $websiteId, $packageType, true);

            if (count($notAllowedServiceRules)) {
                foreach ($result as $key => &$id) {
                    if (isset($notAllowedServiceRules[$id])) {
                        // remove products from static rules if found in service not allowed
                        unset($result[$key]);
                        continue;
                    }
                }

                if ($debugRules) {
                    Mage::log("Not allowed service rules: ", null, 'ServiceExpressionRules.log');
                    Mage::log(json_encode($notAllowedServiceRules), null, 'ServiceExpressionRules.log');

                    Mage::log("Normal allowed rules after service not allowed: ", null, 'ServiceExpressionRules.log');
                    Mage::log(json_encode($result), null, 'ServiceExpressionRules.log');
                }
            }

            if ($debugRules) {
                Mage::log("Service allowed rules: ", null, 'ServiceExpressionRules.log');
                Mage::log(json_encode(array_keys($allowedServiceRules)), null, 'ServiceExpressionRules.log');
            }

            $result = array_merge($result, array_keys($allowedServiceRules));
        }

        return array_map('strval', $result);
    }

    /**
     * @return array
     */
    public function getMandatoryWarnings()
    {
        //todo: remove this and figure it out why this functionality has a different behavior beside NL
        return [];
    }


    /**
     * @return array
     */
    public function getCardinalityWarnings()
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getModel('checkout/session')->getQuote();
        $packages = $quote->getCartPackages(true);
        $warnings = [];
        foreach ($packages as $packageModel) {
            $packageId = $packageModel->getPackageId();
            $warnings[$packageId] = [];
            if (!empty($packageId)) {
                $packageName = Mage::getModel('dyna_package/packageType')->loadByCode($packageModel->getType())->getFrontEndName();
                $packageConfiguration = Mage::getModel('package/configuration')->setPackage($packageModel);
                $packageSubtypes = $packageConfiguration->getConfiguration();
                foreach ($packageSubtypes as $packageSubtype) {
                    // Define package_subtype product attribute
                    $subtypeIdentifier = $packageSubtype->getPackageCode();
                    // Load all package items
                    $packageItems = $packageModel->getItems(strtolower($subtypeIdentifier));

                    if($subtypeIdentifier == Vznl_Catalog_Model_Type::SUBTYPE_DEVICE) {
                        $this->skipLevyProduct($packageItems);
                    }

                    // Set count limit to which a product of certain type can reach
                    $packageSubtypeCardinality = $packageSubtype->getCardinality();

                    /** @var Dyna_Catalog_Helper_ProcessContext $processContext */
                    $processContext = Mage::helper('dyna_catalog/processContext');
                    $processContextId = $processContext->getProcessContextId($packageModel);

                    if ($processContextId > 0) {
                        $subtypeCardinality = Mage::getModel('dyna_package/packageSubtypeCardinality')
                            ->loadBySubtypeAndContext($packageSubtype->getId(), $processContextId)
                            ->getData();

                        if (isset($subtypeCardinality['cardinality']) && $subtypeCardinality['cardinality'] != '') {
                            $packageSubtypeCardinality = $subtypeCardinality['cardinality'];
                        }
                    }

                    $minItems = $packageConfiguration->getMinProducts($packageSubtypeCardinality);
                    $maxItems = $packageConfiguration->getMaxProducts($packageSubtypeCardinality);
                    $packageItemsCount = count($packageItems);

                    if (!$minItems && !$maxItems && $packageItemsCount > $maxItems) {
                        $diff = $packageItemsCount - $maxItems;
                        $message = abs($diff) == 1 ? 'Remove %d product from package subtype %s of Package %d: %s (this includes hidden products).'
                            : 'Remove %d products from package subtype %s of Package %d: %s (this includes hidden products)';
                        $warnings[$packageId][$subtypeIdentifier] = htmlspecialchars(Mage::helper('dyna_configurator')->__($message,
                            $diff,
                            $subtypeIdentifier,
                            $packageId,
                            $packageName
                        ));
                    }

                    if ($minItems && $minItems != -1 && $packageItemsCount < $minItems) {
                        $diff = $minItems - $packageItemsCount;
                        $message = abs($diff) == 1 ? 'Add %d product in the package subtype %s of Package %d: %s.'
                            : 'Add %d products in the package subtype %s of Package %d: %s.';
                        $warnings[$packageId][$subtypeIdentifier] = htmlspecialchars(Mage::helper('dyna_configurator')->__($message,
                            $diff,
                            $subtypeIdentifier,
                            $packageId,
                            $packageName
                        ));
                    }
                    // If max is not infinite
                    if ($maxItems && $maxItems != -1 && $packageItemsCount > $maxItems) {
                        $diff = $packageItemsCount - $maxItems;
                        $message = abs($diff) == 1 ? 'Remove %d product from package subtype %s of Package %d: %s (this includes hidden products).'
                            : 'Remove %d products from package subtype %s of Package %d: %s (this includes hidden products)';
                        $warnings[$packageId][$subtypeIdentifier] = htmlspecialchars(Mage::helper('dyna_configurator')->__($message,
                            $diff,
                            $subtypeIdentifier,
                            $packageId,
                            $packageName
                        ));
                    }
                }
            }
            if (empty($warnings[$packageId])) {
                unset($warnings[$packageId]);
            }
        }

        return $warnings ?? [];
    }

    /**
     * Skip levy product for cardinality
     * @param $packageItems
     */
    public function skipLevyProduct(&$packageItems)
    {
        $levyAddonId = Mage::getResourceSingleton('catalog/product')->getIdBySku(Mage::getStoreConfig(Dyna_PriceRules_Model_Observer::COPY_LEVY_PREFIX . 'standaard'));
        foreach ($packageItems as $key => $item) {
            if ($item->getProductId() == $levyAddonId) {
                unset($packageItems[$key]);
            }
        }
    }

    /**
     * Based on object used in expression, only initialize the needed objects
     * @param $ssExpression
     * @return array
     */
    protected function getServiceExpressionParams($ssExpression)
    {
        $params = [];

        if(strpos($ssExpression, 'customer.') !== FALSE){
            if(!$this->customerObject){
                $this->customerObject = Mage::getModel('dyna_configurator/expression_customer');
            }
            $params['customer'] = $this->customerObject;
        }

        if(strpos($ssExpression, 'cart.') !== FALSE){
            if(!$this->cartObject){
                $this->cartObject = Mage::getModel('dyna_configurator/expression_cart');
            }
            $params['cart'] = $this->cartObject;
        }

        if(strpos($ssExpression, 'availability.') !== FALSE){
            if(!$this->availabilityObject){
                $this->availabilityObject = Mage::getModel('dyna_configurator/expression_availability');
            }
            $params['availability'] = $this->availabilityObject;
        }

        if(strpos($ssExpression, 'installBase.') !== FALSE){
            if(!$this->installBaseObject){
                $this->installBaseObject = Mage::getModel('dyna_configurator/expression_installBase');
            }
            $params['installBase'] = $this->installBaseObject;
        }

        if(strpos($ssExpression, 'activeInstallBase.') !== FALSE){
            if(!$this->activeInstallBaseObject){
                $this->activeInstallBaseObject = Mage::getModel('dyna_configurator/expression_activeInstallBase');
            }
            $params['activeInstallBase'] = $this->activeInstallBaseObject;
        }

        if(strpos($ssExpression, 'agent.') !== FALSE){
            if(!$this->agentObject){
                $this->agentObject = Mage::getModel('dyna_configurator/expression_agent');
            }
            $params['agent'] = $this->agentObject;
        }

        if(strpos($ssExpression, 'serviceability.') !== FALSE){
            if(!$this->servicabilityObject){
                $this->servicabilityObject = Mage::getModel('vznl_configurator/expression_serviceability');
            }
            $params['serviceability'] = $this->servicabilityObject;
        }

        return $params;
    }

    /**
     * @param $ssExpression
     * @return array|bool|string
     */
    protected function evaluateServiceSourceExpression($ssExpression)
    {
        /** @var $dynaCoreHelper Dyna_Core_Helper_Data */
        $dynaCoreHelper = Mage::helper('dyna_core');
        try {
            $passed = $dynaCoreHelper->evaluateExpressionLanguage(
                $ssExpression,
                $this->getServiceExpressionParams($ssExpression)
            );
        } catch (\Exception $e) {
            $passed = false;
        }
        return $passed;
    }

    /**
     * Produces Min/Max/Eql warning messages for current cart
     * @param $cartProductsHash
     * @return array
     * @internal param int $packageId
     * @internal param string $section
     * @internal param array $productsInSection
     * @internal param array $updatedSectionProducts
     */
    public function getMinMaxEqlWarnings($cartProductsHash)
    {
        $cache_key = sprintf('min_max_eql_rules_for_quote_items_%s', $cartProductsHash);
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getModel('checkout/session')->getQuote();
        $packages = $quote->getCartPackages(true);
        $warningMessages = [];
        $response = $this->getCache()->load($cache_key);
        if ($response !== false) {
            return unserialize($response);
        }
        $warnings
            = $cartProducts
            = $cartCategories
            = $eachProductCategories
            = $ruleRightProductIds
            = [];

        foreach ( $packages as $packageId => $package ) {
            $packageItems = $package->getItems();
            $cartProducts[$packageId] = [];
            $cartCategories[$packageId] = [];
            $cartCategoriesProducts[$packageId] = [];

            foreach ($packageItems as $item) {
                $flippedCategoryIds = array_flip(Mage::getResourceModel("catalog/product")->getCategoryIdsWithAnchors($item->getProduct()));
                $productId = intval($item->getProductId());
                if (!isset($eachProductCategories[$productId])) {
                    $eachProductCategories[$productId] = Mage::getResourceModel("catalog/product")->getCategoryIdsWithAnchors($item->getProduct());
                }

                if (!in_array($productId, $cartProducts[$packageId])) {
                    $cartProducts[$packageId][$item->getSku()] = $productId;
                    $productCategoriesQty = array_fill_keys($eachProductCategories[$productId], $item->getQty());
                    foreach (array_unique(array_keys($cartCategories[$packageId] + $productCategoriesQty)) as $key) {
                        $cartCategories[$packageId][$key] = (isset($cartCategories[$packageId][$key]) ? $cartCategories[$packageId][$key] : 0) + (isset($productCategoriesQty[$key]) ? $productCategoriesQty[$key] : 0);
                        if (isset($flippedCategoryIds[$key])) {
                            $cartCategoriesProducts[$packageId][$key][$item->getProductId()] = $item->getSku() . '_' . $item->getName();
                        }
                    }
                }
            }
        }

        $ruleCollection = $this->getRulesForProductsInCart($cartProducts, $eachProductCategories, $packages ?? null);
        foreach ($ruleCollection as $packageId => $packageRules) {
            $warnings[$packageId] = [];
            foreach ($packageRules as $rule) {
                $ruleId = $rule['product_match_rule_id'];
                $ruleRightId = $rule['right_id'];
                $ruleOperation = $rule['operation'];
                $ruleOperationValue = intval($rule['operation_value']);
                $ruleOperationType = $rule['operation_type'];

                $targetType = 'category';
                if (array_key_exists($ruleRightId, $cartCategories[$packageId])) {
                    $cartCategories[$packageId][$ruleRightId] = $cartCategories[$packageId][$ruleRightId] ?: 0;
                } else {
                    $cartCategories[$packageId][$ruleRightId] = 0;
                }

                switch ($ruleOperationType) {
                    case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_P2S:
                    case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_C2S:
                    case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_S2S:
                        $ruleRightProductIds = $rule['right_id'];
                        /** @var Dyna_Catalog_Model_Product $productModel */
                        $productModel = Mage::getModel('catalog/product');
                        // convert all product ids to skus
                        $targetProductsSkusArray = $productModel->getSkusByIds($ruleRightProductIds);
                        $productsCount = 0;
                        $rightProductIdsInCartToSkus = [];
                        $cartProductIdsToSkus = array_flip($cartProducts[$packageId]);
                        foreach ($ruleRightProductIds as $ruleRightProductId) {
                            if (isset($cartProductIdsToSkus[$ruleRightProductId])) {
                                unset($targetProductsSkusArray[$ruleRightProductId]);
                                $rightProductIdsInCartToSkus[$ruleRightProductId] = $cartProductIdsToSkus[$ruleRightProductId];
                                $productsCount++;
                            }
                        }
                        $targetType = 'service';
                        break;
                    case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_S2P:
                    case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_P2P:
                    case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_C2P:
                        /** @var Varien_Db_Adapter_Interface $adapter */
                        $adapter = Mage::getSingleton('core/resource')->getConnection('core_read');
                        $select = $adapter->select()->from('catalog_category_product', array('category_id'))->where('product_id = :product_id');
                        $bind = array('product_id' => $rule['right_id']);
                        // @todo in case of target=product rules we should get the family category they are part of
                        $ruleRightId = array_shift($adapter->fetchCol($select, $bind));
                        $ruleRightProductId = $rule['right_id'];
                        $targetType = 'product';
                        break;
                }

                switch ($ruleOperation) {
                    case Dyna_ProductMatchRule_Model_Rule::OP_MIN_N:
                        if ($targetType == 'category' && array_key_exists($ruleRightId, $cartCategories[$packageId])) {
                            $qty = $ruleOperationValue - $cartCategories[$packageId][$ruleRightId];
                            $warnings[$packageId][$ruleId][$targetType][$ruleRightId] = $qty > 0 ? $qty : 0;
                        } elseif ($targetType == 'product') {
                            if (in_array($ruleRightProductId, $cartProducts[$packageId])) {
                                $warnings[$packageId][$ruleId][$targetType][$ruleRightProductId] = 0;
                            } else {
                                $warnings[$packageId][$ruleId][$targetType][$ruleRightProductId] = 1;
                            }
                        } elseif ($targetType == 'service') {
                            $qty = $ruleOperationValue - $productsCount;
                            $warnings[$packageId][$ruleId][$targetType][$ruleRightId] = $qty > 0 ? $qty : 0;
                        }
                        break;
                    case Dyna_ProductMatchRule_Model_Rule::OP_MAX_N:
                        if ($targetType == 'category' && array_key_exists($ruleRightId, $cartCategories[$packageId])) {
                            $qty = $ruleOperationValue - $cartCategories[$packageId][$ruleRightId];
                            $warnings[$packageId][$ruleId][$targetType][$ruleRightId] = $qty < 0 ? $qty : 0;
                        } elseif ($targetType == 'product') {
                            $warnings[$packageId][$ruleId][$targetType][$ruleRightProductId] = in_array($ruleRightProductId, $cartProducts[$packageId]) ? 0 : 1;
                        } elseif ($targetType == 'service') {
                            $qty = $ruleOperationValue - $productsCount;
                            $warnings[$packageId][$ruleId][$targetType][$ruleRightId] = $qty < 0 ? $qty : 0;
                        }
                        break;
                    case Dyna_ProductMatchRule_Model_Rule::OP_EQL_N:
                        if ($targetType == 'category' && array_key_exists($ruleRightId, $cartCategories[$packageId])) {
                            $warnings[$packageId][$ruleId][$targetType][$ruleRightId] = $ruleOperationValue - $cartCategories[$packageId][$ruleRightId];
                        } elseif ($targetType == 'product') {
                            $warnings[$packageId][$ruleId][$targetType][$ruleRightProductId] = in_array($ruleRightProductId, $cartProducts[$packageId]) ? 0 : 1;
                        } elseif ($targetType == 'service') {
                            $warnings[$packageId][$ruleId][$targetType][$ruleRightId] = $ruleOperationValue - $productsCount;
                        }
                        break;
                }
            }
        }

        foreach ($packages as $packageModel) {
            $packageId = $packageModel->getPackageId();
            $packageName = Mage::getModel('dyna_package/packageType')->loadByCode($packageModel->getType())->getFrontEndName();
            if (empty($packageId) || empty($warnings[$packageId])) {
                continue;
            }
            foreach ($warnings[$packageId] as $warningRule => $w) {
                $warningType = array_keys($w)[0];
                $wid = array_keys($w[$warningType])[0];
                $wQty = (int)$w[$warningType][$wid];
                if (isset($cartCategoriesProducts[$packageId][$wid])) {
                    $ruleOperationValue = count($cartCategoriesProducts[$packageId][$wid]) - abs($wQty);
                }
                if ($wQty === 0) {
                    continue;
                }
                if ($warningType == 'category') {
                    if ($wQty > 0) {
                        $message = 'Add %d %s product(s) to %s.';
                        $message = Mage::helper('dyna_configurator')->__($message,
                            abs($wQty),
                            Mage::getModel('catalog/category')->load($wid)->getName(),
                            $packageName
                        );
                    } else {
                        $message = 'It is only possible to select %d product(s) of the following list of selected products: %s';
                        $message = Mage::helper('dyna_configurator')->__($message,
                            $ruleOperationValue,
                            implode(', ', $cartCategoriesProducts[$packageId][$wid])
                        );
                    }
                } elseif ($warningType == 'product') {
                    $message = ($wQty < 0 ? 'Remove' : 'Add') . ' %s from %s.';
                    $message = Mage::helper('dyna_configurator')->__($message,
                        Mage::getModel('catalog/product')->load($wid)->getName(),
                        $packageName
                    );
                } elseif ($warningType == 'service') {
                    if ($wQty > 0) {
                        $message = 'Add %d %s product(s) to %s.';
                        $message = Mage::helper('dyna_configurator')->__($message,
                            abs($wQty),
                            implode(', ', $targetProductsSkusArray),
                            $packageName
                        );
                    } else {
                        $message = 'It is only possible to select %d product(s) of the following list of selected products: %s';
                        $message = Mage::helper('dyna_configurator')->__($message,
                            $ruleOperationValue,
                            implode(', ', $rightProductIdsInCartToSkus)
                        );
                    }
                }
                $warningMessages[$packageId][$warningRule] = htmlspecialchars($message);
            }
        }

        $this->getCache()->save(serialize($warningMessages), $cache_key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());

        return $warningMessages;
    }

    /**
     * Retrieves existing cart package items based on type and package id.
     * @param $type
     * @param null $packageId
     * @return array
     */
    public function getCartItems($type, $packageId = null)
    {
        $ids = $installedBaseProductsIds = $acqProductIds = array();
        if (!$packageId) {
            return $ids;
        }
        /** @var Vznl_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        /** @var Vznl_Checkout_Model_Sales_Quote_Item $item */
        foreach ($quote->getPackageItems($packageId) as $item) {

            if (in_array($type, explode(",", $item->getPackageType()))) {
                $packageSubtype = strtolower(current($item->getProduct()->getType()));

                $ids[$packageSubtype][] = (int)$item->getProductId();
                $ids[$packageSubtype] = array_values(array_unique($ids[$packageSubtype]));
                $acqProductIds[] = (int)$item->getProductId();

                //Get get_contract_start_date flag of each package to check if it belongs to installed base (Inlife Sales)
                if ($item->getContractStartDate() && $item->getPreselectProduct()) {
                    $installedBaseProductsIds[(int)$item->getProductId()] = [];
                    if ($item->getContractStartDate()) {
                        $installedBaseProductsIds[(int)$item->getProductId()]['contract_start_date'] = date('d-m-Y', strtotime($item->getContractStartDate()));
                    }
                    if ($item->getContractEndDate()) {
                        $installedBaseProductsIds[(int)$item->getProductId()]['contract_end_date'] = date('d-m-Y', strtotime($item->getContractEndDate()));
                    }
                    if($item->getContractPossibleCancellationDate()){
                        $installedBaseProductsIds[(int)$item->getProductId()]['contract_possible_cancellation_date'] = date('d-m-Y', strtotime($item->getContractPossibleCancellationDate()));
                    }
                }
            }
        }
        //merge alternate items that are preselected or not
        $installedBase = array_merge($quote->getAlternateItems(), $quote->getAlternateItems(null, false, false));
        // Append the Installed Base items that were removed from current quote
        foreach ($installedBase as $alternateItem) {
            $installedBaseProductsIds[(int)$alternateItem->getProductId()] = [];
        }

        $ids['initCart'] = $ids;
        $ids['installedBaseCart'] = $installedBaseProductsIds;
        $ids['acqProductIds'] = $acqProductIds;

        return $ids;
    }

    /**
     * Creates a mew package and sets it as active package in the current quote
     * @param string $type
     * @param string $saleType
     * @param null $ctn
     * @param null $currentProducts
     * @param array $oneOffDealOptions
     * @param null $parentPackageId
     * @param null $relatedProductSku
     * @param null $bundleId
     * @param null $contractEndDate
     * @param null $contractStartDate
     * @return mixed
     */
    public function initNewPackage($type, $saleType, $ctn = null, $currentProducts = null, $oneOffDealOptions = array(), $parentPackageId = null, $relatedProductSku = null, $bundleId = null, $packageCreationTypeId = null, $offerId = null, $contractEndDate = null, $contractStartDate = null, $isOffer = 0)
    {
        /** @var Dyna_Configurator_Helper_Rules $rulesHelper */
        $rulesHelper = Mage::helper('dyna_configurator/rules');

        // always clear cache for new package
        $rulesHelper->cacheSave(null, $rulesHelper->getCacheKey($type));

        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $quoteId = $quote->getId();
        $newPackageId = Omnius_Configurator_Model_CartObserver::getNewPackageId($quote);
        /** @var Dyna_Customer_Model_Session $customerSession */
        $customerSession = Mage::getSingleton('customer/session');

        if (!$quoteId) {
            $quote->setCustomerId($customerSession->getId());
        }

        $quote->setActivePackageId($newPackageId);
        $quote->save();

        if (!$quoteId) {
            Mage::getSingleton('checkout/cart')->getCheckoutSession()->setQuoteId($quote->getId());
        }

        // Create the new package for the quote or update if already created
        /** @var Dyna_Package_Model_Package $packageModel */
        $packageModel = Mage::getModel('package/package')->getCollection()
            ->addFieldToFilter('quote_id', $quote->getId())
            ->addFieldToFilter('package_id', $newPackageId)
            ->getFirstItem();

        // If package creation type is not provided, try to select it by the package type
        if ($packageCreationTypeId === null) {
            $packageCreationTypeId = $this->getCreationTypeId(Dyna_Catalog_Model_Type::getCreationTypesMapping()[strtolower($type)]);

            if (!$packageCreationTypeId) {
                Mage::throwException("Cannot determine the package creation type.");
            }
        }

        if (!$packageModel->getId()) {
            // Cannot create new serviceability dependent package without a service address id
            $serviceAddressId = Mage::getSingleton('dyna_address/storage')->getAddressId();

            if (!in_array($type, Dyna_Catalog_Model_Type::getMobilePackages())) {
                $packageModel->setServiceAddressId($serviceAddressId);
                $packageModel->setServiceAddressData(json_encode(
                        array_merge(
                            array('area_code' => Mage::getSingleton('dyna_address/storage')->getAreaCode()),
                            array('asb' => Mage::getSingleton('dyna_address/storage')->getAsb()),
                            Mage::getSingleton('dyna_address/storage')->getServiceAddressParts(),
                            array('id' => $serviceAddressId))
                    )
                );
            }

            $packageModel->setQuoteId($this->_getQuote()->getId())
                ->setPackageId($newPackageId)
                ->setType($type)
                ->setSaleType($saleType)
                ->setCtn($ctn)
                ->setCurrentProducts($currentProducts)
                ->setParentId($parentPackageId)
                ->setRedplusRelatedProduct($relatedProductSku)
                ->setPackageCreationTypeId($packageCreationTypeId)
                ->setBundleId($bundleId)
                ->takeServicesSnapshot()
                ->setCtnOriginalStartDate($contractStartDate)
                ->setCtnOriginalEndDate($contractEndDate);

            if ($isOffer) {
                $packageModel->setData('is_offer', 1);
            } else {
                $packageModel->setData('is_offer', 0);
            }
        }

        $redplusCreationTypeId = $this->getCreationTypeId(Dyna_Catalog_Model_Type::CREATION_TYPE_REDPLUS);
        if ($packageCreationTypeId == $redplusCreationTypeId) {
            // get package creation type
            $typeModel = Mage::getModel('dyna_package/packageCreationTypes');
            $packageCreationType = $typeModel->getById($redplusCreationTypeId);
            //get permanent filters for the red+ member package
            $filters = $packageCreationType->getFilterAttributes();
            $packageModel->setPermanentFilters($filters);
        }

        if($this->ilsInitialSocs) {
            $packageModel->setIlsInitialSocs($this->ilsInitialSocs);
        }
        if($this->permanentFilters) {
            $packageModel->setPermanentFilters($this->permanentFilters);
        }

        if (!empty($oneOffDealOptions)) {
            $packageModel->setOneOfDeal(true);
            $quote->setItemsQty(count(explode(',', $ctn)));
            $packageModel->setRetainable($oneOffDealOptions['retainable']);
        }
        $uctParams = $customerSession->getUctParams();

        if ($uctParams && !empty($uctParams['transactionId']) && !empty($uctParams['campaignId']) && $offerId) {
            if ($offerId) {
                $uctParams['offerId'] = $offerId;
                $customerSession->setUctParams($uctParams);
                $packageModel->setOfferId($offerId);
            }
        }
        $packageModel->save();

        return $newPackageId;
    }


    /**
     * gets cached sorted cart items
     * @param array $sortedProductIds
     * @param array $cacheProductIds
     * @param string $packageId
     * @param array $items
     * @return mixed
     */
    private function getCachedSortedProducts($cacheProductIds = [], $items = [])
    {
        $sortedProductResult = [];
        foreach($cacheProductIds as $product_id) {
            foreach ($items as $item) {
               if($product_id == $item->getProduct()->getId()) {
                    $sortedProductResult[] = $item;
                    continue;
                }
            }
        }
        return $sortedProductResult;
    }

    /**
     * sorts the cart items and stores in session
     * @param array $items
     * @param bool $useCache
     * @return array
     */
    public function getSortedCartProducts(
        $items,
        $useCache = true
    )
    {
        $itemProductId = [];
        $itemCategories = [];
        $packageIds =[];
        $packageTypeId = [];
        foreach ($items as $item) {
            $itemIds[] = $item->getId();
            $product = $item->getProduct();
            $packageIds[] = $item->getPackageId();
            $packageTypeId[] = $product->getPackageTypeId();
            $itemProductId[] = $product->getId();
            $itemCategories[] = $product->getCategoryIds();
        }

        $uniquePackageId = array_unique($packageIds);
        $uniquePackageTypeId = array_unique($packageTypeId);
        $packageId = (count($uniquePackageId) > 1) ? implode('_', $uniquePackageId) : $uniquePackageId[0];
        sort($itemProductId);
        $sortCachekey = 'SORT_'.implode('_', $itemProductId);

        if ($useCache && $cache = unserialize($this->getCache()->load($sortCachekey))) {
            return $this->getCachedSortedProducts($cache, $items);
        } else {
            $itemFamilyCategoriesFinal = $this->getCartFamilyCategoriesCollection($itemCategories);
            $productCollection = $this->getCartProductCollection($itemProductId);
            $subTypeArray = $this->getCartSubTypeCollection($uniquePackageTypeId);

            $familyLevelSortedItems = [];

            foreach ($itemFamilyCategoriesFinal as $category) {
                foreach ($productCollection as $coll) {
                    $categoryId = $coll->getCategoryIds();
                    foreach ($items as $item) {
                        if ($item->getProduct()->isServiceItem() && !in_array($item, $familyLevelSortedItems)) {
                            $familyLevelSortedItems[] = $item;
                        } elseif (isset($categoryId[0])
                            && ($category->getId() == $categoryId[0])
                            && ($item->getProductId() == $coll->getId())) {
                            $familyLevelSortedItems[] = $item;
                            continue;
                        }
                    }
                }
            }

            $finalSortedResult = [];
            $sotedResultProductIds = [];
            foreach ($subTypeArray as $frontendPosition => $name) {
                foreach ($familyLevelSortedItems as $sortedItem) {
                    if(strtolower($sortedItem->getProduct()->getAttributeText('package_subtype')) == strtolower($name)) {
                        $finalSortedResult[] = $sortedItem;
                        $sotedResultProductIds[] = $sortedItem->getProduct()->getId();
                        continue;
                    }
                }
            }

            //caching sorted product id
            $this->getCache()->save(serialize($sotedResultProductIds), $sortCachekey, array(Vznl_Configurator_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());

            return $finalSortedResult;
        }
    }

    /**
     * creates a collection according to family categories
     * @param array $itemCategories
     * @return array
     */
    public function getCartFamilyCategoriesCollection($itemCategories)
    {
        $itemFamilyCategoriesCollection = [];
        $itemFamilyCategoriesNull =[];
        $itemCategories = array_filter(array_map('array_filter', $itemCategories));

        $itemFamilyCategories = Mage::getModel('catalog/category')
            ->getCollection()
            ->addAttributeToSelect('entity_id')
            ->addAttributeToSelect('family_sort_order')
            ->addAttributeToFilter('entity_id', array('in'=> $itemCategories))
            ->addAttributeToFilter('is_active', 1)
            ->setOrder('family_sort_order', Varien_Data_Collection::SORT_ORDER_ASC)
            ->load();

        foreach ($itemFamilyCategories as $key => $value) {
            if ($value->getFamilySortOrder() == null) {
                $itemFamilyCategoriesNull[] = $value;
            } else {
                $itemFamilyCategoriesCollection[] = $value;
            }
        }
        $itemFamilyCategoriesCollection = array_merge($itemFamilyCategoriesCollection ,$itemFamilyCategoriesNull);

        return $itemFamilyCategoriesCollection;
    }

    /**
     * creates a collection according to cart products
     * @param array $itemProductId
     * @return array
     */
    public function getCartProductCollection($itemProductId)
    {
        $productCollection = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToFilter('entity_id', array('in'=> $itemProductId));

        $productCollection
            ->addAttributeToSort('sorting', Mage_Eav_Model_Entity_Collection_Abstract::SORT_ORDER_ASC)
            ->addAttributeToSort('name', Mage_Eav_Model_Entity_Collection_Abstract::SORT_ORDER_ASC)
            ->getSelect()
                // Resetting magento's way of doing it because it adds null sorting values at the top of the list
            ->reset(Zend_Db_Select::ORDER)
            ->order("ISNULL(sorting), CAST(sorting AS UNSIGNED) ASC, name ASC", Mage_Eav_Model_Entity_Collection_Abstract::SORT_ORDER_ASC);

        return $productCollection;
    }

    /**
     * creates a collection according to package subtype
     * @param array $packageTypeId
     * @return array
     */
    public function getCartSubTypeCollection($packageTypeId)
    {
        $subTypes = [];
        $subTypeCollection = Mage::getModel('dyna_package/packageSubtype')
            ->getCollection()
            ->addFieldToSelect('front_end_position')
            ->addFieldToSelect('package_code')
            ->addFieldToFilter('type_package_id', array('in' => $packageTypeId))
            ->setOrder('front_end_position',Mage_Eav_Model_Entity_Collection_Abstract::SORT_ORDER_ASC)
            ->load();
        foreach ($subTypeCollection as $subType) {
            $subTypes[$subType->getFrontEndPosition()] = $subType->getPackageCode();
        }

        return $subTypes;
    }

    /**
     * @param string $type
     * @param array $productIds
     * @return mixed
     */
    public function getCampaignOfferCompatibility($type, $productIds)
    {
        $websiteId = Mage::app()->getStore()->getWebsiteId();
        /** @var Dyna_Configurator_Helper_Rules $rulesHelper */
        $rulesHelper = Mage::helper('dyna_configurator/rules');
        $rules = null;

        $rulesHelper->setNeedsFauxQuote(false);
        $this->setFauxProcessContextId(Dyna_Catalog_Model_ProcessContext::ACQ);
        $rules = $rulesHelper->getPreconditionRules($productIds, $type, $websiteId);

        //$rulesHelper->cacheSave(null, $rulesHelper->getCacheKey($type));
        if (in_array($type, Dyna_Catalog_Model_Type::getMobilePackages()) && count($productIds) === 1) {
            $rules['rules'] = $productIds;
        }

        return count(array_intersect($productIds, $rules['rules'])) == count($productIds) ? true : false;
    }

    /**
     * Only instantiate models if expression requires them
     * @param $ssExpression
     * @param $packageType
     * @return array
     */
    public function getTargetExpressionParams($ssExpression, $packageType){
        $params = [];
        if(strpos($ssExpression, 'cart.') !== FALSE){
            if(!$this->cartObject){
                $this->cartObject = Mage::getModel('dyna_configurator/expression_cart');
            }
            $params['cart'] = $this->cartObject;
        }
        if(strpos($ssExpression, 'catalog.') !== FALSE){
            if(!$this->catalogObject){
                $this->catalogObject = Mage::getModel('dyna_configurator/expression_catalog', $packageType);
            }
            $params['catalog'] = $this->catalogObject;
        }

        return $params;
    }

    /**
     * Evaluate service target expression
     * @param $ssExpression
     * @param $packageType
     * @return array|bool|string
     */
    protected function evaluateServiceTargetExpression($ssExpression, $packageType)
    {
        /** @var $dynaCoreHelper Dyna_Core_Helper_Data */
        $dynaCoreHelper = Mage::helper('dyna_core');
        try {
            $result = $dynaCoreHelper->evaluateExpressionLanguage(
                trim($ssExpression),
                $this->getTargetExpressionParams($ssExpression, $packageType)
            );
        } catch (\Exception $e) {
            $result = false;
        }
        return $result === false ? [] : $result;
    }

    /**
     * Retrieves all Min/Max/Eql rule IDs applicable to products in cart
     * @param $cartProducts
     * @param $eachProductCategories
     * @param null $packages
     * @return array
     * @internal param $cartCategories
     * @internal param $packageId
     */
    protected function getRulesForProductsInCart($cartProducts, $eachProductCategories, $packages = null)
    {
        $rules = [];
        if (empty($cartProducts)) {
            return $rules;
        }
        /** @var Dyna_Package_Model_Package $packageModel */
        foreach ($packages as $packageModel) {
            $packageType = null;
            if ($packageModel) {
                $packageType = $packageModel->getType();
            }

            /** @var Dyna_Catalog_Helper_ProcessContext $processContextHelper */
            $processContextHelper = Mage::helper("dyna_catalog/processContext");
            $processContextId = $processContextHelper->getProcessContextId($packageModel);

            $cache_key = sprintf('all_min_max_eql_rules_for_package_type_%s_%s', $packageType, $processContextId);
            $cachedValues = $this->getCache()->load($cache_key);
            if ($cachedValues === false) {
                $ruleModel = Mage::getSingleton('dyna_productmatchrule/rule');
                $ruleCollection = $ruleModel->getCollection();
                if($processContextId) {
                    $ruleCollection->getSelect()->joinLeft(
                        array('product_match_rule_process_context' => 'product_match_rule_process_context'),
                        'product_match_rule_process_context.rule_id = main_table.product_match_rule_id',
                        array('process_context_id' => 'product_match_rule_process_context.process_context_id')
                    );
                    $ruleCollection->addFieldToFilter('process_context_id', array('eq' => $processContextId));
                }
                $ruleCollection->addFieldToFilter('operation', array('in' => self::MIN_MAX_EQL_OPERATIONS))
                    ->addFieldToFilter('effective_date', array(
                        array('to' => date("Y-m-d"), 'date' => true),
                        array('null' => true)
                    ))
                    ->addFieldToFilter('expiration_date', array(
                        array('from' => date("Y-m-d"), 'date' => true),
                        array('null' => true)
                    ))
                    ->setOrder('priority', 'ASC');

                // Appending package type filter by package model
                if ($packageType) {
                    $packageTypeModel = Mage::getModel('dyna_package/packageType')
                        ->loadByCode($packageType);
                    $ruleCollection->addFieldToFilter('package_type', array('eq' => $packageTypeModel->getId()));
                }
                $allMinMaxEqlRules = $ruleCollection->toArray();
                $this->getCache()->save(serialize($allMinMaxEqlRules), $cache_key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
            } else {
                $allMinMaxEqlRules = unserialize($cachedValues);
            }

            $rulesForProduct = [];
            $packageId = $packageModel->getPackageId();
            $packageProducts = $cartProducts[$packageId];
            foreach ($packageProducts as $product) {
                foreach ($allMinMaxEqlRules['items'] as $rule) {
                    $ruleId = $rule['product_match_rule_id'];
                    switch ($rule['operation_type']) {
                        case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_S2P:
                        case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_S2C:
                            if ($this->evaluateServiceSourceExpression($rule['service_source_expression'])) {
                                $rulesForProduct[$ruleId] = $rule;
                            }
                            break;
                        case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_S2S:
                            if ($this->evaluateServiceSourceExpression($rule['service_source_expression'])) {
                                $rulesForProduct[$ruleId] = $rule;
                            } else {
                                continue 2;
                            }
                            $rulesForProduct[$ruleId]['right_id'] = $this->evaluateServiceTargetExpression($rule['service_target_expression'], $packageType);
                            break;
                        case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_P2S:
                            if ($rule['left_id'] == $product) {
                                $rulesForProduct[$ruleId] = $rule;
                            }
                            $rulesForProduct[$ruleId]['right_id'] = $this->evaluateServiceTargetExpression($rule['service_target_expression'], $packageType);
                            break;
                        case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_C2S:
                            foreach ($eachProductCategories[$product] as $category) {
                                if ($rule['left_id'] == $category) {
                                    $rulesForProduct[$ruleId] = $rule;
                                }
                            }
                            $rulesForProduct[$ruleId]['right_id'] = $this->evaluateServiceTargetExpression($rule['service_target_expression'], $packageType);
                            break;
                        case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_P2C:
                        case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_P2P:
                            if ($rule['left_id'] == $product) {
                                $rulesForProduct[$ruleId] = $rule;
                            }
                            break;
                        case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_C2C:
                        case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_C2P:
                            foreach ($eachProductCategories[$product] as $category) {
                                if ($rule['left_id'] == $category) {
                                    $rulesForProduct[$ruleId] = $rule;
                                }
                            }
                            break;
                    }
                }
            }

            $rules[$packageId] = $rulesForProduct;
        }

        return $rules;
    }
}
