<?php
/**
 * Class Inlife Helper
 */
class Vznl_Configurator_Helper_Inlife extends Mage_Core_Helper_Abstract
{
    const DEFAULT_SIMCARD_NANO_SKU = '2400002'; // used in Indirect MultiSIM

    /**
     * Process addon data received from the InLife service call to usable data
     *
     * @param $rawData
     * @return array
     * @throws Exception
     */
    public function processAddonData($rawData)
    {
        $result = array();

        foreach ($rawData as $addonCatalog) {
            $addon = array();

            $addon['assigned_component_id'] = isset($addonCatalog['component_identifier']['assigned_id']) ? $addonCatalog['component_identifier']['assigned_id'] : null;
            $addon['component_code'] = $addonCatalog['component_identifier']['catalog_code'];
            $addon['parent_assigned_component_id'] = isset($addonCatalog['component_identifier']['assigned_parent_id']) ? $addonCatalog['component_identifier']['assigned_parent_id'] : '';

            if ($addonCatalog['component_identifier']['catalog_code'] == 'VF_Garant_SPSlink') {
                $addon['vf_garant'] = true;
                $insuranceCategory = array();

                if (isset($addonCatalog['component_identifier']['item_attributes']['catalog_id'])) {
                    $addonCatalog['component_identifier']['item_attributes'] = array($addonCatalog['component_identifier']['item_attributes']);
                }

                foreach ($addonCatalog['component_identifier']['item_attributes'] as $itemAttr) {
                    if ($itemAttr['catalog_code'] == 'Commitment_Period') {
                        foreach ($itemAttr['valid_values'] as $value) {
                            $Commitment_Period[$value['code']] = $value['name'];
                        }
                    } elseif ($itemAttr['catalog_code'] == 'Insurance_Service_Variant') {
                        foreach ($itemAttr['valid_values'] as $value) {
                            $Insurance_Service_Variant[$value['code']] = $value['name'];
                        }
                    }
                }
            } elseif ($addonCatalog['component_identifier']['catalog_code'] == 'Additional_Resource') {
                $addon['multisim'] = true;
            } elseif ($addonCatalog['component_identifier']['catalog_code'] == 'Vast_Op_Mobiel') {
                $addon['vom'] = true;
                $vomOptions = array();
                if (isset($addonCatalog['component_identifier']['item_attributes'])) {
                    foreach ($addonCatalog['component_identifier']['item_attributes'] as $iAttr) {
                        if (isset($iAttr['catalog_code'])) {
                            switch ($iAttr['catalog_code']) {
                                case 'MAX_Fax_Lines_Number_Allowed':
                                    $addon['max_fax_numbers'] = isset($iAttr['selected_value']) ? (int)$iAttr['selected_value'] : 0;
                                    break;
                                case 'Total_Vast_Numbers_Quantity':
                                    $addon['max_vom_numbers'] = isset($iAttr['selected_value']) ? (int)$iAttr['selected_value'] : (int)$iAttr['default_value'];
                                    break;
                                case 'Former_provider':
                                    $addon['providers'] = $iAttr['valid_values'];
                                    break;
                            }
                        }
                    }
                }
            }

            if(!isset($addonCatalog['component_identifier']['pricing_elements'])) {
                continue;
            }

            if (isset($addonCatalog['component_identifier']['pricing_elements']['catalog_pricing_id'])) {
                $addonCatalog['component_identifier']['pricing_elements'] = array($addonCatalog['component_identifier']['pricing_elements']);
            }

            $addonOptions = array();
            foreach($addonCatalog['component_identifier']['pricing_elements'] as $_addon) {
                if (isset($_addon['dynamic_attributes']) && is_array($_addon['dynamic_attributes'])) {
                    $addon['disable'] = array_filter($_addon['dynamic_attributes'], function($val) {
                        return (
                            (strtolower($val['name']) == "restrict removal" && isset($val['value']) && (strtolower($val['value']) != 'system')) ||
                            (strtolower($val['name']) == 'is eligible for removal via online' && isset($val['value']) && strtolower($val['value']) == 'n')
                        );
                    });

                    /** @var Vznl_Validators_Helper_Data $dataValidator */
                    $dataValidator = Mage::helper('vznl_validators/data');

                    // Ignore certain addons
                    if ((!$dataValidator->validateAddonCode($_addon['catalog_pricing_code'])) ||
                        (!$dataValidator->validateAddonAttributes($_addon['dynamic_attributes'], 'UI Display Category'))) {
                        continue;
                    }

                    foreach ($_addon['dynamic_attributes'] as $attr) {

                        if ($attr['name'] == "Offer Type" && $attr['value'] == "AddOn") {

                            $_addon['ui_display_category'] = array_filter($_addon['dynamic_attributes'], function($val) {
                                return ($val['name'] == "UI Display Category" && isset($val['value']));
                            });
                            $_addon['ui_display_category'] = reset($_addon['ui_display_category']);

                            if (isset($_addon['pricing_info']) && isset($_addon['pricing_info']['prices']['rate'])) {
                                $_addon['pricing_info']['prices'] = array($_addon['pricing_info']['prices']);
                            }
                            if (isset($_addon['pricing_info']) && !isset($_addon['pricing_info']['prices']['rate']) && is_array($_addon['pricing_info'])) {
                                foreach ($_addon['pricing_info']['prices'] as $price) {
                                    if (isset($price['dimensions_atr_value'])) {
                                        $nowValue = $price['dimensions_atr_value'];
                                        $nowValueCount = $this->getArrayCount($nowValue);

                                        if ($nowValueCount == 2 && isset($nowValue[1]) && $nowValue[1]['name'] == 'Insurance category' && isset($addon['vf_garant']) && $addon['vf_garant']) {
                                            $insuranceCategory[$nowValue[1]['value']][$nowValue[0]['value']] =  array(
                                                'name' => $Insurance_Service_Variant[$nowValue[0]['value']],
                                                'price' => $price['rate']
                                            );

                                        } elseif ($nowValueCount == 2 && isset($nowValue['name']) && $nowValue['name'] == 'Quantity scale' && isset($addon['vom']) && $addon['vom']) {
                                            $vomOptions[$nowValue['value']] =  array(
                                                'name' => $nowValue['value'],
                                                'price' => $price['rate']
                                            );
                                        }
                                    }
                                }

                                if (isset($_addon['pricing_info']['price_type'])) {
                                    $addon['price_type'] = $_addon['pricing_info']['price_type'];
                                }
                            }

                            if (isset($addon['vf_garant']) && $addon['vf_garant']) {
                                $addon['insurance_category'] = $insuranceCategory;
                                ksort($Commitment_Period);
                                $addon['commitment_period'] = $Commitment_Period;
                            }
                            if (isset($addon['vom']) && $addon['vom']) {
                                $addon['vom_options'] = $vomOptions;
                            }

                            unset($_addon['dynamic_attributes']);

                            if (isset($addon['multisim']) && $addon['multisim']) {
                                $addonOptions[] = $_addon;
                            } else {
                                $addon['billing_offer_code'] = $_addon['catalog_pricing_code'];
                                $addon['addon_name'] = $_addon['catalog_pricing_description'];
                                $addon['addon_options'] = array($_addon);
                                $addon['context'] = $addonCatalog['context'];
                                $result[] = $addon;
                            }
                        }
                    }
                }
            }

            if (isset($addon['multisim']) && $addon['multisim'] && !empty($addonOptions)) {
                $addon['billing_offer_code'] = $addonOptions[0]['catalog_pricing_code'];
                $addon['addon_name'] = $addonOptions[0]['catalog_pricing_description'];
                $addon['addon_options'] = $addonOptions;
                $addon['context'] = $addonCatalog['context'];
                $result[] = $addon;
            }
        }

        return $result;
    }

    private function getArrayCount($dataArray)
    {
        return count($dataArray);

    }

    /**
     * Search for current SimCard in the InLife getAssignedProductDetails() service call response
     *
     * @param $rawData
     * @return null|string
     */
    public function inlifeFetchCurrentSim($rawData)
    {
        if (!isset($rawData['child_implemented_products'])) return null;
        $simCards = array();
        foreach ($rawData['child_implemented_products'] as $row) {
            if (isset($row['catalog_product_cod']) && ($row['catalog_product_cod'] == 'Primary_MSISDN' || $row['catalog_product_cod'] == 'Additional_Resource')) {
                if (isset($row['child_implemented_products'])) {
                    if (!isset($row['child_implemented_products'][0])) {
                        $row['child_implemented_products'] = !empty($row['child_implemented_products']) ? array($row['child_implemented_products']): [];
                    }

                    foreach ($row['child_implemented_products'] as $cip) {
                        if (!isset($cip['implemented_attributes'][0])) {
                            $cip['implemented_attributes'] = !empty($cip['implemented_attributes']) ? array($cip['implemented_attributes']) : [];
                        }
                        foreach ($cip['implemented_attributes'] as $attribute) {
                            if (!empty($attribute['code']) && $attribute['code'] == 'RLC_SIM card number') {
                                if (!empty($attribute['selected_localized_value'])) {
                                    $simCards[$attribute['selected_localized_value']] = $row['catalog_product_name'];
                                }
                            }
                        }
                    }
                }
            }
        }

        return $simCards;
    }

    /**
     * Get simcard producs that have identified_simcard_selector = (multisim || simswap)
     *
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     */
    public function getAvailableSimcards()
    {
        $attribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', Vznl_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR);
        $simcardAttr = Mage::helper('dyna_catalog')->getAttributeAdminLabel($attribute, null, Vznl_Catalog_Model_Type::SUBTYPE_SIMCARD);
        $simAttribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', Vznl_Catalog_Model_Product::SIM_SELECTION_ATTRIBUTE);
        $simSelection[] = Mage::helper('dyna_catalog')->getAttributeAdminLabel($simAttribute, null, Vznl_Catalog_Model_Product::SIM_SELECTION_MULTISIM);
        $simSelection[] = Mage::helper('dyna_catalog')->getAttributeAdminLabel($simAttribute, null, Vznl_Catalog_Model_Product::SIM_SELECTION_SIMSWAP);

        return Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToSelect('*')
            ->addWebsiteFilter()
            ->addAttributeToFilter(Vznl_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR, $simcardAttr)
            ->addAttributeToFilter(Vznl_Catalog_Model_Product::SIM_SELECTION_ATTRIBUTE, array('in' => array($simSelection)))
            ->addAttributeToFilter('type_id', Mage_Catalog_Model_Product_Type::TYPE_SIMPLE)
            ->load();
    }

    /**
     * Used to display service errors in a formatted way
     *
     * @param $array
     * @return string
     */
    public function array2ul($array)
    {
        $output = '<ul class="list-unstyled">';
        foreach ($array as $key => $value) {
            $function = is_array($value) ? __FUNCTION__ : 'htmlspecialchars';
            if (!empty($value)) {
                $output .= '<li>' . $this->$function($value) . '</li>';
            }
        }
        return $output . '</ul>';
    }

    /**
     * @param $text
     * @return string
     */
    public function htmlspecialchars($text)
    {
        return htmlspecialchars($text);
    }

    /**
     * Helper function to calculate prices with and without taxes
     *
     * @param $price
     * @param int $percent
     * @return array
     */
    public function calculateTaxAmount($price, $percent = null)
    {
        $percent = $percent ?: Mage::helper('dyna_catalog')->getDefaultCountryTaxRate();
        $price = round($price, 2);
        $calculator = Mage::getSingleton('tax/calculation');
        $taxAmount = round($calculator->calcTaxAmount($price, $percent, true, $roundTaxFirst = false), 2);
        $priceExclVat = round($price - $taxAmount, 2);

        return array('price' => $price, 'tax_amount' => $taxAmount, 'excl_tax' => $priceExclVat);
    }

    /**
     * Check if the current telesales logged in agent can migrate price plans
     *
     * @return bool
     */
    public function canMigratePricePlans()
    {
        return (
            (Mage::helper('agent')->isTelesalesLine())
            && Mage::getSingleton('customer/session')->getAgent(true)->isGranted(array('ILS_MIGRATE_PRICE_PLANS'))
        );
    }

    /**
     * Check if the customer is moving to a different address
     *
     * @return bool
     */
    public function isCustomerMovingToDifferentAddress($orderType) {
        if ($orderType == Vznl_Configurator_Model_PackageType::ORDER_TYPE_MOVE) {
            $customerFootprint = '';
            $serviceData = Mage::getSingleton('dyna_customer/session')->getServiceCustomers();
            $serviceFootprint= $quote = Mage::getSingleton('checkout/cart')->getQuote()->getFootprint();
            $serviceData = array_shift($serviceData[Vznl_Customer_Adapter_Search_Adapter::ZIGGO_CUSTOMER_STACK]);
            $address = $serviceData['postal_addresses'];

            foreach ($address ?? [] as $addkey => $addvalue) {
                if ($addvalue['AddressTypeCode'] == 'S') {
                    $customerFootprint = $addvalue['FootPrint'];
                    break;
                }
            }

            if($customerFootprint != $serviceFootprint) {
                return true;
            }
        }

        return false;
    }

    /**
     * get the service id
     *
     * @return integer
     */
    public function getServiceAddressId($quoteId)
    {
        $quote = Mage::getModel('sales/quote')->load($quoteId);
        $serviceAddress = unserialize($quote->getServiceAddress());

        return $serviceAddress['addressId'];
    }

}
