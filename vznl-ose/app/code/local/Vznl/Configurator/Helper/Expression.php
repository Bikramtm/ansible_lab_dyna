<?php

class Vznl_Configurator_Helper_Expression extends Dyna_Configurator_Helper_Expression
{
    CONST BUNDLE_PACKAGES_KEY = 'bundled_packages';

    /**
     * Get the product in quote by sku
     *
     * @param $productSku
     * @return array
     */
    public function getProductsInQuoteBySku($productSku)
    {
        if (!isset($this->quoteProducts[$productSku])) {
            return [];
        }

        if (empty($this->quote->getItemsCollection())) {
            return [];
        }

        $products = [];

        foreach ($this->quote->getItemsCollection() as $item) {
            if ($productSku === $item->getSku()) {
                $products[] = $item;
            }
        }

        return $products;
    }

    /**
     * Return magento sql operation for filtering collections
     * @param $operator
     * @return null|string
     */
    public static function getOperationSqlByOperator($operator)
    {
        //map the operator
        switch (trim($operator)) {
            case ">":
                $returnOperator = "gt";
                break;
            case "<":
                $returnOperator = "lt";
                break;
            case "<=":
                $returnOperator = "lteq";
                break;
            case ">=":
                $returnOperator = "gteq";
                break;
            case "=":
            case "==":
            case "===":
                $returnOperator = "eq";
                break;
            case "!=":
            case "!==":
                $returnOperator = "neq";
                break;
            default:
                // default null or eq?
                $returnOperator = null;
        }

        return $returnOperator;
    }

    /**
     * This method should be called by both hasProductAttributeValue and getProductsWithAttributeValue
     * @param null $packageSubtype
     * @param $attributeId
     * @param $operator
     * @param $attributeValue
     * @param array $productSkus
     * @return bool
     */
    public function getProductsCollectionByAttr($packageSubtype = null, $attributeId, $operator, $attributeValue, $productSkus = [])
    {
        //Products collection
        $products = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToSelect('sku');

        if (count($productSkus)) {
            $products->addAttributeToFilter('sku', array('in' => $productSkus));
        }

        if ($attributeValue !== null) {
            $attr = Mage::getModel('catalog/product')->getResource()->getAttribute($attributeId);
            if ($attr->getFrontendInput() == 'boolean') {
                $attributeValue = Mage::helper('eav')->__(ucfirst($attributeValue));
            }
            if ($attr->usesSource()) {
                $attributeValue = $attr->getSource()->getOptionId($attributeValue);
            }
            $products->addAttributeToFilter($attributeId, array($this::getOperationSqlByOperator($operator) => $attributeValue));
        }

        if ($packageSubtype !== null) {
            $attribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', Dyna_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR);
            $subtypeProduct = Mage::helper('dyna_catalog')->getAttributeAdminLabel($attribute, null, $packageSubtype);
            $products->addAttributeToFilter(Dyna_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR, array('eq' => $subtypeProduct));
        }

        return $products;
    }

    /**
     * Checks for products that contains the given attribute.
     * @param null $packageSubtype
     * @param $attributeId
     * @param $operator
     * @param $attributeValue
     * @return bool
     */
    public function hasProductAttributeValue($packageSubtype = null, $attributeId, $operator, $attributeValue = null)
    {
        if (($packageSubtype == null && (($attributeValue == null) || ($attributeId == null) || ($operator == null)))) {
            return false;
        }

        $productsCollection = $this->getProductsCollectionByAttr($packageSubtype, $attributeId, $operator, $attributeValue, $this->quoteProducts);

        return $productsCollection->load()->getSize() > 0;
    }

    /**
     * Get the products in quote based on the given category path.
     *
     * @param $categoryPath
     * @return array
     */
    public function getProductsInQuoteByCategory($categoryPath)
    {
        $items = [];
        $category = Mage::getModel('catalog/category')->getCategoryByNamePath($categoryPath, '->');

        if (!$category) {
            return [];
        }

        foreach ($this->quote->getItemsCollection() as $item) {
            if (in_array($category->getId(), $item->getProduct()->getCategoryIds())) {
                $items[] = $item;
            }
        }

        return $items;
    }

    /*
     * @param $categoryId
     * @return mixed
     *
     * Extend original method as we don't have both root and default category to unset
     */
    public function getCategoryPath($categoryId)
    {
        if (!isset($this->categoryIdToStringPathCache[$categoryId])) {
            $categoryPath = $this->allCategories[$categoryId]['path'];
            $categoryPathExploded = explode("/", $categoryPath);

            // Unset root
            unset($categoryPathExploded[0]);

            // Unset also default if/when found
            if(isset($categoryPathExploded[1]) && isset($this->allCategories[$categoryPathExploded[1]]['name']) && $this->allCategories[$categoryPathExploded[1]]['name'] == 'Default Category') {
                unset($categoryPathExploded[1]);
            }

            $categoryNames = array_map(function ($categoryId) {
                return isset($this->allCategories[$categoryId]) ? $this->allCategories[$categoryId]['name'] : '';
            }, $categoryPathExploded);

            $this->categoryIdToStringPathCache[$categoryId] = implode($this->categoryPathSeparator, $categoryNames);
        }


        return $this->categoryIdToStringPathCache[$categoryId];
    }

    /**
     * Determines whether entire cart contains or not product in a certain category
     * @param $categoryName
     * @return array
     */
    public function containsProductInCategory($categoryName, $notExpression = false)
    {
        if ($this->cartEmpty) {
            return array();
        }

        $response = array();

        if (in_array($categoryName, $this->quoteCategories)) {
            // cart contains product in this category, let's get all packages that satisfy this condition
            foreach ($this->packages as $packageId => $packageData) {
                foreach ($this->_getCartPackages() as $package) {
                    if ((int)$package->getPackageId() != (int)$packageId) {
                        continue;
                    }

                    if ($this->currentBundle && !$this->hasProcessContext($package)) {
                        continue 2;
                    }

                    if ($this->currentBundle && $this->currentBundle->isSuso() && $package->isPartOfSuso()) {
                        continue 2;
                    }

                    if ($this->currentBundle && $this->currentBundle->isMobileFixed() && $package->isPartOfMobileFixed()) {
                        continue 2;
                    }
                }
                if (!empty($this->packages[$packageId]['categories']) && in_array($categoryName, $this->packages[$packageId]['categories'])) {
                    if (empty($response)) {
                        if (Mage::registry('install_base_expression_result') !== null) {
                            $response = Mage::registry('install_base_expression_result');
                        } else {
                            $response[Dyna_Bundles_Helper_Data::EXPRESSION_PACKAGES_RESULT] = array();
                        }
                    }
                    if (!$notExpression && !empty($response[Dyna_Bundles_Helper_Data::EXPRESSION_PACKAGES_RESULT]) && !in_array($packageId, $response[Dyna_Bundles_Helper_Data::EXPRESSION_PACKAGES_RESULT])) {
                        $response[Dyna_Bundles_Helper_Data::EXPRESSION_PACKAGES_RESULT][] = $packageId;
                    } elseif ($notExpression && in_array($packageId, $response[Dyna_Bundles_Helper_Data::EXPRESSION_PACKAGES_RESULT])) {
                        unset($response[Dyna_Bundles_Helper_Data::EXPRESSION_PACKAGES_RESULT][array_search($packageId, $response[Dyna_Bundles_Helper_Data::EXPRESSION_PACKAGES_RESULT])]);
                    }
                }
            }
            // save response on register only if it is not empty
            if (!empty($response)) {
                if (Mage::registry('install_base_expression_result') !== null) {
                    Mage::unregister('install_base_expression_result');
                }

                Mage::register('install_base_expression_result', $response);
            }

            return $response;
        }

        return array();
    }

    /**
     * Checks whether or not active package has a certain package creation type code
     * @param $packageCode
     * @return array
     */
    public function packageCreationTypeId($packageCode)
    {
        // force uppercase value
        $packageCode = strtoupper($packageCode);

        /** @var Dyna_Package_Model_PackageCreationTypes $packageType */
        $packageType = $this->getPackageCreationTypeInstance()->getTypeByPackageCode($packageCode);
        if (!$packageType->getId()) {
            return array();
        }

        if (($activePackage = $this->quote->getCartPackage()) && $activePackage->getPackageCreationTypeCode() == $packageCode) {
            return $this->packageType($activePackage->getType());
        }

        return array();
    }

    /**
     * Determine whether or not current cart contains a certain package creation type
     * @param string $packageCode
     * @return array
     */
    public function containsPackageCreationTypeId(string $packageCode)
    {
        // force uppercase value
        $packageCode = strtoupper($packageCode);

        /** @var Dyna_Package_Model_PackageCreationTypes $packageType */
        $packageType = $this->getPackageCreationTypeInstance()->getTypeByPackageCode($packageCode);
        if (!$packageType->getId()) {
            return array();
        }

        foreach ($this->_getCartPackages(true) as $package) {
            if ($package->getPackageCreationTypeCode() == $packageCode) {
                return $this->containsPackageCreationType($package->getType());
            }
        }

        return array();
    }

    /**
     * Determine whether or not cart contained a product in a certain category
     * @param $categoryPath
     * @return bool
     */
    public function containedProductInCategory($categoryPath)
    {
        /** @var Dyna_Checkout_Model_Sales_Quote_Item $item */
        foreach ($this->quote->getAllItems(true, true) as $item) {
            if (($item->isContract() && $item->isDeleted()) || $item->getIsContractDrop()) {
                foreach ($item->getProduct()->getCategoryIds() as $categoryId) {
                    if ($this->getCategoryPath($categoryId) == $categoryPath) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * Gather quote information (product and categories) and active package information used by expression rules
     * @return $this
     */
    public function updateInstance(Dyna_Checkout_Model_Sales_Quote $quote)
    {
        // Set quote reference on current instance
        $this->quote = $quote;
        $this->packages = array();
        $this->packageTypes = array();
        $this->quoteProducts = array();
        $this->injectedProducts = array();
        $this->quoteCategories = array();
        $this->quoteCategoriesCounter = array();
        $this->packageCategoriesCounter = array();
        $this->quoteProductsCategories = array();
        $this->activePackage = null;
        $this->activePackageId = null;
        $this->productCache = array();
        $this->activePackageCategories = array();
        $this->activePackageProducts = array();
        $this->activePackageProductsCategories = array();

        if ($this->quote->isEmpty()) {
            $this->cartEmpty = true;
        }

        // Set customer on current helper
        $this->customer = Mage::getSingleton('customer/session')->getCustomer();
        $this->quoteCategoriesCounter = array();

        // Set quote items products for current instance
        /** @var Dyna_Checkout_Model_Sales_Quote_Item $item */
        foreach ($quote->getAllItems() as $item) {
            $product = new Varien_Object();
            $product->setEntityId($item->getProductId());
            $product->setId($item->getProductId());
            $product->setSku($item->getSku());
            $product->setPackageId($item->getPackageId());
            $this->mapCategories($product);
        }

        $newProductsSkus = array_diff($this->injectedProducts, array_keys($this->productCache));

        $newProducts = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToSelect('sku')// <- careful with this
            ->addAttributeToFilter(
                'sku', array('in' => $newProductsSkus)
            )
            ->load();

        foreach ($this->injectedProducts as $product) {
            if (isset($this->productCache[$product]) && $this->productCache[$product]) {
                $this->mapCategories($this->productCache[$product], true);
            }
        }
        foreach ($newProducts as $product) {
            $product->setPackageId($this->activePackageId);
            $this->mapCategories($product, true);
            $this->productCache[$product->getSku()] = $product;
        }


        // Setting package items for active package
        $this->activePackage = $this->quote->getActivePackage();
        $this->activePackageId = $this->quote->getActivePackageId();
        $this->packages[$this->activePackageId] = array();
        $this->packageCategoriesCounter = array();

        // Might be null
        if ($this->activePackage) {
            foreach ($this->quote->getAllItems() as $item) {
                // add categories node for current package if it doesn't exist
                if (!isset($this->packages[$item->getPackageId()]['categories'])) {
                    // Build categories for current sku
                    $this->packages[$item->getPackageId()]['categories'] = array();
                }
                // Get all categories for current product
                //$cats = $item->getProduct()->getCategoryIds();
                $product = new Varien_Object();
                $product->setEntityId($item->getProductId());
                $product->setId($item->getProductId());
                $product->setSku($item->getSku());

                // Get all categories for current product
                $cats = Mage::getResourceModel("catalog/product")->getCategoryIdsWithAnchors($product);

                // Preserve product sku
                $sku = $item->getSku();
                // Add sku to quote list of products
                $this->packages[$item->getPackageId()][$sku] = $sku;
                $this->packageTypes[$item->getPackageId()] = strtoupper($item->getPackageType());
                foreach ($cats as $categoryId) {
                    // Should never be empty because foreign keys are set to product and category entities (even so imports with foreign key checks disabled might cause problems)
                    if (!empty($this->allCategories[$categoryId])) {
                        // Add category id and name to the list of categories for current sku
                        $this->quoteCategoriesCounter[$this->getCategoryPath($categoryId)] = ($this->quoteCategoriesCounter[$this->getCategoryPath($categoryId)] ?? 0) + 1;
                        $this->packageCategoriesCounter[$item->getPackageId()][$this->getCategoryPath($categoryId)] = ($this->packageCategoriesCounter[$item->getPackageId()][$this->getCategoryPath($categoryId)] ?? 0) + 1;
                        $this->packages[$item->getPackageId()]['categories'][$categoryId] = $this->getCategoryPath($categoryId);
                    }
                }

                if ($item->getPackageId() == $this->activePackage->getPackageId()) {
                    // Add sku to quote list of products
                    $this->activePackageProducts[$sku] = $sku;
                    // Build categories for current sku
                    $this->activePackageProductsCategories[$sku] = array();
                    foreach ($cats as $categoryId) {
                        // Should never be empty because foreign keys are set to product and category entities (even so imports with foreign key checks disabled might cause problems)
                        if (!empty($this->allCategories[$categoryId])) {
                            // Add category id and name to the list of categories for current sku
                            $this->activePackageProductsCategories[$sku][$categoryId] = $this->getCategoryPath($categoryId);
                        }
                    }
                }
            }
        }

        if (is_array($alternateItems = $this->quote->getAlternateItems(null, true, false))) {
            foreach ($alternateItems as $item) {
                $cats = Mage::getResourceModel("catalog/product")->getCategoryIdsWithAnchors($item->getProduct());
                foreach ($cats as $categoryId) {
                    $this->packages[$item->getPackageId()]['contained_categories'][$categoryId] = $this->getCategoryPath($categoryId);
                }
            }
        }

        // Build quote categories
        $this->quoteCategories = array_unique(array_reduce($this->quoteProductsCategories, "array_merge", array()));

        // Build active package categories
        $this->activePackageCategories = array_unique(array_reduce($this->activePackageProductsCategories, "array_merge", array()));

        return $this;
    }

    /**
     * Get list of packages from current quote which are not part of a bundle
     * @return array|mixed
     */
    public function getNonBundledPackages()
    {
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $packageIds = [];
        if($bundledPackages = Mage::registry(self::BUNDLE_PACKAGES_KEY)){
            return $bundledPackages;
        }
        else{
            foreach($quote->getCartPackages() as $package){
                $packageIds[$package->getId()] = $package->getId();
            }
            $packages = Mage::getModel('dyna_bundles/bundlePackages')
                ->getCollection()
                ->addFieldToFilter('bundle_rule_id', array('notnull' => true))
                ->addFieldToFilter('package_id', array('in' => $packageIds));

            foreach($packages as $package){
                unset($packageIds[$package->getPackageId()]);
            }

            Mage::unregister(self::BUNDLE_PACKAGES_KEY);
            Mage::register(self::BUNDLE_PACKAGES_KEY, $packageIds);
        }

        return $packageIds;
    }

    /**
     * @param $partialResult
     * @return $response
     */
    public function updateBundleResult($partialResult)
    {
        $response = array(Dyna_Bundles_Helper_Data::EXPRESSION_PACKAGES_RESULT => [],
            Dyna_Bundles_Helper_Data::EXPRESSION_PACKAGES_STACK => [],
            'package_types' => []);

        //if other methods were already processed, intersect result
        $foundSoFar = Mage::registry('install_base_expression_result');
        if(empty($partialResult) || empty($partialResult['package_types'])){
            $foundSoFar = ['package_types' => []];
        }
        else{
            foreach($partialResult['package_types'] as $packageType => $packageIds){
                if(isset($foundSoFar['package_types'][$packageType])){
                    $foundSoFar['package_types'][$packageType] = array_intersect(
                        $foundSoFar['package_types'][$packageType],
                        $partialResult['package_types'][$packageType]
                    );
                }
                else{
                    $foundSoFar['package_types'][$packageType] = $partialResult['package_types'][$packageType];
                }
            }
        }


        //update response and save it in memory
        $response[Dyna_Bundles_Helper_Data::EXPRESSION_PACKAGES_RESULT] = empty($foundSoFar['package_types']) ? NULL:array_unique(call_user_func_array('array_merge', $foundSoFar['package_types']));
        $response[Dyna_Bundles_Helper_Data::EXPRESSION_PACKAGES_STACK] = array_keys($foundSoFar['package_types']);
        $response[Dyna_Bundles_Helper_Data::EXPRESSION_INSTALL_BASE_PRODUCT] = isset($foundSoFar['installBaseProduct']) ? $foundSoFar['installBaseProduct'] : [];
        $response['package_types'] = $foundSoFar['package_types'];

        //unset registry before updating results
        if (Mage::registry('install_base_expression_result') !== null) {
            Mage::unregister('install_base_expression_result');
        }

        Mage::register('install_base_expression_result', $response);

        return $response;
    }

    /**
     * Checks if bundle processing should be skiped. When mobile package is active, the rest of mobile packages should be checked
     * @param $activePackage
     * @param $packageChecked
     * @param $packageId
     * @return bool
     */
    public function shouldSkipBundleProcessing($activePackage, $checkedPackageType, $checkedPackageId = null)
    {
        if (!is_null($activePackage)) {
            //if active package is not mobile, it should be processed
            if (strcasecmp($activePackage->getType(), Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE) != 0){
                return false;
            }

            //if package being checked is not mobile, should be processed
            if (strcasecmp($checkedPackageType, Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE) != 0){
                return false;
            }

            //if active mobile package is being checked, should be processed
            if ($checkedPackageId != null && $activePackage->getPackageId() == $checkedPackageId){
                return false;
            }
        }

        return true;
    }
}
