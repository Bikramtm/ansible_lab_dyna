<?php

class Vznl_ResetBasket_Helper_Data extends Vznl_Core_Helper_Data
{
    /*
    * @return String
    * */
    public function getAdapterChoice()
    {
        return Mage::getStoreConfig('vodafone_service/reset_basket/adapter_choice');
    }

    /*
    * @return string peal endpoint url
    * */
    public function getEndPoint()
    {
        return Mage::getStoreConfig('vodafone_service/reset_basket/end_point');
    }

    /*
    * @return string peal username
    * */
    public function getLogin()
    {
        return Mage::getStoreConfig('vodafone_service/reset_basket/login');
    }

    /*
    * @return string peal password
    * */
    public function getPassword()
    {
        return Mage::getStoreConfig('vodafone_service/reset_basket/password');
    }

    /*
    * @return string peal cty
    * */
    public function getCountry()
    {
        return Mage::getStoreConfig('vodafone_service/reset_basket/country');
    }

    /*
    * @return string peal channel
    * */
    public function getChannel()
    {
        $agentHelper = Mage::helper('agent');
        $salesChannel = $agentHelper->getDealerSalesChannel();
        if ($salesChannel) {
            return trim($salesChannel);
        } else {
            return trim(Mage::getStoreConfig('vodafone_service/reset_basket/channel'));
        }
    }

    /*
    * @return object omnius_service/client_stubClient
    * */
    public function getStubClient()
    {
        return Mage::getModel('omnius_service/client_stubClient');
    }

    /*
    * @return boolean peal verify
    * */
    public function getVerify()
    {
        return Mage::getStoreConfigFlag('vodafone_service/reset_basket/verify');
    }

    /*
    * @return boolean peal proxy
    * */
    public function getProxy()
    {
        return Mage::getStoreConfigFlag('vodafone_service/reset_basket/proxy');
    }

    /*
     * @return object Omnius_Service_Model_Logger
     */
    public function getLogger()
    {
        return Mage::getSingleton('omnius_service/logger');
    }

    /*
    * @param $basketId
    * @return array peal/stub response based on adpter choice
    * */
    public function resetBasket($basketId)
    {
        $factoryName = $this->getAdapterChoice();
        $validAdapters = Mage::getSingleton('resetBasket/system_config_source_resetBasket_adapter')->toArray();
        if (!in_array($factoryName, $validAdapters)) {
            return 'Either no adapter is chosen or the chosen adapter is no longer/not supported!';
        }
        $adapter = $factoryName::create();
        return $adapter->call($basketId);
    }

    public function getOseItemList($itemList, &$oseItemList)
    {
        foreach($itemList as $item) {
            $oseItemList[$item["basketItemId"]] = array(
                'serialNumber' => $item["serialNumber"],
                'hardwareName' => $item["hardwareName"],
                'basketItemId' => $item["basketItemId"],
                'offerId' => $item["offerId"]
            );

            if(!empty($item["bundledItems"])) {
                $this->getOseItemList($item["bundledItems"], $oseItemList);
            }
            if(!empty($item["childItems"])) {
                $this->getOseItemList($item["childItems"], $oseItemList);
            }
        }

        return $oseItemList;
    }
}
