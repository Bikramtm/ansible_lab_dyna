<?php

/**
 * SmartValidation default front controller
 */
class Vznl_SmartValidation_IndexController extends Mage_Core_Controller_Front_Action
{
    /** @var Vznl_SmartValidation_Helper_Data|null */
    protected $_smartValHelper = null;

    /** @var Dyna_Checkout_Helper_Data|null */
    protected $_checkoutHelper = null;

    /** @var Dyna_Core_Helper_Data|null */
    protected $_dynaCoreHelper = null;

    /**
     * Get the smart validation helper
     * @return Vznl_SmartValidation_Helper_Data The smart validation helper
     */
    protected function getSmartValHelper()
    {
        if ($this->_smartValHelper === null) {
            $this->_smartValHelper = Mage::helper('smartvalidation');
        }

        return $this->_smartValHelper;
    }

    /**
     * Get the checkout helper
     * @return Dyna_Checkout_Helper_Data The checkout helper
     */
    protected function getCheckoutHelper()
    {
        if ($this->_checkoutHelper === null) {
            $this->_checkoutHelper = Mage::helper('dyna_checkout');
        }

        return $this->_checkoutHelper;
    }

    /**
     * Get the dyna core helper
     * @return Dyna_Core_Helper_Data The dyna core helper
     */
    protected function getDynaCoreHelper()
    {
        if ($this->_dynaCoreHelper === null) {
            $this->_dynaCoreHelper = Mage::helper('dyna_core');
        }

        return $this->_dynaCoreHelper;
    }

    /**
     * Triggers the SmartVal service to send a SMS-code to the given mobile number
     */
    public function sendSmsCodeAction()
    {
        $mobileNumber = $this->getRequest()->getParam('mobile_number');

        $result = $this->getSmartValHelper()->callSmartValidationService($mobileNumber);

        $response = [
            'error' => false,
        ];
        if(is_array($result)){
            // If error code = BSL-60058 it is not an error but a warning
            if($result['code'] != 'BSL-60058'){
                $response['error'] = true;
            }
            $errorMessage = $this->getErrorMessageForBslCode($result['code']);
            $response['message'] = $errorMessage ?: $result['message'];

            $errorTitle = $this->getErrorTitleForBslCode($result['code']);
            $response['title'] = $errorTitle;
        }
        return $this->jsonResponse($response);
    }

    public function validateSmsCodeAction()
    {
        $mobileNumber = $this->getRequest()->getParam('mobile_number');
        $smsCode = $this->getRequest()->getParam('sms_code', null);

        $result = $this->getSmartValHelper()->callSmartValidationService($mobileNumber, $smsCode);

        $response = [
            'error' => false,
        ];
        if(is_array($result)){
            $response['error'] = true;
            $errorMessage = $this->getErrorMessageForBslCode($result['code']);
            $response['message'] = $errorMessage ?: $result['message'];
        }
        return $this->jsonResponse($response);
    }

    /**
     * Gets the error message for the given BSL code.
     * @param $bslCode The BSL code.
     * @return null|string The error message or null if nothing found for the given code.
     */
    protected function getErrorMessageForBslCode($bslCode){
        $errorMessage = null;
        switch($bslCode){
            case 'BSL-60056':
                $errorMessage = $this->__('Try to reset this function via uFix');
                break;
            case 'BSL-60057':
                $errorMessage = $this->__('Validation code was to often not correct');
                break;
            case 'BSL-60058':
                $errorMessage = $this->__('Code is already validated. Continue.');
                break;
            case 'BSL-60059':
                $errorMessage = $this->__('Phonenumber not correctly entered');
                break;
            case 'BSL-60061':
                $errorMessage = $this->__('Validation code is not correct.');
                break;
        }
        return $errorMessage;
    }

    /**
     * Gets the error title for the given BSL code.
     * @param $bslCode The BSL code.
     * @return null|string The error title or null if nothing found for the given code.
     */
    protected function getErrorTitleForBslCode($bslCode){
        $errorTitle = null;
        switch($bslCode){
            case 'BSL-60056':
                $errorTitle = $this->__('Maximum amount of SMS-codes reached for today');
                break;
        }
        return $errorTitle;
    }

    /**
     * @param $data
     * @param int $statusCode
     * @throws InvalidArgumentException
     */
    protected function jsonResponse($data, $statusCode = 200)
    {
        $response = $this->getDynaCoreHelper()
            ->jsonResponse($data, $statusCode);

        $this->getResponse()
            ->setBody($response['body'])
            ->setHeader('Content-Type', 'application/json')
            ->setHttpResponseCode($response['status']);
    }
}
