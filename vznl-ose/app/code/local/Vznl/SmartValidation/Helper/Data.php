<?php
/**
 * Class Vznl_SmartValidation_Helper_Data
 */
class Vznl_SmartValidation_Helper_Data extends Mage_Core_Helper_Data
{
    /**
     * Get the store config values for the given name for SmartValidation
     * @param string $name The name of the config
     * @return mixed The value if any
     */
    public function getSmartValidationConfig($name = 'use_stubs')
    {
        return Mage::getStoreConfig('vodafone_service/smartvalidation/' . $name);
    }

    /**
     * Call the SmartValidation
     * @param string $mobileNumber The mobile number to send the SMS-code to
     * @param string|null If set it will try to validate the code if null it will request a SMS-code.
     * @return array|bool <true> if successful, an array with code and message if an error occured
     */
    public function callSmartValidationService($mobileNumber, $validationCode = null)
    {
        $customerSession = Mage::getSingleton('customer/session');
        $agent = $customerSession->getAgent();
        $dealerCode = $agent ? $agent->getDealerCode() : null;

        $loggerWriter = new Aleron75_Magemonolog_Model_Logwriter('services/SmartValidation/'.\Ramsey\Uuid\Uuid::uuid4().'.log');
        $smartValAdapter = Vznl_SmartValidation_Adapter_Factory::create($this->getSmartValidationConfig('wsdl'), $loggerWriter->getLogger(), $this->getDefaultOptions());
        $response = $smartValAdapter->send($mobileNumber, $dealerCode, $validationCode);

        if(!$response){
            return [
                'code' => '',
                'message' => $this->__('An exception occured when trying to contact the SmartValidation service')
            ];
        }

        $body = $response['Body'];
        if(array_key_exists('smartValResponse', $body) && $body['smartValResponse']['response'] == 'OK'){
            return true;
        }

        // Check if the body has an Fault child element
        if(array_key_exists('Fault', $body)){
            $errorData = $body['Fault']['detail']['error'];

            return [
                'code' => $errorData['errorCode'],
                'message' => $errorData['errorMessage']
            ];
        }

        return [
            'code' => '',
            'message' => 'Error message from the SmartValidation service'
        ];
    }

    /**
     * @return array
     */
    protected function getDefaultOptions()
    {
        return array(
            'wsdl' => $this->getSmartValidationConfig('wsdl'),
            'endpoint' => $this->getSmartValidationConfig('usage_url'),
            'timeout' => $this->getSmartValidationConfig('timeout'),
            'username' => $this->getSmartValidationConfig('username'),
            'password' => $this->getSmartValidationConfig('password'),
            'enduser' => $this->getSmartValidationConfig('enduser'),
            );
    }
}
