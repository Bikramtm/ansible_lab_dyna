<?php

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;

/**
 * Class Vznl_GetOrderStatus_Adapter_Peal_Adapter
 */
class Vznl_GetOrderStatus_Adapter_Peal_Adapter
{
    CONST name = "GetOrderStatus";
    /**
     * @var string
     */
    private $endpoint;

    /**
     * @var Client
     */
    private $client;

    /**
     * @var string
     */
    private $channel;

    /**
     * @var string
     */
    private $cty;

    /**
     * @var string
     */
    private $executionTimeId;

    /**
     * @var string
     */
    private $requestTimeId;

    /**
     * Adapter constructor.
     *
     * @param Client $client
     * @param string $endpoint
     * @param string $channel
     * @param string $cty
     */
    public function __construct(Client $client, string $endpoint, string $channel, string $cty)
    {
        $this->endpoint = $endpoint;
        $this->channel = $channel;
        $this->cty = $cty;
        $this->client = $client;
    }

    /**
     * @param $orderNumber
     * @return string|array
     * @throws Exception
     * @throws GuzzleException
     */
    public function call($orderNumber)
    {
        $this->executionTimeId = $this->getHelper()->initTimeMeasurement();

        if (is_null($orderNumber)) {
            return 'No order number provided to adapter';
        }
        $userName = $this->getHelper()->getLogin();
        $password = $this->getHelper()->getPassword();
        $verify = $this->getHelper()->getVerify();
        $proxy = $this->getHelper()->getProxy();
        $requestLog['url'] = $this->getUrl($orderNumber);

        $this->requestTimeId = $this->getHelper()->initTimeMeasurement();
        try {
            if ($userName != '' && $password != '') {
                $response = $this->client->request('GET', $this->getUrl($orderNumber), ['auth' => [$userName, $password], 'verify' => $verify, 'proxy' => $proxy]);
            } else {
                $response = $this->client->request('GET', $this->getUrl($orderNumber), ['verify' => $verify, 'proxy' => $proxy]);
            }

            $this->getHelper()->transferLog(
                json_encode($requestLog),
                json_encode(json_decode($response->getBody())),
                self::name,
                [
                    'execution' => $this->executionTimeId,
                    'request' => $this->requestTimeId
                ],
                'PEAL'
            );

            $source = json_decode($response->getBody(), true);
        } catch (ClientException|RequestException $exception) {
            $this->getHelper()->transferLog(
                $exception->getRequest(),
                ['msg' => $exception->getMessage(), 'code' => $exception->getCode()],
                self::name,
                [
                    'execution' => $this->executionTimeId,
                    'request' => $this->requestTimeId
                ],
                'PEAL'
            );

            $source = array (
                'error'=> true,
                'statusCode' => $exception->getCode(),
                'message' => $exception->getMessage(),
                'code' => $exception->getCode()
            );
        }
        return $source;
    }

    /**
     * @param $orderNumber
     * @return string
     */
    public function getUrl($orderNumber)
    {
        return $this->endpoint . $orderNumber .
            "?cty=" . $this->cty .
            "&chl=" . $this->channel;
    }

    /**
     * @return object Vznl_GetOrderStatus_Helper_Data
     */
    public function getHelper()
    {
        return Mage::helper('vznl_getorderstatus');
    }
}
