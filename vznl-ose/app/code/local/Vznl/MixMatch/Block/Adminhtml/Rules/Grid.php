<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * MixMatch attributes grid
 *
 * @category   Vznl
 * @package    Vznl_MixMatch
 */
class Vznl_MixMatch_Block_Adminhtml_Rules_Grid extends Dyna_MixMatch_Block_Adminhtml_Rules_Grid
{
    /**
     * @return $this|Mage_Adminhtml_Block_Catalog_Product_Attribute_Grid
     * @throws Exception
     */
    protected function _prepareColumns()
    {
        parent::_prepareColumns();

        $this->addColumn('entity_id',
            array(
                'header' => Mage::helper('dyna_mixmatch')->__('ID'),
                'width' => '10px',
                'type' => 'number',
                'index' => 'entity_id',
            ));
        $this->addColumn('source_sku',
            array(
                'header' => Mage::helper('dyna_mixmatch')->__('Source SKU'),
                'index' => 'source_sku',
            ));
        $this->addColumn('source_category',
            array(
                'header' => Mage::helper('dyna_mixmatch')->__('Source Category'),
                'index' => 'source_category',
            ));
        $this->addColumn('target_sku',
            array(
                'header' => Mage::helper('dyna_mixmatch')->__('Target SKU'),
                'index' => 'target_sku',
            ));

        $this->addColumn('device_subscription_sku',
            array(
                'header' => Mage::helper('dyna_mixmatch')->__('Device Subscription SKU'),
                'index' => 'device_subscription_sku',
            ));

        $this->addColumn('target_category',
            array(
                'header' => Mage::helper('dyna_mixmatch')->__('Target Category'),
                'index' => 'target_category',
            ));
        $this->addColumn('subscriber_segment',
            array(
                'header' => Mage::helper('dyna_mixmatch')->__('Subscriber segment'),
                'index' => 'subscriber_segment',
            ));
        $this->addColumn('red_sales_id',
            array(
                'header' => Mage::helper('dyna_mixmatch')->__('Red Sales Id'),
                'index' => 'red_sales_id',
            ));
        $this->addColumn('uploaded_at',
            array(
                'header' => Mage::helper('dyna_mixmatch')->__('Uploaded at'),
                'type' => 'datetime',
                'index' => 'uploaded_at',
            ));

        $store = $this->_getStore();
        $this->addColumn('price',
            array(
                'header' => Mage::helper('dyna_mixmatch')->__('OT Price'),
                'type' => 'price',
                'currency_code' => $store->getBaseCurrency()->getCode(),
                'index' => 'price',
                'renderer'  => 'dyna_mixmatch/adminhtml_rules_renderer_price',
            ));
        $this->addColumn('maf',
            array(
                'header' => Mage::helper('dyna_mixmatch')->__('Maf'),
                'type' => 'price',
                'currency_code' => $store->getBaseCurrency()->getCode(),
                'index' => 'maf',
                'renderer'  => 'dyna_mixmatch/adminhtml_rules_renderer_maf',
            ));
        $this->addColumn('effective_date',
            array(
                'header' => Mage::helper('dyna_mixmatch')->__('Effective date'),
                'type' => 'date',
                'index' => 'effective_date',
            ));
        $this->addColumn('expiration_date',
            array(
                'header' => Mage::helper('dyna_mixmatch')->__('Expiration date'),
                'type' => 'date',
                'index' => 'expiration_date',
            ));
        $this->addColumn('stream',
            array(
                'header' => Mage::helper('dyna_mixmatch')->__('Stream'),
                'index' => 'stream',
            ));
        $this->addColumn('website_id',
            array(
                'header' => Mage::helper('catalog')->__('Website'),
                'index' => 'website_id',
                'type' => 'options',
                'options' => Mage::getModel('core/website')->getCollection()->toOptionHash(),
            ));
        return $this;
    }

    /**
     * @param PHPExcel $objPHPExcel
     * @param $title
     * @param int $index
     * @param $websiteId
     * @return mixed
     */
    protected function _buildWorksheet($objPHPExcel, $title, $index, $websiteId)
    {
        if ($index == 0) {
            $ws = $objPHPExcel->getActiveSheet();
        } else {
            $ws = $objPHPExcel->createSheet();
        }
        $ws->setTitle($title);
        // Also retrieve disabled products(which have no price) for mixmatch
        Mage::register('mix_match_with_disabled_products', true);
        $devices = Mage::getModel('dyna_configurator/' . $title)->getProductsOfType(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE, $websiteId);
        $subscriptions = Mage::getModel('dyna_configurator/' . $title)->getProductsOfType(Vznl_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION, $websiteId);
        $deviceSubscriptions = [];
        $colIndex = 1;
        foreach ($subscriptions as $subscription) {
            $this->setCellValues($subscription, $deviceSubscriptions, $ws, $colIndex, $devices);
        }

        Mage::unregister('mix_match_with_disabled_products');

        return $ws;
    }
}
