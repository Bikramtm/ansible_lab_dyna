<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */

/** Class  Vznl_MixMatch_Model_Pricing */
class Vznl_MixMatch_Model_Pricing extends Dyna_MixMatch_Model_Pricing
{
    /**
     * Overwrite the price of the added device (item) if there is a subscription in the cart
     * @param $item
     * @return bool
     */
    public function getDevicePrice(Mage_Sales_Model_Quote_Item $item)
    {
        $sac = 0;
        $subscriptions = null;
        $deviceRC = null;
        $siblings = $item->getQuote()->getPackageItems($item->getPackageId());
        $package = Mage::getModel('package/package')
            ->getPackages(null, $item->getQuoteId(), $item->getPackageId())
            ->getFirstItem();

        foreach ($siblings as $sibling) {
            if ($sibling->getProduct()->is(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE_SUBSCRIPTION)) {
                $deviceRC = $sibling->getSku();
            }

            // check if there is a subscription
            if (!Mage::helper('dyna_catalog')->is(array(Vznl_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION),
                $sibling->getProduct())
            ) {
                continue;
            }

            $subscription = $sibling->getSku();
            $subscriptionModel = $sibling;

            // keep track of the first sac value
            if (!$sac && $sibling->getProduct()->hasSac()) {
                $sac = $sibling->getProduct()->getSac();
            }
        }

        if (isset($subscription) && $subscription !== null) {

            if ($deviceRC == null) {
                $deviceRC = $package->isRetention()
                    ? Mage::helper('vznl_checkout/aikido')->getDefaultRetentionSku()
                    : Mage::helper('vznl_checkout/aikido')->getDefaultAcquisitionSku();
            }

            if (isset($subscriptionModel) && !$subscriptionModel->getProduct()->getAikido()) {
                $deviceRC = null;
            }
            $websiteId = Mage::app()->getStore()->getWebsiteId();
            $collection = $this->getMixMatchCollection($item->getSku(), $subscription, $deviceRC, $websiteId);
            $mixmatch = $collection->getFirstItem();
        }

        if (isset($mixmatch) && $mixmatch->getPrice()) {
            $price = $mixmatch->getPrice();
        } else {
            if (!$sac || !$item->getProduct()->getPurchasePrice()) {
                $price = null;
            } else {
                $price = $this->calculatePrice($item->getProduct()->getPurchasePrice(), $sac);
            }
        }
        return $price;
    }

    /**
     * @param $devicePurchasePrice
     * @param $sac
     * @return float
     */
    public function calculatePrice($devicePurchasePrice, $sac, $taxRate = null)
    {
        $taxRate = $taxRate ?: Mage::helper('vznl_catalog')->getDefaultCountryTaxRate();
        return ((round(((((($devicePurchasePrice - $sac) * (1 + $taxRate)) - 4.99) * 2) / 10), 0) * 10) / 2) + 4.99;
    }

    public function checkMixMatchAvailable($previousResults, $products, $deviceCollection, $subsCollection, $deviceSubsCollection)
    {
        $device = false;
        $aikidoSubscription = false;
        $deviceSubscription = false;

        foreach ($products as $prod) {
            if ($prod->is(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE)) {
                $device = $prod;
            }

            if ($prod->is(Dyna_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION) && $prod->getAikido()) {
                $aikidoSubscription = $prod;
            }

            if ($prod->is(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE_SUBSCRIPTION)) {
                $deviceSubscription = $prod;
            }
        }

        $finalResult = $previousResults;
        $flippedDeviceSubsCollection =  array_flip($deviceSubsCollection);
        $flippedSubsCollection =  array_flip($subsCollection);
        $flippedDeviceCollection =  array_flip($deviceCollection);
        if($device && $aikidoSubscription)
        {
            $mixMatchResult = $this->getForColumn($aikidoSubscription, $device, false);
            $flippedMixMatchResult = array_flip($mixMatchResult);
            $newResult = [];
            foreach ($finalResult as $item) {
                if ((isset($flippedDeviceSubsCollection[$item]) && isset($flippedMixMatchResult[$item])) || !isset($flippedDeviceSubsCollection[$item])) {
                    $newResult[] = $item;
                }
            }
            $finalResult = $newResult;
        }

        if($device && $deviceSubscription)
        {
            $mixMatchResult = $this->getForColumn(false, $device, $deviceSubscription);
            $flippedMixMatchResult = array_flip($mixMatchResult);
            $newResult = [];
            foreach ($finalResult as $item) {
                if ((isset($flippedSubsCollection[$item]) && isset($flippedMixMatchResult[$item])) || !isset($flippedSubsCollection[$item])) {
                    $newResult[] = $item;
                }
            }
            $finalResult = $newResult;
        }

        if($aikidoSubscription && $deviceSubscription)
        {
            $mixMatchResult = $this->getForColumn($aikidoSubscription, false, $deviceSubscription);
            $flippedMixMatchResult = array_flip($mixMatchResult);
            $newResult = [];
            foreach ($finalResult as $item) {
                if ((isset($flippedDeviceCollection[$item]) && isset($flippedMixMatchResult[$item])) || !isset($flippedDeviceCollection[$item])) {
                    $newResult[] = $item;
                }
            }
            $finalResult = $newResult;
        }

        //Indirect scenario for deviceRC filtering
        if ($aikidoSubscription && !$device && Mage::helper('agent')->isIndirectStore()) {
            $subsContractPeriod = $aikidoSubscription->getData('identifier_commitment_months');
            /** @var Vznl_Checkout_Helper_Aikido $aikidoHelper */
            $aikidoHelper = Mage::helper('vznl_checkout/aikido');
            $allowedDeviceRcs = $aikidoHelper->getSpecialDeviceRcIds();
            $flippedAllowedDeviceRcs = array_flip($allowedDeviceRcs);
            $finalResultWithoutDeviceRC = array_diff($finalResult, $deviceSubsCollection);
            $newResult = [];

            $query = "SELECT cpei.entity_id,cpei.value FROM catalog_product_entity_int AS cpei INNER JOIN eav_attribute AS e ON e.`attribute_id`=cpei.`attribute_id` AND e.`attribute_code` LIKE 'identifier_commitment_months'";
            $icmProducts = Mage::getSingleton('core/resource')->getConnection('core_read')->fetchPairs($query);
            foreach ($finalResult as $item) {
                if ((isset($flippedDeviceSubsCollection[$item]) && isset($icmProducts[$item]) && $icmProducts[$item]  == $subsContractPeriod) || isset($flippedAllowedDeviceRcs[$item])) {
                    $newResult[] = $item;
                }
            }
            $finalResult = array_merge($finalResultWithoutDeviceRC, $newResult);
        }

        return array_values(array_unique($finalResult));
    }


    /**
     * @param Dyna_Catalog_Model_Product|bool $aikidoSubscription
     * @param Dyna_Catalog_Model_Product|bool $device
     * @param Dyna_Catalog_Model_Product|bool $deviceSubscription
     * @return array
     */
    public function getForColumn($aikidoSubscription = false, $device = false, $deviceSubscription = false)
    {
        /** @var Dyna_Core_Helper_Data $coreHelper */
        $coreHelper = Mage::helper('dyna_core');
        $websiteId = Mage::app()->getStore()->getWebsiteId();

        // Trim ids from SKUs in case the product has been deleted.
        $aikidoSubscriptionSku = $aikidoSubscription ? $coreHelper->trimSku($aikidoSubscription->getSku()) : 'false';
        $deviceSku = $device ? $coreHelper->trimSku($device->getSku()) : 'false';
        $deviceSubscriptionSku = $deviceSubscription ? $coreHelper->trimSku($deviceSubscription->getSku()) : 'false';
        $key = serialize(array(
            __METHOD__,
            $aikidoSubscriptionSku,
            $deviceSku,
            $deviceSubscriptionSku,
            $websiteId
        ));
        if ($result = unserialize($this->getCache()->load($key))) {
            return $result;
        } else {

            $mixmatchCollection = Mage::getModel('dyna_mixmatch/priceIndex')
                ->getCollection()
                ->addFieldToFilter('website_id', $websiteId);

            if ($deviceSubscription) {
                $mixmatchCollection->addFieldToFilter('device_subscription_sku', $deviceSubscriptionSku);
            } else {
                $columnName = 'device_subscription_sku';
            }

            if ($aikidoSubscription) {
                $mixmatchCollection->addFieldToFilter('source_sku', $aikidoSubscriptionSku);
            } else {
                $columnName = 'source_sku';
            }

            if ($device) {
                $mixmatchCollection->addFieldToFilter('target_sku', $deviceSku);
            } else {
                $columnName = 'target_sku';
            }

            $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
            $mixmatchCollection->getSelect()
                ->joinLeft(array("wl" => Mage::getSingleton('core/resource')->getTableName('catalog/product')),
                    "main_table." . $columnName . " = wl.sku")
                ->reset(Zend_Db_Select::COLUMNS)
                ->columns('wl.entity_id')
                ->distinct(true);

            $result = array_unique($connection->fetchCol($mixmatchCollection->getSelect()));

            $this->getCache()->save(serialize($result), $key, array(Vznl_Configurator_Model_Cache::PRODUCT_TAG),
                $this->getCache()->getTtl());
            return $result;
        }
    }

    /**
     * @param string $targetSku
     * @param string $sourceSku
     * @param null $loanSku
     * @param int $websiteId
     * @return Omnius_MixMatch_Model_Resource_Price_Collection|Varien_Data_Collection
     */
    public function getMixMatchCollection($targetSku, $sourceSku, $loanSku = null, $websiteId = null)
    {
        if ($sourceSku == $targetSku) {
            return null;
        }

        if ($websiteId == null) {
            $websiteId = Mage::app()->getStore()->getWebsiteId();
        }

        if ($this->salesId === false) {
            /** @var Dyna_Checkout_Model_Sales_Quote $currentQuote */
            $currentQuote = Mage::getSingleton('checkout/cart')->getQuote();
            $redSalesId = $currentQuote->getSalesId();
        } else {
            $redSalesId = $this->salesId;
        }

        $key = serialize([
            __METHOD__,
            $targetSku,
            $sourceSku,
            $websiteId,
            $loanSku,
        ]);

        if ($result = unserialize($this->getCache()->load($key))) {
            return $result;
        } else {
            /** @var Omnius_MixMatch_Model_Resource_Price_Collection $collection */
            $collection = Mage::getModel('dyna_mixmatch/priceIndex')->getCollection()
                ->addFieldToSelect(['source_sku', 'target_sku', 'device_subscription_sku',  'price', 'maf'])
                ->addFieldToFilter('website_id', array('eq' => $websiteId))
                ->addFieldToFilter('effective_date', array('to' => date("Y-m-d H:i:s"), 'date' => true))
                ->addFieldToFilter('expiration_date', array('from' => date("Y-m-d H:i:s"), 'date' => true))
                ->addFieldToFilter('subscriber_segment', array('null' => true));

            if (!empty($redSalesId)) {
                $collection->addFieldToFilter(
                    array('red_sales_id', 'red_sales_id'),
                    array(array('eq' => $redSalesId), array('null' => true))
                );
            } else {
                $collection->addFieldToFilter('red_sales_id', array('null' => true));
            }

            if ($sourceSku !== null) {
                $collection->addFieldToFilter('source_sku', $sourceSku);
            }

            if ($targetSku !== null) {
                $collection->addFieldToFilter('target_sku', $targetSku);
            }

            if ($loanSku !== null) {
                $collection->addFieldToFilter('device_subscription_sku', $loanSku);
            }

            $collection->getSelect()->order("priority DESC");

            $result = new Varien_Data_Collection();
            foreach ($collection->getItems() as $item) {
                $result->addItem($item);
            }

            $result->setOrder('price', Zend_Db_Select::SQL_ASC);

            $this->getCache()->save(serialize($result), $key, [Dyna_Cache_Model_Cache::PRODUCT_TAG],
                $this->getCache()->getTtl());

            return $result;
        }
    }

    /**
     * @return Dyna_Configurator_Model_Cache
     */
    protected function getCache()
    {
        if ( ! $this->_cache) {
            $this->_cache = Mage::getSingleton('vznl_configurator/cache');
        }
        return $this->_cache;
    }

}
