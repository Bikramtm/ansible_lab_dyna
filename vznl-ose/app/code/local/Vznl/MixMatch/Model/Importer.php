<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */

/**
 * Class Vznl_MixMatch_Model_Importer
 */
class Vznl_MixMatch_Model_Importer extends Dyna_MixMatch_Model_Importer
{

    public function __construct()
    {
        parent::__construct();
        /** @var Vznl_Import_Helper_Data _helper */
        $this->_helper = Mage::helper('vznl_import');
    }

    /**
     * @param string $path
     * @param boolen $isCLI
     * @param array $websiteIds
     * @return bool
     */
    public function importFile($path, $isCLI = false, $websiteIds = array())
    {
        $fileExtension = pathinfo($path, PATHINFO_EXTENSION);
        if ($fileExtension == "csv") {
            $startDate = gmdate('Y-m-d h:i:s');
            $userName = null;
            $this->importCsvFile($path, $startDate, $userName);
        } else {
            $contents = Mage::helper("vznl_data/data")->getFileContents($path);

            //BOM deleting for UTF files
            if (ord($contents[0]) == 0xEF &&
                ord($contents[1]) == 0xBB &&
                ord($contents[2]) == 0xBF) {
                $contents = substr($contents, 3);
                $io = new Varien_Io_File();
                $io->streamOpen($path);
                $io->streamWrite($contents);
                $io->streamClose();
            }
            unset($contents);
            $startDate = gmdate('Y-m-d h:i:s');
            if ($isCLI && Mage::getSingleton('admin/session')->getUser()) {
                $userName = Mage::getSingleton('admin/session')->getUser()->getUsername();
            }
            $objReader = PHPExcel_IOFactory::createReaderForFile($path);
            $objReader->setReadDataOnly(true);
            $objPHPExcel = $objReader->load($path);
            $worksheets = $objPHPExcel->getWorksheetIterator();
            foreach ($websiteIds as $websiteId) {
                //delete all entries from the database
                /** @var Dyna_ProductMatchRule_Model_Indexer_Productmatch $indexer */
                $indexer = Mage::getSingleton('productmatchrule/indexer_productmatch');
                $tableName = Mage::getResourceSingleton('vznl_mixmatch/price')->getMainTable();
                $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
                $connection->delete($tableName, array('website_id = ?' => (int) $websiteId));
                $indexer::removeAllMixMatchRules($websiteId); //remove all mixmatch rules for a website
                //import data

                $counter = 0;
                foreach ($worksheets as $ws) {
                    $max_columns = PHPExcel_Cell::columnIndexFromString($ws->getHighestColumn());
                    $max_rows = (int) $ws->getHighestRow();

                    $rows = $ws->getRowIterator();
                    $subscriptionsHeader = $rows->current();
                    $rows->next();
                    $deviceSubscriptionsHeader = $rows->current();
                    $rows->next();
                    do {
                        $row = $rows->current();
                        $cells = $row->getCellIterator();
                        $device = $cells->current();

                        // if no sku on devices row, proceed to next row
                        if (!trim($device->getValue())) {
                            $rIndex = $row->getRowIndex();
                            $rows->next();
                            $this->_log('Skipping row' . $counter . ' because there is no device SKU on this row');
                            continue;
                        }

                        $cells->next();

                        // init header to column 1 (skip first empty column)
                        $subscriptions = $subscriptionsHeader->getCellIterator();
                        $deviceSubscriptions = $deviceSubscriptionsHeader->getCellIterator();
                        $subscriptions->next();
                        $deviceSubscriptions->next();

                        /** @var Mage_Core_Model_Resource_Transaction $transaction */
                        $transaction = Mage::getResourceModel('core/transaction');

                        do {
                            $cell = $cells->current();
                            $subscription = $subscriptions->current();
                            $deviceSubscription = $deviceSubscriptions->current();

                            // if no sku on subscription column, proceed to next column
                            if (!trim($subscription->getValue())) {
                                $column = PHPExcel_Cell::columnIndexFromString($subscription->getColumn()) + 1;
                                $cells->next();
                                $subscriptions->next();
                                $deviceSubscriptions->next();
                                continue;
                            }

                            $price = (
                                is_numeric($cell->getValue()) &&
                                (float) $cell->getValue() >= 0) ?
                                (float) $cell->getValue() : null;
                            if ($price !== null) {
                                try {
                                    if ($this->getProductIdBySku($device->getValue()) &&
                                        $this->getProductIdBySku($subscription->getValue())) {
                                        if (trim($deviceSubscription->getValue())) {
                                            if ($this->getProductIdBySku($deviceSubscription->getValue())) {
                                                $transaction->addObject(Mage::getModel('vznl_mixmatch/price')
                                                    ->setDeviceSku($device->getValue())
                                                    ->setSubscriptionSku($subscription->getValue())
                                                    ->setDeviceSubscriptionSku($deviceSubscription->getValue())
                                                    ->setPrice($price)
                                                    ->setUploadedAt($startDate)
                                                    ->setUploadedBy($userName)
                                                    ->setWebsiteId($websiteId));
                                                $this->_log('Setting device ' . $device->getValue() . ' with subscription ' . $subscription->getValue() . ' to price '
                                                    . $price . ' for website ' . $websiteId . ' using row ' . $counter);
                                                $this->_log(
                                                    sprintf(
                                                        'Setting device %s with subscription %s and subscription device %s to price %s for website %s using row %d',
                                                        $device->getValue(),
                                                        $subscription->getValue(),
                                                        $deviceSubscription->getValue(),
                                                        $price,
                                                        $websiteId,
                                                        $counter
                                                    )
                                                );
                                            }
                                        } else {
                                            $transaction->addObject(Mage::getModel('vznl_mixmatch/price')
                                                ->setDeviceSku($device->getValue())
                                                ->setSubscriptionSku($subscription->getValue())
                                                ->setPrice($price)
                                                ->setUploadedAt($startDate)
                                                ->setUploadedBy($userName)
                                                ->setWebsiteId($websiteId));
                                            $this->_log('Setting device ' . $device->getValue() .
                                                ' with subscription ' . $subscription->getValue() .
                                                ' to price '. $price .
                                                ' for website ' . $websiteId .
                                                ' using row ' . $counter);
                                        }
                                    }
                                } catch (Exception $e) {
                                    Mage::getSingleton('core/logger')->logException($e);
                                }
                            }

                            $column = PHPExcel_Cell::columnIndexFromString($subscription->getColumn());

                            $cells->next();
                            $subscriptions->next();
                            $deviceSubscriptions->next();
                        } while ($column < $max_columns);

                        try {
                            $transaction->save();
                        } catch (Exception $e) {
                            Mage::getSingleton('core/logger')->logException($e);
                        }
                        unset($transaction);

                        $rIndex = $row->getRowIndex();
                        $rows->next();
                    } while ($rIndex < $max_rows);
                    $counter++;
                }
                $this->setImportedFileName($path, $websiteId);
            }
            $objPHPExcel->disconnectWorksheets();
            $objPHPExcel->garbageCollect();
        }
    }

    public function getFileLogName()
    {
        return strtolower($this->stack) . '_' . $this->_logFileName . '.' . $this->_logFileExtension;
    }

    /**
     * Method to import mixmatch rules from csv file
     * @param string $path
     * @param date $startDate
     * @param string $userName
     * @return bool
     */
    protected function importCsvFile($path, $startDate, $userName)
    {
        try {
            /**
             * @var $objReader PHPExcel_Reader_Abstract
             * @var $objPHPExcel PHPExcel
             */
            $csvValues = $this->getCsvNameValues($path);
            if (!$csvValues) {
                return false;
            }

            $this->importFileForWebsite($path, $csvValues, $startDate, $userName);

        } catch (Exception $e) {
            $this->_logError('[ERROR] Exception - Failed importing: ' . $e->getMessage());
            //fwrite(STDERR, '[ERROR] Exception - Failed importing: ' . $e->getMessage());

            $return = false;
        }

        return isset($return) ? $return : true;
    }

    /**
     * Perform excel parsing data and insert in database
     * @param string $path
     * @param array $excelValues
     * @param date $startDate
     * @param string $userName
     * @throws PHPExcel_Exception
     * @throws bool
     */
    protected function importFileForWebsite($path, $excelValues, $startDate, $userName)
    {
        $counter = 1;
        $deviceSubscription = new Varien_Object();
        $websiteId = '';
        $websites = array();
        $cachedWebsitesIds = [];

        foreach ($excelValues as $valuesArray) {
            // skip empty row
            if (!array_filter($valuesArray)) {
                continue;
            }

            $valuesArray['stack'] = $this->stack;
            $ws = new Varien_Object();

            $ws->setData($valuesArray);
            /** @var Mage_Core_Model_Resource_Transaction $transaction */
            $transaction = Mage::getResourceModel('core/transaction');

            $sourceSku = trim($ws->getSourceSku());
            $targetSku = trim($ws->getTargetSku());
            $targetCategory = trim($ws->getTargetCategory());
            $sourceCategory = trim($ws->getSourceCategory());
            $subscriberSegment = $ws->getSubscriberSegment();
            $deviceSubscription->setValue($ws->getDeviceSubscriptionSku());

            if ($subscriberSegment === Dyna_MixMatch_Model_SubscriberSegment::SUBSCRIBER_SEGMENT_VALUE_ALL) {
                $subscriberSegment = implode(Mage::getSingleton('dyna_mixmatch/subscriberSegment')->getAllValues(), ',');
            } else if ($subscriberSegment == Dyna_MixMatch_Model_SubscriberSegment::SUBSCRIBER_SEGMENT_VALUE_EMPTY) {
                $subscriberSegment = '';
            }

            if ($ws->getEffectiveDate()) {
                $effectiveDate = PHPExcel_Style_NumberFormat::toFormattedString($ws->getEffectiveDate(), PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2);
                $effectiveDate = date("Y-m-d h:i:s", strtotime($effectiveDate));

            } else {
                $effectiveDate = "";
            }

            if ($ws->getExpirationDate()) {
                $expirationDate = PHPExcel_Style_NumberFormat::toFormattedString($ws->getExpirationDate(), PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2);
                $expirationDate = date("Y-m-d", strtotime($expirationDate));
            } else {
                $expirationDate = "";
            }
            $stream = $ws->getStream();
            $redSalesId = $ws->getRedSalesId();
            $priority = $ws->getPriority();
            $maf = str_replace(',', '.', trim($ws->getMaf()));
            $maf = is_numeric($maf) ? $maf : null;
            $price = str_replace(',', '.', trim($ws->getPrice()));
            $price = is_numeric($price) ? $price : null;
            // if no price and no maf entries skip row
            if (is_null($maf) && is_null($price)) {
                $this->_logError('[ERROR] Skipping row ' . $counter . ' because there is both Price and MAF are NULL on this row');
                $this->_skippedFileRows++;
                continue;
            }

            // if no sku on subscription column, proceed to next column
            if (!$sourceSku && !$sourceCategory) {
                $this->_logError(
                    '[ERROR] Skipping row ' . $counter .
                    ' because there is no source SKU and no source Category on this row');
                $this->_skippedFileRows++;
                continue;
            }
            // if no sku on devices row, proceed to next row
            if (!$targetSku && !$targetCategory) {
                $this->_logError(
                    '[ERROR] Skipping row ' . $counter .
                    ' because there is no target SKU and no target Category on this row');
                $this->_skippedFileRows++;
                continue;
            }
            //If target sku is not valid
            if (!empty($targetSku) && empty($targetCategory) && !$this->getProductIdBySku($targetSku)) {
                $this->_logError(sprintf('[ERROR] Target product is not found: %s', $targetSku));
                $this->_skippedFileRows++;
                continue;
            }
            //if source sku is not valid
            if (!empty($sourceSku) && empty($sourceCategory) && !$this->getProductIdBySku($sourceSku)) {
                $this->_logError(sprintf('[ERROR] Source product is not found: %s', $sourceSku));
                $this->_skippedFileRows++;
                continue;
            }
            //if devicesubscription sku is not valid
            if (!empty($deviceSubscription->getValue()) && !$this->getProductIdBySku($deviceSubscription->getValue())) {
                $this->_logError(sprintf('[ERROR] Device Subscription product is not found: %s', $deviceSubscription->getValue()));
                $this->_skippedFileRows++;
                continue;
            }

            $websites = array_map("trim", explode(',', $ws->getChannel()));
            $flippedWebsites = array_flip($websites);
            // get website id based on name
            $websiteIds = array();
            foreach (Mage::app()->getWebsites() as $website) {
                $websiteCode = $website->getCode();
                if (isset($flippedWebsites[$websiteCode])) {
                    $websiteIds[] = $website->getWebsiteId();
                }
            }

            foreach ($websiteIds as $websiteId) {
                $this->clearMixmatchWithNoStack($websiteId, $targetSku, $sourceSku, $deviceSubscription, $targetCategory, $sourceCategory);
                if (!$websiteId && $ws['channel']) {
                    $channel = 0;
                    if (!empty($cachedWebsitesIds[$ws['channel']])) {
                        $channel = $cachedWebsitesIds[$ws['channel']]['website_id'];

                        /** Clear all website data at first load for the current stream and mark it as emptied */
                        if (empty($cachedWebsitesIds[$ws['channel']][$ws['stream']])) {
                            $cachedWebsitesIds[$ws['channel']][$ws['stream']] = $this->clearMixmatchData($channel, $ws['stream']);
                        }
                    } else {
                        /** Load website and cache it's id */
                        $channel = Mage::getModel('core/website')->load(strtolower($ws['channel']), 'code')->getId();
                        $cachedWebsitesIds[$ws['channel']]['website_id'] = $channel;

                        /** Clear all website data at first load for the current stream and mark it as emptied */
                        if (empty($cachedWebsitesIds[$ws['channel']][$ws['stream']])) {
                            $cachedWebsitesIds[$ws['channel']][$ws['stream']] = $this->clearMixmatchData($channel, $ws['stream']);
                        }
                    }

                    if ($channel) {
                        $this->importProduct(
                            $targetSku,
                            $sourceSku,
                            $targetCategory,
                            $sourceCategory,
                            $subscriberSegment,
                            $redSalesId,
                            $priority,
                            $deviceSubscription,
                            $transaction,
                            $price,
                            $maf,
                            $startDate,
                            $userName,
                            $websiteId,
                            $counter,
                            $effectiveDate,
                            $expirationDate,
                            $stream
                        );
                    } else {
                        $this->_logError('[ERROR] Skipping row' . $counter . ' because the channel column contains an invalid website code');
                        $this->_skippedFileRows++;
                    }
                } else {
                    /** Clear all website data at first load for the current stream and mark it as emptied */
                    if (empty($cachedWebsitesIds[$websiteId][$ws['stream']])) {
                        $cachedWebsitesIds[$websiteId][$ws['stream']] = $this->clearMixmatchData($websiteId, $ws['stream']);
                    }
                    $this->importProduct(
                        $targetSku,
                        $sourceSku,
                        $targetCategory,
                        $sourceCategory,
                        $subscriberSegment,
                        $redSalesId,
                        $priority,
                        $deviceSubscription,
                        $transaction,
                        $price,
                        $maf,
                        $startDate,
                        $userName,
                        $websiteId,
                        $counter,
                        $effectiveDate,
                        $expirationDate,
                        $stream
                    );
                }
            }

            try {
                $transaction->save();
            } catch (Exception $e) {
                $this->_logError('[ERROR] Skipping row no ' . $counter . ' since similar data is found in the system.');
                $counter++;
                $this->_skippedFileRows++;
                continue;
            }
            unset($transaction);

            $counter++;
        }

        $this->setImportedFileName($path, $websiteId);
    }

    protected function importProduct(
        $targetSku,
        $sourceSku,
        $targetCategory,
        $sourceCategory,
        $subscriberSegment,
        $redSalesId,
        $priority,
        $deviceSubscription,
        $transaction,
        $price,
        $maf,
        $startDate,
        $userName,
        $websiteId,
        $counter,
        $effectiveDate,
        $expirationDate,
        $stream
    )
    {
        try {

            $transaction->addObject(Mage::getModel('omnius_mixmatch/price')
                ->setTargetSku($targetSku)
                ->setSourceSku($sourceSku)
                ->setPriority($priority)
                ->setSubscriberSegment($subscriberSegment)
                ->setDeviceSubscriptionSku($deviceSubscription->getValue())
                ->setTargetCategory($targetCategory)
                ->setSourceCategory($sourceCategory)
                ->setPrice($price)
                ->setMaf($maf)
                ->setRedSalesId($redSalesId)
                ->setUploadedAt($startDate)
                ->setUploadedBy($userName)
                ->setEffectiveDate($effectiveDate)
                ->setExpirationDate($expirationDate)
                ->setStack($this->stack)
                ->setWebsiteId($websiteId)
                ->setStream($stream));
            $this->_log(
                sprintf(
                    'Setting device %s 
                        with subscription %s 
                        and subscription device %s 
                        to price %s for website %s 
                        using row %d',
                    $targetSku,
                    $sourceSku,
                    $deviceSubscription->getValue(),
                    $price,
                    $websiteId,
                    $counter
                ), Zend_Log::INFO
            );
        } catch (Exception $e) {
            Mage::getSingleton('core/logger')->logException($e);
        }
    }

    public function clearMixmatchWithNoStack($websiteId, $targetSku, $sourceSku, $deviceSubscription, $targetCategory, $souceCategory)
    {
        $coreConnection = Mage::getSingleton('core/resource');
        $connection = $coreConnection->getConnection('core_write');
        $resTableName = Mage::getResourceSingleton('omnius_mixmatch/price');
        $tableName = $resTableName->getMainTable();
        if ($deviceSubscription->getValue() == "") {
            if (($connection->delete($tableName,
                    [
                        'website_id = ?' => (int)$websiteId,
                        'target_sku = ?' => $targetSku,
                        'source_sku = ?' => $sourceSku
                    ])
                ) || (($connection->delete($tableName,
                    [
                        'website_id = ?' => (int)$websiteId,
                        'target_category = ?' => $targetCategory,
                        'source_category = ?' => $souceCategory
                    ])))
            )
            {
                return "Rules removed";
            }
        } else {
            if (($connection->delete($tableName,
                    [
                        'website_id = ?' => (int)$websiteId,
                        'target_sku = ?' => $targetSku,
                        'source_sku = ?' => $sourceSku,
                        'device_subscription_sku = ?' => $deviceSubscription->getValue() ? $deviceSubscription->getValue() : null
                    ])
                ) || (($connection->delete($tableName,
                    [
                        'website_id = ?' => (int)$websiteId,
                        'target_category = ?' => $targetCategory,
                        'source_category = ?' => $souceCategory,
                        'device_subscription_sku = ?' => $deviceSubscription->getValue() ? $deviceSubscription->getValue() : null
                    ])))
            )
            {
                return "Rules removed";
            }
        }
    }
}