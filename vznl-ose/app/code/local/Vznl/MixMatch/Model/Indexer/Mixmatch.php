<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */

/**
 * Class Vznl_MixMatch_Model_Indexer_Mixmatch
 */
class Vznl_MixMatch_Model_Indexer_Mixmatch extends Dyna_MixMatch_Model_Indexer_Mixmatch
{
    protected $whitelistTable;

    /**
     * Initiate indexer
     */
    public function __construct()
    {
        parent::__construct();

        $this->table = (string) Mage::getResourceSingleton('dyna_mixmatch/priceIndex')->getMainTable();
        $this->whitelistTable = (string) Mage::getResourceSingleton('productmatchrule/matchRule')->getMainTable();
        $this->connection = Mage::getSingleton('core/resource')->getConnection('core_write');

        /** @var Dyna_Core_Helper_System $coreHelper */
        $systemHelper = Mage::helper('dyna_core/System');
        $this->flushOnMemory = $systemHelper->getMemoryLimit();
        $this->maxPacketsLength = $systemHelper->getMysqlMaxAllowedPacketSize();

        $this->_init('omnius_mixmatch/indexer_mixmatch');
    }

    /**
     * Builds INSERT statements and executes them
     * Iterates over the items withing the $matches array
     * and builds the INSERT statements, always checking
     * if we approach the MySQL max_allowed_packet limit.
     * If we approach the limit too much, we execute the current
     * SQL statement and start building the statements for the remaining items
     */
    protected function insertMultiple()
    {
        $values = $allProcessContextsString = '';
        // get process context set id (get set of all sale types)
        $processContextModel = Mage::getModel('dyna_catalog/processContext');
        $allProcessContexts = $processContextModel->getAllProcessContextCodeIdPairs();
        foreach ($allProcessContexts as $processContextId) {
            $allProcessContextsString .= $processContextId . ',';
        }
        $allProcessContextsString = rtrim($allProcessContextsString,',');
        // to set it on each allow rule
        $processContextSetId = $processContextModel->getProcessContextSetId($allProcessContextsString);

        foreach ($this->items as $key => &$combination) {
            $values .= vsprintf('(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s),', $combination);
            if (strlen($values) >= $this->maxPacketsLength) {
                $this->persistData($values);
                $values = '';
            }
            // add allow rule for each mixmatch
            $this->addMixMatchAllowRule($combination, $processContextSetId);

            unset($this->items[$key]);
        }
        unset($combination);

        if ($values) {
            $this->persistData($values);
            unset($values);
        }
    }


    /**
     * Add allow rules on mixmatch indexer
     * @param $mixMatchEntry
     * @param $processContextSetId
     */
    protected function addMixMatchAllowRule($mixMatchEntry, $processContextSetId)
    {
        $sourceProductId = Mage::getResourceSingleton('catalog/product')->getIdBySku(trim($mixMatchEntry[0], '"'));
        $targetProductId = Mage::getResourceSingleton('catalog/product')->getIdBySku(trim($mixMatchEntry[2], '"'));

        if ($sourceProductId && $targetProductId) {
            $productResource = Mage::getResourceModel('catalog/product');
            // get all package types for each product as entity_id from catalog_package_types
            $sourceProductPackageTypes = $productResource->getPackageTypesById($sourceProductId);
            $targetProductPackageTypes = $productResource->getPackageTypesById($targetProductId);

            if (!empty($sourceProductPackageTypes) || !empty($targetProductPackageTypes)) {
                $packageTypes = array_merge((!empty($sourceProductPackageTypes) ? $sourceProductPackageTypes : array()), (!empty($targetProductPackageTypes) ? $targetProductPackageTypes : array()));

                $websiteId = trim($mixMatchEntry[5], '"');
                $ruleId = trim($mixMatchEntry[10], '"');
                $deviceRCid = (gettype($mixMatchEntry[11]) == 'string') ? Mage::getResourceSingleton('catalog/product')->getIdBySku(trim($mixMatchEntry[11], '"')) : false;

                foreach (array_unique($packageTypes) as $packageType) {
                    // if deviceRC is existing (not Zend_Db_Expr('(NULL)'))
                    if ($deviceRCid) {
                        $allowedRulesValues = array(
                            sprintf('("%s","%s","%s","%s","%s","%s","%s", %d),', $websiteId, $packageType, $sourceProductId, $targetProductId, 0, $processContextSetId, $ruleId, 1),
                            sprintf('("%s","%s","%s","%s","%s","%s","%s", %d),', $websiteId, $packageType, $deviceRCid, $sourceProductId, 0, $processContextSetId, $ruleId, 1),
                            sprintf('("%s","%s","%s","%s","%s","%s","%s", %d),', $websiteId, $packageType, $deviceRCid, $targetProductId, 0, $processContextSetId, $ruleId, 1)
                        );
                    } else {
                        $allowedRulesValues = array(
                            sprintf('("%s","%s","%s","%s","%s","%s","%s", %d),', $websiteId, $packageType, $sourceProductId, $targetProductId, 0, $processContextSetId, $ruleId, 1)
                        );
                    }

                    foreach ($allowedRulesValues as $allowedRuleValues) {
                        $sql = sprintf(
                            'INSERT INTO `%s` (`website_id`,`package_type`,`source_product_id`,`target_product_id`,`override_initial_selectable`,`product_match_whitelist_index_process_context_set_id`,`rule_id`,`source_collection`) VALUES %s ON DUPLICATE KEY UPDATE `website_id`=VALUES(`website_id`);' . PHP_EOL,
                            $this->whitelistTable,
                            trim($allowedRuleValues, ',')
                        );
                        $this->connection->query($sql);
                    }
                }
                unset($sql, $allowedRulesValues);
            }
        }

        unset($sourceProductId, $targetProductId);
    }

    /**
     * @param $mixMatch Mage_Core_Model_Abstract
     * @return array
     */
    protected function getEntryData($mixMatch):array
    {
        // Ensure a dates are indexed
        if (!$mixMatch->getEffectiveDate()) {
            $dateTime = new DateTime('now -1 day');
            $mixMatch->setEffectiveDate($dateTime->format('Y-m-d 00:00:00'));
        }
        if (!$mixMatch->getExpirationDate()) {
            $dateTime = new DateTime('now +10years');
            $mixMatch->setExpirationDate($dateTime->format('Y-m-d 00:00:00'));
        }
        unset($dateTime);

        return [
            $mixMatch->getDataForSql('source_sku'),
            $mixMatch->getDataForSql('priority'),
            $mixMatch->getDataForSql('target_sku'),
            $mixMatch->getDataForSql('price'),
            $mixMatch->getDataForSql('maf'),
            $mixMatch->getDataForSql('website_id'),
            $mixMatch->getDataForSql('red_sales_id'),
            $mixMatch->getDataForSql('subscriber_segment'),
            $mixMatch->getDataForSql('effective_date'),
            $mixMatch->getDataForSql('expiration_date'),
            $mixMatch->getDataForSql('entity_id'),
            $mixMatch->getDataForSql('device_subscription_sku')
        ];
    }

    /**
     * @param $values
     */
    protected function persistData($values)
    {
        $sql = sprintf(
            'INSERT INTO `%s` (`source_sku`,`priority`,`target_sku`,`price`,`maf`,`website_id`,`red_sales_id`,`subscriber_segment`,`effective_date`,`expiration_date`, `rule_id`, `device_subscription_sku`) VALUES %s ON DUPLICATE KEY UPDATE `maf`=VALUES(`maf`), `price`=VALUES(`price`)' . PHP_EOL,
            $this->table,
            trim($values, ',')
        );

        $this->connection->query($sql);
        unset($sql);
    }
}