<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */

require_once Mage::getModuleDir('controllers', 'Dyna_MixMatch') . DS . 'Adminhtml' . DS . 'RulesController.php';

/**
 * Class Vznl_MixMatch_Adminhtml_RulesController
 */
class Vznl_MixMatch_Adminhtml_RulesController extends Dyna_MixMatch_Adminhtml_RulesController
{
    public function indexAction()
    {
        $this->_title($this->__("MixMatch"));
        $this->_title($this->__("Manage MixMatch"));

        $this->_initAction()
            ->_addContent($this->getLayout()->createBlock('vznl_mixmatch/adminhtml_rules'))
            ->renderLayout();
    }

    public function uploadAction()
    {
        $validPostRequest = false;
        if ($postData = $this->getRequest()->getPost()) {
            if (isset($_FILES['file']['tmp_name'])) {

                if ($file = $_FILES['file']['tmp_name']) {
                    $validPostRequest = true;
                    $uploader = new Mage_Core_Model_File_Uploader('file');
                    $uploader->setAllowedExtensions(array('xlsx'));
                    $path = Mage::app()->getConfig()->getTempVarDir() . '/import/';
                    $uploader->save($path);
                    if ($uploadFile = $uploader->getUploadedFileName()) {
                        $newFilename = 'import-' . date('YmdHis') . '_' . $uploadFile;
                        rename($path . $uploadFile, $path . $newFilename);
                    }
                }
                if (isset($newFilename) && $newFilename) {
                    Mage::getModel('vznl_mixmatch/importer')->importFile($path . $newFilename, false, $postData['website_id']);
                    $this->_redirect('*/*');
                }
            }
        }

        if (!$validPostRequest) {
            Mage::getSingleton('adminhtml/session')->addError(
                $this->__('Invalid POST data (please check post_max_size and upload_max_filesize settings in your php.ini file).')
            );
            $this->_redirect('*/*');
        }
    }

    /**
     *  Export order grid to Excel XML format
     */
    public function exportAction()
    {
        /** @var Vznl_MixMatch_Block_Adminhtml_Rules_Grid $grid */
        $grid = $this->getLayout()->createBlock('vznl_mixmatch/adminhtml_rules_grid');

        // check if website filter has been set
        $filters_name = $grid->getVarNameFilter();
        if ($filter = $this->getRequest()->getParam($filters_name)) {
            $filter = Mage::helper('adminhtml')->prepareFilterString($filter);
        }

        if (!isset($filter) || !is_array($filter) || !isset($filter['website_id']) || !$filter['website_id']) {
            Mage::getSingleton("adminhtml/session")->addError(Mage::helper("dyna_mixmatch")->__("Filter by Website first."));

            return $this->_redirect('*/*');
        }

        $fileName = 'MixMatch-W' . $filter['website_id'] . '-' . date("Ymdhis") . '.xlsx';
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName, $filter['website_id']));
    }


    /**
     *  Get the latest uploaded Mixmatch file
     */
    public function lastexportAction()
    {
        /** @var Vznl_MixMatch_Block_Adminhtml_Rules_Grid $grid */
        $grid = $this->getLayout()->createBlock('vznl_mixmatch/adminhtml_rules_grid');

        // check if website filter has been set
        $filters_name = $grid->getVarNameFilter();
        if ($filter = $this->getRequest()->getParam($filters_name)) {
            $filter = Mage::helper('adminhtml')->prepareFilterString($filter);
        }

        if (!isset($filter) || !is_array($filter) || !isset($filter['website_id']) || !$filter['website_id']) {
            Mage::getSingleton("adminhtml/session")->addError(Mage::helper("dyna_mixmatch")->__("Filter by Website first."));

            return $this->_redirect('*/*');
        }

        $fileName = Mage::helper('dyna_mixmatch')->getLastImportedFileName($filter['website_id']);
        if (!$fileName || !realpath($fileName)) {
            Mage::getSingleton("adminhtml/session")->addError(Mage::helper("dyna_mixmatch")->__("File not found or empty."));

            return $this->_redirect('*/*');
        }
        $content = Mage::helper("vznl_data/data")->getFileContents($fileName);
        $this->_prepareDownloadResponse($fileName, $content);
    }
}
