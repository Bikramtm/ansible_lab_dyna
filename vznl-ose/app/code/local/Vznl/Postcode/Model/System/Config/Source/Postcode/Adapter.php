<?php

/**
 * Used in admin panel adapter selector for postcode service
 *
 */
class Vznl_Postcode_Model_System_Config_Source_Postcode_Adapter
{
    const DEFAULT_ADAPTER = 'Default';
    const STUB_ADAPTER = 'Stub';
    const POSTCODEAPI_NU = 'PostcodeApiNu';

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => self::DEFAULT_ADAPTER, 'label'=>Mage::helper('adminhtml')->__('Default adapter')),
            array('value' => self::STUB_ADAPTER, 'label'=>Mage::helper('adminhtml')->__('Stub adapter')),
            array('value' => self::POSTCODEAPI_NU, 'label'=>Mage::helper('adminhtml')->__('PostcodeAPI.nu adapter')),
        );
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        $optionArray = $this->toOptionArray();
        $array = [];
        foreach($optionArray as $option){
            $array[] = $option['value'];
        }
        return $array;
    }

}
