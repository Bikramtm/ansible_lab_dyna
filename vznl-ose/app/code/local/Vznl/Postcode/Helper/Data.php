<?php
class Vznl_Postcode_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Get the store config values for the given name for the Postcode module
     * @param string $name The name of the config
     * @return mixed The value if any
     */
    public function getPostcodeConfig($name = 'wsdl')
    {
        return Mage::getStoreConfig('vodafone_service/postcode/' . $name);
    }

    /**
     * Gets an address belonging to the postcode.
     * @param string $postcode The postcode.
     * @param int $houseno The house number.
     * @return array The postcode address data.
     * @throws Exception when the chosen adapter doesn't exist.
     */
    public function getPostcodeAddressData(string $postcode, int $houseno): array
    {
        $adapterToUse = $this->getPostcodeConfig('adapter_choice');

        $validAdapters = Mage::getSingleton('postcode/system_config_source_postcode_adapter')->toArray();
        if(!in_array($adapterToUse, $validAdapters)){
            throw new Exception('Either no adapter is chosen or the chosen adapter is no longer/not supported!');
        }

        $factoryName = 'Vznl_Postcode_Adapter_' . $adapterToUse . '_AdapterFactory';
        $adapter = $factoryName::create([]);

        if(!$adapter){
            throw new Exception('Something went wrong while creating the postcode adapter');
        }

        return $adapter->send($postcode, $houseno);
    }
}
