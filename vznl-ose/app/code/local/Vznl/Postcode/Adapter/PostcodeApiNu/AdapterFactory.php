<?php

use Psr\Log\LoggerInterface;
use Zend\Soap\Client;

/**
 * Class Vznl_Postcode_Adapter_PostcodeApiNu_AdapterFactory
 */
class Vznl_Postcode_Adapter_PostcodeApiNu_AdapterFactory
{
    /**
     * Create an adapter with the given variables
     * @param string $wsdl The wsdl
     * @param LoggerInterface $logger The logger to use
     * @param array $options An array with options
     * @return Vznl_Postcode_Adapter_Default_DefaultAdapter The adapter
     */
    public static function create(array $options=[])
    {
        $options = array_merge(self::getDefaultOptions(), $options);
        return new Vznl_Postcode_Adapter_PostcodeApiNu_Adapter($options);
    }

    /**
     * @return array
     */
    protected static function getDefaultOptions()
    {
        /** @var Vznl_Postcode_Helper_Data $postcodeHelper */
        $postcodeHelper = Mage::helper('postcode');

        $clientOptions = [
            'verify' => ($postcodeHelper->getPostcodeConfig('ssl_peer') == 1),
        ];
        if ($postcodeHelper->getPostcodeConfig('proxy_usage') == 1) {
            $proxyUrl = $postcodeHelper->getPostcodeConfig('proxy_url') ? : '';
            $clientOptions['proxy'] = $proxyUrl;
        }

        return array(
            'client_options' => $clientOptions,
            'country' => 'NL',
            'api_key' => $postcodeHelper->getPostcodeConfig('api_key')
        );
    }
}
