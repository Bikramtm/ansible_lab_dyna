<?php

use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Zend\Soap\Client;

/**
 * Class Vznl_Postcode_Adapter_Stub_Adapter
 */
class Vznl_Postcode_Adapter_Stub_Adapter
{
    public function send($postcode, $houseNumber)
    {
        return array(
            "street" => 'Mercator',
            "house_number" => $houseNumber,
            "postcode" => $postcode,
            "town" => 'Sittard'
        );
    }
}
