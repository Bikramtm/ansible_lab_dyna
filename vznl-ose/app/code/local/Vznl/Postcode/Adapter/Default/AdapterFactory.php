<?php

use Psr\Log\LoggerInterface;
use Zend\Soap\Client;

/**
 * Class Vznl_Postcode_Adapter_Default_AdapterFactory
 */
class Vznl_Postcode_Adapter_Default_AdapterFactory
{
    /**
     * Create an adapter with the given variables
     * @param array $options An array with options
     * @return Vznl_Postcode_Adapter_Default_DefaultAdapter The adapter
     */
    public static function create(array $options=[])
    {
        $options = array_merge(self::getDefaultOptions(), $options);
        return new Vznl_Postcode_Adapter_Default_Adapter($options['wsdl'], $options);
    }

    /**
     * @return array
     */
    protected static function getDefaultOptions()
    {
        $options = Mage::helper('omnius_service')->getZendSoapClientOptions('postcode') ? : [];

        /** @var Vznl_Postcode_Helper_Data $postcodeHelper */
        $postcodeHelper = Mage::helper('postcode');
        $options['wsdl'] = $postcodeHelper->getPostcodeConfig('wsdl');
        $options['endpoint'] = $postcodeHelper->getPostcodeConfig('usage_url');
        $options['soap_version'] = SOAP_1_2;
        $options['trace'] = true;
        $options['connection_timeout'] = $postcodeHelper->getPostcodeConfig('timeout');
        $options['keep_alive'] = false;

        return $options;
    }
}
