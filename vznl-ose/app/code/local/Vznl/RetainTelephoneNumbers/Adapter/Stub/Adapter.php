<?php
use GuzzleHttp\Client;

/**
 * Class Vznl_RetainTelephoneNumbers_Adapter_Stub_Adapter
 *
 */
class Vznl_RetainTelephoneNumbers_Adapter_Stub_Adapter
{
    /**
     * @var string
     */
    private $endpoint;

    /**
     * @var Client
     */
    private $client;

    /**
     * @var string
     */
    private $channel;

    /**
     * @var string
     */
    private $cty;

    /**
     * Adapter constructor.
     *
     * @param Client $client
     * @param string $endpoint
     * @param string $channel
     * @param string $cty
     */
    public function __construct(Client $client, string $endpoint, string $channel, string $cty)
    {
        $this->endpoint = $endpoint;
        $this->channel = $channel;
        $this->cty = $cty;
        $this->client = $client;
    }

    /**
     * @param $basketId
     * @return $responseData
     */
    public function call($validateAddress)
    {
        if (is_null($validateAddress)) {
            return 'Address information not provided to adapter';
        }
        $name = 'RetainTelephoneNumbers';
        $requestData =  $this->getUrl($validateAddress);
        $stubClient = Mage::helper('vznl_retainTelephoneNumbers')->getStubClient();
        $stubClient->setNamespace('Vznl_RetainTelephoneNumbers');
        $responseData = $stubClient->call($name, $validateAddress);
        Mage::helper('vznl_retainTelephoneNumbers')->transferLog($requestData, $responseData, $name);
        return $responseData;
    }

    /**
     * @param $validateAddress
     * @return peal call url
     */
    public function getUrl($validateAddress)
    {
        if (isset($validateAddress['customer_id'])) {
            return $this->endpoint.
                "?cty=".$this->cty .
                "&chl=".$this->channel.
                "&custId=".$validateAddress['customer_id'].
                "&footPrint=".$validateAddress['footPrint'].
                "&houseFlatNumber=".$validateAddress['houseFlatNumber'].
                "&postcode=".$validateAddress['postCode'].
                "&addressId=".$validateAddress['addressId'].
                "&telephoneNumber=".$validateAddress['telephoneNumber'];
        }
        return $this->endpoint.
            "?cty=".$this->cty .
            "&chl=".$this->channel.
            "&footPrint=".$validateAddress['footPrint'].
            "&houseFlatNumber=".$validateAddress['houseFlatNumber'].
            "&postcode=".$validateAddress['postCode'].
            "&addressId=".$validateAddress['addressId'].
            "&telephoneNumber=".$validateAddress['telephoneNumber'];
    }

    public function getHelper()
    {
        return Mage::helper('vznl_retainTelephoneNumbers');
    }
}