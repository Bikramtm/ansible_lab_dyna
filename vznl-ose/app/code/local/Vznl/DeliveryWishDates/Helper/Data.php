<?php

class Vznl_DeliveryWishDates_Helper_Data extends Vznl_Core_Helper_Data
{
    protected $_namespace;

    /**
     * @return string
     */
    public function getAdapterChoice()
    {
        return Mage::getStoreConfig('vodafone_service/deliverywishdates/adapter_choice');
    }

    /**
     * @return string peal endpoint url
     */
    public function getEndPoint()
    {
        return Mage::getStoreConfig('vodafone_service/deliverywishdates/end_point');
    }

    /**
     * @return string peal username
     */
    public function getLogin()
    {
        return Mage::getStoreConfig('vodafone_service/deliverywishdates/login');
    }

    /**
     * @return string peal password
     */
    public function getPassword()
    {
        return Mage::getStoreConfig('vodafone_service/deliverywishdates/password');
    }

    /**
     * @return string peal cty
     */
    public function getCountry()
    {
        return Mage::getStoreConfig('vodafone_service/deliverywishdates/country');
    }

    /**
     * @return string peal channel
     */
    public function getChannel()
    {
        $agentHelper = Mage::helper('agent');
        $salesChannel = $agentHelper->getDealerSalesChannel();
        if ($salesChannel) {
            return trim($salesChannel);
        } else {
            return trim(Mage::getStoreConfig('vodafone_service/deliverywishdates/channel'));
        }
    }

    /**
     * @return object omnius_service/client_stubClient
     */
    public function getStubClient()
    {
        return Mage::getModel('omnius_service/client_stubClient');
    }

    /*
    * @return boolean peal verify
    * */
    public function getVerify()
    {
        return Mage::getStoreConfigFlag('vodafone_service/deliverywishdates/verify');
    }

    /*
    * @return boolean peal proxy
    * */
    public function getProxy()
    {
        return Mage::getStoreConfigFlag('vodafone_service/deliverywishdates/proxy');
    }

    /*
     * @return object Omnius_Service_Model_Logger
     */
    public function getLogger()
    {
        return Mage::getSingleton('omnius_service/logger');
    }

     /**
     * @param $basketId
     * @param $customerType
     * @param $deliveryMethod
     * @return string|array
     */
    public function getDeliveryWishDates($basketId, $customerType, $deliveryMethod, $isOverstappen, $newHardware)
    {
        $factoryName = $this->getAdapterChoice();
        $validAdapters = Mage::getSingleton('deliverywishdates/system_config_source_DeliveryWishDates_adapter')->toArray();
        if (!in_array($factoryName, $validAdapters)) {
            return 'Either no adapter is chosen or the chosen adapter is no longer/not supported!';
        }
        $adapter = $factoryName::create();
        return $adapter->call($basketId, $customerType, $deliveryMethod, $isOverstappen, $newHardware);
    }
}
