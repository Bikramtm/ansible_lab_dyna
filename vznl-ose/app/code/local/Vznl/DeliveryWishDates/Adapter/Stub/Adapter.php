<?php

/**
 * Class Vznl_DeliveryWishDates_Adapter_Stub_Adapter
 */
class Vznl_DeliveryWishDates_Adapter_Stub_Adapter
{
    const MODULE_NAME = 'DeliveryWishDates';
    const OVERSTAPPEN_MODULE_NAME = 'OverstappenWishDates';

    /**
     * @param $basketId
     * @param $customerType
     * @param $deliveryMethod
     * @return $responseData
     */
    public function call($basketId, $customerType, $deliveryMethod, $isOverstappen, $newHardware)
    {
        if (is_null($basketId)) {
            return 'No basketId provided to adapter';
        }

        if (is_null($customerType)) {
            return 'No customerType provided to adapter';
        }


        $arguments = array($basketId, $customerType, $newHardware ? $deliveryMethod : null, $isOverstappen);
        $stubClient = Mage::helper('vznl_deliverywishdates')->getStubClient();
        $stubClient->setNamespace('Vznl_DeliveryWishDates');
        if ($isOverstappen) {
            $responseData = $this->getDeliveryStubDates();
        } else {
            $responseData = $this->getDeliveryStubDates();
        }
        Mage::helper('vznl_deliverywishdates')->transferLog($arguments, $responseData, self::MODULE_NAME);
        return $responseData;
    }

    public function getDeliveryStubDates()
    {
        $dateList= array();

        $query_date = date('d-m-Y');
        for($i = 0; $i <= 30; $i++)
        {
            $dateList[] = array(
                'availableStartDate' => date('d-m-Y',strtotime('+'.$i.' days',strtotime($query_date))),
                'availableTimeSlots' => []
            );
        }

        return $return = [
            "appointmentId" => null,
            "availableDateDetails" => $dateList,
            "explanationRemarks" => [
                'WSHDT_RULE_RES_CUSTOMER'
            ],
            "proposedOverstappenDate"=> "01-01-2021",
            "warningCode" => null,
            "warningDescription" => null
        ];

    }

}