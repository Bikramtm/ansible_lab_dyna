<?php

class Vznl_DeliveryWishDates_Adapter_Stub_Factory
{
    public static function create()
    {
        return new Vznl_DeliveryWishDates_Adapter_Stub_Adapter();
    }
}