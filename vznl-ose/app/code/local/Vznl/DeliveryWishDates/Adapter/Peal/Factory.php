<?php

use GuzzleHttp\Client;

class Vznl_DeliveryWishDates_Adapter_Peal_Factory
{
    public static function create()
    {
        $client = new Client();
        $deliveryWishDatesHelper = Mage::helper('vznl_deliverywishdates');
        return new Vznl_DeliveryWishDates_Adapter_Peal_Adapter(
            $client,
            $deliveryWishDatesHelper->getEndPoint(),
            $deliveryWishDatesHelper->getChannel(),
            $deliveryWishDatesHelper->getCountry()
        );
    }
}