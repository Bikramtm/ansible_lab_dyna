<?php

/**
 * Class Vznl_Data_Helper_Data
 */
class Vznl_Data_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * @var string
     */
    private $_import_log_file;

    /**
     * @var string
     */
    public $cron_file = 'dataimport/index/import';
    /**
     * @var string
     */
    public $customers_file = 'customers';
    /**
     * @var string
     */
    public $campaigns_file = 'campaigns';

    /**
     * Vznl_Data_Helper_Data constructor.
     */
    public function __construct()
    {
        $this->setImportLogFile('customers_import_' . date("Y-m-d") . '.log');
    }

    /**
     * @param Exception $ex
     */
    public function log($ex)
    {
        Mage::log($ex->getMessage(), null, $this->getImportLogFile());
    }

    /**
     * @param string $msg
     */
    public function logMsg($msg)
    {
        Mage::log($msg, null, $this->getImportLogFile());
    }

    /**
     * @param string $name
     */
    public function setImportLogFile($name)
    {
        $this->_import_log_file = $name;
    }

    /**
     * @return string
     */
    public function getImportLogFile()
    {
        return $this->_import_log_file;
    }

    /**
     * @return string
     */
    public function getCronFile()
    {
        return $this->cron_file;
    }

    /**
     * @return string
     */
    public function getCustomersFile()
    {
        return $this->customers_file;
    }


    /**
     * @return string
     */
    public function getCampaignsFile()
    {
        return $this->campaigns_file;
    }

    /**
     * Customer import files should be moved to a folder to be reviewed later on
     *
     * @return string
     */
    public function getCustomerImportOldFiles()
    {
        $uploadPath = Mage::getBaseDir() . '/' . Mage::getStoreConfig('dataimport_configuration/connection_configuration/copy_import_file_path');
        if (substr($uploadPath, -1) != '/') {
            $uploadPath = $uploadPath . '/';
        };
        $io = new Varien_Io_File();
        if (!$io->fileExists($uploadPath)) {
            $io->mkdir($uploadPath);
        }
        return $uploadPath;
    }

    /**
     * @param string $type
     *
     * @return string
     */
    public function getMenuUrlByType($type)
    {
        return Mage::getStoreConfig('topmenu_url/url_configuration/' . $type);
    }

    /**
     * @param string $section
     * @param bool $isCustomerScreen
     *
     * @return array
     */
    public function getSectionSkeleton($section, $isCustomerScreen)
    {
        $subject = '';
        $sectionHeaderTitle = '';
        $sectionHeaderTotalText = '';
        $buckets = [];
        switch (mb_strtolower($section)) {
            case "failed-validations":
                $subject = 'FAILED_VALIDATIONS';
                $sectionHeaderTitle = $this->__('Validation Errors');
                $sectionHeaderTotalText = $this->__('Validation Errors');
                $buckets = ['fv-declined', 'fv-failed'];
                break;
            case "number-porting":
                $subject = 'NUMBERPORTING';
                $sectionHeaderTitle = $this->__('Number porting');
                $sectionHeaderTotalText = $this->__('ongoing number porting');
                $buckets = ['np-rejectedcancelled', 'np-rejected', 'np-pending', 'np-approved'];
                break;
            case "credit-checks":
                $subject = 'CREDITCHECK';
                $sectionHeaderTitle = $this->__('Validations');
                $sectionHeaderTotalText = $this->__('ongoing validations');
                $buckets = ['cc-rejected', 'cc-pending', 'cc-additionalinforequired', 'cc-approved', 'cc-partial'];
                break;
            case "open-orders":
                $subject = !$isCustomerScreen ? 'WORKLIST' : 'ORDER';
                $sectionHeaderTitle = !$isCustomerScreen ? $this->__('Open orders') : $this->__('Orders');
                $buckets = ['oo-error', 'oo-ready', 'oo-check'];
                if ($isCustomerScreen) {
                    $buckets[] = 'oo-other';
                } else {
                    // When not customer screen and in retail store the telesales bucket should be shown (before oo-other-open)
                    if (Mage::helper('agent')->isRetailStore()) {
                        $buckets[] = 'oo-other-store';
                    }
                    $buckets[] = 'oo-other-open';
                }
                if (Mage::helper('agent')->isIndirectStore()) {
                    $buckets[] = 'oo-imei';
                }
                if ($isCustomerScreen) {
                    $buckets[] = 'oo-inlife';
                }
                break;
        }

        return [
            'buckets' => $buckets,
            'subject' => $subject,
            'sectionHeaderTitle' => $sectionHeaderTitle,
            'sectionHeaderTotalText' => $sectionHeaderTotalText,
        ];
    }

    /**
     * @param string $cluster
     * @param array $buckets
     * @param int $agentId
     * @param int $customerId
     * @param int $dealerId
     * @param string $section
     * @param int $websiteId
     * @param bool $customerScreen
     * @param string $countQuery
     * @param array $clusterData
     *
     * @return array
     * @throws Exception
     */
    public function getBucketBody(
        $cluster,
        $buckets,
        $agentId,
        $customerId,
        $dealerId,
        $section,
        $websiteId,
        $customerScreen,
        $countQuery = 'true',
        $clusterData = []
    )
    {
        $bodyData = [];

        // Init empty buckets
        foreach ($buckets as $bucket) {
            $bodyData['buckets'][$bucket] = [];
        }

        // Validate if cluster belongs to this bucket
        if (!in_array($cluster, $buckets)) {
            throw new Exception('Cluster "' . $cluster . '" does not belong to this bucket');
        }

        // Get the totals
        $bodyData = $this->getBucketBodyCounts($bodyData, $cluster, $agentId, $customerId, $dealerId, $section, $websiteId, $customerScreen);

        // Get orders for the given bucket
        if ($countQuery !== 'true') {
            $bodyData = $this->getBucketBodyOrders($bodyData, $cluster, $agentId, $customerId, $dealerId, $section, $websiteId, $customerScreen, $clusterData);
        }

        return $bodyData;
    }

    /**
     * @param array $bodyData
     * @param string $cluster
     * @param int $agentId
     * @param int $customerId
     * @param int $dealerId
     * @param string $section
     * @param int $websiteId
     * @param bool $customerScreen
     *
     * @return mixed
     * @throws Exception
     */
    protected function getBucketBodyCounts(
        $bodyData,
        $cluster,
        $agentId,
        $customerId,
        $dealerId,
        $section,
        $websiteId,
        $customerScreen
    )
    {
        /** @var $packageModel Vznl_Package_Model_Package */
        $packageModel = Mage::getModel('vznl_package/package');
        /** @var Vznl_Superorder_Helper_Data $orderHelper */
        $orderHelper = Mage::helper('vznl_superorder');

        // Get the totals
        if ($cluster != 'oo-inlife') {
            /** @var Omnius_Package_Model_Mysql4_Package_Collection $orderPackageCollection */
            $orderPackageCollection = $packageModel->getOrdersPackages(
                $agentId,
                $customerId,
                $dealerId,
                $section,
                $websiteId,
                $cluster,
                null,
                null,
                1,
                null,
                $customerScreen
            );
            if ($cluster === 'oo-imei') {
                $nonSimOnlyOrderPackages = $orderHelper->excludeSimOnlyOrders($orderPackageCollection);
                $count = count($nonSimOnlyOrderPackages);
            } else {
                $count = $orderPackageCollection->getSize();
            }

            $countLabel = $count;
            if ($count > 50) {
                $countLabel = '+50';
            }
            $bodyData['buckets'][$cluster]['count']['value'] = $count;
            $bodyData['buckets'][$cluster]['count']['label'] = $countLabel;
        } else if ($cluster == 'oo-inlife') {
            $inlifeOrders = $this->getInlifeOrders();
            $count = (int)count($inlifeOrders);
            $countLabel = $count;
            if ($count > 50) {
                $countLabel = '+50';
            }
            $bodyData['buckets'][$cluster]['count']['value'] = $count;
            $bodyData['buckets'][$cluster]['count']['label'] = $countLabel;
        }

        return $bodyData;
    }

    /**
     * @param $bodyData
     * @param $cluster
     * @param $agentId
     * @param $customerId
     * @param $dealerId
     * @param $section
     * @param $websiteId
     * @param $customerScreen
     * @param $clusterData
     * @return mixed
     * @throws Exception
     */
    protected function getBucketBodyOrders($bodyData, $cluster, $agentId, $customerId, $dealerId, $section, $websiteId, $customerScreen, $clusterData)
    {
        /** @var $packageModel Vznl_Package_Model_Package */
        $packageModel = Mage::getModel('vznl_package/package');
        /** @var Vznl_Superorder_Helper_Data $orderHelper */
        $orderHelper = Mage::helper('vznl_superorder');

        if ($cluster != 'oo-inlife') {
            /** @var Omnius_Package_Model_Mysql4_Package_Collection $orderPackageCollection */
            $orderPackageCollection = $packageModel->getOrdersPackages(
                $agentId,
                $customerId,
                $dealerId,
                $section,
                $websiteId,
                $cluster,
                $clusterData['orderField'] ?? null,
                $clusterData['orderType'] ?? null,
                $clusterData['page'] ?? null,
                $clusterData['resultsPerPage'] ?? null,
                $customerScreen
            );

            if ($cluster === 'oo-imei') {
                $orderPackageCollection = $orderHelper->excludeSimOnlyOrders($orderPackageCollection);
            }
            $orders = $orderHelper->formatOrdersInfo($orderPackageCollection, $cluster);
        } elseif ($cluster == 'oo-inlife') {
            $orders = $this->getInlifeOrders();
        }

        $bodyData['buckets'][$cluster]['data'] = $orders;

        $bodyData['buckets'][$cluster]['order_byfield'] = $clusterData['orderField'] ?? null;
        $bodyData['buckets'][$cluster]['order_type'] = $clusterData['orderType'] ?? null;
        $bodyData['buckets'][$cluster]['page'] = $clusterData['page'] ?? null;
        $bodyData['buckets'][$cluster]['results_per_page'] = $clusterData['resultsPerPage'] ?? null;

        // Calculating pages
        $bodyData['buckets'][$cluster]['totalpages'] = $bodyData['buckets'][$cluster]['results_per_page'] > 0 ? ceil($bodyData['buckets'][$cluster]['count']['value'] / $bodyData['buckets'][$cluster]['results_per_page']) : 1;

        // Set defaults to drop warnings
        if (!isset($clusterData['page'])) {
            $clusterData['page'] = 0;
        }
        if (!isset($clusterData['totalpages'])) {
            $clusterData['totalpages'] = 0;
        }

        // Set page urls
        $pagesIds = Mage::helper('dyna_package')->getPagesIds($bodyData['buckets'][$cluster]['totalpages'], $clusterData['page'], Vznl_Package_Model_Package::MAX_PAGES_TOP_MENU);
        $bodyData['buckets'][$cluster]['pages'] = $pagesIds;

        if ($clusterData['page']  > 1) {
            $bodyData['buckets'][$cluster]['prev_page'] = $clusterData['page'] - 1;
        } else {
            $bodyData['buckets'][$cluster]['prev_page'] = $clusterData['page'];
        }

        if (isset($bodyData['buckets'][$cluster]) && $clusterData['page'] < $bodyData['buckets'][$cluster]['totalpages']) {
            $bodyData['buckets'][$cluster]['next_page'] = $clusterData['page'] + 1;
        } else {
            $bodyData['buckets'][$cluster]['next_page'] = $clusterData['page'];
        }

        return $bodyData;
    }

    /**
     * @return array|false|mixed
     */
    protected function getInlifeOrders()
    {
        /** @var Vznl_Customer_Helper_Data $customerHelper */
        $customerHelper = Mage::helper('dyna_customer');
        /** @var Dyna_Customer_Model_Session $customerSession */
        $customerSession = Mage::getModel('customer/session');
        /** @var Dyna_Customer_Model_Customer $customer */
        $customer = $customerSession->getCustomer();

        $key = sprintf('%s_%s_%s', Mage::getSingleton('core/session')->getEncryptedSessionId(), $customer->getBan(), 'inlife_orders');
        if ($inlifeOrders = Mage::app()->getCache()->load($key)) {
            $inlifeOrders = unserialize($inlifeOrders);
        } else {
            $inlifeOrders = $customerHelper->getInlifeOrders($customer, false);
            Mage::app()->getCache()->save(serialize($inlifeOrders), $key, [Dyna_Cache_Model_Cache::CACHE_TAG], 3600); // 5min cache
        }

        return $inlifeOrders;
    }

    /**
     * Trigger file_get_contents to decrease
     * @param $param
     * @return false|string
     */
    public function getFileContents($param)
    {
        return file_get_contents($param);
    }
}
