<?php

/**
 * Class Vznl_Validators_Helper_Data
 */
class Vznl_Validators_Helper_Data extends Dyna_Validators_Helper_Data
{
    /** @var Vznl_Configurator_Model_Cache */
    protected $_cache;

    /**
     * Validate a date is entered in the format dd.mm.yyyy
     *
     * @param string $date
     * @param string $delimiter
     * @return bool
     */
    public function validateIsDate($date, $delimiter = '.')
    {
        if (empty($date)) {
            return false;
        }

        $dateData = explode($delimiter, $date);
        if (count($dateData) !== 3) {
            return false;
        }

        $pattern = sprintf('/^[0-9]{2}%s[0-9]{2}%s[0-9]{4}$/', $delimiter, $delimiter);
        return preg_match($pattern, $date) && checkdate($dateData[1], $dateData[0], $dateData[2]);
    }

    public function validateNLPostcode($code)
    {
        return  $this->testRegex('/^[0-9][0-9]{3}\s?[a-zA-Z]{2}$/', $code);
    }

    /**
     * Transform the wildcarded emails to regex for fast parsing
     *
     * @return array
     */
    public function getRegexesForDummyEmail()
    {
        $list = $this->getDummyEmailList();
        $result = [];
        foreach ($list as $item) {
            $result[] = "/^" . str_replace("%", ".*?", preg_quote($item)) . "$/";
        }

        return $result;
    }

    /**
     * Transform the wildcarded text to regex for fast parsing
     * @return array
     */
    public function getRegexesForDisallowedAddresses()
    {
        $list = $this->getDisallowedAdresses();
        $result = [];
        foreach ($list as $item) {
            $result[] = "/^" . str_replace("%", ".*?", preg_quote($item)) . "$/";
        }

        return $result;
    }

    /**
     * Retrieve the list of dummy emails from backend and convert it to an array
     */
    public function getDummyEmailList()
    {
        // Remove carriage return, and only rely on \n
        $string = str_replace("\r", "", Mage::getStoreConfig('vodafone_customer/dummy_emails/list'));
        // Replace * with % so we have consistency when generating regexes
        $string = str_replace("*", "%", $string);

        $result = array_map(function ($el) {
            return mb_strtolower(trim($el));
        }, explode("\n", $string));

        return array_filter($result, "strlen");
    }

    /**
     * Retrieve the list of disallowed adresses from backend and convert it to an array
     *
     * @return array
     */
    public function getDisallowedAdresses()
    {
        // Remove carriage return, and only rely on \n
        $string = str_replace("\r", "", Mage::getStoreConfig('vodafone_customer/disallowed_address/list'));
        // Replace * with % so we have consistency when generating regexes
        $string = str_replace("*", "%", $string);

        $result = array_map(function ($el) {
            return mb_strtolower(trim($el));
        }, explode("\n", $string));

        return array_filter($result, "strlen");
    }

    /**
     * @param string $iban
     * @return bool
     */
    public function validateNLIBAN($iban)
    {
        $key = sprintf('%s|%s', __METHOD__, $iban);
        if ($result = (unserialize($this->getCache()->load($key)))) {
            return $result;
        }

        $iban = trim($iban);
        $length = strlen($iban);

        switch ($length) {
            case 18:
                $result = $this->validate18IBAN($iban) ? $this->validateBicIban($iban) : false;
                break;
            case 9:
                $result = $this->validateBicIban($iban);
                break;
            case 8:
            case 7:
                $result = preg_match('/^[0-9]+$/', $iban) ? $this->convertBicToIban($iban) : false;
                break;
            default:
                $result = false;
        }
        $this->getCache()->save(serialize($result), $key, array(Vznl_Configurator_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());

        return $result;
    }

    /**
     * Calls webservice to convert BIC to IBAN and returns the IBAN.
     * @param $bic
     * @return string
     */
    public function convertBicToIban($bic)
    {
        /** @var Omnius_Service_Model_Client_IbanClient $ibanConverter */
        $ibanConverter = Mage::helper('omnius_service')->getClient('iban');
        return $ibanConverter->convert($bic);
    }


    /**
     * Validate a date is entered in the format dd-mm-yyyy
     *
     * @param $date
     * @return bool
     */
    public function validateIsNLDate($date)
    {
        if (empty($date)) {
            return false;
        }

        $dateData = explode('-', $date);
        if (count($dateData) !== 3) {
            return false;
        }

        return preg_match('/^[0-9]{2}-[0-9]{2}-[0-9]{4}$/', $date) && checkdate($dateData[1], $dateData[0], $dateData[2]);
    }

    /**
     * Validate addon code vs regex from backend
     *
     * @param string $catalogPricingCode
     *
     * @return bool
     */
    public function validateAddonCode($catalogPricingCode)
    {
        $regex = Mage::getStoreConfig(self::FIELD_RULES_CONFIG_PATH . 'rcvalue');
        if (!trim($regex)) {
            return true;
        }

        return !$this->testRegex($regex, $catalogPricingCode);
    }

    /**
     * Validate addon attributes
     *
     * @param array $attributes
     * @param string $attributeName
     *
     * @return bool
     */
    public function validateAddonAttributes($attributes, $attributeName)
    {
        $regex = Mage::getStoreConfig(self::FIELD_RULES_CONFIG_PATH . 'inlife_addon_attribute');
        if (trim($regex)) {
            $regexArray = explode("\n", $regex);
            foreach ($regexArray as $regexRow) {
                foreach ($attributes as $attribute) {
                    if ($attribute['name'] == $attributeName && isset($attribute['value']) && $this->testRegex($regexRow, $attribute['value'])) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    /**
     * Validates the name against possibly defined and restricted patterns in the backend
     * @param string $name
     * @return bool
     */
    public function validateName($name)
    {
        return false === $this->testRegex($this->getNameRegex(), $name);
    }

    /**
     * Tests the given email address if it is an valid emailaddress and not a dummy emailaddress.
     * @param string $email The email to validate.
     * @return bool <true> if valid, <false> if invalid.
     */
    public function isValidEmail($email)
    {
        if(!$this->validateEmailSyntax($email)){
            return false;
        }

        $regexes = $this->getRegexesForDummyEmail();
        foreach ($regexes as $regex) {
            $result = $this->testRegex($regex, $email);
            if($result){
                return false;
            }
        }

        return true;
    }

    /**
     * Validates that a set of fields from the address object were filled in
     * @param Mage_Customer_Model_Address $address
     * @throws Exception
     * @throws Zend_Validate_Exception
     */
    public function validateAddressData($address)
    {
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $isFixed = $quote->hasFixed();
        $errorLog = '';

        if (!Zend_Validate::is($address->getFirstname(), 'NotEmpty')) {
            $address->addError(Mage::helper('customer')->__('Please enter the first name.'));
            $errorLog = $errorLog.' || first name is empty';
        }

        if (!Zend_Validate::is($address->getLastname(), 'NotEmpty')) {
            $address->addError(Mage::helper('customer')->__('Please enter the last name.'));
            $errorLog = $errorLog.' || last name is empty';
        }

        if (!$isFixed) {
            if (!Zend_Validate::is($address->getStreet(1), 'NotEmpty')) {
                $address->addError(Mage::helper('customer')->__('Please enter the street.'));
                $errorLog = $errorLog.' || street is empty';
            }

            if (!Zend_Validate::is($address->getCity(), 'NotEmpty')) {
                $address->addError(Mage::helper('customer')->__('Please enter the city.'));
                $errorLog = $errorLog.' || city is empty';
            }

            $_havingOptionalZip=Mage::helper('directory')->getCountriesWithOptionalZip();
            if (!in_array($address->getCountryId(), $_havingOptionalZip)
                && !Zend_Validate::is($address->getPostcode(), 'NotEmpty')
            ) {
                $address->addError(Mage::helper('customer')->__('Please enter the zip/postal code.'));
                $errorLog = $errorLog.' || postal is empty';
            }
        }

        if (!Zend_Validate::is($address->getCountryId(), 'NotEmpty')) {
            $address->addError(Mage::helper('customer')->__('Please enter the country.'));
            $errorLog = $errorLog.' || country is empty';
        }

        if ($address->getCountryModel()->getRegionCollection()->getSize()
            && !Zend_Validate::is($address->getRegionId(), 'NotEmpty')
            && Mage::helper('directory')->isRegionRequired($address->getCountryId())
        ) {
            $address->addError(Mage::helper('customer')->__('Please enter the state/province.'));
            $errorLog = $errorLog.' || country is empty';
        }

        if($errorLog!='') {
            Mage::log("validate address : $errorLog\n\n", null, 'address_validation.log');
        }
    }

    /**
     * Validate a KvK number to make sure it has the right format
     * @param string $coc
     * @return bool
     */
    public function validateCocForeign($coc)
    {
        if (preg_match(Mage::helper('vznl_validators')->getRule('kvk_number'), $coc)) {
            return true;
        }
        return false;
    }

    /**
     * Validate not empty
     * @param string $data
     * @return bool
     */
    public function validateIsNotEmpty($data)
    {
        if (true === empty($data)) {
            return false;
        }
        return true;
    }

    /**
     * Make sure the specific country exists in the database
     * @param $countryId
     * @return bool
     */
    public function validateCountryCode($countryId)
    {
        $countryIso2 = Mage::getModel("directory/country")->load($countryId);
        $countryIso3 = Mage::getModel("directory/country")->load($countryId, 'iso3_code');

        return (bool) ($countryIso2->getId() || $countryIso3->getId());
    }

    /**
     * @param string $code
     * @param string $country
     * @return bool
     */
    public function validateForeignPostcode($code, $country = '')
    {
        switch (strtoupper($country)) {
            case 'DE':
            case 'DEU':
                return $this->testRegex('/^[0-9]{5}$/', $code);
            case 'BE':
            case 'BEL':
                return $this->testRegex('/^[0-9]{4}$/', $code);
            default:
                return true;
        }
    }

    /**
     * Validates the identification document based on its type (passport, id, etc)
     * @param $idType
     * @param $idNumber
     * @param $countryCode
     * @return bool
     */
    public function validateAPIIdType($idType, $idNumber, $countryCode)
    {
        switch ($idType) {
            case 'P':
                if (strtoupper($countryCode) == 'NL') {
                    // If country is NL check passport
                    $return = $this->testRegex('/^[a-zA-Z][a-zA-Z0-9]{7}\d$/', $idNumber);
                } else {
                    // If not NL, then no validation at all
                    $return = true;
                }
                break;
            case 'N': // ID card
                // NL Only and 9 digits
                if (strtoupper($countryCode) == 'NL') {
                    $return = $this->testRegex('/^[a-zA-Z][a-zA-Z0-9]{7}\d$/', $idNumber);
                } else {
                    $return = false;
                }
                break;
            default:
                $return = $this->validateIdType($idType, $idNumber, $countryCode);
        }

        return $return;
    }
}
