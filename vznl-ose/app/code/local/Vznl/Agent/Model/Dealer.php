<?php

/**
 * Class Vznl_Agent_Model_Dealer
 * @method getRedSalesId()
 * @method getGroupId()
 */
class Vznl_Agent_Model_Dealer extends Dyna_Agent_Model_Dealer
{

    /**
     * String
     */
    const MOBILE_MOVE_DEALERCODE = '00899999';
}