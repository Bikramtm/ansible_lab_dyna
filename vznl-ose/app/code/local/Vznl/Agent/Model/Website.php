<?php

/**
 * Class Vznl_Agent_Model_Website
 */
class Vznl_Agent_Model_Website extends Dyna_AgentDE_Model_Website
{
    const WEBSITE_RETAIL_CODE = 'retail';
    const WEBSITE_TELESALES_CODE = 'telesales';
    const WEBSITE_BELCOMPANY_CODE = 'belcompany';
    const WEBSITE_INDIRECT_CODE = 'indirect';
    const WEBSITE_WEB_SHOP_CODE = 'vodafone_webshop';
    const WEBSITE_BC_WEBSHOP_CODE = 'belcompany_webshop';
    const WEBSITE_TRADERS_CODE = 'vodafone_traders';
    const WEBSITE_FRIENDS_AND_FAMILY = 'friendsfamily';
    const WEBSITE_MOBILE_MOVE = 'mobile_move';
    //only for st2 purpose
    const WEBSITE_DEFAULT_CODE = 'default';

    /** @var array */
    protected static $_codes = array();

    /**
     * Returns an array containing all website codes
     *
     * @return array
     */
    public static function getWebsiteCodes()
    {
        if (!self::$_codes) {
            foreach (array_keys(Mage::app()->getWebsites()) as $_websiteId) {
                array_push(self::$_codes,  Mage::app()->getWebsite($_websiteId)->getCode());
            }
        }
        return self::$_codes;
    }
}
