<?php
/**
 * Copyright (c) 2019. Dynacommerce B.V.
 */

/**
 * Class Observer
 */
class Vznl_Agent_Model_Observer
{
    /**
     * Check if the agent should be logged out based on inactivity
     */
    public function checkAutoLogout()
    {
        $agentHelper = Mage::helper('agent');
        if (!$agentHelper->getAgent()) {
            return;
        }
        $agentHelper->updateLastActiveTime();
    }
}
