<?php

/**
 * Class Dyna_Agent_Model_Mapper_AbstractMapper
 */
abstract class Vznl_Agent_Model_Mapper_AbstractMapper
{
    /**
     * @param array $data
     * @return mixed
     */
    abstract public function map(array $data);

    /**
     * @param $property
     * @param $value
     * @param $entity
     * @return mixed
     */
    public function mapProperty($property, $value, $entity)
    {
        if ( ! empty($value)) {
            $method = 'map'.str_replace(" ", "", ucwords(strtr($property, "_-", "  ")));
            if (method_exists($this, $method)) {
                return $this->{$method}($value, $entity); //$this->mapSomeProperty($propertyValue, $entity)
            }
        }
        return $value;
    }
} 