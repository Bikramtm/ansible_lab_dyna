<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/* @var $installer Mage_Core_Model_Resource_Setup */

$installer = $this;
$installer->startSetup();
$installer->setConfigData('vodafone_service/rsa_keys/rsa_key_path', 'adt/private.key');
$installer->endSetup();
