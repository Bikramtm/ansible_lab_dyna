<?php

$write = Mage::getSingleton('core/resource')->getConnection('core_write');

$write->insert(
    "role_permission",
    array(
        "name" => Vznl_Agent_Model_Agent::ALLOW_ZIGGO,
        'description'=> "Able to create an order for fixed customer and view fixed product")
);