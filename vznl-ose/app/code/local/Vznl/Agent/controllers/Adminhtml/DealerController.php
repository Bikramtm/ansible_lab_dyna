<?php
require_once Mage::getModuleDir('controllers', 'Dyna_Agent') . DS . '/Adminhtml/DealerController.php';
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Vznl_Agent_Adminhtml_DealerController
 */
class Vznl_Agent_Adminhtml_DealerController extends Dyna_Agent_Adminhtml_DealerController
{
	public function saveAction()
    {
        $post_data = $this->getRequest()->getPost();


        if ($post_data) {
            try {

                $deliverydata=$post_data['delivery_method'];
		        $deliveryValues = implode(',', $deliverydata);
		        $post_data['delivery_method'] = $deliveryValues;

                foreach($post_data as &$data) {
                    $data = is_string($data) ? trim($data) : $data;
                }


                $model = Mage::getModel("agent/dealer")
                    ->addData($post_data)
                    ->setId($this->getRequest()->getParam("id"))
                    ->save();

                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Dealer was successfully saved"));
                Mage::getSingleton("adminhtml/session")->setDealerData(false);

                if ($this->getRequest()->getParam("back")) {
                    $this->_redirect("*/*/edit", array("id" => $model->getId()));
                    return;
                }
                $this->_redirect("*/*/");
                return;
            } catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                Mage::getSingleton("adminhtml/session")->setDealerData($this->getRequest()->getPost());
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
                return;
            }

        }
        $this->_redirect("*/*/");
    }
}