<?php

require_once Mage::getModuleDir('controllers', 'Dyna_Agent') . DS . '/Adminhtml/DealergroupController.php';

/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Vznl_Agent_Adminhtml_DealergroupController
 */
class Vznl_Agent_Adminhtml_DealergroupController extends Dyna_Agent_Adminhtml_DealergroupController
{
    public function saveAction()
    {
        $postData = $this->getRequest()->getPost();

        if ($postData) {
            $currentId = $this->getRequest()->getParam("id");
            try {
                $model = Mage::getModel("agent/dealergroup")
                    ->addData($postData)
                    ->setId($this->getRequest()->getParam("id"))
                    ->save();

                // If a new dealer group was added, we must parse all products which have the visibility set to excluding, and add this group as a possible group
                if ($currentId === null) {
                    $values = [];
                    // Get all the products that have the exclude visibility strategy
                    $productIds = Mage::getResourceModel('catalog/product_collection')->addAttributeToFilter('product_visibility_strategy', ['like' => '{"strategy":"exclude","groups":["%'])->getAllIds();
                    foreach ($productIds as $productId) {
                        $values[] = ['product_id' => $productId, 'group_id' => $model->getId()];
                    }
                    if(count($values) > 0) {
                        /** @var Magento_Db_Adapter_Pdo_Mysql $write */
                        $write = Mage::getSingleton('core/resource')->getConnection('core_write');

                        $tableName = Mage::getResourceModel('productfilter/filter')->getTable('filter');
                        $write->insertMultiple($tableName, $values);
                    }
                }

                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Dealer group was successfully saved"));
                Mage::getSingleton("adminhtml/session")->setDealergroupData(false);

                if ($this->getRequest()->getParam("back")) {
                    $this->_redirect("*/*/edit", array("id" => $model->getId()));
                    return;
                }
                $this->_redirect("*/*/");
                return;
            } catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                Mage::getSingleton("adminhtml/session")->setDealergroupData($this->getRequest()->getPost());
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
                return;
            }

        }
        $this->_redirect("*/*/");
    }
}
