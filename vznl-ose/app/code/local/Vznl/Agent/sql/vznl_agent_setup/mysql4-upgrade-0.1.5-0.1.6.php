<?php

$installer = $this;
$installer->startSetup();

// Add primary_brand (VF/Ziggo) in agent table
$this->getConnection()->addColumn($this->getTable("agent/dealer"), "primary_brand", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 20,
    "comment" => "Primary Brand",
    "default" => ""
]);

$this->endSetup();