<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;

$installer->startSetup();

// Add call_center_activity in agent table
$this->getConnection()->addColumn($this->getTable("agent/dealer"), "sender_domain", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 45,
    "after" => "delivery_method",
    "comment" => "Sender Domain",
]);

$installer->endSetup();