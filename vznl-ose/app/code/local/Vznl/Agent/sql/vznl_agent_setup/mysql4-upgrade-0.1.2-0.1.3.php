<?php

$installer = $this;
$installer->startSetup();

// Add call_center_activity in agent table
$this->getConnection()->addColumn($this->getTable("agent/dealer"), "call_center_activity", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 20,
    "after" => "bounce_info_email_address",
    "comment" => "call_center_activity",
]);