<?php

$installer = $this;
$installer->startSetup();

// Add name, description and dealer type in dealer table
$table = $this->getTable("agent/dealer");

// Add name in dealer table
$this->getConnection()->addColumn($table, "name", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 255,
    "after" => "dealer_id",
    "comment" => "Dealer name",
]);

// Add description in dealer table
$this->getConnection()->addColumn($table, "description", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 255,
    "after" => "name",
    "comment" => "Description",
]);

// Add dealer_type in dealer table
$this->getConnection()->addColumn($table, "dealer_type", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 255,
    "after" => "description",
    "comment" => "Dealer type",
]);