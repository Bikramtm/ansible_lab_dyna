<?php

$installer = $this;
$installer->startSetup();

$conn = Mage::getSingleton('core/resource')->getConnection('core_write');

// add configurator permissions for all roles
$permissions = array(
    'ALLOW_MOB',
    'ALLOW_CAB'
);

foreach ($permissions as $permission) {
    // Get permission id
    $sql = "SELECT `entity_id` FROM `role_permission` WHERE `name` LIKE '$permission'";
    $permissionId = $conn->fetchOne($sql);
    // Add permission to all roles
    $sql = "SELECT `role_id` FROM `agent_role`";
    $roles = $conn->fetchAll($sql, [], Zend_Db::FETCH_COLUMN);

    foreach ($roles as $role) {
        $sql = "SELECT `link_id` FROM `role_permission_link` WHERE `role_id` = " . $role . " AND `permission_id` = " . $permissionId;
        $linkId = $conn->fetchOne($sql);

        if (!$linkId) {
            $conn->insert('role_permission_link', array('role_id' => $role, 'permission_id' => $permissionId));
        }
    }
}

$installer->endSetup();