<?php

$installer = $this;
$installer->startSetup();

$installer->getConnection()->addIndex(
    $installer->getTable("agent_role"),
    $installer->getIdxName("role_id_fk", array("role_id")),
    array("role_id")
);

$installer->getConnection()->dropForeignKey(
    $installer->getTable("agent"),
    "fk_agent_role"
);

$installer->getConnection()->addForeignKey(
    $this->getFkName($this->getTable("agent"), "role_id", $this->getTable("agent_role"), "role_id"),
    $this->getTable("agent"), "role_id", $this->getTable("agent_role"), "role_id",
    Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
);

$installer->endSetup();
