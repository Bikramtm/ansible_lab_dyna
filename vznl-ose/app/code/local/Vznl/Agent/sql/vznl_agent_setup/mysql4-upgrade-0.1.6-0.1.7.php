<?php

$installer = $this;
$installer->startSetup();

// Add delivery_method (VF/Ziggo) in agent table
$this->getConnection()->addColumn($this->getTable("agent/dealer"), "delivery_method", [
	"type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 255,
    "comment" => "delivery method",
    "default" => ""
]);

$installer->getConnection()->modifyColumn('agent_role', 'entity_id', 'INT(11) UNSIGNED NOT NULL AUTO_INCREMENT');
$installer->endSetup();

