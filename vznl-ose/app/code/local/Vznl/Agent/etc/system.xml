<?xml version="1.0"?>
<!--
  Copyright (c) 2016. Dynacommerce B.V.
  -->

<config>
    <tabs>
        <dynasecurity>
            <label>Dyna Security</label>
            <sort_order>106</sort_order>
        </dynasecurity>
    </tabs>
    <sections>
        <agent>
            <label>Agent Configuration</label>
            <tab>dynasecurity</tab>
            <sort_order>200</sort_order>
            <show_in_default>1</show_in_default>
            <show_in_website>0</show_in_website>
            <show_in_store>0</show_in_store>
            <groups>
                <logout_configuration>
                    <label>Automatic logout configuration</label>
                    <frontend_type>text</frontend_type>
                    <sort_order>2</sort_order>
                    <show_in_default>1</show_in_default>
                    <show_in_website>1</show_in_website>
                    <show_in_store>1</show_in_store>
                    <expanded>0</expanded>
                    <fields>
                        <enable translate="label comment">
                            <label>Enable</label>
                            <frontend_type>select</frontend_type>
                            <source_model>adminhtml/system_config_source_yesno</source_model>
                            <sort_order>1</sort_order>
                            <show_in_default>1</show_in_default>
                            <show_in_website>1</show_in_website>
                            <show_in_store>1</show_in_store>
                        </enable>
                        <forced_logoff translate="label comment">
                            <label>Forced logoff seconds</label>
                            <comment><![CDATA[Number of seconds after which the dialog will appear]]></comment>
                            <frontend_type>text</frontend_type>
                            <sort_order>10</sort_order>
                            <show_in_default>1</show_in_default>
                            <show_in_website>1</show_in_website>
                            <show_in_store>1</show_in_store>
                            <validate>validate-number</validate>
                        </forced_logoff>
                        <dialog_seconds translate="label comment">
                            <label>Dialog seconds</label>
                            <comment><![CDATA[Number of seconds that the dialog will be displayed on the screen]]></comment>
                            <frontend_type>text</frontend_type>
                            <sort_order>20</sort_order>
                            <show_in_default>1</show_in_default>
                            <show_in_website>1</show_in_website>
                            <show_in_store>1</show_in_store>
                            <validate>validate-number</validate>
                        </dialog_seconds>
                        <password_expiry_duration translate="label comment">
                            <label>Password expiry duration</label>
                            <comment><![CDATA[Expiration interval for agent passwords in days]]></comment>
                            <frontend_type>text</frontend_type>
                            <validate>validate-not-negative-number</validate>
                            <sort_order>30</sort_order>
                            <show_in_default>1</show_in_default>
                            <show_in_website>1</show_in_website>
                            <validate>validate-number</validate>
                        </password_expiry_duration>
                    </fields>
                </logout_configuration>
                <password_validation>
                    <label>Password settings</label>
                    <sort_order>3</sort_order>
                    <show_in_default>1</show_in_default>
                    <show_in_website>1</show_in_website>
                    <show_in_store>1</show_in_store>
                    <expanded>0</expanded>
                    <fields>
                        <car_number_regexp translate="label comment">
                            <label>Car Number Validation Regexp</label>
                            <comment><![CDATA[A regexp against a Car Number like password will be validated]]></comment>
                            <frontend_type>text</frontend_type>
                            <sort_order>1</sort_order>
                            <show_in_default>1</show_in_default>
                            <show_in_website>1</show_in_website>
                            <show_in_store>1</show_in_store>
                        </car_number_regexp>
                        <tel_regexp translate="label comment">
                            <label>Telephone Validation Regexp</label>
                            <comment><![CDATA[A regexp against a Phone Number like password will be validated]]></comment>
                            <frontend_type>text</frontend_type>
                            <sort_order>2</sort_order>
                            <show_in_default>1</show_in_default>
                            <show_in_website>1</show_in_website>
                            <show_in_store>1</show_in_store>
                        </tel_regexp>
                        <dob_regexp translate="label comment">
                            <label>DOB Validation Regexp</label>
                            <comment><![CDATA[A regexp against a Date of Birth like password will be validated]]></comment>
                            <frontend_type>text</frontend_type>
                            <sort_order>3</sort_order>
                            <show_in_default>1</show_in_default>
                            <show_in_website>1</show_in_website>
                            <show_in_store>1</show_in_store>
                        </dob_regexp>
                        <seq_nr_regexp translate="label comment">
                            <label>Sequence Number Regexp</label>
                            <comment><![CDATA[A regexp against a consecutive digits sequence will be validated]]></comment>
                            <frontend_type>text</frontend_type>
                            <sort_order>4</sort_order>
                            <show_in_default>1</show_in_default>
                            <show_in_website>1</show_in_website>
                            <show_in_store>1</show_in_store>
                        </seq_nr_regexp>
                        <pwd_attempts translate="label comment">
                            <label>Wrong Password Attempts</label>
                            <comment><![CDATA[The maximum number of password attempts until the customer account will be blocked]]></comment>
                            <frontend_type>text</frontend_type>
                            <sort_order>4</sort_order>
                            <show_in_default>1</show_in_default>
                            <show_in_website>1</show_in_website>
                            <show_in_store>1</show_in_store>
                            <validate>validate-number</validate>
                        </pwd_attempts>
                        <reset_password translate="label">
                            <label>Recover password text</label>
                            <frontend_type>text</frontend_type>
                            <sort_order>70</sort_order>
                            <show_in_default>1</show_in_default>
                            <show_in_website>1</show_in_website>
                            <show_in_store>1</show_in_store>
                            <comment>The text displayed for the forgotten password option</comment>
                        </reset_password>
                        <reset_password_form translate="label">
                            <label>Enable recover password form</label>
                            <frontend_type>select</frontend_type>
                            <source_model>adminhtml/system_config_source_yesno</source_model>
                            <sort_order>70</sort_order>
                            <show_in_default>1</show_in_default>
                            <show_in_website>1</show_in_website>
                            <show_in_store>1</show_in_store>
                            <comment>If this is set to No, the Recover password text is used</comment>
                        </reset_password_form>
                    </fields>
                </password_validation>
                <forgot_credentials translate="label">
                    <label>Forgot password email</label>
                    <frontend_type>text</frontend_type>
                    <sort_order>4</sort_order>
                    <show_in_default>1</show_in_default>
                    <show_in_website>0</show_in_website>
                    <show_in_store>0</show_in_store>
                    <fields>
                        <send_credentials translate="label">
                            <label>Recover password email template</label>
                            <frontend_type>select</frontend_type>
                            <source_model>adminhtml/system_config_source_email_template</source_model>
                            <sort_order>1</sort_order>
                            <show_in_default>1</show_in_default>
                            <show_in_website>1</show_in_website>
                            <show_in_store>1</show_in_store>
                            <comment><![CDATA[Two variables are parsed to the template: $username AND $password_reset_link]]></comment>
                        </send_credentials>
                        <max_tokens translate="label">
                            <label>Maximum amount of tokens that can be generated</label>
                            <frontend_type>text</frontend_type>
                            <sort_order>2</sort_order>
                            <show_in_default>1</show_in_default>
                            <show_in_website>1</show_in_website>
                            <show_in_store>1</show_in_store>
                            <comment><![CDATA[The maximum number of forgot password link requests until the agent will be blocked. The amount of tries will be reset after the password has been changed.]]></comment>
                        </max_tokens>
                        <token_duration translate="label">
                            <label>Amount of time the token is valid</label>
                            <frontend_type>text</frontend_type>
                            <sort_order>3</sort_order>
                            <show_in_default>1</show_in_default>
                            <show_in_website>1</show_in_website>
                            <show_in_store>1</show_in_store>
                            <comment><![CDATA[In seconds]]></comment>
                        </token_duration>
                        <next_attempt_delay translate="label">
                            <label>Amount of time before the agent can reset his password again after changing it</label>
                            <frontend_type>text</frontend_type>
                            <sort_order>4</sort_order>
                            <show_in_default>1</show_in_default>
                            <show_in_website>1</show_in_website>
                            <show_in_store>1</show_in_store>
                            <comment><![CDATA[In seconds]]></comment>
                        </next_attempt_delay>
                    </fields>
                </forgot_credentials>
                <general_config>
                    <label>General settings</label>
                    <sort_order>1</sort_order>
                    <show_in_default>1</show_in_default>
                    <show_in_website>1</show_in_website>
                    <show_in_store>1</show_in_store>
                    <expanded>1</expanded>
                    <fields>
                        <channel translate="label comment">
                            <label>Channel type</label>
                            <frontend_type>select</frontend_type>
                            <source_model>Dyna_Agent_Model_System_Config_Channel</source_model>
                            <sort_order>1</sort_order>
                            <show_in_default>1</show_in_default>
                            <show_in_website>1</show_in_website>
                            <show_in_store>1</show_in_store>
                        </channel>
                        <allowed_urls translate="label comment">
                            <label>Public URLs</label>
                            <comment><![CDATA[URLs that are allowed to be accessed without agent login. One item per row, e.g.: contacts_index_index]]></comment>
                            <frontend_type>textarea</frontend_type>
                            <sort_order>10</sort_order>
                            <show_in_default>1</show_in_default>
                            <show_in_website>1</show_in_website>
                            <show_in_store>1</show_in_store>
                        </allowed_urls>
                        <axi_warehouse translate="label comment">
                            <label>Default AXI Warehouse</label>
                            <comment><![CDATA[This is used when a Dealer does not have an Axi store code defined]]></comment>
                            <frontend_type>text</frontend_type>
                            <sort_order>20</sort_order>
                            <show_in_default>1</show_in_default>
                            <show_in_website>1</show_in_website>
                            <show_in_store>1</show_in_store>
                        </axi_warehouse>
                    </fields>
                </general_config>
                <open_orders_limits>
                    <label>Open orders results limits</label>
                    <frontend_type>text</frontend_type>
                    <sort_order>101</sort_order>
                    <show_in_default>1</show_in_default>
                    <show_in_website>1</show_in_website>
                    <show_in_store>1</show_in_store>
                    <expanded>1</expanded>
                    <fields>
                        <default_oo_results translate="label comment">
                            <label>Default returned results</label>
                            <frontend_type>text</frontend_type>
                            <sort_order>1</sort_order>
                            <show_in_default>1</show_in_default>
                            <show_in_website>1</show_in_website>
                            <show_in_store>1</show_in_store>
                        </default_oo_results>
                        <maximum_oo_results translate="label comment">
                            <label>Maximum returned results</label>
                            <frontend_type>text</frontend_type>
                            <sort_order>1</sort_order>
                            <show_in_default>1</show_in_default>
                            <show_in_website>1</show_in_website>
                            <show_in_store>1</show_in_store>
                        </maximum_oo_results>
                    </fields>
                </open_orders_limits>
            </groups>
        </agent>
    </sections>
</config>
