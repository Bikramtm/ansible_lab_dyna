<?php

/**
 * Class Vznl_Agent_Block_Adminhtml_Dealer_Edit_Tab_Form
 * rewrites Dyna_AgentDE_Block_Adminhtml_Dealer_Edit_Tab_Form to include dealer campaigns
 */
class Vznl_Agent_Block_Adminhtml_Dealer_Edit_Tab_Form extends Dyna_AgentDE_Block_Adminhtml_Dealer_Edit_Tab_Form
{
    /**
     * Data regarding allowed campaigns is set on registry in agentde helper through mapCampaignData() method
     * and saved in Dyna_AgentDE_Model_Dealer which extends Dyna_Agent_Dealer model
     *
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset("agent_form", array("legend" => Mage::helper("agent")->__("Dealer Information")));

        $fields = [
            array(
                "name" => "name",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("agent")->__("Dealer name"),
                    "name" => "name",
                    "required" => true
                )
            ),
            array(
                "name" => "description",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("agent")->__("Description"),
                    "name" => "description"
                )
            ),
            array(
                "name" => "dealer_type",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("agent")->__("Dealer type"),
                    "name" => "dealer_type"
                )
            ),
            array(
                "name" => "street",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("agent")->__("Street"),
                    "class" => "required-entry",
                    "required" => true,
                    "name" => "street",
                )
            ),
            array(
                "name" => "postcode",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("agent")->__("Postal Code"),
                    "class" => "required-entry",
                    "required" => true,
                    "name" => "postcode",
                )
            ),
            array(
                "name" => "house_nr",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("agent")->__("House Nr."),
                    "class" => "required-entry",
                    "required" => true,
                    "name" => "house_nr",
                )
            ),
            array(
                "name" => "city",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("agent")->__("City"),
                    "class" => "required-entry",
                    "required" => true,
                    "name" => "city",
                )
            ),
            array(
                "name" => "telephone",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("agent")->__("Telephone"),
                    "class" => "required-entry",
                    "required" => true,
                    "name" => "telephone",
                )
            ),
            array(
                "name" => "store_id",
                "type" => "select",
                "parameters" => array(
                    'label' => Mage::helper('agent')->__('Store'),
                    'values' => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(),
                    'name' => 'store_id',
                    "class" => "required-entry",
                    "required" => true,
                )
            ),
            array(
                "name" => "hotline_number",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("agent")->__("Hotline number"),
                    "name" => "hotline_number",
                    "class" => "validation_hotline_number",
                )
            ),
            array(
                "name" => "axi_store_code",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("agent")->__("AXI store code"),
                    "name" => "axi_store_code",
                )
            ),
            array(
                "name" => "vf_dealer_code",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("agent")->__("VF dealer code"),
                    "name" => "vf_dealer_code",
                )
            ),
            array(
                "name" => "group_id",
                "type" => "multiselect",
                "parameters" => array(
                    'label' => Mage::helper('agent')->__('Dealer Group ID'),
                    'values' => Dyna_Agent_Block_Adminhtml_Dealer_Grid::getDealerGroups(),
                    'name' => 'group_id',
                    "class" => "required-entry",
                    "required" => true,
                )
            ),
            array(
                "name" => "delivery_method",
                "type" => "multiselect",
                "parameters" => array(
                    'label' => Mage::helper('agent')->__('Block delivery method'),
                    'values' => $this->getDeliveryRules(),
                    'name' => 'delivery_method',
                )
            ),
            array(
                "name" => "campaign_id",
                "type" => "multiselect",
                "parameters" => array(
                    'label' => Mage::helper('agentde')->__('Allowed campaigns'),
                    'values' => Mage::helper('agentde')->getAvailableCampaigns(),
                    'name' => 'campaign_id',
                    "class" => "",
                    "required" => false,
                )
            ),
            array(
                "name" => "naw_data_dealer_code",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("agent")->__("NAW DataDealer Code"),
                    "name" => "naw_data_dealer_code",
                )
            ),
            array(
                "name" => "axi_code",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("agent")->__("AXI Code"),
                    "name" => "axi_code",
                )
            ),
            array(
                "name" => "paid_code",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("agent")->__("Paid Code"),
                    "name" => "paid_code",
                )
            ),
            array(
                "name" => "email",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("agent")->__("Store email address"),
                    "name" => "email",
                )
            ),
            array(
                "name" => "bounce_info_email_address",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("agent")->__("Bounce Information Email Address"),
                    "name" => "bounce_info_email_address",
                    "class" => "validate-mails validate-email validate-emailSender",
                )
            ),
            array(
                "name" => "call_center_activity",
                "type" => "select",
                "parameters" => array(
                    "label" => Mage::helper("agent")->__("Call Center Activity"),
                    "name" => "call_center_activity",
                    "required" => true,
                    'values' => array(
                        ''=>'select',
                        'inbound' => 'Inbound Call',
                        'outbound' => 'Outbound Call',
                        'multiskill' => 'Multiskill Call'
                    )
                )
            ),
            array(
                "name" => "line_of_business",
                "type" => "select",
                "parameters" => array(
                    "label" => Mage::helper("agent")->__("Line Of Business"),
                    "name" => "line_of_business",
                    "required" => true,
                    'values' => array(
                        ''=>'select',
                        'VF' => 'Mobile',
                        'Ziggo' => 'Fixed',
                        'Combined' => 'Mixed'
                    )
                )
            ),
            array(
                "name" => "primary_brand",
                "type" => "select",
                "parameters" => array(
                    "label" => Mage::helper("agent")->__("Primary Brand"),
                    "name" => "primary_brand",
                    "required" => true,
                    'values' => array(
                        ''=>'select',
                        'Vodafone' => 'Vodafone',
                        'Ziggo' => 'Ziggo'
                    )
                )
            ),
            array(
                "name" => "sales_code",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("agent")->__("Sales code"),
                    "class" => "required-entry",
                    "required" => true,
                    "name" => "sales_code",
                )
            ),
            array(
                "name" => "sales_channel",
                "type" => "select",
                "parameters" => array(
                    "label" => Mage::helper("agent")->__("Sales channel"),
                    "name" => "sales_channel",
                    "class" => "required-entry",
                    "required" => true,
                    "values" => $this->getSalesChannelList(),
                )
            ),
            array(
                "name" => "sales_location",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("agent")->__("Sales location"),
                    "name" => "sales_location",
                )
            ),
            array(
                "name" => "sender_domain",
                "type" => "text",
                "parameters" => array(
                    "label" => Mage::helper("agent")->__("Sender domain"),
                    "class" => "required-entry",
                    "required" => true,
                    "name" => "sender_domain",
                )
            )
        ];

        foreach ($fields as $field) {
            $fieldset->addField($field["name"], $field["type"], $field["parameters"]);
        }

        $form->getElement('hotline_number')->setAfterElementHtml("<script>
            Validation.add('validation_hotline_number', 'Number not match to pattern 000/000000000 or +00/000000000', function(val) {
                return val.length == 0 ? true : /(^(\+[0-9]{2}|[0-9]{3})\/([0-9]*)$)/g.test(val);
            });
        </script>");

        Mage::helper('agentde')->mapCampaignData();

        if (Mage::getSingleton("adminhtml/session")->getDealerData()) {
            $form->setValues(Mage::getSingleton("adminhtml/session")->getDealerData());
            Mage::getSingleton("adminhtml/session")->setDealerData(null);
        } elseif (Mage::registry("dealer_data")) {
            $form->setValues(Mage::registry("dealer_data")->getData());
        }

        return Mage_Adminhtml_Block_Widget_Form::_prepareForm();
    }

    protected function getDeliveryRules() {
        $data_array =array();
        $data_array[] = array('value' => 'LSP_FIXED', 'label' => 'LSP_FIXED');
        $data_array[] = array('value' => 'LSP_DIFFERENT_ADD_FIXED', 'label' => 'LSP_DIFFERENT_ADD_FIXED');
        $data_array[] = array('value' => 'SHOP_FIXED', 'label' => 'SHOP_FIXED');
        $data_array[] = array('value' => 'TECHNICIAN_DELIVERY_FIXED', 'label' => 'TECHNICIAN_DELIVERY_FIXED');
        $data_array[] = array('value' => 'TECHNICIAN_ASAP_FIXED', 'label' => 'TECHNICIAN_ASAP_FIXED');
        $data_array[] = array('value' => 'ONE_DAY_FULL_INSTALL_FIXED', 'label' => 'ONE_DAY_FULL_INSTALL_FIXED');
        $data_array[] = array('value' => 'ENGINEER_APPOINTMENT_FIXED', 'label' => 'ENGINEER_APPOINTMENT_FIXED');
        $data_array[] = array('value' => 'LSP_MOBILE', 'label' => 'LSP_MOBILE');
        $data_array[] = array('value' => 'LSP_DIFFERENT_ADD_MOBILE', 'label' => 'LSP_DIFFERENT_ADD_MOBILE');
        $data_array[] = array('value' => 'SHOP_MOBILE', 'label' => 'SHOP_MOBILE');
        $data_array[] = array('value' => 'OTHER_SHOP_MOBILE', 'label' => 'OTHER_SHOP_MOBILE');

        return $data_array;
    }

    public static function getSalesChannelList()
    {

        $SalesChannelCollection = Mage::getModel('agent/dealer')->getCollection();
        $SalesChannelCollection->addFieldToFilter('sales_channel',['neq' => 'NULL']);
        $SalesChannelCollection->getSelect()->group('sales_channel');

        $data_array = array();
        foreach ($SalesChannelCollection as $group) {
            $data_array[] = array('value' => $group->getSalesChannel(), 'label' => $group->getSalesChannel());
        }
        return $data_array;
    }
}
