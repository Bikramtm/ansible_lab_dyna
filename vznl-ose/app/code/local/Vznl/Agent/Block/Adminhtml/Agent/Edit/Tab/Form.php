<?php

/**
 * Class Vznl_Agent_Block_Adminhtml_Agent_Edit_Tab_Form
 */
class Vznl_Agent_Block_Adminhtml_Agent_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset("agent_form", array("legend" => Mage::helper("agent")->__("Agent information")));
        $data = Mage::registry("agent_data")->getData();

        $yesno = array(
            '0' => Mage::helper('catalog')->__('No'),
            '1' => Mage::helper('catalog')->__('Yes')
        );

        $fieldset->addField("username", "text", array(
            "label" => Mage::helper("agent")->__("Username"),
            "class" => "required-entry",
            "required" => true,
            "name" => "username",
        ));

        $fieldset = $this->getPasswordField($fieldset, $data);

        $fieldset->addField("first_name", "text", array(
            "label" => Mage::helper("agent")->__("First Name"),
            "class" => "validate-emailSender",
            "required" => true,
            "name" => "first_name",
        ));

        $fieldset->addField("last_name", "text", array(
            "label" => Mage::helper("agent")->__("Last Name"),
            "class" => "validate-emailSender",
            "required" => true,
            "name" => "last_name",
        ));

        $fieldset->addField("phone", "text", array(
            "label" => Mage::helper("agent")->__("Phone"),
            "required" => true,
            "name" => "phone",
        ));

        $fieldset->addField("email", "text", array(
            "label" => Mage::helper("agent")->__("Email"),
            "class" => "validate-email",
            "required" => true,
            "name" => "email",
        ));

        $fieldset->addField('is_active', 'select', array(
            'label' => Mage::helper('agent')->__('Is Active'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'is_active',
            'onclick' => "",
            'onchange' => "",
            'selected' => '1',
            'values' => $yesno
        ));

        $fieldset->addField('everlasting_pass', 'select', array(
            'label' => Mage::helper('agent')->__('Has everlasting password'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'everlasting_pass',
            'onclick' => "",
            'onchange' => "",
            'selected' => '0',
            'values' => $yesno
        ));

        $fieldset = $this->getMultipleStoreAgentFields($fieldset, $data);

        $fieldset->addField("employee_number", "text", array(
            "label" => Mage::helper("agent")->__("Vodafone employee number"),
            "class" => "required-entry",
            "required" => true,
            "name" => "employee_number",
        ));

        $fieldset->addField('reset_attempts', 'checkbox', array(
            'label' => Mage::helper('agent')->__('Reset login attempts'),
            'name' => 'reset_attempts',
            'value' => '1',
            'after_element_html' => '<small>Current attempts: ' . (isset($data['login_attempts']) && !empty($data['login_attempts']) ? $data['login_attempts'] : 0) . '</small>'
        ));

        $fieldset->addField("sales_code", "text", array(
            "label" => Mage::helper("agent")->__("Sales Code"),
            "name" => "sales_code",
        ));

        if (Mage::getSingleton("adminhtml/session")->getAgentData()) {
            $form->setValues(Mage::getSingleton("adminhtml/session")->getAgentData());
            Mage::getSingleton("adminhtml/session")->setAgentData(null);
        } elseif (Mage::registry("agent_data")) {
            $form->setValues(Mage::registry("agent_data")->getData());
        }

        return parent::_prepareForm();
    }

    protected function getPasswordField($fieldset, $data)
    {
        if (isset($data['agent_id']) && isset($data['password'])) {
            $fieldset->addField('password', 'hidden', array(
                'label' => Mage::helper('agent')->__('Password'),
                'after_element_html' => '<tr><td class="label"><label for="password">' . Mage::helper('agent')->__('Password') . '</label></td>
                <td class="value">******<input type="password" style="display: none" /></td></tr>',
            ));

            $fieldset->addField("change_password", "obscure", array(
                "label" => Mage::helper("agent")->__("Change password"),
                "required" => false,
                "name" => "change_password",
            ));
        } else {
            $fieldset->addField("password", "obscure", array(
                "label" => Mage::helper("agent")->__("Password"),
                "class" => "required-entry",
                "required" => true,
                "name" => "password",
            ));
        }
        return $fieldset;
    }

    protected function getMultipleStoreAgentFields($fieldset, $data)
    {
        $count = 0;
        foreach (Mage::app()->getStores() as $store) {
            $checked = '';
            $agentcode = '';
            $role_id = '';
            $dealer_id = '';

            if ($count === 0) {
                $fieldset->addField('note', 'note', array(
                    'label' => 'Winkel',
                    'after_element_html' => "
                        <script type=\"text/javascript\">
                            document.observe('dom:loaded', function(){
                                $$('.store-select').invoke('observe', 'click', function(){
                                    $$('#'+$(this).id+'_content')[0].toggle();
                                });
                            });
                        </script>
                    "
                ));
            }

            // Compare current agent and set default values
            if (isset($data['username'])) {
                $agent = Mage::getModel('agent/agent')
                    ->getCollection()
                    ->addFieldToFilter('username', $data['username'])
                    ->addFieldToFilter('store_id', $store->getStoreId())
                    ->setPageSize(1, 1)
                    ->getLastItem();

                if (isset($agent) && $agent->getId()) {
                    $agentcode = $agent->getData('dealer_code');
                    $role_id = $agent->getRoleId();
                    $dealer_id = $agent->getDealerId();
                    $checked = 'checked';
                }
            }

            $dealers = $this->getDealers($dealer_id);
            $options = $this->getRoles($role_id);

            if ($store->getCode() === Vznl_Agent_Model_Website::WEBSITE_TELESALES_CODE) {
                $agentCodeInput = '
                        <div style="float: left; width: 180px; padding-left: 22px">
                            <label style="font-weight: bold"><sub>Agent code</sub>
                                <input class="input-text" style="width: 176px; border-radius: 3px" type="text" name="agent_code_' . $store->getStoreId() . '" placeholder="Agent code" value="' . $agentcode . '" />
                            </label>
                        </div>
                        ';
            } else {
                $agentCodeInput = '';
            }
            $fieldset->addField('store_id_' . $store->getStoreId(), 'checkbox', array(
                'label' => '',
                'class' => 'store-select',
                'value' => '2',
                'after_label' => 'test',
                'checked' => $checked,
                'after_element_html' => '' . $store->getName() . '</td></tr>
                    <tr><td></td><td>
                    <div id="store_id_'.$store->getStoreId().'_content" style="display:'.($checked == 'checked' ? 'block' : 'none').'; width:100%; box-sizing: border-box; padding-top: 12px; padding-bottom: 20px; padding-left: 0px; padding-right: 20px; float: left; background-color: #EEEEEE;border-radius: 4px;">
                        <div style="float: left; width: 180px; padding-left: 22px">
                            <label style="font-weight: bold"><sub>Dealer code</sub>
                                <select style="width: 180px; border-radius: 3px" name="dealer_code_' . $store->getStoreId() . '">' . $dealers . '</select>
                            </label>
                        </div>
                        <div style="float: left; width: 180px; padding-left: 22px">
                            <label style="font-weight: bold"><sub>Role</sub>
                                <select style="width: 180px; border-radius: 3px" name="role_' . $store->getStoreId() . '">' . $options . '</select>
                            </label>
                        </div>
                        ' . $agentCodeInput . '
                    </div>',
                'name' => 'store_id_' . $store->getStoreId(),
            ));
            $count++;
        }

        return $fieldset;
    }

    protected function getDealers($dealer_id)
    {
        $dealers = '';
        foreach (Dyna_Agent_Block_Adminhtml_Agent_Grid::getValueArray8() as $key => $val) {
            if ($dealer_id == $key) {
                $dealers .= '<option value="' . $key . '" selected>' . $val . '</option>';
            } else {

                $dealers .= '<option value="' . $key . '">' . $val . '</option>';
            }
        }
        return $dealers;
    }

    protected function getRoles($role_id)
    {
        $roles = '';
        foreach (Dyna_Agent_Block_Adminhtml_Agent_Grid::getValueArray6() as $key => $val) {
            if ($role_id == $key) {
                $roles .= '<option value="' . $key . '" selected>' . $val . '</option>';
            } else {
                $roles .= '<option value="' . $key . '">' . $val . '</option>';
            }
        }
        return $roles;
    }
}
