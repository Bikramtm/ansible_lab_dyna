<?php

/**
 * Class Vznl_Agent_Block_Adminhtml_Agentrole_Edit_Tab_Form
 */
class Vznl_Agent_Block_Adminhtml_Agentrole_Edit_Tab_Form extends Dyna_Agent_Block_Adminhtml_Agentrole_Edit_Tab_Form
{
    protected function getPermissionsOptions()
    {
        $options = parent::getPermissionsOptions();
        usort($options, function($a, $b) {
            return $a['label'] <=> $b['label'];
        });

        return $options;
    }
}
