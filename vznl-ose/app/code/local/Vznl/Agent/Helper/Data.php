<?php

class Vznl_Agent_Helper_Data extends Dyna_Agent_Helper_Data
{

    private $currentStoreId = null;

    /**
     * Check if website is telesales like (webshop)
     *
     * @param null|string $websiteCode
     * @return bool
     */
    public function isTelesalesLine($websiteCode = null)
    {
        return $this->isStoreView($websiteCode, $this->getTelesalesCodes());
    }

    /**
     * Check if website is telesales like (webshop)
     * @return bool
     */
    public function isTelesalesChannel()
    {
        return $this->isTelesalesLine();
    }

    /**
     * Check if is a retail store
     *
     * @param null|string $websiteCode
     * @return bool
     */
    public function isRetailStore($websiteCode = null)
    {
        return $this->isStoreView($websiteCode, $this->getRetailStoreCodes());
    }

    /**
     * Check if is a indirect store
     * @param null $websiteCode
     * @return bool
     */
    public function isIndirectStore($websiteCode = null)
    {
        return $this->isStoreView($websiteCode, $this->getIndirectStoreCodes());
    }

    /**
     * Check if website is webshop.
     *
     * @param null|string $websiteCode
     * @return bool
     */

    public function checkIsWebshop($websiteCode = null)
    {
        return $this->isStoreView($websiteCode, $this->getWebshopCodes());
    }

    /**
     * Check if website is webshop or telesales.
     * @param websiteId
     * @return bool
     */
    public function checkIsTelesalesOrWebshop($websiteId = null)
    {
        $websiteCode = $this->getWebsiteCode($websiteId);

        return (
            $this->isTelesalesLine($websiteCode)
            || $this->checkIsWebshop($websiteCode)
        );
    }

    /**
     * Checks whether the given website code or the current website code is of an assisted channel (retail/indirect).
     * @return bool <true> will mean the website code belongs to an assisted channel.
     */
    public function isAssistedChannel($websiteCode = null)
    {
        return $this->isIndirectStore($websiteCode) || $this->isRetailStore($websiteCode);
    }

    /**
     * Checks if the given website code (or the current website code) can be found inside the given website codes list.
     * @param string|null $websiteCode The website code to check, null means that the current website code will be
     * retrieved and used instead.
     * @param $websiteCodes The list with website codes to check for.
     * @return bool <true> if the given website code is inside the given website codes list.
     */
    protected function isStoreView($websiteCode, $websiteCodes)
    {
        if ($websiteCode === null) {
            $websiteCode = $this->getWebsiteCode();
        }

        return (in_array($websiteCode, $websiteCodes));
    }

    /**
     * Return dealer data
     * @return object
     */
    public function getDealerData()
    {
        $dealer = false;

        $agent = $this->getCurrentAgent();

        if ($agent) {
            $dealer = $agent->getDealer();
            $superAgent = $this->getCurrentSuperAgent();
            if ($superAgent) {
                $dealer = $superAgent->getDealer();
            }
        }

        return $dealer;
    }

    public function getDealerSalesChannel()
    {
        return $this->getDealerData()->getData('sales_channel');
    }

    /**
     * Return website code
     * @param websiteId
     * @return mixed
     */
    public function getWebsiteCode($websiteId = null)
    {
        return Mage::app()->getWebsite($websiteId)->getCode();
    }

    /**
     * Return customer agent from session
     * @return Dyna_Agent_Model_Dealer
     */
    public function getCurrentAgent()
    {
        $session = Mage::getSingleton('customer/session');

        return $session->getAgent();
    }

    /**
     * Return customer super agent from session
     * @return Dyna_Agent_Model_Dealer
     */
    public function getCurrentSuperAgent()
    {
        $session = Mage::getSingleton('customer/session');

        return $session->getSuperAgent();
    }

    /**
     * Returns an array with telesales codes
     * @return array
     */
    protected function getTelesalesCodes()
    {
        return [
            Vznl_Agent_Model_Website::WEBSITE_TELESALES_CODE,
            Vznl_Agent_Model_Website::WEBSITE_BC_WEBSHOP_CODE,
            Vznl_Agent_Model_Website::WEBSITE_TRADERS_CODE
        ];
    }

    /**
     * Returns an array with retail stores codes
     * @return array
     */
    protected function getRetailStoreCodes()
    {
        return [
            Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE,
            Vznl_Agent_Model_Website::WEBSITE_BELCOMPANY_CODE
        ];
    }

    /**
     * Returns an array with indirect stores codes
     * @return array
     */
    protected function getIndirectStoreCodes()
    {
        return [
            Vznl_Agent_Model_Website::WEBSITE_INDIRECT_CODE
        ];
    }

    /**
     * Returns an array with webshop codes
     * @return array
     */
    protected function getWebshopCodes()
    {
        return [
            Vznl_Agent_Model_Website::WEBSITE_WEB_SHOP_CODE,
            Vznl_Agent_Model_Website::WEBSITE_MOBILE_MOVE
        ];
    }

    /**
     * @return mixed
     */
    public function getWharehouseCode()
    {
        if (Mage::app()->getWebsite()->getCode() == Vznl_Agent_Model_Website::WEBSITE_BELCOMPANY_CODE) {
            return Mage::getStoreConfig('vodafone_service/axi/belcompany_warehouse');
        } else {
            return Mage::getStoreConfig('vodafone_service/axi/vodafone_warehouse');
        }
    }

    /**
     * Get the da dealer code from the current agent, or from the agent which is given if an agent is given.
     * @param Vznl_Agent_Model_Agent|null $agent The given agent or null.
     * @return string The da dealer code
     */
    public function getDaDealerCode($agent = null)
    {
        $daDealercode = self::DEFAULT_DEALERCODE;
        if (is_null($agent)) {
            $agent = $this->getAgent();
        }

        if ($agent && $agent->getDealer()) {
            return $agent->getDealer()->getVfDealerCode();
        }

        return $daDealercode;
    }

    /**
     * Get the dealer location from the current agent, or from the agent which is given if an agent is given.
     * @param Vznl_Agent_Model_Agent|null $agent The given agent or null.
     * @return string The dealer location
     */
    public function getDealerLocation($agent = null)
    {
        $dealerAddress = '';
        if (is_null($agent)) {
            $agent = $this->getAgent();
        }

        if ($agent && $agent->getDealer()) {
            return $agent->getDealer()->getAddress();
        }

        if ($agent && ($dealer = Mage::getModel('agent/dealer')->load($agent->getDealerId()))) {
            $dealerAddress = $dealer->getCity() . ', ' . $dealer->getStreet() . ' ' . $dealer->getHouseNr();
        }

        return $dealerAddress;
    }

    /**
     * Gets the agent name.
     * @return string
     */
    public function getAgentName()
    {
        $agent = $this->getAgent();
        if ($agent) {
            return $agent->getFirstName() !== $agent->getLastName() ? $agent->getFirstName() . " " . $agent->getLastName() : $agent->getLastName();   
        }
        return null;
    }

    /**
     * Gets the DeliveryMethod to be blocked.
     * @return array
     */
    public function getBlockDeliveryMethod() {
        $agent = $this->getAgent();
        $dealer = $agent->getDealer();
        $dealerGroup = $dealer->getDeliveryMethod();
        $value = array_values(explode(',', $dealerGroup));
        return $value;

    }

    /**
     * @param bool $asJson
     * @param bool $noStore
     * @param $hideDeleted <false> if all dealers should be shown, <true> if deleted dealers should be removed from this list.
     * @return object|string
     *
     * Get list of dealers as a collection or a json list
     */
    public function getDealers($asJson = false, $noStore = false, $hideDeleted = false)
    {
        $collection = Mage::getModel('agent/dealer')->getCollection();
        if (!$noStore) {
            $collection->addFieldToFilter('store_id', Mage::app()->getStore()->getId());
        }
        if ($hideDeleted) {
            // is_deleted should not be 1
            $collection->addFieldToFilter(
                'is_deleted',
                [
                    [
                        'neq' => 1
                    ],
                    [
                        'null' => true
                    ]
                ]
            );
        }

        if (!$asJson) {
            return $collection;
        }

        $dealers = array();
        /** @var Dyna_Agent_Model_Dealer $dealer */
        foreach ($collection as $dealer) {
            array_push($dealers, array(
                    'value' => $dealer->getAddress(),
                    'data' => $dealer->getId(),
                )
            );
        }

        return json_encode($dealers);
    }

    /**
     * @return mixed
     */
    public function getCurrentStoreId()
    {
        if (!Mage::getSingleton('customer/session')->getAgent()) {
            Mage::log('no agent logged in getProductsAction');
        }
        if (!$this->currentStoreId) {
            $this->currentStoreId = parent::getCurrentStoreId();
        }

        return $this->currentStoreId;
    }

    /**
     * @param $request
     * @param $controller
     *
    public function checkOrderEditMode($request, $controller)
    {
        if (Mage::getSingleton('customer/session')->getOrderEditMode() && $request->getRouteName() == 'cms' && $request->getControllerName() == 'index' && $request->getActionName() == 'index') {
            if (!$request->getParam('orderId')) {
                $lockInfo = Mage::getSingleton('customer/session')->getOrderLockInfo();
                if (is_array($lockInfo) && !empty($lockInfo)) {
                    reset($lockInfo);
                    $superOrderId = (key($lockInfo)); ;
                    $superOrder = Mage::getModel('superorder/superorder')->load($superOrderId);
                    $orderNumber = $superOrder->getOrderNumber();
                    if ($orderNumber) {
                        $controller->getResponse()->setRedirect(sprintf('%s?orderId=%s',Mage::getUrl(), $orderNumber))->sendHeaders();
                        exit;
                    }
                }
            }
        }
    }*/

    /**
     * Update the last active time
     */
    public function updateLastActiveTime()
    {
        Mage::getSingleton('core/session')->setLastActiveTime(time());
    }

    /**
     * Reset the last active time
     */
    public function resetLastActiveTime()
    {
        Mage::getSingleton('core/session')->unsLastActiveTime();
    }

    /**
     * Get the last active time
     * 
     * @return int
     */
    public function getLastActiveTime(): int
    {
        return (int)Mage::getSingleton('core/session')->getLastActiveTime();
    }
}
