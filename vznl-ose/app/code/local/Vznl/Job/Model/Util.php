<?php

/**
 * Class Vznl_Job_Model_Util
 */
class Vznl_Job_Model_Util
{
    const SERVICE_CALLS_SUPPORT_PHONE = 'general/store_information/service_calls_support_phone';

    /**
     * @param null $storeId
     * @return mixed
     */
    public function getSupportPhone($storeId = null)
    {
        return Mage::getStoreConfig(self::SERVICE_CALLS_SUPPORT_PHONE, $storeId);
    }

    /**
     * @return array
     */
    public function generateId()
    {
        return $this->_extractData(func_get_args());
    }

    /**
     * @param $data
     * @return array
     */
    protected function _extractData($data)
    {
        $items = array();
        if (is_array($data)) {
            foreach ($data as $key => $value) {
                $items[$key] = $this->_extractData($value);
            }
        } elseif ($data instanceof Varien_Data_Collection) {
            return $this->_extractData($data->getAllIds());
        } elseif ($data instanceof Varien_Object) {
            if (false && $data->getId()) {
                return sprintf('%s#%s', get_class($data), $data->getId());
            } else {
                return $this->_extractData($data->getData());
            }
        } elseif (is_object($data)) {
            if ($data instanceof SimpleXMLElement) {
                return $data->asXML();
            } else {
                return $this->_extractData(get_object_vars($data));
            }
        } elseif (is_scalar($data) || is_null($data)) {
            return $this->_filterVariables($data);
        }
        return $items;
    }

    /**
     * @param $data
     * @return null
     */
    protected function _filterVariables($data)
    {
        $time = strtotime($data);
        if ($time && 0 < $time) {
            return null;
        } else {
            return $data;
        }
    }
} 