<?php

/**
 * Class Vznl_Job_Model_RedisProcessor
 */
class Vznl_Job_Model_RedisProcessor extends Vznl_Job_Model_Processor
{
    /**
     * Processes jobs present in the queue
     */
    public function process()
    {
        $this->_processed = 0;
        /** @var Vznl_Job_Model_Queue $queue */
        $queue = Mage::helper('vznl_job')->getQueue('default');
        $retryLimit = $queue->getConfig()->getData('retry_limit') ? (int) $queue->getConfig()->getData('retry_limit') : 1;
        foreach ($this->_getNamespaces($queue) as $namespace => $batchCount) {
            $batchCount = (int) $batchCount;
            $counter = 0;
            /** @var Vznl_Job_Model_Job_Abstract $job */
            while ($job = $queue->pop($namespace)) {

                if ($job->shouldBeDelayed()) {
                    // check if the job has not passed the maximum time limit for re-en-queuing
                    $maxReEnqueueAge = $queue->getConfig()->getData('max_reenqueue_age', 0);
                    if ($maxReEnqueueAge && $job->getAge() > $maxReEnqueueAge) {
                       // Vznl_Job_Model_Processor::log(sprintf("The job (%s) should be delayed but the max time limit for delay has passed. Not re-en-queuing.", $job->getId()));
                        continue;
                    }
                    //Vznl_Job_Model_Processor::log(sprintf("The job (%s) can not be processed as it should be delayed. Delaying it five positions. Age: ", $job->getId(), $job->getAge()));
                    if (empty($job->getData('createdAt'))) {
                        $job->setData('createdAt', time());
                    }
                    $queue->reEnqueue($job, 5);
                    continue;
                }

                //Vznl_Job_Model_Processor::log(sprintf("Found job %s. Trying to find consumers for it.\n", $job->getId()));
                /** @var Vznl_Job_Model_Consumer_Abstract $consumer */
                foreach ($this->_getConsumers() as $consumer) {
                    if ($consumer->supports($job)) {
                        //Vznl_Job_Model_Processor::log(sprintf("Found consumer %s for job %s", get_class($consumer), $job->getId()));
                        $tries = 0;
                        while ($tries < $retryLimit) {
                            try {
                                //Vznl_Job_Model_Processor::log(sprintf('Tries: %s; Job %s', $tries, $job->getId()));
                                $job->incrementTries();
                                $consumer->processJob($job);
                                $job->setStatus(Vznl_Job_Model_Job_Abstract::SUCCESS);
                                $job->unsException();
                                $counter++;
                                //Vznl_Job_Model_Processor::log(sprintf('Process successful for job %s', $job->getId()));

                                // Unset GUID
                                if ($data = $job->getData('additional_info')) {
                                    $superOrderId = isset($data['hardware_order_id']) && !empty($data['hardware_order_id']) ? $data['hardware_order_id'] : $data['superorder_id'];
                                    $superOrder = Mage::getModel('superorder/superorder')->load($superOrderId);

                                    if($superOrder->getId()){
                                        $superOrder->setOrderGuid(null);
                                        $superOrder->save();
                                    }
                                }
                                break;
                            } catch (Exception $e) {
                                Vznl_Job_Model_Processor::log(sprintf('Process failed for job %s. Exception (%s) with message "%s", trace %s', $job->getId(), get_class($e), $e->getMessage(), $e->getTraceAsString()));
                                Mage::getSingleton('core/logger')->logException($e);
                                $job->setStatus(Vznl_Job_Model_Job_Abstract::FAILED);
                                $job->setException(new Exception($e->getMessage(), $e->getCode()));

                                if ($data = $job->getData('additional_info')) {
                                    // For ILS, save error information in case of simSwap or orderSim fail
                                    if (isset($data['dealer_code']) && isset($data['hardware_order_id']) && !empty($data['hardware_order_id'])) {
                                        $con = Mage::getSingleton('core/resource')->getConnection('core_write');
                                        $con->query(
                                            sprintf(
                                                'UPDATE superorder SET error_code="%s", error_detail="%s" WHERE entity_id="%s";',
                                                $e->getCode(),
                                                $e->getMessage(),
                                                $data['hardware_order_id']
                                            )
                                        );
                                    }
                                    if (isset($data['initial_state']) && $data['initial_state'] && isset($e->setFailStatus) && $e->setFailStatus) {
                                        $con = Mage::getSingleton('core/resource')->getConnection('core_write');
                                        $con->query(
                                            sprintf(
                                                'UPDATE superorder SET order_status="%s" WHERE entity_id="%s";',
                                                Vznl_Superorder_Model_Superorder::SO_FAILED,
                                                $data['superorder_id']
                                            )
                                        );

                                        // Also update the history with the status
                                        Mage::getModel('superorder/statusHistory')
                                            ->setType(Vznl_Superorder_Model_StatusHistory::SUPERORDER_STATUS)
                                            ->setSuperorderId($data['superorder_id'])
                                            ->setStatus(Dyna_Superorder_Model_Superorder::SO_FAILED)
                                            ->setCreatedAt(now())
                                            ->save();

                                    }
                                }

                                /**
                                 * Retry only if timeout related issue
                                 */
                                if ($this->_isTimeoutError($e)) {
                                    //Vznl_Job_Model_Processor::log(sprintf('Job (%s) process failed with timeout. Retrying', $job->getId()));
                                    $tries++;
                                    usleep((int) $queue->getConfig()->getData('pause_between_tries') * 1000000); //wait for X second before retrying
                                } else {
                                    break;
                                }
                            }
                        }
                        $this->_processed += 1;
                        $queue->set($job); //save processed job
                        //Vznl_Job_Model_Processor::log(sprintf("Job (%s) result saved to Redis\n", $job->getId()));
                        break; //do not let a job be processed by multiple consumers
                    } else {
                        Vznl_Job_Model_Processor::log(sprintf("Consumer of type %s cannot process job %s", get_class($consumer), $job->getId()));
                    }
                }
                if (0 !== $batchCount && $counter >= $batchCount) {
                    break;
                }

                time_nanosleep(0, $this->throttle());
            }
        }
    }
}
