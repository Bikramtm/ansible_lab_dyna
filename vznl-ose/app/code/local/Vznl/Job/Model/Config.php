<?php

/**
 * Class Vznl_Job_Model_Config
 */
class Vznl_Job_Model_Config extends Varien_Simplexml_Config
{
    /**
     * Vznl_Job_Model_Config constructor.
     */
    public function __construct()
    {
        parent::__construct($this->_getConfigFile());
    }

    /**
     * @return string
     */
    protected function _getConfigFile()
    {
        return Mage::getBaseDir('etc') . DS . 'jobs.xml';
    }
}