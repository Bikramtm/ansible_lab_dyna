<?php

/**
 * Class Vznl_Job_Model_Queue
 */
class Vznl_Job_Model_Queue
{
    const JOBS_CONFIG_NODE = 'jobs_config';

    const QUEUE_KEY_PREFIX = 'j_q___default';

    private static $_queues = array();

    /** @var Vznl_Job_Model_Backend_Abstract */
    private $_backend;

    /** @var Varien_Object */
    private $_config;

    /** @var string */
    private $_namespace;

    /** @var array|null */
    private $_supportedCalls = array();

    /**
     * @param null $namespace
     * @throws Exception
     */
    protected function __construct($namespace = null)
    {
        if ($namespace) {
            $_config = Mage::getSingleton('vznl_job/config')->getNode(sprintf('%s/queues/%s', self::JOBS_CONFIG_NODE, $namespace));
            $this->_config = new Varien_Object($_config ? $_config->asArray() : array());
        } else {
            $_config = Mage::getSingleton('vznl_job/config')->getNode(sprintf('%s/queues', self::JOBS_CONFIG_NODE))->asArray();
            $_config = reset($_config);
            $this->_config = new Varien_Object();
            if (isset($_config['backend'])) {
                $this->_config->addData($_config);
            }
        }

        if (!count($this->_config->getData())) {
            throw new Exception('Jobs config node not found in XML.');
        }
        $this->_namespace = $namespace;

        if (($this->_config->getData('backend') == 'Vznl_Job_Model_Backend_Null')
            || ($this->_config->getData('backend') == 'vznl_job/backend_null')) {
            $this->_supportedCalls[strtolower($namespace)] = array();
        }
    }

    /**
     * Re-enqueues the job with the given id
     * for reprocessing
     *
     * @param $jobId
     * @return $this
     */
    public function retry($jobId)
    {
        if (!($job = $this->get($jobId)) instanceof Vznl_Job_Model_Job_Abstract) {
            throw new LogicException(sprintf('No job was found for the given id %s', $jobId));
        }

        /** @var Vznl_Job_Model_Job_Abstract $job */
        $job
            ->setStatus(Vznl_Job_Model_Job_Abstract::PENDING)
            ->setLastUpdated()
            ->resetTries()
            ->unsResult()
            ->unsException();

        $this->push($job);

        return $this;
    }

    /**
     * @return bool
     */
    public function supportsNamespaces()
    {
        if ($this->getBackend() instanceof Vznl_Job_Model_Backend_Redis) {
            return true;
        }
        return false;
    }

    /**
     * @param null $queueId
     * @return Vznl_Job_Model_Queue
     */
    public static function create($queueId = null)
    {
        $key = $queueId ?: 'default';
        if (!isset(self::$_queues[$key])) {
            return self::$_queues[$key] = new self($queueId);
        }

        return self::$_queues[$key];
    }

    /**
     * @param Vznl_Job_Model_Job_Abstract $job
     */
    public function push(Vznl_Job_Model_Job_Abstract $job)
    {
        $this->getBackend()->push($job);
        $job->afterPush();
    }

    /**
     * @param null $namespace
     * @return Vznl_Job_Model_Job_Abstract|null
     */
    public function pop($namespace = null)
    {
        return $this->getBackend()->pop($namespace);
    }

    /**
     * @param null $namespace
     * @return mixed
     */
    public function size($namespace = null)
    {
        return $this->getBackend()->size($namespace);
    }

    /**
     * @return int|mixed
     */
    public function getWaitingItems()
    {
        $count = 0;

        foreach ($this->getKeys(self::QUEUE_KEY_PREFIX . '*') as $list) {
            $count += $this->getListSize($list);
        }

        return $count;
    }

    /**
     * @param $list
     * @return mixed
     */
    protected function getListSize($list)
    {
        return $this->getBackend()->count($list);
    }

    /**
     * @param null $namespace
     * @return bool
     */
    public function isEmpty($namespace = null)
    {
        return 0 === (int) $this->getBackend()->size($namespace);
    }

    /**
     * @param string $pattern
     * @return mixed
     */
    public function getKeys($pattern = '*')
    {
        return $this->getBackend()->keys($pattern);
    }

    /**
     * @param $uid
     * @return bool|string
     */
    public function get($uid)
    {
        return $this->getBackend()->get($uid);
    }

    /**
     * @param Vznl_Job_Model_Job_Abstract $job
     * @return mixed
     */
    public function del(Vznl_Job_Model_Job_Abstract $job)
    {
        return $this->getBackend()->del($job);
    }

    /**
     * @param Vznl_Job_Model_Job_Abstract $job
     */
    public function set(Vznl_Job_Model_Job_Abstract $job)
    {
        $this->getBackend()->set($job->setLastUpdated());
    }

    /**
     * @param Vznl_Job_Model_Job_Abstract $job
     * @param int $positions
     */
    public function reEnqueue(Vznl_Job_Model_Job_Abstract $job, $positions = 5)
    {
        $nextJobs = $this->getBackend()->getNext($positions);
        $lastJob = end($nextJobs);
        if ($lastJob) {
            $result = $this->getBackend()->pushAfter($job, $lastJob->getId());
            if ($result[0] == -1) {
                if ($this->getWaitingItems() < 10) {
                    $this->getBackend()->push($job);
                } else {
                    $this->reEnqueue($job, $positions * 2);
                }
            }
        } else {
            $this->getBackend()->push($job);
        }
        unset($nextJobs, $lastJob);
    }

    /**
     * @param string $jobId
     * @return bool
     */
    public function jobIsInQueue($jobId)
    {
        return $this->getBackend()->findJob($jobId);
    }

    /**
     * @return Varien_Object
     */
    public function getConfig()
    {
        return $this->_config;
    }

    /**
     * @param $name
     * @param $namespace
     * @return bool
     */
    public function supportsCall($name, $namespace)
    {
        return in_array($name, $this->getSupportedCalls($namespace));
    }

    /**
     * @param null $namespace
     * @return array
     */
    protected function getSupportedCalls($namespace = null)
    {
        $namespace = strtolower($namespace);
        if (!isset($this->_supportedCalls[$namespace])) {
            $res = array();
            $queuedCalls = $this->getConfig()->getData('queued_calls');
            foreach ((is_array($queuedCalls) ? $queuedCalls : array()) as $client => $calls) {
                if ($namespace && $namespace != $client) {
                    continue;
                }
                $res = array_filter(array_map('trim', explode(',', $calls)));
            }
            $this->_supportedCalls[$namespace] = $res;
        }
        return $this->_supportedCalls[$namespace];
    }

    /**
     * @return Vznl_Job_Model_Backend_Abstract
     */
    public function getBackend()
    {
        if (!$this->_backend) {
            if ($backendClass = $this->_config->getData('backend')) {
                $options = $this->_config->getData('backend_options');
                $options['ttl'] = $this->_config->getData('clear_job_older_than');
                if (false === strpos('/', $backendClass)) {
                    if (!class_exists($backendClass)) {
                        throw new RuntimeException(sprintf('Invalid backend provided in config. Backend class (%s) not present.', $backendClass));
                    }
                    $this->_backend = new $backendClass($options);
                } else {
                    if (!($this->_backend = Mage::getModel($backendClass, $options))) {
                        throw new RuntimeException(sprintf('Invalid backend provided in config. Backend class (%s) not present.', $backendClass));
                    }
                }
            } else {
                throw new RuntimeException('Invalid backend configuration.');
            }
        }
        return $this->_backend;
    }

    /**
     * Get the time since a job was added
     *
     * @param string $uid
     * @return int
     */
    public function getJobAge($uid)
    {
        return $this->getBackend()->getKeyAge($uid);
    }
}
