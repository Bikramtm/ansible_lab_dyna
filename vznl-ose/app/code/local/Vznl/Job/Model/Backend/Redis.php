<?php

/**
 * Class Vznl_Job_Model_Backend_Redis
 */
class Vznl_Job_Model_Backend_Redis extends Vznl_Job_Model_Backend_Abstract
{
    /** @var Credis_Client */
    private $_client;

    /** @var array */
    private $_options;

    /**
     * @param array $options
     */
    public function __construct(array $options)
    {
        $this->_options = new Varien_Object($options);
    }

    /**
     * Saves the item to the queue
     *
     * @param $job
     * @return mixed
     */
    public function push(Vznl_Job_Model_Job_Abstract $job)
    {
        $data = $job->getData();
        $data['__class'] = get_class($job);
        $result = $this->_getClient()
            ->pipeline()
            ->set($job->getId(), serialize($data))
            ->expire($job->getId(), $this->_options->getData('ttl'))
            ->lpush(Vznl_Job_Model_Queue::QUEUE_KEY_PREFIX, $job->getId())
            ->exec();

        //Vznl_Job_Model_Processor::log(sprintf("job %s push result: %s", $job->getId(), implode('|',$result)));
        //Vznl_Job_Model_Processor::log(sprintf("Job (id %s) pushed to redis:\n", $job->getId()));
    }

    /**
     * @param Vznl_Job_Model_Job_Abstract $job
     * @param $pivot
     * @return mixed|void
     */
    public function pushAfter(Vznl_Job_Model_Job_Abstract $job, $pivot)
    {
        return $this->_insert($job, 'after', $pivot);
    }

    /**
     * @param Vznl_Job_Model_Job_Abstract $job
     * @param $pivot
     * @return mixed|void
     */
    public function pushBefore(Vznl_Job_Model_Job_Abstract $job, $pivot)
    {
        $this->_insert($job, 'before', $pivot);
    }

    /**
     * Retrieves the next item from the queue
     *
     * @return Vznl_Job_Model_Job_Abstract|null
     */
    public function pop()
    {
        try {
            if ($jobId = $this->_getClient()->rpop(Vznl_Job_Model_Queue::QUEUE_KEY_PREFIX)) {
                return $this->get($jobId);
            } else {
                return null;
            }
        } catch (CredisException $e) {
            Vznl_Job_Model_Processor::log(sprintf("Credis exception: %s", $e->getMessage()));
            return null;
        } catch (Exception $e) {
            Vznl_Job_Model_Processor::log(sprintf("Job popped failed with exception (%s) message: %s, trace: %s", get_class($e), $e->getMessage(), $e->getTraceAsString()));
            Mage::getSingleton('core/logger')->logException($e);
            return null;
        }
    }

    /**
     * @param int $count
     * @return array
     */
    public function getNext($count = 1)
    {
        $count = (0 >= $count) ? 0 : $count - 1;
        $jobs = array();
        foreach ($this->_getClient()->lRange(Vznl_Job_Model_Queue::QUEUE_KEY_PREFIX, 0, $count) as $jobId) {
            $jobs[] = $this->get($jobId);
        }
        return $jobs;
    }

    /**
     * Check if a certain job is in the queue
     *
     * @param string $id
     * @return bool
     */
    public function findJob($id)
    {
        foreach ($this->_getClient()->lRange(Vznl_Job_Model_Queue::QUEUE_KEY_PREFIX, 0, $this->size()) as $jobId) {
            if ($jobId == $id) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns the size of the queue
     *
     * @return mixed
     */
    public function size()
    {
        return $this->_getClient()->lLen(Vznl_Job_Model_Queue::QUEUE_KEY_PREFIX);
    }

    /**
     * Fetches the result
     *
     * @param $id
     * @return bool|string
     */
    public function get($id)
    {
        try {
            if ($jobData = unserialize($this->_getClient()->get($id))) {
                if ( ! is_array($jobData)) {
                    throw new UnexpectedValueException(sprintf('Expected array instance, got %s', is_object($jobData) ? get_class($jobData) : gettype($jobData)));
                } elseif ( ! isset($jobData['__class'])) {
                    throw new RuntimeException(sprintf('Node "__class" not present.'));
                } elseif( ! class_exists($jobData['__class'])) {
                    throw new RuntimeException(sprintf('Job class "%s" not found', $jobData['__class']));
                }
                $job = new $jobData['__class']();
                $job->addData($jobData);
                //Vznl_Job_Model_Processor::log(sprintf("Job (id %s) retrieval from redis:\n", $job->getId()));
                return $job;
            } else {
                return null;
            }
        } catch (CredisException $e) {
            return null;
        } catch (Exception $e) {
            Vznl_Job_Model_Processor::log(sprintf("Job retrieval failed with exception (%s) message: %s, trace: %s", get_class($e), $e->getMessage(), $e->getTraceAsString()));
            Mage::getSingleton('core/logger')->logException($e);
            return null;
        }
    }

    /**
     * Saves the processed job
     *
     * @param Vznl_Job_Model_Job_Abstract $job
     * @return mixed
     */
    public function set(Vznl_Job_Model_Job_Abstract $job)
    {
        $data = $job->getData();
        $data['__class'] = get_class($job);
        $this->_getClient()
            ->pipeline()
            ->set($job->getId(), serialize($data))
            ->expire($job->getId(), $this->_options->getData('ttl'))
            ->exec();
        //Vznl_Job_Model_Processor::log(sprintf("Job (id %s) set to redis:\n", $job->getId()));
    }

    /**
     * Deletes the processed job
     *
     * @param Vznl_Job_Model_Job_Abstract $job
     * @return mixed
     */
    public function del(Vznl_Job_Model_Job_Abstract $job)
    {
        //Vznl_Job_Model_Processor::log(sprintf("Job (id %s) delete:\n", $job->getId()));
        $this->_getClient()->del($job->getId());
    }

    /**
     * Deletes the job at the given index
     *
     * @param $index
     * @return mixed
     */
    public function delAtPos($index)
    {
        //https://groups.google.com/forum/#!topic/redis-db/c-IpJ0YWa9I
        $this->_getClient()
            ->pipeline()
            ->lSet(Vznl_Job_Model_Queue::QUEUE_KEY_PREFIX, $index, '___DELETED___')
            ->lRem(Vznl_Job_Model_Queue::QUEUE_KEY_PREFIX, 1, '___DELETED___')
            ->exec();
    }

    /**
     * Returns the list of keys present in the
     * current db
     *
     * @param string $pattern
     * @return mixed
     */
    public function keys($pattern = '*')
    {
        return $this->_getClient()->keys($pattern);
    }

    /**
     * @param $list
     * @return mixed
     */
    public function count($list)
    {
        return (int) $this->_getClient()->lLen($list);
    }

    /**
     * @return Credis_Client
     */
    public function getConnection()
    {
        return $this->_getClient();
    }

    /**
     * @param Vznl_Job_Model_Job_Abstract $job
     * @param $direction
     * @param $pivot
     */
    protected function _insert(Vznl_Job_Model_Job_Abstract $job, $direction, $pivot)
    {
        $direction = strtoupper($direction);
        if ( ! in_array($direction, array('BEFORE', 'AFTER'))) {
            throw new InvalidArgumentException('Invalid direction for insertion provided');
        }

        $data = $job->getData();
        $data['__class'] = get_class($job);
        return $this->_getClient()
            ->pipeline()
            ->lInsert(Vznl_Job_Model_Queue::QUEUE_KEY_PREFIX, 'BEFORE', $pivot, $job->getId())
            ->set($job->getId(), serialize($data))
            ->expire($job->getId(), $this->_options->getData('ttl'))
            ->exec();
    }

    /**
     * @return Credis_Client
     */
    protected function _getClient()
    {
        if ( ! $this->_client) {
            $db = $this->_options->getData('database') ?: 0;
            $password = $this->_options->getData('password') ?: null;
            $this->_client = new Credis_Client(
                $this->_options->getData('server'),
                $this->_options->getData('port'),
                $this->_options->getData('timeout'),
                $this->_options->getData('persistent'),
                $db,
                $password
            );
        }

        return $this->_client;
    }

    /**
     * Return time since key was added
     *
     * @param string $id
     *
     * @return int
     */
    public function getKeyAge($id)
    {
        $ttl =  $this->_getClient()->ttl($id);
        return $this->_options->getData('ttl') - $ttl;
    }
}
