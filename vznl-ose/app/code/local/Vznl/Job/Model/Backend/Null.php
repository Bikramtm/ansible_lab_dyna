<?php

/**
 * Class Vznl_Job_Model_Backend_Null
 */
class Vznl_Job_Model_Backend_Null extends Vznl_Job_Model_Backend_Abstract
{
    /**
     * Saves the item to the queue
     *
     * @param $job
     * @return mixed
     */
    public function push(Vznl_Job_Model_Job_Abstract $job)
    {
        //no-op
    }

    /**
     * Saves the item to the queue after the given index (pivot)
     *
     * @param Vznl_Job_Model_Job_Abstract $job
     * @param $pivot
     * @return mixed
     */
    public function pushAfter(Vznl_Job_Model_Job_Abstract $job, $pivot)
    {
        //no-op
    }

    /**
     * Saves the item to the queue before the given index (pivot)
     *
     * @param Vznl_Job_Model_Job_Abstract $job
     * @param $pivot
     * @return mixed
     */
    public function pushBefore(Vznl_Job_Model_Job_Abstract $job, $pivot)
    {
        //no-op
    }

    /**
     * Retrieves the next item from the queue
     *
     * @return Vznl_Job_Model_Job_Abstract|null
     */
    public function pop()
    {
        return null;
    }

    /**
     * Retrieves the next X items from the queue
     *
     * @param int $count
     * @return mixed
     */
    public function getNext($count = 1)
    {
        return $count || !$count;
    }

    /**
     * Returns the size of the queue
     *
     * @return mixed
     */
    public function size()
    {
        return 0;
    }

    /**
     * Fetches the result
     *
     * @param $uid
     * @return bool|string
     */
    public function get($uid)
    {
        return null;
    }

    /**
     * Saves the processed job
     *
     * @param Vznl_Job_Model_Job_Abstract $job
     * @return mixed
     */
    public function set(Vznl_Job_Model_Job_Abstract $job)
    {
        //no-op
    }

    /**
     * Deletes the processed job
     *
     * @param Vznl_Job_Model_Job_Abstract $job
     * @return mixed
     */
    public function del(Vznl_Job_Model_Job_Abstract $job)
    {
        //no-op
    }

    /**
     * Deletes the job at the given index
     *
     * @param $index
     * @return mixed
     */
    public function delAtPos($index)
    {
        //no-op
    }

    /**
     * Returns the list of keys present in the
     * current db
     *
     * @return mixed
     */
    public function keys()
    {
        return array();
    }

    /**
     * @param $list
     * @return mixed
     */
    public function count($list)
    {
        return 0;
    }

    /**
     * @return mixed
     */
    public function getConnection()
    {
        return null;
    }

    /**
     * Check if a certain job is in the queue
     *
     * @param string $id
     * @return bool
     */
    public function findJob($id)
    {
        // no-op
    }
}