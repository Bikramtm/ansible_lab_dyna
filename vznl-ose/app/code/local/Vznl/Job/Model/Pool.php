<?php

/**
 * Class Vznl_Job_Model_Pool
 */
class Vznl_Job_Model_Pool
{
    /** @var Vznl_Job_Model_Queue */
    private $_queue;

    /**
     * @param $id
     * @return Exception|mixed|null
     * @throws Exception
     */
    public function getResult($id)
    {
        /** @var Vznl_Job_Model_Job_Abstract $job */
        if ($job = $this->getQueue()->get($id)) {
            if ($job->isProcessed()) {
                if ($job->isSuccessful()) {
                    return $job->getResult();
                } elseif ($exception = $job->getException()) {
                    throw $exception;
                }
            } else {
                return null;
            }
        }
        return false;
    }

    /**
     * @return Vznl_Job_Model_Queue
     */
    protected function getQueue()
    {
        if ( ! $this->_queue) {
            $this->_queue = Mage::helper('vznl_job')->getQueue();
        }
        return $this->_queue;
    }
}
