<?php

/**
 * Class Vznl_Job_Model_Consumer_SoapConsumer
 */
class Vznl_Job_Model_Consumer_SoapConsumer extends Vznl_Job_Model_Consumer_Abstract
{
    /** @var int */
    private $_priority = 0;

    /**
     * Returns true if the given job can be processed
     *
     * @param Vznl_Job_Model_Job_Abstract $job
     * @return mixed
     */
    public function supports(Vznl_Job_Model_Job_Abstract $job = null)
    {
        return $job instanceof Vznl_Job_Model_Job_Soap;
    }

    /**
     * @param Vznl_Job_Model_Job_Abstract $job
     * @return mixed|void
     * @throws Exception
     */
    public function processJob(Vznl_Job_Model_Job_Abstract $job)
    {
        /** @var Dyna_Service_Helper_Data $h */
        if (strtolower($job->getNamespace(true)) == 'inlife') {
            $h = Mage::helper('vznl_inlife');
        } else {
            $h = Mage::helper('vznl_service');
        }

        $client = $h->getClient($job->getNamespace(true));
        if ($additionalInfo = $job->getData('additional_info')) {
            if (isset($additionalInfo['run_check_lock']) && isset($additionalInfo['superorder_id']) && isset($additionalInfo['agent_id'])) {
                $reason = (isset($additionalInfo['do_approve_package'])) ? 'Approve packages' : 'Order change';
                // Lock order in frontend and AXI
                $superorder = Mage::getModel('superorder/superorder')->getSuperOrderByOrderNumber($additionalInfo['superorder_number']);

                $lockData = Mage::helper('dyna_service')->getClient('lock_manager')->doGetLockInfo($superorder);
                $lockState = (isset($lockData['lock']['lock_state']) && !is_array($lockData['lock']['lock_state'])) ? $lockData['lock']['lock_state'] : '';
                $isLocked = (strtolower($lockState) == 'locked' && $lockData['lock']['user_i_d'] != '');

                $retryLimit = Mage::helper('job')->getQueue('default')->getConfig()->getData('retry_limit') ?: 0;
                $retryMaximum = $retryLimit;
                $pauseBetweenRetries = Mage::helper('job')->getQueue('default')->getConfig()->getData('pause_between_tries') ?: 3;

                while ($isLocked && $lockData['lock']['user_i_d'] == 'ESB' && $retryLimit > 1) {
                    usleep($pauseBetweenRetries * 1000000);

                    $lockData = Mage::helper('dyna_service')->getClient('lock_manager')->doGetLockInfo($superorder);
                    $lockState = (isset($lockData['lock']['lock_state']) && !is_array($lockData['lock']['lock_state'])) ? $lockData['lock']['lock_state'] : '';
                    $isLocked = (strtolower($lockState) == 'locked' && $lockData['lock']['user_i_d'] != '');

                    $retryLimit--;
                }

                $allowHandover = isset($lockData['lock']['lock_type']['allow_hand_over']) && $lockData['lock']['lock_type']['allow_hand_over'];

                if ($isLocked) {
                    if ($lockData['lock']['user_i_d'] != 'ESB') {
                        if ($allowHandover) {
                            Mage::helper('dyna_service')->getClient('lock_manager')->doHandOverLock($additionalInfo['superorder_number'], $lockData['lock']['user_i_d'], 'ESB');
                        } else {
                            throw new Zend_Soap_Client_Exception('Hand over is not allowed for the order ' . $additionalInfo['superorder_number']);
                        }
                    } else {
                        throw new Zend_Soap_Client_Exception('The order ' . $additionalInfo['superorder_number'] . ' was still locked by ESB after ' . $retryMaximum . ' retries');
                    }
                } else {
                    Mage::helper('dyna_service')->getClient('lock_manager')->lockSaleOrder($superorder, $additionalInfo['agent_id'], $reason);
                    if ($allowHandover) {
                        Mage::helper('dyna_service')->getClient('lock_manager')->doHandOverLock($additionalInfo['superorder_number'], $additionalInfo['agent_id'], 'ESB');
                    } else {
                        throw new Zend_Soap_Client_Exception('Hand over is not allowed for the order ' . $additionalInfo['superorder_number']);
                    }
                }
            }
            if (isset($additionalInfo['dealer_code'])) {
                $client->setOption('dealer_code', $additionalInfo['dealer_code']);
            }
        }

        $job->runBeforeProcess();

        $job->setResult(
            $client->__call($job->getData('call_name'), $job->getData('request'), true)
        );
        if ($additionalInfo && isset($additionalInfo['dealer_code'])) {
            $client->removeOption('dealer_code');
        }

        $job->runAfterProcess();
    }
}
