<?php

/**
 * Class Vznl_Job_Model_Consumer_AmqpConsumer
 */
class Vznl_Job_Model_Consumer_AmqpConsumer extends Vznl_Job_Model_Consumer_Abstract
{
    /** @var int */
    private $_priority = 0;

    /** @var Vznl_Esb_Helper_Data */
    protected $_helper;

    /**
     * Returns true if the given job can be processed
     *
     * @param Vznl_Job_Model_Job_Abstract $job
     * @return mixed
     */
    public function supports(Vznl_Job_Model_Job_Abstract $job = null)
    {
        return $job instanceof Vznl_Job_Model_Job_Amqp;
    }

    /**
     * @param Vznl_Job_Model_Job_Abstract $job
     * @return mixed|void
     */
    public function processJob(Vznl_Job_Model_Job_Abstract $job)
    {
        if ($data = $job->getData('body')) {
            if ($arguments = explode(';', $data)) {
                $method = array_shift($arguments);
                if ( ! method_exists($this->_getHelper(), $method)) {
                    throw new RuntimeException(sprintf('Invalid AMQP message received. No "%s" method on the ESB helper (data: %s)', $method, $data));
                }
                $this->_getHelper()->{$method}(implode(';', $arguments));
                $job->runAfterProcess();
            } else {
                throw new RuntimeException(sprintf('Invalid AMQP message received. Malformed body ("%s")', $data));
            }
        } else {
            throw new RuntimeException('Invalid AMQP message received. Message has no body');
        }
    }

    /**
     * @return Vznl_Esb_Helper_Data
     */
    protected function _getHelper()
    {
        if ( ! $this->_helper) {
            $this->_helper = Mage::helper('esb');
        }
        return $this->_helper;
    }
}
