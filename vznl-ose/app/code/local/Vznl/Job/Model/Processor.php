<?php

/**
 * Class Vznl_Job_Model_Processor
 */
class Vznl_Job_Model_Processor
{
    const LOG_FILE = '%s_job.log';

    /** @var array */
    protected $_config;

    protected $_pause = 100000;

    protected $_sensibility = 1000;

    protected $_maxLoadPerCore = 1.5;

    protected $_timeoutMessages = array(
        'could not connect to host',
        'error fetching http headers',
        'gateway time-out',
        'has exceeded the allotted timeout'
    );

    protected $_processed = 0;

    protected static $_logHandler;

    /**
     * Init processor
     */
    public function __construct()
    {
        $this->_config = new Varien_Object(Mage::getSingleton('vznl_job/config')->getNode(Vznl_Job_Model_Queue::JOBS_CONFIG_NODE)->asArray());
    }

    /**
     * Cleanup
     */
    public function __destruct()
    {
        if (is_resource(static::$_logHandler)) {
            @fclose(static::$_logHandler);
        }
    }

    /**
     * @param $message
     * @throws Exception
     */
    public static function log($message)
    {
        $baseLogDir = Mage::getBaseDir('var') . DS . 'log' . DS . 'job_processing';
        if ( ! @realpath($baseLogDir)) {
            if ( ! @mkdir($baseLogDir, 0777, true)) {
                throw new Exception(sprintf('Could not create job processing log directory at %s', $baseLogDir));
            }
        }
        $logFile = 'job_processing' . DS . sprintf(self::LOG_FILE, date('Ymd'));
        Mage::log($message, Zend_log::DEBUG, $logFile, true);
    }

    /**
     * @return int
     */
    public function getProcessed()
    {
        return $this->_processed;
    }

    /**
     * @param Vznl_Job_Model_Job_Abstract $job
     * @return bool
     */
    protected function shouldDelayJob($job)
    {
        return $job->shouldBeDelayed();
    }

    /**
     * @param Vznl_Job_Model_Queue $queue
     */
    protected function processQueue($queue)
    {
        $retryLimit = $queue->getConfig()->getData('retry_limit') ? (int) $queue->getConfig()->getData('retry_limit') : 1;
        foreach ($this->_getNamespaces($queue) as $namespace => $batchCount) {
            $batchCount = (int) $batchCount;
            $counter = 0;
            while ($job = $queue->pop($namespace)) { /** @var Vznl_Job_Model_Job_Abstract $job */

                if ($this->shouldDelayJob($job)) {
                    //Vznl_Job_Model_Processor::log(sprintf("The job (%s) can not be processed as it should be delayed. Delaying it five positions", $job->getId()));
                    $queue->reEnqueue($job, 5);
                    continue;
                }

                //Vznl_Job_Model_Processor::log(sprintf("Found job %s. Trying to find consumers for it.\n", $job->getId()));
                foreach ($this->_getConsumers() as $consumer) { /** @var Vznl_Job_Model_Consumer_Abstract $consumer */
                    if ($consumer->supports($job)) {
                        //Vznl_Job_Model_Processor::log(sprintf("Found consumer %s for job %s", get_class($consumer), $job->getId()));
                        $tries = 0;
                        while($tries < $retryLimit) {
                            try {
                                //Vznl_Job_Model_Processor::log(sprintf('Tries: %s; Job %s', $tries, $job->getId()));
                                $job->incrementTries();
                                $consumer->processJob($job);
                                $job->setStatus(Vznl_Job_Model_Job_Abstract::SUCCESS);
                                $counter++;
                                //Vznl_Job_Model_Processor::log(sprintf('Process successfull for job %s', $job->getId()));
                                break;
                            } catch (Exception $e) {
                                Vznl_Job_Model_Processor::log(sprintf('Process failed for job %s. Exception (%s) with message "%s", trace %s', $job->getId(), get_class($e), $e->getMessage(), $e->getTraceAsString()));
                                Mage::getSingleton('core/logger')->logException($e);
                                $job->setStatus(Vznl_Job_Model_Job_Abstract::FAILED);
                                $job->setException(new Exception($e->getMessage(), $e->getCode()));
                                /**
                                 * Retry only if timeout related issue
                                 */
                                if ($this->_isTimeoutError($e)) {
                                    //Vznl_Job_Model_Processor::log(sprintf('Job (%s) process failed with timeout. Retrying', $job->getId()));
                                    $tries++;
                                    usleep((int) $queue->getConfig()->getData('pause_between_tries') * 1000000); //wait for X second before retrying
                                } else {
                                    break;
                                }
                            }
                        }
                        $this->_processed += 1;
                        $queue->set($job); //save processed job
                        //Vznl_Job_Model_Processor::log(sprintf("Job (%s) result saved to Redis\n", $job->getId()));
                        break; //do not let a job be processed by multiple consumers
                    } else {
                        Vznl_Job_Model_Processor::log(sprintf("Consumer of type %s cannot process job %s", get_class($consumer), $job->getId()));
                    }
                }
                if (0 !== $batchCount && $counter >= $batchCount) {
                    break;
                }

                time_nanosleep(0, $this->throttle());
            }
        }
    }

    /**
     * @param Exception $e
     * @return bool
     */
    protected function _isTimeoutError(Exception $e)
    {
        $exMessage = strtolower($e->getMessage());
        foreach ($this->_timeoutMessages as $message) {
            if (false !== strpos($exMessage, $message)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return int
     */
    protected function throttle()
    {
        /** @var Vznl_Job_Helper_Console $h */
        $h = Mage::helper('vznl_job/console');
        $load = sys_getloadavg();
        $cores = $h->getCoresCount();
        if (($load[0] / $cores) >= $this->_maxLoadPerCore) {
            $this->_pause += $this->_sensibility;
        } else {
            $this->_pause -= $this->_sensibility;
        }

        if ($this->_pause < 1) {
            $this->_pause = 1;
        }

        return $this->_pause;
    }

    /**
     * @return array
     */
    protected function _getQueues()
    {
        $queues = array();
        foreach ($this->_config->getData('queues') as $queueId => $config) {
            $queues[] = Mage::helper('vznl_job')->getQueue($queueId);
        }
        return $queues;
    }

    /**
     * @param $queue
     * @return array
     */
    protected function _getNamespaces(Vznl_Job_Model_Queue $queue)
    {
        if (!$queue->supportsNamespaces()) {
            return array(null);
        }

        $prios = $this->_getPriorities($queue->getConfig());
        $priorities = array();
        foreach ($prios as $prio => $count) {
            $priorities[(0 !== strpos($prio, $queue::QUEUE_KEY_PREFIX) ? $queue::QUEUE_KEY_PREFIX . '_' . $prio : $prio)] = $count;
        }
        $namespaces = array_diff($queue->getKeys($queue::QUEUE_KEY_PREFIX . '*'), $priorities);
        if ($namespaces) {
            $namespaces = array_combine(
                array_values($namespaces),
                array_fill(0, count($namespaces), $queue->getConfig()->getData('batch_count'))
            );
            ksort($namespaces);
        }
        return ($priorities ? $priorities : array()) + ($namespaces ? $namespaces : array());
    }

    /**
     * @param Varien_Object $config
     * @return array
     */
    protected function _getPriorities(Varien_Object $config)
    {
        $priorities = is_array($config->getData('priorities')) ? $config->getData('priorities') : array();
        $prio = array();
        usort($priorities, function($a, $b) use ($prio, $priorities) {
            if ($a['order'] == $b['order']) {
                return 0;
            }
            return ($a['order'] < $b['order']) ? -1 : 1;
        });
        $result = array();
        foreach ($priorities as $priority) {
            $result[$priority['namespace']] = (int) $priority['items'];
        }
        return $result;
    }

    /**
     * @return array
     */
    protected function _getConsumers()
    {
        $consumers = array(Mage::getSingleton('vznl_job/consumer_soapConsumer'), Mage::getSingleton('vznl_job/consumer_amqpConsumer'));

        return $consumers;
    }
} 