<?php

/**
 * Class Vznl_Job_Helper_Console
 */
class Vznl_Job_Helper_Console
{
    const DEFAULT_MEMORY_PER_PROC = '256M';
    const REDIS_CACHE_KEY = "VZNL_JOB_HELPER_CONSOLE_JOB_ACTIVE";
    const JOBS_ENABLED = "1";
    const JOBS_DISABLED = "2";
    const ADMINHTML_KEY = 'dyna_job/processor/enabled';
    const ADMINHTML_TTL = 'dyna_job/processor/ttl';

    /** @var Varien_Object */
    protected $_consoleConf;

    /** @var int */
    protected $_coreCount;

    /**
     * Init helper
     */
    public function __construct()
    {
        $this->_consoleConf = new Varien_Object(Mage::getSingleton('vznl_job/config')->getNode(sprintf('%s/consumer', Vznl_Job_Model_Queue::JOBS_CONFIG_NODE))->asArray());
    }

    /**
     * @throws Exception
     */
    public function persistConsumers()
    {
        $limit = $this->_getConsumerLimit();
        if ($this->getConsumerCount() < $limit && $this->canStartConsumers()) {
            $this->execute($this->_getCommand(), false);
        }
    }

    /**
     * @throws Exception
     */
    public function stopConsumers()
    {
        $flag = $this->_getStopFlag();
        if (false == @file_put_contents($flag, time())) {
            throw new RuntimeException(sprintf('Could not create flag (%s). Please check permissions.', $flag));
        }

        $waitingLimit = $this->getConsumerCount() * 10;
        $waiting = 0;
        while ($this->getConsumerCount()) {
            usleep(1 * 1000000);
            $waiting++;
            if ($waiting > $waitingLimit) {
                throw new Exception('The processes take too long to stop');
            }
        }

        if (false == @unlink($flag)) {
            throw new Exception(sprintf('Could not delete stop flat at "%s". Delete it manually', $flag));
        }
    }

    /**
     * @throws Exception
     */
    public function restartConsumers()
    {
        $this->stopConsumers();
        $this->startConsumers();
    }

    /**
     * @throws Exception
     */
    public function startConsumers()
    {
        // Only start consumers if backend flag is on
        if ($this->canStartConsumers()) {
            @unlink($this->_getStopFlag());
            $this->execute($this->_getCommand(), false);
        }
    }

    /**
     * @return string
     */
    protected function _getCommand()
    {
        return sprintf('nohup nice -n 10 %s%s %s > /dev/null 2>&1 &', $this->_getBinary(), $this->_getMemoryLimit(true), $this->_getConsumerScript());
    }

    /**
     * @return int
     */
    public function getCoresCount()
    {
        if ( ! $this->_coreCount) {
            $cpus = 1;
            if (is_file('/proc/cpuinfo')) {
                $cpuinfo = Mage::helper("vznl_data/data")->getFileContents('/proc/cpuinfo');
                preg_match_all('/^processor/m', $cpuinfo, $matches);
                $cpus = count($matches[0]);
            } else {
                $process = @popen('sysctl -a', 'rb');
                if (false !== $process) {
                    $output = stream_get_contents($process);
                    preg_match('/hw.ncpu: (\d+)/', $output, $matches);
                    if ($matches) {
                        $cpus = intval($matches[1][0]);
                    }
                    pclose($process);
                }
            }
            return $this->_coreCount = $cpus;
        }
        return $this->_coreCount;
    }

    /**
     * @return int
     * @throws Exception
     */
    public function getConsumerCount()
    {
        $consumerScript = explode(DS, $this->_getConsumerScript());
        $consumerScript = end($consumerScript);
        $cmd = sprintf('ps --no-headers ax | grep "%s"', $consumerScript);
        if ($output = $this->execute($cmd, false)) {
            preg_match_all(sprintf('/(.*)%s/', $consumerScript), $output, $matches);

            return count(array_filter($matches[0], function($el) {
                return (false !== strpos($el, ' grep ') || false === strpos($el, Mage::getBaseDir())) ? false : $el;
            }));
        } else {
            return 0;
        }
    }

    /**
     * @return int
     */
    public function _getConsumerLimit()
    {
        $maxProcessors = $this->_consoleConf->getData('max_consumers') ?: 100;
        $minConsumerOverhead = $this->_consoleConf->getData('min_consumer_overhead');
        if ($minConsumerOverhead == ''){
            $minConsumerOverhead = 4;
        }

        /** @var Dyna_Job_Model_Queue $q */
        $q = Mage::helper('job')->getQueue();

        $currProcs = $this->getConsumerCount();
        $itemsWaiting = $q->getWaitingItems() ?: 0;

        $limit = $maxProcessors < 2 ? $maxProcessors : 2;
        if ($itemsWaiting && ($itemsWaiting > $currProcs)) {
            $limit = intval(min($itemsWaiting, $maxProcessors));
        }
        return $limit + $minConsumerOverhead;
    }

    /**
     * @return int
     */
    protected function _getSysMem()
    {
        $data = explode("\n", Mage::helper("vznl_data/data")->getFileContents("/proc/meminfo"));
        $meminfo = array();
        foreach ($data as $line) {
            if (false !== strpos($line, ':')) {
                list($key, $val) = explode(":", $line);
                $meminfo[$key] = trim($val);
            }
        }
        return $this->_toBytes(preg_replace('/kB$/', 'k', $meminfo['MemTotal']));
    }

    /**
     * @param $value
     * @return int
     */
    protected function _toBytes($value)
    {
        $unit = strtolower(substr(trim($value), -1, 1));
        $value = (int) $value;
        switch($unit) {
            case 'g':
                $value *= 1024;
            // no break (cumulative multiplier)
            case 'm':
                $value *= 1024;
            // no break (cumulative multiplier)
            case 'k':
                $value *= 1024;
        }

        return $value;
    }

    /**
     * @param $cmd
     * @param bool $throw
     * @return string
     * @throws Exception
     */
    protected function execute($cmd, $throw = true)
    {
        @exec($cmd, $output, $status);
        if (0 === $status) {
            return join(PHP_EOL, $output);
        } else {
            if ($throw) {
                throw new Exception(sprintf('Command "%s" could not be executed.', $cmd), $status);
            } else {
                return null;
            }
        }
    }

    /**
     * @return string
     */
    protected function _getConsumerScript()
    {
        $consumerScript = trim($this->_consoleConf->getData('script'));
        if ( ! $consumerScript) {
            throw new RuntimeException('No "script" found in jobs config');
        }

        return 0 === strpos($consumerScript, DS)
            ? $consumerScript
            : sprintf('%s/%s', Mage::getBaseDir(), trim($consumerScript, DS));
    }

    /**
     * @return string
     */
    protected function _getStopFlag()
    {
        $flag = trim($this->_consoleConf->getData('stop_flag'));
        if ( ! $flag) {
            throw new RuntimeException('No "stop_flag" found in jobs config');
        }

        return 0 === strpos($flag, DS)
            ? $flag
            : sprintf('%s/%s', Mage::getBaseDir(), trim($flag, DS));
    }

    /**
     * @param bool $asParams
     * @return string
     */
    public function _getMemoryLimit($asParams = false)
    {
        $limit = trim($this->_consoleConf->getData('memory_limit'));
        if ( ! $limit) $limit = self::DEFAULT_MEMORY_PER_PROC;

        return $asParams
            ? sprintf(' -d memory_limit=%s', $limit)
            : $limit;
    }

    /**
     * @throws Exception
     */
    public function _getBinary()
    {
        $binary = trim($this->_consoleConf->getData('binary'));
        if ( ! $binary) {
            if (defined('PHP_BINARY') && strlen(constant('PHP_BINARY'))) {
                return constant('PHP_BINARY');
            } else {
                if (trim ($output = $this->execute('which php'))) {
                    return $output;
                } else {
                    throw new RuntimeException('Consumer script binary could not be detected');
                }
            }
        } else { //we need to make sure that the binary exists
            if (trim ($output = $this->execute(sprintf('which %s', $binary)))) {
                return $output;
            } else {
                throw new RuntimeException('Consumer script binary not found');
            }
        }
    }

    /**
     * Check redis key or db to see if it's allowed to run the processor
     */
    public function canStartConsumers()
    {
        $config = Mage::getConfig();
        $cache = Mage::app()->getCache();
        $options = $config->getNode('global/cache');
        if ($options) {
            $options = $options->asArray();
        } else {
            $options = array();
        }
        $key = Cm_Cache_Backend_Redis::PREFIX_KEY . $cache->getOption('cache_id_prefix') . self::REDIS_CACHE_KEY;
        // If there is a redis backend defined, then try to read from redis, if not, just use DB
        if (!empty($options['backend_options']['server'])) {
            $options = $options['backend_options'];
            $a = new Credis_Client($options['server'], $options['port'], null, '', $options['database'], $options['password'] ?: null);
            // Check if redis key exists and if true continue
            // If key does not exist, check db for flag and create key in redis for fast caching
            $value = $a->hGet($key, Cm_Cache_Backend_Redis::FIELD_DATA);
            if ($value === false) {
                $value = Mage::helper('vznl_job/util')->readConfig(self::ADMINHTML_KEY, 0);
                $ttl = Mage::helper('vznl_job/util')->readConfig(Vznl_Job_Helper_Console::ADMINHTML_TTL, 30);
                if ($value === false) {
                    $value = Mage::getStoreConfig(self::ADMINHTML_KEY, 0);
                }
                $cache->save($value, self::REDIS_CACHE_KEY, array(Vznl_Configurator_Model_Cache::CACHE_TAG), $ttl);
            }
        } else {
            $value = Mage::helper('vznl_job/util')->readConfig(self::ADMINHTML_KEY, 0);
            if ($value === false) {
                $value = Mage::getStoreConfig(self::ADMINHTML_KEY, 0);
            }
        }

        return $value == self::JOBS_ENABLED;
    }
}
