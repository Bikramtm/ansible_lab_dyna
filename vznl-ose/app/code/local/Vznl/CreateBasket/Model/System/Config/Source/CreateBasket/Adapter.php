<?php

/**
 * Used in admin panel adapter selector for Createbasket service
 *
 */
class Vznl_CreateBasket_Model_System_Config_Source_CreateBasket_Adapter
{
    const STUB_ADAPTER = 'Vznl_CreateBasket_Adapter_Stub_Factory';
    const PEAL_ADAPTER = 'Vznl_CreateBasket_Adapter_Peal_Factory';

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => self::STUB_ADAPTER, 'label' => Mage::helper('adminhtml')->__('Stub Adapter')),
            array('value' => self::PEAL_ADAPTER, 'label' => Mage::helper('adminhtml')->__('Peal Adapter')),
        );
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        $optionArray = $this->toOptionArray();
        $array = [];
        foreach ($optionArray as $option) {
            $array[] = $option['value'];
        }
        return $array;
    }

}
