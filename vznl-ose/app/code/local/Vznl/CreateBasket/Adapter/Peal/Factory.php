<?php

use GuzzleHttp\Client;

class Vznl_CreateBasket_Adapter_Peal_Factory
{
    public static function create()
    {
        $client = new Client();
        $basketHelper = Mage::helper('vznl_createbasket');
        return new Vznl_CreateBasket_Adapter_Peal_Adapter(
            $client,
            $basketHelper->getEndPoint(),
            $basketHelper->getChannel(),
            $basketHelper->getCountry()
        );
    }
}