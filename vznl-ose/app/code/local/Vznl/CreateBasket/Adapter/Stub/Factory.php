<?php

class Vznl_CreateBasket_Adapter_Stub_Factory
{
    public static function create()
    {
        return new Vznl_CreateBasket_Adapter_Stub_Adapter();
    }
}