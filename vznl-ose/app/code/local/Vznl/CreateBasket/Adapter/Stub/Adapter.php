<?php

/**
 * Class Vznl_CreateBasket_Adapter_Adapter
 */
class Vznl_CreateBasket_Adapter_Stub_Adapter
{
    CONST NAME = 'CreateBasket';
    /**
     * @param $lastName
     * @param $addressId
     * @return $responseData
     */
    public function call($lastName = null, $addressId = null)
    {
        $stubClient = Mage::helper('vznl_createbasket')->getStubClient();
        $stubClient->setNamespace('Vznl_CreateBasket');
        $arguments = array($lastName, $addressId);
        $responseData = $stubClient->call(self::NAME, $arguments);
        Mage::helper('vznl_createbasket')->transferLog($arguments, $responseData, self::NAME);
        return $responseData;
    }
}
