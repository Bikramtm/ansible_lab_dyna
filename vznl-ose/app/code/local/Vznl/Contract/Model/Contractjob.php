<?php

/**
 * Copyright (c) 2019. Dynacommerce B.V.
 * Class Vznl_Contract_Model_Contractjob
 */
class Vznl_Contract_Model_Contractjob extends Mage_Core_Model_Abstract
{
    const FLAG_LOCKED = 1;
    const FLAG_ERROR  = 1;

    protected function _construct()
    {
        $this->_init("vznl_contract/contractjob");
    }

    /**
     * Create new job in DB
     * @param string $path
     * @param string $fileName
     * @param string|null $contractData
     * @return Vznl_Contract_Model_Contractjob|null
     */
    public function addSftpJob(
        string $path,
        string $fileName,
        ?string $contractData
    ):?Vznl_Contract_Model_Contractjob
    {
        $data = array(
            'file_name' => $path.';'.$fileName,
            'contract_data' => $contractData,
            'created_at' => now()
        );
        return $this->setData($data)->save();
    }

    /**
     * Deletes job from DB
     * @param int $jobId
     * @return bool
     */
    public function deleteByJobId(
        int $jobId
    ):bool
    {
        return $this->getResource()->deleteByJobId($jobId);
    }

    /**
     * Updates job details in DB
     * @param int $jobId
     * @param array $data
     * @return bool
     */
    public function updateByCondition(
        int $jobId,
        array $data
    ):bool
    {
        return $this->getResource()->updateByCondition($jobId, $data);
    }

    /**
     * Increase tries for sending item
     * @return bool
     */
    public function increaseTries():bool
    {
        $tries = $this->getTries() + 1;
        $data = array(
            'tries' => $tries,
            'updated_at' => now()
        );
        return $this->updateByCondition($this->getJobId(), $data);
    }

    /**
     * Lock item
     * @return bool
     */
    public function lock():bool
    {
        $data = array(
            'locked' => 1,
            'updated_at' => now()
        );
        return $this->updateByCondition($this->getJobId(), $data);
    }

    /**
     * Unlock item
     * @return bool
     */
    public function unlock():bool
    {
        $data = array(
            'locked' => 0,
            'updated_at' => now()
        );
        return $this->updateByCondition($this->getJobId(), $data);
    }

    /**
     * Save error message
     * @param string $message
     * @return bool
     */
    public function setErrorMessage(
        string $message
    ):bool
    {
        $data = array(
            'error_message' => $message,
            'error_status' => 1
        );
        return $this->updateByCondition($this->getJobId(), $data);
    }
}
