<?php
/**
 * Copyright (c) 2019. Dynacommerce B.V.
 * Class Vznl_Contract_Model_Resource_Contract_Collection
 */

class Vznl_Contract_Model_Resource_Contractjob_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    public function _construct()
    {
        $this->_init('vznl_contract/contractjob');
    }
}
