<?php
/**
 * Copyright (c) 2019. Dynacommerce B.V.
 * Class Vznl_Contract_Model_Resource_Contractjob
 */

class Vznl_Contract_Model_Resource_Contractjob extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('vznl_contract/contract_job', 'id');
    }

    /**
     * Deletes job from DB
     * @param int $jobId
     * @return bool
     */
    public function deleteByJobId(
        int $jobId
    ):bool
    {
        $table = $this->getMainTable();
        $where = array();
        $where[] = $this->_getWriteAdapter()->quoteInto('job_id = ?', $jobId);
        $result = $this->_getWriteAdapter()->delete($table, $where);
        return $result;
    }

    /**
     * Updates job in DB by given conditions
     * @param int $jobId
     * @param array $data
     * @return bool
     */
    public function updateByCondition(
        int $jobId,
        array $data
    ):bool
    {
        $table = $this->getMainTable();
        $where = array();
        $where[] = $this->_getWriteAdapter()->quoteInto('job_id = ?', $jobId);
        $result = $this->_getWriteAdapter()->update($table, $data, $where);
        return $result;
    }
}
