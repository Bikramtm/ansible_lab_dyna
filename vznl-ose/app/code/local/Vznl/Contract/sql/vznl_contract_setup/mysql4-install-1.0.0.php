<?php

$installer = $this;
$installer->startSetup();
$connection = $installer->getConnection();
$tableName = $connection->getTableName('vznl_contract/contract_job');

// Drop table if it already exists
if ($installer->tableExists($tableName)) {
	$connection->dropTable($tableName);
}

// Create the table
$table = $connection->newTable($installer->getTable($tableName))
    ->addColumn('job_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
    	'unsigned' => true,
		'primary' => true,
		'auto_increment' => true,
		'nullable' => false
	), 'Job Id')
	->addColumn('file_name', Varien_Db_Ddl_Table::TYPE_VARCHAR, 150, array(
		'nullable' => false
	), 'File Name')
	->addColumn('contract_data', Varien_Db_Ddl_Table::TYPE_BLOB, null, array(
		'nullable' => false
	), 'Contract Data')
	->addColumn('tries', Varien_Db_Ddl_Table::TYPE_SMALLINT, 2, array(
		'unsigned' => true,
		'nullable' => false,
		'default'   => '0'
	), 'Retry Count')
	->addColumn('locked', Varien_Db_Ddl_Table::TYPE_SMALLINT, 1, array(
		'unsigned' => true,
		'nullable' => false,
		'default'   => '0'
	), 'Lock Status')
	->addColumn('error_status', Varien_Db_Ddl_Table::TYPE_SMALLINT, 1, array(
		'unsigned' => true,
		'nullable' => false,
		'default'   => '0'
	), 'Error Status')
	->addColumn('error_message', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(), 'Error Message')
	->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(), 'Creation Time')
	->addColumn('updated_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(), 'Update Time')
	->addIndex($installer->getIdxName('vznl_contract/contract_job', array('tries','locked','error_status')),
        array('tries','locked','error_status'))
	->setComment('Contracts SFTP Upload Jobs Table');
	$connection->createTable($table);

$installer->endSetup();
