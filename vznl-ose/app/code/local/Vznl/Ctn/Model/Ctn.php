<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */

/**
 * Class Vznl_Ctn_Model_Ctn
 */
class Vznl_Ctn_Model_Ctn extends Omnius_Ctn_Model_Ctn
{
    const CONSUMER_ONE_OFF_LIMIT = .7; // 70%
    const BUSINESS_ONE_OFF_LIMIT = .5; // 50%

    /**
     * @param $allCTNsCount
     * @param $retainebleCTNCount
     * @return bool
     */
    public function canActivateOneOffDeal($allCTNsCount, $retainebleCTNCount)
    {
        $customer = $this->getCustomer();
        $limit = (!$customer->getId() || !$customer->getIsBusiness()) ? self::CONSUMER_ONE_OFF_LIMIT : self::BUSINESS_ONE_OFF_LIMIT;

        return ($allCTNsCount > 1 && (($retainebleCTNCount / $allCTNsCount) >= $limit));
    }

    public function getCurrentSubscription($nameOnly = true)
    {
        $currentSubscription = [];
        $currentProducts = $this->getAdditional();
        if (isset($currentProducts['products']['type'])) {
            $currentProducts['products'] = array($currentProducts['products']);
        }
        foreach ($currentProducts['products'] as $product) {
            if (isset($product['type']) && ($product['type'] == 'PRICE_PLAN' || $product['type'] == 'Mobile_Main')) {
                $currentSubscription = $product;
            }
        }

        return $currentSubscription ? ($nameOnly ? $currentSubscription['description'] : $currentSubscription) : ($nameOnly ? '' : []);
    }

    // Overwrite getCtn in order to replace 06 with 316
    public function getCtn()
    {
        return Mage::helper('vznl_customer')->parseCtn($this->getData('ctn'));
    }
}