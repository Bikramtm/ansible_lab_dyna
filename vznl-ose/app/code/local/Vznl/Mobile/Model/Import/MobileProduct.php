<?php

/**
 * Class Vznl_Mobile_Model_Import_MobileProduct
 */
class Vznl_Mobile_Model_Import_MobileProduct extends Dyna_Mobile_Model_Import_MobileProduct
{
    protected $_attributeId = array();
    protected $_confAssociateProduct = array();
    protected $_attributeAssigned = array();

    public function __construct()
    {
        parent::__construct();
        /** @var Vznl_Import_Helper_Data _helper */
        $this->_helper = Mage::helper('vznl_import');
    }

    /**
     * Import cable product
     *
     * @param array $data
     *
     * @return void
     */
    public function importProduct($data)
    {
        $skip = false;
        $configurationCheck = true;
        //for logging purposes
        $this->_totalFileRows++;
        $data = $this->_setDataMapping($data);

        if (!$this->_helper->hasValidPackageDefined($data)) {
            $this->_skippedFileRows++;
            return;
        }

        if (!array_key_exists('sku', $data) || empty($data['sku'])) {
            $this->_logError('Skipping line without sku');
            $skip = true;
        } elseif (!array_key_exists('name', $data) || empty($data['name'])) {
            $this->_logError('Skipping line without name');
            $skip = true;
        }

        if ($skip) {
            //for logging purposes
            $this->_skippedFileRows++;
            return;
        }

        $data['stack'] = $this->stack;
        $data['sku'] = trim($data['sku']);
        $this->_currentProductSku = $data['sku'];
        $this->_log('Start importing ' . trim($data['name']), true);
        $data = $this->checkPrice($data);

        try {
            /** @var Dyna_Catalog_Model_Product $product */
            $product = Mage::getModel('catalog/product');
            $id = $product->getIdBySku($data['sku']);
            $new = ($id == 0);
            if (!$new) {
                $this->_log(
                    'Loading existing product "'
                    . $data['sku'] . '" with ID ' . $id,
                    true
                );
                $product->load($id);
            }

            // check vat value and tax
            $vatValue = $data['vat'];
            $vatClass = Mage::helper('dyna_catalog')->getVatTaxClass($vatValue);

            $skip = false;
            if (!$this->_setAttributeSetByCode($product, $data)) {
                $this->_logError(
                    'Unknown attribute set "' .
                    $data['attribute_set'] . '" for product ' .
                    $data['sku']
                );
                $skip = true;
            }
            if (!$this->_setWebsitesByCsvString($product, $data)) {
                $this->_logError(
                    'Unknown website "' .
                    $data['_product_websites'] . '" for product ' .
                    $data['sku']
                );
                $skip = true;
            }

            if ($skip) {
                //for logging purposes
                $this->_skippedFileRows++;
                return;
            }

            /*Reset product Id to original one*/
            $productInfoModel = Mage::getModel('productinfo/info')->loadBySku(trim($data['sku']));
            if($productInfoModel->getData()[0]) {
                $product->setEntityId($productInfoModel->getData()[0]['entity_id']);
            }

            if ($new) {
                try {
                    if ($productInfoModel->getData()[0]['entity_id']) {
                        $this->clearStockData('cataloginventory/stock_item', $productInfoModel->getData()[0]['entity_id']);
                        $this->clearStockData('cataloginventory/stock_status', $productInfoModel->getData()[0]['entity_id']);
                    }
                    $this->_createStockItem($product);

                } catch (Exception $exception) {
                    $this->_logError('[ERROR] Skipping stock import data with sku ' . $data['sku']);
                    $this->_logError($exception->getMessage());
                    $this->_skippedFileRows++;
                    return;
                }
            }

            if (!$this->setTaxClassIdByName($product, $vatClass)) {
                $this->_logError(
                    (empty($vatClass) ?
                        'Empty vat value' : 'Unknown vat value "'
                        . $vatClass . '"') . ' for product ' .
                    $data['sku']
                );
                $this->_skippedFileRows++;
                return;
            }
            $this->_setDefaults($product, $data);


            $this->_helper->setProductVisibilityStrategy($product, $data);
            if (isset($data['group_ids'])) {
                unset($data['group_ids']);
            }
            if (isset($data['strategy'])) {
                unset($data['strategy']);
            }
            $cleanData = $data;
            /* Unset configurable value for getting validate on attribute level*/
            if (isset($cleanData['product_specifications'])) {
                unset($cleanData['product_specifications']);
            }
            if (isset($cleanData['configurable_sku'])) {
                unset($cleanData['configurable_sku']);
            }
            if (isset($cleanData['configurable_attributes'])) {
                unset($cleanData['configurable_attributes']);
            }
            $this->_setProductData($product, $cleanData);

            // set product categories
            if (isset($data['category']) && is_array($data['category']) && count($data['category'])) {
                $product->setCategoryIds($data['category']);
            }

            // Adding product versions to product
            if (isset($data['product_version'])) {
                $versionIds = array();
                foreach (array_filter(explode(",", $data['product_version'])) as $versionCode) {
                    if (!empty($value = $this->productVersionModel::getProductVersionIdByCode($versionCode))) {
                        $versionIds[] = $value;
                    }
                }
                $product->setData("product_version", implode(",", $versionIds));
            }
            // Adding product family to product
            if (isset($data['product_family'])) {
                $familyIds = array();
                foreach (array_filter(explode(",", $data['product_family'])) as $familyCode) {
                    if (!empty($value = $this->productFamilyModel::getProductFamilyIdByCode($familyCode))) {
                        $familyIds[] = $value;
                    }
                }
                $product->setData("product_family", implode(",", $familyIds));
            }

            if (trim($data['product_specifications']) == 'configurable_product') {

                $configurationCheck = $this->_setConfigurableOption($product, $data);
            }

            if (!$this->getDebug()) {
                /*creating association product list*/
                $this->_confAssociateProduct[$data['configurable_sku']][]
                    = $data['sku'];
                if ($configurationCheck) {
                    $this->_setStatus($product, $data);
                    $product->save();

                    /*Save the new product in info table for latter use*/
                    if (!isset($productInfoModel)) {
                        $productInfo = Mage::getModel('productinfo/info');
                        $productInfo->setData('entity_id', $product->getId());
                        $productInfo->setData('sku', trim($data['sku']));
                        $productInfo->save();
                    }
                }
            }

            if (!$this->getDebug()) {
                $productVisibilityInfo = @json_decode(
                    $product->getProductVisibilityStrategy(),
                    true
                );
                if ($productVisibilityInfo) {
                    $groups = $productVisibilityInfo['strategy'] ? explode(',', $productVisibilityInfo['groups']) : null;
                    Mage::getModel('productfilter/filter')
                        ->createRule(
                            $product->getId(),
                            $productVisibilityInfo['strategy'], $groups
                        );
                }
            }

            unset($product);
            $this->_qties++;

            $this->_log('Finished importing ' . $data['name'], true);
            $this->_currentProductSku = null;
        } catch (Exception $ex) {
            //for logging purposes
            $this->_skippedFileRows++;
            echo $ex->getMessage();
            $this->_logEx($ex);
            $this->_currentProductSku = null;
        }
    }

    /* created configurable option and assign simple products
    * @param $product
    * @param $data
    */
    protected function _setConfigurableOption($product, $data)
    {
        /* Concept to import configurable products*/
        if (!isset($this->_confAssociateProduct[$data['sku']])) {
            $this->_logError("[Warning] sku:" . $data['sku'] ." has no simple products associated.");
        }

        $this->clearSuperAttributeData('catalog/product_super_attribute', $product->getEntityId());

        $attributeIdCollection = array();
        if ($data['configurable_attributes']) {
            $configurableAttribute = array_map('trim', explode(',', $data['configurable_attributes']));
            foreach ($configurableAttribute as $attributeCode) {
                if (!isset($this->_attributeId[$attributeCode])) {
                    $attr = Mage::getModel('catalog/resource_eav_attribute')
                        ->loadByCode('catalog_product', $attributeCode);
                    if (null !== $attr['attribute_id'] && $attr['is_configurable'] == 1) {
                        /*Check if attribute is a dropdown type and global scope */
                        if (
                            $attr['is_global'] == 1
                            && $attr['frontend_input'] == 'select'
                        ) {
                            $this->_attributeId[$attributeCode]
                                = $attr['attribute_id'];
                            $attributeIdCollection[] = $attr['attribute_id'];
                        } else {
                            $this->_logError(
                                "[ERROR] Skipping sku:" . $data['sku'] .
                                " The attribute code :" . $attributeCode .
                                " should be a dropdown type 
                                and the scope should be global"
                            );
                            $this->_skippedFileRows++;
                            return false;
                        }
                    } else {
                        $this->_logError(
                            "[ERROR] Skipping sku:" . $data['sku'] .
                            " Invalid configurable_attributes 
                            or the attribute code :"
                            . $attributeCode . " is not made configurable"
                        );
                        $this->_skippedFileRows++;
                        return false;
                    }
                } else {
                    $attributeIdCollection[] = $this->_attributeId[$attributeCode];
                }

                /*Check if the attribute is assigned to the given attribute set*/
                if (
                !$this->checkAttributeAssiged(
                    $this->_attributeId[$attributeCode],
                    $data['attribute_set']
                )
                ) {
                    $this->_logError(
                        "[ERROR] Skipping sku:" . $data['sku'] .
                        " As attribute code:" . $attributeCode .
                        " is not assigned to attributeSet: " .
                        $data['attribute_set']
                    );
                    $this->_skippedFileRows++;
                    return false;
                }
            }
        } else {
            $this->_logError(
                "[ERROR] Skipping sku:" . $data['sku'] .
                " configurable_attributes is required to 
                create configurable products."
            );
            $this->_skippedFileRows++;
            return false;
        }
        //attribute ID of attribute
        $product->getTypeInstance()
            ->setUsedProductAttributeIds($attributeIdCollection);
        $configurableAttributesData = $product->getTypeInstance()
            ->getConfigurableAttributesAsArray();
        $product->setCanSaveConfigurableAttributes(true);
        $product->setConfigurableAttributesData($configurableAttributesData);

        $configurableProductsData = array();
        foreach ($this->_confAssociateProduct[$data['sku']] as $simpleSku) {
            $simpleProObj = Mage::getModel('catalog/product')
                ->loadByAttribute('sku', $simpleSku);

            $options = array();
            foreach ($configurableAttribute as $attributeCode) {
                $attributeValue = $simpleProObj->getData($attributeCode);
                if (!$attributeValue) {
                    $this->_logError(
                        "[WARNING] Sku: " .
                        $simpleSku . " missing " . $attributeCode .
                        " value, product can not be associated to " .
                        $data['sku']
                    );
                }
                $option = array();
                $option['label'] = $simpleProObj->getAttributeText($attributeCode);
                $option['attribute_id'] = $this->_attributeId[$attributeCode];
                $option['value_index'] = $attributeValue;
                $option['is_percent'] = 0;
                $option['pricing_value'] = '';
                $options[] = $option;
            }
            $configurableProductsData[$simpleProObj->getId()] = $options;
        }
        $product->setConfigurableProductsData($configurableProductsData);
        return true;
    }


    /** Check if an attribute is assigned to given attribute set
     * @param $attributeId
     * @param $attributeSet
     *
     * @return boolen
     */
    public function checkAttributeAssiged($attributeId, $attributeSet)
    {
        if (!isset($this->_attributeAssigned[$attributeSet])) {
            $resource = Mage::getSingleton('core/resource');
            $readConnection = $resource->getConnection('core_read');
            $query = "SELECT a.attribute_id FROM " . $resource->getTableName('eav_entity_attribute') .
                " as a  left join " . $resource->getTableName('eav_attribute_set') .
                " as s  on a.attribute_set_id = s.attribute_set_id where s.attribute_set_name = :attributeset";

            $params = array(':attributeset' => $attributeSet);
            $results = $readConnection->query($query,$params)->fetchAll();
            $this->_attributeAssigned[$attributeSet] = array_column($results, 'attribute_id');
        }
        if (in_array($attributeId, $this->_attributeAssigned[$attributeSet])) {
            return true;
        }
        return false;
    }

    /**
     * Set product model default values
     *
     * @param Mage_Catalog_Model_Product $product
     * @param array $data
     */
    protected function _setDefaults($product, $data)
    {
        /* Set product type*/
        if ($data['product_specifications'] == 'configurable_product') {
            $product->setTypeId(Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE);
        } else {
            $product->setTypeId(Mage_Catalog_Model_Product_Type::TYPE_SIMPLE);
        }
        // This will format itself trough observer
        $product->setUrlKey($data['name']);
        $product->setProductVisibilityStrategy(
            json_encode(
                array(
                    'strategy' => Vznl_ProductFilter_Model_Filter::STRATEGY_INCLUDE_ALL,
                    'groups' => null,
                )
            )
        );
        $product->setVisibility(
            Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH
        );
        $product->setIsDeleted('0');
    }

    /**
     * Process custom data mapping for mobile products
     *
     * @param array $data
     *
     * @return array
     */
    protected function _setDataMapping($data)
    {
        // Map websites if set or mark telesales as default
        $importHelper = Mage::helper('vznl_import');
        if (!empty($data['channel']) && $data['channel'] == '*') {
            $data['_product_websites'] = $importHelper->getAllWebsitesCodes();
        } else {
            $data['_product_websites'] = !empty($data['channel']) ?
                strtolower(str_replace(' ', '', $data['channel'])) :
                'telesales';
        }

        $header_mappings
            = array_flip(
                $this->_helper->getMapping('product_template')
        );

        foreach ($header_mappings as $new => $old) {
            if (!isset($data[$new]) && isset($data[$old])) {
                if (!empty($data[$old])) {
                    $data[$new] = $data[$old];
                }
                unset($data[$old]);
            }
        }
        // SKU format as specified in CAT-11_Product Catalogue HLD_version_2.4
        $data['package_subtype']
            = $data['package_subtype'] == "add on" ?
            "Addon"
            : $data['package_subtype'];

        // new name => old name
        $renamedFields = [];

        $renamedFields = array_merge($renamedFields, $header_mappings);

        foreach ($renamedFields as $new => $old) {
            if (empty($data[$new]) && !empty($data[$old])) {
                $data[$new] = $data[$old];
                unset($data[$old]);
            }
        }

        Mage::helper("dyna_import/data")->preventExtraColumns($data);

        $priceFields = [
            'maf',
            'price',
            'new_price',
            'prijs_belminuut_buiten_bundel',
            'prijs_mb_buiten_bundel',
            'prijs_thuiskopieheffing_bedrag',
            'special_price',
            'maf_discount',
            'price_discount',
            'hawaii_subscr_device_price',
        ];

        foreach ($priceFields as $pricefield) {
            if (!empty($data[$pricefield])) {
                $data[$pricefield] = $this->getFormattedPrice($data[$pricefield]);
            }
        }
        unset($pricefield);

        $dateTimeFields = array(
            'effective_date',
            'expiration_date',
            'new_price_date',
            'special_from_date',
            'special_to_date',
        );

        foreach ($dateTimeFields as $dateTimeField) {
            if (!empty($data[$dateTimeField])) {
                $data[$dateTimeField] = date(
                    "Y-m-d H:i:s",
                    strtotime($data[$dateTimeField])
                );
            }
        }

        /**
         * Text retrieved for service_description attribute needs
         * to be parsed as BB code
         */
        $bbParser = new Dyna_Mobile_Model_Import_BBParser();
        if (!empty($data['additional_text'])) {
            $bbParser->setText($data['additional_text']);
            $data['additional_text'] = $bbParser->parseText();
        }

        /**
         * Get value for checkout product attribute
         */
        $data['checkout_product'] = $this->getCheckoutProductValue($data);

        /**
         * Get correct value for product visibility attribute
         */
        if (isset($data['product_visibility'])) {
            $data['product_visibility']
                = $this->parseProductVisibility($data['product_visibility']);
        }

        return $data;
    }

    public function getFileLogName($productFileName)
    {
        return strtolower($productFileName) . '_import.' . $this->_logFileExtension;
    }

    /**
     * @param $price
     *
     * @return string
     */
    protected function getFormattedPrice($price)
    {
        $price = str_replace(',', '.', $price);

        return number_format($price, 4, '.', null);
    }

    /**
     * Determine checkout product attribute value
     *
     * @param array $data
     *
     * @return string
     */
    protected function getCheckoutProductValue($data)
    {
        if (isset($data['checkout_product']) && $data['checkout_product']) {
            return ('yes' == strtolower($data['checkout_product'])) ? 'Yes' : 'No';
        }
        return 'No';
    }

    /**
     * Set product status
     *
     * @param Mage_Catalog_Model_Product $product
     * @param array $data
     */
    protected function _setStatus($product, $data)
    {
        if (array_key_exists('status', $data) && $data['status']) {
            switch (strtolower($data['status'])) {
                case 'enabled':
                case 'ingeschakeld':
                    $product->setStatus(1);
                    break;
                case 'disabled':
                case 'uitgeschakeld':
                default:
                    $product->setStatus(2);
                    break;
            }
        } else {
            $product->setStatus(2);
        }
    }

    /**
     * Log function
     *
     * @param string $msg
     * @param bool $verbose
     */
    public function _log($msg, $verbose = false)
    {
        if ($verbose && !$this->_debug) {
            return;
        }

        if ($this->_currentProductSku) {
            $msg = '[' . $this->_currentProductSku . '] ' . $msg;
        }

        $this->_helper->logMsg($msg, false);
    }

    /**
     * Log function
     *
     * @param string $msg
     */
    public function _logError($msg)
    {
        if ($this->_currentProductSku) {
            $msg = '[' . $this->_currentProductSku . '] ' . $msg;
        }

        $this->_helper->logMsg($msg);
    }

    public function clearStockData($model, $productId)
    {
        $coreConnection = Mage::getSingleton('core/resource');
        $connection = $coreConnection->getConnection('core_write');
        $resTableName = Mage::getResourceSingleton($model);
        $tableName = $resTableName->getMainTable();
        if (($connection->delete($tableName,
            [
                'product_id = ?' => $productId,
            ])
        )
        )
        {
            return "Data removed";
        }
    }

    public function clearSuperAttributeData($model, $productId)
    {
        $coreConnection = Mage::getSingleton('core/resource');
        $connection = $coreConnection->getConnection('core_write');
        $tableName = Mage::getSingleton('core/resource')->getTableName($model);
        if (($connection->delete($tableName,
            [
                'product_id = ?' => $productId,
            ])
        )
        )
        {
            return "Data removed";
        }
    }

    protected function _createStockItem($product)
    {
        $product->setStockData([
            'inventory_use_config_manage_stock' => 1,
            'inventory_use_config_min_sale_qty' => 1,
            'inventory_use_config_max_sale_qty' => 1,
            'inventory_use_config_enable_qty_increments' => 1,
            'is_in_stock' => 0,
            'qty' => 0,
            'manage_stock' => 0
        ]);
    }
}
