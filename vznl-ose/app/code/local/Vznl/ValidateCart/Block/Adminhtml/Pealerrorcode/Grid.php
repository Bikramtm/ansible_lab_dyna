<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Vznl_ValidateCart_Block_Adminhtml_Pealerrorcode_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('pealErrorcodeGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('validatecart/errorcode_collection');
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * Create the error code list
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareColumns()
    {
        $this->addColumn('entity_id', array(
            'header'    => Mage::helper('vznl_checkout')->__('ID'),
            'align'     =>'right',
            'width'     => '30px',
            'index'     => 'entity_id',
        ));

        $this->addColumn('service', array(
            'header'        => Mage::helper('vznl_checkout')->__('Service'),
            'align'         => 'left',
            'index'         => 'service',
        ));

        $this->addColumn('code', array(
            'header'        => Mage::helper('vznl_checkout')->__('Error Code'),
            'align'         => 'left',
            'index'         => 'code',
        ));

        $this->addColumn('error', array(
            'header'    => Mage::helper('vznl_checkout')->__('Response Message'),
            'align'     => 'left',
            'index'     => 'error',
            
        ));

        $this->addColumn('translation', array(
            'header'    => Mage::helper('vznl_checkout')->__('Translation'),
            'align'     => 'left',
            'index'     => 'translation',

        ));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('entity_id' => $row->getEntityId()));
    }
}
