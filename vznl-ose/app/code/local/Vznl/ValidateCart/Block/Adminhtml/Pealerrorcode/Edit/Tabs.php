<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Vznl_ValidateCart_Block_Adminhtml_Pealerrorcode_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

    /**
     * Vznl_ValidateCart_Block_Adminhtml_Pealerrorcode_Edit_Tabs constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('peal_errorcode');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('operator')->__('Item Information'));
    }

    /**
     * @return Mage_Core_Block_Abstract
     * @throws Exception
     */
    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
            'label' => Mage::helper('vznl_checkout')->__('Item Information'),
            'title' => Mage::helper('vznl_checkout')->__('Item Information'),
            'content' => $this->getLayout()->createBlock('validatecart/adminhtml_pealerrorcode_edit_tab_form')->toHtml(),
        ));

        return parent::_beforeToHtml();
    }
}
