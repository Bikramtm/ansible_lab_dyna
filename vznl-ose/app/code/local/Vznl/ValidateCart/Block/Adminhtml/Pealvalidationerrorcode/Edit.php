<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Vznl_ValidateCart_Block_Adminhtml_Pealvalidationerrorcode_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * Vznl_ValidateCart_Block_Adminhtml_Pealvalidationerrorcode_Edit constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_objectId = 'entity_id';
        $this->_blockGroup = 'validatecart';
        $this->_controller = 'adminhtml_pealvalidationerrorcode';

        $this->_updateButton('save', 'label', Mage::helper('vznl_checkout')->__('Save Error Code'));

        $objId = $this->getRequest()->getParam($this->_objectId);

        if (!empty($objId)) {
            $this->_addButton('delete', array(
                'label' => Mage::helper('vznl_checkout')->__('Delete'),
                'class' => 'delete',
                'onclick' => 'deleteConfirm(\''
                    . Mage::helper('core')->jsQuoteEscape(
                        Mage::helper('vznl_checkout')->__('Are you sure you want to do this?')
                    )
                    . '\', \''
                    . $this->getDeleteUrl()
                    . '\')',
            ));
        }
    }

    /**
     * @return string
     */
    public function getHeaderText()
    {
        if (Mage::registry('peal_validationerrorcode_data') && Mage::registry('peal_validationerrorcode_data')->getEntityId()) {
            return Mage::helper('vznl_checkout')->__("Edit peal service error code with id '%s'", $this->escapeHtml(Mage::registry('peal_validationerrorcode_data')->getEntityId()));
        } else {
            return Mage::helper('vznl_checkout')->__('Add new peal validation error code');
        }
    }
}
