<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Vznl_ValidateCart_Block_Adminhtml_Pealvalidationerrorcode_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('pealValidationerrorcodeGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('validatecart/validationerrorcode_collection');
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * Create the error code list
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareColumns()
    {
        $this->addColumn('entity_id', array(
            'header'    => Mage::helper('vznl_checkout')->__('ID'),
            'align'     =>'right',
            'width'     => '30px',
            'index'     => 'entity_id',
        ));

        $this->addColumn('code', array(
            'header'        => Mage::helper('vznl_checkout')->__('Error Code'),
            'align'         => 'left',
            'index'         => 'code',
        ));

        $this->addColumn('error', array(
            'header'    => Mage::helper('vznl_checkout')->__('Error Message'),
            'align'     => 'left',
            'index'     => 'error',

        ));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('entity_id' => $row->getEntityId()));
    }
}
