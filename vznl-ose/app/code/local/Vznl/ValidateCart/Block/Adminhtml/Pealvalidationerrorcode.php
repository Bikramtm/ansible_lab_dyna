<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Vznl_ValidateCart_Block_Adminhtml_Pealvalidationerrorcode extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * Vznl_ValidateCart_Block_Adminhtml_Pealvalidationerrorcode constructor.
     */
    public function __construct()
    {
        $this->_controller = 'adminhtml_pealvalidationerrorcode';
        $this->_blockGroup = 'validatecart';
        $this->_headerText = 'Peal Validation Error Codes';
        $this->_addButtonLabel = 'Add new error code';
        parent::__construct();
    }
}