<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Vznl_ValidateCart_Block_Adminhtml_Pealerrorcode extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * Vznl_Validatecart_Block_Adminhtml_Pealerrorcode constructor.
     */
    public function __construct()
    {
        $this->_controller = 'adminhtml_pealerrorcode';
        $this->_blockGroup = 'validatecart';
        $this->_headerText = 'Peal Service Error Codes';
        $this->_addButtonLabel = 'Add new error code';
        parent::__construct();
    }
}