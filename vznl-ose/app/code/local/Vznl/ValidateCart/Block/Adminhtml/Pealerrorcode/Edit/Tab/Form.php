<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Vznl_ValidateCart_Block_Adminhtml_Pealerrorcode_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $data = Mage::registry('peal_errorcode_data');
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $dataFieldset = $form->addFieldset('reason', array('legend' => Mage::helper('vznl_checkout')->__('Peal Service Error Code details')));

        $dataFieldset->addField('service', 'select', array(
            'label' => Mage::helper('vznl_checkout')->__('Service'),
            'name' => 'service',
            "class" => "required-entry",
            "required" => true,
            'value' => $data->getService(),
            'values' => [
                "*" => "*",
                Vznl_AddItemToBasket_Adapter_Peal_Adapter::name => "Add Item To Basket",
                Vznl_CheckoutOptions_Adapter_Peal_Adapter::name => "Checkout Options",
                Vznl_CreateBasket_Adapter_Peal_Adapter::name => "Create Basket",
                Vznl_DeliveryWishDates_Adapter_Peal_Adapter::name => "Delivery Wish Dates",
                Vznl_GetBasket_Adapter_Peal_Adapter::name => "Get Basket",
                Vznl_GetOrderStatus_Adapter_Peal_Adapter::name => "Get Order Status",
                Vznl_GetTelephoneNumbers_Adapter_Peal_Adapter::name => "Get Telephone Numbers",
                Vznl_RemoveItemFromBasket_Adapter_Peal_Adapter::name => "Remove Item From Basket",
                Vznl_ReserveTelephoneNumber_Adapter_Peal_Adapter::name => "Reserve Telephone Number",
                Vznl_ResetBasket_Adapter_Peal_Adapter::name => "Reset Basket",
                Vznl_RetainTelephoneNumbers_Adapter_Peal_Adapter::name => "Retain Telephone Numbers",
                Vznl_ValidateBasket_Adapter_Peal_Adapter::name => "Validate Basket",
                Vznl_ValidateSerialNumber_Adapter_Peal_Adapter::name => "Validate Serial Number",
                $this->__('Serviceability Check') => "Serviceability Check",
                $this->__('Validate Address') => "Validate Address"
            ]
        ));
        $dataFieldset->addField('code', 'text', array(
            'label' => Mage::helper('vznl_checkout')->__('Error Code'),
            'name' => 'code',
            "class" => "required-entry",
            "required" => true,
            'value' => $data->getCode(),
        ));

        $dataFieldset->addField('error', 'text', array(
            'label' => Mage::helper('vznl_checkout')->__('Error Description'),
            'name' => 'error',
            "class" => "required-entry",
            "required" => true,
            'value' => $data->getError(),
        ));

        $dataFieldset->addField('translation', 'text', array(
            'label' => Mage::helper('vznl_checkout')->__('Translation'),
            'name' => 'translation',
            "class" => "required-entry",
            "required" => true,
            'value' => $data->getTranslation(),
        ));

        return parent::_prepareForm();
    }
}
