<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */

/**
 * Class Vznl_ValidateCart_Block_Adminhtml_Pealsubstatus_Grid
 */
class Vznl_ValidateCart_Block_Adminhtml_Pealsubstatus_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('pealSubstatus');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('validatecart/pealsubstatus_collection');
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * Create peal sub status list
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareColumns()
    {
        $this->addColumn('entity_id', array(
            'header'    => Mage::helper('vznl_checkout')->__('ID'),
            'align'     =>'right',
            'width'     => '30px',
            'index'     => 'entity_id',
        ));

        $this->addColumn('substatus', array(
            'header'        => Mage::helper('vznl_checkout')->__('PEAL Sub Status'),
            'align'         => 'left',
            'index'         => 'substatus',
        ));

        $this->addColumn('translation', array(
            'header'    => Mage::helper('vznl_checkout')->__('Translated Values'),
            'align'     => 'left',
            'index'     => 'translation',
            
        ));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('entity_id' => $row->getEntityId()));
    }
}
