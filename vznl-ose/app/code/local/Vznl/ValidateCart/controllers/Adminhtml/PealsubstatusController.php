<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */

/**
 * Class Vznl_ValidateCart_Adminhtml_PealsubstatusController
 */
class Vznl_ValidateCart_Adminhtml_PealsubstatusController extends Mage_Adminhtml_Controller_Action
{

  /**
     * @return $this
     */
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('system/peal_substatus')
            ->_addBreadcrumb(Mage::helper('omnius_checkout')->__('Manual activation reasons'), Mage::helper('omnius_checkout')->__('Manual activation reasons'));

        return $this;
    }

    /**
     * Display the admin grid (table) with all peal sub statuses
     */
    public function indexAction()
    {
       $this->_initAction()->renderLayout();
    }

    /**
     * Gather data and generate edit form
     *
     */
    public function editAction()
    {
        $id = $this->getRequest()->getParam('entity_id');
        $model = Mage::getModel('validatecart/pealsubstatus')->load($id);

        if ($model->getEntityId() || $id == 0) {
            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
            if (!empty($data)) {
                $model->addData($data);
            }

            Mage::register('peal_substatus_data', $model);

            $this->loadLayout();
            $this->_setActiveMenu('system/peal_substatus');
            
            $this->_addContent($this->getLayout()->createBlock('validatecart/adminhtml_pealsubstatus_edit'))
                 ->_addLeft($this->getLayout()->createBlock('validatecart/adminhtml_pealsubstatus_edit_tabs'));
            
            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('omnius_checkout')->__('Item does not exist'));
            $this->_redirect('*/*/');
        }
    }

    /**
     * Admin add a new peal sub status, forwards to edit action
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    /**
     * Save a new or existing peal sub status.
     */
    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            try {
                $id = $this->getRequest()->getParam('entity_id');
                $model = Mage::getModel('validatecart/pealsubstatus')->load($id);
                $model->addData($data);
                $model->save();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('omnius_checkout')->__('The Peal Sub status was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', ['entity_id' => $model->getEntityId()]);
                    return;
                }
            } catch (Exception $e) {
                $this->_throwErrror($e->getMessage(), $data);

                return;
            }
            $this->_redirect('*/*/');
            return;
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('omnius_checkout')->__('Unable to find item to save'));
        $this->_redirect('*/*/');
    }

    /**
     * Method to deleted a peal sub status
     *
     */
    public function deleteAction()
    {
        $id = $this->getRequest()->getParam('entity_id');
        if ($id > 0) {
            try {
                $model = Mage::getModel('validatecart/pealsubstatus')->load($id);
                if (!$model->getId()) {
                    throw new Exception('Peal Sub status with the provided ID not found');
                }
                $model->delete();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('omnius_checkout')->__('Item was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit',
                    array('entity_id' => $id));
            }
        }
        $this->_redirect('*/*/');
    }

    protected function _isAllowed()
    {
        return true;
    }
}
