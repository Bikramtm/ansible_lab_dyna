<?php

/**
 * Class Vznl_ValidateCart_ValidateController
 */
class Vznl_ValidateCart_ValidateController extends Mage_Core_Controller_Front_Action
{

    private $_dynaCoreHelper = null;

    /**
     * @return Omnius_Core_Helper_Data
     */
    protected function getDynaCoreHelper()
    {
        if ($this->_dynaCoreHelper === null) {
            $this->_dynaCoreHelper = Mage::helper('omnius_core');
        }

        return $this->_dynaCoreHelper;
    }

    /**
     * Helper method used to return a response as json
     * @param array $data
     * @param int $statusCode Response status code
     */
    protected function jsonResponse($data, $statusCode = 200)
    {
        $response = $this->getDynaCoreHelper()
            ->jsonResponse($data, $statusCode);

        $this->getResponse()
            ->setBody($response['body'])
            ->setHeader('Content-Type', 'application/json')
            ->setHttpResponseCode($response['status']);
    }

    public function consolidateAction()
    {
        $cartPackages = Mage::getSingleton('checkout/cart')->getQuote()->getCartPackages();
        $process_contexts = [];
        $consolidate = ($this->getRequest()->getPost('consolidate') == 'true') ? 1 : 0;
        $hasHardwareProduct = false;

        $Helper = Mage::helper('vznl_validatecart');
        $Helper->setConsolidatedata($consolidate);
        $Helper->removePealAddedProduct();
        $customerSession = Mage::getSingleton('dyna_customer/session');
        $terminationReasons = $customerSession->getData('partial_termination');
        $validateCart = $Helper->consolidateValidate($consolidate, $terminationReasons);

        $cartPackages = Mage::getSingleton('checkout/cart')->getQuote()->getCartPackages();
        $process_contexts = [];

        if ($validateCart === 'TRUE') {
            if ($cartPackages) {
                foreach ($cartPackages as $cartPackage) {
                    $cartPackage->updateUseCaseIndication($cartPackage);
                    $process_contexts[$cartPackage->getPackageId()] = $cartPackage->getSaleType();
                }
            }
            $html = Mage::getSingleton('core/layout')->createBlock('dyna_configurator/rightsidebar')
                ->setTemplate('customer/expanded_cart.phtml')->toHtml();
            $cartProducts = [];
            foreach (Mage::getModel('checkout/session')->getQuote()->getAllItems() as $item) {
                $packageId = $item->getPackageId();
                $cartProducts[$packageId]['products'][] = $item->getProductId();
                $cartProducts[$packageId]['type'] = $item->getPackageType();
                $cartProducts[$packageId]['process_context'] = $process_contexts[$packageId];
                if($item->getProduct()->is(Vznl_Catalog_Model_Type::SUBTYPE_FIXED_PACKAGE)) {
                    $packageName = $item->getName();
                }
                if ($item->getAddedViaPeal()) {
                    $hasHardwareProduct = true;
                }
            }
            foreach ($cartProducts as $packageId => $package) {
                sort($package['products']);
                $cartProducts[$packageId] = [
                    'products' => $package['products'],
                    'type' => $package['type'],
                    'process_context' => $package['process_context'],
                ];
            }
            if ($hasHardwareProduct) {
                $hasHardwareProduct = $packageName;
            }
            $response = Mage::helper('omnius_core')->jsonResponse(['html' => $html, 'cartProducts' => md5(serialize($cartProducts)), "reload" => false, "callValidatecart" => true, "hasHardwareProduct" => $hasHardwareProduct]);
            $this->getResponse()
                ->setBody($response['body'])
                ->setHeader('Content-Type', 'application/json');
        } else {
            $response = Mage::helper('omnius_core')->jsonResponse(['reload' => true, 'callValidatecart' => true, "hasHardwareProduct" => false]);
            $this->getResponse()
                ->setBody($response['body'])
                ->setHeader('Content-Type', 'application/json');
        }
    }

    public function consolidatenoAction()
    {
        $Helper = Mage::helper('vznl_validatecart');
        $Helper->setConsolidatedata(0);
    }

    public function validateCancelledItemsAction() {
        $callValidateCart = Mage::getSingleton('checkout/cart')->getQuote()->hasFixed();
        $helper = Mage::helper('vznl_validatecart');

        // Send validate basket request to peal with cancellation reasons
        if ($callValidateCart) {

            $terminationReasons = [
                'reason' => $this->getRequest()->getParam('termination_reason'),
                'subreason' => $this->getRequest()->getParam('termination_subreason'),
                'competitor' => $this->getRequest()->getParam('termination_competitor')
            ];

            $customerSession = Mage::getSingleton('dyna_customer/session');
            $customerSession->setData('partial_termination', $terminationReasons);

            $validateCart = $helper->validate(true, $terminationReasons);
            if (isset($validateCart['error'])) {
                $this->jsonResponse($validateCart);
                return;
            }
        } else {
            $validateCart = "TRUE";
        }

        $response = $helper->processValidateCart($validateCart, $callValidateCart);
        $this->jsonResponse($response);
    }
}
