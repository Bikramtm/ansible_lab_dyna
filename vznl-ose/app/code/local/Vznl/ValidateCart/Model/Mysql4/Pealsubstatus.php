<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */

/**
 * Class Vznl_ValidateCart_Model_Mysql4_Pealsubstatus
 */
class Vznl_ValidateCart_Model_Mysql4_Pealsubstatus extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init("validatecart/pealsubstatus", "entity_id");
    }
}
