<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Vznl_ValidateCart_Model_Mysql4_Whitelistaction_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
	/**
     * Override constructor
     */
    public function _construct()
    {
        $this->_init("validatecart/whitelistaction");
    }
}
