<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */

/**
 * Class Vznl_ValidateCart_Model_Mysql4_Pealsubstatus_Collection
 */
class Vznl_ValidateCart_Model_Mysql4_Pealsubstatus_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        $this->_init("validatecart/pealsubstatus");
    }
}
