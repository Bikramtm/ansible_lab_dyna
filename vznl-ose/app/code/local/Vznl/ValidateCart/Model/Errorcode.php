<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Vznl_ValidateCart_Model_Errorcode extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init("validatecart/errorcode");
    }

    protected function _beforeSave()
    {
        if ($this->getData('created_at') === null) {
            $this->setData('created_at', now());
        } else {
            $this->setData('updated_at', now());
        }

        return parent::_beforeSave();
    }

    public function getByCode($code,$message,$service)
    {
        $collection =  $this->getCollection()
            ->addFieldToFilter('code', $code)
            ->addFieldToFilter('error', $message)
            ->addFieldToFilter('service', $service)
            ->setPageSize(1, 1)
            ->getLastItem();
        if (empty($collection)) {
            $collection = $this->getCollection()
                ->addFieldToSelect('*')
                ->addFieldToFilter('code', $code)
                ->addFieldToFilter('error', $message)
                ->getLastItem();
        }

        if (empty($collection['translation'])) {
            $collection['translation'] = $message;
        }
        return $collection;
    }
}
