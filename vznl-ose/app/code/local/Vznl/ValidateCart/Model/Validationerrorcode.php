<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Vznl_ValidateCart_Model_Validationerrorcode extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init("validatecart/validationerrorcode");
    }

    protected function _beforeSave()
    {
        if ($this->getData('created_at') === null) {
            $this->setData('created_at', now());
        } else {
            $this->setData('updated_at', now());
        }

        return parent::_beforeSave();
    }

    public function getByValidationCode($code)
    {
        return $this->getCollection()
            ->addFieldToFilter('code', $code)
            ->setPageSize(1, 1)
            ->getLastItem();
    }
}
