<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Vznl_ValidateCart_Model_Mapper extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init("validatecart/mapper");
    }

    public function getBySku($sku, $mapper)
    {
       return $this->getCollection()
           ->addFieldToFilter('sku', $sku)
           ->addFieldToFilter('direction', array('IN' => ['both', $mapper]))
           ;
    }
}
