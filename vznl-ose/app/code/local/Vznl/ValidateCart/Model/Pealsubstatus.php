<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */

/**
 * Class Vznl_ValidateCart_Model_Pealsubstatus
 */
class Vznl_ValidateCart_Model_Pealsubstatus extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init("validatecart/pealsubstatus");
    }

    protected function _beforeSave()
    {
        if ($this->getData('created_at') === null) {
            $this->setData('created_at', now());
        } else {
            $this->setData('updated_at', now());
        }

        return parent::_beforeSave();
    }

    /**
     * Load entity by substatus
     *
     * @param string $substatus
     * @return bool|Vznl_ValidateCart_Model_Pealsubstatus
     */
    public function loadBySubstatus($substatus)
    {
        $translation = $this->getCollection()
            ->addFieldToFilter('substatus', $substatus)
            ->setPageSize(1, 1)
            ->getLastItem();
        return $translation ? $translation : false;
    }

}
