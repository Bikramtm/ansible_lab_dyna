<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Vznl_ValidateCart_Model_Whitelistaction extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init("validatecart/whitelistaction");
    }

    protected function _beforeSave()
    {
        if ($this->getData('created_at') === null) {
            $this->setData('created_at', date("Y-m-d H:i:s"));
        } else {
            $this->setData('updated_at', date("Y-m-d H:i:s"));
        }

        return parent::_beforeSave();
    }
}
