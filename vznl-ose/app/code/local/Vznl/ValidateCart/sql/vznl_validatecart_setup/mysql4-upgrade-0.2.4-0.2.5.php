<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * @category    Vznl
 * @package     Vznl_ValidateCart
 */

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();

$installer->getConnection()->modifyColumn($installer->getTable("sales/quote"), 'consolidate', [
    "type" => Varien_Db_Ddl_Table::TYPE_INTEGER,
    "default" => null
]);

$installer->getConnection()->modifyColumn($installer->getTable("sales/order"), 'consolidate', [
    "type" => Varien_Db_Ddl_Table::TYPE_INTEGER,
    "default" => null
]);

$installer->endSetup();