<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 *  Installer for updating agent table field properties:
 *  code: changing it to VARCHAR 250
 */

/**
 * @category    Vznl
 * @package     Vznl_ValidateCart
 */

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();

$installer->getConnection()->modifyColumn($installer->getTable("validatecart/errorcode"), 'code', [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 250,
    "default" => null
]);

$installer->endSetup();
