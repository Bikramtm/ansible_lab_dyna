<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */

/**
 * @category    Vznl
 * @package     Vznl_Validate
 */

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();

/**
 * Create table
 */
if (!$installer->tableExists($installer->getTable('validatecart/pealsubstatus'))) {
    $table = $installer->getConnection()
        ->newTable($installer->getTable('validatecart/pealsubstatus'))
        ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity'  => true,
            'unsigned'  => true,
            'nullable'  => false,
            'primary'   => true,
        ), 'ID')
        ->addColumn('substatus', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        ), 'Substatus')
        ->addColumn('translation', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        ), 'Translation')
        ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        ), 'Created')
        ->addColumn('updated_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        ), 'Updated')

        ->addIndex($installer->getIdxName($installer->getTable('validatecart/pealsubstatus'), array('substatus'), Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE),
            array('substatus'), array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE))
        ->setComment('Vznl Peal Sub statuses');

    $installer->getConnection()->createTable($table);
}

$installer->endSetup();