<?php

$installer = $this;

$installer->startSetup();

$sql = <<<SQL
ALTER TABLE sales_flat_quote
  ADD COLUMN consolidate INTEGER (10) DEFAULT 0,
  ADD COLUMN basket_id VARCHAR (45) DEFAULT NULL
SQL;

$installer->getConnection()->query($sql);

$installer->endSetup();

