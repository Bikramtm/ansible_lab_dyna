<?php

class Vznl_Transaction_ServiceController extends Mage_Core_Controller_Front_Action
{
    /**
     * Error codes
     */
    const ERROR_INVALID_REQUEST_METHOD = 1;
    const ERROR_TYPE_NOT_VALID = 10;
    const ERROR_HTTPCODE_NOT_VALID = 11;
    const ERROR_AGENT_NOT_FOUND = 12;
    const ERROR_ORDER_NOT_FOUND = 13;

    public function indexAction()
    {
        try {
            if (!$this->getRequest()->isPost()) {
                throw new Vznl_Transaction_Exception_InvalidRequestException('Invalid request method', 501);
            }

            $errors = [];
            $params = $this->getRequest()->getPost();
            $errorMessage = isset($params['error']) && !empty($params['error']) ? $params['error'] : null;
            if (!isset($params['type']) || empty($params['type'])) {
                $errors[] = ['code' => self::ERROR_TYPE_NOT_VALID, 'message' => 'Type parameter not set or not valid'];
            }
            if (!isset($params['httpcode']) || empty($params['httpcode'])) {
                $errors[] = ['code' => self::ERROR_HTTPCODE_NOT_VALID, 'message' => 'Httpcode parameter not set or not valid'];
            }
            $agent = Mage::getModel('agent/agent')->load($params['agent_id']);
            if (is_null($agent->getId())) {
                $errors[] = ['code' => self::ERROR_AGENT_NOT_FOUND, 'message' => 'Agent not found'];
            }
            $order = null;
            if (isset($params['order_id'])) {
                $order = Mage::getModel('sales/order')->load($params['order_id']);
                if (is_null($order->getId())) {
                    $errors[] = ['code' => self::ERROR_ORDER_NOT_FOUND, 'message' => 'Order not found'];
                }
            }

            if (count($errors) === 0) {
                $responsecode = 200;
                /** @var Vznl_Transaction_Helper_Data $helper */
                $helper = Mage::helper('transaction');
                $transaction = $helper->save(
                    $params['type'],
                    $params['httpcode'],
                    $agent,
                    $errorMessage,
                    $order
                );
                $data['data'][] = [
                    'type' => 'transaction',
                    'id' => $transaction->getId(),
                    'attributes' => $transaction->jsonSerialize(),
                    'links' => [
                        'self' => Mage::getUrl().'transaction/service/'.$transaction->getId(),
                    ],
                ];
                $data['links']['self'] = Mage::getUrl().'transaction/service';
            } else {
                $responsecode = 400;
                $data['errors'] = $errors;
            }

            // Send respone
            $this->getResponse()->setHeader('Content-Type', 'application/json');
            $this->getResponse()->setBody(json_encode($data));
            $this->getResponse()->setHttpResponseCode($responsecode);

        } catch (Exception $e) {
            Mage::logException($e);
            $this->getResponse()->setHeader('Content-Type', 'application/json');
            $this->getResponse()->setBody(json_encode([
                'errors'=> [
                    'code' => self::ERROR_INVALID_REQUEST_METHOD,
                    'message' => $e->getMessage()
                ]
            ]));
            $this->getResponse()->setHttpResponseCode($e->getCode());
        }
    }
}
