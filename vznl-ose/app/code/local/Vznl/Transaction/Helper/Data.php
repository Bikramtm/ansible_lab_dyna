<?php

class Vznl_Transaction_Helper_Data extends Mage_Core_Helper_Abstract
{
    const CONTRACT_PRINT_TYPE = 'contract.print';
    const CONTRACT_SIGN_TYPE = 'contract.sign';

    /**
     * Save a transaction to the RDBMS
     *
     * @param $type
     * @param $httpcode
     * @param Vznl_Agent_Model_Agent $agent
     * @param null $error
     * @param Vznl_Checkout_Model_Sales_Order|null $order
     * @return Mage_Core_Model_Abstract
     */
    public function save($type, $httpcode, Vznl_Agent_Model_Agent $agent, $error = null, Vznl_Checkout_Model_Sales_Order $order = null)
    {
        $transaction = Mage::getModel('transaction/transaction');
        $transaction->setType($type);
        $transaction->setHttpcode($httpcode);
        $transaction->setAgentId($agent ? $agent->getId(): null);
        $transaction->setError($error);
        $transaction->setOrderId($order ? $order->getId(): null);
        $transaction->setCreatedAt(date('Y-m-d H:i:s'));
        $transaction->save();

        return Mage::getModel('transaction/transaction')->load($transaction->getId());
    }
}
