<?php

/**
 * Class Vznl_ProductMatchRule_Model_Indexer_Productmatch
 */
class Vznl_ProductMatchRule_Model_Indexer_Productmatch extends Dyna_ProductMatchRule_Model_Indexer_Productmatch
{


	/**
     * @param $websiteId
     * @param $left
     * @param $right
     * @param $priority
     * @param $contextId
     * @return bool
     */
    public function canInsertAllowedRule($websiteId, $left, $right, $priority, $contextId)
    {

        $key = $this->ruleCache->buildKey($websiteId, $left, $right);
        $ruleInfo = $this->ruleCache->get($key);
        $prioNotAllowed = false;

        if (($contextId && strpos($ruleInfo,'.' . $contextId . '.') !== false)) {
            $prioNotAllowed = (int)strtok($ruleInfo, ".");
        }

        return $prioNotAllowed === false || $priority > $prioNotAllowed;
    }

    /**
     * Create Redis cache for not allowed rules that will be used when creating the allowed rules
     * @param $packageId
     */
    public function buildNotAllowedCache($packageId, $sourceCollectionId)
    {
        $processStartTime = microtime(true);
        $notAllowedCollection = Mage::getResourceModel('dyna_productmatchrule/rule_collection')
            ->addFieldToSelect(['product_match_rule_id', 'package_type', 'operation_type', 'priority', 'left_id', 'right_id', 'operation'])
            ->addFieldToFilter('package_type', ['eq' => (int)$packageId])
            ->addFieldToFilter('operation_type', ['in' => [
                Dyna_ProductMatchRule_Model_Rule::OP_TYPE_P2P,
                Dyna_ProductMatchRule_Model_Rule::OP_TYPE_P2C,
                Dyna_ProductMatchRule_Model_Rule::OP_TYPE_C2C,
                Dyna_ProductMatchRule_Model_Rule::OP_TYPE_C2P
            ]])
            ->addFieldToFilter('operation', ['eq' => Dyna_ProductMatchRule_Model_Rule::OP_NOTALLOWED])
            ->addFieldToFilter('source_collection', ['eq' => $sourceCollectionId])
            ->addFieldToFilter('effective_date', array(
                array('to' => date("Y-m-d"), 'date' => true),
                array('null' => true)
            ))
            ->addFieldToFilter('expiration_date', array(
                array('from' => date("Y-m-d"), 'date' => true),
                array('null' => true)
            ))
            ->setPageSize(self::BATCH_SIZE);

        $notAllowedCollection->getSelect()
            ->columns('(SELECT GROUP_CONCAT(website_id) FROM product_match_rule_website r WHERE main_table.product_match_rule_id = r.rule_id) AS website_ids')
            ->group('main_table.product_match_rule_id')
            ->order('main_table.priority', 'asc')
            ->order('main_table.product_match_rule_id', 'asc');

        $totalNotAllowedRecords = $notAllowedCollection->getSize();

        $this->productMatchRuleHelper->logIndexer("[Productmatch] Caching Not Allowed rules: " . $totalNotAllowedRecords . " Not Allowed rules.");
        $notAllowedCurrentPage = 1;
        $notAllowedPages = $notAllowedCollection->getLastPageNumber();

        do {
            $pageStartTime = microtime(true);
            $notAllowedCollection->setCurPage($notAllowedCurrentPage);
            $this->associateRulesToContexts($notAllowedCollection);

            foreach ($notAllowedCollection as $notAllowedRule) {
                $this->processNotAllowedRule($notAllowedRule);
            }

            unset($notAllowedRule);
            $pageResultTime = gmdate('H:i:s', microtime(true) - $pageStartTime);
            if ($pageResultTime >= '00:01:00') {
                $this->productMatchRuleHelper->logIndexer("Not Allowed Page " . $notAllowedCurrentPage . "/" . $notAllowedPages . " took " . $pageResultTime);
            }
            $notAllowedCurrentPage++;
            $notAllowedCollection->clear();
        } while ($notAllowedCurrentPage <= $notAllowedPages);

        $processResultTime = gmdate('H:i:s', ceil(microtime(true) - $processStartTime));
        $this->productMatchRuleHelper->logIndexer("Process Not Allowed Items took: " . $processResultTime);

        $notAllowedCollection->clear();
        unset($notAllowedCollection, $notAllowedCurrentPage);
    }
}