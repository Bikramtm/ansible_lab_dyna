<?php
class Vznl_ProductMatchRule_Model_Indexer_RemovalNotAllowed extends Dyna_ProductMatchRule_Model_Indexer_RemovalNotAllowed
{
    /**
     * Reindex all not allowed rules
     */
    protected function _reindexAll() 
    {
        // Make sure we have keys enabled before we start deleting/truncating
        $this->getConnection()->query('ALTER TABLE ' . $this->_table . ' ENABLE KEYS;');
        $this->getConnection()->query('SET UNIQUE_CHECKS = 1;');
        $this->getConnection()->query('SET AUTOCOMMIT = 1;');
        $this->start();
        if (empty($this->args)) {
            $this->productMatchRuleHelper->logIndexer("[" . static::LOG_PREFIX . "] Truncating table " . $this->_table . ".");
            $deleteStartTime = microtime(true);
            $this->getConnection()->query(sprintf('TRUNCATE TABLE `%s`', $this->_table));
            $this->closeConnection();
            $deleteResultTime = gmdate('H:i:s', microtime(true) - $deleteStartTime);
            if ($deleteResultTime <= '00:01:00') {
                $deleteResultTime = null;
            }
            $this->productMatchRuleHelper->logIndexer("[" . static::LOG_PREFIX . "] Truncated table" . (empty($deleteResultTime) ? "." : " in " . $deleteResultTime . "."));
        }

        foreach ($this->packageTypes as $packageCode => $packageId) {
            if (!empty($this->args)) {
                $this->productMatchRuleHelper->logIndexer("[" . static::LOG_PREFIX . "] Removing indexer entries for package type: " . $packageCode . ".");
                $this->getConnection()->query(sprintf('delete from `%s` where package_type=%s;', $this->_table, $packageId));
            }
            $this->productMatchRuleHelper->logIndexer("[" . static::LOG_PREFIX . "] Building indexer entries for package type: " . $packageCode . ".");

            /** @var Omnius_ProductMatchRule_Model_Resource_Rule_Collection $collection */
            $collection = Mage::getResourceModel('dyna_productmatchrule/rule_collection')
                ->addFieldToFilter('operation_type', ['in' => [
                    Dyna_ProductMatchRule_Model_Rule::OP_TYPE_P2P,
                    Dyna_ProductMatchRule_Model_Rule::OP_TYPE_P2C,
                    Dyna_ProductMatchRule_Model_Rule::OP_TYPE_C2C,
                    Dyna_ProductMatchRule_Model_Rule::OP_TYPE_C2P,
                    Dyna_ProductMatchRule_Model_Rule::OP_TYPE_S2P,
                    Dyna_ProductMatchRule_Model_Rule::OP_TYPE_S2C,
                ]])
                ->addFieldToFilter('operation',
                    array(
                        array('eq' => Dyna_ProductMatchRule_Model_Rule::OP_REMOVAL_NOTALLOWED)
                    )
                )
                ->addFieldToFilter('package_type', ['eq' => $packageId])
                ->addFieldToFilter('effective_date', array(
                    array('to' => date("Y-m-d"), 'date' => true),
                    array('null' => true)
                ))
                ->addFieldToFilter('expiration_date', array(
                    array('from' => date("Y-m-d"), 'date' => true),
                    array('null' => true)
                ));
            $collection->setPageSize(self::BATCH_SIZE);
            $currentPage = 1;
            $pages = $collection->getLastPageNumber();
            // Disabling indexing on mysql table to speed up import
            $this->getConnection()->query('ALTER TABLE ' . $this->_table . ' DISABLE KEYS;');
            $this->getConnection()->query('SET UNIQUE_CHECKS = 0;');
            $this->closeConnection();

            /**
             * Process the rules within batches to remove
             * the risk using all the allocated memory
             * only to load the rule collections
             * This way, we never load the whole
             * collection into the current memory
             */
            do {
                $collection->setCurPage($currentPage);
                $collection->load();
                /** @var Dyna_ProductMatchRule_Model_Rule $rule */
                foreach ($collection as $rule) {
                    $this->processItem($rule);
                }
                $currentPage++;
                $collection->clear();
            } while ($currentPage <= $pages);

            $this->_applyChanges();
            //reenabling indexing on mysql table after import
            $this->getConnection()->query('ALTER TABLE ' . $this->_table . ' ENABLE KEYS;');
            $this->getConnection()->query('SET UNIQUE_CHECKS = 1;');
            $this->closeConnection();
        }

        $this->end();
    }
}