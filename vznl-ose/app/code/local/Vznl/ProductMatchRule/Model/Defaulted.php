<?php

/**
 * Class Dyna_ProductMatchRule_Model_Rule
 */
class Vznl_ProductMatchRule_Model_Defaulted extends Dyna_ProductMatchRule_Model_Defaulted
{
    /**
     * Get all service expression applicable rules
     * @param array $productIds Contains the currently added products
     * @param int $websiteId
     * @param Dyna_Package_Model_Package $packageModel
     * @param $removal
     * @return array
     * @throws Dyna_ProductMatchRule_Model_Exception
     */
    public function loadDefaultedServiceRules($productIds, $websiteId, $packageModel, $removal = false)
    {
        /** @var Dyna_Catalog_Helper_ProcessContext $processContextHelper */
        $processContextHelper = Mage::helper("dyna_catalog/processContext");

        $packageType = $packageModel->getType();
        /** @var Dyna_Package_Model_PackageType $packageTypeModel */
        $packageTypeModel = Mage::getSingleton("dyna_package/packageType")
            ->loadByCode($packageType);

        $packageTypeId = $packageTypeModel->getId();
        $processContextId = $processContextHelper->getProcessContextId();

        $cacheKey = "CACHED_DEFAULTED_SERVICE_RULES_" . md5(serialize(array($productIds, $websiteId, (int)$packageTypeId, $removal, $processContextId)));

        if (!$finalCollectionSerialized = $this->getCache()->load($cacheKey)) {
            /** @var Omnius_ProductMatchRule_Model_Resource_Rule_Collection $ruleCollection */
            $ruleCollection = Mage::getModel('dyna_productmatchrule/rule')->getCollection()
                ->addFieldToFilter('source_type', array('eq' => 1))
                // Filter only relevant rules -- for better performance
                ->addFieldToFilter('operation', array('in' => array(Dyna_ProductMatchRule_Model_Rule::OP_DEFAULTED, Dyna_ProductMatchRule_Model_Rule::OP_DEFAULTED_OBLIGATED)))
                ->addFieldToFilter('operation_type', array('eq' => Dyna_ProductMatchRule_Model_Rule::OP_TYPE_S2P))
                ->addFieldToFilter('service_source_expression', array('notnull' => true))
                ->addFieldToFilter('effective_date', array(
                    array('to' => date("Y-m-d"), 'date' => true),
                    array('null' => true)
                ))
                ->addFieldToFilter('expiration_date', array(
                    array('from' => date("Y-m-d"), 'date' => true),
                    array('null' => true)
                ));

            if ($packageType) {
                if ($packageTypeId) {
                    $ruleCollection->addFieldToFilter('package_type', array('eq' => (int)$packageTypeId));
                } else {
                    /** @var Dyna_ProductMatchRule_Model_Exception $exception */
                    $exception = Mage::getModel('dyna_productmatchrule/exception', "Got package type value but cannot find resource. Were imports ran?");
                    throw $exception;
                }
            } else {
                /** @var Dyna_ProductMatchRule_Model_Exception $exception */
                $exception = Mage::getModel('dyna_productmatchrule/exception', "Not type set on active package");
                throw $exception;
            }

            $ruleCollection->getSelect()
                ->join(array('rule_website' => 'product_match_rule_website'), 'rule_website.rule_id = main_table.product_match_rule_id')
                ->where('rule_website.website_id = ?', $websiteId)
                ->order('main_table.priority ASC');


            if ($processContextHelper->getProcessContextId()) {
                $ruleCollection->getSelect()
                    ->join(array('product_match_rule_process_context' => 'product_match_rule_process_context'), 'product_match_rule_process_context.rule_id = main_table.product_match_rule_id')
                    ->where('product_match_rule_process_context.process_context_id = ?', (int)$processContextHelper->getProcessContextId());
            }

            $ruleCollection->load();

            $staticCollection = new Varien_Data_Collection();
            foreach ($ruleCollection->getItems() as $item) {
                $staticCollection->addItem($item);
            }

            $this->getCache()->save(serialize($staticCollection), $cacheKey, array($this->getCache()::PRODUCT_TAG), $this->getCache()->getTtl());
        } else {
            $ruleCollection = unserialize($finalCollectionSerialized);
        }

        /** @var Dyna_Configurator_Model_Expression_Cart $cartModel */
        $cartModel = Mage::getModel('dyna_configurator/expression_cart');
        if (!$removal) {
            $cartModel->appendCurrentProducts($productIds);
        } else {
            $cartModel->updateCurrentPackages($packageModel->getQuote());
        }

        $inputParams = [
            'customer' => Mage::getModel('dyna_configurator/expression_customer'),
            'cart' => $cartModel,
            'availability' => Mage::getModel('dyna_configurator/expression_availability'),
            'serviceability' => Mage::getModel('vznl_configurator/expression_serviceability'),
            'agent' => Mage::getModel('dyna_configurator/expression_agent'),
            'installBase' => Mage::getModel('dyna_configurator/expression_installBase'),
        ];

        /**
         * @var $dynaCoreHelper Dyna_Core_Helper_Data
         */
        $dynaCoreHelper = Mage::helper('dyna_core');

        $defaulted = [];
        $errors = [];
        foreach ($ruleCollection as $rule) {
            $ssExpression = $rule->getServiceSourceExpression();
            try {
                $passed = false;

                if ($dynaCoreHelper->evaluateExpressionLanguage($ssExpression, $inputParams)) {
                    $passed = true;
                }

                if ($passed) {
                    $productId = $rule->getRightId();
                    if ($this->debug) {
                        $this->log(sprintf('Service defaulted rule evaluated to true. Id: %s, Name: %s', $rule->getId(), $rule->getRuleTitle()));
                    }
                    if ($productId) {
                        // inject product in service rules evaluation
                        $cartModel->appendCurrentProducts([$productId]);
                        switch ($rule->getOperation()) {
                            case Dyna_ProductMatchRule_Model_Rule::OP_DEFAULTED:
                                $defaulted[$productId] = array(
                                    'target_product_id' => $productId,
                                    'obligated' => 0,
                                    'rule_id' => $rule->getId(),
                                    'is_service' => 1
                                );

                                if ($this->debug) {
                                    $this->log(sprintf('Service rule should defaulted product: %s', $productId));
                                }

                                break;
                            case Dyna_ProductMatchRule_Model_Rule::OP_DEFAULTED_OBLIGATED:
                                $defaulted[$productId] = array(
                                    'target_product_id' => $productId,
                                    'obligated' => 1,
                                    'rule_id' => $rule->getId(),
                                    'is_service' => 1
                                );

                                if ($this->debug) {
                                    $this->log(sprintf('Service rule should default product: %s', $productId));
                                }
                                break;
                            default:
                                continue;
                        }
                    }
                }
            } catch (Exception $e) {
                // Error
                $errors[$ssExpression] = '[Rule ID = ' . $rule->getId() . '] ' . $e->getMessage();
            }
        }

        if (count($errors) > 0) {
            $this->log('[ERROR] Found ' . count($errors) . ' errors within the rules', Zend_Log::NOTICE);
        }

        $messages = '';
        foreach ($errors as $key => $value) {
            $messages = sprintf("%s [RULE] \n Expression: %s \n Message: %s \n", $messages, $key, $value);
        }

        if (strlen($messages) > 0) {
            $this->log($messages, Zend_Log::NOTICE);
        }

        if ($this->debug) {
            $this->log(sprintf('Service rules defaulted products: %s', var_export($defaulted, true)));
        }

        return $defaulted;
    }
}