<?php

/**
 * Class Vznl_ProductMatchRule_Model_Rule
 */
class Vznl_ProductMatchRule_Model_Rule extends Dyna_ProductMatchRule_Model_Rule
{
    /**
     * Get all service expression applicable rules
     *
     * Because the serviceAbility service returns multiple streams and that the rules need to
     * be evaluated per stream and not a collection of streams, we build one "availability" variable
     * per stream, restraining the rule context to one stream at a time, allowing conditions like
     *
     *      availability.hasTechnology('test', '1') AND availability.hasOption('option', 'test')
     *
     * that will evaluate to true ONLY if the that one stream has BOTH conditions true and avoid the case when we
     * have two streams in the context and the first condition evaluates as true on the first stream and the second
     * condition evaluates as true on the second stream, making the whole rule pass as true while it should not
     *
     * @param array $productIds
     * @param $websiteId
     * @param $packageType
     * @param bool $returnNotAllowed
     * @return array
     * @throws Mage_Core_Exception
     */
    public function loadAllowedServiceRules($productIds, $websiteId, $packageType, $returnNotAllowed = false)
    {
        $notInitiallySelectable = Mage::helper('dyna_configurator/cart')->getNotInitiallySelectableProducts($packageType);
        /** @var Dyna_Configurator_Model_Expression_Cart $cartModel */
        $cartModel = Mage::getModel('dyna_configurator/expression_cart');
        $cartModel->appendCurrentProducts($productIds ?: array());
        $debug = Mage::helper('dyna_productmatchrule')->isDebugMode();

        $targetAllowed = [self::OP_TYPE_P2S, self::OP_TYPE_C2S];

        $inputParams = Mage::helper('dyna_productmatchrule')->getServiceCompRulesObjects();
        $inputParams['cart'] = $cartModel;

        // separated from above object because it is needed in targeting products, not for evaluation rules
        /** @var Dyna_Configurator_Model_Expression_Catalog $catalogObject */
        $catalogObject = Mage::getModel('dyna_configurator/expression_catalog', $packageType);

        /**
         * @var $dynaCoreHelper Dyna_Core_Helper_Data
         */
        $dynaCoreHelper = Mage::helper('dyna_core');

        // split rule collections due to caching issues
        $sourceCollectionS2S = $this->getCollectionData($productIds, $websiteId, $packageType, 'source', [self::OP_TYPE_S2S]);
        $sourceCollectionS2P = $this->getCollectionData($productIds, $websiteId, $packageType, 'source', [self::OP_TYPE_S2P]);
        $sourceCollectionS2C = $this->getCollectionData($productIds, $websiteId, $packageType, 'source', [self::OP_TYPE_S2C]);

        if ($productIds) {
            $targetCollectionP2S = $this->getCollectionData($productIds, $websiteId, $packageType, 'target', [self::OP_TYPE_P2S]);
            $targetCollectionC2S = $this->getCollectionData($productIds, $websiteId, $packageType, 'target', [self::OP_TYPE_C2S]);

            $mergedCollectionsIds = array_merge(
                $sourceCollectionS2S->getAllIds(),
                $sourceCollectionS2P->getAllIds(),
                $sourceCollectionS2C->getAllIds(),
                $targetCollectionP2S->getAllIds(),
                $targetCollectionC2S->getAllIds()
            );
        } else {
            $mergedCollectionsIds = array_merge($sourceCollectionS2S->getAllIds(), $sourceCollectionS2P->getAllIds(), $sourceCollectionS2C->getAllIds());
        }

        $finalCollection = $this->getRulesByIds($mergedCollectionsIds);

        $errors = [];
        $allowed = [];
        $notAllowed = [];

        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();

        if ($debug) {
            $this->log('===== Processing quote: ' . $quote->getId());
        }

        foreach ($finalCollection as $rule) {
            try {
                $operationType = $rule->getOperationType();
                $operationType = (int)$operationType;
                if (($operationType == self::OP_TYPE_P2S) || ($operationType == self::OP_TYPE_C2S)) {
                    $passed = true;
                } else {
                    if (strpos('serviceability.item', $rule->getServiceSourceExpression()) !== false) {
                        $ssExpression = strtolower($rule->getServiceSourceExpression());
                    } else {
                        $ssExpression = $rule->getServiceSourceExpression();
                    }
                    $passed = false;
                    if ($dynaCoreHelper->evaluateExpressionLanguage($ssExpression, $inputParams)) {
                        $passed = true;
                    }
                }

                if ($passed) {
                    $ruleId = $rule->getId();
                    $operation = $rule->getOperation();
                    if ($debug) {
                        $type = $operation == static::OP_NOTALLOWED ? 'not allowed' : 'allowed';
                        $this->log(sprintf('Service %s rule evaluated to true. Id: %s, Name: %s', $type, $ruleId,
                            $rule->getRuleTitle()));
                    }
                    $products = [];
                    if (($operationType == self::OP_TYPE_P2S) || ($operationType == self::OP_TYPE_C2S) || ($operationType == static::OP_TYPE_S2S )) {
                        $targetedProductsExpression = trim($rule->getServiceTargetExpression());

                        $indexed = $dynaCoreHelper->evaluateExpressionLanguage($targetedProductsExpression, array(
                            // let the evaluator know what kind of operation is evaluating
                            'catalog' => $catalogObject,
                            'cart' => $cartModel,
                        ));
                    } else {
                        $indexed = $this->getIndexedServiceRules($ruleId, $websiteId);
                    }

                    if ($indexed && is_array($indexed)) {
                        foreach ($indexed as $product) {
                            if ($rule->getOverrideInitialSelectable() == 1 || !in_array($product,
                                    $notInitiallySelectable)) {
                                $products[$product] = $rule->getPriority();
                            }
                        }
                    }

                    if ($debug) {
                        $this->log('Products affected: ' . json_encode($products));
                    }
                    switch ($rule->getOperation()) {
                        case static::OP_ALLOWED:
                        case static::OP_DEFAULTED:
                        case static::OP_DEFAULTED_OBLIGATED:
                        case static::OP_OBLIGATED:
                            $allowed = self::mergeArray($allowed, $products);
                            break;
                        case static::OP_NOTALLOWED:
                            $notAllowed = self::mergeArray($notAllowed, $products);
                            break;
                        default:
                            continue;
                    }
                }
            } catch (Exception $e) {
                // Error
                $errors[$ssExpression] = '[Rule ID = ' . $rule->getId() . '] ' . $e->getMessage();
            }
        }

        if (count($errors) > 0) {
            $this->log('[ERROR] Found ' . count($errors) . ' errors within the rules', Zend_Log::NOTICE);
        }

        $messages = '';
        foreach ($errors as $key => $value) {
            $messages = sprintf("%s [RULE] \n Expression: %s \n Message: %s \n", $messages, $key, $value);
        }

        if (strlen($messages) > 0) {
            $this->log($messages, Zend_Log::NOTICE);
        }

        ksort($allowed);
        ksort($notAllowed);
        if ($debug) {
            $this->log('Products allowed by service rules: ' . json_encode($allowed));
            $this->log('Products not allowed by service rules: ' . json_encode($notAllowed));
        }

        if ($returnNotAllowed) {
            return $notAllowed;
        } else {
            foreach ($notAllowed as $id => $prio) {
                if (isset($allowed[$id]) && $allowed[$id] <= $prio) {
                    // remove not allowed with higher prio
                    unset($allowed[$id]);
                    continue;
                }
            }
            return $allowed;
        }
    }

    /**
     * Gets a list of categories and associated products
     * @param int|null $websiteId
     * @param bool $withPaths
     * @return array
     */
    public function getAllCategoryProducts($websiteId, $withPaths = false)
    {
        $website = Mage::getModel('core/website')->load($websiteId);
        // Add store id condition to handle correctly indexer for anchor categories
        // This will get only the default store, for all stores @todo
        $storeId = $website->getDefaultStore()->getStoreId();

        $productCategories = $categoryNames = array();
        $categories = Mage::getModel('catalog/category')->getCollection()->addAttributeToSelect('*');
        foreach ($categories as $category) {
            $categoryProductIds = Mage::getResourceModel('catalog/product_collection')
                ->setStoreId($storeId)
                ->addWebsiteFilter(array($websiteId))
                ->addCategoryFilter($category)
                ->getAllIds();
            $productCategories[$category->getId()] = $categoryProductIds;
            $path = substr($category->getPath(), 4);
            if ($withPaths && !empty($path)) {
                $categoryNames[$category->getId()] = $category->getName();
                $cats = explode('/', $path);
                unset($path);
                foreach ($cats as &$cat) {
                    if (!empty($categoryNames[$cat])) {
                        $cat = $categoryNames[$cat];
                    }
                }
                $productCategories['paths'][implode('/', $cats)] = $category->getId();
                unset($cats);
            }
        }

        return $productCategories;
    }

    /**
     * @param $productIds
     * @param $websiteId
     * @param $packageType
     * @param $type
     * @param $operationType
     * @return mixed
     * @throws Mage_Core_Exception
     */
    protected function getCollectionData($productIds, $websiteId, $packageType, $type, $operationType)
    {
        /** @var Dyna_Catalog_Helper_ProcessContext $processContextHelper */
        $processContextHelper = Mage::helper("dyna_catalog/processContext");
        $processContextId = $processContextHelper->getProcessContextId();
        $moveInitiated  = Mage::registry('move_process_initiated');

        // no process context found if there is no package in the cart (campaign flow). default ACQ
        if ($processContextId === null) {
            $processContextId = Mage::getModel('dyna_catalog/processContext')->getProcessContextIdByCode(Dyna_Catalog_Model_ProcessContext::ACQ);
        }

        static $cache = null;
        $cacheKey = "CACHED_SERVICE_RULES_" . md5(serialize(array(
                $productIds,
                $websiteId,
                $packageType,
                $type,
                $operationType,
                $processContextId,
                $moveInitiated
            )));
        if (isset($cache[$cacheKey])) {
            return $cache[$cacheKey];
        }

        // caching collection in redis for avoiding heavy db usage (this collection will not change)
        if (!$finalCollectionSerialized = $this->getCache()->load($cacheKey)) {
            $ruleCollection = $this->getCollection()
                ->addFieldToFilter('operation_type', ['in' => $operationType])
                ->addFieldToFilter('main_table.operation', ['in' => [
                    static::OP_ALLOWED,
                    static::OP_DEFAULTED,
                    static::OP_DEFAULTED_OBLIGATED,
                    static::OP_OBLIGATED,
                    static::OP_NOTALLOWED
                ]])
                ->addFieldToFilter('effective_date', array(
                    array('to' => date("Y-m-d"), 'date' => true),
                    array('null' => true)
                ))
                ->addFieldToFilter('expiration_date', array(
                    array('from' => date("Y-m-d"), 'date' => true),
                    array('null' => true)
                ));

            if(null !== $moveInitiated) {
                $agentConditions = new Zend_Db_Expr(
                    "IF(`main_table`.`service_source_expression` not like '%agent.hasDealerGroup%' and `main_table`.`service_source_expression` not like '%agent.hasDealerCode%' and `main_table`.`service_source_expression` not like '%agent.hasAgentId%' ,1,IF(`main_table`.`operation` != 0,1,0)) =1");
                $ruleCollection->getSelect()->where($agentConditions);
            }

            if ($packageType) {
                /** @var Dyna_Package_Model_PackageType $packageTypeModel */
                $packageTypeModel = Mage::getSingleton("dyna_package/packageType")
                    ->loadByCode($packageType);
                if ($packageTypeModel->getId()) {
                    $ruleCollection->addFieldToFilter("main_table.package_type",
                        array('eq' => $packageTypeModel->getId()));
                } else {
                    Mage::throwException("Got package type value but cannot find resource in Allowed Service Rules call.");
                }
            } else {
                Mage::throwException("Empty package type in Allowed Service Rules call");
            }

            $ruleCollection->getSelect()
                ->join(array('rule_website' => 'product_match_rule_website'),
                    'rule_website.rule_id = main_table.product_match_rule_id')
                ->where('rule_website.website_id = ?', $websiteId)
                ->order('main_table.priority ASC');


            if ($processContextId) {
                $ruleCollection->getSelect()
                    ->join(array('product_match_rule_process_context' => 'product_match_rule_process_context'),
                        'product_match_rule_process_context.rule_id = main_table.product_match_rule_id')
                    ->where('product_match_rule_process_context.process_context_id = ?', $processContextId);
            }

            if ($type == 'target') {
                $ruleCollection->getSelect()
                    ->join(
                        array('pmsi' => 'product_match_service_index'),
                        'main_table.operation_type IN (' . implode(',', $operationType) . ') AND
                   main_table.product_match_rule_id = pmsi.rule_id ' .
                        (count($productIds) > 0 ? ' AND pmsi.source_product_id IN
                       (' . implode(',', $productIds) . ')' : ''),
                        ['pmsi.source_product_id' => 'source_product_id']

                    )
                    ->where('main_table.operation_type IN (' . implode(',', $operationType) . ')')
                    ->group('main_table.product_match_rule_id');
            }

            $finalCollection = new Varien_Data_Collection();
            foreach ($ruleCollection->getItems() as $item) {
                $finalCollection->addItem($item);
            }
            $cache[$cacheKey] = $finalCollection;

            $this->getCache()->save(serialize($finalCollection), $cacheKey, array($this->getCache()::PRODUCT_TAG),
                $this->getCache()->getTtl());
        } else {
            $finalCollection = unserialize($finalCollectionSerialized);
        }

        return $finalCollection;
    }
}
