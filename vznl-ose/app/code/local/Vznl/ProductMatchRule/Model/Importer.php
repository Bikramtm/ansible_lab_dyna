<?php

/**
 * Class Vznl_ProductMatchRule_Model_Importer
 */
class Vznl_ProductMatchRule_Model_Importer extends Dyna_ProductMatchRule_Model_Importer
{
    /** @var string $_logFileName */
    protected $_logFileName = "comp_rules_import";

    public function __construct()
    {
        parent::__construct();
        /** @var Vznl_Import_Helper_Data _helper */
        $this->_helper = Mage::helper('vznl_import');
    }

    /**
     * Set the websites that are available as an array of [websiteID => websiteCode]
     */
    protected function setAllDefaultWebsites()
    {
        if (!$this->websites) {
            $presentWebsites = Mage::app()->getWebsites();
            /**
             * @var Mage_Core_Model_Website $website
             */
            foreach ($presentWebsites as $website) {
                $this->websites[$website->getId()] = strtolower($website->getCode());
            }
        }
    }

    /**
     * Try to set the websites for the rule;
     * Go through each website id or website code provided (the list provided in the csv can be of the form "id1,id2" or "code1,code2"
     * and see if the code/id provided is in the list of websites codes/ids that are possible.
     * If one id/code is not found in the possible list => skip the row, error
     *
     * @return bool
     */
    protected function setWebsiteIds()
    {
        $websitesData = explode(',', str_replace(' ','', strtolower($this->line[$this->position['website_id']])));
        foreach ($websitesData as $key => $possibleWebsite) {
            // if one of the possible websites provided in the csv is "*" the rule will be available for all websites; skip the rest of the code
            if ($possibleWebsite == self::ALL_WEBSITES) {
                $this->data['website_id'] = array_keys($this->websites);

                return true;
            }
            // the website provided is the code and we should store the id
            if ($validWebsite = array_search($possibleWebsite, $this->websites)) {
                $this->data['website_id'][$key] = $validWebsite;
            } // the website provided is id and we should store it
            elseif (array_key_exists($possibleWebsite, $this->websites)) {
                $this->data['website_id'][$key] = $possibleWebsite;
            } // not match can be found between the given website and a code or an id
            else {
                $this->_logError('[ERROR] The website_id ' . var_export($websitesData,
                        true) . ' is not present/valid (rule title = ' . $this->data['rule_title'] . '). skipping row');

                return false;
            }
        }

        return true;
    }

    /**
     * @param $path
     * @return bool
     */
    public function import($path)
    {
        /** @var Dyna_ProductMatchRule_Helper_Data $helper */
        $this->helper = Mage::helper('productmatchrule');
        $this->processContextHelper = Mage::helper("dyna_catalog/processContext");
        $this->helper->importHelper = $this->_helper;
        $file = fopen($path, 'r');
        $lc = 0;
        $success = false;

        $this->setDefaultOperationTypes();
        $this->operationsWithValues = $this->helper->getOperationsWithValues();
        $this->setDefaultOperations();
        $this->allowedOperationTypeCombinations = $this->helper->getAllowedOperationTypeCombinations();
        $this->setDefaultProcessContexts();
        $this->setDefaultSourceCollections();
        $this->setDefaultSourceType();
        $this->setDefaultRuleOrigin();
        $this->setAllDefaultWebsites();

        foreach ($this->websites as $websiteId => $websiteCode) {
            /** @var Dyna_Catalog_Model_Category $categoryModel */
            $categoryModel = Mage::getModel('catalog/category');
            $this->websiteProducts[$websiteId] = $categoryModel->getWebsitesProductsId($websiteId);

            /** @var Dyna_ProductMatchRule_Model_Rule $productMatchRuleModel */
            $productMatchRuleModel = Mage::getModel('productmatchrule/rule');
            $productCategories = $productMatchRuleModel->getAllCategoryProducts($websiteId, true);
            $this->websiteCategoryProducts[$websiteId] = $productCategories;
            unset($productCategories);
        }

        $this->helper->setWebsiteProducts($this->websiteProducts);
        $this->helper->setWebsiteCategoryProducts($this->websiteCategoryProducts);

        $this->position = [];
        /** @var Dyna_Import_Helper_Data $importHelper */
        $importHelper = $this->getImportHelper();

        /** @var Zend_Db_Adapter_Abstract $connection */
        $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
        $connection->exec('SET FOREIGN_KEY_CHECKS = 0;');

        while (($this->line = fgetcsv($file, null, self::CSV_DELIMITER)) !== false) {
            if ($lc == 0) {
                $this->line = $this->formatKey($this->line);
                $type = 'productMatchRules';
                //get header mapping
                $check = $importHelper->checkHeader(array_map('strtolower', $this->line), $type);
                //log messages
                $messages = $importHelper->getMessages();
                $attachments = [];
                foreach ($messages as $key => $msg) {
                    //write file log
                    $this->_logError($msg);
                    if ($key !== 'success' && substr($this->_pathToFile, 0, 9) != '/vagrant/') {
                        $attachments[] = ['key' => $key, 'text' => $msg];
                    }
                }
                if (count($attachments)) {
                    $msg = 'While processing file ' . $this->_pathToFile . ' we encountered the following:';
                    $importHelper->postToSlack($msg, $attachments);
                }
                //check if the headers are ok
                if (!$check) {
                    break;
                }
                $headers = $importHelper->getMapping($type);
                unset($type);
                //parse data
                $flippedLine = array_flip($this->line);
                foreach ($headers as $key => $value) {
                    if (isset($flippedLine[$key])) {
                        $this->position[$value] = array_search($key, $this->line);
                    }
                }
            }

            $lc++;
            if ($lc == 1) {
                continue;
            }
            $this->opType = [];
            $this->data = [];
            $this->data['stack'] = $this->stack;
            $foundLeftIdOrSourceExpressionCollection = false;
            $foundRightIdOrTargetExpressionCollection = false;

            // get the rule title ; trim it
            if (isset($this->position['rule_title'])) {
                $this->data['rule_title'] = trim($this->line[$this->position['rule_title']]);
            }

            // determine the id of the operation type based on the string(PRODUCT-PRODUCT)
            if (isset($this->position['operation_type']) && !$this->setOperationType()) {
                $this->_logError('[ERROR] operation type cannot be determined for rule title: ' . $this->data['rule_title'] . '. Skipping row no' . $lc);
                continue;
            }

            if (isset($this->position['operation'])) {
                // determine the id of the operation based on the string (allowed, min)
                if (!$this->setOperation()) {
                    $this->_logError('[ERROR] operation cannot be determined for rule title: ' . $this->data['rule_title'] . '. Skipping row no ' . $lc);
                    continue;
                }

                // determine the the operation value for operations min, max, equal
                if (!$this->setOperationValue()) {
                    $this->_logError('[ERROR] Skipping row no ' . $lc);
                    continue;
                }
            }
            // determine the override_initial_selectable
            if (in_array($this->data['operation'], $this->helper->getOperationsWithOverrideInitialSelectable())) {
                $this->setOverrideInitialSelectable();
            }

            // determine the source_type; must be a string that can be associated with an id or a valid id; else error
            if (isset($this->position['source_type'])) {
                if (!$this->setSourceType()) {
                    $this->_logError('[ERROR] source type cannot be determined for rule title: ' . $this->data['rule_title'] . '. Skipping row no ' . $lc);
                    continue;
                }
            }

            // determine the target_type; must be a string that can be associated with an id or a valid id; else error
            if (isset($this->position['target_type'])) {
                if (!$this->setTargetType()) {
                    $this->_logError('[ERROR] target type cannot be determined for rule title: ' . $this->data['rule_title'] . '. Skipping row no ' . $lc);
                    continue;
                }
            }

            // determine the service_source_expression
            if (isset($this->position['service_source_expression'])) {
                $this->data['service_source_expression'] = trim($this->line[$this->position['service_source_expression']]);
                if ($this->data['service_source_expression']) {
                    //validate the service source expression
                    if (!$this->validateServiceSourceExpression($this->data['service_source_expression'])) {
                        $this->_logError('[ERROR] service_source_expression ' . $this->data['service_source_expression'] . ' is not valid ( rule tile = ' .
                            $this->data['rule_title'] . ') Skipping row no ' . $lc);
                        continue;
                    }
                    $foundLeftIdOrSourceExpressionCollection = true;
                }
                // the service source expression is mandatory if the source type is service OMNVFDE-408
                if (!$this->data['service_source_expression'] && isset($this->data['source_type']) && $this->data['source_type']) {
                    $this->_logError('[ERROR] service_source_expression cannot be determined and is mandatory when the source type is Service( rule tile = ' .
                        $this->data['rule_title'] . ') Skipping row no ' . $lc);
                    continue;
                }
            }

            // determine the service_target_expression
            if (isset($this->position['service_target_expression'])) {
                $this->data['service_target_expression'] = isset($this->line[$this->position['service_target_expression']]) ? trim($this->line[$this->position['service_target_expression']]) : '';
                if ($this->data['service_target_expression']) {
                    //validate the service source expression
                    if (!$this->validateServiceTargetExpression($this->data['service_target_expression'])) {
                        $this->_logError('[ERROR] service_target_expression ' . $this->data['service_target_expression'] . ' is not valid ( rule tile = ' .
                            $this->data['rule_title'] . ') Skipping row no ' . $lc);
                        continue;
                    }
                    $foundRightIdOrTargetExpressionCollection = true;
                }
                // the service source expression is mandatory if the source type is service OMNVFDE-408
                if (!$this->data['service_target_expression'] && isset($this->data['target_type']) && $this->data['target_type']) {
                    $this->_logError('[ERROR] service_target_expression cannot be determined and is mandatory when the source type is Service( rule tile = ' .
                        $this->data['rule_title'] . ') Skipping row no ' . $lc);
                    continue;
                }
            }

            // determine the the left id value for the given sku and op type
            if (isset($this->position['left_id']) && !empty($this->opType)) {
                if (!$foundLeftIdOrSourceExpressionCollection && $this->setLeftId()) {
                    $foundLeftIdOrSourceExpressionCollection = true;
                }
            }

            // validate if the operation_type has the required values
            if (!$this->validateRequiredProductIdOrServiceSourceExpression($foundLeftIdOrSourceExpressionCollection, 'source')) {
                $this->_logError('[ERROR] Skipping row no ' . $lc);
                continue;
            }

            // determine the the right id value for the given sku and op type
            if (isset($this->position['right_id']) && !empty($this->opType)) {
                if (!$foundRightIdOrTargetExpressionCollection && $this->setRightId()) {
                    $foundRightIdOrTargetExpressionCollection = true;
                }
            }

            if (!$this->validateRequiredProductIdOrServiceSourceExpression($foundRightIdOrTargetExpressionCollection, 'target')) {
                $this->_logError('[ERROR] Skipping row no ' . $lc);
                continue;
            }

            // determine the rule description
            if (isset($this->position['rule_description'])) {
                $this->data['rule_description'] = trim($this->line[$this->position['rule_description']]);
                if (!$this->data['rule_description']) {
                    unset($this->data['rule_description']);
                }
            }

            // determine the priority
            if (isset($this->position['priority'])) {
                if (!$this->setPriority()) {
                    $this->_logError('[ERROR] Skipping row no ' . $lc);
                    continue;
                }
            }

            // determine if is locked or not; this is not a required field; it will be locked only when the value is locked or 1, else it will be unlocked
            if (isset($this->position['locked'])) {
                $this->setIsLocked();
            }

            // determine the website ids (if is provided a code it must be mapped to a possible id; if is an id it must be a valid one, else -> error)
            if (isset($this->position['website_id'])) {
                if (!$this->setWebsiteIds()) {
                    $this->_logError('[ERROR] Skipping row no ' . $lc);
                    continue;
                }
            } else {
                foreach (Mage::app()->getWebsites() as $website) {
                    /**
                     * @var $website Mage_Core_Model_Website
                     */
                    $this->data['website_id'][] = $website->getId();
                }
            }

            // determine the rule origin, if is set we will use that value, else 1 = import
            if (isset($this->position['rule_origin'])) {
                $this->setRuleOrigin();
            } else {
                $this->_log('rule_origin was set to the default value for import 1', true);
                $this->data['rule_origin'] = 1;
            }


            // determine the process context. if is a string it must be a valid string that can be mapped to an id, if is an id it must be a valid one, else -> error)
            if (isset($this->position['process_context'])) {
                if (!$this->setProcessContextId()) {
                    $this->_logError('[ERROR] Skipping row no ' . $lc);
                    continue;
                }
            }

            // determine the effective date, if it's a string it must be a valid date string, else null is inserted
            if (isset($this->position['effective_date'])) {
                $this->setEffectiveDate();
            }

            // determine the expiration date, if it's a string it must be a valid date string, else null is inserted
            if (isset($this->position['expiration_date'])) {
                $this->setExpirationDate();
            }

            // determine to which package type this rule belongs
            if (isset($this->position['package_type'])) {
                $this->position['package_type_id'] = $this->position['package_type'];
            }
            if (isset($this->position['package_type_id'])) {
                try {
                    $this->setPackageType();
                } catch (Exception $e) {
                    $this->_logError('[ERROR]' . $e->getMessage());
                    $this->_logError('[ERROR] Trying to import a rule with an invalid package type (' . $this->position['package_type_id'] . '): ' . $this->data['rule_title'] . '. Skipping row no ' . $lc);
                    continue;
                }
            }

            // determine the source_collection; must be a string that can be associated with an id or a valid id; else error
            // the source collection is mandatory only if source type is product selection OMNVFDE-408
            if (isset($this->position['source_collection'])) {
                if ((!$this->setSourceCollection() || (isset($this->data['source_collection']) && $this->data['source_collection'] == 0) || !isset($this->data['left_id'])) && $this->data['source_type'] == 0) {
                    $this->_logError('[ERROR] source_collection and left_id are required for Product source type( rule tile = ' .
                        $this->data['rule_title'] . ') Skipping row no ' . $lc);
                    continue;
                }
            }

            // set the left_id as a condition
            if (isset($this->data['sourceSku'])) {
                $this->data['conditions'] = [
                    '1' => [
                        'type' => 'catalogrule/rule_condition_combine',
                        'aggregator' => 'all',
                        'value' => '1',
                        'new_child' => '',
                    ],
                    '1--2' => [
                        'type' => 'catalogrule/rule_condition_product',
                        'attribute' => 'sku',
                        'operator' => '==',
                        'value' => $this->data['sourceSku']
                    ]
                ];
            }
            if (isset($this->data['sourceCategoryId'])) {
                $this->data['conditions'] = [
                    '1' => [
                        'type' => 'catalogrule/rule_condition_combine',
                        'aggregator' => 'all',
                        'value' => '1',
                        'new_child' => '',
                    ],
                    '1--2' => [
                        'type' => 'catalogrule/rule_condition_product',
                        'attribute' => 'category_ids',
                        'operator' => '==',
                        'value' => $this->data['sourceCategoryId']
                    ]
                ];
            }

            // Parse POST data to Dyna_ProductMatchRule_Model_Rule model
            $response = $this->helper->setRuleData($this->data, true);

            if (!is_array($response)) {
                $this->_logError('[ERROR] ( rule tile = ' . $this->data['rule_title'] . ')' . $response);
                $this->_logError('[ERROR] Skipping row no ' . $lc);
                // Error occurred
                continue;
            }

            try {
                /** @var Dyna_ProductMatchRule_Model_Rule $model */
                $flippedRulesTableColumns = array_flip($this->getRulesTableColumns());
                foreach ($response as $model) {
                    $insertInto = $insertValues = [];

                    if (!empty($this->data['conditions'])) {
                        // For raw query, we need to get the conditions_serialized
                        $model->setData('conditions_serialized', $model->getSerializedConditions());
                    }

                    foreach ($model->getData() as $column => $value) {
                        if (isset($flippedRulesTableColumns[$column])) {
                            $insertInto[] = $column;
                            $insertValues[':' . $column] = $value;
                        }
                    }

                    $sql = 'INSERT INTO `product_match_rule` ('.implode(', ', $insertInto).') VALUES ('.implode(', ', array_keys($insertValues)).')';

                    $connection->query($sql, $insertValues);
                    $lastInsertId = $connection->lastInsertId();
                    $model->setId($lastInsertId);

                    $model->setWebsiteIds($this->data['website_id']);
                    $model->setProcessContextsIds($this->data['process_context']);

                }

                unset($model);
                $success = true;
            } catch (Exception $e) {
                $this->_logError('[ERROR] Exception ( rule tile = ' .
                    $this->data['rule_title'] . ')' . $e->getMessage());
                $this->_logError('[ERROR] Skipping row no ' . $lc);


               // fwrite(STDERR, '[ERROR] Exception ( rule tile = ' .
                 //   $this->data['rule_title'] . ')' . $e->getMessage());
                // Error occurred
                continue;
            }

            $this->noImports++;
            $this->_log('Imported row ' . var_export($this->line, true), true);
            $this->line = null;
            $this->data = null;
            $this->opType = null;
        }
        $connection->exec('SET FOREIGN_KEY_CHECKS = 1;');

        fclose($file);
        $this->_totalFileRows = $lc == 0 ? $lc : $lc - 1;
        $this->_skippedFileRows = $this->_totalFileRows - $this->noImports;

        return $success;
    }

    public function getFileLogName()
    {
        return strtolower($this->stack) . '_' . $this->_logFileName . '.' . $this->_logFileExtension;
    }

    /**
     * Set the right id based on the operation type and sku
     * If it cannot be determined => error, skip row
     *
     * @return bool
     */
    protected function setRightId()
    {
        $this->data['right_id'] = $this->_getEntity($this->opType[1], trim($this->line[$this->position['right_id']]));
        if (!$this->data['right_id']) {
            $this->_logError('[ERROR] The target is not valid [sku: '.$this->line[$this->position['right_id']].'](line = ' . ($this->noImports + 2) . '). skipping row');

            return false;
        }

        return true;
    }
}
