<?php

class Vznl_GetTelephoneNumbers_Adapter_Stub_Factory
{
    public static function create()
    {
        return new Vznl_GetTelephoneNumbers_Adapter_Stub_Adapter();
    }
}