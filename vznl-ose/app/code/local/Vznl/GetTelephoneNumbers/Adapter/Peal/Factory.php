<?php
use GuzzleHttp\Client;

class Vznl_GetTelephoneNumbers_Adapter_Peal_Factory
{
    public static function create()
    {
        $client = new Client();
        $helper = Mage::helper('vznl_getTelephoneNumbers');
        return new Vznl_GetTelephoneNumbers_Adapter_Peal_Adapter(
            $client,
            $helper->getEndPoint(),
            $helper->getChannel(),
            $helper->getCountry()

        );
    }
}