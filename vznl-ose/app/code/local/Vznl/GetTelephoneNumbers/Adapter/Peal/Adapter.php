<?php
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;

/**
 * Class Vznl_GetTelephoneNumbers_Adapter_Peal_Adapter
 */
class Vznl_GetTelephoneNumbers_Adapter_Peal_Adapter
{
    CONST name = "GetTelephoneNumbers";
    /**
     * @var string
     */
    private $endpoint;

    /**
     * @var Client
     */
    private $client;

    /**
     * @var string
     */
    private $channel;

    /**
     * @var string
     */
    private $cty;

    /**
     * @var string
     */
    private $executionTimeId;

    /**
     * @var string
     */
    private $requestTimeId;

    /**
     * Adapter constructor.
     *
     * @param Client $client
     * @param string $endpoint
     * @param string $channel
     * @param string $cty
     */
    public function __construct(Client $client, string $endpoint, string $channel, string $cty)
    {
        $this->endpoint = $endpoint;
        $this->channel = $channel;
        $this->cty = $cty;
        $this->client = $client;
    }

    /**
     * @param $validateAddress
     * @return $source
     * @throws Exception
     */
    public function call($validateAddress = array())
    {
        $this->executionTimeId = $this->getHelper()->initTimeMeasurement();

        if (is_null($validateAddress)) {
            return 'Address information not provided to adapter';
        }
        $userName = $this->getHelper()->getLogin();
        $password = $this->getHelper()->getPassword();
        $verify = $this->getHelper()->getVerify();
        $proxy = $this->getHelper()->getProxy();
        $requestLog['url'] = $this->getUrl($validateAddress);

        $this->requestTimeId = $this->getHelper()->initTimeMeasurement();

        try {
            if($userName != '' && $password != ''){
                $response = $this->client->request('GET', $this->getUrl($validateAddress), ['auth' => [$userName, $password], 'verify' => $verify, 'proxy' => $proxy]);
            }else{
                $response = $this->client->request('GET', $this->getUrl($validateAddress),['verify' => $verify, 'proxy' => $proxy]);
            }

            $this->getHelper()->transferLog(
                json_encode($requestLog),
                json_encode(json_decode($response->getBody())),
                self::name,
                [
                    'execution' => $this->executionTimeId,
                    'request' => $this->requestTimeId
                ],
                'PEAL'
            );

            $source = json_decode($response->getBody(), true);
        } catch (ClientException|RequestException $exception) {
            $this->getHelper()->transferLog(
                $exception->getRequest(),
                ['msg' => $exception->getMessage(), 'code' => $exception->getCode()],
                self::name,
                [
                    'execution' => $this->executionTimeId,
                    'request' => $this->requestTimeId
                ],
                'PEAL'
            );

            $result['error'] = true;
            $result['message'] = $exception->getMessage();
            $result['code'] = $exception->getCode();
            return $result;
        }
        return $source;
    }

    /**
     * @param $validateAddress
     * @return string
     */
    public function getUrl($validateAddress)
    {
        return $this->endpoint.
            "?cty=".$this->cty .
            "&chl=".$this->channel.
            "&houseFlatNumber=".$validateAddress['houseFlatNumber'].
            "&houseFlatExt=".$validateAddress['houseFlatExt'].
            "&postCode=".$validateAddress['postCode'].
            "&returnset=".$this->getHelper()->getReturnSet().
            "&consecutiveNum=".$this->getHelper()->getConsecutiveNum().
            "&customerType=".$validateAddress['customerType'].
            "&footPrint=".$validateAddress['footPrint'];
    }

    /**
     * @return object Vznl_GetTelephoneNumbers_Helper_Data
     */
    public function getHelper()
    {
        return Mage::helper('vznl_getTelephoneNumbers');
    }

}
