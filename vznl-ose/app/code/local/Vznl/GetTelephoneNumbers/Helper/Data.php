<?php
class Vznl_GetTelephoneNumbers_Helper_Data extends Vznl_Core_Helper_Data
{
    /*
    * @return String
    * */
    public function getAdapterChoice()
    {
        return Mage::getStoreConfig('vodafone_service/gettelephonenumber/adapter_choice');
    }

    /*
    * @return string peal endpoint url
    * */
    public function getEndPoint()
    {
        return trim(Mage::getStoreConfig('vodafone_service/gettelephonenumber/end_point'));
    }

    /*
    * @return string peal username
    * */
    public function getLogin()
    {
        return trim(Mage::getStoreConfig('vodafone_service/gettelephonenumber/login'));
    }

    /*
    * @return string peal password
    * */
    public function getPassword()
    {
        return trim(Mage::getStoreConfig('vodafone_service/gettelephonenumber/password'));
    }

    /*
    * @return string peal cty
    * */
    public function getCountry()
    {
        return trim(Mage::getStoreConfig('vodafone_service/gettelephonenumber/country'));
    }

    /*
    * @return string peal channel
    * */
    public function getChannel()
    {
        $agentHelper = Mage::helper('agent');
        $salesChannel = $agentHelper->getDealerSalesChannel();
        if ($salesChannel) {
            return trim($salesChannel);
        } else {
            return trim(Mage::getStoreConfig('vodafone_service/gettelephonenumber/channel'));
        }
    }

    /*
    * @return string peal returnset
    * */
    public function getReturnSet()
    {
        return trim(Mage::getStoreConfig('vodafone_service/gettelephonenumber/returnset'));
    }

    /*
    * @return string peal consecutivenum
    * */
    public function getConsecutiveNum()
    {
        return trim(Mage::getStoreConfig('vodafone_service/gettelephonenumber/consecutivenum'));
    }
    /*
    * @return object omnius_service/client_stubClient
    * */
    public function getStubClient()
    {
        return Mage::getModel('omnius_service/client_stubClient');
    }

    /*
    * @return boolean peal verify
    * */
    public function getVerify()
    {
        return Mage::getStoreConfigFlag('vodafone_service/gettelephonenumber/verify');
    }

    /*
    * @return boolean peal proxy
    * */
    public function getProxy()
    {
        return Mage::getStoreConfigFlag('vodafone_service/gettelephonenumber/proxy');
    }

    /*
     * @return object Omnius_Service_Model_Logger
     */
    public function getLogger()
    {
        return Mage::getSingleton('omnius_service/logger');
    }

    /*
    * @param $validateAddress
    * @param $arguments
    * @return array peal/stub response
    * */
    public function getTelephoneNumbers($validateAddress , $arguments = array())
    {
        $factoryName = $this->getAdapterChoice();
        $validAdapters = Mage::getSingleton('getTelephoneNumbers/system_config_source_adapterChoice_adapter')->toArray();
        if (!in_array($factoryName, $validAdapters)) {
            return 'Either no adapter is chosen or the chosen adapter is no longer/not supported!';
        }
        $adapter = $factoryName::create();
        return $adapter->call($validateAddress);
    }
}
