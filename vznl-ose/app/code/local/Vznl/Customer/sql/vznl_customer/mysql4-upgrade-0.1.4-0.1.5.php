<?php
/**
 * Copyright (c) 2019. Dynacommerce B.V.
 *
 * Adding missing indexes
 *
 */

/* @var $installer Mage_Customer_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

/** Customer ID update */

$data = [
    'entity_type_id' => '1',
    'attribute_code' => 'issue_date',
    'backend_model'  => 'eav/entity_attribute_backend_datetime',
    'backend_type'   => 'datetime',
    'frontend_model' => 'eav/entity_attribute_frontend_datetime',
    'frontend_input' => 'date',
    'frontend_label' => 'ID Issue Date',
    'note'           => 'Customer ID Issue Date'
];

$installer->getConnection()->insertForce($installer->getTable('eav/attribute'), $data);

$lastInsertedId = intval($installer->getConnection()->fetchOne('SELECT attribute_id FROM eav_attribute ORDER BY attribute_id DESC LIMIT 1'));

$data = [
    'entity_type_id'     => '1',
    'attribute_set_id'   => '1',
    'attribute_group_id' => '18',
    'attribute_id'       => $lastInsertedId
];

$installer->getConnection()->insertForce($installer->getTable('eav/entity_attribute'), $data);

$data = [
    'attribute_id'    => $lastInsertedId,
    'is_visible'      => '1',
    'multiline_count' => '0',
    'is_system'       => '1',
    'sort_order'      => '0'
];

$installer->getConnection()->insertForce($installer->getTable('customer/eav_attribute'), $data);

$data = [
    [
        'form_code' => 'adminhtml_customer',
        'attribute_id' => $lastInsertedId
    ],
    [
        'form_code' => 'checkout_register',
        'attribute_id' => $lastInsertedId
    ],
    [
        'form_code' => 'customer_account_create',
        'attribute_id' => $lastInsertedId
    ],
    [
        'form_code' => 'customer_account_edit',
        'attribute_id' => $lastInsertedId
    ]
];

foreach ($data as $row) {
    $installer->getConnection()->insertForce($installer->getTable('customer/form_attribute'), $row);
}

$installer->getConnection()->update($installer->getTable('eav/attribute'), ['frontend_label' => 'ID Number'], "attribute_code = 'id_number'");
$installer->getConnection()->update($installer->getTable('eav/attribute'), ['frontend_label' => 'ID Type'], "attribute_code = 'id_type'");
$installer->getConnection()->update($installer->getTable('eav/attribute'), ['frontend_label' => 'ID Issuing Country'], "attribute_code = 'issuing_country'");
$installer->getConnection()->update($installer->getTable('eav/attribute'), ['frontend_label' => 'ID Valid Until'], "attribute_code = 'valid_until'");


/** Contractant ID update */

$data = [
    'entity_type_id' => '1',
    'attribute_code' => 'contractant_issue_date',
    'backend_model'  => 'eav/entity_attribute_backend_datetime',
    'backend_type'   => 'datetime',
    'frontend_model' => 'eav/entity_attribute_frontend_datetime',
    'frontend_input' => 'date',
    'frontend_label' => 'Contractant ID Issue Date',
    'note'           => 'Contractant ID Issue Date'
];

$installer->getConnection()->insertForce($installer->getTable('eav/attribute'), $data);

$lastInsertedId = intval($installer->getConnection()->fetchOne('SELECT attribute_id FROM eav_attribute ORDER BY attribute_id DESC LIMIT 1'));

$data = [
    'entity_type_id'     => '1',
    'attribute_set_id'   => '1',
    'attribute_group_id' => '18',
    'attribute_id'       => $lastInsertedId
];

$installer->getConnection()->insertForce($installer->getTable('eav/entity_attribute'), $data);

$data = [
    'attribute_id'    => $lastInsertedId,
    'is_visible'      => '1',
    'multiline_count' => '0',
    'is_system'       => '1',
    'sort_order'      => '0'
];

$installer->getConnection()->insertForce($installer->getTable('customer/eav_attribute'), $data);

$data = [
    [
        'form_code' => 'adminhtml_customer',
        'attribute_id' => $lastInsertedId
    ],
    [
        'form_code' => 'checkout_register',
        'attribute_id' => $lastInsertedId
    ],
    [
        'form_code' => 'customer_account_create',
        'attribute_id' => $lastInsertedId
    ],
    [
        'form_code' => 'customer_account_edit',
        'attribute_id' => $lastInsertedId
    ]
];

foreach ($data as $row) {
    $installer->getConnection()->insertForce($installer->getTable('customer/form_attribute'), $row);
}

$installer->endSetup();
