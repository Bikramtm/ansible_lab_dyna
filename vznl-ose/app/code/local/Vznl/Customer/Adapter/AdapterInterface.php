<?php
interface Vznl_Customer_Adapter_AdapterInterface
{
	public function getProfile(Vznl_Customer_Model_Customer $customer);

	public function getInstalledBase(Dyna_Customer_Model_Customer $customer);

}
