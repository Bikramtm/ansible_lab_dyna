<?php
require_once Mage::getModuleDir('controllers', 'Dyna_Customer') . DS . '/DetailsController.php';

/**
 * Class Vznl_Customer_DetailsController
 */
class Vznl_Customer_DetailsController extends Dyna_Customer_DetailsController
{
    /**
     * Set the show BTW
     */
    public function setCustomerTypeSohoAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }

        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        /** @var Omnius_Customer_Model_Session $session */
        $customer = Mage::getSingleton('customer/session')
            ->getCustomer();

        $state = (bool)$this->getRequest()->getParam('state');
        $customer->setData('party_type', $state ? Dyna_Customer_Helper_Services::PARTY_TYPE_SOHO : Dyna_Customer_Helper_Services::PARTY_TYPE_PRIVATE);
        $quote->getCustomer()->setIsBusiness($state);
        $quote->setCustomerIsBusiness($state);
        $quote->save();
    }

    /**
     * Load Customer Action
     */
    public function loadCustomerAction()
    {
        $parameters = new Varien_Object($this->getRequest()->getParams());
        /** @var Dyna_PriceRules_Helper_Data $priceRulesHelper */
        $priceRulesHelper = Mage::helper('vznl_pricerules');
        $infoHelper = Mage::helper('vznl_customer/info');

        $customer = Mage::getModel('vznl_customer/customer')->load($parameters->getData('id'));
        $customerId = $customer->getId();

        if (!$customerId) {
            $this->getResponse()
                ->setHeader('Content-Type', 'application/json')
                ->setBody(Mage::helper('core')->jsonEncode(array('message'=>$this->__('Customer not found'))));
            return true;
        }

        $session = $this->_getCustomerSession();
        $quote = Mage::getSingleton('checkout/cart')->getQuote();

        if (!$customer->getIsBusiness() && $quote && $quote->containsBusinessSubscription()) {
            $this->getResponse()
                ->setHeader('Content-Type', 'application/json')
                ->setBody(Mage::helper('core')->jsonEncode(array(
                    'error' => true,
                    'message' => $this->__('It is not possible to select \'Private\' because the shopping cart contains a Business Subscription. Change the shopping cart or enter business customer information.')
                )));

            return true;
        } elseif ($customer->getIsGemini() && $quote && $quote->containsAikidoSubscription()) {
            $this->getResponse()
                ->setHeader('Content-Type', 'application/json')
                ->setBody(Mage::helper('core')->jsonEncode(array(
                    'error' => true,
                    'message' => $this->__('There are Aikido products in the cart, which are not available to Gemini customers.')
                )));

            return true;
        } elseif ($parameters->getData('cart')) {
            $session->setLoadedCustomer($customer->getId());
            if(!$customer->getBan()) {
                // Allow loading customers without ban
                $session->setCustomer($customer);
            }
            $priceRulesHelper->decrementAll($quote);
            $priceRulesHelper->reinitSalesRules($quote);

            $response = array(
                'error' => false,
                'message' => $this->__('Customer successfully loaded')
            );
        } else {
            $response = $this->loadCustomerOnSession($customer);
        }
        $quote->updateFields(array('suspect_fraud' => 0));

        $session->setCustomerInfoSync($customer->getIsProspect());
        $this->getResponse()
            ->setHeader('Content-Type', 'application/json')
            ->setBody(Mage::helper('core')->jsonEncode($response));

        return true;
    }

    /**
     * Load Customer On Session
     */
    protected function loadCustomerOnSession($customer, $mode = 1, $isProspect = false)
    {
        /** @var Dyna_PriceRules_Helper_Data $priceRulesHelper */
        $priceRulesHelper = Mage::helper('vznl_pricerules');

        //remember current quote as this will need to be either removed or associated to the loaded customer
        /** @var Dyna_Checkout_Model_Sales_Quote $currentQuote */
        $currentQuote = Mage::getSingleton('checkout/cart')->getQuote();

        //if customer exists, load the entity to retrieve the campaigns, ctn records and other data

        $session = $this->_getCustomerSession();


        $session->setCustomer($customer);
        $session->unsBtwState();

        $unloadQuote = $this->getRequest()->get('unload_quote') === 'true' ? true : false;
        $customerId = $customer->getId();
        $customer = Mage::getModel('vznl_customer/customer')->load($customerId);
        $billingAddressData = array();
        if ($customer->getDefaultBillingAddress()) {
            $billingAddressData = $customer->getDefaultBillingAddress()->getData();
        }
        $shippingAddressData = $customer->getDefaultShippingAddress() ? $customer->getDefaultShippingAddress()->getData() : array();
        if (!$unloadQuote) {
            // If quote is assigned to a customer, we clone it to the new customer removing Retention and any promos, else we just assign the quote to the customer
            if ($currentQuote->getCustomerId() && $currentQuote->getCustomerId() != $customerId) {
                Mage::helper('dyna_checkout')->cloneQuoteWithoutCustomerData($currentQuote);
            } else {
                $currentQuote->setCustomerId($customerId);
                $currentQuote->setBillingAddress(Mage::getModel('sales/quote_address')->setData($billingAddressData));
                $currentQuote->setShippingAddress(Mage::getModel('sales/quote_address')->setData($shippingAddressData));
                $currentQuote->getBillingAddress()->save();
                $currentQuote->getShippingAddress()->save();
                $currentQuote->setCustomer($customer);
                $currentQuote->save();

                Mage::getSingleton('checkout/cart')->setQuote($currentQuote);
                Mage::getSingleton('checkout/session')->setQuoteId($currentQuote->getId());
            }
        } else {
            $newQuote = Mage::getModel('sales/quote');
            $theQuote = Mage::getSingleton('checkout/cart')->getQuote();
            $newQuote->setIsActive(true)->save();
            $theQuote->setIsActive(false)->save();
            Mage::getSingleton('checkout/cart')->setQuote($newQuote);
            Mage::getSingleton('checkout/session')->setQuoteId($newQuote->getId());
        }

        $activePackageId = Mage::getSingleton('checkout/cart')->getQuote()->getActivePackageId();
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $priceRulesHelper->decrementAll($quote);
        $priceRulesHelper->reinitSalesRules($quote);

        $customerData = $customer->getCustomData();
        $customerData['ban'] = $customer->getdata('ban');

        $session = $this->_getCustomerSession();
        $session->setCustomer($customer);
        $session->unsBtwState();


        return array(
            'sticker' => $this->loadLayout('page_three_columns')->getLayout()->getBlock('customer.details')->setData('hideCounters', true)->toHtml(),
            'rightSidebar' => $this->loadLayout('page_three_columns')->getLayout()->getBlock('cart_sidebar')->setActivePackageId($activePackageId)->toHtml(),
            'saveCartModal' => $this->getLayout()->createBlock('core/template')->setTemplate('page/html/save_cart_modal.phtml')->toHtml(),
            'customerData' => $customerData,
            'addressData' => (! empty($billingAddressData)) ? $billingAddressData : '',
            'doAjaxUpdate' => ($this->getRequest()->get('doAjaxUpdate')) ? true : false,
            'mode' => $mode,
            'isProspect' => $isProspect
        );
    }

    /**
     * Performs an update on the current logged in customer
     * by retrieving the information from the DA and mapping
     * it against the customer entity
     */
    public function poolCustomerDataAction()
    {
        $error = false;
        $message = 'Success';
        $customerData = null;

        if ($this->getRequest()->getPost('cart')) {
            /** @var Mage_Customer_Model_Customer $customer */
            $customerId = $this->_getCustomerSession()->getLoadedCustomer();
        } else {
            $customerId = $this->_getCustomerSession()->getCustomerId();
        }

        /** @var Mage_Customer_Model_Customer $customer */
        $customer = Mage::getModel('customer/customer')->load($customerId);

        $pin = $this->getRequest()->getPost('pin');
        if ($pin) {
            $customer->setPin($pin);
        }

        $showNotContinueError = '';
        if ($customer->getId() && $customer->getBan()) {
            $serviceHelper = Mage::helper('vznl_service');
            if (!$serviceHelper->useStubs() && !$serviceHelper->isOverride() && !$customer->getIsProspect()) {
                try {
                    /** @var Vznl_Service_Model_Client_DealerAdapterClient $client */
                    $client = Mage::helper('vznl_service')->getClient('dealer_adapter');
                    $response = $client->getCustomerInfo($customer);
                    /** @var Vznl_Service_Model_ResponseMapper $mapper */
                    $mapper = Mage::getModel('vznl_service/responseMapper');
                    $customer = $mapper->map('customer_info', $response, $customer);
                    if ($pin) {
                        $customer->setPin($pin);
                    }
                    $customer->save();
                } catch (Exception $e) {
                    $suppressError = false;
                    // skip the ctn/ban not found error
                    $rCode = (isset($e->responseCode) && $e->responseCode) ? $e->responseCode : $e->getMessage();

                    if (Mage::helper('vznl_service')->isExpectedError($e->getCode(), $rCode)) {
                        $this->_getCustomerSession()->setCustomerInfoSync(true);
                        $suppressError = true;
                    } else {
                        $showNotContinueError = $rCode . '-' . $e->getMessage();
                    }
                    if (!$this->getRequest()->getPost('cart') && !$suppressError) {
                        Mage::helper('vznl_service')->returnServiceError($e);
                        return;
                    }
                }
            }

            if (!$showNotContinueError) {
                $this->_getCustomerSession()->setCustomerInfoSync(true);
            }
            if ($this->getRequest()->getPost('cart')) {
                if ($showNotContinueError) {
                    $this->_getCustomerSession()->setShowModalErrorOnLoad($showNotContinueError);
                }
                if ($this->_getCustomerSession()->getCustomerInfoSync()) {
                    $this->loadCustomerOnSession($customer);
                }
            }

            /**
             * If customer is Unify, retrieve all inlife orders for its CTNs
             */
            if ($customer->getCustomerLabel() === Vznl_Customer_Model_Customer_Customer::LABEL_UNIFY) {
                $key = sprintf('%s_%s_%s', Mage::getSingleton('core/session')->getEncryptedSessionId(), $customer->getBan(), 'inlife_orders');
                $inlifeOrders = Mage::helper('dyna_customer')->getInlifeOrders($customer);
                Mage::app()->getCache()->save(serialize($inlifeOrders), $key, array(Vznl_Configurator_Model_Cache::CACHE_TAG));
            }

            $customerData = Mage::helper('dyna_configurator')->toArray($customer);
            $customerData['ctn'] = Mage::helper('dyna_configurator')->toArray($customer->getCtn());
            $customerData['campaigns'] = Mage::helper('dyna_configurator')->toArray($customer->getCampaigns());
            $customerData['billing'] = Mage::helper('dyna_configurator')->toArray($customer->getPrimaryBillingAddress());
            $customerData['shipping'] = Mage::helper('dyna_configurator')->toArray($customer->getPrimaryShippingAddress());
            $customerData['dob'] = $customer->hasDob() ? date('Y-m-d', strtotime($customer->getDob())) : false;
            $customerData['customer_label'] = $customer->getCustomerLabel() ?: '';
            $customerData['dob_label'] = $customer->hasDob() ? Mage::helper('dyna_configurator')->__('Date of birth') : '';
            $customerData['gender'] = Mage::helper('dyna_configurator')->__($customer->getPrefix());
            $customerData['customer_value'] =
                Mage::helper('dyna_configurator')->__('Value') .
                ': ' .
                $customer->getResource()
                    ->getAttribute('customer_value')
                    ->getFrontend()
                    ->getValue($customer)
            ;
            $customerData['campaign'] = $this->__('Campaign') . ': ' . $customer->getResource()->getAttribute('campaign')->getFrontend()->getValue($customer);
            $customerData['ctn_overlay'] = $this->getLayout()->createBlock('core/template')
                ->setTemplate('customer/partials/ctn_details.phtml')
                ->setCustomer($customer)
                ->setSection('products-content')
                ->toHtml();

            unset($customerData['password_hash']);
        }

        $response = array(
            'error' => $error,
            'message' => $message,
            'data' => $customerData,
            'complete' => Mage::helper('dyna_checkout')->canCompleteOrder(),
        );

        $this->getResponse()
            ->setHeader('Content-Type', 'application/json')
            ->setBody(Mage::helper('core')->jsonEncode($response));
    }

    public function showCustomerPanelAction()
    {
        if (($this->getRequest()->getParam('section') == 'products-content'
            || $this->getRequest()->getParam('section') == 'update-details')
            && Mage::getStoreConfig('omnius_service/customer_search_implementation_configuration/customer_search')
        ) {
            if (!$this->getRequest()->isPost()) {
                return;
            }

            if (!$customer = Mage::getSingleton("dyna_customer/session")->getCustomer()) {
                $response = $this->parseError("Please select a customer.");

                $this->setJsonResponse($response);
                return;
            }

            if (!$section = $this->getRequest()->getParam('section')) {
                $response = $this->parseError("No valid section requested.");

                $this->setJsonResponse($response);
                return;
            }

            /**
             * @var $panelHelper Dyna_Customer_Helper_Panel
             */
            $panelHelper = Mage::helper('dyna_customer/panel');
            $response = $panelHelper->getPanelData($section);
            if ($customer->getIsSoho()) {
                $customerName = $customer->getCompanyName();
            } else {
                $customerName = $customer->getFirstname() . " " . $customer->getLastname();
            }
            // Creating breadcrumbs for each links clicked
            if ($section == "link-details") {
                $response['breadcrumb'] = $this->__('Linked Account');
            } elseif ($section == "products-content") {
                $response['breadcrumb'] = $this->__('Products');
                $response['customer_details'] = $customerName;
            }
            $response['peal_party_subtype'] = $customer->getData('peal_party_subtype');

            /**
             * Check if customer products are eligible for Red+ bundling and mark them as such
             * so we can enable the Add Red+ member button in frontend
             */
            if (isset($response['KIAS']) && $response['KIAS']) {
                $response['KIAS'] = $panelHelper->updateKiasSubscriptionsWithRedplusBundles($response['KIAS']);
            }

            $this->setJsonResponse($response);

        } else {
            $customer_id = Mage::getSingleton('customer/session')->getCustomer()->getId();
            $customer = Mage::getModel('customer/customer')->load($customer_id);

            if ($customer) {
                /** RFC-150641 - Customer screen redesigned, rest of screens remain the same */
                if ($this->getRequest()->getParam('section') == 'orders-content') {
                    $response = array(
                        'error'=>false,
                        'customerSearch'=>true,
                        'customerPanel'=>Mage::getBlockSingleton('page/html')
                            ->setTemplate('customer/partials/orders.phtml')
                            ->setCustomer($customer)
                            ->setSection($this->getRequest()->getParam('section'))
                            ->toHtml(),
                    );

                } elseif ($this->getRequest()->getParam('section') == 'link-details') {
                    $response=array(
                        'error'=>false,
                        'customerSearch'=>true,
                        'customerPanel'=>Mage::getBlockSingleton('page/html')
                            ->setTemplate('customer/partials/link-details.phtml')
                            ->setCustomer($customer)
                            ->setSection($this->getRequest()->getParam('section'))
                            ->toHtml(),
                    );

                } else {
                    $response = array(
                        'error'=>false,
                        'customerSearch'=>true,
                        'customerPanel'=>Mage::getBlockSingleton('page/html')
                            ->setTemplate('customer/partials/ctn_details.phtml')
                            ->setCustomer($customer)
                            ->setSection( filter_var( trim( str_replace('(', '', $this->getRequest()->getParam('section'))), FILTER_SANITIZE_STRING))
                            ->toHtml(),
                    );
                }
            } else {
                $response = array(
                    'error'=>true,
                    'message'=>$this->__('Invalid customer or customer not found')
                );
            }

            $this->getResponse()
                ->setHeader('Content-Type', 'application/json')
                ->setBody(Mage::helper('core')->jsonEncode($response));
        }
    }

    /**
     * Retrieve the list of ctns for a customer
     */
    public function showRetainableAction()
    {
        $this->getResponse()
            ->setHeader('Content-Type', 'application/json')
            ->setBody(Mage::helper('core')->jsonEncode(
                Mage::helper('vznl_customer')->filterRetainableCtns(Mage::getSingleton('checkout/session')->getQuote())
            ));
    }

    /**
     * Creates an one of deal with all ctns packages
     */
    public function oneOffDealAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }

        $nonRetainableCTNs = $this->getRequest()->getParam('nonRetainable', array());
        $flippedNonRetainableCTNs = array_flip($nonRetainableCTNs);

        $this->getResponse()
            ->setHeader('Content-Type', 'application/json');

        $customer_id = Mage::getSingleton('customer/session')->getCustomer()->getId();

        /** @var Omnius_Customer_Model_Customer_Customer $customer */
        $customer = Mage::getModel('customer/customer')->load($customer_id);

        $customerCTNs = $customer->getData('ctn');
        Mage::getSingleton('checkout/cart')->getQuote()->clearQuote();
        $ctns = array();
        foreach ($customerCTNs as $ctn) {
            $additional = $ctn->getAdditional();
            $ctns[] = array(
                'ctn' => $ctn->getData('ctn'),
                'additional' => isset($additional['products']) ? array_reduce(
                        $additional['products'],
                        function ($p1, $p2) {
                            $separator = $p1 && $p2 ? ', ' : '';
                            return ($p1 ?: '') . $separator . ($p2 ? $p2['name']: '');
                        }
                    ) : '',
            );
        }

        $packageIds = array();

        foreach ($ctns as $ctn) {
            $id = Mage::helper('omnius_configurator/cart')->initNewPackage(
                Omnius_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE,
                Vznl_Checkout_Model_Sales_Quote_Item::RETENTION,
                $ctn['ctn'],
                $ctn['additional'],
                array('retainable' => !isset($flippedNonRetainableCTNs[$ctn['ctn']]))
            );
            $packageIds[$ctn['ctn']] = array(
                'id'         => $id,
                'additional' => $ctn['additional']
            );
        }

        Mage::getSingleton('checkout/cart')->getQuote()->save();

        $this->getResponse()->setBody(
            Mage::helper('core')->jsonEncode(array(
                'rightSidebar' => $this->loadLayout('page_three_columns')->getLayout()->getBlock('cart_sidebar')->toHtml(),
                'packageIds' => $packageIds,
                'saleType'   => Vznl_Checkout_Model_Sales_Quote_Item::RETENTION,
                'type'       => Omnius_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE
            ))
        );
    }

    /**
     * Creates an renewAll with all ctns packages
     */
    public function renewAllAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }

        $nonRetainableCTNs = $this->getRequest()->getParam('nonRetainable', array());
        $flippedNonRetainableCTNs = array_flip($nonRetainableCTNs);
        $customerNumber = $this->getRequest()->getPost('customernumber');
        $this->getResponse()
            ->setHeader('Content-Type', 'application/json');

        $customerStorage = Mage::getSingleton('dyna_customer/session');
        $customerProducts = $customerStorage->getCustomerProducts();
        /** @var Omnius_Customer_Model_Customer_Customer $customer */
        $customerCTNs = $customerProducts[Vznl_Customer_Helper_Services::ACCOUNT_CATEGORY_BSL][$customerNumber]['contracts'];
        Mage::getSingleton('checkout/cart')->getQuote()->clearQuote();
        $ctns = array();
        foreach ($customerCTNs as $ctn) {
            foreach ($ctn['subscriptions'] as $subscriptions) {
                $additional = isset($subscriptions['options']) ? $subscriptions['options'] : '';
                $ctns[] = array(
                'ctn' => $subscriptions['ctn'],
                'package_creation_type_id' => $subscriptions['package_creation_type_id'],
                'additional' => isset($additional['Uncategorized']) ? array_reduce(
                        $additional['Uncategorized'],
                        function ($p1, $p2) {
                            $separator = $p1 && $p2 ? ', ' : '';
                            return ($p1 ?: '') . $separator . ($p2 ? $p2['name']: '');
                        }
                    ) : '',
                );
            }
        }

        $packageIds = array();

        foreach ($ctns as $ctn) {
            $id = Mage::helper('omnius_configurator/cart')->initNewPackage(
                Omnius_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE,
                Vznl_Checkout_Model_Sales_Quote_Item::RETENTION,
                $ctn['ctn'],
                $ctn['additional'],
                array('retainable' => !isset($flippedNonRetainableCTNs[$ctn['ctn']])),
                null,
                null,
                null,
                $ctn['package_creation_type_id']
            );
            $packageIds[$ctn['ctn']] = array(
                'id'         => $id,
                'additional' => $ctn['additional'],
                'packageCreationTypeId' => $ctn['package_creation_type_id']
            );
        }

        Mage::getSingleton('checkout/cart')->getQuote()->save();

        $this->getResponse()->setBody(
            Mage::helper('core')->jsonEncode(array(
                'rightSidebar' => $this->loadLayout('page_three_columns')->getLayout()->getBlock('cart_sidebar')->toHtml(),
                'packageIds' => $packageIds,
                'saleType'   => Vznl_Checkout_Model_Sales_Quote_Item::RETENTION,
                'type'       => Omnius_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE
            ))
        );
    }

    /**
     * Retrieve the list of ctns for a customer
     */
    public function showExtendableAction()
    {
        $this->getResponse()
            ->setHeader('Content-Type', 'application/json')
            ->setBody(Mage::helper('core')->jsonEncode(
                Mage::helper('vznl_customer')->filterRetainableCtns(Mage::getSingleton('checkout/session')->getQuote())
            ));
    }

    /*
     * Retrieve the session customer info json by ID
     */
    public function retrieveCustomerJsonAction()
    {
        $parameters = new Varien_Object($this->getRequest()->getParams());
        if (!empty($parameters)) {
            try {
                $customer = Mage::getModel('customer/customer')->load($parameters->getData('id'));
                $infoHelper = Mage::helper('vznl_customer/info');
                $isProspectCustomer = $customer->checkIsProspect();
                // Save all returned results to session
                $session = $this->_getCustomerSession();
                $this->loadCustomerOnSession($customer);
                $session->setCustomer($customer);
                $session->unsBtwState();
                $basicInfo = $infoHelper->buildCustomerDetails($customer, 1, $isProspectCustomer);
                $additionalCustomerInfo = $infoHelper->buildCustomerDetails($customer, 2, $isProspectCustomer);

                // Should run after the session is set. Specifically for bundle Info of the selected customer
                $responseEligibleBundles['eligibleBundles'] = [];

                /** @var Dyna_Customer_Helper_Data $customerHelper */
                $responseOpenOrders = [];
                $responseOpenOrders['has_open_orders'] = [];

                $response = array(
                    'customerData' => array_merge($basicInfo, $additionalCustomerInfo, $responseEligibleBundles, $responseOpenOrders),
                    'totals' => Mage::getSingleton('core/layout')->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/totals.phtml')->toHtml()
                );
            } catch(Exception $e) {
                Mage::logException($e);
                $response = array(
                    'error' => true,
                    'message' => $e->getMessage()
                );
            }
        } else {
            $response = array(
                'error'   => true,
                'message' => $this->__('Method should be a POST request')
            );
        }
        $this->setJsonResponse($response);
    }

    /**
     * Call back after product customer search
     */
    public function loadCustomerCallBackAction()
    {
        $response = array(
            'saveCartModal' => $this->getLayout()->createBlock('core/template')->setTemplate('page/html/save_cart_modal.phtml')->toHtml()
        );
        $this->getResponse()
            ->setHeader('Content-Type', 'application/json')
            ->setBody(Mage::helper('core')->jsonEncode($response));
    }

    /**
     * Retrieve customer information from services
     */
    public function loadAction()
    {
        $requestData = json_decode(file_get_contents("php://input"),true);
        //if json decode fails due to special char returned in php://input, try to fallback to default method
        if(empty($requestData)){
            $request = $this->getRequest()->getPost();
            $requestData = json_decode($request['data'], true);
        }
        if (!empty($requestData)) {
            try {
                $customer = Mage::getModel('vznl_customer/customer');
                $infoHelper = Mage::helper('vznl_customer/info');

                $session = Mage::getSingleton('customer/session');
                $getCachedData = $session->getCustomerCachedHeader();

                if(Mage::getSingleton('checkout/session')->getIncomeTestData()) {
                    // unset incomeTest data if already their in the session on loading the customer.
                    Mage::getSingleton('checkout/session')->unsIncomeTestData();
                }

                if (is_array($getCachedData)) {
                    if ($requestData['_accountIdentifier'] == $getCachedData['customerAccountIdentifier'] && !empty($getCachedData['passwordCached'])) {
                        $requestData['_party']['_meta']['customerpassword'] = $getCachedData['passwordCached'];
                    }
                }
                // Map search to model
                $customerSearchData = $customer->mapSearchFields($requestData);
                //sync customer data account identifier with ose db
                $data = $customer->syncCustomerData($customerSearchData);
                $customer->addData($data);

                if($customer->getData('ban') == "" && $customer->getData('pan') == "") {
                    $isProspectCustomer = true;
                    $customer = Mage::getModel('customer/customer')->load($data['account_number']);
                } else {
                    // Retrieve customer profile
                    $adapterFactoryName = $this->getAdapterInfo();
                    $adapter = $adapterFactoryName::create();
                    $customerData = $adapter->getProfile($customer);

                    //ON No Cutomer Data from OIL
                    if(is_object($customerData) && (get_class($customerData) == 'Exception')) {
                        $wrongPassword = false;
                        if(strtolower($customerData->getMessage()) == 'invalid password entered') {
                            $wrongPassword = true;
                        }
                        $response = array(
                            'error' => true,
                            'message' => $this->__($customerData->getMessage()),
                            'wrong_password'=> $wrongPassword,
                            'requestData' => $requestData
                        );
                        $this->setJsonResponse($response);
                        return;
                    }

                    // Update the local DB with the latest data for the customer from the service call
                    $customer = $customer->updateCustomerInfo($data['account_number'], $customerData);
                    $customer->setkvk($data['kvk']);
                    $customer->setPhone($data['phone']);
                    $customer->setNoOrderingAllowed($customerData['no_ordering_allowed']);
                    $customer->setCustomerPassword($data['search_customer_password']);
                    $this->loadCustomerOnSession($customer);
                    $isProspectCustomer = false;
                }
                $customer->setPaymentMethod($customerData['billing_account']['PaymentMethod']);
                // Save all returned results to session
                $session = $this->_getCustomerSession();
                $session->setCustomer($customer);
                $session->setData('customerSearchData', $customerSearchData);
                $session->unsBtwState();

                $basicInfo = $infoHelper->buildCustomerDetails($customer, 1, $isProspectCustomer);
                $additionalCustomerInfo = $infoHelper->buildCustomerDetails($customer, 2, $isProspectCustomer);

                // Should run after the session is set. Specifically for bundle Info of the selected customer
                $responseEligibleBundles['eligibleBundles'] = [];

                /** @var Dyna_Customer_Helper_Data $customerHelper */
                $responseOpenOrders = [];
                $responseOpenOrders['has_open_orders'] = [];

                $session->setCustomerCachedHeader(false);
                $response = array(
                    'customerData' => array_merge($basicInfo, $additionalCustomerInfo, $responseEligibleBundles, $responseOpenOrders),
                    'totals' => Mage::getSingleton('core/layout')->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/totals.phtml')->toHtml()
                );
            } catch(Exception $e) {
                Mage::logException($e);
                $response = array(
                    'error' => true,
                    'message' => $this->__($e->getMessage())
                );
            }
        } else {
            $response = array(
                'error'   => true,
                'message' => $this->__('Method should be a POST request')
            );

        }
        $this->setJsonResponse($response);
    }

    /**
     * Returns the UCT caller & campaign data
     */
    public function getCampaignsAction()
    {
        $customer = Mage::getSingleton('dyna_customer/session')->getCustomer();
        if (!$this->getRequest()->isPost()) {
            return;
        }
        $postData = $this->getRequest()->getPost();
        $defaultCampaign = $postData['defaultCampaign'];

        if (array_key_exists('serviceability', $postData)) {
            $serviceability = json_decode($postData['serviceability'], true);
            if (is_array($serviceability)) {
                $serviceability = $serviceability['data'];
            }
        } else {
            $serviceability = null;
        }
        try {
            $uctParams = Mage::getSingleton('dyna_customer/session')->getUctParams();

            $result = [];
            $result['callerData'] = Mage::helper('bundles')->getCallerData($uctParams['transactionId']);
            $result['campaignData'] = Mage::helper('vznl_bundles')->getCampaignData(
                $uctParams['campaignId'],
                $uctParams['callingNumber'],
                $defaultCampaign,
                $serviceability
            );
            $result['success'] = true;
            $result['campaignData']['hasFixedPackageInCart'] =  $this->getQuote()->hasFixed() ? 1 : 0;
            $result['campaignData']['is_business'] = $customer->getIsBusiness();
            $result['campaignData']['party_type'] = $customer->getPartyType() ? : Dyna_Customer_Helper_Services::PARTY_TYPE_PRIVATE;
        } catch (Exception $e) {
            $result = [
                'success' => false,
                'message' => $e->getMessage()
            ];
        }
        $this->setJsonResponse($result);
    }
}
