<?php

/**
 * Class Vznl_Customer_Helper_Panel
 */
class Vznl_Customer_Helper_Panel extends Dyna_Customer_Helper_Panel
{
    /**
     * Parses response for requested customer section
     *
     * @param $section
     * @param $household
     *
     * @return array
     */
    public function getPanelData($section, $household = false)
    {
        $panelData = null;

        switch ($section) {
            case "link-details" :
                $panelData = $this->parseLinkDetails();
                break;
            case "orders-content" :
                $panelData = $this->getCustomerOrders();
                break;
            case "carts-content" :
                $panelData = $this->parseCartsContent();
                break;
            case "products-content" :
                $panelData = $this->getCustomerProducts($household);
                break;
            case "household" :
                $panelData = $this->getHouseholdMembers();
                break;
            case "update-details" :
                $panelData = $this->getUpdateDetails();
                break;
            case "good-deal" :
                $panelData = $this->getTipGoodDealDetails();
                break;
            case "my-360-view" :
                $panelData = $this->getViewDetails();
                break;
            default :
                $panelData = [
                    "error" => true,
                    "message" => $this->__("No valid section requested for customer panel"),
                ];
                break;
        }

        return $panelData;
    }

    protected function getCustomer()
    {
        return Mage::getSingleton('customer/session')->getCustomer();
    }

    /**
     * Return the customer products ctns
     *
     * @return array|mixed
     */
    protected function getCustomerCtns()
    {
        /** @var Dyna_Customer_Model_Session $customerStorage */
        $customerStorage = Mage::getSingleton('dyna_customer/session');
        $ctns = $customerStorage->getCustomer()->getdata('ctn');
        $result = [];
        foreach ($ctns as $ctn) {
            $orderData = [];

            // Order creation date converted to local time format
            $orderData["entityId"] = $ctn->getId();
            $orderData["ctn"] = $ctn->getCtn();
            $orderData["phonenumber"] = Mage::helper('dyna_checkout')->formatCtn($ctn->getCtn());
            $orderData["end_date"] = $ctn->getEndDate();

            $ctnAdditional = $ctn->getAdditional();
            $ctnProducts = isset($ctnAdditional['products']) ? $ctnAdditional['products'] : [];
            $ctnProductsFlat = '';
            if (!empty($ctnProducts)) {
                if (isset($ctnProducts['name'])) {
                    // only one item
                    $ctnProducts = array($ctnProducts);
                }
                foreach ($ctnProducts as $ctnProduct) {
                    $ctnProductsFlat .= $ctnProduct['name'] . ', ';
                }
            }
            if ($ctnProductsFlat) {
                $ctnProductsFlat = rtrim($ctnProductsFlat, ', ');
            }

            if($ctn->getStatus() == 'ACTIVE' && Mage::getSingleton('customer/session')->getCustomerInfoSync()) {
                $orderData["ctnIsInlife"] = true;
            }

            $orderData["ctnProductsFlat"] = !$this->htmlEscape($ctnProductsFlat);
            $orderData["showCtnInfo"] = !empty($ctnProducts);
            $result[] = $orderData;
        }

        if (!count($result)) {
            $result['error'] = true;
            $result['message'] = $this->__("No records found");
        }

        return $result;
    }

    /**
     * Getting Vodafone customer information from session
     * @return array
     */
    protected function getVodafoneCustomerInformation()
    {
        $customer = $this->getCustomer();
        $legalAddress = $customer->getAddress();

        $address = [
            'name'       =>  $customer->getName(),
            'global_id'  =>  $customer->getCustomerGlobalId(),
            'firstname'  =>  $customer->getFirstName(),
            'middlename' =>  $customer->getMiddlename(),
            'lastname'   =>  $customer->getLastName(),
            'postal_code' => $customer->getPostalCode(),
            'house_no'   =>  $customer->getHouseNo(),
            'house_no_addition'  =>  $customer->getHouseAddition(),
            'address'    =>  '',
            'phone'      =>  $customer->getPhoneNumber(),
            'email'      =>  $customer->getEmail(),
            'iban'       =>  $customer->getBankAccountNumber(),
        ];

        if ($legalAddress) {
            $address['address'] = $legalAddress->getStreet()[0]." ".
                $legalAddress->getHouseNumber()." ".
                $customer->getHouseAddition()." ".
                $customer->getPostalCode().' '.$legalAddress->getCity();
            if (!$address['phone']) {
                $address['phone'] = $legalAddress->getPhoneNumber();
            }
        } else {
            $billingAddressData = $customer->getDefaultBillingAddress()->getData();
            $billingAddressData['street'] = $customer->getDefaultBillingAddress()->getStreet(1);
            $billingAddressData['house_no'] = $customer->getDefaultBillingAddress()->getStreet(2);
            $billingAddressData['house_no_addition'] = $customer->getDefaultBillingAddress()->getStreet(3);

            $address['address'] = $billingAddressData['street']." ".
                $billingAddressData['house_no']." ".
                $billingAddressData['house_no_addition']." ".
                $billingAddressData['postcode'].' '.$billingAddressData['city'];

            if (!$address['house_no']) { $address['house_no'] = $billingAddressData['house_no']; }
            if (!$address['house_no_addition']) { $address['house_no_addition'] = $billingAddressData['house_no_addition']; }
            if (!$address['postal_code']) { $address['postal_code'] = $billingAddressData['postcode']; }

            if (!$address['phone']) {$address['phone'] = $billingAddressData['telephone']; }
        }

        return $address;
    }

    /**
     * Parses response for customer information
     * @return array
     */
    public function getUpdateDetails()
    {
        $customer_vodafone = $this->getVodafoneCustomerInformation();
        $result['name'] = $this->getCustomer()->getName();
        $result['customer_information'] = [
            'vodafone' => [
                $this->__('Customer ID') => $customer_vodafone['global_id'],
                $this->__('First name') => $customer_vodafone['firstname'],
                $this->__('Middle Name/Initial') => $customer_vodafone['middlename'],
                $this->__('Surname') => $customer_vodafone['lastname'],
                $this->__('Postal Code') => $customer_vodafone['postal_code'],
                $this->__('House number') => $customer_vodafone['house_no'],
                $this->__('House number addition') => $customer_vodafone['house_no_addition'],
                $this->__('Billing Address') => $customer_vodafone['address'],
                $this->__('Phone number') => $customer_vodafone['phone'],
                $this->__('Email address') => $customer_vodafone['email'],
                $this->__('IBAN') => $customer_vodafone['iban']
            ]
        ];
        return $result;
    }

    /**
     * Get 360 view details
     *
     * @return array
     */
    public function getViewDetails()
    {
        try {
            $result = [
                'error' => false,
                'ctns' => $this->getCustomerCtn(),
            ];

            return $result;
        } catch (\Exception $e) {
            Mage::logException($e);
            return [
                'error' => true,
                'message' => $e->getMessage(),
            ];
        }
    }

    /**
     * Parses response for requested customer CTN only from Mobile
     * @return array
     */
    protected function getCustomerCtn()
    {
        return $this->getCustomer()->getCtn(true,true);
    }

    /**
     * Parses response for requested customer good deal
     * @return array
     */
    public function getTipGoodDealDetails()
    {
        $result['ctn'] = $this->getCustomerCtn();
        $result['message'] = $this->__("Good Deal");
        return $result;
    }

    /**
     * @param $websiteId
     * @return mixed
     */
    public function getWebsite($websiteId)
    {
        return Mage::getModel('core/website')->load($websiteId);
    }

    /**
     * Parse left sidebar orders for a selected customer
     * No customer exists on session check as it is expected to be checked in controller
     *
     * @param Dyna_Customer_Model_Customer|null $customer
     * @return mixed
     */
    public function getDbOrders(Dyna_Customer_Model_Customer $customer = null)
    {
        if (!$customer) {
            /** @var Mage_Customer_Model_Customer $customer */
            $customer = Mage::getSingleton('dyna_customer/session')->getCustomer();
        }

        $magentoOrders = Mage::getModel('superorder/superorder')
            ->getCollection()
            ->addFieldToFilter('customer_number', $customer->getCustomerNumber())
            ->load();

        $result = [];
        /** @var Dyna_Superorder_Model_Superorder $order */
        foreach ($magentoOrders as $order) {
            $orderData = [];

            // Order creation date converted to local time format
            $orderData["creationDate"] = Mage::app()
                ->getLocale()
                ->date(
                    $order->getCreatedAt(),
                    Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_MEDIUM),
                    null,
                    false
                )
                ->toString('dd-MM-yyyy');
            $orderData["orderNumber"] = $order->getOrderNumber();
            $orderData["orderStatus"] = $order->getOrderStatus();
            $orderData["salesChannel"] = $this->getWebsite($order->getWebsiteId())->getName();

            $orderPackages = $order->getPackages();
            $orderData["packages"] = $this->getSizeOf($orderPackages);
            $orderData["packageTypes"] = array();
            $orderData[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD] = $orderData[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS] = $orderData[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN] = 0;
            /** @var Dyna_Package_Model_Package $package */
            foreach ($orderPackages as $package) {
                $orderData["packageTypes"][$package->getType()] = 1;
                $package->isCable() ? $orderData[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD] = "1" : $orderData[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD];
                $package->isMobile() ? $orderData[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS] = "1" : $orderData[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS];
                $package->isFixed() ? $orderData[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN] = "1" : $orderData[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN];
            }

            $result[] = $orderData;
        }

        if (!count($result)) {
            $result['error'] = true;
            $result['message'] = $this->__("No local orders found");
        }

        return $result;
    }

    /**
     * @param $orderPackages
     * @return count
     */
    public function orderPackagesCount($orderPackages)
    {
        return count($orderPackages);
    }

    /**
     * @param Dyna_Customer_Model_Customer|null $customer
     * @return array
     */
    public function getCustomerOrders(Dyna_Customer_Model_Customer $customer = null)
    {
        try {
            $nonOsfOrders = $this->getServiceCustomerOrders();
            $dbOrders = $this->getDbOrders($customer);
            $result = [
                'nonOsfOrders' => $nonOsfOrders,
                'dbOrders' => $dbOrders,
            ];

            if (isset($nonOsfOrders['error']) && $nonOsfOrders['error'] && isset($dbOrders['error']) && $dbOrders['error']) {
                $result['error'] = false;
                $result['message'] = $this->__("No orders found for this customer.");
            }
        } catch (\Exception $e) {
            $result['error'] = true;
            $result['message'] = $this->__("An error occurred and customer orders could not be loaded.");
        }

        return $result;
    }

    /**
     * TODO: This method should retrieve non-osf orders for given customer (that are not local) - needs implementation
     */
    public function getServiceCustomerOrders()
    {
        /** @var Dyna_Customer_Model_Session $customerStorage */
        $customerStorage = Mage::getSingleton('dyna_customer/session');
        $customerSession = Mage::getSingleton('customer/session');
        try {
            $customerOrdersStorage = $customerStorage->getCustomerOrders();
            if (false === empty($customerOrdersStorage)) {
                $data = $customerOrdersStorage;
            } else {
                if (Mage::getStoreConfig('omnius_service/customer_search_implementation_configuration/customer_search')) {
                    if ($customerData = $customerSession->getCustomerMode2()) {
                        $dataCustomerFromMode2 = $customerData;
                    } else {
                        $customer = Mage::getSingleton('dyna_customer/session')->getCustomer();
                        if (!empty($customer->getData('ban')) || !empty($customer->getData('pan'))) {
                            $adapterFactoryName = Mage::getStoreConfig('omnius_service/customer_search_implementation_configuration/customer_search_adapter');
                            $adapter = $adapterFactoryName::create();
                            $dataCustomerFromMode2 = $adapter->getInstalledBase($customer);
                            $customerSession->setCustomerMode2($dataCustomerFromMode2);
                            $this->saveCustomerCtns($customer);
                        }
                    }
                } else {
                    /** @var Dyna_Customer_Model_Client_RetrieveCustomerInfo $customerInfoClient */
                    $customerInfoClient = Mage::getModel('dyna_customer/client_retrieveCustomerInfo');
                    $dataCustomerFromMode2 = $customerInfoClient->executeRetrieveCustomerInfo([], 2);
                }

                // if the service did not respond with success -> display a message
                $allCustomers = $customerStorage->getServiceCustomers();
                $data = [];
                foreach ($allCustomers as $customersPerStack) {
                    foreach ($customersPerStack as $customerData) {
                        if (isset($customerData['orders'])) {
                            foreach ($customerData['orders'] as $stackType => $stackOrders) {
                                $data[strtolower($stackType)] = $stackOrders;
                            }
                        }
                    }
                }
                if (!$dataCustomerFromMode2['Success']) {
                    return [
                        "error" => true,
                        "message" => $this->__("There was a problem with the loading of customer orders . Please try again!"),
                    ];
                }
                $customerStorage->setCustomerOrders($data);
            }

            $result = $data;
            if (!count($data)) {
                $result['error'] = true;
                $result['message'] = $this->__("No non-osf orders found");
            }


            return $result;

        } catch (\Exception $e) {
            $customerStorage->clearCustomerOrders();
            Mage::logException($e);
            throw $e;
        }
    }

    /**
     * Return the customer products after a service call to getCustomerDetails Mode2
     *
     * @return array|mixed
     */
    public function getCustomerProducts($household = false)
    {
        /** @var Dyna_Customer_Model_Session $customerStorage */
        $customerStorage = Mage::getSingleton('dyna_customer/session');

        $this->catalogHelper = Mage::helper('dyna_catalog');

        try {
            $data = $this->parseProductsResults($household);

            /** @var $validationHelper Vznl_Customer_Helper_Validation */
            $validationHelper = Mage::helper('vznl_customer/validation');
            $data = $validationHelper->checkRestrictionsForButtons($data);
            $customerStorage->setCustomerProducts($data);
            $this->setOnSessionIfCustomerIsBundleEligible($data);
            return $validationHelper->checkAgentPermissions($data);

        } catch (\Exception $e) {
            $customerStorage->clearCustomerProducts();
            Mage::getSingleton('customer/session')->setCustomerIsBundleEligible(false);
            Mage::logException($e);
            return [
                "error" => true,
                "message" => $e->getMessage(),
            ];
        }
    }


    /**
     * todo: move logic to client and remove hardcodes
     * @param $mode2Data
     * @return mixed
     */
    protected function parseProductsResults($household = false)
    {
        $parsedCustomerProducts = [];
        if ($household) {
            $allCustomers = Mage::getSingleton('dyna_customer/session')->getHouseholdServiceCustomers();
        } else {
            $allCustomers = Mage::getSingleton('dyna_customer/session')->getServiceCustomers();
        }

        $customer = $this->getCustomer();
        $no_ordering_allowed = $customer->getData('no_ordering_allowed');
        $agent = Mage::getSingleton('customer/session')->getAgent();
        $sell_fixed_to_blacklisted = !empty($agent) ? $agent->isGranted(Vznl_Agent_Model_Agent::SELL_FIXED_TO_BLACKLISTED) : false;
        $_store = Mage::app()->getStore();

        // used for indexing cable subscriptions for an unique combination of customer number - product id
        $productIdCounter = 1;
        $inc = 0;
        if ($allCustomers) {
            foreach ($allCustomers as $accountType => $serviceCustomers) {
                foreach ($serviceCustomers as $serviceCustomerKey => $serviceCustomer) {
                    if (isset($serviceCustomer['customer_number'])) {
                        $customerNumber = $serviceCustomer['customer_number'];
                        $pc = [
                            'customer_number' => $customerNumber,
                            'dunning_status' => $serviceCustomer['dunning_status'],
                            'dunning_status_text' => (@$serviceCustomer['dunning_status'] == 'IN_DUNNING') ? $this->__('IN DUNNING') : null,
                            'customer_status' => $serviceCustomer['customer_status'],
                            'last_billing_amount' => $serviceCustomer['last_billing_amount'] ? $serviceCustomer['last_billing_amount'] : 0,
                            'last_billing_due_amount' => $serviceCustomer['last_billing_due_amount'] ? $serviceCustomer['last_billing_due_amount'] : 0,
                            'last_back_out_amount' => $serviceCustomer['last_back_out_amount'] ? $serviceCustomer['last_back_out_amount'] : 0,
                            'show_dunning_amount' => isset($serviceCustomer['last_back_out_amount']),
                            'customer_status_text' => $this->mapStatus($serviceCustomer['customer_status']),
                            'legacy_dunning_amount' => $serviceCustomer['legacy_dunning_amount'] ? $serviceCustomer['legacy_dunning_amount'] : 0,
                            'display_dunning_amount' => $serviceCustomer['display_dunning_amount'] ? $serviceCustomer['display_dunning_amount'] : 0
                        ];

                        $serviceLineId = null;
                        $contracts = isset($serviceCustomer['contracts']) ? $serviceCustomer['contracts'] : [];

                        foreach ($contracts as $contract) {
                            $ps = [];
                            $initialContract = $contract;
                            unset($initialContract['subscriptions'], $initialContract['products']);
                            $contractBundleMarkers = [];
                            $productComponent = [];

                            // Iterate through Contract > Subscription nodes
                            foreach ($contract['subscriptions'] as $subscription) {
                                $allSubscriptionPhoneNo = [];
                                $fnAddons = [];
                                $serviceAddress = $userAddress = null;
                                if (!empty($subscription['addresses'])) {
                                    foreach ($subscription['addresses'] as $address) {
                                        if ($address['address_type'] == Dyna_Customer_Model_Customer::SERVICE_ADDRESS) {
                                            $serviceAddress = $address;
                                            if ($accountType == 'FN' || (!isset($pc['service_address']))) {
                                                $pc['service_address'] = $serviceAddress;
                                            }
                                        } elseif ($address['address_type'] == Dyna_Customer_Model_Customer::USER_ADDRESS) {
                                            $userAddress = $address;
                                        }
                                    }
                                }

                                $countryCode = !empty($subscription['ctn']['CountryCode']) ? trim($subscription['ctn']['CountryCode']) : '';
                                $areaCode = !empty($subscription['ctn']['LocalAreaCode']) ? trim($subscription['ctn']['LocalAreaCode']) : '';
                                $phoneNumber = !empty($subscription['ctn']['PhoneNumber']) ? trim($subscription['ctn']['PhoneNumber']) : '';
                                $ctnType = !empty($subscription['ctn']['Type']) ? trim($subscription['ctn']['Type']) : '';

                                $ctn = "";
                                if (!empty($subscription['ctn'])) {
                                    if (is_array($subscription['ctn'])) {
                                        $ctn = $countryCode . $areaCode . $phoneNumber;
                                    } else {
                                        $ctn = $subscription['ctn'];
                                    }
                                }

                                // fallback contract type to Postpaid
                                $contractType = empty($contract['contract_type']) ? Dyna_Customer_Helper_Customer::MOBILE_CONTRACT_TYPE_POSTPAID : $contract['contract_type'];
                                $packageType = null;


                                if ($accountType == Vznl_Customer_Helper_Services::ACCOUNT_CATEGORY_BSL) {
                                    $packageType = ($contractType == Dyna_Customer_Helper_Customer::MOBILE_CONTRACT_TYPE_POSTPAID)
                                        ? Dyna_Catalog_Model_Type::TYPE_MOBILE : Dyna_Catalog_Model_Type::TYPE_PREPAID;
                                } elseif ($accountType == Vznl_Customer_Helper_Services::ACCOUNT_CATEGORY_PEAL) {
                                    $packageType = Dyna_Catalog_Model_Type::TYPE_FIXED_DSL;
                                }

                                if (!empty($packageType)) {
                                    $packageCreationTypeId = Mage::helper('dyna_configurator/cart')->getCreationTypeId(Vznl_Catalog_Model_Type::getCreationTypesMapping()[strtolower($packageType)]);
                                }


                                $validationFields = [
                                    'party_type' => $serviceCustomer['party_type'],
                                    'account_class' => $serviceCustomer['account_class']
                                ];

                                $subs = [
                                    'id' => $subscription['id'],
                                    'contract_type' => $contractType,
                                    'package_type' => $packageType ? strtolower($packageType) : "",
                                    'package_creation_type_id' => $packageCreationTypeId ? $packageCreationTypeId : "",
                                    'package_creation_type' => $packageType ? strtolower($packageType) : "",
                                    'customer_number' => $customerNumber,
                                    'value_indicator' => $this->mapValueIndicator($subscription['value_indicator']),
                                    'last_subsidy_date' => $subscription['last_subsidy_date'],
                                    'ctn' => $ctn,
                                    'in_minimum_duration' => $subscription['in_minimum_duration'],
                                    'ctn_country_code' => $countryCode,
                                    'ctn_localAreaCode' => $areaCode,
                                    'ctn_type' => $ctnType,
                                    'ctn_phoneNumber' => $phoneNumber,
                                    'contract_possible_cancellation_date' => $contract['contract_possible_cancellation_date'] ?? null,
                                    'service_address' => $serviceAddress,
                                    'user_address' => $userAddress,
                                    'contact_first_name' => isset($subscription['contact']) && isset($subscription['contact']['Role'])
                                    && isset($subscription['contact']['Role']['Person']) && isset($subscription['contact']['Role']['Person']['FirstName']) ?
                                        $subscription['contact']['Role']['Person']['FirstName'] : '',
                                    'contact_last_name' => isset($subscription['contact']) && isset($subscription['contact']['Role'])
                                    && isset($subscription['contact']['Role']['Person']) && isset($subscription['contact']['Role']['Person']['FamilyName']) ?
                                        $subscription['contact']['Role']['Person']['FamilyName'] : '',
                                    'contact_birth_date' => isset($subscription['contact']['Role']['Person']['BirthDate']) ? $subscription['contact']['Role']['Person']['BirthDate'] : '',
                                    /**
                                     * OMNVFDE-1843
                                     * we assume initially that the subscription is not part of bundle nor eligible for bundle;
                                     */
                                    'bundle_class' => '',
                                    'part_of_bundle' => false,
                                    'bundle_eligible' => false,
                                    'validationFields' => $validationFields,
                                    'actual_month_of_contract' => $subscription['actual_month_of_contract'],
                                    'serial_number' => $subscription['serial_number'],
                                    'sim_number' => $subscription['sim_number'],
                                    'product_offer_id' => !empty($subscription['product_offer_id']) ? $subscription['product_offer_id'] : null,
                                    'product_offer_type' => !empty($subscription['product_offer_type']) ? $subscription['product_offer_type'] : null,
                                    'fixed_subscription_missing' => true,
                                    'fixed_productdetails_missing' => false,
                                    'contract_retention_indicator' => $contract['contract_retention_indicator']
                                ];
                                // this will store the value true if at least one of the TV products has env contract
                                $prods = [];
                                $productCtn = '';
                                $mapperProductSkus = [];
                                if (isset($subscription['products'])) {
                                    $index = [];
                                    foreach ($subscription['products'] as $product) {
                                        $productCategory = !empty($product['product_category']) ? $product['product_category'] : null;
                                        $bundleClass = '';
                                        $bundleTooltip = '';
                                        $partOfBundle = false;
                                        // this will store the value true if at least one of the components of a kip product has triple play
                                        $kipProductHasTriplePlay = false;
                                        // this will store the value true if at least one of the components of a TV product has env contract
                                        $tvProductHasEnvContract = false;

                                        if (!isset($index[$productCategory])) {
                                            $index[$productCategory] = 0;
                                        } else {
                                            $index[$productCategory]++;
                                        }

                                        $productId = $index[$productCategory];
                                        $installProductId = $product['product_id'];
                                        $productIdCounter++;
                                        $packageType = null;

                                        $devices = [];
                                        if (!empty($product['devices'])) {
                                            $devices=$this->mapDevicesByCategory($product['devices']);
                                        }

                                        if (!$productCtn) {
                                            $productCtn = $product['feature_categories']['tel_main_number'] ?? null;
                                        }
                                        $product['sku'] = $product['sku'] ?? null;
                                        $sku = $product['sku'];
                                        $productComponent[] = array(
                                            'product_id' => $productId,
                                            'product_sku' => $sku,
                                            'product_package_subtype' => $packageType,
                                            'categories' => $productCategory,
                                            'product_family' => Mage::getModel('catalog/product')->load($productId)->getProductFamily(),
                                            'title' => !empty($product['product_name']) ? $product['product_name'] : null,
                                        );
                                        try {
                                            $thumbnailImage = Mage::helper('vznl_catalog')->getThumbnailBySku((string)$product['sku']);
                                        } catch (Exception $exception) {
                                            $thumbnailImage = false;
                                        }

                                        $feProductData = [
                                            'inc' => $inc++,
                                            'customer_number' => $customerNumber,
                                            'product_id' => !empty($product['product_id']) ? $product['product_id'] : null,
                                            'product_offer_id' => !empty($product['product_offer_id']) ? $product['product_offer_id'] : null,
                                            'product_offer_type' => !empty($product['product_offer_type']) ? $product['product_offer_type'] : null,
                                            'product_name' => !empty($product['product_name']) ? $product['product_name'] : null,
                                            'package_type' => $packageType,
                                            'product_category' => $productCategory,
                                            'product_status' => !empty($product['product_status']) ? $product['product_status'] : null,
                                            'product_status_display' => $this->getProductStatusDisplay($product['product_status']),
                                            'commodity_classification_nature_code' => !empty($product['commodity_classification_nature_code']) ? $product['commodity_classification_nature_code'] : null,
                                            'product_status_text' => $this->mapStatus($product['product_status']),
                                            'devices' => $devices,
                                            'service_address' => $serviceAddress,
                                            'user_address' => $userAddress,
                                            'contract_type' => $contractType,
                                            'contract_start_date' => $initialContract['contract_start_date'] ?? $subscription['contract_start_date'],
                                            'contract_end_date' => $initialContract['contract_end_date'] ?? $subscription['contract_end_date'],
                                            'contract_activation_date' => $initialContract['contract_activation_date'],
                                            'contract_last_notification_date' => $initialContract['contract_last_notification_date'],
                                            'sharing_group_details' => $subscription['sharing_group'] ?? null,
                                            'subproducts' => $product['subproducts'] ?? null,
                                            'addon' => [],
                                            'subscription_missing' => true,
                                            'productdetails_missing' => false,
                                            'hardware_serial_number' => $product['smart_card_Details']['SerialNumber'] ?? null,
                                            'hardware_name' => $product['smart_card_Details']['Type'] ?? null,
                                            'smartcard_name' => $product['smart_card_Details']['ExtResourceSpec'] ?? null,
                                            'smartcard_number' => $product['smart_card_Details']['SmartcardId'] ?? null,
                                            'thumbnailImage' => $thumbnailImage,
                                            'thumbnailImageRetina' => Mage::helper('vznl_catalog')->getThumbnailBySku((string)$product['sku'], true)
                                        ];

                                        $disabled = '';
                                        if ($no_ordering_allowed) {
                                            $disabled = $sell_fixed_to_blacklisted ? '' : 'disabled';
                                        }
                                        $feProductData['blacklisted_disabled'] = $disabled;

                                        if($accountType == Vznl_Customer_Helper_Services::ACCOUNT_CATEGORY_BSL) {
                                            if(isset($product['additional_Item'])) {
                                                foreach ($product['additional_Item'] as $additionalItem) {
                                                    if ($additionalItem['ID'] == 'Mobile_Main') {
                                                        $date1 = date_create($feProductData['contract_start_date']);
                                                        $date2 = date_create($feProductData['contract_end_date']);
                                                        $diff = date_diff($date1, $date2);
                                                        $diffMonths = $diff->format("%y") * 12 + $diff->format("%m");
                                                        if($diff->format("%d") > 0) {
                                                            $diffMonths++;
                                                        }
                                                        $monthsforMM = 1;
                                                        if ($diffMonths <= 1) {
                                                            $monthsforMM = 1;
                                                        } elseif ($diffMonths > 1 && $diffMonths <= 12) {
                                                            $monthsforMM = 12;
                                                        } elseif ($diffMonths > 12) {
                                                            $monthsforMM = 24;
                                                        }
                                                        $processAdditional = 0;
                                                        $ExtendedID = null;
                                                        $feProductData['subscription_missing'] = false;
                                                        foreach ($product['subproducts'] as $subproductkey => $subproductsvalue) {
                                                            foreach ($subproductsvalue['additional_item'] as $addkey => $addvalue) {
                                                                if ($addvalue['ID'] == 'Base_Plan') {
                                                                    foreach ($subproductsvalue['pricing_element'] as $pricekey => $pricevalue) {
                                                                        $ExtendedID = $pricevalue['CatalogueItemIdentification']['ExtendedID'];
                                                                        if (strpos($pricevalue['CatalogueItemIdentification']['ExtendedID'], 'BO_') === 0) {
                                                                            $ExtendedID = $pricevalue['CatalogueItemIdentification']['ExtendedID'];
                                                                            $processAdditional = 1;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        if ($ExtendedID) {
                                                            $request = array();
                                                            $request['ExtendedID'] = $ExtendedID;
                                                            $request['months'] = $monthsforMM;
                                                            $sku=$this->reverseMultiMapBSL($request, 'Base');
                                                            if ($sku) {
                                                                $productModel = Mage::getModel('catalog/product');
                                                                $productId = $productModel->getIdBySku($sku);
                                                                if ($productId) {
                                                                    $item = $productModel->load($productId);
                                                                    if ($processAdditional) {
                                                                        $feProductData['product_name'] = $item->getName();
                                                                        $feProductData['product_description'] = $item->getDescription();
                                                                        $feProductData['product_subtype_id'] = $item->getPackageSubtype();
                                                                        $feProductData['product_subtype'] = $item->getType();
                                                                        $feProductData['sku'] = $sku;
                                                                        $feProductData['thumbnail'] = $item->getThumbnail() ? (string)Mage::helper('catalog/image')->init($item, 'thumbnail')->resize(30) : 'no_selection';
                                                                    }

                                                                    $categoryIds = $item->getCategoryIds();
                                                                    if (!empty($categoryIds) && $processAdditional) {
                                                                        $categoryId = $categoryIds[0];
                                                                        $category = Mage::getModel('catalog/category')->load($categoryId);
                                                                        $feProductData['category_name'] = $category->getDisplayName();
                                                                    }

                                                                    $productComponent[] = array(
                                                                        'product_id' => $productId,
                                                                        'product_sku' => $sku,
                                                                        'product_package_subtype' => $feProductData['product_subtype'],
                                                                        'categories' => $categoryIds,
                                                                        'product_family' => $item->getProductFamily(),
                                                                        'title' => $feProductData['product_name'],
                                                                        'description' => $feProductData['product_description']
                                                                    );
                                                                }
                                                            } else {
                                                                $feProductData['productdetails_missing'] = true;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            $subProductDetails = array();
                                            $index = 0;
                                            $processSub = 0;
                                            foreach ($product['subproducts'] as $subproductkey => $subproductsvalue) {
                                                if (!is_array($subproductsvalue['pricing_element'])) continue;
                                                foreach ($subproductsvalue['pricing_element'] as $pricekey => $pricevalue) {
                                                    $ExtendedID = $pricevalue['CatalogueItemIdentification']['ExtendedID'];
                                                    if (strpos($pricevalue['CatalogueItemIdentification']['ExtendedID'], 'AO_') === 0) {
                                                        $processSub = 1;
                                                    }
                                                    if ($ExtendedID) {
                                                        $request['ExtendedID'] = $ExtendedID;
                                                        $sku = $this->reverseMultiMapBSL($request, 'Addon');
                                                        if ($sku) {
                                                            $productModel = Mage::getModel('catalog/product');
                                                            $productId = $productModel->getIdBySku($sku);
                                                            if ($productId) {
                                                                $index++;
                                                                $subitem = $productModel->load($productId);
                                                                $categoryIds = $subitem->getCategoryIds();
                                                                if ($processSub) {
                                                                    if (!empty($categoryIds)) {
                                                                        $foundFamilyName = false;
                                                                        foreach ($categoryIds as $categoryId) {
                                                                            $category=Mage::getModel('catalog/category')->load($categoryId);
                                                                            if($category->getIsFamily()) {
                                                                                $foundFamilyName = true;
                                                                                $subProductDetails[$categoryId]['name']=$category->getDisplayName();
                                                                                $subProductDetails[$categoryId]['position']=$category->getFamilySortOrder();
                                                                                break;
                                                                            }
                                                                        }
                                                                        if(!$foundFamilyName) {
                                                                            $categoryId='';
                                                                            $subProductDetails[$categoryId]['name']='';
                                                                            $subProductDetails[$categoryId]['position'] = 999999;
                                                                        }
                                                                    } else {
                                                                        $categoryId = '';
                                                                        $subProductDetails[$categoryId]['name'] ='';
                                                                        $subProductDetails[$categoryId]['position'] = 999999;
                                                                    }

                                                                    $subProductDetails[$categoryId]['products'][$index]['name'] = $subitem->getName();
                                                                    $subProductDetails[$categoryId]['products'][$index]['position'] = $subitem->getSorting();
                                                                    $subProductDetails[$categoryId]['products'][$index]['product_description'] = $subitem->getDescription();
                                                                    $subProductDetails[$categoryId]['products'][$index]['product_subtype_id'] = $subitem->getPackageSubtype();
                                                                    $subProductDetails[$categoryId]['products'][$index]['product_subtype'] = $subitem->getType();
                                                                    $subProductDetails[$categoryId]['products'][$index]['sku'] = $sku;
                                                                    $subProductDetails[$categoryId]['products'][$index]['contract_start_date'] = $initialContract['contract_start_date'] ?? $subscription['contract_start_date'];
                                                                    $subProductDetails[$categoryId]['products'][$index]['contract_end_date'] = $initialContract['contract_end_date'] ?? $subscription['contract_end_date'];
                                                                }

                                                                $productComponent[] = array(
                                                                    'product_id' => $subitem->getId(),
                                                                    'product_sku' => $sku,
                                                                    'product_package_subtype' => $subProductDetails[$categoryId]['products'][$index]['product_subtype'],
                                                                    'categories' => $categoryIds,
                                                                    'product_family' => $subitem->getProductFamily(),
                                                                    'title' => $subProductDetails[$categoryId]['products'][$index]['name'],
                                                                    'description' => $subProductDetails[$categoryId]['products'][$index]['product_description']
                                                                );
                                                            }
                                                        } else {
                                                            $feProductData['productdetails_missing']=true;
                                                        }
                                                    }
                                                }
                                            }
                                            $subProductDetails = $this->getSortedProduct($subProductDetails);
                                            $feProductData['addon'] =  $subProductDetails;
                                            $feProductData['products']['components'] = $productComponent;
                                        } else {
                                            $packageType = Vznl_Catalog_Model_Type::TYPE_FIXED;
                                            $feProductData['package_type'] = $packageType;

                                            if (!empty($product['product_offer_id'])) {
                                                Mage::getSingleton('customer/session')->setHasFixedProducts(true);
                                                $feProductData['product_name'] = '';
                                                $mapperRecords = $this->reverseMultiMapPEAL($product['product_offer_id'], $mapperProductSkus);
                                                if ($mapperRecords) {
                                                    foreach ($mapperRecords as $mapperRecord) {
                                                        $mapperProductSkus[] = $mapperRecord;
                                                        $productModel = Mage::getModel('catalog/product');
                                                        $productId = $productModel->getIdBySku($mapperRecord);
                                                        if ($productId) {
                                                            $item = $productModel->load($productId);
                                                            $feProductData['product_name'] = $item->getName();
                                                            $feProductData['product_description'] = $item->getDescription();
                                                            $feProductData['product_subtype_id'] = $item->getPackageSubtype();
                                                            $feProductData['product_subtype'] = $item->getPackageSubtypeName();
                                                            $feProductData['product_type'] = $item->getType();
                                                            $feProductData['sorting'] = $item->getSorting();

                                                            $categoryIds = $item->getCategoryIds();
                                                            if (!empty($categoryIds)) {
                                                                $categoryId = $categoryIds[0];
                                                                $category = Mage::getModel('catalog/category')->load($categoryId);
                                                                $feProductData['category_name'] = $category->getDisplayName();
                                                            }

                                                            $productComponent[] = array(
                                                                'product_id' => $productId,
                                                                'product_sku' => $mapperRecord,
                                                                'product_package_subtype' => $feProductData['product_subtype_id'],
                                                                'categories' => $categoryIds,
                                                                'product_family' => $item->getProductFamily(),
                                                                'title' => $feProductData['product_name'],
                                                                'description' => $feProductData['product_description']
                                                            );

                                                            if (strtoupper($item->getPackageSubtypeName()) == Vznl_Catalog_Model_Type::SUBTYPE_FIXED_PACKAGE) {
                                                                $subs['product_name'] = $item->getName();
                                                                if ($item->getDescription()) {
                                                                    $subs['product_description'] = $item->getDescription();
                                                                }
                                                                if ($item->getAdditionalText()) {
                                                                    $subs['product_description'] = $item->getAdditionalText();
                                                                }
                                                                $subs['product_subtype_id'] = $item->getPackageSubtype();
                                                                $subs['product_subtype'] = $item->getPackageSubtypeName();
                                                                $subs['product_type'] = $item->getType();
                                                                $subs['product_maf'] = $_store->roundPrice($_store->convertPrice($item->getMaf()));
                                                                $subs['product_price'] = $_store->roundPrice($_store->convertPrice($item->getPrice()));

                                                                $categoryIds = $item->getCategoryIds();
                                                                if (!empty($categoryIds)) {
                                                                    $categoryId = $categoryIds[0];
                                                                    $category = Mage::getModel('catalog/category')->load($categoryId);
                                                                    $subs['category_name'] = $category->getDisplayName();
                                                                }
                                                                $subs['fixed_subscription_missing'] = false;
                                                            }

                                                            if (strtoupper($item->getPackageSubtypeName()) == Vznl_Catalog_Model_Type::SUBTYPE_FIXED_PACKAGE_DETAILS) {
                                                                $subs['details'][$categoryId]['name'] = $feProductData['category_name'];
                                                                $subs['details'][$categoryId]['products'][$feProductData['sorting']][$mapperRecord] = $feProductData;
                                                            }
                                                            if (strtoupper($item->getPackageSubtypeName()) == Vznl_Catalog_Model_Type::SUBTYPE_FIXED_HARDWARE_MATERIALS) {
                                                                $subs['details'][$categoryId]['name'] = $feProductData['category_name'];
                                                                $subs['details'][$categoryId]['products'][$feProductData['sorting']][$mapperRecord] = $feProductData;
                                                            }

                                                            if (strtoupper($item->getPackageSubtypeName()) == Vznl_Catalog_Model_Type::SUBTYPE_FIXED_PACKAGE_ADDONS) {
                                                                $subs['addons'][$categoryId]['name'] = $feProductData['category_name'];
                                                                $subs['addons'][$categoryId]['products'][$feProductData['sorting']][$mapperRecord] = $feProductData;
                                                            }

                                                            if (strtoupper($item->getPackageSubtypeName()) == Vznl_Catalog_Model_Type::SUBTYPE_FIXED_GOODIES) {
                                                                $subs['goodies'][$categoryId]['name'] = $feProductData['category_name'];
                                                                $subs['goodies'][$categoryId]['products'][$feProductData['sorting']][$mapperRecord] = $feProductData;
                                                            }

                                                            if (strtoupper($item->getPackageSubtypeName()) == Vznl_Catalog_Model_Type::SUBTYPE_FIXED_REPLACEMENT_MATERIALS) {
                                                                $subs['materials'][$categoryId]['name'] = $feProductData['category_name'];
                                                                $subs['materials'][$categoryId]['products'][$feProductData['sorting']][$mapperRecord] = $feProductData;
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    $subs['fixed_productdetails_missing'] = true;
                                                }
                                            }
                                            $feProductData['products']['components'] = $productComponent;
                                        }

                                        $reversedMappedProducts = [];
                                        $discounts = $options = [];
                                        // $allSocs has the socs for all products
                                        // $mappedSocs has the socs only for the products that could not be reversed mapped
                                        // $mappedSkus has all the skus that could be reversed mapped
                                        $allSocs = $mappedSocs = $mappedSkus = [];

                                        if (!isset($tariff) && isset($hidden)) {
                                            $tariff = array_shift($hidden);
                                        } elseif (!isset($tariff) && isset($pc['tariff'])) {
                                            // Get tariff from main Contract
                                            $tariff = $pc['tariff'];
                                        }

                                        $feProductData['phone_numbers'] = isset($phoneNumbers) ? $phoneNumbers : null;
                                        $feProductData['ctn'] = $productCtn ?? null;
                                        $feProductData['bundle_class'] = $bundleClass;
                                        $feProductData['bundle_tooltip'] = $bundleTooltip;
                                        $feProductData['part_of_bundle'] = $partOfBundle;
                                        if (isset($feProductData['options'])) {
                                            $feProductData['options_visibility'] = $this->isOneProductVisible($feProductData['options']);
                                        }
                                        if (isset($feProductData['discounts'])) {
                                            $feProductData['discounts_visibility'] = $this->isOneProductVisible($feProductData['discounts']);
                                        }
                                        $feProductData['install_base_product_id'] = $installProductId;
                                        $feProductData['components'] = $reversedMappedProducts;

                                        foreach ($fnAddons as $reversedFnAddon) {
                                            if (!empty($reversedFnAddon['sku'])) {
                                                $mappedSkus[] = $reversedFnAddon['sku'];
                                            }
                                        }

                                        $feProductData['mapped_socs'] = implode(',', $mappedSocs);
                                        $feProductData['mapped_skus'] = implode(',', $mappedSkus);
                                        $feProductData['all_socs'] = implode(',', $allSocs);
                                        $feProductData['service_line_id'] = $product['service_line_id'] ?: $subscription['standard_item_identification'];

                                        //validation fields per stack
                                        switch ($accountType) {
                                            case Vznl_Customer_Helper_Services::ACCOUNT_CATEGORY_BSL:
                                            case Vznl_Customer_Helper_Services::ACCOUNT_CATEGORY_PEAL:
                                                if (isset($feProductData['tariff']['sku'])) {
                                                    $subs['validationFields']['tariff'] = $feProductData['tariff']['sku'];
                                                }
                                                $subs['validationFields']['mapped_skus'] = $feProductData['mapped_skus'];
                                                $subs['validationFields']['service_line_id'] = $feProductData['service_line_id'];
                                                $subs['validationFields']['open_order'] = [];
                                                break;
                                        }

                                        $prods[$productId] = $feProductData;
                                        $subs = $subs + $feProductData;

                                        unset($tariff, $options, $discounts, $hidden, $phoneNumbers, $kipProductHasTriplePlay, $tvProductHasEnvContract, $reversedMappedProducts);
                                    }
                                }

                                if ($this->getSizeOf($allSubscriptionPhoneNo)) {
                                    $subs['all_phone_no'] = $allSubscriptionPhoneNo;
                                }

                                if ($productCtn) {
                                    $subs['ctn'] = $productCtn;
                                }

                                if ($packageType) {
                                    $subs['package_type'] = strtolower($packageType);
                                }

                                $ps[] = array_merge($subs,$initialContract ,['products' => $prods]);

                                unset($tariff, $options, $hidden, $discounts, $phoneNumbers, $allSubscriptionPhoneNo);
                            }
                            if ($this->getSizeOf($ps)) {
                                $pc['subscriptions'] = $ps;
                                $pc += $contractBundleMarkers;
                            }

                            $parsedCustomerProducts[$accountType][$pc['customer_number']]['contracts'][] = $pc;
                        }
                    }
                }
            }
        }

        $warning_type = '';
        if ($no_ordering_allowed) {
            $warning_type = $sell_fixed_to_blacklisted ? 'warning' : 'error';
        }

        if (isset($pc) && isset($parsedCustomerProducts[Vznl_Customer_Helper_Services::ACCOUNT_CATEGORY_PEAL])) {
            $parsedCustomerProducts[Vznl_Customer_Helper_Services::ACCOUNT_CATEGORY_PEAL][$pc['customer_number']]['blacklisted_warning_type'] = $warning_type;
        }

        return $parsedCustomerProducts;
    }

    /**
     * Returns the total amount of products for the current logged in customer
     *
     * @return int
     */
    public function getProductsCount()
    {
        $productsCount=0;
        $data=$this->getCustomerProducts();
        $types=array_keys($data);
        if (is_array($types)) {
            foreach ($types as $type) {
                if (isset($data[$type]) && is_array($data[$type])) {
                    foreach ($data[$type] as $customer) {
                        foreach ($customer['contracts'] as $contract) {
                            $productsCount+=$this->getSizeOf($contract['subscriptions']);
                        }
                    }
                }
            }
        }
        return $productsCount;
    }

    /**
     * Just for avoid WARNINGS from phpcs
     * @param $table
     * @return int
     */
    public function getSizeOf($table)
    {
        return count($table);
    }

    /**
     * @param $accountType
     * @param $component
     * @param $product
     * @return array
     */
    protected function reverseMultiMap($accountType, $component, $products = null)
    {
        switch ($accountType) {
            case Vznl_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS:
            case Vznl_Customer_Helper_Services::ACCOUNT_CATEGORY_BSL:
            case Vznl_Customer_Helper_Services::ACCOUNT_CATEGORY_PEAL:
                $result = $this->mapKiasComponent($component, $products);
                break;
            /*
            case Vznl_Customer_Helper_Services::ACCOUNT_CATEGORY_PEAL:
                $result = $this->mapPealComponent($component, $products);
                break;
            */
            case Vznl_Customer_Helper_Services::ACCOUNT_CATEGORY_KD:
                $result = $this->mapKdComponent($component, $products);
                break;
            case Vznl_Customer_Helper_Services::ACCOUNT_CATEGORY_FN:
                $result = $this->mapFnComponent($component, $products);
                break;
            default:
                $result = [];
                break;
        }

        return $result;
    }

    public function reverseMultiMapBSL($request, $mode)
    {
        if($mode == 'Base') {
            $ExternalId = $request['ExtendedID'];
            $month = $request['months'];
            $service_expression = "customer.subscription.commitment('".$month."')";
        } else {
            $ExternalId = $request['ExtendedID'];
            $service_expression = null;
            $month = null;
        }
        $mappers = Mage::getModel('vznl_multimapper/mapper')->getSkuByExternalID($ExternalId, null, $service_expression);

        return $mappers->getPpSku();
    }

    /**
     * Save customer ctns
     * @param $customer
     */
    public function saveCustomerCtns($customer) {
        foreach (Mage::getSingleton('dyna_customer/session')->getServiceCustomers() as $customersPerStack) {
            foreach ($customersPerStack as $customerData) {
                if (isset($customerData['contracts'])) {
                    foreach ($customerData['contracts'] as $contract) {
                        if (isset($contract['subscriptions'])) {
                            foreach ($contract['subscriptions'] as $subscription) {
                                if (!isset($subscription['ctn']['PhoneNumber']) || empty($subscription['ctn']['PhoneNumber']) || empty($contract['contract_id'])) continue;
                                $subscriptionStatus = strtotime($subscription['contract_end_date']) > strtotime('today') ? Omnius_Customer_Customer_DetailsController::CTN_STATUS_ACTIVE : '';
                                $ctn = $subscription['ctn']['PhoneNumber'];
                                $contractId = $contract['contract_id'];
                                $savedCtn = Mage::getModel('ctn/ctn')->loadCtn($ctn, $customer->getId());
                                try {
                                    if ($savedCtn->getCtn() || $savedCtn->getCode()) {
                                        if ($savedCtn->getCode() == $contractId) continue;
                                        $savedCtn
                                            ->setCode($contractId)
                                            ->setStatus($subscriptionStatus)
                                            ->setStartDate($contract['contract_start_date'])
                                            ->setEndDate($contract['contract_end_date'])
                                            ->setAdditional(array('products' => $subscription['products']))
                                            ->save();
                                    } else {
                                        Mage::getModel('ctn/ctn')
                                            ->setCtn($ctn)
                                            ->setCode($contractId)
                                            ->setCustomerId($customer->getId())
                                            ->setStatus($subscriptionStatus)
                                            ->setStartDate($contract['contract_start_date'])
                                            ->setEndDate($contract['contract_end_date'])
                                            ->setAdditional(array('products' => $subscription['products']))
                                            ->save();
                                    }
                                } catch (Exception $e) {
                                    Mage::log($e->getMessage());
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * sort customer products
     * @param  Array $product
     * @return Array $product
     */
    private function getSortedProduct($product)
    {
        $ad_array_position = [];
        $ad_array_name = [];
        foreach ($product as $key => $value) {
            foreach ($value['products'] as $pkey => $pvalue) {
                $pc_array_name[$key] = $value['name'];
                $pc_array_position[$key] = $value['position'];
            }
            if((count($pc_array_position) === count($pc_array_name)) && (count($pc_array_position) === count($value['products']))){
                array_multisort($pc_array_position, SORT_ASC, $pc_array_name, SORT_DESC, $value['products']);
                $ad_array_name[$key] = $value['name'];
                $ad_array_position[$key] = $value['position'];
            }
        }
        if((count($ad_array_position) === count($ad_array_name)) && (count($ad_array_position) === count($product))){
            array_multisort($ad_array_position, SORT_ASC, $ad_array_name, SORT_DESC, $product);
        }
        return $product;
    }

    protected function reverseMultiMapPEAL($soc, $productSkus = [])
    {
        $mappers = Mage::getModel('vznl_multimapper/mapper')->getMappersBySoc($soc);

        $skus = [];
        /** @var $dynaCoreHelper Dyna_Core_Helper_Data */
        $dynaCoreHelper = Mage::helper('dyna_core');
        foreach ($mappers ?? [] as $mapper) {
            if ($mapper->getServiceExpression()) {
                $passed = $dynaCoreHelper->evaluateExpressionLanguage(
                    $mapper->getServiceExpression(),
                    array('customer' => Mage::getSingleton('vznl_configurator/expression_customer'),
                        'order' => Mage::getModel('vznl_configurator/expression_order'))
                );
                if ($passed) {
                    if(in_array($mapper->getSku(), $productSkus)) {
                        continue;
                    }
                    $skus[] = trim($mapper->getSku());
                    break;
                }
            } else {
                // Get the first match item
                if(in_array($mapper->getSku(), $productSkus)) {
                    continue;
                }
                $skus[] = trim($mapper->getSku());
                break;
            }
        }

        return $skus;
    }

    public function getFixedHasInstalledBase($customer)
    {
        $customerLoggedIn = $customer->isCustomerLoggedIn();
        if ($customerLoggedIn && Mage::getSingleton('customer/session')->getHasFixedProducts()){
                return true;
        }
    }
}
