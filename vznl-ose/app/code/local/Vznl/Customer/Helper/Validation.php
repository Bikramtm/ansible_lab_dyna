<?php

/**
 * Class Vznl_Customer_Helper_Validation Helper
 */
class Vznl_Customer_Helper_Validation extends Dyna_Customer_Helper_Validation
{
    /**
     * Validation fields for myProducts buttons
     * @var array
     */
    public static $myProductsActionButtons = [
        'modify_package' => [
            'validation' => [
                'mapped_skus',
                'tariff',
                'open_order'
            ]
        ],
        'renew_package' => [
            'validation' => [
                'mapped_skus',
                'tariff',
                'open_order'
            ]
        ],
        'change_service' => [
            'validation' => [
                'address_id',
                'open_order'
            ]
        ],
        "add_red" => [
            'validation' => [
                'tariff',
            ]
        ],
        "create_red" => [
            'validation' => [
                'tariff',
            ]
        ],
        "relocation" => [
            'validation' => [
                'tariff',
                'open_order'
            ]
        ],
        "convert_to_member" => [
            'validation' => [
                'tariff',
                'open_order'
            ]
        ],
        "pre_post" => [
            'validation' => [
                'open_order',
            ],
        ],
        'inlife_sales' => [
            'validation' => [
                'open_order',
            ],
        ],
    ];

     /**
     * @param $params
     * @return array
     *
     *
     */
    public function validateCustomerAdvancedSearchParameters($params)
    {
        $this->searchParams = $params;

        foreach ($params as $key => $value) {
            if (!empty($value) && trim($value)) {
                $this->validateAdvancedSearchField($key, $value);
            }
        }

        $this->validateRequiredAdvanceSearchFieldsArePresent($params);

        ($this->error) ? $this->setValidationErrorResponse() : $this->setSuccessResponse($params);

        return $this->response;
    }

    /**
     * @param string $key
     * @param  string $value
     * @return void
     */
    private function validateAdvancedSearchField($key, $value)
    {
        switch ($key) {
            case 'ban':
                if (!is_numeric($value)) {
                    $this->error = true;
                    $this->errorMessage = $this->__('Invalid ban number');
                }
                break;
            case 'customer_ctn':
                if (!is_numeric($value)) {
                    $this->error = true;
                    $this->errorMessage = $this->__('Invalid mobile number');
                }
                break;
        }
    }

    /**
     * Perform the validation for an advanced search request.
     *
     * Required fields:
     * Consumer: CTN and Date of Birth
     * Business: BAN + Chamber of Commerce number
     * Business less 3 numbers: BAN/PIN + Chamber of Commerce number
     *
     * @param array $params
     * @return void
     */
    private function validateRequiredAdvanceSearchFieldsArePresent($params)
    {
        $customerType = $params['customer_type'] ?? '1';

        switch ($customerType) {
            case '1':
            default:
                $requiredFields = ['customer_ctn', 'dob'];
                break;
            case '2':
                $requiredFields = ['ban', 'company_coc'];
                break;
            case '3':
                $requiredFields = ['customer_ctn', 'company_coc'];
                break;
        }

        foreach ($requiredFields as $field) {
            if (!array_key_exists($field, $params)) {
                $this->errorMessage = $this->__('Customers can search sole basis of CTN + Date of birth, CTN + CoC number, BAN + CoC number');
                $this->error = true;
            }
        }
    }

    /**
     * Check if we have restrictions for myProducts buttons
     * @param $data
     * @return mixed
     */
    public function checkRestrictionsForButtons($data)
    {
        foreach ($data as $stack_name => &$stack_data) {
            foreach ($stack_data as &$stack_contracts) {
                if (!isset($stack_contracts['contracts'])) continue;
                foreach ($stack_contracts['contracts'] as &$contract) {
                    foreach ($contract['subscriptions'] as &$subscription) {
                        switch ($stack_name) {
                            case Vznl_Customer_Helper_Services::ACCOUNT_CATEGORY_PEAL:
                               $this->checkRestrictionsForButtonsStackwise($subscription);
                               break;
                        }
                    }
                }
            }
        }
        return $data;
    }

    /**
     * Check if we have restrictions for myProducts buttons for fixed ILS flow
     * @param $subscription
     * @return mixed
     */
    private function checkRestrictionsForButtonsStackwise(&$subscription)
    {
        $serviceAddress = $subscription['service_address'] ?? null;
        $needChangeServiceAddress = false;

        /** @var Vznl_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $serviceAddressTop = unserialize($quote->getServiceAddress());
        if (isset($serviceAddressTop)) {
            if (($serviceAddressTop['postalcode'] != $serviceAddress['postcode'])
                    || ($serviceAddressTop['street'] != $serviceAddress['street'])
                    || ($serviceAddressTop['housenumber'] != $serviceAddress['house_number'])
                    || ($serviceAddressTop['city'] != $serviceAddress['city'])) {
                $needChangeServiceAddress = true;
            }
        }

        return $subscription['disableButtons']['perform_serviceability'] = [
            'enabled' => empty($serviceAddressTop) || $needChangeServiceAddress ? 1 : 0,
            'serviceAddress' => $serviceAddress,
            'serviceAddressID' => md5(serialize($serviceAddress))
        ];
    }

    /**
     * Verify if agent has right permissions, and remove part of records if not
     * @param $data
     * @return mixed
     */
    public function checkAgentPermissions($data)
    {
        $customerSession = Mage::getSingleton('dyna_customer/session');
        $agent = $customerSession->getAgent();
        $agentHasAllowMob = !empty($agent) ? $agent->isGranted('ALLOW_MOB') : false;
        $agentHasAllowCab = !empty($agent) ? $agent->isGranted('ALLOW_CAB') : false;
        $agentHasAllowDsl = !empty($agent) ? $agent->isGranted('ALLOW_DSL') : false;
        $agentHasUnknownProducts = !empty($agent) ? $agent->isGranted('UNKNOWN_PRODUCTS') : false;

        // general permissions
        if (!$agentHasAllowCab) {
            unset($data[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD]);
        }

        if (!$agentHasAllowDsl) {
            unset($data[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN]);
        }

        if (!$agentHasAllowMob) {
            unset($data[Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS]);
        }

        // parse and remove unknown products if permission is not granted
        if (!$agentHasUnknownProducts) {
            foreach ($data as $stack_name => $stack_data) {
                foreach ($stack_data as $customer_number => $stack_contracts) {
                    if (!isset($stack_contracts['contracts'])) continue;
                    foreach ($stack_contracts['contracts'] as $key => $contract) {
                        foreach ($contract['subscriptions'] as $keySubscription => $subscription) {
                            // mobile and dsl
                            $flippedCategoriesArray = array_flip([
                                Vznl_Customer_Helper_Services::ACCOUNT_CATEGORY_PEAL,
                                Vznl_Customer_Helper_Services::ACCOUNT_CATEGORY_BSL,
                                Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KIAS,
                                Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_FN,]);
                            if (isset($flippedCategoriesArray[$stack_name])) {
                                if (empty($subscription['validationFields'])) {
                                    unset($data[$stack_name][$customer_number]['contracts'][$key]);
                                }
                            }
                        }
                    }
                }
            }
        }
        return $data;
    }
}
