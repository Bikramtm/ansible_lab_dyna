<?php

/**
 * Class Vznl_Customer_Helper_Data
 */
class Vznl_Customer_Helper_Data extends Dyna_Customer_Helper_Data
{
    CONST VODAFONE_CUSTOMER_CONFIG_PATH = 'vodafone_customer/data_usage/%s';
    CONST CHECK_CLUSTER_OO_INLIFE = 'oo-inlife';
    CONST CHECK_CLUSTER_OO_IMEI = 'oo-imei';
    CONST LIMIT_COUNT_SHOW = 50;

    /**
     * Get vodafone usage URL
     *
     * @return string
     */
    public function getUsageUrl()
    {
        return $this->getVodafoneCustomerConfig('usage_url');
    }

    /**
     * @return string
     */
    public function getNbaUrl()
    {
        $url = $this->getVodafoneCustomerConfig('nba_url');
        $agentHelper = Mage::helper('agent');
        $agent = $agentHelper->getAgent();
        $agentUsername = ($agent) ? $agent->getUsername() : '';

        $address = '';
        $callReason = '';
        if ($agentHelper->isRetailStore()) {
            $address = str_replace(',', '%2C', ($agent && $agent->getDealerId()) ? Mage::getModel('agent/dealer')->load($agent->getDealerId())->getAddress() : '');
            $callReason = 'Ubuy Retail';
        }

        // Replace url parameters as requested in RFC-150360
        $url = str_replace(array('{agentId}', '{agentGroup}', '{callReason}'), array($agentUsername, $address, $callReason), $url);

        return $url;
    }

    /**
     * Get vodafone config entry
     *
     * @param string $config
     * @return string
     */
    public function getVodafoneCustomerConfig($config)
    {
        if (is_string($config)) {
            return Mage::getStoreConfig(sprintf(self::VODAFONE_CUSTOMER_CONFIG_PATH, (string)$config));
        }
        throw new InvalidArgumentException('Invalid config path given. String expected, but other type was given.');
    }

    /**
     * @param $customer
     * @param bool $asObject
     * @return array
     */
    public function getInlifeOrders($customer, $asObject = true)
    {
        /** @var Vznl_Inlife_Helper_Data $h */
        $h = Mage::helper('vznl_inlife/data');
        /** @var Vznl_Inlife_Model_Client_InlifeClient $client */
        $client = $h->getClient('inlife', ['options' => []]);
        $orders = [];

        try {
            $response = $client->searchInlifeOrder($customer->getBan());
            $read = Mage::getSingleton('core/resource')->getConnection('core_read');
            if (isset($response['orders_list'])) {
                $ords = $response['orders_list'];
                if (is_array($ords) && isset($ords['order_id'])) {
                    $ords = [$ords];
                }
                foreach ($ords as $order) {
                    // Convert date format from epoch time
                    $order['created_date'] = substr($order['created_date'], 0, strlen($order['created_date']) - 3);
                    if (isset($order['external_order_i_d']) && !empty($order['external_order_i_d'])) {
                        $daOrderId = $order['external_order_i_d'];

                        $subQuery = Mage::getModel('superorder/superorder')->getCollection()
                            ->addFieldToSelect('entity_id')
                            ->addFieldToFilter('customer_id', $customer->getId())
                            ->getSelect();
                        $package = Mage::getModel('package/package')->getCollection()
                            ->addFieldToFilter('package_esb_number', $daOrderId)
                            ->addFieldToFilter('order_id', ['in' => $subQuery])
                            ->setPageSize(1)
                            ->getLastItem();

                        if ($package->getPackageId()) {
                            continue;
                        }
                    }
                    $orders[] = $asObject ? Mage::getModel('vznl_checkout/sales_inlifeOrder')->setData($order)->getData() : $order;
                }
            }
        } catch (Exception $e) {
            Mage::getSingleton('core/logger')->logException($e);
        }

        return $orders;
    }

    /**
     * Convert number format to 31xxx
     *
     * @param $value
     * @return string
     */
    public function parseCtn($value)
    {
        if(!is_string($value)){
            return '';
        }
        if (strpos($value, '06') === 0 || strpos($value, '097') === 0 )
            $value = '31' . substr($value, 1);
        elseif (strpos($value, '0031') === 0)
            $value = '31' . substr($value, 4);
        elseif (strpos($value, '+31') === 0)
            $value = '31' . substr($value, 3);
        return $value;
    }

    /**
     * Format the given post code
     * It will return an array with 2 formats
     *
     * @param $value string
     * @return array -- ['{number}{letters}', '{number} {letters}']
     */
    public function formatPostCode($value)
    {
        preg_match('/(\d+)/', $value, $numberMatches);
        preg_match('/([a-zA-Z]+)/', $value, $letterMatches);
        return array(
            ($numberMatches ? $numberMatches[0] : '') . ($letterMatches ? $letterMatches[0] : ''),
            ($numberMatches ? $numberMatches[0] : '') . ($letterMatches ? ' ' . $letterMatches[0] : ''),
        );
    }

    /**
     * Format the given house number.
     *
     * @param $value string
     * @return string
     */
    public function formatHouseNumber($value)
    {
        preg_match('/(\d+)/', $value, $numberMatches);
        preg_match('/([a-zA-Z]+)/', $value, $letterMatches);
        $toSearch = '';

        if (count($numberMatches) > 0) {
            $toSearch .= "%\n" . $numberMatches[0] . (count($letterMatches) > 0 ? "\n" . $letterMatches[0] : '') . '%';
        }

        return $toSearch;
    }

    /**
     * Retrieve customer from DA and map into local database
     * @param $requestParameters
     * @return Mage_Core_Model_Abstract|Mage_Customer_Model_Customer|mixed
     * @throws Exception
     */
    public function fetchCustomer($requestParameters)
    {
        /** @var Vznl_Service_Model_ResponseMapper $mapper */
        $mapper = Mage::getModel('vznl_service/responseMapper');
        /** @var Vznl_Service_Model_Client_DealerAdapterClient $client */
        $client = Mage::helper('dyna_service')->getClient('dealer_adapter');
        /** @var Vznl_Customer_Model_Customer $customer */
        $customer = Mage::getModel('customer/customer');
        if (!empty($requestParameters['entity_id'])) {
            $customer->load($requestParameters['entity_id']);
        }
        $customer->addData($requestParameters);
        $response = $client->getCustomerInfo($customer);
        if ($response) {
            /** @var Dyna_Service_Model_DotAccessor $accessor */
            $accessor = Mage::getSingleton('dyna_service/dotAccessor');
            $ban = $accessor->getValue($response, 'party_customer.customer.customer_account.id');
            if ($ban) {
                $customerExists = Mage::getModel('customer/customer')->getCollection()
                    ->addFieldToFilter('ban', $ban)->setPageSize(1, 1)->getLastItem();

                if ($customerExists->getId()) {
                    $customer = Mage::getModel('customer/customer')->load($customerExists->getId());
                } else {
                    $email = microtime(true) . uniqid(true) . Vznl_Customer_Model_Customer::DEFAULT_EMAIL_SUFFIX;
                    $customer->setEmail($email)->save();
                }

            } else {
                $email = microtime(true) . uniqid(true) . Vznl_Customer_Model_Customer::DEFAULT_EMAIL_SUFFIX;
                $customer->setEmail($email)->save();
            }
            $customer = $mapper->map('customer_info', $response, $customer);

            // Overwrite pin if set correctly
            if(isset($requestParameters['pin']) && isset($requestParameters['advanced_search']) && $requestParameters['advanced_search']){
                $customer->setPin($requestParameters['pin']);
            }
            $customer->save();

            return $customer;
        } else {
            throw new Exception('No customer was found.');
        }
    }

    /**
     * @param null $customerNumber
     * @param $packageTypes
     * @param null $ctn
     * @return Omnius_Superorder_Model_Mysql4_Superorder_Collection
     */
    public function getOSFOpenOrders($customerNumber = null, $packageTypes, $ctn = null, $serviceLineId = NULL)
    {
        if (!is_array($packageTypes)) {
            $packageTypes = [$packageTypes];
        }
        $packageTypes = array_map('strtolower', $packageTypes);

        $serviceCustomerNumbers = $this->getServiceCustomerNumbersForEachStack($customerNumber);

        $custNumbers = [];

        foreach (array_unique($packageTypes) as $packageType) {
            $custNumbers = array_merge(
                $custNumbers,
                $serviceCustomerNumbers[Dyna_Customer_Helper_Services::getStackPerPackage($packageType)]
            );
        }

        $sessionCustomerNumber = Mage::getSingleton('customer/session')->getCustomer()->getCustomerNumber();

        // Check db Orders
        /** @var Omnius_Superorder_Model_Mysql4_Superorder_Collection $superOrders */
        $superOrders = Mage::getModel('superorder/superorder')->getCollection()
            ->addFieldToSelect('entity_id')
            ->addFieldToFilter(
                'main_table.order_status',
                ['neq' => Dyna_Superorder_Model_Superorder::SO_COMPLETED_SUCCESS]
            )
            ->addFieldToFilter(
                'main_table.order_status',
                ['neq' => Dyna_Superorder_Model_Superorder::SO_COMPLETED_FAILED]
            )
            ->addFieldToFilter('main_table.customer_number', ['eq' => $sessionCustomerNumber]);

        $superOrders
            ->getSelect()
            ->group('main_table.entity_id')
            ->join(["package" => "catalog_package"], "main_table.entity_id = package.order_id", [])
            ->where('package.type IN(?)', $packageTypes);

        if (empty($custNumbers)) {
            $custNumbers = [Mage::getSingleton('customer/session')->getCustomer()->getCustomerNumber()];
        }

        if ($custNumbers) {
            $superOrders
                ->getSelect()
                ->where(
                    'package.parent_account_number IN (?) OR package.parent_account_number IS NULL',
                    array_map('strval', $custNumbers)
                );
        }

        if (!is_null($ctn)) {
            $superOrders
                ->getSelect()
                ->where('package.ctn = ?', $ctn);
        }

        return $superOrders;
    }

    /**
     * Get Customer Order Count
     */
    public function getCustomerOrderCount()
    {
        $customerSession = Mage::getSingleton('customer/session');
        $customerData = $customerSession->getCustomer()->getCustomData();
        $orderCounter = 0;
        /** @var Vznl_Data_Helper_Data $dataHelper */
        $dataHelper = Mage::helper('vznl_data/data');
        $section = 'open-orders';
        $sectionSkeleton = $dataHelper->getSectionSkeleton($section, true);
        $buckets = $sectionSkeleton['buckets'];
        $subject = $sectionSkeleton['subject'];
        $customerId = $customerSession->getCustomer()->getId();
        $agent = $customerSession->getSuperAgent() ?: $customerSession->getAgent(true);
        $websiteId = $subject == 'FAILED_VALIDATIONS' ? $agent->getPermittedValidationWebsites() : $agent->getPermittedSearchWebsites([], $subject);

        $agentId = 0;
        $dealerId = 0;
        if ($agent->isGranted('SEARCH_' . $subject . '_OF_AGENT')) {
            $agentId = $agent->getId();
        }
        if ($agent->isGranted('SEARCH_' . $subject . '_OF_DEALER')) {
            $dealerId = $agent->getDealer()->getDealerId();
        }
        if ($agent->isGranted('SEARCH_' . $subject . '_OF_GROUP')) {
            $agentId = $agent->getAgentsInSameGroup();
        }

        $orderCounter = $this->_getOrderCount($buckets, $agentId, $customerId, $dealerId, $section, $websiteId);
        return $orderCounter;
    }

    /**
	 * method to get the order count for cutomer 360 view
	 */
	protected function _getOrderCount($buckets, $agentId, $customerId, $dealerId, $section, $websiteId)
	{
	    $acceptedSections = Mage::getModel('vznl_package/package')->getAcceptedSections();
	    if (!in_array($section, $acceptedSections)) {
                throw new InvalidArgumentException(sprintf('Invalid section provided. Expected one of %s, got %s', join(', ', $this->acceptedSections), $section));
            }

	    $collection = $this->_getMainOrderCountQueryCollection($customerId, $agentId, $dealerId, $websiteId, true);

	    $orderNumbers = $customerWhere = array();
            $ooImei = $ooInlife = false ;
	    foreach ($buckets as $cluster) {
	        if ($cluster == self::CHECK_CLUSTER_OO_INLIFE) {
		    // skip oo-inlife
		    continue;
                } elseif ($cluster == self::CHECK_CLUSTER_OO_IMEI) {
            	// oo-imei excludes simOnly oders check seperate
            	$ooImei = true;
            	continue;
                }

		$customWhere[] = $this->_getClusterQueryFilter($cluster);
	    }

	    $where = implode('OR', $customWhere);
	    $collection->getSelect()->where($where);
	    $ordersCount = $collection->getSize();

		foreach ($collection as $item) {
			$orderNumbers[] = $item->getSuperOrderNumber();
		}

		$ooImeiCount = $this->_getOoImeiOrderCount($ooImei, $customerId, $agentId, $dealerId, $websiteId, $ordersCount, $orderNumbers);
		$ordersCount = $ooImeiCount;

		if ($ordersCount > self::LIMIT_COUNT_SHOW) {
			$ordersCount = '+50';
		}

		return $ordersCount;
	}

	/**
	 * method to get the order count with oo-imei cluster
	 */
	protected function _getOoImeiOrderCount($ooImei, $customerId, $agentId, $dealerId, $websiteId, $ordersCount, &$orderNumbers)
	{
		if ($ordersCount < self::LIMIT_COUNT_SHOW && $ooImei) {
			$collection = $this->_getMainOrderCountQueryCollection($customerId, $agentId, $dealerId, $websiteId, true);

			// Add extra joins that are required to check for a device product
			$collection->getSelect()->where('s.oo_imei = ?', 1);

			foreach ($collection as $item) {
				if (!in_array($item->getSuperOrderNumber(), $orderNumbers)) {
					$orderNumbers[] = $item->getSuperOrderNumber();
					$ordersCount++;
				}
			}
		}

		return $ordersCount;
	}

	/**
	 * method the get the main query to get customer order count
	 */
	protected function _getMainOrderCountQueryCollection(
	    $customerId,
        $agentId,
        $dealerId,
        $websiteId,
        $customerScreen = false,
        $orderId = null
    )
	{
	    $packageModel = Mage::getModel('vznl_package/package');
        $collection = $packageModel->getCollection();
        $collection->getSelect()
            ->join(['s' => 'superorder'], 'main_table.order_id = s.entity_id', ['super_order_number' => 'order_number'])
            ->join(['o' => 'sales_flat_order'], 's.entity_id = o.superorder_id', [])
            ->join(['oa' => 'sales_flat_order_address'], 'o.shipping_address_id = oa.entity_id', [])
            ->where('main_table.checksum is not null')
            ->where('o.edited = ?', 0);
        if($orderId) {
            $collection->getSelect()->where('s.entity_id = ?', $orderId)->group('s.entity_id');
        } else{
            $collection->getSelect()->group('s.entity_id');
        }
	    // Custom filters
        if (!$customerScreen) {
            $dateLimit = $packageModel->getDateLimit();
            $collection->getSelect()->where('s.entity_id >= (SELECT MIN(entity_id) FROM superorder WHERE created_at >= ?)', $dateLimit);
        }

        if ($customerId) {
            $collection->getSelect()->where('s.customer_id = ?', $customerId);
        }

        $this->_addOrderCountAgentIdFilter($agentId, $collection);
        $this->_addOrderCountDealerIdFilter($dealerId, $collection);
        $this->_addOrderCountWebsiteFilter($websiteId, $collection);

        $currentWebsite = Mage::app()->getWebsite();
        if (empty($agentId) && empty($dealerId) && $currentWebsite->getCode() == Vznl_Agent_Model_Website::WEBSITE_INDIRECT_CODE) {
            $collection->getSelect()->where('s.created_agent_id = ?', Mage::getSingleton('customer/session')->getAgent(true)->getId());
            $collection->getSelect()->where('s.created_website_id = ?', Mage::app()->getWebsite()->getId());
        }

        if ($currentWebsite->getCode() != Vznl_Agent_Model_Website::WEBSITE_BELCOMPANY_CODE) {
            $collection->getSelect()->where('s.is_vf_only = ?', 1);
        }

        return $collection;
	}

	/**
	 * add agent Id filter
	 */
	protected function _addOrderCountAgentIdFilter($agentId, &$collection)
	{
        if ($agentId) {
            if (is_array($agentId && count($agentId) != 0)) {
                $collection->getSelect()->where('s.created_agent_id IN (?)', $agentId);
            } else if(is_int($agentId)) {
                $collection->getSelect()->where('s.created_agent_id = ?', $agentId);
            }
        }
	}

	/**
	 * add dealer Id filter
	 */
	protected function _addOrderCountDealerIdFilter($dealerId, &$collection)
	{
        if ($dealerId) {
            if (is_array($dealerId)) {
                $collection->getSelect()->where('(s.created_dealer_id IN(?) OR oa.delivery_store_id IN(?))', $dealerId, $dealerId);
            } else {
                $collection->getSelect()->where('(s.created_dealer_id = ? OR oa.delivery_store_id = ?)', $dealerId, $dealerId);
            }
        }
	}

	/**
	 * add Website Filters
	 */
	protected function _addOrderCountWebsiteFilter($websiteId, &$collection)
	{
        //If website id is true, the agent may search orders of ALL websites. The filter should not be applied.
        if ($websiteId && $websiteId != -1) {
            if (is_array($websiteId)) {
                $collection->getSelect()->where('s.created_website_id IN(?)', $websiteId);
            } else {
                $collection->getSelect()->where('s.created_website_id = ?', $websiteId);
            }
        }
	}

	/**
	 * method to get the additional filter based on cluster
	 */
	protected function _getClusterQueryFilter($cluster)
	{
		$where = '';
		switch ($cluster) {
            case 'oo-other-store' :
                $session = Mage::getSingleton('customer/session');
                $agent = $session->getSuperAgent() ?: $session->getAgent(true);
                $dealerIdNow = $agent->getDealer()->getDealerId();
                $telesalesStoreId = Mage::app()->getWebsite(Vznl_Agent_Model_Website::WEBSITE_TELESALES_CODE)->getId();
				$where = "(oa.delivery_store_id = '$dealerIdNow' AND (s.created_website_id = '$telesalesStoreId' OR s.dealer_id != oa.delivery_store_id)
				    AND s.order_status = '" . Vznl_Superorder_Model_Superorder::SO_FULFILLMENT_IN_PROGRESS . "'
					AND main_table.creditcheck_status = '" . Vznl_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_CC_APPROVED . "'
				)";
                break;

			case 'oo-error' :
			    $orderStatus = implode("','",[
                    Vznl_Superorder_Model_Superorder::SO_CANCELLED,
                    Vznl_Superorder_Model_Superorder::SO_CLOSED,
                    Vznl_Superorder_Model_Superorder::SO_FULFILLED,
                    Vznl_Superorder_Model_Superorder::SO_STATUS_CANCELLED,
                    Vznl_Superorder_Model_Superorder::SO_STATUS_REJECTED,
                    Vznl_Superorder_Model_Superorder::SO_VALIDATION_FAILED,
                ]);
			    $where = "(main_table.status <> '" .  Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_DELIVERED . "'
				    AND (s.error_code IS NOT NULL OR main_table.vf_status_code IS NOT NULL AND TRIM(main_table.vf_status_code) <> '')
				    AND s.order_status NOT IN('". $orderStatus . "')
				)";
                break;

			case 'oo-ready' :
			    $where = "((s.error_code IS NULL OR main_table.vf_status_code IS NULL AND TRIM(main_table.vf_status_code) = '')
				    AND s.order_status = '" . Vznl_Superorder_Model_Superorder::SO_FULFILLMENT_IN_PROGRESS . "'
				)";
				break;

			case 'oo-check' :
                $ccStatuses = Vznl_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUSES_CC_RUNNING;
                $ccStatuses[] = Vznl_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_CC_PARTIAL;
				$ccStatusCheck = implode("','", $ccStatuses);

                $npStatuses = Vznl_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUSES_NP_RUNNING;
                $npStatuses[] = Vznl_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_NP_REJECTED;
				$npStatusCheck = implode("','", $npStatuses);

				$orderStatus = implode("','", [
                    Vznl_Superorder_Model_Superorder::SO_CANCELLED,
                    Vznl_Superorder_Model_Superorder::SO_CLOSED,
                    Vznl_Superorder_Model_Superorder::SO_FULFILLED,
                    Vznl_Superorder_Model_Superorder::SO_STATUS_CANCELLED,
                    Vznl_Superorder_Model_Superorder::SO_STATUS_REJECTED,
                    Vznl_Superorder_Model_Superorder::SO_VALIDATION_FAILED,
                    Vznl_Superorder_Model_Superorder::SO_FAILED,
                    Vznl_Superorder_Model_Superorder::SO_FULFILLMENT_IN_PROGRESS,
                    Vznl_Superorder_Model_Superorder::SO_WAITING_VALIDATION_ACCEPTED_AXI,
                ]);

				$where = "(s.error_code IS NULL AND (main_table.creditcheck_status IN('" . $ccStatusCheck . "') OR main_table.porting_status IN('" . $npStatusCheck . "'))
				    AND s.order_status NOT IN('" . $orderStatus . "')
				)";
				break;

            case 'oo-other' :
			    $orderStatus = implode("','", [
                    Vznl_Superorder_Model_Superorder::SO_CANCELLED,
                    Vznl_Superorder_Model_Superorder::SO_CLOSED,
                    Vznl_Superorder_Model_Superorder::SO_FULFILLED,
                    Vznl_Superorder_Model_Superorder::SO_VALIDATED,
                    Vznl_Superorder_Model_Superorder::SO_STATUS_CANCELLED,
                    Vznl_Superorder_Model_Superorder::SO_STATUS_REJECTED,
                    Vznl_Superorder_Model_Superorder::SO_VALIDATION_FAILED,
                ]);

                /**
                 * Order should be visible regardless of the combination of status.
                 */
                /*$npStatus = implode("','", [
                    Vznl_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_NP_INITIAL,
                    Vznl_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_NP_PENDING,
                    Vznl_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_NP_REJECTED,
                ]);

                $oStatus = implode("','", [
                    Vznl_Superorder_Model_Superorder::SO_CANCELLED,
                    Vznl_Superorder_Model_Superorder::SO_STATUS_CANCELLED,
                ]);*/

				/*$where = "(s.order_status IN('" . $orderStatus . "')
				    AND ((main_table.porting_status NOT IN ('" . $npStatus . "') OR s.order_status IN('" . $oStatus . "') OR s.peal_order_id IS NOT NULL)
					    OR main_table.porting_status IS NULL)
				)";*/
                $where = "(s.order_status IN('" . $orderStatus . "'))";
                break;

			case 'oo-other-open' :
			    $orderStatus = implode("','", [
				    Vznl_Superorder_Model_Superorder::SO_VALIDATED,
					Vznl_Superorder_Model_Superorder::SO_WAITING_VALIDATION_ACCEPTED_AXI,
					Vznl_Superorder_Model_Superorder::SO_FAILED,
					Vznl_Superorder_Model_Superorder::SO_INITIAL
			    ]);

				$npStatus = implode("','", [
					Vznl_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_NP_INITIAL,
					Vznl_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_NP_PENDING,
					Vznl_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_NP_REJECTED,
				]);

			    $where = "(((s.error_code IS NULL OR s.error_code = '') AND (main_table.vf_status_code IS NULL or main_table.vf_status_code = ''))
				    AND s.order_status IN('" . $orderStatus . "') 
					AND (main_table.porting_status IS NULL OR main_table.porting_status NOT IN('" . $npStatus ."'))
				)";
				break;

			case 'oo-imei' :
                // Orders should either not have an actual porting date yet or the porting date should be in the future.
                            $dateTime = new DateTime();
                $where = "((main_table.device_name IS NOT NULL OR pt.value = aov.option_id) AND main_table.imei IS NULL
                    AND main_table.porting_status = '" . Vznl_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_NP_AUTHORIZED . "'
					AND (main_table.actual_porting_date IS NULL OR '" .  $dateTime->format('Y-m-d') . "' < DATE(main_table.actual_porting_date))
				)";
				break;

            default:
                throw new Exception('Unknown cluster set for this section');
        }

		return $where;
	}

    /**
     * @param $quote
     * @return array
     */
    public function filterRetainableCtns($quote)
    {
        $getRetainableCtns = $this->getRetainableCtns($quote);

        /*
        retainability_indicator
        Can be;
        - Retainable
        - Block Over Ruleable (= overuleable by backoffice)
        - Blocked (= not retainable)
        */
        // get all CTNs
        $ctns = isset($getRetainableCtns['ctns']) ? $getRetainableCtns['ctns'] : array();
        // retrieve all CTNS in a package
        $ctnInPackages = $this->getCTNsFromPackages($quote);
        // remove ctns already in a package
        $ctnsNotInPackages  = array_filter(
            $ctns,
            function ($ctn) use ($ctnInPackages) {
                return !in_array($ctn['ctn'], $ctnInPackages);
            }
        );
        // get retainable CTNs
        $retainable = isset($getRetainableCtns['retainable']) ? $getRetainableCtns['retainable'] : array();
        $allRetainableCount = count($retainable);

        // check if one-of-deal is active
        $isOneOffDeal = Mage::helper('omnius_configurator/cart')->isOneOfDealActive();
        if ($isOneOffDeal) {
            // if already on of deal return all ctns not in packages as retainable
            $isOneOffDeal = false;
            $responseCTNs = $ctnsNotInPackages;
        } else {
            $retainableNotInPackages = array_filter(
                $retainable,
                function ($ctn) use ($ctnInPackages) {
                    return !in_array($ctn['ctn'], $ctnInPackages);
                }
            );
            if ($allRetainableCount != count($retainableNotInPackages)) {
                // there are retainables in a package, so no oneOff deal
                $isOneOffDeal = false;
            } else {
                $isOneOffDeal = Mage::getModel('ctn/ctn')->setCustomerId($quote->getCustomerId())->canActivateOneOffDeal(
                    count($ctns),       // all CTNs
                    $allRetainableCount
                );
            }
            $responseCTNs = $retainableNotInPackages;
        }

        $allCustomers = Mage::helper('dyna_customer/panel')->getCustomerProducts();
        if ($allCustomers['BSL']) {
            $bslProducts = $allCustomers['BSL'];
            foreach ($bslProducts as $accountType => $contracts) {
                $contracts = isset($contracts['contracts']) ? $contracts['contracts'] : [];
            }
        }

        return array(
            'creditProfileCheckResult' => isset($getRetainableCtns) ? $getRetainableCtns : [],
            'oneOffDeal' => $isOneOffDeal,
            'ctns' => array_values($responseCTNs),
            'retentions' => $contracts,
        );
    }

    /**
     * @param $quote
     * @return array
     * Return a list of ctns that are already in a package
     */
    private function getCTNsFromPackages($quote)
    {
        // get ctns already in one of the cart packages
        $ctnInPackages = array();
        $packages = $quote->getPackages();

        foreach ($packages as $package) {
            $packageCtns = isset($package['ctn']) ? [$package['ctn']] : [];
            if (!empty($packageCtns)) {
                $ctnInPackages = array_merge($ctnInPackages, $packageCtns);
            }
        }

        return $ctnInPackages;
    }

    /**
     * Get retainable ctns from provided quote's customer
     *
     * @param $quote
     * @return array
     */
    public function getRetainableCtns($quote)
    {
        /** @var Dyna_Service_Model_DotAccessor $dot */
        $dot = Mage::getSingleton('dyna_service/dotAccessor');
        /** @var Dyna_Service_Model_Client_DealerAdapterClient $client */
        $client = Mage::helper('vznl_service')->getClient('dealer_adapter');
        $customer = $quote->getCustomer();

        if (!$customer) {
            return array(
                'error' => true,
                'message' => $this->__('No customer currently logged in.')
            );
        }

        if ($customer->getIsBusiness()) {
            if (trim($customer->getCompanyCoc()) == '') {
                return array(
                    'error' => true,
                    'serviceError' => $this->__('Service Call Error'),
                    'message' => $this->__('CoC number for this business customer is not set.')
                );
            }
        }

        try {
            $result = $client->getCreditProfile($customer);
            Mage::getSingleton('customer/session')->setRetainableCtnsChecked($customer->getCustomerNumber());
        } catch (LogicException $e) {
            return array(
                'error' => true,
                'manualOverride' => true,
            );
        } catch (Exception $e) {
            Mage::getSingleton('core/logger')->logException($e);

            return array(
                'error'        => true,
                'serviceError' => $this->__('Service Call Error'),
                'message'      => $e->getMessage(),
            );
        }

        // get all CTNs
        $ctns = $this->getAllCTNs($dot, $result);
        // get retainable CTNs
        $retainable = array_filter($ctns, function($ctn) { return $ctn['retainable']; });

        return array(
            'ctns' => $ctns,
            'retainable' => $retainable,
            'creditProfileCheckResult' => $this->__($dot->getValue($result, 'party_customer.customer.customer_credit_profile.credit_profile_check_results.result_code')),
        );
    }

    /**
     * @param Dyna_Service_Model_DotAccessor $dot
     * @param array $result
     * @return array
     * Parse the credit profile string and get all ctns
     */
    private function getAllCTNs($dot, $result)
    {
        $statuses = array();
        foreach ($dot->getValue($result, 'party_end_user.subscription', array()) as $key => $value) {
            if (is_numeric($key)) {
                $statuses[] = array(
                    'status' => $dot->getValue($value, 'status'),
                    'ctn' => $dot->getValue($value, 'ctn'),
                    'retainable' => $dot->getValue(
                            $value,
                            'customer_account.customer.customer_credit_profile.retainability_indicator'
                        ) == 'Retainable',
                    'retainableStatus' => $dot->getValue($value,
                        'customer_account.customer.customer_credit_profile.retainability_indicator'),
                    'reasoncode' => $dot->getValue($value,
                        'customer_account.customer.customer_credit_profile.credit_profile_check_results.result_code')
                );
            } else {
                $statuses = array(
                    array(
                        'status' => $dot->getValue($result, 'party_end_user.subscription.status'),
                        'ctn' => $dot->getValue($result, 'party_end_user.subscription.ctn'),
                        'retainable' => $dot->getValue(
                                $result,
                                'party_end_user.subscription.customer_account.customer.customer_credit_profile.retainability_indicator'
                            ) == 'Retainable',
                        'retainableStatus' => $dot->getValue($result,
                            'party_end_user.subscription.customer_account.customer.customer_credit_profile.retainability_indicator'),
                        'reasoncode' => $dot->getValue($result,
                            'party_end_user.subscription.customer_account.customer.customer_credit_profile.credit_profile_check_results.result_code')
                    )
                );
                return $statuses;
            }
        }

        return $statuses;
    }

    /**
     * Generate a 15 chars id + suffix
     *
     * @return string
     * This function was brought from Omnius_Customer_Helper_Data because it doesn't exist in either Vznl_Customer_Helper_Data or Dyna_Customer_Helper_Data
     */
    public function generateDummyEmail()
    {
        //return uniqid(gmp_strval(rand(1, 1295), 10, 36)) . Vznl_Customer_Model_Customer::DUMMY_EMAIL_SUFFIX;
        return uniqid(base_convert(rand(1, 1295), 10, 36)) . Vznl_Customer_Model_Customer::DUMMY_EMAIL_SUFFIX;
    }

    /**
     * TODO: This method should return non-osf orders for given customer (that are not local) - needs implementation
     */
    public function hasNonOSFOrders($customerNumber = null, $packageTypes, $ctn = null)
    {
        return false;
    }

    public function getRefundReasonsChangeOptions($store = null)
    {
        return $this->_prepareOptions(
            Mage::getStoreConfig('checkout/cart/refund_reason_change', $store)
        );
    }

    /**
     * Unserialize and clear options
     *
     * @param string $options
     * @return array|bool
     */
    protected function _prepareOptions($options)
    {
        $options = trim($options);
        if (empty($options)) {
            return false;
        }
        $result = array();
        $options = explode("\n", $options);
        foreach ($options as $value) {
            $values = explode(':', trim($value));
            $code = '';
            $label = '';

            if (strlen($values[0])) {
                $code = $this->escapeHtml(trim($values[0]));
                $label = $this->escapeHtml(trim($values[0]));
            }

            if (isset($values[1])) {
                $label = $this->escapeHtml(trim($values[1]));
            }

            $result[$code] = $this->__($label);
        }
        return $result;
    }

    public function getRefundReasonsCancelOptions($store = null)
    {
        return $this->_prepareOptions(
            Mage::getStoreConfig('checkout/cart/refund_reason_cancel', $store)
        );
    }

    /**
     * @param string $type
     * @param bool $flip
     * @return string
     */
    public function mapCustomerIDType(string $type, bool $flip = false): string
    {
        $identificationTypes = [
            'P' => 'PASSPORT',
            'R' => 'DUTCH_DRIVERS_LICENCE',
            'N' => 'ID_CARD',
            '0' => 'TYPE_EU',
            '1' => 'TYPE_I',
            '2' => 'TYPE_II',
            '3' => 'TYPE_III',
            '4' => 'TYPE_IV',
            '5' => 'SENIORENPAS',
            '6' => 'TOERISTENKAART',
            '7' => 'COC_NUMBER',
            '8' => 'VAT_NUMBER',
            '9' => 'LEGAL_FORM'
        ];

        if ($flip) {
            $identificationTypes = array_flip($identificationTypes);
        }

        return $identificationTypes[$type] ?? $type;
    }

    public function unloadCustomer()
    {
        /** @var Omnius_Checkout_Model_Sales_Quote $theQuote */
        $theQuote = Mage::getSingleton('checkout/cart')->getQuote();
        $theQuote->setIsActive(false)->save();

        // Unset RetrieveCustomerInfo data
        $this->getCustomerSession()->unsServiceCustomers();
        $this->getDynaCustomerSession()->unsCustomerOrders();

        // Unset Campaign Offer Product Compatibility checks
        $this->getCustomerSession()->unsOfferCompatibility();
        $this->getCustomerSession()->unsKiasLinkedAccountNumber();
        $this->getCustomerSession()->unsKdLinkedAccountNumber();
        $this->getCustomerSession()->unsSubscriberCtn();
        $this->getCustomerSession()->clearCustomerOrders();

        $this->getCustomerSession()->setData('household_members_count', 0);

        /** Clear serviceability data from session when unloading customer */
        Mage::getSingleton('dyna_address/storage')->clearAddress();

        // Clear session migration / move offnet
        $this->clearMigrationData();

        $this->getCustomerSession()->unsetCustomer();
        $this->getCustomerSession()->unsOrderEdit();
        $this->getCustomerSession()->unsOrderEditMode();
        $this->getCustomerSession()->unsNewEmail();
        $this->getCustomerSession()->unsWelcomeMessage();
        $this->getCustomerSession()->unsProspect();
        $this->getCheckoutSession()->unsIsEditMode();
        $this->getCheckoutSession()->setCustomerInfoSync(false);
        $this->getCheckoutSession()->setHasFixedProducts(false);
        $this->getCheckoutSession()->clear();

        // Clear active bundle
        $this->getCustomerSession()->unsActiveBundleId();

        // Clear customer products
        $this->getCustomerSession()->clearCustomerProducts();
        $this->getCustomerSession()->setData('all_installed_base_products', null);

        // Unset process context
        $this->getDynaCustomerSession()->setData('process_context', null);
        Mage::getSingleton('core/session')->setResetResultArray(null);
        Mage::getSingleton('customer/session')->setHardwareDetailsIls(null);
    }

    /**
     * Save OO-IMEI to order
     * @param Vznl_Superorder_Model_Superorder $superOrder
     * @return void
     * @throws Mage_Core_Exception
     * @todo This can be refactored or moved to another step of order processing
     */
    public function setOoImeiInOrder(
        Vznl_Superorder_Model_Superorder $superOrder
    ):void
    {
        $dealerId = Mage::helper('agent')->getDaDealerCode($superOrder->getAgent());
        $agentId = Mage::helper('agent')->getAgentId();
        $websiteId = Mage::app()->getWebsite()->getId();
        $collection = $this->_getMainOrderCountQueryCollection(
            $superOrder->getCustomerId(),
            $agentId,
            $dealerId ,
            $websiteId,
            true,
            $superOrder->getId()
        );
        $where = $this->_getClusterQueryFilter(Vznl_Customer_Helper_Data::CHECK_CLUSTER_OO_IMEI);
        $collection->getSelect()->where($where);

        // Add extra joins that are required to check for a device product
        $collection->getSelect()
            ->join(['oi' => 'sales_flat_order_item'], 'o.entity_id = oi.order_id AND oi.package_id = main_table.package_id', [])
            ->join(['a' => 'eav_attribute'], 'a.attribute_code = "identifier_package_subtype"', [])
            ->join(['pt' => 'catalog_product_entity_int'], 'oi.product_id = pt.entity_id AND pt.attribute_id = a.attribute_id', [])
            ->join(['ao' => 'eav_attribute_option'], 'pt.attribute_id = ao.attribute_id', [])
            ->join(['aov' => 'eav_attribute_option_value'], 'ao.option_id = aov.option_id AND aov.value = "Device"', []);

        $orderHelper = Mage::helper('vznl_superorder');
        $nonSimOnlyOrderPackages = $orderHelper->excludeSimOnlyOrders($collection);
        if (count($nonSimOnlyOrderPackages) > 0){
            $superOrder->setOoImei(1);
            $superOrder->save();
        }
    }

    /**
     * Get privacy directory value for the given key
     * @param string $key
     * @param Mage_Customer_Model_Customer $customer
     * @return mixed|string The value if any
     */
    public function getPrivacyDirectoryValue($key, Mage_Customer_Model_Customer $customer)
    {
        switch ($key) {
            case 'SMS':
                $value = $customer->getData('privacy_sms');
                break;
            case 'EMAIL':
                $value = $customer->getData('privacy_email');
                break;
            case 'DIRECTORY_LISTING':
            case 'PHONEBOOK_LISTING':
                if ($customer->getData('privacy_phone_books')) {
                    $value = 'InfoGids';
                } elseif ($customer->getData('privacy_number_information') && !$customer->getData('privacy_phone_books')) {
                    $value = 'Info';
                } elseif (!$customer->getData('privacy_number_information') && !$customer->getData('privacy_phone_books')) {
                    $value = 'No';
                }
                break;
            default:
                throw new InvalidArgumentException('No valid key specified');
        }
        return $value;
    }
}
