<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */


/**
 * Class Vznl_Customer_Model_Customer_Resource_Customer
 */
class Vznl_Customer_Model_Customer_Resource_Customer extends Omnius_Customer_Model_Customer_Resource_Customer
{
    /**
     * Add pan, account_identifier_mobile, account_identifier_fixed, account_identifier_linkid  to the default fields
     * 
     * @return array
     */
    protected function _getDefaultAttributes()
    {
        return array_unique(array_merge(array(
            'pan',
            'account_identifier_mobile',
            'account_identifier_fixed',
            'account_identifier_linkid',
            'customer_number'
        ), parent::_getDefaultAttributes()), SORT_REGULAR);
    }
}
