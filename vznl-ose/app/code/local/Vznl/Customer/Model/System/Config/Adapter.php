<?php

class Vznl_Customer_Model_System_Config_Adapter
{
    const SEARCH_ADAPTER = 'Vznl_Customer_Adapter_Search_Factory';

    public function toOptionArray()
    {
        return array(
            array(
                'value' => Omnius_Customer_Model_System_Config_Adapter::STUB_ADAPTER,
                'label' => 'Stub'
            ),
            array(
                'value' => Vznl_Customer_Model_System_Config_Adapter::SEARCH_ADAPTER,
                'label' => 'Search'
            )
        );
    }
}
