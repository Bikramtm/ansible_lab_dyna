<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */


/**
 * Class Customer
 */
class Vznl_Customer_Model_Customer extends Dyna_Customer_Model_Customer
{
    const DEFAULT_EMAIL_SUFFIX = '@ssfe.vodafone.com';
    const DUMMY_EMAIL_SUFFIX = '_geen@vodafone.com';
    CONST DUMMY_EMAIL_ADDRESS = "dummy@upc.nl";
    const CUSTOMER_SEARCH_STACK_PATH = 'omnius_service/customer_search_implementation_configuration/customer_search_stack';

    /**
     * @param array $party
     * @return string
     */
    protected function mapCustomerFirstName(array $party) : string
    {
        if (isset($party['_name'])) {
           return $party['_name'];
        } elseif (isset($party['_firstName'])) {
           return $party['_firstName'];
        }
        return null;
    }

    /**
     * @param array $party
     * @return string
     */
    protected function mapCustomerLastName(array $party) : string
    {
        if (isset($party['_name'])) {
           return $party['_name'];
        } elseif (isset($party['_lastName'])) {
           return $party['_lastName'];
        }
        return null;
    }

    private function mapField(array $requestData, $linkid="") : array
    {
        $dob = null;
        if (isset($requestData['_party']['_dateOfBirth'])) {
            $dob = explode('T', $requestData['_party']['_dateOfBirth']);
            $dob = date('Y-m-d H:i:s', strtotime($dob[0]));
        }

        $data = [
            'order_numbers' => $requestData['_orderNumbers'] ?? [],
            'account_identifier' => $requestData['_accountIdentifier'],
            'party_identifier' => $requestData['_party']['_identifier'],
            'customer_number' => $requestData['_accountNumber'],
            'firstname' => $this->mapCustomerFirstName($requestData['_party']),
            'lastname' => $this->mapCustomerLastName($requestData['_party']),
            'dob' => $dob,
            'customer_status' => $requestData['_accountStatus'] === 'active' ? true : false,
            'account_category' => $requestData['_lineOfBusiness'],
            'phone' => $requestData['_party']['_groupedDetails']['phone'][0]['number'] ?? "",
            'email' => $requestData['_party']['_groupedDetails']['email'][0]['emailAddress'] ?? "",
            'linkid' => $linkid,
            'kvk' => $requestData['_party']['_chamberOfCommerce'] ?? null,
            'search_customer_password' => $requestData['_party']['_meta']['customerpassword'] ?? null
        ];
        return $data;
    }

    /**
     * mapping the search response fields to the corresponding ones in OSE
     * @param array $requestData
     * @return array
     */
    public function mapSearchFields(array $requestData) : array
    {
        if (isset($requestData['_linkId'])) {
            $data=$this->mapField($requestData, $requestData['_linkId']);
        } else {
            $data=$this->mapField($requestData);
        }
        if(isset($requestData['_linkedAccounts'])) {
            foreach ($requestData['_linkedAccounts'] as $rdata) {
                $data['linked_accounts'][] = $this->mapField($rdata, $requestData['_linkId'] ?? "");
            }
        }
        return $data;
    }

    /**
     * Create new Customer with product data
     * @param  array
     * @return $customerid
     */
    private function newCustomer($customerObject)
    {
        $customer = Mage::getModel('customer/customer')
            ->setFirstname($customerObject['firstname'])
            ->setLastname($customerObject['lastname'])
            ->setAdditionalEmail($customerObject['email'])
            ->setPhoneNumber($customerObject['phone'])
            ->setEmail(uniqid() . Omnius_Customer_Model_Customer_Customer::DEFAULT_EMAIL_SUFFIX)
            ->setIsProspect(true)
            ->save();

        if($customerObject['account_category'] == 'mobile')
            $customer->setBan($customerObject['customer_number'])->save();
        if($customerObject['account_category'] == 'fixed')
            $customer->setPan($customerObject['customer_number'])->save();

        return $customer->getId();
    }

    private function saveCustomerAccountData($customerObject, $customer, $islinked=false)
    {
        //updating AccountIdentifierMobile
        if($customerObject['account_category'] == "mobile") {
            //updating BAN if Null
            if($customer->getBan() == "") {
                $customer->setBan($customerObject['customer_number']);
            }

            if(!$islinked) {
                $customer->setPan('');
            }

            if($customer->getAccountIdentifierMobile() == "") {
                $customer->setAccountIdentifierMobile($customerObject['account_identifier']);
            }

            if($customer->getPartyIdentifierMobile() == "" && !empty($customerObject['party_identifier'])) {
                $customer->setPartyIdentifierMobile($customerObject['party_identifier']);
            }
        }

        //updating AccountIdentifierFixed
        if($customerObject['account_category'] == "fixed") {
            //updating Peal Account Number
            if($customer->getPan() == "") {
                $customer->setPan($customerObject['customer_number']);
            }

            if(!$islinked) {
                $customer->setBan('');
            }

            if($customer->getAccountIdentifierFixed() == "") {
                $customer->setAccountIdentifierFixed($customerObject['account_identifier']);
            }

            if($customer->getPartyIdentifierFixed() == "" && !empty($customerObject['party_identifier'])) {
                $customer->setPartyIdentifierFixed($customerObject['party_identifier']);
            }
        }

        if($customerObject['kvk']) {
            $customer->setCompanyCoc($customerObject['kvk']);
        }

        if($customerObject['dob']) {
            $customer->setDob($customerObject['dob']);
        }

        if($customerObject['phone']) {
            $customer->setPhoneNumber($customerObject['phone']);
        }

        if($customerObject['search_customer_password']) {
            $customer->setPin($customerObject['search_customer_password']);
        }

        $customer->setAccountIdentifierLinkid($customerObject['linkid']);
        $customer->save();

        return $customer;
    }

    public function syncCustomerData($data)
    {
        if (isset($data['linked_accounts'])) {
            foreach ($data['linked_accounts'] as $customerData) {
                //get customer ID with order Number
                if (isset($customerData['order_numbers'][0])) {
                    $package = Mage::getModel('superorder/superorder')->getCollection()
                        ->addFieldToFilter('order_number', $customerData['order_numbers'][0])
                        ->setPageSize(1, 1)
                        ->getLastItem();
                    $customerid = $package->getCustomerID();
                } else if (isset($data['customer_number'])) {
                    $customer = Mage::getModel('customer/customer')->getCollection()
                        ->addFieldToFilter($customerData['account_category'] == 'mobile' ? 'ban' : 'pan', $customerData['customer_number'])
                        ->setPageSize(1, 1)
                        ->getLastItem();
                    $customerid=$customer->getId();
                }

                //is prospect customer
                if (!isset($customerid)) {
                    $customer=Mage::getModel('customer/customer')->getCollection()
                        ->addFieldToFilter($customerData['account_category'] == 'mobile' ? 'account_identifier_mobile' : 'account_identifier_fixed', $customerData['account_identifier'])
                        ->setPageSize(1, 1)
                        ->getLastItem();
                    $customerid=$customer->getId();
                }
            }
            if (isset($customerid)) {
                $this->mergeCustomers($customerid);
            }
        } else {
            if (isset($data['order_numbers'][0])) {
                $package=Mage::getModel('superorder/superorder')->getCollection()
                    ->addFieldToFilter('order_number', $data['order_numbers'][0])
                    ->setPageSize(1, 1)
                    ->getLastItem();
                $customerid=$package->getCustomerID();
            } else if (isset($data['customer_number'])) {
                $customer=Mage::getModel('customer/customer')->getCollection()
                    ->addFieldToFilter($data['account_category'] == 'mobile' ? 'ban' : 'pan', $data['customer_number'])
                    ->setPageSize(1, 1)
                    ->getLastItem();
                $customerid=$customer->getId();
            }

            //is prospect customer
            if (!isset($customerid)) {
                $customer=Mage::getModel('customer/customer')->getCollection()
                    ->addFieldToFilter($data['account_category'] == 'mobile' ? 'account_identifier_mobile' : 'account_identifier_fixed', $data['account_identifier'])
                    ->setPageSize(1, 1)
                    ->getLastItem();
                $customerid=$customer->getId();
            }
        }

        //create new customer if not found in OSE databse
        if(!isset($customerid)) {
            $customerid = $this->newCustomer($data);
        }

        $customer = Mage::getModel('customer/customer')->load($customerid);
        if(isset($data['linked_accounts'])) {
            $customerSearchStack = Mage::getStoreConfig(self::CUSTOMER_SEARCH_STACK_PATH) ?? 'BSL';
            $customerLineOfBusiness = $customerSearchStack == 'BSL' ? 'mobile' : 'fixed';
            foreach ($data['linked_accounts'] as $d) {
                if ($customerLineOfBusiness != $d['account_category']) {
                    $this->saveCustomerAccountData($d, $customer, true);
                } else {
                    $customerSearchStackData = $d;
                }
            }
            $this->saveCustomerAccountData($customerSearchStackData, $customer, true);
        } else {
            $this->saveCustomerAccountData($data, $customer, false);
        }

        return [
            'id' => $customer->getid(),
            'account_number' => $customer->getid(),
            'ban' => $customer->getBan(),
            'pan' => $customer->getPan(),
            'firstname' => $customer->getFirstName(),
            'lastname' => $customer->getLastName(),
            'email' => $customer->getEmailAddress(),
            'phone' => $customer->getPhoneNumber(),
            'dob' => $customer->getDob(),
            'kvk' => $customer->getCompanyCoc(),
            'search_customer_password' => $data['search_customer_password'],
        ];
    }

    /**
     * Update the customer data in OSE DB using the service response
     *
     * @param $customerId
     * @param $serviceResponse
     // * @return  Vznl_Customer_Model_Customer_Customer
     */
    public function updateCustomerInfo($customerId, $serviceResponse)
    {
        $customer = Mage::getModel('customer/customer')->load($customerId);
        $mapper = Mage::getModel('vznl_service/responseMapper');
        $customer = $mapper->map('customer_info', $serviceResponse, $customer);
        $customer->setIsProspect(false);
        $customer->save();
        return $customer;
    }

    /**
     * Get customer SOHO status from customer attributes
     * @return boolean
     */
    public function getIsSoho()
    {
        return (bool)$this->getData("is_business");
    }

    /**
     * checks if customer isProspect
     * @return boolean
     */
    public function checkIsProspect()
    {
        if ($this->getPan() == "" && $this->getBan() == "") {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Return all install base products with index and subscription contract reference
     * @return array
     */
    public function getAllInstallBaseProducts($filterLinkedAccounts = true)
    {
        // caching response on customer session
        $customerSession = Mage::getSingleton('customer/session');
        //don't check if no customer is logged in
        if (!$customerSession->getCustomer()->isCustomerLoggedIn() || $this->isProspect()) {
            return array();
        }

        $installBaseProducts = array();
        $index = 1;

        /** @var $panelHelper Vznl_Customer_Helper_Panel */
        $panelHelper = Mage::helper('vznl_customer/panel');
        $customerData = $panelHelper->getPanelData("products-content");

        foreach ($customerData as $type => $customers) {
            if (!is_array($customers)) continue;
            foreach ($customers as $customer) {
                if (!isset($customer['contracts']) ||
                    (!is_array($customer['contracts']) && (!$customer['contracts'] instanceof Traversable))
                ) {
                    $contracts = [];
                } else {
                    $contracts = $customer['contracts'];
                }

                foreach ($contracts as $contract) {
                    // Skip if customer status in not active (see OMNVFDE-2733)
                    if (in_array($contract['customer_status'], Dyna_Customer_Helper_Customer::getCustomerInactiveStatusList()) ||
                        $contract['dunning_status'] == Dyna_Customer_Helper_Services::DUNNING_IN
                    ) {
                        continue;
                    }
                    switch ($type) {
                        case Vznl_Customer_Helper_Services::ACCOUNT_CATEGORY_BSL:
                            foreach ($contract['subscriptions'] as $subscriptionData) {
                                foreach ($subscriptionData['products'] as $products) {
                                    // Skip if product status is not active (see OMNVFDE-2733)
                                    if ((isset($subscriptionData['product_status']) && in_array($subscriptionData['product_status'], Dyna_Customer_Helper_Customer::getContractProductInactiveStatusList()))) {
                                        continue;
                                    }
                                    $hisProducts = $this->parseComponents($products);

                                    if (!empty($products)) {
                                        $installBaseProducts[] = array(
                                            'index_id' => $index,
                                            'part_of_bundle' => isset($products['part_of_bundle']) ? $products['part_of_bundle'] : false,
                                            'contract_id' => $contract['customer_number'],
                                            'in_minimum_duration' => isset($subscriptionData['in_minimum_duration']) ? $subscriptionData['in_minimum_duration'] : false,
                                            'sharing_group_id' => isset($subscriptionData['sharing_group_details']['id']) ? $subscriptionData['sharing_group_details']['id'] : null,
                                            'sharing_group_member_type' => isset($subscriptionData['sharing_group_details']['member_type']) ? $subscriptionData['sharing_group_details']['member_type'] : null,
                                            'sharing_group_number_of_members' => isset($subscriptionData['sharing_group_details']['number_of_members']) ? $subscriptionData['sharing_group_details']['number_of_members'] : null,
                                            'product_id' => $products['install_base_product_id'] ?? null,
                                            'contract_category' => Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD,
                                            'contract_activation_date' => $subscriptionData['contract_activation_date'],
                                            'package_type' => isset(self::$productCategoryToPackageType[strtolower("kias")]) ? self::$productCategoryToPackageType[strtolower("kias")] : "",
                                            'package_type_icon' => isset(self::$productCategoryToPackageType[strtolower("kias")]) ? self::$productCategoryToPackageType[strtolower("kias")] : "",
                                            'ctn' => isset($subscriptionData['ctn']) ? $subscriptionData['ctn'] : "",
                                            'products' => $hisProducts,
                                            'service_address' => $subscriptionData['service_address'],
                                            "actual_month_of_contract" => $subscriptionData['actual_month_of_contract'],
                                            "serial_number" => $subscriptionData['serial_number']
                                        );

                                        $index++;
                                    }
                                }
                            }
                            break;
                        case Vznl_Customer_Helper_Services::ACCOUNT_CATEGORY_PEAL:
                            foreach ($contract['subscriptions'] as $subscriptionData) {
                                foreach ($subscriptionData['products'] as $products) {
                                    // Skip if product status is not active (see OMNVFDE-2733)
                                    if ((isset($subscriptionData['product_status']) && in_array($subscriptionData['product_status'], Dyna_Customer_Helper_Customer::getContractProductInactiveStatusList()))) {
                                        continue;
                                    }
                                    $hisProducts = $this->parseComponents($products);

                                    if (!empty($products)) {
                                        $installBaseProducts[] = array(
                                            'index_id' => $index,
                                            'part_of_bundle' => $products['part_of_bundle'] ?? false,
                                            'contract_id' => $contract['customer_number'],
                                            'in_minimum_duration' => $subscriptionData['in_minimum_duration'] ?? false,
                                            'sharing_group_id' => $subscriptionData['sharing_group_details']['id'] ?? null,
                                            'sharing_group_member_type' => $subscriptionData['sharing_group_details']['member_type'] ?? null,
                                            'sharing_group_number_of_members' => $subscriptionData['sharing_group_details']['number_of_members'] ?? null,
                                            'product_id' => $products['install_base_product_id'] ?? null,
                                            'contract_category' => Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD,
                                            'contract_activation_date' => $subscriptionData['contract_activation_date'],
                                            'package_type' => self::$productCategoryToPackageType[strtolower("dsl")] ?? "",
                                            'package_type_icon' => self::$productCategoryToPackageType[strtolower("dsl")] ?? "",
                                            'ctn' => $subscriptionData['ctn'] ?? "",
                                            'products' => $hisProducts,
                                            'service_address' => $subscriptionData['service_address'],
                                            "actual_month_of_contract" => $subscriptionData['actual_month_of_contract'],
                                            "serial_number" => $subscriptionData['serial_number']
                                        );

                                        $index++;
                                    }
                                }
                            }
                            break;
                        default:
                            // Do nothing
                            break;
                    }
                }
            }
        }

        $customerSession->setAllInstalledBaseProducts($installBaseProducts);

        //build a list of IB skus to easily check in various places if a sku is part of IB or new
        $allSkus = [];
        foreach($installBaseProducts as $installBaseProduct){
            foreach($installBaseProduct['products'] as $ibProduct){
                $allSkus[$ibProduct['product_sku']] = $ibProduct['product_sku'];
            }
        }
        $customerSession->setAllInstalledBaseProductSkus($allSkus);

        $this->alignInstallBaseProductsWithLinkedAccounts($installBaseProducts, $filterLinkedAccounts);

        return $installBaseProducts;
    }

    /**
     * Method that returns an array of products in install base by parsing components
     * @param $product
     * @return array
     */
    protected function parseComponents($products)
    {
        $subscriptionProducts = array();

        foreach ($products as $product) {

            if(!isset($product['product_sku']) && isset($product['components'])){
                $subscriptionProducts = array_merge($subscriptionProducts, self::parseComponents($product['components']));
            }

            /** @var Dyna_Catalog_Model_Product $productModel */
            $productModel = Mage::getModel('catalog/product');

            /** @var Dyna_Catalog_Model_Category $categoryModel */
            $categoryModel = Mage::getModel('catalog/category');

            $productSku = isset($product['product_sku']) ? trim($product['product_sku']) : null;
            $productId = $productSku ? $productModel->getIdBySku($productSku) : null;

            if ($productId) {
                //Loading product to get the familyId
                $productObject = $productModel->load($productId);
                $productFamily = $productObject->getProductFamily();

                $categories = $categoryModel->getProductCategoryIds($productId);
                // return subscription product with it's associated categories
                $subscriptionProducts[] = array(
                    'product_id' => $productId,
                    'product_sku' => $productSku,
                    'product_package_subtype' => $productModel->getPackageSubtypeName(),
                    'categories' => $categories,
                    'product_family' => $productFamily,
                    'title' => $product['name'] ?? "",
                    'description' => '', // @todo previously this was the value of attribute label from product
                    // @todo determine what to do with this key and if it really is necessarily
                );
            }
        }

        return $subscriptionProducts;
    }

    public function mergeCustomers($customerEntityId)
    {
        $customerOrder = array();
        $customer = Mage::getModel('customer/customer')->load($customerEntityId);
        $write = Mage::getSingleton('core/resource')->getConnection('core_write');

        $customerCollection = Mage::getModel('customer/customer')->getCollection()
            ->addAttributeToFilter(
                array(
                    array(
                        'attribute' => 'ban',
                        'eq' => $customer->getBan()
                    ),
                    array(
                        'attribute' => 'pan',
                        'eq' => $customer->getPan()
                    )
                ));

        if ($customerCollection->getSize() > 1) {

            foreach ($customerCollection as $customer) {
                $customerOrder[$customer->getId()] = $customer->getId();
            }

            foreach ($customerOrder as $customerId => $count) {

                if ($customerEntityId == $customerId) {
                    continue;
                }

                $updateData = array(
                    'customer_id' => $customerEntityId
                );

                $write->update(
                    'superorder',
                    $updateData,
                    array('customer_id = ?' => $customerId)
                );

                $write->update(
                    'sales_flat_order_grid',
                    $updateData,
                    array('customer_id = ?' => $customerId)
                );

                $write->update(
                    'sales_flat_order',
                    $updateData,
                    array('customer_id = ?' => $customerId)
                );

                $write->update(
                    'sales_flat_order_address',
                    $updateData,
                    array('customer_id = ?' => $customerId)
                );

                $write->update(
                    'sales_flat_quote',
                    $updateData,
                    array('customer_id = ?' => $customerId)
                );

                $write->update(
                    'sales_flat_quote_address',
                    $updateData,
                    array('customer_id = ?' => $customerId)
                );

                Mage::getModel('customer/customer')->load($customerId)
                    ->setBan(null)
                    ->setPan(null)
                    ->setAccountIdentifierMobile(null)
                    ->setAccountIdentifierFixed(null)
                    ->setAccountIdentifierLinkid(null)
                    ->save();
            }
        }
    }

    protected function _beforeSave()
    {
        parent::_beforeSave();
    }
}