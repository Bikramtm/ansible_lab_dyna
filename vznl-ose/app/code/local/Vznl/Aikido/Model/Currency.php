<?php

/**
 * Class Vznl_Aikido_Model_Currency
 * @override Mage_Directory_Model_Currency
 */
class Vznl_Aikido_Model_Currency extends Mage_Directory_Model_Currency
{
    /**
     * Custom number format path
     */
    const NUMBER_FORMAT_PATH = 'general/locale/currency_format';
    const NUMBER_FORMAT_NO_DOT_PATH = 'general/locale/currency_format_no_dot';

    /**
     * @param float $price
     * @param array $options
     * @param bool|true $includeContainer
     * @param bool|false $addBrackets
     * @return string
     */
    public function format($price, $options = array(), $includeContainer = true, $addBrackets = false)
    {
        if (!isset($options['format'])) {
            $options['format'] = $this->getNumberFormat();
        }

        return parent::format($price, $options, $includeContainer, $addBrackets);
    }

    /**
     * @return string
     */
    public function getNumberFormat()
    {
        return Mage::getStoreConfig(self::NUMBER_FORMAT_PATH);
    }

    /**
     * @return string
     */
    public function getNumberFormatNoDot()
    {
        return Mage::getStoreConfig(self::NUMBER_FORMAT_NO_DOT_PATH);
    }
}
