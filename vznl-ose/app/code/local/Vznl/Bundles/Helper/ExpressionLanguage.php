<?php
class Vznl_Bundles_Helper_ExpressionLanguage extends Dyna_Bundles_Helper_ExpressionLanguage
{
    /**
     * Updating related product for each package evaluated to true
     * @return $this
     */
    public function updateResponseForTargetedPackages()
    {
        $bundleHelper = Mage::helper('dyna_bundles/expression');
        $targetedPackages = array();

        if (!empty($this->response[Dyna_Bundles_Helper_Data::EXPRESSION_PACKAGES_RESULT])) {
            $validatedPackages = array_unique($this->response[Dyna_Bundles_Helper_Data::EXPRESSION_PACKAGES_RESULT], SORT_REGULAR);
            foreach ($validatedPackages as $packageId) {
                $eligibleProductSku = null;
                $cartPackage = $this->getQuote()->getCartPackage($packageId);
                if ($cartPackage) {

                    foreach ($cartPackage->getAllItems() as $item) {
                        if ($item->getProduct()->isRedPlusOwner() || $item->getProduct()->isSubscription()) {

                            if($bundleHelper->getSimulatedProductId() && $item->getProductId() != $bundleHelper->getSimulatedProductId()){
                                continue;
                            }

                            $eligibleProductSku = $item->getSku();
                            // break loop otherwise it will get overridden
                            break;
                        }
                    }

                    $targetedPackages[] = array(
                        'packageId' => (int)$packageId,
                        'eligibleProductSku' => $eligibleProductSku,
                    );
                }
            }
        }

        $this->response[Dyna_Bundles_Helper_Data::EXPRESSION_PACKAGES_RESULT] = $targetedPackages;
        $this->response[Dyna_Bundles_Helper_Data::EXPRESSION_HIGHLIGHT_PACKAGES] = $targetedPackages;

        return $this;
    }
}