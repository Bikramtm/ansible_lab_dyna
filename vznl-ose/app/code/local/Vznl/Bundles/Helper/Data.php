<?php

class Vznl_Bundles_Helper_Data extends Dyna_Bundles_Helper_Data
{
    const INSTALL_BASE_CUSTOMER_NUMBER = "customerNumber";
    const INSTALL_BASE_PRODUCT_ID = "productId";
    const INSTALL_BASE_NR_MEMBERS = "numberOfMembers";
    // constants used for identifying expression language evaluation
    const EXPRESSION_INSTALL_BASE_PRODUCT = "installBaseProduct";
    const EXPRESSION_PACKAGES_RESULT = "validPackages";
    const EXPRESSION_PACKAGES_STACK = "filterStack";
    const EXPRESSION_REQUIRED_PRODUCTS = "expressionRequiredProducts";
    const EXPRESSION_HIGHLIGHT_PACKAGES = "highlightPackages";

    const EXPRESSION_SCOPE_INSTALL_BASE = "installBase";
    const PROVIS_STATUS_AK = 'AK';
    protected $activeBundles = false;
    /** @var Dyna_Bundles_Model_BundleRule */
    protected $currentBundle = null;
    protected $simulation = false;

    // store salesId error
    protected $salesIdError = '';

    protected $_showServiceabilityNotification = false;
    protected $_privateProductSegments  = ['Residential', 'residential', 'Privat', 'Consumer', 'Both'];
    protected $_businessProductSegments = ['Soho', 'soho', 'Business', 'Both', 'business'];

    protected $debugFile = 'bundles_debug.log';
    protected $debug = false;
    protected $ibExpression = [];

    /**
     * Return current the bundle that is currently evaluating
     * @return Dyna_Bundles_Model_BundleRule
     */
    public function getCurrentBundle()
    {
        return $this->currentBundle;
    }

    /**
     * Set current the bundle on this helper
     *
     * @param Dyna_Bundles_Model_BundleRule $bundleRule
     * @return $this
     */
    public function setCurrentBundle(Dyna_Bundles_Model_BundleRule $bundleRule)
    {
        $this->currentBundle = $bundleRule;
        return $this;
    }

    public function isSimulation()
    {
        return $this->simulation;
    }

    /**
     * @param $transactionid
     * @return array
     */
    public function getCallerData($transactionid)
    {
        $data = Mage::getSingleton('dyna_customer/session')->getUctParams();
        $customer = Mage::getSingleton('dyna_customer/session')->getCustomer();
        $carts = $customer->getCustomerNumber() ? $customer->getShoppingCartsForCustomer(null, $customer->getCustomerNumber()) : array();
        $offer_id = null;
        $timeFrom = time();
        $timeTo = time();
        foreach ($carts as $key => $cart) {
            if ($cart['cart'][0]->getIsOffer() || $cart['cart'][0]->getCartStatus() == Omnius_Checkout_Model_Sales_Quote::CART_STATUS_SAVED) {
                $offer_id = $key;
                break;
            }
        }
        $campaignId = $data['campaignId'];
        $callingNumber = $data['callingNumber'];
        $Idwithtrans = $data['Idwithtrans'];

        /** @var Vznl_Bundles_Model_Campaign $campaign */
        $campaign = Mage::getModel('dyna_bundles/campaign')->load($campaignId, 'campaign_id');
        $dateModel = Mage::getSingleton('core/date');
        $timeNow = date('Y-m-d', $dateModel->timestamp(time()));
        if ($campaign && $campaign->getId()) {
            $timeFrom = date('Y-m-d', $dateModel->timestamp(($campaign->getValidFrom() != '1970-01-01' ? $campaign->getValidFrom() : time())));
            $timeTo = date('Y-m-d', $dateModel->timestamp(($campaign->getValidTo() != '1970-01-01' ? $campaign->getValidTo() : time())));
        }

        if (!$campaign || ($timeNow < $timeFrom || $timeNow > $timeTo)) {
            $campaign = $this->retrieveDefaultCampaign();
            if ($campaign) {
                $campaignId = $campaign->getId();
                $timeFrom = date('Y-m-d', $dateModel->timestamp(($campaign->getValidFrom() != '1970-01-01' ? $campaign->getValidFrom() : time())));
                $timeTo = date('Y-m-d', $dateModel->timestamp(($campaign->getValidTo() != '1970-01-01' ? $campaign->getValidTo() : time())));
            }
        }

        Mage::getSingleton('dyna_customer/session')->setUctParams(array(
            'callingNumber' => $callingNumber,
            'campaignId' => $campaignId,
            'transactionId' => $transactionid,
            'Idwithtrans' => $Idwithtrans,
            'success' => true
        ));

        if ($campaign && $campaign->getId() && $timeNow >= $timeFrom && $timeNow <= $timeTo) {
            $data = [
                'ctn' => $callingNumber,
                'campaign_id' => $campaign->getId() ?: 0,
                'offer_id' => $offer_id,
                'cluster_category' => $campaign->getData('cluster_category'),
                'cluster' => $campaign->getData('cluster'),
                'campaign_hint' => $campaign->getData('campaign_hint'),
                'offer_hint' => $campaign->getData('offer_hint'),
                'timeFrom' => $timeFrom,
                'timeTo' => $timeTo,
                'promotion_hint' => $campaign->getData('promotion_hint'),
            ];
        } else {
            $data = [
                'ctn' => $callingNumber,
                'campaign_id' => 0,
            ];
        }
        return $data;
    }

    /**
     * @return array
     */
    protected function _getProductSegmentsFilter(): array
    {
        $isBusiness = Mage::getSingleton('dyna_customer/session')
            ->getCustomer()
            ->getIsBusiness()
        ;
        $productSegments = $isBusiness ? $this->_businessProductSegments : $this->_privateProductSegments;
        $filter = [];
        foreach ($productSegments as $productSegment) {
            $filter[] = ['like' => '%'.$productSegment.'%'];
        }

        return $filter;
    }

    /**
     * @param $campaignId
     * @param $callingNumber
     * @param $defaultCampaign
     * @param $serviceabilityItems
     * @return array
     */
    public function getCampaignData($campaignId, $callingNumber, $defaultCampaign = 0, $serviceabilityItems = null)
    {
        $campaignsCollection = Mage::getModel('vznl_bundles/campaign')->getCollection();
        $dealerValues = $this->getDealerCode();
        $dealerGroup = $dealerValues['dealerGroup'];
        $campaignsCollection
            ->addFieldToFilter('default', $defaultCampaign)
            ->addFieldToFilter('product_segment', $this->_getProductSegmentsFilter())
            ->addFieldToFilter('dealer_id', array('finset' => $dealerValues['dealer']))
            ->addFieldToFilter('channel', ['like' => '%' . Mage::app()->getWebsite()->getCode() . '%'])
            ->addFieldToFilter('valid_from', ['lteq' => date('Y-m-d')])
            ->addFieldToFilter('valid_to', ['gteq' => date('Y-m-d')])
            ->getSelect()
            ->limit(1)
        ;

        /** @var Vznl_Bundles_Model_Campaign $campaign */
        $campaignDealerGroup = explode(',',$campaignsCollection->getLastItem()->getDealerGroup());
        $commonDealer = array_intersect($dealerGroup,$campaignDealerGroup);
        if($commonDealer) {
            $campaign = $campaignsCollection->getLastItem();
        } else {
            $campaign = null;
        }

        $offers = [];
        if ($campaign && $campaign->getId()) {
            /** @var Omnius_Package_Model_Mysql4_Package $packageCollection */
            $packageCollection = Mage::getModel('package/package')
                ->getPackages(
                    null,
                    Mage::getSingleton('checkout/session')->getQuote()->getId()
                )
            ;
            $packageTypes = [];
            /** @var Vznl_Package_Model_Package $package */
            foreach ($packageCollection as $package) {
                $packageTypes[] = $package->getType();
            }
            if ($defaultCampaign == 0) {
                $default = 1;
            } else {
                $default = 0;
            }
            $campaignData = [
                'default' => $default,
                'title' => $campaign->getCampaignTitle(),
                'validFrom' => $campaign->getValidFrom(),
                'validTo' => $campaign->getValidTo(),
                'hint' => $campaign->getCampaignHint(),
                'promotionHints' => array_filter([
                    $campaign->getPromotionHint1(),
                    $campaign->getPromotionHint2(),
                    $campaign->getPromotionHint3()
                ])
            ];
            /** @var Dyna_Bundles_Model_CampaignOffer $campaignOffer */
            foreach ($campaign->getOffers() as $campaignOffer) {
                $offerProductSKUs = array_filter(
                    array_map(
                        'trim',
                        explode(',', $campaignOffer->getProductIds())
                    )
                );
                if (empty($offerProductSKUs)) {
                    continue;
                }
                $offerData = $this->_getOfferData(
                    $offerProductSKUs,
                    is_array($serviceabilityItems) ? $serviceabilityItems : [],
                    $packageTypes,
                    $campaignOffer,
                    $callingNumber
                );

                if ($offerData) {
                    $offers[] = $offerData;
                }
            }
        } else {
            $campaignData = false;
        }

        if (empty($offers)) {
            $campaignData = false;
        }

        return [
            'campaignData' => $campaignData,
            'offers' => $offers,
            'showServiceabilityNotification' => (int)$this->_showServiceabilityNotification,
            'isFixedCustomer' => $this->isFixedCustomer()
        ];
    }

    /**
     * Check if customer belong to fixed
     */
    public function isFixedCustomer()
    {
        $ils = 0;
        $customer = Mage::getSingleton('dyna_customer/session')->getCustomer();
        $isFixedCustomer = trim($customer->getData('pan'));
        if ($isFixedCustomer) {
            $ils = 1;
        }
        return $ils;
    }

    /**
     * $postdata has the param campaign_id
     *
     * @param $postdata
     * @return mixed
     */
    public function setDefaultCampaignData($postdata)
    {
        $uctParams = Mage::getSingleton('dyna_customer/session')->getUctParams();
        $uctParams['campaignId'] = $postdata["campaign_id"];

        Mage::getSingleton('dyna_customer/session')->setUctParams($uctParams);

        $result["success"]=true;
        return $result;
    }
    /**
     * @param $campaignId
     * @return array|string
     */
    public function getCampaignProducts($campaignId)
    {
        /** @var Vznl_Bundles_Model_Campaign $campaign */
        $campaign = Mage::getModel('dyna_bundles/campaign')->load($campaignId, 'campaign_id');

        $result = $campaign->getOffers(1, 999);

        $campaignProducts = [];

        $dateModel = Mage::getSingleton('core/date');
        $timeNow = date('Y-m-d', $dateModel->timestamp(time()));
        $timeFrom = date('Y-m-d', $dateModel->timestamp(($campaign->getValidFrom() != '1970-01-01' ?: time())));
        $timeTo = date('Y-m-d', $dateModel->timestamp(($campaign->getValidTo() != '1970-01-01' ?: time())));
        if ($timeNow < $timeFrom || $timeNow > $timeTo) {

            return '';
        }

        /** @var Dyna_Bundles_Model_CampaignOffer $campaignOffer */
        foreach ($result['campaignOffers'] ?? [] as $campaignOffer) {
            $offerProductSKUs = explode(',', $campaignOffer->getProductIds());
            $offerProducts = [];

            if ($offerProductSKUs) {
                $productModel = Mage::getModel('catalog/product');
                /** @var Dyna_Catalog_Model_Product $item */
                foreach ($offerProductSKUs as $offerProductSKU) {
                    $offerProductSKU = trim($offerProductSKU);
                    $item = $this->getOfferProduct($productModel, $offerProductSKU, true);
                    if ($item) {
                        $offerProducts[] = $item;
                    }
                }
            }

            $campaignProducts = array_merge($campaignProducts, $offerProducts);
        }

        return array_unique($campaignProducts);
    }

    /**
     * @param Dyna_Bundles_Model_BundleRule $bundle
     * @param $activePackage
     * @param $bundleCreatedPackage
     * @return Dyna_Bundles_Model_Expression_Parser|Mage_Core_Model_Abstract
     * @internal param int $installPackageId
     * @internal param int $quotePackageId
     */
    public function buildRuleParserFromBundleRule(Dyna_Bundles_Model_BundleRule $bundle, $activePackage, $bundleCreatedPackage) : Dyna_Bundles_Model_Expression_Parser
    {
        /** @var Dyna_Bundles_Model_Service_Logger $logger */
        $logger = Mage::getModel("dyna_bundles/service_logger", ['logFile' => 'bundleRules.log']);

        $availableScopes = [
            'installBase' => Mage::getModel("dyna_bundles/expression_packageScope", array(
                'package' => $bundleCreatedPackage,
                'bundle' => $bundle,
            )),
            'quote' => Mage::getModel("dyna_bundles/expression_quotePackageScope", array(
                'package' => $activePackage,
                'bundle' => $bundle,
                'interStackPackageId' => isset($bundleCreatedPackage) ? $bundleCreatedPackage->getPackageId() : null,
            )),
        ];

        $data = [
            'scopes' => $availableScopes,
            'bundle' => $bundle,
            'logger' => $logger,
        ];
        return Mage::getModel("dyna_bundles/expression_parser", $data);
    }

    /**
     * @param Dyna_Bundles_Model_BundleRule $bundle
     * @param $activePackage
     * @param $bundleCreatedPackage
     * @return Dyna_Bundles_Model_Expression_Parser
     */
    public function buildRuleParserFromBundleRuleForDummyScope(Dyna_Bundles_Model_BundleRule $bundle, $activePackage, $bundleCreatedPackage, $interStackPackageId) : Dyna_Bundles_Model_Expression_Parser
    {
        /** @var Dyna_Bundles_Model_Service_Logger $logger */
        $logger = Mage::getModel("dyna_bundles/service_logger", ['logFile' => 'bundleRules.log']);

        $availableScopes = [
            'installBase' => Mage::getModel("dyna_bundles/expression_packageDummyScope", array(
                'package' => $bundleCreatedPackage,
                'bundle' => $bundle,
            )),
            'quote' => Mage::getModel("dyna_bundles/expression_quotePackageDummyScope", array(
                'package' => $activePackage,
                'bundle' => $bundle,
                'interStackPackageId' => $interStackPackageId,
            )),
        ];

        $data = [
            'scopes' => $availableScopes,
            'bundle' => $bundle,
            'logger' => $logger,
        ];
        return Mage::getModel("dyna_bundles/expression_parser", $data);
    }

    public function buildBundlePromoRuleParserFromBundleRule(Dyna_Bundles_Model_BundleRule $bundle, $installBasePackage, $quotePackage): Dyna_Bundles_Model_Expression_Parser
    {
        /** @var Dyna_Bundles_Model_Service_Logger $logger */
        $logger = Mage::getModel("dyna_bundles/service_logger", ['logFile' => 'bundleRules.log']);

        $availableScopes = [
            'installedBase' => Mage::getModel("dyna_bundles/expression_packageScope", ['package' => $installBasePackage, 'bundle' => $bundle]),
            'quote' => Mage::getModel("dyna_bundles/expression_promoOnlyQuotePackageScope", ['package' => $quotePackage, 'bundle' => $bundle]),
        ];

        $data = [
            'scopes' => $availableScopes,
            'bundleActions' => $bundle->getActions(),
            'logger' => $logger,
            'bundle' => $bundle
        ];
        return Mage::getModel("dyna_bundles/expression_parser", $data);
    }

    public function hasMandatoryFamiliesCompleted()
    {
        return empty($this->getMandatoryFamilies());
    }

    /**
     * Returns the missing mandatory families
     * @return array|mixed
     */
    public function getMandatoryFamilies()
    {
        /** @var Dyna_Bundles_Model_Expression_ParserResponse $parserResponse */
        $parserResponse = Mage::getSingleton('dyna_customer/session')->getBundleRuleParserResponse();

        $mandatoryFamilies = [];

        if ($parserResponse) {
            if ($this->savedParserResponseAppliesToQuote($parserResponse)) {
                $mandatoryFamilies = $parserResponse->getMandatoryChooseFromFamiliesFormat();
                $mandatoryFamilies = $this->getMissingMandatoryFamilies($mandatoryFamilies);
            } else {
                Mage::getSingleton('dyna_customer/session')->unsBundleRuleParserResponse();
                $mandatoryFamilies = [];
            }
        }

        return $mandatoryFamilies;
    }

    /**
     * @param Dyna_Bundles_Model_Expression_ParserResponse  $parserResponse
     * @return bool|mixed
     */
    public function savedParserResponseAppliesToQuote($parserResponse)
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $packages = $quote->getCartPackages();

        $applies = false;

        foreach ($packages as $package) {
            foreach ($package->getBundles() as $bundlePackage) {
                if ($bundlePackage->getId() == $parserResponse->getBundleRuleId()) {
                    $applies = $bundlePackage->getId();
                    break;
                }
            }
        }

        return $applies;
    }

    /**
     * Filter mandatory choose families by products in cart
     * @param $mandatoryFamilies
     * return $this
     */
    protected function getMissingMandatoryFamilies($mandatoryFamilies)
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        foreach ($mandatoryFamilies as $entityId => &$families) {
            foreach ($families as $key => $family) {
                $package = $quote->getCartPackageByEntityId($family['packageId']);
                if (!$package) {
                    continue;
                }
                foreach ($package->getAllItems() as $item) {
                    if (in_array($family['familyId'], $item->getProduct()->getCategoryIds())) {
                        unset($families[$key]);
                    }
                }
            }

            if (empty($mandatoryFamilies[$entityId])) {
                unset($mandatoryFamilies[$entityId]);
            }
        }

        return $mandatoryFamilies;
    }

    public function isBundleChoiceFlow()
    {
        /** @var Dyna_Bundles_Model_Expression_ParserResponse $parserResponse */
        $parserResponse = $this->getBundleRuleParser();
        if ($parserResponse) {
            return $this->savedParserResponseAppliesToQuote($parserResponse) && !empty($parserResponse->getMandatoryChooseFromFamilies());
        }
        return false;
    }

    /**
     * @return array|false|Mage_Core_Model_Abstract|null
     */
    public function getBundleRuleParser()
    {
        $activePackage = Mage::getSingleton('checkout/session')->getQuote()->getActivePackage();
        $packageId = $activePackage ? $activePackage->getEntityId() : null;
        $result = null;
        if ($packageId) {
            /** @var Dyna_Bundles_Model_BundlePackages $bundleRelation */
            $bundleRelation = Mage::getModel('dyna_bundles/bundlePackages')
                ->getCollection()
                ->addFieldToFilter('package_id', $packageId)
                ->addFieldToFilter('mandatory_families', array('neq' => 'NULL'))
                ->getLastItem();
            $result = $bundleRelation ? Mage::getModel("dyna_bundles/expression_parserResponse", ['mandatoryChooseFromFamilies' => $bundleRelation->getMandatoryFamilies(), 'bundleRuleId' => $bundleRelation->getBundleRuleId()]) : [];
        }

        return $result;
    }

    public function choiceAppliesToActivePackage()
    {
        /** @var Dyna_Bundles_Model_Expression_ParserResponse $parserResponse */
        $parserResponse = Mage::getSingleton('customer/session')->getBundleRuleParserResponse();
        $activePackage = $this->getQuote()->getCartPackage();
        $applies = false;

        if ($parserResponse && !empty($parserResponse->getMandatoryChooseFromFamilies())) {
            foreach ($parserResponse->getMandatoryChooseFromFamilies() as $family) {
                foreach ($family as $key => $familyId) {
                    if ($activePackage->getId() == $key) {
                        $applies = true;
                    }
                }
                // preserve by entity id as package index will change on package deletion
            }
        }

        return $applies;
    }

    /**
     *  Returns all mandatory families
     */
    public function getAllMandatoryFamilies()
    {
        /** @var Dyna_Bundles_Model_Expression_ParserResponse $parserResponse */
        $parserResponse = $this->getBundleRuleParser();

        $mandatoryFamilies = [];

        if ($parserResponse) {
            if ($this->savedParserResponseAppliesToQuote($parserResponse)) {
                $mandatoryFamilies = $parserResponse->getMandatoryChooseFromFamiliesFormat();
            }
        }

        return $mandatoryFamilies;
    }

    /**
     * @param $finalProducts
     * @param $offerId
     * @return bool
     */
    public function productsInOffer($finalProducts, $offerId)
    {
        $offerIds = $this->getOfferProducts($offerId);
        return !empty(array_intersect($offerIds, $finalProducts));
    }

    /**
     * Returns ids of products from a specific offer
     * @param $offerId
     * @return array
     */
    protected function getOfferProducts($offerId)
    {
        /** @var Dyna_Bundles_Model_Campaign $campaign */
        $offer = Mage::getModel('dyna_bundles/campaignOffer')
            ->getCollection()
            ->addFieldToFilter('offer_id', $offerId)
            ->setPageSize(1, 1)
            ->getLastItem();

        $offerSkus = explode(',', $offer->getData('product_ids'));
        $offerIds = [];
        foreach ($offerSkus as $offerSku) {
            /** @var Vznl_Catalog_Model_Product $product */
            $product = Mage::getModel('catalog/product');
            $offerIds[] = $this->getOfferProduct($product, trim($offerSku), true);
        }

        return $offerIds;
    }

    /**
     * Get Red Sales Id
     * @param null $cluster_category
     * @return array
     */
    public function getRedSalesId($cluster_category = null)
    {
        /* If we have a campaign, every quote must have set the data campaign*/
        /** @var Dyna_Customer_Model_Session $session */
        $session = Mage::getSingleton('dyna_customer/session');

        /** @var Vznl_Agent_Model_Dealer $agent */
        $agent = $session->getAgent();
        $uctParams = $session->getUctParams();
        $redSalesId = array();

        // if there is a valid campaign id, retrieve red_sales_id from bundle_campaign_dealercode (Telesales agent)
        if ($uctParams && !empty($uctParams['transactionId']) && !empty($uctParams['campaignId'])) {
            $this->getTelesalesAgentVoids($session, $uctParams, $redSalesId, $cluster_category);
        } else {
            // else, mapping of red_sales_id is made from agent->getRedSalesId() (alternative of wave's 2 COPS agent)
            $redSalesId = $this->getProvisRedSalesId(array($agent->getRedSalesId()));
        }

        return $redSalesId == array() ? null : $redSalesId;
    }

    /**
     * Get voids for Telesales agents
     * @param $session
     * @param $uctParams
     * @param $redSalesId
     * @param null $cluster_category
     */
    private function getTelesalesAgentVoids($session, $uctParams, &$redSalesId, $cluster_category = null)
    {
        /** @var Vznl_Agent_Model_Dealer $dealer */
        $dealer = $session->getAgent()->getDealer();
        $agency = Mage::getSingleton('agent/dealergroup')->load($dealer->getGroupId())->getName();
        $city = $dealer->getCity();
        if (is_null($cluster_category)) {
            $cluster_category = $this->getCampaignData($uctParams['campaignId'], $uctParams['callingNumber'])['campaignData']['cluster_category'];
        }
        $marketing_code = $this->getCampaignData($uctParams['campaignId'], $uctParams['callingNumber'])['campaignData']['marketing_code'];

        // Determine the agent VOID (Red Sales Id)

        /** @var Dyna_Bundles_Model_CampaignDealercode $dealerCodeModel */
        $dealerCodeModel = Mage::getModel('dyna_bundles/campaignDealercode')
            ->getCollection()
            ->addFieldToFilter('agency', array('in' => [$agency, '*']))
            ->addFieldToFilter('city', array('in' => [$city, '*']))
            ->addFieldToFilter('cluster_category', array('in' => [$cluster_category, '*']))
            ->addFieldToFilter('marketing_code', array('in' => [$marketing_code, '*']));

        if (count($dealerCodeModel)) {
            foreach ($dealerCodeModel as $dealerCodeRow) {
                $redSalesId[] = $dealerCodeRow->getRedSalesId();
            }
        }

        $redSalesId = $this->getProvisRedSalesId($redSalesId);
    }

    public function getSelectSalesIDUrl()
    {
        return $this->_getUrl('bundles/index/selectSalesId');
    }

    /**
     * Get the right red_sales_id from Provis import
     * @param $redSalesIds
     * @return boolean
     */
    public function getProvisRedSalesId($redSalesIds)
    {
        if (empty($redSalesIds) || (!$redSalesIds[0])) {
            $message = $this->__('Please be aware! Unable to submit an order due to unavailable Sales ID');
        } else {
            /** @var Dyna_AgentDE_Model_ProvisSales $agentProvisSales */
            $agentProvisSales = Mage::getModel('agentde/provisSales')
                ->getCollection()
                ->addFieldToFilter('red_sales_id', array('in' => $redSalesIds));

            if (count($agentProvisSales)) {
                foreach ($agentProvisSales as $provisSalesId) {
                    if ($provisSalesId->getStatus() != 'AK') {
                        $message = $this->__('Please be aware! Unable to submit an order due to an inactive Sales ID');
                    } elseif (!$provisSalesId->getBlueSalesId() || !$provisSalesId->getYellowSalesId()) {
                        $message = $this->__('Please be aware! Unable to submit an order due to an invalid Sales ID');
                    } else {
                        return $provisSalesId->getRedSalesId();
                    }
                }
            } else {
                $message = $this->__('Please be aware! Unable to submit an order due to an invalid Sales ID');
            }
        }

        $this->salesIdError = $message;
        return false;
    }

    /**
     * Get data from Provis import by red_sales_id
     */
    public function getProvisDataByRedSalesId($quoteRedSalesId = null)
    {
        if ($quoteRedSalesId) {
            // check if quote salesId is a valid one, as an extra safety step
            $redSalesId = $this->getProvisRedSalesId(array($quoteRedSalesId));
        } else {
            $redSalesId = $this->getRedSalesId();
        }

        if (!$redSalesId) {
            return array(
                'error' => true,
                'message' => $this->salesIdError
            );
        }

        /** @var Dyna_AgentDE_Model_ProvisSales $agentProvisSales */
        $agentProvisSales = Mage::getModel('agentde/provisSales')
            ->getCollection()
            ->addFieldToFilter('red_sales_id', $redSalesId)
            ->setPageSize(1, 1)
            ->getLastItem();

        return array(
            'kias' => $agentProvisSales->getRedSalesId(),
            'fn' => $agentProvisSales->getYellowSalesId(),
            'kd' => $agentProvisSales->getBlueSalesId()
        );
    }

    /**
     * Return a list of eligible bundles that will be sent forward to frontend (has a special structure)
     * @param bool $skipSimulation
     * @param bool $filterLinkedAccounts
     * @param null $forTariffs
     * @param null|array $bundleTypes
     * @return array
     */
    public function getFrontendEligibleBundles(
        $skipSimulation = false,
        $filterLinkedAccounts = true,
        $forTariffs = null,
        $bundleTypes = null
    ) {

        //store whether or not simulation is enabled to know when checking the conditions
        Mage::unregister('skipSimulation');
        Mage::register('skipSimulation', $skipSimulation);
        
        $this->debugLog('====== getFrontendEligibleBundles execution ======');

        $eligible = array();
        $hints = array();
        $targetedPackages = [];

        // get quote
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();

        // get quote active package
        $activePackage = $quote->getActivePackage();

        /** @var Dyna_Package_Model_PackageType $packageTypeModel */
        $packageTypeModel = Mage::getModel('dyna_package/packageType');

        /** @var Dyna_Package_Model_PackageType $packageType */
        $packageType = $activePackage ? $packageTypeModel->loadByCode($activePackage->getType()) : null;

        $relatedRedPlusTariffSKU = '';
        if ($activePackage) {
            // check if current package is redPlus related; if so, send its tariff's sku to addRedPlusMember button
            foreach ($activePackage->getAllItems() as $item) {
                if (strtolower($item->getProductType()) == strtolower(Dyna_Catalog_Model_Type::MOBILE_PREPAID_SUBSCRIPTION)) {
                    $relatedRedPlusTariffSKU = $item->getSku();
                    break;
                }
            }
        }

        // holds section products for each section visible in configurator
        $allSectionProducts = array();

        // get bundles collection
        /** @var Dyna_Bundles_Model_Mysql4_BundleRule_Collection $bundles */

        $currentWebsiteId = Mage::app()->getWebsite()->getId();
        $bundles = Mage::getModel('dyna_bundles/bundleRule')
            ->getCollection()
            ->addFieldToFilter('website_id', array('finset' => $currentWebsiteId))
            ->addFieldToFilter('effective_date', array(
                array('to' => date("Y-m-d H:i:s"), 'date' => true),
                array('null' => true)
            ))
            ->addFieldToFilter('expiration_date', array(
                array('from' => date("Y-m-d H:i:s"), 'date' => true),
                array('null' => true)
            ));

        if ($bundleTypes !== null) {
            if (!isset($bundleTypes[0])) {
                $bundleTypes = [$bundleTypes];
            }
            $bundles->addFieldToFilter('bundle_type', array('in' => $bundleTypes));
        }

        if (!$bundles->getSize()) {
            return $eligible;
        }

        // loading products from catalog for all sections
        if ($packageType && !$skipSimulation && $forTariffs) {
            $allSectionProducts = Mage::getModel('catalog/product')
                ->getCollection()
                ->addFieldToFilter('entity_id', array('in' => $forTariffs));
        }

        // get expression language instance
        /** @var Dyna_Bundles_Helper_ExpressionLanguage $expressionLanguage */
        $expressionLanguage = Mage::helper('dyna_bundles/expressionLanguage');
        /** @var Dyna_Bundles_Helper_Expression $bundlesSimulatorHelper */
        $bundlesSimulatorHelper = Mage::helper('dyna_bundles/expression');
        $bundlesSimulatorHelper->updateInstance($quote);

        $cartObject = Mage::getModel('dyna_configurator/expression_cart')
            ->setBundlesHelper($bundlesSimulatorHelper);

        /** @var Dyna_Configurator_Model_Expression_InstallBase $installBaseEvaluator */
        $installBaseEvaluator = Mage::getModel('dyna_configurator/expression_installBase');
        $installBaseEvaluator->setBundlesHelper($this);

        if (!$filterLinkedAccounts) {
            $installBaseEvaluator->removeLinkedAccountsFilter();
        }

        $parentPackageId = $this->getParentPackageId();

        $expressionObjects = array(
            'customer' => Mage::getModel('dyna_configurator/expression_customer'),
            'cart' => $cartObject,
            'availability' => Mage::getModel('dyna_configurator/expression_availability'),
            'installBase' => $installBaseEvaluator,
            'agent' => Mage::getModel('dyna_configurator/expression_agent'),
        );

        if (!$skipSimulation) {

            $this->simulation = true;
            $expressionLanguage->updateSimulationStatus(true);
            // there goes nothing
            foreach ($allSectionProducts as $product) {
                $this->debugLog('=========================================================== ');
                $this->debugLog('Simulate bundle rules for selected tariff: ' . $product->getSku());
                $this->debugLog('=========================================================== ');

                /** @var Dyna_Catalog_Model_Product $product */
                // starting new product simulation
                $bundlesSimulatorHelper
                    ->takeSnapshot()
                    ->simulateProduct($product);

                $bundleProcessingResult = $this->processBundleRules(
                    $bundles,
                    $bundlesSimulatorHelper,
                    $expressionLanguage,
                    $relatedRedPlusTariffSKU,
                    $expressionObjects,
                    $parentPackageId,
                    $targetedProducts);

                //remember targeted packages if found
                foreach($bundles as $bundle){
                    if(isset($bundleProcessingResult['eligible'][$bundle->getId()]['targetedPackagesInCart']) && !empty($bundleProcessingResult['eligible'][$bundle->getId()]['targetedPackagesInCart'])){
                        $targetedPackages[$bundle->getId()] = $bundleProcessingResult['eligible'][$bundle->getId()]['targetedPackagesInCart'];
                    }
                }

                //overwrite results
                $eligible = array_replace($eligible, $bundleProcessingResult['eligible']);

                //set back targeted packages if found
                foreach($bundles as $bundle){
                    if(isset($targetedPackages[$bundle->getId()])) {
                        $eligible[$bundle->getId()]['targetedPackagesInCart'] = $targetedPackages[$bundle->getId()];
                    }
                }

                $hints = $hints + $bundleProcessingResult['hints'];

                // reverting helper instance to cart's state
                $bundlesSimulatorHelper->clearSnapshot();
            }
        } else {
            $bundleProcessingResult = $this->processBundleRules(
                $bundles,
                $bundlesSimulatorHelper,
                $expressionLanguage,
                $relatedRedPlusTariffSKU,
                $expressionObjects,
                $parentPackageId);

            $eligible = $bundleProcessingResult['eligible'];
            $hints = $bundleProcessingResult['hints'];
        }

        // save bundle hints
        Mage::unregister('bundleHints');
        Mage::register('bundleHints', $hints);

        // resetting response keys (so array arrives in javascript response)
        return array_values($eligible);
    }

    /**
     * @param $bundles
     * @param Dyna_Bundles_Helper_Expression         $bundlesSimulatorHelper
     * @param Dyna_Bundles_Helper_ExpressionLanguage $expressionLanguage
     * @param $relatedRedPlusTariffSKU
     * @param $expressionObjects
     * @param $parentPackageId
     * @param array $targetedProducts
     * @return array
     */
    protected function processBundleRules(
        $bundles,
        $bundlesSimulatorHelper,
        $expressionLanguage,
        $relatedRedPlusTariffSKU,
        $expressionObjects,
        $parentPackageId,
        &$targetedProducts = []
    ) {
        $eligible = array();
        $hints = array();

        // used for returning post evaluation
        $dummyScope = Mage::getModel('dyna_bundles/expression_scope');

        // holds an array consisting of the bundle id as key and the package type that it allows as value
        $packagesAltered = array();

        /** @var Vznl_Configurator_Model_Expression_InstallBase $installBaseEvaluator */
        $installBaseEvaluator = $expressionObjects['installBase'];

        //$installBaseEligibleForRedPlus = $installBaseEvaluator->getInstallBaseProductsForRedPlus();
        $installBaseEligible = $installBaseEvaluator->getInstallBaseProducts();

        // getting the list of bundles that enable packages and that are eligible
        /** @var Dyna_Bundles_Model_BundleRule $bundle */
        foreach ($bundles as $bundle) {
            $this->debugLog(' =========================================================== ');
            $this->debugLog('Start evaluate bundle rule [' . $bundle->getId() . ']: ' . $bundle->getBundleRuleName());
            $bundlesSimulatorHelper->setCurrentBundle($bundle);
            $this->currentBundle = $bundle;
            // reset expression evaluation result
            // will be later used for returning eligible RedPlus subscriptions
            $expressionLanguage->resetResponse();

            if (!isset($packagesAltered[$bundle->getId()])) {
                /** @var Dyna_Bundles_Model_BundleAction $action */
                foreach ($bundle->getActions() as $action) {
                    // checking whether this bundle enables a certain package
                    // if it contains the allow package string, than we evaluate the expression with a dummy object that returns the sent parameter
                    if ((strpos($action->getAction(),
                                Dyna_Bundles_Model_Expression_Scope::ALLOW_PACKAGE_TYPE_STRING) !== false)
                        || (strpos($action->getAction(),
                                Dyna_Bundles_Model_Expression_Scope::ALLOW_PACKAGE_CREATION_TYPE_ID_STRING) !== false)
                    ) {
                        $packageCreationType = strtolower($expressionLanguage->evaluate($action->getAction(),
                            array('scope' => $dummyScope)));

                        // Load the package type information and build an array of enabled packages

                        /** @var Dyna_Package_Model_PackageCreationTypes $typeModel */
                        $typeModel = Mage::getModel('dyna_package/packageCreationTypes');
                        $creationType = $typeModel->getTypeByPackageCode($packageCreationType);
                        $type = $creationType->getReferencedPackageType();

                        $packagesAltered[$bundle->getId()][] = array(
                            'packageCode' => $type->getPackageCode(true),
                            'creationTypeCode' => $creationType->getPackageTypeCode(),
                            'filterAttributes' => $creationType->getFilterAttributes()
                        );
                    }
                }
            } else {
                // skip bundles that allow packages (these will be evaluated by hints action)
                continue;
            }

            $bundleValid = false;

            if (!$bundle->isRedPlus() && !empty($installBaseEligible)) {
                foreach ($installBaseEligible as $index => $installBaseProduct) {

                    $this->debugLog(' - Evaluate for IB product: ' . $installBaseProduct['contract_id'] . ' :: ' .  $installBaseProduct['product_id']);

                    if (!isset($this->ibExpression[$index])) {
                        /** @var Vznl_Configurator_Model_Expression_InstallBase $currentEvaluator */
                        $currentEvaluator = Mage::getModel('dyna_configurator/expression_installBase', ['skipSetup' => true]);
                        $currentEvaluator->setBundlesHelper($this);

                        // Send the IB products one-by-one for evaluation
                        $currentEvaluator->setInstallBaseProducts([$installBaseProduct]);

                        // set the evaluator with only one package
                        $expressionObjects['installBase'] = $currentEvaluator;

                        $this->ibExpression[$index] = $expressionObjects;

                    } else {
                        $expressionObjects = $this->ibExpression[$index];
                    }

                    $tmpEvaluationResponse = $this->processBundleConditions(
                        $bundle,
                        $expressionLanguage,
                        $expressionObjects,
                        $packagesAltered
                    );

                    if ($tmpEvaluationResponse === false) {
                        $this->debugLog(' -- Go to next product');
                        continue;
                    } else {
                        $bundleValid = true;
                        $evaluationResponse = $tmpEvaluationResponse;
                    }
                }
            } else {
                // keep the initial IB products
                $expressionObjects['installBase'] = $installBaseEvaluator;

                $evaluationResponse = $this->processBundleConditions(
                    $bundle,
                    $expressionLanguage,
                    $expressionObjects,
                    $packagesAltered);

                if ($evaluationResponse === false) {
                    $this->debugLog('- Go to next bundle');
                    continue;
                } else {
                    $bundleValid = true;
                }
            }

            if ($bundleValid) {
                $this->debugLog(sprintf('Bundle rule [%d] is VALID', $bundle->getId()));

                /** @var Dyna_Package_Model_PackageType $packageTypeModel */
                $packageTypeModel = Mage::getModel('dyna_package/packageType');
                // might be just an install base bundle with cart active package
                $alteredPackageType = isset($packagesAltered[$bundle->getId()]) ? current($packagesAltered[$bundle->getId()])['packageCode'] : "";

                $packageCodes = [];
                if (isset($packagesAltered[$bundle->getId()])) {
                    foreach ($packagesAltered[$bundle->getId()] as $packageInfo) {
                        $packageCodes[] = $packageInfo['packageCode'];
                    }
                }

                if ($bundlesSimulatorHelper->isSimulateActive()) {
                    if ($bundlesSimulatorHelper->getSimulatedProductId()) {
                        $targetedProducts[$bundle->getId()][] = $bundlesSimulatorHelper->getSimulatedProductId();
                    }
                } else {
                    $targetedProducts[$bundle->getId()] = $evaluationResponse[Dyna_Bundles_Helper_Data::EXPRESSION_REQUIRED_PRODUCTS] ?? array();
                }

                if (!isset( $eligible[$bundle->getId()])) {
                    $eligible[$bundle->getId()] = array(
                        'bundleId' => (int)$bundle->getId(),
                        'susoBundle' => $bundle->isSuso(),
                        'redPlusBundle' => $bundle->isRedPlus(),
                        'lowEntryBundle' => $bundle->isLowEntry(),
                        'packageName' => $packageTypeModel->loadByCode($alteredPackageType)->getFrontEndName(),
                        'addButtonTitle' => $bundle->getHintWithLocation(Dyna_Bundles_Model_BundleRule::LOCATION_OPTION) ? $bundle->getHintWithLocation(Dyna_Bundles_Model_BundleRule::LOCATION_OPTION)->getText() : null,
                        'parentPackageId' => $parentPackageId,
                        'activatePackages' => isset($packagesAltered[$bundle->getId()]) ? $packageCodes : [],
                        'creationTypeCode' => isset($packagesAltered[$bundle->getId()]) ? current($packagesAltered[$bundle->getId()])['creationTypeCode'] : null,
                        'filterAttributes' => isset($packagesAltered[$bundle->getId()]) ? current($packagesAltered[$bundle->getId()])['filterAttributes'] : null,
                        'addButtonInSection' => $bundle->getHintLocations(), // $bundle->getHintLocation(),
                        'relatedProductSku' => $relatedRedPlusTariffSKU
                    );

                    if ($cartHint = $bundle->getHintWithLocation(Dyna_Bundles_Model_BundleRule::LOCATION_SLIDE_IN)) {
                        $hints[$bundle->getId()] = array(
                            'hintEnabled' => $cartHint->getHintEnabled(),
                            'hintTitle' => $cartHint->getTitle(),
                            'hintText' => $cartHint->getText(),
                            'hintLocation' => $cartHint->getLocation(),
                            'bundleId' => $bundle->getId(),
                        );
                    }
                }

                $eligible[$bundle->getId()]['targetedProducts'] = $targetedProducts[$bundle->getId()] ?? [];
                // an array containing a list of combinations subscription number (aka customer number) - product id in order to have a clear reference
                // to which what install base product the bundle will be created
                $eligible[$bundle->getId()]['targetedSubscriptions'] = array_values($evaluationResponse[static::EXPRESSION_INSTALL_BASE_PRODUCT] ?? []);

                $eligible[$bundle->getId()]['targetedPackagesInCart'] = $evaluationResponse[Dyna_Bundles_Helper_Data::EXPRESSION_PACKAGES_RESULT] ?? [];
                $eligible[$bundle->getId()]['highlightPackages'] = $bundle->hasLocationCart() ? ($evaluationResponse[Dyna_Bundles_Helper_Data::EXPRESSION_HIGHLIGHT_PACKAGES] ?? []) : [];
            }

            Mage::unregister('install_base_expression_result');
            $bundlesSimulatorHelper->clearCurrentBundle();
        }

        $this->currentBundle = null;

        return ['eligible' => $eligible, 'hints' => $hints];
    }

    /**
     * @param Dyna_Bundles_Model_BundleRule          $bundle
     * @param Dyna_Bundles_Helper_ExpressionLanguage $expressionLanguage
     * @param $expressionObjects
     * @param $packagesAltered
     * @return array|bool
     */
    protected function processBundleConditions(
        $bundle,
        $expressionLanguage,
        $expressionObjects,
        $packagesAltered
    ) {
        // skip if one of the conditions fail
        foreach ($bundle->getConditions() as $bundleCondition) {
            $this->debugLog(' -- Evaluate condition: ' . $bundleCondition->getCondition());
            // skip if one of the conditions fail
            if (!$expressionLanguage->evaluate($bundleCondition->getCondition(), $expressionObjects)) {
                // be sure to clear the registered responses
                $this->debugLog('Condition failed');
                Mage::unregister('install_base_expression_result');
                return false;
            }
        }

        // if this bundles enables a create package button, that add it to response
        $evaluationResponse = $expressionLanguage->getResponse();
        // be sure to clear the registered responses
        Mage::unregister('install_base_expression_result');

        // skip Red+ bundles if no creation type is found
        if ($bundle->isRedPlus()
            && (!isset($packagesAltered[$bundle->getId()]) || !isset(current($packagesAltered[$bundle->getId()])['creationTypeCode']))
        ) {
            return false;
        }

        // check for packages to have valid data
        foreach ($evaluationResponse[Dyna_Bundles_Helper_Data::EXPRESSION_PACKAGES_RESULT] ?? [] as $index => $package) {
            if (!$package['eligibleProductSku']) {
                unset($evaluationResponse[Dyna_Bundles_Helper_Data::EXPRESSION_PACKAGES_RESULT][$index]);
            }
        }

        return $evaluationResponse;
    }

    /**
     * Debug purpose method to log info
     * @param $message
     * @return $this
     */
    protected function debugLog($message)
    {
        if (!$this->debug) {
            return $this;
        }

        Mage::log($message, Zend_Log::DEBUG, $this->debugFile);
    }

    /**
     * Determine parent package by the following logic:
     * If active package has parent, then get parent package id, otherwise return active package id
     * @return null
     */
    public function getParentPackageId()
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $activePackage = $quote->getActivePackage();
        switch (true) {
            case !$activePackage:
                $activePackageId = null;
                break;
            case $activePackage->getPackageId():
                $activePackageId = null;
                foreach ($quote->getCartPackages() as $package) {
                    if ($package->getId() == $activePackage->getParentId()) {
                        $activePackageId = $package->getPackageId();
                    }
                }
                break;
            default:
                $activePackageId = $activePackage->getPackageId();
        }

        return $activePackageId;
    }

    /**
     * Return current active bundles to frontend
     * @return array
     */
    public function getActiveBundles()
    {
        if ($this->activeBundles === false) {
            $tempPackages = array();
            $cartPackages = $this->getQuote()->getCartPackages();
            if ($cartPackages) {
                foreach ($cartPackages as $package) {
                    foreach ($package->getBundles() as $bundle) {
                        // if bundled and already added to our response list, update list to include this package
                        if (!empty($package->getBundles()) && isset($tempPackages[$bundle->getId()])) {
                            $tempPackages[$bundle->getId()]['packages'][] = array(
                                'packageId' => (int)$package->getPackageId(),
                                'relatedProductId' => $package->getBundleComponents(),
                                // returning install base targeted products from package (if any)
                                'targetedSubscriptions' => array(
                                    static::INSTALL_BASE_CUSTOMER_NUMBER => $package->getParentAccountNumber(),
                                    static::INSTALL_BASE_PRODUCT_ID => $package->getServiceLineId(),
                                )
                            );
                        } elseif (!empty($package->getBundles())) {
                            // load bundle
                            $tempPackages[$bundle->getId()] = array(
                                'bundleId' => (int)$bundle->getId(),
                                'susoBundle' => $bundle->isSuso(),
                                'redPlusBundle' => $bundle->isRedPlus()
                            );
                            $tempPackages[$bundle->getId()]['packages'][] = array(
                                'packageId' => (int)$package->getPackageId(),
                                'packageName' => $package->getTitle(),
                                'relatedProductId' => $package->getBundleComponents(),
                                // returning install base targeted products from package (if any)
                                'targetedSubscriptions' => array(
                                    static::INSTALL_BASE_CUSTOMER_NUMBER => $package->getParentAccountNumber(),
                                    static::INSTALL_BASE_PRODUCT_ID => $package->getServiceLineId(),
                                )
                            );
                        }
                    }
                }
            }
            $this->activeBundles = $tempPackages;
        }

        return array_values($this->activeBundles);
    }

    /**
     * Clear cached active bundles on this instance
     * @return $this
     */
    public function clearCachedActiveBundles()
    {
        $this->activeBundles = false;

        return $this;
    }

    /**
     * Get session quote
     * @return Dyna_Checkout_Model_Sales_Quote
     */
    protected function getQuote()
    {
        return Mage::getSingleton('checkout/cart')->getQuote();
    }

    /**
     * Returns whether the user is eligible to add red+ packages
     * from the shopping cart
     * @return bool
     */
    public function cartIsRedPlusEligible()
    {
        $eligibleBundles = $this->getFrontendEligibleBundles(true);
        $this->filterBundlesBySubscriptionStatus($eligibleBundles);

        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $activePackage = $quote->getCartPackage();
        $banSelected = $activePackage ? $activePackage->getParentAccountNumber() : false;

        foreach ($eligibleBundles as &$bundle) {
            if ($bundle['redPlusBundle'] && (!empty($bundle['targetedSubscriptions']) || !empty($bundle['targetedPackagesInCart']))) {
                if ($banSelected) {
                    foreach ($bundle['targetedSubscriptions'] as &$targetedSubscription) {
                        if ($targetedSubscription['customerNumber'] == $banSelected) {
                            return true;
                        }
                    }
                    if (!empty($bundle['targetedPackagesInCart'])) {
                        return true;
                    }
                } elseif ($bundle['creationTypeId']) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Return product ids that are bundle component in order to break the bundles if needed
     * @return array
     */
    public function getBundleComponents()
    {
        $activePackageId = $this->getQuote()->getActivePackageId();
        $result = [];

        foreach ($this->getQuote()->getAllItems() as $quoteItem) {
            if ($quoteItem->getPackageId() != $activePackageId || !$quoteItem->getBundleComponent()) {
                continue;
            }

            $result[] = array(
                'productId' => $quoteItem->getProductId(),
                'bundleId' => $quoteItem->getBundleComponent(),
            );
        }

        return array_values($result);
    }

    /**
     * Return bundle hints for eligible bundles
     * @return array
     */
    public function getBundleHints()
    {
        $hints = Mage::registry('bundleHints');

        $result = array();
        if (!empty($hints)) {
            foreach ($hints as $bundleHint) {
                $result[] = array(
                    'title' => $bundleHint['hintTitle'],
                    'description' => $bundleHint['hintText'],
                    'bundleId' => (int)$bundleHint['bundleId'],
                );
            }
        }

        return $result;
    }

    /**
     * Filters the eligible bundles and removes the subscriptions with the wrong status
     * @param $eligibleBundles
     */
    public function filterBundlesBySubscriptionStatus(&$eligibleBundles)
    {
        /** @var Dyna_Customer_Model_Customer $customer */
        $customer = Mage::getSingleton('dyna_customer/session')->getCustomer();
        $allInstallBaseProducts = $customer->getAllInstallBaseProducts();

        foreach ($eligibleBundles as &$eligibleBundle) {
            foreach ($eligibleBundle['targetedSubscriptions'] as $key => $targetedSubscription) {
                foreach ($allInstallBaseProducts as $installBaseProduct) {
                    if ($targetedSubscription['productId'] != null
                        && $targetedSubscription['customerNumber'] != null
                        && $installBaseProduct['product_id'] == $targetedSubscription['productId']
                        && $installBaseProduct['contract_id'] == $targetedSubscription['customerNumber']
                        && isset($installBaseProduct['product_status'])
                        && $installBaseProduct['product_status'] == Dyna_Customer_Helper_Customer::MOBILE_CONTRACT_PRODUCT_STATUS_R) {
                        unset($eligibleBundle['targetedSubscriptions'][$key]);
                        break;
                    }
                }
            }
            $eligibleBundle['targetedSubscriptions'] = array_values($eligibleBundle['targetedSubscriptions']);
        }
    }

    public function calculateTotalsForInstalledBase($skus = [])
    {
        /** @var Dyna_Catalog_Model_Product $productModel */
        $productModel = Mage::getModel("catalog/product");
        $totals = 0;
        foreach ($skus as $sku) {
            /** @var Vznl_Catalog_Model_Product $product */
            $product = $productModel->loadByAttribute('sku', $sku);
            if ($product->getId()) {
                $mafInclTax =  Mage::helper('tax')->getPrice($product, $product->getMaf(), false);
                $totals += $mafInclTax;
            }
        }

        return $totals;
    }

    /**
     * TODO: Calculate price slide and repeat for discounts
     * @param $products
     * @param int $oldPrice
     * @return int
     */
    public function calculateTotalsForProducts($products, $oldPrice = 0)
    {
        foreach ($products as $product) {
            if ($product['maf']) {
                if ($product['type'] == 'remove') {
                    $oldPrice -= $product['maf'];
                } else {
                    $oldPrice += $product['maf'];
                }
            } else {
                $oldPrice -= $product['promoDiscount'];
            }
        }

        return $oldPrice;
    }

    /** @return null|Dyna_Bundles_Model_Campaign */
    protected function retrieveDefaultCampaign()
    {
        $channel = Mage::app()->getWebsite()->getCode();
        $dealerValues = $this->getDealerCode();
        $dealerGroup = $dealerValues['dealerGroup'];

        $campaignCollection = Mage::getModel('vznl_bundles/campaign')
            ->getCollection()
            ->addFieldToFilter('default', 1)
            ->addFieldToFilter('dealer_id', array('finset' => $dealerValues['dealer']))
            ->addFieldToFilter('valid_from', ['lteq' => date('Y-m-d')])
            ->addFieldToFilter('valid_to', ['gteq' => date('Y-m-d')])
            ->addFieldToFilter('channel', array('like' => '%' . $channel . '%'))
        ;
        $campaignCollection->getSelect()->limit(1);
        /** @var Vznl_Bundles_Model_Campaign $defaultCampaign */
        $campaignDealerGroup = explode(',',$campaignCollection->getLastItem()->getDealerGroup());
        $commonDealer = array_intersect($dealerGroup,$campaignDealerGroup);
        if($commonDealer) {
            $defaultCampaign = $campaignCollection->getLastItem();
        } else {
            $defaultCampaign = null;
        }

        if (!$defaultCampaign) {
            return null;
        }
        $uctParams = Mage::getSingleton('dyna_customer/session')->getUctParams();
        $uctParams['campaignId'] = $defaultCampaign->getCampaignId();
        Mage::getSingleton('dyna_customer/session')->setUctParams($uctParams);

        return $defaultCampaign;
    }

    /**
     * Creates a new relation in bundle_packages
     *
     * @param $bundleId
     * @param $packageId
     */
    public function addBundlePackageEntry($bundleId, $packageId)
    {
        if ($bundleId && $packageId) {
            $existingEntries = Mage::getModel('dyna_bundles/bundlePackages')
                ->getCollection()
                ->addFieldToFilter('bundle_rule_id', $bundleId)
                ->addFieldToFilter('package_id', $packageId);

            if ($existingEntries->getSize() == 0) {
                Mage::getModel('dyna_bundles/bundlePackages')
                    ->setBundleRuleId($bundleId)
                    ->setPackageId($packageId)
                    ->save();
            }
        }
    }

    /**
     * Removes an entry from bundle_packages
     *
     * @param $bundleId
     * @param $packageId
     */
    public function removeBundlePackageEntry($bundleId, $packageId)
    {
        /** @var Dyna_Bundles_Model_BundlePackages $existingEntry */
        $existingEntry = Mage::getModel('dyna_bundles/bundlePackages')
            ->getCollection()
            ->addFieldToFilter('bundle_rule_id', $bundleId)
            ->addFieldToFilter('package_id', $packageId)
            ->setPageSize(1, 1)
            ->getLastItem();

        $existingEntry->delete();
    }

    /**
     * @param Dyna_Checkout_Model_Sales_Quote $quote
     */
    public function recreateBundles(Dyna_Checkout_Model_Sales_Quote $quote)
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $parentQuote */
        $parentQuote = Mage::getModel('sales/quote')
            ->load($quote->getQuoteParentId());
        /** @var Dyna_Bundles_Helper_Data $bundlesHelper */
        $bundlesHelper = Mage::helper('dyna_bundles');

        foreach ($parentQuote->getCartPackages() as $oldPackage) {
            // get the corresponding package from the new quote
            $newPackage = $quote->getCartPackage($oldPackage->getPackageId());
            if ($newPackage) {
                $bundlePackages = Mage::getModel('dyna_bundles/bundlePackages')
                    ->getCollection()
                    ->addFieldToFilter("package_id", $oldPackage->getId());

                foreach ($bundlePackages as $bundle) {
                    $bundlesHelper->addBundlePackageEntry($bundle->getBundleRuleId(), $newPackage->getId());
                }
            }
        }
    }

    protected function hasPermission($packageType)
    {
        $customerSession = Mage::getSingleton('dyna_customer/session');
        $agent = $customerSession->getAgent();

        switch (strtolower($packageType)) {
             case strtolower(Vznl_Catalog_Model_Type::TYPE_CABLE_TV):
             case strtolower(Vznl_Catalog_Model_Type::TYPE_CABLE_INDEPENDENT):
             case strtolower(Vznl_Catalog_Model_Type::TYPE_CABLE_INTERNET_PHONE):
             case strtolower(Vznl_Catalog_Model_Type::TYPE_FIXED):
                 return $agent->isGranted('ALLOW_CAB');
             case strtolower(Vznl_Catalog_Model_Type::TYPE_MOBILE):
             case strtolower(Vznl_Catalog_Model_Type::TYPE_PREPAID):
                 return $agent->isGranted('ALLOW_MOB');
             case strtolower(Vznl_Catalog_Model_Type::TYPE_LTE):
             case strtolower(Vznl_Catalog_Model_Type::TYPE_FIXED_DSL):
                 return $agent->isGranted('ALLOW_DSL');
             default:
                 return false;

         }
    }

    /**
     * @param null $ruleParser
     */
    public function setBundleRuleParser($ruleParser = null)
    {
        if (!$ruleParser) {
            return;
        }
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $packagesQuote = $quote->getCartPackages();
        $packages = [];
        foreach ($packagesQuote as $package) {
            $packages[] = $package->getId();
        }
        $ruleParserArray = $ruleParser->toArray();
        /** @var Dyna_Bundles_Model_BundlePackages $existingEntry */
        $bundleRelations = Mage::getModel('dyna_bundles/bundlePackages')
            ->getCollection()
            ->addFieldToFilter('bundle_rule_id', $ruleParserArray['bundleRuleId'])
            ->addFieldToFilter('package_id', ['in' => $packages]);


        if ($bundleRelations && $ruleParserArray['mandatoryChooseFromFamilies']) {
            foreach ($bundleRelations as $bundleRelation) {
                $this->setMandatoryFamiliesForBundleRuleParser($bundleRelation, $ruleParserArray);
            }
        }
    }

    /**
     * @param Dyna_Bundles_Model_BundlePackages $bundleRelation
     * @param $ruleParserArray
     */
    protected function setMandatoryFamiliesForBundleRuleParser($bundleRelation, $ruleParserArray)
    {
        $bundleRelation->setMandatoryFamilies($ruleParserArray['mandatoryChooseFromFamilies'])->save();
    }

    protected function _getOfferData(
        $offerProductSKUs,
        $serviceabilityItems,
        $packageTypes,
        $campaignOffer,
        $callingNumber
    ) {
        $packageTypesCount = count($packageTypes);
        $nonCableType = count(array_diff($packageTypes, Dyna_Catalog_Model_Type::getCablePackages()));
        $offerProducts = [];
        $availability = 0;
        $hasInvalidProducts = 0;
        $packageProducts = [];
        $ils = $this->isFixedCustomer();
        $productStatus = 1;
        $checkFixedPermission = true;
        $checkMobilePermission = true;

        $agent = Mage::getSingleton('customer/session')->getAgent();
        $hasFixedPermission = $agent->isGranted(Vznl_Agent_Model_Agent::ALLOW_ZIGGO);
        $hasMobilePermission = $agent->isGranted(Vznl_Agent_Model_Agent::ALLOW_MOBILE);

        /** @var Dyna_Catalog_Model_Product $item */
        foreach ($offerProductSKUs as $offerProductSKU) {
            $productModel = Mage::getModel('catalog/product');
            $item = $this->getOfferProduct($productModel, $offerProductSKU);

            if ($item) {
                if ($checkFixedPermission && !$hasFixedPermission && in_array(strtolower($item->getPackageType()), Vznl_Catalog_Model_Type::getFixedPackages())) {
                    return false;
                } else {
                    $checkFixedPermission = false;
                }

                if ($checkMobilePermission && !$hasMobilePermission && in_array(strtolower($item->getPackageType()), Vznl_Catalog_Model_Type::getMobilePackages())) {
                    return false;
                } else {
                    $checkMobilePermission = false;
                }
            }

            if (!$item) {
                $hasInvalidProducts = 1;
                $offerProducts[] = $offerProductSKU;
            }
            if ($item->getMarketable() === false) {
                $hasInvalidProducts = 1;
            }

            //check if product is available
            $websiteId = Mage::app()->getWebsite()->getId();
            $today = date("Y-m-d H:i:s");

            $expired = $item->getData('expiration_date') ? $item->getData('expiration_date') : 0;
            $effective = $item->getData('effective_date') ? $item->getData('effective_date') : 0;

            if (($expired && ($item->getData('expiration_date') <= $today)) ||
                ($effective && ($item->getData('effective_date') >= $today)) ||
                ($item->getData('status') == Mage_Catalog_Model_Product_Status::STATUS_DISABLED) ||
                (!in_array($websiteId, $item->getWebsiteIds()))) {
                $productStatus = 0;
            }

            $attribute = $item->getResource()->getAttribute(Dyna_Catalog_Model_Product::CATALOG_PACKAGE_TYPE_ATTR);
            $packageTypeObject = json_decode(Mage::helper('omnius_catalog')->getAttributeAdminLabel($attribute, $item->getData(Dyna_Catalog_Model_Product::CATALOG_PACKAGE_TYPE_ATTR)));
            $packageTypeObject = (array)$packageTypeObject;
            $packageType = strtolower(current($packageTypeObject));
            if (!isset($packageProducts[$packageType])) {
                $packageProducts[$packageType] = [];
            }

            if (!in_array($packageType, Vznl_Catalog_Model_Type::getMobilePackages())) {
                if ($this->_showServiceabilityNotification === false && !empty($serviceabilityItems)) {
                    $this->_showServiceabilityNotification = true;
                }

                $serviceabilityProdValues = [];
                foreach ($serviceabilityItems as $serviceItem) {
                    if ($serviceItem['id'] !== 'cable-internet-phone') {
                        continue;
                    }
                    foreach ($serviceItem['attributes']['items'] as $attribute) {
                        if (!$attribute['value']) {
                            continue;
                        }
                        $serviceabilityProdValues[] = $attribute['key'];
                    }
                }

                if ($item->getData('required_serviceability_items')) {
                    $productValues = array_map('trim', explode(',', $item->getData('required_serviceability_items')));
                } else {
                    $productValues = [];
                }

                if (array_intersect($productValues, $serviceabilityProdValues) == $productValues) {
                    $availability++;
                }
            }

            if ($packageType == strtolower(Vznl_Catalog_Model_Type::TYPE_MOBILE)) {
                $packageTypeForCreation = Vznl_Catalog_Model_Type::CREATION_TYPE_MOBILE;
            } elseif ($packageType == strtolower(Vznl_Catalog_Model_Type::TYPE_FIXED)) {
                $packageTypeForCreation = Vznl_Catalog_Model_Type::CREATION_TYPE_FIXED;
            } else {
                $packageTypeForCreation = $packageType;
            }

            $packageCreationTypeId = Mage::helper('dyna_configurator/cart')->getCreationTypeId($packageTypeForCreation) ?? null;

            $offerProducts[] = $item->getId();
            $packageProducts[$packageType]['products'][] = $item->getId();
            $packageProducts[$packageType]['packageCreationTypeId'] = $packageCreationTypeId;
        }

        $offerProductList = implode(',', $offerProducts);
        if (!$hasInvalidProducts && empty($offerProductList)) {
            $hasInvalidProducts = 1;
        }

        //check if all offer products meet serviceability requirements from serviceability check
        if ($availability !== count($offerProducts) && !in_array($packageType, Dyna_Catalog_Model_Type::getMobilePackages())) {
            $status = 0;
        } else {
            $status = 1;
        }

        /** @var Dyna_Configurator_Helper_Data $helper */
        $helper = Mage::helper('dyna_configurator');
        $packages = [];
        $defaultEnabled = true;
        foreach ($packageProducts as $packageType => $package) {
            //check if there are already packages of the same type in the cart
            if (
                $packageTypesCount &&
                (
                    in_array($packageType, Dyna_Catalog_Model_Type::getMobilePackages())
                    || in_array($packageType, Dyna_Catalog_Model_Type::getDslPackages())
                    || (in_array($packageType, Dyna_Catalog_Model_Type::getCablePackages())
                        && ($nonCableType
                            || in_array($packageType, $packageTypes)))
                )
            ) {
                $status = 0;
            }
            $packages[] = [
                'type' => $packageType,
                'currentProducts' => implode(',', $package['products']),
                'packageCreationTypeId' => $package['packageCreationTypeId'],
            ];

            if (in_array($packageType, Dyna_Catalog_Model_Type::getMobilePackages())) {
                $status = 1;
            }

            //disable the offer if the address is not serviceable.
            if (!in_array($packageType, Dyna_Catalog_Model_Type::getMobilePackages())) {
                if (is_array($serviceabilityItems[0])) {
                    foreach ($serviceabilityItems[0] as $key => $value) {
                        if ($key == 'attributes') {
                            $serviceableItems = $value['items'];
                            if (is_array($serviceableItems)) {
                                foreach ($serviceableItems as $item => $itemValue) {
                                    if ($itemValue['key'] == 'Footprint') {
                                        if (!in_array(strtoupper($itemValue['value']), ['ZIGGO', 'UPC'])) {

                                            $productStatus = 0;
                                        }
                                    }
                                }
                            }
                        }
                    }
                } elseif ($serviceabilityItems) {
                    $productStatus = 0;
                }
            }
        }

        foreach ($packages as $package) {
            $defaultEnabled = Mage::helper('dyna_configurator/cart')->getCampaignOfferCompatibility($package['type'], explode(',', $package['currentProducts']));
            if ($defaultEnabled) {
                break;
            }
        }

        $usps = [];
        for ($i = 0; $i <= 5; $i++) {
            $uspValue = $campaignOffer->getData('usp'.$i);
            if (!$uspValue) {
                continue;
            }
            $usps[] = [
                'usp' => $uspValue,
                'packageType' => $campaignOffer->getData('usp'.$i.'pakagetype')
            ];
        }
        if ($packageType == 'int_tv_fixtel' && $this->_showServiceabilityNotification) {
            if ($ils == 0) {
                $status = 1;
            } else {
                $status = 0;
            }
        }

        $customer = Mage::getSingleton('dyna_customer/session')->getCustomer();
        $data = [
            'identifier' => $campaignOffer->getOfferId(),
            'titleDescription' => $campaignOffer->getTitleDescription(),
            'subtitleDescription' => $campaignOffer->getSubtitleDescription(),
            'discountedMonths' => sprintf(Mage::helper('bundles')->__('Running for %d months'), (int)$campaignOffer->getDiscountedMonths()),
            'usps' => $usps,
            'discountedPricePerMonth' => (float)$campaignOffer->getDiscountedPricePerMonth(),
            'undiscountedFromMonth' => (float)$campaignOffer->getUndiscountedFromMonth(),
            'undiscountedPricePerMonth' => (float)$campaignOffer->getUndiscountedPricePerMonth(),
            'status' => $status,
            'packageType' => $packageType,
            'productStatus' => $productStatus,
            'initConfiguratorData' => [
                'packages' => $packages,
                'packagesStringified' => rawurlencode(json_encode($packages)),
                'packageId' => null,
                'saleType' => Dyna_Checkout_Model_Sales_Quote_Item::ACQUISITION,
                'ctn' => $callingNumber,
                'hasInvalidProducts' => $hasInvalidProducts,
                'disabledByPermission' => !$this->hasPermission($packageType),
                'hasOpenOrder' => $customer->isCustomerLoggedIn() ? Mage::helper('dyna_customer')->hasOpenOrders(null, $packageType) : false
            ],
            'defaultEnabled' => $defaultEnabled,
        ];
        if ($packageType == 'int_tv_fixtel') {
            $data['ils'] = $ils;
        }
        return $data;
    }

    protected function getOfferProduct($productModel, $offerProductSKU, $returnId=false)
    {
        $productId = $productModel->getIdBySku($offerProductSKU);
        if ($returnId) {
            return $productId;
        }
        if ($productId) {
            return $productModel->load($productId);
        }
    }

    /**
     *  Return dealerCode of logged agent
     *  @return array
     */
    public function getDealerCode() {
        $dealerContent = [];
        $dealerData = Mage::getSingleton('dyna_customer/session')->getAgent()->getDealer();
        $dealerContent['dealerGroup'] = $dealerData->getGroupId();
        $dealerContent['dealer'] = $dealerData->getVfDealerCode();

        return $dealerContent;
    }
}
