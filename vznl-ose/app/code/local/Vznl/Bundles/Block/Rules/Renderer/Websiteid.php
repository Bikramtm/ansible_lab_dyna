<?php

class Vznl_Bundles_Block_Rules_Renderer_Websiteid extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $websites = Mage::getModel('core/website')->getCollection();
        foreach ($websites->getData() as $website) {
            $websiteName[$website['website_id']] = $website['name'];
        }

        $value = explode(',', $row->getData($this->getColumn()->getIndex()));
        foreach ($value as $item) {
            $indexData[] = $websiteName[$item];
        }
        return implode(',', $indexData);
    }
}
?>