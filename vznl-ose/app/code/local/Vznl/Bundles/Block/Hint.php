<?php
/**
 * Copyright (c) 2017. Dynacommerce B.V.
 */

class Vznl_Bundles_Block_Hint extends Dyna_Bundles_Block_Hint
{
    public function getBundlesToDisplay()
    {
        $eligibleBundles = $this->filterBundlesByHintLocation(Dyna_Bundles_Model_BundleRule::LOCATION_DRAW, $this->getEligibleBundles());

        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = $this->getQuote();
        $activePackage = $quote->getActivePackage();
        $activePackageId = $quote->getActivePackageId();
        /** @var Dyna_Customer_Model_Customer $customer */
        $customer = Mage::getSingleton('dyna_customer/session')->getCustomer();
        $serviceAddress = Mage::getSingleton('dyna_address/storage')->getServiceAddress(true);

        $bundles = array();
        $quotePackages = $quote->getCartPackages();
        $bundledPackages = array();
        //get bundled packages from cart
        foreach ($quotePackages as $package) {
            foreach ($package->getBundles() as $bundle) {
                if ($bundle->getId()) {
                    $bundledPackages[$package->getPackageId()] = $package->isPartOfRedPlus();
                }
            }
        }

        foreach ($eligibleBundles as $eligibleBundle) {
            // inter stack bundle
            if (!empty($eligibleBundle['targetedPackagesInCart'])) {
                foreach ($eligibleBundle['targetedPackagesInCart'] as $bundleCartPackage) {
                    if ($bundleCartPackage['packageId'] == $activePackageId || (in_array($bundleCartPackage['packageId'], array_keys($bundledPackages)) && !$bundledPackages[$bundleCartPackage['packageId']] )) {
                        continue;
                    }
                    $bundle = Mage::getModel('dyna_bundles/bundleRule')->load((int)$eligibleBundle['bundleId']);

                    // Determine the Tariff product in quote package
                    $quoteItem = $this->getActivePackageTariff();
                    $quoteBaseItem = $this->getTariffForPackage($bundleCartPackage['packageId']);
                    $quoteBasePackage['product_sku'] = $quoteBaseItem->getSku();
                    $quoteBasePackage['active_bundle_id'] = $eligibleBundle['bundleId'];
                    $quoteBasePackage['package_id'] = $quoteBasePackage['index_id'] = $quoteBaseItem->getPackageId();
                    $quoteBasePackage['ctn'] = $this->getCtnForPackage($bundleCartPackage['packageId'], $customer);
                    $quoteBasePackage['package_type'] = $quoteBaseItem->getPackageType();
                    $quoteBasePackage['package_type_name'] = $this->getPackageNameByCode($quoteBasePackage['package_type']);
                    $quoteBasePackage['process_context'] = $quoteBaseItem->getPackage()->getSaleType();
                    $quoteBasePackage['product_id'] = $quoteBaseItem->getProductId();
                    $quoteBasePackage['title'] = $quoteBaseItem->getName();
                    $quoteBasePackage['is_interstack'] = true;
                    $quoteBasePackage['account_number'] = $customer->getBan();
                    $quoteBasePackage['address'] = !in_array(strtolower($quoteBaseItem->getPackageType()), Dyna_Catalog_Model_Type::getMobilePackages()) ? $serviceAddress : null;
                    $packageType = $activePackage->getType();

                    $quotePackage = [
                        'type' => $packageType,
                        'ctn' => $customer->getPhoneNumber(),
                        'package_type_name' => $this->getPackageNameByCode($packageType),
                        'product' => $quoteItem ? $quoteItem->getProduct()->getDisplayNameConfigurator() ?
                            $quoteItem->getProduct()->getDisplayNameConfigurator() : $quoteItem->getName() : '',
                        'address' => !in_array($packageType, Dyna_Catalog_Model_Type::getMobilePackages()) ? $serviceAddress : null,
                        'account_number' => $customer->getBan(),
                        'description' => $quoteItem ? $quoteItem->getProduct()->getServiceDescription() : '',
                        'process_context' => $quoteItem ? $quoteItem->getPackage()->getSaleType() : 'ACQ'
                    ];

                    $bundle
                        ->setQuoteBasePackages([$quoteBasePackage])
                        ->setQuotePackage($quotePackage)
                        ->setIsInterStackBundle(true);

                    $bundles[] = $bundle;
                }
            }
            // install base bundle
            if (!empty($eligibleBundle['targetedSubscriptions'])) {
                $customerProducts = Mage::getSingleton('dyna_customer/session')->getCustomerProducts();
                $fixPackage = current($customerProducts[Vznl_Customer_Model_System_Config_Stack::ZIGGO_STACK]);
                $installBaseProduct = isset($fixPackage['contracts'][0]['subscriptions'][0]) ? $fixPackage['contracts'][0]['subscriptions'][0] : array();
                $productId = $installBaseProduct['product_offer_id'];

                /** @var Dyna_Bundles_Model_BundleRule $bundle */
                $bundle = Mage::getModel('dyna_bundles/bundleRule')->load((int)$eligibleBundle['bundleId']);

                $bundle
                    ->setCtn($installBaseProduct['ctn'])
                    ->setPackageType($installBaseProduct['package_type'])
                    ->setBundleId($eligibleBundle['bundleId'])
                    ->setSubscriptionNumber($installBaseProduct['customerNumber'])
                    ->setProductId($productId);

                $product = [];
                $product['contract_id'] = $installBaseProduct['ctn'];
                $product['entity_id'] = $productId;
                $product['product_id'] = $productId;
                $product['ctn'] = $installBaseProduct['ctn'];
                $product['package_type'] = $installBaseProduct['package_type'];
                $product['package_type_icon'] = $installBaseProduct['package_type'];
                $product['type'] = $installBaseProduct['package_type'];
                $product['active_bundle_id'] = $eligibleBundle['bundleId'];
                $product['title'] = $installBaseProduct['product_name'];
                $product['address'] = !in_array(strtolower($installBaseProduct['package_type']), Dyna_Catalog_Model_Type::getMobilePackages()) ? $serviceAddress : null;

                $installBasePackages = [$product];

                $bundle->setInstalledBasePackages($installBasePackages);
                $bundle->setTitle($installBaseProduct['product_name']);
                $packageType = $quote->getPackageType($activePackage->getPackageId());
                $quoteItem = null;
                $items = $quote->getPackageItems($activePackage->getPackageId());

                foreach ($items as $item) {
                    if ($item->getProduct()->isSubscription()) {
                        $quoteItem = $item;
                        break;
                    }
                }

                $quotePackage = [
                    'package_id' => $activePackage->getPackageId(),
                    'type' => $packageType,
                    'package_type_name' => $this->getPackageNameByCode($packageType),
                    'product' => $quoteItem ? $quoteItem->getProduct()->getDisplayNameConfigurator() ?
                        $quoteItem->getProduct()->getDisplayNameConfigurator() : $quoteItem->getName() : '',
                    'address' => !in_array($packageType, Dyna_Catalog_Model_Type::getMobilePackages()) ? $serviceAddress : null,
                    'ctn' => $customer->getPhoneNumber(), // todo ??
                    'description' => $quoteItem ? $quoteItem->getProduct()->getServiceDescription() : '',
                    'is_interstack' => false,
                    'process_context' => $quoteItem ? $quoteItem->getPackage()->getSaleType() : 'ACQ'
                ];

                $bundle->setQuotePackage($quotePackage);
                $bundles[] = $bundle;
            }
        }

        return $bundles;
    }
}
