<?php

/**
 * Class Vznl_Bundles_Model_Campaign
 * @method getCampaignId()
 * @method getCampaignTitle()
 * @method getCampaignHint()
 * @method getPromotionHint1()
 * @method getPromotionHint2()
 * @method getPromotionHint3()
 * @method getValidFrom()
 * @method getValidTo()
 */
class Vznl_Bundles_Model_Campaign extends Dyna_Bundles_Model_Campaign
{
    /**
     * Get Campaign Offers
     * @return array
     */
    public function getOffers($page = 1, $pageSize = 10) {
        $campaignId = $this->getData('entity_id');
        $campaignOfferIds = Mage::getModel('dyna_bundles/campaignOfferRelation')
            ->getCollection()
            ->addFieldToFilter('campaign_id', array('eq' => $campaignId))
            ->setOrder('entity_id', 'asc')
            ->getColumnValues('offer_id');
        $campaignOfferIds = array_unique($campaignOfferIds);
        /** @var Dyna_Bundles_Model_Mysql4_CampaignOffer_Collection $campaignOffersUnsorted */
        $campaignOffersUnsorted = Mage::getModel('dyna_bundles/campaignOffer')
            ->getCollection()
            ->addFieldToFilter('entity_id', array('in' => $campaignOfferIds))
        ;

        $campaignOffers = [];
        $campaignOfferIds = array_flip($campaignOfferIds);
        foreach ($campaignOffersUnsorted as $key => $campaignOffer) {
            $sortedPosition = $campaignOfferIds[$campaignOffer->getId()];
            $campaignOffers[$sortedPosition] = $campaignOffer;
        }
        if(isset($campaignOffers) && is_array($campaignOffers)){
            ksort($campaignOffers);
        }

        return $campaignOffers;
    }
}
