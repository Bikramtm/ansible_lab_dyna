<?php

/** @var Mage_Sales_Model_Entity_Setup $this */

$this->startSetup();
$connection = $this->getConnection();
$bundleRuleTable = $this->getTable("dyna_bundles/bundle_rules");

if ($connection->tableColumnExists($bundleRuleTable, 'ogw_orchestration_type')) {
    $connection->dropColumn($bundleRuleTable,"ogw_orchestration_type");
}
$this->endSetup();
