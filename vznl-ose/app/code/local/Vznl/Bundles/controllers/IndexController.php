<?php
require_once Mage::getModuleDir('controllers', 'Dyna_Bundles') . DS . 'IndexController.php';
require_once Mage::getModuleDir('controllers', 'Vznl_Configurator') . DS . 'IndexController.php';
/**
 * Class Vznl_Bundles_IndexController
 */
class Vznl_Bundles_IndexController extends Dyna_Bundles_IndexController
{
    public function getAdditionalBundleInfoAction()
    {
        $selectedBundle = $this->getRequest()->get('bundle_id');
        // this tells us whether or not we should create an inter stack bundle or not
        $productId = $this->getRequest()->get('product_id') ?: null;
        // ctn/customernumber
        $customerNumber = $this->getRequest()->get('customer_number') ?: null;

        $selectedInstalledBaseProductEntityId = $this->getRequest()->get('entity_id') ?: null;

        $interStackPackageId = $this->getRequest()->getParam('inter_stack_package_id') ?: null;

        /** @var Dyna_Bundles_Model_BundleRule $bundle */
        $bundle = Mage::getModel('dyna_bundles/bundleRule')->load($selectedBundle);
        if (!$bundle->getId()) {
            $response = [
                'error' => true,
                'url' => $this->__("There was an error loading requested bundle"),
            ];

            $this->jsonResponse($response);
        }

        $bundleProductsSummary = $bundle->getSummaryBundleDetails($customerNumber, $productId, $selectedInstalledBaseProductEntityId, $interStackPackageId);

        $packagesModelArray = Mage::getModel('dyna_package/packageType')->getCollection()->toArray()['items'];
        $packagesNames = [];

        foreach ($packagesModelArray as $package) {
            if (in_array(strtolower($package['package_code']), array_keys(!empty($bundleProductsSummary['products']) ? $bundleProductsSummary['products'] : array()))) {
                $packagesNames[strtolower($package['package_code'])] = $package['front_end_name'];
            }
        }

        $response = [
            'bundleProducts' => $bundleProductsSummary['products'],
            //'totals' => $bundleProductsSummary['totals'],
            'packageNames' => $packagesNames,
            'bundleName' => $bundle->getBundleGuiName(),
            'bundleDescription' => $bundle->getDescription()
        ];

        $this->jsonResponse($response);
    }

    /**
     * Saves the selected bundle choices
     *
     * Previously selected bundle choices are removed
     * Promos on previously selected choices are removed
     * Promo bundle rules are reevaluated to add promos on the new bundle choices.
     */
    public function updateBundleChoiceAction()
    {
        if (!$this->getRequest()->isPost()) {
            return $this;
        }

        $selectedBundleChoiceProducts = $this->getRequest()->get('products');
        $bundleId = $this->getRequest()->get('bundleId');
        $skus = array_values($selectedBundleChoiceProducts);

        if (!empty($skus)) {
            /** @var Dyna_Package_Model_Package $package */
            $package = $this->getCart()->getQuote()->getCartPackage();
            /** @var Dyna_Bundles_Model_Expression_QuotePackageScope $quoteScope */
            $quoteScope = Mage::getModel("dyna_bundles/expression_quotePackageScope", ['package' => $package, 'bundle' => $package->getBundleById($bundleId)]);
            $items = $package->getItems();

            try {
                $this->validateBundleChoice();
            } catch (Exception $exception) {
                $exceptionMessage = [
                    'error' => true,
                    'message' => $exception->getMessage(),
                    'currentBundleChoices' => $exception->bundleChoiceProducts
                ];
                return $this->jsonResponse($exceptionMessage);
            }
            // Remove old bundle choices
            $targetProductIds = array();
            foreach ($items as $item) {
                if ($item->getBundleChoice()) {
                    $this->getCart()->removeItem($item->getId());
                    $targetProductIds[] = $item->getProductId();
                }
            }

            // Remove all promo items that were added by bundle rules when the parent product is removed
            if (count($targetProductIds)) {
                foreach ($items as $item) {
                    if (in_array($item->getTargetId(), $targetProductIds) && $item->isBundlePromo()) {
                        $this->getCart()->removeItem($item->getId());
                    }
                }
            }

            $quoteScope->add(...$skus);
            foreach ($skus as $sku) {
                $product = $this->getProduct($sku);
                $quoteScope->markProductAsBundleChoice($product);
            }

            // Reevaluates addPromoProduct rules, to support adding promos on bundle choices
            $activeBundleRule = $package->getBundleById($bundleId);

            foreach ($package->getPackagesInSameBundle($bundleId, true) as $sibling) {
                if ($sibling->getEditingDisabled()) {
                    $ruleParser = Mage::helper('bundles')->buildBundlePromoRuleParserFromBundleRule($activeBundleRule, $sibling, $package);
                    $ruleParser->parse();
                }
            }

            $this->getCart()->save();
        }

        $quote = $this->getCart()->getQuote();
        $packageId = $quote->getActivePackageId();
        $cartStatusResponse = $this->getCartStatusResponse($quote->getId(), $packageId);

        return $this->jsonResponse($cartStatusResponse);
    }

    protected function getProduct($sku)
    {
        $productId = Mage::getModel('catalog/product')->getResource()->getIdBySku($sku);
        return Mage::getModel('catalog/product')->load($productId);
    }

    /**
     * Create bundle and updated cart with customer install base products
     * configurator/cart/createBundle
     */
    public function createAction()
    {
        if ($this->getRequest()->isPost()) {

            Mage::register('skipSimulation', true); 
            // get post data
            $bundleId = $this->getRequest()->getPost('bundleId');
            $parentAccountNumber = $this->getRequest()->getPost('subscriptionNumber') ?: null;
            $serviceLineId = $this->getRequest()->getPost('productId') ?: null;
            // this tells us whether or not we should create an inter stack bundle or not
            $interStackPackageId = $this->getRequest()->getPost('interStackPackageId') ?: null;

            /** @var Dyna_Checkout_Model_Sales_Quote $quote */
            $quote = $this->getCart()->getQuote();

            // if no interstack package id received through request, means bundle is done with the fixed IB package
            if (!$interStackPackageId) {

                $customerProducts = Mage::getSingleton('dyna_customer/session')->getCustomerProducts();
                $fixPackage = current($customerProducts[Vznl_Customer_Model_System_Config_Stack::ZIGGO_STACK]);
                $fixedSubscription = isset($fixPackage['contracts'][0]['subscriptions'][0]) ? $fixPackage['contracts'][0]['subscriptions'][0] : array();

                //simulate ils call before creating the bundle
                $request=$this->getRequest();
                $ilsRequestData = [
                    'button'=>'modify_package',
                    'stack' => Vznl_Customer_Model_System_Config_Stack::ZIGGO_STACK,
                    'customer_id' => $fixedSubscription['customer_number'],
                    'service_line_id' => $fixedSubscription['validationFields']['service_line_id'],
                    'products'=>'',
                    'tariff'=>'',
                    'allSocs'=>'',
                    'package_type' => Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_FIXED,
                    'ctn' => $fixedSubscription['ctn'],
                    'context' => Vznl_Catalog_Model_ProcessContext::ILSBLU,
                    'contract_start_date' => $fixedSubscription['contract_start_date'],
                    'contract_end_date' => $fixedSubscription['contract_end_date'],
                    'bundle_product_ids'=>''
                ];

                if(Mage::registry('simulate_has_fixed_package')) {
                    Mage::unregister('simulate_has_fixed_package');
                }
                Mage::register('simulate_has_fixed_package', 'true');

                foreach($ilsRequestData as $key=>$val){
                    $request->setPost($key, $val);
                }

                $interStackPackageId=$quote->getActivePackageId();
                try {
                    $controller = Mage::getControllerInstance(
                        'Vznl_Configurator_IndexController',
                        $request,
                        Mage::app()->getResponse());
                    $return = $controller->inlifeAction();
                }
                catch (Exception $e){
                    $this->jsonResponse(array(
                        'error' => true,
                        'message' => $e->getMessage(),
                    ));
                }
            }

            /** @var Dyna_Bundles_Helper_Data $bundlesHelper */
            $bundlesHelper = Mage::helper('dyna_bundles');

            // preserving initial active package in order to notify frontend that active package has changed and trigger initConfigurator
            $initialPackageId = $quote->getActivePackageId();

            // if no valid bundle id received, notify frontend
            /** @var Dyna_Bundles_Model_BundleRule $bundle */
            $bundle = Mage::getModel('dyna_bundles/bundleRule')->load($bundleId);
            if (!$bundle->getId()) {
                $response = [
                    'error' => true,
                    'url' => $this->__("There was an error loading requested bundle"),
                ];

                $this->jsonResponse($response);
            }

            try {
                switch (true) {
                    case $interStackPackageId != null:
                        $activePackageId = $bundle->createInterStackBundle($interStackPackageId);
                        break;
                    default:
                        // Fall back in creating the wave1 bundle with an install base product
                        $activePackageId = $bundle->createLegacyBundle($parentAccountNumber, $serviceLineId);
                        break;
                }

                $response = [
                    'error' => false,
                    'rightBlock' => Mage::getSingleton('core/layout')->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/partials/package_block.phtml')->toHtml(),
                    'initNewPackage' => $activePackageId !== $initialPackageId ? $activePackageId : 0,
                    'totals' => Mage::getSingleton('core/layout')->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/totals.phtml')->toHtml(),
                    'eligibleBundles' => Mage::helper('dyna_bundles')->getFrontendEligibleBundles(),
                ];
                $response['optionsPanel'] = [];
                if ($bundlesHelper->isBundleChoiceFlow()) {
                    $response['optionsPanel'] = $this->getLayout()->createBlock('dyna_bundles/options')->setTemplate('bundles/options.phtml');
                }

                $this->jsonResponse($response);
            } catch (Mage_Core_Exception $e) {
                $this->jsonResponse(array(
                    'error' => true,
                    'message' => $e->getMessage(),
                ));
            }
        }
    }
}
