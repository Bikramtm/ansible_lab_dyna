<?php

require_once Mage::getModuleDir('controllers', 'Dyna_Bundles') . DS . 'Adminhtml' . DS . 'RulesController.php';

class Vznl_Bundles_Adminhtml_RulesController extends Dyna_Bundles_Adminhtml_RulesController
{

    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            $data['website_id'] = implode(',', $data['website_id']);
            try {
                unset($data['form_key']);

                /** @var Dyna_Bundles_Model_Campaign $model */
                $model = Mage::getModel('dyna_bundles/bundleRule')
                    ->addData($data)
                    ->setId($this->getRequest()->getParam('id', null))
                    ->save();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('bundles')->__('The bundle rule was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                Mage::unregister("bundle_rules_data");

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', ['id' => $model->getId()]);
                    return;
                }
            } catch (Exception $e) {
                $this->_throwErrror($e->getMessage(), $data);

                return;
            }
            $this->_redirect('*/*/');

            return;
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('bundles')->__('Unable to find rule to save'));
        $this->_redirect('*/*/');
    }

    private function _throwErrror($message, $data)
    {
        Mage::getSingleton('adminhtml/session')->addError($message);
        Mage::getSingleton('adminhtml/session')->setFormData($data);
        $this->_redirect('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
    }
}
