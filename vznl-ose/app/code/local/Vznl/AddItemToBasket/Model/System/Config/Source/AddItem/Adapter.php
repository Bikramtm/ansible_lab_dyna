<?php

/**
 * Used in admin panel adapter selector for Additemtobasket service
 *
 */
class Vznl_AddItemToBasket_Model_System_Config_Source_AddItem_Adapter
{
    const STUB_ADAPTER = 'Vznl_AddItemToBasket_Adapter_Stub_Factory';
    const PEAL_ADAPTER = 'Vznl_AddItemToBasket_Adapter_Peal_Factory';

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => self::STUB_ADAPTER, 'label' => Mage::helper('adminhtml')->__('Stub Adapter')),
            array('value' => self::PEAL_ADAPTER, 'label' => Mage::helper('adminhtml')->__('Peal Adapter')),
        );
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        $optionArray = $this->toOptionArray();
        $array = [];
        foreach ($optionArray as $option) {
            $array[] = $option['value'];
        }
        return $array;
    }

}
