<?php

/**
 * Class Vznl_AddItemToBasket_Adapter_Stub_Adapter
 *
 */
class Vznl_AddItemToBasket_Adapter_Stub_Adapter
{
    CONST NAME = 'AddItemToBasket';

    /**
     * @param $basketId
     * @param $technicalId
     * @return $responseData
     */
    public function call($basketId, $technicalId = false)
    {
        if (is_null($basketId)) {
            return 'No basketId provided to adapter';
        }

        $stubClient = Mage::helper('vznl_additemtobasket')->getStubClient();
        $stubClient->setNamespace('Vznl_AddItemToBasket');
        $arguments = array($basketId,$technicalId);
        $responseData = $stubClient->call(self::NAME);
        Mage::helper('vznl_additemtobasket')->transferLog($arguments, $responseData, self::NAME);

        return $responseData;
    }
}