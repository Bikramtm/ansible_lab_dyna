<?php

use GuzzleHttp\Client;

class Vznl_AddItemToBasket_Adapter_Peal_Factory
{
    public static function create()
    {
        $client = new Client();
        $basketHelper = Mage::helper('vznl_additemtobasket');
        return new Vznl_AddItemToBasket_Adapter_Peal_Adapter(
            $client,
            $basketHelper->getEndPoint(),
            $basketHelper->getChannel(),
            $basketHelper->getCountry()
        );
    }
}