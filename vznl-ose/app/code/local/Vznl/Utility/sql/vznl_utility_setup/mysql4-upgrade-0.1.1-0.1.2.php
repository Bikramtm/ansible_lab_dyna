<?php

/* @var $customerInstaller Mage_Customer_Model_Resource_Setup */
$customerInstaller = new Mage_Customer_Model_Resource_Setup('core_setup');
$customerInstaller->startSetup();
$setup = Mage::getSingleton('core/resource')->getConnection('core_read');

// Drastically improves select times with ORDER BY startedat
$customerInstaller->run("ALTER TABLE `dyna_perf_log_data` DROP INDEX `IDX_CALL_AVG_LOOKUP`;");
$customerInstaller->run("ALTER TABLE `dyna_perf_log_data` ADD INDEX `IDX_CALLTYPE_CALLNAME_STARTDATEAT` (calltype, callname, startedat);");

$customerInstaller->endSetup();
