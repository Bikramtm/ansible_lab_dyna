<?php

$installer = $this;
$installer->startSetup();
$connection = $installer->getConnection();

// Create only if it does not exists
$tableName = 'dyna_perf_log_run';
if (!$installer->tableExists($tableName)) {
	$table = $connection->newTable($installer->getTable($tableName))
        ->addColumn('server', Varien_Db_Ddl_Table::TYPE_VARCHAR, 16, array(
		    'primary' => true,
		    'nullable' => false
	    ))
	    ->addColumn('timestamp', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array()
	    );
	$connection->createTable($table);
}

$tableName = 'dyna_perf_log_data';
if (!$installer->tableExists($tableName)) {
	$table = $connection->newTable($installer->getTable($tableName))
        ->addColumn('id', Varien_Db_Ddl_Table::TYPE_BIGINT, 20, array(
    	    'unsigned' => true,
		    'primary' => true,
		    'auto_increment' => true,
		    'nullable' => false
	    ))
	    ->addColumn('calltype', Varien_Db_Ddl_Table::TYPE_VARCHAR, 25, array(
		    'nullable' => false
	    ))
	    ->addColumn('callname', Varien_Db_Ddl_Table::TYPE_VARCHAR, 45, array(
		    'nullable' => false
	    ))
	    ->addColumn('startedat', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array())
	    ->addColumn('server', Varien_Db_Ddl_Table::TYPE_VARCHAR, 16, array(
		    'nullable' => false,
	    ))
	    ->addColumn('duration', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
		    'unsigned' => true,
		    'nullable' => false,
	    ))
	    ->addIndex('IDX_CALL_AVG_LOOKUP',array('calltype','callname'))
	    ->addIndex('IDX_CALL_STARTED_AT',array('startedat'));
	$connection->createTable($table);
}

$installer->endSetup();
