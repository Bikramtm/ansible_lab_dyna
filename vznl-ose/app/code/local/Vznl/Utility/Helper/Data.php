<?php
/**
 * Copyright (c) 2019. Dynacommerce B.V.
 * Class Vznl_Utility_Helper_Data
 */

class Vznl_Utility_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * wrapper method to save models where call is needed inside for/while loops
     * @param $model
     */
	public function saveModel(&$model)
    {
    	$model->save();
    }
    
    /**
     * wrapper method to load models by id where call is needed inside for/while loops
     * @param $model
     * @param $id
     */
    public function loadModelById($model, $id)
    {
        $model = $model->load($id);
        return $model;
    }
    
    /**
     * wrapper method to get array counts where call is needed inside for/while loops
     * @param $array
     */
    public function getArrayCount($array)
    {
    	$count = count($array);
    	return $count;
    }    
}
