<?php

class Vznl_Guru_Model_Url
{

    /**
     * Base URL config path
     * @var string
     */
    const GURU_CONFIG_URL = 'vodafone_customer/guru/url';

    /**
     * Generate URL to external system
     * @param string $token JWT compact
     * @param string $customerNumber Customer BAN/PAN
     * @param string $customerCtn Customer CTN (empty for Ziggo)
     * @param string $agentUsername
     * @param string $dealerCode
     * @throws \Exception
     * @return string
     */
    public function getUrl(
        string $token,
        string $customerNumber,
        string $customerCtn,
        string $agentUsername,
        string $dealerCode
    ):string {
        $baseUrl = Mage::getStoreConfig(self::GURU_CONFIG_URL);

        $parameters = [
            'ContainerName' => 'NextBestAction',
            'Channel' => 'Ubuy',
            'Direction' => 'Inbound',
            'CustomerCRMSystem' => 'Vodafone',
            'NBALevel' => 'MobileSubscription',
            'CustomerNumber' => $customerNumber,
            'SubscriptionNumber' => $customerCtn,
            'SelectedPropositionID' => '',
            'NumOfPropositions' => '3',
            'AgentID' => $agentUsername,
            'DealerCode' => $dealerCode,
            'pzSkinName' => 'UhelpPega',
            'action' => 'display',
            'harnessName' => 'CSAdvisor',
            'className' => 'Data-Portal',
            'TrustToken' => $token
        ];

        return $baseUrl . '?' . http_build_query( $parameters, '', '&' );
    }
}
