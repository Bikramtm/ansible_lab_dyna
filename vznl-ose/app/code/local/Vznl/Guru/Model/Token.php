<?php

use Jose\Component\Core\AlgorithmManager;
use Jose\Component\Core\Converter\StandardConverter;
use Jose\Component\Signature\Algorithm\RS256;
use Jose\Component\Signature\JWSBuilder;
use Jose\Component\Encryption\Algorithm\KeyEncryption\RSAOAEP256;
use Jose\Component\Encryption\Algorithm\ContentEncryption\A128CBCHS256;
use Jose\Component\Encryption\Compression\CompressionMethodManager;
use Jose\Component\Encryption\Compression\Deflate;
use Jose\Component\Encryption\JWEBuilder;
use Jose\Component\KeyManagement\JWKFactory;
use Jose\Component\Encryption\NestedTokenBuilder;
use Jose\Component\Signature\Serializer\JWSSerializerManager;
use Jose\Component\Encryption\Serializer\JWESerializerManager;

class Vznl_Guru_Model_Token
{

    /**
     * Config path for certs
     * @var string
     */
    const CERTS_CONFIG = 'vodafone_customer/guru/%s';

    /**
     * JWT expiration time
     * @var int
     */
    const EXPIRY_SECONDS = 3600 * 8;

    /**
     * Private key to sign JTW and create JWS
     * @var string
     */
    private $jwsKey;

    /**
     * PEGA cert
     * @var string
     */
    private $jweCert;

    /**
     * PEGA private key ID
     * @var string
     */
    private $jwsKeyId;

    /**
     * Private key passphrase (empty if not set)
     * @var string
     */
    private $jwsKeyPassphrase;

    /**
     * Token storage (local)
     * @var array
     */
    private $tokenStorage;

    /**
     * Prepare JWT config details
     * @return void
     */
    public function __construct() {
        $this->jwsKey = Mage::getStoreConfig(sprintf(self::CERTS_CONFIG, 'jws_key'));
        $this->jwsKeyPassphrase = Mage::getStoreConfig(sprintf(self::CERTS_CONFIG, 'jws_key_pass'));
        $this->jwsKeyId = Mage::getStoreConfig(sprintf(self::CERTS_CONFIG, 'jws_key_id'));
        $this->jweCert = Mage::getStoreConfig(sprintf(self::CERTS_CONFIG, 'jwe_cert'));
    }

    /**
     * Get JWE (encrypted and signed JWT)
     * @param string $agentId
     * @param string $customerNumber
     * @param string $customerType
     * @throws /Exception
     * @return string
     */
    public function getToken(
        string $agentId,
        string $customerNumber,
        string $customerType
    ):string {
        $tokenId = md5( $agentId . '' . $customerNumber . '' . $customerType);
        $token = $this->loadFromStorage($tokenId);

        if (empty($token)) {
            $token = $this->generateToken($agentId, $customerNumber, $customerType);
            $this->saveToStorage($tokenId, $token);
        }

        return $token;
    }

    /**
     * Get JWE (encrypted and signed JWT)
     * @param string $agentId
     * @param string $customerNumber
     * @param string $customerType
     * @throws /Exception
     * @return string
     */
    private function generateToken(
        string $agentId,
        string $customerNumber,
        string $customerType
    ) {
        $keyEncryptionAlgorithmManager = AlgorithmManager::create([
            new RS256(),
            new RSAOAEP256()
        ]);
        $contentEncryptionAlgorithmManager = AlgorithmManager::create([
            new A128CBCHS256(),
            new RSAOAEP256()
        ]);
        $compressionMethodManager = CompressionMethodManager::create([
            new Deflate(),
        ]);

        $jwkSign = JWKFactory::createFromKey(
            $this->jwsKey,
            $this->jwsKeyPassphrase,
            [
                'use' => 'sig'
            ]
        );
        $jwkEncrypt = JWKFactory::createFromCertificate(
            $this->jweCert,
            [
                'use' => 'enc'
            ]
        );

        $jsonConverter = new StandardConverter();

        $issueDate = (new \DateTime("now", new \DateTimeZone("UTC")))
            ->getTimestamp();
        $expirationDate = (new \DateTime("now", new \DateTimeZone("UTC")))
            ->setTimestamp(time() + self::EXPIRY_SECONDS)
            ->getTimestamp();

        $payload = $jsonConverter->encode([
            'iat' => $issueDate,
            'exp' => $expirationDate,
            'sub' => $agentId,
            'cnum' => $customerNumber,
            'ctype' => $customerType
        ]);

        $jweBuilder = new JWEBuilder(
            $jsonConverter,
            $keyEncryptionAlgorithmManager,
            $contentEncryptionAlgorithmManager,
            $compressionMethodManager
        );
        $jwsBuilder = new JWSBuilder(
            $jsonConverter,
            $keyEncryptionAlgorithmManager
        );

        $jweSerializerManager = JWESerializerManager::create([
            new Jose\Component\Encryption\Serializer\CompactSerializer(new StandardConverter())
        ]);
        $jwsSerializerManager = JWSSerializerManager::create([
            new Jose\Component\Signature\Serializer\CompactSerializer(new StandardConverter())
        ]);

        $nestedTokenBuilder = new NestedTokenBuilder(
            $jweBuilder,
            $jweSerializerManager,
            $jwsBuilder,
            $jwsSerializerManager
        );

        $token = $nestedTokenBuilder->create(
            $payload,
            [[
                'key' => $jwkSign,
                'protected_header' => [
                    'alg' => 'RS256',
                    'kid' => $this->jwsKeyId
                ]
            ]],
            'jws_compact',
            [
                'alg' => 'RSA-OAEP-256',
                'enc' => 'A128CBC-HS256'
            ],
            [],
            [[
                'key' => $jwkEncrypt
            ]],
            'jwe_compact'
        );

        return $token;
    }

    /**
     * Save token in storage
     * @param string $tokenId
     * @param string $token
     * @return void
     */
    private function saveToStorage(
        string $tokenId,
        string $token
    ):void {
        $this->tokenStorage[$tokenId] = $token;
    }

    /**
     * Load token from storage to avoid crypto overhead with creating multiple tokens of the same value
     * @param string $tokenId
     * @return string
     */
    private function loadFromStorage(
        string $tokenId
    ):string {
        return isset($this->tokenStorage[$tokenId]) ? $this->tokenStorage[$tokenId] : '';
    }
}
