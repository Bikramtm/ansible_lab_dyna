<?php

use GuzzleHttp\Client;

class Vznl_CheckoutOptions_Adapter_Peal_Factory
{
    public static function create()
    {
        $client = new Client();
        $helper = Mage::helper('vznl_checkoutoptions');
        return new Vznl_CheckoutOptions_Adapter_Peal_Adapter(
            $client,
            $helper->getEndPoint(),
            $helper->getChannel(),
            $helper->getCountry()
        );
    }
}