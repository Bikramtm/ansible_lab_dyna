<?php

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;

/**
 * Class Vznl_GetCheckoutOptions_Adapter_Peal_Adapter
 */
class Vznl_CheckoutOptions_Adapter_Peal_Adapter
{
    CONST name = "CheckoutOptions";
    /**
     * @var string
     */
    private $endpoint;

    /**
     * @var Client
     */
    private $client;

    /**
     * @var string
     */
    private $channel;

    /**
     * @var string
     */
    private $cty;

    /**
     * @var string
     */
    private $executionTimeId;

    /**
     * @var string
     */
    private $requestTimeId;

    /**
     * Adapter constructor.
     *
     * @param Client $client
     * @param string $endpoint
     * @param string $channel
     * @param string $cty
     */
    public function __construct(Client $client, string $endpoint, string $channel, string $cty)
    {
        $this->endpoint = $endpoint;
        $this->channel = $channel;
        $this->cty = $cty;
        $this->client = $client;
    }

    /**
     * @param $basketID
     * @return array|string $source
     * @throws Exception
     * @throws GuzzleException
     */
    public function call($basketID)
    {
        $this->executionTimeId = $this->getHelper()->initTimeMeasurement();

        if (is_null($basketID)) {
            return 'No basket id provided to adapter';
        }
        $userName = $this->getHelper()->getLogin();
        $password = $this->getHelper()->getPassword();
        $verify = $this->getHelper()->getVerify();
        $proxy = $this->getHelper()->getProxy();
        try {
            $this->requestTimeId = $this->getHelper()->initTimeMeasurement();
            if ($userName != '' && $password != '') {
                $response = $this->client->request('GET', $this->getUrl($basketID), ['auth' => [$userName, $password], 'verify' => $verify, 'proxy' => $proxy]);
            } else {
                $response = $this->client->request('GET', $this->getUrl($basketID), ['verify' => $verify, 'proxy' => $proxy]);
            }

            $this->getHelper()->transferLog(
                json_encode(['url' => $this->getUrl($basketID), 'auth' => [$userName, $password], 'verify' => $verify, 'proxy' => $proxy]),
                json_encode(json_decode($response->getBody())),
                self::name,
                [
                    'execution' => $this->executionTimeId,
                    'request' => $this->requestTimeId
                ],
                'PEAL'
            );

            $source = json_decode($response->getBody(), true);
        } catch (ClientException|RequestException $exception) {
            $this->getHelper()->transferLog(
                $exception->getRequest(),
                ['msg' => $exception->getMessage(), 'code' => $exception->getCode()],
                self::name,
                [
                    'execution' => $this->executionTimeId,
                    'request' => $this->requestTimeId
                ],
                'PEAL'
            );

            $source = array (
                'error'=> true,
                'statusCode' => $exception->getCode()
            );
        }
        return $source;
    }

    /**
     * @param $basketID
     * @return string
     */
    public function getUrl($basketID)
    {
        return $this->endpoint . $basketID . "/checkoutoptions?" .
            "cty=" . $this->cty .
            "&chl=" . $this->channel;
    }

    /**
     * @return object Vznl_ValidateSerialNumber_Helper_Data
     */
    public function getHelper()
    {
        return Mage::helper('vznl_checkoutoptions');
    }
}
