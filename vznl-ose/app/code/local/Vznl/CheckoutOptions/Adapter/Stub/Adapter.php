<?php

/**
 * Class Vznl_CheckoutOptions_Adapter_Stub_Adapter
 *
 */
class Vznl_CheckoutOptions_Adapter_Stub_Adapter
{
    CONST NAME = 'CheckoutOptions';
    /**
     * @param $basketID
     * @return $responseData
     */
    public function call($basketID, $arguments = array())
    {
        if (is_null($basketID)) {
            return 'No basket id provided to adapter';
        }
        $stubClient = Mage::helper('vznl_checkoutoptions')->getStubClient();
        $stubClient->setNamespace('Vznl_CheckoutOptions');
        $responseData = $stubClient->call(self::NAME, $arguments);
        Mage::helper('vznl_checkoutoptions')->transferLog($arguments, $responseData, self::NAME);
        return $responseData;
    }
}