<?php

class Vznl_CheckoutOptions_Helper_Data extends Vznl_Core_Helper_Data
{
    /*
    * @return String
    * */
    public function getAdapterChoice()
    {
        return Mage::getStoreConfig('vodafone_service/checkout_options/adapter_choice');
    }

    /*
    * @return string peal endpoint url
    * */
    public function getEndPoint()
    {
        return Mage::getStoreConfig('vodafone_service/checkout_options/end_point');
    }

    /*
    * @return string peal username
    * */
    public function getLogin()
    {
        return Mage::getStoreConfig('vodafone_service/checkout_options/login');
    }

    /*
    * @return string peal password
    * */
    public function getPassword()
    {
        return Mage::getStoreConfig('vodafone_service/checkout_options/password');
    }

    /*
    * @return string peal cty
    * */
    public function getCountry()
    {
        return Mage::getStoreConfig('vodafone_service/checkout_options/country');
    }

    /*
    * @return string peal channel
    * */
    public function getChannel()
    {
        $agentHelper = Mage::helper('agent');
        $salesChannel = $agentHelper->getDealerSalesChannel();
        if ($salesChannel) {
            return trim($salesChannel);
        } else {
            return trim(Mage::getStoreConfig('vodafone_service/checkout_options/channel'));
        }
    }

    /*
    * @return object omnius_service/client_stubClient
    * */
    public function getStubClient()
    {
        return Mage::getModel('omnius_service/client_stubClient');
    }

    /*
    * @return boolean peal verify
    * */
    public function getVerify()
    {
        return Mage::getStoreConfigFlag('vodafone_service/checkout_options/verify');
    }

    /*
    * @return boolean peal proxy
    * */
    public function getProxy()
    {
        return Mage::getStoreConfigFlag('vodafone_service/checkout_options/proxy');
    }

    /*
     * @return object Omnius_Service_Model_Logger
     */
    public function getLogger()
    {
        return Mage::getSingleton('omnius_service/logger');
    }

    /*
    * @param $basketID
    * @return array peal/stub response based on adpter choice
    * */
    public function checkoutOptions($basketID)
    {
        $factoryName = $this->getAdapterChoice();
        $validAdapters = Mage::getSingleton('checkoutOptions/system_config_source_checkoutOptions_adapter')->toArray();
        if (!in_array($factoryName, $validAdapters)) {
            return 'Either no adapter is chosen or the chosen adapter is no longer/not supported!';
        }
        $adapter = $factoryName::create();
        return $adapter->call($basketID);
    }

    /**
     * Method used in SC API to parse PEAL privacy settings
     *
     * @param Mage_Sales_Model_Quote $quote
     * @param array $privacySettings
     * @return array
     */
    public function proceedFixedPrivacySettings(
        Mage_Sales_Model_Quote $quote,
        array $privacySettings
    ): array {
        $additionalAttributes = [];
        foreach ($privacySettings['productOptions'] ?? [] as $productOption) {
            if ($productOption['productOptionName'] == Vznl_Catalog_Model_Type::FIXED_TELEFONIE_PRODUCT
                && ($productOption['businessLine'] == Vznl_Configurator_Model_PackageType::TELEPHONY_FIRST_LINE
                    || $productOption['businessLine'] == Vznl_Configurator_Model_PackageType::TELEPHONY_BOTH_LINE)
            ) {
                foreach ($productOption['options'] as $option) {
                    if (strtolower($option['key']) == 'add_phone_book') {
                        $additionalAttributes[] = [
                            'key' => 'privacyFixed.addPhoneBook',
                            'value' => (strtolower($option['defaultValue']) == 'yes'
                                || $quote->getDefaultValueForField('privacy_add_phone_book', 'checkout'))
                                ? 'true' : 'false'
                        ];
                    }
                    if (strtolower($option['key']) == 'block_my_caller_id') {
                        $additionalAttributes[] = [
                            'key' => 'privacyFixed.blockMyCallerId',
                            'value' => (strtolower($option['defaultValue']) == 'yes'
                                || $quote->getDefaultValueForField('privacy_block_caller_id', 'checkout'))
                                ? 'true' : 'false'
                        ];
                    }
                    if (strtolower($option['key']) == 'add_0900') {
                        $additionalAttributes[] = [
                            'key' => 'privacyFixed.add0900',
                            'value' => (strtolower($option['defaultValue']) == 'yes'
                                || $quote->getDefaultValueForField('privacy_add_info', 'checkout'))
                                ? 'true' : 'false'
                        ];
                    }
                    if (strtolower($option['key']) == 'hide_number_from_bill') {
                        $additionalAttributes[] = [
                            'key' => 'privacyFixed.hideNumberFromBill',
                            'value' => (strtolower($option['defaultValue']) == 'yes'
                                || $option['defaultValue'] == null
                                || strtolower($option['defaultValue']) == 'null'
                                || $quote->getDefaultValueForField('privacy_hide_number', 'checkout'))
                                ? 'true' : 'false'
                        ];
                    }
                }
            }
        }

        return $additionalAttributes;
    }
}
