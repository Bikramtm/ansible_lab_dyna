<?php

/**
 * Class Vznl_Import_Model_Summary
 */
class Vznl_Import_Model_Summary extends Dyna_Import_Model_Summary
{

    protected $_logFileExtension= "log";
    public $_logFileName = "version_import";

    protected function _construct()
    {
        $this->setIdFieldName('import_id');
        $this->_init('import/summary');
        /** @var Vznl_Import_Helper_Data _helper */
        $this->_helper = Mage::helper("vznl_import");
    }

    public function getFileLogName()
    {
        return $this->_logFileName . '.' . $this->_logFileExtension;
    }
}
