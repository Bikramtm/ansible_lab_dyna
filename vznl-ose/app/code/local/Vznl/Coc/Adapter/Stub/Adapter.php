<?php

use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Zend\Soap\Client;

/**
 * Class Vznl_Coc_Adapter_Stub_Adapter
 */
class Vznl_Coc_Adapter_Stub_Adapter
{
    public function send($cocNumber)
    {
        return array(
            "k_v_k_number" => $cocNumber,
            "company_name" => 'Dynacommerce',
            "houseno" => '1',
            "postcode" => '1111AA',
            "company_vat_id" => '3423534',
            "date" => date('d-m-Y'),
        );
    }
}
