<?php

use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Zend\Soap\Client;

/**
 * Class Vznl_Coc_Adapter_Coc_Adapter
 */
class Vznl_Coc_Adapter_Coc_Adapter
{
    /** @var LoggerInterface */
    private $logger;

    /**
     * @var array
     */
    private $options = [];

    public function __construct(array $options=[])
    {
        $this->options = array_merge($this->options, $options);
        $this->setLogger();
    }

    public function send($cocNumber)
    {
        try {
            $clientOptions = isset($this->options['client_options']) ? $this->options['client_options'] : [];
            $client = new GuzzleHttp\Client($clientOptions);
            $res = $client->request('GET', $this->options['service_url'] . '/organization/' . $cocNumber);
            $data = json_decode($res->getBody(), true);
        } catch(Exception $e){
            Mage::getSingleton('core/logger')->logException($e);
            throw new Exception(sprintf(Mage::helper('dyna_checkout')->__('CoC number %s not found.'), $cocNumber));
        }

        $this->logAction(PHP_EOL . 'Request:' . PHP_EOL . 'CoC Number: ' . $cocNumber . PHP_EOL . 'Response:' . PHP_EOL . $res->getBody() . PHP_EOL);
        return $this->formatCocResponse($data);
    }

    /**
     * Format the given response to the correct/supported format
     * @param $result The result from the service
     * @return array|null The result or null if data was missing.
     */
    protected function formatCocResponse($cocCompany)
    {
        $result = [];
        // Checks if a company is returned
        if (!$cocCompany || empty($cocCompany)) {
            return null;
        }else {
            // Get the creation date
            $creationDate = '';
            if($cocCompany['creation_time']){
                $timeString = strtotime($cocCompany['creation_time']);
                if($timeString){
                    $creationDate = date('d-m-Y', $timeString);
                }
            }

            $result['k_v_k_number'] = isset($cocCompany['chamber_of_commerce']) ? $cocCompany['chamber_of_commerce'] : '';
            $result['company_name'] = isset($cocCompany['company_name']) ? $cocCompany['company_name'] : '';
            $result['houseno'] = isset($cocCompany['house_number']) ? $cocCompany['house_number'] : '';
            $result['postcode'] = isset($cocCompany['zip_code']) ? $cocCompany['zip_code'] : '';
            $result['company_vat_id'] = isset($cocCompany['id']) ? $cocCompany['id'] : '';
            $result['date'] = $creationDate;
        }

        return $result;
    }

    /**
     * @param $message
     * @return void
     */
    public function logAction($message)
    {
        $this->logger->log(Psr\Log\LogLevel::DEBUG, $message);
    }

    /**
     * Set the logger and the file it should log in
     */
    protected function setLogger()
    {
        $logger = new Aleron75_Magemonolog_Model_Logwriter('services/Coc/'.\Ramsey\Uuid\Uuid::uuid4().'.log');
        $this->logger = $logger->getLogger();
    }
}
