<?php

/**
 * Class Vznl_Coc_Adapter_Default_Adapter
 */
class Vznl_Coc_Adapter_Default_Adapter extends Omnius_Service_Model_Client_Client
{
    /** @var Dyna_Configurator_Model_Cache */
    protected $_cache;

    /**
     * Retrieves additional info regarding the provided KVK
     *
     * @param $kvk
     * @return array|mixed
     * @throws Exception
     */
    public function send($kvk)
    {
        $defaultParams = $this->_getDefaultParams();
        $values = array_values($this->executeRequest('GetKVKDetails', $this->_getRequestBuilder()
            ->create(
                $this->getNamespace(),
                'GetKVKDetails',
                array_merge(array('KVKNumber' => $kvk), $defaultParams)
            )
        ));
        $response = reset($values);

        try {
            $response = new SimpleXMLElement($this->getAccessor()->getValue($response, 'string.1'));
        } catch (Exception $e) {
            $message = "\n" . $e->__toString();
            $message .= "\nClient: " . json_encode($this->_options);
            $message .= "\nMethod: GetKVKDetails";
            $message .= "\nArguments: " . $kvk;
            $message .= "\nResponse: " . json_encode($response);
            $message .= "\nSoap request: " . parent::__getLastRequest();
            $message .= "\nSoap request headers: " . parent::__getLastRequestHeaders();
            $message .= "\nSoap response: " . parent::__getLastResponse();
            $message .= "\nSoap response headers: " . parent::__getLastResponseHeaders();
            $message .= "\n\n\n\n";
            $this->log($message, Zend_Log::CRIT);
            throw $e;
        }

        $result = $this->getNormalizer()
            ->normalizeKeys(
                $this->getNormalizer()
                    ->objectToArray($this->object_to_array($response->children())));

        return $this->formatCocResponse($result);
    }

    /**
     * Format the given response to the correct/supported format
     * @param $result The result from the service
     * @return array|null The result or null if data was missing.
     */
    protected function formatCocResponse($result)
    {
        if(!(is_array($result) && (!empty($result['list_of_k_v_k_details']) || !empty($result['k_v_k_number'])))){
            return null;
        }

        if(isset($result['list_of_k_v_k_details']) && isset($result['list_of_k_v_k_details']['@attributes'])){
            $result = $this->formatCocResponseList($result);
        }

        if (empty($result['k_v_k_number']) || empty($result['zip_code']) ||  empty($result['house_number'])) {
            return null;
        } else {
            $result = $this->formatCocResponseAddress($result);
        }

        return $result;
    }

    protected function formatCocResponseList($result)
    {
        $attribute = $result['list_of_k_v_k_details']['@attributes'];
        $detail = $result['list_of_k_v_k_details']['k_v_k_detail'];
        $kvkCount = isset($attribute['count']) ? (int)$attribute['count'] : 0;
        if ($kvkCount == 1) {
            $result = $detail;
        } else {
            $kvks = $detail;
            uasort(
                $kvks,
                function ($elem1, $elem2) {
                    if ($elem1['date'] == $elem2['date']) {
                        return 0;
                    }
                    return $elem1['date'] < $elem2['date'] ? 1 : -1;
                }
            );
            $result = $kvks[0];
        }
        return $result;
    }

    protected function formatCocResponseAddress($result)
    {
        $address = null;
        try {
            /** @var Dyna_Postcode_Helper_Data $postcodeHelper */
            $postcodeHelper = Mage::helper('postcode');
            $address = $postcodeHelper->getPostcodeAddressData((string)$result['zip_code'], (int)$result['house_number']);
        } catch (\Exception $e) {
            $this->log($e->getMessage(), Zend_Log::ALERT);
        }
        $date = $result['date'];
        if(strlen($date) == 8) {
            $result['date'] = substr($date,6,2) . '-' . substr($date,4,2) . '-' . substr($date,0,4);
        }
        if($address) {
            $result = array_merge($result, array(
                "street"   => $address["street"],
                "houseno"  => $address["house_number"],
                "postcode" => $address["postcode"],
                "city"     => $address["town"]
            ));
        } else {
            return null;
        }
        return $result;
    }

    /**
     * Convert object to array recursive
     * @param $obj
     * @return array
     */
    protected function object_to_array($obj) {
        if(is_object($obj)) $obj = (array) $obj;
        if(is_array($obj)) {
            $new = array();
            foreach($obj as $key => $val) {
                $new[$key] = $this->object_to_array($val);
            }
        }
        else $new = $obj;

        return $new;
    }

    /**
     * @param $functionality
     * @param $request
     * @return mixed
     */
    public function executeRequest($functionality, $request)
    {
        $params = array(
            'functionalityName' => $functionality,
            'loginName' => $this->getHelper()->getKvkConfig('login'),
            'password' => $this->getHelper()->getKvkConfig('password'),
            'request' => $request,
        );

        return $this->ExecuteRequestWithLogin($params);
    }

    /**
     * @return Dyna_Configurator_Model_Cache
     */
    protected function getCache()
    {
        if ( ! $this->_cache) {
            $this->_cache = Mage::getSingleton('dyna_configurator/cache');
        }
        return $this->_cache;
    }

    /**
     * @return string
     */
    public function getNamespace()
    {
        return 'Kvk';
    }

    public function getUseStubs(){
        return false;
    }
}
