<?php

/**
 * Used in admin panel adapter selector for coc service
 *
 */
class Vznl_Coc_Model_System_Config_Source_Coc_Adapter
{
    const DEFAULT_ADAPTER = 'Default';
    const STUB_ADAPTER = 'Stub';
    const COC_ADAPTER = 'Coc';

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => self::DEFAULT_ADAPTER, 'label'=>Mage::helper('adminhtml')->__('Default adapter')),
            array('value' => self::STUB_ADAPTER, 'label'=>Mage::helper('adminhtml')->__('Stub adapter')),
            array('value' => self::COC_ADAPTER, 'label'=>Mage::helper('adminhtml')->__('CoC adapter')),
        );
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        $optionArray = $this->toOptionArray();
        $array = [];
        foreach($optionArray as $option){
            $array[] = $option['value'];
        }
        return $array;
    }

}
