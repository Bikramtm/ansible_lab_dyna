<?php
class Vznl_Coc_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Get the store config values for the given name for the Coc module
     * @param string $name The name of the config
     * @return mixed The value if any
     */
    public function getCocConfig($name = 'wsdl')
    {
        return Mage::getStoreConfig('vodafone_service/coc/' . $name);
    }

    /**
     * Gets the CoC data
     * @param $cocNumber The CoC number
     * @return array the coc data.
     * @throws Exception when the chosen adapter doesn't exist.
     */
    public function getCoCData($cocNumber)
    {
        $adapterToUse = $this->getCocConfig('adapter_choice');

        $validAdapters = Mage::getSingleton('coc/system_config_source_coc_adapter')->toArray();
        if(!in_array($adapterToUse, $validAdapters)){
            throw new Exception('Either no adapter is chosen or the chosen adapter is no longer/not supported!');
        }

        $factoryName = 'Vznl_Coc_Adapter_' . $adapterToUse . '_AdapterFactory';
        $adapter = $factoryName::create([]);

        if(!$adapter){
            throw new Exception('Something went wrong while creating the coc adapter');
        }

        return $adapter->send($cocNumber);
    }
}
