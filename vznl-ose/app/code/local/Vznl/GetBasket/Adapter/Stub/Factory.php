<?php

class Vznl_GetBasket_Adapter_Stub_Factory
{
    public static function create()
    {
        return new Vznl_GetBasket_Adapter_Stub_Adapter();
    }
}