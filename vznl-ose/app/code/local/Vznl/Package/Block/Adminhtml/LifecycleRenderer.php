<?php

class Vznl_Package_Block_Adminhtml_LifecycleRenderer extends Dyna_Package_Block_Adminhtml_LifecycleRenderer
{
    /**
     * Renders grid column for package type visibility
     * @param   Varien_Object $row
     * @return  string
     */
    public function render(Varien_Object $row)
    {
        $renderedData = $row->getData();
        if (($renderedData['stack'] == Vznl_Sandbox_Helper_Data::FILENAME_FIXED_KEYWORD)
            && !empty($renderedData['package_subtype_lifecycle_status'])) {
            // if stack is Fixed, just return the value as it needs no further mapping
            $lifeCycleValues = $renderedData['package_subtype_lifecycle_status'];
        } else {
            $lifeCycleValue = $row->getPackageSubtypeLifecycleStatus();
            $lifeCycleValues = Dyna_Catalog_Model_Lifecycle::getLifecycleTypes();
            $lifeCycleValues = $lifeCycleValues[$lifeCycleValue] ?? "-";
        }

        return $lifeCycleValues;
    }
}
