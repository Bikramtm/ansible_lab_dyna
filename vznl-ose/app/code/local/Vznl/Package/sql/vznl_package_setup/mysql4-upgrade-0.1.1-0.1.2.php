<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */

/** @var Mage_Core_Model_Resource_Setup $installer */

$installer = $this;
$installer->startSetup();
$connection = $installer->getConnection();

$table = $this->getTable('package/package');
$index = 'IDX_CATALOG_PACKAGE_ORDER_ID_PACKAGE_ID';

$connection->addIndex($table, $index, array('order_id','package_id'));

$installer->endSetup();
