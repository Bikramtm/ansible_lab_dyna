<?php
/**
 * Class Vznl_Package_Helper_Data
 */
class Vznl_Package_Helper_Data extends Dyna_Package_Helper_Data
{
    /**
     * @param array $package
     * @return array
     */
    public function packageIsAcquisition($package)
    {
        return in_array($package->getSaleType(), [
                Dyna_Checkout_Model_Sales_Quote_Item::ACQUISITION,
                Vznl_Checkout_Model_Sales_Quote_Item::ACQUISITIE
            ]
        );
    }

    /**
     * @param array $data
     * @param int $packageId
     * @param Vznl_Checkout_Model_Sales_Order $order
     * @return array
     */
    public function processPrices($data, $packageId, $order)
    {
        $deviceRc = null;
        $subscription = null;
        /** @var Dyna_Checkout_Model_Sales_Order_Item $item */
        foreach ($order->getAllItems() as $item) {
            if ($item->getProduct()->is(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE_SUBSCRIPTION) && $packageId == $item->getPackageId()) {
                $deviceRc = $item;
            }

            if ($item->getProduct()->is(Dyna_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION) && $packageId == $item->getPackageId()) {
                $subscription = $item;
            }
        }

        if ($deviceRc) {
            $contractDuration = $subscription->getProduct()->getSubscriptionDuration();
            $recurringCumulated = $deviceRc->getItemFinalMafInclTax() * (int)$contractDuration;
            $mixmatch = $this->formatPrice($data['mixmatch']);
            $baseMixmatch = $recurringCumulated + $mixmatch;
            $catalogPrice = $this->formatPrice($data['catalog_price']);
            $catalogPriceInclTax = $this->formatPrice($data['catalog_price_incl_tax']);
        } else {
            return null;
        }

        return [
            'device_name' => $data['device_name'],
            'imei_number' => $data['imei_number'],
            'mixmatch' => $mixmatch,
            'base_mixmatch' => $baseMixmatch,
            'catalog_price' => $catalogPrice,
            'catalog_price_incl_tax' => $catalogPriceInclTax,
            'recurring_cumulated' => $recurringCumulated
        ];
    }

    /**
     * @param null $superOrderId
     * @param null $quoteId
     * @param null $packageId
     * @return mixed
     */
    public function getPackages($superOrderId = null, $quoteId = null, $packageId = null)
    {
        $fields = $constraints = array();
        $superOrderId = (int)$superOrderId;
        $quoteId = (int)$quoteId;
        $packageId = (int)$packageId;

        if ((!$superOrderId) && (!$quoteId)) {
            Mage::throwException('Must provide at least a super order ID or a quote ID');
        }

        $key = __METHOD__ . ' ' . $superOrderId . ' ' . $quoteId . ' ' . $packageId;
        if (Mage::registry('freeze_models') === true) {
            if ($data = Mage::registry($key)) {
                return $data;
            }
        }

        $collection = Mage::getResourceModel('package/package_collection');

        if ($superOrderId && $quoteId) {
            $fields = array('order_id', 'quote_id');
            $constraints = array(
                array('eq' => $superOrderId),
                array('eq' => $quoteId),
            );
        } elseif (!$superOrderId) {
            $fields = 'quote_id';
            $constraints = array('eq' => $quoteId);
        } elseif (!$quoteId) {
            $fields = 'order_id';
            $constraints = array('eq' => $superOrderId);
        }

        $collection->addFieldToFilter($fields, $constraints);

        if ($packageId) {
            $collection->addFieldToFilter('package_id', $packageId);
        }

        if (Mage::registry('freeze_models') === true) {
            Mage::unregister($key);
            Mage::register($key, $collection);
        } else {
            Mage::unregister($key);
        }

        return $collection;
    }

    /**
     * Check if the given package or one of it ancestor packages ever had a device rc with an amount higher then 0.
     * @param $packageId The package id
     * @param Dyna_Checkout_Model_Sales_Order $deliveryOrder The delivery order
     * @return boolean <true> if an device rc existed for this package, <false> if not.
     */
    public function packageEverHadADeviceRc($packageId, $deliveryOrder, $deviceId = false)
    {
        // If device id is not set current device is always the same device (also for indirect)
        $sameDevice = $deviceId ? false : true;
        $hadAValue = false;
        $isDoa = false;
        /** @var Dyna_Checkout_Model_Sales_Order_Item|null $deviceRc */
        foreach ($deliveryOrder->getAllItems() as $item) {
            if ($item->getPackageId() != $packageId) {
                continue;
            }

            $productType = $item->getProduct()->getType();
            $flippedProductType = array_flip($productType);
            if (isset($flippedProductType[Dyna_Catalog_Model_Type::SUBTYPE_DEVICE_SUBSCRIPTION])) {
                if ($item->getTotalPaidByMaf() > 0) {
                    $hadAValue = true;
                }
            } else if (isset($flippedProductType[Dyna_Catalog_Model_Type::SUBTYPE_DEVICE])) {
                $currentDeviceId = $item->getProduct()->getId();
                // check if same and not doa (if doa the item looks the same but is not)
                if ($deviceId == $currentDeviceId) {
                    $sameDevice = true;
                }
                $deviceId = $currentDeviceId;
                $isDoa = $item->getItemDoa();
            }
        }

        if ($deviceId && !$sameDevice) {
            return false;
        } else if ($hadAValue) {
            return true;
        } else if ($isDoa) {
            // If this item is marked as DOA the next order will not have the same device so it won't matter
            return false;
        }

        if ($parentId = $deliveryOrder->getParentId()) {
            $parentDeliveryOrder = Mage::getModel('sales/order')->load($parentId);
            if ($parentDeliveryOrder && $parentDeliveryOrder->getEntityId() && $this->packageEverHadADeviceRc($packageId, $parentDeliveryOrder, $deviceId)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if the quote contains changes if compared to the super order.
     * @param Dyna_Checkout_Model_Sales_Quote $quote
     * @param Dyna_Superorder_Model_Superorder $superOrder
     * @param int $packageIdToCheck The package id to check, if <null>  check all packages.
     * @return boolean <true> if contains changed, <false> if not.
     */
    public function containsChanges($quote, $superOrder, $packageIdToCheck = null)
    {
        $packages = $superOrder->getPackages();
        /** @var Dyna_Package_Model_Package $package */
        foreach ($packages as $package) {
            $packageId = $package->getPackageId();
            if ($packageIdToCheck && $packageId != $packageIdToCheck) {
                continue;
            }

            // Checks whether the package is known in the quote
            $quotePackage = $quote->getPackageById($packageId);
            if (!$quotePackage || !$quotePackage->getId()) {
                continue;
            }

            $quoteItems = $quote->getPackageItems($packageId);
            $packageItems = $package->getPackageItems();

            // Check if more or less items
            if ($this->getArrayCount($packageItems) <> $this->getArrayCount($quoteItems)) {
                return true;
            }

            // If item amount is equal check if all the items are the same
            /** @var Dyna_Checkout_Model_Sales_Quote_Item $quoteItem */
            foreach ($quoteItems as $quoteItem) {
                $itemFound = false;
                /** @var Dyna_Checkout_Model_Sales_Order_Item $packageItem */
                foreach ($packageItems as $packageItem) {
                    if ($quoteItem->getSku() == $packageItem->getSku()) {
                        $itemFound = true;
                    }
                }
                if (!$itemFound) {
                    return true;
                }
            }
        }

        return false;
    }

    public function getArrayCount($dataArray)
    {
        return count($dataArray);
    }

    /**
     * @param $key
     * @return mixed|string
     */
    public function getPackageNameByKey($key)
    {
        $packageNames = [
            'ACQ' => 'acquisitie',
            'RETBLU' => 'retentie'
        ];

        if (array_key_exists(strtoupper($key), $packageNames)) {
            return $packageNames[$key];
        } else {
            return strtoupper($key);
        }
    }

      public function getPackageState($package)
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        /** @var Dyna_Bundles_Helper_Data $bundlesHelper */
        $bundlesHelper = Mage::helper('dyna_bundles');

        if ($this->cachedEligibleBundles === null) {
            $this->cachedEligibleBundles = $bundlesHelper->getFrontendEligibleBundles(true);
        }

        $response = [
            'title' => null, // can be sharing-group | bundle | intra-stack-bundle | installed-base-bundle |disabled
            /*
             * sharing-group
             * intra-stack-bundle
             *
             */
            'state' => [
                'icon'  => null,
                'badge' => null // empty | empty darkcyan | darkcyan  (colors: darkcyan | darkorchid | darkslategray | hint | attachment)
            ]
        ];

        $packageModel = isset($package['package_id']) ? $quote->getCartPackage($package['package_id']) : null;
        /** @var $packageCreationTypeModel Dyna_Package_Model_PackageCreationTypes */
        $packageCreationTypeModel = Mage::getSingleton('dyna_package/packageCreationTypes');

        if (isset($packageModel)) {

            $processContextHelper = Mage::helper("dyna_catalog/processContext");
            $processContextName = $processContextHelper->getNameByCode($packageModel->getSaleType());
            $response['tooltipInfo'] = array(
                'title' => isset($processContextName) ? $processContextName : '',
            );

            if (in_array($packageModel->getSaleType(), Dyna_Catalog_Model_ProcessContext::ILSProcessContexts())) {
                $response['state']['badge'] = 'ils-modify';
                $response['state']['icon'] = null;
                $response['tooltipInfo']['title'] = $this->__('Modify package');
            }
            if (in_array($packageModel->getSaleType(), Dyna_Catalog_Model_ProcessContext::getProlongationProcessContexts())) {
                $response['state']['badge'] = 'ils-renew';
                $response['state']['icon'] = null;
                $response['tooltipInfo']['title'] = $this->__('Renew package: ');
                if ($packageModel->getSaleType() == Dyna_Catalog_Model_ProcessContext::RETTER){
                    $response['tooltipInfo']['title'] .= $this->__('Early Bird');
                }elseif($packageModel->getSaleType() == Dyna_Catalog_Model_ProcessContext::RETNOBLU){
                    $response['tooltipInfo']['title'] .= $this->__('Standard Prolongation');
                }else{
                     $response['tooltipInfo']['title'] .= $this->__('Phone number');
                }

            }

            if ($packageModel->getSaleType() ==  Dyna_Catalog_Model_ProcessContext::MIGCOC){
                $response['state']['badge'] = 'ils-change';
                $response['state']['icon'] = null;
                $packageCreationTypeFrontName = str_replace('_',' ',$packageCreationTypeModel->load($packageModel->getPackageCreationTypeId())->getPackageTypeCode());
                $response['tooltipInfo']['title'] = $this->__('Change service: ');
                $migrationSourcePackageCreationType = Mage::getSingleton('customer/session')->getSourcePackageCreationType();
                $migrationSourcePackageCreationTypeFrontName = $packageCreationTypeModel->getNameByPackageTypeCode($migrationSourcePackageCreationType);
                if ($packageCreationTypeFrontName == 'DSL'){
                    $response['state']['badge'] .= ' to-dsl';
                    $response['tooltipInfo']['title'] .= $this->__($migrationSourcePackageCreationTypeFrontName) .' to '.$packageCreationTypeFrontName;
                }elseif (strpos($packageCreationTypeFrontName,'Cable') !== false){
                    $response['state']['badge'] .= ' to-cable';
                    $response['tooltipInfo']['title'] .=$this->__($migrationSourcePackageCreationTypeFrontName) .' to '.$packageCreationTypeFrontName;
                }else{
                    $response['tooltipInfo']['title'] .=$this->__('PRE to POST');
                }
            }

            if ($packageModel->getSaleType() ==  Dyna_Catalog_Model_ProcessContext::ACQ){
                $response['state']['badge'] = 'acq';
                $response['state']['icon'] = null;
                $processContextHelper = Mage::helper("dyna_catalog/processContext");
                $processContextName = $processContextHelper->getNameByCode($packageModel->getSaleType());
                $response['tooltipInfo'] = array(
                        'title' => isset($processContextName) ? $processContextName : '',
                );
            }

            if ($packageModel->isPartOfGigaKombi() || $packageModel->isPartOfLowEntry()) {
                if(!empty($package['installed_base_products'])){
                    $response['title'] = 'bundle';
                    $response['title'] .= ' disabled';
                } else {
                    $response['title'] = 'bundle default';
                }
            }
            if ($packageModel->isPartOfRedPlus()) {
                $response = $this->getRedPlusPackageDisplay($package);
            } elseif ($packageModel->isPartOfSuso()) {
                if (strtolower($package['type']) == strtolower(Dyna_Catalog_Model_Type::TYPE_PREPAID)) {
                    $response['state']['badge'] = 'badge empty attachment';
                    $response['state']['icon'] = 'vfde-icon vfde-attach';
                } else {
                    $response['state']['badge'] = 'badge attachment';
                    $response['state']['icon'] = 'vfde-icon vfde-attach';
                }
            }
        }

        foreach ($this->cachedEligibleBundles as $bundle) {
            if ($bundle['lowEntryBundle']) {
                foreach ($bundle['highlightPackages'] as $highlightPackage) {
                    if (isset($package['package_id']) && $package['package_id'] == $highlightPackage['packageId']) {
                        if (empty($response['state']['partOfGroup'])) {
                            $response['state']['badge'] = 'hint';
                            $response['state']['icon'] = 'vfde-lightbulb';
                        }

                        /** @var Dyna_Bundles_Model_BundleRule $tooltipBundle */
                        $tooltipBundle = Mage::getModel('dyna_bundles/bundleRule')->load($bundle['bundleId']);
                        $cartHint = $tooltipBundle->getHintWithLocation(Dyna_Bundles_Model_BundleRule::LOCATION_CART);
                        $response['tooltipInfo'] = array(
                            'title' => isset($cartHint) ? $cartHint->getTitle() : '',
                            'message' => isset($cartHint) ? $cartHint->getText() : ''
                        );
                    }
                }
            }
        }

        return $response;
    }

    /**
     * Update list of cancelled packages during the current session
     *
     * @param int|mixed $params
     * @throws Varien_Exception
     */
    public function updateCancelledPackagesNow($params)
    {
        $sessionQuoteId = Mage::getSingleton('customer/session')->getOrderEdit();
        $packagesIds = array_map('trim', explode(',', $params));
        $cancelPackages = Mage::getSingleton('customer/session')->getCancelledPackagesNow() ?: array($sessionQuoteId => array());
        $cancelPackages[$sessionQuoteId] = isset($cancelPackages[$sessionQuoteId]) ? $cancelPackages[$sessionQuoteId] : array();

        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getModel('sales/quote')->load($sessionQuoteId);
        //Remove changed quotes for one off deal if cancelling other packages.
        $firstPackageModel = Mage::getModel('package/package')->getPackages(null, $sessionQuoteId)->setPageSize(1, 1)->getLastItem();
        if ($firstPackageModel->getOneOfDeal()) {
            $cancelChanged = $this->getModifiedQuotes();
            foreach ($cancelChanged as $quoteToRemove) {
                $quoteToRemove->setSuperQuoteId(null)
                    ->setSuperOrderEditId(null)
                    ->save();
            }
        }

        foreach ($packagesIds as $packageId) {
            if (!in_array($packageId, $cancelPackages[$sessionQuoteId])) {
                $cancelPackages[$sessionQuoteId][] = $packageId;
            }

            foreach ($quote->getAllItems() as $item) {
                if ($item->getPackageId() == $packageId) {
                    $item->setIsCancelled(1)->save();
                }
            }
        }

        /** @var Dyna_FFHandler_Helper_Request $requestHelper */
        $requestHelper = Mage::helper("ffhandler/request");
        $requestHelper->removeCodeUsageByPackagesIds($packagesIds);

        $quote->save();

        Mage::getSingleton('customer/session')->setCancelledPackagesNow($cancelPackages);
    }

    /**
     * Get prices as html for the promos display in extended shopping cart
     * @param $item
     * @param $quoteItem
     * @param $tempKey
     * @param $maf
     * @param $price
     */
    public function getPromoProductsDisplayPrices($item, $quoteItem, $tempKey, &$maf, &$price, $mafAfterDiscount, $isBtw = true)
    {
        $product = $quoteItem->getProduct();

        // get applied promo amount for later use in displaying the minus discounted promo products
        $appliedPromoAmount = $quoteItem->getAppliedPromoAmount();
        $appliedMafPromoAmount = $quoteItem->getAppliedMafPromoAmount();

        $hasPromoValue = false;

        $priceMafNewAmount = $product->getPrijsMafNewAmount();
        $mafDiscount = $product->getMafDiscount();
        // not clear which of these 2 attributes will be used, so will leave both
        $mafDiscountPercent = $product->getMafDiscountPercent() ?: $product->getPrijsMafDiscountPercent();

        if ($priceMafNewAmount || $mafDiscount || $mafDiscountPercent) {
            $hasPromoValue = true;

            if ($priceMafNewAmount) { // if new maf amount is given, display it as the new price
                $mafDiscounted = $priceMafNewAmount;
                $priceDiscounted = $quoteItem->getItemFinalPriceInclTax();
            } elseif ($mafDiscount) { // fixed maf discount
                if ($mafDiscount < $quoteItem->getMafInclTax()) {
                    $mafDiscounted = -1 * abs($appliedMafPromoAmount);
                } else {
                    $mafDiscounted = -1 * abs($mafDiscount);
                }
                $priceDiscounted = -1 * abs($appliedPromoAmount);
            } elseif ($mafDiscountPercent) { // maf percentage discount
                if(($mafDiscountPercent > 0) && ($mafDiscountPercent <= 100)) {
                    $mafDiscounted = -1 * abs($appliedMafPromoAmount);
                    $priceDiscounted = -1 * abs($appliedMafPromoAmount);
                }
            }
        }

        if(!$isBtw) {
            $mafDiscounted = Mage::helper('tax')->getPrice($product, $mafDiscounted, $isBtw);
        }

        $mafDiscounted = $mafAfterDiscount ? $mafAfterDiscount : $mafDiscounted;
        if ($hasPromoValue) {
            $maf[$tempKey] = str_replace('<label class="orderlineprice">', '<label class="orderlineprice striked">', $maf[$tempKey]);
            $maf[] = '<label>' . Mage::helper('core')->currency($mafDiscounted, true, false) . '</label>';
            $price[count($price) - 1] = '<label></label>';
            // keep the old logic and display one time prices only for promos whose target is a deviceRC
            $price[] = $item->getProduct()->is(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE_SUBSCRIPTION) ?
                ('<label>' . Mage::helper('core')->currency($priceDiscounted, true, false) . '</label>') : '<label></label>';
        } else {
            $maf[] = '<label></label>';
            $price[] = '<label></label>';
        }
    }

    /**
     * Get prices as html for the one time fixed promos display in extended shopping cart
     * @param $item
     * @param $quoteItem
     * @param $tempKey
     * @param $price
     * @param $AfterDiscount
     */
    public function getPromoProductsDisplayOneTimePrices($item, $quoteItem, $tempKey, &$price, $AfterDiscount, $isBtw = true)
    {
        $product = $quoteItem->getProduct();

        // get applied promo amount for later use in displaying the minus discounted promo products
        $appliedPromoAmount = $quoteItem->getAppliedPromoAmount();

        $hasPromoValue = false;
        $priceTempKey = $tempKey;

        $priceDiscount = $product->getPriceDiscount();
        $priceDiscountPercent = $product->getPriceDiscountPercent() ?: $product->getdata('price_discount_percent ');
        $promoNewPrice = $product->getPromoNewPrice();

        if($priceDiscount || $priceDiscountPercent || $promoNewPrice) {
            $hasPromoValue = true;

            if ($priceDiscount) {
                if($priceDiscount < $item->getPriceInclTax()) {
                    $Discounted = -1 * abs($appliedPromoAmount);
                } else {
                    $Discounted = $priceDiscount;
                }
            } elseif ($priceDiscountPercent) {
                if(($priceDiscountPercent > 0) && ($priceDiscountPercent <= 100)) {
                    $Discounted = -1 * abs($appliedPromoAmount);
                }
            } elseif ($promoNewPrice) {
                $Discounted = $promoNewPrice;
            }
        }

        if(!$isBtw) {
            $Discounted = Mage::helper('tax')->getPrice($product, $Discounted, $isBtw);
        }

        $Discounted = $AfterDiscount ? $AfterDiscount : $Discounted;

        if ($hasPromoValue) {
            $price[] = $item->getProduct()->is(Vznl_Catalog_Model_Type::SUBTYPE_FIXED_PACKAGE_DETAILS) ?
                ('<label>' . Mage::helper('core')->currency($Discounted, true, false) . '</label>') : '<label></label>';
        } else {
            $price[] = '<label></label>';
        }

    }

    /**
     * Order applied promos in cart and extended cart in the order of selection in the stackable promos drawer
     * @param $promoTargetPackageId
     * @param $allItems
     */
    public function orderAppliedPromosBySelection($promoTargetPackageId, &$allItems)
    {
        /** @var Vznl_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();

        if ($promoTargetPackageId) {
            // reorder applied promos by their selection in the stackable promos drawer
            $packageModel = $quote->getCartPackage($promoTargetPackageId);
            $previousPromosProducts = ''; //json_decode($packageModel->getAppliedPromosOrder(), true);

            if (!empty($previousPromosProducts)) {
                foreach ($previousPromosProducts as $previousPromosProduct) {
                    foreach ($allItems as $keyItem => $unsortedItem) {
                        if ($unsortedItem->getProductId() == $previousPromosProduct) {
                            unset($allItems[$keyItem]);
                            $allItems[] = $unsortedItem;
                            break;
                        }
                    }
                }
            }
        }
    }

    /**
     * getPackageState
     * @param  $package
     * @return array
     */
    public function getPackageState($package)
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();

        $response = [
            'title' => null, // can be sharing-group | bundle | intra-stack-bundle | installed-base-bundle |disabled
            /*
             * sharing-group
             * intra-stack-bundle
             *
             */
            'state' => [
                'icon'  => null,
                'badge' => null // empty | empty darkcyan | darkcyan  (colors: darkcyan | darkorchid | darkslategray | hint | attachment)
            ]
        ];

        $packageModel = isset($package['package_id']) ? $quote->getCartPackage($package['package_id']) : null;
        if (empty($packageModel)) {
            $salesQuote = Mage::getModel('sales/quote')->load(Mage::getSingleton('customer/session')->getOrderEdit());
            $packageModel = isset($package['package_id']) ? $salesQuote->getCartPackage($package['package_id']) : null;
        }
        /** @var $packageCreationTypeModel Dyna_Package_Model_PackageCreationTypes */
        $packageCreationTypeModel = Mage::getSingleton('dyna_package/packageCreationTypes');

        if (isset($packageModel)) {

            $processContextHelper = Mage::helper("dyna_catalog/processContext");
            $processContextName = $processContextHelper->getNameByCode($packageModel->getSaleType());
            $response['tooltipInfo'] = array(
                'title' => isset($processContextName) ? $processContextName : '',
            );

            if (in_array($packageModel->getSaleType(), Dyna_Catalog_Model_ProcessContext::ILSProcessContexts())) {
                $response['state']['badge'] = 'ils-modify';
                $response['state']['icon'] = null;
                $response['tooltipInfo']['title'] = $this->__('Modify Package');
            }
            if (in_array($packageModel->getSaleType(), Dyna_Catalog_Model_ProcessContext::getProlongationProcessContexts())) {
                $response['state']['badge'] = 'ils-renew';
                $response['state']['icon'] = null;
                $response['tooltipInfo']['title'] = $this->__('Renew Package');
            }

            if ($packageModel->getSaleType() ==  Dyna_Catalog_Model_ProcessContext::MIGCOC) {
                $response['state']['badge'] = 'ils-change';
                $response['state']['icon'] = null;
                $packageCreationTypeFrontName = str_replace('_',' ',$packageCreationTypeModel->load($packageModel->getPackageCreationTypeId())->getPackageTypeCode());
                $response['tooltipInfo']['title'] = $this->__('Change service: ');
                $migrationSourcePackageCreationType = Mage::getSingleton('customer/session')->getSourcePackageCreationType();
                $migrationSourcePackageCreationTypeFrontName = $packageCreationTypeModel->getNameByPackageTypeCode($migrationSourcePackageCreationType);
                if ($packageCreationTypeFrontName == 'DSL') {
                    $response['state']['badge'] .= ' to-dsl';
                    $response['tooltipInfo']['title'] .= $this->__($migrationSourcePackageCreationTypeFrontName) .' to '.$packageCreationTypeFrontName;
                } elseif (strpos($packageCreationTypeFrontName,'Cable') !== false){
                    $response['state']['badge'] .= ' to-cable';
                    $response['tooltipInfo']['title'] .=$this->__($migrationSourcePackageCreationTypeFrontName) .' to '.$packageCreationTypeFrontName;
                } else {
                    $response['tooltipInfo']['title'] .=$this->__('PRE to POST');
                }
            }

            if ($packageModel->getSaleType() ==  Dyna_Catalog_Model_ProcessContext::MOVE) {
                $response['state']['badge'] = 'ils-change';
                $response['tooltipInfo'] = array(
                    'title' => $this->__('Move service')
                );
            }

            if ($packageModel->isPartOfMobileFixed()) {
                if(!empty($package['installed_base_products'])) {
                    $response['title'] = 'bundle';
                    $response['title'] .= ' disabled';
                } else {
                    $response['title'] = 'bundle default';
                }
            }
        }

        return $response;
    }

    /**
     * Returns the type of modal to be displayed when the user attempts to delete a package
     * @param $package
     * @return array
     */
    public function getDeleteModalType($package)
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $packageModel = isset($package['package_id']) ? $quote->getCartPackage($package['package_id']) : null;

        $result = [
            'modalType' => 'default',
            'bundleId' => null,
            'packageTitle' => $packageModel ? $packageModel->getTitle() : null,
            'dummyPackageTitle' => null,
            'modalMessage' => null,
        ];

        if ($packageModel && $packageModel->isPartOfMobileFixed()) {
            $bundledPackages = $quote->getCartPackagesInBundle($packageModel->getBundleOfType(Vznl_Bundles_Model_BundleRule::TYPE_MOBILEFIXED)->getId());

            foreach ($bundledPackages as $bundledPackage) {
                if ($bundledPackage->getId() !== $packageModel->getId()) {
                    $result['dummyPackageTitle'] = $bundledPackage->getTitle();
                    break;
                }
            }
            $result['modalType'] = 'alert-gigakombi-delete-modal';
            $result['bundleId'] = $packageModel->getBundleOfType(Vznl_Bundles_Model_BundleRule::TYPE_MOBILEFIXED)->getId();
            $result['modalMessage'] = $this->__('Changing or deleting the current package will break the bundle. Are you sure you want to continue?');

        }

        return $result;
    }

    /**
     * Remove package from configurator
     * @param $packageId
     * @param null $accepted
     * @param null $entityId
     * @return array
     * @throws Exception
     */
    public function removePackage($packageId, $accepted = null, $entityId = null)
    {
        // check if one of deal
        $isOneOfDeal = Mage::helper('omnius_configurator/cart')->isOneOfDealActive();
        /** @var Dyna_Checkout_Model_Cart $cart */
        $cart = Mage::getSingleton('checkout/cart');
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = $cart->getQuote();
        /** @var Dyna_Bundles_Helper_Data $bundlesHelper */
        $bundlesHelper = Mage::helper('dyna_bundles');

        /** @var Dyna_Package_Model_Package $packageModel */
        if ($entityId) {
            foreach ($quote->getCartPackages() as $packageModel) {
                if ($packageModel->getEntityId() == $entityId) {
                    break;
                }
            }
        } else {
            $packageModel = $quote->getCartPackage($packageId);
        }

        if(!isset($packageModel) || !$packageModel->getId()) {
            throw new Exception(sprintf($this->__('Could not find package %d for quote: %d'), $packageId, $quote->getId()));
        }

        /** Store deleted packages ids */
        $packageIds = array();

        /** @var Dyna_Bundles_Model_BundleRule $bundle */
        if ($bundle = $packageModel->getBundleOfType(Vznl_Bundles_Model_BundleRule::TYPE_MOBILEFIXED)) {
            /** @var Dyna_Package_Helper_Data $packageHelper */
            $packageHelper = Mage::helper('dyna_package');
            /** @var Dyna_Bundles_Helper_Data $bundleHelper */
            $bundleHelper = Mage::helper('dyna_bundles');
            foreach ($packageModel->getPackagesInSameBundle($bundle->getId(), true) as $otherBundlePackage) {

                $bundleHelper->removeBundlePackageEntry($bundle->getId(), $otherBundlePackage->getId());
                $otherBundlePackage->updateBundleTypes();
                $packageIds[] = $otherBundlePackage->getPackageId();
                $otherBundlePackage->delete();
            }

            Mage::getSingleton('customer/session')->setBundleRuleParserResponse(null);
            Mage::getSingleton('checkout/cart')->save();
        }

        $removeRetainables = false;
        $couponsToDecrement = array();
        if ($isOneOfDeal && $packageModel->getRetainable()) {
            if ($accepted != '1') {
                $this->jsonResponse(array(
                    'error'              => false,
                    'showOneOfDealPopup' => true
                ));
                return;
            }

            // Get all the packages
            $packagesModels = Mage::getModel('package/package')->getCollection()
                ->addFieldToFilter('quote_id', $quote->getId());

            foreach($packagesModels as $otherPackagesModel) {
                if($otherPackagesModel->getOneOfDeal()) {
                    $packageIds[] = $otherPackagesModel->getPackageId();
                    $couponsToDecrement[] = $otherPackagesModel->getCoupon();
                    $otherPackagesModel->delete();

                }
            }

            $removeRetainables = true;
        }

        $couponsToDecrement[] = $packageModel->getCoupon();
        // Before deleting, check if it is part of migration / move offnet flow
        if ($packageModel->getId() == Mage::getSingleton('checkout/session')->getMigrationPackage()) {
            Mage::helper('dyna_customer')->clearMigrationData();
        }

        $deletedPackageType = $packageModel->getType();

        // Remove the package from the packages table
        $packageIds[] = $packageModel->getPackageId();
        $packageModel->delete();

        $customerId = $quote->getCustomerId();
        foreach($couponsToDecrement as $coupon){
            Mage::helper('pricerules')->decrementCoupon($coupon, $customerId);
        }

        $packageIds = array_unique($packageIds);
        if (in_array((int) $quote->getActivePackageId(), $packageIds)) {
            $quote->setActivePackageId(false);
        }

        $packagesIndex = Mage::helper('dyna_configurator/cart')->reindexPackagesIds($quote);

        /** @var Dyna_Configurator_Helper_Rules $rulesHelper */
        $rulesHelper = Mage::helper('dyna_configurator/rules');
        $remainingCablePackage = "";
        if ($packagesIndex && $rulesHelper->checkGateWay($deletedPackageType)) {
            if (!$quote->getCartPackages()) {
                $rulesHelper->cacheSave(null);
            }
            /** @var Dyna_Package_Model_Package $package */
            foreach ($quote->getCartPackages() as $packageId => $package) {
                if ($rulesHelper->checkGateWay($package->getType())) {
                    $remainingCablePackage = $package->getType();
                    /*****************************
                     * SPECIAL HANDLING FOR CABLE
                     ******************************/
                    // Setting this package as active on quote and collect_totals_after event will execute cable post condition rules for it
                    $quote
                        ->setActivePackageId($packageId)
                        ->setTotalsCollectedFlag(false)
                        ->setSkipCablePostConditions(false);
                    // Make sure passes collect totals after (restriction exists in observer that requires addMulti origin for cable post condition rules to be processed)
                    Mage::register('product_add_origin', 'addMulti');
                    // Saving cart will trigger the post condition rules
                    // No need to save cart here, controller or who ever calls this remove package method should handle cart save
                    break;
                }
            }
        }

        return array(
            'packagesIndex'         => $packagesIndex,
            'removeRetainables'     => $removeRetainables,
            'packageIds'            => $packageIds,
            'remainingCablePackage' => $remainingCablePackage,
            'deletedPackageType'    => $deletedPackageType
        );
    }

    /**
     * Method to check if during bundle breaking the other package should be removed or emptied
     *
     * @param $package
     * @param $activePackageId
     * @return bool
     */
    public static function shouldRemovePackage($package, $activePackageId)
    {
        $isActivePackage = $package->getPackageId() == $activePackageId;
        $isFixedMoveOrIls = $package->isFixed() && ($package->isIls() || $package->isMove());
        $isMobileRetOrIls = $package->isMobile() && ($package->isRetention() || $package->isILS());

        return !$isActivePackage && ($isFixedMoveOrIls || $isMobileRetOrIls);
    }

    /**
     * Get title of package type and subtype
     * @param Dyna_Package_Model_Package $package
     * @param $packageType
     * @param string $guiSection
     * @return array
     */
    public function getTypeAndSubtypeTitle($package, $packageType, $guiSection = '')
    {
        if( !isset($package['package_creation_type_id'])) {
            $package['package_creation_type_id'] = 0;
        }
        if( !isset($package['type'])) {
            $package['type'] = Vznl_Catalog_Model_Type::TYPE_FIXED;
        }

        $packageTitle = null;
        $packageCreationType = Mage::getSingleton('dyna_package/packageCreationTypes')->getById($package['package_creation_type_id']);
        if ($packageCreationType) {
            $packageTitle = $packageCreationType->getGuiName();
            $packageCreationGroupId = $packageCreationType->getPackageCreationGroupId();
            if ($packageCreationGroupId > 0) {
                $packageCreationGroupName = Mage::getSingleton('dyna_package/packageCreationGroups')
                    ->getById($packageCreationGroupId)
                    ->getGuiName()
                ;
                if ($packageCreationGroupName) {
                    $packageTitle = trim($packageCreationGroupName.' '.$packageTitle);
                }
            }
        }

        $packageSubtypeTitle = null;
        $productType = strtoupper($package['type']) === Vznl_Catalog_Model_Type::TYPE_FIXED ? Vznl_Catalog_Model_Type::SUBTYPE_FIXED_PACKAGE : Dyna_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION;
        if (is_array($package['items'])) {
            foreach ($package['items'] as $item) {
                if (!in_array($productType, $item->getProduct()->getType())) {
                    continue;
                }
                $packageSubtypeTitle = $item->getName();
                break;
            }
            if ($packageSubtypeTitle === null && count($package['items']) > 0) {
                $items = array_reverse($package['items']);
                $firstItem = array_pop($items);
                if ($firstItem) {
                    $packageSubtypeTitle = $firstItem->getName();
                }
            }
        }

        return [
            'packageSubtypeTitle' => $packageSubtypeTitle,
            'packageTitle' => $packageTitle
        ];
    }
}
