<?php

require_once Mage::getModuleDir('controllers', 'Dyna_Package') . DS . '/Adminhtml/PackageController.php';

/**
 * Class Vznl_Package_Adminhtml_PackageController
 */
class Vznl_Package_Adminhtml_PackageController extends Dyna_Package_Adminhtml_PackageController
{
    public function editAction()
    {
        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("package/packageSubtype")->load($id);
        Mage::register("package_model_configuration", $model);

        if ($model->getId()) {
            $this->_title($this->__("Package subtype"));
            $this->_title($this->__("Edit package subtype configuration"));

            $this->_initAction();
            $this->renderLayout();
        } else {
            $this->_initAction();
            $this->renderLayout();
        }
    }
}