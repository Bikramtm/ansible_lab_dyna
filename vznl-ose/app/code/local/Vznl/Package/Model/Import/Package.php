<?php

/**
 * Class Vznl_Package_Model_Import_Package
 */
class Vznl_Package_Model_Import_Package extends Dyna_Package_Model_Import_Package
{
    /** @var string $_logFileExtension */
    protected $_logFileExtension = "log";
    /** @var string $_logFileName */
    protected $_logFileName = "package_type_import";

    public function __construct()
    {
        parent::__construct();
        /** @var Vznl_Import_Helper_Data _helper */
        $this->_helper = Mage::helper("vznl_import");
    }

    /**
     * @param array $data
     * @return void
     */
    public function importPackage($data)
    {
        $data = array_map("trim", $data);
        try {
            $data = array_combine($this->_header, $data);
            $data['stack'] = $this->stack;

            // Let's make use of a Varien_Object
            /**
             * @method getPackageSubtypeName()
             */
            $importData = new Varien_Object();
            $importData->setData($data);

            $packageTypeCode = $importData->getPackageTypeCode();
            if (empty($this->packageTypes[$packageTypeCode])) {
                // Let's try loading it
                /** @var Dyna_Package_Model_PackageType $packageTypeModel */
                $packageTypeModel = Mage::getModel('dyna_package/packageType')
                    ->getCollection()
                    ->addFieldToFilter('package_code', ['eq' => $packageTypeCode])
                    ->setPageSize(1, 1)
                    ->getLastItem();
                if (!$packageTypeModel || !$packageTypeModel->getId()) {
                    $packageTypeModel = Mage::getModel('dyna_package/packageType');
                }
                // If exists, update its data
                $packageTypeModel
                    ->setFrontEndName($importData->getPackageTypeName())
                    ->setPackageCode($packageTypeCode)
                    ->setLifecycleStatus(in_array($importData->getLifecycleStatus(), Dyna_Catalog_Model_Lifecycle::getLifecycleTypes()) ? $importData->getLifecycleStatus() : null )
                    ->setStack($importData->getStack())
                    ->save();

                $this->packageTypes[$packageTypeCode] = $packageTypeModel;
            } else {
                $packageTypeModel = $this->packageTypes[$packageTypeCode];
            }

            // Try loading package subtype by code
            /** @var Dyna_Package_Model_PackageSubtype $packageSubTypeModel */
            $packageSubTypeModel = Mage::getModel('dyna_package/packageSubtype')
                ->getCollection()
                ->addFieldToFilter('package_code', ['eq' => $importData->getPackageSubtypeCode()])
                ->addFieldToFilter('type_package_id', ['eq' => $packageTypeModel->getId()])
                ->setPageSize(1, 1)
                ->getLastItem();
            if (!$packageTypeModel || !$packageTypeModel->getId()) {
                $packageSubTypeModel = Mage::getModel('package/configuration');
            }

            // Set data
            $packageSubTypeModel
                ->setTypePackageId($packageTypeModel->getId())
                ->setPackageCode($importData->getPackageSubtypeCode())
                ->setFrontEndName($importData->getPackageSubtypeName())
                ->setCardinality($importData->getCardinality())
                ->setIgnoredByCompatibilityRules(strtolower($importData->getIgnoredByCompatibilityRules()) == 'yes' ? 1 : 0);

            $cardinality = mb_convert_encoding($packageSubTypeModel->getCardinality(), "UTF-8");
            // Dot sign in example files has an invalid character converted to question sign
            // Doesn't matter how many points are in cardinality, they will be trimmed
            $cardinality = str_replace("?", "...", $cardinality);
            $packageSubTypeModel->setCardinality($cardinality);

            // Set visibility
            $packageSubTypeModel->setPackageSubtypeVisibility($this->parseProductVisibility($importData->getPackageSubtypeVisibility()));
            // Set position
            $packageSubTypeModel->setFrontEndPosition(
            // If no position defined, set the max (least) one
                (int)$importData->getPackageSubtypePosition() ?: 10
            );

            $packageSubTypeModel->setManualOverrideAllowed($importData->getManualOverrideAllowed() != null && trim($importData->getManualOverrideAllowed()) == 'yes' ? 1 : 0);
            $packageSubTypeModel->setPackageSubtypeLifecycleStatus(in_array($importData->getPackageSubtypeLifecycleStatus(), Dyna_Catalog_Model_Lifecycle::getLifecycleTypes()) ? $importData->getPackageSubtypeLifecycleStatus() : null );
            $packageSubTypeModel->setPackageSubtypePosition($packageSubTypeModel->getPackageSubtypeGuiSorting());
            $packageSubTypeModel->setStack($importData->getStack());
            $packageSubTypeModel->save();

            // save the cardinality of the subtype per process context
            $subtypeId = $packageSubTypeModel->getId();

            $cardinalityContexts = $this->parseContextCardinality($importData);

            foreach ($cardinalityContexts as $context) {
                $subtypeContextCardinality = Mage::getModel('dyna_package/packageSubtypeCardinality');
                $subtypeContextCardinality
                    ->setData('package_subtype_id', $subtypeId)
                    ->setData('process_context_id', $context['context_id'])
                    ->setData('cardinality', $context['cardinality'])
                    ->setData('stack', $importData->getStack());

                $subtypeContextCardinality->save();
            }


            $this->_totalFileRows++;
            $this->_log('Finished importing row: ' . $packageTypeModel->getFrontEndName() . " >> " . $packageSubTypeModel->getFrontEndName(), true);
        } catch (Exception $ex) {
            $this->_skippedFileRows++;
            $this->_logEx($ex);
            fwrite(STDERR, 'Skipped file because of '.$ex->getMessage());
        }
    }

    /**
     * Method to parse the value of the package subtype visibility
     * @param string $values
     * @return string
     */
    private function parseProductVisibility($values)
    {
        $markedOptions = [];
        $markedOptionIds = [];
        $allVisibilityOptionsArray = array();

        $attribute = Mage::getSingleton('eav/config')
            ->getAttribute(Mage_Catalog_Model_Product::ENTITY,
                Dyna_Catalog_Model_Product::CATALOG_PACKAGE_PRODUCT_VISIBILITY_ATTR);

        if ($attribute->usesSource()) {
            $allVisibilityOptionsArray = array_map(function ($value) {
                return strtolower(str_replace(' ', '_', trim($value)));
            }, array_column($attribute->getSource()->getAllOptions(false), 'label'));
        }

        if ('*' == $values) {
            $markedOptionIds = $allVisibilityOptionsArray;
        } else {
            if (trim($values)) {
                $values = explode(',', $values);
                foreach ($values as $value) {
                    $markedOptions[] = strtolower(str_replace(' ', '_', trim($value)));
                }
                $flippedAllVisibilityOptionsArray = array_flip($allVisibilityOptionsArray);
                foreach ($markedOptions as $option) {
                    if (isset($flippedAllVisibilityOptionsArray[$option])) {
                        $markedOptionIds[] = $option;
                    } else {
                        $this->_logError('[Warning] package_subtype_visibility value "' . $option . '" cannot be mapped for row no: ' . $this->_totalFileRows);
                        continue;
                    }
                }
            }
        }

        return (!$markedOptionIds) ? null : implode(',', $markedOptionIds);
    }

    /**
     * Method to parse the package subtype cardinality for
     * each existing process context
     *
     * @param Varien_Object $values
     * @return array
     */
    private function parseContextCardinality($values)
    {
        /** @var Dyna_Catalog_Model_ProcessContext $contextModel */
        $contextModel = Mage::getModel('dyna_catalog/processContext');
        $subtypeCardinality = array();

        $allProcessContexts = $contextModel->getAllProcessContextCodeIdPairs();
        foreach ($allProcessContexts as $context => $id) {
            $subtypeCardinality[] = [
                'context_id' => $id,
                'cardinality' => $values->getData(strtolower($context) . '_cardinality') ?? null
            ];
        }

        return $subtypeCardinality;
    }

    /**
     * Get log file name for single file import
     * @return string
     */
    public function getFileLogName()
    {
        return strtolower($this->stack) . '_' . $this->_logFileName . '.' . $this->_logFileExtension;
    }
}