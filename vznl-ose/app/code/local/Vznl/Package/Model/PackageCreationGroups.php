<?php

/**
 * Class Vznl_Package_Model_PackageCreationGroups
 */
class Vznl_Package_Model_PackageCreationGroups extends Dyna_Package_Model_PackageCreationGroups
{
    /**
     * Get all package creation groups with the package types
     * - first they are ordered by package_creation_groups.sorting
     * - secondly they are ordered by package_creation_types.sorting
     *
     * @return array
     */
    public function getPackageCreationGroupsWithPackageTypesOrdered()
    {
        $packageGroups = $this->getCollection()->getSelect()->order('main_table.sorting', 'ASC')->query()->fetchAll();
        /** @var Dyna_Package_Model_PackageCreationTypes $model */
        $model = Mage::getModel('dyna_package/packageCreationTypes');
        /** @var Vznl_Agent_Helper_Data $agentHelper */
        $agentHelper = Mage::helper('agent');
        $agent = Mage::getSingleton('customer/session')->getAgent();
        $hasFixedPermission = $agent->isGranted(Vznl_Agent_Model_Agent::ALLOW_ZIGGO);
        $hasMobilePermission = $agent->isGranted(Vznl_Agent_Model_Agent::ALLOW_MOBILE);
        $flippedPackagesAvailableForIndirectStore = array_flip(Vznl_Catalog_Model_Type::packagesAvailableForIndirectStore());
        $flippedFixedPackages = array_flip(Vznl_Catalog_Model_Type::getFixedPackages());
        $flippedMobilePackages = array_flip(Vznl_Catalog_Model_Type::getMobilePackages());
        foreach ($packageGroups as $groupKey => &$group) {
            $group['package_creation_types'][] = [];
            $packageCreationTypes = $model->getPackageCreationTypesByGroup($group['entity_id']);
            foreach ($packageCreationTypes as $packageKey => &$creationType) {
                $targetPackage = Mage::getModel('package/packageType')
                    ->getCollection()
                    ->addFieldToFilter('entity_id', $creationType['package_type_id'])
                    ->setPageSize(1, 1)
                    ->getLastItem();
                
                //Check if the given package code is allowed for indirect channel
                $packageCodeLowerCase = strtolower($targetPackage->getPackageCode());
                if ($agentHelper->isIndirectStore() && !isset($flippedPackagesAvailableForIndirectStore[$packageCodeLowerCase])) {
                    unset($packageCreationTypes[$packageKey]);
                }

                //Check if the agent has permission to see ziggo products
                if(!$hasFixedPermission && isset($flippedFixedPackages[$packageCodeLowerCase])) {
                    unset($packageCreationTypes[$packageKey]);
                }

                //Check if the agent has permission to see Vodafone products
                if(!$hasMobilePermission && isset($flippedMobilePackages[$packageCodeLowerCase])) {
                    unset($packageCreationTypes[$packageKey]);
                }


                $creationType['target_package'] = $targetPackage->getPackageCode();
            }

            if (empty($packageCreationTypes)) {
                unset($packageGroups[$groupKey]);
                continue;
            } else {
                $group['package_creation_types'] = $packageCreationTypes;
            }
        }
        return $packageGroups;
    }
}