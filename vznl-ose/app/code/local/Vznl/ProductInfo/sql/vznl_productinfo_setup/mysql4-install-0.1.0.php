<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */
require_once Mage::getBaseDir('base') . DS . 'shell'. DS . 'repairDbOrders.php';
require_once Mage::getBaseDir('base') . DS . 'shell'. DS . 'repairDbQuotes.php';
$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();
$tableName = $installer->getTable('productinfo/info');
if (!$installer->tableExists($tableName)) {
    $table = $installer->getConnection()
        ->newTable($tableName)
        ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity' => true,
            'unsigned' => true,
            'nullable' => false,
            'primary' => true,
        ), 'ID')
        ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 50, array(), 'Product Entity Id')
        ->addColumn('sku', Varien_Db_Ddl_Table::TYPE_VARCHAR, 100, array(), 'SKU')
        ->addIndex($installer->getIdxName($installer->getTable($tableName), array('sku'), Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE),
            array('sku'), array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE))
        ->setComment('catalog_product_info SKU');
    $installer->getConnection()->createTable($table);

    /** Insert product information */
    $productCollection = Mage::getModel('catalog/product')->getCollection()->addAttributeToSelect(array('entity_id', 'sku'));
    foreach ($productCollection->getData() as $data) {
        $installer->getConnection()->insert($tableName, array('entity_id' => $data['entity_id'], 'sku' => $data['sku']));
    }

}

/*Repair order item*/
$orders = new Tools_Db_Repair_Orders();
$orders->setReadOnly(false);
$orders->run();

/*Repair quote item*/
$quotes = new Tools_Db_Repair_Quotes();
$quotes->setReadOnly(false);
$quotes->run();

$installer->endSetup();

