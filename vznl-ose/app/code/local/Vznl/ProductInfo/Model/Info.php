<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Vznl_ProductInfo_Model_Info extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init("productinfo/info");
    }

    public function loadBySku($sku)
    {
        $product = $this->getCollection();
        $product->addFieldToFilter('sku', $sku)->getSelect()->limit(1);
        return $product;

    }
}
