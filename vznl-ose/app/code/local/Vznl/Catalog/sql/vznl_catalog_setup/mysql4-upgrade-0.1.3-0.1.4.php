<?php
// Create hide_original_price attribute
// (used to overwrite original price of products targeted by sales rules which bring a promo product)

/* @var $installer Dyna_Cable_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

// Update attributes
$attributeCode = 'hide_original_price';
$attributeOptions = [
    'group' => 'General',
    'input' => 'select',
    'source' => 'eav/entity_attribute_source_boolean',
    'type' => 'int',
    'label' => 'Hide original price',
    'visible' => true,
    'required' => false,
    'visible_on_front' => true,
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE
];

$attributeId = $installer->getAttributeId($entityTypeId, $attributeCode);

if (!$attributeId) {
    $installer->addAttribute($entityTypeId, $attributeCode, $attributeOptions);
}

$installer->endSetup();
