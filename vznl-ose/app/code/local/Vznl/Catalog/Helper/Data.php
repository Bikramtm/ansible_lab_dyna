<?php

/**
 * Class Vznl_Catalog_Helper_Data
 */
class Vznl_Catalog_Helper_Data extends Dyna_Catalog_Helper_Data
{
    /* ---------- StockStatus Text ----------------------------------------------------------------------------------- */
    const STOCK_TEXT_INSTOCK = 'In stock';
    const STOCK_TEXT_LIMITED = 'Limited stock';
    const STOCK_TEXT_OTHERSTORE = 'Deliverable in other store(s)';
    const STOCK_TEXT_BACKORDER = 'Available through backorder';
    const STOCK_TEXT_COMINGSOON = 'New (coming Soon)';
    const STOCK_TEXT_SOLDOUT = 'Sold Out';
    const STOCK_TEXT_ENDOFLIFE = 'End of life';
    const STOCK_TEXT_LONGDELIVERYTIME = 'Long delivery time';

    /* ---------- StockStatus Class ---------------------------------------------------------------------------------- */
    const STOCK_CLASS_INSTOCK = 'sellable-voorradig';
    const STOCK_CLASS_LIMITED = 'end-of-sale-op-op';
    const STOCK_CLASS_OTHERSTORE = 'sellable-otherstore';
    const STOCK_CLASS_BACKORDER = 'sellable-backorder';
    const STOCK_CLASS_COMINGSOON = 'new-coming-soon';
    const STOCK_CLASS_SOLDOUT = 'sellable-uitverkocht';
    const STOCK_CLASS_ENDOFLIFE = 'end-of-life-niet-meer-leverbaar';

    /**
     * Checks if a product is sellable.
     * @param $stock The amount of stock
     * @param $backOrders The amount of back orders possible
     * @param Vznl_Catalog_Model_Product $product The product to check
     * @return bool <true> if sellable, <false> if not.
     */
    public function checkIfSellable($stock, $backOrders, $product)
    {
        $stock = (int) $stock;
        $backOrders = (int) $backOrders;

        $stockStatus = $product->getStockStatus();
        $threshold = $product->getAxiThreshold();

        $stockAttribute = $this->getProductStockAttributeValue($product);
        $stock_status_array = $this->getStockStatusArray();
        $stockStatusClass = isset($stock_status_array[$stockStatus]) ? $stock_status_array[$stockStatus] : '';

        $returnValue = false;
        if ($this->isDevMode()) {
            $product->setStockStatusText($this->__($stockAttribute));
            $product->setStockStatusClass($stockStatusClass);
            $returnValue = true;
        }

        if (!$returnValue) {
            $returnValue = $this->updateStockLabelForHasStock($product, $stockStatusClass, $stockAttribute, $stock);
        }

        if (!$returnValue) {
            $returnValue = $this->updateStockLabelForBackOrderAble($product, $stockStatusClass, $stockAttribute, $backOrders, $threshold);
        }

        if (!$returnValue) {
            $returnValue = $this->updateStockLabelForSellableOtherStore($product);
        }

        if (!$returnValue) {
            $returnValue = $this->updateStockLabelForOther($product, $stockStatusClass, $stockAttribute);
        }

        return $returnValue;
    }

    /**
     * Checks whether dev mode is used
     * @return bool <true> means dev mode is on
     */
    protected function isDevMode()
    {
        return (Mage::getStoreConfig('general/store_information/dev_mode') != 0);
    }

    /**
     * Gets product stock attribute value for the given product.
     * @param Vznl_Catalog_Model_Product $product The product
     * @return string The stock attribute value.
     */
    protected function getProductStockAttributeValue($product)
    {
        if (!($stockAttribute = Mage::registry('catalog_product_stock_attr'))) {
            $stockAttribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', 'stock_status');
            Mage::unregister('catalog_product_stock_attr');
            Mage::register('catalog_product_stock_attr', $stockAttribute);
        }

        return $stockAttribute->getFrontend()->getValue($product);
    }

    /**
     * Gets an array with the stock statuses as key and class to use as value.
     * @return array The array.
     */
    protected function getStockStatusArray()
    {
        if (!($stock_status_array = Mage::registry('catalog_product_stock_status_arr'))) {
            $stock_status_array = Mage::helper('dyna_catalog')->getStockStatusSlugs();
            Mage::unregister('catalog_product_stock_status_arr');
            Mage::register('catalog_product_stock_status_arr', $stock_status_array);
        }

        return $stock_status_array;
    }
    
    /**
     * Checks if product is in network
     * @return bool
     */
    public function checkProductIsNetwork($item, $network = Vznl_Configurator_Model_AttributeSet::ATTRIBUTE_NETWORK_VODAFONE)
    {
        $product = $item->getProduct();
        if ($attr = $product->getResource()->getAttribute(Vznl_Configurator_Model_AttributeSet::ATTRIBUTE_NETWORK)) {
            if (strtolower($network) == strtolower($attr->getFrontend()->getValue($product))) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if the product has stock and updates the stock status, text and class.
     * @param Vznl_Catalog_Model_Product $product The product.
     * @param $stockStatusClass The stock status class.
     * @param $stockAttributeValue The stock attribute value.
     * @param $stock The amount of stock.
     * @return bool <true> means that the stock was sufficient and the order is sellable.
     */
    protected function updateStockLabelForHasStock($product, $stockStatusClass, $stockAttributeValue, $stock)
    {
        if ($stock > 0) {

            $this->setStockStatusTextAndClass($product, self::STOCK_CLASS_INSTOCK, self::STOCK_TEXT_INSTOCK, self::STOCK_CLASS_INSTOCK);

            return true;
        }
        return false;
    }

    /**
     * Checks if the product can still be back ordered and updates the stock status, text and class.
     * @param Vznl_Catalog_Model_Product $product The product.
     * @param $stockStatusClass The stock status class.
     * @param $stockAttributeValue The stock attribute value.
     * @param $backOrders The amount of back orders done.
     * @return bool <true> means that the amount of back orders did not exceed the threshold and the product is sellable.
     */
    protected function updateStockLabelForBackOrderAble($product, $stockStatusClass, $stockAttributeValue, $backOrders, $threshold)
    {
        if ($backOrders < $threshold) {
            switch ($stockStatusClass) {
                case self::STOCK_CLASS_INSTOCK:
                    $this->setStockStatusTextAndClass($product, self::STOCK_CLASS_BACKORDER, self::STOCK_TEXT_LONGDELIVERYTIME, self::STOCK_CLASS_BACKORDER);
                    break;
                case self::STOCK_CLASS_COMINGSOON:
                    $this->setStockStatusTextAndClass($product, null, $stockAttributeValue, $stockStatusClass);
                    break;
                default:
                    return false;
            }
            return true;
        }
        return false;
    }

    /**
     * Checks if the product is sellable in another store and updates the stock status, text and class.
     * @param Vznl_Catalog_Model_Product $product The product.
     * @return bool <true> means that the stock is sufficient in another store and the order is sellable.
     */
    protected function updateStockLabelForSellableOtherStore($product)
    {
        $axiStoreCode = $this->getAxiStoreId();
        $storeId = $this->getStoreId();
        if ($this->isSellableInOtherStore($product, $axiStoreCode, $storeId)) {
            $this->setStockStatusTextAndClass($product, self::STOCK_CLASS_OTHERSTORE, self::STOCK_TEXT_OTHERSTORE, self::STOCK_CLASS_OTHERSTORE);
            return true;
        }
        return false;
    }

    /**
     * Updates the stock status, text and class.
     * @param Vznl_Catalog_Model_Product $product The product.
     * @param $stockStatusClass The stock status class.
     * @param $stockAttributeValue The stock attribute value.
     * @return bool <true> means that the stock was sufficient and the order is sellable.
     */
    protected function updateStockLabelForOther($product, $stockStatusClass, $stockAttributeValue)
    {
        switch ($stockStatusClass) {
            case self::STOCK_CLASS_INSTOCK:
            case self::STOCK_CLASS_LIMITED:
            case self::STOCK_CLASS_COMINGSOON:
                $this->setStockStatusTextAndClass($product, self::STOCK_CLASS_SOLDOUT, self::STOCK_TEXT_SOLDOUT, self::STOCK_CLASS_SOLDOUT);
                break;
            default:
                $this->setStockStatusTextAndClass($product, null, $stockAttributeValue, $stockStatusClass);
                break;
        }
        return false;
    }

    /**
     * Updates the given variables on the product.
     * @param Vznl_Catalog_Model_Product $product The product.
     * @param null|string $stockStatus The stock status, if not <null> this will be set as new stock status.
     * @param null|string $stockText The stock text, if not <null> this will be set as new stock text.
     * @param null|string $stockClass The stock class, if not <null> this will be set as new stock class.
     */
    protected function setStockStatusTextAndClass($product, $stockStatus = null, $stockText = null, $stockClass = null)
    {
        if (!is_null($stockStatus)) {
            $product->setStockStatus($stockStatus);
        }
        if (!is_null($stockText)) {
            $product->setStockStatusText($this->__($stockText));
        }
        if (!is_null($stockClass)) {
            $product->setStockStatusClass($stockClass);
        }
    }

    /**
     * Gets if the product is sellable in other stores than the given store codes
     * @param Vznl_Catalog_Model_Product $product The product
     * @param $axiStoreCode The axi store code
     * @param $storeId The store id
     * @return bool <true> is sellable and has stock, <false> is not sellable and no stock
     */
    public function isSellableInOtherStore($product, $axiStoreCode, $storeId)
    {
        /** @var Vznl_Stock_Helper_Data $stockHelper */
        $stockHelper = Mage::helper('stock');
        $stock = $stockHelper->getStockOtherStores($product->getEntityId(), $axiStoreCode, $storeId);
        return $stock > 0;
    }

    /**
     * Get the current axi store id
     * @return int The axi store id
     */
    protected function getStoreId()
    {
        $customerSession = Mage::getSingleton('customer/session');
        $storeId = $customerSession->getAgent() && $customerSession->getAgent()->getDealer() ? $customerSession->getAgent()->getDealer()->getStoreId() : Mage::app()->getStore()->getId();
        return $storeId;
    }

    /**
     * Get the store id of the current agent
     * @return int The store id
     */
    protected function getAxiStoreId()
    {
        return Mage::helper('agent')->getCurrentStoreId();
    }

    /**
     * Checks if package contains a vodafone subscription
     * @param null $package
     * @return bool
     */
    public function packageContainsVfDaSubscription($package = null)
    {
        if (!$this->packageContains(Vznl_Configurator_Model_AttributeSet::ATTRIBUTE_NETWORK, Vznl_Configurator_Model_AttributeSet::ATTRIBUTE_NETWORK_VODAFONE, $package)) {
            return false;
        }

        if (!$package) {
            foreach (Mage::getSingleton('checkout/session')->getQuote()->getPackages() as $package) {
                if ($this->packageContainsVfDaSubscription($package)) {
                    return true;
                }
            }
        } else {
            foreach ($package['items'] as $item) {
                /** @var Vznl_Catalog_Model_Product $product */
                $product = $item->getProduct();
                if ($product->isSubscription()) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @param $package
     * @return bool
     */
    public function packageContainsHollandsNieuweSubscription($package)
    {
        return $this->packageContains(
            Vznl_Configurator_Model_AttributeSet::ATTRIBUTE_NETWORK,
            Vznl_Configurator_Model_AttributeSet::ATTRIBUTE_NETWORK_HOLLANDSNIEUWE,
            $package
        );
    }

    /**
     * Checks if the given package contains a product with
     * the given value for the given attribute
     *
     * @param $attributeName
     * @param $value
     * @param array $package
     * @return bool
     */
    public function packageContains($attributeName, $value, array $package = null)
    {
        if (!$package) {
            $quote = Mage::getSingleton('checkout/session')->getQuote();
            $key = sprintf('package_contains_%s', md5(serialize([$attributeName, $value, $quote->getUpdatedAt()])));
            if ($quote->hasData($key)) {
                return $quote->getData($key);
            } else {
                foreach ($quote->getPackages() as $package) {
                    if ($this->packageContains($attributeName, $value, $package)) {
                        return true;
                    }
                }
            }
        } else {
            foreach ($package['items'] as $item) {
                $product = $item->getProduct();
                if ($product->hasAttributeWithValue($attributeName, $value)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Gets the possible values for prodspecs_brand attribute.
     * @return array The list with all possible values for the prodspecs_brand attribute.
     */
    public function getPossibleProdspecsBrandValues()
    {
        $query = 'select value from eav_attribute_option_value where option_id in (
                    select option_id from eav_attribute_option where attribute_id in (
		              select attribute_id from eav_attribute where attribute_code = \'prodspecs_brand\'
                    )
                  );';
        $options = Mage::getSingleton('core/resource')
            ->getConnection('core_read')
            ->fetchAll($query);

        // We only need the value, so lets map it
        $options = array_map(
            function($data) {
                if (isset($data['value'])) {
                    return $data['value'];
                }
            },
            $options ? : []
        );
        return $options;
    }

    /**
     * Do not take into consideration Wettelijke Thuiskopieheffing addon
     * @param $productIds
     * @return mixed
     */
    public static function skipLevyProduct($productIds)
    {
        $levyAddonId = Mage::getResourceSingleton('catalog/product')->getIdBySku(Mage::getStoreConfig(Dyna_PriceRules_Model_Observer::COPY_LEVY_PREFIX . 'standaard'));
        foreach ($productIds as $key => $product) {
            if ($product == $levyAddonId) {
                unset($productIds[$key]);
                break;
            }
        }
        return $productIds;
    }

    /**
     * Get VAT tax class
     * @param string $vatValue
     * @return string
     */
    public function getVatTaxClass($vatValue)
    {
        $vatTaxClass = '';

        switch ($vatValue) {
            case '21':
                $vatTaxClass = 'TAX_21';
                break;
            case '19':
                $vatTaxClass = 'TAX_19';
                break;
            case '6':
                $vatTaxClass = 'TAX_6';
                break;
            case '0':
                $vatTaxClass = 'TAX_0';
                break;
        }

        return $vatTaxClass;
    }

    /**
     * Get array of all mutually exclusive products [entity_id (category) => product_id]
     * if reverse is true returns array of categories for each product
     */
    public function getMutuallyExclusiveProducts()
    {
        $cacheKey = md5("all_mutually_exclusive_products");
        if (!($result = unserialize($this->getCache()->load($cacheKey)))) {
            $result = [];

            /** @var Vznl_Catalog_Model_Category $categories */
            $categories = Mage::getModel('vznl_catalog/category')
                ->getCollection()
                ->addAttributeToFilter('is_family', 1)
                ->addAttributeToFilter('allow_mutual_products', 1)
                ->load();

            foreach ($categories as $category) {
                // get category's products
                $products = Mage::helper('dyna_productmatchrule')->getProductIdsAndSkus($category->getEntityId());
                if (!empty($products)) {
                    $result[$category->getEntityId()] = $products;
                }
                unset($products);
            }

            $this->getCache()->save(serialize($result), $cacheKey, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
        }

        return $result;
    }

    /**
     * Return array with a list of mutual excl categories for each product
     * @return array
     */
    public function getMutuallyExclusiveCategories()
    {
        $products = [];
        $categories = $this->getMutuallyExclusiveProducts();
        foreach($categories as $category){
            foreach($category as $categoryId => $categoruProductSku){
                if(!isset($products[$categoruProductSku])){
                    $products[$categoruProductSku] = [];
                }
                $products[$categoruProductSku][] =  $categoryId;
            }
        }

        return $products;
    }

    /**
     * Get package type id of current quote active package
     * @return mixed|null
     */
    public function getPackageTypeId()
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $packageTypeId = null;

        if ($activePackage = $quote->getCartPackage()) {
            /** @var Dyna_Package_Model_PackageType $packageModel */
            $packageModel = Mage::getModel('dyna_package/packageType');

            $packageTypeId = $packageModel->getPackageTypeIdByCode($activePackage->getType());
        }

        return $packageTypeId;
    }

    /**
     * @param string $sku
     * @param bool $retina
     * @throws Exception
     */
    public function getThumbnailBySku(string $sku, bool $retina = false): string
    {
        if ($retina === false) {
            return $this->_getThumbnailPath($sku);
        }
        $thumbnails = [];
        for ($size = 1; $size <= 3; $size++) {
            try {
                $path = $this->_getThumbnailPath($sku, $size);
            } catch (Exception $exception) {
                continue;
            }
            $thumbnails[] = $path.' '.$size.'x';
        }
        
        return implode(', ', $thumbnails);
    }

    /**
     * @param string $sku
     * @param int $size
     * @throws Exception
     */
    private function _getThumbnailPath(string $sku, int $size = 1): string
    {
        foreach (['png', 'jpg'] as $extension) {
            $path = '/product_image/thumbnail'.($size > 1 ? $size.'x' : '').'/'.$sku.'.'.$extension;
            if (!file_exists(Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA).$path)) {
                continue;
            }

            return str_replace(DS.$path, $path, Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . $path);
        }

        throw new Exception('Invalid thumbnail path: '.$path);
    }
}
