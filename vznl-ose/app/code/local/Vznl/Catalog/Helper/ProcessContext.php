<?php

/**
 * Class Vznl_Catalog_Helper_ProcessContext
 */
class Vznl_Catalog_Helper_ProcessContext extends Dyna_Catalog_Helper_ProcessContext
{
    /**
     * @param $currentPackage
     * @return $processContextIds
     */
    public function getProcessContextSetIds($currentPackage = false)
    {
        // if there are multiple packages in cart, and a package is specified, use that instead of active package
        if ($currentPackage && $currentPackage instanceof Vznl_Package_Model_Package) {
            $processContextPackage = $currentPackage;
        } else {
            /** @var Dyna_Checkout_Model_Sales_Quote $quote */
            $quote = Mage::getSingleton('checkout/cart')->getQuote();
            $processContextPackage = $quote->getCartPackage();
        }

        if (!$processContextPackage) {
            return null;
        }

        if (!isset($this->processContextIds[$processContextPackage->getEntityId()])) {
            $connection = Mage::getSingleton('core/resource')->getConnection('core/read');
            $processContextId = $this->getProcessContextId($processContextPackage);
            if(isset($processContextId)) {
                $this->processContextIds[$processContextPackage->getEntityId()] = $connection->query("select entity_id from product_match_whitelist_index_process_context_set where find_in_set({$processContextId}, process_context_ids)")->fetchAll(\PDO::FETCH_COLUMN);
            }
        }
        if (isset($this->processContextIds[$processContextPackage->getEntityId()])) {
           return $this->processContextIds[$processContextPackage->getEntityId()];
        }
        return null;
    }
}