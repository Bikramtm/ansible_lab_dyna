<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */

/**
 * Class Vznl_Catalog_Model_Product
 */
class Vznl_Catalog_Model_Product extends Dyna_Catalog_Model_Product
{
    CONST VOLTE_ATTRIBUTE_NAME = 'prodspecs_network';
    CONST VOLTE_ATTRIBUTE_VALUE = 'VoLTE';
    CONST VILTE_ATTRIBUTE_VALUE = 'ViLTE';
    CONST VOWIFI_ATTRIBUTE_VALUE = 'VoWiFi';
    CONST ADDON_TYPE_ATTR = 'identifier_addon_type';
    CONST SERIAL_NUMBER_TYPE_ATTR = 'serial_number_type';
    CONST LIFECYCLE_STATUS = 'lifecycle_status';

    CONST LOAN_INDICATED_ATTRIBUTE = 'loan_indicator';
    CONST LOAN_ATTRIBUTE_VALUE = 'LOAN';
    CONST ILT_ATTRIBUTE_VALUE = 'LOAN + ILT';
    CONST IDENTIFIER_CONSUMER = 'Privat';
    CONST IDENTIFIER_BUSINESS = 'Business';
    CONST CATALOG_CONSUMER_TYPE = 'product_segment';
    CONST IDENTIFIER_RESIDENTIAL = 'Residential';
    CONST IDENTIFIER__SOHO = 'SOHO';
    const INDIRECT_SIMONLY_SPECIAL_PRODUCT_SKU = '4700104';
    const DEVICE_TYPES = [
        Dyna_Catalog_Model_Type::SUBTYPE_DEVICE,
        Omnius_Catalog_Model_Type::SUBTYPE_DEVICE
    ];

    const ACCESSORY_TYPES = [
        Dyna_Catalog_Model_Type::SUBTYPE_ACCESSORY,
        Omnius_Catalog_Model_Type::SUBTYPE_ACCESSORY
    ];

    const SIM_TYPES = [
        Dyna_Catalog_Model_Type::SUBTYPE_SIMCARD,
        Omnius_Catalog_Model_Type::SUBTYPE_SIMCARD
    ];

    const SUBSCRIPTION_TYPES = [
        Vznl_Catalog_Model_Type::FIXED_SUBTYPE_SUBSCRIPTION,
        Dyna_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION,
        Omnius_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION
    ];

    public static $sections = [
        Dyna_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION => [Dyna_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION],
        Vznl_Catalog_Model_Type::SUBTYPE_DEVICE => [Vznl_Catalog_Model_Type::SUBTYPE_DEVICE],
        Vznl_Catalog_Model_Type::SUBTYPE_ADDON => [Vznl_Catalog_Model_Type::SUBTYPE_ADDON],
        Dyna_Catalog_Model_Type::SUBTYPE_ACCESSORY => [Dyna_Catalog_Model_Type::SUBTYPE_ACCESSORY],
        Dyna_Catalog_Model_Type::SUBTYPE_GOODY => [Dyna_Catalog_Model_Type::SUBTYPE_GOODY],
        Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_DEVICE => [Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_DEVICE],
        Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_SUBSCRIPTION => [Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_SUBSCRIPTION],
        Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_PROMOTION => [Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_PROMOTION],
        Dyna_Catalog_Model_Type::SUBTYPE_PROMOTION => [Dyna_Catalog_Model_Type::SUBTYPE_PROMOTION],
        Dyna_Catalog_Model_Type::MOBILE_PREPAID_SUBSCRIPTION => [Dyna_Catalog_Model_Type::MOBILE_PREPAID_SUBSCRIPTION],
        Vznl_Catalog_Model_Type::SUBTYPE_DEVICE_SUBSCRIPTION => [Vznl_Catalog_Model_Type::SUBTYPE_DEVICE_SUBSCRIPTION],
        /** Fixed custom subtypes */
        Vznl_Catalog_Model_Type::SUBTYPE_FIXED_PACKAGE => [Vznl_Catalog_Model_Type::SUBTYPE_FIXED_PACKAGE],
        Vznl_Catalog_Model_Type::SUBTYPE_FIXED_PACKAGE_DETAILS => [Vznl_Catalog_Model_Type::SUBTYPE_FIXED_PACKAGE_DETAILS],
        Vznl_Catalog_Model_Type::SUBTYPE_FIXED_HARDWARE_MATERIALS => [Vznl_Catalog_Model_Type::SUBTYPE_FIXED_HARDWARE_MATERIALS],
        Vznl_Catalog_Model_Type::SUBTYPE_FIXED_PACKAGE_ADDONS => [Vznl_Catalog_Model_Type::SUBTYPE_FIXED_PACKAGE_ADDONS],
        Vznl_Catalog_Model_Type::SUBTYPE_FIXED_GOODIES => [Vznl_Catalog_Model_Type::SUBTYPE_FIXED_GOODIES],
        Vznl_Catalog_Model_Type::SUBTYPE_FIXED_REPLACEMENT_MATERIALS => [Vznl_Catalog_Model_Type::SUBTYPE_FIXED_REPLACEMENT_MATERIALS],
    ];

    /**
     * Check if the product supports VoLTE
     *
     * @return bool
     */
    public function hasVoLTE()
    {
        $attrValues = $this->getAttributeText(self::VOLTE_ATTRIBUTE_NAME);

        return $attrValues
            && (
                (is_array($attrValues) && in_array(self::VOLTE_ATTRIBUTE_VALUE, $attrValues))
                || (is_string($attrValues) && $attrValues == self::VOLTE_ATTRIBUTE_VALUE)
            );
    }

    public function isHollandsNieuweNetworkProduct()
    {
        return $this->getAttributeText(Vznl_Configurator_Model_AttributeSet::ATTRIBUTE_NETWORK) === Vznl_Configurator_Model_AttributeSet::ATTRIBUTE_NETWORK_HOLLANDSNIEUWE;
    }

    /**
     * @return bool
     */
    public function isLoanRequired()
    {
        return $this->getAttributeText(self::LOAN_INDICATED_ATTRIBUTE) === self::LOAN_ATTRIBUTE_VALUE || $this->isIltRequired();
    }

    /**
     * @return bool
     */
    public function isIltRequired()
    {
        return $this->getAttributeText(self::LOAN_INDICATED_ATTRIBUTE) === self::ILT_ATTRIBUTE_VALUE;
    }

    /**
     * Check if a product is any of the hardware types (device/sim/accessory)
     * @param bool $skipSim <true> means that check for sim will be skipped. <false> means types will be checked vs sim types.
     * @return bool <true> means the product has a hardware type.
     */
    public function isOfHardwareType($skipSim = false)
    {
        if ($this->isCopyLevy()) {
            return false;
        }
        $hardwareTypes = array_merge(self::DEVICE_TYPES, self::ACCESSORY_TYPES);
        if (!$skipSim) {
            $hardwareTypes = array_merge($hardwareTypes, self::SIM_TYPES);
        }

        foreach ($hardwareTypes as $hardwareType) {
            if ($this->is($hardwareType)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check if the product is of subscription type.
     * @return bool <true> means that the product is a subscription.
     */
    public function isSubscription()
    {
        foreach (self::SUBSCRIPTION_TYPES as $subscriptionType) {
            if ($this->is($subscriptionType)) {
                return true;
            }
        }
        return parent::isSubscription();
    }

    /**
     * Check if the product is of option type.
     * @return bool <true> means that the product is an option.
     */
    public function isOption()
    {
        return $this->is(Vznl_Catalog_Model_Type::$options);
    }

    public function isSubTypeFixedPackageDetails ()
    {
        return $this->is(Vznl_Catalog_Model_Type::SUBTYPE_FIXED_PACKAGE_DETAILS);
    }

    /**
     * Check if the product is of device subscription type.
     * @return bool <true> means that the product is a device subscription.
     */
    public function isDeviceSubscription()
    {
        return $this->is(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE_SUBSCRIPTION);
    }

    /**
     * Checks whether this product is of type Data_subscription.
     * @return bool <true> means that the product is a data subscription
     */
    public function isDataSubscription()
    {
        return $this->getAttributeSet() == Omnius_Configurator_Model_AttributeGroup::ATTR_GROUP_DATA_SUBSCRIPTIONS;
    }

    /**
     * Checks whether the product has an attribute identifier_subscr_brand with given value
     * @param string $network The network value to check.
     * @return bool <true> if network is set to the given value.
     */
    public function isNetwork($network = Vznl_Configurator_Model_AttributeSet::ATTRIBUTE_NETWORK_VODAFONE)
    {
        return $this->hasAttributeWithValue(Vznl_Configurator_Model_AttributeSet::ATTRIBUTE_NETWORK, $network);
    }

    /**
     * Checks if the product contains the attribute with the given value
     * @param string $name The attribute name.
     * @param string $value The attribute value.
     * @return bool <true> if the value was equal. <false> if not.
     */
    public function hasAttributeWithValue($name, $value)
    {
        if ($attr = $this->getResource()->getAttribute($name)) {
            if (strtolower($value) == strtolower($attr->getFrontend()->getValue($this))) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get the product for a sim
     *
     * @param $subscription
     * @param $simType
     * @param $simProd
     * @return mixed
     * @throws Mage_Core_Exception
     */
    public function getSimProduct($subscription, $simType, $simProd)
    {
        $key = sprintf('%s_product_%s_type_%s_sim', $subscription->getId(), $simType, $simProd);
        if ($result = $this->getCache()->load($key)) {
            return unserialize($result);
        } else {
            if (!$simProd) {
                $productModel = Mage::getModel('catalog/product');
                $attr = $productModel->getResource()->getAttribute(Omnius_Catalog_Model_Product::DEFAULT_SIM_ATTRIBUTE);
                if ($attr->usesSource()) {
                    $simId = $attr->getSource()->getOptionId($simType);
                }

                $collection = Mage::getResourceSingleton('catalog/product_collection')
                    ->addAttributeToFilter('name', array('in' => $subscription->getAttributeText(Omnius_Catalog_Model_Product::AVAILABLE_SIM_TYPES_ATTRIBUTE)))
                    ->addAttributeToFilter(Omnius_Catalog_Model_Product::DEFAULT_SIM_ATTRIBUTE, $simId)
                    ->addOrder('name', Varien_Data_Collection::SORT_ORDER_ASC)
                    ->setPageSize(1)
                    ->setCurPage(1)
                    ->load();
            } else {
                $collection = Mage::getResourceSingleton('catalog/product_collection')
                    ->addAttributeToFilter('name', $simProd)
                    ->setPageSize(1)
                    ->setCurPage(1)
                    ->load();
            }
            if ($collection->getSize() < 1) {
                Mage::throwException('Invalid SIM type provided');
            }
            $result = $collection->setPageSize(1, 1)->getLastItem();
            $this->getCache()->save(serialize($result), $key, [Dyna_Cache_Model_Cache::PRODUCT_TAG], $this->getCache()->getTtl());
            return $result;
        }
    }

    /**
     * Check if the product is of given subscription type.
     * @return bool
     */
    public function isNetworkProduct()
    {
        return $this->is(Dyna_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION);
    }

    /**
     * Check if the product is a simonly product
     */
    public function getSimOnly()
    {
        return (strtolower($this->getAttributeText(Omnius_Catalog_Model_Product::POSTP_SIMONLY_KEY)) == strtolower(Omnius_Catalog_Model_Product::SIM_ONLY_VALUE));
    }


   /**
     * Check if the product is a simonly special product in indirect channel
     * @return bool
     */
    public function isIndirectSpecialProductSimOnly()
    {
        if (Mage::helper('vznl_agent')->isIndirectStore()) {
            if ($this->getSku() == Vznl_Catalog_Model_Product::INDIRECT_SIMONLY_SPECIAL_PRODUCT_SKU) {
                return true;
            }
        }
        return false;
    }

    /**
     * Determines whether the product is required to be returned.
     * Currently applies to all hardware except sim cards.
     * @return bool <true> if it is required to be returned, <false> if not.
     */
    public function isRequiredTobeReturned()
    {
        if ($this->isOfHardwareType() && !$this->isSim()) {
            return true;
        }
        return false;
    }

    /**
     * Go-to method for getProducts action
     */
    public function getCatalogProducts($params) {
        $websiteId = $params['websiteId'];
        $processContext = $params['processContext'] ?: 'ACQ';
        $packageCreationTypeId = $params['packageCreationTypeId'];
        $customerType = $params['customerType'];
        $showPriceWithBtw = $params['showPriceWithBtw'];
        $axiStoreCode = $params['axiStoreCode'];

        $response = array();
        /** @var Dyna_Configurator_Helper_Data $helper */
        $helper = Mage::helper('dyna_configurator');
        /** @var Dyna_Catalog_Helper_Data $catalogHelper */
        $catalogHelper = Mage::helper('dyna_catalog');

        $generalFilters = $additionalFilters = $permanentFiltersAttributePairs = [];

        /** @var Dyna_Package_Model_PackageCreationTypes $packageCreationType */
        $packageCreationType = Mage::getModel('dyna_package/packageCreationTypes')->load($packageCreationTypeId);
        if ($packageCreationType) {
            /** @var Dyna_Package_Model_PackageType $packageType */
            $packageType = Mage::getModel('dyna_package/packageType')->load($packageCreationType->getPackageTypeId());
            $type = strtolower($packageType->getPackageCode());

            $generalFilters = $packageCreationType->getFilterAttributes($processContext);

            $tmpFilters = $packageCreationType->getPackageSubtypeFilterAttributes();
            if (!empty($tmpFilters)) {
                $additionalFilters = Mage::helper('dyna_configurator')->getSubtypeFilterCondition($tmpFilters);
            }

            foreach ($packageType->getPackageSubTypes() as $packageSubType) {
                if (!$packageSubType->getVisibleConfigurator()) {
                    continue;
                }

                $permanentFiltersAttributePairs[$packageSubType->getPackageCode(true)] = $helper->getAttributePairsPermanentFilters($packageSubType->getPackageCode(true), $generalFilters);
            }
        }

        foreach ($additionalFilters as $section => $filter) {

            if (isset($permanentFiltersAttributePairs[strtolower($section)])) {
                $permanentFiltersAttributePairs[strtolower($section)] = array_merge($permanentFiltersAttributePairs[strtolower($section)], $helper->getAttributePairsPermanentFilters(strtolower($section), $filter));
            } else {
                $permanentFiltersAttributePairs[strtolower($section)] = $helper->getAttributePairsPermanentFilters(strtolower($section), $filter);
            }
        }

        $permanentFiltersSearchText = $helper->getSearchTxtPermanentFilters($generalFilters);

        Mage::unregister('products_visibility_package_type');
        Mage::register('products_visibility_package_type', $type);

        /** @var Dyna_Configurator_Model_Catalog $configuratorTypeModel */
        $configuratorTypeModel = @Mage::getModel(sprintf('dyna_configurator/%s', $type));
        // fall back to catalog model if package type is not extended
        if (!$configuratorTypeModel) {
            $configuratorTypeModel = Mage::getModel('dyna_configurator/catalog');
        }
        // let configuration model know what package type it configures
        $configuratorTypeModel->setPackageType($type);
        $configuratorTypeModel->setCustomerType($customerType);
        $configuratorTypeModel->setShowPriceWithBtw($showPriceWithBtw);
        $configuratorTypeModel->setAxiStoreCode($axiStoreCode);
        $products = $configuratorTypeModel->getAllConfiguratorItems($websiteId, $permanentFiltersAttributePairs, $processContext);

        //append installBase products even if they are expired or not effective yet
        if ($processContext == Vznl_Catalog_Model_ProcessContext::ILSBLU || $processContext == Vznl_Catalog_Model_ProcessContext::MOVE) {
            $this->appendInstallBaseProducts($products);
        }

        $productFilters = $products['filters'] ?? null;
        $productFilters = isset($productFilters) && is_array($productFilters) && count($productFilters) ? $helper->toArray($productFilters) : [];
        unset($products['filters']);
        $allProducts = count($products) ? $helper->toArray($products) : [];

        // determine if a product should be visible or not
        foreach ($allProducts as $key => &$currentProducts) {
            if ($key != 'consumer_filters') {
                foreach ($currentProducts as &$currentProduct) {
                    /** @var Dyna_Catalog_Model_Product $product */
                    $product = $products[$key]->getItemById($currentProduct['entity_id']);
                    $currentProduct['product_visibility'] = $catalogHelper->getVisibility(
                        Dyna_Catalog_Model_Product::PRODUCT_VISIBILITY_CONFIGURATOR,
                        $currentProduct['sku'],
                        $product,
                        $product->getData('product_visibility')
                    );
                }
                unset($currentProduct);
            }
        }
        unset($currentProducts);

        $response[sprintf('prod_%s', $type)] = $allProducts;
        $response[sprintf('filter_%s', $type)] = $productFilters;

        $processedFamilies = $configuratorTypeModel->processFamilies($type, $allProducts);
        foreach ($processedFamilies['families']['items'] as &$family) {
            // If no family position set, moving it to the end of the list
            $family['family_position'] = isset($family['family_sort_order']) ? (int)$family['family_sort_order'] : 9999;
        }
        $response[sprintf('fam_%s', $type)] = $processedFamilies['families'];

        foreach ($permanentFiltersAttributePairs as $section => $filters) {
            $sectionFilters = array_map(
                function ($filter) {
                    return $filter['string_filter'];
                }, $filters);

            if($sectionFilters) {
                $response[sprintf('permanent_filters_%s', $type)][mb_strtolower($section)] = [
                    'all_permanent_filters' => $sectionFilters,
                    'permanent_search_filters' => $permanentFiltersSearchText
                ];
            }
        }

        if (isset($products['consumer_filters'])) {
            $consumerProductFilters = $products['consumer_filters'];
            $consumerProductFilters = isset($consumerProductFilters) && is_array($consumerProductFilters) && count($consumerProductFilters) ? $helper->toArray($consumerProductFilters) : new stdClass();
            unset($products['consumer_filters']);
            $response[sprintf('filter_consumer_%s', $type)] = $consumerProductFilters;
        }

        return $response;
    }

    /**
     * @param $products
     * @return mixed
     */
    protected function appendInstallBaseProducts($products) {
        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $productTypeAttribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', 'type');
        $productTypeValue = Mage::helper('dyna_catalog')->getAttributeAdminLabel($productTypeAttribute, null, Dyna_Cable_Model_Product_Attribute_Option::TYPE_OPTION_CONTAINER);

        foreach ($quote->getAllItems() as $quoteItem) {
            if ($quoteItem->getSystemAction() == 'existing') {
                $installBaseProduct = Mage::getModel('catalog/product')->load($quoteItem->getProductId());
                $installBaseProductType = $installBaseProduct->getType() ? strtolower(reset($installBaseProduct->getType())) : '';

                if (isset($products[$installBaseProductType]) && !empty($products[$installBaseProductType]) &&
                    ($installBaseProduct->getExpirationDate() < date('Y-m-d') || $installBaseProduct->getEffectiveDate() > date('Y-m-d'))) {

                    // Only return the following properties to avoid large json objects with properties that are not used
                    $keys = array_merge(Mage::getModel('vznl_configurator/catalog')->getFrontendAttributes(), Mage::getModel('omnius_configurator/catalog')->getFilterableAttributeCodes());
                    $mandatoryCategories = Mage::helper('omnius_configurator')->getMandatoryCategories();
                    $installBaseItem = Mage::getModel('vznl_configurator/catalog')->getFrontendProduct($installBaseProduct, $keys, $mandatoryCategories, $productTypeValue);

                    try {
                        $products[$installBaseProductType]->addItem($installBaseItem);
                    } catch (Exception $e) {
                        continue;
                    }
                }
            }
        }

        return $products;
    }

    /**
     * Checks if a product is of type Promotional. This is determined based on the attribute set
     *
     * @return bool
     */
    public function isPromo()
    {
        return $this->getAttributeSet() == Omnius_Configurator_Model_AttributeGroup::ATTR_GROUP_PROMOTIONS || $this->getAttributeSet() == Vznl_Configurator_Model_AttributeSet::ATTR_GROUP_FIXED_PROMOTIONS;
    }

    /**
     * Returns a product by ID
     *
     * @param $id
     * @return Vznl_Catalog_Model_Product
     */
    public function getById($id)
    {
        return $this->load($id);
    }

    /**
     * If is red plus owner => is eligible for red => disabled in vznl
     * @return bool
     */
    public function isRedPlusOwner()
    {
        return false;
    }

    /**
     * Check if product is eligible to be member for a red+ group => disabled in vznl
     * @return bool
     */
    public function isRedPlusMember()
    {
        return false;
    }

    /**
     * Check if product can be Dead On Arrival.
     *
     * @return bool
     */
    public function canBeDOA()
    {
        return ($this->is(Vznl_Catalog_Model_Type::SUBTYPE_ACCESSORY) || $this->is(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE)) && $this->isCopyLevy() === false;
    }

    /**
     * Check if product is copy levy (wettelijke thuiskopieheffing)
     *
     * @return bool
     */
    public function isCopyLevy()
    {
        return in_array($this->getSku(), Mage::getStoreConfig('checkout/copy_levy'));
    }

    /**
     * Retrieve assigned category Ids
     * @param int $websiteId
     * @param $subtype string Subtype of the product
     * @return array
     */
    public function getStockCall($websiteId, $subtype, $axiStoreId)
    {
        /** @var Dyna_Configurator_Model_Cache $cache */
        $cache = Mage::getSingleton('dyna_cache/cache');
        $attributeCacheKey = $subtype . '_attribute_stock_admin_labels_' . $websiteId;
        if ($attributeCached = $cache->load($attributeCacheKey)) {
            $product = unserialize($attributeCached);
        } else {
            $attribute = Mage::getSingleton("eav/config")
                ->getAttribute('catalog_product', Dyna_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR);
            $product = Mage::helper('dyna_catalog')->getAttributeAdminLabel(
                $attribute,
                null,
                $subtype
            );
            unset($attribute);
            $cache->save(serialize($product), $attributeCacheKey, array(), $cache->getTtl());
        }

        $productsCacheKey = 'catalog_products_' . $subtype . '_stock_call_' . $websiteId;
        if ($productsCached = $cache->load($productsCacheKey)) {
            $products = unserialize($productsCached);
        } else {
            $productsColl = Mage::getResourceModel('catalog/product_collection')
                ->addAttributeToSelect(array('stock_status', 'axi_threshold'), true)
                ->addWebsiteFilter($websiteId)
                ->addAttributeToFilter('is_deleted', array('neq' => 1))
                ->addAttributeToFilter(Dyna_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR, array('eq' => $product));

            $products = Mage::getSingleton('core/resource')->getConnection('core_read')->fetchAssoc($productsColl->getSelect());
            unset($productsColl);
            $cache->save(serialize($products), $productsCacheKey, array(), $cache->getTtl());
            unset($productsCacheKey);
        }

        /** @var Dyna_Stock_Helper_Data $stockHelper */
        $stockHelper = Mage::helper('stock');
        $stocks = $stockHelper->getStockForProducts(array_keys($products), $axiStoreId);
        $result = array();

        foreach ($products as $id => $item) {
            $result[$id] = array(
                'stock_status_text' => null,
                'stock_status_class' => null
            );
            if (isset($item[Dyna_Stock_Model_Stock::STOCK_STATUS_ATTRIBUTE_CODE])) {

                $objProd = Mage::getModel('catalog/product');
                $objProd->setData($item);
                $ss = $item[Dyna_Stock_Model_Stock::STOCK_STATUS_ATTRIBUTE_CODE];

                if ($ss) {
                    // Override status for current context
                    $stock = isset($stocks[$id]['qty']) ? $stocks[$id]['qty'] : 0;
                    $backorders = isset($stocks[$id]['backorder_qty']) ? $stocks[$id]['backorder_qty'] : 0; // Is negative amount

                    $cs = Mage::getSingleton('customer/session');
                    $storeId = $cs->getAgent() && $cs->getAgent()->getDealer() ? $cs->getAgent()->getDealer()->getStoreId() : Mage::app()->getStore()->getId();

                    $objProd->isSellable($stock, $backorders, $axiStoreId, $storeId); // Also sets the status attributes
                    $result[$id]['stock_status'] = $objProd->getStockStatus();
                    $result[$id]['stock_status_text'] = $objProd->getStockStatusText();
                    $result[$id]['stock_status_class'] = $objProd->getStockStatusClass();
                }
            }
        }

        return $result;
    }
}
