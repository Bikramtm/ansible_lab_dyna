<?php

/**
 * Class Vznl_Catalog_Model_Import_CategoriesTree
 */
class Vznl_Catalog_Model_Import_CategoriesTree extends Dyna_Catalog_Model_Import_CategoriesTree
{
    protected $_logFileName = "categories_import";
    protected $_logFileNameProducts = "categorytree_import";
    protected $_logFileExtension = 'log';

    public function __construct()
    {
        parent::__construct();
        /** @var Vznl_Import_Helper_Data _helper */
        $this->_helper = Mage::helper('vznl_import');
    }

    /**
     * Import FN category from csv file
     * @param $data
     */
    public function importCategoryTree($data)
    {
        $data = array_combine($this->_header, $data);
        if (!array_filter($data)) {
            return false;
        }

        $skip = false;
        //for logging purposes
        $this->_totalFileRows++;

        if (!array_key_exists('external_category_id', $data) || empty($data['external_category_id'])) {
            $this->_logError('Skipping line without external category id');
            $skip = true;
        }

        if ($skip) {
            $this->_skippedFileRows++;
            return;
        }
        $this->_log('Start importing the category tree ' . $data['external_category_id'], true);

        Mage::helper("dyna_import/data")->preventExtraColumns($data);

        try {
            // search category
            /** @var Dyna_Catalog_Model_Category $categoryModel */
            $categoryModel = Mage::getModel('dyna_catalog/category')
                ->getCollection()
                ->addFieldToFilter('unique_id', ['eq' => $data['external_category_id']])
                ->getFirstItem();
            $isNew = false;
            if (!$categoryModel || !$categoryModel->getId()) {
                // create a new category
                $isNew = true;
                /** @var Dyna_Catalog_Model_Category $categoryModel */
                $categoryModel = Mage::getModel('dyna_catalog/category');
            }

            // set data for new categories
            $categoryName = (isset($data['category_name']) && trim($data['category_name']) != '') ?
                $data['category_name'] : $data['external_category_id'];

            // set parent if is defined
            if (isset($data['external_parent_category_id']) && trim($data['external_parent_category_id']) != '') {
                $parentCategoryModel = Mage::getModel('dyna_catalog/category')
                    ->getCollection()
                    ->addFieldToFilter('unique_id', ['eq' => $data['external_parent_category_id']])
                    ->getFirstItem();

                if ($parentCategoryModel && $parentCategoryModel->getId()) {
                    $parentCategory = $parentCategoryModel;
                } else {
                    $this->_logError('Skipped category ' . $data['category_name'] . ' with unique id ' . $data['external_category_id'] . ' because parent category with unique id ' . $data['external_parent_category_id'] . ' doesn\'t exists');
                    $this->_skippedFileRows++;
                    return;
                }
            } else {
                // if no parent, set this category on root level
                $parentCategory = Mage::getModel('catalog/category')->load(self::DEFAULT_ID_CATEGORY);
            }

            // for a new category set path and name
            if ($isNew) {
                $general['name'] = $categoryName;
                $general['path'] = $parentCategory->getPath();
            }

            // set categories data
            $general['display_mode'] = self::CATEGORY_DISPLAY_MODE;
            $general['is_active'] = self::CATEGORY_IS_ACTIVE;
            $general['display_name'] = !empty($data['family_display_name']) ? trim($data['family_display_name']) : '';
            $general['unique_id'] = $data['external_category_id'];
            $general['is_anchor'] = ($data['is_anchor'] == 'yes') ? 1 : 0;
            $general['is_family'] = ($data['is_family'] == 'yes') ? 1 : 0;
            $general['allow_mutual_products'] = ($data['mutually_exclusive'] == 'yes') ? 1 : 0;
            $general['description'] = $data['description'] ?? '';
            $general['family_sort_order'] = $data['family_category_sorting'] ?? null;
            $general['external_identifier'] = $data['external_category_id'] ?? '';
            $general['hide_not_selected_products'] = ($data['hide_not_selected_products'] == 'yes') ? 1 : 0;
            $general['stack'] = $this->stack;

            $categoryModel->addData($general);
            $categoryModel->save();
            $this->_qties++;
            $this->_log('Finished importing the category tree ' . $data['category_name'] . '. Action: ' . ($isNew ? 'new' : 'modified'), true);
        } catch (Exception $ex) {
            $io = new Varien_Io_File();
            $io->streamOpen(STDERR);
            $io->streamWrite($ex->getMessage());
            $io->streamClose();
            $this->_logError($ex->getMessage());
        }
    }

    /**
     * Import FN category from csv file
     * @param $data
     */
    public function importCategoryProducts($data)
    {
        $data = array_combine($this->_header, $data);
        if (!array_filter($data)) {
            return false;
        }

        $skip = false;
        //for logging purposes
        $this->_totalFileRows++;
        if (!array_key_exists('external_category_id', $data) || empty($data['external_category_id'])) {
            $this->_logError('Skipping line without category tree unique Id');
            $skip = true;
        } elseif (!array_key_exists('sku', $data) || empty($data['sku'])) {
            $this->_logError('Skipping line without sku');
            $skip = true;
        } elseif (!in_array($data['external_category_id'], array_keys($this->_categories))) {
            $this->_logError('Skipping line without valid category for identifier ' . $data['external_category_id']);
            $skip = true;
        }

        if ($skip) {
            $this->_skippedFileRows++;
            return;
        }
        $this->_log('Start importing the product ' . $data['sku'] . ' for category' . $data['external_category_id'], true);

        Mage::helper("dyna_import/data")->preventExtraColumns($data);

        try {
            if (!in_array($data['sku'], $this->_products)) {
                $productId = Mage::getModel("catalog/product")->getIdBySku($data['sku']);
                $this->_products[$data['sku']] = $productId;
            } else {
                $productId = $this->_products[$data['sku']];
            }

            if (!$productId) {
                $this->_logError('Skipping line because of invalid product with SKU: ' . $data['sku']);
                $this->_skippedFileRows++;
                return;
            }

            //add new connection between product and category
            $write = Mage::getSingleton("core/resource")->getConnection("core_write");
            $query = "INSERT INTO catalog_category_product (category_id, product_id, position) VALUES (:category_id, :product_id, :position) ON DUPLICATE KEY UPDATE position=VALUES(position)";
            $binds = array(
                'category_id' => $this->_categories[$data['external_category_id']],
                'product_id' => $productId,
                'position' => isset($data['position']) ? $data['position'] : 1,
            );
            $write->query($query, $binds);

            $this->_qties++;
            $this->_log('Finished importing the product with sku ' . $data['sku'] . ' for category ' . $data['external_category_id'], true);
        } catch (Exception $ex) {
            $this->_logError($ex->getMessage());
        }
    }

    public function getFileLogName($type)
    {
        if ($type == 'tree') {
            return strtolower($this->stack) . '_' . $this->_logFileName . $this->_logFileExtension;
        } else {
            return strtolower($this->stack) . '_' . $this->_logFileNameProducts . $this->_logFileExtension;
        }
    }
}
