<?php

/** @var Mage_Catalog_Model_Resource_Eav_Mysql4_Setup $installer */
$installer = $this;
$installer->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

$result = $installer->getConnection()->fetchAssoc(
    'SELECT 
  t1.option_id,
  t1.value,
  t2.`attribute_id` 
FROM
  eav_attribute_option_value t1 
  LEFT JOIN eav_attribute_option t2 
    ON t2.`option_id` = t1.`option_id` 
WHERE VALUE IN (\'Consumer\', \'Business\') 
  AND t2.`attribute_id` NOT IN 
  (
    SELECT DISTINCT 
      attribute_id 
    FROM
      eav_attribute
  ) 
  AND t2.`attribute_id` IN 
  (
    SELECT DISTINCT 
      attribute_id 
    FROM
      catalog_product_entity_text
  )
');

$identifierConsBusId = null;
$identifierConsBusOptions = [];

foreach ($result as $option) {
    $identifierConsBusId = $option['attribute_id'];
    $identifierConsBusOptions[$option['option_id']] = $option['value'];
}

if ($identifierConsBusId && !empty($identifierConsBusOptions)) {
    $attribute = Mage::getModel('eav/config')->getAttribute($entityTypeId, 'product_segment');
    $options = $attribute->getSource()->getAllOptions(false);

    $currentOptions = [];
    foreach ($options as $key => $option) {
        if ($option['label'] == 'Privat') {
            $option['label'] = 'Consumer';
        }
        $currentOptions[$option['label']] = $option['value'];
    }

    $products = $installer->getConnection()->fetchAll(
        "
          SELECT
            entity_id,
            value
          FROM
            catalog_product_entity_text
          WHERE
            attribute_id = $identifierConsBusId 
        "
    );

    $installer->getConnection()->beginTransaction();

    try {
        foreach ($products as $product) {
            $values = explode(',', $product['value']);
            foreach ($values as &$value) {
                $value = $currentOptions[$identifierConsBusOptions[$value]];
            }

            $installer->getConnection()->update(
                'catalog_product_entity_text',
                array(
                    'value' => implode(',', $values),
                    'attribute_id' => $attribute->getAttributeId()
                ),
                'entity_id = ' . $product['entity_id'] . ' AND attribute_id=' . $identifierConsBusId
            );
        }
        $installer->getConnection()->commit();
    } catch (Exception $e) {
        $installer->getConnection()->rollBack();
    }
}