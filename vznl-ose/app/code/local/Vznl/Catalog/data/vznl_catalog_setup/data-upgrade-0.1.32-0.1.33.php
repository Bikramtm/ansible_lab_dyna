<?php
/* Update attributes for deviceRC, addon, accessoire, promotion attribute sets*/

/** @var Mage_Catalog_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();

$installer->defaultGroupIdAssociations = null;

/** @var Varien_Db_Adapter_Interface $connection */
$connection = $this->getConnection();
$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

$attributeSets = [
    'deviceRC' => [
        'General' => [
            'sort' => 1,
            'attributes' => [
                'maf',
                'vat',
                'effective_date',
                'checkout_product',
                'hierarchy_value',
                'contract_value',
                'display_name_inventory',
                'display_name_configurator',
                'display_name_cart',
                'display_name_communication',
                'identifier_ean_or_upc_code',
                'price_increase_maf',
                'price_increase_scope',
                'product_type',
                'confirmation_text',
                'ask_confirmation_first',
                'product_version',
                'product_family',
            ],
            'attributesToRemove' => [
                'prijs_aansluitkosten',
                'agent_groups_visibility',
                'hide_original_price',
            ],
        ],
        'Features' => [
            'attributesToRemove' => [
                'prodspecs_4g_lte',
                'prodspecs_aantal_belminuten',
                'prodspecs_data_aantal_mb',
                'prodspecs_sms_amount',
            ],
        ],
        'Niet nodig' => [
            'attributesToRemove' => [
                'special_price',
                'special_from_date',
                'special_to_date',
            ]
        ],
    ],
    'addon' => [
        'General' => [
            'attributes' => [
                'maf',
                'vat',
                'effective_date',
                'checkout_product',
                'hierarchy_value',
                'contract_value',
                'display_name_inventory',
                'display_name_configurator',
                'display_name_cart',
                'display_name_communication',
                'price_increase_maf',
                'price_increase_scope',
                'product_type',
                'confirmation_text',
                'ask_confirmation_first',
                'product_version',
                'product_family',
                'prodspecs_wms_codes',
                'prijs_mb_buiten_bundel',
                'prijs_sms_buiten_bundel',
                'lifecycle_status',
            ],
            'attributesToRemove' => [
                'agent_groups_visibility',
                'hide_original_price',
                'maf_discount_percent',
                'price_discount_percent',
            ]
        ]
    ],
    'accessoire' => [
        'General' => [
            'attributes' => [
                'vat',
                'maf',
                'effective_date',
                'hierarchy_value',
                'contract_value',
                'lifecycle_status',
                'display_name_inventory',
                'display_name_configurator',
                'display_name_cart',
                'display_name_communication',
                'product_version',
                'product_family',
                'product_type',
                'ask_confirmation_first',
                'checkout_product',
                'confirmation_text',
                'price_increase_maf',
                'price_increase_scope',
            ],
            'attributesToRemove' => [
                'hide_original_price',
            ]
        ],
    ],
    'promotion' => [
        'General' => [
            'attributes' => [
                'maf',
                'vat',
                'effective_date',
                'checkout_product',
                'hierarchy_value',
                'contract_value',
                'display_name_cart',
                'display_name_communication',
                'display_name_configurator',
                'display_name_inventory',
                'price_increase_maf',
                'price_increase_scope',
                'product_type',
                'ask_confirmation_first',
                'confirmation_text',
                'product_family',
                'product_version',
                'prodspecs_soccode',
                'identifier_hawaii_id',
                'identifier_hawaii_type',
                'prodspecs_hawaii_channel',
                'prodspecs_hawaii_usp1',
                'prodspecs_hawaii_usp2',
                'prodspecs_hawaii_usp3',
                'price_discount',
                'price_discount_percent',
                'prijs_aansluitkosten',
                'discount_period',
                'discount_period_amount',
                'maf_discount',
                'maf_discount_percent',
                'percentagepromotion',
                'promotion_label',
                'lifecycle_status',
            ],
            'attributesToRemove' => [
                'agent_groups_visibility',
                'prodspecs_brand',
            ]
        ],
    ],
];

$attributeDefinition = [
    'promotion_label' => [
        'label' => 'Promotion label',
        'input' => 'text',
        'type' => 'text',
        'required'  => 0,
    ],
];

foreach ($attributeSets as $attributeSetName => $attributeSetGroups) {
    // load current attribute set
    $attributeSet = $installer->getAttributeSet($entityTypeId, $attributeSetName);

    foreach ($attributeSetGroups as $groupName => $groupData) {
        /** @var Mage_Eav_Model_Entity_Setup $currentGroup */
        $currentGroup = $installer->getAttributeGroup($entityTypeId, $attributeSet['attribute_set_id'], $groupName);

        if (!$currentGroup) {
            $installer->addAttributeGroup($entityTypeId, $attributeSet['attribute_set_id'], $groupName, $groupData['sort']);

            $currentGroup = $installer->getAttributeGroup($entityTypeId, $attributeSet['attribute_set_id'], $groupName);
        }

        //get a list of all attribute codes of the current group
        $currentGroupAttributes = Mage::getResourceModel('catalog/product_attribute_collection')
            ->setAttributeGroupFilter($currentGroup['attribute_group_id']);
        $currentGroupAttributes->getSelect()->reset(Zend_Db_Select::COLUMNS);
        $currentGroupAttributes->addFieldToSelect('attribute_code');
        $currentGroupAttributes = $connection->fetchCol($currentGroupAttributes->getSelect());

        if (isset($groupData['attributes'])) {
            foreach ($groupData['attributes'] as $attributeCode) {
                //verify if attributeCode is in current group
                if (!in_array($attributeCode, $currentGroupAttributes)) {
                    $attributeId = Mage::getModel('eav/entity_attribute')->loadByCode($entityTypeId, $attributeCode)->getId();

                    if ( !$attributeId && isset($attributeDefinition[$attributeCode]) ) {
                        $installer->addAttribute(
                            $entityTypeId, $attributeCode, $attributeDefinition[$attributeCode]
                        );

                        $attributeId = Mage::getModel('eav/entity_attribute')->loadByCode($entityTypeId, $attributeCode)->getId();
                    }
                    //assign attribute to current group
                    $installer->addAttributeToGroup($entityTypeId, $attributeSet['attribute_set_id'], $currentGroup['attribute_group_id'], $attributeId);
                }
            }
        }

        // remove unused attributes from attribute set group
        if (isset($groupData['attributesToRemove'])) {
            foreach ($groupData['attributesToRemove'] as $attributeCode) {
                if (in_array($attributeCode, $currentGroupAttributes)) {
                    $attributeId = Mage::getModel('eav/entity_attribute')->loadByCode($entityTypeId, $attributeCode)->getId();
                    $installer->deleteTableRow(
                        'eav/entity_attribute',
                        'attribute_id',
                        $attributeId,
                        'attribute_set_id',
                        $attributeSet['attribute_set_id']
                    );
                }
            }
        }
    }
}

$this->endSetup();