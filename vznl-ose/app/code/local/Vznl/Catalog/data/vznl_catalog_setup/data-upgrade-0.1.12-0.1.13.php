<?php

$installer = $this;
$installer->startSetup();

$conn->insert('catalog_package_subtypes', [
    'type_package_id' => 1,
    'package_code' => 'Simcard',
    'front_end_name' => 'Simcard',
    'front_end_position' => 6,
    'ignored_by_compatibility_rules' => 1,
    'package_subtype_visibility' => 'inventory,shopping_cart,extended_shopping_cart,order_overview,voice_log,offer_pdf,call_summary,checkout',
    'manual_override_allowed' => 0,
    'package_subtype_lifecycle_status' => 0,
    'stack' => 'Mobile'
]);

$installer->endSetup();