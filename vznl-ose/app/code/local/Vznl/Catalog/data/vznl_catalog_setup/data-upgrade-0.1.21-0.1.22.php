<?php
$this->startSetup();

/** @var Varien_Db_Adapter_Interface $connection */
$connection = $this->getConnection();

$packageTypesTable = $this->getTable('catalog_package_types');
$packageSubtypesTable = $this->getTable('catalog_package_subtypes');

/** Get Hardware package_type */
$packageTypeHardware = $connection->fetchRow("SELECT * FROM `{$packageTypesTable}` WHERE `package_code` = '" . Omnius_Catalog_Model_Type::TYPE_HARDWARE . "'");

if ($packageTypeHardware) {
    /** Get all subtypes of hardware package type */
    $selectSql = "SELECT * FROM `{$packageSubtypesTable}` WHERE `type_package_id` = " . $packageTypeHardware['entity_id'];
    $hardwareSubtypes = $connection->fetchAll($selectSql);

    foreach ($hardwareSubtypes as $hardwareSubtype) {
        /** Update Goody to Accessory */
        if ($hardwareSubtype['package_code'] == 'Goody') {
            $connection->query("UPDATE `{$packageSubtypesTable}` SET `package_code` = 'Accessory' WHERE `package_code` = 'Goody'");
        }
    }
}

$this->endSetup();
