<?php
$this->startSetup();

/** @var Varien_Db_Adapter_Interface $connection */
$connection = $this->getConnection();

/** Update GUI name for fixed package */
$updateSql = "UPDATE `package_creation_types` SET `gui_name` = 'TV, Internet & Vaste Telefonie' WHERE `package_type_code` = 'ZIG'";
$connection->query($updateSql);

/** Remove cable creation type */
$deleteSql = "DELETE FROM `package_creation_types` WHERE `package_type_code` = 'Cable_internet_phone'";
$connection->query($deleteSql);

$this->endSetup();

