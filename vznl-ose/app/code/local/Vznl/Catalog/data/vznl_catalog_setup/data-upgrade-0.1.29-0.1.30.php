<?php
$installer = $this;

$installer->startSetup();
$installer->getConnection();
$installer->updateAttribute('catalog_product', 'additional_text', 'frontend_input', 'textarea');
$this->endSetup();