<?php

$installer = $this;
$installer->startSetup();
// special handling of this hardcoded product
$levyAddonSku = Mage::getStoreConfig(Dyna_PriceRules_Model_Observer::COPY_LEVY_PREFIX . 'standaard');

if ($levyAddonSku) {
    $levyAddonProduct = Mage::getModel('catalog/product')->loadByAttribute('sku', $levyAddonSku);
    // set Mobile for package_type
    $attribute = Mage::getModel('eav/config')->getAttribute( Mage_Catalog_Model_Product::ENTITY ,'package_type');
    $packageTypeOptions = $attribute->getSource()->getAllOptions();
    foreach ($packageTypeOptions as $option) {
        if ($option['label'] == 'Mobile') {
            $levyAddonProduct->setPackageType($option['value']);
            $levyAddonProduct->getResource()->saveAttribute($levyAddonProduct, 'package_type');
        }
    }
    // set Addon for package_subtype
    $attribute = Mage::getModel('eav/config')->getAttribute( Mage_Catalog_Model_Product::ENTITY ,'package_subtype');
    $packageSubtypeOptions = $attribute->getSource()->getAllOptions();
    foreach ($packageSubtypeOptions as $option) {
        if ($option['label'] == 'Device') {
            $levyAddonProduct->setPackageSubtype($option['value']);
            $levyAddonProduct->getResource()->saveAttribute($levyAddonProduct, 'package_subtype');
        }
    }
}

$installer->endSetup();