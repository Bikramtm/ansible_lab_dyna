<?php
$this->startSetup();

/** @var Mage_Tax_Model_Calculation_Rate[] $taxCalculationRules */
$taxCalculationRules = Mage::getModel('tax/calculation_rate')->getCollection();
try {
    foreach ($taxCalculationRules as $calculationRule) {
        $calculationRule->setTaxCountryId(Mage::getStoreConfig(Mage_Tax_Model_Config::CONFIG_XML_PATH_DEFAULT_COUNTRY));
        $calculationRule->save();
    }
} catch (Exception $e) {
	Mage::logException($e);
}

$this->endSetup();