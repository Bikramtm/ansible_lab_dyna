<?php
/**
 * Add attribute and attribute set for fixed products. These attributes are created for fixed products import, and will be
 * available in the configurator for creating fixed rules
 */
/* @var $installer Mage_Sales_Model_Entity_Setup */

$installer = $this;
$installer->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

// Create attributes
$attributes = [
    'gui_name' => [
        'label' => 'gui_name',
        'input' => 'text',
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => '',
        'required' => false,
        'unique' => false,
        'filterable' => false,
        'apply_to' => 'simple'
    ],
    'checkout_product' => [
        'label' => 'checkout_product',
        'input' => 'text',
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'unique' => false,
        'filterable' => false,
        'apply_to' => 'simple'
    ],
    'channel' => [
        'label' => 'channel',
        'input' => 'text',
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => '',
        'unique' => false,
        'filterable' => false,
        'apply_to' => 'simple'
    ],
    '360_template' => [
        'label' => '360_template',
        'input' => 'text',
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => '',
        'unique' => false,
        'filterable' => false,
        'apply_to' => 'simple'
    ],
    'product_family' => [
        'label' => 'product_family',
        'input' => 'text',
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => '',
        'unique' => false,
        'filterable' => false,
        'apply_to' => 'simple'
    ],
    'product_version' => [
        'label' => 'product_versions',
        'input' => 'text',
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => '',
        'unique' => false,
        'filterable' => false,
        'apply_to' => 'simple'
    ],
    'product_version' => [
        'label' => 'product_versions',
        'input' => 'text',
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => '',
        'unique' => false,
        'filterable' => false,
        'apply_to' => 'simple'
    ],
    'product_type' => [
        'label' => 'product_type',
        'input' => 'text',
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => '',
        'unique' => false,
        'filterable' => false,
        'apply_to' => 'simple'
    ],
    'price_discount' => [
        'label' => 'price_discount',
        'input' => 'text',
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => '',
        'unique' => false,
        'filterable' => false,
        'apply_to' => 'simple'
    ],
    'maf_discount' => [
        'label' => 'maf_discount',
        'input' => 'text',
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => '',
        'unique' => false,
        'filterable' => false,
        'apply_to' => 'simple'
    ],
    'discount_period_amount' => [
        'label' => 'discount_period_amount',
        'input' => 'text',
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => '',
        'unique' => false,
        'filterable' => false,
        'apply_to' => 'simple'
    ],
    'discount_period' => [
        'label' => 'discount_period',
        'input' => 'text',
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => '',
        'unique' => false,
        'filterable' => false,
        'apply_to' => 'simple'
    ],
    'confirmation_text' => [
        'label' => 'confirmation_text',
        'input' => 'text',
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => '',
        'unique' => false,
        'filterable' => false,
        'apply_to' => 'simple'
    ],
    'ask_confirmation_first' => [
        'label' => 'ask_confirmation_first',
        'input' => 'boolean',
        'source' => 'eav/entity_attribute_source_boolean',
        'type' => 'int',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => '',
        'unique' => false,
        'filterable' => false,
        'apply_to' => 'simple'
    ]

];

foreach ($attributes as $code => $options) {
    $attributeId = $installer->getAttributeId(Mage_Catalog_Model_Product::ENTITY, $code);
    if (!$attributeId) {
        $options['user_defined'] = 1;
        $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, $code, $options);
    }
}
unset($attributes);

// creating attribute set
$attributeSetCode = array(
    "Fixed_Bundle_Product",
    "Fixed_TV_Product",
    "Fixed_Internet_Product",
    "Fixed_Tel_Product",
    "Fixed_Generic_Product",
    "Fixed_Promotions"
);
$attributeGroupNames = [
    1 => 'Fixed Attributes',
    2 => 'Prices',
    3 => 'Meta Information',
    4 => 'Images',
    5 => 'Recurring Profile',
    6 => 'Design',
    7 => 'Gift Options',
    8 => 'Adyen'
];
$attributeSortOrder = 10;
foreach ($attributeSetCode as $code) {
    $installer->addAttributeSet($entityTypeId, $code, $attributeSortOrder++);
    $attributeSetId = $installer->getAttributeSet($entityTypeId, $code, "attribute_set_id");
    foreach ($attributeGroupNames as $sortOrder => $attributeGroupName) {
        $installer->addAttributeGroup($entityTypeId, $attributeSetId, $attributeGroupName, $sortOrder);
    }
}

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

// Assign attribute to attribute set with group label
$attributeSets = array(
    "Fixed_Bundle_Product" => [
        "General" => [
            "required_serviceability_items",
            "requires_serviceability",
            "hide_original_price",
            "tags",
            "use_service_value",
            "stack",
            "product_locked",
            "country_of_manufacture",
            "agent_groups_visibility",
            "weight", "news_from_date",
            "news_to_date",
            "terms_and_conditions",
            "url_key",
            "short_description",
            "visibility",
            "is_deleted"
        ],
        "Fixed Attributes" => [
            "gui_name",
            "checkout_product",
            "channel",
            "360_template",
            "product_family",
            "product_version"
        ],
        "Prices" => [
            "group_price",
            "special_price",
            "special_from_date",
            "special_to_date",
            "cost", "tier_price",
            "msrp_enabled",
            "msrp_display_actual_price_type",
            "msrp",
            "tax_class_id",
            "price_view",
            "new_price",
            "new_price_date"
        ],
        "Meta Information" => [
            "meta_title",
            "meta_keyword",
            "meta_description"
        ],
        "Images" => [
            "image",
            "small_image",
            "thumbnail",
            "media_gallery",
            "gallery"
        ],
        "Recurring Profile" => [
            "is_recurring",
            "recurring_profile"
        ],
        "Design" => [
            "custom_design",
            "custom_design_from",
            "custom_design_to",
            "custom_layout_update",
            "page_layout",
            "options_container"
        ],
        "Gift Options" => [
            "gift_message_available"
        ],
        "Adyen" => [
            "adyen_pre_order"
        ]
    ],

    "Fixed_TV_Product" => [
        "General" => [
            "required_serviceability_items",
            "requires_serviceability",
            "hide_original_price",
            "tags",
            "use_service_value",
            "stack",
            "product_locked",
            "country_of_manufacture",
            "agent_groups_visibility",
            "weight", "news_from_date",
            "news_to_date",
            "terms_and_conditions",
            "url_key",
            "short_description",
            "visibility",
            "is_deleted"
        ],
        "Fixed Attributes" => [
            "gui_name",
            "checkout_product",
            "channel",
            "360_template",
            "product_family",
            "product_version"
        ],
        "Prices" => [
            "group_price",
            "special_price",
            "special_from_date",
            "special_to_date",
            "cost", "tier_price",
            "msrp_enabled",
            "msrp_display_actual_price_type",
            "msrp",
            "tax_class_id",
            "price_view",
            "new_price",
            "new_price_date"
        ],
        "Meta Information" => [
            "meta_title",
            "meta_keyword",
            "meta_description"
        ],
        "Images" => [
            "image",
            "small_image",
            "thumbnail",
            "media_gallery",
            "gallery"
        ],
        "Recurring Profile" => [
            "is_recurring",
            "recurring_profile"
        ],
        "Design" => [
            "custom_design",
            "custom_design_from",
            "custom_design_to",
            "custom_layout_update",
            "page_layout",
            "options_container"
        ],
        "Gift Options" => [
            "gift_message_available"
        ],
        "Adyen" => [
            "adyen_pre_order"
        ]
    ],

    "Fixed_Internet_Product" => [
        "General" => [
            "required_serviceability_items",
            "requires_serviceability",
            "hide_original_price",
            "tags",
            "use_service_value",
            "stack",
            "product_locked",
            "country_of_manufacture",
            "agent_groups_visibility",
            "weight", "news_from_date",
            "news_to_date",
            "terms_and_conditions",
            "url_key",
            "short_description",
            "visibility",
            "is_deleted"
        ],
        "Fixed Attributes" => [
            "gui_name",
            "checkout_product",
            "channel",
            "360_template",
            "product_family",
            "product_version"
        ],
        "Prices" => [
            "group_price",
            "special_price",
            "special_from_date",
            "special_to_date",
            "cost", "tier_price",
            "msrp_enabled",
            "msrp_display_actual_price_type",
            "msrp",
            "tax_class_id",
            "price_view",
            "new_price",
            "new_price_date"
        ],
        "Meta Information" => [
            "meta_title",
            "meta_keyword",
            "meta_description"
        ],
        "Images" => [
            "image",
            "small_image",
            "thumbnail",
            "media_gallery",
            "gallery"
        ],
        "Recurring Profile" => [
            "is_recurring",
            "recurring_profile"
        ],
        "Design" => [
            "custom_design",
            "custom_design_from",
            "custom_design_to",
            "custom_layout_update",
            "page_layout",
            "options_container"
        ],
        "Gift Options" => [
            "gift_message_available"
        ],
        "Adyen" => [
            "adyen_pre_order"
        ]
    ],

    "Fixed_Tel_Product" => [
        "General" => [
            "required_serviceability_items",
            "requires_serviceability",
            "hide_original_price",
            "tags",
            "use_service_value",
            "stack",
            "product_locked",
            "country_of_manufacture",
            "agent_groups_visibility",
            "weight", "news_from_date",
            "news_to_date",
            "terms_and_conditions",
            "url_key",
            "short_description",
            "visibility",
            "is_deleted"
        ],
        "Fixed Attributes" => [
            "gui_name",
            "checkout_product",
            "channel",
            "360_template",
            "product_family",
            "product_version"
        ],
        "Prices" => [
            "group_price",
            "special_price",
            "special_from_date",
            "special_to_date",
            "cost", "tier_price",
            "msrp_enabled",
            "msrp_display_actual_price_type",
            "msrp",
            "tax_class_id",
            "price_view",
            "new_price",
            "new_price_date"
        ],
        "Meta Information" => [
            "meta_title",
            "meta_keyword",
            "meta_description"
        ],
        "Images" => [
            "image",
            "small_image",
            "thumbnail",
            "media_gallery",
            "gallery"
        ],
        "Recurring Profile" => [
            "is_recurring",
            "recurring_profile"
        ],
        "Design" => [
            "custom_design",
            "custom_design_from",
            "custom_design_to",
            "custom_layout_update",
            "page_layout",
            "options_container"
        ],
        "Gift Options" => [
            "gift_message_available"
        ],
        "Adyen" => [
            "adyen_pre_order"
        ]
    ],

    "Fixed_Generic_Product" => [
        "General" => [
            "required_serviceability_items",
            "requires_serviceability",
            "hide_original_price",
            "tags",
            "use_service_value",
            "stack",
            "product_locked",
            "country_of_manufacture",
            "agent_groups_visibility",
            "weight", "news_from_date",
            "news_to_date",
            "terms_and_conditions",
            "url_key",
            "short_description",
            "visibility",
            "is_deleted"
        ],
        "Fixed Attributes" => [
            "gui_name",
            "checkout_product",
            "channel",
            "360_template",
            "product_family",
            "product_version"
        ],
        "Prices" => [
            "group_price",
            "special_price",
            "special_from_date",
            "special_to_date",
            "cost", "tier_price",
            "msrp_enabled",
            "msrp_display_actual_price_type",
            "msrp",
            "tax_class_id",
            "price_view",
            "new_price",
            "new_price_date"
        ],
        "Meta Information" => [
            "meta_title",
            "meta_keyword",
            "meta_description"
        ],
        "Images" => [
            "image",
            "small_image",
            "thumbnail",
            "media_gallery",
            "gallery"
        ],
        "Recurring Profile" => [
            "is_recurring",
            "recurring_profile"
        ],
        "Design" => [
            "custom_design",
            "custom_design_from",
            "custom_design_to",
            "custom_layout_update",
            "page_layout",
            "options_container"
        ],
        "Gift Options" => [
            "gift_message_available"
        ],
        "Adyen" => [
            "adyen_pre_order"
        ]
    ],

    "Fixed_Promotions" => [
        "General" => [
            "sku",
            "name",
            "description",
            "package_type",
            "package_subtype",
            "status",
            "product_visibility",
            "price",
            "required_serviceability_items",
            "requires_serviceability",
            "hide_original_price",
            "tags",
            "use_service_value",
            "stack",
            "product_locked",
            "country_of_manufacture",
            "agent_groups_visibility",
            "weight", "news_from_date",
            "news_to_date",
            "terms_and_conditions",
            "url_key",
            "short_description",
            "visibility",
            "is_deleted"
        ],
        "Fixed Attributes" => [
            "vat",
            "maf",
            "sorting",
            "effective_date",
            "expiration_date",
            "initial_selectable",
            "hierarchy_value",
            "contract_value",
            "lifecycle_status",
            "display_name_inventory",
            "display_name_configurator",
            "display_name_cart",
            "display_name_communication",
            "product_version_id",
            "product_family_id",
            "product_segment",
            "type_other_addons",
            "template_360",
            "gui_name",
            "checkout_product",
            "channel",
            "360_template",
            "product_family",
            "product_version",
            "product_type",
            "price_discount",
            "maf_discount",
            "discount_period_amount",
            "discount_period",
            "confirmation_text",
            "ask_confirmation_first"
        ],
        "Prices" => [
            "group_price",
            "special_price",
            "special_from_date",
            "special_to_date",
            "cost", "tier_price",
            "msrp_enabled",
            "msrp_display_actual_price_type",
            "msrp",
            "tax_class_id",
            "price_view",
            "new_price",
            "new_price_date"
        ],
        "Meta Information" => [
            "meta_title",
            "meta_keyword",
            "meta_description"
        ],
        "Images" => [
            "image",
            "small_image",
            "thumbnail",
            "media_gallery",
            "gallery"
        ],
        "Recurring Profile" => [
            "is_recurring",
            "recurring_profile"
        ],
        "Design" => [
            "custom_design",
            "custom_design_from",
            "custom_design_to",
            "custom_layout_update",
            "page_layout",
            "options_container"
        ],
        "Gift Options" => [
            "gift_message_available"
        ],
        "Adyen" => [
            "adyen_pre_order"
        ]
    ]
);

foreach ($attributeSets as $setCode => $attributes) {
    foreach ($attributes as $groupName => $attribute) {
        foreach ($attribute as $attributeMap) {
            $attributeSetId = $installer->getAttributeSet($entityTypeId, $setCode, "attribute_set_id");
            $installer->addAttributeToSet($entityTypeId, $attributeSetId, $groupName, $attributeMap);
        }
    }
}
$installer->endSetup();
