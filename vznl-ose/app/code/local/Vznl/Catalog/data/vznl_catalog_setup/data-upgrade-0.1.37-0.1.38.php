<?php
/* Remove promotion_label attribute from Mobile promotion attribute set*/

/** @var Mage_Catalog_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();

/** @var Varien_Db_Adapter_Interface $connection */
$connection = $this->getConnection();
$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

$attributeCode = 'promotion_label';
$attributeSetName = 'promotion';

$attributeId = Mage::getModel('eav/entity_attribute')->loadByCode($entityTypeId, $attributeCode)->getId();
$attributeSet = $installer->getAttributeSet($entityTypeId, $attributeSetName);
if ($attributeId && !empty($attributeSet)) {
    $installer->deleteTableRow(
        'eav/entity_attribute',
        'attribute_id',
        $attributeId,
        'attribute_set_id',
        $attributeSet['attribute_set_id']
    );
}

$installer->endSetup();