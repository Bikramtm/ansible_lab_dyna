<?php
/* @var $installer Mage_Catalog_Model_Resource_Setup */

$installer = $this;

$installer->startSetup();

/*** Update produt catalog attribute*/
$installer->updateAttribute('catalog_product', 'maf', 'backend_type', Varien_Db_Ddl_Table::TYPE_DECIMAL);
$installer->updateAttribute('catalog_product', 'vat', 'backend_type', 'int');

$installer->endSetup();