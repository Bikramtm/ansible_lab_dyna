<?php
/**
 * Add attributes to all the fixed attribute sets except promotion attribute set
 */
/* @var $installer Mage_Sales_Model_Entity_Setup */

$installer = $this;
$installer->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

$attributeSets = array(
    "accessoire" => [
        "General" => [
            "promo_description"
        ]
    ],
    "addon" => [
        "General" => [
            "addon_type",
            "identifier_commitment_months"
        ]
    ],
    "voice_subscription" => [
        "Features" => [
            "prodspecs_brand",
            "ask_confirmation_first",
            "checkout_product",
            "product_type",
            "product_version",
            "contract_value",
            "display_name_cart",
            "display_name_communication",
            "display_name_configurator",
            "display_name_inventory",
            "hierarchy_value",
            "lifecycle_status"
        ]
    ]
);

foreach ($attributeSets as $setCode => $attributes) {
    foreach ($attributes as $groupName => $attribute) {
        foreach ($attribute as $attributeMap) {
            $attributeSetId = $installer->getAttributeSet($entityTypeId, $setCode, "attribute_set_id");
            $installer->addAttributeToSet($entityTypeId, $attributeSetId, $groupName, $attributeMap);
        }
    }
}
$installer->endSetup();