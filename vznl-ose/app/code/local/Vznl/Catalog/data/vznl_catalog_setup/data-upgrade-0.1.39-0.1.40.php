<?php
$this->startSetup();

/** @var Varien_Db_Adapter_Interface $connection */
$connection = $this->getConnection();

/** delete attribute entities where attribute group doesn't exist */
$deleteAttrGroupsSql = "DELETE FROM eav_entity_attribute WHERE attribute_group_id NOT IN (SELECT attribute_group_id FROM eav_attribute_group);";
$connection->query($deleteAttrGroupsSql);

/** delete attribute entities where attribute id doesn't exist */
$deleteAttrIdSql = "DELETE FROM eav_entity_attribute WHERE attribute_id NOT IN (SELECT attribute_id FROM eav_attribute);";
$connection->query($deleteAttrIdSql);

$this->endSetup();

