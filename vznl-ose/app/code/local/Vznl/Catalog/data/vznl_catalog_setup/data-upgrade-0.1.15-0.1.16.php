<?php
/**
 * Remove attributes from attributes sets.
 */
$installer = $this;
$installer->startSetup();
$attributeSetId = Mage::getModel('eav/entity_attribute_set')->getCollection()->addFieldToFilter('attribute_set_name', 'simcard')->getFirstItem()->getId();
if ($attributeSetId) {
    $productCollection = Mage::getModel('catalog/product')
        ->getCollection()
        ->addAttributeToSelect('*')
        ->addFieldToFilter('attribute_set_id', $attributeSetId);

    // set SimCard for package_subtype
    $attribute = Mage::getModel('eav/config')->getAttribute( Mage_Catalog_Model_Product::ENTITY ,'package_subtype');
    $packageSubtypeOptions = $attribute->getSource()->getAllOptions();
    foreach ($packageSubtypeOptions as $option) {
        if ($option['label'] == 'Simcard') {
            foreach ($productCollection as $_product) {
                $_product->setPackageSubtype($option['value']);
                $_product->getResource()->saveAttribute($_product, 'package_subtype');
            }
        }
    }
}

$installer->endSetup();