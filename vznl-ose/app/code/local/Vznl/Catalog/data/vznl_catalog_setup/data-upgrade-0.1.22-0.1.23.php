<?php

/* @var $installer Mage_Catalog_Model_Resource_Eav_Mysql4_Setup */
$installer = $this;

$installer->startSetup();
$conn = Mage::getSingleton('core/resource')->getConnection('core_write');

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

// Remove Product Attribute
$installer->removeAttribute($entityTypeId, 'identifier_cons_bus');

$attribute = Mage::getModel('eav/config')->getAttribute($entityTypeId, 'product_segment');
$conn = Mage::getSingleton('core/resource')->getConnection('core_write');
$options = $attribute->getSource()->getAllOptions(false);
foreach ($options as $key => &$option) {
    $option['label'] = ucfirst($option['label']);
    $conn->update('eav_attribute_option_value', array('value' => ucfirst($option['label'])), 'option_id = "' . $option['value'] . '"');
}

$installer->endSetup();