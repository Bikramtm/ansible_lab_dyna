<?php
/**
 * Changes required for OMNVFNL-5743
 */
$this->startSetup();

/** @var Varien_Db_Adapter_Interface $connection */
$connection = $this->getConnection();

$packageTypesTable = $this->getTable('catalog_package_types');
$packageSubtypesTable = $this->getTable('catalog_package_subtypes');

/** Get Hardware package_type */
$packageTypeHardware = $connection->fetchRow("SELECT * FROM `{$packageTypesTable}` WHERE `package_code` = '" . Omnius_Catalog_Model_Type::TYPE_HARDWARE . "'");

if ($packageTypeHardware) {
    /** Get all subtypes of hardware package type */
    $selectSql = "SELECT * FROM `{$packageSubtypesTable}` WHERE `type_package_id` = " . $packageTypeHardware['entity_id'];
    $hardwareSubtypes = $connection->fetchAll($selectSql);

    foreach ($hardwareSubtypes as $hardwareSubtype) {
        /** Update Goody to Accessory */
        if ($hardwareSubtype['package_code'] == 'Goody') {
            $connection->query("UPDATE `{$packageSubtypesTable}` SET `package_code` = 'Accessory' WHERE `package_code` = 'Goody'");
        }
    }
}

$this->endSetup();

/** @var Mage_Catalog_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

$attributeSets = [
    'Fixed_Bundle_Product' => [
        'General' => [
            'sort' => 0,
            'attributes' => [
                'sku_type',
            ]
        ],
        'Fixed Attributes' => [
            'sort' => 1,
            'attributes' => [
                'bundle_hint',
                'bundle_identifier',
                'download_speed',
                'dtv_bundle_level',
                'dtv_bundle_type',
                'has_dtv',
                'has_fixed_tel',
                'has_internet',
                'identifier_required_footprint',
                'int_bundle_type',
                'is_ccr_benefit_eligible_part',
                'tv_box',
                'tv_box_type',
                'tv_nr_channels',
                'upload_speed',
                'voice_plan',
                'voice_product',
                'required_serviceability_items',
                'requires_serviceability',
                'template_360',
            ]
        ],
    ],
    'Fixed_Generic_Product' => [
        'General' => [
            'sort' => 0,
            'attributes' => [
            ]
        ],
        'Fixed Attributes' => [
            'sort' => 0,
            'attributes' => [
                'type_other_addons',
                'required_serviceability_items',
                'requires_serviceability',
                'template_360',
            ]
        ],
    ],
    'Fixed_Internet_Product' => [
        'General' => [
            'sort' => 0,
            'attributes' => [
            ]
        ],
        'Fixed Attributes' => [
            'sort' => 0,
            'attributes' => [
                'download_speed',
                'upload_speed',
                'identifier_required_footprint',
                'int_security_licenses',
                'int_security_licenses_tag',
                'is_ccr_benefit_product',
                'is_fixed_ip_tier',
                'required_serviceability_items',
                'requires_serviceability',
                'template_360',
            ]
        ],
    ],
    'Fixed_Promotions' => [
        'General' => [
            'sort' => 0,
            'attributes' => [
            ]
        ],
        'Fixed Attributes' => [
            'sort' => 0,
            'attributes' => [
                'prijs_maf_new_amount',
                'promo_new_price',
                'price_discount',
                'price_discount_percent',
                'maf_discount',
                'maf_discount_percent',
                'discount_period_amount',
                'discount_period',
                'binding_duration',
                'hide_original_price',
                'product_type',
                'checkout_product',
                'confirmation_text',
                'ask_confirmation_first',
            ]
        ],
    ],
    'Fixed_Tel_Product' => [
        'General' => [
            'sort' => 0,
            'attributes' => [
            ]
        ],
        'Fixed Attributes' => [
            'sort' => 0,
            'attributes' => [
                'identifier_required_footprint',
                'voice_plan',
                'voice_plan_applies_over',
                'voice_product',
                'required_serviceability_items',
                'requires_serviceability',
                'template_360',
            ]
        ],
    ],
    'Fixed_TV_Product' => [
        'General' => [
            'sort' => 0,
            'attributes' => [
            ]
        ],
        'Fixed Attributes' => [
            'sort' => 0,
            'attributes' => [
                'identifier_required_footprint',
                'is_ccr_benefit_product',
                'tv_box',
                'tv_box_type',
                'tv_nr_channels',
                'tv_content',
                'tv_product',
                'required_serviceability_items',
                'requires_serviceability',
                'template_360',
            ]
        ],
    ],
];

$commonGroupAttributes = [
    'General' => [
        'sku',
        'name',
        'package_type',
        'package_subtype',
        'price',
        'maf',
        'vat',
        'tax_class_id',
        'product_visibility',
        'visibility',
        'stack',
        'effective_date',
        'expiration_date',
        'lifecycle_status',
        'status',
        'is_deleted',
        'sorting',
        'additional_text',
    ],
    'Fixed Attributes' => [
        'contract_value',
        'display_name_cart',
        'display_name_communication',
        'display_name_configurator',
        'display_name_inventory',
        'hierarchy_value',
        'initial_selectable',
        'product_family',
        'product_segment',
        'product_version_id',
    ],
];

// add additional_text
$attr = Mage::getResourceModel('catalog/eav_attribute')->loadByCode($entityTypeId, 'additional_text');
if (!$attr->getId()) {
    $installer->addAttribute(
        $entityTypeId,
        'additional_text',
        [
            'label' => 'Additional text',
            'input' => 'text',
            'type' => 'text',
            'required' => false,
        ]
    );
}

// add maf_discount_percent
$attr = Mage::getResourceModel('catalog/eav_attribute')->loadByCode($entityTypeId, 'maf_discount_percent');
if (!$attr->getId()) {
    $installer->addAttribute(
        $entityTypeId,
        'maf_discount_percent',
        [
            'label' => 'Maf discount percent',
            'input' => 'text',
            'type' => 'int',
            'required'  => 0,
        ]
    );
}

// add price_discount_percent
$attr = Mage::getResourceModel('catalog/eav_attribute')->loadByCode($entityTypeId, 'price_discount_percent ');
if (!$attr->getId()) {
    $installer->addAttribute(
        $entityTypeId,
        'price_discount_percent ',
        [
            'label' => 'Price discount percent',
            'input' => 'text',
            'type' => 'int',
            'required'  => 0,
        ]
    );
}

// add binding_duration
$attr = Mage::getResourceModel('catalog/eav_attribute')->loadByCode($entityTypeId, 'binding_duration');
if (!$attr->getId()) {
    $installer->addAttribute(
        $entityTypeId,
        'binding_duration',
        [
            'type'      => 'int',
            'label'     => 'Binding duration',
            'input'     => 'select',
            'source'    => 'eav/entity_attribute_source_boolean',
            'global'    => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
            'required'  => 0,
        ]
    );
}

foreach ($attributeSets as $attributeSetName => $attributeSetGroups) {
    // load current attribute set
    $attributeSet = $installer->getAttributeSet($entityTypeId, $attributeSetName);

    // re-add attribute set groups
    foreach ($attributeSetGroups as $groupName => $groupAttributes) {
        $data = array_merge($commonGroupAttributes[$groupName], $groupAttributes['attributes']);

        /** @var Mage_Eav_Model_Entity_Setup $currentGroup */
        $currentGroup = $installer->getAttributeGroup($entityTypeId, $attributeSet['attribute_set_id'], $groupName);
        if (!$currentGroup) {
            $installer->addAttributeGroup(
                $entityTypeId,
                $attributeSet['attribute_set_id'],
                $groupName,
                $groupAttributes['sort']
            );

            $currentGroup = $installer->getAttributeGroup($entityTypeId, $attributeSet['attribute_set_id'], $groupName);
        }

        $currentGroupAttributes = Mage::getResourceModel('catalog/product_attribute_collection')
            ->setAttributeGroupFilter($currentGroup['attribute_group_id'])
            ->load();

        foreach ($currentGroupAttributes as $currentAttribute) {
            if (!in_array($currentAttribute->getName(), $data)) {
                // removed unused attributes
                $installer->deleteTableRow(
                    'eav/entity_attribute',
                    'attribute_id',
                    $currentAttribute->getId(),
                    'attribute_set_id',
                    $attributeSet['attribute_set_id']
                );
            } else {
                // update used attributes
                addAttributeToGroupByCode(
                    $installer,
                    $attributeSet['attribute_set_id'],
                    $currentGroup['attribute_group_id'],
                    $currentAttribute->getName(),
                    array_search($currentAttribute->getName(), $data)
                );
            }
        }

        // add missing attributes
        $currentGroupAttributes = Mage::getResourceModel('catalog/product_attribute_collection')
            ->setAttributeGroupFilter($currentGroup['attribute_group_id'])
            ->getData();

        $currentGroupAttributeNames = array_map(function ($g) {
            return $g['attribute_code'];
        }, $currentGroupAttributes);

        foreach ($data as $sort => $attributeCode) {
            if (!in_array($attributeCode, $currentGroupAttributeNames)) {
                addAttributeToGroupByCode(
                    $installer,
                    $attributeSet['attribute_set_id'],
                    $currentGroup['attribute_group_id'],
                    $attributeCode,
                    $sort
                );
            }
        }
    }
}

$installer->endSetup();

function addAttributeToGroupByCode($installer, $attributeSetId, $groupId, $attributeCode, $sortOrder)
{
    $attributeId = Mage::getModel('eav/entity_attribute')->loadByCode(
        Mage_Catalog_Model_Product::ENTITY,
        $attributeCode
    )->getId();
    $installer->addAttributeToGroup(
        Mage_Catalog_Model_Product::ENTITY,
        $attributeSetId,
        $groupId,
        $attributeId,
        $sortOrder
    );
}
