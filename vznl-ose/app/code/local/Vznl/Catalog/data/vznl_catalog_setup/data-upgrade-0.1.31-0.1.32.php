<?php
/** @var Mage_Catalog_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();

$installer->defaultGroupIdAssociations = null;

/** @var Varien_Db_Adapter_Interface $connection */
$connection = $this->getConnection();
$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

$attributeSets = [
    'voice_device' => [
        'General' => [
            'sort' => 1,
            'attributes' => [
                'maf',
                'vat',
                'effective_date',
                'checkout_product',

                'hierarchy_value',
                'contract_value',
                'lifecycle_status',
                'display_name_inventory',
                'display_name_configurator',
                'display_name_cart',
                'display_name_communication',
                'price_increase_maf',
                'price_increase_scope',
                'product_type',
                'confirmation_text',
                'ask_confirmation_first',
                'product_version',
                'product_family',
            ],
            'attributesToRemove' => [
                'hide_original_price',
                'maf_discount_percent',
                'price_discount_percent',
            ],
        ],
        'Features' => [
            'sort' => 3,
            'attributes' => [
                'prodspecs_data_aantal_mb',
                'prodspecs_sms_amount',
                'prodspecs_soccode',
                'prodspecs_belmin_sms_config',
                'prodspecs_data_mb_config',
                'prodspecs_gprs_edge',
                'prodspecs_hawaii_speed_max',
                'prodspecs_vfwallet',
                'prodspecs_aantal_belminuten',
            ],
            'attributesToRemove' => [
                'prodspecs_network',
                'prodspecs_network_2g',
                'prodspecs_network_3g',
                'prodspecs_network_4g',
            ],
        ],
    ],
    'data_device' => [
        'General' => [
            'sort' => 1,
            'attributes' => [
                'maf',
                'vat',
                'effective_date',
                'checkout_product',

                'hierarchy_value',
                'contract_value',
                'lifecycle_status',
                'display_name_inventory',
                'display_name_configurator',
                'display_name_cart',
                'display_name_communication',
                'price_increase_maf',
                'price_increase_scope',
                'product_type',
                'confirmation_text',
                'ask_confirmation_first',
                'product_version',
                'product_family',
            ],
            'attributesToRemove' => [
                'hide_original_price',
                'maf_discount_percent',
                'price_discount_percent',
            ],
        ],
        'Features' => [
            'sort' => 3,
            'attributes' => [
                'prodspecs_data_aantal_mb',
                'prodspecs_sms_amount',
                'prodspecs_soccode',
                'prodspecs_belmin_sms_config',
                'prodspecs_data_mb_config',
                'prodspecs_gprs_edge',
                'prodspecs_hawaii_speed_max',
                'prodspecs_vfwallet',
                'prodspecs_aantal_belminuten',
                'prodspecs_spreektijd',
            ],
            'attributesToRemove' => [
                'prodspecs_network',
                'prodspecs_network_2g',
                'prodspecs_network_3g',
                'prodspecs_network_4g',
                'prodspecs_opslagcapaciteit',
            ],
        ],
    ]
];

foreach ($attributeSets as $attributeSetName => $attributeSetGroups) {
    // load current attribute set
    $attributeSet = $installer->getAttributeSet($entityTypeId, $attributeSetName);

    foreach ($attributeSetGroups as $groupName => $groupData) {
        /** @var Mage_Eav_Model_Entity_Setup $currentGroup */
        $currentGroup = $installer->getAttributeGroup($entityTypeId, $attributeSet['attribute_set_id'], $groupName);

        if (!$currentGroup) {
            $installer->addAttributeGroup($entityTypeId, $attributeSet['attribute_set_id'], $groupName, $groupData['sort']);

            $currentGroup = $installer->getAttributeGroup($entityTypeId, $attributeSet['attribute_set_id'], $groupName);
        }

        //get a list of all attribute codes of the current group
        $currentGroupAttributes = Mage::getResourceModel('catalog/product_attribute_collection')
            ->setAttributeGroupFilter($currentGroup['attribute_group_id']);
        $currentGroupAttributes->getSelect()->reset(Zend_Db_Select::COLUMNS);
        $currentGroupAttributes->addFieldToSelect('attribute_code');
        $currentGroupAttributes = $connection->fetchCol($currentGroupAttributes->getSelect());

        foreach ($groupData['attributes'] as $attributeCode) {
            //verify if attributeCode is in current group
            if (!in_array($attributeCode, $currentGroupAttributes)) {
                $attributeId = Mage::getModel('eav/entity_attribute')->loadByCode($entityTypeId, $attributeCode)->getId();
                //assign attribute to current group
                $installer->addAttributeToGroup($entityTypeId, $attributeSet['attribute_set_id'], $currentGroup['attribute_group_id'], $attributeId);
            }
        }

        // removed unused attributes from attribute set group
        if (isset($groupData['attributesToRemove'])) {
            foreach ($groupData['attributesToRemove'] as $attributeCode) {
                if (in_array($attributeCode, $currentGroupAttributes)) {
                    $attributeId = Mage::getModel('eav/entity_attribute')->loadByCode($entityTypeId, $attributeCode)->getId();
                    $installer->deleteTableRow(
                        'eav/entity_attribute',
                        'attribute_id',
                        $attributeId,
                        'attribute_set_id',
                        $attributeSet['attribute_set_id']
                    );
                }
            }
        }
    }
}

$this->endSetup();