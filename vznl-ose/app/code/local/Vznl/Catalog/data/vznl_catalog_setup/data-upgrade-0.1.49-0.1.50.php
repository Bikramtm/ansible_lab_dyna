<?php
/* @var $installer Mage_Sales_Model_Entity_Setup */

$installer = $this;
$installer->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

$code = 'prijs_aansluitkosten_ret';
$options = [
    'group' => 'General',
    'label' => 'Aansluitkosten Retentie',
    'input' => 'price',
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'required' => false,
    'filterable' => false,
    'apply_to' => 'simple',
    'user_defined' => 1
];

$attributeId = $installer->getAttributeId($entityTypeId, $code);
$attributeSets = [
    'voice_subscription',
    'data_subscription'
];

if (!$attributeId) {
    $installer->addAttribute($entityTypeId, $code, $options);
    foreach ($attributeSets as $setCode) {
        $attributeSetId = $installer->getAttributeSet($entityTypeId, $setCode, "attribute_set_id");
        $installer->addAttributeToSet($entityTypeId, $attributeSetId, 'General', $code);
    }
}

$installer->endSetup();