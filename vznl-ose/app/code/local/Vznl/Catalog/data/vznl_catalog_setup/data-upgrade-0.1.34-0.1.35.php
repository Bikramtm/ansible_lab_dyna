<?php
/**
 * Add attribute and attribute set for fixed products. These attributes are created for fixed products import, and will be
 * available in the configurator for creating fixed rules
 */
/* @var $installer Mage_Sales_Model_Entity_Setup */

$installer = $this;
$installer->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

// Create attributes
$attributes = [
    'serial_number_type' => [
        'group' => 'General',
        'label' => 'serial_number_type',
        'input' => 'multiselect',
        'type' => 'text',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'required' => true,
        'unique' => false,
        'filterable' => false,
        'apply_to' => '',
        'option' =>
            array(
                'values' =>
                    array(
                        0 => 'box_serial_number',
                        1 => 'smartcard_serial_number'
                    ),
            ),
        'default' => '',
    ],
    'capture_ean' => [
        'group' => 'General',
        'label' => 'capture_ean',
        'input' => 'boolean',
        'source' => 'eav/entity_attribute_source_boolean',
        'type' => 'int',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'required' => true,
        'unique' => false,
        'filterable' => false,
        'apply_to' => '',
    ],
];

foreach ($attributes as $code => $options) {
    $attributeId = $installer->getAttributeId(Mage_Catalog_Model_Product::ENTITY, $code);
    if (!$attributeId) {
        $options['user_defined'] = 1;
        $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, $code, $options);
    }
}
unset($attributes);

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

// Assign attribute to attribute set with group label
$attributeSets = array(
    "Fixed_Bundle_Product" => [
        "General" => [
            "axi_los_verkoopbaar", "identifier_axi_stockable_prod", "axi_safety_stock", "axi_threshold", "serial_number_type", "identifier_ean_or_upc_code", "capture_ean", "prodspecs_brand", "identifier_sap_codes", "new_price", "new_price_date"
        ]
    ],

    "Fixed_TV_Product" => [
        "General" => [
            "axi_los_verkoopbaar", "identifier_axi_stockable_prod", "axi_safety_stock", "axi_threshold", "serial_number_type", "identifier_ean_or_upc_code", "capture_ean", "prodspecs_brand", "identifier_sap_codes", "new_price", "new_price_date"
        ]
    ],

    "Fixed_Internet_Product" => [
        "General" => [
            "axi_los_verkoopbaar", "identifier_axi_stockable_prod", "axi_safety_stock", "axi_threshold", "serial_number_type", "identifier_ean_or_upc_code", "capture_ean", "prodspecs_brand", "identifier_sap_codes", "new_price", "new_price_date"
        ]
    ],

    "Fixed_Tel_Product" => [
        "General" => [
            "axi_los_verkoopbaar", "identifier_axi_stockable_prod", "axi_safety_stock", "axi_threshold", "serial_number_type", "identifier_ean_or_upc_code", "capture_ean", "prodspecs_brand", "identifier_sap_codes", "new_price", "new_price_date"
        ]
    ],

    "Fixed_Generic_Product" => [
        "General" => [
            "axi_los_verkoopbaar", "identifier_axi_stockable_prod", "axi_safety_stock", "axi_threshold", "serial_number_type", "identifier_ean_or_upc_code", "capture_ean", "prodspecs_brand", "identifier_sap_codes", "new_price", "new_price_date"
        ]
    ],

    "Fixed_Promotions" => [
        "General" => [
            "axi_los_verkoopbaar", "identifier_axi_stockable_prod", "axi_safety_stock", "axi_threshold", "serial_number_type", "identifier_ean_or_upc_code", "capture_ean", "prodspecs_brand", "identifier_sap_codes", "new_price", "new_price_date"
        ]
    ]
);

foreach ($attributeSets as $setCode => $attributes) {
    foreach ($attributes as $groupName => $attribute) {
        foreach ($attribute as $attributeMap) {
            $attributeSetId = $installer->getAttributeSet($entityTypeId, $setCode, "attribute_set_id");
            $installer->addAttributeToSet($entityTypeId, $attributeSetId, $groupName, $attributeMap);
        }
    }
}

$arg_attribute = 'package_subtype';
$arg_value = 'HARDWARE_MATERIALS';

$attr_model = Mage::getModel('catalog/resource_eav_attribute');
$attr = $attr_model->loadByCode('catalog_product', $arg_attribute);
$attr_id = $attr->getAttributeId();

$option['attribute_id'] = $attr_id;
$option['value']['any_option_name'][0] = $arg_value;

$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
$setup->addAttributeOption($option);

$installer->endSetup();