<?php
/** Remove "Fixed Attributes" group from mobile attribute sets */

/* @var $installer Mage_Sales_Model_Entity_Setup */
$installer = $this;
$installer->startSetup();

/** @var Varien_Db_Adapter_Interface $connection */
$connection = $installer->getConnection();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

// mobile attribute sets
$mobileAttributeSetsCodes = array(
    "accessoire",
    "addon",
    "addon_hybride",
    "data_device",
    "data_subscription",
    "default",
    "deviceRC",
    "dummy_device",
    "prepaid_simonly",
    "promotion",
    "service_item",
    "simcard",
    "voice_device",
    "voice_subscription",
);

$attributeGroupName = "Fixed Attributes";

foreach ($mobileAttributeSetsCodes as $code) {
    $attributeSetId = $installer->getAttributeSet($entityTypeId, $code, "attribute_set_id");

    $attributeGroup = $installer->getAttributeGroup($entityTypeId, $attributeSetId, $attributeGroupName);
    if ($attributeGroup && $attributeGroup['attribute_group_id']) {
        $installer->removeAttributeGroup($entityTypeId, $attributeSetId, $attributeGroupName);
    }
}

$installer->endSetup();



