<?php

$installer = $this;
$installer->startSetup();

/* Update Lifecycle in sales rules's conditions (NL to DE values) */
$conn = Mage::getSingleton('core/resource')->getConnection('core_write');

$sql = "SELECT `rule_id`, `conditions_serialized` FROM `salesrule`";
$salesRules = $conn->fetchAll($sql);

foreach ($salesRules as $salesRule) {
    // unserialize conditions
    $conditions = unserialize($salesRule['conditions_serialized']);
    if (!empty($conditions['conditions'])) {
        // search for acquisitie (NL) and replace it with ACQ (DE)
        if (($searchedValue = array_search(Vznl_Checkout_Model_Sales_Quote_Item::ACQUISITIE, array_column($conditions['conditions'],'value'))) !== false) {
            $conditions['conditions'][$searchedValue]['value'] = Dyna_Checkout_Model_Sales_Quote_Item::ACQUISITION;
        }
        // search for retentie (NL) and replace it with RETBLU (DE)
        if (($searchedValue = array_search(Vznl_Checkout_Model_Sales_Quote_Item::RETENTIE, array_column($conditions['conditions'],'value'))) !== false) {
            $conditions['conditions'][$searchedValue]['value'] = Dyna_Checkout_Model_Sales_Quote_Item::RETENTION;
        }
    }
    // serialize them back in order to update the table
    $serializedConditions = serialize($conditions);

    $conn->update($this->getTable('salesrule'),
        array('conditions_serialized' => $serializedConditions),
        array('rule_id =?' => $salesRule['rule_id']));
}

/* Update promotions with Mobile package type and Addon package subtype */
$attributeSetId = Mage::getModel('eav/entity_attribute_set')
    ->load('promotion', 'attribute_set_name')
    ->getAttributeSetId();

// get promo products
$products = Mage::getModel('catalog/product')
    ->getCollection()
    ->addFieldToFilter('attribute_set_id', $attributeSetId);

foreach($products as $product) {
    $product = Mage::getModel('catalog/product')->load($product->getId());
    // set Mobile for package_type
    $attribute = Mage::getModel('eav/config')->getAttribute( Mage_Catalog_Model_Product::ENTITY ,'package_type');
    $packageTypeOptions = $attribute->getSource()->getAllOptions();
    foreach ($packageTypeOptions as $option) {
        if ($option['label'] == 'Mobile') {
            $product->setPackageType($option['value']);
            $product->getResource()->saveAttribute($product, 'package_type');
        }
    }
    // set Addon for package_subtype
    $attribute = Mage::getModel('eav/config')->getAttribute( Mage_Catalog_Model_Product::ENTITY ,'package_subtype');
    $packageSubtypeOptions = $attribute->getSource()->getAllOptions();
    foreach ($packageSubtypeOptions as $option) {
        if ($option['label'] == 'Addon') {
            $product->setPackageSubtype($option['value']);
            $product->getResource()->saveAttribute($product, 'package_subtype');
        }
    }
}

$installer->endSetup();