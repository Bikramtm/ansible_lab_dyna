<?php

$data_entries = array();

$data_entries['accessoire'] = $data_entries['prepaid_simonly'] = $data_entries['service_item'] = $data_entries['simcard'] = array(
    'sku','name','description','package_type_id','package_subtype_id','vat','status','product_visibility','maf',
    'prodspecs_brand','prodspecs_hawaii_channel','prodspecs_hawaii_usp1','prodspecs_hawaii_usp2','prodspecs_hawaii_usp3',
    'sorting','effective_date','expiration_date','ask_confirmation_first','axi_los_verkoopbaar','identifier_axi_stockable_prod',
    'channel','checkout_product','confirmation_text','contract_value','display_name_cart','display_name_communication',
    'display_name_configurator','display_name_inventory','hierarchy_value','identifier_hawaii_id','initial_selectable',
    'lifecycle_status','new_price','new_price_date','price','price_increase_maf','price_increase_scope','prodspecs_wms_codes',
    'product_family_id','product_segment','product_type','product_version_id','special_from_date','special_price',
    'special_to_date','stack','tags','use_service_value','visibility','additional_text','strategy','group_ids',
    'axi_safety_stock','axi_threshold','prodspecs_kleur','prodspecs_weight_gram','identifier_ean_or_upc_code','stock_status',
    'prodspecs_hawaii_htmlkleurcode','meta_description','promo_description','short_description','identifier_simcard_selector');

$data_entries['addon'] = array(
    'sku','name','description','package_type_id','package_subtype_id','vat','status','product_visibility','maf',
    'prijs_aansluitkosten','prodspecs_soccode','prodspecs_4g_lte','prodspecs_aantal_belminuten','prodspecs_brand',
    'prodspecs_data_aantal_mb','prodspecs_hawaii_channel','prodspecs_hawaii_usp1','prodspecs_hawaii_usp2',
    'prodspecs_hawaii_usp3','prodspecs_sms_amount','sorting','effective_date','expiration_date','ask_confirmation_first',
    'axi_los_verkoopbaar','identifier_axi_stockable_prod','channel','checkout_product','confirmation_text','contract_value',
    'display_name_cart','display_name_communication','display_name_configurator','display_name_inventory','hierarchy_value',
    'identifier_hawaii_id','initial_selectable','lifecycle_status','new_price','new_price_date','price','price_increase_maf',
    'price_increase_scope','prijs_mb_buiten_bundel','prijs_sms_buiten_bundel','prodspecs_wms_codes','product_family_id',
    'product_segment','product_type','product_version_id','special_from_date','special_price','special_to_date','stack','tags',
    'tax_class_id','use_service_value','visibility','additional_text','strategy','group_ids','identifier_ean_or_upc_code',
    'identifier_hawaii_type','url_path','addon_type','identifier_commitment_months'
);

$data_entries['deviceRC'] = array(
    'sku','name','description','package_type_id','package_subtype_id','vat','status','product_visibility','maf',
    'prodspecs_soccode','prodspecs_hawaii_channel','prodspecs_hawaii_usp1','prodspecs_hawaii_usp2','prodspecs_hawaii_usp3',
    'sorting','effective_date','expiration_date','ask_confirmation_first','axi_los_verkoopbaar','identifier_axi_stockable_prod',
    'channel','checkout_product','confirmation_text','contract_value','display_name_cart','display_name_communication',
    'display_name_configurator','display_name_inventory','hierarchy_value','identifier_hawaii_id','initial_selectable',
    'lifecycle_status','new_price','new_price_date','price','price_increase_maf','price_increase_scope','product_family_id',
    'product_segment','product_type','product_version_id','stack','tags','use_service_value','additional_text','strategy',
    'group_ids','identifier_ean_or_upc_code','identifier_hawaii_type','loan_indicator','identifier_commitment_months'
);

$data_entries['voice_device'] = $data_entries['data_device'] = $data_entries['dummy_device'] = array(
    'sku','name','description','package_type_id','package_subtype_id','vat','status','product_visibility','maf','prodspecs_soccode',
    'prodspecs_4g_lte','prodspecs_aantal_belminuten','prodspecs_brand','prodspecs_data_aantal_mb','prodspecs_hawaii_channel',
    'prodspecs_hawaii_usp1','prodspecs_hawaii_usp2','prodspecs_hawaii_usp3','prodspecs_sms_amount','sorting','effective_date',
    'expiration_date','ask_confirmation_first','axi_los_verkoopbaar','identifier_axi_stockable_prod','channel','checkout_product',
    'confirmation_text','contract_value','display_name_cart','display_name_communication','display_name_configurator',
    'display_name_inventory','hierarchy_value','identifier_hawaii_id','initial_selectable','lifecycle_status','new_price',
    'new_price_date','price','price_increase_maf','price_increase_scope','prodspecs_wms_codes','product_family_id',
    'product_segment','product_type','product_version_id','special_from_date','special_price','special_to_date','stack',
    'tags','use_service_value','visibility','strategy','group_ids','axi_safety_stock','axi_threshold','hawaii_def_subscr_cbu',
    'hawaii_def_subscr_ebu','identifier_ean_or_upc_code','identifier_hawaii_type','identifier_hawaii_type_name',
    'prijs_thuiskopieheffing_type','prodspecs_2e_camera','prodspecs_3g','prodspecs_accu_verwisselb','prodspecs_afmetingen_breedte',
    'prodspecs_afmetingen_dikte','prodspecs_afmetingen_lengte','prodspecs_afmetingen_unit','prodspecs_audio_out','prodspecs_batterij',
    'prodspecs_belmin_sms_config','prodspecs_besturingssysteem','prodspecs_bluetooth','prodspecs_bluetooth_version','prodspecs_camera',
    'prodspecs_data_mb_config','prodspecs_display_features','prodspecs_filmen_in_hd','prodspecs_flitser','prodspecs_gprs_edge',
    'prodspecs_gps','prodspecs_hawaii_htmlkleurcode','prodspecs_hawaii_os_versie','prodspecs_hawaii_preorder','prodspecs_hawaii_speed_max',
    'prodspecs_hdmi','prodspecs_kleur','prodspecs_megapixels_1e_cam','prodspecs_nfc','prodspecs_opslag_gb','prodspecs_opslag_uitbreidbaar',
    'prodspecs_prepaid_simlock','prodspecs_processor','prodspecs_schermdiagonaal_inch','prodspecs_schermresolutie','prodspecs_spreektijd',
    'prodspecs_standby_tijd','prodspecs_touchscreen','prodspecs_vfwallet','prodspecs_weight_gram','prodspecs_werkgeheugen_mb',
    'prodspecs_wifi','prodspecs_wifi_frequenties','sap_code','stock_status','additional_text'
);

$data_entries['promotion'] = array(
    'sku','name','description','package_type_id','package_subtype_id','vat','status','product_visibility','maf',
    'prijs_aansluitkosten','prodspecs_soccode','prodspecs_hawaii_channel','prodspecs_hawaii_usp1','prodspecs_hawaii_usp2',
    'prodspecs_hawaii_usp3','sorting','effective_date','expiration_date','ask_confirmation_first','axi_los_verkoopbaar',
    'identifier_axi_stockable_prod','channel','checkout_product','confirmation_text','contract_value','display_name_cart',
    'display_name_communication','display_name_configurator','display_name_inventory','hierarchy_value','identifier_hawaii_id',
    'initial_selectable','lifecycle_status','new_price','new_price_date','price','price_increase_maf','price_increase_scope',
    'product_family_id','product_segment','product_type','product_version_id','special_from_date','special_price',
    'special_to_date','stack','tags','use_service_value','additional_text','identifier_hawaii_type','maf_discount',
    'maf_discount_percent','percentagepromotion','price_discount','price_discount_percent','prijs_aansluit_promo_bedrag',
    'prijs_aansluit_promo_procent','prijs_promo_duur_maanden','discount_period','discount_period_amount','hide_original_price',
    'prijs_maf_new_amount','promo_new_price','promotion_label'
);

$data_entries['voice_subscription'] = $data_entries['data_subscription'] = array(
    'sku','name','description','package_type_id','package_subtype_id','vat','status','product_visibility','maf',
    'prijs_aansluitkosten','prodspecs_soccode','prodspecs_4g_lte','prodspecs_aantal_belminuten','prodspecs_brand',
    'prodspecs_data_aantal_mb','prodspecs_hawaii_channel','prodspecs_hawaii_def_phone','prodspecs_hawaii_usp1',
    'prodspecs_hawaii_usp2','prodspecs_hawaii_usp3','prodspecs_network','prodspecs_sms_amount','sorting','effective_date',
    'expiration_date','ask_confirmation_first','axi_los_verkoopbaar','identifier_axi_stockable_prod','channel',
    'checkout_product','confirmation_text','contract_value','display_name_cart','display_name_communication',
    'display_name_configurator','display_name_inventory','hawaii_subscr_device_price','hawaii_subscr_family',
    'hawaii_subscr_kind','hierarchy_value','identifier_hawaii_id','identifier_hawaii_subscr_orig',
    'identifier_subsc_postp_simonly','identifier_subscr_brand','identifier_subscr_category','identifier_subscr_type',
    'initial_selectable','lifecycle_status','new_price','new_price_date','price','price_increase_maf','price_increase_scope',
    'prijs_belminuut_buiten_bundel','prijs_mb_buiten_bundel','prijs_sms_buiten_bundel','prodspecs_wms_codes','product_family_id',
    'product_segment','product_type','product_version_id','special_from_date','special_price','special_to_date','stack',
    'tags','tax_class_id','use_service_value','visibility','additional_text','strategy','group_ids','identifier_simcard_allowed','aikido'
);

$this->startSetup();

/** @var Varien_Db_Adapter_Interface $connection */
$connection = $this->getConnection();

//identify product entity type
$entityTypeId = Mage::getModel('eav/entity_type')->loadByCode('catalog_product')->getId();

//first identify the General group id for each attribute set
$attrSetGroup = array();
$attrGroups = Mage::getModel('eav/entity_attribute_group')
    ->getCollection()
    ->addFieldToFilter('attribute_group_name', 'General');
foreach($attrGroups as $attrGroup){
    $attrSetGroup[$attrGroup->getAttributeSetId()] = $attrGroup->getAttributeGroupId();
}

//get all attributes sets to be able to skip the fixed ones
$attributeSetCollection = Mage::getResourceModel('eav/entity_attribute_set_collection') ->load();
$attributeSetNames = array();
foreach($attributeSetCollection as $attributeSet){
    if(strpos($attributeSet->getAttributeSetName(), 'Fixed_') !== 0){
        $attributeSetNames[$attributeSet->getAttributeSetName()] = $attributeSet->getId();
    }
}

$allAttributes = Mage::getResourceModel('eav/entity_attribute_collection')->setEntityTypeFilter($entityTypeId);
$attributes = array();
foreach($allAttributes as $attribute){
    $attributes[$attribute->getAttributeCode()] = $attribute->getId();
}

foreach($data_entries as $attributeSetName => $attributeNames) {
    if(!isset($attributeSetNames[$attributeSetName])){
        continue;
    }
    $attributeSetId = $attributeSetNames[$attributeSetName];
    foreach($attributeNames as $attributeName){
        if(!isset($attributes[$attributeName])){
            continue;
        }
        if(!isset($attrSetGroup[$attributeSetId])){
            continue;
        }
        $newItem = Mage::getModel('eav/entity_attribute');
        $newItem
            ->setEntityTypeId($entityTypeId)
            ->setAttributeSetId($attributeSetId)
            ->setAttributeGroupId($attrSetGroup[$attributeSetId])
            ->setAttributeId($attributes[$attributeName])
            ->save();
    }
}

$this->endSetup();

