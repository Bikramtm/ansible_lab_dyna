<?php
/* @var $this Mage_Sales_Model_Entity_Setup */
$this->startSetup();

$entityTypeId = Mage_Catalog_Model_Category::ENTITY;
$attributeCode = 'hide_not_selected_products';

$attribute = Mage::getResourceModel('catalog/eav_attribute')->loadByCode($entityTypeId, $attributeCode);

if (!$attribute->getId()) {
    $this->addAttribute($entityTypeId, $attributeCode, array(
        'group' => 'General Information',
        'sort_order' => 3,
        'input' => 'select',
        'source' => 'eav/entity_attribute_source_boolean',
        'type' => 'int',
        'label' => 'Hide products from the family category if at least one is selected',
        'backend' => '',
        'visible' => true,
        'required' => false,
        'visible_on_front' => true,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    ));
}

$this->endSetup();
