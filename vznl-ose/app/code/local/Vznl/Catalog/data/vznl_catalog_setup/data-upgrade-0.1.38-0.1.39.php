<?php
$installer = $this;

$installer->startSetup();
$installer->getConnection();
$entityTypeId = Mage_Catalog_Model_Product::ENTITY;
$installer->removeAttribute($entityTypeId, 'vat');
$this->endSetup();