<?php

class Vznl_Catalog_Block_Adminhtml_Catalogupload_Edit extends Dyna_Catalog_Block_Adminhtml_Catalogupload_Edit
{
    protected $_objectId = 'id';
    protected $_formScripts = [];
    protected $_formInitScripts = [];
    protected $_mode = 'edit';
    protected $_blockGroup = 'dyna_catalog';

    protected $_controller = 'adminhtml_catalogupload';

    public function __construct()
    {
        parent::__construct();

        $this->_updateButton('save', 'label', $this->__('Upload files'));
        $this->_removeButton('delete');
        $this->_removeButton('reset');
    }

    public function getHeaderText()
    {
        return Mage::helper('job')->__('Catalog upload');
    }

    public function getPostMaxSize()
    {
        return ini_get('post_max_size');
    }

    public function getUploadMaxSize()
    {
        return ini_get('upload_max_filesize');
    }

    public function getDataMaxSize()
    {
        return min($this->getPostMaxSize(), $this->getUploadMaxSize());
    }
}
