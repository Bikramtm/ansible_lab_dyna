<?php

class Vznl_Catalog_Block_Adminhtml_LifecycleRenderer extends Dyna_Catalog_Block_Adminhtml_LifecycleRenderer
{
    /**
     * Renders grid column for package type visibility
     * @param   Varien_Object $row
     * @return  string
     */
    public function render(Varien_Object $row)
    {
        $lifeCycleValue = $row->getLifecycleStatus();
        $lifeCycleValues = Dyna_Catalog_Model_Lifecycle::getLifecycleTypes();
        return  in_array($lifeCycleValue,$lifeCycleValues)?$lifeCycleValue: "-";
    }
}
