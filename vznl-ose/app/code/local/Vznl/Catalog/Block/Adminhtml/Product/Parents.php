<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */

/**
 * Class Vznl_Catalog_Block_Adminhtml_Product_Parents
 */
class Vznl_Catalog_Block_Adminhtml_Product_Parents extends Omnius_Catalog_Block_Adminhtml_Product_Parents implements Mage_Adminhtml_Block_Widget_Tab_Interface
{

    /**
     * @return string
     */
    public function getTabTitle()
    {
        return Mage::helper('omnius_catalog')->__('Parent Products');
    }

    /**
     * @return string
     */
    public function getTabLabel()
    {
        return Mage::helper('omnius_catalog')->__('Parent Products');
    }

    /**
     * @return bool
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * @return bool
     */
    public function canShowTab()
    {
        return true;
    }
}
