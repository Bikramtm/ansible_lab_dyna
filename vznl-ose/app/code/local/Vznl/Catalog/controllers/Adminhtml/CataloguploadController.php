<?php

require_once ('Dyna/Catalog/controllers/Adminhtml/CataloguploadController.php');

class Vznl_Catalog_Adminhtml_CataloguploadController extends Dyna_Catalog_Adminhtml_CataloguploadController
{
    const ALLOWED_FILES = [
        'Mobile_Package_Type.csv',
        'Mobile_PackageCreation.csv',
        'Mobile_Accessory.csv',
        'Mobile_Addon.csv',
        'Mobile_Devices.csv',
        'Mobile_DeviceRC.csv',
        'Mobile_Promotions.csv',
        'Mobile_Subscription.csv',
        'Mobile_Multimapper_BSL.csv',
        'Mobile_Mixmatch.csv',
        'Mobile_CategoryTree.csv',
        'Mobile_Categories.csv',
        'Mobile_Comp_Rules.csv',
        'Mobile_Sales_Rules.csv',
        'Mobile_ProductFamily.csv',
        'Mobile_ProductVersion.csv',
        'Mobile_Version.xml',
        'Campaigns.xml',
        'Campaign_Offers.xml',
        'Campaign_Version.xml',
        'Fixed_Package_Type.csv',
        'Fixed_PackageCreation.csv',
        'Fixed_Bundle_Products.csv',
        'Fixed_Gen_Products.csv',
        'Fixed_Int_Products.csv',
        'Fixed_Tel_Products.csv',
        'Fixed_TV_Products.csv',
        'Fixed_Promotions.csv',
        'Fixed_Multimapper_peal.csv',
        'Fixed_CategoryTree.csv',
        'Fixed_Categories.csv',
        'Fixed_Comp_Rules.csv',
        'Fixed_Sales_Rules.csv',
        'Fixed_ProductFamily.csv',
        'Fixed_ProductVersion.csv',
        'Fixed_ProductFamily.csv',
        'Fixed_Version.xml',
        'Bundle.xml',
    ];

    public function uploadAction()
    {
        try {
            if ($postData = $this->getRequest()->getPost()) {
                $uploader = Mage::getModel('core/file_uploader', 'file');
                $uploader->setAllowedExtensions(['zip']);
                $path = Mage::app()->getConfig()->getTempVarDir().'/import/';

                // Empty directory first
                $file = new Varien_Io_File();
                $file->open(array('path' => $path));
                foreach ($file->ls() as $ref) {
                    if ($ref['leaf']) {
                        $file->rm($ref['text']);
                    }
                }
                $isAllowedFile = true;

                $uploader->save($path);
                if ($uploadFile = $uploader->getUploadedFileName()) {
                    $newFilename = 'import-'.date('YmdHis').'_'.$uploadFile;
                    $file->mv($path.$uploadFile, $path.$newFilename);

                    // Unzip the file
                    $zip = new ZipArchive;
                    $zip->open($path.$newFilename);
                    for ($i = 0; $i < $zip->numFiles; $i++) {
                        $stat          = $zip->statIndex($i);
                        $filename      = $stat['name'];
                        $isAllowedFile = $this->validateFileName($filename);
                    }
                    if (!$isAllowedFile) {
                        $this->_redirect('*/*');
                    }

                    $zip->extractTo($path);
                    $zip->close();

                    $file->rm($path.$newFilename);
                }
                if (isset($newFilename) && $newFilename && $isAllowedFile) {
                    Mage::getSingleton('adminhtml/session')->addSuccess(
                        $this->__('Files are uploaded succesfully and will be processed on the next run')
                    );
                    $this->_redirect('*/*');
                }
            } else {
                Mage::getSingleton('adminhtml/session')->addError(
                    $this->__('Invalid POST data (please check post_max_size and upload_max_filesize settings in your php.ini file).')
                );
                $this->_redirect('*/*');
            }
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError(
                $e->getMessage()
            );
            $this->_redirect('*/*');
        }
    }

    private function validateFileName($filename)
    {
        if (!in_array($filename, self::ALLOWED_FILES)) {
            Mage::getSingleton('adminhtml/session')->addError(
                $this->__("Not allowed filename in ZIP archive: $filename")
            );
            return false;
        } else {
            return true;
        }
    }

}