<?php

class Vznl_FFHandler_Helper_Request extends Mage_Core_Helper_Data
{
    /**
     * @param $requestEmail
     * @param array $authValue
     * @param Vznl_FFManager_Model_Relation $relation
     * @return Vznl_FFHandler_Model_Request
     */
    public function generateValidByEmail($requestEmail, $authValue, Vznl_FFManager_Model_Relation $relation)
    {
        $requestModel = $this->buildRequest($requestEmail, $authValue, $relation);

        Mage::log('[Helper request][generateValidByEmail] Request to be created auth type  = ' . Vznl_FFHandler_Model_Request::CUSTOMER_AUTH_TYPE_EMAIL);
        $requestModel->setCustomerAuthType(Vznl_FFHandler_Model_Request::CUSTOMER_AUTH_TYPE_EMAIL);
        $requestModel->save();

        return $requestModel;
    }

    public function generateGenericCode(Vznl_FFManager_Model_Relation $relation)
    {
        switch ($relation->getAuthType()) {
            case Vznl_FFManager_Model_Relation::AUTH_TYPE_CODE_EMAIL_EXT:
            case Vznl_FFManager_Model_Relation::AUTH_TYPE_CODE_EMAIL:
                $requestModel = $this->buildRequest(Vznl_FFHandler_Model_Request::SALES_MANAGER_GENERATED_VALUE, [], $relation);
                Mage::log('[Helper request][generateGenericCode] Request to be created auth type  = ' . Vznl_FFHandler_Model_Request::CUSTOMER_AUTH_TYPE_EMAIL);
                $requestModel->setCustomerAuthType(Vznl_FFHandler_Model_Request::CUSTOMER_AUTH_TYPE_EMAIL);
                break;
            case Vznl_FFManager_Model_Relation::AUTH_TYPE_CODE_CUSTOMER_REF:
                $requestAuthValues = $this->getArrayOfStringsFromRelationAuthValuesModels($relation);
                $requestModel = $this->buildRequest(Vznl_FFHandler_Model_Request::SALES_MANAGER_GENERATED_VALUE, $requestAuthValues, $relation);
                Mage::log('[Helper request][generateGenericCode] Request to be created auth type  = ' . Vznl_FFHandler_Model_Request::CUSTOMER_AUTH_TYPE_REFERENCE);
                $requestModel->setCustomerAuthType(Vznl_FFHandler_Model_Request::CUSTOMER_AUTH_TYPE_REFERENCE);
                break;
            default:
                $requestModel = $this->buildRequest(Vznl_FFHandler_Model_Request::SALES_MANAGER_GENERATED_VALUE, [], $relation);
                Mage::log('[Helper request][generateGenericCode] Request to be created auth type  = ' . Vznl_FFHandler_Model_Request::CUSTOMER_AUTH_TYPE_GENERIC_CODE);
                $requestModel->setCustomerAuthType(Vznl_FFHandler_Model_Request::CUSTOMER_AUTH_TYPE_GENERIC_CODE);
                break;
        }
        $requestModel->complete();
        return $requestModel;
    }

    public function generateFailedRequesterAlreadyBlocked($requesterEmail, $customerAuthValue)
    {
        Mage::log('[Helper request][generateFailedRequesterAlreadyBlocked] Already Blocking request with customer auth value= ' . $customerAuthValue);
        Mage::log('[Helper request][generateFailedRequesterAlreadyBlocked] requesterEmail= ' . $requesterEmail);
        /**
         * @var Vznl_FFHandler_Model_Request $requestModel
         */
        $requestModel = Mage::getModel("ffhandler/request");

        $requestModel->setCustomerAuthType(Vznl_FFHandler_Model_Request::CUSTOMER_AUTH_TYPE_EMAIL);
        $requestModel->setRequesterEmail($requesterEmail);
        $requestModel->setCustomerAuthValues([$customerAuthValue]);

        $requestModel->setStatus(Vznl_FFHandler_Model_Request::STATUS_FAILED_ALREADY_BLOCKED);

        Mage::log('[Helper request][generateFailedRequesterAlreadyBlocked] Already Blocking request ' . var_export($requestModel->toArray(), true));
        $requestModel->save();

        return $requestModel;
    }

    public function generateFailedNoRelation($requesterEmail, $customerAuthValue)
    {
        Mage::log('[Helper request][generateFailedNoRelation] Customer auth value= ' . $customerAuthValue);
        Mage::log('[Helper request][generateFailedNoRelation] requesterEmail = ' . $requesterEmail);
        /**
         * @var Vznl_FFHandler_Model_Request $requestModel
         */
        $requestModel = Mage::getModel("ffhandler/request");

        $requestModel->setRequesterEmail($requesterEmail);
        $requestModel->setCustomerAuthType(Vznl_FFHandler_Model_Request::CUSTOMER_AUTH_TYPE_EMAIL);
        $requestModel->setCustomerAuthValues([$customerAuthValue]);

        $requestModel->setStatus(Vznl_FFHandler_Model_Request::STATUS_FAILED_NO_RELATION);

        Mage::log('[Helper request][generateFailedNoRelation] Failed no relation request = ' . var_export($requestModel->toArray(), true));
        $requestModel->save();

        return $requestModel;
    }

    /**
     * Get relation by provided email
     *
     * @param $email
     * @return Vznl_FFManager_Model_Relation|null
     */
    public function searchRelationByEmail($email)
    {
        /**
         * @var Vznl_FFManager_Model_Resource_Relation_Collection $relationsCollection
         */
        $relationsCollection = Mage::getModel("ffmanager/relation")->getCollection();
        // First search the relation for a specific email
        Mage::log('[Helper request][searchRelationByEmail] filterActiveUnexpiredRelations by email. params to search. email =  ' . $email);
        $relationsCollection->filterActiveUnexpiredRelations(Vznl_FFManager_Model_Relation::AUTH_TYPE_CODE_EMAIL, $email);
        Mage::log('[Helper request][searchRelationByEmail] filterActiveUnexpiredRelations. no relations found =  ' . count($relationsCollection));

        /**
         * @var Vznl_FFManager_Model_Relation $relation
         */
        foreach ($relationsCollection as $relation) {
            // If any relation was found, return it, as it should be maximum 1
            Mage::log('[Helper request][searchRelationByEmail] filterActiveUnexpiredRelations. relation found by e-mail');
            return $relation;
        }

        /**
         * @var Vznl_FFManager_Helper_Data $dataHelper
         */
        $dataHelper = Mage::helper("ffmanager");

        /**
         * @var Vznl_FFManager_Model_Resource_Relation_Collection $relationsCollection
         */
        $relationsCollection = Mage::getModel("ffmanager/relation")->getCollection();
        // If no relation was found for a specific email, search by extension

        Mage::log('[Helper request][searchRelationByEmail] filterActiveUnexpiredRelations by extension. params to search. extension =  ' . $dataHelper->getDomainFromEmail($email));
        $relationsCollection->filterActiveUnexpiredRelations(
            Vznl_FFManager_Model_Relation::AUTH_TYPE_CODE_EMAIL_EXT,
            $dataHelper->getDomainFromEmail($email)
        );
        Mage::log('[Helper request][searchRelationByEmail] filterActiveUnexpiredRelations. no relations found =  ' . count($relationsCollection));


        /**
         * @var Vznl_FFManager_Model_Relation $relation
         */
        foreach ($relationsCollection as $relation) {
            // If any relation was found, return it, as it should be maximum 1
            Mage::log('[Helper request][searchRelationByEmail] filterActiveUnexpiredRelations. relation found by extension');
            return $relation;
        }

        /**
         * No unblocked relation was found for the specified relation
         */
        Mage::log('[Helper request][searchRelationByEmail] filterActiveUnexpiredRelations. no  unblocked relation was found for the specified relation');
        return null;
    }

    /**
     * @param  string $requestEmail
     * @param  array $customerAuthValue
     * @param  Vznl_FFManager_Model_Relation $relation
     * @return Vznl_FFHandler_Model_Request
     */
    protected function buildRequest($requestEmail, array $customerAuthValue, Vznl_FFManager_Model_Relation $relation)
    {
        /**
         * @var Vznl_FFHandler_Model_Request $requestModel
         */
        $requestModel = Mage::getModel("ffhandler/request");

        $requestModel->setRequesterEmail($requestEmail);
        $requestModel->setCustomerAuthValues($customerAuthValue);
        $requestModel->setRelation($relation);

        $requestModel->setUsagePerCode($relation->getUsagePerCode());
        $requestModel->setUsagePerCustomer($relation->getUsagePerCustomer());

        if ($relation->getValidDateFrom()) {
            $requestModel->setValidFrom($relation->getValidDateFrom()->getTimestamp());
        } else {
            $requestModel->setValidFrom(null);
        }
        if ($relation->getValidDateTo()) {
            $requestModel->setValidTo($relation->getValidDateTo()->getTimestamp());
        } else {
            $requestModel->setValidTo(null);
        }

        $requestModel->setStatus(Vznl_FFHandler_Model_Request::STATUS_WAITING);

        Mage::log('[Helper request][Build request] Building the request with requesterEmail = ' . $requestEmail);
        Mage::log('[Helper request][Build request] Building the request with customerAuthValue = ' . var_export($customerAuthValue, true));
        Mage::log('[Helper request][Build request] Building the request with relation = ' . var_export($relation->toArray(), true));
        Mage::log('[Helper request][Build request] Request to be created: ' . var_export($requestModel->toArray(), true));

        return $requestModel;
    }

    /**
     * Add block the email for this relation
     *
     * @param $requesterEmail
     * @return Vznl_FFManager_Model_Blocklist
     * @throws Exception
     */
    public function addToBlockList($requesterEmail)
    {
        /**
         * @var Vznl_FFManager_Model_Blocklist $blockListModel
         */
        $blockListModel = Mage::getModel("ffmanager/blocklist");
        Mage::log('[Helper request][addToBlockList] requesterEmail = ' . $requesterEmail);

        $blockListModel->setEmail($requesterEmail);
        $blockListModel->save();

        return $blockListModel;
    }

    /**
     * Returns true if request email was blocked because the threshold was exceeded, false otherwise
     * The sales manager is not affected by this
     *
     * @access public
     * @param Vznl_FFHandler_Model_Request $request
     * @return bool
     * @throws Exception
     */
    public function blockExceededThresholdRequest(Vznl_FFHandler_Model_Request $request)
    {
        Mage::log('[Helper request][blockExceededThresholdRequest] request = ' . var_export($request->toArray(), true));

        if (
            $request->getRequesterEmail() == Vznl_FFHandler_Model_Request::SALES_MANAGER_GENERATED_VALUE
            || !$request->getRelation()->limitExceeded($request->getRequesterEmail())
        ) {

            return false;
        }

        Mage::log('[Helper request][blockExceededThresholdRequest] Limit threshold exceeded. Request will be blocked.');
        $this->blockEmailAndSendNotificationMail($request);
        return true;
    }

    /**
     * Blocks the requester e-mail:
     * - adds new record in ff_blocked_emails
     * - changes the status to STATUS_FAILED_BLOCKED
     * - notifies through an e-mail sent to the requester_email that the e-mail address has been blocked
     *
     * @access public
     * @param Vznl_FFHandler_Model_Request $request
     * @throws Exception
     */
    public function blockEmailAndSendNotificationMail($request)
    {
        /**
         * @var Vznl_FFHandler_Helper_Request $requestHelper
         * @var Dyna_FFReport_Helper_Email $emailHelper
         */
        $requestHelper = Mage::helper("ffhandler/request");
        $emailHelper = Mage::helper("vznl_ffreport/email");

        $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
        try {
            Mage::log('[Helper request][blockEmailAndSendNotificationMail] initial request = ' . var_export($request->toArray(), true));
            $connection->beginTransaction();

            $salesManager = $request->getRelation()->getCreatedBy();
            $adminEmail = $salesManager ? $salesManager->getEmail() : null;

            $requestHelper->addToBlockList($request->getRequesterEmail());
            Mage::log('[Helper request][addToBlockList] requester e-mail ' . $request->getRequesterEmail() . ' blocked');
            Mage::log('[Helper request][addToBlockList] sending e-mail blocked. requesterEmail = ' . $request->getRequesterEmail() . 'adminEmail = ' . $adminEmail);

            $emailHelper->emailBlocked($request->getRequesterEmail(), $adminEmail);

            $request->block();
            Mage::log('[Helper request][updateRequest] Request blocked. Status is ' . Vznl_FFHandler_Model_Request::STATUS_FAILED_BLOCKED);

            $connection->commit();
        } catch (Exception $e) {

            $connection->rollback();
            Mage::log('[Helper request][blockEmailAndSendNotificationMail] failed = ' . $e->getMessage());
            throw new Exception('Could not block e-mail.');
        }
    }

    /**
     * Confirms the email:
     * - sets status to completed
     * - sets the generated code
     * - sends notification e-mail
     *
     * @access public
     * @param Vznl_FFHandler_Model_Request $request
     * @throws Exception
     */
    public function confirmEmailAndSendNotificationMail($request)
    {
        /**
         * @var Dyna_FFReport_Helper_Email $emailHelper
         */
        $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
        try {
            Mage::log('[Helper request][confirmEmailAndSendNotificationMail] initial request = ' . var_export($request->toArray(), true));
            $connection->beginTransaction();

            $request->complete();
            Mage::log('[Helper request][confirmEmailAndSendNotificationMail] request completed = ' . var_export($request->toArray(), true));

            $emailHelper = Mage::helper("dyna_ffreport/email");
            Mage::log('[Helper request][updateRequest] Sending ffCodeGenerated e-mail');
            $emailHelper->ffCodeGenerated($request);

            $connection->commit();
        } catch (Exception $e) {

            $connection->rollback();
            Mage::log('[Helper request][confirmEmailAndSendNotificationMail] failed = ' . $e->getMessage());
            throw new Exception('Could not confirm e-mail. Please try again later.');
        }
    }

    public function getClientEmailFromQuote(Mage_Sales_Model_Quote $quote)
    {
        $customer = Mage::getModel('customer/customer')->load($quote->getCustomer()->getId());

        if ($customer->getId()) {
            $emails = $customer->getAdditionalEmail();
        } else {
            $emails = $quote->getAdditionalEmail();
        }
        $emails = explode(';', $emails);

        return $emails[0];
    }

    public function getClientReferenceFromQuote(Mage_Sales_Model_Quote $quote)
    {
        return $quote->getFfCustomerReference();
    }

    /**
     * Returns false if exists already a request created with the given code
     *
     * @access public
     * @param string $code
     * @param int|null $itself
     * @return bool
     */
    public function validateUniqueFFCode($code, $itself = null)
    {
        /**
         * @var Vznl_FFHandler_Model_Request $requestModel
         */

        $requestModel = Mage::getModel("ffhandler/request")->loadByCode($code);
        $coupon = Mage::getModel('salesrule/coupon')->load($code, 'code');

        if ($coupon->getId()) {
            // If a sales rule with this coupon name exists, do not create it
            return false;
        }

        if (!$requestModel->getId()) {
            Mage::log('[Helper request][validateUniqueFFCode] Code is unique since no request was found for the code ' . $code);
            return true;
        }

        if (!$requestModel->getRelationId() || $requestModel->getRelationId() != $itself) {
            Mage::log('[Helper request][validateUniqueFFCode] Code ' . $code . ' is not unique. Relation id = ' . $requestModel->getRelationId() . 'itself id = ' . $itself);
            return false;
        }

        Mage::log('[Helper request][validateUniqueFFCode] Code is unique');
        return true;
    }

    /**
     * Update an existing request with  new values
     *
     * @access public
     * @param Vznl_FFHandler_Model_Request $requestModel
     * @param Vznl_FFManager_Model_Relation $relation
     * @return Vznl_FFHandler_Model_Request
     */
    public function updateRequest(Vznl_FFHandler_Model_Request $requestModel, Vznl_FFManager_Model_Relation $relation)
    {
        Mage::log('[Helper request][updateRequest] Initial request = ' . var_export($requestModel->toArray(), true));
        Mage::log('[Helper request][updateRequest] Relation = ' . var_export($relation->toArray(), true));

        $requestAuthValues = $this->getArrayOfStringsFromRelationAuthValuesModels($relation);
        Mage::log('[Helper request][updateRequest] Relation auth values = ' . var_export($requestAuthValues, true));

        $requestModel->setCustomerAuthValues($requestAuthValues);
        $requestModel->setUsagePerCode($relation->getUsagePerCode());
        $requestModel->setUsagePerCustomer($relation->getUsagePerCustomer());
        if ($relation->getValidDateFrom()) {
            $requestModel->setValidFrom($relation->getValidDateFrom()->getTimestamp());
        } else {
            $requestModel->setValidFrom(null);
        }
        if ($relation->getValidDateTo()) {
            $requestModel->setValidTo($relation->getValidDateTo()->getTimestamp());
        } else {
            $requestModel->setValidTo(null);
        }

        Mage::log('[Helper request][updateRequest] Updated request = ' . var_export($requestModel->toArray(), true));
        Mage::log('[Helper request][updateRequest] Updated request customer values = ' . var_export($requestModel->getCustomerAuthValues(), true));

        return $requestModel;
    }

    /**
     * Get the auth values for a relation as a an array of strings instead of array of objects Vznl_FFManager_Model_Relation_Auth_Value
     *
     * @access private
     * @param Vznl_FFManager_Model_Relation $relation
     * @return array
     */
    private function getArrayOfStringsFromRelationAuthValuesModels($relation)
    {
        $arrayRequestValues = [];
        $requestAuthValues = $relation->getAuthValues();
        foreach ($requestAuthValues as $key => $requestAuthValue) {
            /**
             * @var Vznl_FFManager_Model_Relation_Auth_Value $requestAuthValue
             */
            $arrayRequestValues[$key] = $requestAuthValue->getValue();
        }

        return $arrayRequestValues;
    }


    public function updateFFCodeUsageForPackage(Dyna_Package_Model_Package $package, Mage_Sales_Model_Quote $quote = null)
    {
        $this->removeCodeUsageByPackage($package);
        $this->addCodeUsage($package, $quote);
    }

    /**
     * @param Dyna_Package_Model_Package $package
     * @param Mage_Sales_Model_Quote|null $quote -- this is used for caching purpose,
     *      to prevent calls to database when the quote is already available to the caller
     * @throws Exception
     */
    public function addCodeUsage(Dyna_Package_Model_Package $package, Mage_Sales_Model_Quote $quote = null)
    {
        if (!$quote) {
            $quote = Mage::getModel("sales/quote")->load($package->getQuoteId());
        }

        /**
         * @var Vznl_FFHandler_Model_Request $requestModel
         */
        $requestModel = Mage::getModel("ffhandler/request");

        $requestModel->loadByCode($package->getCoupon());

        if (!$requestModel->getId()) {
            // When the ffCode saved to quote does not exist, this should never happen
            return;
        }

        /**
         * @var Vznl_FFHandler_Model_Request_Usage $requestUsageModel
         */
        $requestUsageModel = Mage::getModel("ffhandler/request_usage");
        $requestUsageModel->setRequest($requestModel);
        $requestUsageModel->setQuote($quote);
        $requestUsageModel->setPackage($package);
        $requestUsageModel->setClient($this->getClientEmailFromQuote($quote));
        $requestUsageModel->save();
    }

    public function removeCodeUsageByPackagesIds(array $packageIds)
    {
        /** @var Vznl_FFHandler_Model_Resource_Request_Usage_Collection $requestUsageCollection */
        $requestUsageCollection = Mage::getModel("ffhandler/request_usage")->getCollection();
        $requestUsageCollection->addFieldToFilter('package_id', array('in' => $packageIds));

        /** @var Vznl_FFHandler_Model_Request_Usage $requestUsage */
        foreach ($requestUsageCollection as $requestUsage) {
            $requestUsage->delete();
        }
    }

    /**
     * Adds usage for all packages of a given quote
     *
     * @param Dyna_Checkout_Model_Sales_Quote $quote
     * @param null $packages -- this is used only for caching purpose,
     *      it must be the collection of packages for the provided quote,
     */
    public function addCodeUsageForQuote(Dyna_Checkout_Model_Sales_Quote $quote, $packages = null)
    {
        /** @var Dyna_Package_Model_Mysql4_Package_Collection $packages */
        if (!$packages) {
            $packages = Mage::getModel('package/package')->getCollection();
            $packages->addFieldToFilter('quote_id', $quote->getId());
        }

        /** @var Dyna_Package_Model_Package $package */
        foreach ($packages as $package) {
            if (!$package->getCoupon() || !$package->getIsFfCode()) {
                continue;
            }

            $this->addCodeUsage($package, $quote);
        }
    }

    public function removeCodeUsageByPackage(Dyna_Package_Model_Package $package)
    {
        /** @var Vznl_FFHandler_Model_Request_Usage $requestUsageModel */
        $requestUsageModel = Mage::getModel("ffhandler/request_usage");
        $requestUsageModel->load($package->getId(), 'package_id');

        if (!$requestUsageModel->getId()) {
            return;
        }

        $requestUsageModel->delete();
    }

    public function removeCodeUsageByQuote(Mage_Sales_Model_Quote $quote)
    {
        /**
         * @var Vznl_FFHandler_Model_Resource_Request_Usage_Collection $requestUsageCollection
         */
        $requestUsageCollection = Mage::getModel("ffhandler/request_usage")->getCollection();
        $requestUsageCollection->addFieldToFilter('quote_id', $quote->getId());

        /** @var Vznl_FFHandler_Model_Request_Usage $requestUsage */
        foreach ($requestUsageCollection as $requestUsage) {
            $requestUsage->delete();
        }
    }

    /**
     * @param int $quoteId
     * @param int $orderId
     * @throws Exception
     */
    public function updateUsageToSuperorder($quoteId, $orderId)
    {
        /** @var Vznl_FFHandler_Model_Resource_Request_Usage_Collection $requestUsageCollection */
        $requestUsageCollection = Mage::getModel("ffhandler/request_usage")->getCollection();
        $requestUsageCollection->addFieldToFilter('quote_id', $quoteId);

        /** @var Vznl_FFHandler_Model_Request_Usage $requestUsage */
        foreach ($requestUsageCollection as $requestUsage) {
            $requestUsage->setQuoteId(null);
            $requestUsage->setOrderId($orderId);

            $requestUsage->save();
        }
    }
}