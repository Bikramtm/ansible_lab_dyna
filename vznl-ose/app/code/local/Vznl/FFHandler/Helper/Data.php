<?php

/**
 * Class Vznl_FFHandler_Helper_Data
 */
class Vznl_FFHandler_Helper_Data extends Mage_Core_Helper_Data
{
    public function getRequestStatusTypes()
    {
        return array(
            Vznl_FFHandler_Model_Request::STATUS_WAITING => $this->__('Waiting'),
            Vznl_FFHandler_Model_Request::STATUS_COMPLETED => $this->__('Completed'),
            Vznl_FFHandler_Model_Request::STATUS_CANCELED => $this->__('Canceled'),
            Vznl_FFHandler_Model_Request::STATUS_FAILED_BLOCKED => $this->__('Failed blocked'),
            Vznl_FFHandler_Model_Request::STATUS_FAILED_ALREADY_BLOCKED => $this->__('Failed already blocked'),
            Vznl_FFHandler_Model_Request::STATUS_FAILED_NO_RELATION => $this->__('Failed no relation')
        );
    }

    public function getCustomerAuthTypes()
    {
        return array(
            Vznl_FFHandler_Model_Request::CUSTOMER_AUTH_TYPE_EMAIL => $this->__('Unique Email'),
            Vznl_FFHandler_Model_Request::CUSTOMER_AUTH_TYPE_REFERENCE => $this->__('Customer reference'),
            Vznl_FFHandler_Model_Request::CUSTOMER_AUTH_TYPE_GENERIC_CODE => $this->__('General code')
        );
    }
}