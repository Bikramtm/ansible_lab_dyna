<?php

/**
 * Class Vznl_FFHandler_Model_Request
 *
 * @method string getRequesterEmail()
 * @method string getCustomerAuthType()
 * @method int getRelationId()
 * @method string getConfirmationHash()
 * @method int getStatus()
 * @method int getCouponId()
 * @method string getCode()
 * @method string getUsagePerCode()
 * @method string getUsagePerCustomer()
 * @method bool getThresholdCountable()
 * @method int getValidFrom()
 * @method int getValidTo()
 * @method int getCreatedAt()
 * @method int getUpdatedAt()
 * @method Vznl_FFHandler_Model_Request setRequesterEmail(string $requesterEmail)
 * @method Vznl_FFHandler_Model_Request setCustomerAuthType(string $authType)
 * @method Vznl_FFHandler_Model_Request setRelationId(int $relationId)
 * @method Vznl_FFHandler_Model_Request setConfirmationHash(string $confirmationHash)
 * @method Vznl_FFHandler_Model_Request setStatus(int $status)
 * @method Vznl_FFHandler_Model_Request setCouponId(int $couponId)
 * @method Vznl_FFHandler_Model_Request setCode(int $code)
 * @method Vznl_FFHandler_Model_Request setUsagePerCode(int $usagePerCode)
 * @method Vznl_FFHandler_Model_Request setUsagePerCustomer(int $usagePerCustomer)
 * @method Vznl_FFHandler_Model_Request setThresholdCountable(int $thresholdCountable)
 * @method Vznl_FFHandler_Model_Request setValidFrom(int $validFrom)
 * @method Vznl_FFHandler_Model_Request setValidTo(int $validTo)
 * @method Vznl_FFHandler_Model_Request setCreatedAt(int $createdAt)
 * @method Vznl_FFHandler_Model_Request setUpdatedAt(int $updatedAt)
 */
class Vznl_FFHandler_Model_Request extends Mage_Core_Model_Abstract
{
    const STATUS_WAITING = 1;
    const STATUS_COMPLETED = 2;
    const STATUS_CANCELED = 3;
    const STATUS_FAILED_BLOCKED = 4;
    const STATUS_FAILED_ALREADY_BLOCKED = 5;
    const STATUS_FAILED_NO_RELATION = 6;

    const CUSTOMER_AUTH_TYPE_EMAIL = 1;
    const CUSTOMER_AUTH_TYPE_REFERENCE = 2;
    const CUSTOMER_AUTH_TYPE_GENERIC_CODE = 3;

    const SALES_MANAGER_GENERATED_VALUE = "_SALES_MANAGER_";

    /**
     * @var Vznl_FFManager_Model_Relation
     */
    protected $relation;

    /**
     * @var array|null
     */
    protected $usage;

    protected $customerAuthValues;
    protected $unsavedCustomerAuthValues = array();

    protected function _construct()
    {
        $this->_init("ffhandler/request");
    }

    protected function _beforeSave()
    {
        $dateTime = Zend_Date::now();
        $this->setUpdatedAt($dateTime->getTimestamp());
        if (!$this->getId()) {

            $this->setCreatedAt($dateTime->getTimestamp());
            $this->setConfirmationHash(bin2hex(openssl_random_pseudo_bytes(32)));
        }
        return parent::_beforeSave();
    }
    public function save()
    {
        parent::save();

        /**
         * Save all customer auth values that were added before the request was saved
         *
         * @var Vznl_FFHandler_Model_Request_Customer_Auth_Value $customerAuthValue
         */
        foreach ($this->unsavedCustomerAuthValues as $customerAuthValue) {
            // Set the current request and save the value
            $customerAuthValue->setRequest($this);
            $customerAuthValue->save();

            if ($this->customerAuthValues) {
                $this->customerAuthValues[] = $customerAuthValue;
            }
        }
        $this->unsavedCustomerAuthValues = array();
        return $this;
    }

    /**
     * Return the status description
     *
     * @return null|string
     */
    public function getStatusDescription()
    {
        switch ($this->getStatus()) {
            case self::STATUS_WAITING:
                return "Waiting";
            case self::STATUS_COMPLETED:
                return "Completed";
            case self::STATUS_CANCELED:
                return "Cancelled by requester";
            case self::STATUS_FAILED_BLOCKED:
                return "Failed - requester was blocked";
            case self::STATUS_FAILED_ALREADY_BLOCKED:
                return "Failed - requester already blocked";
            case self::STATUS_FAILED_NO_RELATION:
                return "Failed - no relation was found";
        }

        return null;
    }

    public function getValidDateFrom()
    {
        $date = new Zend_Date($this->getValidFrom(), "YYYY-MM-dd");
        $date->setTime("00:00:00", "HH:mm:ss");
        return $date;
    }

    public function getValidDateTo()
    {
        $date = new Zend_Date($this->getValidTo(), "YYYY-MM-dd");
        $date->setTime("23:59:59", "HH:mm:ss");
        return $date;
    }

    public function loadByConfirmationHash($confirmationHash)
    {
        return $this->load($confirmationHash, "confirmation_hash");
    }

    public function loadByCode($code)
    {
        return $this->load($code, "code");
    }

    /**
     * @return Vznl_FFManager_Model_Relation
     */
    public function getRelation()
    {
        if (!$this->getRelationId()) {
            Mage::log('Relation id not found');
            return null;
        }

        if (!$this->relation) {
            /**
             * @var Vznl_FFManager_Model_Relation $relationModel
             */
            $relationModel = Mage::getModel("ffmanager/relation");
            $relationModel->load($this->getRelationId());
            $this->relation = $relationModel;
        }

        return $this->relation;
    }

    public function setRelation(Mage_Core_Model_Abstract $relation)
    {
        $this->relation = $relation;

        return $this->setRelationId($relation->getId());
    }

    public function isValid()
    {
        if (!$this->getValidFrom() && !$this->getValidTo()) {
            return true;
        }
        $crtTime = time();

        $validFrom = strtotime($this->getValidDateFrom());
        $validTo = strtotime($this->getValidDateTo());

        //verify crt date is between valid_from and valid_to
        if ( $crtTime >= $validFrom && $crtTime <= $validTo ) {
            return true;
        }

        return false;
    }

    public function canBeUsedByCustomer($authValue, $packageId)
    {
        if (in_array($this->getCustomerAuthType(), array(self::CUSTOMER_AUTH_TYPE_EMAIL, self::CUSTOMER_AUTH_TYPE_REFERENCE))) {
            // If the email is required for authentication
            $isCustomerAuthorised = $this->hasCustomerAuthValue($authValue);
        } else {
            // If the authentication type generic, do not check for anything as this is a public code
            $isCustomerAuthorised = true;
        }

        if (!$isCustomerAuthorised) {
            Mage::log(sprintf("Customer is not allowed to use this request. Customer - %s; Request - %s", $authValue, $this->getId()));
        }

        if (!$this->getEnabled()) {
            Mage::log(sprintf("The request is disabled. Customer - %s; Request - %s", $authValue, $this->getId()));
        }
        if (!$this->isValid()) {
            Mage::log(sprintf("The request is expired. Customer - %s; Request - %s", $authValue, $this->getId()));
        }
        if (!(!$this->getUsagePerCode() || $this->getUsagePerCode() > $this->getRealCodeUsageNr($packageId))) {
            Mage::log(sprintf("Exceeded request usage. Customer - %s; Request - %s", $authValue, $this->getId()));
        }
        if (!(!$this->getUsagePerCustomer() || $this->getUsagePerCustomer() > $this->getRealCustomerUsageNr($authValue, $packageId))) {
            Mage::log(sprintf("Exceeded customer usage. Customer - %s; Request - %s", $authValue, $this->getId()));
        }

        if (!($this->getStatus() == self::STATUS_COMPLETED)) {
            Mage::log(sprintf("Request does not have the correct status. Current status - %s; Customer - %s; Request - %s", $this->getStatus(), $authValue, $this->getId()));
        }

        return $this->canBeUsed($packageId)
        && (!$this->getUsagePerCustomer() || $this->getUsagePerCustomer() > $this->getRealCustomerUsageNr($authValue, $packageId)) // Customer did not exceed the maximum usage
        && $isCustomerAuthorised; // Is authorised
    }

    public function canBeUsedByQuote(Mage_Sales_Model_Quote $quote, $packageId)
    {
        if (!$this->getId()) {
            return false;
        }

        /**
         * @var Vznl_FFHandler_Helper_Request $requestHelper
         */
        $requestHelper = Mage::helper("ffhandler/request");

        if ($this->getCustomerAuthType() == self::CUSTOMER_AUTH_TYPE_EMAIL) {
            // If the email is required for authentication
            $authValue = $requestHelper->getClientEmailFromQuote($quote);
        } else if ($this->getCustomerAuthType() == self::CUSTOMER_AUTH_TYPE_REFERENCE) {
            // If the reference is required for authentication
            $authValue = $requestHelper->getClientReferenceFromQuote($quote);
        } else {
            // If the authentication type generic, do not check for anything as this is a public code
            $authValue = '';
        }

        return $this->canBeUsedByCustomer($authValue, $packageId);
    }

    public function canBeUsed($packageId = null)
    {
        return $this->getId()
        && $this->getEnabled() // Relation is enabled for this code
        && $this->isValid() // Request is not expired
        && (!$this->getUsagePerCode() || $this->getUsagePerCode() > $this->getRealCodeUsageNr($packageId)) // Request did not exceed the maximum usage
        && $this->getStatus() == self::STATUS_COMPLETED; // Has the correct status
    }

    public function complete()
    {
        $relation = $this->getRelation();
        if (!$relation) {
            Mage::log(sprintf('Cannot complete an request #%s. No relation found', $this->getId()));
            throw new Exception("Cannot complete an request without a relation");
        }

        Mage::log('[Model request][complete] Completing Request for relation  = '. var_export($relation->toArray(), true));
        if ($relation->getAutoGenerate()) {
            $codeGenerator = Mage::getSingleton('salesrule/coupon_codegenerator', array('length' => 16));
            $code = $codeGenerator->generateCode();

            /**
             * @var Vznl_FFHandler_Helper_Request $requestHelper
             */
            $requestHelper = Mage::helper("ffhandler/request");

            while (!$requestHelper->validateUniqueFFCode($code)) {
                $code = $codeGenerator->generateCode();
            }
            Mage::log('[Model request][complete] Auto generating final code = '. $code);
        } else {
            $code = $relation->getCode();
            Mage::log('[Model request][complete] Using existing code = '. $code);
        }

        try {
            $this->setCode($code)
                ->setStatus(Vznl_FFHandler_Model_Request::STATUS_COMPLETED)
                ->save();

            Mage::log('[Model request][complete] Completed request = '. $code);
        }
        catch(Exception $e) {
            Mage::log(sprintf('Code could not be assigned for request #%d. %s',$this->getId(), $e->getMessage()));
            Mage::log($e->getTraceAsString());
            throw new Exception("Code could not be assigned");
        }
    }

    public function block()
    {
        $this->setStatus(Vznl_FFHandler_Model_Request::STATUS_FAILED_BLOCKED)
            ->save();
    }

    public function getEnabled()
    {
        if (!$this->getRelation()) {
            return false;
        }

        return $this->getRelation()->getEnabled();
    }

    /**
     * @return array
     */
    public function getUsage($packageId = null)
    {
        if (!$this->usage) {
            /**
             * @var Vznl_FFHandler_Model_Resource_Request_Usage_Collection $requestUsageCollection
             */
            $requestUsageCollection = Mage::getModel("ffhandler/request_usage")->getCollection();
            $requestUsageCollection->filterByRequest($this);
            if ($packageId) {
                $requestUsageCollection->addFieldToFilter('package_id', array('neq' => $packageId));
            }

            $this->usage = array();

            foreach($requestUsageCollection as $requestUsage) {
                $this->usage[] = $requestUsage;
            }
        }
        return $this->usage;
    }

    /**
     * @param $clients
     * @return $this
     */
    public function setUsage($clients)
    {
        $this->usage = array();
        foreach ($clients as $client) {
            $this->addUsage($client);
        }

        return $this;
    }

    /**
     * @param string $client
     * @return $this
     */
    public function addUsage($client)
    {
        /**
         * @var Vznl_FFHandler_Model_Request_Usage $requestUsage
         */
        $requestUsage = Mage::getModel("ffhandler/request_usage");

        $requestUsage->setRequest($this);
        $requestUsage->setClient($client);

        $requestUsage->save();

        if ($this->usage) {
            $this->usage[] = $requestUsage;
        }

        return $this;
    }

    public function getRealCodeUsageNr($packageId = null)
    {
        return count($this->getUsage($packageId));
    }

    public function getRealCustomerUsageNr($customer, $packageId)
    {
        /**
         * @var Vznl_FFHandler_Model_Resource_Request_Usage_Collection $requestUsageCollection
         */
        $requestUsageCollection = Mage::getModel("ffhandler/request_usage")->getCollection();
        $requestUsageCollection
            ->filterByRequestAndClient($this, $customer)
            ->addFieldToFilter('package_id', array('neq' => $packageId));

        return count($requestUsageCollection);
    }

    /**
     * Get all customer auth values
     * Returns a list of Vznl_FFHandler_Model_Request_Customer_Auth_Value models
     *
     * @return array
     */
    public function getCustomerAuthValues()
    {
        if (!$this->getId()) {
            return $this->unsavedCustomerAuthValues;
        }

        if (!$this->customerAuthValues) {
            $this->customerAuthValues = array();
            /**
             * @var Vznl_FFHandler_Model_Resource_Request_Customer_Auth_Value_Collection $customerAuthValueCollection
             */
            $customerAuthValueCollection = Mage::getModel("ffhandler/request_customer_auth_value")->getCollection();
            $customerAuthValueCollection->filterByRequestId($this->getId());

            foreach ($customerAuthValueCollection as $customerAuthValue) {
                $this->customerAuthValues[] = $customerAuthValue;
            }
        }

        return $this->customerAuthValues;
    }

    /**
     * Check if the request has the the provided auth
     * This will call getCustomerAuthValues therefore it will query all the customer auth values for this request
     *
     * @param string $customerAuthValue
     * @return bool
     */
    public function hasCustomerAuthValue($customerAuthValue)
    {
        $customerAuthValues = $this->getCustomerAuthValues();

        /**
         * @var Vznl_FFHandler_Model_Request_Customer_Auth_Value $customerAuthValueModel
         */
        foreach ($customerAuthValues as $customerAuthValueModel) {
            if ($customerAuthValue == $customerAuthValueModel->getValue()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Add a new customer auth value to the request
     *
     * @param string $value
     * @return $this
     */
    public function addCustomerAuthValue($value)
    {
        /**
         * @var Vznl_FFHandler_Model_Request_Customer_Auth_Value $customerAuthValue
         */
        $customerAuthValue = Mage::getModel("ffhandler/request_customer_auth_value");
        $customerAuthValue->setValue($value);

        if (!$this->getId()) {
            // If the request was not saved, add the customer auth value to unsaved array, they will not be added into DB until the request is saved
            $this->unsavedCustomerAuthValues[] = $customerAuthValue;
            return $this;
        }

        $customerAuthValue->setRequest($this);
        $customerAuthValue->save();

        if ($this->customerAuthValues) {
            // if customerAuthValues variable was initialised, probably because getCustomerAuthValues was called, add the new auth to the list

            $this->customerAuthValues[] = $customerAuthValue;
        }

        return $this;
    }

    /**
     * Set the auth values for a request
     * This will delete all previous auth values and add the new ones
     *
     * @param array $values [array of strings]
     * @return $this;
     */
    public function setCustomerAuthValues(array $values)
    {
        $customerAuthValues = $this->getCustomerAuthValues();

        /**
         * @var Vznl_FFHandler_Model_Request_Customer_Auth_Value $customerAuthValue
         */
        foreach ($customerAuthValues as $customerAuthValue) {
            $customerAuthValue->delete();
        }

        $this->customerAuthValues = array();

        foreach ($values as $value) {
            $this->addCustomerAuthValue($value);
        }

        return $this;
    }

    public function getPublicData()
    {
        $output = $this->getData();

        unset($output["relation_id"]);
        $output["statusDescription"] = $this->getStatusDescription();

        $customerAuthValues = $this->getCustomerAuthValues();
        $output["customerAuthValues"] = array();
        /**
         * @var Vznl_FFHandler_Model_Request_Customer_Auth_Value $customerAuthValue
         */
        foreach ($customerAuthValues as $customerAuthValue) {
            $output["customerAuthValues"][] = $customerAuthValue->getValue();
        }

        return $output;
    }
}
