<?php

/**
 * Class Vznl_FFHandler_Model_Request_Usage
 *
 * @method int getRequestId()
 * @method string getClient()
 * @method int getQuoteId()
 * @method int getOrderId()
 * @method int getPackageId()
 *
 * @method Vznl_FFHandler_Model_Request_Usage setRequestId(int $requestId)
 * @method Vznl_FFHandler_Model_Request_Usage setClient(string $client)
 * @method Vznl_FFHandler_Model_Request_Usage setQuoteId(string $quoteId)
 * @method Vznl_FFHandler_Model_Request_Usage setOrderId(string $orderId)
 * @method Vznl_FFHandler_Model_Request_Usage setPackageId(string $packageId)
 */
class Vznl_FFHandler_Model_Request_Usage extends Mage_Core_Model_Abstract
{
    /**
     * @var Vznl_FFHandler_Model_Request
     */
    protected $request;

    /**
     * @var Mage_Sales_Model_Quote
     */
    protected $quote;

    /**
     * @var Vznl_Package_Model_Package
     */
    protected $package;

    public function _construct()
    {
        $this->_init("ffhandler/request_usage");
    }

    /**
     * @param Vznl_FFHandler_Model_Request $request
     * @return $this
     */
    public function setRequest(Vznl_FFHandler_Model_Request $request)
    {
        $this->request = $request;
        $this->setRequestId($request->getId());
        return $this;
    }

    /**
     * @return Vznl_FFHandler_Model_Request
     */
    public function getRequest()
    {
        if (!$this->request) {
            $this->request = Mage::getModel("ffhandler/request")->load($this->getRequestId());
        }

        return $this->request;
    }

    /**
     * @param Vznl_Package_Model_Package $package
     * @return $this
     */
    public function setPackage(Vznl_Package_Model_Package $package)
    {
        $this->package = $package;
        $this->setPackageId($package->getId());
        return $this;
    }

    /**
     * @return Vznl_Package_Model_Package
     */
    public function getPackage()
    {
        if (!$this->package) {
            $this->package = Mage::getModel("package/package")->load($this->getPackageId());
        }

        return $this->package;
    }

    public function setQuote(Mage_Sales_Model_Quote $quote)
    {
        $this->quote = $quote;
        $this->setQuoteId($quote->getId());
        return $this;
    }

    /**
     * @return Mage_Sales_Model_Quote
     */
    public function getQuote()
    {
        if (!$this->quote) {
            $this->quote = Mage::getModel("sales/quote")->load($this->getQuoteId());
        }

        return $this->quote;
    }
}
