<?php

/**
 * Resource Model for Friends and Family Request Handlers
 *
 * @category    Vznl
 * @package     Vznl_FFHandler
 */
class Vznl_FFHandler_Model_Resource_Request_Usage_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    /**
     * Initialize resource
     *
     */
    protected function _construct()
    {
        $this->_init('ffhandler/request_usage');
    }

    public function filterByRequestId($requestId)
    {
        $this->addFieldToFilter("request_id", $requestId);
        return $this;
    }

    public function filterByRequest(Vznl_FFHandler_Model_Request $request)
    {
        $this->filterByRequestId($request->getId());
        return $this;
    }

    public function filterByRequestIdAndClient($requestId, $client)
    {
        $this->addFieldToFilter("request_id", $requestId);
        $this->addFieldToFilter("client", $client);
        return $this;
    }
    public function filterByRequestAndClient(Vznl_FFHandler_Model_Request $request, $client)
    {
        $this->filterByRequestIdAndClient($request->getId(), $client);
        return $this;
    }
}
