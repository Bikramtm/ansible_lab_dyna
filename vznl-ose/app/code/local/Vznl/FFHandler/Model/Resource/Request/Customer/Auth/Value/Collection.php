<?php

class Vznl_FFHandler_Model_Resource_Request_Customer_Auth_Value_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    public function _construct()
    {
        $this->_init("ffhandler/request_customer_auth_value");
    }

    /**
     * @param int $requestId
     * @return $this
     */
    public function filterByRequestId($requestId)
    {
        $this->addFieldToFilter("request_id", $requestId);

        return $this;
    }
}