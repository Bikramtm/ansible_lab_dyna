<?php
/**
 * Class Vznl_ReserveTelephoneNumber_Adapter_Stub_Adapter
 *
 */
class Vznl_ReserveTelephoneNumber_Adapter_Stub_Adapter
{
    const MODULE_NAME = 'ReserveTelephoneNumber';

    /**
     * @param array $arguments
     * @return $responseData
     */
    public function call($arguments = array())
    {
        $stubClient = Mage::helper('vznl_reserveTelephoneNumber')->getStubClient();
        $stubClient->setNamespace('Vznl_ReserveTelephoneNumber');
        $responseData = $stubClient->call(self::MODULE_NAME, $arguments);
        Mage::helper('vznl_reserveTelephoneNumber')->transferLog($arguments, $responseData, self::MODULE_NAME);
        return $responseData;
    }
}