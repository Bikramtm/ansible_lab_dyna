<?php

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;

/**
 * Class Vznl_ReserveTelephoneNumber_Adapter_Peal_Adapter
 */
class Vznl_ReserveTelephoneNumber_Adapter_Peal_Adapter
{
    CONST name = "ReserveTelephoneNumber";
    /**
     * @var string
     */
    private $endpoint;

    /**
     * @var Client
     */
    private $client;

    /**
     * @var string
     */
    private $channel;

    /**
     * @var string
     */
    private $cty;

    /**
     * @var string
     */
    private $footprint;

    /**
     * @var string
     */
    private $executionTimeId;

    /**
     * @var string
     */
    private $requestTimeId;

    /**
     * Adapter constructor
     * @param Client $client
     * @param string $endpoint
     * @param string $channel
     * @param string $cty
     */
    public function __construct(Client $client, string $endpoint, string $channel, string $cty, string $footprint)
    {
        $this->endpoint = $endpoint;
        $this->channel = $channel;
        $this->cty = $cty;
        $this->client = $client;
        $this->footprint = $footprint;
    }

    /**
     * @param $arguments
     * @return string|array
     * @throws GuzzleException
     */
    public function call($arguments)
    {
        $this->executionTimeId = $this->getHelper()->initTimeMeasurement();

        try {
            $userName = $this->getHelper()->getLogin();
            $password = $this->getHelper()->getPassword();
            $verify = $this->getHelper()->getVerify();
            $proxy = $this->getHelper()->getProxy();

            $footPrint = $this->getHelper()->getFootPrint();
            $requestType = $arguments['requestType'];
            $resRelTelephoneNumberView = array(
                'houseFlatNumber' => $arguments['houseFlatNumber'],
                'postCode' => $arguments['postCode'],
                'directoryNumber' => $arguments['directoryNumber'],
                'customerType' => $arguments['customerType'],
                'houseFlatExt' => $arguments['houseFlatExt']
            );
            $requestLog['url'] = $this->getUrl($requestType);
            $requestLog['body'] = $resRelTelephoneNumberView;

            $this->requestTimeId = $this->getHelper()->initTimeMeasurement();

            if ($userName != '' && $password != '') {
                $response = $this->client->request('POST', $this->getUrl($requestType),
                    [
                        'auth' => [$userName, $password],
                        'body' => json_encode($resRelTelephoneNumberView),
                        'verify' => $verify,
                        'proxy' => $proxy,
                        'headers' => [
                            'Content-Type' => 'application/json; charset=UTF-8'
                        ]
                    ]
                );
            } else {
                $response = $this->client->request('POST', $this->getUrl($requestType),
                    [
                        'body' => json_encode($resRelTelephoneNumberView),
                        'verify' => $verify,
                        'proxy' => $proxy,
                        'headers' => [
                            'Content-Type' => 'application/json; charset=UTF-8'
                        ]
                    ]
                );
            }

            $this->getHelper()->transferLog(
                json_encode($requestLog),
                json_encode(json_decode($response->getBody())),
                self::name,
                [
                    'execution' => $this->executionTimeId,
                    'request' => $this->requestTimeId
                ],
                'PEAL'
            );

            $source = json_decode($response->getBody(), true);
        } catch (ClientException|RequestException $exception) {
            $this->getHelper()->transferLog(
                $exception->getRequest(),
                ['msg' => $exception->getMessage(), 'code' => $exception->getCode()],
                self::name,
                [
                    'execution' => $this->executionTimeId,
                    'request' => $this->requestTimeId
                ],
                'PEAL'
            );

            $result['error'] = true;
            $result['message'] = $exception->getMessage();
            $result['code'] = $exception->getCode();
            return $result;
        }
        return $source;
    }

    /**
     * @param $requestType
     * @param $validateAddress
     * @return string
     */
    public function getUrl($requestType)
    {
        return $this->endpoint .
            "?cty=" . $this->cty .
            "&chl=" . $this->channel .
            "&requestType=" . $requestType .
            "&footPrint=" . $this->getHelper()->getFootPrint();
    }

    /**
     * @return object Vznl_ReserveTelephoneNumber_Helper_Data
     */
    public function getHelper()
    {
        return Mage::helper('vznl_reserveTelephoneNumber');
    }

}
