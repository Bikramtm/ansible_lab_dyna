<?php
use GuzzleHttp\Client;

class Vznl_ReserveTelephoneNumber_Adapter_Peal_Factory
{
    public static function create()
    {
        $client = new Client();
        $helper = Mage::helper('vznl_reserveTelephoneNumber');
        return new Vznl_ReserveTelephoneNumber_Adapter_Peal_Adapter(
            $client,
            $helper->getEndPoint(),
            $helper->getChannel(),
            $helper->getCountry(),
            $helper->getFootPrint()
        );
    }
}