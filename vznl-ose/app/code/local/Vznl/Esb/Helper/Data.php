<?php

// TODO: Refactor this peace of crap once definitive
class Vznl_Esb_Helper_Data extends Mage_Core_Helper_Abstract
{
	const VZNL_AGENT_CODE_CUSTOMER_ESHOP = 'Customer';

	private function _logMsg($msg)
    {
        Mage::log($msg, null, 'soap_esb.log');
    }

    public function setDeliveryDetails($data)
    {
        $this->_logMsg('Received setDeliveryDetails with data: ' . $data);

        list($param1, $param2, $param3, $param4) = explode(';', $data . ";"); //added extra semicolon due to message inconsistency as sometimes the last param is not sent

        $orderNumber = $param2;
        $order = $this->getDeliveryOrder($orderNumber);
        if (!$order || !$order->getId()) {
            $this->_logMsg('Order not found: ' . $orderNumber);

            return 'Error: Order not found';
        }
        $plannedDate = trim($param3); // dd-mm-yyyy hh:mm
        $carrierName = trim($param4);
        if (strpos($param4, ',') !== false) {
            $param4Array = explode(',', $param4);
            $trackAndTraceUrl = !empty($param4Array[1]) ? trim($param4Array[1]) : null;
            $carrierName = trim($param4Array[0]);
            // prevent replacing a previously set url with an empty url
            if ($trackAndTraceUrl) {
                $order->setTrackAndTraceUrl($trackAndTraceUrl);
            }
        }
        if ($carrierName) {
            $order->setCarrierName($carrierName);
        }
        if ($plannedDate) {
            $order->setPlannedDate($plannedDate);
        }
        $order->save();

        return 'Success: Changed delivery details';
    }

    public function setCiboodleId($data)
    {
        $this->_logMsg('Received setCiboodleId with data: ' . $data);

        list($param1, $param2) = explode(';', $data);

        $packageInfo = explode('.', $param1);
        $orderNumber = $packageInfo[0];
        $superorder = Mage::getModel('superorder/superorder')->getSuperOrderByOrderNumber((string) $orderNumber);
        $packageNr = $packageInfo[1]; // Format used is saleOrderNumber.PackageNr, xxxxx.1 xxxxx.2 etc
        $ciBoodleId = $param2;

        $write = Mage::getSingleton('core/resource')->getConnection('core_write');
        $updateData = array(
        	'updated_at' => now(),
        	'ciboodl_cases' => $ciBoodleId
        );
        $where = array(
        	'order_id = ?' => $superorder->getEntityId(),
        	'package_id = ?' => $packageNr
        );
        $rows = $write->update(
            'catalog_package',
        	$updateData,
        	$where
        );

        if ($rows <= 0) {
            return 'Error: Invalid order ID/package_id or ciboodle_cases was already set to this value';
        }
        return 'Success: Changed ciboodle_cases';
    }

    public function setSaleOrderError($data)
    {
        $this->_logMsg('Received setSaleOrderError with data: ' . $data);

        list($param1, $param2, $param3) = explode(';', $data . ";"); //added extra semicolon due to message inconsistency as sometimes the last param is not sent

        $orderNumber = $param1;
        $errorCode = $param2;
        $errorMessage = $param3;

        if (empty($orderNumber)) {
            return 'Error: Empty order number';
        }

        $error = Mage::getModel('superorder/errors')->setErrorCode($errorCode);

        $superOrder = Mage::getModel('superorder/superorder')->getSuperOrderByOrderNumber($orderNumber);
        $superOrder->setErrorCode($errorCode);

        if ($errorMessage) {
        	$error->setErrorDetail($errorMessage);
            $superOrder->setErrorDetail($errorMessage);
        }

        $superOrder->save();

        // Create a saved shopping cart from the quote of the failed order
        $error->setSuperorderId($superOrder->getId())
            ->setCreatedAt(now())
            ->save();

        if ($superOrder && $superOrder->getId()) {
            $this->generateQuoteOnFail($superOrder);
        }

        if ($superOrder->getId() <= 0) {
            return 'Error: Invalid order ID or status was already set to this value';
        }
        return 'Success: Changed sales order error code';
    }

    private function _setSaleOrderStatus($superOrder, $orderNumber, $status, $pealOrderId, $previousStatus)
    {
        if (strcasecmp($status, $previousStatus) !== 0 || $pealOrderId) {
            //Unset GUID for each AXI statuses, it allow to generate unique GUID for other OIL requests.
            $superOrder->setOrderGuid(null);
            // make db call only if status/ peal order id needs to be updated
            $superOrder->setOrderStatus($status);
    		if ($pealOrderId) {
                $superOrder->setPealOrderId($pealOrderId);
    		}
            $superOrder->save();
            return $superOrder->getId();
    	}
    	return 0;
    }

    public function sendCommunicationItemsForValidationApproved($data)
    {

        $this->_logMsg('Received sendCommunicationItemsForValidationApproved with data: ' . $data);

        list($param1, $param2, $param3, $param4) = explode(';', $data . ";");
        $param3 = trim($param3);
        $agentId = $param3 ?: null;

        $param4 = trim($param4);
        $pealOrderId = $param4 ?: null;
        $orderNumber = trim($param1);
        $status = trim($param2);

        if (empty($orderNumber)) {
            return 'Error: Empty order number';
        }
        if (empty($status)) {
            return 'Error: Empty status';
        }

        $status = $status == 'Validation Approved' ? Vznl_Superorder_Model_Superorder::SO_VALIDATED : $status;

        $pdfHelper = Mage::helper('vznl_checkout/pdf');
        /** @var Vznl_Superorder_Model_Superorder $superOrder */
        $superOrder = Mage::getModel('superorder/superorder')->getSuperOrderByOrderNumber($orderNumber);

        /** @var Vznl_Communication_Helper_Email $emailHelper */
        $emailHelper = Mage::helper('communication/email');
        /** @var Vznl_Communication_Helper_Sms $smsHelper */
        $smsHelper = Mage::helper('communication/sms');
        if (!$superOrder->getId()) {
            $this->_logMsg('Order not found: ' . $orderNumber);
        }

        //fix for translation
        Mage::getSingleton('core/translate')->setLocale(Mage::app()->getLocale()->getLocaleCode())->init('frontend', true);

        // Emulate the superorder code in order to read the product data for that store instead of default which may be different
        $ccValidated = (strtolower($status) == strtolower(Vznl_Superorder_Model_Superorder::SO_VALIDATED)) ? true : false;
        $ccBeforeEbs = strtolower(Vznl_Superorder_Model_Superorder::SO_PRE_IN_INITIAL);
        $appEmulation = Mage::getSingleton('core/app_emulation');
        $storeId = Mage::getModel('core/website')->load($superOrder->getWebsiteId())->getDefaultStore()->getId();
        $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($storeId);

        $superOrderWebsiteCode = Mage::app()->getWebsite($superOrder->getWebsiteId())->getCode();
        $sendConfirmationMail = false;
        $agentHelper = Mage::helper('vznl_agent');
        $isIndirect =  $agentHelper->isIndirectStore();
        /** @var vznl_checkout_Model_Sales_Order $deliveryOrder */
        $homeDelivery = false;

        foreach ($superOrder->getOrders() as $deliveryOrder) {
            if ($deliveryOrder->isOtherStoreDelivery() && $ccValidated) {
                // Email that the goods pickup can be done
                try {
                    $payload = $emailHelper->getOrderReadyForPickupPayload($deliveryOrder);
                    Mage::getSingleton('communication/pool')->add($payload, $superOrderWebsiteCode);
                    $this->_logMsg('Send ready4pickup email in Telesales for order: ' . $deliveryOrder->getIncrementId());
                } catch (Exception $e) {
                    $this->_logMsg('Error sending ready4pickup email in Telesales for order: ' . $deliveryOrder->getIncrementId());
                    Mage::log($e->getMessage(), null, 'EmailErrorLog.log', true);
                }

                $this->sendReady4PickupToStore($deliveryOrder, false);
            }
            // Generate documents if needed
            if ($deliveryOrder->hasMobilePackage() && $deliveryOrder->isHomeDelivery()) {
                $homeDelivery = true;
                // Contract doesn't need logo when created for printing in warehouse
                $contractOutput = Mage::helper('vznl_checkout/sales')->generateContract($deliveryOrder, true, false);
                /** @var vznl_checkout_Helper_Pdf */
                $pdfHelper->saveContract($contractOutput, $deliveryOrder->getIncrementId(), 'pdf');
                // Add the loadOverview
                if ($deliveryOrder->isLoanRequired()) {
                    $pdfHelper->saveLoanOverview($deliveryOrder, false);
                }
            }
            $sendConfirmationMail = (($deliveryOrder->isHomeDelivery() || $deliveryOrder->isOtherStoreDelivery()) && !$isIndirect)
                ? true : $sendConfirmationMail;
        }

        $isWebshop = Mage::helper('agent')->checkIsWebshop(Mage::app()->getWebsite($superOrder->getWebsiteId())->getCode());
        $triggerMail = true;
        $triggerFixedMail = true;
        $SalesOrderAction = $superOrder->getSalesOrderActionType();
        $triggerCommunicationP85 = $superOrder->getCommunicationP85List($superOrder->getOrderNumber());
        if (count($triggerCommunicationP85) > 0 && $SalesOrderAction == Vznl_Superorder_Model_Superorder::SO_TYPE_CREATE) {
            $triggerFixedMail = false;
        }

        if (count($triggerCommunicationP85) > 0 && $SalesOrderAction == Vznl_Superorder_Model_Superorder::SO_TYPE_CREATE && !$param3) {
            $triggerMail = false;
        }

        if (!$isWebshop && $sendConfirmationMail && $superOrder->hasMobilePackage() && $ccValidated && $triggerMail) {
            try {
                $confirmationPayload = $emailHelper->getOrderConfirmationPayload($superOrder);
                Mage::getSingleton('communication/pool')->add($confirmationPayload, $superOrderWebsiteCode);

                $smsHelper->createOrderConfirmationSms($superOrder);
            } catch (Exception $e) {
                Mage::log($e->getMessage(), null, 'EmailErrorLog.log', true);
            }
        } elseif (!$isWebshop && $sendConfirmationMail && $superOrder->hasMobilePackage() && strtolower($status) == $ccBeforeEbs && $triggerMail && $SalesOrderAction == Vznl_Superorder_Model_Superorder::SO_TYPE_CREATE) {
            try {
                $confirmationPayload = $emailHelper->getOrderConfirmationPayload($superOrder);
                Mage::getSingleton('communication/pool')->add($confirmationPayload, $superOrderWebsiteCode);
                $smsHelper->createOrderConfirmationSms($superOrder);
            } catch (Exception $e) {
                Mage::log($e->getMessage(), null, 'EmailErrorLog.log', true);
            }
        } elseif ($triggerFixedMail && $superOrder->hasFixed()) {
            try {
                $confirmationPayload = $emailHelper->getOrderConfirmationPayload($superOrder);
                Mage::getSingleton('communication/pool')->add($confirmationPayload, $superOrderWebsiteCode);
                $smsHelper->createOrderConfirmationSms($superOrder);
            } catch (Exception $e) {
                Mage::log($e->getMessage(), null, 'EmailErrorLog.log', true);
            }
        }

        if ($homeDelivery && $superOrder->hasMobilePackage() && $ccValidated) {
            $softContractPayload = $emailHelper->getSoftContractPayload($superOrder);
            Mage::getSingleton('communication/pool')->add($softContractPayload, $superOrderWebsiteCode);
        }

        $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);

        $this->_logMsg('Completed sendCommunicationItemsForValidationApproved with data: ' . $data);

        return 'Success: Communication task completed';
    }

    public function sendCommunicationItemsForFixedValidationApproved($data)
    {
        $this->_logMsg('Received sendCommunicationItemsForFixedValidationApproved with data: ' . $data);

        list($param1, $param2, $param3, $param4) = explode(';', $data . ";");
        $param3 = trim($param3);
        $agentId = $param3 ?: null;

        $param4 = trim($param4);
        $pealOrderId = $param4 ?: null;
        $orderNumber = trim($param1);
        $status = trim($param2);

        if (empty($orderNumber)) {
            return 'Error: Empty order number';
        }
        if (empty($status)) {
            return 'Error: Empty status';
        }

        $pdfHelper = Mage::helper('vznl_checkout/pdf');
        /** @var Vznl_Superorder_Model_Superorder $superOrder */
        $superOrder = Mage::getModel('superorder/superorder')->getSuperOrderByOrderNumber($orderNumber);

        /** @var Vznl_Communication_Helper_Email $emailHelper */
        $emailHelper = Mage::helper('communication/email');

        if (!$superOrder->getId()) {
            $this->_logMsg('Order not found: ' . $orderNumber);
        }

        //fix for translation
        Mage::getSingleton('core/translate')->setLocale(Mage::app()->getLocale()->getLocaleCode())->init('frontend', true);

        $superOrderWebsiteCode = Mage::app()->getWebsite($superOrder->getWebsiteId())->getCode();
        $isHomeDelivery = true;
        foreach ($superOrder->getOrders() as $deliveryOrder) {
            if ($deliveryOrder->isHomeDelivery()) {
                $isHomeDelivery = false;
            }
            if ($deliveryOrder->hasFixed() && !$deliveryOrder->hasMobilePackage()) {
                $contractOutput = Mage::helper('vznl_checkout/sales')->generateFixedContract($deliveryOrder, true, false);
                $pdfHelper->saveContract($contractOutput, $deliveryOrder->getIncrementId(), 'pdf', true);
            }
        }

        if (!$superOrder->hasMobilePackage() && $superOrder->hasFixed() && $isHomeDelivery) {
//            $softContractPayload = $emailHelper->getSoftContractPayload($superOrder);
//            Mage::getSingleton('communication/pool')->add($softContractPayload, $superOrderWebsiteCode);
        }

        $this->_logMsg('Completed sendCommunicationItemsForFixedValidationApproved with data: ' . $data);

        return 'Success: Fixed Contract Communication task completed';
    }

    public function setSaleOrderStatus($data)
    {
        $this->_logMsg('Received setSaleOrderStatus with data: ' . $data);

        list($param1, $param2, $param3, $param4) = explode(';', $data . ";");
        $param3 = trim($param3);
        $agentId = $param3 ?: null;

        $param4 = trim($param4);
        $pealOrderId = $param4 ?: null;
        $orderNumber = trim($param1);
        $status = trim($param2);

        if (empty($orderNumber)) {
            return 'Error: Empty order number';
        }
        if (empty($status)) {
            return 'Error: Empty status';
        }

        $status = $status == 'Validation Approved' ? Vznl_Superorder_Model_Superorder::SO_VALIDATED : $status;

        // Get the current status
        $superOrder = Mage::getModel('superorder/superorder')->getSuperOrderByOrderNumber($orderNumber);
        $previousStatus = $superOrder && $superOrder->getEntityId() ? $superOrder->getOrderStatus() : '';
        $rows = $this->_setSaleOrderStatus($superOrder, $orderNumber, $status, $pealOrderId, $previousStatus);
        if ($rows <= 0) {
            return 'Error: Invalid order ID or status was already set to this value';
        } else {
            // If the status set is from e-shop, force it to customer instead of the agent itself
            $agentId = ($status == Vznl_Superorder_Model_Superorder::SO_CANCELLED &&
            	Mage::app()->getWebsite($superOrder->getWebsiteId())->getCode() == Vznl_Agent_Model_Website::WEBSITE_WEB_SHOP_CODE)
                ? self::VZNL_AGENT_CODE_CUSTOMER_ESHOP : $agentId;

            Mage::getModel('superorder/statusHistory')
                ->setType(Vznl_Superorder_Model_StatusHistory::SUPERORDER_STATUS)
                ->setSuperorderId($superOrder->getId())
                ->setStatus($status)
                ->setAgentId($agentId)
                ->setCreatedAt(now())
                ->save();

            /** @var Vznl_Communication_Helper_Email $emailHelper */
            $emailHelper = Mage::helper('communication/email');

            if (!$superOrder->getId()) {
                $this->_logMsg('Order not found: ' . $orderNumber);
            }

            //fix for translation
            Mage::getSingleton('core/translate')->setLocale(Mage::app()->getLocale()->getLocaleCode())->init('frontend', true);

            switch(strtolower($status))
            {
                case strtolower(Vznl_Superorder_Model_Superorder::SO_VALIDATED) :
                case strtolower(Vznl_Superorder_Model_Superorder::SO_PRE_IN_INITIAL) :
                    $iltHelper = Mage::helper('ilt');
                    $iltHelper->removeIltData($superOrder);

                    // Run communication tasks (without waiting for it to execute)
                    $data = 'esb_api.sendCommunicationItemsForValidationApproved;' . $data;
                    exec('php ' . Mage::getBaseDir() . '/shell/statusdaemon/esb_parser.php -c "' . $data . '" > /dev/null 2>&1 &');
                    break;
                case strtolower(Vznl_Superorder_Model_Superorder::SO_WAITING_VALIDATION_ACCEPTED_AXI) :
                    //do nothing
                    break;
                case strtolower(Vznl_Superorder_Model_Superorder::SO_CANCELLED) :
                    $superOrderWebsiteCode = Mage::app()->getWebsite($superOrder->getWebsiteId())->getCode();

                    // False as part of MS-3875, this code stays here as it might needs to be reactivated if required
                    $triggerCancelDuringProvision = true;

                    if ($triggerCancelDuringProvision && $superOrder->hasMobilePackage()) {
                        try {
                            $cancelledDuringProvisionPayload = $emailHelper->getCancelledDuringProvisioningPayload($superOrder);
                            Mage::getSingleton('communication/pool')->add($cancelledDuringProvisionPayload, $superOrderWebsiteCode);
                        } catch (Exception $e) {
                            Mage::log($e->getMessage(), null, 'EmailErrorLog.log', true);
                        }
                    }

                    // Check whether it is needed to send the P34B email
                    $p34aIsSend = in_array($previousStatus, array(Vznl_Superorder_Model_Superorder::SO_VALIDATED, Vznl_Superorder_Model_Superorder::SO_FULFILLMENT_IN_PROGRESS));
                    /** @var vznl_checkout_Model_Sales_Order $deliveryOrder */
                    foreach ($superOrder->getOrders() as $deliveryOrder) {
                        if ($deliveryOrder->isOtherStoreDelivery() && $p34aIsSend) {
                            $this->sendReady4PickupToStore($deliveryOrder, true);
                        }
                    }
                    break;

                case (strtolower(Vznl_Superorder_Model_Superorder::SO_VALIDATION_FAILED)) :
                    if ($superOrder && $superOrder->getId()) {
                        $this->generateQuoteOnFail($superOrder);
                    }
                    break;
            }

            return 'Success: Changed sales order status';
        }
    }

    public function setDeliveryOrderStatus($data)
    {
        $this->_logMsg('Received setDeliveryOrderStatus with data: ' . $data);

        list($param0, $param1, $param2) = explode(';', $data);
        $orderNumber = $param1;
        $status = $param2;
        if (empty($orderNumber)) {
            return 'Error: Empty order number';
        }
        if (empty($status)) {
            return 'Error: Empty status';
        }
        $statusCode = str_replace(" ", "_", strtolower($status));

        $order = $this->getDeliveryOrder($orderNumber);
        if ($order && $order->getId()) {
            try {
                $this->_logMsg('Setting setDeliveryOrderStatus for ' . $orderNumber . ' to: ' . $statusCode);
                $order->setStatus($statusCode)
                    ->setUpdatedAt(now())
                    ->save();
            } catch (Exception $ex) {
                $this->_logMsg('ERROR found: not able to setDeliveryOrderStatus for ' . $orderNumber . ' to: ' . $statusCode . '. Order not found');
            }
        } else {
            $this->_logMsg('Unable to setDeliveryOrderStatus for ' . $orderNumber . ' to: ' . $statusCode . '. Order not found');
        }

        return 'OK';
    }

    public function setCustomerBan($data)
    {
        $this->_logMsg('Received setCustomerBan with data: ' . $data);

        //added order number for future purposes although it is not used at this point
        list($orderNumber, $param1, $param2, $param3) = explode(';', $data . ";"); //added extra semicolon due to message inconsistency as sometimes the last param is not sent

        $customerId = $param1;
        $customerBan = $param2;
        $customerMmlStatus = $param3;

        /** @var Vznl_Customer_Model_Customer $customer */
        $customer = Mage::getModel('customer/customer')->load($customerId);

        if ($customer && $customer->getId()) {
            $customer->setBan($customerBan);
            $customer->setCustomerLabel($customerMmlStatus);
            $customer->save();

            $order =  Mage::getModel('superorder/superorder')->getSuperOrderByOrderNumber((string) $orderNumber);

            // Calling API to convert prospect to active customer
            $searchHelper = Mage::helper('vznl_customer/search');
            $customer = $searchHelper->createApiCustomer($customer,null, $order,'SaveExistingFields');
            $customer = $searchHelper->updateProspectApiCall($customer);
            $searchHelper->updateOrderNumberApiCall($order, $customer);
            return 'Success';
        }

        return 'Error: Customer does not exists';
    }

    public function setVfOrderNr($data)
    {
        $this->_logMsg('Received setVfOrderNr with data: ' . $data);
        list($param1, $param2, $param3) = explode(';', $data);

        $orderNumber = $param1;
        $packageNumber = $param2;
        $vfNumber = $param3;
        $so = Mage::getModel('superorder/superorder')->getSuperOrderByOrderNumber($orderNumber);
        if (!$so || !$so->getId()) {
            return 'Error: Order does not exists';
        }

        $package = Mage::getModel('package/package')->getCollection()
            ->addFieldToFilter('order_id', $so->getId())
            ->addFieldToFilter('package_id', $packageNumber)
            ->setPageSize(1, 1)
            ->getLastItem();

        $package->setUpdatedAt(now())
            ->setPackageEsbNumber($vfNumber)
            ->save();

        if ($package->getId() <= 0) {
            return 'Error: Invalid order ID or package ID, or package_esb_number was already set to this value';
        }
        return 'Success';
    }

    public function setZgOrderNr($data)
    {
        $this->_logMsg('Received setZgOrderNr with data: ' . $data);
        list($param1, $param2, $param3) = explode(';', $data);

        $orderNumber = $param1;
        $packageNumber = $param2;
        $zgNumber = $param3;
        $so = Mage::getModel('superorder/superorder')->getSuperOrderByOrderNumber($orderNumber);
        if (!$so || !$so->getId()) {
            return 'Error: Order does not exists';
        }

        $package = Mage::getModel('package/package')->getCollection()
            ->addFieldToFilter('order_id', $so->getId())
            ->addFieldToFilter('package_id', $packageNumber)
            ->setPageSize(1, 1)
            ->getLastItem();

        $package->setUpdatedAt(now())
            ->setPackageEsbNumber($zgNumber)
            ->save();

        if ($package->getId() <= 0) {
            return 'Error: Invalid order ID or package ID, or package_esb_number was already set to this value';
        }

        $so->setPealOrderId($zgNumber)
            ->save();

        if ($so->getId() <= 0) {
            return 'Error: Invalid order ID or Ziggo Order ID or peal_order_id was already set to this value';
        }
        return 'Success';
    }

    private function _getPackageNr($esbPackageNr)
    {
        $parts = explode('.', $esbPackageNr);
        return ((count($parts) > 1) ? $parts[1] : $parts[0]);
    }

    public function setVfStatusCode($data)
    {
        $this->_logMsg('Received setVfStatusCode with data: ' . $data);

        list($param1, $param2, $param3, $param4) = explode(';', $data);
        $orderNumber = $param1;
        $packageNumber = $this->_getPackageNr($param2);
        $vfStatusCode = trim($param3);
        $vfStatusDesc = trim($param4);

        $so = Mage::getModel('superorder/superorder')->getSuperOrderByOrderNumber($orderNumber);
        if (!$so || !$so->getId()) {
            return 'Error: Order does not exists';
        }

        $package = Mage::getModel('package/package')->getCollection()
            ->addFieldToFilter('order_id', $so->getId())
            ->addFieldToFilter('package_id', $packageNumber)
            ->setPageSize(1, 1)
            ->getLastItem();

        if (strpos($vfStatusCode, 'CRD') === 0) { // This is credit case update
            $package->setUpdatedAt(now())
                ->setVfCreditcode($vfStatusCode)
                ->save();

            if ($package->getId() <= 0) {
                return 'Error: Invalid order ID or package ID, or vf_creditcode was already set to this value';
            }

            Mage::getModel('superorder/statusHistory')
                ->setType(Vznl_Superorder_Model_StatusHistory::PACKAGE_CREDIT_CODE)
                ->setPackageId($package->getId())
                ->setStatus($vfStatusCode)
                ->setCreatedAt(now())
                ->save();
        } else {
            if ($vfStatusCode != 'VOD-00-000-000') {
                // Create the original quote as shopping cart
                $this->generateQuoteOnFail($so);

                $package->setUpdatedAt(now())
                    ->setStatus(Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_FAILED)
                    ->setVfStatusCode($vfStatusCode)
                    ->setVfStatusDesc($vfStatusDesc)
                    ->save();

                // Also set the error on sale order level, temporary fix; otherwise its not visible in failed order screen for telesales
                if ($so->getId()) {
                    $so->setErrorCode($vfStatusCode)
                        ->setErrorDetail($vfStatusDesc)
                        ->save();

                    Mage::getModel('superorder/errors')
                        ->setErrorCode($vfStatusCode)
                        ->setErrorDetail($vfStatusDesc)
                        ->setSuperorderId($so->getId())
                        ->setCreatedAt(now())
                        ->save();
                }

                if ($package->getId() <= 0) {
                    return 'Error: Invalid order ID or package ID, or status was already set to this value';
                }

                Mage::getModel('superorder/statusHistory')
                    ->setType(Vznl_Superorder_Model_StatusHistory::PACKAGE_STATUS)
                    ->setPackageId($package->getId())
                    ->setStatus(Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_FAILED)
                    ->setCreatedAt(now())
                    ->save();

                Mage::getModel('superorder/statusHistory')
                    ->setType(Vznl_Superorder_Model_StatusHistory::PACKAGE_VF_STATUS_CODE)
                    ->setPackageId($package->getId())
                    ->setStatus($vfStatusCode)
                    ->setCreatedAt(now())
                    ->save();
            }
        }

        return 'Success';
    }

    public function setVfCreditStatus($data)
    {
        $this->_logMsg('Received setVfCreditStatus with data: ' . $data);

        list($param1, $param2, $param3, $param4) = explode(';', $data . ";"); //added extra semicolon due to message inconsistency as sometimes the last param is not sent
        $orderNumber = $param1;
        $packageNumber = $this->_getPackageNr($param2);
        $vfIdentifyingEntity = $param3;
        $vfId = $param4;
        $so = Mage::getModel('superorder/superorder')->getSuperOrderByOrderNumber($orderNumber);
        if (!$so || !$so->getId()) {
            return 'Error: Order does not exists';
        }

        $package = Mage::getModel('package/package')->getCollection()
            ->addFieldToFilter('order_id', $so->getId())
            ->addFieldToFilter('package_id', $packageNumber)
            ->setPageSize(1, 1)
            ->getLastItem();

        if (!empty($vfId) && $vfId != '0') {
            $package->setUpdatedAt(now())
                ->setCreditcheckStatus('VALIDATION_FAILED')
                ->setVfStatusCode($vfId)
                ->setVfStatusDesc($vfIdentifyingEntity);

            if ($package->getCurrentNumber()) {
                $package->setPortingStatus('VALIDATION_FAILED');
            }

            $package->save();

            $so->setErrorDetail($vfIdentifyingEntity)
                ->save();

            $this->generateQuoteOnFail($so);
        } else {
            $package->setUpdatedAt(now())
                ->setVfStatusCode($vfId)
                ->setVfStatusDesc($vfIdentifyingEntity);

            $package->save();
        }

        if ($package->getId() <= 0) {
            return 'Error: Invalid order ID or package ID, or status was already set to this value';
        }
        Mage::getModel('superorder/statusHistory')
            ->setType(Vznl_Superorder_Model_StatusHistory::PACKAGE_VF_STATUS_CODE)
            ->setPackageId($package->getId())
            ->setStatus($vfId)
            ->setCreatedAt(now())
            ->save();

        return 'Success';
    }

    public function setNumberportingStatus($data)
    {
        $this->_logMsg('Received setNumberportingStatus with data: ' . $data);

        $list = explode(';', $data);
        $param1 = $list[0];
        $param2 = $list[1];
        $param3 = $list[2];
        $param4 = isset($list[3]) ? $list[3] : null;

        $orderNumber = $param1;
        $packageNumber = $param2;
        $npStatus = $this->getNumberPortingStatus($param3);

        $reference = $param4 ? Mage::getModel('vznl_porting/reference')->load($param4, 'code') : null;
        $referenceStatus = ($reference && $reference->getId()) ? ("'" . $reference->getId() . "'") : 'NULL';
        if ($orderNumber) {
            /** @var Vznl_Superorder_Model_Superorder $so */
            $so = Mage::getModel('superorder/superorder')->getSuperOrderByOrderNumber($orderNumber);
            if (!$so || !$so->getId()) {
                return 'Error: Order does not exists';
            }

            $package = Mage::getModel('package/package')->getCollection()
                ->addFieldToFilter('order_id', $so->getId())
                ->addFieldToFilter('package_id', $packageNumber)
                ->setPageSize(1, 1)
                ->getLastItem();

            $package->setUpdatedAt(now())
                ->setPortingStatus($npStatus)
                ->setPortingStatusUpdated(now())
                ->setPortingReferenceId($referenceStatus)
                ->save();

            if ($package->getId() <= 0) {
                return 'Error: Invalid order ID or package ID, or status was already set to this value';
            }

            /** @var Vznl_Package_Model_Package $package */
            $package = Mage::getModel('package/package')->getCollection()
                ->addFieldToFilter('order_id', $so->getId())
                ->addFieldToFilter('package_id', $packageNumber)
                ->setPageSize(1, 1)
                ->getLastItem();

            Mage::getModel('superorder/statusHistory')
                ->setType(Vznl_Superorder_Model_StatusHistory::PACKAGE_PORTING_STATUS)
                ->setPackageId($package->getId())
                ->setStatus($npStatus)
                ->setCreatedAt(now())
                ->save();

            return 'Success';
        } else {
            $this->_logMsg('Unable to find setNumberportingStatus order/reference with data: ' . $data);
        }
    }

    /**
     * @param $param
     *
     * @return string
     */
    protected function getNumberPortingStatus($param)
    {
        switch ($param) {
            case 'Pending':
                $npStatus = Vznl_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_NP_PENDING;
                break;
            case 'Approved':
                $npStatus = 'APPROVED';
                break;
            case 'Rejected':
            case 'ServiceNotAvailable':
                $npStatus = Vznl_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_NP_REJECTED;
                break;
            case 'Referred':
                $npStatus = 'REFERRED';
                break;
            case 'Final Rejected':
                $npStatus = Vznl_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_NP_REJECTED_CANCELLED;
                break;
            default:
                $npStatus = '';
        }

        return $npStatus;
    }

    public function setCreditcheckStatus($data)
    {
        $this->_logMsg('Received setCreditcheckStatus with data: ' . $data);

        $list = explode(';', $data);
        $param1 = $list[0];
        $param2 = $list[1];
        $param3 = isset($list[2]) ? $list[2] : null;
        $param4 = isset($list[3]) ? $list[3] : null;

        $orderNumber = $param1;
        $packageNumber = $param2;
        $packageDescription = $status = $ccStatus = null;

        $this->getCreditCheckStatuses($param3, $param4, $status, $ccStatus, $packageDescription);

        if ($orderNumber) {
            /** @var Vznl_Superorder_Model_Superorder $so */
            $so = Mage::getModel('superorder/superorder')->getSuperOrderByOrderNumber($orderNumber);
            if (!$so || !$so->getId()) {
                return 'Error: Order does not exists';
            }
            $ccDate = $this->getCcDate();

            /** @var Vznl_Package_Model_Package $package */
            $package = Mage::getModel('package/package')->getCollection()
                ->addFieldToFilter('order_id', $so->getId())
                ->addFieldToFilter('package_id', $packageNumber)
                ->setPageSize(1, 1)
                ->getLastItem();

            $package->setStatus($status)
                ->setCreditcheckStatus($ccStatus)
                ->setCreditcheckStatusUpdated($ccDate)
                ->setUpdatedAt(now());

            if ($packageDescription) {
                $package->setVfStatusDesc($packageDescription);
            }

            $package->save();

            if (!$package->getId()) {
                return 'Error: Invalid order ID or package ID, or status was already set to this value';
            }

            Mage::getModel('superorder/statusHistory')
                ->setType(Vznl_Superorder_Model_StatusHistory::PACKAGE_CC_STATUS)
                ->setPackageId($package->getId())
                ->setStatus($ccStatus)
                ->setCreatedAt(now())
                ->save();

            Mage::getModel('superorder/statusHistory')
                ->setType(Vznl_Superorder_Model_StatusHistory::PACKAGE_STATUS)
                ->setPackageId($package->getId())
                ->setStatus($status)
                ->setCreatedAt(now())
                ->save();

            /** @var Vznl_Communication_Helper_Email $emailHelper */
            $emailHelper = Mage::helper('communication/email');

            /** @var vznl_checkout_Model_Sales_Order $deliveryOrder */
            $deliveryOrder = $package->getDeliveryOrder();
            $superOrderWebsiteCode = Mage::app()->getWebsite($so->getWebsiteId())->getCode();

            // If Rejected and home delivery
            if ($ccStatus == Vznl_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_CC_REJECTED && $deliveryOrder->isHomeDelivery()) {
                // Get the cc rejected payload
                // Commented as part of OMNVFNL-6240, this code stays here as it might needs to be reactivated if required
                if (Mage::getSingleton('checkout/session')->getOrderNumberEmail() != $orderNumber) {
                    $ccRejectedPayload = $emailHelper->getCreditCheckRejectedPayload($package);
                    Mage::getSingleton('communication/pool')->add($ccRejectedPayload, $superOrderWebsiteCode);
                }
                Mage::getSingleton('checkout/session')->setOrderNumberEmail($orderNumber);
            }

            // if order-validation-failed and home order
            // validation failed mails disabled as part of CR
           /* if ($status == Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_FAILED && $ccStatus == Vznl_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_CC_VALIDATION_FAILED && $deliveryOrder->isHomeDelivery()) {
                $ccRejectedPayload=$emailHelper->getOrderValidationFailedPayload($package);
                Mage::getSingleton('communication/pool')->add($ccRejectedPayload, $superOrderWebsiteCode);
           }*/

            return 'Success';
        }
    }

    /**
     * @param $param
     * @param $description
     * @param $status
     * @param $ccStatus
     * @param $packageDescription
     */
    protected function getCreditCheckStatuses($param, $description, &$status, &$ccStatus, &$packageDescription)
    {
        switch ($param) {
            case 'Approved':
            case 'Succeeded': // Temp
                $status = 'Package Validated';
                $ccStatus = Vznl_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_CC_APPROVED;
                break;
            case 'Rejected':
                $status = 'Cancelled';
                $ccStatus = Vznl_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_CC_REJECTED;
                break;
            case 'AdditionalInfoRequired':
                $status = 'Additional info required';
                $ccStatus = Vznl_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_CC_ADDITIONNAL_INFO_REQUIRED;
                $packageDescription = $description;
                break;
            case 'Referred':
                $status = 'Referred';
                $ccStatus = Vznl_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_CC_REFERRED;
                break;
            case 'Pending':
                $status = 'Pending';
                $ccStatus = Vznl_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_CC_PENDING;
                break;
            case 'Partial':
                $status = 'Partial';
                $ccStatus = Vznl_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_CC_PARTIAL;
                $packageDescription = $description;
                break;
            default:
                $status = Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_FAILED;
                $ccStatus = Vznl_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_CC_VALIDATION_FAILED;
                $packageDescription = $description;
        }
    }

    public function setOrderMsdisn($data)
    {
        $this->_logMsg('Received setOrderMsdisn with data: ' . $data);

        list($orderNr, $packageNr, $daOrderId, $msisdn) = explode(';', $data);

        $superorder = Mage::getModel('superorder/superorder')->getSuperOrderByOrderNumber($orderNr);

        $customerId = $superorder->getCustomerId();

        // Get packageID, match to super order, match to order, match to order_item and set MSISDN
        $package = Mage::getModel('package/package')
            ->getCollection()
            ->addFieldToFilter('order_id', $superorder->getId())
            ->addFieldToFilter('package_id', $packageNr)
            ->setPageSize(1, 1)
            ->getLastItem();

        $package->setUpdatedAt(now())
            ->setTelNumber($msisdn)
            ->save();

        // Insert CTN entry to search for this customer without waiting for dump
        if ($customerId && $msisdn) {
            $write = Mage::getSingleton('core/resource')->getConnection('core_write');
            $rows = $write->insertOnDuplicate('customer_ctn',
            	array ('customer_id' => $customerId, 'ctn' => $msisdn),
            	array( 'ctn' => 'ctn')
            );
        }

        // Call vNext search API to add customer CTN
        $searchHelper = Mage::helper('vznl_customer/search');
        $searchHelper->addCustomerCtnApiCall(
            Mage::getModel("customer/customer")->load($customerId),
            $msisdn,
            Vznl_Customer_Helper_Search::API_CUSTOMER_CTN_TYPE_MOBILE
        );

        if ($package->getId() <= 0) {
            return 'Error: Invalid or unknown dealer adapter number';
        }

        return 'Success: Changed MSISDN for order';
    }

    private function _getCcStatus($param)
    {
        $param = trim($param);
        $ccStatus = null;
    	$status = $param;
    	switch ($param) {
            case 'Package Validated':
                $status = Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_PACKAGE_VALIDATED;
    			$ccStatus = 'APPROVED';
    			break;
    		case 'Delivered':
    			$status = Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_DELIVERED;
    			$ccStatus = 'APPROVED';
    			break;
    		case 'BackOrder':
    			$status = Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_BACK_ORDER;
    			break;
    		default :
    			break;
    	}

    	return array($status, $ccStatus);
    }

    public function setPackageStatus($data)
    {
        $this->_logMsg('Received setPackageStatus with data ' . $data);
        list ($param1, $param2, $param3) = explode(';', $data);
        $ccStatus = null;
        $orderNumber = $param1;
        $packageNumber = $this->_getPackageNr($param2);

        $errors = [];

        list ($status, $ccStatus) = $this->_getCcStatus($param3);

        if (empty($orderNumber)) {
            $errors[] = 'Error: Empty order number';
        }
        if (empty($packageNumber)) {
            $errors[] = 'Error: Empty package number';
        }
        if (empty($status)) {
            $errors[] = 'Error: Empty status';
        }

        /** @var Vznl_Superorder_Model_Superorder $superOrder */
        $so = Mage::getModel('superorder/superorder')->getSuperOrderByOrderNumber($orderNumber);

        if (!$so || !$so->getId()) {
            $errors[] = 'Error: Order does not exists';
        }

        $package = Mage::getModel('package/package')
            ->getCollection()
            ->addFieldToFilter('order_id', $so->getId())
            ->addFieldToFilter('package_id', $packageNumber)
            ->setPageSize(1, 1)
            ->getLastItem();

        $initialStatus = $package->getStatus();

        if (!$package) {
            $errors[] = 'Error: Package does not exists';
        }

        if (empty($errors)) {
            $ccDate = $this->getCcDate();
            try  {
                if ($ccStatus !== null) {
                    $package->setStatus($status)
                        ->setCreditcheckStatus($ccStatus)
                        ->setCreditcheckStatusUpdated($ccDate)
                        ->setUpdatedAt(now())
                        ->save();
                } else {
                    $package->setStatus($status)
                        ->setUpdatedAt(now())
                        ->save();
                }
            } catch (Exception $error){
                $this->_logMsg($error->getMessage());
            }

            // If delivered
            if ($status == Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_DELIVERED) {
                $this->_updateDeliveryOrder($package);
            }

            $this->_updatePackageStatusHistory($package, $ccStatus, $status);

            $package = Mage::getModel('package/package')
                ->getCollection()
                ->addFieldToFilter('order_id', $so->getId())
                ->addFieldToFilter('package_id', $packageNumber)
                ->setPageSize(1, 1)
                ->getLastItem();

            $result = 'Success: Changed package status ("'.$status.'" for package #'.$package->getId().', package '.$packageNumber.', status on package is: '.$package->getStatus().', initial was '.$initialStatus.')';
        } else {
            $result = implode( ", ", $errors );
        }

        $this->_logMsg($result);

        return $result;
    }

    private function _updatePackageStatusHistory($package, $ccStatus, $status)
    {
        if ($ccStatus !== null) {
            Mage::getModel('superorder/statusHistory')
                ->setType(Vznl_Superorder_Model_StatusHistory::PACKAGE_CC_STATUS)
                ->setPackageId($package->getId())
                ->setStatus($ccStatus)
                ->setCreatedAt(now())
                ->save();
        }
        Mage::getModel('superorder/statusHistory')
            ->setType(Vznl_Superorder_Model_StatusHistory::PACKAGE_STATUS)
            ->setPackageId($package->getId())
            ->setStatus($status)
            ->setCreatedAt(now())
            ->save();
    }

    private function _updateDeliveryOrder($package)
    {
        $items = $package->getPackageItems();
        /** @var vznl_checkout_Model_Sales_Order_Item $item */
        foreach($items as $item){
            /** @var Vznl_Catalog_Model_Product $product */
            $product = $item->getProduct();
            $isGarant = ($product && (strtolower($product->getData(Vznl_Catalog_Model_Product::ADDON_TYPE_ATTR)) == 'garant'
                    || strtolower($product->getAttributeText(Vznl_Catalog_Model_Product::ADDON_TYPE_ATTR)) == 'garant'));
            if ($isGarant) {
                $deliveryOrder = $package->getDeliveryOrder();
                break;
            }
        }
    	// Check if home delivery and contract signing date not set yet
    	if ($deliveryOrder && $deliveryOrder->isHomeDelivery() && !$deliveryOrder->getContractSignDate()) {
    		$deliveryOrder->setContractSignDate(date("Y-m-d H:i:s"));
    		$deliveryOrder->save();
    	}
    }

    private function getCcDate()
    {
        return now();
    }

    /**
     * @param Vznl_Superorder_Model_Superorder $superOrder
     * @param integer $packageId
     */
    public function sendGarantMail($superOrder, $packageId)
    {
        list($packagesWithGarant, $garantProductOrders) = $superOrder->getGarantProductOrders();
        foreach ($packagesWithGarant as $orderId => $packageIds) {
            $flippedPackageIds = array_flip($packageIds);
            if (isset($flippedPackageIds[$packageId])) {
                $payload = Mage::helper('communication/email')->getGarantPolisPayload($superOrder, $this->getGarantAttachmentPath($packagesWithGarant, $garantProductOrders, $superOrder, $packageId));
                Mage::getSingleton('communication/pool')->add($payload, Mage::app()->getWebsite($superOrder->getWebsiteId())->getCode());
                Mage::helper('communication/log')->logEmail($payload['receiver'], Vznl_Communication_Helper_Log::SUBTYPE_GARANT, $payload['email_code'], $superOrder);
            }
        }
    }

    public function getGarantAttachmentPath($packagesWithGarant, $garantProductOrders, $superOrder, $packageId)
    {
        $attachmentPaths = [];
        $orderCollection = Mage::getModel('sales/order')->getCollection()->addFieldToFilter('entity_id', array('in' => $garantProductOrders));
        foreach ($orderCollection as $order) {
            $packageItems = array();
            $flippedPackagesWithGarant = array_flip($packagesWithGarant[$order->getId()]);
            foreach ($order->getAllItems() as $item) {
                $packageId = $item->getPackageId();
                if ($item->getPackageId() == $packageId && isset($packagesWithGarant[$order->getId()]) && isset($flippedPackagesWithGarant[$packageId])) {
                    $packages[$item->getPackageId()] = isset($packages[$item->getPackageId()])
                        ? $packages[$item->getPackageId()]
                        : Mage::getModel('package/package')->getOrderPackages($superOrder->getId(), $item->getPackageId());
                    $package = $packages[$item->getPackageId()];
                    if (!$package->getImei()) {
                        //if IMEI has not yet been filled in, wait for the ESB to update IMEI for the rest of the packages
                        return;
                    }
                    $packageItems[$item->getPackageId()] = isset($packageItems[$item->getPackageId()]) ? $packageItems[$item->getPackageId()] : array();
                    $packageItems[$item->getPackageId()][] = $item;
                }
            }
            $attachmentPaths = array_merge($attachmentPaths, Mage::helper('vznl_checkout/pdf')->saveWarranty($order, $packageItems));
        }

        return $attachmentPaths;
    }

    /**
     * @param $superOrder
     */
    protected function generateQuoteOnFail($superOrder)
    {
        $initialQuote = $superOrder->getOriginalQuote();
        $savedCarts = Mage::getModel('sales/quote')->getCollection()
            ->addFieldToFilter('quote_parent_id', $initialQuote->getId())
            ->addFieldToFilter('cart_status', vznl_checkout_Model_Sales_Quote::CART_STATUS_SAVED);

        // If the quote is not already saved as a cart, we save it now
        if (count($savedCarts) == 0) {
            $initialQuote->setSaveAsCart(true);
            Mage::helper('vznl_checkout')->createNewCartQuote($initialQuote, $superOrder->getId());
        }
    }

    public function setHomeDeliveryStatus($data)
    {
        $this->_logMsg('Received setHomeDeliveryStatus with data: ' . $data);
        list($param0, $param1, $param2, $param3) = explode(';', $data . ";"); //added extra semicolon due to message inconsistency as sometimes the last param is not sent

        $orderNumber = trim($param1);
        $status = trim($param2);
        $reasonCode = trim($param3);

        if (empty($orderNumber)) {
            return 'Error: Empty order number';
        }
        if (empty($status)) {
            return 'Error: Empty status';
        }

        switch ($status) {
            case 'CANCEL':
                $statusCode = 'cancelled';
                break;
            default:
                $statusCode = str_replace(" ", "_", strtolower($status));
        }

        /** @var $order vznl_checkout_Model_Sales_Order */
        $order = $this->getDeliveryOrder($orderNumber);
        if ($order && $order->getId()) {
            try {
                $this->_logMsg('Setting home delivery status for ' . $orderNumber . ' to: ' .
                    $statusCode . ', reason code: ' . $reasonCode);

                $order->setEcomStatus($statusCode)
                    ->setReasonCode($reasonCode)
                    ->setUpdatedAt(now())
                    ->save();

                // Add a status history to the db
                Mage::getModel('vznl_superorder/statusHistory')
                    ->setType(Vznl_Superorder_Model_StatusHistory::ECOM_STATUS)
                    ->setOrderId($order->getId())
                    ->setStatus($statusCode)
                    ->setCreatedAt(now())
                    ->save();

            } catch (Exception $ex) {
                $this->_logMsg('ERROR found: not able to setHomeDeliveryStatus for ' . $orderNumber . ' to: ' . $statusCode . ', reason code: ' . $reasonCode . '');
            }
        } else {
            $this->_logMsg('Unable to setHomeDeliveryStatus for ' . $orderNumber . ' to: ' . $statusCode . ', reason code: ' . $reasonCode . '');
        }

        return 'OK';
    }

    /**
     * Used for setting the approve_order status of the package in case the approve package call is performed from within the process order call.
     * @param $data
     * @return string
     */
    public function setOrderPackageApproval($data)
    {
        $this->_logMsg('Received setOrderPackageApproval with data: ' . $data);
        if (substr_count($data, ';') === 3) {
            list($param1, $param2, $param3, $param4) = explode(';', $data);
        } else {
            list($param1, $param2, $param3) = explode(';', $data);
            $param4 = '';
        }

        if (empty($param1)) {
            return 'Error: Empty order number';
        }

        if (empty($param2)) {
            return 'Error: Empty package id';
        }

        if (empty($param3)) {
            return 'Error: Empty status';
        }

        try {
            $superorder = Mage::getModel('superorder/superorder')->getSuperOrderByOrderNumber((string) $param1);
            $package = Mage::getModel('package/package')->getCollection()
                ->addFieldToFilter('order_id', $superorder->getId())
                ->addFieldToFilter('package_id', $param2)
                ->setPageSize(1, 1)
                ->getLastItem();

            if ($param3 == 'true') {
                $package->setApproveOrder(1)
                    ->setApproveOrderJobId(null)
                    ->setUpdatedAt(now())
                    ->save();
            } elseif ($param3 == 'false') {
                $superorder->setErrorDetail($param4)
                    ->setUpdatedAt(now())
                    ->save();
            }
        } catch (Exception $e) {
            $this->_logMsg('Unable to set approve order package status for ' . $param1 . '.' . $param2 . ', error: ' . $e->getMessage());
        }

        return 'OK';
    }

    public function setActualPortingDate($data)
    {
        $this->_logMsg('Received setActualPortingDate with data: ' . $data);
        list($orderNumber, $packageId, $portingDate) = explode(';', $data . ";");

        $actualPortingDate = date("Y-m-d H:i:s", strtotime($portingDate));
        $so = Mage::getModel('superorder/superorder')->getSuperOrderByOrderNumber($orderNumber);
        if (!$so || !$so->getId()) {
            return 'Error: Order does not exists';
        }

        $package = Mage::getModel('package/package')->getCollection()
            ->addFieldToFilter('order_id', $so->getId())
            ->addFieldToFilter('package_id', $packageId)
            ->setPageSize(1, 1)
            ->getLastItem();

        $package->setUpdatedAt(now())
            ->setActualPortingDate($actualPortingDate)
            ->save();

        if ($package->getId() <= 0) {
            return "Error";
        }

        return "Success";
    }

    public function setManualActivation($data)
    {
        $this->_logMsg('Received setManualActivation with data: ' . $data);

        list($param1, $param2) = explode(';', $data);

        $orderNumber = $param1;
        $packageNumber = $param2;
        $so = Mage::getModel('superorder/superorder')->getSuperOrderByOrderNumber($orderNumber);
        if (!$so || !$so->getId()) {
            return 'Error: Order does not exists';
        }

        $package = Mage::getModel('package/package')->getCollection()
            ->addFieldToFilter('order_id', $so->getId())
            ->addFieldToFilter('package_id', $packageNumber)
            ->setPageSize(1, 1)
            ->getLastItem();

        $package->setUpdatedAt(now())
            ->setEsbManualActivation('1')
            ->save();

        if ($package->getId() <= 0) {
            return 'Error: Invalid order ID or package ID, or package_esb_number was already set to this value';
        }

        return 'Success';
    }

    /**
     * Sends the ready 4 picked towards the store for the given delivery order
     * @param vznl_checkout_Model_Sales_Order $deliveryOrder The delivery order
     */
    public function sendReady4PickupToStore($deliveryOrder, $cancelled = false)
    {
        /** @var Vznl_Communication_Helper_Email $emailHelper */
        $emailHelper = Mage::helper('communication/email');
        $superOrderWebsiteCode = Mage::app()->getWebsite($deliveryOrder->getSuperOrder()->getWebsiteId())->getCode();
        $notice = $cancelled ? 'cancelled notice' : 'notice';
        try {
            $payloadForStore = $emailHelper->getOrderReadyForPickupStorePayload($deliveryOrder, $cancelled);
            $message = 'Send ready4pickup %s email towards delivery store for order: %s';
            if($payloadForStore){
                Mage::getSingleton('communication/pool')->add($payloadForStore, $superOrderWebsiteCode);
            }else{
                $message = 'Ready4pickup %s email towards delivery store for order: %s is not send since the delivery store does not have an email address yet.';
            }
            $this->_logMsg(sprintf($message, $notice,  $deliveryOrder->getIncrementId()));
        } catch (Exception $e) {
            $message = 'Failed sending ready4pickup %s email towards delivery store for order: %s';
            $this->_logMsg(sprintf($message, $notice,  $deliveryOrder->getIncrementId()));
            Mage::log($e->getMessage(), null, 'EmailErrorLog.log', true);
        }
    }

    protected function getDeliveryOrder($incrementId)
    {
        return Mage::getModel('sales/order')->loadByIncrementId($incrementId);
    }

}
