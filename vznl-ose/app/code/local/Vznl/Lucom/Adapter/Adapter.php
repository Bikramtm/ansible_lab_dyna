<?php

use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Zend\Soap\Client;

/**
 * Class Vznl_Lucom_Adapter_Adapter
 */
class Vznl_Lucom_Adapter_Adapter
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var Exception
     */
    private $exception;

    /**
     * @var array
     */
    private $options = [];

    /**
     * @var Dyna_Checkout_Model_Sales_Order
     */
    private $deliveryOrder;

    /**
     * @var bool
     */
    private $stubMode;

    /**
     * @var bool
     */
    private $isFixed;

    public function __construct(Client $client, LoggerInterface $logger, array $options = [], $stubbed = false)
    {
        $this->client = $client;
        $this->logger = $logger;
        $this->options = array_merge($this->options, $options);
        $this->stubMode = $stubbed;
    }

    /**
     * Get the last exception
     * @return Exception
     */
    public function getLastException()
    {
        return $this->exception;
    }

    /**
     * Send data towards Lucom's GenericDataService
     * @param Dyna_Checkout_Model_Sales_Order $deliveryOrder
     * @return array|bool The array with response data or <false> if something completly went wrong
     */
    public function send($deliveryOrder)
    {
        $this->deliveryOrder = $deliveryOrder;
        //setting isFixed or not
        $this->isFixed = $this->deliveryOrder->hasFixed();     
        $requestData = $this->buildRequest();

        // Check if stub mode is on
        if ($this->stubMode) {
            $response = $this->getStubbedResponse();
            $this->logStub($requestData, $response);
            return $this->prepareResponse($response);
        }

        ini_set('default_socket_timeout', isset($this->options['timeout']) ? $this->options['timeout'] : 10);
        try {
            $this->client->setLocation($this->options['endpoint']);
            $this->client->putData(null, $requestData, ($this->isFixed ? 'startUBuyFixedContract.js' : 'startUBuyContract.js'));
            $this->log();
        } catch (Exception $e) {
            $this->log($e);
            $this->exception = $e;
            return false;
        }

        $response = $this->client->getLastResponse();
        return $this->prepareResponse($response);
    }

    /**
     * Build the request
     * @return SimpleXMLElement The request data
     */
    protected function buildRequest()
    {
        $isConsumer = (!$this->deliveryOrder->getCustomer()->getIsBusiness());
        $networkOperator = $this->getNetworkOperator();

        // Convert created at date to the right format
        $createdAt = $this->deliveryOrder->getCreatedAt();
        if ($createdAt) {
            $createdAt = date('Y-m-d h:m:s', strtotime($createdAt));
            $createdAt = str_replace(' ', 'T', $createdAt);
        }

        // Building the xml
        $root = new SimpleXMLElement('<root/>');
        $rootElement = $root->addChild($this->isFixed?'startUBuyFixedContractRequest':'startUBuyContractRequest');

        // Add default data
        if ($this->isFixed) {
            $dataNode = $rootElement->addChild('UBuyFixedContractXML');
            $dataNode->addChild('ContractType', ($isConsumer) ? 'Consumer' : 'Business');
            $dataNode->addChild('NetworkOperator', 'Ziggo');
            $dataNode->addChild('DateCreated', str_replace(' ', 'T', date('Y-m-d h:m:s')));
        } else {
            $dataNode = $rootElement->addChild('UBuyContractXML');
            $dataNode->addChild('ContractType', ($isConsumer) ? 'Consumer' : 'Business');
            $dataNode->addChild('NetworkOperator', $networkOperator);
            $dataNode->addChild('DateCreated', $createdAt);
        }

        $dataNode->addChild('DealerCoder', $this->deliveryOrder->getDealer()->getVfDealerCode());

        $this->addCustomerDetails($dataNode);
        if (!$isConsumer && !$this->isFixed) {
            $this->addCompanyDetails($dataNode);
        }
        if (!$this->isFixed) {
            $this->addOrderDetails($dataNode);
        } else {
            $this->addFixedOrderDetails($dataNode);
        }

        // Add contract
        if ($this->isFixed) {
            $contract = Mage::helper('vznl_checkout/sales')->generateFixedContract($this->deliveryOrder, true, true);
        } else {
            $contract = Mage::helper('vznl_checkout/sales')->generateContract($this->deliveryOrder, false, true);
        }
        $contractBase64 = base64_encode($contract);
        $rootElement->addChild('ContractBase64', $contractBase64);

        return $rootElement->asXML();
    }

    /**
     * @param SimpleXMLElement $node
     * @return SimpleXMLElement
     */
    protected function addCustomerDetails($node)
    {
        /** @var Dyna_Customer_Model_Customer_Customer $customer */
        $customer = $this->deliveryOrder->getCustomer();
        $isConsumer = !$customer->getIsBusiness();

        // Convert date of birth to the right format
        $dateOfBirth = ($isConsumer) ? $this->deliveryOrder->getCustomerDob() : $this->deliveryOrder->getContractantDob();
        if ($dateOfBirth) {
            $dateOfBirth = date('Y-m-d', strtotime($dateOfBirth));
        }

        // Convert document expiry date to the right format
        $documentExpiryDate = ($isConsumer) ? $this->deliveryOrder->getCustomerValidUntil() : $customer->getContractantValidUntil();
        if ($documentExpiryDate) {
            $documentExpiryDate = date('Y-m-d', strtotime($documentExpiryDate));
        }

        // Start building the node
        $customerNode = $node->addChild('CustomerDetails');
        $customerNode->addChild('CustomerID', $customer->getBan());

        if ($isConsumer) {
            $nameNode = $customerNode->addChild('Name');
            $nameNode->addChild('Title', $this->deliveryOrder->getCustomerPrefix());
            $nameNode->addChild('Initials', $this->deliveryOrder->getCustomerFirstname());
            $nameNode->addChild('LastName', join(' ', array_filter(array(
                $this->deliveryOrder->getCustomerMiddlename(),
                $this->deliveryOrder->getCustomerLastname()
            ))));

            $this->addAddressNode($customerNode);
        }

        // Get the customer email address
        $emails = explode(';', $this->deliveryOrder->getAdditionalEmail());
        $emailAddress = !is_null($this->deliveryOrder->getCorrespondanceEmail()) ? $this->deliveryOrder->getCorrespondanceEmail() : $emails[0];

        if ($emailAddress) {
            $customerNode->addChild('EmailAddress', $emailAddress);
        }
        $customerNode->addChild('DateOfBirth', $dateOfBirth);

        if (!$this->isFixed) {
            $identificationNode = $customerNode->addChild('Identification');
            if ($docType = $this->convertIdType(($isConsumer) ? $this->deliveryOrder->getCustomerIdType() : $this->deliveryOrder->getContractantIdType())) {
                $identificationNode->addChild('DocType', $docType);
            }
            if ($docNr = ($isConsumer) ? $this->deliveryOrder->getCustomerIdNumber() : $this->deliveryOrder->getContractantIdNumber()) {
                $identificationNode->addChild('DocNr', $docNr);
            }
            if ($documentExpiryDate) {
                $identificationNode->addChild('DocExpiryDate', $documentExpiryDate);
            }
        
            $paymentNode = $customerNode->addChild('PaymentInformation');
            $paymentNode->addChild('BankAccountNumber', ($this->deliveryOrder->getCustomerAccountNumberLong() ?: $this->deliveryOrder->getCustomerAccountNumber()));
            $paymentNode->addChild('BankAccountHolder', $this->deliveryOrder->getCustomerAccountHolder());
            $paymentNode->addChild('PaymentMethod', Mage::helper('dyna_checkout')->getPaymentOptionTextForName($this->deliveryOrder->getPayment()->getMethod()));
        }

        return $node;
    }

    /**
     * @param SimpleXMLElement $node
     * @return SimpleXMLElement
     */
    protected function addCompanyDetails($node)
    {
        // Start building the node
        $companyNode = $node->addChild('CompanyDetails');
        $companyNode->addChild('CompanyName', empty($this->deliveryOrder->getCompanyName()) ? 
            join(' ', array_filter(array(
            $this->deliveryOrder->getCustomer()->getContractantPrefix(),
            $this->deliveryOrder->getContractantFirstname(),
            $this->deliveryOrder->getContractantMiddlename(),
            $this->deliveryOrder->getContractantLastname()
        ))) : $this->deliveryOrder->getCompanyName());

        $this->addAddressNode($companyNode);
       
        $coc = str_pad($this->deliveryOrder->getCompanyCoc(), 8, "0", STR_PAD_RIGHT);
        $companyNode->addChild('KVKNumber', $coc);
        if ($btwNumber = $this->deliveryOrder->getCompanyVatId()) {
            $companyNode->addChild('BTWNumber', $btwNumber);
        }

        $contactNode = $companyNode->addChild('ContactPerson');
        $title = $this->deliveryOrder->getContractantPrefix();
        if ($title == 1) {
           $title = 'Mr.'; 
        } else if ($title == 2) {
           $title = 'Mrs.';
        }
        $contactNode->addChild('Title', $title);
        $contactNode->addChild('Initials', $this->deliveryOrder->getContractantFirstname());
        $contactNode->addChild('LastName', join(' ', array_filter(array(
            $this->deliveryOrder->getContractantMiddlename(),
            $this->deliveryOrder->getContractantLastname()
        ))));
    }

    /**
     * @param SimpleXMLElement $node
     * @return SimpleXMLElement
     */
    protected function addFixedOrderDetails($node)
    {
        $orderDetailsNode = $node->addChild('OrderDetails');
        $orderDetailsNode->addChild('DateCreated', str_replace(' ', 'T', date('Y-m-d h:m:s')));
        $activationDate = $this->deliveryOrder->getFixedDeliveryWishDate();
        $orderDetailsNode->addChild('ActivationDate', str_replace(' ', 'T', date('Y-m-d h:m:s', strtotime($activationDate))));
    }

    /**
     * @param SimpleXMLElement $node
     * @return SimpleXMLElement
     */
    protected function addOrderDetails($node)
    {
        // Start building the node
        $orderDetailsNode = $node->addChild('OrderDetails');
        $packagesNode = $orderDetailsNode->addChild('Packages');

        /** @var Vznl_Package_Model_Package $package */
        foreach ($this->deliveryOrder->getPackages() as $package) {
            if ($package->isFixed())
                continue;
            /** @var Dyna_Superorder_Model_Superorder $superOrder */
            $superOrder = $package->getSuperOrder();
            $isIndirectOrder = $this->deliveryOrder->isOrderedInIndirect();

            // Get the transaction type
            $transactionType = '';
            if ($package->isNumberPorting()) {
                $transactionType = 'Numberporting';
            } elseif ($package->isAcquisition()) {
                $transactionType = 'NewNumber';
            } elseif ($package->isRetention() || $package->isILS() || $package->isMove()) {
                $transactionType = 'Retention';
            }

            // Extract some info from the package items
            $subscriptionType = '';
            $subscriptionDuration = '';
            $deviceType = '';
            $deviceLoanPeriod = '';
            foreach ($this->deliveryOrder->getAllItems() as $item) {
                if ($item->getPackageId() == $package->getPackageId()) {
                    $product = $item->getProduct();
                    $flippedProduct = array_flip($product->getType());
                    if (isset($flippedProduct[Vznl_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION])) {
                        // Is subscription
                        $subscriptionType = $product->getName();
                        $subscriptionDuration = $product->getAttributeText('identifier_commitment_months');
                    } else if (isset($flippedProduct[Vznl_Catalog_Model_Type::SUBTYPE_DEVICE])) {
                        // is device
                        $deviceType = $product->getName();
                    } else if (isset($flippedProduct[Vznl_Catalog_Model_Type::SUBTYPE_DEVICE_SUBSCRIPTION])) {
                        $deviceLoanPeriod = $product->getAttributeText('identifier_commitment_months');
                    } else if (isset($flippedProduct[Vznl_Catalog_Model_Type::SUBTYPE_FIXED_PACKAGE])) {
                        $subscriptionType = $product->getName();
                    } else if (isset($flippedProduct[Vznl_Catalog_Model_Type::SUBTYPE_FIXED_HARDWARE_MATERIALS])) {
                        $deviceType = $deviceType.", ".$product->getName();
                    }
                }
            }

            if (!$deviceType && $isIndirectOrder) {
                $deviceType = $package->getDeviceName();
            }

            // Convert created at date to the right format
            $createdAt = $package->getCreatedAt();
            if ($createdAt) {
                $createdAt = date('Y-m-d h:m:s', strtotime($createdAt));
                $createdAt = str_replace(' ', 'T', $createdAt);
            }

            // Add the package node
            $packageNode = $packagesNode->addChild('Package');
            $packageNode->addChild('CTN', $package->getTelNumber());
            $packageNode->addChild('TransactionType', $transactionType);
            $packageNode->addChild('SubscriptionType', $subscriptionType);
            $packageNode->addChild('SubscriptionPeriod', $subscriptionDuration);
            $packageNode->addChild('OrderID', $superOrder->getOrderNumber());
            $packageNode->addChild('DateCreated', $createdAt);
            $packageNode->addChild('DeviceType', $deviceType);
            if ($deviceLoanPeriod) {
                $packageNode->addChild('DeviceLoanPeriod', $deviceLoanPeriod);
            }
        }
    }

    /**
     * @param SimpleXMLElement $node
     * @return SimpleXMLElement
     */
    protected function addAddressNode($node)
    {
        $address = $this->deliveryOrder->getBillingAddress();

        // Add the address node
        $addressNode = $node->addChild('Address');
        $addressNode->addChild('Street', $address->getStreet1());
        $addressNode->addChild('HouseNumber', $address->getStreet2());
        if ($houseNumberAddition = $address->getStreet3()) {
            $addressNode->addChild('HouseNumberAddition', $houseNumberAddition);
        }
        $addressNode->addChild('PostalCode', $address->getPostcode());
        $addressNode->addChild('City', $address->getCity());
        $addressNode->addChild('Country', Mage::app()->getLocale()->getCountryTranslation($address->getCountry()));
    }

    /**
     * Get the network operator
     * @return string The network operator
     */
    protected function getNetworkOperator()
    {
        foreach ($this->deliveryOrder->getPackages() as $package) {
            if ($package->getNetworkOperator()) {
                return $package->getNetworkOperator();
            }
        }
        return '';
    }

    /**
     * Get the right name by the given Id type.
     * @param $idType the id type
     * @return mixed The name
     */
    protected function convertIdType($idType)
    {
        $identificationTypes = [
            'N' => 'Dutch ID card',
            'P' => 'Passport',
            'R' => 'Drivers license',
            '0' => 'Type EU',
            '1' => 'Type I',
            '2' => 'Type II',
            '3' => 'Type III',
            '4' => 'Type IV',
        ];

        if (!array_key_exists($idType, $identificationTypes)) {
            return false;
        }
        return $identificationTypes[$idType];
    }

    /**
     * Prepare the response to be returned as an array, also skip the soap envelops etc
     * @param $response The xml response
     * @return array The xml as array.
     */
    protected function prepareResponse($response)
    {
        $responseArray = Mage::helper('vznl_core/service')->xmlStringToArray($response);
        $actualResponse = isset($responseArray['soapenv:Body']) ? $responseArray['soapenv:Body'] : [];
        $actualResponse = isset($actualResponse['ns1:putDataResponse']) ? $actualResponse['ns1:putDataResponse'] : [];
        $actualResponse = isset($actualResponse['putDataReturn']) ? $actualResponse['putDataReturn'] : [];
        if ($actualResponse && isset($actualResponse['@content'])) {
            return Mage::helper('vznl_core/service')->xmlStringToArray($actualResponse['@content']);
        }
        return $responseArray;
    }

    /**
     * Get the stubbed response from the GenericDataService of Lucom.
     * @return mixed The json response.
     */
    protected function getStubbedResponse()
    {
        $status = 'SUCCESS';
        $code = 0;
        $message = 'https://google.nl';

        /** @var Dyna_Customer_Model_Customer_Customer $customer */
        $customer = $this->deliveryOrder->getCustomer();
        $customerLastName = (!$customer->getIsBusiness()) ? $customer->getLastname() : $customer->getContractantLastname();

        switch ($customerLastName) {
            case 'LucomInvalid':
                $status = 'ERROR';
                $code = 100;
                $message = 'Input is not a valid XML string';
                break;
            case 'LucomNotValid':
                $status = 'ERROR';
                $code = 110;
                $message = 'XML validation error: \<message\>';
                break;
            case 'LucomDocType':
                $status = 'ERROR';
                $code = 120;
                $message = 'Document type not supported';
                break;
            case 'LucomMissingContract':
                $status = 'ERROR';
                $code = 200;
                $message = 'Missing Contract PDF';
                break;
            case 'LucomInvalidContract':
                $status = 'ERROR';
                $code = 210;
                $message = 'Invalid Contract PDF';
                break;
            case 'LucomMissingSignature':
                $status = 'ERROR';
                $code = 220;
                $message = 'Missing PDF signature field';
                break;
            case 'LucomError':
                $status = 'ERROR';
                $code = 999;
                $message = 'Internal error / Unhandled exception';
                break;
        }

        $responseRoot = new SimpleXMLElement('<startUBuyContractResponse/>');
        $responseRoot->addChild('status', $status);
        $responseRoot->addChild('code', $code);
        $responseRoot->addChild('message', $message);

        $root = new SimpleXMLElement('<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"/>');
        $bodyNode = $root->addChild('soap:Body', null, 'http://schemas.xmlsoap.org/soap/envelope/');
        $bodyResponseNode = $bodyNode->addChild('ns1:putDataResponse', null, 'http://www.lucom.com/ffw/GenericDataService');
        $bodyResponseNode->addChild('putDataReturn', null, '');

        $xmlString = $root->asXML();
        $responseData = str_replace('<?xml version="1.0"?>', '', $responseRoot->asXML());
        $cdata = sprintf('<![CDATA[%s]]>', $responseData);
        $xmlString = str_replace('<putDataReturn xmlns=""/>', '<putDataReturn xmlns:xsi="http://www.lucom.com/" xsi:type="xsd:string">' . $cdata . '</putDataReturn>', $xmlString);
        return $xmlString;
    }

    /**
     * Log the request/response of the actual performed call
     * @param null|Exception $e The exception if any
     */
    protected function log($e = null)
    {
        $log = "\n";
        $log .= sprintf("Request headers:\n%s\n", $this->client->getLastRequestHeaders());
        $log .= sprintf("Request:\n%s\n", $this->format($this->client->getLastRequest()));
        $log .= sprintf("Response headers:\n%s\n", $this->client->getLastResponseHeaders());
        $responseMessage = $this->client->getLastResponse();
        $responseMessage = $responseMessage ? $this->format($responseMessage) : $responseMessage;
        if ($e != null) {
            if (empty($responseMessage)) {
                $responseMessage = $e->getMessage() . "\n";
                $responseMessage .= $e->getTraceAsString();
            }
        }
        $log .= sprintf("Response:\n%s", $responseMessage);
        $this->logger->log(LogLevel::DEBUG, $log);
    }

    /**
     * Log the stubbed request/response
     * @param $requestData The request to log
     * @param $response The response to log
     */
    protected function logStub($requestData, $response)
    {
        $requestData = htmlspecialchars($this->format($requestData));
        $log = "\n";
        $log .= sprintf("Request headers:\n%s\n", '');
        $log .= sprintf("Request:\n%s\n\n", $this->hideContractBase64($requestData));
        $log .= sprintf("Response headers:\n%s\n", '');
        $log .= sprintf("Response:\n%s", $this->format($response));
        $this->logger->log(LogLevel::DEBUG, $log);
    }

    /**
     * Format the given xml string
     *
     * @param $mixed The xml string
     * @return string The formatted xml string
     */
    protected function format($mixed)
    {
        try {
            $mixed = $this->hideContractBase64($mixed);
            $dom = new DOMDocument();
            $dom->preserveWhiteSpace = false;
            $dom->formatOutput = false;
            if (!empty($mixed)) {
                $dom->loadXML($mixed);
            }
            return preg_replace("/\n/", "", $dom->saveXML());
        } catch (Exception $e) {
            Mage::logException($e);
            return $mixed;
        }
    }

    /**
     * Hide data from given string, replacing it with '***'.
     * @param string $string The string to remove data from.
     * @return string The adjusted string
     */
    protected function hideContractBase64($string)
    {
        return preg_replace('/(&lt;ContractBase64&gt;)(.*?)(&lt;\/ContractBase64&gt;)/', '&lt;ContractBase64&gt;***&lt;/ContractBase64&gt;', $string);
    }
}
