<?php
/**
 * Installer that adds order_type colum in Quote and Order table.
 */

/** @var Mage_Core_Model_Resource_Setup $this */

$installer = $this;

$installer->startSetup();

$installer->getConnection()->addColumn($installer->getTable("sales/quote"), "order_type", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 50,
    "comment" => 'order type',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/order"), "order_type", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 50,
    "comment" => 'order type',
]);


$installer->getConnection()->addColumn($installer->getTable("sales/quote_item"), "new_hardware", [
    "type" => Varien_Db_Ddl_Table::TYPE_INTEGER,
    "length" => 1,
    "default" => 0,
    "comment" => 'bom id from peal validate basket call',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/order_item"), "new_hardware", [
    "type" => Varien_Db_Ddl_Table::TYPE_INTEGER,
    "length" => 1,
    "default" => 0,
    "comment" => 'bom id from peal validate basket call',
]);

$installer->endSetup();

