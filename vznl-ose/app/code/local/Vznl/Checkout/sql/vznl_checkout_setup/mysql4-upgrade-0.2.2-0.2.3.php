<?php
/**
 * Installer that adds the fixed_written_consent
 * column after customer_number
 */

/** @var Mage_Core_Model_Resource_Setup $this */

$installer = $this;

$installer->startSetup();

$installer->getConnection()->addColumn($installer->getTable("sales/quote"), "fixed_separate_delivery_allowed", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 45,
    "after" => "fixed_delivery_type",
    "comment" => 'separate_delivery_allowed from overview',
]);

$installer->endSetup();