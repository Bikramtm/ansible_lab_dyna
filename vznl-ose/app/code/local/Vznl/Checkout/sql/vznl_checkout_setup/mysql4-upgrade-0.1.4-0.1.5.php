<?php
/**
 * Installer that adds the fixed_payment_method_monthly_charges,
 * fixed_bill_distribution_method,
 * fixed_payment_method_one_time_charge,
 * fixed_account_holder_name,
 * fixed_iban column after banknumber_accountname
 */

/** @var Mage_Core_Model_Resource_Setup $this */

$this->startSetup();

// Add fixed_payment_method_monthly_charges on sales quote resource
$this->getConnection()->addColumn($this->getTable("sales/quote"), "fixed_payment_method_monthly_charges", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 100,
    "after" => "banknumber_accountname",
    "comment" => "Payment Method Monthly Charges from validatecart services",
]);

// Add fixed_bill_distribution_method on sales quote resource
$this->getConnection()->addColumn($this->getTable("sales/quote"), "fixed_bill_distribution_method", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 100,
    "after" => "fixed_payment_method_monthly_charges",
    "comment" => "Bill Distribution Method from validatecart services",
]);

// Add fixed_payment_method_one_time_charge on sales quote resource
$this->getConnection()->addColumn($this->getTable("sales/quote"), "fixed_payment_method_one_time_charge", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 100,
    "after" => "fixed_payment_method_monthly_charges",
    "comment" => "Payment Method One Time Charge from validatecart services",
]);

// Add fixed_telephone_number on sales order resource
$this->getConnection()->addColumn($this->getTable("sales/order"), "fixed_telephone_number", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 20,
    "after" => "customer_number",
    "comment" => "External fixed telephone number to which the superorder maps to",
]);

// Add fixed_payment_method_monthly_charges on sales order resource
$this->getConnection()->addColumn($this->getTable("sales/order"), "fixed_payment_method_monthly_charges", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 100,
    "after" => "banknumber_accountname",
    "comment" => "Payment Method Monthly Charges from validatecart services",
]);

// Add fixed_bill_distribution_method on sales order resource
$this->getConnection()->addColumn($this->getTable("sales/order"), "fixed_bill_distribution_method", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 100,
    "after" => "fixed_payment_method_monthly_charges",
    "comment" => "Bill Distribution Method from validatecart services",
]);

// Add fixed_payment_method_one_time_charge on sales order resource
$this->getConnection()->addColumn($this->getTable("sales/order"), "fixed_payment_method_one_time_charge", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 100,
    "after" => "fixed_payment_method_monthly_charges",
    "comment" => "Payment Method One Time Charge from validatecart services",
]);

$this->endSetup();