<?php
/**
 * Installer that adds eal_different_address_id colum in Quote and Order table.
 */

/** @var Mage_Core_Model_Resource_Setup $this */

$installer = $this;

$installer->startSetup();

$installer->getConnection()->addColumn($installer->getTable("sales/quote"), "peal_different_address_id", [
    "type" => Varien_Db_Ddl_Table::TYPE_INTEGER,
    "length" => 50,
    "comment" => 'fixed delivery different address id',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/order"), "peal_different_address_id", [
    "type" => Varien_Db_Ddl_Table::TYPE_INTEGER,
    "length" => 50,
    "comment" => 'fixed delivery different address id',
]);

$installer->endSetup();