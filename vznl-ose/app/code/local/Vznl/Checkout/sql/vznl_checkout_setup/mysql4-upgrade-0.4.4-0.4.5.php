<?php

/* @var $customerInstaller Mage_Customer_Model_Resource_Setup */
$customerInstaller = new Mage_Customer_Model_Resource_Setup('core_setup');
$customerInstaller->startSetup();
$setup = Mage::getSingleton('core/resource')->getConnection('core_read');

// Fixes issues with superorder_id index
$customerInstaller->run("ALTER TABLE `sales_flat_order` DROP INDEX `IDX_SALES_FLAT_SUPERORDERID`;");
$customerInstaller->run("ALTER TABLE `sales_flat_order` ADD INDEX `IDX_SALES_FLAT_SUPERORDERID` (`superorder_id` ASC);");

$customerInstaller->endSetup();
