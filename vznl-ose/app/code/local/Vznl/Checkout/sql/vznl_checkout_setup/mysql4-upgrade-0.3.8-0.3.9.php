<?php
/**
 * Copyright (c) 2019. Dynacommerce B.V.
 */

/* @var $salesInstaller Mage_Sales_Model_Resource_Setup */
$salesInstaller = new Mage_Sales_Model_Resource_Setup('core_setup');
$salesInstaller->startSetup();

$quoteItemAttr = array(
    'contract_start_date' => array(
        'comment'       => 'Contract Start Date',
        'type'          => Varien_Db_Ddl_Table::TYPE_DATETIME,
        'required'      => false
    ),
);
foreach ($quoteItemAttr as $attributeCode => $attributeProp) {
    $salesInstaller->addAttribute('quote_address_item', $attributeCode, $attributeProp);
    $salesInstaller->addAttribute('quote_item', $attributeCode, $attributeProp);
    $salesInstaller->addAttribute('order_item', $attributeCode, $attributeProp);
}
/* end QUOTE_ITEM and ORDER_ITEM attributes */

$salesInstaller->endSetup();
