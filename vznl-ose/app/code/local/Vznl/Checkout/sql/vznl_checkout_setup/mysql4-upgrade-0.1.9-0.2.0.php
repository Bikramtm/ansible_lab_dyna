<?php
/**
 * Installer that adds the fixed_written_consent
 * column after customer_number
 */

/** @var Mage_Core_Model_Resource_Setup $this */

$installer = $this;

$installer->startSetup();

$installer->getConnection()->addColumn($installer->getTable("sales/quote"), "fixed_delivery_check", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "comment" => 'delivery_check from overview',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/quote"), "fixed_delivery_method", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 45,
    "comment" => 'delivery_method from overview',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/quote"), "fixed_delivery_type", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 45,
    "comment" => 'delivery_type from overview',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/order"), "fixed_delivery_check", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "comment" => 'delivery_check from overview',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/order"), "fixed_delivery_method", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 45,
    "comment" => 'delivery_method from overview',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/order"), "fixed_delivery_type", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 45,
    "comment" => 'delivery_type from overview',
]);

$installer->endSetup();

