<?php
/**
 * Installer that adds the ziggo_telephone, ziggo_email columns
 */

/** @var Mage_Core_Model_Resource_Setup $this */

$installer = $this;

$installer->startSetup();

$installer->getConnection()->addColumn($installer->getTable("catalog_package"), "fixed_number_porting_status", [
    "type" => Varien_Db_Ddl_Table::TYPE_INTEGER,
    "length" => 1,
    "after" => "service_address_id",
    "comment" => 'Number porting enable or disable',
]);

$installer->getConnection()->addColumn($installer->getTable("catalog_package"), "fixed_first_line_phone", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 12,
    "after" => "fixed_number_porting_status",
    "comment" => 'Fixed first line phone number',
]);

$installer->getConnection()->addColumn($installer->getTable("catalog_package"), "fixed_second_line_phone", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 12,
    "after" => "fixed_first_line_phone",
    "comment" => 'Fixed second line phone number',
]);

$installer->getConnection()->addColumn($installer->getTable("catalog_package"), "fixed_overstappen_status", [
    "type" => Varien_Db_Ddl_Table::TYPE_INTEGER,
    "length" => 1,
    "after" => "fixed_second_line_phone",
    "comment" => 'Fixed overstappen enable or disable',
]);

$installer->getConnection()->addColumn($installer->getTable("catalog_package"), "fixed_overstappen_current_provider", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "after" => "fixed_overstappen_status",
    "comment" => 'Fixed overstappen current provider',
]);

$installer->getConnection()->addColumn($installer->getTable("catalog_package"), "fixed_overstappen_delivery_date", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "after" => "fixed_overstappen_current_provider",
    "comment" => 'Fixed overstappen delivery wish date',
]);

$installer->getConnection()->addColumn($installer->getTable("catalog_package"), "fixed_overstappen_contract_id", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "after" => "fixed_overstappen_delivery_date",
    "comment" => 'Fixed overstappen contract id',
]);

$installer->getConnection()->addColumn($installer->getTable("catalog_package"), "fixed_overstappen_termination_fee", [
    "type" => Varien_Db_Ddl_Table::TYPE_INTEGER,
    "length" => 1,
    "after" => "fixed_overstappen_contract_id",
    "comment" => 'Fixed overstappen termination fee',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/quote_item"), "ean_number", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 50,
    "comment" => 'EAN number',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/quote_item"), "box_serial_number", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 50,
    "comment" => 'Box serial number',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/order_item"), "ean_number", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 50,
    "comment" => 'EAN number',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/order_item"), "box_serial_number", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 50,
    "comment" => 'Box serial number',
]);

$installer->endSetup();

