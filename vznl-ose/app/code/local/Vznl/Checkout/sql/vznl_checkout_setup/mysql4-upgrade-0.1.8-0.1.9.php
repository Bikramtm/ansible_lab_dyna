<?php
/**
 * Installer that adds the fixed_written_consent
 * column after customer_number
 */

/** @var Mage_Core_Model_Resource_Setup $this */

$this->startSetup();

// Add peal_order_id to superorder resource
$this->getConnection()->addColumn($this->getTable("superorder"), "peal_order_id", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 50,
    "comment" => "peal order id received from oil",
]);

