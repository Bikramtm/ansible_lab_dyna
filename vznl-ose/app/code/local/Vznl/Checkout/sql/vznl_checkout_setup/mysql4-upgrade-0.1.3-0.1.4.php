<?php

$installer = $this;

$installer->startSetup();

$installer->getConnection()->addColumn($installer->getTable("sales/quote_item"), "basket_item_id", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 45,
    "comment" => 'basket item id from peal validate basket call',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/quote_item"), "offer_id", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 45,
    "comment" => 'offer id from peal validate basket call',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/quote_item"), "action", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 45,
    "comment" => 'action from peal validate basket call',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/quote_item"), "bom_id", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 45,
    "comment" => 'bom id from peal validate basket call',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/quote_item"), "product_name", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 45,
    "comment" => 'product name from peal validate basket call',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/quote_item"), "payment_method", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "comment" => 'payment method from peal validate basket call',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/quote_item"), "dispatch_method", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "comment" => 'dispatch method from peal validate basket call',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/quote"), "service_address", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "comment" => 'service address from peal api call',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/order_item"), "basket_item_id", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 45,
    "comment" => 'basket item id from peal validate basket call',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/order_item"), "offer_id", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 45,
    "comment" => 'offer id from peal validate basket call',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/order_item"), "action", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 45,
    "comment" => 'action from peal validate basket call',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/order_item"), "bom_id", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 45,
    "comment" => 'bom id from peal validate basket call',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/order_item"), "product_name", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 45,
    "comment" => 'product name from peal validate basket call',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/order_item"), "dispatch_method", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "comment" => 'dispatch method from peal validate basket call',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/order_item"), "payment_method", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "comment" => 'payment method from peal validate basket call',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/order"), "service_address", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "comment" => 'service address from peal api call',
]);

$installer->endSetup();
