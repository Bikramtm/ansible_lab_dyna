<?php

$installer = $this;

$installer->startSetup();

$installer->getConnection()->addColumn($installer->getTable("sales/quote"), "customer_issue_date", [
    'type' => Varien_Db_Ddl_Table::TYPE_DATE,
    "after" => "customer_issuing_country",
    "comment" => 'ID issue date',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/quote"), "contractant_issue_date", [
    'type' => Varien_Db_Ddl_Table::TYPE_DATE,
    "after" => "contractant_id_number",
    "comment" => 'ID issue date',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/order"), "customer_issue_date", [
    'type' => Varien_Db_Ddl_Table::TYPE_DATE,
    "after" => "customer_issuing_country",
    "comment" => 'ID issue date',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/order"), "contractant_issue_date", [
    'type' => Varien_Db_Ddl_Table::TYPE_DATE,
    "after" => "contractant_id_number",
    "comment" => 'ID issue date',
]);

$installer->endSetup();
