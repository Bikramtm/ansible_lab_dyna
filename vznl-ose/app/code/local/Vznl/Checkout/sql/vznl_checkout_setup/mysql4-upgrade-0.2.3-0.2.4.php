<?php
/**
 * Installer that adds the fixed_written_consent
 * column after customer_number
 */

/** @var Mage_Core_Model_Resource_Setup $this */

$this->startSetup();

// Add peal_order_status to superorder resource
$this->getConnection()->addColumn($this->getTable("superorder"), "peal_order_status", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 255,
    "comment" => "peal order status received from oil",
]);