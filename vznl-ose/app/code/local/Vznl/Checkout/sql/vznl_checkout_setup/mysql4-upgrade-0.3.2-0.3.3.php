<?php
/**
 * Remove the foreign key from Quote Id
 */

/** @var Mage_Core_Model_Resource_Setup $this */

$installer = $this;
/* @var $installer Mage_Sales_Model_Entity_Setup */
$installer->startSetup();
$installer->run("
ALTER TABLE sales_flat_quote_item DROP FOREIGN KEY FK_SALES_FLAT_QUOTE_ITEM_QUOTE_ID_SALES_FLAT_QUOTE_ENTITY_ID;
ALTER TABLE `sales_flat_quote_item` CHANGE `quote_id` `quote_id` INT(10) UNSIGNED NULL DEFAULT NULL COMMENT 'Quote Id';
ALTER TABLE `sales_flat_quote_item` ADD INDEX(`alternate_quote_id`);
");
$installer->installEntities();
$installer->endSetup();
