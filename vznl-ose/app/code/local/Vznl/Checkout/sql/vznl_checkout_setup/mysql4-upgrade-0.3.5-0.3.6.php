<?php

Mage::getConfig()->saveConfig('checkout/vodafoneoptions/saved_carts_validity_period' , 14, 'default', 0);

Mage::getConfig()->saveConfig('checkout/vodafoneoptions/saved_carts_readonly_period' , 1, 'default', 0);

Mage::getConfig()->saveConfig('checkout/vodafoneoptions/saved_offers_validity_period' , 14, 'default', 0);

Mage::getConfig()->saveConfig('checkout/vodafoneoptions/saved_offers_readonly_period' , 1, 'default', 0);

Mage::getModel('core/config')->cleanCache();
