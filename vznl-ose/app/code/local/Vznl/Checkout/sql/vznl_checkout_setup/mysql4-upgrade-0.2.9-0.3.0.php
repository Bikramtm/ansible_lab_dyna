<?php

$installer = $this;

$installer->startSetup();

$installer->getConnection()->addColumn($installer->getTable("sales/quote_item"), "added_via_peal", [
    "type" => Varien_Db_Ddl_Table::TYPE_INTEGER,
    "length" => 1,
    "default" => 0,
    "comment" => 'bom id from peal validate basket call',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/order_item"), "added_via_peal", [
    "type" => Varien_Db_Ddl_Table::TYPE_INTEGER,
    "length" => 1,
    "default" => 0,
    "comment" => 'bom id from peal validate basket call',
]);

$installer->endSetup();