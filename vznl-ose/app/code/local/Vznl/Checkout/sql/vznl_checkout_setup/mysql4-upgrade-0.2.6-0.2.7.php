<?php
/**
 * Installer that adds the ziggo_telephone, ziggo_email columns
 */

/** @var Mage_Core_Model_Resource_Setup $this */

$installer = $this;

$installer->startSetup();

$installer->getConnection()->addColumn($installer->getTable("sales/quote"), "ziggo_telephone", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "after" => "additional_email",
    "comment" => 'Ziggo telephone number',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/quote"), "ziggo_email", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "after" => "ziggo_telephone",
    "comment" => 'Ziggo email address',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/order"), "ziggo_telephone", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "after" => "additional_email",
    "comment" => 'Ziggo telephone number',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/order"), "ziggo_email", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "after" => "ziggo_telephone",
    "comment" => 'Ziggo email address',
]);

$installer->endSetup();