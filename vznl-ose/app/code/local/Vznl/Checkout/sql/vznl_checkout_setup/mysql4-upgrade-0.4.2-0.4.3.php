<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

$installer = $this;

$installer->startSetup();

// Add call_center_activity in agent table
$this->getConnection()->addColumn($this->getTable("sales/order"), "edited_packages_ids", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 50,
    "comment" => "Edited Package Ids",
]);

$installer->endSetup();
