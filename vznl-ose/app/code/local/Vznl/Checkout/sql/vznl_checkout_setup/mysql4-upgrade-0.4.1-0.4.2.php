<?php
/**
 * Copyright (c) 2019. Dynacommerce B.V.
 */

/** @var Mage_Core_Model_Resource_Setup $installer */

$installer = $this;
$installer->startSetup();

$connection = $installer->getConnection();
$table = $this->getTable('sales/quote');

$index = 'IDX_SALES_FLAT_QUOTE_CUST_ID_CART_STATUS';
$connection->addIndex($table, $index, array('customer_id', 'cart_status'));

$index = 'IDX_SALES_FLAT_QUOTE_CUST_ID_CART_STATUS_IS_OFFER';
$connection->addIndex($table, $index, array('customer_id', 'cart_status', 'is_offer'));

$installer->endSetup();