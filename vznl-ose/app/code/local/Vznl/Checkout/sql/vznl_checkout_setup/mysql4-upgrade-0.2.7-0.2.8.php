<?php
/**
 * Installer that adds the fixed_written_consent
 * column after customer_number
 */

/** @var Mage_Core_Model_Resource_Setup $this */

$installer = $this;

$installer->startSetup();

$installer->getConnection()->addColumn($this->getTable("sales/quote"), "footprint", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 100,
    "after" => "fixed_written_consent",
    "comment" => "footprint from peal",
]);

$installer->getConnection()->addColumn($this->getTable("sales/order"), "footprint", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 20,
    "after" => "fixed_written_consent",
    "comment" =>  "footprint from peal",
]);


$installer->getConnection()->addColumn($installer->getTable("sales/quote_address"), "additional_line", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "after" => "postcode",
    "comment" => 'Additional address line',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/order_address"), "additional_line", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "after" => "postcode",
    "comment" => 'Additional address line',
]);

$installer->endSetup();

