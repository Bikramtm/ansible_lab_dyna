<?php

$installer = $this;

$installer->startSetup();

$installer->getConnection()->addColumn($installer->getTable("sales/quote"), "hardware_name", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 100,
    "after" => "fixed_delivery_type",
    "comment" => 'hardware name from peal validate basket call',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/quote"), "serial_number", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 45,
    "after" => "hardware_name",
    "comment" => 'serial number from peal validate basket call',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/quote_item"), "hardware_name", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 100,
    "after" => "has_non_standard_rate",
    "comment" => 'hardware name from peal validate basket call',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/quote_item"), "serial_number", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 45,
    "after" => "hardware_name",
    "comment" => 'serial number from peal validate basket call',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/order"), "hardware_name", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 100,
    "after" => "fixed_delivery_type",
    "comment" => 'hardware name from peal validate basket call',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/order"), "serial_number", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 45,
    "after" => "hardware_name",
    "comment" => 'serial number from peal validate basket call',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/order_item"), "hardware_name", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 100,
    "after" => "offer_type",
    "comment" => 'hardware name from peal validate basket call',
]);

$installer->getConnection()->addColumn($installer->getTable("sales/order_item"), "serial_number", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 45,
    "after" => "hardware_name",
    "comment" => 'serial number from peal validate basket call',
]);

$installer->endSetup();
