<?php
/**
 * @category    Dyna
 * @package     Vznl_Checkout
 */

/**
 * Discount
 */

class Vznl_Checkout_Block_Tax_Discount extends Mage_Tax_Block_Checkout_Discount
{
    public function setMafTotal($total)
    {
        $this->setData('maf_total', $total);
        if ($total->getAddress()) {
            $this->_store = $total->getAddress()->getQuote()->getStore();
        }
        return $this;
    }
}
