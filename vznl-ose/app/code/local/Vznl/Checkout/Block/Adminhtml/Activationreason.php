<?php

class Vznl_Checkout_Block_Adminhtml_Activationreason extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_activationreason';
        $this->_blockGroup = 'vznl_checkout';
        $this->_headerText = Mage::helper('vznl_checkout')->__('Manage manual activation reasons');
        $this->_addButtonLabel = Mage::helper('vznl_checkout')->__('Add new manual activation reason');
        parent::__construct();
    }
}
