<?php
class Vznl_Checkout_Block_Html_Head extends Mage_Page_Block_Html_Head
{
    /**
     * Add JavaScript file to HEAD entity
     *
     * @param string $name
     * @param string $params
     * @return Mage_Page_Block_Html_Head
     */
    public function addJs($name, $params = "")
    {
        //only add checksum if js merge
        if(!Mage::getStoreConfigFlag('dev/js/merge_files')){
            $filename = Mage::getBaseDir() . DS . 'js' . DS . $name;
            if(is_file($filename) && strpos($name, 'vodafone') !== false){
                $checksum = sha1_file($filename);
                $name .= (strpos($name, '?') === false ? '?' : '&') . 'cks=' . $checksum;
            }
        }
        return parent::addJs($name, $params);
    }
}
