<?php

/**
 * Class Vznl_Checkout_Block_Pdf_Offer_Package
 * @method Vznl_Checkout_Model_Sales_Quote getQuote()
 * @method Vznl_Package_Model_Package getPackage()
 */
class Vznl_Checkout_Block_Pdf_Offer_Package extends Mage_Page_Block_Html{

    /** @var Omnius_Customer_Model_Customer_Customer */
    private $customer = null;

    /** @var Vznl_Checkout_Model_Sales_Quote_Item  */
    private $subscription = null;

    /** @var Vznl_Checkout_Model_Sales_Quote_Item  */
    private $promotion = null;

    /** @var Vznl_Checkout_Model_Sales_Quote_Item  */
    private $_deviceRC = null;

    /**
     * Returns items belonging to the loaded package.
     * @return array
     */
    public function getItems()
    {
        $returnItems = array();
        foreach ($this->getQuote()->getAllItems() as $item) {
            if ($item->getPackageId() == $this->getPackage()->getPackageId()) {
                if (Mage::helper('dyna_catalog')->is(array(Vznl_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION), $item->getProduct())) {
                    $index = 0;
                } else {
                    if (Mage::helper('dyna_catalog')->is(array(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE), $item->getProduct())) {
                        $index = 1;
                    } else {
                        $index = rand(2, 100);
                    }
                }
                $returnItems[$index] = $item;
            }
        }

        ksort($returnItems);
        return $returnItems;
    }

    /**
     *
     * @return Omnius_Customer_Model_Customer_Customer|Mage_Core_Model_Abstract
     */
    public function getCustomer()
    {
        if ($this->customer == null) {
            $this->customer = Mage::getModel('customer/customer')->load($this->getQuote()->getCustomerId());
        }

        return $this->customer;
    }

    /**
     * @return null
     */
    public function getCtn()
    {
        return $this->getCustomer()->getCustomerCtn($this->getPackage()->getCtn());
    }

    /**
     * @param DateTime|string $date
     * @return null|string
     */
    public function stripTimeFromDate($date)
    {
        if ( ! $date) return null;

        if(is_object($date)){
            return $date->format('d/m/Y');
        }

        if(is_string($date)){
            $date = new DateTime($date);
            return $date->format('d/m/Y');
        }

        return null;
    }

    /**
     * @return null|string
     */
    public function getCtnEndDate()
    {
        return $this->stripTimeFromDate($this->getCtn()->getEndDate());
    }

    /**
     * @return null|string
     */
    public function getEffectiveDate()
    {
        return $this->stripTimeFromDate($this->getQuote()->getCreatedAt());
    }

    /**
     * @return int|string
     */
    public function getContractDuration()
    {
        $duration = '';
        /** @var Vznl_Checkout_Model_Sales_Order_Item $item */
        foreach ($this->getItems() as $item) {
            if (
                Mage::helper('dyna_catalog')->is(
                    array(Vznl_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION),
                    $item->getProduct()
                )
            ) {
                $duration = $item->getProduct()->getSubscriptionDuration();
            }
        }

        return $duration;
    }

    /**
     * @return bool
     */
    public function mafHasPromotion()
    {
        /** @var Vznl_Checkout_Model_Sales_Order_Item $item */
        foreach ($this->getItems() as $item) {
            if (
                (
                    $item->getProduct()->is(Vznl_Catalog_Model_Type::SUBTYPE_ADDON)
                    || $item->getProduct()->is(Vznl_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION)
                )
                && $item->getMafDiscountAmount() > 0
            ) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return Vznl_Checkout_Model_Sales_Quote_Item
     */
    public function getSubscription()
    {
        if ($this->subscription == null) {
            foreach ($this->getItems() as $item) {
                if (Mage::helper('dyna_catalog')->is(array(Vznl_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION), $item->getProduct())) {
                    $this->subscription = $item;
                }
            }
        }
        return $this->subscription;
    }

    /**
     * @return Vznl_Checkout_Model_Sales_Quote_Item
     */
    public function getPromotionForSubscription()
    {
        if ($this->promotion == null) {
            /** @var Vznl_Checkout_Model_Sales_Quote_Item $item */
            foreach ($this->getItems() as $item) {
                if ($item->isPromo()
                    && $this->getSubscription()->getProductId() == $item->getTargetId()
                    && $this->getSubscription()->getMafDiscountAmount()
                ) {
                    $this->promotion = $item;
                }
            }
        }
        return $this->promotion;
    }

    public function hasSubscription()
    {
        if ($this->getSubscription()) {
            return true;
        }
        return false;
    }

    public function hasDeviceRCNonZero()
    {
        foreach ($this->getItems() as $item) {
            if (
                $item->getProduct()->is(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE_SUBSCRIPTION)
                && $item->getSku() != Mage::helper('vznl_checkout/aikido')->getDefaultAcquisitionSku()
                && $item->getSku() != Mage::helper('vznl_checkout/aikido')->getDefaultRetentionSku()
            ) {
                $this->_deviceRC = $item;
                return true;
            }
        }
        return false;
    }

    public function getDeviceRC()
    {
        if($this->_deviceRC == null) {
            foreach ($this->getItems() as $item) {
                if (
                    $item->getProduct()->is(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE_SUBSCRIPTION)
                    && $item->getSku() != Mage::helper('vznl_checkout/aikido')->getDefaultAcquisitionSku()
                    && $item->getSku() != Mage::helper('vznl_checkout/aikido')->getDefaultRetentionSku()
                ) {
                    $this->_deviceRC = $item;
                }
            }
        }
        return $this->_deviceRC;
    }
}
