<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Dyna
 * @package     Vznl_Checkout
 */

/**
 * One page checkout status
 *
 * @category   Dyna
 * @package    Vznl_Checkout
 */
class Vznl_Checkout_Block_Pdf_Warranty extends Mage_Page_Block_Html
{
    /** @var Vznl_Checkout_Model_Sales_Order  */
    protected $_order = null;
    /** @var Vznl_Checkout_Model_Sales_Order_Item|array*/
    protected $_packageItems = array();
    /** @var array */
    protected $_packageId = array();
    /** @var Vznl_Package_Model_Package null  */
    protected $_package = null;

    protected $_allWarranties = null;
    /** @var Vznl_Checkout_Model_Sales_Order_Item|null */
    protected $_warranty = null;
    /** @var Vznl_Checkout_Model_Sales_Order_Item|null */
    protected $_device = null;

    /**
     * @param Vznl_Checkout_Model_Sales_Order $order
     * @param Vznl_Checkout_Model_Sales_Order_Item[] $package
     * @param int $packageId
     *
     * @return Vznl_Checkout_Block_Pdf_Warranty
     */
    public function init($order, $package, $packageId)
    {
        $this->_order = $order;
        $this->_packageItems = $package;
        $this->_packageId = $packageId;

        $this->_initWarrantyProducts();

        return $this;
    }

    protected function _initWarrantyProducts()
    {
        $this->_allWarranties = array();

        foreach ($this->_packageItems as $item) {
            if ($item->getProduct()
                && (strtolower($item->getProduct()->getData(Dyna_Catalog_Model_Product::ADDON_TYPE_ATTR)) == 'garant'
                || strtolower($item->getProduct()->getAttributeText(Dyna_Catalog_Model_Product::ADDON_TYPE_ATTR))=='garant'))
            {
                $this->_allWarranties[$item->getId()] = $item;
            }
        }

        return $this;
    }

    public function getCustomer()
    {
        if (!$this->_order->getCustomer()) {
            if ($this->_order->getCustomerId()) {
                $customer = Mage::getModel('customer/customer')->load($this->_order->getCustomerId());
                $this->_order->setCustomer($customer);
            }
        }

        return $this->_order->getCustomer();
    }

    public function getCustomerIsBusiness()
    {
        return $this->getCustomer()->getIsBusiness();
    }

    public function getName()
    {
        return $this->getCustomerIsBusiness()
            ?  $this->_order->getCompanyName()
            :  join(' ',
                array_filter(array($this->__($this->_order->getCustomerPrefix()),
                    $this->_order->getCustomerFirstname(),
                    $this->_order->getCustomerMiddlename(),
                    $this->_order->getCustomerLastname())));
    }

    public function getAddress()
    {
        if ($this->getCustomerIsBusiness()) {
            return $this->_order->getCompanyStreet()
                . ' '
                . $this->_order->getCompanyHouseNr()
                . ', '
                . $this->_order->getCompanyPostcode()
                . ' '
                . $this->_order->getCompanyCity();
        }

        $fullAddress = $this->_order->getBillingAddress();
        if ($fullAddress) {
            return $fullAddress->getStreetFull()
                . ', '
                . $fullAddress->getPostcode()
                . ' '
                . $fullAddress->getCity();
        } else {
            return 'no address';
        }
    }

    public function getBirthDate()
    {
        return $this->getCustomerIsBusiness()
            ? $this->stripTimeFromDate($this->_order->getContractantDob())
            : $this->stripTimeFromDate($this->_order->getCustomerDob());
    }

    public function getIncrementId()
    {
        return $this->_order->getIncrementId();
    }

    public function getOrderDate()
    {
        return $this->stripTimeFromDate($this->_order->getCreatedAt());
    }


    public function getWarrantyProducts()
    {
        return $this->_allWarranties;
    }

    /**
     * @return Vznl_Checkout_Model_Sales_Order_Item|null
     */
    public function getWarrantyProduct()
    {
        return $this->_warranty;
    }

    /**
     * @param Vznl_Checkout_Model_Sales_Order_Item $item
     * @return $this
     */
    public function setWarrantyProduct($item)
    {
        $this->_warranty = $item;
        return $this;
    }

    public function getWarrantyPrice()
    {
        if (!$this->_warranty) {
            return false;
        }

        return $this->getCustomerIsBusiness()
            ? $this->_warranty->getItemFinalMafExclTax()
            : $this->_warranty->getItemFinalMafInclTax();
    }

    public function getDeviceProduct()
    {
        if (!$this->_device) {
            foreach ($this->_packageItems as $item) {
                if ($item->getProduct() && Mage::helper('dyna_catalog')->is(
                        array(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE),
                        $item->getProduct()
                    ))
                {
                    $this->_device = $item;
                    break;
                }
            }
        }

        return $this->_device;
    }

    public function getPackageModel()
    {
        if (!$this->_package) {
            //TODO also read items with the packages
            $this->_package = Mage::getModel('package/package')
                ->getPackages($this->_order->getSuperorderId(), null, $this->_packageId)
                ->getFirstItem();
        }

        return $this->_package;
    }

    public function getEffectiveDate()
    {
        $date = '';

        $package = $this->getPackageModel();
        if (!$package) {
            return $date;
        }

        if ($package->getCtn()) {
            // retention
            $date = ($package->getCtnOriginalEndDate()
                && strtotime($package->getCtnOriginalEndDate()->format('d-m-Y')) > time())
                ? $package->getCtnOriginalEndDate()->format('d-m-Y')
                : $this->getSigningDate();
        } elseif ($package->getCurrentNumber()) {
            // number porting
            $date = $this->stripTimeFromDate($package->getContractEndDate());
        } elseif ($package->getTelNumber()) {
            // new number
            $date = $this->getSigningDate();
        }

        return $date;
    }

    public function getSigningDate()
    {
        if (!$this->_order->getContractSignDate()) {
            return $this->stripTimeFromDate(date("Y-m-d"));
        }

        return $this->stripTimeFromDate($this->_order->getContractSignDate());
    }

    public function stripTimeFromDate($date)
    {
        if (!$date) return null;
        $date = new DateTime($date);
        return $date->format('d-m-Y');
    }
}
