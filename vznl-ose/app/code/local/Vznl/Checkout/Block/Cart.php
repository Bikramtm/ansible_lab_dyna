<?php

class Vznl_Checkout_Block_Cart extends Mage_Checkout_Block_Cart
{
    protected $_customerSession = null;
    protected $_checkoutHelper = null;
    protected $_packageHelper = null;
    protected $_catalogHelper = null;
    protected $_cartPackages = null;
    protected $_packages = null;
    private $_changedQuotes = null;
    protected $isOnlyRetention;

    /**
     * Get instance of the Dyna_Customer_Model_Session
     *
     * @return Dyna_Customer_Model_Session
     */
    public function getCustomerSession()
    {
        if (!$this->_customerSession) {
            $this->_customerSession = Mage::getSingleton('customer/session');
        }

        return $this->_customerSession;
    }

    /**
     * Get instance of the Vznl_Checkout_Helper_Data
     *
     * @return Vznl_Checkout_Helper_Data
     */
    public function getCheckoutHelper()
    {
        if (!$this->_checkoutHelper) {
            $this->_checkoutHelper = Mage::helper('vznl_checkout');
        }

        return $this->_checkoutHelper;
    }

    /**
     * Get instance of the Dyna_Package_Helper_Data
     *
     * @return Dyna_Package_Helper_Data
     */
    public function getPackageHelper()
    {
        if (!$this->_packageHelper) {
            $this->_packageHelper = Mage::helper('dyna_package');
        }

        return $this->_packageHelper;
    }

    /**
     * Get instance of the Dyna_Catalog_Helper_Data
     *
     * @return Dyna_Catalog_Helper_Data
     */
    public function getCatalogHelper()
    {
        if (!$this->_catalogHelper) {
            $this->_catalogHelper = Mage::helper('dyna_catalog');
        }

        return $this->_catalogHelper;
    }

    /**
     * Return if ILT is required
     * @return bool
     */
    public function isIltRequired() : bool
    {
        foreach ($this->getPackages() as $package) {
            foreach ($package['items'] as $quoteItem) {
                if ($quoteItem->getProduct()->isIltRequired()) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Return if loan is required
     * @return bool
     */
    public function isLoanRequired() : bool
    {
        foreach ($this->getPackages() as $package) {
            foreach ($package['items'] as $quoteItem) {
                if ($quoteItem->getProduct()->isLoanRequired()) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Retrieve active quote
     *
     * @return Vznl_Checkout_Model_Sales_Quote
     */
    public function getQuote()
    {
        if (!isset($this->_quote)) {
            $this->_quote = Mage::registry('quote');

            if (!$this->_quote) {
                // quote not found or empty in registry
                if ($this->getCustomerSession()->getOrderEdit()) {
                    $this->_quote = Mage::getSingleton('checkout/cart')->getQuote(true);
                } else {
                    $this->_quote = Mage::getSingleton('checkout/cart')->getQuote();
                }

                Mage::register('quote', $this->_quote);
            }
        }

        return $this->_quote;
    }

    /**
     * Get quote packages with all their items
     *
     * @return array|null
     */
    public function getPackages()
    {
        if ($this->_packages == null) {
            $this->_packages = Mage::registry('packages');

            if (!$this->_packages) {
                $this->_packages = $this->preparePackages();

                // Ensure it is not already saved but empty
                Mage::unregister('packages');
                Mage::register('packages', $this->_packages);
            }
        }
        return $this->_packages;
    }

    /**
     * URL to the media directory
     *
     * @return string
     */
    public function getMediaUrl()
    {
        return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
    }

    /**
     * Format the quote packages to contain only specific information
     *
     * @return array|null
     */
    public function getCartPackages()
    {
        if ($this->_cartPackages == null) {
            $this->_cartPackages = Mage::registry('cart_packages');

            if (!$this->_cartPackages) {
                $this->_cartPackages = $this->getCheckoutHelper()->getCartPackages($this->getPackages());

                Mage::register('cart_packages', $this->_cartPackages);
            }
        }

        return $this->_cartPackages;
    }

    /**
     * Get current customer
     *
     * @return Dyna_Customer_Model_Session|null
     */
    public function getCustomer()
    {
        if (!isset($this->_customer)) {
            $this->_customer = Mage::registry('customer');

            if ($this->_customer !== false && $this->_customer === null) {
                // customer not found in registry or empty
                if ($this->getQuote()->getCustomer()->getId()) {
                    $this->_customer = $this->getQuote()->getCustomer();
                } elseif ($this->getCustomerSession()->getProspect()) {
                    $this->_customer = $this->getCustomerSession()->getCustomer();
                } else {
                    $this->_customer = false;
                }

                Mage::register('customer', $this->_customer);
            }
        }

        return $this->_customer;
    }

    /**
     * Verify if the quote has multiple packages
     *
     * @return bool
     */
    public function quoteHasMultiplePackages()
    {
        if (!isset($this->_quoteHasMultiplePackages)) {
            $this->_quoteHasMultiplePackages = Mage::registry('multiple_packages');

            if ($this->_quoteHasMultiplePackages === null) {
                $this->_quoteHasMultiplePackages = (count($this->getCartPackages()) > 1) ? true : false;

                Mage::register('multiple_packages', $this->_quoteHasMultiplePackages);
            }
        }

        return $this->_quoteHasMultiplePackages;
    }

    public function skipToDeliverSteps()
    {
        if (!isset($this->_skipToDeliverSteps)) {
            $this->_skipToDeliverSteps = Mage::registry('skipToDeliverSteps');

            if ($this->_skipToDeliverSteps === null) {
                $superOrderNo = $this->getRequest()->get('orderId');
                $this->_skipToDeliverSteps = ($superOrderNo && ($superOrder = Mage::getModel('superorder/superorder')->getSuperOrderByOrderNumber($superOrderNo)) && ($superOrder->getId()));

                if ($this->_skipToDeliverSteps) {
                    Mage::register('order', $superOrder);
                }

                Mage::register('skipToDeliverSteps', $this->_skipToDeliverSteps);
            }
        }
        return $this->_skipToDeliverSteps;
    }

    public function getAllCancelledPackages()
    {
        if (!isset($this->_allCancelledPackages)) {
            $this->_allCancelledPackages = Mage::registry('allCancelledPackages');

            if ($this->_allCancelledPackages === null) {
                $this->_allCancelledPackages = array_merge($this->getPackageHelper()->getCancelledPackagesNow(), $this->getPackageHelper()->getCancelledPackages());

                Mage::register('allCancelledPackages', $this->_allCancelledPackages);
            }
        }

        return $this->_allCancelledPackages;
    }

    public function getIsOrderEdit()
    {
        return $this->getCustomerSession()->getOrderEdit();
    }

    public function canAgentEdit()
    {
        $agent = Mage::getSingleton('customer/session')->getAgent(true);
        return $agent->isGranted(array('CHANGE_INPUT'));
    }

    protected function getModifiedQuotes()
    {
        if (!isset($this->_changedQuotes)) {
            $this->_changedQuotes = Mage::registry('allChangedPackages');

            if ($this->_changedQuotes === null) {

                $this->_changedQuotes = Mage::helper('dyna_package')->getModifiedQuotes();
                Mage::register('allChangedPackages', $this->_changedQuotes);
            }
        }

        return $this->_changedQuotes;
    }

    private function preparePackages()
    {
        $quote = $this->getQuote();
        $packagesResult = array();

        if (Mage::getSingleton('customer/session')->getOrderEdit()) {
            $editedQuotes = $this->getModifiedQuotes();
            foreach ($editedQuotes as $q) {
                foreach ($q->getPackages() as $packId => $pack) {
                    $packagesResult[$packId] = $pack;
                }
            }

            foreach ($quote->getPackages() as $packageId => $package) {
                if (!isset($packagesResult[$packageId])) {
                    $packagesResult[$packageId] = $package;
                }
            }
        } else {
            $packagesResult = $quote->getPackages();
        }

        $this->_packages = $packagesResult;

        return $this->_packages;
    }

    // Check if the quote has a business or a personal contract
    public function checkShouldShowRambours()
    {
        if ($this->getCustomerSession()->getOrderEdit()) {
            $quote = Mage::getModel('sales/quote')->load($this->getCustomerSession()->getOrderEdit());
            // Order edit
            $packageDatas = array();
            $allPackages = $this->getPackages();

            foreach ($allPackages as $packageId => $package) {
                $packageModel = Mage::getModel('package/package')->getPackages($quote->getSuperOrderEditId(), null, $packageId)->getFirstItem();
                $packageDatas[] = array($packageModel, $package['items']);
            }

            $editTotals = $this->getCheckoutHelper()->getEditTotals($packageDatas);
            $grandTotal = $editTotals['total_price'];
        } else {
            // Not order edit
            $quote = $this->getQuote();

            // Check if amount is within the specified margin
            $grandTotal = $quote->getGrandTotal();
        }

        if ($this->getCheckoutHelper()->checkIsBusinessCustomer($quote)) {
            $validateAmount = $grandTotal < Vznl_Checkout_Model_Sales_Quote_Item::BUSINESS_MAX_AMMOUNT;
        } else {
            $validateAmount = $grandTotal < Vznl_Checkout_Model_Sales_Quote_Item::CONSUMER_MAX_AMMOUNT;
        }

        return $validateAmount;
    }

    public function checkShowProductRambours()
    {
        $noAccessories = $noPrepaid = $noHardwareOnly = true;
        $flippedCancelledPackages = array_flip($this->getAllCancelledPackages());
        foreach ($this->getPackages() as $package) {
            // Skip cancelled packages
            $packageId = $package['package_id'];
            if (isset($flippedCancelledPackages[$packageId])) {
                continue;
            }

            // Skip Mobile packages according to DF-005101
            if ($package['type'] == Omnius_Configurator_Model_PackageType::PACKAGE_TYPE_MOBILE) {
                continue;
            }

            foreach ($package['items'] as $item) {
                // Check if there are accessories
                $flippedProductType = array_flip($item->getProduct()->getType());
                if (isset($flippedProductType[Vznl_Catalog_Model_Type::SUBTYPE_ACCESSORY])) {
                    $noAccessories = false;
                }

                // Check if the package is prepaid
                if ($item->getPackageType() == Omnius_Configurator_Model_PackageType::PACKAGE_TYPE_PREPAID) {
                    $noPrepaid = false;
                }

                // Check if the package is hardware
                if ($item->getPackageType() == Omnius_Configurator_Model_PackageType::PACKAGE_TYPE_HARDWARE) {
                    $noHardwareOnly = false;
                }
            }
        }

        return $noAccessories && $noHardwareOnly && $noPrepaid;
    }

    /**
     * Checks if the order is delivered by the current store.
     * @return bool <true> if yes, <false> if not.
     */
    public function isDeliveredByThisStore()
    {
        $quote = $this->getQuote();
        $shippingData = $quote->getData('shipping_data');
        $shippingData = Mage::helper('core')->jsonDecode($shippingData);

        $deliverStoreId = null;
        if (isset($shippingData['deliver']['address']['store_id'])) {
            $deliverStoreId = $shippingData['deliver']['address']['store_id'];
        }
        $agentStoreId = $this->getCustomerSession()->getAgent(true)->getDealer()->getId();

        if ($deliverStoreId == $agentStoreId) {
            return true;
        }

        return false;
    }

    /**
     * Gets whether the superorder has status fulfilled/fulfillment in progress or is closed and such the contract can be
     * viewed by all shops.
     * @return bool <true> means has one of the statuses, <false> means not.
     */
    public function isContractAvailableForEveryone()
    {
        $superOrder = $this->getCurrentSuperOrder();

        /** @var Vznl_Superorder_Model_Superorder $superOrder */
        switch ($superOrder->getOrderStatus()) {
            case Vznl_Superorder_Model_Superorder::SO_FULFILLED:
            case Vznl_Superorder_Model_Superorder::SO_FULFILLMENT_IN_PROGRESS:
            case Vznl_Superorder_Model_Superorder::SO_CLOSED:
                return true;
            default:
                return false;
        }
    }

    /**
     * Gets the current super order.
     * @return Vznl_Superorder_Model_Superorder The super order.
     */
    private function getCurrentSuperOrder()
    {
        if ($this->skipToDeliverSteps()) {
            $superOrder = Mage::registry('order');
        } else {
            $superOrder = Mage::getModel('superorder/superorder')->load($this->getQuote()->getSuperOrderEditId());
        }
        return $superOrder;
    }

    /**
     * @return string
     * @todo Reduce complexity
     */
    public function getShippingMethod()
    {
        $deliveryMethod = '';
        $shippingData = Mage::helper('core')->jsonDecode($this->getQuote()->getDefaultValueForField('shipping_data'));

        if (!$shippingData) {
            return (Mage::helper('agent')->isTelesalesLine()) ? 'deliver' : 'direct';
        }

        if (isset($shippingData['pakket'])) {
            $deliveryMethod = 'split';
        } else {
            $address = $shippingData['deliver']['address']['address'];

            if ($address == 'billing_address' || $address == 'other_address') {
                $deliveryMethod = 'deliver';
            } elseif ($address == 'store') {
                if (
                    (!Mage::helper('agent')->isTelesalesLine())
                    && ($shippingData['deliver']['address']['store_id'] == $this->getCustomerSession()->getAgent(true)->getDealer()->getId())
                ) {
                    $deliveryMethod = 'direct';
                } else {
                    $deliveryMethod = 'pickup';
                }
            }
        }

        return $deliveryMethod;
    }

    /**
     * Gets the checkout sections that are available
     * @return array The checkout sections
     */
    public function getCheckoutSections()
    {
        /** @var Vznl_Agent_Helper_Data $agentHelper */
        $agentHelper = Mage::helper('vznl_agent');
        $dealerData = $agentHelper->getDealerData();
        $orderEdit = $this->getCustomerSession()->getOrderEdit();
        $skipToDeliverSteps = $this->skipToDeliverSteps();
        $isFixed = $this->getQuote()->hasFixed();
        $isMobile = $this->getQuote()->hasMobilePackage();
        $checkoutSections = [];
        $customerSession = $this->getCustomerSession();
        $delivery = $this->getRequest()->getParam('delivery');
        $this->getCartPackages();
        if ($orderEdit)
        {
            if ($agentHelper->isAssistedChannel()) {
                if ($this->hasVodafone() && !$skipToDeliverSteps) {
                    $checkoutSections[] = ['name' => 'save_credit_check'];
                }

                if ($this->getRequest()->getParam('delivery')) {
                    $checkoutSections = [];
                    $skipToDeliverSteps = true;
                }

                if ($this->shouldContainNumberSelectionSection($delivery)) {
                    $checkoutSections[] = [
                        'name' => 'save_number_selection',
                        'data' => [
                            'show_step' => $skipToDeliverSteps || $orderEdit
                        ]
                    ];
                }

                $checkoutSections[] = [
                    'name' => 'save_contract',
                    'data' => [
                        'is_order_overview' => true,
                        'show_step' => ($skipToDeliverSteps || $orderEdit),
                        'hide_step' => $this->shouldContractSectionBeHidden()
                    ]
                ];
            }
            $checkoutSections[] = ['name' => 'order_confirmation'];
        }
        else if ($isFixed && $isMobile && !$this->getRequest()->getParam('delivery')) {
            if($this->shouldContainNumberPortingConvergedSection()) {
                $checkoutSections[] = ['name' => 'save_number_porting'];
            }
            $checkoutSections[] = ['name' => 'save_customer'];
            if ($this->shouldContainIltSection()) {
                $checkoutSections[] = ['name' => 'save_income_test'];
            }
            $checkoutSections[] = ['name' => 'save_delivery_address'];
            $checkoutSections[] = ['name' => 'save_payment'];
            $checkoutSections[] = ['name' => 'save_overview'];

            if ($agentHelper->isAssistedChannel()) {
                if ($this->hasVodafone() && !$skipToDeliverSteps) {
                    $checkoutSections[] = ['name' => 'save_credit_check'];
                }
                if ($this->shouldContainNumberSelectionSection()) {
                    $checkoutSections[] = [
                        'name' => 'save_number_selection',
                        'data' => [
                            'show_step' => ($isFixed && $isMobile) || $skipToDeliverSteps,
                        ]
                    ];
                }
                $checkoutSections[] = ['name' => 'save_contract'];
            }
            $checkoutSections[] = ['name' => 'order_confirmation'];
        }
        else if ($isMobile && !$this->getRequest()->getParam('delivery')) {
            if ($this->shouldContainNumberPortingSection() && !$skipToDeliverSteps) {
                $checkoutSections[] = ['name' => 'save_number_porting'];
            }
            $checkoutSections[] = ['name' => 'save_customer'];

            if ($this->shouldContainIltSection()) {
                $checkoutSections[] = ['name' => 'save_income_test'];
            }

            if (!$agentHelper->isIndirectStore()) {
                $checkoutSections[] = ['name' => 'save_delivery_address'];
            }
            $checkoutSections[] = ['name' => 'save_payment'];
            $checkoutSections[] = ['name' => 'save_overview'];

            if ($agentHelper->isAssistedChannel()) {
                if ($this->hasVodafone() && !$skipToDeliverSteps) {
                    $checkoutSections[] = ['name' => 'save_credit_check'];
                }
                if ($this->shouldContainNumberSelectionSection()) {
                    $checkoutSections[] = [
                        'name' => 'save_number_selection',
                        'data' => [
                            'show_step' => $skipToDeliverSteps,
                        ]
                    ];
                }
                $checkoutSections[] = ['name' => 'save_contract'];
            }
            $checkoutSections[] = ['name' => 'order_confirmation'];

        }
        else if ($isFixed && !$this->getRequest()->getParam('delivery')) {
            if($this->shouldContainIlsNumberPorting()) {
                $checkoutSections[] = ['name' => 'save_number_porting'];
            }
            $checkoutSections[] = ['name' => 'save_customer'];
            $checkoutSections[] = ['name' => 'save_delivery_address'];
            $checkoutSections[] = ['name' => 'save_payment'];
            $checkoutSections[] = ['name' => 'save_overview'];

            if ($agentHelper->isAssistedChannel()) {
                $checkoutSections[] = [
                    'name' => 'save_number_selection',
                    'data' => [
                        'show_step' => true,
                    ]
                ];
                $checkoutSections[] = ['name' => 'save_contract'];
            }
            $checkoutSections[] = ['name' => 'order_confirmation'];
        }

//                    ['name' => 'show_fixed_service_address'],
//                    ['name' => 'save_fixed_number_selection'],
//                    ['name' => 'save_customer'],
//                    ['name' => 'save_fixed_delivery_address'],
//                    ['name' => 'save_fixed_payment'],
//                );
//            }
//
//            if ($agentHelper->isAssistedChannel()) {
//                $checkoutSections = array(
//                    ['name' => 'show_fixed_service_address'],
//                    ['name' => 'save_customer'],
//                    ['name' => 'save_fixed_delivery_address'],
//                    ['name' => 'save_fixed_payment'],
//                    ['name' => 'save_fixed_number_selection'],
//                );
//            }
//
//            if ($agentHelper->isTelesalesChannel() &&
//                ($dealerData->getData('call_center_activity') == 'multiskill' || $dealerData->getData('call_center_activity') == 'outbound') &&
//                !$customerSession->getCustomer()->getIsSoho()
//            ) {
//                $checkoutSections[] = ['name' => 'save_written_consent'];
//            }
//
//            if (!$skipToDeliverSteps) {
//                if ($orderEdit) {
//                    $checkoutSections[] = ['name' => 'your_order'];
//                } else {
//                    $checkoutSections[] = ['name' => 'save_overview'];
//                }
//            }
//
//            if ($agentHelper->isAssistedChannel()) {
//                $checkoutSections[] = ['name' => 'save_contract'];
//            }
//
//            $checkoutSections[] = ['name' => 'order_confirmation'];
//            return $checkoutSections;
//        }
//
//        if ($this->shouldContainNumberPortingSection() && !$skipToDeliverSteps) {
//            $checkoutSections[] = ['name' => 'save_number_porting'];
//        }
//
//        $checkoutSections[] = ['name' => 'save_customer'];
//
//        if ($this->shouldContainIltSection()) {
//            $checkoutSections[] = ['name' => 'save_income_test'];
//        }
//
//        if (!$agentHelper->isIndirectStore()) {
//            $checkoutSections[] = ['name' => 'save_delivery_address'];
//        }
//
//        if (!$skipToDeliverSteps) {
//            if ($orderEdit) {
//                $checkoutSections[] = ['name' => 'your_order'];
//            } else {
//                $checkoutSections[] = ['name' => 'save_overview'];
//            }
//        }
//
//        if ($agentHelper->isAssistedChannel()) {
//            if ($this->hasVodafone() && !$skipToDeliverSteps) {
//                $checkoutSections[] = ['name' => 'save_credit_check'];
//            }
//
//            if ($this->shouldContainNumberSelectionSection()) {
//                $checkoutSections[] = [
//                    'name' => 'save_number_selection',
//                    'data' => [
//                        'show_step' => $skipToDeliverSteps
//                    ]
//                ];
//            }
//
//            if (($agentHelper->isIndirectStore() && $this->shouldContainDeviceSelectionSection()) ||
//                ($orderEdit && $agentHelper->isIndirectStore() && $this->shouldContainDeviceSelectionSection())) {
//                $checkoutSections[] = [
//                    'name' => 'save_device_selection',
//                    'data' => [
//                        'show_step' => $skipToDeliverSteps
//                    ]
//                ];
//            }
//
//            $checkoutSections[] = [
//                'name' => 'save_contract',
//                'data' => [
//                    'is_order_overview' => true,
//                    'show_step' => ($skipToDeliverSteps || $orderEdit),
//                    'hide_step' => $this->shouldContractSectionBeHidden()
//                ]
//            ];
//        }
//
//        $checkoutSections[] = ['name' => 'order_confirmation'];

        return $checkoutSections;
    }

    /**
     * Determines whether the number porting section is required.
     * @return bool <true> if the np section is required, <false> if not.
     */
    public function shouldContainNumberPortingSection()
    {
        $catalogHelper = $this->getCatalogHelper();
        $hasVodafone = $this->hasVodafone();
        $hasSubscriptionNoDataSubscription = false;
        $items = $this->getAllVisibleItems();
        $isOnlyRetention = $hasVodafone;
        $quote = $this->getQuote();

        foreach ($items as $item) {
            if ($catalogHelper->checkProductIsNetwork($item)) {
                // Check if there is only packages with CTNs, in which case we dont want to show numberporting and selection
                $isOnlyRetention = ($isOnlyRetention && ((bool) $quote->getPackageCtns($item->getPackageId())));
            }
            /** @var Dyna_Catalog_Model_Product $product */
            $product = $item->getProduct();
            if ($product && $catalogHelper->is(array(Vznl_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION), $product)) {
                if (!$product->isDataSubscription()) {
                    $hasSubscriptionNoDataSubscription = true;
                }
            }
        }
        return $hasVodafone && $hasSubscriptionNoDataSubscription && !$isOnlyRetention;
    }

    /**
     * Determines whether the number porting section is required in converged.
     * @return bool <true> if the np section is required, <false> if not.
     */
    public function shouldContainNumberPortingConvergedSection()
    {
        $catalogHelper = $this->getCatalogHelper();
        $hasVodafone = $this->hasVodafone();
        $items = $this->getAllVisibleItems();
        $isOnlyRetention = $hasVodafone;
        $quote = $this->getQuote();

        foreach ($items as $item) {
            if ($catalogHelper->checkProductIsNetwork($item)) {
                // Check if there is only packages with CTNs, in which case we dont want to show numberporting and selection
                $isOnlyRetention = ($isOnlyRetention && ((bool) $quote->getPackageCtns($item->getPackageId())));
            }
        }
        $fixedNumberPortingSection = $this->shouldContainIlsNumberPorting();
        if ($isOnlyRetention && !$fixedNumberPortingSection) {
            return false;
        } else {
            return true;
        }
    }
    /**
     * Determines whether the ilt section is required.
     * @return bool <true> if the ilt section is required, <false> if not.
     */
    protected function showConvergedCustomerSection()
    {
        $agentHelper = Mage::helper('vznl_agent');
        $isFixed    = $this->getQuote()->hasFixed();
        $isMobile   = $this->getQuote()->hasMobilePackage();
        $isPrepaid  = $this->getQuote()->hasPrepaid();
        $isHardware  = $this->getQuote()->hasHardware();
        if (!$this->shouldContainNumberPortingSection() && !$isFixed && $agentHelper->isAssistedChannel()) {
            return true;
        } elseif (!$this->shouldContainNumberPortingSection() && !$isFixed && $agentHelper->isTelesalesChannel()) {
            return true;
        } elseif ($isFixed && !$isMobile && !$this->shouldContainIlsNumberPorting()) {
            return true;
        } elseif ($agentHelper->isTelesalesChannel() && $isFixed && !$isMobile) {
            return false;
        } elseif ($agentHelper->isAssistedChannel() && $isFixed && ($isPrepaid || $isHardware) && !$this->shouldContainNumberPortingSection())      {
            return true;
        } elseif ($isFixed && $isMobile && !$this->shouldContainNumberPortingConvergedSection()) {
            return true;
        } elseif (!$this->shouldContainNumberPortingSection() && $isFixed && $agentHelper->isTelesalesChannel()) {
            return false;
        } elseif (Mage::getSingleton('customer/session')->getOrderEdit()) {
            return true;
        }
    }
    /**
     * Determines whether the ilt section is required.
     * @return bool <true> if the ilt section is required, <false> if not.
     */
    protected function shouldContainIltSection()
    {
        $customerSession = $this->getCustomerSession();
        $isBusiness = $customerSession->getCustomer()->getIsSoho() ? (bool)$customerSession->getCustomer()->getIsSoho() : false;

        if ($this->getQuote()) {
            $isBusiness = $this->getQuote()->getCustomer()->getIsBusiness();
        }
        return !$isBusiness && $this->isIltRequired();
    }

    /**
     * Determines whether the number selection section is required.
     * @return bool <true> if the number selection section is required, <false> if not.
     */
    protected function shouldContainNumberSelectionSection($delivery = false)
    {
        $catalogHelper = $this->getCatalogHelper();
        $superOrder = $this->getCurrentSuperOrder();
        $hasVodafone = $this->hasVodafone();
        $items = $this->getAllVisibleItems();
        $isOnlyRetention = $hasVodafone;
        $quote = $this->getQuote();
        $hasFixed = $quote->hasFixed();
        $agentHelper = Mage::helper('vznl_agent');
        $isIndirect = $agentHelper->isIndirectStore();

        foreach ($items as $item) {
            if ($catalogHelper->checkProductIsNetwork($item)) {
                // Check if there is only packages with CTNs, in which case we dont want to show numberporting and selection
                $isOnlyRetention = ($isOnlyRetention && ((bool) $quote->getPackageCtns($item->getPackageId())));
            }
        }
        if ($isOnlyRetention && $isIndirect) {
            $this->isOnlyRetention = $isOnlyRetention;
        } else {
            $isOnlyRetention = !$isOnlyRetention;
        }

        $isNewOrPartial = !$this->getCustomerSession()->getOrderEdit() ||
            $superOrder &&
            $superOrder->getId() &&
            in_array($superOrder->getOrderStatus(), [
                Vznl_Superorder_Model_Superorder::SO_VALIDATED_PARTIALLY,
                Vznl_Superorder_Model_Superorder::SO_VALIDATED,
                Vznl_Superorder_Model_Superorder::SO_FULFILLMENT_IN_PROGRESS,
            ]);
        if ($delivery) {
            return ($hasFixed) || ($hasVodafone && $isNewOrPartial);
        } else {
            return ($hasFixed) || ($hasVodafone && $isNewOrPartial && $isOnlyRetention);
        }
    }

    /**
     * Determines whether the device selection section is required.
     * @return bool <true> if the device selection section is required, <false> if not.
     */
    public function shouldContainDeviceSelectionSection()
    {
        $catalogHelper = $this->getCatalogHelper();
        $quote = $this->getQuote();
        $noHardware = Mage::helper('vznl_checkout/aikido')->getIndirectNoHardwareSku();
        $hasVodafone = $this->hasVodafone();
        $showDeviceSelection = false;
        foreach ($quote->getAllVisibleItems() as $item) {
            /** @var Dyna_Catalog_Model_Product $product */
            $product = $item->getProduct();
            if ($product && $catalogHelper->is(array(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE_SUBSCRIPTION), $product) && $product->getSku() != $noHardware) {
                $showDeviceSelection = true;
            }
        }

        return $hasVodafone && $showDeviceSelection;
    }

    /**
     * Checks whether the save contract section should be hidden by default
     * @return bool <true> if should be hidden, <false> if not.
     */
    protected function shouldContractSectionBeHidden()
    {
        /** @var Vznl_Agent_Helper_Data $agentHelper */
        $agentHelper = Mage::helper('vznl_agent');
        $superOrder = $this->getCurrentSuperOrder();
        $showContract = $this->hasVodafone();
        $hasDeletedProducts = false;

        if (isset($superOrder)) {
            $showContract = $showContract && $superOrder->isValidated();
            $hasDeletedProducts = $superOrder->hasDeletedProducts();
        }

        return !($showContract && (!$agentHelper->isRetailStore() || !$hasDeletedProducts));
    }

    /**
     * Checks whether on of the packages has a vodafone da subscription.
     * @return bool <true> if it has, <false> if not.
     */
    public function hasVodafone()
    {
        $catalogHelper = $this->getCatalogHelper();
        $packages = $this->getPackages();
        $cancelledPackages = $this->getAllCancelledPackages();
        $flippedCancelledPackages = array_flip($cancelledPackages);

        foreach ($packages as $packageId => $package) {
            // Skip packages that were cancelled
            if (isset($flippedCancelledPackages[$packageId])) {
                continue;
            }
            if ($catalogHelper->packageContainsVfDaSubscription($package)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Gets all visible items for the current quote/superorder.
     * @return array The array with the visible items.
     */
    protected function getAllVisibleItems()
    {
        $items = [];
        if ($this->skipToDeliverSteps()) {
            $currentPackageId = $this->getRequest()->get('packageId');
            $superOrder = Mage::registry('order');
            $orders = Mage::getModel('sales/order')->getNonEditedOrderItems($superOrder->getId());
            /** @var Vznl_Checkout_Model_Sales_Order $order */
            foreach ($orders as $order) {
                /** @var Vznl_Checkout_Model_Sales_Order_Item $item */
                foreach ($order->getAllItems() as $item) {
                    if ($item->getPackageId() != $currentPackageId) {
                        continue;
                    }
                    $items[] = $item;
                }
            }
        } else {
            $items = $this->getQuote()->getAllVisibleItems();
        }

        return $items;
    }

    /**
     * Checks whether contract checkbox in edit order.
     * @return bool.
     */
    public function getIsOrderView()
    {
        if ($this->getCustomerSession()->getOrderEdit()) {
            return true;
        }
    }

    /**
     * Get subscription name for order overview.
     * @return array.
     */
    public function getSubscriptionName($package) {
        $packageName = '';
        foreach ($package['items'] as &$item) {
            $packageName .= $item->getName() . ', ';
            if($item->getProduct()->isSubscription())
            {
                $subscriptionName = $item->getName();
            }
            if($package['type'] == strtolower(Vznl_Catalog_Model_Type::TYPE_PREPAID)|| $package['type'] == strtolower(Vznl_Catalog_Model_Type::TYPE_HARDWARE)) {
                if($item->getProduct()->isDevice()) {
                    $subscriptionName = $item->getName();
                }
            }
        }
        return $subscriptionName ?? '';
    }

    /**
    * Checks whether fixed package has voice product .
    * @return bool.strtolower(Vznl_Catalog_Model_Type::TYPE_FIXED)
    */
    public function hasFixedVoiceProduct() {
        $cart = Mage::getModel('checkout/cart')->getQuote();
        $firstLoop = true;
        foreach ($cart->getAllItems(true) as $item) {
            if ($item['product']->getData('stack') == ucfirst(Vznl_Catalog_Model_Type::TYPE_FIXED_PACKAGE) && $item['product']->getAttributeText('package_subtype') == Vznl_Catalog_Model_Type::SUBTYPE_FIXED_PACKAGE && $item['product']->getAttributeText('voice_product') && $firstLoop) {
                $firstLoop = false;
                return true;
            } else if ($item['product']->getData('stack') == ucfirst(Vznl_Catalog_Model_Type::TYPE_FIXED_PACKAGE)) {
                return false;
            }
        }
    }

    /**
     * Checks whether fixed package has voice product secondline .
     * @return bool.
     */
    public function hasSecondLineVoiceProduct() {
        $cart = Mage::getModel('checkout/cart')->getQuote();
        $firstLoop = true;
        foreach ($cart->getAllItems(true) as $item) {
            if ($item['product']->getData('stack') == ucfirst(Vznl_Catalog_Model_Type::TYPE_FIXED_PACKAGE) && $item['product']->getAttributeText('package_subtype') == Vznl_Catalog_Model_Type::SUBTYPE_FIXED_PACKAGE && $item['product']->getAttributeText('voice_product') == Vznl_Checkout_Model_Sales_Quote_Item::FIXED_VOICE_PRODUCT_SECONDLINE && $firstLoop) {
                $firstLoop = false;
                return true;
            } else if ($item['product']->getData('stack') == ucfirst(Vznl_Catalog_Model_Type::TYPE_FIXED_PACKAGE)) {
                return false;
            }
        }
    }

    public function shouldContainIlsNumberPorting() {
        $installProductHasVoice = false;
        $newProductHasVoice = false;
        $newProductHasInternet = false;
        $showIlsNumberPorting = false;
        $showNumberPorting = false;
        $newVoiceProduct = false;
        $cart = Mage::getModel('checkout/cart')->getQuote();
        $contextHelper = Mage::helper('dyna_catalog/processContext');
        $processContext = $contextHelper->getProcessContextCode();
        $ilsNumberEnable =($processContext == Vznl_Catalog_Model_ProcessContext::ILSBLU);
        $moveNumberEnable =($processContext == Vznl_Catalog_Model_ProcessContext::MOVE);
        foreach ($cart->getAllItems() as $item) {
            $ProductInInstalled = "";
            $addedProducts = "";
            $installedBaseProducts = $item->getData('preselect_product');
            if(!$installedBaseProducts) {
                $addedProducts = $item->getProduct();
            }
            if($addedProducts) {
                $newVoice = $addedProducts->getAttributeText('voice_product');
                if ($newVoice) {
                    $newVoiceProduct = true;
                    if ($newVoice == Vznl_Configurator_Model_PackageType::TELEPHONY_FIRST_LINE) {
                        $newProductHasVoice = true;
                    }
                }
                if ($addedProducts->getHasInternet()) {
                    $newProductHasInternet = true;
                }
            }

            if($installedBaseProducts) {
                $ProductInInstalled = $item->getProduct();
            }
            if($ProductInInstalled) {
                if($ProductInInstalled->getAttributeText('voice_product')) {
                    $voiceProduct = $ProductInInstalled->getAttributeText('voice_product');
                    if ($voiceProduct == (Vznl_Configurator_Model_PackageType::TELEPHONY_BOTH_LINE)) {
                        $bothLines = true;
                    }
                    $installProductHasVoice = true;
                }
            }
        }
        $alterProductHasInternet = false;
        $showSwitchProvider = false;
        foreach ($cart->getAlternateItems() as $alternateItem) {
            $alternateItems[] = $alternateItem;
            foreach ($alternateItems as &$alternateItem) {
                $product = $alternateItem->getProduct();
                $cancelledProduct[] = $product->getVoiceProduct()?:false;
                if($product->getHasInternet()){
                    $alterProductHasInternet = true;
                }
            }
        }
        if ($ilsNumberEnable && $bothLines && $newProductHasVoice) {
            $this->getCheckoutHelper()->setTelephoneNumber(false);
            $cart->save();
        }

        if (($ilsNumberEnable && $newProductHasInternet && !$alterProductHasInternet) || (!$moveNumberEnable && !$ilsNumberEnable && $newProductHasInternet)){
            $showSwitchProvider = true;
        }
        if ($ilsNumberEnable && $installProductHasVoice) {
          $showIlsNumberPorting = false;
        } else {
            if ($ilsNumberEnable && $newVoiceProduct) {
                $showIlsNumberPorting = true;
            }
        }
        if (($moveNumberEnable && !$installProductHasVoice && !$newVoiceProduct) || (!$moveNumberEnable && !$ilsNumberEnable && !$newVoiceProduct)) {
            $showNumberPorting = false;
        } else {
            $showNumberPorting = true;
        }
        if (($ilsNumberEnable && !$showSwitchProvider && !$showIlsNumberPorting) || ($moveNumberEnable && !$showSwitchProvider && !$showNumberPorting) || (!$moveNumberEnable && !$ilsNumberEnable && !$newVoiceProduct && !$showSwitchProvider)) {
            return false;
        } else {
            return true;
        }
    }
}
