<?php

/**  */
class Vznl_Checkout_Block_Cart_Steps_SaveOverview extends Vznl_Checkout_Block_Cart
{

    public function getIsGarantCartProductsAvailable(){

        //ADDON_TYPE_ATTR = 'identifier_addon_type'
        $eavAttribute = new Mage_Eav_Model_Mysql4_Entity_Attribute();
        $attrId = $eavAttribute->getIdByCode('catalog_product', Omnius_Catalog_Model_Product::ADDON_TYPE_ATTR);
        $_product = Mage::getModel('catalog/product');
        $attr = $_product->getResource()->getAttribute(Omnius_Catalog_Model_Product::ADDON_TYPE_ATTR);
        
        if ($attr && $attr->usesSource()) {
            $valueId = $attr->getSource()->getOptionId("Garant");
        }
        
        $quote = Mage::getModel('checkout/cart')->getQuote();
        
        foreach ($quote->getAllItems() as $item) {
            $product= Mage::getModel('catalog/product')->load($item->getProductId());

            if ($item->getParentItemId()) {
                continue;
            }

            if($valueId == $product->getData( Omnius_Catalog_Model_Product::ADDON_TYPE_ATTR)){
                return true;
            }
         }
        
        return false;
    }
}
