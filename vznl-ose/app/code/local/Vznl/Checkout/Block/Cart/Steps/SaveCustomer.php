<?php

/**  */
class Vznl_Checkout_Block_Cart_Steps_SaveCustomer extends Vznl_Checkout_Block_Cart
{
    public function canShowDummyCheckbox()
    {
        $allowedWebsites = [
            Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE,
            Vznl_Agent_Model_Website::WEBSITE_TELESALES_CODE,
            Vznl_Agent_Model_Website::WEBSITE_BELCOMPANY_CODE,
            Vznl_Agent_Model_Website::WEBSITE_INDIRECT_CODE,
            //only for st2 purpose
            Vznl_Agent_Model_Website::WEBSITE_DEFAULT_CODE

        ];

        return (in_array(Mage::app()->getWebsite()->getCode(), $allowedWebsites));
    }
}
