<?php

/**
 * Class Vznl_Checkout_Helper_Validation
 */
class Vznl_Checkout_Helper_Validation extends Dyna_Checkout_Helper_Validation
{
    const CUSTOMER_DEFAULT_COUNTRY = 'NLD';

    /**
     * @var bool
     */
    protected $isAPI = false;

    /**
     * @var bool
     */
    protected $isBusiness = false;


    /**
     * @var bool
     */
    protected $isMobileCustomer = null;

    /**
     * @var array
     */
    protected $customerAdditionalAttributes = [
        'ILT_ALREADY_PROVIDED' => [
            'type' => 'bool',
            'value' => true
        ],
        'ILT_CHECK_DEFERRED' => [
            'type' => 'bool',
            'value' => true
        ],
        'ILT_FAMILY_TYPE' => [
            'type' => 'string'
        ],
        'ILT_INCOME' => [
            'type' => 'int'
        ],
        'ILT_HOUSING_COSTS' => [
            'type' => 'int'
        ],
        'ILT_BIRTH_MIDDLENAME' => [
            'type' => 'string',
            'pattern' => '~[0-9]+~'
        ],
        'ILT_BIRTH_LASTNAME' => [
            'type' => 'string',
            'pattern' => '~[0-9]+~'
        ],
        'ILT_INITIALS' => [
            'type' => 'string',
            'pattern' => '~[0-9]+~'
        ],
    ];

    /**
     * Validate whole SC API request
     *
     * @param array $request
     * @return bool
     * @throws Mage_Exception
     * @throws Exception
     */
    public function validateShoppingCart(array $request): bool
    {
        $this->isAPI = true;

        if (!empty($request)) {
            // Needs to be taken from request
            $hasSubscription = false;
            $isMobile = false;
            $isBoth = false;
            $skipIdentificationCheck = false;
            $this->isBusiness = $request['business'] ?? false;
            foreach ($request['packages'] ?? [] as $package) {
                if (in_array(
                    $package['processContext'],
                    [Dyna_Catalog_Model_ProcessContext::ILSBLU, Dyna_Catalog_Model_ProcessContext::ILSNOBLU]
                )) {
                    $skipIdentificationCheck = true;
                    break;
                }
            }

            // Let's validate
            $errors = [];
            // Customer
            $customer = $request['customer'][0] ?? null;
            if ($customer) {
                $customerData = $this->mapApiCustomer($customer);
                $errors += $this->APIValidations($request);

                if (!empty($customerData)) {
                    $errors += $this->validateCustomerData($customerData, $hasSubscription, $isMobile, $isBoth);
                    if (!$skipIdentificationCheck) {
                        $errors += $this->validateAPIIdNumberByType($customerData);
                    }

                    // Billing address
                    $billingData = $this->mapApiBillingAddress($request['customer'][0] ?? []);
                    $errors += $this->validateBillingAddressData($billingData, $hasSubscription);
                }
            }

            if (!empty($errors)) {
                throw new Mage_Exception($this->mapErrorsToAPI($errors));
            }
        }

        return true;
    }

    /**
     * @param array $data
     * @param bool $hasSubscription
     * @param bool $isMobile
     * @return array
     * @throws Exception
     */
    public function validateBillingAddressData($data, $hasSubscription = false, $isMobile = false)
    {
        $errors = [];
        $validations = $hasSubscription ? [
            'fax' => [
                'required' => false,
                'validation' => [
                    'validateName',
                ]
            ],
        ] : [];

        switch ($data['address']) {
            case 'other_address':
                $addressValidations = [
                    'street' => [
                        'required' => true,
                        'validation' => [
                        ]
                    ],
                    'houseno' => [
                        'required' => true,
                        'validation' => [
                            'validateNumber'
                        ]
                    ],
                    'addition' => [
                        'required' => false,
                        'validation' => [
                        ]
                    ],
                    'postcode' => [
                        'required' => true,
                        'validation' => $isMobile && $this->isMobileCustomer() ? [] : ['validateNLPostcode'],
                    ],
                    'city' => [
                        'required' => true,
                        'validation' => [
                        ]
                    ],
                ];

                if ($this->isAPI) {
                    $addressValidations['country_id'] = [
                        'required' => true,
                        'validation' => [
                            'validateCountryCode',
                        ]
                    ];
                }

                $errors += Mage::helper('vznl_checkout')->_performValidate(
                    $data['otherAddress'],
                    $addressValidations,
                    'address[otherAddress][', ']'
                );
                break;
            case 'foreign_address':
                $addressValidations = [
                    'street' => [
                        'required' => true,
                        'validation' => [
                        ]
                    ],
                    'houseno' => [
                        'required' => true,
                        'validation' => [
                            'validateNumber'
                        ]
                    ],
                    'addition' => [
                        'required' => false,
                        'validation' => [
                        ]
                    ],
                    'extra' => [
                        'required' => false,
                        'validation' => [
                        ]
                    ],
                    'city' => [
                        'required' => true,
                        'validation' => [
                        ]
                    ],
                    'region' => [
                        'required' => false,
                        'validation' => [
                        ]
                    ],
                    'postcode' => [
                        'required' => true,
                        'validation' => [
                        ]
                    ],
                    'country_id' => [
                        'required' => true,
                        'validation' => [
                            $this->isAPI ? 'validateCountryCode' : null
                        ]
                    ],
                ];
                $errors += Mage::helper('vznl_checkout')->_performValidate(
                    $data['foreignAddress'],
                    $addressValidations,
                    'address[foreignAddress][', ']'
                );

                if ($this->isAPI && !Mage::helper('vznl_validators')->validateForeignPostcode(
                        $data['foreignAddress']['postcode'],
                        $data['foreignAddress']['country_id']
                    )) {
                    $errors['address[foreignAddress][postcode]'] = $this->__('Field has invalid data');
                }
                break;
            default :
                throw new Exception(Mage::helper('vznl_checkout')->__('Missing company address data'));
        }

        $errors += Mage::helper('vznl_checkout')->_performValidate($data, $validations, 'address[', ']');

        return $errors;
    }

    /**
     * @param array $data
     * @return array
     */
    public function validateCustomerAdditionalData($data)
    {
        $errors = [];

        // Validate additional email data
        $emails = array_filter($data['email'] ?: [], 'strlen');
        foreach ($emails as $id => $email) {
            if (!Mage::helper('dyna_validators')->validateEmailSyntax($email)
                || !Mage::helper('dyna_validators')->validateEmailDns($email)) {
                $errors += [
                    'additional[email][' . $id . ']' =>
                        Mage::helper('vznl_checkout')->__('Invalid e-mail address')
                ];
            }
        }
        //TODO Validate additional fax
        //TODO Validate additional phone

        return $errors;
    }

    /**
     * @param array $data
     * @param bool $hasSubscription
     * @param bool $isMobile
     * @param bool $isBoth
     * @return array
     * @throws Exception
     */
    public function validateCustomerData(
        $data,
        $hasSubscription = false,
        $isMobile = false,
        $isBoth = false
    ) {
        $errors = [];

        $inputFieldsPrivate = $this->getCustomerDataFields($hasSubscription, $isMobile, $isBoth);
        $inputFieldsBusiness = $this->getBusinessDataFields($data, $hasSubscription, $isMobile, $isBoth);
        if ($data['is_business']) {
            $validations = $inputFieldsBusiness;
            switch ($data['company_address']) {
                case 'other_address' :
                    $validationsBusiness = [
                        'company_street' => [
                            'required' => true,
                            'validation' => [
                                'validateName'
                            ]
                        ],
                        'company_house_nr' => [
                            'required' => true,
                            'validation' => [
                                'validateNumber'
                            ]
                        ],
                        'company_house_nr_addition' => [
                            'required' => false,
                            'validation' => [
                            ]
                        ],
                        'company_postcode' => [
                            'required' => true,
                            'validation' => $isMobile && $this->isMobileCustomer() ? [] : ['validateNLPostcode'],
                        ],
                        'company_city' => [
                            'required' => true,
                            'validation' => [
                            ]
                        ],
                    ];
                    $errors += Mage::helper('vznl_checkout')->_performValidate(
                        $data['otherAddress'],
                        $validationsBusiness,
                        'customer[otherAddress][', ']'
                    );
                    break;
                case 'foreign_address' :
                    $validationsBusiness = [
                        'company_street' => [
                            'required' => true,
                            'validation' => [
                                'validateName'
                            ]
                        ],
                        'company_house_nr' => [
                            'required' => true,
                            'validation' => [
                                'validateNumber'
                            ]
                        ],
                        'company_house_nr_addition' => [
                            'required' => false,
                            'validation' => [
                            ]
                        ],
                        'extra' => [
                            'required' => false,
                            'validation' => [
                            ]
                        ],
                        'company_city' => [
                            'required' => true,
                            'validation' => [
                            ]
                        ],
                        'company_region' => [
                            'required' => false,
                            'validation' => [
                            ]
                        ],
                        'company_postcode' => [
                            'required' => true,
                            'validation' => [
                            ]
                        ],
                        'company_country_id' => [
                            'required' => true,
                            'validation' => [
                                $this->isAPI ? 'validateCountryCode' : null
                            ]
                        ],
                    ];
                    $errors += Mage::helper('vznl_checkout')->_performValidate($data['foreignAddress'],
                        $validationsBusiness,
                        'customer[foreignAddress][', ']');

                    if ($this->isAPI && !Mage::helper('vznl_validators')->validateForeignPostcode(
                        $data['foreignAddress']['company_postcode'],
                        $data['foreignAddress']['company_country_id']
                    )) {
                        $errors['customer[foreignAddress][company_postcode]'] = $this->__('Field has invalid data');
                    }
                    break;
                default :
                    throw new Exception(Mage::helper('vznl_checkout')->__('Missing company address data'));
            }
        } else {
            $validations = $inputFieldsPrivate;
        }

        $customerId = Mage::getSingleton('customer/session')->getCustomer()->getId();
        if ($hasSubscription) {
            if ($this->isAPI) {
                $errors += $this->validateAPIIdNumberByType($data);
            } else if (!$customerId) {
                $errors += $this->validateIdNumberByType($data);
            }
        }

        return $errors + Mage::helper('vznl_checkout')->_performValidate($data, $validations, 'customer[', ']');
    }

    /**
     * @param array $data
     * @param bool $hasSubscription
     * @param bool $isMobile
     * @param bool $isBoth
     * @return array
     */
    public function validateReuseCustomerData($data, $hasSubscription = false, $isMobile = false, $isBoth = false)
    {
        if ($data['is_business']) {
            $validations = [
                'contractant_reuse_known_data' => [
                    'required' => true,
                    'validation' => [
                        'validateIsNotEmpty'
                    ]
                ],
                'contractant_dob' => [
                    'required' => ($isMobile && !$isBoth) ? true : false,
                    'validation' => [
                        'validateMaxAge',
                        'validateMinAge',
                        'validateIsNLDate'
                    ]
                ],
                'contractant_id_type' => [
                    'required' => $hasSubscription ? true : false,
                    'validation' => [
                        'validateName',
                    ]
                ],
                'contractant_id_number' => [
                    'required' => $hasSubscription ? true : false,
                    'validation' => []
                ],
                'contractant_valid_until' => [
                    'required' => $hasSubscription ? true : false,
                    'validation' => [
                        'validateFutureDate',
                        'validateIsNLDate'
                    ]
                ],
                'contractant_issuing_country' => [
                    'required' => ($hasSubscription || $this->isAPI) ? true : false,
                    'validation' => [
                        'validateCountryCode',
                    ]
                ],
            ];
        } else {
            $validations = [
                'reuse_known_data' => [
                    'required' => true,
                    'validation' => [
                        'validateIsNotEmpty'
                    ]
                ],
                'dob' => [
                    'required' => ($isMobile && !$isBoth) ? true : false,
                    'validation' => [
                        'validateMaxAge',
                        'validateMinAge',
                        'validateIsNLDate'
                    ]
                ],
                'id_type' => [
                    'required' => $hasSubscription ? true : false,
                    'validation' => []
                ],
                'id_number' => [
                    'required' => $hasSubscription ? true : false,
                    'validation' => []
                ],
                'valid_until' => [
                    'required' => $hasSubscription ? true : false,
                    'validation' => [
                        'validateFutureDate',
                        'validateIsNLDate'
                    ]
                ],
                'issuing_country' => [
                    'required' => ($hasSubscription || $this->isAPI) ? true : false,
                    'validation' => [
                        'validateCountryCode',
                    ]
                ],
            ];
        }

        return Mage::helper('vznl_checkout')->_performValidate($data, $validations, 'customer[', ']');
    }

    /**
     * @param array $data
     * @return array
     */
    public function validateVodafoneHomeData($data)
    {
        //TODO validate the input fields
        return [];
    }

    /**
     * @param array $data
     * @return array
     */
    public function validateFixedToMobileData($data)
    {
        //TODO validate the input fields
        return [];
    }

    /**
     * @param array $data
     * @param string $packageId
     * @return array
     * @throws Exception
     */
    public function validateSplitDeliveryAddressData($data, $packageId)
    {
        $errors = [];
        switch ($data['address']) {
            case 'store':
                $addressValidations = [
                    'street' => [
                        'required' => true,
                        'validation' => [
                        ]
                    ],
                    'postcode' => [
                        'required' => true,
                        'validation' => [
                            'validateNLPostcode'
                        ]
                    ],
                    'city' => [
                        'required' => true,
                        'validation' => [
                        ]
                    ],
                ];
                if (Mage::helper('vznl_checkout')->_performValidate(
                    $data, $addressValidations, 'delivery[pakket][' . $packageId . ']store[', ']'
                )) {
                    $errors += [
                        'delivery[pakket][' . $packageId . '][address]' =>
                            Mage::helper('vznl_checkout')->__('Select a store')
                    ];
                }
                break;
            case 'billing_address':
                $addressValidations = [
                    'street' => [
                        'required' => true,
                        'validation' => [
                        ]
                    ],
                    'houseno' => [
                        'required' => false,
                        'validation' => [
                            'validateNumber'
                        ]
                    ],
                    'addition' => [
                        'required' => false,
                        'validation' => [
                        ]
                    ],
                    'postcode' => [
                        'required' => true,
                        'validation' => [
                            'validateNLPostcode'
                        ]
                    ],
                    'city' => [
                        'required' => true,
                        'validation' => [
                        ]
                    ],
                ];
                $errors += Mage::helper('vznl_checkout')->_performValidate(
                    $data,
                    $addressValidations,
                    'delivery[pakket][' . $packageId . '][billingaddress][', ']'
                );
                break;
            case 'other_address' :
                $addressValidations = [
                    'street' => [
                        'required' => true,
                        'validation' => [
                        ]
                    ],
                    'houseno' => [
                        'required' => true,
                        'validation' => [
                            'validateNumber'
                        ]
                    ],
                    'addition' => [
                        'required' => false,
                        'validation' => [
                        ]
                    ],
                    'postcode' => [
                        'required' => true,
                        'validation' => [
                            'validateNLPostcode'
                        ]
                    ],
                    'city' => [
                        'required' => true,
                        'validation' => [
                        ]
                    ],
                ];
                $errors += Mage::helper('vznl_checkout')->_performValidate(
                    $data,
                    $addressValidations,
                    'delivery[pakket][' . $packageId . '][otherAddress][', ']'
                );
                break;
            case 'foreign_address' :
                $addressValidations = [
                    'street' => [
                        'required' => true,
                        'validation' => [
                        ]
                    ],
                    'houseno' => [
                        'required' => false,
                        'validation' => [
                            'validateNumber'
                        ]
                    ],
                    'addition' => [
                        'required' => false,
                        'validation' => [
                        ]
                    ],
                    'extra' => [
                        'required' => false,
                        'validation' => [
                        ]
                    ],
                    'city' => [
                        'required' => true,
                        'validation' => [
                        ]
                    ],
                    'region' => [
                        'required' => false,
                        'validation' => [
                        ]
                    ],
                    'postcode' => [
                        'required' => true,
                        'validation' => [
                        ]
                    ],
                    'country_id' => [
                        'required' => true,
                        'validation' => [
                            $this->isAPI ? 'validateCountryCode' : null
                        ]
                    ],
                ];
                $errors += Mage::helper('vznl_checkout')->_performValidate(
                    $data,
                    $addressValidations,
                    'delivery[pakket][' . $packageId . '][foreignAddress][', ']'
                );

                if ($this->isAPI && !Mage::helper('vznl_validators')->validateForeignPostcode(
                    $data['postcode'],
                    $data['country_id']
                )) {
                    $errors['delivery[pakket][' . $packageId . '][foreignAddress][postcode]'] =
                        $this->__('Field has invalid data');
                }
                break;
            default :
                throw new Exception(Mage::helper('vznl_checkout')->__('Missing delivery address data'));
        }

        return $errors;
    }

    /**
     * @param array $data
     * @return array
     */
    public function validateSplitPaymentAndDelivery($data)
    {
        $errors = [];

        if (isset($data['pakket']) && is_array($data['payment'])) {
            foreach ($data['pakket'] as $packetId => $packetData) {
                if (empty($data['payment'][$packetId])) {
                    $errors += [
                        'payment[pakket][' . $packetId . '][type]' =>
                            Mage::helper('vznl_checkout')->__('Please select a payment method')
                    ];
                    continue;
                }
                if (Mage::getSingleton('customer/session')->getOrderEdit()) {
                    $quote = Mage::getModel('sales/quote')
                        ->load(Mage::getSingleton('customer/session')->getOrderEdit());
                    foreach ($quote->getAllItems() as $item) {
                        if ($item->getPackageId() == $packetId) {
                            $packetData['order_id'] = $item->getEditOrderId();
                            break;
                        }
                        continue;
                    }
                    $temp[$packetId] = Mage::helper('core')->jsonEncode($packetData);
                } else {
                    $temp[$packetId] = Mage::helper('core')->jsonEncode($packetData);
                }

                foreach ($temp as $id => $newtemp) {
                    if ($id !== $packetId && $newtemp == $temp[$packetId]) {
                        if ($data['payment'][$id] !== $data['payment'][$packetId]) {
                            $errors += [
                                'payment[pakket][' . $packetId . '][type]' =>
                                    Mage::helper('vznl_checkout')->
                                    __('Payment methods should be the same for same address')
                            ];
                        }
                    }
                }
            }
        }

        return $errors;
    }

    /**
     * @param array $data
     * @return array
     */
    public function validateFixedNumberSelectionData($data)
    {
        $result = [];
        if (empty($data) || !is_array($data)) {
            return $result;
        }

        if (!empty($data['fixed_telephone_number']) && !empty($data['fixed_extra_telephone_no'])) {
            if($data['fixed_telephone_number'] != "") {
                if ($data['fixed_telephone_number'] == $data['fixed_extra_telephone_no']) {
                    $result += [
                        'fixed_extra_telephone_no' => Mage::helper('vznl_checkout')->__('Number is already selected')
                    ];
                }
            }
        }

        return $result;
    }

    /**
     * @param array $data
     * @return array
     */
    public function validateNumberSelectionData($data)
    {
        $result = [];
        if (empty($data) || !is_array($data)) {
            return $result;
        }

        $usedNumbers = [];
        foreach ($data as $id => $number) {
            if ($id && !empty($number['mobile_number'])) {
                // Validate number is unique
                if (in_array($number['mobile_number'], $usedNumbers)) {
                    $result += [
                        'numberselection[' . $id . '][mobile_number]' =>
                            Mage::helper('vznl_checkout')->__('Number is already selected')
                    ];
                } else {
                    array_push($usedNumbers, $number['mobile_number']);
                }
                // Validate sim is correct format
                if (!empty($number['new_sim_number']) && !Mage::helper('dyna_validators/data')->validateSim($number['new_sim_number'])) {
                    $result += [
                        'numberselection[' . $id . '][new_sim_number]' =>
                            Mage::helper('vznl_checkout')->__('Invalid simcard number')
                    ];
                }
            }
        }
        unset($usedNumbers);

        return $result;
    }

    /**
     * @param array $data
     * @return array
     */
    public function validateNewNetherlands($data)
    {
        $result = [];
        foreach ($data as $id => $number) {
            if (!isset($number['order_number'])) {
                continue;
            }
            $number['order_number'] = trim($number['order_number']);
            if ($id && (!isset($number['order_number']) || empty($number['order_number']))) {
                $result += [
                    'new_netherlands[' . $id . '][order_number]' =>
                        Mage::helper('vznl_checkout')->__('This is a required field.')
                ];
            }
        }

        return $result;
    }

    /**
     * @param array $data
     * @return array
     */
    public function validateIncomeTestData($data)
    {
        $errors = [];

        $choice = $data['choice'];
        if ($choice == 'fill-in-later') {
            $email = $data['email'];
            if (!Mage::helper('dyna_validators')->validateEmailSyntax($email)) {
                $errors += [
                    'incometest[email]' =>
                        Mage::helper('vznl_checkout')->__('Please enter a valid email address. For example johndoe@domain.com.')
                ];
            }
        }
        if ($choice == 'fill-in-now') {
            $errors += Mage::helper('ilt')->validateData($data);
        }

        return $errors;
    }

    /**
     * @param array $data
     * @return array
     * @throws Exception
     */
    public function validateSplitBillingAddressData($data)
    {
        $errors = [];
        switch ($data['address']) {
            case 'billing_address':
                $addressValidations = [
                    'street' => [
                        'required' => true,
                        'validation' => [
                        ]
                    ],
                    'houseno' => [
                        'required' => false,
                        'validation' => [
                            'validateNumber'
                        ]
                    ],
                    'addition' => [
                        'required' => false,
                        'validation' => [
                        ]
                    ],
                    'postcode' => [
                        'required' => true,
                        'validation' => [
                            'validateNLPostcode'
                        ]
                    ],
                    'city' => [
                        'required' => true,
                        'validation' => [
                        ]
                    ],
                ];
                $errors += Mage::helper('vznl_checkout')->_performValidate(
                    $data,
                    $addressValidations,
                    'billAddress[',
                    ']'
                );
                break;

            default :
                throw new Exception(Mage::helper('vznl_checkout')->__('Missing billing address data'));
        }

        return $errors;
    }

    /**
     * Validate the id_number depending on the id_type
     *
     * @param array $data
     * @return array
     */
    public function validateIdNumberByType($data)
    {
        $errors = [];
        if ($data['is_business']) {
            if (!Mage::helper('dyna_validators/data')->validateIdType($data['contractant_id_type'],
                $data['contractant_id_number'], $data['contractant_issuing_country'])) {
                $errors['customer[contractant_id_number]'] = Mage::helper('vznl_checkout')->__('Field has invalid data');
            }
        } else {
            if (!Mage::helper('dyna_validators/data')->validateIdType($data['id_type'], $data['id_number'],
                $data['issuing_country'])) {
                $errors['customer[id_number]'] = Mage::helper('vznl_checkout')->__('Field has invalid data');
            }
        }

        return $errors;
    }

    /**
     * @param bool $hasSubscription
     * @param bool $isMobile
     * @param bool $isBoth
     * @return array
     */
    public function getCustomerDataFields($hasSubscription = false, $isMobile = false, $isBoth = false)
    {
        $customerId = Mage::getSingleton('customer/session')->getCustomer()->getId();
        return [
            'firstname' => [
                'required' => true,
                'validation' => [
                    'validateName'
                ]
            ],
            'middlename' => [
                'required' => false,
                'validation' => [
                    'validateName'
                ]
            ],
            'lastname' => [
                'required' => true,
                'validation' => !$this->isAPI ? [
                    'validateName'
                ] : []
            ],
            'gender' => [
                'required' => true,
                'validation' => [
                    'validateIsNumber'
                ]
            ],
            'dob' => [
                'required' => true,
                'validation' => [
                    'validateMaxAge',
                    'validateMinAge',
                    'validateIsNLDate'
                ]
            ],
            'id_type' => [
                'required' => ($hasSubscription && !$customerId) ? true : false,
                'validation' => []
            ],
            'id_number' => [
                'required' => ($hasSubscription && !$customerId) ? true : false,
                'validation' => []
            ],
            'valid_until' => [
                'required' => ($hasSubscription && !$customerId) ? true : false,
                'validation' => (!empty($customerId)) ? [
                    'validateFutureDate',
                    'validateIsNLDate'
                ] : []
            ],
            'issuing_country' => [
                'required' => (!$customerId && ($hasSubscription || $this->isAPI)) ? true : false,
                'validation' => [
                    'validateCountryCode',
                ]
            ],
        ];
    }

    /**
     * @param array $data
     * @param bool $hasSubscription
     * @param bool $isMobile
     * @param bool $isBoth
     * @return array
     */
    public function getBusinessDataFields(
        $data = [],
        $hasSubscription = false,
        $isMobile = false,
        $isBoth = false
    ) {
        $customerId = Mage::getSingleton('customer/session')->getCustomer()->getId();
        return [
            'company_vat_id' => [
                'required' => false,
                'validation' => [
                    'validateName'
                ]
            ],
            'company_coc' => [
                'required' => ($this->isAPI || $customerId) ? false : true,
                'validation' => (!$customerId) ? Mage::helper('vznl_checkout')->getKvKValidation($data) : ''
            ],
            'company_name' => [
                'required' => true,
                'validation' => !$this->isAPI ? [
                    'validateName'
                ] : []
            ],
            'company_date' => [
                'required' => true,
                'validation' => [
                    'validatePastDate',
                    'validateIsNLDate'
                ]
            ],
            'company_legal_form' => [
                'required' => true,
                'validation' => [
                    'validateName'
                ]
            ],
            'contractant_gender' => [
                'required' => true,
                'validation' => [
                    'validateIsNumber'
                ]
            ],
            'contractant_firstname' => [
                'required' => true,
                'validation' => [
                    'validateName'
                ]
            ],
            'contractant_middlename' => [
                'required' => false,
                'validation' => [
                    'validateName'
                ]
            ],
            'contractant_lastname' => [
                'required' => true,
                'validation' => !$this->isAPI ? [
                    'validateName'
                ] : []
            ],
            'contractant_dob' => [
                'required' => true,
                'validation' => [
                    'validateMaxAge',
                    'validateMinAge',
                    'validateIsNLDate'
                ]
            ],
            'contractant_id_type' => [
                'required' => ($hasSubscription && !$customerId) ? true : false,
                'validation' => [
                    'validateName',
                ]
            ],
            'contractant_id_number' => [
                'required' => ($hasSubscription && !$customerId) ? true : false,
                'validation' => []
            ],
            'contractant_valid_until' => [
                'required' => ($hasSubscription && !$customerId) ? true : false,
                'validation' => (!empty($customerId)) ? [
                    'validateFutureDate',
                    'validateIsNLDate'
                ] : []
            ],
            'contractant_issuing_country' => [
                'required' => (!$customerId && ($hasSubscription || $this->isAPI)) ? true : false,
                'validation' => [
                    'validateCountryCode',
                ]
            ],
        ];
    }

    /**
     * Checks CTN validity
     * @param string $value
     * @param string $errorPrefix
     * @return array
     */
    public function checkCtn(string $value, string $errorPrefix = 'customer'): array
    {
        $errors = [];
        $valid = false;
        if (!(!is_numeric($value)
            || strpos($value, '097') === 0
            || strpos($value, '0800') === 0
            || strpos($value, '0900') === 0
            || substr($value, 0, 1) == '+'
            || strpos($value, ' ') !== false
            || strlen($value) < 10
            || strlen($value) > 20)) {
            $valid = true;
        }

        if (!$valid) {
            $errors[$errorPrefix . '[phone_number]'] = $this->__('Invalid phone number');
        }

        return $errors;
    }

    /**
     * @param array $additionalAttributes
     * @return array
     */
    public function validateApiILTData($additionalAttributes)
    {
        $errors = [];
        if (!empty($additionalAttributes)) {
            foreach ($additionalAttributes as $additionalAttribute) {
                // Check allowed keys

                $errors += $this->validateApiILTKey($additionalAttribute);

                // Check allowed types
                $errors += $this->validateApiILTType($additionalAttribute);

                // Check allowed values
                $errors += $this->validateApiILTValue($additionalAttribute);
            }
        }
        return $errors;
    }

    /**
     * @param array $additionalAttribute
     * @return array
     */
    protected function validateApiILTKey(array $additionalAttribute): array
    {
        $errors = [];
        if (!in_array($additionalAttribute['key'], array_keys($this->customerAdditionalAttributes))) {
            $errors['customer[additional_attributes]'] = sprintf(
                'Additional attributes must be one of [%s]',
                implode(', ', array_keys($this->customerAdditionalAttributes))
            );
        }
        return $errors;
    }

    /**
     * @param array $additionalAttribute
     * @return array
     */
    protected function validateApiILTType(array $additionalAttribute): array
    {
        $errors = [];
        $type = $this->customerAdditionalAttributes[$additionalAttribute['key']]['type'] ?? '';
        $typeErrorMsg = 'Additional attribute "%s" must have a type "%s"';
        switch ($type) {
            case 'bool':
                if ($additionalAttribute['value'] === 'true' || $additionalAttribute['value'] === 'false') {
                    $additionalAttribute['value'] = $additionalAttribute['value'] === 'true' ? true : false;
                } else {
                    $errors['customer[additional_attributes]'] = sprintf(
                        $typeErrorMsg,
                        $additionalAttribute['key'],
                        $type
                    );
                }
                break;
            case 'int':
                if (!is_numeric($additionalAttribute['value'])
                    || strpos($additionalAttribute['value'], '.') !== false
                    || strpos($additionalAttribute['value'], ',') !== false
                ) {
                    $errors['customer[additional_attributes]'] = sprintf(
                        $typeErrorMsg,
                        $additionalAttribute['key'],
                        $type
                    );
                }
                break;
            case 'string':
                if (!is_string($additionalAttribute['value'])) {
                    $errors['customer[additional_attributes]'] = sprintf(
                        $typeErrorMsg,
                        $additionalAttribute['key'],
                        $type
                    );
                }
                $pattern = $this->customerAdditionalAttributes[$additionalAttribute['key']]['pattern'] ?? '';
                if (!empty($pattern)) {
                    if (preg_match($pattern, $additionalAttribute['value'])) {
                        $errors['customer[additional_attributes]'] = sprintf(
                            'Additional attribute "%s" does not match the pattern [%s]',
                            $additionalAttribute['key'],
                            $pattern
                        );
                    }
                }
                break;
        }
        return $errors;
    }

    /**
     * @param array $additionalAttribute
     * @return array
     */
    protected function validateApiILTValue(array $additionalAttribute): array
    {
        $errors = [];
        $value = $this->customerAdditionalAttributes[$additionalAttribute['key']]['value'] ?? '';
        $type = $this->customerAdditionalAttributes[$additionalAttribute['key']]['type'] ?? '';
        if (!empty($value) && $additionalAttribute['value'] !== $value) {
            $errors['customer[additional_attributes]'] = sprintf(
                'Additional attribute "%s" must have a value "%s"',
                $additionalAttribute['key'],
                $type !== 'bool' ? $value :
                    $type === 'bool' && (int)$value === 1 ? 'true' : 'false'
            );
        }
        return $errors;
    }

    /**
     * @param array $requestCustomer
     * @return array
     */
    protected function mapApiCustomer(array $requestCustomer): array
    {
        if (!empty($requestCustomer)) {
            if (!$this->isBusiness) {
                $customerData = $requestCustomer['party']['individual'] ?? null;
                $identificationData = $customerData['individualIdentification'][0] ?? null;
                $countryObj = Mage::getModel("directory/country")->load(
                    $identificationData['country'] ?? self::CUSTOMER_DEFAULT_COUNTRY, 'iso3_code'
                );
                if (!empty($countryObj->getId())) {
                    $country = $countryObj->getCountryId();
                }

                $customer = [
                    'is_business' => $this->isBusiness,
                    'firstname' => $customerData['initials'] ?? null,
                    'middlename' => null,
                    'lastname' => $customerData['lastName'] ?? null,
                    'gender' => $this->mapApiGender($customerData['gender'] ?? null),
                    'dob' => !empty($customerData['birthDate']) ?
                        date('d-m-Y', strtotime($customerData['birthDate'])) : null,
                    'id_type' => Mage::helper('dyna_customer')
                        ->mapCustomerIDType($identificationData['type'] ?? '', true),
                    'id_number' => $identificationData['identificationId'] ?? null,
                    'valid_until' => !empty($identificationData['validFor']['endDate']) ?
                        date('d-m-Y', strtotime($identificationData['validFor']['endDate'])) : null,
                    'issue_date' => null,
                    'issuing_country' => $country ?? $identificationData['country'] ?? null
                ];
            } else {
                // business customer
                $organizationData = $requestCustomer['party']['organization'] ?? null;
                $customerData = $organizationData['contractingParty']['details'] ?? null;
                $identificationData = $customerData['individualIdentification'][0] ?? null;
                $companyAddress = $requestCustomer['contactMedium']['addresses'][0] ?? null;
                $contactantCountryObj = Mage::getModel("directory/country")->load(
                    $identificationData['country'] ?? self::CUSTOMER_DEFAULT_COUNTRY, 'iso3_code'
                );
                if (!empty($contactantCountryObj->getId())) {
                    $contractantCountry = $contactantCountryObj->getCountryId();
                }
                $companyCountryObj = Mage::getModel("directory/country")->load(
                    $companyAddress['country'] ?? self::CUSTOMER_DEFAULT_COUNTRY, 'iso3_code'
                );
                if (!empty($companyCountryObj->getId())) {
                    $companyCountry = $companyCountryObj->getCountryId();
                }

                $customer = [
                    'is_business' => $this->isBusiness,
                    'company_name' => $organizationData['name'] ?? null,
                    'company_date' => !empty($organizationData['establishmentDate']) ?
                        date('d-m-Y', strtotime($organizationData['establishmentDate'])) : null,
                    'contractant_firstname' => $customerData['initials'] ?? null,
                    'contractant_middlename' => null,
                    'contractant_lastname' => $customerData['lastName'] ?? null,
                    'contractant_gender' => $this->mapApiGender($customerData['gender'] ?? null),
                    'contractant_dob' => !empty($customerData['birthDate']) ?
                        date('d-m-Y', strtotime($customerData['birthDate'])) : null,
                    'contractant_id_type' => Mage::helper('dyna_customer')
                        ->mapCustomerIDType($identificationData['type'] ?? '', true),
                    'contractant_id_number' => $identificationData['identificationId'] ?? null,
                    'contractant_valid_until' => !empty($identificationData['validFor']['endDate']) ?
                        date('d-m-Y', strtotime($identificationData['validFor']['endDate'])) : null,
                    'contractant_issue_date' => null,
                    'contractant_issuing_country' => $contractantCountry ?? $identificationData['country'] ?? null,
                    'company_legal_form' => 'EENMANSZAAK'
                ];

                if ($companyAddress['country'] !== self::CUSTOMER_DEFAULT_COUNTRY) {
                    $customer['company_address'] = 'foreign_address';
                    $customer['foreignAddress'] = [
                        'company_street' => $companyAddress['street'] ?? null,
                        'company_house_nr' => $companyAddress['houseNumber'] ?? null,
                        'company_house_nr_addition' => $companyAddress['houseNumberAddition'] ?? null,
                        'company_city' => $companyAddress['city'] ?? null,
                        'company_postcode' => $companyAddress['postalCode'] ?? null,
                        'company_country_id' => $companyCountry ?? $companyAddress['country'] ?? null
                    ];
                } else {
                    $customer['company_address'] = 'other_address';
                    $customer['otherAddress'] = [
                        'company_street' => $companyAddress['street'] ?? null,
                        'company_house_nr' => $companyAddress['houseNumber'] ?? null,
                        'company_house_nr_addition' => $companyAddress['houseNumberAddition'] ?? null,
                        'company_city' => $companyAddress['city'] ?? null,
                        'company_postcode' => $companyAddress['postalCode'] ?? null,
                        'company_country_id' => $companyCountry ?? $companyAddress['country'] ?? null
                    ];
                }
            }
        }

        return !empty($customerData) ? ($customer ?? []) : [];
    }

    /**
     * @param array $requestCustomer
     * @return array
     */
    protected function mapApiBillingAddress(array $requestCustomer): array
    {
        if (!empty($requestCustomer)) {
            $address = $requestCustomer['contactMedium']['addresses'][0] ?? null;
            $countryObj = Mage::getModel("directory/country")->load(
                $address['country'] ?? self::CUSTOMER_DEFAULT_COUNTRY, 'iso3_code'
            );
            if (!empty($countryObj->getId())) {
                $country = $countryObj->getCountryId();
            }

            if ($address['country'] !== self::CUSTOMER_DEFAULT_COUNTRY) {
                $billingAddress = [
                    'address' => 'foreign_address',
                    'foreignAddress' => [
                        'street' => $address['street'] ?? null,
                        'houseno' => $address['houseNumber'] ?? null,
                        'addition' => $address['houseNumberAddition'] ?? null,
                        'city' => $address['city'] ?? null,
                        'postcode' => $address['postalCode'] ?? null,
                        'country_id' => $country ?? $address['country'] ?? null
                    ]
                ];
            } else {
                $billingAddress = [
                    'address' => 'other_address',
                    'otherAddress' => [
                        'street' => $address['street'] ?? null,
                        'houseno' => $address['houseNumber'] ?? null,
                        'addition' => $address['houseNumberAddition'] ?? null,
                        'city' => $address['city'] ?? null,
                        'postcode' => $address['postalCode'] ?? null,
                        'country_id' => $country ?? $address['country'] ?? null
                    ]
                ];
            }
        }
        return $billingAddress ?? [];
    }

    /**
     * @param string|null $gender
     * @return int
     */
    protected function mapApiGender(string $gender = null): int
    {
        switch (strtoupper($gender)) {
            case 'FEMALE':
                $gender = 2;
                break;
            case 'MALE':
                $gender = 1;
                break;
            default:
                $gender = 0;
                break;
        }

        return $gender;
    }

    /**
     * @param array|null $mageErrors
     * @return string
     */
    protected function mapErrorsToAPI(array $mageErrors = null): string
    {
        $errors = [];
        if (!empty($mageErrors)) {
            foreach ($mageErrors as $field => $error) {
                if (strpos($field, 'customer[') !== false) {
                    $fieldName = $this->mapCustomerErrorsToAPI($field) . ': ' . $error;
                }
                if (strpos($field, 'address[') !== false) {
                    $fieldName = $this->mapBillingAddressErrorsToAPI($field) . ': ' . $error;
                }
                $errors[] = $fieldName ?? $field . ': ' . $error;
            }
        }

        return Mage::helper('core')->jsonEncode(array_unique($errors));
    }

    /**
     * @param string $field
     * @return string
     */
    protected function mapCustomerErrorsToAPI(string $field): string
    {
        preg_match("/\[([^\]]*)\]/", $field, $matches);
        $fieldName = $matches[1] ?? $field;

        $mappings = [
            'dob' => '[customer[].party.individual.individualIdentification[].birthDate]',
            'valid_until' => '[customer[].party.individual.individualIdentification[].validFor.endDate]',
            'id_number' => '[customer[].party.individual.individualIdentification[].identificationId]',
            'issuing_country' => '[customer[].party.individual.individualIdentification[].country]',
            'additional_attributes' => '[customer[].party.individual.additionalAttributes]',
            'bank_account_number' => '[customer[].bankAccount.number]',
            'phone_number' => '[customer[].phoneNumbers[].fullNumber]',
            'email' => '[customer[].emailAddresses[].value]'
        ];

        if ($this->isBusiness) {
            $mappings = array_merge($mappings, [
                'dob' => '[customer[].party.organization.contractingParty.details[].birthDate]',
                'valid_until' => '[customer[].party.individual.contractingParty.details[].validFor.endDate]',
                'id_number' => '[customer[].party.individual.contractingParty.details[].identificationId]',
                'issuing_country' => '[customer[].party.individual.contractingParty.details[].country]',
                'company_date' => '[customer[].party.organization.establishmentDate]',
                'additional_attributes' => '[customer[].party.organization.additionalAttributes]',
                'contractant_id_number' =>
                    '[customer[].party.organization.contractingParty[].individualIdentification[].identificationId]',
                'organization_identification_country' =>
                    '[customer[].party.organization.organizationIdentification[].country]',
                'organization_identification_valid_until' =>
                    '[customer[].party.organization.organizationIdentification[].validFor.endDate]',
                'company_id_number' => '[customer[].party.organization.organizationIdentification[].identificationId]'
            ]);
        }

        if (array_key_exists($fieldName, $mappings)) {
            return $mappings[$fieldName];
        }

        return $fieldName;
    }

    /**
     * @param string $field
     * @return string
     */
    protected function mapBillingAddressErrorsToAPI(string $field): string
    {
        preg_match_all("/\[([^\]]*)\]/", $field, $matches);
        $fieldName = $matches[1][1] ?? $field;

        $mappings = [
            'country_id' => '[customer[].contactMedium.addresses[].country]',
        ];

        if (array_key_exists($fieldName, $mappings)) {
            return $mappings[$fieldName];
        }

        return $fieldName;
    }

    /**
     * @param array $organizationIdentification
     * @return array
     */
    protected function validateApiOrganizationData(array $organizationIdentification): array
    {
        $errors = [];
        if ($this->isBusiness && !empty($organizationIdentification)) {
            $countryObj = Mage::getModel("directory/country")->load(
                $organizationIdentification['country'] ?? self::CUSTOMER_DEFAULT_COUNTRY, 'iso3_code'
            );
            if (!empty($countryObj->getId())) {
                $country = $countryObj->getCountryId();
            }

            $type = Mage::helper('dyna_customer')->mapCustomerIDType($organizationIdentification['type'] ?? '', true);
            if (!Mage::helper('dyna_validators/data')->validateIdType(
                $type,
                $organizationIdentification['identificationId'],
                $country ?? $organizationIdentification['country'])
            ) {
                // Do not validate types from 5 to 9 as there are no parsers
                if (!(is_numeric($type) && $type > 4)) {
                    $errors['customer[company_id_number]'] = $this->__('Invalid ID number');
                }
            }

            $validateData = [
                'country' => $country ?? $organizationIdentification['country'] ?? null,
                'valid_until' => !empty($organizationIdentification['validFor']['endDate']) ?
                    date('d-m-Y', strtotime($organizationIdentification['validFor']['endDate'])) : null
            ];

            $validations = [
                'country' => [
                    'required' => false,
                    'validation' => [
                        'validateCountryCode',
                    ]
                ],
                'valid_until' => [
                    'required' => false,
                    'validation' => [
                        'validateFutureDate',
                        'validateIsNLDate'
                    ]
                ]
            ];

            $errors += Mage::helper('vznl_checkout')->_performValidate(
                $validateData,
                $validations,
                'customer[organization_identification_',
                ']'
            );
        }

        return $errors;
    }

    /**
     * @param array $address
     * @param string $errorPrefix
     * @return array
     */
    public function validateApiAddress(array $address, string $errorPrefix = 'checkoutDetails'): array
    {
        $errors = [];
        if (!empty($address)) {
            $addressValidations = [
                'street' => [
                    'required' => true,
                    'validation' => [
                    ]
                ],
                'houseNumber' => [
                    'required' => false,
                    'validation' => [
                        'validateNumber'
                    ]
                ],
                'houseNumberAddition' => [
                    'required' => false,
                    'validation' => [
                    ]
                ],
                'postalCode' => [
                    'required' => true,
                    'validation' => []
                ],
                'city' => [
                    'required' => true,
                    'validation' => []
                ],
                'country' => [
                    'required' => true,
                    'validation' => [
                        'validateCountryCode'
                    ]
                ],
                'type' => [
                    'required' => true,
                    'validation' => []
                ],
            ];

            if ($address['country'] !== self::CUSTOMER_DEFAULT_COUNTRY) {
                if (!Mage::helper('vznl_validators')->validateForeignPostcode(
                    $address['postalCode'],
                    $address['country']
                )) {
                    $errors[$errorPrefix . '[postalCode]'] = $this->__('Field has invalid data');
                }
            } else {
                $addressValidations['postalCode'] = [
                    'required' => true,
                    'validation' => [
                        'validateNLPostcode'
                    ]
                ];
            }
            $errors += Mage::helper('vznl_checkout')->_performValidate(
                $address,
                $addressValidations,
                $errorPrefix . '[',
                ']'
            );
        }

        return $errors;
    }

    /**
     * @param array $data
     * @return array
     */
    public function validateApiPhoneNumbers(array $data): array
    {
        $errors = [];
        foreach ($data['customer'][0]['contactMedium']['phoneNumbers'] ?? [] as $phoneData) {
            $errors += $this->checkCtn($phoneData['fullNumber'] ?? '');
        }
        if ($this->isBusiness) {
            $contractingParty = $data['customer'][0]['party']['organization']['contractingParty'] ?? null;
            $phoneNumbers = $contractingParty['contactMedium']['phoneNumbers'] ?? [];
            foreach ($phoneNumbers as $phoneData) {
                $errors += $this->checkCtn($phoneData['fullNumber'] ?? '', 'contractant');
            }
        }
        return $errors;
    }

    /**
     * @param array $data
     * @return array
     */
    public function validateApiEmails(array $data): array
    {
        $errors = [];
        foreach ($data['customer'][0]['contactMedium']['emailAddresses'] ?? [] as $emailData) {
            if (!Mage::helper('dyna_validators')->validateEmailSyntax($emailData['value'] ?? '')) {
                $errors += [
                    'customer[email]' =>
                        $this->__('Please enter a valid email address. For example johndoe@domain.com.')
                ];
            }
        }
        if ($this->isBusiness) {
            $contractingParty = $data['customer'][0]['party']['organization']['contractingParty'] ?? null;
            $emails = $contractingParty['contactMedium']['emailAddresses'] ?? [];
            foreach ($emails as $emailData) {
                if (!Mage::helper('dyna_validators')->validateEmailSyntax($emailData['value'] ?? '')) {
                    $errors += [
                        'contractant[email]' =>
                            $this->__('Please enter a valid email address. For example johndoe@domain.com.')
                    ];
                }
            }
        }
        return $errors;
    }

    /**
     * @param array $data
     * @return array
     */
    public function validateApiRetentionData(array $data): array
    {
        $errors = [];
        foreach ($data['packages'] ?? [] as $package) {
            if (in_array(
                    $package['processContext'] ?? null,
                    [Vznl_Catalog_Model_ProcessContext::RETBLU, Vznl_Catalog_Model_ProcessContext::RETNOBLU]
                )
                && empty($package['ctn'])
            ) {
                $errors += [
                    'package[ctn]' =>
                        $this->__('Please enter a valid CTN.')
                ];
            }
        }
        return $errors;
    }

    /**
     * Validations specific only for SC API
     *
     * @param array $data
     * @return array
     */
    protected function APIValidations(array $data): array
    {
        $errors = [];
        $customer = $data['customer'][0] ?? null;
        $party = $customer['party'] ?? null;
        $customerData = $party['individual'] ?? null;
        $checkoutDetails = $data['checkoutDetails'] ?? null;
        if ($this->isBusiness) {
            $customerData = $party['organization']['contractingParty']['details'] ?? null;
            $organizationIdentification = $party['organization']['organizationIdentification'][0] ?? null;
        }

        // Validate customer additional attributes
        $errors += $this->validateApiILTData($customerData['additionalAttributes'] ?? []);

        // Validate checkout details delivery address
        $address = $checkoutDetails['deliveryOption']['deliverToAddress']['address'] ?? [];
        $errors += $this->validateApiAddress($address);

        // Bank account
        $iban = $data['customer'][0]['bankAccount']['number'] ?? null;
        if ($iban && !Mage::helper('dyna_validators')->validateNLIBAN($iban)) {
            $errors += [
                'customer[bank_account_number]' => $this->__('Incorrect bank account')
            ];
        }

        // Validate phone numbers
        $errors += $this->validateApiPhoneNumbers($data);

        // Emails
        $errors += $this->validateApiEmails($data);

        // Retention requests
        $errors += $this->validateApiRetentionData($data);

        if ($this->isBusiness) {
            // Validate company identification
            $errors += $this->validateApiOrganizationData($organizationIdentification ?? []);

            // Validate contractant contact address
            $address = $party['organization']['contractingParty']['contactMedium']['addresses'][0] ?? [];
            $errors += $this->validateApiAddress($address, 'contractant');
        }

        return $errors;
    }

    /**
     * Validate the id_number depending on the id_type
     *
     * @param array $data
     * @return array
     */
    public function validateAPIIdNumberByType($data)
    {
        $errors = [];
        if ($data['is_business']) {
            if (!empty($data['contractant_id_type'])
                && !empty($data['contractant_id_number'])
                && !empty($data['contractant_issuing_country'])) {
                if (!Mage::helper('dyna_validators/data')->validateAPIIdType($data['contractant_id_type'],
                    $data['contractant_id_number'], $data['contractant_issuing_country'])) {
                    $errors['customer[contractant_id_number]'] = Mage::helper('vznl_checkout')
                        ->__('Field has invalid data');
                }

                // NLD not allowed for TYPE_I to IV and EU
                if (in_array($data['contractant_id_type'] ?? null, ['0', '1', '2', '3', '4'])
                    && ($data['contractant_issuing_country'] ?? null) == 'NL') {
                    $errors['customer[contractant_issuing_country]'] = Mage::helper('vznl_checkout')
                        ->__('Contract issuing country cannot be The Netherlands.');
                }
            }
        } else {
            if (!empty($data['id_type'])
                && !empty($data['id_number'])
                && !empty($data['issuing_country'])) {
                if (!Mage::helper('dyna_validators/data')->validateAPIIdType($data['id_type'], $data['id_number'],
                    $data['issuing_country'])) {
                    $errors['customer[id_number]'] = Mage::helper('vznl_checkout')
                        ->__('Field has invalid data');
                }

                // NLD not allowed for TYPE_I to IV and EU
                if (in_array($data['id_type'] ?? null, ['0', '1', '2', '3', '4'])
                    && ($data['issuing_country'] ?? null) == 'NL') {
                    $errors['customer[issuing_country]'] = Mage::helper('vznl_checkout')
                        ->__('Contract issuing country cannot be The Netherlands.');
                }
            }
        }

        return $errors;
    }

    /**
     * Validate payment data
     *
     * @param $customer
     * @param $data
     * @return array
     */
    public function validatePaymentData($data, $customer)
    {
        $errors = [];

        $validations = [];

        if (isset($data['monthly_method']) && $data['monthly_method'] == 'direct_debit') {
            if ($customer->getIsProspect() || !$customer->getId()) {
                $validations = [
                    'account_no' => [
                        'required' => true,
                        'validation' => [
                            'validateBicIban'
                        ]
                    ],
                    'account_holder' => [
                        'required' => true,
                        'validation' => [
                            'validateName',
                        ]
                    ],

                ];
            }
        }
        else if (isset($data['monthly_method']) && $data['monthly_method'] == 'multiple_payment') {
            // for split payments mobile
            if (isset($data['account_no_mobile']) && !empty($data['account_no_mobile']) && (strpos($data['account_no_mobile'], '*') === false)) {
                $validations['account_no_mobile'] = [
                    'required' => true,
                    'validation' => [
                        'validateBicIban'
                    ]
                ];
            }
            if (isset($data['fixed_method']) && $data['fixed_method'] == 'direct_debit_fixed') {
                // for split payments fixed
                if (isset($data['account_no_fixed_different']) && !empty($data['account_no_fixed_different']) && (strpos($data['account_no_fixed_different'], '*') === false)) {
                    $validations['account_no_fixed_different']=[
                        'required'=>true,
                        'validation'=>[
                            'validateBicIban'
                        ]
                    ];
                }
            }
        }

        $errors += Mage::helper('vznl_checkout')->_performValidate($data, $validations, 'payment[', ']');

        return $errors;
    }

    /**
     * @return bool
     */
    private function isMobileCustomer() : bool
    {
        if ($this->isMobileCustomer === null) {
            $customer = Mage::getSingleton('customer/session')->getCustomer();

            $this->isMobileCustomer = $customer->getId() && $customer->getBan();
        }

        return $this->isMobileCustomer;

    }

}
