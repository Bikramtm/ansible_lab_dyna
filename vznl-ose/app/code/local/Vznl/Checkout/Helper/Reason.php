<?php

class Vznl_Checkout_Helper_Reason extends Mage_Core_Helper_Abstract
{
    /** @var Vznl_Configurator_Model_Cache */
    protected $_cache;

    public function getManualActivationForDropdown()
    {
        $key = md5(__METHOD__);
        $checkoutHelper = Mage::helper('vznl_checkout');
        if (!($result = unserialize($this->getCache()->load($key)))) {
            $reasons = Mage::getResourceModel('vznl_checkout/reason_collection')->load();
            $result = [['key' => '', 'value' => $checkoutHelper->__('Select an option'), 'remark' => false]];
            foreach ($reasons as $key => $reason) {
                $result[] = ['key' => $checkoutHelper->__($reason->getName()), 'value' => $checkoutHelper->__(trim($reason->getName())), 'remark' => $reason->getRequiresInput()];
            }

            $this->getCache()->save(serialize($result), $key, array(Vznl_Configurator_Model_Cache::REASON_TAG), $this->getCache()->getTtl());
        }

        return $result;
    }

    /**
     * @return Vznl_Configurator_Model_Cache
     */
    protected function getCache()
    {
        if (!$this->_cache) {
            $this->_cache = Mage::getSingleton('vznl_configurator/cache');
        }

        return $this->_cache;
    }
}
