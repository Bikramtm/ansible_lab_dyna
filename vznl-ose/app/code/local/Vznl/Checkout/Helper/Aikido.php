<?php

/**
 * Class Aikido Helper
 */
class Vznl_Checkout_Helper_Aikido extends Mage_Core_Helper_Abstract
{
    const XML_PATH_RETENTION = 'catalog/frontend/sku_retention';
    const XML_PATH_ACQUISITION = 'catalog/frontend/sku_acquisition';
    const XML_PATH_INDIRECT_NO_HARDWARE = 'catalog/frontend/sku_indirect_no_hardware';

    public function getDefaultRetentionSku()
    {
        return Mage::getStoreConfig(self::XML_PATH_RETENTION);
    }

    public function getDefaultAcquisitionSku()
    {
        return Mage::getStoreConfig(self::XML_PATH_ACQUISITION);
    }

    /**
     * @return bool|string
     */
    public function getIndirectNoHardwareSku()
    {
        $value = Mage::getStoreConfig(self::XML_PATH_INDIRECT_NO_HARDWARE);
        return count(trim($value)) > 0 ? trim($value) : false;
    }

    /**
     * This function should return a list with the special deviceRCs ids
     * @return array
     */
    public function getSpecialDeviceRcIds()
    {
        $skus = [
            $this->getDefaultRetentionSku(),
            $this->getDefaultAcquisitionSku(),
            $this->getIndirectNoHardwareSku()
        ];
        $ids = [];
        foreach ($skus as $sku) {
            $ids[] = Mage::getResourceSingleton('catalog/product')->getIdBySku($sku);
        }

        return $ids;
    }
}
