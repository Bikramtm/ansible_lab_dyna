<?php

class Vznl_Checkout_Helper_RegisterReturn extends Mage_Core_Helper_Abstract
{
    /**
     * Register a return for the package with the given package id and the super order with the given super order id.
     * @param int $superOrderId The super order id.
     * @param int $packageId The package id.
     * @param array $packageData The package data.
     * @param bool $triggerComplete
     * @param array $temporaryQueueItems
     * @return array The results.
     */
    public function registerReturnForPackage($superOrderId, $packageId, $packageData, $triggerComplete = false, $temporaryQueueItems = [])
    {
        $customerSession = Mage::getSingleton('customer/session');
        /** @var $superOrder Vznl_Superorder_Model_Superorder */
        $superOrder = Mage::getModel('superorder/superorder')->load($superOrderId);
        $packageCompleteReturn = $triggerComplete && isset($packageData['complete_return']) && $packageData['complete_return'];

        if ($packageCompleteReturn || trim($packageData['return_date'])) {
            $this->_validateRefundData($packageData);
        }
        /** @var Vznl_Package_Model_Package $package */
        $package = Mage::getModel('package/package')->getPackages($superOrderId, null, $packageId)->getFirstItem();

        if ($package->getReturnDate()){
            throw new Exception($this->__('The package is already returned'), 5);
        }
        $orderPackage = $this->updateOrderPackage($package, $packageData, $triggerComplete);
        $orderPackage->save();

        $currentItems = $this->updatePackageItems($packageId, $orderPackage, $temporaryQueueItems);

        $payload = Mage::helper('communication/email')->getReturnGoodsPayload($superOrder);
        Mage::getSingleton('communication/pool')->add($payload);

        $result = [];
        $result['html'] = Mage::getSingleton('core/layout')
            ->createBlock('core/template')
            ->setPackage($orderPackage)
            ->setItems($currentItems)
            ->setTemplate('checkout/cart/steps/return_note.phtml')
            ->toHtml();
        $result['error'] = false;

        //only trigger service call if an item is registered as return
        if (isset($packageData['items'])) {
            if (!$superOrder->hasPackageChanged($packageId)) {
                Mage::helper('vznl_checkout')->callWorkItem($packageId, $superOrderId);
            }
        }
        $superOrder->setUpdatedAt(now())->save();

        if ($packageCompleteReturn) {
            // check if there is a new package id
            if ($orderPackage->getNewPackageId()) {
                $packageId = $orderPackage->getNewPackageId();
            }

            $newSuperOrder = $superOrder->getChildSuperorders($packageId) ?: $superOrder;

            if (!$superOrder->cantProcessOrder()) {
                if ($newSuperOrder->getXmlToBeSent()) {
                    Mage::register(
                        'job_additional_information',
                        array(
                            'run_superorder_history' => true,
                            'superorder_id' => $newSuperOrder->getId(),
                            'superorder_parent_id' => $newSuperOrder->getId() == $superOrder->getId() ? null : $superOrder->getId(),
                            'agent_id' => $customerSession->getAgent(true)->getId(),
                            'run_check_lock' => true,
                            'superorder_number' => $superOrder->getOrderNumber(),
                            'initial_state' => true,
                        )
                    );
                    $client = Mage::helper('omnius_service')->getClient('ebs_orchestration');
                    $result['polling_id'] = $client->call('CreateOrder', null, $newSuperOrder->getXmlToBeSent());
                    $customerSession->setData(
                        $result['polling_id'],
                        array(
                            'superorder_id' => $newSuperOrder->getId(),
                            'parent_superorder_id' => $superOrder->getId()
                        )
                    );
                }
            }
        }

        // Change the order confirmation message as stated in RFC-150663
        $result['oldOrderMessage'] = Mage::helper('vznl_checkout')->__('The goods return is successfully registered under the following number.');

        return $result;
    }

    /**
     * Update the order package.
     * @param $orderPackage Vznl_Package_Model_Package The order package
     * @param array $packageData The package data.
     * @param boolean $triggerComplete
     * @return Vznl_Package_Model_Package The updated package.
     */
    private function updateOrderPackage($orderPackage, $packageData, $triggerComplete){
        $allowedFields = $this->getAllowedFields($packageData, $triggerComplete);
        $flippedAllowedFields = array_flip($allowedFields);

        foreach ($packageData as $fieldName => $fieldData) {
            if (isset($flippedAllowedFields[$fieldName])) {
                //for return notes, append to existing notes, don't overwrite
                if ($fieldName == 'return_notes') {
                    $orderPackage->setData($fieldName,
                        $orderPackage->getData($fieldName) . "\n" . $fieldData);
                } else {
                    $orderPackage->setData($fieldName, $fieldData);
                }
            }
        }
        $orderPackage->setReturnedBy($packageData['agent_name']);

        return $orderPackage;
    }

    /**
     * Get the allowed fields.
     * @param array $packageData
     * @param boolean $triggerComplete
     * @return array The array with all allowed fields.
     */
    private function getAllowedFields($packageData, $triggerComplete)
    {
        $allowedFields = array('return_notes', 'not_damaged_items', 'return_channel');
        if ($triggerComplete) {
            $allowedFields[] = 'complete_return';
        }
        if (isset($packageData['complete_return']) && $packageData['complete_return']) {
            $allowedFields[] = 'return_date';
        }

        return $allowedFields;
    }

    /**
     * Update the package items and return the items from the temporaryQueueItems with the given package id.
     * @param int $packageId The package id.
     * @param Vznl_Package_Model_Package $orderPackage
     * @param array $temporaryQueueItems
     * @return array The $temporaryQueueItems from the package.
     */
    private function updatePackageItems($packageId, $orderPackage, $temporaryQueueItems){
        // Get returned items list
        $packageItems = !empty($orderPackage['items']) ? (is_array($orderPackage['items']) ? $orderPackage['items'] : array($orderPackage['items'])) : [];
        $flippedPackageItems = array_flip($packageItems);

        // Set order items as returned
        $items = $orderPackage->getPackageItems();
        foreach ($items as $item) {
            $itemProductId = $item->getProductId();
            if (isset($flippedPackageItems[$itemProductId])) {
                $item->setRegisterReturn(1)->save();
            }
        }

        // Also set temporary queue items as returned
        $currentItems = array();
        foreach ($temporaryQueueItems as $item) {
            if ($item->getPackageId() == $packageId) {
                $currentItems[] = $item;
                $itemProductId = $item->getProductId();
                if (isset($flippedPackageItems[$itemProductId])) {
                    $item->setRegisterReturn(1)->save();
                }
            }
        }

        return $currentItems;
    }

    /**
     * Validate the refund data.
     * @param [] $packageData
     * @throws Exception
     */
    private function _validateRefundData($packageData)
    {

        if (!isset($packageData['return_date'])
            || !Mage::helper('dyna_validators/data')->validateIsNLDate($packageData['return_date'])) {
            throw new Exception($this->__('The value is not a valid date format.'), 3);
        }
    }
}
