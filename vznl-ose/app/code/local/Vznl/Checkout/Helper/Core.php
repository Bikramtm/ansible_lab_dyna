<?php

/**
 * Class Vznl_Checkout_Helper_Core
 */
class Vznl_Checkout_Helper_Core extends Mage_Core_Helper_Data
{
    // PHP 7 backward compatibility fix
    public function jsonDecode($encodedValue, $objectDecodeType = Zend_Json::TYPE_ARRAY)
    {
        if (($encodedValue === null) || ($encodedValue === false)) {
            return null;
        }

        return parent::jsonDecode($encodedValue, $objectDecodeType);
    }
}
