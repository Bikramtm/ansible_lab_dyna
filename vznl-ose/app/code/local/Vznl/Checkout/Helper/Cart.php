<?php

/**
 * Class Vznl_Checkout_Helper_Cart
 */
class Vznl_Checkout_Helper_Cart extends Dyna_Checkout_Helper_Cart
{
    protected $removedMovedItems;
    /**
     * A new customer is created as a prospect customer
     * - The customer should not be login (OMNVFDE-937)
     * - Method moved from CartController (for multiple usage)
     * @param $vars
     * @return Dyna_Customer_Model_Customer
     */
    public function createDeProspectFromCart($vars)
    {
        /** @var Dyna_Customer_Model_Customer $customer */
        $customer = $this->getCustomerModel();

        if ($vars['guestCustomerIsSoho']) {
            $customer->setCompanyName($vars['companyname']);
        }

        return $this->setCustomerData($vars);
    }

    protected function getCustomerModel()
    {
        return Mage::getModel('customer/customer');
    }

    protected function setCustomerData($vars)
    {
        $customer = $this->getCustomerModel();
        $customer->setFirstname(isset($vars['firstname']) ? $vars['firstname'] : '')
            ->setLastname(isset($vars['lastname']) ? $vars['lastname'] : '')
            ->setAdditionalEmail(isset($vars['email_send']) ? $vars['email_send'] : '')
            ->setAdditionalTelephone(isset($vars['additional_telephone']) ? $customer::COUNTRY_CODE . $vars['additional_telephone'] : '')
            ->setZiggoTelephone(isset($vars['ziggo_telephone']) ? $vars['ziggo_telephone'] : '')
            ->setZiggoEmail(isset($vars['ziggo_email']) ? $vars['ziggo_email'] : '')
            ->setPhoneNumber(isset($vars['telephone']) ? $vars['telephone'] : '')
            ->setEmail(uniqid() . Omnius_Customer_Model_Customer_Customer::DEFAULT_EMAIL_SUFFIX)
            ->setIsProspect(true)
            ->setIsBusiness((int)$vars['guestCustomerIsSoho'])
            ->setDob(isset($vars['dob']) ? $vars['dob'] : '')
            ->save();
        return $customer;
    }

    /**
     * Returns list of products that are not allowed based on current selection together with package totals
     * @param bool $simError
     * @param null $simName
     * @param null $packageStatus
     * @param null $allowedSim
     * @return array
     */
    public function returnCombinations($simError = false, $simName = null, $packageStatus = null, $allowedSim = null)
    {
        /** @var Dyna_Checkout_Model_Sales_Quote_Item $el */
        $closure = function ($result, $el) {
            $result[(int)$el->getProductId()] = [
                'product_id' => (int)$el->getProductId(),
                'type' => strtolower($el->getProduct()->getTypeText()),
                'mandatory' => (bool)$el->getIsObligated(),
                'isSuso' => false,
                'isGiven' => $el->getIsDefaulted(),
                'isDefaulted' => $el->getIsDefaulted(),
                'isContract' => $el->getIsContract()
            ];
            return $result;
        };

        $quoteItems = $this->_getQuote()->getPackageItems($this->_getQuote()->getActivePackageId());
        $cartProducts = array_reduce($quoteItems, $closure, array());
        $processContext = $this->getContextHelper()->getProcessContextCode();

        $campaignProducts = [];
        if ($uctParams = Mage::getSingleton('customer/session')->getUctParams()) {
            $campaignProducts = Mage::helper('bundles')->getCampaignProducts($uctParams['campaignId']);
        }

        $flippedCampaignProducts = array_flip($campaignProducts);
        foreach ($cartProducts as &$cartProduct) {
            $cartProductId = $cartProduct['product_id'];
            if (isset($flippedCampaignProducts[$cartProductId])) {
                $cartProduct['isCampaign'] = 1;
            } else {
                $cartProduct['isCampaign'] = 0;
            }
        }

        // If an installed base product was removed from cart, we should still show the `CONTRACT` label in configurator
        if($processContext != Dyna_Catalog_Model_ProcessContext::ACQ) {
            $alternateItems = array_merge($this->_getQuote()->getAlternateItems(),
                $this->_getQuote()->getAlternateItems(null, false, false));
            if (count($alternateItems)) {
                foreach ($cartProducts as &$cartProduct) {
                    foreach ($alternateItems as $alternateItem) {
                        if ($cartProduct['product_id'] == $alternateItem['product_id']) {
                            $cartProduct['isContract'] = 1;
                        }
                    }
                }
            }
        }

        // From this point models can be cached (cart products must be retrieved before activating the freeze_models flag)
        Mage::register('freeze_models', true, true);
        $totalsUpdated = Mage::helper('omnius_configurator')->getActivePackageTotals();
        $totals = Mage::getSingleton('core/layout')->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/totals.phtml')->toHtml();
        $packageId = $this->_getQuote()->getActivePackageId();
        $package = $this->_getQuote()->getPackageById($packageId);
        $promoRules = Mage::helper('pricerules')->findApplicablePromoRules(true, $packageId);
        $promoCounter = Mage::getSingleton('core/session')->getPromoCounter();
        $promoCounter[$packageId] = $promoRules;
        Mage::getSingleton('core/session')->setPromoCounter($promoCounter);

        try {
            $rightBlock = Mage::getSingleton('core/layout')->createBlock('checkout/cart_totals')
                ->setTemplate('checkout/cart/partials/package_block.phtml')
                ->setData([
                    'guiSection' => Dyna_Catalog_Model_Product::GUI_CART,
                    'countPromoRules' => $promoRules
                ])
                ->toHtml();
        }
        catch (\Throwable $exception) {
            ob_clean();
            $rightBlock = null;
        }

        if (!isset($cartProducts[$this->_clickedProductId]) &&
            $this->_clickedItemSelected === false &&
            $this->_clickedItemDefaulted === true &&
            $this->_clickedProductSku
        ) {
            $refusedDefaultedItems = $package->getRefusedDefaultedItems() ? explode(',', $package->getRefusedDefaultedItems()) : [];
            if (!in_array($this->_clickedProductSku, $refusedDefaultedItems)) {
                $refusedDefaultedItems[] = $this->_clickedProductSku;
            }

            //only update refused items if there is any change
            if($package->getRefusedDefaultedItems() != implode(',', $refusedDefaultedItems)) {
                $package->setRefusedDefaultedItems(implode(',', $refusedDefaultedItems))->save();
            }
        }

        $hintMessages = Mage::getSingleton('customer/session')->getHints() ?: array();
        $hintMessages = array_filter($hintMessages, function($hint) use ($packageId) {
            return $hint['packageId'] == $packageId;
        });

        $response = array(
            'error' => false,
            'activePackageId' => (int)$packageId,
            'totalMaf' => $totalsUpdated['totalmaf'],
            'totalPrice' => $totalsUpdated['totalprice'],
            'totals' => $totals,
            'rightBlock' => $rightBlock,
            'promoRules' => $promoRules,
            'complete' => Mage::helper('dyna_checkout')->hasAllPackagesCompleted(),
            'quoteHash' => Mage::helper('dyna_checkout/cart')->getQuoteProductsHash(),
            'processContextCode' => $processContext,
            'hintMessages' => $hintMessages,
            'packageEntityId' => (int)$package->getEntityId(),
            'salesId' => Mage::getSingleton('dyna_checkout/cart')->getQuote()->getSalesId(),
            'drc_eligible' => $this->_getQuote()->canShowDeviceRC()
        );

        if (Mage::helper('bundles')->isBundleChoiceFlow()) {
            $bundleChoiceBlock = Mage::getSingleton('core/layout')->createBlock('dyna_bundles/options')->setTemplate('bundles/options.phtml')->setCurrentPackageId($packageId)->toHtml();
            $response['bundleChoiceBlock'] = $bundleChoiceBlock;
        } else {
            $response['removeBundleChoiceBlock'] = 1;
        }

        if ($packageStatus === null) {
            $packageStatus = Mage::helper('omnius_checkout')->checkPackageStatus($packageId);
        }
        if (($incompleteSections = $packageStatus) && is_array($incompleteSections)) {
            $response['incomplete_sections'] = $incompleteSections;
        }

        // Removed default not needed anymore to be sent to frontend

        $response['cartProducts'] = $cartProducts;

        if ($simError) {
            $response['simError'] = $simError;
        } else {
            $response['sim'] = $simName;
        }

        if ($allowedSim) {
            $response['allowedSimcards'] = $allowedSim;
        }

        if (Mage::helper('bundles')->hasMandatoryFamiliesCompleted()) {
            $response['removeBundleErrorNotice'] = 1;
        }

        $productModel = Mage::getSingleton('catalog/product');
        $warningDefaulted = '';
        $productNames = [];
        $message = 'The following product(s): %s cannot be added because they are incompatible with the current selection';
        foreach ($this->_incompatibleDefaultedProducts as $productId) {
            $productNames[] = $productModel->load($productId)->getDisplayNameConfigurator() ?: $productModel->load($productId)->getName();
        }
        if (!empty($productNames)) {
            $warningDefaulted .= htmlspecialchars(Mage::helper('dyna_configurator')->__($message, implode(', ', $productNames)));
        }

        if ($warningDefaulted != '') {
            $logMessage = sprintf(
                "\n!!!=======================!!!\n"
                . "Quote ID: %s\n%s\n"
                . "!!!=======================!!!\n",
                $this->_getQuote()->getId(),
                $warningDefaulted
            );

            /**
             * Log defaulted products that don't have allowed rules with the current cart to help debugging
             */
            Mage::log($logMessage, Zend_Log::DEBUG, 'defaulted_missing_product_allowed_rules.log', true);
            $response['warningDefaulted'] = $warningDefaulted;
        }

        return $response;
    }

    /**
     * @param $finalProducts
     * @param $allExclusiveFamilyCategoryIds
     * @param $exclusiveFamilyCategoryIds
     * @param $packageType
     * @param $arrayToAdd
     * @param $addedDefaultItems
     */
    protected function addDefaultedProducts(&$finalProducts, &$allExclusiveFamilyCategoryIds, &$exclusiveFamilyCategoryIds, $arrayToAdd, &$addedDefaultItems, $productsWithFlags = [])
    {
        if ($arrayToAdd) {
            $this->getExtraLogHelper()->extraLog("Initial array to add: ");
            $this->getExtraLogHelper()->extraLog($arrayToAdd);
        }

        $isServiceDefaulted = false;
        if (isset($arrayToAdd['service'])) {
            unset($arrayToAdd['service']);
            $isServiceDefaulted = true;
        }

        $currentWebsite = Mage::app()->getWebsite()->getId();
        /** @var Omnius_Catalog_Model_Resource_Product_Collection $defaultedCollection */
        $defaultedCollection = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('footnote_id')
            ->addAttributeToSelect('status')
            ->addWebsiteFilter($currentWebsite)
            ->addIdFilter($arrayToAdd);

        $quote = $this->_getQuote();
        foreach ($arrayToAdd as $itemToAdd) {
            $product = $defaultedCollection->getItemById($itemToAdd);
            if (!$product) {
                // Skip products that don't exist
                $this->getExtraLogHelper()->extraLog("Skipping as product doesn't exist: " . $itemToAdd);
                continue;
            }

            //skip items that are added as defaulted but has status enabled
            if ($product->getStatus() != Mage_Catalog_Model_Product_Status::STATUS_ENABLED) {
                $this->getExtraLogHelper()->extraLog("Skipping as product has status disabled: " . $itemToAdd);
                continue;
            }

            if (!$product->getFootnoteId()) {
                // Product is part of a mutually exclusive category
                if ($categoriesExclusive = array_intersect($product->getCategoryIds(), $allExclusiveFamilyCategoryIds)) {
                    if (array_intersect($product->getCategoryIds(), $exclusiveFamilyCategoryIds)) {
                        $this->getExtraLogHelper()->extraLog("Skipping from exclusive category: " . $itemToAdd);
                        // There is already a product in this mutually exclusive category
                        continue;
                    } else {
                        // Add the categories to the list so we make sure the next product is not in them
                        foreach ($categoriesExclusive as $categoryExclusive) {
                            $exclusiveFamilyCategoryIds[$product->getId()] = $categoryExclusive;
                        }
                    }
                }
            }

            $newItem = $this->addProductToCart($itemToAdd, true, $isServiceDefaulted, $productsWithFlags[$itemToAdd]);
            if ($newItem) {
                $addedDefaultItems[] = $newItem;
                // Add the product to the list to make sure the next ones are also compatible with it
                array_push($finalProducts, $itemToAdd);
            }

        }

        Mage::dispatchEvent('current_quote_content_changed', array('quote' => $quote));
    }

    /**
     * Method that performs adding product to cart
     * @param $data
     * @return array|mixed
     * @throws Exception
     */
    public function processAddMulti($data)
    {
        // package type
        $type = $data['type'];
        // section in the configurator (product type)
        $section = $data['section'];
        // current package id
        $packageId = $data['packageId'];
        // products from the current section
        $products = $data['products'] ?? [];

        /** @var Dyna_Package_Model_Package $packageModel */
        $packageModel = $this->_getQuote()->getCartPackage($packageId);

        $this->_section = $section;
        // product that was clicked in the configurator (selected or deselected)
        $this->_clickedProductId = $data['clickedItemId'] ?? null;
        // flag to know if the product was added or removed
        $this->_clickedItemSelected = $data['clickedItemSelected'] && ($data['clickedItemSelected'] === 'true');
        /**Handle address data(contains json object {"serviceability":..., "validate" :...}*/
        if (isset($data['addressData'])) {
            $addressData = json_decode($data['addressData'], true);
        } else {
            $addressData["serviceability"] = json_decode(Mage::getSingleton('dyna_address/storage')->getData('addressData'), true);
        }

        $sim = $data['simType'] ?? null;
        $oldSim = $data['oldSim'] ?? null;

        $this->_getQuote()->setCurrentStep(null)
            ->saveActivePackageId($packageId);
        // Setting package status OPEN, it will be evaluated on expanded cart since OMNVFDE-3896
        if($packageModel->getCurrentStatus() != Omnius_Configurator_Model_AttributeSet::PACKAGE_STATUS_OPEN) {
            $packageModel->setCurrentStatus(Omnius_Configurator_Model_AttributeSet::PACKAGE_STATUS_OPEN);
        }

        $removedDefaultFlag = false;

        // Retrieve all the exclusive families
        $allExclusiveFamilyCategoryIds = Mage::getModel('catalog/category')->getAllExclusiveFamilyCategoryIds();

        $this->checkCampaignOfferData($packageModel, $data, $packageId, "register");

        $allowedSimCards = [];
        //get allowed simcards for the selected tariff.
        if($section == strtolower(Dyna_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION) && !$sim) {
            $allowedSimCards = Mage::helper('vznl_configurator/attribute')->getSimCardTypes($products);
        }

        $isTariffBeingChanged = false;
        //if tariff is changed, remove IB products

        if($packageModel->isIlsSaleType() && Mage::getModel('catalog/product')->load($this->_clickedProductId)->isSubscription()){
            foreach($this->_getQuote()->getAllItems() as $quoteItem){
                if($quoteItem->getProduct()->isSubscription() && $quoteItem->getPreselectProduct()){
                    $isTariffBeingChanged = true;
                }
            }
        }

        if($isTariffBeingChanged){
            foreach($this->_getQuote()->getAllItems() as $item){
                if(!$item->getPreselectProduct() && $item->getIsDefaulted()){
                    $this->_getQuote()->removeItem($item->getId());
                }

            }
        }


        // Create the new package for the quote or update if already created
        if (!$packageModel || !$packageModel->getId()) {
            $packageModel = $packageModel ?: Mage::getModel('package/package');
            $packageModel->setQuoteId($this->_getQuote()->getId())->setPackageId($packageId)->setType($type);
        }
        // Update old_sim attribute to the current package if it is a retention package or if it is HN subscription
        if ($packageModel->isRetention() || $packageModel->hasHollandsNieuwe($this->_getQuote())) {
            $oldSimValue = ($oldSim == 'true') ? 1 : 0;
            $packageModel->setOldSim($oldSimValue);
        }

        $packageModel->updateUseCaseIndication();

        $added = [];
        $hadSubscription = false;
        $allItems = $this->_getQuote()->getCartPackage()->getAllItems();
        $persistedProducts = [];
        $productsInCart = $appliedRulesArray = [];

        $deletedItems = [];
        $removedDefault = [];
        // It will be set to true if it had one and it gets removed
        $notDefaulted = array();
        $subProduct = null;

        try {
            $bundleBrokenFlag = false;
            $products = array_filter(
                array_filter(
                    is_array($products) ? $products : array(), 'trim'
                )
            );
            $flippedProducts = array_flip($products);
            $removeDeviceRC = ($section == strtolower(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE) || $section == strtolower(Dyna_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION) ? true : false);
            /** @var Dyna_Checkout_Model_Sales_Quote_Item $item */
            foreach ($allItems as $item) {

                if (empty($products)) { //clear DeviceRC
                    if ($item->getPackageId() == $packageId && !$item->isDeleted()) {
                        if ($item->getProduct()->is(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE_SUBSCRIPTION) && $removeDeviceRC) {
                            $this->_getCart()->removeItem($item->getId());
                        }
                    }
                }

                if (($this->_clickedProductId && $item->getProductId() == $this->_clickedProductId)
                    && (($item->getIsDefaulted() || $item->getIsServiceDefaulted()) && $item->getProduct()->isOption())
                ) {
                    $this->_clickedProductSku = $item->getSku();
                    $this->_clickedItemDefaulted = true;
                }

                if (isset($deletedItems[$item->getId()])) {
                    continue;
                }

                $productsInCart[$item->getId()] = $item->getProductId();

                if ($item->getProduct()->isSubscription()) {
                    $hadSubscription = true;
                    $subProduct = $item->getProduct();
                }

                if (($categoriesExclusive = array_intersect($item->getProduct()->getCategoryIds(), $allExclusiveFamilyCategoryIds))
                    && ($item->getPackageId() == $packageId)
                ) {
                    foreach ($categoriesExclusive as $categoryExclusive) {
                        $this->_exclusiveFamilyCategoryIds[$item->getProductId()] = $categoryExclusive;
                    }
                }

                // check if offer addon is added if other exist in cart and persist them along with the offer one
                if ($data['isOffer'] && (strtolower($data['section']) == strtolower(Vznl_Catalog_Model_Type::SUBTYPE_ADDON) || strtolower($data['section']) == strtolower(Vznl_Catalog_Model_Type::FIXED_SUBTYPE_ADDON)) &&
                    (strtolower(reset($item->getProduct()->getType())) == strtolower(Vznl_Catalog_Model_Type::SUBTYPE_ADDON) || strtolower(reset($item->getProduct()->getType())) == strtolower(Vznl_Catalog_Model_Type::FIXED_SUBTYPE_ADDON))) {
                    $products[] = $item->getProductId();
                    $flippedProducts = array_flip($products);
                }

                if ($item->getPackageId() == $packageId && ($item->getProduct()->isInSection(strtolower($section)))) {
                    $itemProductId = $item->getProductId();
                    if (!isset($flippedProducts[$itemProductId])) {
                        $productsInCart = $this->removeItemAndParseDefault(
                            $item,
                            $productsInCart,
                            $packageModel,
                            $allItems,
                            $deletedItems,
                            $removedDefaultFlag,
                            $removedDefault, $type
                        );

                        if ($item->getBundleComponent()) {
                            $bundleBrokenFlag = $this->checkAndRemoveBundle($packageModel);
                            $this->removeContractBundleMarkers($packageModel);
                        }
                    } else {
                        $persistedProducts[] = $item->getProductId();
                    }
                    continue;
                }
            }
        } catch (Exception $e) {
            Mage::getSingleton('core/logger')->logException($e);
            return array(
                'error' => true,
                'message' => $this->__('Cannot add the item to shopping cart'),
            );
        }

        if ($this->emptyCartOnTariffChangeOrRemove($hadSubscription, $type, $allItems, $packageModel) || !$this->_getQuote()->getCartPackages() || (count($this->_getQuote()->getCartPackages()) > 0 && $bundleBrokenFlag)) {
            if (!$this->_getQuote()->getCartPackages() || (count($this->_getQuote()->getCartPackages()) > 0 && $bundleBrokenFlag)) {
                $products = [];
            }
            $productsInCart = [];
            $persistedProducts = [];
        }

        // If it had a subscription, and one of them was bundle component, break bundle
        /// @see OVG-2270
        $stillHasSubscription = false;
        if ($hadSubscription) {
            // Check whether or not subscription remains in cart
            foreach ($packageModel->getAllItems() as $item) {
                if ($item->getProduct()->isSubscription()) {
                    $stillHasSubscription = true;
                }
            }
            // If it is not in cart, break bundle
            if (!$stillHasSubscription) {
                $this->checkAndRemoveBundle($packageModel);
            }
        }

        // Reinitialising items with remaining quote items
        $allItems = $this->_getQuote()->getPackageItems($this->_getQuote()->getActivePackageId());
        foreach ($allItems as $item) {
            if (!$item->getIsDefaulted()) {
                $notDefaulted[] = $item->getProductId();
            }
        }

        $productsInCart = array_unique(array_merge($productsInCart, $products));
        $defaultedToAdd = $this->getDefaultedProducts($productsInCart, $packageModel);
        $finalDefaultedProducts = [];

        foreach ($defaultedToAdd as $key => $defaultedItemToAdd) {
            if ($key != 'service') {
                foreach ($defaultedItemToAdd as $staticDefaultToAdd) {
                    if (isset($staticDefaultToAdd['target_product_id'])) {
                        $finalDefaultedProducts[] = $staticDefaultToAdd['target_product_id'];
                    }
                }
            } else {
                $finalDefaultedProducts = array_merge($finalDefaultedProducts, array_keys($defaultedItemToAdd));
            }
        }
        $validateProducts = array_unique(array_merge($finalDefaultedProducts, $productsInCart));

        if (strtoupper($packageModel->getSaleType()) == Dyna_Catalog_Model_ProcessContext::MOVE) {
            Mage::register('move_process_initiated',$packageModel->getSaleType());
        }

        try {
            $this->validateProductsNL($validateProducts, $allItems, $packageId, $type);
        } catch (Exception $e) {
            // We need a new instance of the quote to get the actual items from DB
            $quoteId = $this->_getQuote()->getId();
            /** @var Vznl_Checkout_Model_Sales_Quote $quoteModel */
            $quoteModel = Mage::getModel('sales/quote')->load($quoteId);
            $quoteItems = $quoteModel->getPackageItems($packageId);

            Mage::logException($e);
            return array(
                'error' => true,
                'message' => $e->getMessage(),
                'cartProducts' => array_reduce($quoteItems, function ($result, $el) {
                    $result[] = (int)$el->getProductId();
                    return $result;
                }, array()),
            );
        }

        Mage::unregister('move_process_initiated');
        $products = array_diff($products, array_keys($removedDefault));

        $isAcqTariff = false;
        foreach ($packageModel->getAllItems() as $item) {
            if ($item->getProduct()->isSubscription() && !$item->getPreselectProduct()) {
                $isAcqTariff = true;
                break;
            }
        }

        if ($type == "fixed" && isset($this->_clickedProductId) && $this->_section !== strtolower(Vznl_Catalog_Model_Type::SUBTYPE_FIXED_PACKAGE)) {
            $finalProducts = array_unique(array_merge($persistedProducts, $added, $productsInCart));

            if (empty($added)) {
                $serviceDefaultedProducts = $this->parseServiceDefaultedProducts($notDefaulted, $packageModel, $finalProducts, $allExclusiveFamilyCategoryIds, $this->_exclusiveFamilyCategoryIds);
                $addedDefaultItems = $serviceDefaultedProducts;
            } else {
                /** @var Dyna_Checkout_Model_Sales_Quote_Item[] $addedDefaultItems */
                $addedDefaultItems = $this->parseDefaultedProducts($added, $packageModel, $finalProducts, $allExclusiveFamilyCategoryIds, $this->_exclusiveFamilyCategoryIds);
            }

            $addedDefaultItemsIds = [];
            foreach ($addedDefaultItems as $addedDefaultItem) {
                $addedDefaultItemsIds[] = $addedDefaultItem->getProductId();
            }

            $flippedPersistedProducts = array_flip($persistedProducts);
            foreach ($products as $productId) {
                try {
                    if (isset($flippedPersistedProducts[$productId])) {
                        continue;
                    }

                    $quoteItem = $this->parseProduct($packageId, $productId, $appliedRulesArray, $added);

                    // Changes source collection in Inlife if tariff is changed
                    if ((strtoupper($packageModel->getSaleType()) != Dyna_Catalog_Model_ProcessContext::ACQ) && ($isAcqTariff || $quoteItem->getProduct()->isSubscription())) {
                        //OMNVFNL-5267 Avoid "Mage registry key "rules_source_collection" already exists" exception
                        Mage::unregister('rules_source_collection');
                        if (Mage::helper('dyna_productmatchrule')->isDebugMode()) {
                            Mage::log("*********************************************************************", null, "ServiceExpressionRules.log");
                            Mage::log("****TARIFF CHANGE - CALLING CQ SOURCE COLLECTION******", null, "ServiceExpressionRules.log");
                            Mage::log("*********************************************************************", null, "ServiceExpressionRules.log");
                        }
                        Mage::register('rules_source_collection', Dyna_ProductMatchRule_Helper_Data::SOURCE_COLLECTION_CQ);
                    }



                    $infringingExclusiveCategoryProducts = [];
                    if (isset($this->_exclusiveFamilyCategoryIds[$productId])) {
                        $infringingExclusiveCategoryProducts = array_keys($this->_exclusiveFamilyCategoryIds, $this->_exclusiveFamilyCategoryIds[$productId]);
                    }

                    if (count($infringingExclusiveCategoryProducts) > 1) {
                        $toBeRemoved = array_shift(array_diff($infringingExclusiveCategoryProducts, [$productId]));
                        unset($this->_exclusiveFamilyCategoryIds[$toBeRemoved]);
                        $this->removeProductFromCart($toBeRemoved, $packageModel);
                    }

                } catch (RuntimeException $e) {
                    continue;
                } catch (Mage_Core_Exception $e) {
                    $messages = array_map(function ($el) {
                        return Mage::helper('core')->escapeHtml($el);
                    }, array_unique(explode("\n", $e->getMessage())));

                    $this->removeProductFromCart($added, $packageModel);
                    $this->_getCart()->save();
                    return array(
                        'error' => true,
                        'message' => join(', ', $messages),
                    );
                } catch (Exception $e) {
                    Mage::getSingleton('core/logger')->logException($e);

                    $this->removeProductFromCart($added, $packageModel);
                    $this->_getCart()->save();

                    return array(
                        'error' => true,
                        'message' => $this->__('Cannot add the item to shopping cart'),
                    );
                }
            }
        } else {
            $flippedPersistedProducts = $persistedProducts;
            foreach ($products as $productId) {
                try {
                    if (isset($flippedPersistedProducts[$productId])) {
                        continue;
                    }

                    $quoteItem = $this->parseProduct($packageId, $productId, $appliedRulesArray, $added);

                    if ($data['isOffer'] == 1 && $quoteItem->getProduct()->isSubscription()) {
                        $hadSubscription = true;
                        $subProduct = $quoteItem->getProduct();
                    }

                    // Changes source collection in Inlife if tariff is changed
                    if ((strtoupper($packageModel->getSaleType()) != Dyna_Catalog_Model_ProcessContext::ACQ) && ($isAcqTariff || $quoteItem->getProduct()->isSubscription())) {
                        //OMNVFNL-5267 Avoid "Mage registry key "rules_source_collection" already exists" exception
                        Mage::unregister('rules_source_collection');
                        if (Mage::helper('dyna_productmatchrule')->isDebugMode()) {
                            Mage::log("*********************************************************************", null, "ServiceExpressionRules.log");
                            Mage::log("****TARIFF CHANGE - CALLING CQ SOURCE COLLECTION******", null, "ServiceExpressionRules.log");
                            Mage::log("*********************************************************************", null, "ServiceExpressionRules.log");
                        }
                        Mage::register('rules_source_collection', Dyna_ProductMatchRule_Helper_Data::SOURCE_COLLECTION_CQ);
                    }

                    $infringingExclusiveCategoryProducts = [];
                    if (isset($this->_exclusiveFamilyCategoryIds[$productId])) {
                        $infringingExclusiveCategoryProducts = array_keys($this->_exclusiveFamilyCategoryIds, $this->_exclusiveFamilyCategoryIds[$productId]);
                    }

                    if (count($infringingExclusiveCategoryProducts) > 1) {
                        $toBeRemoved = array_shift(array_diff($infringingExclusiveCategoryProducts, [$productId]));
                        unset($this->_exclusiveFamilyCategoryIds[$toBeRemoved]);
                        $this->removeProductFromCart($toBeRemoved, $packageModel);
                    }

                } catch (RuntimeException $e) {
                    continue;
                } catch (Mage_Core_Exception $e) {
                    $messages = array_map(function ($el) {
                        return Mage::helper('core')->escapeHtml($el);
                    }, array_unique(explode("\n", $e->getMessage())));

                    $this->removeProductFromCart($added, $packageModel);
                    $this->_getCart()->save();
                    return array(
                        'error' => true,
                        'message' => join(', ', $messages),
                    );
                } catch (Exception $e) {
                    Mage::getSingleton('core/logger')->logException($e);

                    $this->removeProductFromCart($added, $packageModel);
                    $this->_getCart()->save();

                    return array(
                        'error' => true,
                        'message' => $this->__('Cannot add the item to shopping cart'),
                    );
                }
            }
            // Add sim if needed
            $simError = false;
            $simName = null;
            $package = $this->_getQuote()->getPackageById($packageId);

            if ($sim && $hadSubscription) {
                $simProd = Mage::getModel('catalog/product')->loadByAttribute('name', $sim);
                if ($simProd) {
                    $simT = $simProd->getAttributeText(Dyna_Catalog_Model_Product::DEFAULT_SIM_ATTRIBUTE);
                    if (Mage::helper('vznl_configurator/attribute')->checkSimTypeAvailable($subProduct, $simT)) {
                        try {
                            $sim = Mage::helper('vznl_configurator/attribute')->checkSubscriptionAllowsSim($subProduct, $sim);
                            $addedSim = $this->addSim($subProduct, $type, $simT, $sim);
                            $simName = $addedSim->getName();
                        } catch (Exception $e) {
                            // Exception already logged in addSim method
                            $simError = sprintf('%s %s: ', Mage::helper('vznl_configurator')->__('Simcard'), $sim);
                            $simError .= $e->getMessage();
                        }
                    }
                }
            } elseif ($hadSubscription && $oldSim != 'true' && ($sim = $package->hasHardwareWithoutSim($this->_getQuote()))) {
                try {
                    $addedSim = $this->addSim($subProduct, $type, $sim, null);
                    $simName = $addedSim->getName();
                } catch (Exception $e) {
                    // Exception already logged in addSim method
                    $simError = sprintf('%s %s: ', Mage::helper('vznl_configurator')->__('Simcard'), $sim);
                    $simError .= $e->getMessage();
                }
            }

            $finalProducts = array_unique(array_merge($persistedProducts, $added, $productsInCart));
            if (empty($added)) {
                $serviceDefaultedProducts = $this->parseServiceDefaultedProducts($notDefaulted, $packageModel, $finalProducts, $allExclusiveFamilyCategoryIds, $this->_exclusiveFamilyCategoryIds);
                $addedDefaultItems = $serviceDefaultedProducts;
            } else {
                /** @var Dyna_Checkout_Model_Sales_Quote_Item[] $addedDefaultItems */
                $addedDefaultItems = $this->parseDefaultedProducts($added, $packageModel, $finalProducts, $allExclusiveFamilyCategoryIds, $this->_exclusiveFamilyCategoryIds);
            }

            $addedDefaultItemsIds = [];
            foreach ($addedDefaultItems as $addedDefaultItem) {
                $addedDefaultItemsIds[] = $addedDefaultItem->getProductId();
            }
        }

        $this->_getCart()->save();

        $packageStatus = Mage::helper('dyna_checkout')->checkPackageStatus($packageId);
        $this->checkOfferPackage($packageModel, $finalProducts);
        $packageModel->updateDefaultedRemoved($removedDefaultFlag);

        $sessionSpecialOfferProducts = Mage::getSingleton('core/session')->getSpecialOfferPromoProducts();

        if ($data['isOffer'] && empty($sessionSpecialOfferProducts[$packageId])) {
            $this->checkCampaignOfferData($packageModel, $data, $packageId, "register");
        }

        $this->checkCampaignOfferData($packageModel, $data, $packageId, "check");
        $packageModel->save();

        return $this->returnCombinations($simError, $simName, $packageStatus, $allowedSimCards);
    }

    /**
     * Add the sim to the cart
     * @param $simType
     * @param $type
     * @return bool|Varien_Object
     * @throws Exception
     */
    public function addSim($subscription, $type, $simType, $simProd)
    {
        $sim = Mage::getModel('catalog/product')->getSimProduct($subscription, $simType, $simProd);
        try {
            /** @var Mage_Sales_Model_Quote_Item $item */
            foreach ($this->_getQuote()->getItemsCollection() as $item) {
                if (!$item->isDeleted()
                    && $item->getPackageId() == $this->_getQuote()->getActivePackageId()
                    && $item->getProduct()->isSim()
                ) {
                    $item->delete();
                }
            }
            $this->addProductToCart($sim->getId(), $type);
            return $sim;
        } catch (Exception $e) {
            Mage::getSingleton('core/logger')->logException($e);
            throw $e;
        }
    }

    /**
     * @param $productId
     * @param bool $isDefaulted
     * @param bool $isServiceDefaulted
     * @return bool|Dyna_Checkout_Model_Sales_Quote_Item
     * @param bool $saleType
     * @return bool
     */
    public function addProductToCart($productId, $isDefaulted = false, $isServiceDefaulted = false, $isObligated = false, $saleType = false)
    {
        $productId = (int)$productId;
        $websiteId = Mage::app()->getWebsite()->getId();
        if (($emulatedWebsiteId = Mage::helper('superorder')->shouldEmulateStore($websiteId)) != $websiteId) {
            // If a product from another shop should be added, emulate that store
            $storeId = Mage::app()->getWebsite($emulatedWebsiteId)->getDefaultStore()->getId();
        } else {
            $storeId = Mage::app()->getStore()->getId();
        }
        /** @var Vznl_Checkout_Model_Cart $cart */
        $cart = $this->_getCart();

        $numberCtns = count($this->_getCart()->getQuote()->getPackageCtns());
        $isOneOfDeal = Mage::helper('omnius_configurator/cart')->isOneOfDealActive();

        /** Mage_Sales_Model_Quote_Item $item */
        if (!($item = $this->exists($productId))) {
            if ($productId) {
                $product = Mage::getModel('catalog/product')
                    ->setStoreId($storeId)
                    ->load($productId);
                if (!$product->getId()) {
                    Mage::throwException('Invalid product ID given');
                }

                if ($product->getData('is_deleted')) {
                    // Skip products that are marked as "deleted" in backend
                    return Mage::getModel('sales/quote_item');
                }
            } else {
                Mage::throwException('Invalid product ID given');
            }
            $result = $cart->addProduct($product, array(
                    'qty' => $numberCtns ?: 1,
                    'one_of_deal' => ($isOneOfDeal && $numberCtns) ? 1 : 0
                )
            );

            if (is_string($result)) {
                return false;
            } else {
                $item = $result;
            }
        } else {
            $this->getExtraLogHelper()->extraLog("Skipping product: " . $productId);
        }

        // Make sure the item has the flag so we know when to delete it
        if ($item) {
            $action = null;
            $quoteItem = Mage::getModel('sales/quote_item')->getCollection()
                ->addFieldToFilter('quote_id', $item->getQuoteId())
                ->addFieldToFilter('sku', $item->getProduct()->getSku());

            if (!isset($quoteItem->getData()[0]) || !$quoteItem->getData()[0]['preselect_product']) {
                $action = Dyna_Checkout_Model_Sales_Quote_Item::SYSTEM_ACTION_ADD;
            } else {
                $action = Dyna_Checkout_Model_Sales_Quote_Item::SYSTEM_ACTION_EXISTING;
            }

            //if new item is being defaulted, check if sku is in IB to already set the preselect flag
            if(!$item->getId() && ($isDefaulted || $isServiceDefaulted) && $saleType != Dyna_Catalog_Model_ProcessContext::ACQ){
                $ibSkus = Mage::getSingleton('customer/session')->getAllInstalledBaseProductSkus();
                if(isset($ibSkus[$item->getSku()])){
                    $item->setPreselectProduct(1);
                }
            }

            if ($isDefaulted) {
                $item->setIsDefaulted(true);
                $item->setSystemAction($action);
            }
            if ($isServiceDefaulted) {
                $item->setIsServiceDefaulted(true);
                $item->setSystemAction($action);
            }
            if ($isObligated) {
                $item->setIsObligated($isObligated);
                $item->setSystemAction($action);
            }
            if ($saleType) {
                $item->setSaleType($saleType);
            }
        }

        return $item;
    }

    /**
     * Checks whether the product is eligible with the rest of the cart.
     *
     * @param $productsInCart
     * @param $allItems
     * @param $packageId
     * @param $type
     * @param bool $skipServiceRules
     * @throws Exception
     */
    protected function validateProductsNL(&$productsInCart, $allItems, $packageId, $type)
    {
        $websiteId = Mage::app()->getWebsite()->getId();

        $validProducts = $this->getConfiguratorCartHelper()
            ->getCartRules($productsInCart, [], $websiteId, false, $type);
        $flippedValidProducts = array_flip($validProducts);
        $invalidProducts = [];
        $quoteId = null;
        /** @var Mage_Sales_Model_Quote_Item $item */
        foreach ($allItems as $item) {
            $quoteId = $item->getQuoteId();
            $itemProductId = $item->getProductId();
            if ($item->getPackageId() == $packageId) {
                if ($item->getPreselectProduct()
                    && !isset($flippedValidProducts[$itemProductId])
                    && strtolower($this->getContextHelper()->getProcessContextCode()) != strtolower(Dyna_Catalog_Model_ProcessContext::ACQ)
                ) {
                    $item->isDeleted(true);
                    $productsInCart = array_diff($productsInCart, [$item->getProductId()]);
                } elseif ($item->getProduct()->isSim()
                    || (!$item->getProduct()->getInitialSelectable()
                        && !$item->getBundleChoice()
                        && empty(array_intersect($validProducts, [$item->getProductId()]))
                    )
                ) {
                    // Option of option logic
                    // Remove products that are not initially selectable and are not found in allowed rules
                    $item->isDeleted(true);
                    $productsInCart = array_diff($productsInCart, [$item->getProductId()]);
                } elseif ($item->getProduct()->getType() && !$item->getProduct()->is(Omnius_Catalog_Model_Type::SUBTYPE_NONE)) {
                    if (isset($flippedValidProducts[$itemProductId])
                        || $item->getProduct()->isServiceItem()
                    ) {
                        continue;
                    }
                    $invalidProducts[] = $item->getProduct()->getSku();
                }
            }
        }
        /**
         * Try and make the error message more explicit by pointing to the exact items that don't have the needed rules
         */
        if (count($invalidProducts)) {
            $adapter = Mage::getSingleton('core/resource')->getConnection('core_read');
            if (!empty($validProducts)) {
                $validProductSkus = array_column($adapter
                    ->fetchAll('SELECT sku FROM catalog_product_entity WHERE entity_id IN (' . implode(', ', array_filter(array_map('intval', $validProducts))) . ')'),
                    'sku');
            } else {
                $validProductSkus = ["No available skus"];
            }

            if ($productsInCart) {
                $cartProducts = array_column($adapter
                    ->fetchAll('SELECT sku FROM catalog_product_entity WHERE entity_id IN (' . implode(', ', array_filter(array_map('intval', $productsInCart))) . ')'),
                    'sku');
            } else {
                $cartProducts = ["No products in cart"];
            }

            $logMessage = sprintf(
                "\n!!!=======================!!!\n"
                . "Quote ID: %s\nProducts in cart: %s\nInvalid: %s\nAllowed: %s\n"
                . "!!!=======================!!!\n",
                $quoteId,
                join(', ', $cartProducts),
                join(', ', $invalidProducts),
                join(', ', $validProductSkus)
            );

            /**
             * Log this info to help debugging
             */
            Mage::log($logMessage, Zend_Log::NOTICE, 'missing_product_allowed_rules.log', true);

            throw new Exception(sprintf(
                '%s.<br/><strong>%s:</strong> %s',
                $this->__("The following products cannot be added to the cart because they don't have allowed rules"),
                $this->__("Invalid"),
                join(', ', $invalidProducts)
            ));
        }
    }

    /**
     * Checks whether a product already exists in the current package of the shopping cart.
     * @param $productId
     * @return bool|Mage_Sales_Model_Quote_Item
     */

    protected function exists($productId)
    {
        /** @var Mage_Sales_Model_Quote_Item $quoteItem */
        foreach ($this->_getQuote()->getItemsCollection() as $quoteItem) {
            if ( ! $quoteItem->isDeleted()) {
                if ($quoteItem->getPackageId() == $this->_getQuote()->getActivePackageId()
                    && $quoteItem->getProductId() == $productId
                ) {
                    return $quoteItem;
                }
            }
        }
        return false;
    }

    /**
     * Return the cart helper
     * @return Vznl_Configurator_Helper_Cart|Mage_Core_Helper_Abstract
     */
    protected function getConfiguratorCartHelper()
    {
        if ($this->configuratorCartHelper === false) {
            $this->configuratorCartHelper = Mage::helper('vznl_configurator/cart');
        }

        return $this->configuratorCartHelper;
    }

    /**
     * Remove a certain bundle from cart
     * @param Dyna_Package_Model_Package $package
     * @throws Exception
     *
     * @return bool
     */
    protected function checkAndRemoveBundle(Dyna_Package_Model_Package $package)
    {
        $bundleBrokenFlag = false;
        // get gigakombi bundle for current package
        $bundle = $package->getBundleOfType(Vznl_Bundles_Model_BundleRule::TYPE_MOBILEFIXED);
        // if not a gigakombi bundle, try intrastack
        if ($bundle) {
            $bundleId = $bundle->getId();
            /** @var Dyna_Package_Helper_Data $packageHelper */
            $packageHelper = Mage::helper('dyna_package');
            /** @var Dyna_Bundles_Helper_Data $bundleHelper */
            $bundleHelper = Mage::helper('dyna_bundles');
            foreach ($package->getPackagesInSameBundle($bundleId) as $package) {
                $bundleHelper->removeBundlePackageEntry($bundleId, $package->getId());
                $package->clearCachedBundles();
                $packageHelper->removePackage($package->getPackageId(), null, $package->getId());
            }
            $bundleBrokenFlag = true;

            Mage::getSingleton('customer/session')->setBundleRuleParserResponse(null);
        }

        return $bundleBrokenFlag;
    }

    /**
     * Checks whether the product is eligible with the rest of the cart.
     *
     * @param array $productsInCart
     * @param array $allItems
     * @param string $packageId
     * @param string $type
     * @throws Exception
     */
    public function validateProducts(array &$productsInCart, array $allItems, string $packageId, string $type)
    {
        $this->validateProductsNL($productsInCart, $allItems, $packageId, $type);
    }

    public function checkCampaignOfferData($packageModel, $data, $packageId, $action)
    {
        //if method is triggered during addMulti the first time, set the package
        $request = Mage::app()->getRequest();
        if(($request->getParam('packages') > 0 || $request->getParam('products') > 0) && $request->getParam('isOffer') == 1 ){
            if($packageModel->getData('is_offer') != 1) {
                $packageModel->setData('is_offer', 1)->save();
            }
        }
        if ($packageModel->getData('is_offer') || !$data['isOffer']) {
            $packageItems = Mage::getSingleton('sales/quote_item')->getCollection()
                ->addFieldToFilter("quote_id", $packageModel->getData("quote_id"))
                ->addFieldToFilter("package_id", $packageId);

            if ($action == "register") {
                //if offer products are already identified for this package, skip the register part
                $offerSession = Mage::getSingleton('core/session')->getSpecialOfferPromoProducts();
                if(isset($offerSession[$packageId]['offerProducts'])){
                    return;
                }

                $offerAppliedRuleIds = [];
                foreach ($packageItems->getData() as $packageItem) {
                    if ($packageItem['applied_rule_ids'] != null || !empty($packageItem['applied_rule_ids'])) {
                        $offerAppliedRuleIds = array_unique(array_merge($offerAppliedRuleIds, explode(",", $packageItem['applied_rule_ids'])));
                    }
                }

                foreach ($offerAppliedRuleIds as $offerAppliedRule) {
                    $offerRule = Mage::getModel('salesrule/rule')->load($offerAppliedRule);

                    if (($offerRule->getData('service_source_expression') != null || !empty($offerRule->getData('service_source_expression'))) &&
                        (strpos($offerRule->getData('service_source_expression'), 'cart.package.isOfferPackage()') !== false) &&
                        ($offerRule->getData('promo_sku') != null || !empty($offerRule->getData('promo_sku')))
                    ) {
                        $offerProducts = $offerRule->getData('promo_sku');

                        $conditions = unserialize($offerRule->getData('conditions_serialized'));

                        foreach($conditions['conditions'][0]['conditions'] as $condition)
                        {
                            if($condition['attribute'] == 'sku')
                            {
                                $offerTargetProducts = array_map("trim", explode(',', $condition['value']));

                                break;
                            }
                        }
                    }
                }

                $offerSession[$packageId] = array("offerProducts" => $offerProducts ?? [], "packageId" => $packageId, 'offerTargetProducts' => $offerTargetProducts ?? []);
                //only set data on session if an offer was found
                if(!empty($offerProducts)) {
                    Mage::getSingleton('core/session')->setSpecialOfferPromoProducts($offerSession);
                }
            } else {
                $sessionPromoProducts = Mage::getSingleton('core/session')->getSpecialOfferPromoProducts();

                //if no products are available on session, no point in checking
                if(!$sessionPromoProducts[$packageId]){
                    return;
                }

                $offerProducts = $sessionPromoProducts[$packageId];

                $newPackageItems = [];
                foreach ($packageItems->getData() as $packageItem) {
                    if ($packageItem['sku'] != null || !empty($packageItem['sku'])) {
                        array_push($newPackageItems, $packageItem['sku']);
                    }
                }
                $holdOffer = 0;
                if (count($offerProducts['offerTargetProducts']) > 0) {
                    foreach ($offerProducts['offerTargetProducts'] as $targetProduct) {

                        if (in_array($targetProduct, $newPackageItems)) {
                            $holdOffer = 1;
                            break;
                        }
                    }
                    $offerProducts['offerProducts'] = array_map("trim", explode(',', $offerProducts['offerProducts']));
                    if (array_intersect($offerProducts['offerProducts'], $newPackageItems) && $holdOffer == 0) {
                        $holdOffer = 0;
                    } elseif (!array_intersect($offerProducts['offerProducts'], $newPackageItems) && $holdOffer == 1) {
                        $holdOffer = 0;
                    } elseif (array_intersect($offerProducts['offerProducts'], $newPackageItems) && $holdOffer == 1) {
                        $holdOffer = 1;
                    }
                } else {
                    if (!in_array($offerProducts['offerProducts'], $newPackageItems)) {
                        $holdOffer = 0;
                    } else {
                        $holdOffer = 1;
                    }

                }
                if($holdOffer == 0)
                {
                    if (count($sessionPromoProducts[$packageId]['offerProducts']) > 0) {
                        $quote_id = Mage::getSingleton('checkout/cart')->getQuote()->getId();
                        $quote = Mage::getModel('sales/quote')->load($quote_id);;
                        foreach ($quote->getAllItems() as $key => $item) {
                            if ($item->getSku() == $sessionPromoProducts[$packageId]['offerProducts']
                                && $item->getPackageId() == $packageId) {
                                $itemId = $item->getData('item_id');
                                $quote->removeItem($itemId);
                                Mage::helper('vznl_utility')->saveModel($quote);
                            }
                        }
                    }

                    unset($sessionPromoProducts[$packageId]);
                    Mage::getSingleton('core/session')->setSpecialOfferPromoProducts($sessionPromoProducts);
                }

                $packageModel->setData('is_offer', $holdOffer);
                if($action == 'save') {
                    $packageModel->save();
                }
            }
        }
    }

    public function validateCampaignOffer($packageId)
    {
        $packageModel = Mage::getSingleton('checkout/cart')->getQuote()->getCartPackage($packageId);
        Mage::helper('vznl_checkout/cart')->checkCampaignOfferData($packageModel, ['isOffer' => 0], $packageId, 'register');
        Mage::helper('vznl_checkout/cart')->checkCampaignOfferData($packageModel, ['isOffer' => 0], $packageId, 'save');
    }


    /**
     * Checks whether the product is eligible with the rest of the cart.
     *
     * @param array $productIds
     * @param array $allItems
     * @param string $packageId
     * @param string $packageType
     * @return array
     */
    public function validateMoveProducts($productIds, $allItems, $packageId, $packageType)
    {
        $removedProds = [];
        $initialProducts = $productIds;

        $initialServiceProds = $validServiceProductIds = $this->getServiceabilityValidProduct($productIds, $allItems, $packageId, $packageType);
        $serviceRemovedProducts = array_diff($initialProducts,$validServiceProductIds);

        $this->removedMovedItems = [];

        //while invalid products are found, keep removing them
        $currentCount = count($validServiceProductIds);
        while($currentCount){
            $tempCount = count($this->validateMoveProduct($validServiceProductIds, $allItems, $packageId, $packageType));
            if($tempCount == $currentCount){
                break;
            }
            $currentCount=$tempCount;
        }

        $compRemovedProducts = array_diff($initialServiceProds,$validServiceProductIds);

        $allItems = Mage::helper('dyna_configurator/cart')->getSortedCartProducts($allItems);
        foreach($allItems as $item) {
            $prod = $item->getProduct();
            $sku = $prod->getSku();
            if ($prod->isSubscription()) {
                $removedProds['packageName'] = $prod->getName();
            }
            if ($prod->getId() == $serviceRemovedProducts[$sku]) {
                $removedProds['serviceRules'][$sku] = $prod->getName();
            } elseif ($prod->getId() == $compRemovedProducts[$sku]) {
                $removedProds['productMatchRules'][$sku] = $prod->getName();
            }
        }
        return $removedProds;
    }

    public function validateMoveProduct(&$productsInCart, $allItems, $packageId, $type)
    {
        $websiteId = Mage::app()->getWebsite()->getId();
        $validProducts = $this->getConfiguratorCartHelper()
            ->getCartRules($productsInCart, [], $websiteId, false, $type);

        $flippedValidProducts = array_flip($validProducts);
        $quoteId = null;
        $allItems = Mage::helper('dyna_configurator/cart')->getSortedCartProducts($allItems);
        /** @var Mage_Sales_Model_Quote_Item $item */
        foreach ($allItems as $item) {
            if(in_array($item->getSku(), $this->removedMovedItems)){
                continue;
            }
            $quoteId = $item->getQuoteId();
            $itemProductId = $item->getProductId();
            if ($item->getPackageId() == $packageId) {
                if ($item->getPreselectProduct()
                    && !isset($flippedValidProducts[$itemProductId])
                    && strtolower($this->getContextHelper()->getProcessContextCode()) != strtolower(Dyna_Catalog_Model_ProcessContext::ACQ)
                ) {
                    $item->isDeleted(true);
                    $item->setSystemAction(Dyna_Checkout_Model_Sales_Quote_Item::SYSTEM_ACTION_REMOVE);
                    $this->removedMovedItems[]=$item->getSku();
                    $productsInCart = array_diff($productsInCart, [$item->getProductId()]);
                    break;
                } elseif ($item->getProduct()->isSim()
                    || (!$item->getProduct()->getInitialSelectable()
                        && !$item->getBundleChoice()
                        && empty(array_intersect($validProducts, [$item->getProductId()]))
                    )
                ) {
                    // Option of option logic
                    // Remove products that are not initially selectable and are not found in allowed rules
                    $item->setSystemAction(Dyna_Checkout_Model_Sales_Quote_Item::SYSTEM_ACTION_REMOVE);
                    $item->isDeleted(true);
                    $this->removedMovedItems[]=$item->getSku();
                    $productsInCart = array_diff($productsInCart, [$item->getProductId()]);
                    break;
                } elseif ($item->getProduct()->getType() && !$item->getProduct()->is(Omnius_Catalog_Model_Type::SUBTYPE_NONE)) {
                    if (isset($flippedValidProducts[$itemProductId])
                        || $item->getProduct()->isServiceItem()
                    ) {
                        continue;
                    }
                }
            }
        }

        return $productsInCart;
    }

    /**
     * Identify prodcuts which are still serviceable
     * @param $productIds
     * @param $allItems
     * @param $packageId
     * @param $packageType
     * @return array
     */
    public function getServiceabilityValidProduct($productIds, $allItems, $packageId, $packageType)
    {
        $products = [];
        $serviceProductId = [];

        foreach ($allItems as $item) {
            $productObject = $item->getProduct();
            if($productObject->getRequiresServiceability() && $productObject->getRequiredServiceabilityItems()) {
                $products[] = $productObject->toArray();
                $serviceProductId[$productObject->getSku()] = $productObject->getId();
            }
        }

        $catalogObject = Mage::getModel('dyna_configurator/expression_catalog', $packageType);
        $servicableProducts = $catalogObject->getServiceableProducts($products);

        $inValidServiceProduct = array_diff($serviceProductId,$servicableProducts);

        foreach ($allItems as $item) {
            $sku = $item->getProduct()->getSku();
            if(isset($inValidServiceProduct[$sku]) && $item->getPreselectProduct()) {
                $item->isDeleted(true);
                $item->setSystemAction('remove');
                $item->setIsDefaulted(true);
                $productIds = array_diff($productIds, [$item->getProductId()]);
            }
        }
        return $productIds;
    }

    /**
     * Identify if IB tariff/bundle is still eligible
     * @param array $tariffArray
     * @param $type
     * @param $tariffId
     * @return bool
     * @throws Mage_Core_Exception
     */
    public function validateSubscriptionProduct($tariffArray = [], $type, $tariffId)
    {
        $tariffProduct = [];
        $websiteId = Mage::app()->getWebsite()->getId();
        $validProducts = $this->getConfiguratorCartHelper()
            ->getCartRules($tariffArray, [], $websiteId, false, $type);

        $tariffProduct[] = Mage::getModel('catalog/product')->load($tariffId)->toArray();
        $catalogObject = Mage::getModel('dyna_configurator/expression_catalog', $type);
        $servicableProducts = $catalogObject->getServiceableProducts($tariffProduct,[],true);

        $isValidSubscription = in_array($tariffId,$validProducts) && in_array($tariffId,$servicableProducts);

        return $isValidSubscription;
    }
}
