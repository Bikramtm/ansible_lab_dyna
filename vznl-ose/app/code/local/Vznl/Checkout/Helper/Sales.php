<?php

/**
 * Class Sales
 */
class Vznl_Checkout_Helper_Sales extends Mage_Core_Helper_Data
{
    protected $options = [
        'page-size' => 'A4',
        'dpi' => 96, // Not changable in Linux
        'margin-bottom' => 40,
        'disable-smart-shrinking' => true,
        ];
    /**
     * @param Mage_Sales_Model_Order $order
     * @return Mage_Sales_Model_Quote
     */
    public function orderToQuote(Mage_Sales_Model_Order $order, $quote = null)
    {
        /** @var Mage_Sales_Model_Convert_Order $orderConverter */
        $orderConverter = Mage::getModel('sales/convert_order');

        if (!($quote instanceof Mage_Sales_Model_Quote)) {
            $quote = Mage::getModel('sales/quote');
        }
        $packagesArray = array();
        /** @var Mage_Sales_Model_Quote $quote */
        $quote = $orderConverter->toQuote($order, $quote);
        $quote->save();
        foreach ($order->getAllItems() as $orderItem) {
            /** @var Mage_Sales_Model_Quote_Item $quoteItem */
            $orderItem->setSystemAction(Dyna_Checkout_Model_Sales_Quote_Item::SYSTEM_ACTION_ADD);
            $quoteItem = $orderConverter->itemToQuoteItem($orderItem);
            $quoteItem->setQuote($quote);
            $quoteItem->setCustomPrice($orderItem->getData('row_total'));
            $quoteItem->setOriginalCustomPrice($orderItem->getData('row_total_incl_tax'));
            $packagesArray[] = $quoteItem->getPackageId();
            $quoteItem->save();
        }
        /** @var Mage_Sales_Model_Order_Address $shippingAddress */
        $shippingAddress = clone $order->getShippingAddress();
        /** @var Mage_Sales_Model_Order_Address $billingAddress */
        $billingAddress = clone $order->getBillingAddress();
        $shippingAddress->unsetData('entity_id');
        $shippingAddress->unsetData('parent_id');
        $billingAddress->unsetData('entity_id');
        $billingAddress->unsetData('parent_id');
        $shippingAddress = Mage::getModel('sales/quote_address')
            ->setData($shippingAddress->getData());
        $billingAddress = Mage::getModel('sales/quote_address')
            ->setData($billingAddress->getData());
        $quote->setShippingAddress($shippingAddress);
        $quote->setBillingAddress($billingAddress);
        $packagesArray = array_unique($packagesArray);

        $packages = Mage::getModel('package/package')->getCollection()
            ->addFieldToFilter('order_id', $order->getSuperorderId())
            ->addFieldToFilter('package_id', array('in' => $packagesArray));

        foreach ($packages as $pack) {
            Mage::getModel('package/package')
                ->setData($pack->getData())
                ->setEntityId(null)
                ->setQuoteId($quote->getId())
                ->setOrderId(null)
                ->save();
        }

        return $quote;
    }

    /**
     * @param Mage_Sales_Model_Quote $quote
     * @return Mage_Sales_Model_Order
     */
    public function quoteToOrder(Mage_Sales_Model_Quote $quote, $order = null, $includeShipping = false)
    {
        /** @var Mage_Sales_Model_Convert_Quote $quoteConverter */
        $quoteConverter = Mage::getModel('sales/convert_quote');

        /** @var Mage_Sales_Model_Order $order */
        if (!($order instanceof Mage_Sales_Model_Order)) {
            $order = Mage::getModel('sales/order');
        }
        //$order = $quoteConverter->toOrder($quote, $order);
        $order->setIncrementId($quote->getReservedOrderId())
            ->setStoreId($quote->getStoreId())
            ->setQuoteId($quote->getId())
            ->setQuote($quote)
            ->setCustomer($quote->getCustomer());

        Mage::helper('core')->copyFieldset('sales_convert_quote', 'to_order', $quote, $order);

        foreach ($quote->getAllItems() as $quoteItem) {
            /** @var Mage_Sales_Model_Order_Item $orderItem */
            $orderItem = $quoteConverter->itemToOrderItem($quoteItem);
            $orderItem->setOrder($order);
            $order->addItem($orderItem);
        }

        if ($includeShipping) {
            /** @var Mage_Sales_Model_Order_Address $shippingAddress */
            $shippingAddress = clone $quoteConverter->addressToOrderAddress($quote->getShippingAddress());
            $shippingAddress->unsetData('entity_id');
            $shippingAddress->unsetData('parent_id');
            $shippingAddress = Mage::getModel('sales/order_address')
                ->addData($shippingAddress->getData());
            $order->setShippingAddress($shippingAddress);
        }
        /** @var Mage_Sales_Model_Order_Address $billingAddress */
        $billingAddress = clone $quoteConverter->addressToOrderAddress($quote->getBillingAddress());

        $billingAddress->unsetData('entity_id');
        $billingAddress->unsetData('parent_id');

        $billingAddress = Mage::getModel('sales/order_address')
            ->addData($billingAddress->getData());
        $order->setBillingAddress($billingAddress);
        $order->setCustomerId($quote->getCustomerId());

        return $order;
    }

    public function handleCancelledPackages($cancelledPackages = null, $refundMethod = null, $refundReason = null, $returnChannel=null)
    {
        if (is_null($cancelledPackages)) {
            $cancelledPackages = Mage::helper('dyna_package')->getCancelledPackagesNow();
        }

        if (!$cancelledPackages) {
            return;
        }

        $customerSession = Mage::getSingleton('customer/session');
        $checkoutSession = Mage::getSingleton('checkout/session');
        $sessionQuoteId = $customerSession->getOrderEdit();
        $quote = Mage::getModel('sales/quote')->load($sessionQuoteId);
        $superOrder = Mage::getModel('superorder/superorder')->load($quote->getSuperOrderEditId());/** @var Vznl_Superorder_Model_Superorder $superOrder */
        $superOrderPackages = $superOrder->getPackages();
        $packages = array();
        $refundAmounts = array();
        $orderId = false;

        /**
         * loop through each cancelled package, check which Magento order it came from
         * If the order only had that package, set it to status cancelled, otherwise
         * create a new Magento order without that package
         */
        foreach ($cancelledPackages as $packageId) {
            foreach ($quote->getAllItems() as $item) {
                if ($item->getPackageId() == $packageId) {

                    /**
                     * Set hardware_change flag
                     */
                    if ($item->getProduct()->isRequiredTobeReturned()) {
                        $packageObj = $superOrderPackages->getItemByColumnValue('package_id', $packageId);
                        if ($packageObj && $packageObj->getId()) {
                            if ( ! $packageObj->isDelivered()) {
                                $packageObj->setData('complete_return', 1);
                            }
                            $packageObj->setData('hardware_change', true)
                                ->save();
                        }
                    }

                    $orderId = $item->getEditOrderId();
                } elseif (!in_array($item->getPackageId(), $packages)) {
                    $orderPackages[$item->getPackageId()] = $item->getEditOrderId();
                }
            }
            if (!$orderId) {
                throw new Exception($this->__('Package not found in cancel flow'));
            } else {
                $order = Mage::getModel('sales/order')->load($orderId);

                // Initial order already has been edited after an address change
                if ($order->getEdited() == 1) {
                    $newOrders = Mage::getModel('sales/order')->getCollection()
                        ->addFieldToFilter('parent_id', $orderId);

                    foreach ($newOrders as $tmpNewOrder) {
                        $tmpAllPackages = $tmpNewOrder->getPackageIds();
                        foreach ($tmpAllPackages as $tmpPackageId) {
                            if ($packageId == $tmpPackageId) {
                                $orderId = $tmpNewOrder->getId();
                            } else {
                                $orderPackages[] = $tmpNewOrder->getId();
                            }
                        }
                    }
                }

                $order = Mage::getModel('sales/order')->load($orderId);

                if ($cancelledDoaItems = $checkoutSession->getCancelDoaItems()) {
                    if (isset($cancelledDoaItems[$packageId])) {
                        $sessionDoaItems = unserialize($cancelledDoaItems[$packageId]);
                        $flippedSessionDoaItems = array_flip($sessionDoaItems);
                        foreach ($order->getAllItems() as $item) {
                            $itemSku = $item->getSku();
                            if ($item->getPackageId() == $packageId) {
                                if (isset($flippedSessionDoaItems[$itemSku])) {
                                    // Save DOA flag to order item
                                    $item->setItemDoa(true)
                                        ->save();
                                }
                            }
                        }
                    }
                }

                $orderHasOtherPackages = isset($orderPackages) && in_array($orderId, $orderPackages);
                //Includes the deviceRC amounts. Special case for Aikido where the refund payment node on cancel has to have the full amount.
                $packageTotalsPayments = Mage::helper('dyna_configurator')->calculateRefundAmounts(
                    $quote->getId(),
                    $packageId
                );

                $packageTotalsRefund = Mage::helper('dyna_configurator')->getActivePackageTotals(
                    $packageId,
                    true,
                    $quote->getId()
                );

                $refundAmounts['payments'][$packageId] = $packageTotalsPayments;
                $refundAmounts['refundAmount'][$packageId] = $packageTotalsRefund['totalprice'];
                $packages[] = $packageId;

                //also update package status and refund method if found
                $packageModel = Mage::getModel('package/package')
                    ->getCollection()
                    ->addFieldToFilter('package_id', $packageId)
                    ->addFieldToFilter('order_id', $order->getSuperorderId())
                    ->getFirstItem();
                if ($packageModel->getId()) {
                    // Set refund method and reason only for delivered packages (skip packages that are already returned)
                    if ($packageModel->isDelivered() && !$packageModel->getReturnedBy()) {
                        $returnChannel = $packageModel->getReturnChannel() != null ? $packageModel->getReturnChannel() : $returnChannel;
                        $packageModel->setRefundMethod($refundMethod)
                            ->setReturnChannel($returnChannel)
                            ->setRefundReason($refundReason[$packageId])
                            ->setReturnedBy($customerSession->getAgent(true)->getUsername())
                            ->setCancellationDate(date('d-m-Y'))
                            ->save();
                    }
                }

                //if old order was edited, skip it as it was already handled
                if ($order->getEdited() == 1 || strtolower($order->getStatus()) == strtolower(Vznl_Checkout_Model_Sales_Order::STATUS_CANCELLED)) {
                    continue;
                }
                if ($orderHasOtherPackages) {
                    $salesConverter = Mage::helper('vznl_checkout/sales');
                    $newQuote = $salesConverter->orderToQuote($order); //new quote from found order

                    $oldOrderEmptied = true;
                    $editedPackagesArray = array();
                    $flippedCancelledPackages = array_flip($cancelledPackages);
                    foreach ($newQuote->getAllItems() as $item) {
                        $itemPackageId = $item->getPackageId();
                        if (isset($flippedCancelledPackages[$itemPackageId])) {
                            $editedPackagesArray[] = $item->getPackageId();
                            $item->isDeleted(true);
                        } else {
                            $oldOrderEmptied = false;
                        }
                    }

                    Mage::getModel('package/package')->deletePackages($newQuote->getId(), $editedPackagesArray);
                    if (!$oldOrderEmptied) {
                        $newOrder = Mage::getModel('sales/order');
                        $payment = clone $order->getPayment();
                        $payment->unsetData('entity_id');
                        $newOrder->setPayment($payment);
                        $newOrder->addStatusHistoryComment($order->getStatus(), '[Notice] - Order edited');
                        $newOrder->setData($order->getData());
                        $newOrder->unsetData('entity_id');
                        $newOrder->unsetData('increment_id');
                        $newOrder->setParentId($order->getId());
                        $newOrder->setSuperorderId($order->getSuperorderId());
                        //Need to collect totals
                        $newQuote->setIsActive(0);
                        $newQuote->setSuperQuoteId($sessionQuoteId);
                        $newQuote->setTotalsCollectedFlag(false)->collectTotals()->save();
                        $newOrder = $salesConverter->quoteToOrder($newQuote, $newOrder);

                        $address = Mage::getModel('sales/order_address');
                        $address->setData($newQuote->getShippingAddress()->getData());
                        $address->unsetData('entity_id');
                        $address->unsetData('parent_id');
                        $address->setStreet($address->getStreet());
                        $newOrder->setShippingAddress($address);
                        $newOrder->setEdited(0);
                        $newOrder->save();
                        $order->setEditedOrder(1);
                    }
                }
            }

            $order = Mage::getModel('sales/order')->load($orderId);

            if (!$orderHasOtherPackages || $oldOrderEmptied) {
                $order->setStatus(Vznl_Checkout_Model_Sales_Order::STATUS_CANCELLED)->save();
            } elseif ($orderHasOtherPackages && ! $oldOrderEmptied) {
                // set a flag that is used to determine if approveOrder call should be made or not
                Mage::register('order_has_other_packages', $orderHasOtherPackages);
            }
        }

        $superOrder = Mage::getModel('superorder/superorder')->load($quote->getSuperOrderEditId());

        $checkoutSession->setData('superorder_id', $superOrder->getId());

        return $refundAmounts;
    }

    /**
     * Generate contract content
     *
     * @param Vznl_Checkout_Model_Sales_Order $deliveryOrder
     * @param bool $fromShell
     * @param bool $displayLogo
     * @return string
     */
    public function generateContract(Vznl_Checkout_Model_Sales_Order $deliveryOrder, bool $fromShell = false, bool $displayLogo = true)
    {
        if ($fromShell) {
            Mage::helper('vznl_checkout/email')->setCurrentPackageTheme();
        }

        $header = Mage::app()->getLayout()
            ->createBlock('vznl_checkout/pdf_contract')
            ->setTemplate('checkout/cart/pdf/contract/header.phtml')
            ->init($deliveryOrder)
            ->setLogo($displayLogo)
            ->toHtml();
        $body = Mage::app()->getLayout()
            ->createBlock('vznl_checkout/pdf_contract')
            ->setTemplate('checkout/cart/pdf/contract.phtml')
            ->init($deliveryOrder)
            ->toHtml();
        $footer = Mage::app()->getLayout()
            ->createBlock('vznl_checkout/pdf_contract')
            ->setTemplate('checkout/cart/pdf/contract/footer.phtml')
            ->init($deliveryOrder)
            ->toHtml();

        $options = [
            'margin-left' => 0,
            'margin-right' => 0,
            'margin-top' => 42,
            'header-html' => $header,
            'footer-html' => $footer,
            'header-spacing' => 42,
            'user-style-sheet' => Mage::getBaseDir() .'/skin/frontend/vznl/default/css/contract.css',
        ];

        $options = array_merge($this->options, $options);

        try {
            $pdfInstance = new OmniusSnappyPdf($options);
            return $pdfInstance->getOutputFromHtml($body);
        } catch (Exception $e) {
            Mage::logException($e);
        }
    }

    /**
     * Generate Fixed contract content
     *
     * @param Vznl_Checkout_Model_Sales_Order $deliveryOrder
     * @param bool $fromShell
     * @param bool $displayLogo
     * @return string
     */
    public function generateFixedContract(Vznl_Checkout_Model_Sales_Order $deliveryOrder, bool $fromShell = false, bool $displayLogo = true)
    {
        if ($fromShell) {
            Mage::helper('vznl_checkout/email')->setCurrentPackageTheme();
        }

        $header = Mage::app()->getLayout()
            ->createBlock('vznl_checkout/pdf_contract')
            ->setTemplate('checkout/cart/pdf/contract/fixed/header.phtml')
            ->init($deliveryOrder)
            ->setLogo($displayLogo)
            ->toHtml();
        $body = Mage::app()->getLayout()
            ->createBlock('vznl_checkout/pdf_contract')
            ->setTemplate('checkout/cart/pdf/contract/fixed/contract.phtml')
            ->init($deliveryOrder)
            ->toHtml();
        $footer = Mage::app()->getLayout()
            ->createBlock('vznl_checkout/pdf_contract')
            ->setTemplate('checkout/cart/pdf/contract/fixed/footer.phtml')
            ->init($deliveryOrder)
            ->toHtml();

        $options = [
            'margin-left' => 12,
            'margin-right' => 12,
            'margin-top' => 40,
            'header-html' => $header,
            'footer-html' => $footer,
            'header-spacing' => 10,
            'user-style-sheet' => Mage::getBaseDir() .'/skin/frontend/vznl/default/css/fixed-contract.css',
        ];

        $options = array_merge($this->options, $options);

        try {
            $pdfInstance = new OmniusSnappyPdf($options);
            return $pdfInstance->getOutputFromHtml($body);
        } catch (Exception $e) {
            Mage::logException($e);
        }
    }
}
