<?php

require_once Mage::getModuleDir('controllers', 'Mage_Checkout') . DS . 'CartController.php';

/**
 * Class Vznl_Checkout_Checkout_CartController
 */
class Vznl_Checkout_CartController extends Mage_Checkout_CartController
{
    const PEAL_CUSTOMER_TYPE_PRIVATE = "RESIDENTIAL";
    const PEAL_CUSTOMER_TYPE_BUSINESS = "BUSINESS";
    const FIXED_NUMBER_RELEASE = "Release";
    const FIXED_NUMBER_RESERVE = "Reserve";

    private $dynaServiceHelper = null;

    /**
     * @return Vznl_Core_Helper_Service
     */
    private function getServiceHelper()
    {
        if ($this->dynaServiceHelper === null) {
            $this->dynaServiceHelper = Mage::helper('omnius_service');
        }

        return $this->dynaServiceHelper;
    }

    /**
     * Retrieve checkout session model
     *
     * @return Mage_Checkout_Model_Session
     */
    public function getCheckoutSession()
    {
        return Mage::getSingleton('checkout/session');
    }

    protected function _getQuote()
    {
        if (Mage::getSingleton('customer/session')->getOrderEdit()) {
            $quote = Mage::getModel('sales/quote')->load(Mage::getSingleton('customer/session')->getOrderEdit());
            if ($quote->getId()) {
                //$this->getCheckoutSession()->setLoadInactive(true);
                //$this->getCheckoutSession()->setQuoteId($quote->getId());
                return $quote;
            }
        }

        return parent::_getQuote();
    }

    public function removeCtnAction()
    {
        $ctn = filter_var( trim($this->getRequest()->getParam('ctn')), FILTER_SANITIZE_STRING);
        $packageId = filter_var( trim($this->getRequest()->getParam('packageId')), FILTER_SANITIZE_NUMBER_INT);

        if ($ctn && $packageId) {
            $quote = Mage::getSingleton('checkout/cart')->getQuote();
            $packageInfo = Mage::getModel('package/package')
                ->getPackages(null, $quote->getId(), $packageId)
                ->getFirstItem();
            $ctns = ($packageInfo->getId() && $packageInfo->getCtn()) ? explode(',', $packageInfo->getCtn()) : array();

            if (!count($ctns)) {
                $this->getResponse()
                    ->setHeader('Content-Type', 'application/json')
                    ->setBody(
                        Mage::helper('core')->jsonEncode(
                            array(
                                'error' => false,
                                'message' => 'No CTN to remove'
                            )
                        )
                    );
                return;
            }

            if (false !== ($index = array_search($ctn, $ctns))) {
                unset($ctns[$index]);
            }

            $packageInfo->setCtn(join(',', $ctns));
            $packageInfo->save();

            $this->getResponse()
                ->setHeader('Content-Type', 'application/json')
                ->setBody(Mage::helper('core')->jsonEncode(array(
                    'error' => false,
                    'message' => 'CTN removed',
                    'right_sidebar' => $html = Mage::getSingleton('core/layout')->createBlock('core/template')->setTemplate('customer/right_sidebar.phtml')->toHtml(),
                    'totals' => Mage::getSingleton('core/layout')->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/totals.phtml')->toHtml(),
                    'rightBlock' => Mage::getSingleton('core/layout')->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/partials/package_block.phtml')->setCurrentPackageId($this->_getQuote()->getActivePackageId())->toHtml(),
                )));
        } else {
            $this->getResponse()
                ->setHeader('Content-Type', 'application/json')
                ->setBody(Mage::helper('core')->jsonEncode(array(
                    'error' => true,
                    'message' => 'Please provide the CTN number and package ID'
                )));
        }
    }

    /**
     * Save customer shopping cart for later usage
     */
    public function persistCartAction()
    {
        if ($this->getRequest()->isPost() && $this->getRequest()->isXmlHttpRequest()) {
            try {
                $vars = array();
                $type = $this->getRequest()->get('type',0);

                //if business customer
                if ($type) {
                    $vars['companyname'] = filter_var( trim($this->getRequest()->get('companyname', null)), FILTER_SANITIZE_STRING);
                } else {
                    $vars['title'] = filter_var( trim($this->getRequest()->get('title', null)), FILTER_SANITIZE_STRING);
                    $vars['initial'] = filter_var( trim($this->getRequest()->get('initial', null)), FILTER_SANITIZE_STRING);
                    $vars['firstname'] = filter_var( trim($this->getRequest()->get('firstname', null)), FILTER_SANITIZE_STRING);
                    $vars['lastname'] = filter_var( trim($this->getRequest()->get('lastname', null)), FILTER_SANITIZE_STRING);
                    $vars['dob'] = $this->getRequest()->get('dob', null);
                }

                $address = $this->getRequest()->get('address', null);
                $vars['telephone'] = $address['telephone'];

                $cartId = $this->getRequest()->getParam('cart_id', false);
                if ($cartId) {
                    $theQuote = Mage::getModel('sales/quote')->loadByIdWithoutStore($cartId);
                } else {
                    /** @var Vznl_Checkout_Model_Sales_Quote $theQuote */
                    $theQuote = $this->_getCart()->getQuote();
                }

                $quotePackages = $theQuote->getPackages();
                $errors = array();

                $isGuest = ($this->getRequest()->get('guest', 0) == 1);
                if ($isGuest) {
                    $validators = Mage::helper('dyna_validators');
                    foreach ($vars as $k => $var) {
                        $var = trim($var);

                            if (!$validators->validateName($var)) {
                                $errors[] = Mage::helper('vznl_checkout')->__(
                                    sprintf('The %s field contains invalid characters.', $k)
                                );
                            }

                        if ($k != 'initial' && ($var == null || empty($var))) {
                            $errors[] = sprintf('The %s field is empty.', Mage::helper('vznl_checkout')->__($k));
                        }
                    }

                    if (!Mage::helper('dyna_validators')->validateEmailSyntax($this->getRequest()->get('email_send', null))) {
                        $errors[] = $this->__('Please provide a valid email address');
                    } else {
                        $col = Mage::getModel('customer/customer')->getCollection()
                            ->addAttributeToFilter('is_prospect', true)
                            ->addAttributeToFilter('additional_email', array('like' => '%' . $this->getRequest()->get('email_send')));
                        if (count($col) > 0) {
                            $errors[] = $this->__('A prospect with this email address already exists');
                        } elseif (count($errors) === 0) {
                            //If cart is empty, no customer should be saved
                            /** @var Omnius_Customer_Model_Customer_Customer $customer */
                            switch ($vars['title'] ?? null) {
                                case 2:
                                    $prefix = Mage::helper("vznl_checkout")->__('Mrs.');
                                    break;
                                case 1:
                                    $prefix = Mage::helper("vznl_checkout")->__('Mr.');
                                    break;
                                default:
                                    $prefix = '';
                                    break;
                            }
                            if (count($quotePackages) >= 1) {
                                // API call to create a prospect customer - elastic search.
                                $customer = Mage::getModel('customer/customer')
                                    ->setFirstname($vars['firstname'])
                                    ->setLastname($vars['lastname'])
                                    ->setAdditionalEmail($this->getRequest()->get('email_send'))
                                    ->setGender($vars['title'])
                                    ->setDob(date("Y-m-d", strtotime($vars["dob"])))
                                    ->setPrefix($prefix)
                                    ->setMiddlename($vars['initial'])
                                    ->setEmail(uniqid() . Omnius_Customer_Model_Customer_Customer::DEFAULT_EMAIL_SUFFIX)
                                    ->setPhoneNumber($vars['telephone'])
                                    ->setIsProspect(true);
                                if ($theQuote->getCustomerIsBusiness()) {
                                    $customer->setIsBusiness(1)
                                        ->setCompanyName($vars['companyname'])
                                        ->setContractantFirstname($this->getRequest()->get('firstname'))
                                        ->setContractantLastname($this->getRequest()->get('lastname'))
                                        ->setContractantMiddlename($this->getRequest()->get('initial'));
                                }

                                $addressData['country_id'] = Vznl_Checkout_Model_Sales_Order::DEFAULT_COUNTRY;

                                $addressData['postcode'] = $address['quoteAddress']['postcode'];
                                $addressData['city'] = $address['quoteAddress']['city'];
                                $addressData['telephone'] = $address['telephone'];

                                $addressData['street'] = [
                                    $address['quoteAddress']['street'],
                                    $address['quoteAddress']['houseno'],
                                    $address['quoteAddress']['addition']
                                ];
                                $customer->save();


                                $customerAddress = Mage::getModel('customer/address');
                                $customerAddress->setData($addressData)
                                    ->setCustomerId($customer->getId())
                                    ->setIsDefaultBilling(true)
                                    ->setIsDefaultShipping(true);
                                $customerAddress->save();
                                $customer->setDefaultServiceAddressId($customerAddress->getId())->save();

                                Mage::helper('vznl_customer/search')->createApiCustomer($customer, '', $theQuote, 'SaveCustomerFields');
                                Mage::getSingleton('customer/session')->setCustomer($customer);
                                Mage::getSingleton('customer/session')->setCustomerInfoSync($customer->getIsProspect());
                            }
                        }
                    }
                } else {
                    if (!Mage::helper('dyna_validators')->validateEmailSyntax($this->getRequest()->get('email_send', null))) {
                        $errors[] = $this->__('Please provide a valid email address');
                    }
                }

                if (count($errors) > 0) {
                    $result = array(
                        'error' => true,
                        'message' => implode('<br />', $errors)
                    );
                    $this->getResponse()
                        ->setHeader('Content-Type', 'application/json')
                        ->setBody(Mage::helper('core')->jsonEncode($result));
                    return;
                }

                $shouldSave = $this->getRequest()->get('to_save', 'false') != 'false';
                $shouldSend = $this->getRequest()->get('to_send', 'false') != 'false';
                $newCart = $this->getRequest()->get('new_cart', 'false') != 'false';

                if ($shouldSave) {
                    $theQuote->setCartStatus(Vznl_Checkout_Model_Sales_Quote::CART_STATUS_SAVED)
                        ->setChannel(Mage::app()->getWebsite()->getCode())
                        ->setCartStatusStep(Vznl_Checkout_Model_Sales_Quote::CART_STATUS_STEP_CONFIGURATOR);

                    $customer = Mage::getSingleton('customer/session')->getCustomer();
                    if ($isGuest) {
                        $customerId = $customer->getId() ?: null;
                        $theQuote->setCustomerId($customerId)
                            ->setCustomerFirstname($vars['firstname'])
                            ->setCustomerEmail($this->getRequest()->get('email_send'))
                            ->setAdditionalEmail($this->getRequest()->get('email_send'))
                            ->setCustomerDob(date('Y-m-d H:i:s', strtotime($vars['dob'])))
                            ->setCustomerLastname($this->cleanLastName($vars['lastname'], $vars['initial']))
                            ->setCustomerGender($vars['title'])
                            ->setCustomerMiddlename($vars['initial']);
                    } else {
                        $theQuote->setCustomer($customer);
                    }
                    if (count($quotePackages) < 1) {
                        $this->getResponse()
                            ->setHeader('Content-Type', 'application/json')
                            ->setBody(
                                Mage::helper('core')->jsonEncode(array('error' => true, 'message' => $this->__('You have no packages in the current cart.')))
                            );
                        return;
                    }
                    $incrementId = Mage::helper('vznl_checkout')->getQuoteIncrementPrefix() . $theQuote->getId();
                    $theQuote->setData('prospect_saved_quote', $incrementId);
                    $vars['increment_id'] = $incrementId;
                    $theQuote->save();
                }

                $customer = Mage::getSingleton('customer/session')->getCustomer();
                if ($shouldSend) {
                    if ($this->getRequest()->get('email_send', null) != null) {
                        $address = $this->getRequest()->get('email_send');
                    } else {
                        $address = $customer->getCorrespondanceEmail();
                    }
                    /** @var Vznl_Checkout_Helper_Email $emailHelper */
                    $emailHelper = Mage::helper('vznl_checkout/email');
                    $emailHelper->sendConfirmShoppingCart($theQuote, $address);
                    $result = array(
                        'error' => false,
                        'message' => $this->__('The saved shopping bag has been successfully sent to the customers\' e-mail address'),
                    );
                } else {
                    $result = array(
                        'error' => false,
                        'message' => $this->__('Your shopping bag has been successfully saved')
                    );
                }

                /** @var Dyna_Package_Model_Mysql4_Package_Collection $packages */
                $packages = Mage::getModel('package/package')->getCollection();
                $packages->addFieldToFilter('quote_id', $theQuote->getId());

                $packageIds = [];
                /** @var Dyna_Bundles_Model_Package $package */
                foreach ($packages as $package) {
                    if (!$package->getCoupon() || !$package->getIsFfCode()) {
                        continue;
                    }

                    $packageIds[] = $package->getId();
                }

                if (count($packageIds)) {
                    /** @var Dyna_FFHandler_Helper_Request $requestHelper */
                    $requestHelper = Mage::helper("ffhandler/request");
                    $requestHelper->removeCodeUsageByPackagesIds($packageIds);
                }

                if ($newCart) {
                    Mage::helper('vznl_checkout')->createNewQuote();
                    Mage::helper('pricerules')->decrementAll($theQuote);
                    // API call to create prospect customer if not already existing Mobile / Fixed.
                    $customer = Mage::getSingleton('customer/session')->getCustomer();
                    Mage::helper('vznl_customer/search')->createApiCustomer($customer, '', $theQuote, 'SaveExistingFields');
                } elseif (!$newCart && $shouldSave) {
                    // clone quote and set it as current quote so that many users can edit the same saved quote
                    $newQuote = $theQuote
                        ->cloneQuote()
                        ->setIsActive(true)
                        ->setCartStatus(null)
                        ->setProspectSavedQuote(null)
                        ->setStoreId(Mage::app()->getStore(true)->getId())
                        ->save();

                    $theQuote->setIsActive(false)->save();

                    $this->_getCart()->setQuote($newQuote);
                    Mage::getSingleton('checkout/session')->setQuoteId($newQuote->getId());

                    /** @var Dyna_FFHandler_Helper_Request $requestHelper */
                    $requestHelper = Mage::helper("ffhandler/request");

                    $requestHelper->addCodeUsageForQuote($newQuote);
                }
            } catch (Exception $e) {
                $result = array(
                    'error' => true,
                    'message' => sprintf('Action failed with message "%s"', $e->getMessage())
                );
            }
            Mage::getSingleton('core/session')->setloadCustomer(true);
            $this->getResponse()
                ->setHeader('Content-Type', 'application/json')
                ->setBody(Mage::helper('core')->jsonEncode($result));
        } else {
            $this->getResponse()
                ->setHeader('Content-Type', 'application/json')
                ->setBody(
                    Mage::helper('core')->jsonEncode(array('error' => true, 'message' => 'Method not supported'))
                );
        }
    }

    /**
     * Sends current cart email
     */
    public function emailQuoteAction()
    {
        if (!$this->getRequest()->isXmlHttpRequest()) {
            return;
        }

        $customer = Mage::getSingleton('customer/session')->getCustomer();
        $quote = Mage::getModel('sales/quote')->loadByIdWithoutStore($this->getRequest()->getParam('cartId'));

        if ($customer->getId() != null) {
            $address = $customer->getCorrespondanceEmail();
        } else {
            $address = $quote->getCustomerEmail();
        }

        $valid = Mage::helper('dyna_validators')->validateEmailSyntax($address);
        if ($valid) {
            $check = Mage::helper('vznl_checkout/email')->sendConfirmShoppingCart($quote, $address);
            if ($check) {
                $result = array(
                    'error' => false,
                    'message' => sprintf(
                        "%s %s.",
                        $this->__('Shoppingbag has been successfully sent to'),
                        $address
                    )
                );
            } else {
                $result = array(
                    'error' => true,
                    'message' => $this->__('Something went wrong please retry'),
                    'button' => $this->getCartEmailRetryButton($quote)
                );
            }
        } else {
            $result = array(
                'error' => true,
                'message' => $this->__('The email address is invalid.')
            );
        }

        $this->getResponse()
            ->setHeader('Content-Type', 'application/json')
            ->setBody(
                Mage::helper('core')->jsonEncode($result)
            );
    }

    /**
     * Sets as active the quote with the given "quote_id"
     */
    public function activateCartAction()
    {
        if ($this->getRequest()->isPost() && $this->getRequest()->isXmlHttpRequest()) {
            $params = new Varien_Object($this->getRequest()->getParams());
            /** @var Mage_Customer_Model_Session $customerSession */
            $customerSession = Mage::getSingleton('customer/session')->getCustomer();

            if (!$customerSession->getId() && !$customerSession->getProspect()) {
                $this->getResponse()
                    ->setHeader('Content-Type', 'application/json')
                    ->setBody(
                        Mage::helper('core')->jsonEncode(
                            array('error' => true, 'message' => $this->__('No customer currently logged in'))
                        )
                    );
                return;
            }

            $actualQuote = Mage::getSingleton('checkout/cart')->getQuote();
            $serviceAddress = unserialize($actualQuote->getServiceAddress());

            try {
                if ($params->hasData('quote_id')) {
                    /** @var Mage_Sales_Model_Resource_Quote_Collection $collection */
                    $quote = Mage::getModel('sales/quote')->getCollection()
                        ->addFieldToFilter('entity_id', $params['quote_id'])
                        ->getFirstItem();

                    /** Quote deletion based on status,expiration_date,effective_date and website  */
                    $quoteItemCollection = Mage::getModel('sales/quote_item')->getCollection()->addFieldToFilter('quote_id',$params['quote_id']);

                    if ($quoteItemCollection) {
                        $this->checkItemQuotes($quoteItemCollection, $quote);
                    }
                    if (!$quote) {
                        $this->getResponse()
                            ->setHeader('Content-Type', 'application/json')
                            ->setBody(
                                Mage::helper('core')->jsonEncode(
                                    array('error' => true, 'message' => $this->__('The specified quote could not be found'))
                                )
                            );
                        return;
                    }

                    // If the quote has an order it means it was already completed, and can't be loaded again
                    if ($quote->getReservedOrderId()) {
                        $this->getResponse()
                            ->setHeader('Content-Type', 'application/json')
                            ->setBody(
                                Mage::helper('core')->jsonEncode(
                                    array('error' => true, 'message' => $this->__('Quote was already completed'))
                                )
                            );
                        return;
                    }

                    Mage::getSingleton('core/session')->setloadCustomer(true);
                    $isOffer = false;
                    if ($params->hasData('is_offer') && $params['is_offer'] == 1) {
                        $isOffer = true;
                    }

                    if ($isOffer) {
                        $this->rememberData($quote);
                        $actualQuote->setIsActive(false)->save();
                        // Offer mode
                        $quote
                            ->setIsActive(1)
                            ->setCurrentStep(null)
                            ->setStoreId(Mage::app()->getStore(true)->getId())
                            ->save();

                        $this->_getCart()->setQuote($quote);
                        $this->getCheckoutSession()->setQuoteId($quote->getId());
                        //Mage::getSingleton('customer/session')->setIsOffer($quote->getId());

                        $this->getResponse()
                            ->setHeader('Content-Type', 'application/json')
                            ->setBody(
                                Mage::helper('core')->jsonEncode(
                                    array(
                                        'error' => false,
                                        'message' => $this->__('Offer successfully loaded'),
                                        'packageId' => $quote->getActivePackageId(),
                                        'serviceAddress' => $serviceAddress,
                                    )
                                )
                            );

                        return;
                    }

                    if ($quote->getStoreId() == Mage::app()->getStore(true)->getId() ||
                        (
                            in_array($quote->getStore()->getWebsite()->getCode(), Vznl_Agent_Model_Website::getWebsiteCodes())
                            && in_array(
                                Mage::app()->getWebsite()->getCode(),
                                Vznl_Agent_Model_Website::getWebsiteCodes()
                            )
                        )
                    ) {
                        $actualQuote->setIsActive(false)->save();

                        /** @var Vznl_Checkout_Model_Sales_Quote $quote */
                        $newQuote = $quote->cloneQuote(false);
                        $newQuote = Mage::helper('pricerules')->reinitSalesRules($newQuote);

                        $newQuote->setIsActive(true)
                            ->setCartStatus(null)
                            ->setStoreId(Mage::app()->getStore(true)->getId())
                            ->setProspectSavedQuote(null)
                            ->save();

                        $this->_getCart()->setQuote($newQuote);
                        $this->getCheckoutSession()->setQuoteId($newQuote->getId());

                        $newQuotePackages = Mage::getModel('package/package')
                            ->getPackages(null, $newQuote->getId());


                        /** @var Vznl_Package_Model_Package $p */
                        foreach ($newQuotePackages as $package) {
                            // Check if there is a fixed package for existing customer
                            $package = $this->_updateFixedPackageContents($package, $newQuote);

                            $packageStatus = Mage::helper('vznl_checkout')->checkPackageStatus($package->getPackageId());
                            // Update status
                            $package->setCurrentStatus(Vznl_Configurator_Model_AttributeSet::PACKAGE_STATUS_COMPLETED === $packageStatus
                                ? Vznl_Configurator_Model_AttributeSet::PACKAGE_STATUS_COMPLETED
                                : Vznl_Configurator_Model_AttributeSet::PACKAGE_STATUS_OPEN);
                            $package->save();
                        }

                        /** @var Dyna_FFHandler_Helper_Request $requestHelper */
                        $requestHelper = Mage::helper("ffhandler/request");

                        $requestHelper->addCodeUsageForQuote($newQuote, $newQuotePackages);

                        $this->getResponse()
                            ->setHeader('Content-Type', 'application/json')
                            ->setBody(
                                Mage::helper('core')->jsonEncode(
                                    array(
                                        'error' => false,
                                        'message' => $this->__('Quote successfully loaded'),
                                        'packageId' => $quote->getActivePackageId(),
                                        'serviceAddress' => $serviceAddress,
                                    )
                                )
                            );
                    } else {
                        $this->getResponse()
                            ->setHeader('Content-Type', 'application/json')
                            ->setBody(
                                Mage::helper('core')->jsonEncode(
                                    array('error' => true, 'message' => $this->__('Given quote belongs to another store'))
                                )
                            );
                    }
                }
            } catch (Exception $e) {
                Mage::getSingleton('core/logger')->logException($e);
                $this->getResponse()
                    ->setHeader('Content-Type', 'application/json')
                    ->setBody(Mage::helper('core')->jsonEncode(array('error' => true, 'message' => $e->getMessage())));
            }
        } else {
            $this->getResponse()
                ->setHeader('Content-Type', 'application/json')
                ->setBody(
                    Mage::helper('core')->jsonEncode(array('error' => true, 'message' => $this->__('Method not supported')))
                );
        }
    }
    public function cartPackagesAction()
    {
        // Check if there are any incomplete packages
        $quote = Mage::getSingleton('checkout/session')->getQuote();
        $inProgressPackages = array();
        $isOneOff = false;
        /** @var Mage_Sales_Model_Quote_Item $item */
        foreach ($quote->getPackages() as $packageId => $package) {
            if ($package['current_status'] !== Vznl_Configurator_Model_AttributeSet::PACKAGE_STATUS_COMPLETED) {
                $inProgressPackages[$packageId][] = current($package['items']);
            }

            if ($package['one_of_deal']) {
                $isOneOff = true;
            }
        }

        $show = false;
        if (count($inProgressPackages) > 0) {
            $show = true;
        }

        $response = array(
            'error' => false,
            'show' => $show,
            'modal' => Mage::getSingleton('core/layout')->createBlock('core/template')->setTemplate(
                'checkout/cart/cart_packages.phtml'
            )->toHtml(),
        );

        if ($isOneOff && $show) {
            $response = array(
                'error' => true,
                'message' => $this->__('You can\'t continue an one-off deal order with incomplete packages.')
            );
        }
        $this->getResponse()
            ->setHeader('Content-Type', 'application/json')
            ->setBody(Mage::helper('core')->jsonEncode($response));
    }

    public function editSuperorderFromCreditCheckAction()
    {
        $orderIds = Mage::getSingleton('core/session')->getOrderIds();
        $lastOrderId = reset($orderIds);
        $lastOrder = Mage::getModel('sales/order')->load($lastOrderId, 'increment_id');

        $newQuote = Mage::helper('vznl_checkout')->cloneQuote($lastOrder->getQuoteId());

        // Create the packages for the new quote
        $conn = Mage::getSingleton('core/resource')->getConnection('core_read');
        $packages = Mage::getModel('package/package')
            ->getPackages($lastOrder->getSuperorderId());

        $packagesData = $conn->fetchAll($packages->getSelect());
        foreach ($packagesData as $packageData) {
            Mage::getModel('package/package')
                ->setData($packageData)
                ->setEntityId(null)
                ->setStatus(null)
                ->setCreditcheckStatus(null)
                ->setPortingStatus(null)
                ->setQuoteId($newQuote->getId())
                ->setOrderId(null)
                ->save();
        }

        $this->_getCart()->setQuote($newQuote)->save();
        Mage::getSingleton('checkout/session')->setQuoteId($newQuote->getId());
        $this->_redirect('/');
    }

    public function editSuperorderAction()
    {
        $superOrderNo = $this->getRequest()->get('superorderId');
        $fromOpenOrderView = $this->getRequest()->get('openorders');
        $activeBucket = $this->getRequest()->get('bucket');
        $redirect = $this->getRequest()->getParam('redirect', true);
        $changeOrder = $this->getRequest()->getParam('changeorder', false);
        $shouldLock = $this->getRequest()->getParam('lock', false);

        if (!$superOrderNo) {
            throw new Exception('Order not found');
        }
        /** @var Vznl_Superorder_Model_Superorder $superOrder */
        $superOrder = Mage::getModel('superorder/superorder')->getSuperOrderByOrderNumber($superOrderNo);
        if (!$superOrder->getId()) {
            return $this->_redirect('checkout/cart/preview/');
        }
        if (Mage::helper('dyna_checkout')->checkAgentOrderPermissions($superOrderNo) === false) {
            $this->getResponse()->setHttpResponseCode(401);
            return;
        }

        $cancelledPackages = array();
        $quoteShippingData = array('pakket' => array(), 'payment' => array());

        // Discard any open order edits
        Mage::helper('vznl_checkout')->exitSuperOrderEdit();
        Mage::getSingleton('customer/session')->setLastSuperOrder(null);
        /** @var Mage_Sales_Model_Convert_Order $orderConverter */
        $orderConverter = Mage::getModel('sales/convert_order');

        // Load all the Delivery orders
        /** @var Varien_Data_Collection $deliveryOrders */
        $deliveryOrders = $superOrder->getOrders(true)->getItems();
        if (!count($deliveryOrders)) {
            throw new Exception('Super order does not have any delivery orders');
        }

        /** @var Vznl_Checkout_Model_Sales_Order $firstOrder */
        $firstOrder = array_shift($deliveryOrders);
        /** @var Vznl_Checkout_Model_Sales_Quote $newQuote */
        $newQuote = $orderConverter->toQuote($firstOrder)->save();
        /** @var Mage_Sales_Model_Order_Item $orderItem */

        /** @var Mage_Sales_Model_Order_Address $firstOrderShippingAddress */
        $firstOrderShippingAddress = clone $firstOrder->getShippingAddress();
        $firstOrderShippingAddress->unsetData('entity_id');
        $firstOrderShippingAddress->unsetData('parent_id');
        $firstOrderShippingAddress = Mage::getModel('sales/quote_address')
            ->addData($firstOrderShippingAddress->getData());

        /** @var Mage_Sales_Model_Order_Address $firstOrderBillingAddress */
        $firstOrderBillingAddress = clone $firstOrder->getBillingAddress();
        $firstOrderBillingAddress->unsetData('entity_id');
        $firstOrderBillingAddress->unsetData('parent_id');
        $firstOrderBillingAddress = Mage::getModel('sales/quote_address')
            ->addData($firstOrderBillingAddress->getData());

        $newQuote->setShippingAddress($firstOrderShippingAddress);
        $newQuote->setBillingAddress($firstOrderBillingAddress);

        $newQuote->setPayment($orderConverter->paymentToQuotePayment($firstOrder->getPayment()));

        array_unshift($deliveryOrders, $firstOrder);

        /** @var Vznl_Checkout_Model_Sales_Order $order */
        $addedPackages = array();
        foreach ($deliveryOrders as $order) {
            foreach ($order->getAllItems() as $orderItem) {
                if (in_array($orderItem->getPackageId(), $addedPackages) !== false) {
                    continue;
                }
                $addedPackages[] = $orderItem->getPackageId();
            }
        }
        $addedPackages = array_unique($addedPackages);

        if ($shouldLock) {
            // TODO create a fall back or something which should happen when the order cannot be locked.
            Mage::helper('vznl_checkout')->lockOrder($superOrder->getId(), Mage::getSingleton('customer/session')->getAgent(true)->getId(), 'Order change');
        }
        $countDeliveryOrders = count($deliveryOrders);
        foreach ($deliveryOrders as $order) {
            foreach ($order->getAllItems() as $orderItem) {
                //if order is cancelled, set it's packages as cancelled
                if (strtolower($order->getStatus()) == strtolower(Vznl_Checkout_Model_Sales_Order::STATUS_CANCELLED) && in_array($orderItem->getPackageId(), $cancelledPackages) === false) {
                    $cancelledPackages[] = $orderItem->getPackageId();
                }
                $this->convertOrderItemToQuoteItem($orderConverter, $orderItem, $newQuote, $order);
                $this->updateShippingData($quoteShippingData, $order, $orderItem->getPackageId(), $countDeliveryOrders);
            }
            //add also possible previously cancelled packages
            if ($order->getParentId()) {
                $oldAddedPackages = $addedPackages;
                $oldOrder = Mage::getModel('sales/order')->load($order->getParentId());
                while ($oldOrder) {
                    unset($packageModel);
                    $tmpAddedPackages = array();
                    foreach ($oldOrder->getAllItems() as $oldOrderItem) {
                        if (in_array($oldOrderItem->getPackageId(), $oldAddedPackages) === false) {
                            $this->updateShippingData($quoteShippingData, $oldOrder, $oldOrderItem->getPackageId(), $countDeliveryOrders);
                            $this->convertOrderItemToQuoteItem($orderConverter, $oldOrderItem, $newQuote, $order);
                            $tmpAddedPackages[] = $oldOrderItem->getPackageId();
                            if (!isset($packageModel[$oldOrderItem->getPackageId()])) {
                                $package = Mage::getModel('package/package')
                                    ->getCollection()
                                    ->addFieldToSelect('status')
                                    ->addFieldToFilter('order_id', $oldOrder->getSuperorderId())
                                    ->addFieldToFilter('package_id', $oldOrderItem->getPackageId())
                                    ->getFirstItem();

                                if (($package->getStatus() == Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_CANCELLED ||
                                        $package->getStatus() == Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_PORTING_CANCELLED) &&
                                    in_array($oldOrderItem->getPackageId(), $cancelledPackages) === false) {
                                    $cancelledPackages[] = $oldOrderItem->getPackageId();
                                }
                            }
                        }
                    }
                    $tmpAddedPackages = array_unique($tmpAddedPackages);
                    $oldAddedPackages = array_merge($oldAddedPackages, $tmpAddedPackages);
                    $oldOrder = $oldOrder->getParentId() ? Mage::getModel('sales/order')->load($oldOrder->getParentId()) : null;
                }
            }
        }

        $newQuote
            ->setIsActive(false)
            ->setIsOrderEdit(true)
            ->setSuperOrderEditId($superOrder->getId());

        $packagesFromModel = Mage::getModel('package/package')
            ->getPackages($superOrder->getId());

        foreach ($packagesFromModel as $package) {
            $newPackage = Mage::getModel('package/package');
            $newPackage->setData($package->getData())
                ->setId(null)
                ->setOrderId(null)
                ->setQuoteId($newQuote->getId())
                ->save();

            if (
                $package->getStatus() == Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_CANCELLED ||
                $package->getStatus() == Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_PORTING_CANCELLED ||
                ($package->getStatus() == Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_RETURNED && $package->getCancellationDate())
            ) {
                $cancelledPackages[] = $package->getPackageId();
            }
        }

        if ($remoteAddr = Mage::helper('core/http')->getRemoteAddr()) {
            $newQuote->setRemoteIp($remoteAddr);
            $xForwardIp = Mage::app()->getRequest()->getServer('HTTP_X_FORWARDED_FOR');
            $newQuote->setXForwardedFor($xForwardIp);
        }

        $newQuote
            ->setShippingData(Mage::helper('core')->jsonEncode($quoteShippingData))
            ->setStoreId(Mage::app()->getStore()->getId())
            ->save();

        Mage::getSingleton('customer/session')->setOrderEdit($newQuote->getId());

        foreach (array_unique($cancelledPackages) as $packageId) {
            Mage::helper('dyna_package')->updateCancelledPackages($packageId);
        }
        Mage::getSingleton('core/session')->setOrderIds(null);

        if ($changeOrder) {
            $packageIdToRedirect = '1';
            /** @var Vznl_Package_Model_Package $package */
            foreach ($superOrder->getPackages() as $package) {
                if ($package->getCreditcheckStatus() == Vznl_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_CC_PARTIAL) {
                    $packageIdToRedirect = $package->getPackageId();
                    break;
                }
            }

            Mage::getSingleton('customer/session')->setOrderEditMode(true);
            $this->_redirectUrl(sprintf('/checkout/cart/editSuperorderPackage/?packageId=%s&redirect=directly', $packageIdToRedirect));
            return;
        }
        if ($redirect) {
            $data = [
                'active' => $fromOpenOrderView == 'true' ? true : false,
                'bucket' => $activeBucket,
            ];
            Mage::getSingleton('customer/session')->setData('redirect_to_overview', $data);
            if ($this->getRequest()->get('nopreview')) {
                $this->_redirect('checkout/cart/');
            } else {
                $this->_redirectUrl(sprintf('/checkout/cart/preview/?orderId=%s', $newQuote->getId()));
            }
        }
    }

    public function lockSuperorderAction()
    {
        $customerSession = Mage::getSingleton('customer/session');
        if ($superOrderId = $this->getRequest()->get('orderId')) {
            /** @var Vznl_Checkout_Helper_Data $checkoutHelper */
            $checkoutHelper = Mage::helper('vznl_checkout');
            $state = $this->getRequest()->get('state');

            $agent = $customerSession->getAgent(true);

            $bool = true;
            if ($state == 'locked') {
                $bool = false;

                $packageIds = $checkoutHelper->getModifiedPackages();
                $cancelledPackagesNow = Mage::helper('dyna_package')->getCancelledPackagesNow();

                $response = array(
                    'error' => false,
                    'message' => 'Order unlocked'
                );

                /**
                 * Release order lock
                 */
                try {
                    $checkoutHelper->releaseLockOrder($superOrderId);
                    $customerSession->setCustomerInfoSync(true);
                } catch (Exception $e) {
                    $this->getServiceHelper()->returnServiceError($e);
                    return;
                }

                if (!empty($packageIds) || count($cancelledPackagesNow) > 0) {
                    $response['edited'] = 1;
                    Mage::helper('vznl_checkout')->exitSuperOrderEdit();
                }
            } else {
                $error = false;
                if ($lockAgentId = $checkoutHelper->checkOrder($superOrderId, false)) {
                    if ($lockAgentId === 'crossChannel') {
                        // disable order lock/edit if the cross chanel edit is disabled (order created in Indirect)
                        $error = true;
                        $bool = false;
                        $message = $this->__('It\'s not permitted to change this order due to creation in a different channel.');
                    } else {
                        // if $lockAgentId === true (in override mode only), consider the order as locked
                        $message = $this->__('Order locked');
                    }
                    $response = array(
                        'error' => $error,
                        'message' => $message,
                        'locked' => true,
                        'disabled' => !$bool
                    );
                }

                if (!$error) {
                    /**
                     * Lock order
                     */
                    try {
                        $checkoutHelper->lockOrder($superOrderId, $agent->getId(), 'Order change');
                    } catch (Exception $e) {
                        $this->getServiceHelper()->returnServiceError($e);

                        return;
                    }

                    $response = array(
                        'error' => false,
                        'message' => $this->__('Order locked')
                    );
                }
            }

            $customerSession->setOrderEditMode($bool);
        } else {
            $response = array(
                'error' => true,
                'message' => 'Order not found'
            );
        }
        $this->getResponse()
            ->setHeader('Content-Type', 'application/json')
            ->setBody(
                Mage::helper('core')->jsonEncode($response)
            );
    }

    public function exitChangeOrderAction()
    {
        Mage::helper('vznl_checkout')->exitSuperOrderEdit();
        $this->_redirect('/');
    }

    public function cancelEditOrderAction()
    {
        $redirect = $this->getRequest()->getParam('redirect', true);
        if (Mage::getSingleton('checkout/cart')->getQuote(true)->getSuperOrderEditId()) {
            Mage::helper('vznl_checkout')->releaseLockOrder(Mage::getSingleton('checkout/cart')->getQuote(true)->getSuperOrderEditId());
        }
        Mage::helper('vznl_checkout')->exitSuperOrderEdit();
        if ($redirect) {
            $this->_redirect('/');
        }
    }

    public function leavePageDialogSwitcherAction()
    {
        $data = $this->getRequest()->get('locked');
        $this->getCheckoutSession()->setLeavePageDialogSwitcher($data);
    }

    public function saveEditedQuoteAction()
    {
        if (Mage::getSingleton('checkout/session')->getIsEditMode()) {
            $quote = Mage::getSingleton('checkout/cart')->getQuote();
            $superQuote = Mage::getModel('sales/quote')->load(Mage::getSingleton('customer/session')->getOrderEdit());
            /** @var Vznl_Checkout_Helper_Data $checkoutHelper */
            $checkoutHelper = Mage::helper('vznl_checkout');
            /**
             * Check if current order is not locked by another agent
             */
            $checkoutHelper->checkOrder($superQuote->getSuperOrderEditId());

            $quote->setSuperOrderEditId($superQuote->getSuperOrderEditId());
            $quote->setSuperQuoteId($superQuote->getId());
            Mage::helper('vznl_checkout')->setItemPriceHistory($quote);
            $quote->setIsActive(0)->save();
            Mage::helper('dyna_package')->savePackageDifferences($quote->getActivePackageId());
            Mage::getSingleton('checkout/cart')->exitEditMode();
        }
        $this->_redirect('checkout/cart/');
    }

    public function previewAction()
    {
        echo $this->getLayout()->createBlock('vznl_checkout/cart')
            ->setTemplate('order/preview/order_details.phtml')
            ->toHtml();
    }

    public function indexAction()
    {
        if ($this->getCheckoutSession()->getLeavePageDialogSwitcher() === 'true') {
            // If the agent is pending for a polling screen clear the quote on page refresh
            Mage::helper('vznl_checkout')->exitSuperOrderEdit();
            $this->getCheckoutSession()->setLeavePageDialogSwitcher(false);
            Mage::helper('vznl_checkout')->createNewQuote();
            Mage::getSingleton('customer/session')->setLastSuperOrder(null);
        }

        // If customer not synced with DA
        /*if ((!Mage::getSingleton('customer/session')->getOrderEdit()
                && Mage::getSingleton('customer/session')->getCustomer()->getId()
                && Mage::getSingleton('customer/session')->getCustomer()->getBan()
                && !Mage::getSingleton('customer/session')->getCustomerInfoSync()
                && !Mage::getSingleton('customer/session')->getAllowedManualActivation())
            || Mage::getSingleton('customer/session')->getCustomer()->isInMigration()
        ) {
            $url = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
            $this->_redirectUrl($url);
        }*/
        if (Mage::getSingleton('checkout/session')->getIsEditMode() && !Mage::getSingleton('customer/session')->getOrderEditMode()) {
            $this->clearConfiguratorEditMode();
        }

        $customer = ($this->_getQuote()->getCustomer()->getId() ? true : false);

        $request = $this->getRequest();
        $orderNo = $request->get('orderId');
        try {
            if (!empty($orderNo) && $customer && (!Mage::helper('agent')->isTelesalesLine()) && ($order = Mage::getModel('superorder/superorder')->getSuperOrderByOrderNumber($orderNo))) {
                if (!Mage::getSingleton('customer/session')->getOrderEdit()) {
                    Mage::getSingleton('core/session')->setGeneralError(
                        Mage::helper('vznl_checkout')->__('To deliver an order you must be in order edit mode.')
                    );
                    throw new Exception('To deliver an order you must be in order edit mode');
                }

                return $this->renderIndexLayout();
            }

            if (!(Mage::getSingleton('customer/session')->getOrderEdit() && !$request->isPost())) {
                if ($request->isPost()) {
                    Mage::getSingleton('customer/session')->unsLastSuperOrder();
                    Mage::getSingleton('customer/session')->unsOrderEdit();
                    Mage::helper('vznl_checkout')->processNotesAndPickup($request);
                }
                $complete = Mage::helper('vznl_checkout')->checkOneCompletedPackage();
                if (!$complete) {
                    Mage::getSingleton('core/session')->setGeneralError(Mage::helper('vznl_checkout')->__('You must have at least one complete package'));
                    throw new Exception('Incomplete package');
                }

                // Make sure there are no incomplete packages when arriving to the checkout
                Mage::helper('vznl_checkout')->splitQuote();

                /**
                 * No need to recollect totals as when arriving on this page,
                 * the totals are already collected
                 */
                $this->_getQuote()->setTotalsCollectedFlag(true);

                if ($this->_getQuote()->getCartStatusStep() != Vznl_Checkout_Model_Sales_Quote::CART_STATUS_STEP_CHECKOUT) {
                    $this->_getCart()->getQuote()->updateFields(array('cart_status_step' => Vznl_Checkout_Model_Sales_Quote::CART_STATUS_STEP_CHECKOUT));
                }
            }
        } catch (Exception $e) {
            Mage::logException($e);
            // Redirect back to referer or home
            $url = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
            $this->_redirectUrl($url);

            return;
        }
        $this->renderIndexLayout();
    }

    /**
     * @return bool
     */
    public function printAction()
    {
        $orderIncrementId = $this->getRequest()->getParam('order_id', false);
        $contractType= $this->getRequest()->getParam('contract_type', false);
        $superOrderId = $this->getRequest()->getParam('superorder_id', false);
        $logo = $this->getRequest()->getParam('logo') === 'false' ? false : true;
        if (!$orderIncrementId && !$superOrderId) {
            $this->_redirect('/');
            return false;
        }

        // Check if the superorder contract can be printed
        /** @var Vznl_Checkout_Helper_Data $checkoutHelper */
        $checkoutHelper = Mage::helper('vznl_checkout');
        if (!$checkoutHelper->canPrintContract($orderIncrementId)) {
            $this->_redirect('/');
            return false;
        }

        try {
            $deliveryOrder = Mage::getModel('sales/order')->load($orderIncrementId, 'increment_id');
            $filename = 'contract_' . $deliveryOrder->getIncrementId() . '.pdf';
            if ($contractType == 'fixed') {
                $content = Mage::helper('vznl_checkout/sales')->generateFixedContract($deliveryOrder, false, $logo);
            } else {
                $content = Mage::helper('vznl_checkout/sales')->generateContract($deliveryOrder, false, $logo);
            }

            $this->getResponse()->setHeader('Content-Type', 'application/pdf');
            $this->getResponse()->setHeader('Content-Length', strlen($content));
            $this->getResponse()->setHeader('Content-Disposition', 'inline; filename="' . $filename . '"');
            $this->getResponse()->setBody($content);
        } catch (Exception $e) {
            Mage::logException($e);
        }
    }

    /**
     * @return bool
     * @throws Varien_Exception
     */
    public function printGarantAction()
    {
        $orderIncrementId = $this->getRequest()->getParam('order_id', false);

        if (!$orderIncrementId) {
            $this->_redirect('/');
            return false;
        }

        /** @var Vznl_Checkout_Model_Sales_Order $order */
        $order = Mage::getModel('sales/order')->getFirstNonEditedOrder($orderIncrementId);

        $packageItems = array();
        $items = $order->getAllItems();
        $garantPackage = [];
        foreach ($items as $item) {
            // Set the package items by package id
            $packageItems[$item->getPackageId()] = isset($packageItems[$item->getPackageId()]) ? $packageItems[$item->getPackageId()] : array();
            $packageItems[$item->getPackageId()][] = $item;

            // Check if hasGarant is already set for this item
            $product = $item->getProduct();
            $isGarant = ($product && (strtolower($product->getData(Vznl_Catalog_Model_Product::ADDON_TYPE_ATTR)) == 'garant'
                    || strtolower($product->getAttributeText(Vznl_Catalog_Model_Product::ADDON_TYPE_ATTR))=='garant'));

            if (!isset($hasGarant[$item->getPackageId()]) || $isGarant) {
                $hasGarant[$item->getPackageId()] = $isGarant;
            }
        }

        foreach ($garantPackage as $packageId => $hasGarant) {
            if (!$hasGarant) {
                unset($packageItems[$packageId]);
            }
        }

        /** @var Vznl_Checkout_Helper_Pdf $pdfHelper */
        $pdfHelper = Mage::helper('vznl_checkout/pdf');
        $content = $pdfHelper->getWarrantyPdfOutput($order, $packageItems);

        $this->getResponse()->setHeader('Content-Type', 'application/pdf');
        $this->getResponse()->setHeader('Content-Length', strlen($content));
        $this->getResponse()->setHeader('Content-Disposition', 'inline; filename="garant_' . $orderIncrementId . '.pdf"');
        $this->getResponse()->setBody($content);
    }

    /**
     * @return bool
     * @throws Varien_Exception
     */
    public function printLoanAction()
    {
        $orderIncrementId = $this->getRequest()->getParam('order_id', false);

        if (!$orderIncrementId) {
            $this->_redirect('/');
            return false;
        }

        /** @var Vznl_Checkout_Model_Sales_Order $order */
        $order = Mage::getModel('sales/order')->getFirstNonEditedOrder($orderIncrementId);

        /** @var Vznl_Checkout_Helper_Pdf $pdfHelper */
        $pdfHelper = Mage::helper('vznl_checkout/pdf');
        $content = $pdfHelper->getLoanPdfOutput($order);

        $this->getResponse()->setHeader('Content-Type', 'application/pdf');
        $this->getResponse()->setHeader('Content-Length', strlen($content));
        $this->getResponse()->setHeader('Content-Disposition', 'inline; filename="loan_overview_' . $orderIncrementId . '.pdf"');
        $this->getResponse()->setBody($content);
    }

    /**
     * Get all the hardware items of the package
     * Return format JSON : [ [sku => name], [sku => name] ... ]
     */
    public function getPackageHardwareAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            try {
                $packageId = $request->getPost('package_id');

                $superQuote = $this->_getQuote();
                /** @var Vznl_Package_Model_Package $package */
                $package = Mage::getModel('package/package')->getPackages($superQuote->getSuperOrderEditId(), null, $packageId)->getFirstItem();
                // Setup the reutrn channel that belongs to this package
                if (!Mage::helper('agent')->checkIsTelesalesOrWebshop()) {
                    $returnChannel = Vznl_Package_Model_Package::PACKAGE_RETURN_SHOP;
                } elseif ($package->getStatus() === Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_NOT_DELIVERED) {
                    $returnChannel = Vznl_Package_Model_Package::PACKAGE_RETURN_TRANSPORT;
                } else {
                    $returnChannel = Vznl_Package_Model_Package::PACKAGE_RETURN_GRACEPERIOD;
                }
                $response = [
                    'error' => false,
                    'data' => [
                        'hardwareitems' => $package->getHardwareItems(),
                        'return_channel' => $returnChannel,
                    ],
                    'notice' => $package->hasDOADeletedItems(),
                ];

                $this->getResponse()
                    ->setHeader('Content-Type', 'application/json')
                    ->setBody(Mage::helper('core')->jsonEncode($response));
            } catch (Exception $e) {
                $this->getServiceHelper()->returnError($e);
                return;
            }
        }
    }

    public function beforeEditSuperorderPackageAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            try {
                $checkoutSession = $this->getCheckoutSession();

                // Clear prev data
                $checkoutSession->unsDoaItems();
                $checkoutSession->unsRefundReason();

                $returnChannel = $request->getPost('return_channel');
                $packageId = $request->getPost('package_id');
                $refundReason = $request->getPost('refund_reason');

                $checkoutSession->setRefundReason($refundReason);
                $checkoutSession->setReturnChannel($returnChannel);

                // Save refund reason for cancel package usage
                $cancelRefundReason = $checkoutSession->getCancelRefundReason() ?: [];
                $cancelRefundReason[$packageId] = $refundReason;

                $checkoutSession->setCancelRefundReason($cancelRefundReason);

                $itemsDoa = $request->getPost('item_doa', []);
                if ($itemsDoa[0]) {
                    if (count($itemsDoa)) {
                        $checkoutSession->setDoaItems(serialize($itemsDoa));

                        // Save doa items for cancel package usage
                        $cancelDoaItems = $checkoutSession->getCancelDoaItems() ?: [];
                        $cancelDoaItems[$packageId] = serialize($itemsDoa);

                        $checkoutSession->setCancelDoaItems($cancelDoaItems);
                    }
                }
            } catch (Exception $e) {
                Mage::getSingleton('core/logger')->logException($e);
                $this->getServiceHelper()->returnError($e);
                return;
            }
        }
    }

    public function editSuperorderPackageAction()
    {
        /** @var Vznl_Checkout_Helper_Data $checkoutHelper */
        $checkoutHelper = Mage::helper('vznl_checkout');
        $packageId = $this->getRequest()->getParam('packageId');

        $force = $this->getRequest()->getParam('force', 0);
        if (!$packageId) {
            Mage::throwException('Invalid package id');
        }
        $session = Mage::getSingleton('customer/session');
        $cartSession = Mage::getSingleton('checkout/cart');

        $checkoutSession = $this->getCheckoutSession();
        $checkoutSession->unsInitialItems();
        if (!$session->getOrderEditMode()) {
            $session = Mage::getSingleton('core/session');
            $session->setData('redirect_message', $this->__("You were redirected to home page because you left order edit mode"));
            $this->getResponse()
                ->setRedirect(Mage::getBaseUrl())
                ->sendResponse();
            return;
        }

        /** @var Vznl_Checkout_Model_Sales_Quote $currentQuote */
        $currentQuote = Mage::getModel('sales/quote')->load($session->getOrderEdit());
        if (!$currentQuote->getId()) {
            Mage::throwException('No active super quote found.');
        }

        /**
         * Check if current order is not locked by another agent
         */
        $checkoutHelper->checkOrder($currentQuote->getSuperOrderEditId());

        $quoteItems = $currentQuote->getPackageItems($packageId);

        /** @var Mage_Customer_Model_Customer $customer */
        $customer = $session->getCustomer();

        $oldQuotePackage = Mage::getModel('package/package')
            ->getPackages(null, $currentQuote->getId(), $packageId)
            ->getFirstItem();

        /** @var Vznl_Checkout_Model_Sales_Quote $newQuote */
        $newQuote = Mage::getModel('sales/quote')
            ->setCustomer($customer)
            ->setBillingAddress(Mage::getModel('sales/quote_address')->setData($customer->getDefaultBillingAddress() ? $customer->getDefaultBillingAddress()->getData() : array()))
            ->setShippingAddress(Mage::getModel('sales/quote_address')->setData($customer->getDefaultShippingAddress() ? $customer->getDefaultShippingAddress()->getData() : array()))
            ->setSuperQuoteId($currentQuote->getId())
            ->setStoreId($currentQuote->getStoreId())
            ->setIsOverrulePromoMode($oldQuotePackage->isDelivered())
            ->save();

        $doaItems = $checkoutSession->getDoaItems() ? unserialize($checkoutSession->getDoaItems()) : [];
        $hwItemCount = 0;
        $initialItems = [];
        /** @var Vznl_Checkout_Model_Sales_Quote_Item $quoteItem */
        foreach ($quoteItems as $quoteItem) {
            if ($oldQuotePackage->isDelivered()) {
                $initialItems[$quoteItem->getProductId()] = $quoteItem->getSku();
            } elseif ($quoteItem->isPromo()) {
                continue;
            }

            if ($quoteItem->getProduct()->isOfHardwareType()) {
                $hwItemCount++;
            }
            Mage::getModel('sales/quote_item')
                ->setData($quoteItem->getData())
                ->unsetData('item_id')
                ->setItemDoa(in_array($quoteItem->getSku(), $doaItems) ? 1 : null)
                ->setQuote($newQuote)
                ->save();
        }
        $checkoutSession->setInitialItems($initialItems ?: false);
        $hasDoa = count($doaItems);

        $refundReason = $checkoutSession->getRefundReason();
        $returnChannel = $checkoutSession->getReturnChannel();

        $packageType = Mage::getModel('dyna_package/packageType')->loadByCode($oldQuotePackage->getType());
        $packageCreationTypeId = Mage::getModel('dyna_package/packageCreationTypes')->getCollection()->addFieldToFilter('package_type_id', array('eq' => $packageType->getId()))->getFirstItem()->getId();

        $saleType = $oldQuotePackage->getSaleType();
        if($saleType == Vznl_Checkout_Model_Sales_Quote_Item::ACQUISITIE){
            $saleType = Dyna_Catalog_Model_ProcessContext::ACQ;
        }

        /** @var Vznl_Package_Model_Package $packageModel */
        $packageModel = Mage::getModel('package/package')
            ->setData($oldQuotePackage->getData())
            ->setId(null)
            ->setSaleType($saleType)
            ->setPackageCreationTypeId($packageCreationTypeId)
            ->setQuoteId($newQuote->getId())
            ->setOrderId(null)
            ->setRefundReason($refundReason)
            ->setReturnChannel($returnChannel)
            ->save();

        $superOrderEditId = ($this->_shouldShowConfigurator($refundReason, $hasDoa)) ? null : $currentQuote->getSuperOrderEditId();

        $newQuote->setIsSuperMode(true)
            ->setActivePackageId($packageId)
            ->setCurrentPackageId($packageId)
            ->setSuperOrderEditId($superOrderEditId)
            ->setTotalsCollectedFlag(false)
            ->collectTotals()
            ->save();
        $newQuote->unsIsOverrulePromoMode();
        $newQuote = Mage::getResourceModel('sales/quote_collection')
            ->addFieldToFilter('entity_id', $newQuote->getId())
            ->getLastItem();

        $cartSession->setEditMode($newQuote);

        $checkoutSession->unsDoaItems();
        $checkoutSession->unsRefundReason();
        $packageModel
            ->setCurrentStatus(Vznl_Configurator_Model_AttributeSet::PACKAGE_STATUS_COMPLETED === Mage::helper('vznl_checkout')->checkPackageStatus($packageId)
                ? Vznl_Configurator_Model_AttributeSet::PACKAGE_STATUS_COMPLETED
                : Vznl_Configurator_Model_AttributeSet::PACKAGE_STATUS_OPEN)
            ->save();


        /** @var Vznl_PriceRules_Helper_Data $priceRulesHelper */
        $priceRulesHelper = Mage::helper('pricerules');
        $priceRulesHelper->decrementAll($newQuote);
        $priceRulesHelper->reinitSalesRules($newQuote);

        $response = [];
        if ($this->_shouldShowConfigurator($refundReason, $hasDoa) || $force) {
            $redirect = $this->getRequest()->getParam('redirect');
            if ($redirect == 'directly') {
                $this->_redirectUrl(trim(Mage::getBaseUrl(), '/') . '?packageId=' . $packageId);
                return;
            }
            $response = ['redirect' => trim(Mage::getBaseUrl(), '/') . '?packageId=' . $packageId];
        } else {
            Mage::helper('dyna_package')->savePackageDifferences($newQuote->getActivePackageId());
            $response = ['orderId' => $session->getOrderEdit()];
            $response = ['redirect' => '/checkout/cart/'];
        }

        $this->getResponse()
            ->setHeader('Content-Type', 'application/json')
            ->setBody(Mage::helper('core')->jsonEncode($response));
    }

    /**
     * Check if the configurator is needed in DOA flow
     *
     * @param string $reason
     * @param bool $hasDoa
     * @return bool
     */
    private function _shouldShowConfigurator($reason, $hasDoa)
    {
        switch ($reason) {
            case 'ONTEVREDEN':
            case 'DOA/SWAP':
                return true;
            default:
                return !$hasDoa;
        }
    }

    /**
     * Cancel configurator edit mode
     */
    public function cancelEditSuperorderPackageAction()
    {
        /** @var Vznl_Checkout_Model_Cart $cart */
        $cart = Mage::getSingleton('checkout/cart');
        $quote = $cart->getQuote();
        $superOrderId = Mage::getModel('sales/quote')->load($cart->getQuote()->getSuperQuoteId())->getSuperOrderEditId();
        $superOrder = Mage::getModel('superorder/superorder')->load($superOrderId);
        Mage::helper('pricerules')->decrementAll($quote);
        $quote->setSuperQuoteId(null)
            ->setSuperOrderEditId(null);
        $this->clearConfiguratorEditMode();
        Mage::getSingleton('customer/session')->unsOrderEditMode();

        $this->_redirect(
            '/',
            [
                '_query' => [
                    'orderId' => $superOrder->getOrderNumber()
                ]
            ]
        );
    }

    public function cancelDeliverSuperorderAction()
    {
        Mage::helper('vznl_checkout')->exitSuperOrderEdit();

        $this->_redirect('/');
    }

    /**
     * @throws Exception
     *
     * Removes a package from the cart
     */
    public function removePackageAction()
    {
        //store current packages as cancelled now to know this wasn't cancelled in previous changes
        Mage::helper('vznl_package')->updateCancelledPackagesNow($this->getRequest()->getParam('packageId'));
        $this->_redirect('checkout/cart');
    }

    /**
     * Un-sets the packages that are set to be removed
     */
    public function stopRemovePackageAction()
    {
        Mage::helper('dyna_package')->resetCancelledPackagesNow();
        Mage::getSingleton('customer/session')->unsCancelledPackagesNow();
        $this->_redirect('checkout/cart');
    }

    /**
     * @param $orderConverter
     * @param $orderItem
     * @param $newQuote
     * @param $order
     *
     * Convert order item to quote item
     */
    private function convertOrderItemToQuoteItem($orderConverter, $orderItem, $newQuote, $order)
    {
        $orderConverter = null;
        if ($orderItem->getProduct()->getId()) {
            /** @var Vznl_Checkout_Model_Sales_Quote_Item $quoteItem */
            $quoteItem = Mage::getModel('sales/quote_item')
                ->setStoreId($orderItem->getOrder()->getStoreId())
                ->setQuote($newQuote)
                ->setEditOrderId($order->getId())
                ->setCustomPrice($orderItem->getData('row_total'))
                ->setOriginalCustomPrice($orderItem->getData('row_total_incl_tax'))
                ->setProductId($orderItem->getProductId())
                ->setParentProductId($orderItem->getParentProductId())
                ->setQuoteItemId($orderItem->getId());

            $orderItem->setHiddenTaxAmount(0);

            Mage::helper('core')->copyFieldset('sales_convert_order_item_editorder', 'to_quote_item', $orderItem, $quoteItem);
            $quoteItem->save();
        }
    }

    /**
     * Set shipping data to display on the order display
     *
     * @param $quoteShippingData
     * @param Vznl_Checkout_Model_Sales_Order $order
     * @param int $packageId
     * @param int $multiplePackages
     */
    private function updateShippingData(&$quoteShippingData, $order, $packageId, $deliveryOrdersCount = 1)
    {
        // If multiple deliveryOrders then order is slit delivery
        if ($deliveryOrdersCount > 1) {
            if (!isset($quoteShippingData['pakket'][$packageId])) {
                $shippingData = $this->getShippingDataArray($order);
                $quoteShippingData['pakket'][$packageId] = $shippingData['deliver'];
                $quoteShippingData['payment'][$packageId] = $shippingData['payment'];
            }
        } else {
            $quoteShippingData = $this->getShippingDataArray($order);
        }
    }

    /**
     * Create quote shipping data array from order information
     *
     * @param Vznl_Checkout_Model_Sales_Order $order
     * @return array
     */
    private function getShippingDataArray($order)
    {
        $shippingData = array(
            'deliver' => array(
                'address' => array(
                    'street' => $order->getShippingAddress()->getStreet(),
                    'postcode' => $order->getShippingAddress()->getPostcode(),
                    'city' => $order->getShippingAddress()->getCity(),
                    'address' => $order->getShippingAddress()->getDeliveryType(),
                    'store_id' => $order->getShippingAddress()->getDeliveryStoreId()
                )
            ),
            'payment' => $order->getPayment()->getMethod()
        );

        return $shippingData;
    }

    /**
     * Just render the layout for the indexAction
     */
    private function renderIndexLayout()
    {
        $this
            ->loadLayout()
            ->_initLayoutMessages('checkout/session')
            ->_initLayoutMessages('catalog/session')
            ->getLayout()->getBlock('head')->setTitle($this->__('Shopping Cart'));
        $this->renderLayout();
    }

    /**
     * @param $quote
     *
     * Keep offer quote in session in order to overwrite any possible changes
     */
    public function rememberData($quote)
    {
        Mage::getSingleton('customer/session')->setOfferteData($quote);
    }

    /**
     * Cancel configurator edit mode
     */
    protected function clearConfiguratorEditMode()
    {
        /** @var Vznl_Checkout_Model_Cart $cart */
        $cart = Mage::getSingleton('checkout/cart');
        $quote = $cart->getQuote();
        if ($quote) {
            $quote->setIsActive(0)
                ->save();
        }

        Mage::getSingleton('checkout/session')->setIsEditMode(false);
    }

    /**
     * Restart home delivery process
     */
    public function restartHomeDeliveryAction()
    {
        $orderNumber = $this->getRequest()->getPost('orderNumber');
        $deliveryOrderId = $this->getRequest()->getPost('deliveryId');
        $deliverOrder = Mage::getModel('sales/order')->load($deliveryOrderId);
        $superorder = Mage::getModel('superorder/superorder')->load($deliverOrder->getSuperorderId());

        if ($deliverOrder->getId()) {

            // Create a new delivery order used to restart home delivery
            try {
                /** @var Vznl_Checkout_Model_Sales_Order $newOrder */
                $newOrder = Mage::getModel('sales/order');

                $newOrder->addStatusHistoryComment($deliverOrder->getStatus(), '[Notice] - Restart home delivery');
                $newOrder->setData($deliverOrder->getData());
                $newOrder->unsetData('parent_id');
                $newOrder->unsetData('entity_id');
                $newOrder->unsetData('increment_id');
                $newOrder->unsetData('contract_sign_date');
                $newOrder->unsetData('status');
                $newOrder->unsetData('reason_code');
                $newOrder->setState(Mage_Sales_Model_Order::STATE_PROCESSING, true);

                foreach ($deliverOrder->getAllItems() as $item) {
                    $orderItem = Mage::getModel('sales/order_item');
                    $orderItem->setData($item->getData());
                    $orderItem->unsetData('item_id');
                    $orderItem->unsetData('order_id');
                    $orderItem->setOrder($newOrder);

                    $newOrder->addItem($orderItem);
                }

                $address = Mage::getModel('sales/order_address');
                $address->setData($deliverOrder->getBillingAddress()->getData());
                $address->unsetData('entity_id');
                $address->unsetData('parent_id');
                $address->setStreet($address->getStreet());
                $newOrder->setBillingAddress($address);

                $address = Mage::getModel('sales/order_address');
                $address->setData($deliverOrder->getShippingAddress()->getData());
                $address->unsetData('entity_id');
                $address->unsetData('parent_id');
                $address->setStreet($address->getStreet());
                $newOrder->setShippingAddress($address);

                $payment = Mage::getModel('sales/order_payment');
                $payment->setData($deliverOrder->getPayment()->getData());
                $payment->unsetData('entity_id');
                $payment->unsetData('parent_id');
                $newOrder->setPayment($payment);

                $newOrder->setEdited(0);
                $newOrder->setEcomStatus(null);
                $newOrder->save();

                if ($newOrder->getId()) {
                    // Set old delivery order as edited
                    $deliverOrder->setEdited(1)->save();

                    // Regenerate contracts because phone numbers might have changed
                    /** @var Vznl_Checkout_Helper_Pdf $pdfHelper */
                    $pdfHelper = Mage::helper('vznl_checkout/pdf');
                    $pdfOutput = Mage::helper('vznl_checkout/sales')->generateContract($newOrder, true, false);

                    $pdfHelper->saveContract($pdfOutput, $newOrder->getIncrementId(), 'pdf', false);

                    $agentId = Mage::helper('agent')->getAgentId();
                    // Handle lock to ESB
                    $lockClient = Mage::helper('omnius_service')->getClient('lock_manager');
                    $lockInfo = $lockClient->doGetLockInfo($superorder);
                    $lockUser = isset($lockInfo['lock']) ? ($lockInfo['lock']['user_i_d'] ?: null) : null;
                    if ($lockUser != 'ESB' && !is_null($lockUser)) {
                        $lockClient->doHandOverLock($orderNumber, $lockUser, 'ESB');
                    } else if($lockUser != 'ESB'){
                        $lockClient->doHandOverLock($orderNumber, $agentId, 'ESB');
                    }

                    /** $syncClient Omnius_Service_Model_Client_SyncClient */
                    $syncClient = $this->getServiceHelper()->getClient('sync');
                    $result = $syncClient->doRestartHomeDelivery($orderNumber, $newOrder->getIncrementId(), $deliverOrder->getIncrementId());

                    if ($result && isset($result['succeeded']) && $result['succeeded'] == "true") {
                        $response = array(
                            'error' => false,
                            'message' => $this->__('<strong>' . $this->__('Delivery restarted!') . '</strong>') . sprintf($this->__('The packages for home delivery (%s) have been relisted. New home delivery ID: %s'), $deliverOrder->getIncrementId(),  $newOrder->getIncrementId()),
                        );
                    } else {
                        $newOrder->setEcomStatus(Vznl_Checkout_Model_Sales_Order::STATUS_CANCELLED)
                            ->setReasonCode(Vznl_Checkout_Model_Sales_Order::REASON_CODE_APPOINTMENT)
                            ->save();

                        $response = array(
                            'error' => true,
                            'message' => (is_array($result) && isset($result['message'])) ? $this->__($result['message']) : $this->__('Unknown error response'),
                        );
                    }
                } else {
                    $response = array(
                        'error' => true,
                        'message' => $this->__('Could not create a new delivery order'),
                    );
                }
            } catch (Exception $e) {
                $this->getServiceHelper()->returnServiceError($e);
                return;
            }
        } else {
            $response = array(
                'error' => true,
                'message' => $this->__('Delivery order not found'),
            );
        }

        $this->getResponse()
            ->setHeader('Content-Type', 'application/json')
            ->setBody(Mage::helper('core')->jsonEncode($response));
    }

    public function changeCustomerEmailAction()
    {
        /** @var Dyna_FFHandler_Helper_Request $requestHelper */
        $requestHelper = Mage::helper("ffhandler/request");

        $email = $this->getRequest()->getParam('email');

        /** @var Vznl_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();

        $packages = Mage::getModel('package/package')->getCollection()
            ->addFieldToFilter('quote_id', $quote->getId());

        $hasFFCodes = [];

        foreach ($packages as $package) {
            if (!$package->getCoupon() || !$package->getIsFfCode()) {
                continue;
            }

            $hasFFCodes[] = $package->getId();
        }

        $quote->setAdditionalEmail($email);
        $this->_getCart()->save();

        $packages = Mage::getModel('package/package')->getCollection()
            ->addFieldToFilter('quote_id', $quote->getId());

        $packagesToRemove = [];

        foreach ($packages as $package) {
            if ($package->getCoupon() || !in_array($package->getId(), $hasFFCodes)) {
                continue;
            }

            $packagesToRemove[] = $package->getId();
        }

        $requestHelper->removeCodeUsageByPackagesIds($packagesToRemove);

        $output = $this->getLayout()->createBlock('core/template')->setTemplate('checkout/cart/steps/save_overview.phtml')->toHtml();

        $this->getResponse()->setBody($output);
    }

    /**
     * Register an IMEI number
     *
     * @throws Mage_Core_Exception
     */
    public function registerImeiAction()
    {
        /** @var Vznl_Superorder_Model_Superorder $superOrder */
        $superOrder = Mage::getSingleton('customer/session')->getSuperOrder();
        if (!($superOrder && $superOrder->getId())) {
            $this->_redirect('checkout/cart');
            return;
        }

        if ($this->getRequest()->isPost()) {
            $imeiPackages = $this->getRequest()->getPost('deviceselection', []);
            $packages = $superOrder->getPackages();

            $imeisAdded = 0;
            /** @var Vznl_Package_Model_Package $package */
            foreach ($packages as $package) {
                // Check if this package id exists in the array
                if (!array_key_exists($package->getPackageId(), $imeiPackages)) {
                    continue;
                }

                $imeiData = $imeiPackages[$package->getPackageId()];
                if (!array_key_exists('imei_number', $imeiData)) {
                    continue;
                }
                $newImei = $imeiData['imei_number'];

                if ($newImei != $package->getImei()) {
                    $package->setImei($imeiData['imei_number']);
                    $package->save();
                    $imeisAdded++;
                }
            }

            if ($imeisAdded > 0) {
                $serviceHelper = Mage::helper('vznl_service');
                /** @var Vznl_Service_Model_Client_EbsOrchestrationClient $client */
                $client = $serviceHelper->getClient('ebs_orchestration');
                $client->processCustomerOrder(
                    $superOrder,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    [],
                    false,
                    true
                );
            }
            $this->_redirect('/', ['_query' => ['orderId' => $superOrder->getOrderNumber()]]);

            return;
        }

        $this->loadLayout();
        $this->getLayout()->getBlock('head')->setTitle($this->__('Fill in IMEI'));

        $this->renderLayout();
    }

    /**
     * Save new prospect customer for a guest customer (no login)
     * The e-mail must be unique for this (there should be no saved is_prospect customer with the same e-mail)
     * Some basic validation is done: on e-mail syntax, dob and name
     */
    protected function saveNewProspectCustomer()
    {
        $this->errors = array();

        $validators = Mage::helper('dyna_validators');
        foreach ($this->vars as $k => $var) {
            $this->validateFormKey($var, $k, $validators, $this->errors);
        }

        if (!Mage::helper('omnius_validators')->validateEmailSyntax($this->getRequest()->get('email_send', null))) {
            $this->errors[] = $this->__('Please provide a valid email address');
            return;
        }

        if (count($this->errors) > 0) {
            return;
        }

        $customer = Mage::getModel('customer/customer');
        $customer = $customer->getCollection()
            ->addAttributeToFilter('is_prospect', true)
            ->addAttributeToFilter('additional_email', $this->getRequest()->get('email_send'))
            ->addAttributeToFilter('firstname', $this->getRequest()->get('firstname'))
            ->addAttributeToFilter('lastname', $this->getRequest()->get('lastname'))
            ->addAttributeToFilter('additional_telephone', array('in' => array($this->vars['additional_telephone'],
                $customer::COUNTRY_CODE . $this->vars['additional_telephone'], $customer::SHORT_COUNTRY_CODE . $this->vars['additional_telephone'])))
            ->addAttributeToFilter('phone_number', $this->vars['telephone'])
            ->addAttributeToFilter('is_business', $this->guestCustomerIsSoho)
            ->addAttributeToFilter('dob', date("Y-m-d H:i:s", strtotime($this->vars['dob'])));

        if ($this->guestCustomerIsSoho) {
            $customer->addAttributeToFilter('company_name', $this->vars['companyname']);
        }
        $customer->setPageSize(1, 1)->getLastItem();
        if (count($customer) > 0 && isset($customer->getData()[0]['entity_id'])) {
            $this->prospectCustomer = Mage::getModel('customer/customer')->load($customer->getData()[0]['entity_id']);
        } else {
            /** @var Dyna_Checkout_Helper_Cart $cartDataHelper */
            $cartDataHelper = Mage::helper('dyna_checkout/cart');
            $this->vars['email_send'] = $this->getRequest()->get('email_send');
            $this->vars['firstname'] = $this->getRequest()->get('firstname');
            $this->vars['lastname'] = $this->getRequest()->get('lastname');
            $this->vars['guestCustomerIsSoho'] = $this->guestCustomerIsSoho;
            $this->prospectCustomer = $cartDataHelper->createDeProspectFromCart($this->vars);
        }
    }

    protected function setNewProspectCustomerOnQuote()
    {
        $this->theQuote->setCustomerId($this->prospectCustomer->getId())
            ->setCustomerFirstname($this->vars['firstname'])
            ->setCustomerLastname($this->cleanLastName($this->vars['lastname'], $this->vars['initial']))
            ->setCustomerDob(date('Y-m-d H:i:s', strtotime($this->vars['dob'])));

        if ($this->guestCustomerIsSoho) {
            $this->theQuote->setCustomerCompanyname($this->vars['companyname']);
        }
        $customer = $this->prospectCustomer;

        $this->theQuote->setCustomerEmail($this->getRequest()->get('email_send'))
            ->setAdditionalEmail($this->getRequest()->get('email_send'))
            ->setCustomerAdditionalTelephone($customer::COUNTRY_CODE . $this->vars['telephone'])
            ->setCustomerPhoneNumber($this->vars['telephone'])
            ->setCustomerIsBusiness($this->guestCustomerIsSoho);
    }

    public function getFixedTelephoneNumberAction()
    {
        $helper = Mage::helper('vznl_getTelephoneNumbers');
        $quoteId = Mage::getSingleton('checkout/cart')->getQuote()->getId();
        $quote = Mage::getModel('sales/quote')->load($quoteId);
        if ($quote->getPackages()) {
            $serviceAddress = unserialize($quote->getServiceAddress());
        } else {
            $orderIds = Mage::getSingleton('core/session')->getOrderIds();
            $lastOrderId = reset($orderIds);
            $lastOrder = Mage::getModel('sales/order')->load($lastOrderId, 'increment_id');
            $quote = Mage::getModel('sales/quote')->load($lastOrder->getQuoteId());
            $serviceAddress = unserialize($quote->getServiceAddress());
        }
        $validateAddress = array(
            "houseFlatNumber" => isset($serviceAddress["housenumber"]) ? $serviceAddress["housenumber"] : '',
            "houseFlatExt" => isset($serviceAddress["housenumberaddition"]) ? $serviceAddress["housenumberaddition"] : '',
            "postCode" => isset($serviceAddress["postalcode"]) ? $serviceAddress["postalcode"] : '',
            "footPrint" => 'Ziggo',
            //$customerType value is sent to the peal and the value is as per the peal specification
            "customerType" => ($quote->getData('customer_id_type')) ? Vznl_Checkout_CartController::PEAL_CUSTOMER_TYPE_PRIVATE : Vznl_Checkout_CartController::PEAL_CUSTOMER_TYPE_BUSINESS
        );
        $response = $helper->getTelephoneNumbers($validateAddress);
        if (!$response) {
            $response = "Unable to fetch fixed telephone numbers";
        }
        if ($response['error']) {
            $pealErrorCode = Mage::getModel('validatecart/errorcode')->getByCode($response["code"],$response['message'],Vznl_GetTelephoneNumbers_Adapter_Peal_Adapter::name);
            $response = array(
                'error' => true,
                'message' => $pealErrorCode['translation'],
            );
        }
        $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
            Mage::helper('core')->jsonEncode($response)
        );
        return;
    }

    public function getFixedDeliveryWishDatesAction()
    {
        $helper = Mage::helper('vznl_deliverywishdates');
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $basketId = $quote->getData('basket_id');
        //$customerType value is sent to the peal and the value is as per the peal specification
        $customerType = ($quote->getData('customer_id_type')) ? Vznl_Checkout_CartController::PEAL_CUSTOMER_TYPE_PRIVATE : Dyna_Customer_Helper_Services::PARTY_TYPE_SOHO;
        //$deliveryMethod = $_GET['deliveryMethod'];
        $deliveryMethod = $this->getRequest()->getParam('deliveryMethod');
        $isOverstappen = $this->getRequest()->getParam('overstappen');
        $response = $helper->getDeliveryWishDates($basketId, $customerType, $deliveryMethod, $isOverstappen, $quote->getQuoteNewHardware());
        if (!$response) {
            $response = "Unable to fetch delivery wish dates";
        }
        if (isset($response['error'])) {
            $pealErrorCode = Mage::getModel('validatecart/errorcode')->getByCode($response["code"], $response['message'], Vznl_DeliveryWishDates_Adapter_Peal_Adapter::name);
            $response = array(
                'error' => true,
                'message' => $pealErrorCode['translation'],
            );
        }
        $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
            Mage::helper('core')->jsonEncode($response)
        );
        return;
    }

    /*
     * redirect page after submit order
     */
    public function redirectPageAction()
    {
        $flag = $this->getRequest()->getParam('flag');
        Mage::getSingleton('core/session')->setloadCustomer(true);

        if ($flag == "true") {
            $customerHelper = Mage::helper('vznl_customer');
            $customerHelper->unloadCustomer();
        }

        $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
            Mage::helper('core')->jsonEncode($flag)
        );
    }

    /*
     * validates hardware service number
     */
    public function validateHardwareServiceAction()
    {
        $helper = Mage::helper('vznl_validateserialnumber');
        $serialnumber = $this->getRequest()->getParam('serial_number');
        $response = $helper->validateSerialNumber($serialnumber);
        if (!$response) {
            $response = $this->__('Unable to fetch delivery wish dates');
        }
        if (isset($response['error'])) {
            $pealErrorCode = Mage::getModel('validatecart/errorcode')->getByCode($response["code"], $response['message'],Vznl_ValidateSerialNumber_Adapter_Peal_Adapter::name);
            $response = array(
                'error' => true,
                'message' => $pealErrorCode['translation'],
            );
        }
        $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
            Mage::helper('core')->jsonEncode($response)
        );
        return;
    }

    /*
     * Reserve the fixed telephone number
     */
    public function reserveFixedNumberAction()
    {
        $fixedTelephoneNumber = $this->getRequest()->getParam('fixed_telephone');
        $quote = $this->_getQuote();
        if (!$quote->getPackages()) {
            $orderIds = Mage::getSingleton('core/session')->getOrderIds();
            $lastOrderId = reset($orderIds);
            $lastOrder = Mage::getModel('sales/order')->load($lastOrderId, 'increment_id');
            $quote = Mage::getModel('sales/quote')->load($lastOrder->getQuoteId());
        }
        $serviceAddress = unserialize($quote->getServiceAddress());
        $serviceHouseNumber = isset($serviceAddress["housenumber"]) ? $serviceAddress["housenumber"] : '';
        $serviceHouseNumberAddition = isset($serviceAddress["housenumberaddition"]) ? $serviceAddress["housenumberaddition"] : '';
        $servicePostalCode = isset($serviceAddress["postalcode"]) ? $serviceAddress["postalcode"] : '';
        //$customerType value is sent to the peal and the value is as per the peal specifications.
        $customerType = ($quote->getData('customer_id_type')) ? Vznl_Checkout_CartController::PEAL_CUSTOMER_TYPE_PRIVATE : Vznl_Checkout_CartController::PEAL_CUSTOMER_TYPE_BUSINESS;
        //$requestType action type to release or reserve
        $requestType = Vznl_Checkout_CartController::FIXED_NUMBER_RESERVE;
        //Release Telephone Number to Peal
        $reserveHelper = Mage::helper('vznl_reserveTelephoneNumber');
        $reserveResponse = $reserveHelper->reserveTelephoneNumber($requestType, $serviceHouseNumber, $servicePostalCode, $fixedTelephoneNumber, $customerType, $serviceHouseNumberAddition);
        if ($reserveResponse['error']) {
            $pealErrorCode = Mage::getModel('validatecart/errorcode')->getByCode($reserveResponse['code'],$reserveResponse['message'],Vznl_ReserveTelephoneNumber_Adapter_Peal_Adapter::name);
            $reserveResponse = array(
                'error' => true,
                'message' => $pealErrorCode['translation'],
            );
            $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                Mage::helper('core')->jsonEncode($reserveResponse)
            );
            return;
        }
        if ($reserveResponse) {
            $response = $reserveResponse;
            if ($response['statusCode'] != "200") {
                $result['error'] = false;
                $result['message'] = "Telephone Number Reserve Failed";
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            }
        } else {
            $result['error'] = true;
            $result['message'] = "Telephone Number Reserve Failed";
            $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                Mage::helper('core')->jsonEncode($result)
            );
            return;
        }
        // Set successful response
        $result['error'] = false;
        $result['message'] = "Step data successfully saved";
        $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
            Mage::helper('core')->jsonEncode($result)
        );
    }

    /*
     * Release the old fixed telephone number
     */
    public function releaseOldFixedNumberAction()
    {
        $fixedTelephoneNumber = $this->getRequest()->getParam('old_telephone');
        $quote = $this->_getQuote();
        if (!$quote->getPackages()) {
            $orderIds = Mage::getSingleton('core/session')->getOrderIds();
            $lastOrderId = reset($orderIds);
            $lastOrder = Mage::getModel('sales/order')->load($lastOrderId, 'increment_id');
            $quote = Mage::getModel('sales/quote')->load($lastOrder->getQuoteId());
        }
        $serviceAddress = unserialize($quote->getServiceAddress());
        $serviceHouseNumber = isset($serviceAddress["housenumber"]) ? $serviceAddress["housenumber"] : '';
        $serviceHouseNumberAddition = isset($serviceAddress["housenumberaddition"]) ? $serviceAddress["housenumberaddition"] : '';
        $servicePostalCode = isset($serviceAddress["postalcode"]) ? $serviceAddress["postalcode"] : '';
        //$customerType value is sent to the peal and the value is as per the peal specifications.
        $customerType = ($quote->getData('customer_id_type')) ? Vznl_Checkout_CartController::PEAL_CUSTOMER_TYPE_PRIVATE : Vznl_Checkout_CartController::PEAL_CUSTOMER_TYPE_BUSINESS;
        //$requestType action type to release or reserve
        $requestType = Vznl_Checkout_CartController::FIXED_NUMBER_RELEASE;
        //Release Telephone Number to Peal
        $reserveHelper = Mage::helper('vznl_reserveTelephoneNumber');
        $reserveResponse = $reserveHelper->reserveTelephoneNumber($requestType, $serviceHouseNumber, $servicePostalCode, $fixedTelephoneNumber, $customerType, $serviceHouseNumberAddition);
        if ($reserveResponse['error']) {
            $pealErrorCode = Mage::getModel('validatecart/errorcode')->getByCode($reserveResponse['code'],$reserveResponse['message'],Vznl_ReserveTelephoneNumber_Adapter_Peal_Adapter::name);
            $reserveResponse = array(
                'error' => true,
                'message' => $pealErrorCode['translation'],
            );
            $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                Mage::helper('core')->jsonEncode($reserveResponse)
            );
            return;
        }
        if ($reserveResponse) {
            $response = $reserveResponse;
            if ($response['statusCode'] != "200") {
                $result['error'] = false;
                $result['message'] = "Telephone Number Release Failed";
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            }
        } else {
            $result['error'] = true;
            $result['message'] = "Telephone Number Release Failed";
            $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                Mage::helper('core')->jsonEncode($result)
            );
            return;
        }
        // Set successful response
        $result['error'] = false;
        $result['message'] = "Step data successfully saved";
        $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
            Mage::helper('core')->jsonEncode($result)
        );
    }

    /**
     * Get notes on package
     */
    public function getNotesOfPackageAction()
    {
        if ($postData = $this->getRequest()->getPost()) {
            try {
                $packageId = $postData['packageId'];
                /** @var Dyna_Checkout_Model_Sales_Quote $quote */
                $quote = Mage::getSingleton('checkout/cart')->getQuote();

                $package = $quote->getCartPackage($packageId);
                $package->setData('items', $package->getAllItems());

                // package title
                $packageSubtype = $package->isFixed() ? Vznl_Catalog_Model_Type::SUBTYPE_FIXED_PACKAGE : Dyna_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION;
                $packageTypeAndTariffTitles = Mage::helper('vznl_package')->getTypeAndSubtypeTitle($package, $packageSubtype);

                $response['packageTitle'] = $packageTypeAndTariffTitles['packageSubtypeTitle'] ?: '';
                // package notes
                $packagesNotes = Mage::getModel('dyna_checkout/packageNotes')
                    ->getCollection()
                    ->addFieldToFilter('package_id', $package->getEntityId());

                if ($postData['noteId']) {
                    $packagesNotes->addFieldToFilter('entity_id', $postData['noteId']);
                }

                foreach ($packagesNotes as $packageNote) {
                    $lastUpdateAgentDetails = json_decode($packageNote->getCreatedAgent());
                    foreach ($lastUpdateAgentDetails as $key => $value) {
                        $packageNote->setData('agent_' . $key, $value);
                    }
                    $packageStatus = $packageNote->getCreatedDate() == $packageNote->getLastUpdateDate() ? strtoupper($this->__("Created by")) : strtoupper($this->__("Last edited by"));
                    $packageNote->setPackageAgentStatus($packageStatus);
                    $packageNote->setCreatedDate(date('d-m-Y H:i', strtotime($packageNote->getCreatedDate())));
                    $packageNote->setLastUpdateDate(date('d-m-Y H:i', strtotime($packageNote->getLastUpdateDate())));
                    $packageNote->setData('note', htmlspecialchars($packageNote->getData('note')));
                    $response['notes'][] = $packageNote->getData();
                }

                $this->setJsonResponse($response);
                return;
            } catch (\Exception $e) {
                Mage::logException($e);
                $this->setJsonResponse([
                    'error' => true,
                    'message' => $e->getMessage()
                ]);
            }
        }
    }

    /**
     * @param $result
     */
    protected function setJsonResponse($result)
    {
        $this->getResponse()->setHeader('Content-Type', 'application/json');
        $this->getResponse()->setBody(json_encode($result));
        $this->getResponse()->setHttpResponseCode(200);
    }

    /*
     * @return response from retain telephone number service
     */
    public function retainTelephoneNumberAction()
    {
        $quoteId = Mage::getSingleton('checkout/cart')->getQuote()->getId();
        $quote = Mage::getModel('sales/quote')->load($quoteId);
        $custId = $this->getRequest()->getParam('custId');
        $telephoneNumber = $this->getRequest()->getParam('telephonenumber');
        $helper = Mage::helper('vznl_retainTelephoneNumbers');
        $serviceAddress = unserialize($quote->getServiceAddress());
        $footprint = $quote->getFootprint();
        $validateAddress = array(
            "houseFlatNumber" => isset($serviceAddress["housenumber"]) ? $serviceAddress["housenumber"] : '',
            "houseFlatExt" => isset($serviceAddress["housenumberaddition"]) ? $serviceAddress["housenumberaddition"] : '',
            "postCode" => isset($serviceAddress["postalcode"]) ? $serviceAddress["postalcode"] : '',
            "addressId" => isset($serviceAddress["addressId"]) ? $serviceAddress["addressId"] : '',
            "footPrint" => $footprint,
            "telephoneNumber" => $telephoneNumber,
            "customer_id" => $custId
        );
        $response = $helper->retainTelephoneNumbers($validateAddress);
        if (!$response) {
            $response = "Unable to fetch fixed telephone numbers";
        } elseif (isset($response['statusDescription'])) {
            $response['statusDescription'] = str_replace('No telephone number found to retain for customer', $this->__('No telephone number found to retain for customer'), $response['statusDescription']);
        }
        if (isset($response['error'])) {
            $pealErrorCode = Mage::getModel('validatecart/errorcode')->getByCode($response["code"],$response['message'],Vznl_RetainTelephoneNumbers_Adapter_Peal_Adapter::name);
            $response = array(
                'error' => true,
                'message' => $pealErrorCode['translation'],
            );
        }
        $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
            Mage::helper('core')->jsonEncode($response)
        );
        return;
    }

    /*
     * IBAN Black list Check
     * @return array
     */
    public function ibanBlacklistCheckAction()
    {
        $response = ['valid' => true];
        if ($this->getRequest()->isPost('iban')) {
            $blacklist_ibans = Mage::getConfig()->getNode('default/checkout/blacklistediban/numbers');
            $blacklist_ibans = explode("\r\n", $blacklist_ibans);
            if (in_array($this->getRequest()->getParam('iban'), $blacklist_ibans)) {
                $response['valid'] = false;
            }
        }
        $this->setJsonResponse($response);
    }

    /**
     * @param $quote
     * @return string
     */
    public function getCartEmailRetryButton($quote)
    {
        return $quote->getCartStatus() != Omnius_Checkout_Model_Sales_Quote::CART_STATUS_RISK ? '<div class="btn" onclick="showCartEmailButton('.$quote->getID().')">Retry</div>'  : '&nbsp;';
    }

    /**
     * create a workitem for mobile address in move process context
     * @param int $orderId
     */
    public function moveWorkitemAction()
    {
        $orderIds = Mage::getSingleton('core/session')->getOrderIds();
        $lastOrderId = reset($orderIds);
        $lastOrder = Mage::getModel('sales/order')->load($lastOrderId, 'increment_id');
        $quote = Mage::getModel('sales/quote')->load($lastOrder->getQuoteId());

        try {
            /** @var Vznl_Service_Model_Client_PortalInternalClient $client */
            if (!Mage::helper('vznl_service')->useStubs()) {
                $client=Mage::helper('vznl_service')->getClient('portal_internal');
                $client->moveMobileAddress(
                    $this->_getCustomerSession()->getCustomer(),
                    $quote,
                    $this->_getCustomerSession()->getAgent(true)
                );
            }
            $result=array(
                'error'=>false,
                'message'=>$this->__("The workitem has been sent successfully. A backoffice agent will restore the link and its benefits as soon as possible")
            );
        } catch (LogicException $e) {
            $result=array(
                'error'=>true,
                'message'=>$this->__('An error has occured and the workitem has not been sent please create a support ticket'),
            );
        }
        $this->getResponse()
            ->setHeader('Content-Type', 'application/json')
            ->setBody(
                Mage::helper('core')->jsonEncode($result)
            );
    }

    /**
     * Load up fixed package
     * @param Vznl_Package_Model_Package $package
     * @param Vznl_Checkout_Model_Sales_Quote $newQuote
     * @return Vznl_Package_Model_Package
     */
    private function _updateFixedPackageContents($package, $quote)
    {
        if ($package->isFixed() && $quote->getCustomer()) {
            // Get customer ID
            $customerPan = $quote->getCustomer()->getPan();

            // Set order type
            $orderType = Vznl_Configurator_Model_PackageType::ORDER_TYPE_PRODUCT;
            $quote->setOrderType($orderType);

            // Get service address
            $serviceAddress = unserialize($quote->getServiceAddress());

            // Check if address is different
            $crossFootPrint = Mage::helper('vznl_configurator/inlife')->isCustomerMovingToDifferentAddress($orderType);

            // Get basket
            $data = Mage::helper('vznl_getbasket')->getBasket($customerPan, $serviceAddress['addressId'], $crossFootPrint, $orderType);

            // Read in basket ID
            $basketId = null;
            if ($data) {
                if ($data["basketId"]) {
                    $basketId = $data["basketId"];
                    Mage::helper('vznl_validatecart')->setBasketId($basketId);
                }
            }

            // Consume reset basket API to retrive the installed base
            $resetBasketResponse = Mage::helper('vznl_resetbasket')->resetBasket($basketId);

            $resultArray = array();
            $resultArray = Mage::helper('vznl_validatecart')->getResultArray($resetBasketResponse['itemsList'], $resultArray);
            $resultArray["basketId"] = $basketId;
            Mage::getSingleton('core/session')->setResetResultArray($resultArray);
            Mage::log(" BasketId : ".$basketId." : ". json_encode($resultArray) . "\n\n", null, 'resultilsarray.log');

            $oseItemList = array();
            $productSkus = array();
            $hardwareDetail = array();

            $technicalIds = Mage::helper('vznl_resetbasket')->getOseItemList($resetBasketResponse['itemsList'], $oseItemList);
            foreach ($technicalIds as $technicalId => $value) {
                $commercialSkus = Mage::getModel('dyna_multimapper/mapper')->getSkusForSoc($technicalId, $productSkus);
                $productModel = Mage::getModel('catalog/product');
                $productSkus = array_merge($productSkus, $commercialSkus);
                if (isset($commercialSkus[0])) {
                    foreach ($commercialSkus as $productSku) {
                        $productId = $productModel->getIdBySku($productSku);
                        $hardwareDetail[$productSku]=$value;
                        $hardwareDetail[$productSku]["productId"]=$productId;
                    }
                }
            }

            Mage::register('hardware_details_ils', $hardwareDetail);
            Mage::getSingleton('customer/session')->setHardwareDetailsIls($hardwareDetail);

        }
        return $package;
    }

    /**
     * Check if products have expired or if they were bundle component remove them
     * @param $quoteItemCollection
     * @param $quote
     */
    protected function checkItemQuotes($quoteItemCollection, $quote)
    {
        $websiteId = Mage::app()->getWebsite()->getId();
        $today = date("Y-m-d H:i:s");
        foreach ($quoteItemCollection->getData() as $quoteItem) {
            $productQuote = Mage::getModel('catalog/product')->load($quoteItem['product_id']);
            $expirationDate = $productQuote->getData('expiration_date') ?: null;
            $effectiveDate = $productQuote->getData('effective_date') ?: null;
            $productStatus = $productQuote->getData('status') ?: null;
            $websites = $productQuote->getWebsiteIds();
            $allowedWebsite = in_array($websiteId, $websites);
            if ($productQuote) {
                if ((isset($expirationDate) && $expirationDate <= $today) || (isset($effectiveDate) && $effectiveDate >= $today) ||
                    ($productStatus == Mage_Catalog_Model_Product_Status::STATUS_DISABLED) || (!$allowedWebsite)) {
                    $packagesQuote = Mage::getModel('sales/quote_item')->getCollection()
                        ->addFieldToFilter('quote_id', $quote->getId())
                        ->addFieldToFilter('package_id', $quoteItem['package_id']);
                    $itemIds = [];
                    foreach ($packagesQuote->getData() as $packageQuote) {
                        array_push($itemIds, $packageQuote['item_id']);
                    }
                    $itemIdsList = [];
                    foreach ($packagesQuote->getData() as $packageQuote) {
                        array_push($itemIdsList, $packageQuote['item_id']);
                    }
                    foreach ($itemIds as $itemId) {
                        if (in_array($itemId, $itemIdsList)) {
                            $productQuoteDelete = Mage::getModel('sales/quote_item')->load($itemId);
                            $productQuoteDelete->delete();
                            Mage::getSingleton('customer/session')->setFlagStatusForShopping(true);
                        }
                    }
                }
                //remove bundle components when loading saved cart
                if (!$productQuote->isSubscription() && $quoteItem['bundle_component'] >= 0 && !is_null($quoteItem['bundle_component'])) {
                    $quote->removeItem($quoteItem['item_id']);
                }

                //parse quote items to clean remaining bundle_components that were set
                foreach ($quote->getAllItems() as $item) {
                    if ($item->getBundleComponent()) {
                        $item->setBundleComponent(null);
                    }
                }
            }
        }
    }
    /**
    *
    */
    public function validateEanAction()
    {
        $eanNumber = $this->getRequest()->getParam('eanNumber');
        $eanExist = Mage::getModel('catalog/product')
           ->getCollection()
           ->addAttributeToFilter('identifier_ean_or_upc_code', array(array('like' => '%'.$eanNumber.'%')))->getLastItem()->getidentifierEanOrUpcCode();
        if ($eanExist)
            $response = 'exist';
        else
            $response = 'notExist';
        $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
            Mage::helper('core')->jsonEncode($response)
        );
    }

    private function cleanLastName(string $lastname, string $middlename) : string
    {
        $middlename = strtolower($middlename);
        $lastname = strtolower($lastname);
        $toRemove = ['female', 'mevr', 'mevr.', 'mevrouw', 'mrs.', 'male', 'meneer', 'heer', 'dhr', 'dhr.', 'mr.', ' ' . $middlename . ' ', '. '];
        $lastname = str_replace($toRemove, '', $lastname);

        return $lastname;
    }
}
