<?php

class Vznl_Checkout_Adminhtml_ActivationreasonController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('system/activation_reason')
            ->_addBreadcrumb(Mage::helper('vznl_checkout')->__('Manual activation reasons'), Mage::helper('vznl_checkout')->__('Manual activation reasons'));

        return $this;
    }

    /**
     * Display the admin grid (table) with all the welcome messages
     */
    public function indexAction()
    {
        $this->_initAction()->renderLayout();
    }

    /**
     * Gather data and generate edit form
     *
     */
    public function editAction()
    {
        $id = $this->getRequest()->getParam('entity_id');
        /** @var Dyna_Bundles_Model_Package $model */
        $model = Mage::getModel('vznl_checkout/reason')->load($id);

        if ($model->getEntityId() || $id == 0) {
            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
            if (!empty($data)) {
                $model->addData($data);
            }

            Mage::register('reason_data', $model);

            $this->loadLayout();
            $this->_setActiveMenu('system/activation_reason');

            $this->_addContent($this->getLayout()->createBlock('vznl_checkout/adminhtml_activationreason_edit'))
                ->_addLeft($this->getLayout()->createBlock('vznl_checkout/adminhtml_activationreason_edit_tabs'));

            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('vznl_checkout')->__('Item does not exist'));
            $this->_redirect('*/*/');
        }
    }

    /**
     * Admin add a new Announcement, forwards to edit action
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            try {
                $id = $this->getRequest()->getParam('entity_id');
                /** @var Dyna_Bundles_Model_Category $model */
                $model = Mage::getModel('vznl_checkout/reason')->load($id);
                $model->addData($data);
                $model->save();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('vznl_checkout')->__('The reason was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', ['entity_id' => $model->getEntityId()]);

                    return;
                }
            } catch (Exception $e) {
                $this->_throwErrror($e->getMessage(), $data);

                return;
            }
            $this->_redirect('*/*/');

            return;
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('vznl_checkout')->__('Unable to find item to save'));
        $this->_redirect('*/*/');
    }

    /**
     * Method that deleted a operator code
     *
     */
    public function deleteAction()
    {
        $id = $this->getRequest()->getParam('entity_id');
        if ($id > 0) {
            try {
                $model = Mage::getModel('vznl_checkout/reason')->load($id);
                if (!$model->getId()) {
                    throw new Exception('Activation Reason with the provided ID not found');
                }
                $model->delete();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('vznl_checkout')->__('Item was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit',
                    array('entity_id' => $id));
            }
        }
        $this->_redirect('*/*/');
    }

    //Patch for SUPEE-6285
    protected function _isAllowed()
    {
        return true;
    }
}
