<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Dyna
 * @package     Vznl_Checkout
 */

/**
 * Class Observer
 */
class Vznl_Checkout_Model_Observer extends Mage_Payment_Model_Observer
{

    /**
     * Set forced canCreditmemo flag
     *
     * @param Varien_Event_Observer $observer
     * @return Mage_Payment_Model_Observer
     */
    public function salesOrderBeforeSave($observer)
    {
        /** @var Vznl_Checkout_Model_Sales_Order $order */
        $order = $observer->getEvent()->getOrder();
        $isNotFree = $order->getPayment() && $order->getPayment()->getMethodInstance() && ($order->getPayment()->getMethodInstance()->getCode() != 'free');
        $isNotClosed = $order->isCanceled() || ($order->getState() === Mage_Sales_Model_Order::STATE_CLOSED);
        /**
         * Allow forced creditmemo just in case if it wasn't defined before
         */
        if (!$isNotFree && !$order->canUnhold() && !$isNotClosed && !$order->hasForcedCanCreditmemo()) {
            $order->setForcedCanCreditmemo(true);
        }

        return $this;
    }

    /**
     * @param $observer
     * @return $this
     */
    public function receiveAdyenPayment($observer)
    {
        $invoice = $observer->getInvoice();
        $order = $invoice->getOrder();

        // Check for adyen payment
        if ($order->getPayment()->getMethodInstance()->getCode() === 'adyen_hpp') {
            $superorder = Mage::getModel('superorder/superorder')->load($order->getSuperorderId());
            Mage::log("Adyen payment received.\nSubmit order at: " . $superorder->getOrderNumber(), null, "adyen.log");

            Mage::app()->setCurrentStore(Mage::getModel('core/website')->load(Vznl_Agent_Model_Website::WEBSITE_WEB_SHOP_CODE)->getDefaultStore()->getId());
            Mage::helper('vznl_order')->submitOrder($superorder->getOrderNumber());
        }

        return $this;
    }

    /**
     * Removes previous service items
     * @param $observer
     */
    public function removeServiceItems($observer)
    {
        $quote = $observer->getQuote();
        $existing = array();
        $quoteItems = $quote->getAllItems();
        foreach ($quoteItems as $i) {
            $existing[$i->getPackageId()][] = $i->getProductId();
        }

        foreach ($quoteItems as $item) {
            if ($item->getProduct()->isServiceItem() && !in_array($item->getTargetId(), $existing[$item->getPackageId()])) {
                $quote->removeItem($item->getId());
            }
        }
    }

    /**
     * Rewrites the order of the Adyen total collectors.
     * @param $observer
     */
    public function rewritePaymentFeeCalculation($observer)
    {
        $config = Mage::getConfig();
        $adyenNode = $config->getNode()->xpath('//modules/Adyen_Payment/active');
        if (is_array($adyenNode) && count($adyenNode)) {
            $adyenNode = trim((string)$adyenNode[0]);
            if ($adyenNode == 'true') {
                $paymentFeeNode = $config->getNode()->xpath('global/sales/quote/totals/payment_fee');
                unset($paymentFeeNode[0]->before);
                $paymentFeeNode[0]->after[0] = 'maf_grand_total';
            }
        }
    }
}
