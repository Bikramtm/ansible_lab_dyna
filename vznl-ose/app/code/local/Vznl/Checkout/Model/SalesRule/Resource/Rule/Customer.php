<?php

/**
 * Class Vznl_Checkout_Model_SalesRule_Resource_Rule_Customer
 */
class Vznl_Checkout_Model_SalesRule_Resource_Rule_Customer extends Mage_SalesRule_Model_Resource_Rule_Customer
{
    /**
     * Get rule usage record for a customer
     *
     * @param Mage_SalesRule_Model_Rule_Customer $rule
     * @param int $customerId
     * @param int $ruleId
     * @return Mage_SalesRule_Model_Resource_Rule_Customer
     */
    public function loadByCustomerRule($rule, $customerId, $ruleId)
    {
        $key = sprintf('salesrule_customer_rule_%s_%s', $customerId, $ruleId);
        if (null === Mage::registry($key)) {
            $read = $this->_getReadAdapter();
            $select = $read->select()->from($this->getMainTable())
                ->where('customer_id = :customer_id')
                ->where('rule_id = :rule_id');
            $data = $read->fetchRow($select, array(':rule_id' => $ruleId, ':customer_id' => $customerId));
            if (false === $data) {
                // set empty data, as an existing rule object might be used
                $data = array();
            }
            Mage::unregister($key);
            Mage::register($key, $data);
        } else {
            $data = Mage::registry($key);
        }
        $rule->setData($data);
        return $this;
    }
}
