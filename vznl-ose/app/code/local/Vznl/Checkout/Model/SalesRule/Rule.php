<?php

/**
 * Class Vznl_Checkout_Model_SalesRule_Discount
 */
class Vznl_Checkout_Model_SalesRule_Rule extends Omnius_Checkout_Model_SalesRule_Rule
{
    public function resetRuleAddress(){
        $this->_validatedAddresses = array();
    }

    /** @var Vznl_Configurator_Model_Cache */
    protected $_cache;

    /**
     * Get sales rule customer group Ids
     *
     * @return array
     */
    public function getCustomerGroupIds()
    {
        if (!$this->hasCustomerGroupIds()) {
            $key = sprintf('salesrule_customer_groups_%s', $this->getId());
            if ($result = ((array)unserialize($this->getCache()->load($key)))) {
                $this->setData('customer_group_ids', $result);
                return $result;
            } else {
                $result = (array) $this->_getResource()->getCustomerGroupIds($this->getId());
                $this->setData('customer_group_ids', $result);
                $this->getCache()->save(serialize($result), $key, array(Mage_Customer_Model_Customer::CACHE_TAG), $this->getCache()->getTtl());
                return $result;
            }
        }
        return $this->_getData('customer_group_ids');
    }

    /**
     * Set if not yet and retrieve rule store labels
     *
     * @return array
     */
    public function getStoreLabels()
    {
        if (!$this->hasStoreLabels()) {
            $key = sprintf('salesrule_store_labels_%s', $this->getId());
            if ($labels = (unserialize($this->getCache()->load($key)))) {
                $this->setStoreLabels($labels);
                return $labels;
            } else {
                $labels = $this->_getResource()->getStoreLabels($this->getId());
                $this->setStoreLabels($labels);
                $this->getCache()->save(serialize($labels), $key, array(Vznl_Configurator_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
                return $labels;
            }
        }

        return $this->_getData('store_labels');
    }

    /**
     * @return Vznl_Configurator_Model_Cache
     */
    protected function getCache()
    {
        if ( ! $this->_cache) {
            $this->_cache = Mage::getSingleton('vznl_configurator/cache');
        }
        return $this->_cache;
    }

    /**
     * Assure item has an unique_id
     *
     * @return Vznl_Checkout_Model_SalesRule_Rule
     */
    protected function _beforeSave()
    {
        if ( ! $this->getData('unique_id')) {
            $this->setData('unique_id', hash('sha256', (time() . rand(0, 100) . __FILE__)));
        }
        $promoLabels = $this->getData('promotion_label');
        if (is_array($promoLabels)) {
            $this->setData('promotion_label', implode(',', $promoLabels));
        }
        return parent::_beforeSave();
    }

    /**
     * Save method triggers many other events so in case you only need to update a field that should not
     * affect other data, this function can be used as it is much faster
     */
    public function updateFields($data)
    {
        if($data && $this->getId()) {
            $write = Mage::getSingleton("core/resource")->getConnection("core_write");

            foreach ($data as $field => $value) {
            	$this->setData($field, $value);
            }
            
            $write->update(
            	'salesrule',
            	$data,
            	array('rule_id = ?' => $this->getId())
            );
        }
    }
}
