<?php
/**
 * Discount calculation model
 *
 * @category    Dyna
 * @package     Vznl_Checkout
 */
class Vznl_Checkout_Model_SalesRule_Quote_Discount extends Dyna_Checkout_Model_SalesRule_Quote_Discount
{
    /**
     * @var Dyna_PriceRules_Model_Validator
     */
    protected $_calculator;

    protected $_promos = null;
    protected $_activePackageId = null;

    /**
     * Initialize discount collector
     */
    public function __construct()
    {
        $this->setCode('maf_discount');
        $this->_calculator = Mage::getSingleton('salesrule/validator');
    }

    /**
     * @param Mage_Sales_Model_Quote_Address $address
     * @param $items
     */
    protected function collectProcessPrices(Mage_Sales_Model_Quote_Address $address, $items)
    {
        //skip levy product if found
        $levyAddonId = Mage::getResourceSingleton('catalog/product')->getIdBySku(Mage::getStoreConfig(Dyna_PriceRules_Model_Observer::COPY_LEVY_PREFIX . 'standaard'));
        foreach ($items as $item) {
            if ($item->getProduct()->is(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE) && $item->getProductId() !== $levyAddonId) {
                $prices = $this->getCheckoutHelper()->updateMixMatches($address->getQuote(), $item->getPackageId(), $item);

                $item->setMixmatchSubtotal(isset($prices['subtotal']) ? $prices['subtotal'] : null)
                    ->setMixmatchTax(isset($prices['tax']) ? $prices['tax'] : null)
                    ->setMixmatchMafSubtotal(isset($prices['maf_subtotal']) ? $prices['maf_subtotal'] : null)
                    ->setMixmatchMafTax(isset($prices['maf_tax']) ? $prices['maf_tax'] : null);
            }
        }
    }

    /**
     * Collect address discount amount
     *
     * @param   Mage_Sales_Model_Quote_Address $address
     * @return  Mage_SalesRule_Model_Quote_Discount
     * @throws Mage_Core_Model_Store_Exception
     */
    public function collect(Mage_Sales_Model_Quote_Address $address)
    {
        // Remember the active package in the beginning as it might be changed along the way
        $this->_activePackageId = Mage::getSingleton('checkout/cart')->getQuote()->getActivePackageId();
        $conn = Mage::getSingleton('core/resource')->getConnection('core_write');
        $this->_setAddress($address);
        /**
         * Reset amounts
         */
        $this->_setMafAmount(0);
        $this->_setBaseMafAmount(0);

        $items = $this->_getAddressItems($address);
        if (!count($items)) {
            return $this;
        }

        $quote = $address->getQuote();
        $couponsPerPackage = $quote->getCouponsPerPackage();
        // Remember the previous active package id.
        $prevActivePackageId = $quote->getActivePackageId();
        $store = Mage::app()->getStore($quote->getStoreId());

        $packages = $quote->getPackageModels();

        $this->_calculator->init($store->getWebsiteId(), $quote->getCustomerGroupId(), '', $packages, $address);

        foreach ($items as $item) {
            $packageModel = $packages->getItemByColumnValue('package_id', $item->getPackageId());

            //workaround for packages having id as ctn - to be removed
            if (!$packageModel || !$packageModel->getId()) {
                $packageModel = $packages->getItemByColumnValue('ctn', $item->getPackageId());
            }
            if (!$packageModel) {
                continue;
            }

            $quote->setActivePackageId($item->getPackageId());
            $coupon = isset($couponsPerPackage[$item->getPackageId()]) ? $couponsPerPackage[$item->getPackageId()] : $packageModel->getCoupon();
            $this->_calculator
                ->setActivePackage($packageModel)
                ->setCouponCode($coupon)
                ->setWebsiteId($store->getWebsiteId())
                ->setCustomerGroupId($quote->getCustomerGroupId())
            ;


            $eventArgs = array(
                'website_id' => $store->getWebsiteId(),
                'customer_group_id' => $quote->getCustomerGroupId(),
                'coupon_code' => $packageModel->getCoupon(),
            );

            $eventArgs['item'] = $item;
            Mage::dispatchEvent('sales_quote_address_discount_item', $eventArgs);

            if ($item->getHasChildren() && $item->isChildrenCalculated()) {
                foreach ($item->getChildren() as $child) {
                    $this->_calculator->process($child);
                }
            } else {
                $this->_calculator->process($item);
            }
        }

        $address->unsetData('cached_items_nonnominal');
        $address->unsetData('cached_items_all');
        $address->unsetData('cached_items_nominal');
        $this->_initPromos($address);

        foreach($items as $item) {
            if ($item->getHasChildren() && $item->isChildrenCalculated()) {
                foreach ($item->getChildren() as $child) {
                    $this->process($child);
                    $this->processMaf($child);
                    $this->_aggregateItemDiscount($child);
                }
            } else {
                $this->process($item);
                $this->processMaf($item);
                $this->_aggregateItemDiscount($item);
            }
        }

        $this->collectProcessPrices($address, $items);

        $quote->setActivePackageId($prevActivePackageId);

        $itemIds = array();
        foreach ($packages as $pack) {
            if (
                isset($this->_calculator->couponsApplied[$pack->getPackageId()])
                && $this->_calculator->couponsApplied[$pack->getPackageId()] != $pack->getCoupon()
            ) {
                Mage::helper('pricerules')->incrementCoupon($this->_calculator->couponsApplied[$pack->getPackageId()], $quote->getCustomer()->getId());
                $pack->setCoupon($this->_calculator->couponsApplied[$pack->getPackageId()])->save();
            } elseif (
                !isset($this->_calculator->couponsApplied[$pack->getPackageId()])
                && $pack->getCoupon() != null
            ) {
                Mage::helper('pricerules')->decrementCoupon($pack->getCoupon(), $quote->getCustomer()->getId());
                $itemIds[] = $pack->getId();
            }
        }
        if (count($itemIds)) {
            $conn->update('catalog_package', array('coupon' => null, 'updated_at' => now()), sprintf('entity_id in (%s)', join(',', $itemIds)));
        }

        return $this;
    }

    /**
     * Quote item discount calculation process
     *
     * @param Mage_Sales_Model_Quote_Item_Abstract $item
     * @return Vznl_Checkout_Model_SalesRule_Quote_Discount
     */
    public function process(Mage_Sales_Model_Quote_Item_Abstract $item)
    {
        $quote = $item->getQuote();
        if ($item->getPackageId() != $this->_activePackageId){
            return $this;
        }
        if ($item->isPromo() || $item->isBundlePromo()) {
            return $this;
        }

        $item->setDiscountAmount(0);
        $item->setBaseDiscountAmount(0);

        //if item is not yet persisted (no id) and aparently has price 0, check for safety the price of the product
        $itemPriceInclTax = (!$item->getId() && !$item->getPriceInclTax()) ? $item->getProduct()->getPrice() : $item->getPriceInclTax();
        $itemPrice = $itemPriceInclTax ?: $this->_getItemPrice($item);

        if ($itemPrice < 0) {
            return $this;
        }

        $packageType = $item->getQuote()->getPackageById($item->getPackageId())->getSaleType();
        $connectionFeeAttribute = 'prijs_aansluitkosten';
        if (in_array(
            $packageType,
            [Vznl_Catalog_Model_ProcessContext::RETBLU, Vznl_Catalog_Model_ProcessContext::RETNOBLU]
        )) {
            $connectionFeeAttribute = 'prijs_aansluitkosten_ret';
        }
        $connectionCost = $item->getProduct()->getData($connectionFeeAttribute);

        $discount = $item->getProduct()->getPriceDiscount();
        $quoteAmount = $quote->getStore()->convertPrice($discount);

        $qty = $item->getTotalQty();

        if (isset($this->_promos[$item->getPackageId()]) && Mage::helper('dyna_catalog')->is([
                Vznl_Catalog_Model_Type::SUBTYPE_ADDON,
                Vznl_Catalog_Model_Type::SUBTYPE_FIXED_HARDWARE_MATERIALS,
                Vznl_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION,
                Vznl_Catalog_Model_Type::SUBTYPE_DEVICE_SUBSCRIPTION,
                Vznl_Catalog_Model_Type::SUBTYPE_FIXED_PACKAGE_DETAILS,
            ], $item->getProduct())
        ) {
            foreach ($this->_promos[$item->getPackageId()] as $promo) {
                $promoProd = $promo->getProduct();
                $unitDiscount = 0;
                if ($promo->getTargetId() == $item->getProductId()) {
                    if ($promoProd->getData('prijs_aansluit_promo_bedrag')) {
                        if ($promoProd->getData('prijs_aansluit_promo_bedrag')
                            < $item->getProduct()->getData($connectionFeeAttribute)
                        ) {
                            $quoteAmount = $promoProd->getData('prijs_aansluit_promo_bedrag');
                            $connectionCost = $qty * $quoteAmount;
                        }
                    } elseif ($promoProd->getData('prijs_aansluit_promo_procent')) {
                        if ($promoProd->getData('prijs_aansluit_promo_procent') > 0
                            && $promoProd->getData('prijs_aansluit_promo_procent') <= 100
                        ) {
                            $connectionCost = ($qty * $item->getProduct()->getData($connectionFeeAttribute))
                                * (float)$promoProd->getData('prijs_aansluit_promo_procent') / 100;
                        }
                    } elseif ($promoProd->getPriceDiscount()
                        || $promoProd->getPriceDiscountPercent()
                        || $promoProd->getPromoNewPrice()
                    ) {
                        $unitDiscount = $this->getUnitPriceDiscount($item, $promo, $promoProd, $itemPrice, $qty);
                    }
                }

                if ($unitDiscount > 0) {
                    $discount += $unitDiscount;
                    $promo->setAppliedPromoAmount($quote->getStore()->roundPrice(
                        $quote->getStore()->convertPrice($unitDiscount))
                    );
                    $promo->setBaseAppliedPromoAmount($quote->getStore()->roundPrice($unitDiscount));
                }
            }

            $quoteDiscountAmount = $quote->getStore()->convertPrice($discount);
        }

        $connectionCost = min($connectionCost, $item->getProduct()->getData($connectionFeeAttribute) * $qty);
        $item->setConnectionCost($connectionCost);

        $discountAmount = $qty * ($quoteDiscountAmount ?? 0);
        $baseDiscountAmount = $qty * $discount;
        $discountAmount = $quote->getStore()->roundPrice($discountAmount);
        $baseDiscountAmount = $quote->getStore()->roundPrice($baseDiscountAmount);

        /**
         * We can't use row total here because row total not include tax
         * Discount can be applied on price included tax
         */

        $itemDiscountAmount = $item->getDiscountAmount();
        $itemBaseDiscountAmount = $item->getBaseDiscountAmount();
        $baseItemPrice = $baseItemPrice ?? 0;

        $discountAmount = min($itemDiscountAmount + $discountAmount, $itemPrice * $qty);
        $baseDiscountAmount = min($itemBaseDiscountAmount + $baseDiscountAmount, $baseItemPrice * $qty);

        // setting price discounts on item
        $item->setDiscountAmount($discountAmount);
        $item->setBaseDiscountAmount($baseDiscountAmount);

        return $this;
    }

    public function processMaf(Mage_Sales_Model_Quote_Item_Abstract $item)
    {
        $item->setMafDiscountAmount(0);
        $item->setBaseMafDiscountAmount(0);
        $quote = $item->getQuote();

        $itemMaf = $this->_getItemMaf($item);
        $baseItemPrice = $this->_getItemBaseMaf($item);

        if ($itemMaf < 0) {
            return $this;
        }

        $discount = $item->getProduct()->getDiscountMaf();

        $allowedSubType = array(
            Vznl_Catalog_Model_Type::SUBTYPE_ADDON,
            Vznl_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION,
            Vznl_Catalog_Model_Type::SUBTYPE_DEVICE_SUBSCRIPTION,
            Vznl_Catalog_Model_Type::SUBTYPE_FIXED_PACKAGE,
            Vznl_Catalog_Model_Type::SUBTYPE_FIXED_PACKAGE_DETAILS,
            Vznl_Catalog_Model_Type::SUBTYPE_FIXED_HARDWARE_MATERIALS,
            Vznl_Catalog_Model_Type::SUBTYPE_FIXED_PACKAGE_ADDONS
        );

        if(isset($this->_promos[$item->getPackageId()]) &&
            Mage::helper('vznl_catalog')->is($allowedSubType, $item->getProduct())) {

            $discount = 0;
            $qty = $item->getTotalQty();

            $newAmountPromoCounter = 0;
            /** @var Vznl_Checkout_Model_Sales_Quote_Item $promo */
            foreach ($this->_promos[$item->getPackageId()] as $promo) {
                $promoProd = $promo->getProduct();
                $unitDiscount = $this->getUnitDiscount($item, $promo, $promoProd, $itemMaf, $qty);
                if ($unitDiscount) {
                    // if this is the second (or more) discount of a new amount promo,
                    // remove the old discount and apply the new one
                    if ($promoProd->getPrijsMafNewAmount() && ($newAmountPromoCounter > 0) && (!empty($lastNewAmount))) {
                        $discount = ($discount - $lastNewAmount) + $unitDiscount;
                    } else {
                        $discount += $unitDiscount;
                    }
                    $lastNewAmount = $promoProd->getPrijsMafNewAmount() ? $unitDiscount : false;
                    // store also on promo maf
                    $promo->setAppliedMafPromoAmount($quote->getStore()->convertPrice($unitDiscount));
                    $promo->setBaseAppliedMafPromoAmount($quote->getStore()->roundPrice($unitDiscount));
                }
                if ($promoProd->getPrijsMafNewAmount()) {
                    $newAmountPromoCounter++;
                }
            }
            unset($lastNewAmount,$newAmountPromoCounter);
        }

        $qty = $item->getTotalQty();

        $discountAmount = $baseDiscountAmount = $quote->getStore()->roundPrice($qty * $discount);

        /**
         * We can't use row total here because row total not include tax
         * Discount can be applied on price included tax
         */

        $itemDiscountAmount = $item->getMafDiscountAmount();
        $itemBaseDiscountAmount = $item->getBaseMafDiscountAmount();

        $discountAmount     = min($itemDiscountAmount + $discountAmount, $itemMaf * $qty);
        $baseDiscountAmount = min($itemBaseDiscountAmount + $baseDiscountAmount, $baseItemPrice * $qty);
        $item->setMafDiscountAmount($discountAmount);
        $item->setBaseMafDiscountAmount($baseDiscountAmount);

        return $this;
    }

    /**
     * Return item price
     *
     * @param Mage_Sales_Model_Quote_Item_Abstract $item
     * @return float
     */
    protected function _getItemMaf($item)
    {
        $price = $item->getDiscountCalculationMaf();
        $calcPrice = $item->getCalculationMaf();
        return ($price !== null) ? $price : $calcPrice;
    }

    /**
     * Return item base price
     *
     * @param Mage_Sales_Model_Quote_Item_Abstract $item
     * @return float
     */
    protected function _getItemBaseMaf($item)
    {
        $price = $item->getDiscountCalculationMaf();
        return ($price !== null) ? $item->getBaseDiscountCalculationMaf() : $item->getBaseCalculationMaf();
    }

    /**
     * Return item price
     *
     * @param Mage_Sales_Model_Quote_Item_Abstract $item
     * @return float
     */
    protected function _getItemPrice($item)
    {
        $price = $item->getDiscountCalculationPrice();
        $calcPrice = $item->getCalculationPrice();
        return ($price !== null) ? $price : $calcPrice;
    }

    /**
     * Return item base price
     *
     * @param Mage_Sales_Model_Quote_Item_Abstract $item
     * @return float
     */
    protected function _getItemBasePrice($item)
    {
        $price = $item->getDiscountCalculationPrice();
        return ($price !== null) ? $item->getBaseDiscountCalculationPrice() : $item->getBaseCalculationPrice();
    }

    /**
     * Set total model amount value to address
     *
     * @param   float $amount
     * @return  Mage_Sales_Model_Quote_Address_Total_Abstract
     */
    protected function _setMafAmount($amount)
    {
        if ($this->_canSetAddressAmount) {
            $this->_getAddress()->setMafTotalAmount($this->getCode(), $amount);
        }
        return $this;
    }

    /**
     * Set total model base amount value to address
     *
     * @param   float $amount
     * @return  Mage_Sales_Model_Quote_Address_Total_Abstract
     */
    protected function _setBaseMafAmount($baseAmount)
    {
        if ($this->_canSetAddressAmount) {
            $this->_getAddress()->setBaseMafTotalAmount($this->getCode(), $baseAmount);
        }
        return $this;
    }

    /**
     * Aggregate item discount information to address data and related properties
     *
     * @param   Mage_Sales_Model_Quote_Item_Abstract $item
     * @return  Mage_SalesRule_Model_Quote_Discount
     */
    protected function _aggregateItemDiscount($item)
    {
        if($this->getCode() != 'maf_discount' && $item->getDiscountAmount()) {
            $this->_addAmount(-$item->getDiscountAmount());
        }
        $this->_addBaseAmount(-$item->getBaseDiscountAmount());

        $this->_addMafAmount(-$item->getMafDiscountAmount());
        $this->_addBaseMafAmount(-$item->getBaseMafDiscountAmount());
        return $this;
    }

    /**
     * Add discount total information to address
     *
     * @param   Mage_Sales_Model_Quote_Address $address
     * @return  Mage_SalesRule_Model_Quote_Discount
     */
    public function fetch(Mage_Sales_Model_Quote_Address $address)
    {
        $amount = $address->getMafDiscountAmount();
        if ($amount != 0) {
            $address->addMafTotal(array(
                'code'  => $this->getCode(),
                'title' => Mage::helper('sales')->__('Discount'),
                'value' => $amount
            ));
        }
        return $this;
    }

    /**
     * Add total model amount value to address
     *
     * @param   float $amount
     * @return  Mage_Sales_Model_Quote_Address_Total_Abstract
     */
    protected function _addMafAmount($amount)
    {
        if ($this->_canAddAmountToAddress) {
            $this->_getAddress()->addMafTotalAmount($this->getCode(),$amount);
        }
        return $this;
    }

    /**
     * Add total model base amount value to address
     *
     * @param   float $amount
     * @return  Mage_Sales_Model_Quote_Address_Total_Abstract
     */
    protected function _addBaseMafAmount($baseAmount)
    {
        if ($this->_canAddAmountToAddress) {
            $this->_getAddress()->addBaseMafTotalAmount($this->getCode(), $baseAmount);
        }
        return $this;
    }

    protected function _initPromos($address) {
        $this->_promos = array();
        $items = $this->_getAddressItems($address);
        foreach ($items as $item) {
            if((!$item->isDeleted() && $item->isPromo() || (!$item['is_deleted'] && $item['is_promo']))) {
                $this->_promos[$item->getPackageId()][] = $item;
            }
        }

        return $this->_promos;
    }

    /**
     * @param Mage_Sales_Model_Quote_Item_Abstract $item
     * @param $promo
     * @param $promoProd Vznl_Catalog_Model_Product
     * @param $itemPrice
     * @param $qty
     * @return float|int
     */
    protected function getUnitDiscount(Mage_Sales_Model_Quote_Item_Abstract $item, $promo, $promoProd, $itemPrice, $qty)
    {
        $unitDiscount = 0;

        if ($promo->getTargetId() == $item->getProductId()) {
            if ($promoProd->getMafDiscount()) {
                if ($promoProd->getMafDiscount() < $itemPrice) {
                    $unitDiscount = $promoProd->getMafDiscount() * $qty;
                } else {
                    $unitDiscount = $itemPrice;
                }
            } elseif ($promoProd->getPrijsMafNewAmount()) {
                if($promoProd->getPrijsMafNewAmount() < $itemPrice) {
                    $unitDiscount = ($itemPrice - $promoProd->getPrijsMafNewAmount()) * $qty;
                }
            } elseif ($promoProd->getMafDiscountPercent()) {
                if ($promoProd->getMafDiscountPercent() > 0 && $promoProd->getMafDiscountPercent() <= 100) {
                    $unitDiscount = ($promoProd->getMafDiscountPercent() * $itemPrice * $qty) / 100;
                }
            } elseif ($promoProd->getPrijsMafDiscountPercent()) {
                if($promoProd->getPrijsMafDiscountPercent() > 0 && $promoProd->getPrijsMafDiscountPercent() <= 100) {
                    $unitDiscount = ($promoProd->getPrijsMafDiscountPercent() * $itemPrice * $qty) / 100;
                }
            }
        }
        return $unitDiscount;
    }

    /**
     * @param Mage_Sales_Model_Quote_Item_Abstract $item
     * @param $promo
     * @param $promoProd Vznl_Catalog_Model_Product
     * @param $itemPrice
     * @param $qty
     * @return float|int
     */
    protected function getUnitPriceDiscount(Mage_Sales_Model_Quote_Item_Abstract $item, $promo, $promoProd, $itemPrice, $qty)
    {
        $unitDiscount = 0;

        if ($promo->getTargetId() == $item->getProductId()) {
            if($promoProd->getPriceDiscount()) {
                if ($promoProd->getPriceDiscount() < $itemPrice) {
                    $unitDiscount = $qty * (float)$promoProd->getPriceDiscount();
                } else {
                    $unitDiscount = $itemPrice;
                }
            } elseif ($promoProd->getPriceDiscountPercent()) {
                $percentDiscount = (int)$promoProd->getPriceDiscountPercent();
                if ($percentDiscount > 0 && $percentDiscount <= 100) {
                    $unitDiscount = ((float)$qty * $itemPrice * $percentDiscount) / 100;
                }
            } elseif ($promoProd->getPromoNewPrice()) {
                if($promoProd->getPromoNewPrice() < $itemPrice) {
                    $unitDiscount = ($itemPrice - $promoProd->getPromoNewPrice()) * $qty;
                }
            }
        }

        return $unitDiscount;
    }
}
