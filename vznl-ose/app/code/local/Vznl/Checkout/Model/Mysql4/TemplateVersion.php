<?php

class Vznl_Checkout_Model_Mysql4_TemplateVersion extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init("vznl_checkout/templateVersion", "entity_id");
    }
}
