<?php
/**
 * @category    Dyna
 * @package     Vznl_Checkout
 */


class Vznl_Checkout_Model_Sales_Quote_Address_Total_Subtotal extends Mage_Sales_Model_Quote_Address_Total_Abstract
{

    /**
     * Collect address subtotal
     *
     * @param   Mage_Sales_Model_Quote_Address $address
     * @return  Mage_Sales_Model_Quote_Address_Total_Subtotal
     */
    public function collect(Mage_Sales_Model_Quote_Address $address)
    {
        $packageId = Mage::registry('add_multi_package_id');
        $this->_setAddress($address);

        /** @var Vznl_Checkout_Model_Sales_Quote $quote */
        $quote = $address->getQuote();
        /**
         * Reset amounts
         */
        $this->_setMafAmount(0);
        $this->_setBaseMafAmount(0);

        $address->setTotalQty(0);
        /** @var Dyna_Package_Model_Mysql4_Package_Collection $packages */
        $packages = Mage::getModel('package/package')->getPackages(null, $quote->getId());
        $quote->setPackageModels($packages);
        $baseVirtualAmount = $virtualAmount = 0;

        $address->unsetData('cached_items_nonnominal');
        $address->unsetData('cached_items_all');
        $address->unsetData('cached_items_nominal');
         //Process address items
        $items = $this->_getAddressItems($address);

        /** @var Dyna_PriceRules_Helper_Data $priceRulesHelper */
        $priceRulesHelper = Mage::helper('pricerules');
        Mage::unregister('remove_applied_rule_ids');
        $ruleIdsToBeRemoved = array();

        /** @var Vznl_Checkout_Model_Sales_Quote_Item $item */
        foreach ($items as $item) {
            if (
                !$item->isPromo()
                || $priceRulesHelper->promotionsOverride($item, $quote)
                || $item->getBundleComponent()
            ) {
                $valid = true;
            } else {
                $valid = false;
            }

            if ($valid && $this->_initItem($address, $item) && $item->getQty() > 0) {
                /**
                 * Separately calculate subtotal only for virtual products
                 */
                if ($item->getProduct()->isVirtual()) {
                    $virtualAmount += $item->getMafRowTotal();
                    $baseVirtualAmount += $item->getBaseMafRowTotal();
                }
            } else {
                if (!$packageId || $item->getPackageId() === $packageId) {

                    if ($item->getPackageId() == $quote->getActivePackageId()) {
                        $this->_removeItem($address, $item);

                        //also try to unset the applied rule ids
                        $ruleIdsToBeRemoved = array_merge($ruleIdsToBeRemoved, Mage::helper('vznl_pricerules')->getSkuRulesList($item->getSku()));
                    }
                }
            }
        }
        Mage::register('remove_applied_rule_ids', $ruleIdsToBeRemoved);

        $address->setBaseMafVirtualAmount($baseVirtualAmount);
        $address->setMafVirtualAmount($virtualAmount);

        return $this;
    }

    /**
     * Address item initialization
     *
     * @param  $item
     * @return bool
     */
    protected function _initItem($address, $item)
    {
        if ($item instanceof Mage_Sales_Model_Quote_Address_Item) {
            $quoteItem = $item->getAddress()->getQuote()->getItemById($item->getQuoteItemId());
        }
        else {
            $quoteItem = $item;
        }
        $product = $quoteItem->getProduct();
        $product->setCustomerGroupId($quoteItem->getQuote()->getCustomerGroupId());

        /**
         * Quote super mode flag mean what we work with quote without restriction
         */
        if ($item->getQuote()->getIsSuperMode()) {
            if (!$product) {
                return false;
            }
        }
        else {
            if (!$product || !$product->isVisibleInCatalog()) {
                return false;
            }
        }

        if ($quoteItem->getParentItem() && $quoteItem->isChildrenCalculated()) {
            $finalPrice = $quoteItem->getParentItem()->getProduct()->getPriceModel()->getChildFinalMaf(
               $quoteItem->getParentItem()->getProduct(),
               $quoteItem->getParentItem()->getQty(),
               $quoteItem->getProduct(),
               $quoteItem->getQty()
            );
            $item->setMaf($finalPrice)
                ->setBaseOriginalMaf($finalPrice);
            $item->calcMafRowTotal();
        } else if (!$quoteItem->getParentItem()) {
            $finalPrice = $product->getFinalMaf($quoteItem->getQty());
            $item->setMaf($finalPrice)
                ->setBaseOriginalMaf($finalPrice);
            $item->calcMafRowTotal();
            $this->_addMafAmount($item->getMafRowTotal());
            $this->_addBaseMafAmount($item->getBaseMafRowTotal());
            $address->setTotalQty($address->getTotalQty() + $item->getQty());
        }

        return true;
    }

    /**
     * Remove item
     *
     * @param  $address
     * @param  $item
     * @return Mage_Sales_Model_Quote_Address_Total_Subtotal
     */
    protected function _removeItem($address, $item)
    {
        if ($item instanceof Mage_Sales_Model_Quote_Item) {
            $address->removeItem($item->getId());
            if ($address->getQuote()) {
                $address->getQuote()->removeItem($item->getId());
            }
        }
        elseif ($item instanceof Mage_Sales_Model_Quote_Address_Item) {
            $address->removeItem($item->getId());
            if ($address->getQuote()) {
                $address->getQuote()->removeItem($item->getQuoteItemId());
            }
        }

        return $this;
    }

    /**
     * Assign subtotal amount and label to address object
     *
     * @param   Mage_Sales_Model_Quote_Address $address
     * @return  Mage_Sales_Model_Quote_Address_Total_Subtotal
     */
    public function fetch(Mage_Sales_Model_Quote_Address $address)
    {
        $address->addMafTotal(array(
            'code'  => $this->getCode(),
            'title' => Mage::helper('sales')->__('Maf Subtotal'),
            'value' => $address->getMafSubtotal()
        ));
        return $this;
    }

    /**
     * Get Subtotal label
     *
     * @return string
     */
    public function getLabel()
    {
        return Mage::helper('sales')->__('Maf Subtotal');
    }


    /**
     * Set total model amount value to address
     *
     * @param   float $amount
     * @return  Mage_Sales_Model_Quote_Address_Total_Abstract
     */
    protected function _setMafAmount($amount)
    {
        if ($this->_canSetAddressAmount) {
            $this->_getAddress()->setMafTotalAmount($this->getCode(), $amount);
        }
        return $this;
    }

    /**
     * Set total model base amount value to address
     *
     * @param   float $amount
     * @return  Mage_Sales_Model_Quote_Address_Total_Abstract
     */
    protected function _setBaseMafAmount($baseAmount)
    {
        if ($this->_canSetAddressAmount) {
            $this->_getAddress()->setBaseMafTotalAmount($this->getCode(), $baseAmount);
        }
        return $this;
    }

    /**
     * Add total model amount value to address
     *
     * @param   float $amount
     * @return  Mage_Sales_Model_Quote_Address_Total_Abstract
     */
    protected function _addMafAmount($amount)
    {
        if ($this->_canAddAmountToAddress) {
            $this->_getAddress()->addMafTotalAmount($this->getCode(),$amount);
        }
        return $this;
    }

    /**
     * Add total model base amount value to address
     *
     * @param   float $amount
     * @return  Mage_Sales_Model_Quote_Address_Total_Abstract
     */
    protected function _addBaseMafAmount($baseAmount)
    {
        if ($this->_canAddAmountToAddress) {
            $this->_getAddress()->addBaseMafTotalAmount($this->getCode(), $baseAmount);
        }
        return $this;
    }
}
