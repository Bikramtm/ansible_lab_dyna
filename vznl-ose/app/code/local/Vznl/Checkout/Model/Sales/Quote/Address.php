<?php
/**
 * @category    Dyna
 * @package     Vznl_Checkout
 */

/**
 * Sales Quote address model
 */
class Vznl_Checkout_Model_Sales_Quote_Address extends Mage_Sales_Model_Quote_Address
{
    /**
     * Total data as array
     *
     * @var array
     */
    protected $_mafTotals = array();

    /**
     * Total amounts
     *
     * @var array
     */
    protected $_mafTotalAmounts = array();

    /**
     * Total base amounts
     *
     * @var array
     */
    protected $_baseMafTotalAmounts = array();


    /**
     * Retrieve applied taxes
     *
     * @return array
     */
    public function getAppliedMafTaxes()
    {
        return unserialize($this->getData('applied_maf_taxes'));
    }

    /**
     * Set applied taxes
     *
     * @param array $data
     * @return Mage_Sales_Model_Quote_Address
     */
    public function setAppliedMafTaxes($data)
    {
        return $this->setData('applied_maf_taxes', serialize($data));
    }

    /**
     * Get address totals as array
     *
     * @return array
     */
    public function getMafTotals()
    {
        foreach ($this->getTotalCollector()->getRetrievers() as $model) {
            $model->fetch($this);
        }
        return $this->_mafTotals;
    }

    /**
     * Add total data or model
     *
     * @param Mage_Sales_Model_Quote_Total|array $total
     * @return Mage_Sales_Model_Quote_Address
     */
    public function addMafTotal($total)
    {
        if (is_array($total)) {
            $totalInstance = Mage::getModel('sales/quote_address_total')
                ->setData($total);
        } elseif ($total instanceof Mage_Sales_Model_Quote_Total) {
            $totalInstance = $total;
        }
        $totalInstance->setAddress($this);
        $this->_mafTotals[$totalInstance->getCode()] = $totalInstance;
        return $this;
    }

    /**
     * Set total amount value
     *
     * @param   string $code
     * @param   float $amount
     * @return  Mage_Sales_Model_Quote_Address
     */
    public function setMafTotalAmount($code, $amount)
    {
        $this->_mafTotalAmounts[$code] = $amount;
        if ($code != 'maf_subtotal') {
            $code = $code.'_amount';
        }
        $this->setData($code, $amount);
        return $this;
    }

    /**
     * Set total amount value in base store currency
     *
     * @param   string $code
     * @param   float $amount
     * @return  Mage_Sales_Model_Quote_Address
     */
    public function setBaseMafTotalAmount($code, $amount)
    {
        $this->_baseMafTotalAmounts[$code] = $amount;
        if ($code != 'maf_subtotal') {
            $code = $code.'_amount';
        }
        $this->setData('base_'.$code, $amount);
        return $this;
    }

    /**
     * Add amount total amount value
     *
     * @param   string $code
     * @param   float $amount
     * @return  Mage_Sales_Model_Quote_Address
     */
    public function addMafTotalAmount($code, $amount)
    {
        $amount = $this->getMafTotalAmount($code)+$amount;
        $this->setMafTotalAmount($code, $amount);
        return $this;
    }

    /**
     * Add amount total amount value in base store currency
     *
     * @param   string $code
     * @param   float $amount
     * @return  Mage_Sales_Model_Quote_Address
     */
    public function addBaseMafTotalAmount($code, $amount)
    {
        $amount = $this->getBaseMafTotalAmount($code)+$amount;
        $this->setBaseMafTotalAmount($code, $amount);
        return $this;
    }

    /**
     * Get total amount value by code
     *
     * @param   string $code
     * @return  float
     */
    public function getMafTotalAmount($code)
    {
        if (isset($this->_mafTotalAmounts[$code])) {
            return  $this->_mafTotalAmounts[$code];
        }
        return 0;
    }

    /**
     * Get total amount value by code in base store curncy
     *
     * @param   string $code
     * @return  float
     */
    public function getBaseMafTotalAmount($code)
    {
        if (isset($this->_baseMafTotalAmounts[$code])) {
            return  $this->_baseMafTotalAmounts[$code];
        }
        return 0;
    }

    /**
     * Get all total amount values
     *
     * @return array
     */
    public function getAllMafTotalAmounts()
    {
        return $this->_mafTotalAmounts;
    }

    /**
     * Get all total amount values in base currency
     *
     * @return array
     */
    public function getAllBaseMafTotalAmounts()
    {
        return $this->_baseMafTotalAmounts;
    }

    /**
     * Returns the value from the quote or the default value from the backend
     *
     * @param string $field
     * @param null $section
     * @return mixed|null
     */
    public function getDefaultValueForField($field, $section = null)
    {
        if ($this->getData($field) != null) {
            return $this->getData($field);
        }

        return Mage::helper('vznl_checkout')->getDefaultValueForField($field, $section);
    }

    /**
     * Overrides the validator of the abstract class.
     */
    protected function _basicCheck()
    {
        Mage::helper('dyna_validators')->validateAddressData($this);
    }

    public function getStreetName()
    {
        return $this->getStreet1();
    }

    public function getHouseNo()
    {
        return $this->getStreet2();
    }

    public function getHouseAdd()
    {
        return $this->getStreet3();
    }

}
