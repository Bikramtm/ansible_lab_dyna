<?php

/**
 * Class Vznl_Checkout_Model_Sales_Quote
 * @method string|null getCorrespondenceEmail()
 * @method bool|null getIsOffer()
 * @method int getActivePackageId()
 * @method int getSuperOrderEditId()
 * @method int getSuperQuoteId()
 * @method int getQuoteParentId()
 */
class Vznl_Checkout_Model_Sales_Quote extends Dyna_Checkout_Model_Sales_Quote
{
    const CART_STATUS_SAVED = 1;
    const CART_STATUS_RISK = 2;
    const CART_STATUS_STEP_CONFIGURATOR = 'configurator';
    const CART_STATUS_STEP_CHECKOUT = 'checkout';
    private $_myCached = array();
    public static $_quoteData = array('items_count', 'items_qty', 'grand_total', 'base_grand_total', 'applied_rule_ids', 'coupon_code', 'subtotal', 'base_subtotal', 'subtotal_with_discount', 'base_subtotal_with_discount', 'maf_grand_total', 'base_maf_grand_total', 'maf_subtotal', 'base_maf_subtotal', 'maf_subtotal_with_discount', 'base_maf_subtotal_with_discount');

    const VALIDATION = 'Waiting for Validation';
    /**
     * Clones current quote
     *
     * @return Vznl_Checkout_Model_Sales_Quote
     */
    public function __clone()
    {
        $quote = new self;
        $quote->setData($this->getData());
        $quote->unsetData('entity_id');
        $quote->merge($this);
        return $quote;
    }

    /**
     * @param int $packageId
     * @return Vznl_Checkout_Model_Sales_Quote_Item[]
     */
    public function getPackageItems($packageId)
    {
        if ( ! $packageId) {
            return array();
        }
        $packageItems = array();
        /** @var Vznl_Checkout_Model_Sales_Quote_Item $item */
        foreach ($this->getItemsCollection() as $item) {
            if ($item->getPackageId() == $packageId && !$item->isDeleted()) {
                $packageItems[] = $item;
            }
        }
        return $packageItems;
    }

    /**
     * @return array
     *
     * Get a list of product ids from a package
     */
    public function getPackageProductIds()
    {
        $packageId = Mage::app()->getRequest()->getParam('packageId', false);
        if(!$packageId) {
            return array();
        }
        $ids = array();
        foreach($this->getPackageItems($packageId) as $item) {
            if(!$item->getProduct()->isSim() && !$item->isPromo()) {
                $ids[] = $item->getProductId();
            }
        }

        $ids = array_unique($ids);
        sort($ids);
        return $ids;
    }

    public function getPackageType($packageId = null)
    {
        $packageId = $packageId ?: $this->getActivePackageId();
        $package = Mage::getModel('package/package')->getCollection()->addFieldToFilter('quote_id', $this->getId())->addFieldToFilter('package_id', $packageId)->getFirstItem();

        return $package->getType();
    }

    public function getPackageSaleType($packageId = null)
    {
        $packageId = $packageId ?: $this->getActivePackageId();
        $package = Mage::getModel('package/package')->getCollection()->addFieldToFilter('quote_id', $this->getId())->addFieldToFilter('package_id', $packageId)->getFirstItem();

        return $package->getSaleType();
    }

    /**
     * @param $packageId int
     * @return Vznl_Package_Model_Package
     */
    public function getPackageById($packageId)
    {
        return Mage::getModel('package/package')->getCollection()
            ->addFieldToFilter('quote_id', $this->getId())
            ->addFieldToFilter('package_id', $packageId)
            ->getFirstItem();
    }

    /**
     *
     * @param $packageId
     * @return array
     */
    public function getPackageCtns($packageId = null)
    {
        $packageId = $packageId ?: $this->getActivePackageId();
        $ctns = array();
        $packages = Mage::getModel('package/package')->getCollection()->addFieldToFilter('quote_id', $this->getId())->addFieldToFilter('package_id', $packageId)->getFirstItem();
        if($packages->getCtn()) {
            $ctns = explode(',',$packages->getCtn());
        }

        return array_unique($ctns);
    }

    /**
     * Adding catalog product object data to quote
     *
     * @param   Mage_Catalog_Model_Product $product
     * @return  Mage_Sales_Model_Quote_Item
     */
    protected function _addCatalogProduct(Mage_Catalog_Model_Product $product, $qty = 1)
    {
        $newItem = false;
        /**
         * force creation of a new quote item even if the same product already exists in the quote
         */
        //$item = $this->getItemByProduct($product);
        $item = false;
        if (!$item) {
            $item = Mage::getModel('sales/quote_item');
            $item->setQuote($this);
            if (Mage::app()->getStore()->isAdmin()) {
                $item->setStoreId($this->getStore()->getId());
            } else {
                $item->setStoreId(Mage::app()->getStore()->getId());
            }
            $newItem = true;
        }

        /**
         * We can't modify existing child items
         */
        if ($item->getId() && $product->getParentProductId()) {
            return $item;
        }

        $item->setOptions($product->getCustomOptions())
            ->setProduct($product);

        // Add only item that is not in quote already (there can be other new or already saved item
        if ($newItem) {
            $this->addItem($item);
        }

        return $item;
    }

    /**
     * Collect totals
     *
     * @return Mage_Sales_Model_Quote
     */
    public function collectTotals()
    {
        /**
         * Protect double totals collection
         */
        if ($this->getTotalsCollectedFlag()) {
            return $this;
        }

        $isMultishipping = $this->getIsMultiShipping();
        $this->setData('collect_totals_cache_items', $isMultishipping);
        Mage::unregister('collect_totals_quote');
        Mage::register('collect_totals_quote', $this);

        Mage::dispatchEvent($this->_eventPrefix . '_collect_totals_before', array($this->_eventObject => $this));

        if (Mage::getSingleton('customer/session')->getOrderEdit() || $this->getIsOffer()) {
            $this->setIsSuperMode(true);
        }

        $this->setSubtotal(0);
        $this->setBaseSubtotal(0);

        $this->setSubtotalWithDiscount(0);
        $this->setBaseSubtotalWithDiscount(0);

        $this->setGrandTotal(0);
        $this->setBaseGrandTotal(0);

        $this->setMafSubtotal(0);
        $this->setBaseMafSubtotal(0);

        $this->setMafSubtotalWithDiscount(0);
        $this->setBaseMafSubtotalWithDiscount(0);

        $this->setMafGrandTotal(0);
        $this->setBaseMafGrandTotal(0);

        /** @var Vznl_Checkout_Model_Sales_Quote_Address $address */
        foreach ($this->getAllShippingAddresses() as $address) {

            $address->setSubtotal(0);
            $address->setBaseSubtotal(0);

            $address->setGrandTotal(0);
            $address->setBaseGrandTotal(0);

            $address->setMafSubtotal(0);
            $address->setBaseMafSubtotal(0);

            $address->setMafGrandTotal(0);
            $address->setBaseMafGrandTotal(0);

            $address->collectTotals();


            $this->setSubtotal((float)$this->getSubtotal() + $address->getSubtotal());
            $this->setBaseSubtotal((float)$this->getBaseSubtotal() + $address->getBaseSubtotal());

            $this->setSubtotalWithDiscount(
                (float)$this->getSubtotalWithDiscount() + $address->getSubtotalWithDiscount()
            );
            $this->setBaseSubtotalWithDiscount(
                (float)$this->getBaseSubtotalWithDiscount() + $address->getBaseSubtotalWithDiscount()
            );

            $this->setGrandTotal((float)$this->getGrandTotal() + $address->getGrandTotal());
            $this->setBaseGrandTotal((float)$this->getBaseGrandTotal() + $address->getBaseGrandTotal());


            $this->setMafSubtotal((float)$this->getMafSubtotal() + $address->getMafSubtotal());
            $this->setBaseMafSubtotal((float)$this->getBaseMafSubtotal() + $address->getBaseMafSubtotal());

            $this->setMafSubtotalWithDiscount(
                (float)$this->getMafSubtotalWithDiscount() + $address->getMafSubtotalWithDiscount()
            );
            $this->setBaseMafSubtotalWithDiscount(
                (float)$this->getBaseMafSubtotalWithDiscount() + $address->getBaseMafSubtotalWithDiscount()
            );

            $this->setMafGrandTotal((float)$this->getMafGrandTotal() + $address->getMafGrandTotal());
            $this->setBaseMafGrandTotal((float)$this->getBaseMafGrandTotal() + $address->getBaseMafGrandTotal());
        }
        Mage::helper('sales')->checkQuoteAmount($this, $this->getGrandTotal());
        Mage::helper('sales')->checkQuoteAmount($this, $this->getBaseGrandTotal());

        $this->setItemsCount(0);
        $this->setItemsQty(0);
        $this->setVirtualItemsQty(0);

        foreach ($this->getAllVisibleItems() as $item) {
            if ($item->getParentItem()) {
                continue;
            }

            $children = $item->getChildren();
            if ($children && $item->isShipSeparately()) {
                foreach ($children as $child) {
                    if ($child->getProduct()->getIsVirtual()) {
                        $this->setVirtualItemsQty($this->getVirtualItemsQty() + $child->getQty());
                    }
                }
            }

            if ($item->getProduct()->getIsVirtual()) {
                $this->setVirtualItemsQty($this->getVirtualItemsQty() + $item->getQty());
            }
            $this->setItemsCount($this->getItemsCount() + 1);
            $this->setItemsQty((float) $this->getItemsQty() + $item->getQty());
        }

        $this->setData('trigger_recollect', 0);
        $this->_validateCouponCode();
        $this->setIsMultiShipping($isMultishipping);
        $this->unsetData('collect_totals_cache_items');
        Mage::dispatchEvent($this->_eventPrefix . '_collect_totals_after', array($this->_eventObject => $this));
        Mage::unregister('collect_totals_quote');
        $this->setTotalsCollectedFlag(true);

        return $this;
    }

    /**
     * Get all quote totals (sorted by priority)
     * Method process quote states isVirtual and isMultiShipping
     *
     * @return array
     */
    public function getMafTotals()
    {
        /**
         * If quote is virtual we are using totals of billing address because
         * all items assigned to it
         */
        if ($this->isVirtual()) {
            return $this->getBillingAddress()->getMafTotals();
        }

        $shippingAddress = $this->getShippingAddress();
        $totals = $shippingAddress->getMafTotals();
        // Going through all quote addresses and merge their totals
        foreach ($this->getAddressesCollection() as $address) {
            if ($address->isDeleted() || $address === $shippingAddress) {
                continue;
            }
            foreach ($address->getMafTotals() as $code => $total) {
                if (isset($totals[$code])) {
                    $totals[$code]->merge($total);
                } else {
                    $totals[$code] = $total;
                }
            }
        }

        $sortedTotals = array();
        foreach ($this->getBillingAddress()->getTotalModels() as $total) {
            /* @var $total Mage_Sales_Model_Quote_Address_Total_Abstract */
            if (isset($totals[$total->getCode()])) {
                $sortedTotals[$total->getCode()] = $totals[$total->getCode()];
            }
        }
        return $sortedTotals;
    }

    public function save()
    {
        /**
         * Assure that current agent and dealer are persisted with the quote
         */
        /** @var Mage_Customer_Model_Session $session */
        $session = Mage::getSingleton('customer/session');
        $agent = $session->getAgent(true);
        if($agent && !$this->getIsOffer()) {
            $this->setData('agent_id', $agent->getId());
            $agentDealer = $agent->getDealer();
            if ($agentDealer) {
                $this->setData('dealer_id', $agentDealer->getId());
            }
            $superAgent = $session->getSuperAgent();
            if ($superAgent) {
                $this->setData('super_agent_id', $superAgent->getId());
            }
        }

        if ($this->_preventSaving) {
            return $this;
        }
        return parent::save();
    }

    /**
     * Gets the quotes for a prospect.
     * @param $prospect
     * @return mixed
     */
    public function getQuotesForProspect($prospect) {
        $quotes = $this
            ->getCollection()
            ->addFieldToFilter('customer_email', $prospect)
            ->addFieldToFilter('prospect_saved_quote', array('neq' => null))
            ->addFieldToFilter('store_id', Mage::app()->getStore()->getId());
        $quotes->getSelect()->where('customer_id IS NULL');
        return $quotes;
    }

    /**
     * Returns the value from the quote or the default value from the backend
     *
     * @param string $field
     * @param null $section
     * @return mixed|null
     */
    public function getDefaultValueForField($field, $section = null)
    {
        if ($this->getData($field) != null) {
            return $this->getData($field);
        }

        return Mage::helper('vznl_checkout')->getDefaultValueForField($field, $section);
    }

    public function getDefaultValueForFieldMask($field, $section = null)
    {
        return $this->getDefaultValueForField($field,$section) ? Vznl_Customer_Model_Customer_Customer::MASK_CHAR . substr($this->getDefaultValueForField($field,$section), Vznl_Customer_Model_Customer_Customer::UNMASK_CHAR_LENGTH) : '';
    }
    /**
     * @param $groupType
     * @param $packageId
     * @return bool
     *
     * Check if a certain package from the quote has a product of a specific type
     */
    public function hasItemGroupInPackage($groupType, $packageId)
    {
        $allItems = $this->getAllItems();
        $isIndirect = Mage::app()->getWebsite()->getCode() == Vznl_Agent_Model_Website::WEBSITE_INDIRECT_CODE;

        // Only show the device RC tab when it has an aikido subscription
        if ($groupType == Vznl_Catalog_Model_Type::SUBTYPE_DEVICE_SUBSCRIPTION) {
            $hasAikido = false;
            $hasDeviceRC = false;
            $hasDevice = false;
            /**@var Vznl_Checkout_Model_Sales_Quote_Item $item */
            foreach ($allItems as $item) {
                $flippedItemProductType = array_flip($item->getProduct()->getType());
                if ($item->getPackageId() == $packageId && isset($flippedItemProductType[Vznl_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION]) && $item->getProduct()->getAikido()) {
                    $hasAikido = true;
                }
                if ($item->getPackageId() == $packageId && isset($flippedItemProductType[Vznl_Catalog_Model_Type::SUBTYPE_DEVICE_SUBSCRIPTION])) {
                    $hasDeviceRC = true;
                }
                if ($item->getPackageId() == $packageId && isset($flippedItemProductType[Vznl_Catalog_Model_Type::SUBTYPE_DEVICE])) {
                    $hasDevice = true;
                }
            }
            if (($hasAikido && $hasDeviceRC) || !$hasAikido || ($hasAikido && !$hasDevice && !$isIndirect)) {
                return true;
            }
        }

        return $this->hasGroupPackage($groupType, $packageId);
    }

    /**
     * @param $groupType
     * @param $packageId
     * @return bool
     */
    public function hasGroupPackage($groupType, $packageId)
    {
        /**@var Vznl_Checkout_Model_Sales_Quote_Item $item*/
        foreach ($this->getAllItems() as $item) {
            if ($item->getPackageId() == $packageId && in_array($groupType, $item->getProduct()->getType())) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return array
     *
     * Removed quote items that are set to cancelled
     */
    public function getAllVisibleItems()
    {
        $key = __METHOD__ . ' ' . (int) $this->getId();
        if (Mage::registry('freeze_models') === true) {
            if (isset($this->_myCached[$key])) {
                return $this->_myCached[$key];
            }
        }

        $items = parent::getAllVisibleItems();
        foreach($items as $key => $item) {
            if($item->getIsCancelled() == 1) {
                unset($items[$key]);
            }
        }

        $this->_myCached[$key] = $items;
        return $items;
    }

    public function refreshItemsCollection()
    {
        $this->_items = Mage::getModel('sales/quote_item')->getCollection();
        $this->_items->setQuote($this);
    }

    /**
     * Retrieve quote items array
     *
     * @return Vznl_Checkout_Model_Sales_Quote_Item[]
     */
    public function getAllItems($includingAlternateItems = false, $includingDeleted = false)
    {
        $key = __METHOD__ . ' ' . (int) $this->getId() .
            sprintf('_%s_%s', ($includingAlternateItems ? 1 : 0), ($includingDeleted ? 1 : 0));

        if (Mage::registry('freeze_models') === true) {
            if (isset($this->_myCached[$key])) {
                return $this->_myCached[$key];
            }
        }

        $items = parent::getAllItems($includingAlternateItems, $includingDeleted);
        $this->_myCached[$key] = $items;

        return $items;
    }

    /**
     * @param int|null $packageId
     * @param bool|false $isOffer
     * @return array
     */
    public function calculateTotalsForPackage($packageId = null, $isOffer = false)
    {
        $items = $this->getAllItems();
        $subtotalPrice = 0;
        $subtotalMaf = 0;
        $taxPrice = 0;
        $taxMaf = 0;
        $total = 0;
        $totalMaf = 0;
        $noPromoMaf = 0;
        $noPromoTax = 0;
        foreach ($items as $item) {
            if ($item->getPackageId() == $packageId || $packageId == null) {
                if (!$item->isPromo() || (!$isOffer && $item->getProduct()->is(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE_SUBSCRIPTION))) { //SREQ-6384
                    //$subtotalMaf = $subtotalMaf - $item->getMafDiscountAmount();
                     //$taxMaf = $taxMaf - $item->getMafTaxAmount();

                    $rowTotal = ceil($item->getRowTotal()) ? $item->getItemFinalPriceExclTax(): 0;
                    $subtotalPrice += $rowTotal;
                    $noPromoMaf += $item->getMaf();
                    $noPromoTax += $item->getMafInclTax();
                    $rowTotalMaf = ceil($item->getMafRowTotal()) ? $item->getItemFinalMafExclTax() : 0;
                    $subtotalMaf += $rowTotalMaf;
                    $mafTaxAmmount = ceil($item->getMafTaxAmount()) ? $item->getMafTaxAmount() : 0;
                    $taxMaf += $mafTaxAmmount;
                    $taxAmmount = ceil($item->getTaxAmount()) ? $item->getTaxAmount() : 0;
                    $taxPrice += $taxAmmount;
                    $total += ($rowTotal + $taxAmmount);
                    $totalMaf += ($rowTotalMaf + $mafTaxAmmount);
                }
            }
        }

        return array(
            'no_promo_maf' => $noPromoMaf,
            'no_promo_maf_tax' => $noPromoTax,
            'no_promo_tax_amount' => ($noPromoTax - $noPromoMaf),
            'subtotal_price' => $isOffer && $this->getInitialMixmatchSubtotal() !== null ? $this->getInitialMixmatchSubtotal() : $subtotalPrice ,
            'subtotal_maf' => $subtotalMaf,
            'tax_price' => $isOffer && $this->getInitialMixmatchSubtotal() !== null ? $this->getInitialMixmatchTax() : $taxPrice,
            'tax_maf' => $taxMaf,
            'total' => $isOffer && $this->getInitialMixmatchSubtotal() !== null ? $this->getInitialMixmatchTotal() : $total,
            'total_maf' => $totalMaf,
        );
    }

    /**
     * Check if the quote has at least one package with number porting
     * Optionally, it can be used to check if a specific package has number porting by sending the $packageId
     *
     * @param array $packages
     * @param int $packageId
     * @return bool
     */
    public function containsNumberPorting(array $packages, $packageId = null)
    {
        foreach ($packages as $id => $package) {
            if ($packageId) {
                if ($id == $packageId) {
                    if (isset($package['current_number']) && !empty($package['current_number'])) {
                        return true;
                    }
                } else {
                    continue;
                }
            } else {
                if (isset($package['current_number']) && !empty($package['current_number'])) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Clones an existing quote
     * @param bool $recollectTotals
     * @throws RuntimeException
     * @return Vznl_Checkout_Model_Sales_Quote
     */
    public function cloneQuote($recollectTotals = true)
    {
        /** @var Mage_Core_Model_Resource_Transaction $transaction */
        $transaction = Mage::getResourceModel('core/transaction');

        // clone the quote
        /** @var Vznl_Checkout_Model_Sales_Quote $newQuote */
        $newQuote = new self;
        $newQuote->setData($this->getData());
        $newQuote->unsetData('entity_id');
        $newQuote->unsetData('created_at');
        $newQuote->setQuoteParentId($this->getId());
        $newQuote->save();

        // clone items
        /**@var Vznl_Checkout_Model_Sales_Quote_Item $item */
        foreach ($this->getAllItems(true) as $item) {
            /**
             * Check if the product is available for the current store.
             */
            if (!in_array(Mage::app()->getStore()->getid(), $item->getProduct()->getStoreIds())) {
                continue;
            }
            $newItem = Mage::getModel('sales/quote_item');
            $newItem->setData($item->getData());
            $newItem->unsetData('item_id');
            if($item->getAlternateQuoteId()) {
                $newItem->setAlternateQuoteId($newQuote->getId());
                $newItem->setQuoteId(null);
            } else {
                $newItem->setQuoteId($newQuote->getId());
            }
            $transaction->addObject($newItem);
        }

        // clone shipping address
        /**@var Vznl_Checkout_Model_Sales_Quote_Address $address */
        foreach ($this->getAllAddresses() as $address) {
            $newAddress = Mage::getModel('sales/quote_address');
            $newAddress->setData($address->getData());
            $newAddress->unsetData('address_id');
            $newAddress->setQuoteId($newQuote->getId());
            $transaction->addObject($newAddress);
        }

        //clone payment
        $oldPayment = $this->getPayment();

        $newPayment = Mage::getModel('sales/quote_payment');
        $newPayment->setData($oldPayment->getData());
        $newPayment->setQuoteId($newQuote->getId());
        $transaction->addObject($newPayment);
        $notesArr = array();

        // Create packages
        $packagesModelOld = Mage::getModel('package/package')->getCollection()
            ->addFieldToFilter('quote_id', $this->getId());
        foreach($packagesModelOld as $packageModelOld) {
            $packageModelNew = Mage::getModel('package/package');
            $packageModelNew->setData($packageModelOld->getData())->setQuoteId($newQuote->getId())->setOrderId(null)->setEntityId(null)->save();

            $notesOld = Mage::getModel('dyna_checkout/packageNotes')->getCollection()
                ->addFieldToFilter('package_id', $packageModelOld->getId());
            foreach($notesOld as $singleNote) {
                if ($packageModelOld->getId() == $singleNote->getPackageId()) {
                    array_push($notesArr,array(
                        $packageModelNew->getId() => $singleNote->getData()
                    ));
                }
            }
        }
        // Create notes
        $this->cloneNotesForPackages($notesArr, $newQuote->getId());
        try {
            $transaction->save();
        } catch (Exception $e) {
            $newQuote->delete();
            throw new \RuntimeException('Cannot clone order');
        }

        //Assure prices
        if ($recollectTotals) {
            $newQuote->getBillingAddress();
            $newQuote->getShippingAddress()->setCollectShippingRates(true);
            $newQuote->collectTotals();
            $newQuote->save();
        }

        return $newQuote;
    }

    protected function _validateCouponCode()
    {
        return $this;
    }

    /**
     * Return packages info
     * @return array
     */
    public function getPackagesInfo()
    {
        $conn = Mage::getSingleton('core/resource')->getConnection('core_read');
        $result = array();
        $packages = Mage::getModel('package/package')->getCollection()
            ->addFieldToFilter('quote_id', $this->getId())
            ->setOrder('package_id', 'ASC');
        $packages = $conn->fetchAll($packages->getSelect());
        foreach ($packages as $package) {
            $result[$package['package_id']] = $package;
        }
        return $result;
    }

    /**
     * Saves the notes given to the packages when cloned
     */
    public function cloneNotesForPackages($notes = null, $newQuote)
    {
        foreach($notes as $key => $data) {
            foreach($data as $newPackageId => $value) {
                $notesNew = Mage::getModel('dyna_checkout/packageNotes');
                $notesNew->setData($value)->setPackageId($newPackageId)->setQuoteId($newQuote)->setEntityId(null)->save();
            }
        }
    }

    /**
     * Quote save method triggers many other events so in case you only need to update a field that should not
     * affect other data, this function can be used as it is much faster
     */
    public function updateFields($data)
    {
        if ($data && $this->getId()) {
            $write = Mage::getSingleton("core/resource")->getConnection("core_write");
            foreach ($data as $field => $value) {
            	$this->setData($field, $value);
            }

            $write->update(
            	'sales_flat_quote',
            	$data,
            	array('entity_id = ?' => $this->getId())
            );
        }

        return $this;
    }

    /**
     * Return al list of SimType for each subscription in every package in quote
     * @return array
     */
    public function getSubscriptionSimTypes()
    {
        $subscriptionIds = array();

        //get subscription ids
        $packages = $this->getPackages();
        foreach($packages as $key => &$package) {
            /**@var Vznl_Checkout_Model_Sales_Quote_Item $item */
            foreach ($package['items'] as &$item) {
                if ($item->getProduct()->is(Vznl_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION)) {
                    $subscriptionIds[] = $item->getProduct()->getId();
                }
            }
        }
        unset($package);
        unset($packages);

        $conn = Mage::getSingleton('core/resource')->getConnection('core_read');
        // retrieve subscription products
        $subscriptions = Mage::getSingleton('catalog/product')
            ->getCollection()
            ->addAttributeToSelect(Dyna_Catalog_Model_Product::POSTP_SIMONLY_KEY)
            ->addAttributeToFilter(Dyna_Catalog_Model_Product::POSTP_SIMONLY_KEY, array('neq' => md5(rand(0,1)))) //hack to join attribute
            ->addAttributeToFilter('entity_id', array('in' => $subscriptionIds));
        $subscriptions = $conn->fetchAll($subscriptions->getSelect());

        // create key => value for each subscription id
        $subscriptionIds = array();
        $attribute = Mage::getResourceModel('catalog/product')->getAttribute(Dyna_Catalog_Model_Product::POSTP_SIMONLY_KEY);
        $valueId = null;
        foreach ($attribute->getSource()->getAllOptions(false, true) as $option) {
            if (strtolower($option['label']) === strtolower(Dyna_Catalog_Model_Product::SIM_ONLY_VALUE)) {
                $valueId = $option['value'];
                break;
            }
        }
        /** @var Dyna_Catalog_Model_Product $subscription */
        foreach($subscriptions as $subscription) {
            $subscriptionIds[$subscription['entity_id']] = ($valueId == $subscription[Dyna_Catalog_Model_Product::POSTP_SIMONLY_KEY]);
        }

        unset($subscriptions);
        unset($attribute);

        return $subscriptionIds;
    }

    /**
     * get all the information for every cnt in every package
     * @return array
     */
    public function getCTNsDetails()
    {
        $ctns = array();
        //get all ctns from packages
        $packages = $this->getPackages();
        foreach ($packages as $key => &$package) {
            $ctns[$key] = array(
                'text'        => '',
                'description' => '',
                'packageCTNs' => array(),
            );
            // add cnt package title text (1 ctn => nt, 2+ ctns => n CTN's)
            if (isset($package['ctn']) && strpos($package['ctn'], ',') !== false) {
                // more than 1 ctns
                $ctns[$key]['text'] = count(explode(',', $package['ctn'])) .
                    'CTN\'s' . '<span style="display:none">' . $package['ctn'] . '</span>';
                $ctns[$key]['packageCTNs'] = array_map('trim', explode(',', $package['ctn']));
            } elseif (isset($package['ctn'])) {
                // 1 ctn
                $ctns[$key]['text'] = $package['ctn'];
                $ctns[$key]['packageCTNs'] = array(trim($package['ctn']));
            }
        }
        unset($package);
        unset($packages);

        // get only the ctns number for description
        $allCtns = array_reduce($ctns, function($carry, $current) {
                return array_merge($carry, $current['packageCTNs']);
        }, array());

        // get cnts description for each ctn
        $details = Mage::getModel('ctn/ctn')->getAllCTNDetails($allCtns);

        // add the description to the ctns in packages
        foreach($ctns as $key => &$ctnPackage) {
            $description = array();
            foreach($ctnPackage['packageCTNs'] as &$ctn) {
                $description[] =  isset($details[$ctn]) ? $details[$ctn] : '';
            }
            $ctnPackage['description'] = join(', ', $description);
        }
        unset($ctn);
        unset($details);

        // add the cnts list
        $ctns['allCtns'] = $allCtns;

        return $ctns;
    }

    /**
     * @param $packageId
     *
     * Save the package id and other possible data through a custom query to prevent quote->save method which is very heavy
     * @param array $additionalData
     */
    public function saveActivePackageId($packageId, $additionalData = array())
    {
        $this->setActivePackageId($packageId);
        if ($this->getId()) {
            $write = Mage::getSingleton("core/resource")->getConnection("core_write");
            $data = array("active_package_id" => $packageId) + $additionalData;
            $where = "entity_id = " . $this->getId();
            $resource = Mage::getSingleton('core/resource');
            $tableName = $resource->getTableName('sales/quote');
            $write->update($tableName, $data, $where);
        }
    }

    public function merge(Mage_Sales_Model_Quote $quote)
    {
        // Also add the packages when merging a quote
        $quotePackages = Mage::getModel('package/package')->getCollection()
            ->addFieldToFilter('quote_id', $quote->getId());
        foreach ($quotePackages as $quotePackage) {
            Mage::getModel('package/package')
                ->setData($quotePackage->getData())
                ->setEntityId(null)
                ->setQuoteId($this->getId())
                ->save();
        }

        Mage::dispatchEvent(
            $this->_eventPrefix . '_merge_before',
            array(
                $this->_eventObject => $this,
                'source' => $quote
            )
        );

        foreach ($quote->getAllVisibleItems() as $item) {
            $newItem = clone $item;
            $this->addItem($newItem);
            if ($item->getHasChildren()) {
                foreach ($item->getChildren() as $child) {
                    $newChild = clone $child;
                    $newChild->setParentItem($newItem);
                    $this->addItem($newChild);
                }
            }
        }

        /**
         * Init shipping and billing address if quote is new
         */
        if (!$this->getId()) {
            $this->getShippingAddress();
            $this->getBillingAddress();
        }

        if ($quote->getCouponCode()) {
            $this->setCouponCode($quote->getCouponCode());
        }

        Mage::dispatchEvent(
            $this->_eventPrefix . '_merge_after',
            array(
                $this->_eventObject => $this,
                'source' => $quote
            )
        );

        return $this;
    }

    /**
     * @param $superOrderEditId
     * @param $getFromSession
     * @return mixed
     */
    public function getListOfModifiedQuotes($superOrderEditId, $getFromSession = true)
    {
        $quoteCondition = $getFromSession
            ? array('eq' => Mage::getSingleton('customer/session')->getOrderEdit())
            : array('null' => true);
        return $this->getCollection()
            ->addFieldToFilter('super_quote_id', $quoteCondition)
            ->addFieldToFilter('super_order_edit_id', array('eq' => $superOrderEditId));
    }

    public function getTotalAmount($exclTax = false)
    {
        if (! $exclTax) {
            return $this->getGrandTotal();
        }

        $total = 0;
        $address = $this->getShippingAddress();
        if ($address) {
            $total = $address->getGrandTotal() - $address->getTaxAmount();
        }

        return max($total, 0);
    }

    public function getGrandTotalForShipping()
    {
        if(Mage::getSingleton('customer/session')->getOrderEdit()) {
            $allPackages = $this->getPackages();
            $packageDatas = array();
            foreach($allPackages as $packageId => $package) {
                $packageModel = Mage::getModel('package/package')->getPackages($this->getSuperOrderEditId(), null, $packageId)->getFirstItem();
                $packageDatas[] = array($packageModel, $package['items']);
            }

            $editTotals = Mage::helper('vznl_checkout')->getEditTotals($packageDatas);
            return $editTotals['total_price'];
        } else {
            return $this->getGrandTotal();
        }
    }

    protected function _beforeSave()
    {
        parent::_beforeSave();
        $tmpQuote = Mage::getSingleton('customer/session')->getOfferteData();
        if ($this->getIsOffer() && $tmpQuote && $this->getId() == $tmpQuote->getId()) {
            foreach($this::$_quoteData as $field) {
                $this->setData($field, $tmpQuote->getData($field));
            }
        }

        /** @var Dyna_Checkout_Model_Sales_Quote_Item $item */
        foreach ($this->getCartPackages(true) as $package) {
            $currentProduct = explode(',', $package->getCurrentProducts());
            foreach ($this->getItemsCollection() as $item) {
                if ($item->getPackageId() != $package->getPackageId()) {
                    continue;
                }

                if ($item->isDeleted() && $item->getPreselectProduct() && !$item->getAlternateQuoteId() && !$this->checkAlternateQuoteItem($item->getProduct(), $package->getPackageId())) {
                    /** @var Dyna_Checkout_Model_Sales_Quote_Item $newItem */
                    if(!$this->getItemBySku($item->getSku())) {
                        $newItem=clone $item;
                        $newItem->isDeleted(false);
                        $systemAction=is_null($newItem->getSystemAction()) ? 'remove' : $newItem->getSystemAction();
                        $newItem
                            ->setQuoteId(null)
                            ->setAlternateQuoteId($this->getId())
                            ->setIsContractDrop(1)
                            ->setSystemAction($systemAction)
                            ->setPreselectProduct(1)
                            ->save();
                    }
                }

                //the preselect flag should only be set for IB products
                if (in_array($item->getProductId(), $currentProduct)) {
                    $isFixedIlsOrMove = $this->getPackageType() == Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_FIXED &&
                        ($this->getPackageSaleType() == Vznl_Catalog_Model_ProcessContext::ILSBLU || $this->getPackageSaleType() == Vznl_Catalog_Model_ProcessContext::MOVE);
                    if($isFixedIlsOrMove){
                        $item->setPreselectProduct(1);
                    }
                }
            }
        }

        // clear cached alternate items
        $this->clearCachedAlternateItems($this->getActivePackageId());
    }

    /**
     * @return bool
     */
    public function hasRetention()
    {
        $packages = Mage::getModel('package/package')->getCollection()->addFieldToFilter('quote_id', $this->getId());
        /** @var Vznl_Package_Model_Package $p */
        foreach ($packages as $p) {
            if ($p->isRetention()) {
                return true;
            }
        }

        return false;
    }

    public function hasAcquisition()
    {
        $packages = Mage::getModel('package/package')->getCollection()->addFieldToFilter('quote_id', $this->getId());
        /** @var Vznl_Package_Model_Package $p */
        foreach ($packages as $p) {
            if ($p->isAcquisition()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check if the quote contains at least one Business type subscription
     *
     * @return bool
     */
    public function containsBusinessSubscription()
    {
        $bool = false;
        $consValue = Mage::helper('vznl_configurator/attribute')->getSubscriptionIdentifier(Vznl_Catalog_Model_Product::IDENTIFIER_BUSINESS);

        $items = $this->getAllVisibleItems();
        if (count($items)) {
            foreach ($items as $item) {
                $prod = $item->getProduct();
                if ($prod->is(Vznl_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION)) {
                    if ($prod->getData(Vznl_Catalog_Model_Product::CATALOG_CONSUMER_TYPE) == $consValue) {
                        $bool = true;
                        break;
                    }
                }
            }
        }

        return $bool;
    }

    /**
     * Check if the quote contains at least one Aikido type subscription
     *
     * @return bool
     */
    public function containsAikidoSubscription()
    {
        $bool = false;

        $items = $this->getAllVisibleItems();
        if (count($items)) {
            foreach ($items as $item) {
                /** @var Dyna_Catalog_Model_Product $prod */
                $prod = $item->getProduct();
                if ($prod->is(Vznl_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION) && $prod->getAikido()) {
                    $bool = true;
                    break;
                }
            }
        }

        return $bool;
    }

    /**
     * Check if the quote is a risk
     *
     * @return bool
     */
    public function checkRisk()
    {
        if (
            !Mage::helper('agent')->isTelesalesLine()
            || (
                Mage::getSingleton('customer/session')->getAgent(true)
                && Mage::getSingleton('customer/session')->getAgent()->getId()
                && Mage::getSingleton('customer/session')->getAgent()->canSubmitRiskOrders()
            )
        ) {
            return false;
        }
        $packages = Mage::getModel('package/package')->getPackages(null, $this->getId())->getItems();
        $packageCount = count($packages);
        foreach ($packages as $package) {
            // RFC-140722 REQ2.1 Check if delivery address != billing address and package is retention
            if ($package->isRetention() && $this->getPackageShippingData($package->getPackageId())['address'] != 'billing_address') {
                return true;
            }

            // RFC-140722 REQ2.3 Check if any of the packages have number porting
            if ($package->isAcquisition() && $package->getCurrentNumber()) {
                return true;
            }

            // RFC-140722 REQ2.4 Acquisition package with Pay on Bill and more than 3 package
            if ($packageCount > 3) {
                if ($package->isAcquisition() && $this->getPackageShippingData($package->getPackageId())['payment'] == 'checkmo') {
                    return true;
                }
            }

            // RFC-150064 REQ1: Change conditions for creating risk order at Telesales business customer acquisition as a risk condition
            if ($package->isAcquisition() && $this->getCustomerIsBusiness()) {
                return true;
            }
        }

        return false;
    }

    public function getTypeOfCartLiteral()
    {
        if ($this->isRisk()) {
            return 'risk';
        }

        if ($this->isOffer()) {
            return 'offer';
        }

        if ($this->isSavedCart()) {
            return 'cart';
        }

        return 'generic';
    }


    /**
     * @return bool
     */
    public function isRisk()
    {
        return $this->getIsOffer() && $this->getCartStatus() == self::CART_STATUS_RISK;
    }

    /**
     * @return bool
     */
    public function isOffer()
    {
       return $this->getIsOffer() && $this->getCartStatus() != self::CART_STATUS_RISK;
    }

    /**
     * @return bool
     */
    public function isSavedCart()
    {
        return $this->getCartStatus() == self::CART_STATUS_SAVED;
    }

    /**
     * Returns the shipping type and delivery type for a package
     *
     * @param int $packageId
     * @throws Exception
     * @return array
     */
    public function getPackageShippingData($packageId)
    {
        $deliveryInfo = Mage::helper('core')->jsonDecode($this->getShippingData());

        if (isset($deliveryInfo['deliver'])) {
            return array('address' => $deliveryInfo['deliver']['address']['address'], 'payment' => $deliveryInfo['payment']);
        } else {
            if (isset($deliveryInfo['pakket'][$packageId]) && isset($deliveryInfo['payment'][$packageId])) {
                return array(
                    'address' => $deliveryInfo['pakket'][$packageId]['address']['address'],
                    'payment' => $deliveryInfo['payment'][$packageId]
                );
            } else {
                throw new Exception('Invalid package id');
            }
        }
    }

    public function clearQuote()
    {
        foreach ($this->getAllItems() as $item) {
            $item->isDeleted(1);
        }
        $packages = Mage::getModel('package/package')->getCollection()->addFieldToFilter('quote_id', $this->getId());
        foreach ($packages as $package) {
            $package->delete();
        }

        $this->collectTotals();
    }

    public function load($id, $field = null)
    {
        $this->_beforeLoad($id, $field);

        $coll = Mage::getModel('sales/quote')->getCollection();

        if ($field) {
            $coll->addFieldToFilter($field, $id);
        } else {
            $coll->addFieldToFilter('entity_id', $id);

        }
        $q = $coll->getFirstItem();
        $this->setData($q->getData());

        $this->_afterLoad();
        $this->setOrigData();
        $this->_hasDataChanges = false;
        return $this;
    }

    /**
     * @return bool
     */
    public function canShowDeviceRC()
    {
        $activePackage = $this->getActivePackageId();
        $hasDevice = false;
        $hasAikido = false;
        if (Mage::app()->getWebsite()->getCode() == Vznl_Agent_Model_Website::WEBSITE_INDIRECT_CODE) {
            $hasDevice = true;
        }

        foreach($this->getAllItems() as $item){
            if($item->getPackageId() == $activePackage){
                if($item->getProduct()->is(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE)) {
                    $hasDevice = true;
                }

                if($item->getProduct()->is(Vznl_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION) && $item->getProduct()->getAikido()) {
                    $hasAikido = true;
                }
            }
        }

        return $hasDevice && $hasAikido;
    }

    /**
     * Checks whether the order contains at least one Aikido subscription.
     * @return bool
     */
    public function isAikido()
    {
        foreach ($this->getAllItems() as $item) {
            if ($item->getProduct()->getAikido()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Gets the total of device payments that will be done over time.
     * Returns it as an positive float.
     * @param int|string|null  The package id or null to retrieve all
     * @param bool Return including or excluding tax
     * @return float|int The total of device payments for all packages or for the package from which the id is given.
     */
    public function getTotalDevicePayment($packageId = null, $inclTax = true){
        if(!is_null($packageId)){
            $items = $this->getPackageItems($packageId);
        } else {
            $items = $this->getAllItems();
        }

        $totalDevicePriceDeduction = 0.0;
        /**@var Vznl_Checkout_Model_Sales_Quote_Item $item */
        foreach ($items as &$item) {
            $product = $item->getProduct();
            // Check if product is of type device subscription
            if ($product->is(Vznl_Catalog_Model_Type::SUBTYPE_DEVICE_SUBSCRIPTION)) {
                $totalDevicePriceDeduction += $item->getTotalPaidByMaf(false, $inclTax);
            }
        }
        return $totalDevicePriceDeduction * -1;
    }

    public function hasMobilePackage()
    {
        $packages = Mage::getModel('package/package')->getCollection()->addFieldToFilter('quote_id', $this->getId());
        /** @var Vznl_Package_Model_Package $p */
        foreach ($packages as $p) {
            if ($p->isMobile()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check if quote has a product that requires a Loan
     * @return bool
     */
    public function isLoanRequired() : bool
    {
        if (!is_null($this->getCustomer()) && !(bool)$this->getCustomer()->getIsBusiness()) {
            foreach ($this->getAllItems() as $item) {
                /**
                 * doing $item->getProduct()->isLoanRequired() for some reason doesnt work well
                 * with getAttributeText in the models' isLoanRequired() method.
                 * If I load the product model it works fine.
                 * Just use both in an OR statement (also for unittests since getProductFromItem
                 * is hard to test).
                 */
                $product = $this->getProductFromItem($item->getProductId());
                if ($product->isLoanRequired() || $item->getProduct()->isLoanRequired()) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Check if quote has a product that requires ILT
     * @return bool
     */
    public function isIltRequired() : bool
    {
        if (!is_null($this->getCustomer()) && !(bool)$this->getCustomer()->getIsBusiness()) {
            foreach ($this->getAllItems() as $item) {
                /**
                 * doing $item->getProduct()->isIltRequired() for some reason doesnt work well
                 * with getAttributeText in the models' isIltRequired() method.
                 * If I load the product model it works fine.
                 * Just use both in an OR statement (also for unittests since getProductFromItem
                 * is hard to test).
                 */
                $product = $this->getProductFromItem($item->getProductId());
                if ($product->isIltRequired() || $item->getProduct()->isIltRequired()) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @param $productId
     * @return Vznl_Catalog_Model_Product
     */
    protected function getProductFromItem($productId)
    {
        $productModel = Mage::getModel('catalog/product');
        return $productModel->load($productId);
    }

    /**
     * Checks whether the quote has a package with type fixed.
     * @return bool <true> if has a fixed package, <false> if not.
     */
    public function hasFixed()
    {
        $packages = Mage::getModel('package/package')->getCollection()->addFieldToFilter('quote_id', $this->getId());
        /** @var Vznl_Package_Model_Package $p */
        foreach ($packages as $p) {
            if ($p->isFixed()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks whether the quote has a package with type fixed.
     * @return bool <true> if has a prepaid package, <false> if not.
     */
    public function hasPrepaid()
    {
        $packages = Mage::getModel('package/package')->getCollection()->addFieldToFilter('quote_id', $this->getId());
        /** @var Vznl_Package_Model_Package $p */
        foreach ($packages as $p) {
            if ($p->isPrepaid()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks whether the quote has a package with type fixed.
     * @return bool <true> if has a prepaid package, <false> if not.
     */
    public function hasHardware()
    {
        $packages = Mage::getModel('package/package')->getCollection()->addFieldToFilter('quote_id', $this->getId());
        /** @var Vznl_Package_Model_Package $p */
        foreach ($packages as $p) {
            if ($p->isHardware()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Method is override from Dyna
     * Checking $this->getId() is not empty.
     * Return all packages for current quote
     * @param bool $excludeDummy
     * @return array
     */
    public function getCartPackages($excludeDummy = false)
    {
        if ($this->getId()) {
            return parent::getCartPackages($excludeDummy);
        }

        return [];
    }

    /**
     * Add product to quote
     *
     * return error message if product type instance can't prepare product
     *
     * Dyna overrides Magento core
     *
     * @param mixed $product
     * @param null|float|Varien_Object $request
     * @return Mage_Sales_Model_Quote_Item|string
     */
    public function addProduct(Mage_Catalog_Model_Product $product, $request = null)
    {
        try {
            if (!$item = $this->checkAlternateQuoteItem($product)) {
                return Mage_Sales_Model_Quote::addProduct($product, $request);
            } else {
                /** @var Dyna_Checkout_Model_Sales_Quote_Item $item */
                $item
                    ->setAlternateQuoteId(null)
                    ->setQuoteId($this->getId())
                    ->setIsContractDrop(null)
                    ->setPreselectProduct(1)
                    ->setSystemAction(Dyna_Checkout_Model_Sales_Quote_Item::SYSTEM_ACTION_EXISTING);
                if ($item->getBundleComponent() == -1) {
                    foreach ($this->getAlternateItems($item->getPackageId()) as $bItem) {
                        if ($item->getBundleComponent() == -1) {
                            $bItem
                                ->setAlternateQuoteId(null)
                                ->setQuoteId($this->getId())
                                ->setIsContractDrop(null)
                                ->setPreselectProduct(1)
                                ->setSystemAction(Dyna_Checkout_Model_Sales_Quote_Item::SYSTEM_ACTION_EXISTING);
                                $this->getItemsCollection()->addItem($bItem);
                        }
                    }
                }

                $this->getItemsCollection()->addItem($item);

                // trigger clearing of cached alternate items for this package
                $this->clearCachedAlternateItems($this->getActivePackageId());
            }
        } catch (Dyna_Checkout_Model_Exception_Fee $e) {
            return $e->getMessage();
        } catch (Dyna_Checkout_Model_Exception_Expired $e) {
            return $e->getMessage();
        }

        return $item;
    }

    /**
     * Set shipping data
     *
     * @param array $data
     * @return Vznl_Checkout_Model_Sales_Quote
     */
    public function setShippingData($data) {
        $this->setData('shipping_data', $data);
        return $this;
    }

    /**
     * Get shipping data
     *
     * @return array
     */
    public function getShippingData() {
        return $this->getData('shipping_data');
    }

    /**
     * Get quote item by sku from a certain packageId
     * If no package is provided, the entire cart will be evaluated
     * @param $sku
     * @return Dyna_Checkout_Model_Sales_Quote_Item|null
     */
    public function getItemBySku($sku, $packageId = null, $includingDeleted = false) {
        if ($packageId) {
            $cartPackage = $this->getCartPackage($packageId);
            if (!is_null($cartPackage)) {
                foreach ($cartPackage->getAllItems(false, true, false, $includingDeleted) as $item) {
                    if ($item->getSku() == $sku) {
                        return $item;
                    }
                }
            }

        } else {
            // evaluate entire cart
            foreach ($this->getAllItems(false, $includingDeleted) as $item) {
                if ($item->getSku() == $sku) {
                    return $item;
                }
            }
        }
        // not found
        return null;
    }

    /**
     * Method used in checkout/cart/partials/package_block.phtml
     * for displaying the installed base items in shopping cart (strike-trough)
     * @return array
     */
    public function getAlternatePackages()
    {
        $packagesAll = [];
        // todo: add process context condition
        $alternateItems = $this->getAlternateItems();
        if (count($alternateItems)) {
            $packages = $disabledPackages = [];

            // Set packages type and sale type
            $this->getQuotePackagesInfo($packages, $disabledPackages);


            $quotePackages = $this->getPackages();
            $package = $quotePackages[$this->getActivePackageId()];
            $package['items'] = $alternateItems;

            $alternatePackages[] = $package;

            $packagesAll = Mage::getModel('dyna_checkout/cart')->extractPackageInfo($alternatePackages);
        }

        return $packagesAll;
    }

    /**
     * Determine whether or not there is already an alternate item for this active package with this sku
     * @param Mage_Catalog_Model_Product $product
     * @param $packageId|null
     * @return Dyna_Checkout_Model_Sales_Quote_Item|null
     */
    public function checkAlternateQuoteItem(Mage_Catalog_Model_Product $product, $packageId = null)
    {
        $installBase = array_merge($this->getAlternateItems(), $this->getAlternateItems($packageId, false));
        /** @var Dyna_Checkout_Model_Sales_Quote_Item $quoteItem */
        foreach ($installBase as $quoteItem) {
            if ($quoteItem->getSku() != $product->getSku()) {
                continue;
            }

            return $quoteItem;
        }

        return null;
    }

    /**
     * Return the install base package for the given account number and line id combination
     * @param $parentAccountNumber
     * @param $serviceLineId
     * @return Dyna_Package_Model_Package|null
     */
    public function getInstallBasePackage($parentAccountNumber, $serviceLineId, $excludeDummy = false)
    {
        // get the install base product to check if it is from the cable stack
        /** @var Dyna_Customer_Model_Session $customerSession */
        $customerSession = Mage::getSingleton('customer/session');
        $installBaseProduct = $customerSession->getCustomer()->getInstalledBaseProduct($parentAccountNumber, $serviceLineId);
        if ($installBaseProduct['contract_category'] == Dyna_Customer_Helper_Services::ACCOUNT_CATEGORY_KD) {
            // get KD linked account number from session
            $parentAccountNumber = $customerSession->getKdLinkedAccountNumber();
        }

        foreach ($this->getCartPackages() as $package) {
            if ($excludeDummy && $package->getEditingDisabled()) {
                continue;
            }
            if ($parentAccountNumber && $serviceLineId && $package->getParentAccountNumber() == $parentAccountNumber && $package->getServiceLineId() == $serviceLineId) {
                return $package;
            }
        }

        return null;
    }

    /**
     * Return true if the current quote is a fixed ILS or MOVE
     * @return boolean
     */
    public function isFixedIlsMove()
    {
        $cartPackages = $this->getCartPackages();
        if ($cartPackages) {
            foreach ($cartPackages as $cartPackage) {
                if($cartPackage->getType() == strtolower(Vznl_Catalog_Model_Type::TYPE_FIXED) && in_array($cartPackage->getSaleType(), [Vznl_Catalog_Model_ProcessContext::ILSBLU,Vznl_Catalog_Model_ProcessContext::MOVE]))
                    return true;
            }
        }
        return false;
    }

    /**
     * Return true if the current quote contains fixed ILS
     * @return boolean
     */
    public function isFixedIls()
    {
        $cartPackages = $this->getCartPackages();
        if ($cartPackages) {
            foreach ($cartPackages as $cartPackage) {
                if($cartPackage->getType() == strtolower(Vznl_Catalog_Model_Type::TYPE_FIXED) && $cartPackage->getSaleType() == Vznl_Catalog_Model_ProcessContext::ILSBLU)
                    return true;
            }
        }
        return false;
    }

    /**
     * Return package id for fixed product
     * @return interger
     */
    public function getFixedPackageId()
    {
        $cartPackages = $this->getCartPackages();
        if ($cartPackages) {
            foreach ($cartPackages as $cartPackage) {
                if ($cartPackage->getType() == strtolower(Vznl_Catalog_Model_Type::TYPE_FIXED))
                    return $cartPackage->getPackageId();
            }
        }
    }

    /**
     * Return processContext id for fixed product
     * @return integer
     */
    public function getFixedProcessContextId()
    {
        $processContextHelper = Mage::helper("dyna_catalog/processContext");
        $cartPackages = $this->getCartPackages();
        if ($cartPackages) {
            foreach ($cartPackages as $cartPackage) {
                if($cartPackage->getType() == strtolower(Vznl_Catalog_Model_Type::TYPE_FIXED)) {
                    return $processContextHelper->getProcessContextId($cartPackage);
                }
            }
        }
        return null;
    }


    public function getQuoteNewHardware()
    {
        $newHardware = false;
        foreach($this->getAllItems() as $key => $item) {
            if($item->getNewHardware()){
                $newHardware = $item->getNewHardware();
            }
        }
        return $newHardware;
    }
}
