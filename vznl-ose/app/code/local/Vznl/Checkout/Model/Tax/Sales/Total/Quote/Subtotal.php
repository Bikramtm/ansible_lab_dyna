<?php
/**
 * Calculate items and address amounts including/excluding tax
 */
class Vznl_Checkout_Model_Tax_Sales_Total_Quote_Subtotal extends Mage_Sales_Model_Quote_Address_Total_Abstract
{
    /**
     * Tax calculation model
     *
     * @var Mage_Tax_Model_Calculation
     */
    protected $_calculator = null;

    /**
     * Tax configuration object
     *
     * @var Mage_Tax_Model_Config
     */
    protected $_config = null;

    /**
     * Tax helper instance
     *
     * @var Mage_Tax_Helper_Data|null
     */
    protected $_helper = null;

    /**
     * Default value for subtotal that include taxes
     *
     * @var float
     */
    protected $_mafSubtotalInclTax = 0;

    /**
     * Default value for base subtotal that include taxes
     *
     * @var float
     */
    protected $_baseMafSubtotalInclTax = 0;

    /**
     * Default value for subtotal
     *
     * @var float
     */
    protected $_mafSubtotal = 0;

    /**
     * Default value for base subtotal
     *
     * @var float
     */
    protected $_baseMafSubtotal = 0;

    /**
     * Flag which is initialized when collect method is started and catalog prices include tax.
     * Is used for checking if store tax and customer tax requests are similar
     *
     * @var bool
     */
    protected $_areTaxRequestsSimilar = false;

    /**
     * Request which can be used for tax rate calculation
     *
     * @var Varien_Object
     */
    protected $_storeTaxRequest = null;

    /**
     *  Quote store
     *
     * @var Mage_Core_Model_Store
     */
    protected $_store;

    /**
     * Rounding deltas for prices
     *
     * @var array
     */
    protected $_roundingDeltas = array();

    /**
     * Class constructor
     */
    public function __construct()
    {
        $this->setCode('maf_tax_subtotal');
        $this->_helper = Mage::helper('tax');
        $this->_calculator = Mage::getSingleton('tax/calculation');
        $this->_config = Mage::getSingleton('tax/config');
    }

    /**
     * Calculate item price including/excluding tax, row total including/excluding tax
     * and subtotal including/excluding tax.
     * Determine discount price if needed
     *
     * @param   Mage_Sales_Model_Quote_Address $address
     *
     * @return  Mage_Tax_Model_Sales_Total_Quote_Subtotal
     */
    public function collect(Mage_Sales_Model_Quote_Address $address)
    {
        $this->_store = $address->getQuote()->getStore();
        $this->_address = $address;

        $this->_mafSubtotalInclTax = 0;
        $this->_baseMafSubtotalInclTax = 0;
        $this->_mafSubtotal = 0;
        $this->_baseMafSubtotal = 0;
        $this->_roundingDeltas = array();

        $address->setMafSubtotalInclTax(0);
        $address->setBaseMafSubtotalInclTax(0);
        $address->setMafTotalAmount('maf_subtotal', 0);
        $address->setBaseMafTotalAmount('maf_subtotal', 0);

        $items = $this->_getAddressItems($address);
        if (!$items) {
            return $this;
        }

        $addressRequest = $this->_getAddressTaxRequest($address);
        $storeRequest = $this->_getStoreTaxRequest($address);
        $this->_calculator->setCustomer($address->getQuote()->getCustomer());
        if ($this->_config->priceIncludesTax($this->_store)) {
            $classIds = array();
            foreach ($items as $item) {
                $classIds[] = $item->getProduct()->getTaxClassId();
                if ($item->getHasChildren()) {
                    foreach ($item->getChildren() as $child) {
                        $classIds[] = $child->getProduct()->getTaxClassId();
                    }
                }
            }
            $classIds = array_unique($classIds);
            $storeRequest->setProductClassId($classIds);
            $addressRequest->setProductClassId($classIds);
            $this->_areTaxRequestsSimilar = $this->_calculator->compareRequests($storeRequest, $addressRequest);
        }

        foreach ($items as $item) {
            if ($item->getParentItem()) {
                continue;
            }
            if ($item->getHasChildren() && $item->isChildrenCalculated()) {
                foreach ($item->getChildren() as $child) {
                    $this->_processItem($child, $addressRequest);
                }
                $this->_recalculateParent($item);
            } else {
                $this->_processItem($item, $addressRequest);
            }
            $this->_addMafSubtotalAmount($address, $item);
        }
        $address->setRoundingDeltas($this->_roundingDeltas);
        return $this;
    }

    /**
     * Calculate item price and row total with configured rounding level
     *
     * @param Mage_Sales_Model_Quote_Item_Abstract $item
     * @param Varien_Object $taxRequest
     *
     * @return Mage_Tax_Model_Sales_Total_Quote_Subtotal
     */
    protected function _processItem($item, $taxRequest)
    {
        /**
         * Short-circuited switch to always use the row base calculation for the MAF prices.
         */
        $this->_rowBaseCalculation($item, $taxRequest);
        return $this;
    }

    /**
     * Calculate item price and row total including/excluding tax based on unit price rounding level
     *
     * @param Vznl_Checkout_Model_Sales_Quote_Item $item
     * @param Varien_Object $request
     *
     * @return Mage_Tax_Model_Sales_Total_Quote_Subtotal
     */
    protected function _unitBaseCalculation($item, $request)
    {
        $request->setProductClassId($item->getProduct()->getTaxClassId());
        $rate = $this->_calculator->getRate($request);
        $qty = $item->getTotalQty();

        $price = $taxPrice = $this->_calculator->round($item->getMaf());
        $basePrice = $baseTaxPrice = $this->_calculator->round($item->getBaseMaf());
        $subtotal = $taxSubtotal = $this->_calculator->round($item->getRowTotal());
        $baseSubtotal = $baseTaxSubtotal = $this->_calculator->round($item->getBaseRowTotal());

        $item->setTaxPercent($rate);
        if ($this->_config->priceIncludesTax($this->_store)) {
            if ($this->_sameRateAsStore($request)) {
                // determine which price to use when we calculate the tax
                $taxable        = $price;
                $baseTaxable    = $basePrice;

                $tax             = $this->_calculator->calcTaxAmount($taxable, $rate, true);
                $baseTax         = $this->_calculator->calcTaxAmount($baseTaxable, $rate, true);
                $taxPrice        = $price;
                $baseTaxPrice    = $basePrice;
                $taxSubtotal     = $subtotal;
                $baseTaxSubtotal = $baseSubtotal;
                $price = $price - $tax;
                $basePrice = $basePrice - $baseTax;
                $subtotal = $price * $qty;
                $baseSubtotal = $basePrice * $qty;
                $isPriceInclTax  = true;

                $item->setMafRowTax($tax * $qty);
                $item->setBaseMafRowTax($baseTax * $qty);

            } else {
                $storeRate       = $this->_calculator->getStoreRate($request, $this->_store);

                // determine the customer's price that includes tax
                $taxPrice     = $this->_calculatePriceInclTax($price, $storeRate, $rate);
                $baseTaxPrice = $this->_calculatePriceInclTax($basePrice, $storeRate, $rate);
                // determine which price to use when we calculate the tax
                $taxable      = $taxPrice;
                $baseTaxable  = $baseTaxPrice;

                // determine the customer's tax amount
                $tax             = $this->_calculator->calcTaxAmount($taxable, $rate, true, true);
                $baseTax         = $this->_calculator->calcTaxAmount($baseTaxable, $rate, true, true);
                // determine the customer's price without taxes
                $price = $taxPrice - $tax;
                $basePrice = $baseTaxPrice - $baseTax;
                // determine subtotal amounts
                $taxSubtotal = $taxPrice * $qty;
                $baseTaxSubtotal = $baseTaxPrice * $qty;
                $subtotal = $price * $qty;
                $baseSubtotal = $basePrice * $qty;
                $isPriceInclTax  = true;

                $item->setMafRowTax($tax * $qty);
                $item->setBaseMafRowTax($baseTax * $qty);
            }
        } else {
            // determine which price to use when we calculate the tax
            $taxable = $price;
            $baseTaxable = $basePrice;

            $appliedRates = $this->_calculator->getAppliedRates($request);
            $taxes = array();
            $baseTaxes = array();
            foreach ($appliedRates as $appliedRate) {
                $taxRate = $appliedRate['percent'];
                $taxes[] = $this->_calculator->calcTaxAmount($taxable, $taxRate, false);
                $baseTaxes[] = $this->_calculator->calcTaxAmount($baseTaxable, $taxRate, false);
            }
            $tax             = array_sum($taxes);
            $baseTax         = array_sum($baseTaxes);
            $taxPrice        = $price + $tax;
            $baseTaxPrice    = $basePrice + $baseTax;
            $taxSubtotal     = $taxPrice * $qty;
            $baseTaxSubtotal = $baseTaxPrice * $qty;
            $isPriceInclTax  = false;
        }

        $item->setMaf($basePrice);
        $item->setBaseMaf($basePrice);
        $item->setMafRowTotal($subtotal);
        $item->setBaseMafRowTotal($baseSubtotal);
        $item->setMafInclTax($taxPrice);
        $item->setBaseMafInclTax($baseTaxPrice);
        $item->setMafRowTotalInclTax($taxSubtotal);
        $item->setBaseMafRowTotalInclTax($baseTaxSubtotal);
        $item->setMafTaxableAmount($taxable);
        $item->setBaseMafTaxableAmount($baseTaxable);
        $item->setIsPriceInclTax($isPriceInclTax);

        return $this;
    }

    /**
     * Calculate item price and row total including/excluding tax based on row total price rounding level
     *
     * @param Vznl_Checkout_Model_Sales_Quote_Item $item
     * @param Varien_Object $request
     *
     * @return Mage_Tax_Model_Sales_Total_Quote_Subtotal
     */
    protected function _rowBaseCalculation($item, $request)
    {
        $request->setProductClassId($item->getProduct()->getTaxClassId());
        $rate = $this->_calculator->getRate($request);
        $qty = $item->getTotalQty();

        $price = $taxPrice = $this->_calculator->round($item->getCalculationMafOriginal());
        $basePrice = $baseTaxPrice = $this->_calculator->round($item->getBaseCalculationMafOriginal());
        $subtotal = $taxSubtotal = $this->_calculator->round($item->getMafRowTotal());
        $baseSubtotal = $baseTaxSubtotal = $this->_calculator->round($item->getBaseMafRowTotal());

        // if we have a custom price, determine if tax should be based on the original price

        $item->setTaxPercent($rate);
        if ($this->_config->priceIncludesTax($this->_store)) {
            if ($this->_sameRateAsStore($request)) {
                // determine which price to use when we calculate the tax

                $taxable        = $taxSubtotal;
                $baseTaxable    = $baseTaxSubtotal;

                $rowTax          = $this->_calculator->calcTaxAmount($taxable, $rate, true, true);
                $baseRowTax      = $this->_calculator->calcTaxAmount($baseTaxable, $rate, true, true);
                $taxPrice        = $price;
                $baseTaxPrice    = $basePrice;
                $taxSubtotal     = $subtotal;
                $baseTaxSubtotal = $baseSubtotal;
                $subtotal = $this->_calculator->round($subtotal - $rowTax);
                $baseSubtotal = $this->_calculator->round($baseSubtotal - $baseRowTax);
                $basePrice = $this->_calculator->round($baseSubtotal / $qty);
                $isPriceInclTax  = true;

                $item->setMafRowTax($rowTax);
                $item->setBaseMafRowTax($baseRowTax);
            } else {
                $storeRate       = $this->_calculator->getStoreRate($request, $this->_store);

                // determine the customer's price that includes tax
                $taxPrice     = $this->_calculatePriceInclTax($price, $storeRate, $rate);
                $baseTaxPrice = $this->_calculatePriceInclTax($basePrice, $storeRate, $rate);
                // determine which price to use when we calculate the tax
                $taxable      = $taxPrice;
                $baseTaxable  = $baseTaxPrice;

                // determine the customer's tax amount
                $baseTax         = $this->_calculator->calcTaxAmount($baseTaxable, $rate, true, true);
                // determine the customer's price without taxes
                $basePrice = $baseTaxPrice - $baseTax;
                // determine subtotal amounts
                $taxable        *= $qty;
                $baseTaxable    *= $qty;
                $taxSubtotal     = $taxPrice * $qty;
                $baseTaxSubtotal = $baseTaxPrice * $qty;
                $rowTax          = $this->_calculator->calcTaxAmount($taxable, $rate, true, true);
                $baseRowTax      = $this->_calculator->calcTaxAmount($baseTaxable, $rate, true, true);
                $subtotal        = $taxSubtotal - $rowTax;
                $baseSubtotal    = $baseTaxSubtotal - $baseRowTax;
                $isPriceInclTax  = true;

                $item->setMafRowTax($rowTax);
                $item->setBaseMafRowTax($baseRowTax);
            }
        } else {
            // determine which price to use when we calculate the tax
            $taxable = $subtotal;
            $baseTaxable = $baseSubtotal;

            $appliedRates = $this->_calculator->getAppliedRates($request);
            $rowTaxes = array();
            $baseRowTaxes = array();
            foreach ($appliedRates as $appliedRate) {
                $taxRate = $appliedRate['percent'];
                $rowTaxes[] = $this->_calculator->calcTaxAmount($taxable, $taxRate, false, true);
                $baseRowTaxes[] = $this->_calculator->calcTaxAmount($baseTaxable, $taxRate, false, true);
            }
            $rowTax          = array_sum($rowTaxes);
            $baseRowTax      = array_sum($baseRowTaxes);
            $taxSubtotal     = $subtotal + $rowTax;
            $baseTaxSubtotal = $baseSubtotal + $baseRowTax;
            $taxPrice        = $this->_calculator->round($taxSubtotal/$qty);
            $baseTaxPrice    = $this->_calculator->round($baseTaxSubtotal/$qty);
            $isPriceInclTax  = false;
        }


        $item->setMaf($basePrice);
        $item->setBaseMaf($basePrice);
        $item->setMafRowTotal($subtotal);
        $item->setBaseMafRowTotal($baseSubtotal);
        $item->setMafInclTax($taxPrice);
        $item->setBaseMafInclTax($baseTaxPrice);
        $item->setMafRowTotalInclTax($taxSubtotal);
        $item->setBaseMafRowTotalInclTax($baseTaxSubtotal);
        $item->setMafTaxableAmount($taxable);
        $item->setBaseMafTaxableAmount($baseTaxable);
        $item->setIsPriceInclTax($isPriceInclTax);

        return $this;
    }

    /**
     * Calculate item price and row total including/excluding tax based on total price rounding level
     *
     * @param Vznl_Checkout_Model_Sales_Quote_Item $item
     * @param Varien_Object $request
     *
     * @return Mage_Tax_Model_Sales_Total_Quote_Subtotal
     */
    protected function _totalBaseCalculation($item, $request)
    {
        $calc = $this->_calculator;
        $request->setProductClassId($item->getProduct()->getTaxClassId());
        $rate = $calc->getRate($request);
        $qty = $item->getTotalQty();

        $price = $taxPrice = $this->_calculator->round($item->getCalculationMafOriginal());
        $basePrice = $baseTaxPrice = $this->_calculator->round($item->getBaseCalculationMafOriginal());
        $subtotal = $taxSubtotal = $this->_calculator->round($item->getMafRowTotal());
        $baseSubtotal = $baseTaxSubtotal = $this->_calculator->round($item->getBaseMafRowTotal());

        $item->setTaxPercent($rate);
        if ($this->_config->priceIncludesTax($this->_store)) {
            if ($this->_sameRateAsStore($request)) {
                // determine which price to use when we calculate the tax
                $taxable = $subtotal;
                $baseTaxable = $baseSubtotal;

                $rowTaxExact     = $calc->calcTaxAmount($taxable, $rate, true, false);
                $rowTax          = $this->_deltaRound($rowTaxExact, $rate, true);
                $baseRowTaxExact = $calc->calcTaxAmount($baseTaxable, $rate, true, false);
                $baseRowTax      = $this->_deltaRound($baseRowTaxExact, $rate, true, 'base');

                $taxPrice        = $price;
                $baseTaxPrice    = $basePrice;
                $taxSubtotal = $subtotal;
                $baseTaxSubtotal = $baseSubtotal;

                $subtotal          = $subtotal - $rowTax;
                $baseSubtotal      = $baseSubtotal - $baseRowTax;

                $price = $calc->round($subtotal / $qty);
                $basePrice = $calc->round($baseSubtotal / $qty);

                $isPriceInclTax  = true;

                //Save the tax calculated
                $item->setMafRowTax($rowTax);
                $item->setBaseMafRowTax($baseRowTax);

            } else {
                $storeRate = $calc->getStoreRate($request, $this->_store);

                // determine the customer's price that includes tax
                $taxPrice     = $this->_calculatePriceInclTax($price, $storeRate, $rate);
                $baseTaxPrice = $this->_calculatePriceInclTax($basePrice, $storeRate, $rate);
                // determine which price to use when we calculate the tax
                $taxable      = $taxPrice;
                $baseTaxable  = $baseTaxPrice;

                // determine the customer's tax amount based on the taxable price
                $tax             = $this->_calculator->calcTaxAmount($taxable, $rate, true, true);
                $baseTax         = $this->_calculator->calcTaxAmount($baseTaxable, $rate, true, true);
                // determine the customer's price without taxes
                $price = $taxPrice - $tax;
                $basePrice = $baseTaxPrice - $baseTax;
                // determine subtotal amounts
                $taxable        *= $qty;
                $baseTaxable    *= $qty;
                $taxSubtotal     = $taxPrice * $qty;
                $baseTaxSubtotal = $baseTaxPrice * $qty;
                $rowTax =
                    $this->_deltaRound($calc->calcTaxAmount($taxable, $rate, true, false), $rate, true);
                $baseRowTax =
                    $this->_deltaRound($calc->calcTaxAmount($baseTaxable, $rate, true, false), $rate, true, 'base');
                $subtotal = $taxSubtotal - $rowTax;
                $baseSubtotal = $baseTaxSubtotal - $baseRowTax;
                $isPriceInclTax  = true;

                $item->setMafRowTax($rowTax);
                $item->setBaseMafRowTax($baseRowTax);
            }
        } else {
            // determine which price to use when we calculate the tax

            $taxable = $subtotal;
            $baseTaxable = $baseSubtotal;

            $appliedRates = $this->_calculator->getAppliedRates($request);
            $rowTaxes = array();
            $baseRowTaxes = array();
            foreach ($appliedRates as $appliedRate) {
                $taxId = $appliedRate['id'];
                $taxRate = $appliedRate['percent'];
                $rowTaxes[] = $this->_deltaRound($calc->calcTaxAmount($taxable, $taxRate, false, false), $taxId, false);
                $baseRowTaxes[] = $this->_deltaRound(
                        $calc->calcTaxAmount($baseTaxable, $taxRate, false, false), $taxId, false, 'base');

            }

            $taxSubtotal     = $subtotal + array_sum($rowTaxes);
            $baseTaxSubtotal = $baseSubtotal + array_sum($baseRowTaxes);

            $taxPrice        = $calc->round($taxSubtotal/$qty);
            $baseTaxPrice    = $calc->round($baseTaxSubtotal/$qty);

            $isPriceInclTax = false;
        }

        $item->setConvertedMaf($price);

        $item->setMaf($basePrice);
        $item->setBaseMaf($basePrice);
        $item->setMafRowTotal($subtotal);
        $item->setBaseMafRowTotal($baseSubtotal);
        $item->setMafInclTax($taxPrice);
        $item->setBaseMafInclTax($baseTaxPrice);
        $item->setMafRowTotalInclTax($taxSubtotal);
        $item->setBaseMafRowTotalInclTax($baseTaxSubtotal);
        $item->setMafTaxableAmount($taxable);
        $item->setBaseMafTaxableAmount($baseTaxable);
        $item->setIsPriceInclTax($isPriceInclTax);

        return $this;
    }

    /**
     * Given a store price that includes tax at the store rate, this function will back out the store's tax, and add in
     * the customer's tax.  Returns this new price which is the customer's price including tax.
     *
     * @param float $storePriceInclTax
     * @param float $storeRate
     * @param float $customerRate
     *
     * @return float
     */
    protected function _calculatePriceInclTax($storePriceInclTax, $storeRate, $customerRate)
    {
        $storeTax = $this->_calculator->calcTaxAmount($storePriceInclTax, $storeRate, true, false);
        $priceExclTax = $storePriceInclTax - $storeTax;
        $customerTax = $this->_calculator->calcTaxAmount($priceExclTax, $customerRate, false, false);
        $customerPriceInclTax = $this->_calculator->round($priceExclTax + $customerTax);
        return $customerPriceInclTax;
    }

    /**
     * Checks whether request for an item has same rate as store one
     * Used only after collect() started, as far as uses optimized $_areTaxRequestsSimilar property
     * Used only in case of prices including tax
     *
     * @param Varien_Object $request
     *
     * @return bool
     */
    protected function _sameRateAsStore($request)
    {
        // Maybe we know that all requests for currently collected items have same rates
        if ($this->_areTaxRequestsSimilar) {
            return true;
        }

        // Check current request individually
        $rate = $this->_calculator->getRate($request);
        $storeRate = $this->_calculator->getStoreRate($request, $this->_store);
        return $rate == $storeRate;
    }

    /**
     * Round price based on previous rounding operation delta
     *
     * @param float $price
     * @param string $rate
     * @param bool $direction
     * @param string $type
     *
     * @return float
     */
    protected function _deltaRound($price, $rate, $direction, $type = 'regular')
    {
        if ($price) {
            $rate = (string)$rate;
            $type = $type . $direction;
            // initialize the delta to a small number to avoid non-deterministic behavior with rounding of 0.5
            $delta = isset($this->_roundingDeltas[$type][$rate]) ? $this->_roundingDeltas[$type][$rate] :0.000001;
            $price += $delta;
            $this->_roundingDeltas[$type][$rate] = $price - $this->_calculator->round($price);
            $price = $this->_calculator->round($price);
        }
        return $price;
    }

    /**
     * Recalculate row information for item based on children calculation
     *
     * @param   Vznl_Checkout_Model_Sales_Quote_Item $item
     *
     * @return  Mage_Tax_Model_Sales_Total_Quote_Subtotal
     */
    protected function _recalculateParent(Vznl_Checkout_Model_Sales_Quote_Item $item)
    {
        $rowTotal = 0;
        $baseRowTotal = 0;
        $rowTotalInclTax = 0;
        $baseRowTotalInclTax = 0;
        $rowTax = 0;
        $baseRowTax = 0;
        $store = $item->getStore();
        $qty = $item->getQty();

        /** @var Vznl_Checkout_Model_Sales_Quote_Item $child */
        foreach ($item->getChildren() as $child) {
            $rowTotal += $child->getMafRowTotal();
            $baseRowTotal += $child->getBaseMafRowTotal();
            $rowTotalInclTax += $child->getMafRowTotalInclTax();
            $baseRowTotalInclTax += $child->getBaseMafRowTotalInclTax();
            $rowTax += $child->getMafRowTax();
            $baseRowTax += $child->getBaseMafRowTax();
        }

        $item->setConvertedMaf($store->roundPrice($rowTotal) / $qty);
        $item->setMaf($store->roundPrice($baseRowTotal) / $qty);
        $item->setMafRowTotal($rowTotal);
        $item->setBaseMafRowTotal($baseRowTotal);
        $item->setMafInclTax($store->roundPrice($rowTotalInclTax) / $qty);
        $item->setBaseMafInclTax($store->roundPrice($baseRowTotalInclTax) / $qty);
        $item->setMafRowTotalInclTax($rowTotalInclTax);
        $item->setBaseMafRowTotalInclTax($baseRowTotalInclTax);
        $item->setMafRowTax($rowTax);
        $item->setBaseMafRowTax($baseRowTax);
        return $this;
    }

    /**
     * Get request for fetching store tax rate
     *
     * @param   Mage_Sales_Model_Quote_Address $address
     *
     * @return  Varien_Object
     */
    protected function _getStoreTaxRequest($address)
    {
        if (is_null($this->_storeTaxRequest)) {
            $this->_storeTaxRequest = $this->_calculator->getRateOriginRequest($address->getQuote()->getStore());
        }
        return $this->_storeTaxRequest;
    }

    /**
     * Get request for fetching address tax rate
     *
     * @param   Mage_Sales_Model_Quote_Address $address
     *
     * @return  Varien_Object
     */
    protected function _getAddressTaxRequest($address)
    {
        $addressTaxRequest = $this->_calculator->getRateRequest(
            $address,
            $address->getQuote()->getBillingAddress(),
            $address->getQuote()->getCustomerTaxClassId(),
            $address->getQuote()->getStore()
        );
        return $addressTaxRequest;
    }

    /**
     * Add row total item amount to subtotal
     *
     * @param   Mage_Sales_Model_Quote_Address $address
     * @param   Vznl_Checkout_Model_Sales_Quote_Item $item
     *
     * @return  Mage_Tax_Model_Sales_Total_Quote_Subtotal
     */
    protected function _addMafSubtotalAmount(Mage_Sales_Model_Quote_Address $address, $item)
    {
        if ($this->_config->priceIncludesTax($this->_store)) {
            $subTotal = $item->getMafRowTotalInclTax() - $item->getMafRowTax();
            $baseSubTotal = $item->getBaseMafRowTotalInclTax() - $item->getBaseMafRowTax();
            $address->setMafTotalAmount('maf_subtotal', $address->getMafTotalAmount('maf_subtotal') + $subTotal);
            $address->setBaseMafTotalAmount('maf_subtotal', $address->getBaseMafTotalAmount('maf_subtotal') + $baseSubTotal);
        } else {
            $address->setMafTotalAmount('maf_subtotal',
                $address->getMafTotalAmount('maf_subtotal') + $item->getMafRowTotal()
            );
            $address->setBaseMafTotalAmount('maf_subtotal',
                $address->getBaseMafTotalAmount('maf_subtotal') + $item->getBaseMafRowTotal());
        }
        $address->setMafSubtotalInclTax($address->getMafSubtotalInclTax() + $item->getMafRowTotalInclTax());
        $address->setBaseMafSubtotalInclTax($address->getBaseMafSubtotalInclTax() + $item->getBaseMafRowTotalInclTax());
        return $this;
    }

    /**
     * Unset item prices/totals with price include tax.
     * Operation is necessary for reset item state in case if configuration was changed
     *
     * @deprecated after 1.4.1
     *
     * @param   Mage_Sales_Model_Quote_Item_Abstract $item
     *
     * @return  Mage_Tax_Model_Sales_Total_Quote_Subtotal
     */
    protected function _resetItemPriceInclTax(Mage_Sales_Model_Quote_Item_Abstract $item)
    {
          $tax = $item->getPriceInclTax();
//        $item->setPriceInclTax(null);
//        $item->setBasePriceInclTax(null);
//        $item->setRowTotalInclTax(null);
//        $item->setBaseRowTotalInclTax(null);
        return $this;
    }

    /**
     *
     * @deprecated after 1.4.0.1
     *
     * @param   Mage_Sales_Model_Quote_Address $address
     *
     * @return  Mage_Tax_Model_Sales_Total_Quote_Subtotal
     */
    protected function _processShippingAmount($address)
    {
        return $this;
    }

    /**
     * Recollect item price and row total using after taxes subtract.
     * Declare item price including tax attributes
     *
     * @deprecated after 1.4.1
     *
     * @param   Mage_Sales_Model_Quote_Address $address
     * @param   Mage_Sales_Model_Quote_Item_Abstract $item
     *
     * @return  Mage_Tax_Model_Sales_Total_Quote_Subtotal
     */
    protected function _recollectItem($address, Mage_Sales_Model_Quote_Item_Abstract $item)
    {
        $store = $address->getQuote()->getStore();
        $request = $this->_getStoreTaxRequest($address);
        $request->setProductClassId($item->getProduct()->getTaxClassId());
        $rate = $this->_calculator->getRate($request);
        $qty = $item->getTotalQty();

        $price = $taxPrice = $item->getCalculationMafOriginal();
        $basePrice = $baseTaxPrice = $item->getBaseCalculationMafOriginal();
        $subtotal = $taxSubtotal = $item->getMafRowTotal();
        $baseSubtotal = $baseTaxSubtotal = $item->getBaseMafRowTotal();

        if ($this->_areTaxRequestsSimilar) {
            $item->setMafRowTotalInclTax($subtotal);
            $item->setBaseMafRowTotalInclTax($baseSubtotal);
            $item->setMafInclTax($price);
            $item->setBaseMafInclTax($basePrice);

            // todo
            $item->setTaxCalcPrice($taxPrice);
            $item->setBaseTaxCalcPrice($baseTaxPrice);
            $item->setTaxCalcRowTotal($taxSubtotal);
            $item->setBaseTaxCalcRowTotal($baseTaxSubtotal);
        }

        $this->_mafSubtotalInclTax += $subtotal;
        $this->_baseMafSubtotalInclTax += $baseSubtotal;

        if ($this->_config->getAlgorithm($store) == Mage_Tax_Model_Calculation::CALC_UNIT_BASE) {
            $taxAmount = $this->_calculator->calcTaxAmount($taxPrice, $rate, true);
            $baseTaxAmount = $this->_calculator->calcTaxAmount($baseTaxPrice, $rate, true);
            $unitPrice = $this->_calculator->round($price - $taxAmount);
            $baseUnitPrice = $this->_calculator->round($basePrice - $baseTaxAmount);
            $subtotal = $this->_calculator->round($unitPrice * $qty);
            $baseSubtotal = $this->_calculator->round($baseUnitPrice * $qty);
        } else {
            $taxAmount = $this->_calculator->calcTaxAmount($taxSubtotal, $rate, true, false);
            $baseTaxAmount = $this->_calculator->calcTaxAmount($baseTaxSubtotal, $rate, true, false);
            $unitPrice = ($subtotal - $taxAmount) / $qty;
            $baseUnitPrice = ($baseSubtotal - $baseTaxAmount) / $qty;
            $subtotal = $this->_calculator->round(($subtotal - $taxAmount));
            $baseSubtotal = $this->_calculator->round(($baseSubtotal - $baseTaxAmount));
        }

        $item->setMaf($baseUnitPrice);
        $item->setOriginalMaf($unitPrice);
        $item->setBaseMafPrice($baseUnitPrice);
        $item->setMafRowTotal($subtotal);
        $item->setBaseMafRowTotal($baseSubtotal);
        return $this;
    }

    /**
     * Check if we need subtract store tax amount from item prices
     *
     * @deprecated after 1.4.1
     *
     * @param Mage_Sales_Model_Quote_Address $address
     *
     * @return bool
     */
    protected function _needSubtractTax($address)
    {
        $store = $address->getQuote()->getStore();
        if ($this->_config->priceIncludesTax($store) || $this->_config->getNeedUsePriceExcludeTax()) {
            return true;
        }
        return false;
    }

    /**
     * Subtract shipping tax
     *
     * @deprecated after 1.4.0.1
     *
     * @param Mage_Sales_Model_Quote_Address $address
     *
     * @return bool
     */
    protected function _needSubtractShippingTax($address)
    {
        $store = $address->getQuote()->getStore();
        if ($this->_config->shippingPriceIncludesTax($store) || $this->_config->getNeedUseShippingExcludeTax()) {
            return true;
        }
        return false;
    }
}

