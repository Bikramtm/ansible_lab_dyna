<?php

class Vznl_Checkout_Model_Cookie extends Mage_Core_Model_Cookie
{
    /**
     * Make sure if a frontend_cid cookie also exists, to renew this cookie as well
     */
    public function renew($name, $period = null, $path = null, $domain = null, $secure = null, $httponly = null)
    {
        if ($this->get($name . '_cid')) {
            parent::renew($name . '_cid', $period, $path, $domain, $secure, $httponly);
        }

        return parent::renew($name, $period, $path, $domain, $secure, $httponly);
    }
}
