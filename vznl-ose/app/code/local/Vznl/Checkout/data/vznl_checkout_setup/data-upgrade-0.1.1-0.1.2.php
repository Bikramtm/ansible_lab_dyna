<?php
/**
 * Installer that adds the fixed_telephone_number column after customer_number
 */

/** @var Mage_Core_Model_Resource_Setup $this */

$this->startSetup();

// Add fixed_telephone_number on sales quote resource
$this->getConnection()->addColumn($this->getTable("sales/quote"), "fixed_telephone_number", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 20,
    "after" => "customer_number",
    "comment" => "External fixed telephone number to which the superorder maps to",
]);

$this->getConnection()
    ->addIndex(
        $this->getTable("sales/quote"),
        $this->getIdxName($this->getTable("sales/quote"), "fixed_telephone_number"),
        "fixed_telephone_number");

// Add fixed_telephone_number on sales order resource
$this->getConnection()->addColumn($this->getTable("sales/order"), "fixed_telephone_number", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 20,
    "after" => "customer_number",
    "comment" => "External fixed telephone number to which the superorder maps to",
]);

$this->getConnection()
    ->addIndex(
        $this->getTable("sales/order"),
        $this->getIdxName($this->getTable("sales/order"), "fixed_telephone_number"),
        "fixed_telephone_number");

$this->endSetup();