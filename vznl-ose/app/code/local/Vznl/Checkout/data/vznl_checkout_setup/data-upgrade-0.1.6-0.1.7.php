<?php
/**
 * Installer that adds the fixed_delivery_wish_date column after fixed_telephone_number
 */

/** @var Mage_Core_Model_Resource_Setup $this */

$this->startSetup();

// Add fixed_delivery_wish_date on sales quote resource
$this->getConnection()->addColumn($this->getTable("sales/quote"), "fixed_delivery_wish_date", [
    "type" => Varien_Db_Ddl_Table::TYPE_DATE,
    "after" => "fixed_telephone_number",
    "comment" => "The date on which the delivery is to be done",
]);

$this->getConnection()
    ->addIndex(
        $this->getTable("sales/quote"),
        $this->getIdxName($this->getTable("sales/quote"), "fixed_delivery_wish_date"),
        "fixed_delivery_wish_date");

// Add fixed_delivery_wish_date on sales order resource
$this->getConnection()->addColumn($this->getTable("sales/order"), "fixed_delivery_wish_date", [
    "type" => Varien_Db_Ddl_Table::TYPE_DATE,
    "after" => "fixed_telephone_number",
    "comment" => "The date on which the delivery is to be done",
]);

$this->getConnection()
    ->addIndex(
        $this->getTable("sales/order"),
        $this->getIdxName($this->getTable("sales/order"), "fixed_delivery_wish_date"),
        "fixed_delivery_wish_date");

$this->endSetup();