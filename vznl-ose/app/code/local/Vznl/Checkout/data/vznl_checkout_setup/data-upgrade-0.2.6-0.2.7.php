<?php
/* @var $installer Mage_Checkout_Model_Resource_Setup */
$installer = new Mage_Checkout_Model_Resource_Setup('core_setup');
$installer->startSetup();

$installer->run("INSERT INTO templateversion (path, type, start_date) VALUES ('checkout/cart/pdf/warranty/warranty_6.phtml','garant', now())");

$installer->run("INSERT INTO templateversion (path, type, start_date) VALUES ('/media/garant/Voorwaarden_Vodafone_Garant.pdf','garant_terms', now())");

$installer->run("INSERT INTO templateversion (path, type, start_date) VALUES ('/media/garant/Assurant_IPID.pdf','garant_ipid', now())");

$installer->endSetup();
