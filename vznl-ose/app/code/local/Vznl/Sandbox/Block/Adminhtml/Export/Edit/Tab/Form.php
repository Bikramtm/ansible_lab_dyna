<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */

/**
 * Class Vznl_Sandbox_Block_Adminhtml_Export_Edit_Tab_Form
 */
class Vznl_Sandbox_Block_Adminhtml_Export_Edit_Tab_Form extends Omnius_Sandbox_Block_Adminhtml_Export_Edit_Tab_Form
{
    const AXI_EXPORT_TYPE = 1;
    const DYNA_EXPORT_TYPE = 2;

    protected $_types = array(
        1 => 'Axi',
        2 => 'Dyna'
    );

    /**
     * Returns the export types
     * @return array
     */
    protected function getExportTypeOptions()
    {
        return array(
            "" => Mage::helper('sandbox')->__('Please select...'),
            Vznl_Sandbox_Model_Export::AXI_EXPORT_TYPE => Vznl_Sandbox_Model_Export::$_types[self::AXI_EXPORT_TYPE],
            Vznl_Sandbox_Model_Export::DYNA_EXPORT_TYPE => Vznl_Sandbox_Model_Export::$_types[self::DYNA_EXPORT_TYPE],
        );
    }

    /**
     * Get the fields for the export form
     * @return array
     */
    protected function getFields()
    {
        return array(
            'General' => array(
                'deadline' => array(
                    'type' => 'date',
                    'config' => array(
                        'label' => Mage::helper('sandbox')->__('Execution Deadline'),
                        'name' => 'deadline',
                        'time' => true,
                        'format' => 'yyyy-MM-dd HH:mm:ss',
                        'image' => $this->getSkinUrl('images/grid-cal.gif'),
                        'class' => 'required-entry',
                        'required' => true,
                    ),
                ),
                'type' => array(
                    'type' => 'select',
                    'config' => array(
                        'label' => Mage::helper('sandbox')->__('Export Type'),
                        'name' => 'type',
                        'class' => 'required-entry',
                        'required' => true,
                        'values' => $this->getExportTypeOptions()
                    ),
                ),
                'export_filename' => array(
                    'type' => 'text',
                    'config' => array(
                        'label' => Mage::helper('sandbox')->__('Export filename (including extension)'),
                        'name' => 'export_filename',
                        'class' => 'required-entry',
                        'required' => true,
                        'after_element_html' => '<span>Ex.: products.xml<br>(Timestamp will be appended before .xml extension)</span>'
                    ),
                ),
            ),
            'Destination' => array(
                'replica' => array(
                    'type' => 'select',
                    'config' => array(
                        'label' => Mage::helper('sandbox')->__('Target Replica'),
                        'name' => 'replica',
                        'values' => $this->getReplicaOptions(),
                        'class' => 'required-entry',
                        'required' => true,
                    ),
                ),
            ),
        );
    }

    /**
     * Get options for all replicates
     * @return array
     */
    protected function getReplicaOptions()
    {
        $options = array(
            null => Mage::helper('sandbox')->__('Please select...'),
        );
        /** @var Vznl_Sandbox_Model_Mysql4_Replica_Collection $replicas */
        $replicas = Mage::getResourceModel('sandbox/replica_collection');

        foreach ($replicas as $replica) {
            $options[$replica['name']] = $replica['name'];
        }
        return $options;
    }
}
