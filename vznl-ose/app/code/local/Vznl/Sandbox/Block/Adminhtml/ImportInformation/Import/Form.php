<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */

/**
 * Class Vznl_Sandbox_Block_Adminhtml_ImportInformation_Import_Form
 */
class Vznl_Sandbox_Block_Adminhtml_ImportInformation_Import_Form extends Dyna_Sandbox_Block_Adminhtml_Release_Import_Form
{
    /**
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form(array(
            'id'        => 'edit_form',
            'action'    => $this->getUrl("*/*/import"),
            'method'    => 'post'
        ));
        $form->setUseContainer(true);
        $this->setForm($form);

        $fieldset = $form->addFieldset("release_import_form", array("legend" => "Import"));

        $fieldset->addField('type[]', 'multiselect', array(
            'label'     => "Type import",
            'name'      => 'type[]',
            'values' => array(
                array('value'=>'mobile','label'=>'Mobile'),
                array('value'=>'fixed', 'label'=>'Fixed'),
                array('value'=>'campaign', 'label'=>'Campaign'),
                array('value'=>'bundles', 'label'=>'Bundle')
            ),
            'onclick'  => "",
            'onchange' => "",
            'disabled' => false,
            'tabindex' => 1,
            'required' => true
        ));

        return get_parent_class(parent::class)::_prepareForm();
    }
}
