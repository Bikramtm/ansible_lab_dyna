<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */

/**
 * Class Vznl_Sandbox_Block_Adminhtml_ImportInformation_Edit_Tabs
 */
class Vznl_Sandbox_Block_Adminhtml_ImportInformation_Edit_Tabs extends Dyna_Sandbox_Block_Adminhtml_ImportInformation_Edit_Tabs
{
    /**
     * Adds a custom tab on the form
     * @return Mage_Core_Block_Abstract
     * @throws Exception
     */
    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
            'label' => Mage::helper('sandbox')->__('Import Information'),
            'title' => Mage::helper('sandbox')->__('Import Information'),
            'content' => $this->getLayout()->createBlock('vznl_sandbox/adminhtml_importInformation_edit_tab_form')->toHtml(),
        ));
        return get_parent_class(parent::class)::_beforeToHtml();
    }
}
