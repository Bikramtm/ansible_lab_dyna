<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */

/**
 * Class Vznl_Sandbox_Block_Adminhtml_Release
 */
class Vznl_Sandbox_Block_Adminhtml_Release extends Dyna_Sandbox_Block_Adminhtml_Release
{
    /**
     * Override the constructor to customize the grid
     * Vznl_Sandbox_Block_Adminhtml_Release constructor.
     */
    public function __construct()
    {
        get_parent_class(parent::class)::__construct();

        $this->_controller = 'adminhtml_release';
        $this->_blockGroup = 'sandbox';
        $this->_headerText = Mage::helper('sandbox')->__('Release Manager');
        $this->_addButtonLabel = Mage::helper('sandbox')->__('Add New Release');
    }

}
