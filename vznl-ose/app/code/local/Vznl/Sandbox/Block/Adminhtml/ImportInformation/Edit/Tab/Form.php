<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */

/**
 * Class Vznl_Sandbox_Block_Adminhtml_ImportInformation_Edit_Tab_Form
 */
class Vznl_Sandbox_Block_Adminhtml_ImportInformation_Edit_Tab_Form extends Dyna_Sandbox_Block_Adminhtml_ImportInformation_Edit_Tab_Form
{
    private $_formData = null;
    private $_importSummaryData = [];

    /**
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);

        if (Mage::getSingleton('adminhtml/session')->getReleaseData()) {
            $data = Mage::getSingleton('adminhtml/session')->getReleaseData();
            Mage::getSingleton('adminhtml/session')->setReleaseData(null);
        } elseif (Mage::registry('import_information_data')) {
            $data = Mage::registry('import_information_data')->getData();
        }

        $this->prepareData($data);
        $this->_formData = $data;

        foreach ($this->getFields() as $fieldSetName => $setFields) {
            $fieldSet = $form->addFieldset(sprintf('sandbox_%s_form', strtolower($fieldSetName)),
                array('legend' => Mage::helper('sandbox')->__($fieldSetName)));
            foreach ($setFields as $name => $fieldDefinition) {
                if (isset($data) && isset($data[$name])) {
                    $fieldDefinition['config']['checked'] = $data[$name];
                }
                $fieldSet->addField($name, $fieldDefinition['type'], $fieldDefinition['config']);
            }
        }

        $form->setValues($data);

        return get_parent_class(parent::class)::_prepareForm();
    }

    protected function prepareData(&$data)
    {
        $duration = '-';
        $finishedAt = '-';
        /** @var Vznl_Sandbox_Helper_Data $sandboxHelper */
        $sandboxHelper = Mage::helper('vznl_sandbox');

        if ($data['finished_at']) {
            $startDate = new DateTime($data['started_at']);
            $finishDate = new DateTime($data['finished_at']);
            $interval = $startDate->diff($finishDate);

            $durationDays = $interval->days ? $interval->days . 'd ' : '';
            $durationHours = $interval->h ? $interval->h . 'h ' : '';
            $durationMinutes = $interval->i ? $interval->i . 'm ' : '';
            $durationSeconds = $interval->s ? $interval->s . 's ' : '';

            $duration = $durationDays . $durationHours . $durationMinutes . $durationSeconds;
            $finishedAt = $data['finished_at'];
        }
        $data['type'] = array_map("ucfirst", json_decode($data['type']));
        $data['files_total'] = 0;
        foreach ($data['type'] as $type) {
            foreach ($sandboxHelper->supportedFilenames as $values) {
                $type = str_replace('["', '', $type);
                $type = str_replace('"]', '', $type);
                if ($type == strtolower($values['stack']) && empty($values['optional'])) {
                    $data['files_total']++;
                }
            }
        }
        $data['type'] = implode(', ', $data['type']);
        $dbStatus = strtolower($data['status']);

        $importCollection = Mage::getModel('import/summary')
            ->getCollection()
            ->addFieldToFilter('execution_id', $data['id']);

        $data['files_skipped'] = 0;
        $data['files_imported'] = 0;
        foreach ($importCollection as $item) {
            $itemTotalRows = $item->getData('total_rows') . ' total rows';
            $itemImportedRows = $item->getData('imported_rows') . ' imported rows';
            $itemUpdatedRows = $item->getData('updated_rows') . ' updated rows';
            $itemSkippedRows = $item->getData('skipped_rows') . ' skipped rows';
            $data[$item->getData('category') . '_' . $item->getData('import_id')] = $itemTotalRows . ', ' . $itemImportedRows . ', ' . $itemUpdatedRows . ', ' . $itemSkippedRows;
            if ($item->getData('headers_validated') == 0 || $item->getData('has_errors') == 1) {
                $data['files_skipped']++;
            } else {
                $data['files_imported']++;
            }
            $this->_importSummaryData[] = $item->getData();
        }

        if ($dbStatus === 'running') {
            $response = $sandboxHelper->getRunningImportPIDs($data['id']);
            /** @var Dyna_Sandbox_Model_ImportInformation $executionModel */
            $executionModel = Mage::getModel('dyna_sandbox/importInformation');
            if (($response['exitCode'] != 0 || count($response['output']) == 0) && ($data['files_total'] > $data['files_imported'])) {
                $finishedAt = $sandboxHelper->setStatusForImportExecution($data['id'], Dyna_Sandbox_Model_ImportInformation::STATUS_ERROR) ?? $finishedAt;
                $dbStatus = $executionModel::STATUS_ERROR;
                $data['errors'] = 'Yes';
            } elseif ($data['files_total'] <= $data['files_imported'] && count($response['output']) == 0) {
                $finishedAt = $sandboxHelper->setStatusForImportExecution($data['id'], Dyna_Sandbox_Model_ImportInformation::STATUS_SUCCESS) ?? $finishedAt;
                $dbStatus = $executionModel::STATUS_SUCCESS;
                $data['errors'] = 'No';

            }
            $startDate = new DateTime($data['started_at']);
            $now = Mage::getSingleton('core/date')->date();
            $nowDate = new DateTime($now);
            $interval = $nowDate->diff($startDate);

            $durationDays = $interval->days ? $interval->days . 'd ' : '';
            $durationHours = $interval->h ? $interval->h . 'h ' : '';
            $durationMinutes = $interval->i ? $interval->i . 'm ' : '';
            $durationSeconds = $interval->s ? $interval->s . 's ' : '';

            $duration = $durationDays . $durationHours . $durationMinutes . $durationSeconds;
        }

        $data['status'] = $this->_status[$dbStatus];
        $data['duration'] = $duration;
        $data['finished_at'] = $finishedAt;
        $data['errors'] = $data['errors'] ? 'Yes' : 'No';
    }

    protected function getSummaryFields()
    {
        $result = [];

        /** @var Vznl_Sandbox_Helper_Data $sandboxHelper */
        $sandboxHelper = Mage::helper('vznl_sandbox');

        foreach ($this->_importSummaryData as $item) {
            $filename = substr($item['filename'], 0, strpos($item['filename'], '.'));

            // if Mobile import files have been processed, create the corresponding array
            if ((strpos(strtolower($item['filename']), strtolower($sandboxHelper::FILENAME_MOBILE_KEYWORD)) !== false)
                && (!isset($result[$sandboxHelper->_keywordToLabel[$sandboxHelper::FILENAME_MOBILE_KEYWORD] . ' Files Summary']))) {
                $result[$sandboxHelper->_keywordToLabel[$sandboxHelper::FILENAME_MOBILE_KEYWORD] . ' Files Summary'] = [];
            }
            // if Fixed import files have been processed, create the corresponding array
            if ((strpos(strtolower($item['filename']), strtolower($sandboxHelper::FILENAME_FIXED_KEYWORD)) !== false)
                && (!isset($result[$sandboxHelper->_keywordToLabel[$sandboxHelper::FILENAME_FIXED_KEYWORD] . ' Files Summary']))) {
                $result[$sandboxHelper->_keywordToLabel[$sandboxHelper::FILENAME_FIXED_KEYWORD] . ' Files Summary'] = [];
            }
            // if Campaign import files have been processed, create the corresponding array
            if ((strpos(strtolower($item['filename']), strtolower($sandboxHelper::FILENAME_CAMPAIGN_KEYWORD)) !== false)
                && (!isset($result[$sandboxHelper->_keywordToLabel[$sandboxHelper::FILENAME_CAMPAIGN_KEYWORD] . ' Files Summary']))) {
                $result[$sandboxHelper->_keywordToLabel[$sandboxHelper::FILENAME_CAMPAIGN_KEYWORD] . ' Files Summary'] = [];
            }
            // if Bundle import files have been processed, create the corresponding array
            if ((strpos(strtolower($item['filename']), strtolower($sandboxHelper::FILENAME_BUNDLE_KEYWORD)) !== false)
                && (!isset($result[$sandboxHelper->_keywordToLabel[$sandboxHelper::FILENAME_BUNDLE_KEYWORD] . ' Files Summary']))) {
                $result[$sandboxHelper->_keywordToLabel[$sandboxHelper::FILENAME_BUNDLE_KEYWORD] . ' Files Summary'] = [];
            }
            $fieldData = [
                'type' => 'label',
                'config' => [
                    'label' => Mage::helper('sandbox')->__($item['filename']),
                    'name' => $item['category'] . '_' . $item['import_id'],
                    'after_element_html' => " <a href=\"javascript:void(0)\" style='color: #FBAF34' onclick = \"new Popup('" . $this->getUrl('*/*/showLogs/',
                            array(
                                'executionId' => $item['execution_id'],
                                'category' => $filename
                            )) . "', {title:'View detailed logs', width: 800, height: 450});\">(view log)</a>",
                ],
            ];

            foreach ($sandboxHelper->_keywordToLabel as $keyword => $label) {
                if (strpos(strtolower($item['filename']), strtolower($keyword)) !== false ||
                    in_array(strtolower($item['filename']),
                        array_map('strtolower', $sandboxHelper->_fileToStack[$keyword]))
                ) {
                    $result[$label . ' Files Summary'][$label . ' Stack Log'] = [
                        'type' => 'label',
                        'config' => [
                            'label' => Mage::helper('sandbox')->__($label . ' Stack Log'),
                            'name' => 'Stack Log',
                            'after_element_html' => " <a href=\"javascript:void(0)\" style='color: #FBAF34' onclick = \"new Popup('" . $this->getUrl('*/*/showLogs/',
                                    array(
                                        'executionId' => $item['execution_id'],
                                        'category' => ucfirst($label) . '-stack_import'
                                    )) . "', {title:'View detailed logs', width: 800, height: 450});\">(view log)</a>",
                        ],
                    ];
                    $result[$label . ' Files Summary'][$item['category'] . '_' . $item['import_id']] = $fieldData;
                    break;
                }
            }
        }
        return $result;
    }
}
