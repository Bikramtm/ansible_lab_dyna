<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */

/**
 * Class Vznl_Sandbox_Block_Adminhtml_ImportInformation_Grid
 */
class Vznl_Sandbox_Block_Adminhtml_ImportInformation_Grid extends Dyna_Sandbox_Block_Adminhtml_ImportInformation_Grid
{
    /**
     * Initialize collection
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        /** @var  Dyna_Sandbox_Model_Mysql4_ImportInformation_Collection $collection */
        $collection = Mage::getModel('dyna_sandbox/importInformation')->getCollection()->setOrder('id', 'DESC');
        /** @var Dyna_Sandbox_Helper_Data $sandboxHelper */
        $sandboxHelper = Mage::helper('vznl_sandbox');

        /** @var Dyna_Sandbox_Model_ImportInformation $importExecution */
        foreach ($collection as $importExecution) {
            $dbStatus = strtolower($importExecution->getStatus());
            if ($dbStatus == 'running') {
                $response = $sandboxHelper->getRunningImportPIDs($importExecution->getId());
                foreach ($response['output'] as $k => $line) {
                    if (strpos($line, 'awk') !== false) {
                        unset($response['output'][$k]);
                    }
                }
                $data = $importExecution->getData();
                $data['files_total'] = 0;
                foreach (json_decode($data['type']) as $type) {
                    foreach ($sandboxHelper->supportedFilenames as $values) {
                        if ($type == $values['stack'] && empty($values['optional'])) {
                            $data['files_total']++;
                        }
                    }
                }

                $importCollection = Mage::getModel('import/summary')
                    ->getCollection()
                    ->addFieldToFilter('execution_id', $data['id']);

                $data['files_skipped'] = 0;
                $data['files_imported'] = 0;
                foreach ($importCollection as $item) {
                    if ($item->getData('headers_validated') == 0 || $item->getData('has_errors') == 1) {
                        $data['files_skipped']++;
                    } else {
                        $data['files_imported']++;
                    }
                }

                if ((($response['exitCode'] != 0) || ($this->getOutputCount($response) == 0))
                    || ($data['files_total'] > $data['files_imported'])) {
                    $data['finished_at'] = $sandboxHelper->setStatusForImportExecution($data['id'], Dyna_Sandbox_Model_ImportInformation::STATUS_ERROR) ?? $data['finished_at'];
                    $data['status'] = $importExecution::STATUS_ERROR;
                    $data['errors'] = 1;
                } elseif ($data['files_total'] <= $data['files_imported'] && $this->getOutputCount($response) == 0) {
                    $data['finished_at'] = $sandboxHelper->setStatusForImportExecution($data['id'], Dyna_Sandbox_Model_ImportInformation::STATUS_SUCCESS) ?? $data['finished_at'];
                    $data['status'] = $importExecution::STATUS_SUCCESS;
                    $data['errors'] = 0;
                }

                $importExecution->setData($data);
                $importExecution->save();
            }
        }
        $collection->clear();
        $this->setCollection($collection);

        return get_parent_class(parent::class);
    }

    public function getOutputCount($response)
    {
        return count($response['output']);
    }
}
