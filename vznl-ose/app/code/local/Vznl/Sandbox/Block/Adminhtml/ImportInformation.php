<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */

/**
 * Class Vznl_Sandbox_Block_Adminhtml_ImportInformation
 */
class Vznl_Sandbox_Block_Adminhtml_ImportInformation extends Dyna_Sandbox_Block_Adminhtml_ImportInformation
{
    protected $_importButtonLabel;

    public function __construct()
    {
        $this->_controller = 'adminhtml_importInformation';
        $this->_blockGroup = 'dyna_sandbox';
        $this->_headerText = Mage::helper('sandbox')->__('Import Overview');

        $this->_importButtonLabel = Mage::helper('sandbox')->__('Import');

        $buttonClass = 'go';
        if(count(Mage::helper("vznl_sandbox")->checkRunningImports()) > 1){
            $buttonClass .= ' disabled';
        }

        $this->_addButton('import', array(
            'label'     => Mage::helper('multimapper')->__('Import'),
            'onclick'   => 'javascript:new Popup(\'' . $this->getImportUrl() . '\', {title:\'' . Mage::helper('multimapper')->__('Import') . '\', width: 650, height:420})',
            'class'     => $buttonClass,
        ));

        get_parent_class(parent::class)::__construct();
        $this->_removeButton('add');
    }

    /**
     * @return string
     */
    public function getImportUrl()
    {
        return $this->getUrl('*/*/importView');
    }
}
