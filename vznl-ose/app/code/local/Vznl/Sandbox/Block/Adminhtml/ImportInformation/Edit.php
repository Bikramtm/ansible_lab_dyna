<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */

/**
 * Class Vznl_Sandbox_Block_Adminhtml_ImportInformation_Edit
 */
class Vznl_Sandbox_Block_Adminhtml_ImportInformation_Edit extends Dyna_Sandbox_Block_Adminhtml_ImportInformation_Edit
{
    public function __construct()
    {
        get_parent_class(parent::class)::__construct();

        $this->_objectId = 'id';
        $this->_blockGroup = 'vznl_sandbox';
        $this->_controller = 'adminhtml_importInformation';
        $buttonClass = 'kill';
        if(count(Mage::helper("vznl_sandbox")->checkRunningImports()) == 1){
            $buttonClass .= ' disabled';
        }
        $this->_addButton('Kill', array(
            "id"        => "kill",
            "label"     => $this->__("Kill Import Execution"),
            "onclick"   => "killImportExecution()",
            "class"     => $buttonClass,
        ), -100);
        $this->_removeButton('save');
        $this->_removeButton('delete');
    }

    /**
     * Adds a custom header on the new/edit form
     * @return string
     */
    public function getHeaderText()
    {
        return Mage::helper('sandbox')->__('Import Details');
    }
}
