<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Vznl_Sandbox_Block_Adminhtml_Release_Edit_Tab_Form
 */
class Vznl_Sandbox_Block_Adminhtml_Release_Edit_Tab_Form extends Dyna_Sandbox_Block_Adminhtml_Release_Edit_Tab_Form
{
    /**
     * Returns the options for all replicas
     * @return array
     */
    protected function getReplicaOptions()
    {
        $options = array(
            null => Mage::helper('sandbox')->__('Please select...'),
        );
        /** @var Vznl_Sandbox_Model_Mysql4_Replica_Collection $replicas */
        $replicas = Mage::getResourceModel('sandbox/replica_collection');

        foreach ($replicas as $replica) {
            $options[$replica['name']] = $replica['name'];
        }

        return $options;
    }
}
