<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */

/**
 * Class Vznl_Sandbox_Model_Export
 */
class Vznl_Sandbox_Model_Export extends Dyna_Sandbox_Model_Export
{
    const PRODUCTS_EXPORT_TYPE = 2;
    const PRODUCTS_TYPE = 'Products';
    const AXI_EXPORT_TYPE = 1;
    const AXI_TYPE = 'Axi';
    const DYNA_EXPORT_TYPE = 3;
    const DYNA_TYPE = 'Dyna';

    public static $_types = array(
        1 => 'Axi',
        2 => 'Products',
        3 => 'Dyna'
    );

    /**
     * @return string
     */
    public function getExportType()
    {
        if ( ! isset(self::$_types[(int) $this->getType()])) {
            throw new RuntimeException('Export type "%s" is invalid', $this->getType());
        }
        return self::$_types[(int) $this->getType()];
    }

    /**
     * @return array
     */
    public function getSftpArgs()
    {
        $replica = $this->getReplica();
        if ($replica->getId()) {
            return array_filter(array(
                'host' => $replica->getData('ssh_host'),
                'username' => $replica->getData('ssh_user'),
                'password' => base64_decode($replica->getData('ssh_pass')),
                'port' => $replica->getData('ssh_port'),
                'timeout' => ($replica->hasData('ssh_timeout') && 0 <= (int) $this->hasData('ssh_timeout'))
                    ? $replica->hasData('ssh_timeout')
                    : self::DEFAULT_TIMEOUT,
            ));
        }
        return array();
    }
}
