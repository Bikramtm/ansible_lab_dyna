<?php

/**
 * Class Dyna_Sandbox_Model_ExportStrategy_DynaXmlCatalog
 */
class Vznl_Sandbox_Model_ExportStrategy_DynaXmlCatalog extends Vznl_Sandbox_Model_ExportStrategy_ExportStrategy
{
    const BELCOMPANY_STORECODE = 'belcompany';
    const BELCOMPANY_STORENAME = 'BelCompany';
    const VODAFONE_STORENAME   = 'Vodafone';

    /** @var Vznl_Service_Model_SimpleDOM */
    protected $_data;

    /**
     * Init the XML document
     */
    public function start()
    {
        $baseXml = '<CatalogueRequest xmlns="urn:oasis:names:specification:ubl:schema:xsd:CatalogueRequest-2" xmlns:cac="urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2" xmlns:cbc="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="urn:oasis:names:specification:ubl:schema:xsd:CatalogueRequest-2  http://docs.oasis-open.org/ubl/os-UBL-2.1/xsd/maindoc/UBL-CatalogueRequest-2.1.xsd"/>';
        $data = new Vznl_Service_Model_SimpleDOM($baseXml);
        $namespaces = $data->getDocNamespaces(true);
        $ns = function($ns) use ($namespaces) {
            return isset($namespaces[$ns]) ? $namespaces[$ns] : null;
        };
        $data->addChild('UBLVersionID', 2.1, $ns('cbc'));
        $data->addChild('ID', 1, $ns('cbc'));
        $data->addChild('Name', 'SSFE catalog', $ns('cbc'));
        $data->addChild('IssueDate', date("Y-m-d"), $ns('cbc'));
        $data->addChild('IssueTime', date("H:i:s") . 'Z', $ns('cbc'));
        $data->addChild('Description', null, $ns('cbc'));

        $receiverParty = $data->addChild('ReceiverParty', null, $ns('cac'));
        $receiverParty
            ->addChild('PartyIdentification', null, $ns('cac'))
                ->addChild('ID', 'DYNA', $ns('cbc'));
        $receiverParty
            ->addChild('PartyName', null, $ns('cac'))
                ->addChild('Name', null, $ns('cbc'));
        $receiverParty->addChild('Language', null, $ns('cac'));
        $receiverParty->addChild('PostalAddress', null, $ns('cac'));
        $receiverParty->addChild('Contact', null, $ns('cac'));

        $providerParty = $data->addChild('ProviderParty', null, $ns('cac'));
        $providerParty
            ->addChild('PartyIdentification', null, $ns('cac'))
                ->addChild('ID', $this->_getStoreName(), $ns('cbc'));
        $providerParty
            ->addChild('PartyName', null, $ns('cac'))
                ->addChild('Name', null, $ns('cbc'));
        $providerParty->addChild('Language', null, $ns('cac'));
        $providerParty->addChild('PostalAddress', null, $ns('cac'));
        $providerParty->addChild('Contact', null, $ns('cac'));

        $this->_data = $data;
    }

    /**
     * @return Vznl_Service_Model_SimpleDOM|void
     */
    public function end()
    {
        return $this->_data;
    }

    /**
     * @return $this
     */
    public function resetData()
    {
        $this->_data = null;
        return $this;
    }

    /**
     * Executed for every item in the collection
     *
     * @param Dyna_Catalog_Model_Product $product
     * @return mixed
     */
    public function processItem($product)
    {
        $data = $this->_data;
        $namespaces = $data->getDocNamespaces(true);
        $ns = function($ns) use ($namespaces) {
            return isset($namespaces[$ns]) ? $namespaces[$ns] : null;
        };

        $catalogLine = $data->addChild('CatalogueRequestLine', null, $ns('cac'));
        $catalogLine->addChild('ID', $product->getId(), $ns('cbc'));
        $catalogLine->addChild('LineValidityPeriod', null, $ns('cac'));

        $itemLocationQty = $catalogLine->addChild('RequiredItemLocationQuantity', null, $ns('cac'));
        $price = $itemLocationQty->addChild('Price', null, $ns('cac'));
        $priceAmount = $price->addChild('PriceAmount', $product->getData('purchase_price') ?: '0.000', $ns('cbc'));
        $priceAmount->addAttribute('currencyID', 'EUR');
        $price->addChild('PriceChangeReason', 'New pricelist', $ns('cbc'));

        $item = $catalogLine->addChild('Item', null, $ns('cac'));
        $item
            ->addChild('Description', null, $ns('cbc'))
                ->insertCDATA($product->getShortDescription());
        $item
            ->addChild('Name', null, $ns('cbc'))
                ->insertCDATA($product->getName());
        $item->addChild('BrandName', $product->getAttributeText('prodspecs_brand'), $ns('cbc'));
        $item->addChild('ModelName', null, $ns('cbc'));
        $item
            ->addChild('SellersItemIdentification', null, $ns('cac'))
                ->addChild('ID', $product->getSku(), $ns('cbc'));

        $sapCodes = $product->getSku();
        foreach (array_filter(explode(',', $product->getSku())) as $sapCode) {
            $item
                ->addChild('ManufacturersItemIdentification', null, $ns('cac'))
                    ->addChild('ID', trim($sapCode), $ns('cbc'));
        }

        $attributeSetModel = Mage::getModel("eav/entity_attribute_set");
        $attributeSetModel->load($product->getAttributeSetId());
        $attributeSetName = $attributeSetModel->getAttributeSetName();
        $commodityClassification = $item->addChild('CommodityClassification', null, $ns('cac'));
        $commodityClassification->addChild('NatureCode', $attributeSetName, $ns('cbc'));
        $commodityClassification->addChild('ItemClassificationCode', null, $ns('cbc'));

        if ($sapCodes) {
            $additionalItemProperty = $item->addChild('AdditionalItemProperty', null, $ns('cac'));
            $additionalItemProperty->addChild('Name', 'SAP', $ns('cbc'));
            $additionalItemProperty->addChild('Value', trim($sapCodes), $ns('cbc'));
        }

        $barcodes = array_filter(array_map('trim', explode(',', $product->getData('identifier_ean_or_upc_code'))));
        foreach ($barcodes as $barcode) {
            $additionalItemProperty = $item->addChild('AdditionalItemProperty', null, $ns('cac'));
            $additionalItemProperty->addChild('Name', 'EAN', $ns('cbc'));
            $additionalItemProperty->addChild('Value', $barcode, $ns('cbc'));
        }
        $this->_data = $data;
    }

    /**
     * @return string
     */
    protected function _getStoreName()
    {
        return false !== strpos(strtolower(Mage::app()->getStore()->getCode()), self::BELCOMPANY_STORECODE)
            ? self::BELCOMPANY_STORENAME
            : self::VODAFONE_STORENAME;
    }
}