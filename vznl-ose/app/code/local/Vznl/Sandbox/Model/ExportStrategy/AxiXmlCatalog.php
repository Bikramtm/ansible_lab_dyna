<?php

/**
 * Class Vznl_Sandbox_Model_ExportStrategy_AxiXmlCatalog
 */
class Vznl_Sandbox_Model_ExportStrategy_AxiXmlCatalog extends Vznl_Sandbox_Model_ExportStrategy_ExportStrategy
{
    const BELCOMPANY_STORECODE = 'belcompany';
    const BELCOMPANY_FORMULA = 60000;
    const VODAFONE_FORMULA = 50000;

    /** @var Dyna_Service_Model_SimpleDOM */
    protected $_data;

    /** @var array */
    protected $_stores = array();

    /**
     * Init the XML document
     */
    public function start(Omnius_Core_Model_SimpleDOM $document = null)
    {
        $baseXml = '<n1:SALESPRODUCTLIST xmlns:n1="http://DynaLean/SSFE/Schemas/SalesProductList/V1-0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://DynaLean/SSFE/Schemas/Artiekl/V1-0 Artikel.xsd"/>';
        if (null === $document) {
            $document = new Omnius_Core_Model_SimpleDOM($baseXml);
            foreach ($document->getDocNamespaces(true) as $prefix => $uri) {
                $document->registerXPathNamespace($prefix, $uri);
            }
        }

        $this->_data = $document;
    }

    /**
     * @return Dyna_Service_Model_SimpleDOM|void
     */
    public function end()
    {
        return $this->_data;
    }

    /**
     * @return $this
     */
    public function resetData()
    {
        $this->_data = null;
        return $this;
    }

    private function _getFinanceCategory($product) {
        $attributeSetModel = Mage::getModel("eav/entity_attribute_set");
        $attributeSetModel->load($product->getAttributeSetId());
        $attributeSet  = $attributeSetModel->getAttributeSetName();
        $postOrPrepaidDevice = $product->getAttributeText('identifier_device_post_prepaid'); // identifier_device_post_prepaid # dropdown [ postpaid | prepaid ]
        $brand = $product->getAttributeText('prodspecs_brand'); // prodspecs_brand # dropdown
        $serviceItemType = $product->getAttributeText('service_item_type'); // service_item_type # dropdown [ eigen risico | eVoucher | Repair ]
        $subscriptionBrand = $product->getAttributeText('identifier_subscr_brand'); // identifier_subscr_brand # dropdown [ Vodafone | hollandsnieuwe ]
        $thuisKopieHeffingType = $product->getAttributeText('prijs_thuiskopieheffing_type'); // prijs_thuiskopieheffing_type # dropdown [ geen | Hoog | Laag ]
        $hawaiiType = $product->getData('identifier_hawaii_type'); // identifier_hawaii_type # text
        $postPaidOrSimonly = $product->getAttributeText('identifier_subsc_postp_simonly'); // identifier_subsc_postp_simonly # drodown [ postpaid | sim only ]

        $categoryId = '';

        if ($attributeSet === 'data_device' && $postOrPrepaidDevice === 'postpaid'
            && ($thuisKopieHeffingType === 'Hoog' || $thuisKopieHeffingType === 'Laag' || $thuisKopieHeffingType === 'Standaard')) {
            $categoryId = '1020'; // Tablets
        }
        if ($attributeSet === 'data_device' && $postOrPrepaidDevice === 'postpaid' && $thuisKopieHeffingType === 'geen') {
            $categoryId = '1030'; // USB Modems
        }
        if (($attributeSet === 'voice_subscription' || $attributeSet === 'data_subscription') && $subscriptionBrand === 'Vodafone' && $postPaidOrSimonly === 'sim only') {
            $categoryId = '5010'; // Sim only / Scherp Abonnementen VF
        }
        if (($attributeSet === 'voice_subscription' || $attributeSet === 'data_subscription') && $subscriptionBrand === 'Vodafone' && $postPaidOrSimonly === 'postpaid') {
            $categoryId = '5020'; // Hardware Abonnementen VF
        }
        if (($attributeSet === 'voice_subscription' || $attributeSet === 'data_subscription') && $subscriptionBrand === 'Belcompany' && $postPaidOrSimonly === 'sim only') {
            $categoryId = '5030'; // Sim only / Scherp Abonnementen Belco
        }
        if (($attributeSet === 'voice_subscription' || $attributeSet === 'data_subscription') && $subscriptionBrand === 'Belcompany' && $postPaidOrSimonly === 'postpaid') {
            $categoryId = '5040'; // Hardware Abonnementen Belco
        }
        if (($attributeSet === 'voice_subscription' || $attributeSet === 'data_subscription') && $subscriptionBrand === 'hollandsnieuwe' && $postPaidOrSimonly === 'sim only') {
            $categoryId = '5050'; // Sim only Abonnementen HN
        }
        if (($attributeSet === 'voice_subscription' || $attributeSet === 'data_subscription') && $subscriptionBrand === 'hollandsnieuwe' && $postPaidOrSimonly === 'postpaid') {
            $categoryId = '5060'; // Hardware Abonnementen HN
        }
        if ($attributeSet === 'dummy_device' ) {
            $categoryId = '10'; // Service/swap/demo telefoons
        }
        if ($attributeSet === 'voice_device' && $postOrPrepaidDevice === 'postpaid') {
            $categoryId = '1010'; // Telefoons
        }
        if ($attributeSet === 'prepaid_simonly' && $brand === 'Vodafone' && !$hawaiiType ) {
            $categoryId = '2010'; // Prepaid sim
        }
        if ($attributeSet === 'service_item' && $serviceItemType === 'eVoucher') {
            $categoryId = '2020'; // Prepaid top-up
        }
        if ($attributeSet === 'voice_device' && $postOrPrepaidDevice === 'prepaid') {
            $categoryId = '2030'; // Prepaid pack
        }
        if ($attributeSet === 'prepaid_simonly' && $brand === 'Vodafone' && $hawaiiType === 'Hybride') {
            $categoryId = '2040'; // Prepaid Hybrid
        }
        if ($attributeSet === 'data_device' && $postOrPrepaidDevice === 'prepaid') {
            $categoryId = '2050'; // Prepaid MBB
        }
        if ($attributeSet === 'accessoire') {
            $categoryId = '30'; // Accessoires
        }
        if ($attributeSet === 'simcard' && $subscriptionBrand === 'Vodafone') {
            $categoryId = '4010'; // Postpaid simkaarten VF
        }
        if ($attributeSet === 'simcard' && $subscriptionBrand === 'hollandsnieuwe') {
            $categoryId = '4020'; // Postpaid simkaarten HN
        }
        if ($attributeSet === 'simcard' && $subscriptionBrand === 'Belcompany') {
            $categoryId = '4030'; // Postpaid simkaarten Belcompany
        }
        if ($attributeSet === 'addon' && $subscriptionBrand === 'Vodafone') {
            $categoryId = '7010'; // Add-ons VF
        }
        if ($attributeSet === 'addon' && $subscriptionBrand === 'hollandsnieuwe') {
            $categoryId = '7020'; // Add-ons HN
        }
        if ($attributeSet === 'addon' && $subscriptionBrand === 'Belcompany') {
            $categoryId = '7030'; // Add-ons Belco
        }
        if ($attributeSet === 'service_item' && $serviceItemType === 'Repair') {
            $categoryId = '8010'; // Reparatiekosten
        }
        if ($attributeSet === 'service_item' && $serviceItemType === 'eigen risico') {
            $categoryId = '8020'; // Vodafone Garant eigen risico
        }
        if ($attributeSet === 'service_item' && !$serviceItemType) {
            $categoryId = '90'; // Overige Services
        }
        return $categoryId;
    }

    /**
     * Executed for every item in the collection
     *
     * @param Dyna_Catalog_Model_Product $product
     * @return mixed
     */
    public function processItem($product)
    {
        $taxRate = $product->getId() ? $product->getTaxRate() : Mage::helper('dyna_catalog')->getDefaultCountryTaxRate();
        $data = $this->_data;
        $namespaces = $data->getDocNamespaces(true);
        $ns = function($ns) use ($namespaces) {
            return isset($namespaces[$ns]) ? $namespaces[$ns] : null;
        };

        $salesProduct = $data->addChild('SALESPRODUCT', null, $ns('n1'));
        $salesProduct->addChild('PRODUCTCODE', $this->_getProductCode($product), $ns('n1'));
        $salesProduct->addChild('LASTCHANGED', date('c', strtotime($product->getUpdatedAt())), $ns('n1'));
        $salesProduct->addChild('FORMULE', $this->_getFormula($product), $ns('n1'));
        $salesProduct->addChild('DESCRIPTION', null, $ns('n1'))
            ->addCData($this->_formatAxiString(substr($product->getName(), 0, 80)));
        $salesProduct->addChild('BRANDNAME', $product->getAttributeText('prodspecs_brand'), $ns('n1'));
        $salesProduct->addChild('VATPERCENTAGE', $taxRate, $ns('n1'));
        $salesProduct->addChild('SALESPRICEINCLVAT', round($product->getPrice(), 2), $ns('n1'));
        $salesProduct->addChild('NEWSALESPRICEINCLVAT', ($product->getNewPrice() > 0) ? round($product->getNewPrice(), 2) : '0', $ns('n1'));
        $salesProduct->addChild('NEWSALESPRICESTARTDATETIME', date('c', strtotime($product->getNewPriceDate())), $ns('n1'));
        $salesProduct->addChild('STOCKINDICATOR', $this->_getStockableIndication($product), $ns('n1'));
        $salesProduct->addChild('SEPARATESALESINDICATOR', $this->_getSeperateSalesIndicator($product), $ns('n1'));
        $salesProduct->addChild('PURCHASEPRODUCTCODES', $this->_formatCodeList($product->getData('identifier_sap_codes')), $ns('n1'));
        $salesProduct->addChild('BARCODES', $this->_formatCodeList($product->getData('identifier_ean_or_upc_code')), $ns('n1'));
        $salesProduct->addChild('FINANCECATEGORY', $this->_getFinanceCategory($product), $ns('n1'));

        $this->_data = $data;
    }

    /**
     * @param Dyna_Catalog_Model_Product $product
     * @return int
     */
    protected function _getFormula($product)
    {
        if ( ! count($this->_stores)) {
            $this->_stores = Mage::app()->getStores();
        }

        /** @var Mage_Core_Model_Store $_store */
        $_store = isset($this->_stores[$product->getStoreId()])
            ? $this->_stores[$product->getStoreId()]
            : null;

        return ($_store && false !== strpos(strtolower($_store->getCode()), self::BELCOMPANY_STORECODE))
            ? self::BELCOMPANY_FORMULA
            : self::VODAFONE_FORMULA;
    }

    /**
     * @param Dyna_Catalog_Model_Product $product
     * @return string
     */
    protected function _getProductCode($product)
    {
        return strtoupper(substr($product->getSku(), 0, 20));
    }

    /**
     * @param Dyna_Catalog_Model_Product $product
     * @return string
     */
    protected function _getSeperateSalesIndicator($product)
    {
        return $product->getData('axi_los_verkoopbaar') ? '1' : '0';
    }

    /**
     * @param Dyna_Catalog_Model_Product $product
     * @return string
     */
    protected function _getStockableIndication($product)
    {
        return $product->getData('identifier_axi_stockable_prod') ? '1' : '0';
    }

    /**
     * @param $data
     * @return mixed
     */
    protected function _formatCodeList($data)
    {
        return str_replace(' ', '', str_replace(',', ';', $data));
    }

    /**
     * @param string $string
     * @return string
     */
    protected function _formatAxiString($string)
    {
        return str_replace('€', 'EUR', $string);
    }
}