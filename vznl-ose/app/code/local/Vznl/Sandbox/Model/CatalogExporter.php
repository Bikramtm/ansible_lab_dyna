<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */

/**
 * Class Vznl_Sandbox_Model_CatalogExporter
 */
class Vznl_Sandbox_Model_CatalogExporter
{
    const BATCH_SIZE = 250;

    /**
     * @param $collection
     * @param Vznl_Sandbox_Model_ExportStrategy_ExportStrategy $strategy
     * @param null $document
     * @return mixed
     */
    public function export($collection, Vznl_Sandbox_Model_ExportStrategy_ExportStrategy $strategy, $document = null)
    {
        if (func_num_args() === 3) {
            $args = func_get_args();
            $document = $args[2];
        }
        return $this->walk(
            $collection,
            array($strategy, 'processItem'),
            array($strategy, 'start'),
            array($strategy, 'end'),
            array($strategy, 'afterBatch'),
            $strategy->getBatchSize(),
            isset($document) ? $document : null
        );
    }

    /**
     * @param Varien_Data_Collection $collection
     * @param array $itemCallback
     * @param array $startCallback
     * @param array $endCallback
     * @param array $afterBatchCallback
     * @param null $batchSize
     * @param null $document
     * @return mixed
     */
    protected function walk(Varien_Data_Collection $collection,
                            array $itemCallback,
                            array $startCallback,
                            array $endCallback,
                            array $afterBatchCallback,
                            $batchSize = null,
                            $document = null
    )
    {
        $batchSize = $batchSize ?: self::BATCH_SIZE;

        $this->callUserFunc($startCallback, $document);

        $collection->setPageSize($batchSize);

        $currentPage = 1;
        $pages = $collection->getLastPageNumber();

        do {
            $collection->setCurPage($currentPage);
            $collection->load();
            foreach ($collection as $item) {
                $this->callUserFunc($itemCallback, $item);
            }
            $this->callUserFunc($afterBatchCallback);
            $currentPage++;
            $collection->clear();
        } while ($currentPage <= $pages);

        return $this->callUserFunc($endCallback);
    }

    /**
     * Trigger call_user_func on a given function and parameter
     * @param $function
     * @param $parameter
     */
    protected function callUserFunc($function, $parameter = null)
    {
        return call_user_func($function, $parameter);
    }
}
