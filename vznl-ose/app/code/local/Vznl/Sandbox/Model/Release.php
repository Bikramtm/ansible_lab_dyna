<?php
class Vznl_Sandbox_Model_Release extends Dyna_Sandbox_Model_Release
{
    /**
     * Override the save method to make sure deadline/created_at/execution_time are not affected by environment timezone settings
     * @return $this|Mage_Core_Model_Abstract
     */
    public function save()
    {
        //if status was already set to success, prevent other changes
        $temp = Mage::getModel('sandbox/release')->load($this->getId());
        if ($temp->getStatus() == 'success') {
            return $this;
        }

        if($deadline = Mage::app()->getRequest()->getParam('deadline')){
            $this->setDeadline($deadline);
        }

        if($oneTimeDate = Mage::app()->getRequest()->getParam('one_time_date')){
            $this->setOneTimeDate($oneTimeDate);
        }

        if ( ! $this->hasData('created_at')) {
            $this->setCreatedAt(NULL);
        }

        return parent::save();
    }
}