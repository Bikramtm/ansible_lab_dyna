<?php
/**
 * Copyright (c) 2019. Dynacommerce B.V.
 */

/**
 * Class Vznl_Sandbox_Model_Mysql4_Export
 */
class Vznl_Sandbox_Model_Mysql4_Export extends Mage_Core_Model_Mysql4_Abstract
{
    /**
     * Constructor override
     */
    protected function _construct()
    {
        $this->_init('sandbox/export', 'id');
    }
}
