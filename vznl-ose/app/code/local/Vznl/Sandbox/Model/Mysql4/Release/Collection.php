<?php
/**
 * Copyright (c) 2019. Dynacommerce B.V.
 */

/**
 * Class Vznl_Sandbox_Model_Mysql4_Release_Collection
 */
class Vznl_Sandbox_Model_Mysql4_Release_Collection extends Omnius_Sandbox_Model_Mysql4_Release_Collection
{
    /**
     * Constructor override
     */
    public function _construct()
    {
        $this->_init('sandbox/release');
    }
}
