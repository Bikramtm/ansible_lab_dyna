<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */
require_once ('app/code/community/Dyna/Sandbox/controllers/Adminhtml/ImportInformationController.php');
/**
 * Class Vznl_Sandbox_Adminhtml_ImportInformationController
 */
class Vznl_Sandbox_Adminhtml_ImportInformationController extends Dyna_Sandbox_Adminhtml_ImportInformationController
{
    /**
     * Initialize a certain action
     * @return $this
     */
    protected function _initAction()
    {
        $this->loadLayout()->_setActiveMenu('sandbox/import')
            ->_addBreadcrumb(Mage::helper('adminhtml')->__('Import Overview'), Mage::helper('adminhtml')->__('Import Overview'));
        return $this;
    }

    /**
     * Render the layout on default index action
     */
    public function indexAction()
    {
        $this->_title($this->__('Sandbox'));
        $this->_title($this->__('Import Overview'));

        $this->_initAction();
        $this->renderLayout();
    }

    /**
     * Patch for SUPEE-6285
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return true;
    }

    /**
     * @return string
     */
    public function importViewAction()
    {
        $this->loadLayout();
        $block =  $this->getLayout()->createBlock('vznl_sandbox/adminhtml_importInformation_import');
        $this->getResponse()->setBody($block->toHtml());
    }

    public function importAction()
    {
        $messages = Mage::helper("vznl_sandbox")->checkRunningImports();
        if (($data = $this->getRequest()->getPost())&& count($messages) == 1) {

            /** @var Dyna_Sandbox_Model_ImportInformation $model */
            $model = Mage::getModel('dyna_sandbox/importInformation');

            $model->setType(json_encode($data['type']));
            $model->setErrors(0);
            $model->setStartedAt(date("Y-m-d H:i:s", Mage::getModel('core/date')->timestamp()));
            $model->setStatus($model::STATUS_RUNNING);
            $model->save();

            if($model->getId()){
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Import started for types: '. implode(',',$data['type']) . '. This may take a while.'));
                /** @var Vznl_Sandbox_Helper_Data $importHelper */
                $importHelper = Mage::helper('vznl_sandbox');
                $importHelper->performImport($model);
            }
        }elseif(count($messages) > 1){
            foreach ($messages as $message){
                Mage::getSingleton('adminhtml/session')->addError($message);
            }
        }

        //redirect to index
        $this->_redirect('*/*');
    }

    /**
     * Displays log contents for a specific import
     */
    public function showLogsAction()
    {
        $executionId = $this->getRequest()->getParam("executionId");
        $category = $this->getRequest()->getParam("category");

        /** @var Vznl_Import_Helper_Data $importHelper */
        $importHelper = Mage::helper('vznl_import');
        $logContent = $importHelper->getLogContent($executionId, $category);

        $this->loadLayout();
        /** @var Dyna_Sandbox_Block_Adminhtml_ImportInformation_Log $block */
        $block = $this->getLayout()->createBlock('vznl_sandbox/adminhtml_importInformation_log')->setTemplate('sandbox/showLog.phtml');
        $block->addData([
            'executionId' => $executionId,
            'category' => $category,
            'logContent' => $logContent,
            'categoriesToFiles' => $importHelper->getCategoriesToFiles()
        ]);

        $this->getResponse()->setBody($block->toHtml());
    }

    public function killImportExecutionAction()
    {
        $id = $this->getRequest()->getParam('id');
        $isAjax = $this->getRequest()->getParam('isAjax');
        $messages = Mage::helper("dyna_sandbox")->checkRunningImports();
        if ($this->getRequest()->isPost() && count($messages) != 1) {
            /** @var Dyna_Sandbox_Model_ImportInformation $model */
            $model = Mage::getModel('dyna_sandbox/importInformation')->load($id);

            if($model->getId()){
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Killed import with Execution Id ' . $id));
                /** @var Dyna_Sandbox_Helper_Data $importHelper */
                $importHelper = Mage::helper('dyna_sandbox');
                $importHelper->killImport($model);
                $model->setStatus($model::STATUS_KILLED);
                $model->setFinishedAt(date("Y-m-d H:i:s", Mage::getModel('core/date')->timestamp()));
                $model->save();
            }
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('You cannot kill this'));
        }
        if ($isAjax) {
            $this->getResponse()->setBody($this->getUrl('*/*'));
        } else {
            //redirect to index
            $this->_redirect('*/*');
        }
    }

    public function getExecutionStatusAction()
    {
        $status = "";
        $id = $this->getRequest()->getParam('id');
        $isAjax = $this->getRequest()->getParam('isAjax');
        $messages = Mage::helper("dyna_sandbox")->checkRunningImports();
        if ($this->getRequest()->isPost() && count($messages) != 1) {
            /** @var Dyna_Sandbox_Model_ImportInformation $model */
            $model = Mage::getModel('dyna_sandbox/importInformation')->load($id);
            if($model->getId()){
                $status = $model->getData('status');
            }
        }
        $this->getResponse()->setBody($status);
    }
}
