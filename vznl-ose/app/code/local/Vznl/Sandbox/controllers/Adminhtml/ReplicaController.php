<?php

require_once ('app/code/community/Omnius/Sandbox/controllers/Adminhtml/ReplicaController.php');
/**
 * Class Vznl_Sandbox_Adminhtml_ReplicaController
 */
class Vznl_Sandbox_Adminhtml_ReplicaController extends Omnius_Sandbox_Adminhtml_ReplicaController
{
    /**
     * Persist the data in the database
     */
    public function saveAction()
    {
        $post_data = $this->getRequest()->getPost();
        if ($post_data) {
            if (isset($post_data['mysql_password']) && false !== strpos($post_data['mysql_password'], '*')) {
                unset($post_data['mysql_password']);
            } else {
                $post_data['mysql_password'] = base64_encode($post_data['mysql_password']);
            }
            if (isset($post_data['ssh_pass']) && false !== strpos($post_data['ssh_pass'], '*')) {
                unset($post_data['ssh_pass']);
            } else {
                $post_data['ssh_pass'] = base64_encode($post_data['ssh_pass']);
            }
            if (!isset($post_data['created_at'])) {
                $dateNl = new DateTime("now", new DateTimeZone('Europe/Amsterdam'));
                $post_data['created_at'] = $dateNl->format('Y-m-d H:i:s');
            }
            try {
                $model = Mage::getModel('sandbox/replica')
                    ->addData($post_data)
                    ->setId($this->getRequest()->getParam('id'))
                    ->save();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Replica was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setReplicaData(false);

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId()));

                    return;
                }
                $this->_redirect('*/*/');

                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setReplicaData($this->getRequest()->getPost());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));

                return;
            }
        }
        $this->_redirect('*/*/');
    }

    /**
     * Trigger edit mode for a record
     */
    public function editAction()
    {
        $this->_title($this->__('Sandbox'));
        $this->_title($this->__('Replica'));
        $this->_title($this->__('Edit Item'));

        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('sandbox/replica')->load($id);
        if ($model->getId()) {
            Mage::register('replica_data', $model);
            $this->loadLayout();
            $this->_setActiveMenu('sandbox/replica');
            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Replica Manager'), Mage::helper('adminhtml')->__('Replica Manager'));
            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Replica Description'), Mage::helper('adminhtml')->__('Replica Description'));
            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock('sandbox/adminhtml_replica_edit'))->_addLeft($this->getLayout()->createBlock('sandbox/adminhtml_replica_edit_tabs'));
            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('sandbox')->__('Item does not exist.'));
            $this->_redirect('*/*/');
        }
    }

    /**
     * Deletes a certain provided record
     */
    public function deleteAction()
    {
        if ($this->getRequest()->getParam('id') > 0) {
            try {
                $model = Mage::getModel('sandbox/replica');
                $model->setId($this->getRequest()->getParam('id'))->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }

    /**
     * Trigger the mass remove to delete multiple selected records
     */
    public function massRemoveAction()
    {
        try {
            $ids = $this->getRequest()->getPost('ids', array());
            foreach ($ids as $id) {
                $model = Mage::getModel('sandbox/replica');
                $model->setId($id)->delete();
            }
            Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item(s) was successfully removed'));
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }
        $this->_redirect('*/*/');
    }
}
