<?php

use GuzzleHttp\Client;

class Vznl_RemoveItemFromBasket_Adapter_Peal_Factory
{
    public static function create()
    {
        $client = new Client();
        $basketHelper = Mage::helper('vznl_removeitemfrombasket');
        return new Vznl_RemoveItemFromBasket_Adapter_Peal_Adapter(
            $client,
            $basketHelper->getEndPoint(),
            $basketHelper->getChannel(),
            $basketHelper->getCountry()
        );
    }
}