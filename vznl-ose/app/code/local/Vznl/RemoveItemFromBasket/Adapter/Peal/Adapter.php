<?php

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;

/**
 * Class Vznl_RemoveItemFromBasket_Adapter_Peal_Adapter
 */
class Vznl_RemoveItemFromBasket_Adapter_Peal_Adapter
{
    CONST name = "RemoveItemFromBasket";
    /**
     * @var string
     */
    private $endpoint;

    /**
     * @var Client
     */
    private $client;

    /**
     * @var string
     */
    private $channel;

    /**
     * @var string
     */
    private $cty;

    /**
     * @var string
     */
    private $executionTimeId;

    /**
     * @var string
     */
    private $requestTimeId;

    /**
     * Adapter constructor.
     *
     * @param Client $client
     * @param string $endpoint
     * @param string $channel
     * @param string $cty
     */
    public function __construct(Client $client, string $endpoint, string $channel, string $cty)
    {
        $this->endpoint = $endpoint;
        $this->channel = $channel;
        $this->cty = $cty;
        $this->client = $client;
    }

    /**
     * @param $basketId
     * @param $technicalId
     * @return array|string
     * @throws GuzzleException
     */
    public function call($basketId, $technicalId = false)
    {
        $this->executionTimeId = $this->getHelper()->initTimeMeasurement();

        if (is_null($basketId)) {
            return 'No basketId provided to adapter';
        }
        $userName = $this->getHelper()->getLogin();
        $password = $this->getHelper()->getPassword();
        $verify = $this->getHelper()->getVerify();
        $proxy = $this->getHelper()->getProxy();

        $removeItem['basketItemId'] = $technicalId;
        $requestLog['url'] = $this->getUrl($basketId);
        $requestLog['body'] = $removeItem;
        try {
            $this->requestTimeId = $this->getHelper()->initTimeMeasurement();

            if ($userName != '' && $password != '') {
                $response = $this->client->request('POST', $this->getUrl($basketId),
                    [
                        'auth' => [$userName, $password],
                        'body' => json_encode($removeItem),
                        'verify' => $verify,
                        'proxy' => $proxy,
                        'headers' => [
                            'Content-Type' => 'application/json'
                        ]
                    ]
                );
            } else {
                $response = $this->client->request('POST', $this->getUrl($basketId),
                    [
                        'body' => json_encode($removeItem),
                        'verify' => $verify,
                        'proxy' => $proxy,
                        'headers' => [
                            'Content-Type' => 'application/json'
                        ]
                    ]
                );
            }

            $this->getHelper()->transferLog(
                json_encode($requestLog),
                json_encode(json_decode($response->getBody())),
                self::name,
                [
                    'execution' => $this->executionTimeId,
                    'request' => $this->requestTimeId
                ],
                'PEAL'
            );

            $source = json_decode($response->getBody(), true);
        } catch (ClientException|RequestException $exception) {
            $this->getHelper()->transferLog(
                json_encode($requestLog),
                ['msg' => $exception->getMessage(), 'code' => $exception->getCode()],
                self::name,
                [
                    'execution' => $this->executionTimeId,
                    'request' => $this->requestTimeId
                ],
                'PEAL'
            );

            $result['error'] = true;
            $result['message'] = $exception->getMessage();
            $result['code'] = $exception->getCode();
            return $result;
        }

        return $source;
    }

    /**
     * @param $basketId
     * @return string
     */
    public function getUrl($basketId)
    {
        return $this->endpoint . $basketId . "/items/remove?" .
            "cty=" . $this->cty .
            "&chl=" . $this->channel;
    }

    /**
     * @return object Vznl_RemoveItemFromBasket_Helper_Data
     */
    public function getHelper()
    {
        return Mage::helper('vznl_removeitemfrombasket');
    }
}
