<?php
/**
 * Class Vznl_Barcode_Model_Barcode
 */
class Vznl_Barcode_Model_Barcode
{
    /**
     * @param $data
     * @param string $barcodeType
     * @param null $saveName
     * @return string
     */
    public function generate($data, $barcodeType = 'code128', $saveName = null)
    {
        $imageData = $this->render($data, $barcodeType);

        if (!$saveName) {
            return $imageData;
        }

        $path = $this->getPath($saveName);
        $io = new Varien_Io_File();
        $io->streamOpen($path);
        $io->streamWrite($imageData);
        $io->streamClose();

        return $path;
    }

    /**
     * @param $saveName
     * @return string
     */
    protected function getPath($saveName)
    {
        if (!$saveName) {
            $saveName = md5(rand(0, 100) . time()) . '.png';
        }
        $path = rtrim(Mage::getBaseDir('cache'), DS) . DS . 'barcodes';
        $io = new Varien_Io_File();
        $io->mkdir($path);

        return $path . DS . $saveName;
    }

    /**
     * @param $data
     * @param $barcodeType
     * @return string
     */
    protected function render($data, $barcodeType)
    {
        $config = new Zend_Config([
            'barcode'        => $barcodeType,
            'barcodeParams'  => ['text' => $data],
            'renderer'       => 'image',
            'rendererParams' => ['imageType' => 'png'],
        ]);

        /** @var Zend_Barcode_Renderer_Image $renderer */
        $renderer = Zend_Barcode::factory($config);
        $resource = $renderer->draw();
        ob_start();
        imagepng($resource);
        $data = ob_get_clean();
        @imagedestroy($resource);
        unset($renderer);

        return $data;
    }
} 