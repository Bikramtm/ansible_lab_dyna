<?php

class Vznl_RestApi_Orders_GetDeliveryOrder extends Vznl_RestApi_Orders_Abstract
{
    /**
     * @var array
     */
    const PACKAGE_TYPES_MAP = [
        Dyna_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION => Omnius_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION,
    ];

    protected $order;
    protected $versions;

    /**
     * @param Mage_Core_Controller_Request_Http|array $request
     */
    public function __construct($request)
    {
        if (is_array($request)) {
            $orderId = $request['order_id'];
        } else {
            $routeParts = explode('/', $request->getQuery('route'));
            $orderId = $routeParts[4];
        }

        $this->order = Mage::getModel('sales/order')->load($orderId);

        if (!$this->order->getId()) {
            throw new Exception('Order not found');
        }
        
        // Emulate the superorder code in order to read the product data for that store instead of default which may be different
        $appEmulation = Mage::getSingleton('core/app_emulation');
        $appEmulation->startEnvironmentEmulation($this->order->getStoreId());
    }

    /**
     * Process the HTTP request.
     * 
     * @return array
     */
    public function process()
    {
        $agent = Mage::getModel('agent/agent')->load($this->order->getAgentId());
        $store = Mage::getModel('core/store')->load($this->order->getStoreId());
        $deliveryDateSlot = new \DateTime($this->order->getPlannedDate());
        $shippingAddress = $this->order->getShippingAddress();

        $data = [
            'id' => $this->order->getId(),
            'created' => $this->order->getCreatedAt(),
            'invoicenr' => $this->order->getIncrementId(),

            // Agent
            'agent' => [
                'username' => $agent->getUsername(),
                'firstname' => $agent->getFirstName(),
                'lastname' => $agent->getLastName(),
                'email' => $agent->getEmail(),
                'employee_number' => $agent->getEmployeeNumber(),
                'channel' => $store->getCode(),
                'dealercode' => $agent->getDealer()->getVfDealerCode()
            ],

            // Status
            'status' => [
                'status' => $this->order->getStatus(),
                'date' => is_null($this->order->getPlannedDate()) ? null : $deliveryDateSlot->format('Y-m-d'),
                'time' => is_null($this->order->getPlannedDate()) ? null : $deliveryDateSlot->format('H:i'),
                'service' => $this->order->getCarrierName(),
                'track_n_trace' => $this->order->getTrackAndTraceUrl()
            ],

            // Shipping
            'type' => $shippingAddress->getDeliveryType(),
            'name' => $shippingAddress->getDeliveryType() == 's' ? $shippingAddress->getDeliveryStoreId() : $shippingAddress->getName(),
            'address' => [
                'city' => $shippingAddress->getCity(),
                'country' => $shippingAddress->getCountry(),
                'street' => implode(' ', $shippingAddress->getStreet()),
                'postalcode' => $shippingAddress->getPostcode()
            ],

            // Payment
            'payment' => [
                'type' => $this->order->getPayment()->getMethod(),
                'amount' => round($this->order->getTotals()['total'],2),
                'paid' => $this->order->isPaid()
            ],

            // Packages
            'packages' => $this->getPackagesData(),
        ];

        // Versions
        foreach ($this->getVersions() as $treeRow) {
            $data['versions'][] = $treeRow;
        }

        return $data;
    }

    /**
     * @return array
     */
    protected function getVersions()
    {
        if (is_null($this->versions)) {
            $this->versions = $this->getOrderTree($this->order->getId());
        }
        
        return $this->versions;
    }

    /**
     * Round values of an array
     * @param  array
     * @param  int
     * @return array
     */
    private function roundCurrency($array, $decimal)
    {
        foreach ($array as $key => $value) {
            $array[$key] = round($value, $decimal);
        }
        return $array;
    }
    
    protected function getPackagesData()
    {
        foreach ($this->order->getDeliveryOrderPackages() as $package) {
            
            $packageData = array();
            
            $packageData['package_id'] = $package->getPackageId();
            $packageData['code'] = $package->getPackageEsbNumber();
            $packageData['salestype'] = $package->getSaleType();
            $packageData['type'] = $package->getType();
            $packageData['status_description'] = $package->getVfStatusDesc();

            $packageData['creditcheck']['status'] = $this->getPackageCCHistory($package);

            $packageData['numberporting']['status'] = $this->getPackageNPHistory($package);
            $packageData['status'] = $this->getPackageStatusHistory($package);
                
            $packageData['ctn'] = is_null($package->getCtn()) ? $package->getTelNumber() : $package->getCtn();
            
            $showPackageInfo = $this->order->getParentId() || count($this->getVersions()) === 1 ? true : false;
            $packageData['ciboodle_id'] = $showPackageInfo ? $package->getCiboodleCases() : null;
            $packageData['reservation'] = $showPackageInfo ? $package->getStockStatus() : null;
            $packageData['sim_number'] = $showPackageInfo ? $package->getSimNumber() : null;
            $packageData['imei'] = $showPackageInfo ? $package->getImei() : null;
            $packageData['refund_reason'] = $showPackageInfo ? $package->getRefundReason() : null;

            // Products
            $packageData['products'] = array();
            foreach ($this->order->getAllItems() as $item) {
                if ($item->getPackageId() == $package->getPackageId()) {
                    $productData = array();
                    $productData['sku'] = $item->getSku();
                    $productData['name'] = $item->getProduct()->getName();
                    $type = array_values($item->getProduct()->getType());
                    $productData['type'] = $this->mapPackageType($type[0]);
                    $productData['prices']['monthly']['ex'] = Mage::helper('core')->currency($item->getMafRowTotal(), false, true);
                    $productData['prices']['monthly']['incl'] = Mage::helper('core')->currency($item->getMafRowTotalInclTax(), false, true);
                    $productData['prices']['monthly']['vat'] = Mage::helper('core')->currency($item->getMafTaxAmount(), false, true);
                    $productData['prices']['once']['ex'] = Mage::helper('core')->currency($item->getPrice(), false, true);
                    $productData['prices']['once']['incl'] = Mage::helper('core')->currency($item->getRowTotalInclTax(), false, true);
                    $productData['prices']['once']['vat'] = Mage::helper('core')->currency($item->getTaxAmount(), false, true);
                    $productData['prices']['monthly'] = $this->roundCurrency($productData['prices']['monthly'], 2);
                    $productData['prices']['once'] = $this->roundCurrency($productData['prices']['once'], 2);
                    $packageData['products'][] = $productData;

                    if ($item->getProduct()->isSubscription()) {
                        if ($package->getNetworkProvider()) {
                            $numberData['type'] = $item->getNumberPortingType() == 0 ? 'Consumer' : 'Business';
                            $numberData['connection'] = $item->getConnectionType() == 2 ? 'Prepaid' : 'Subscription';
                            $numberData['ctn'] = $package->getCurrentNumber();
                            $numberData['operator'] = $package->getNetworkOperator();
                            $numberData['provider'] = $package->getNetworkProvider();
                            $date = new DateTime($package->getContractEndDate());
                            $numberData['date'] = $date->format('Y-m-d');
                            if ($package->getCurrentNumber()) {
                                $numberData['current_sim'] = $package->getCurrentSimcardNumber();
                                $numberData['new_sim'] = $package->getSimNumber();
                            } else {
                                $numberData['current_sim'] = $package->getCurrentSimcardNumber();
                            }
                            $packageData['numberporting'] = array_merge($packageData['numberporting'], $numberData);
                        }
                    }
                }
            }
            $result[] = $packageData;
        }

        return $result;
    }
    
    protected function getPackageStatusHistory($package) 
    {
        return $this->getHistory('package_id', $package, Vznl_Superorder_Model_StatusHistory::PACKAGE_STATUS);
    }
    
    protected function getPackageCCHistory($package)
    {
        return $this->getHistory('package_id', $package, Vznl_Superorder_Model_StatusHistory::PACKAGE_CC_STATUS);
    }
    
    protected function getPackageNPHistory($entity)
    {
        return $this->getHistory('package_id', $entity, Vznl_Superorder_Model_StatusHistory::PACKAGE_PORTING_STATUS);
    }

    /**
     * @param string $type
     * @return string
     */
    private function mapPackageType(string $type) : string
    {
        return self::PACKAGE_TYPES_MAP[$type] ?? $type;
    }
}
