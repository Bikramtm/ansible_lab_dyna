<?php

class Vznl_RestApi_Orders_GetPackage extends Vznl_RestApi_Orders_Abstract
{
    protected $order;

    public function __construct($request)
    {
        if(is_array($request))
        {
            $this->order = Mage::getModel('sales/order')->load($request['order_id']);
        }
        else
        {
            $routeParts = explode('/', $request->getQuery('route'));
            $this->order = Mage::getModel('sales/order')->load($routeParts[4]);
        }
        
        if (!$this->order->getId())
        {
            throw new Exception('Order not found');
        }
    }

    protected function getPackagesData()
    {
        foreach ($this->order->getDeliveryOrderPackages() as $package) {
            
            $packageData = array();
            
            $packageData['salestype'] = $package->getSaleType();
            $packageData['type'] = $package->getType();

            foreach($this->getOrderTree($this->order->getId()) as $treeRow)
            { 
                $packageData['versions'][] = $treeRow;
            }

            $packageData['creditcheck']['date'] = $package->getCreditcheckStatusUpdated();
            $packageData['creditcheck']['status'] = $package->getCreditcheckStatus();
            $packageData['creditcheck']['code'] = $package->getPackageEsbNumber();

            $packageData['status'] = $package->getStatus();
            $packageData['reservation'] = $package->getStockStatus();
            $packageData['ciboodle_id'] = $package->getCiboodleCases();

            // Products
            $packageData['products'] = array();
            foreach($package->getPackageItems() as $item)
            {
                $productData = array();
                $productData['sku'] = $item->getSku();
                $productData['name'] = $item->getProduct()->getName();
                $productData['prices']['monthly']['ex'] = Mage::helper('core')->currency($item->getMafRowTotal(), false, true);
                $productData['prices']['monthly']['incl'] = Mage::helper('core')->currency($item->getMafRowTotalInclTax(), false, true);
                $productData['prices']['monthly']['vat'] = Mage::helper('core')->currency($item->getMafTaxAmount(), false, true);
                $productData['prices']['once']['ex'] = Mage::helper('core')->currency($item->getPrice(), false, true);
                $productData['prices']['once']['incl'] = Mage::helper('core')->currency($item->getRowTotalInclTax(), false, true);
                $productData['prices']['once']['vat'] = Mage::helper('core')->currency($item->getTaxAmount(), false, true);
                $packageData['products'][] = $productData;

                if ($item->getProduct()->isSubscription()) {
                    if ($item->getNetworkProvider()) {
                        $numberData['type'] = $item->getNumberPortingType() == 0 ? 'Consumer' : 'Business';
                        $numberData['connection'] = $item->getConnectionType() == 2 ? 'Prepaid' : 'Subscription';
                        $numberData['ctn'] = $item->getCurrentNumber();
                        $numberData['operator'] = $item->getNetworkOperator();
                        $numberData['provider'] = $item->getNetworkProvider();
                        $date = new DateTime($item->getContractEndDate());
                        $numberData['date'] = $date->format('Y-m-d');
                        $numberData['status'] = $package['porting_status'];
                        if ($item->getCurrentNumber())  {
                            $numberData['current_sim'] = $item->getCurrentSimcardNumber();
                            $numberData['new_sim'] = $item->getSimNumber();
                            $packageData['numberporting'] = $numberData;
                        } else {
                            $numberData['current_sim'] = $item->getCurrentSimcardNumber();
                            $packageData['numberrequest'] = $numberData;
                        }
                    }
                }
            }
            $result[] = $packageData;
        }
        return $result;
    }

    public function process()
    {
        $data = $this->getPackagesData();
        return $data;
    }
}
