<?php

class Vznl_RestApi_Orders_Packages_RegisterReturn extends Vznl_RestApi_Orders_Abstract
{
    /**
     * @var Vznl_Superorder_Model_Superorder
     */
    protected $superOrder;

    /** 
     * @var Vznl_Package_Model_Package
     */
    protected $package;

    /** 
     * @var array
     */
    public $packageData = [];

    /** 
     * @var array
     */
    public $response = [];

    /**
     * @param Mage_Core_Controller_Request_Http $request
     */
    public function __construct(Mage_Core_Controller_Request_Http $request)
    {
        /** @var Vznl_Checkout_Helper_RegisterReturn $registerReturnHelper */
        $registerReturnHelper = Mage::helper('vznl_checkout/registerReturn');

        $this->response['error'] = false;
        $this->response['body'] = [];

        // Get the route params
        $routeParts = explode('/', $request->getQuery('route'));
        $superOrderNumber = (int) $routeParts[2];
        $packageId = (int) $routeParts[4];

        // Get the superorder
        $this->superOrder = Mage::getModel('superorder/superorder')->load((string) $superOrderNumber, 'order_number');
        if (!$this->superOrder || !$this->superOrder->getId()) {
            $this->response['error'] = true;
            $this->response['code'] = 1;
            $this->response['body']['message'] = $registerReturnHelper->__('Sales order not found');
            return;
        }

        // Get the package
        $this->package = $this->getPackageWithId($this->superOrder, $packageId);
        if ($this->package == null || !$this->package->getId()) {
            $this->response['error'] = true;
            $this->response['code'] = 2;
            $this->response['body']['message'] = $registerReturnHelper->__('The sales order did not contain a package with the given package id');
            return;
        }

        // Get the post variables and put them in packageData
        $this->packageData = $this->getPackageData($request, $this->superOrder, $this->package);
    }

    /**
     * Process the HTTP request.
     * 
     * @return array
     */
    public function process()
    {
        if ($this->response['error'] == true) {
            return $this->response;
        }

        /** @var Vznl_Checkout_Helper_RegisterReturn $registerReturnHelper */
        $registerReturnHelper = Mage::helper('vznl_checkout/registerReturn');
        $superOrderId = $this->superOrder->getEntityId();
        $packageId = $this->package->getPackageId();
        $triggerCompleteBoolean = true;
        $temporaryQueueItems = [];
        $packageData = $this->packageData;

        try {
            $result = $registerReturnHelper->registerReturnForPackage($superOrderId, $packageId, $packageData, $triggerCompleteBoolean, $temporaryQueueItems);

            if ($result['error'] == false) {
                $this->response['error'] = false;
                $this->response['code'] = 6;
                $this->response['body']['message'] = $registerReturnHelper->__('The package return is successfully registered');
            } else {
                $this->response['error'] = true;
                $this->response['code'] = 7;
                $this->response['body']['message'] = $registerReturnHelper->__('The package return is not successfully registered');
            }
        } catch (Exception $e) {
            $this->response['error'] = true;
            $this->response['code'] = $e->getCode();
            $this->response['body']['message'] = $e->getMessage();
        }

        return $this->response;
    }

    private function getPackageWithId($superOrder, $packageId)
    {
        $packages = $this->superOrder->getPackages($superOrder->getEntityId());

        foreach ($packages as $package) {
            if ($package->getPackageId() == $packageId) {
                return $package;
            }
        }

        return null;
    }

    /**
     * Get the package data.
     * 
     * @param Mage_Core_Controller_Request_Http $request The request.
     * @param Vznl_Superorder_Model_Superorder $superOrder The super order.
     * @param Vznl_Package_Model_Package $package The package.
     */
    private function getPackageData(Mage_Core_Controller_Request_Http $request, $superOrder, $package)
    {
        $packageData = [
            'return_channel' => Vznl_Package_Model_Package::PACKAGE_RETURN_GRACEPERIOD,
            'complete_return' => true,
            'not_damaged_items' => true,
            'return_date' => date('d-m-Y'),
            'return_notes' => 'Return registered through the API',
            'agent_name' => 'SYSTEM',
            'items' => [],
        ];

        $items = $package->getPackageItems();
        /** @var Vznl_Checkout_Model_Sales_Order_Item $item */
        foreach ($items as $item) {
            $packageData['items'][] = $item->getId();
        }

        return $packageData;
    }
}