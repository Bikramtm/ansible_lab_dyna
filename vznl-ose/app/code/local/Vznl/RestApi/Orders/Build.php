<?php

class Vznl_RestApi_Orders_Build extends Vznl_RestApi_Orders_Create
{
    /**
     * @var Mage_Core_Controller_Request_Http
     */
    protected $request;

    /**
     * @var array|mixed|null
     */
    protected $postData;

    /**
     * Vznl_RestApi_Orders_Build constructor.
     * @param Mage_Core_Controller_Request_Http $request
     * @param array|null $postData
     */
    public function __construct($request, array $postData = null)
    {
        $this->request = $request;
        $this->postData = (null !== $postData) ? $postData : Zend_Json::decode(file_get_contents("php://input"));
    }

    /**
     * Get the current checkout session.
     *
     * @return Mage_Checkout_Model_Session
     */
    protected function _getCheckoutSession()
    {
        return Mage::getSingleton('checkout/session');
    }

    /**
     * @return Vznl_Checkout_Model_Type_Multishipping
     */
    protected function getCheckout()
    {
        return Mage::getSingleton('checkout/type_multishipping');
    }

    /**
     * Get current customer session.
     *
     * @return Dyna_Customer_Model_Session
     */
    protected function _getCustomerSession()
    {
        return Mage::getSingleton('customer/session');
    }

    /**
     * Get current cart,
     */
    protected function getCart()
    {
        return Mage::getSingleton('checkout/cart');
    }

    protected function getFFRequests(Mage_Sales_Model_Quote $quote, &$errors)
    {
        if (Mage::app()->getStore()->getCode() != Vznl_Agent_Model_Website::WEBSITE_FRIENDS_AND_FAMILY) {
            // Do nothing if the store is not Friends and Family
            return null;
        }

        $packages = Mage::getModel('package/package')->getPackages(null, $quote->getId());

        $requestModels = [];

        /** @var Vznl_Package_Model_Package $package */
        foreach ($packages as $package) {
            if (!$package->getCoupon()) {
                $errors[] = array(
                    "item_key" => "ff_code",
                    "message" => "The quote does not have a valid request."
                );
                return null;
            }

            /**
             * @var Vznl_FFHandler_Model_Request $requestModel
             */
            $requestModel = Mage::getModel("ffhandler/request");
            $requestModel->loadByCode($package->getCoupon());

            if (
                !$requestModel->getId() // Code does not exist
                || !$requestModel->canBeUsedByQuote($quote, $package->getId()) // Is allowed to be used by customer
            ) {
                $errors[] = array(
                    "item_key" => "ff_code",
                    "message" => "The quote has an invalid Friends and Family code"
                );
                return null;
            }


            $requestModels[$package->getPackageId()] = $requestModel;
        }

        return $requestModels;
    }

    /**
     * @param array $requestModels
     * @param Mage_Sales_Model_Quote $quote
     * @param Vznl_Superorder_Model_Superorder $superOrder
     */
    public function useFFRequests(
        array $requestModels,
        Mage_Sales_Model_Quote $quote,
        Vznl_Superorder_Model_Superorder $superOrder
    ) {
        if (Mage::app()->getStore()->getCode() != Vznl_Agent_Model_Website::WEBSITE_FRIENDS_AND_FAMILY) {
            // Do nothing if the store is not Friends and Family
            return;
        }

        /** @var Vznl_FFHandler_Helper_Request $requestHelper */
        $requestHelper = Mage::helper("ffhandler/request");

        $packageCollection = Mage::getModel('package/package')->getCollection()
            ->addFieldToFilter('order_id', $superOrder->getId());

        /** @var Vznl_Package_Model_Package $package */
        foreach ($packageCollection as $package) {
            $requestHelper->updateFFCodeUsageForPackage($package, $quote);
        }

        /** @var Vznl_FFHandler_Model_Request $requestModel */
        foreach ($requestModels as $requestModel) {
            /**
             * @var Vznl_FFReport_Helper_Email $emailHelper
             */
            $emailHelper = Mage::helper("vznl_ffreport/email");

            $emailHelper->ffCodeUsed($superOrder, $requestModel);
        }
    }

    /**
     * Process the HTTP request.
     *
     * @return array
     */
    public function process()
    {
        // Store the current posted data in the quote
        $this->_getCheckoutSession()->resetCheckout();
        $this->_getCheckoutSession()->clear();
        $this->_getCheckoutSession()->clearHelperData();

        $this->saveQuote = new Vznl_RestApi_Quotes_Save($this->request);
        $this->saveQuote->clearCache();
        $response = $this->saveQuote->process();
        $this->fetchCustomerFromRetrieveCustomerInfo();

        $this->_getCheckoutSession()->replaceQuote(Mage::getModel('sales/quote')->load($response['dynalean_quote_id']));

        if (count($response['errors'])) {
            $this->logBuildError($response['errors']);
            return $response;
        }

        $quote = $this->_getCheckoutSession()->getQuote();

        if (is_null($quote->getId())) {
            $this->logBuildError('Quote not found');
            $response['errors'][] = array('item_key'=>'request', 'message'=>'Quote not found');

            return $response;
        }

        $ffrequests = $this->getFFRequests($quote, $response["errors"]);

        if (count($response['errors'])) {
            $this->logBuildError($response['errors']);
            return $response;
        }

        if (count($response['products']) === 0) {
            $response['errors'][] = array('item_key'=>'products', 'message'=>'There are no products found in this cart');

            return $response;
        }

        try {
            $order = Mage::getModel('sales/order')->load($quote->getId(), 'quote_id');
            if (!$order->getId()) {
                $this->initAgent($quote->getAgentId(), $quote->getDealerId());
                $bslJSON['eligibleComponents'] = $this->postData['inlifesales'];
                $data = $this->create($quote, $bslJSON);
                $order = Mage::getModel('sales/order')->load($quote->getId(), 'quote_id');
                $order->setData('increment_id', $this->postData['adyen_payment_id'])->save();
                $superOrder = Mage::getResourceModel('superorder/superorder_collection')
                    ->addFieldToFilter('order_number', $data['orders'])
                    ->setPageSize(1, 1)
                    ->getLastItem();

                if ($superOrder->getId()) {
                    $superOrder->setData('order_number', $this->postData['dynalean_order_id'])->save();
                    if ($ffrequests) {
                        $this->useFFRequests($ffrequests, $quote, $superOrder);
                    }
                    // Store ILT
                    if ($order->isIltRequired() && isset($this->postData['ilt'])) {
                        $iltHelper = Mage::helper('ilt');
                        $iltHelper->storeIltData($superOrder, $this->postData['ilt']);

                        // Trigger ILT
                        $iltRepository = new Vznl_Ilt_Repository_SuperorderIlt();
                        $iltModel = $iltRepository->get($superOrder->getOrderNumber());
                        if ($iltModel && !$this->storeIltBackendData($superOrder)) {
                            $response['errors'][] = array('item_key'=>'request', 'message'=>'Unable to persist ILT data to backend');
                        }
                    }
                    $data['orders'] = $this->postData['order_number']; //swap order_number with the registered one
                    if (count($this->getErrors()) === 0 && count($response['errors']) === 0) {

                        // API call to update order id
                        $searchHelper = Mage::helper('vznl_customer/search');
                        $searchHelper->updateOrderNumberApiCall($superOrder, $superOrder->getCustomer());

                        return array('success' => true);
                    } else {
                        $this->logBuildError($this->getErrors());
                        foreach ($this->getErrors() as $key => $error) {
                            $response['errors'] = $this->getErrors();
                        }
                    }
                } else {
                    Mage::throwException(Mage::helper('checkout')->__('SaleOrder could not be found.'));
                }
            } else {
                $this->logBuildError('Quote is already an order');
                $response['errors'][] = array('item_key'=>'request', 'message'=>'Quote is already an order');
            }
        } catch (Exception $e) {
            $this->logBuildError($e->getMessage());

            foreach ($this->getErrors() as $key => $error) {
                $response['errors'] = $this->getErrors();
            }
            $response['errors'][] = array('item_key'=>'request', 'message'=>$e->getMessage());
        }

        return $response;
    }

    /**
     * @param Vznl_Superorder_Model_Superorder $superOrder
     * @return bool
     */
    protected function storeIltBackendData(Vznl_Superorder_Model_Superorder $superOrder)
    {
        try {
            $iltRepository = new Vznl_Ilt_Repository_SuperorderIlt();
            $loggerWriter = new Aleron75_Magemonolog_Model_Logwriter('services/Ilt/' . \Ramsey\Uuid\Uuid::uuid4() . '.log');
            if (Mage::getStoreConfig('vodafone_service/ilt/use_stubs')) {
                $iltAdapter = Omnius\InkomensLastenToets\ExternalAdapter\Stub\Factory::create($iltRepository, $loggerWriter->getLogger());
            } else {
                $agent = $this->_getCustomerSession()->getAgent();
                $iltAdapter = Omnius\InkomensLastenToets\ExternalAdapter\Bsl\Factory::create(Mage::getStoreConfig('vodafone_service/ilt/wsdl'), $iltRepository, $loggerWriter->getLogger(), [
                    'username' => Mage::getStoreConfig('vodafone_service/ilt/bsl_username'),
                    'password' => Mage::getStoreConfig('vodafone_service/ilt/bsl_password'),
                    'agent' => $agent->getUsername(),
                    'dealercode' => $agent->getDealerCode(),
                    'service_endpoint' => Mage::getStoreConfig('vodafone_service/ilt/usage_url'),
                    'client_options' => Mage::helper('vznl_core/service')->getZendSoapClientOptions('ilt') ? : []
                ]);
            }
            $data = $iltRepository->get($superOrder->getOrderNumber());
            if (!$iltAdapter->send($data)) {
                return false;
            }
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    protected function logBuildError($message)
    {
        $this->getQuote()->setSuperorderNumber($this->postData['dynalean_order_id']);
        $this->getQuote()->setHawaiiOrderException(json_encode($message))->save();
        $this->getQuote()->setSaveAsCart(true)->save();
        Mage::log('OrderNumber: '.$this->postData['dynalean_order_id'], null, RestApi::LOG_FAILED_ORDERS);
        Mage::log($message, null, RestApi::LOG_FAILED_ORDERS);
    }

    /**
     * Loads customer details from RetrieveCustomerInfo
     * @return bool
     */
    protected function fetchCustomerFromRetrieveCustomerInfo()
    {
        $customerId = $this->saveQuote->getCart()->getQuote()->getCustomerId();
        $customer = Mage::getModel('vznl_customer/customer')->load($customerId);
        $isBusinessWithMissingCompanyName = $this->checkoutIsBusiness() && empty($customer->getCompanyName());
        $isCustomerWithMissingDetails = empty($customer->getFirstName()) || empty($customer->getLastName());

        if (!empty($customer->getBan()) && ($isBusinessWithMissingCompanyName || $isCustomerWithMissingDetails)) {
            try {
                // Retrieve customer profile
                $adapterFactoryName = Mage::getStoreConfig('omnius_service/customer_search_implementation_configuration/customer_search_adapter');
                $adapter = $adapterFactoryName::create();

                $customerData = $adapter->getProfile($customer);

                // When there is no customer data from OIL, we fail
                if ($customerData instanceof Exception) {
                    Mage::logException($customerData);
                    Mage::log('OrderNumber: ' . $this->postData['dynalean_order_id'], null, RestApi::LOG_FAILED_ORDERS);
                    Mage::log($customerData->getMessage(), null, RestApi::LOG_FAILED_ORDERS);
                    return false;
                }

                // Update the local DB with the latest data for the customer from the service call
                $customer->updateCustomerInfo($customer->getId(), is_object($customerData) ? $customerData->getData() : $customerData);

                return true;
            } catch (Exception $e) {
                Mage::logException($e);
                Mage::log('OrderNumber: '.$this->postData['dynalean_order_id'], null, RestApi::LOG_FAILED_ORDERS);
                Mage::log($e->getMessage(), null, RestApi::LOG_FAILED_ORDERS);
                return false;
            }
        }
        return true;
    }

    /**
     * Loads customer CTNs from RetrieveCustomerInfo
     * @return bool
     */
    protected function fetchCustomerCtnsFromRetrieveCustomerInfo()
    {
        $customerId = $this->saveQuote->getCart()->getQuote()->getCustomerId();
        $customer = Mage::getModel('vznl_customer/customer')->load($customerId);
        $isBusinessWithMissingCompanyName = $this->checkoutIsBusiness() && empty($customer->getCompanyName());
        $isCustomerWithMissingDetails = empty($customer->getFirstName()) && empty($customer->getLastName());

        if (!empty($customer->getBan()) && ($isBusinessWithMissingCompanyName || $isCustomerWithMissingDetails)) {
            try {
                // Retrieve customer profile
                $adapterFactoryName = Mage::getStoreConfig('omnius_service/customer_search_implementation_configuration/customer_search_adapter');
                $adapter = $adapterFactoryName::create();

                $customerData = $adapter->getProfile($customer);

                Mage::log($adapterFactoryName . ' called for OrderNumber: ' . $this->postData['dynalean_order_id'], ', internal customer ID: ' . $customerId, null, RestApi::LOG_RETRIEVE_CUSTOMER_CALLED_ORDERS);

                // When there is no customer data from OIL, we fail
                if ($customerData instanceof Exception) {
                    Mage::logException($customerData);
                    Mage::log('RetrieveCustomer (mode 1) API error for OrderNumber: ' . $this->postData['dynalean_order_id'], null, RestApi::LOG_FAILED_ORDERS);
                    Mage::log($customerData->getMessage(), null, RestApi::LOG_FAILED_ORDERS);
                    return false;
                }

                // Update the local DB with the latest data for the customer from the service call
                $customer->updateCustomerInfo($customer->getId(), $customerData);

                return true;
            } catch (Exception $e) {
                Mage::logException($e);
                Mage::log('Fetch (mode 1) failed for OrderNumber: '.$this->postData['dynalean_order_id'], null, RestApi::LOG_FAILED_ORDERS);
                Mage::log($e->getMessage(), null, RestApi::LOG_FAILED_ORDERS);
                return false;
            }
        }

        $salesType = $this->saveQuote->getSalesType();

        if (in_array($salesType, [Vznl_Checkout_Model_Sales_Quote_Item::RETENTION, Vznl_Checkout_Model_Sales_Quote_Item::INLIFE])) {
            try {
                // Retrieve customer installed base
                $adapterFactoryName = Mage::getStoreConfig('omnius_service/customer_search_implementation_configuration/customer_search_adapter');
                $adapter = $adapterFactoryName::create();

                $customerData = $adapter->getInstalledBase($customer);

                // When there is no customer data from OIL, we fail
                if ($customerData instanceof Exception) {
                    Mage::logException($customerData);
                    Mage::log('RetrieveCustomer (mode 2) API error for OrderNumber: ' . $this->postData['dynalean_order_id'], null, RestApi::LOG_FAILED_ORDERS);
                    Mage::log($customerData->getMessage(), null, RestApi::LOG_FAILED_ORDERS);
                    return false;
                }

                // Store customer contract id
                Mage::helper('vznl_customer/panel')->saveCustomerCtns($customer);

                return true;
            } catch (Exception $e) {
                Mage::logException($e);
                Mage::log('Fetch (mode 2) failed for OrderNumber: '.$this->postData['dynalean_order_id'], null, RestApi::LOG_FAILED_ORDERS);
                Mage::log($e->getMessage(), null, RestApi::LOG_FAILED_ORDERS);
                return false;
            }
        }

        return true;
    }

    protected function output($orderNumber)
    {
        $getClass = new Vznl_RestApi_Orders_GetHawaii(array('order_number' => $orderNumber));
        return $getClass->process();
    }
}
