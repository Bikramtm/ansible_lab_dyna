<?php

class Vznl_RestApi_Orders_Search extends Vznl_RestApi_Orders_Abstract
{
    /**
     * @var array
     */
    protected $acceptedFields = array(
        'customer_ctn',
        'postcode',
        'street',
        'customer_ban',
        'customer_firstname',
        'customer_middlename',
        'customer_lastname',
        'customer_email',
        'customer_dob',
        'order_number',
        'created_at_from',
        'created_at_till',
        'company',
        'company_coc',
        'invoicenr',
        'code',
        'sim_number',
        'ciboodle_id',
    );

    /**
     * @param Mage_Core_Controller_Request_Http $request
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Process the HTTP request.
     * 
     * @return array
     */
    public function process()
    {
        $result = array();
        $requestParameters = $this->request->getParams();
        
        // Remove empty and not accepted parameters
        foreach ($requestParameters as $key => $value) {
            if (!in_array($key, $this->acceptedFields) || !trim($value)) {
                unset($requestParameters[$key]);
            }
        }

        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');

        $where = array();
        $package = false;

        foreach ($requestParameters as $key => $value) {
            switch ($key) {
                case 'postcode':
                    $where[]      = "sa.".$key." = ".$readConnection->quote(strtoupper($value))."";
                    break;
                case 'street':
                    $where[]      = "lower(sa.".$key.") like lower(".$readConnection->quote($value.'%').")";
                    break;
                case 'created_at_from':
                    $where[]      = "s.created_at >= ".$readConnection->quote($value)."";
                    break;
                case 'created_at_till':
                    $where[]      = "s.created_at <= ".$readConnection->quote($value)."";
                    break;
                case 'company':
                    $where[]      = "lower(o.company_name) like lower(".$readConnection->quote($value.'%').")";
                    break;
                case 'customer_ban':
                    $where[]      = "c.ban like ".$readConnection->quote($value.'%')."";
                    break;
                case 'customer_ctn':
                    $value        = Mage::helper('vznl_customer')->parseCtn($value);
                    $where[]      = "ctn.ctn like ".$readConnection->quote($value.'%')."";
                    $ctnJoinQuery = ' left join customer_ctn ctn on o.customer_id = ctn.customer_id ';
                    break;
                case 'invoicenr':
                    $where[]      = "o.increment_id = ".$readConnection->quote($value);
                    break;
                case 'code':
                    $where[]      = "cp.package_esb_number = ".$readConnection->quote($value);
                    $package      = true;
                    break;
                case 'sim_number':
                    $where[]      = "cp.sim_number = ".$readConnection->quote($value);
                    $package      = true;
                    break;
                case 'customer_dob':
                    $where[]      = "o.customer_dob = ".$readConnection->quote($value);
                    break;
                case 'customer_email':
                    $where[]      = "o.additional_email like ".$readConnection->quote('%'.$value.'%');
                    break;
                case 'ciboodle_id':
                    $where[]      = "cp.ciboodle_cases = ".$readConnection->quote($value);
                    $package      = true;
                    break;
                default:
                    $where[]      = $key." like ".$readConnection->quote($value.'%');
                    break;
            }
        }
        
        $whereString = '';
        if (count($where) > 0) {
            $whereString .= " where ".implode(' and ', $where)." ";
            $query = "select s.order_number,
                s.order_status,
                s.created_at,
                cs.name as channelname,
                c.ban,
                coalesce((   select value 
                    from customer_entity_varchar cev 
                    join eav_attribute ea on ea.attribute_id = cev.attribute_id 
                    where cev.entity_id = c.entity_id 
                    and ea.attribute_code = 'customer_label'
                ), 'Gemini') as origin,
                '' as customer_ctn, 
                sa.postcode,
                sa.street,
                sa.city,
                sa.delivery_type,
                d.vf_dealer_code,
                o.customer_firstname,
                o.customer_middlename,
                o.customer_lastname,
                o.additional_email,
                o.customer_dob,
                o.company_name,
                o.company_coc
            from superorder s
            join sales_flat_order o on s.entity_id = o.superorder_id
            JOIN sales_flat_order_address oa ON o.entity_id = oa .parent_id AND oa.address_type = 'billing'
            join core_website cs on s.website_id = cs.website_id
            JOIN sales_flat_order_address sa ON o.entity_id= sa.parent_id AND sa.address_type='shipping'
            left join dealer d on s.dealer_id = d.dealer_id
            left join customer_entity c on o.customer_id = c.entity_id"
                .(isset($ctnJoinQuery) ? $ctnJoinQuery : '')
                .($package ? " left join catalog_package cp on cp.order_id = s.entity_id "
                        : "")
                .$whereString
                ."group by s.order_number
            order by s.created_at desc
            limit 200";

            $superOrderCollection = $readConnection->fetchAll($query);
            foreach ($superOrderCollection as $superOrder) {
                $data['order_number'] = $superOrder['order_number'];
                $data['order_status'] = $superOrder['order_status'];
                $data['created_at']   = $superOrder['created_at'];
                $data['channel']      = $superOrder['channelname'];
                $data['dealer_code']  = $superOrder['vf_dealer_code'];
                $data['order_type']   = $superOrder['delivery_type'] === 'store'
                        ? 'Shop delivery' : 'Home delivery';

                $data['customer_ban']        = $superOrder['ban'];
                $data['origin']              = $superOrder['origin'];
                $data['customer_ctn']        = $superOrder['customer_ctn'];
                $data['customer_firstname']  = $superOrder['customer_firstname'];
                $data['customer_middlename'] = $superOrder['customer_middlename'];
                $data['customer_lastname']   = $superOrder['customer_lastname'];
                $data['customer_email']      = trim(explode(';',
                        $superOrder['additional_email'])[0]);
                $date                        = new DateTime($superOrder['customer_dob']);
                $data['customer_dob']        = $date->format('Y-m-d');

                if ($superOrder['customer_is_business']) {
                    $data['postcode'] = $superOrder['company_postcode'];
                    $data['street']   = $superOrder['company_street'];
                    $data['city']     = $superOrder['company_city'];
                } else {
                    $data['postcode'] = $superOrder['postcode'];
                    $data['street']   = $superOrder['street'];
                    $data['city']     = $superOrder['city'];
                }
                $data['company']     = $superOrder['company_name'];
                $data['company_coc'] = $superOrder['company_coc'];
                $result[]            = $data;
            }
        }

        //serialize result or show corresponding message
        return array_values($result);
    }
}