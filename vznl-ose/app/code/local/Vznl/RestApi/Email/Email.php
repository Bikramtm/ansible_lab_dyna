<?php

class Vznl_RestApi_Email_Email
{
    /**
     * @var Mage_Core_Controller_Request_Http
     */
    private $request;

    /**
     * @var array
     */
    private $postData;

    /**
     * @var array
     */
    private $payload;

    /**
     * @var string
     */
    private $cultureCode;

    /**
     * @param Mage_Core_Controller_Request_Http $request
     */
    public function __construct($request)
    {
        $this->request = $request;
        $this->postData = Zend_Json::decode(file_get_contents("php://input"));
    }

    /**
     * Process the HTTP request.
     * 
     * @return array
     */
    public function process()
    {
        $response = array();

        try {
            if ($this->validatePostData() !== true) {
                return $this->validatePostData();
            }
            
            foreach ($this->postData['parameters'] as $key => $value) {
                if(!empty($value['SSFE_SaleOrderNumber'])) {
                    $orderNumber = $value['SSFE_SaleOrderNumber'];
                    break;
                }
            }

            $fromMail = Mage::getModel('superorder/superorder')->getCollection()
                ->addFieldToFilter('order_number', $orderNumber)
                ->getLastItem()->getDealer()->getSenderDomain();
            $this->payload = array(
                'email_code' => $this->postData['email_code'],
                'receiver' => $this->postData['receiver'],
                'cc' => $this->postData['cc'] ?? null,
                'bcc' => $this->postData['bcc'] ?? null,
                'created_at' => date('Y-m-d H:m:i'),
                'parameters' => $this->postData['parameters'],
                'attachments' => $this->postData['attachments'],
                'culture_code' => $this->cultureCode,
                'from' => $fromMail
            );

            if ($this->validatePayload() !== true) {
                return $this->validatePayload();
            }

            Mage::getSingleton('communication/pool')->add($this->payload);

            $response['success'] = true;
        } catch (Exception $e) {
            $response['success'] = false;
            $response['errors'][] = array('item_key' => 'request', 'message' => $e->getMesage());
        }

        return $response;
    }

    /**
     * @return bool
     * @throws Exception
     */
    private function validatePostData()
    {
        // set default culture_code if not set
        $this->cultureCode = str_replace('_', '-', Mage::app()->getLocale()->getLocaleCode());
        if (!empty($this->postData['culture_code'])) {
            $this->cultureCode = $this->postData['culture_code'];
        }

        try {
            // validate email_code
            if (empty($this->postData['email_code'])) {
                throw new Exception('The email code is required.', 101);
            }

            // validate receiver
            if (
                empty($this->postData['receiver']) ||
                !filter_var($this->postData['receiver'], FILTER_VALIDATE_EMAIL)
            ) {
                throw new Exception('The receiver field is required. It has to be an email.', 102);
            }
        } catch (Exception $e) {
            $response['success'] = false;
            $response['errors'][] = array('code' => $e->getCode(), 'message' => $e->getMessage());

            return $response;
        }

        return true;
    }

    /**
     * @return bool
     * @throws Exception
     */
    private function validatePayload()
    {
        try {
            if (empty($this->payload)) {
                throw new Exception('The payload is not set.', 103);
            }
        } catch (Exception $e) {
            $response['success'] = false;
            $response['errors'][] = array('code' => $e->getCode(), 'message' => $e->getMessage());

            return $response;
        }

        return true;
    }
}
