<?php

class Vznl_RestApi_Quotes_Save extends Vznl_RestApi_Quotes_Abstract
{
    /**
     * @var array
     */
    protected $postData;
    protected $cart;

    /**
     * @var array
     */
    protected $allowedDealerStoreIds = [1, 2];

    /**
     * @var array
     */
    public $supportedPaymentMethods  = [
        'adyen_hpp',
        'cashondelivery',
        'checkmo',
        'free'
    ];

    /**
     * @var array
     */
    private $_productTypes = [
        'device_sku',
        'subscription_sku',
        'sim_sku',
        'devicerc_sku',
        'extras'
    ];
    protected $cacheHashes;

    const PORTING_INTERNAL_PROVIDER = 'LBPT';
    const PRODUCT_TYPE_EXTRAS       = 'extras';

    /**
     * @param Mage_Core_Controller_Request_Http $request
     * @throws Exception
     * @throws Zend_Json_Exception
     */
    public function __construct(Mage_Core_Controller_Request_Http $request)
    {
        $this->request        = $request;
        $this->postData       = Zend_Json::decode(Mage::helper("vznl_data/data")->getFileContents("php://input"));
        $this->orderSalesType = $this->getOrderSalestype();
        $this->setQuote($this->postData['dynalean_quote_id']);
    }

    /**
     * Process the HTTP request.
     *
     * @return string
     */
    public function process()
    {
        // Logout the customer if any customer is set on the session
        Mage::getSingleton('customer/session')->unsetCustomer();
        Mage::getSingleton('customer/session')->clear();
        // Save the cart quote and set it to the checkout session also
        $quote                                                              = $this->getCart()->getQuote();
        $quote->setStoreId(Mage::app()->getStore()->getId());
        $quote->setRemoteIp(Mage::helper('core/http')->getRemoteAddr());
        $this->cacheHashes                                                  = unserialize($quote->getHawaiiCacheHashes());
        $quote->setTotalsCollectedFlag(true)->save();
        $this->getCheckoutSession()->replaceQuote($quote);
        $this->getCart()->setQuote($quote);
        $this->getCart()->getQuote()->setCorrespondenceEmail(
            $this->checkoutIsBusiness() ? $this->postData['business_details']['contracting_party']['correspondence_email']
                    : $this->postData['contracting_party']['correspondence_email']);
        // Strip strange data
        $this->cleanData();

        // Dealer
        try {
            $this->setDealer();
        } catch (Exception $e) {
            $this->setupErrors($e);
        }

        // Products
        try {
            $this->setProducts();
        } catch (Exception $e) {
            $this->setupErrors($e);
        }

        // Friends and family specific
        $this->setFfValues();

        // Coupon
        try {
            $this->setCoupons();
        } catch (Exception $e) {
            $this->setupErrors($e);
        }

        // Promotions
        try {
            $this->setPromotions();
        } catch (Exception $e) {
            $this->setupErrors($e);
        }

        $this->getCart()->getQuote()
            ->setTotalsCollectedFlag(false)
            ->collectTotals();
        // Set customer
        try {
            foreach ($quote->getAddressesCollection() as $address) {
                if (!$address->getFirstname()) {
                    $quote->removeAddress($address->getId());
                }
            }
            $customer = $this->getCustomerEntity();
            $this->fixCustomerAddresses($customer);
            $this->setCustomer($quote, $customer);
        } catch (Exception $e) {
            $this->setupErrors($e);
        }

        // Number Porting
        try {
            $this->setNumberPorting();
        } catch (Exception $e) {
            $this->setupErrors($e);
        }

        // Delivery
        try {
            $this->setDeliveryOptions();
        } catch (Exception $e) {
            $this->setupErrors($e);
        }

        // ILT
        try {
            $this->setIltData();
        } catch (Exception $e) {
            $this->setupErrors($e);
        }

        // Save customer details to vNext search
        Mage::helper('vznl_customer/search')->createApiCustomer($quote->getCustomer(), $quote->getCustomer()->getDefaultBillingAddress(), $quote, 'SaveExistingFields');

        $this->getCart()->getQuote()->setHawaiiCacheHashes(serialize($this->cacheHashes));
        $this->getCart()->getQuote()->setHawaiiShoppingOrderId($this->postData['shopping_order_id']);
        $this->getCart()->getQuote()->setHawaiiVersionId($this->postData['version_id']);
        $this->getCart()->getQuote()->setSuperorderNumber($this->postData['dynalean_order_id']);
        $this->getCart()->getQuote()->setHawaiiQuoteErrors((count($this->getErrors())
            == 0 ? null : serialize($this->getErrors())));

        Mage::unregister('quote_packages_'.$this->getCart()->getQuote()->getId().'_'.md5($this->getCart()->getQuote()->getUpdatedAt()));
        $this->getCart()->getQuote()->setTotalsCollectedFlag(true)->save();
        Mage::unregister('quote_packages_'.$this->getCart()->getQuote()->getId().'_'.md5($this->getCart()->getQuote()->getUpdatedAt()));

        return $this->output();
    }

    /**
     * Clean data
     */
    protected function cleanData()
    {
        $this->postData['payment_details']['account_nr']                    = str_replace(' ',
            '', $this->postData['payment_details']['account_nr']);
        $this->postData['contracting_party']['phone_1']                     = str_replace(' ',
            '', $this->postData['contracting_party']['phone_1']);
        $this->postData['contracting_party']['phone_1']                     = str_replace('+',
            '', $this->postData['contracting_party']['phone_1']);
        $this->postData['contracting_party']['phone_2']                     = str_replace(' ',
            '', $this->postData['contracting_party']['phone_2']);
        $this->postData['contracting_party']['phone_2']                     = str_replace('+',
            '', $this->postData['contracting_party']['phone_2']);
        $this->postData['business_details']['contracting_party']['phone_1'] = str_replace(' ',
            '',
            $this->postData['business_details']['contracting_party']['phone_1']);
        $this->postData['business_details']['contracting_party']['phone_1'] = str_replace('+',
            '',
            $this->postData['business_details']['contracting_party']['phone_1']);
        $this->postData['business_details']['contracting_party']['phone_2'] = str_replace(' ',
            '',
            $this->postData['business_details']['contracting_party']['phone_2']);
        $this->postData['business_details']['contracting_party']['phone_2'] = str_replace('+',
            '',
            $this->postData['business_details']['contracting_party']['phone_2']);
    }

    /**
     * Get the current cart.
     *
     * @return Vznl_Checkout_Model_Cart
     */
    public function getCart()
    {
        return Mage::getSingleton('checkout/cart');
    }

    /**
     * Get the current checkout session.
     *
     * @return Mage_Checkout_Model_Session
     */
    protected function getCheckoutSession()
    {
        return Mage::getSingleton('checkout/session');
    }

    /**
     * Clear the hawaii quote cache from the cart.
     *
     * @return mixed
     */
    public function clearCache()
    {
        return $this->getCart()->getQuote()->setHawaiiCacheHashes(null);
    }

    /**
     * Get the order package type.
     *
     * @return string
     */
    protected function getPackageType($data)
    {
        $packageType = 'hardware';

        if ($data['category'] === 'prepaid') {
            return 'prepaid';
        } if (!empty($data['subscription_sku'])) {
            return 'mobile';
        }
        return $packageType;
    }

    /**
     * Get package creation type ID by package type
     *
     * @param string $packageType
     *
     * @return int
     */
    protected function getPackageCreationTypeId(string $packageType): ?int
    {
        $catalogModel = Mage::getModel('catalog/type');
        $packageTypeCodeToLower = strtolower($packageType);
        $creationTypesMapping = $catalogModel->getCreationTypesMapping();

        if (empty($creationTypesMapping[$packageTypeCodeToLower])) {
            return null;
        }

        $creationType = ucfirst($creationTypesMapping[$packageTypeCodeToLower]);
        return Mage::helper('dyna_configurator/cart')->getCreationTypeId($creationType);
    }

    /**
     * Check if there is hardware only available.
     *
     * @return boolean
     */
    protected function hasHardwareOnlyPackage()
    {
        foreach ($this->postData['products'] as $product) {
            if ($product['category'] === 'prepaid') {
                return false;
            } if (!empty($product['subscription_sku'])) {
                return false;
            }
        }

        if ($this->getPaymentMethod() !== 'cashondelivery') {
            return false;
        }
        return true;
    }

    /**
     * Get the type of the sales order
     * Please note that only 1 time can be used for each order
     * @return string
     */
    protected function getOrderSalestype()
    {
        $salestype = null;

        foreach ($this->postData['products'] as $index => $product) {
            if ($salestype === null || $product['salestype'] === $salestype) {
                $salestype = $product['salestype'];
            } else {
                $this->addError('products['.$index.'].salestype',
                    'Not all salestypes are equal in this order');
                return null;
            }
        }
        return $salestype;
    }

    /**
     * Set te quote
     *
     * @param integer $quoteId
     * @throws Exception
     * @return \RestApi_Quotes_Save
     */
    public function setQuote($quoteId)
    {
        $order = Mage::getModel('sales/order')->load($quoteId, 'quote_id');

        if ($order->getId()) {
            throw new Exception('Not possible to save this shopping cart since it already is used to create an order');
        }

        $quote = Mage::getModel('sales/quote')->load($quoteId);
        $this->getCart()->setQuote($quote);
        return $this;
    }

    /**
     * Get the order payment method
     * @example fallback to Adyen_hpp (default)
     * @return string
     */
    protected function getPaymentMethod()
    {
        if ($this->getOrderSalestype() === 'inlife' && $this->postData['payment_method']
            !== 'checkmo') {
            $this->addError('payment_method',
                'Payment method "'.$this->postData['payment_method'].'" not supported for ILS. Suggest using "checkmo"');
            return null;
        }
        if (in_array($this->postData['payment_method'],
                $this->supportedPaymentMethods)) {
            return $this->postData['payment_method'];
        }
        $this->addError('payment_method',
            'Payment method "'.$this->postData['payment_method'].'" not supported. Possible values: "'.implode(', ',
                $this->supportedPaymentMethods).'".');
        return null;
    }

    /**
     * Get customer json properties for caching
     * @return array
     */
    protected function getCustomerDeliveryJsonProperties()
    {
        $properties[] = $this->postData['business_details'];
        $properties[] = $this->postData['chosen_order_type_is_business'];
        $properties[] = $this->postData['is_business'];
        $properties[] = $this->postData['contracting_party'];
        $properties[] = $this->postData['identity'];
        $properties[] = $this->postData['privacy'];
        $properties[] = $this->postData['payment_details'];
        $properties[] = $this->postData['delivery_options'];
        $properties[] = $this->postData['payment_method'];
        return $properties;
    }

    /**
     * Get the coupon json properties
     * @param array $data
     * @return array
     */
    protected function getCouponJsonProperties($data)
    {
        $properties[] = $data['voucher'];
        return $properties;
    }

    /**
     * Get the product package json properties
     * @param array $packageData
     * @return array
     */
    protected function getProductPackageJsonProperties($packageData)
    {
        $packageProperties[] = $packageData['device_sku'];
        $packageProperties[] = $packageData['subscription_sku'];
        $packageProperties[] = $packageData['sim_sku'];
        $packageProperties[] = $packageData['salestype'];
        $packageProperties[] = $packageData['package_id'];
        if (!empty($packageData['extras']['accessories'])) {
            foreach ($packageData['extras']['accessories'] as $accessory) {
                $packageProperties[] = $accessory['sku'];
            }
        }
        if (!empty($packageData['extras']['addons'])) {
            foreach ($packageData['extras']['addons'] as $addon) {
                $packageProperties[] = $addon['sku'];
            }
        }
        return $packageProperties;
    }

    /**
     * Store the delivery options
     */
    protected function setDeliveryOptions()
    {
        if ($this->jsonHasChanges('delivery',  $this->getCustomerDeliveryJsonProperties())) {
            $hasErrors                             = false;
            $data['deliver']                       = array();
            $data['payment']                       = $this->getPaymentMethod();
            $data['deliver']['address']['country'] = 'NL';

            switch ($this->postData['delivery_options']['selected_delivery_option']) {
                case 'pick_up_in_shop':
                    $dealer = Mage::getModel('agent/dealer')->load($this->postData['delivery_options']['pick_up_in_shop']['dealer_code'],
                        'vf_dealer_code');
                    if ($dealer->getId() && in_array($dealer->getStoreId(),
                            $this->allowedDealerStoreIds)) {
                        $data['deliver']['address'] = array(
                            'street' => array($dealer->getStreet(), $dealer->getHouseNr()),
                            'postcode' => $dealer->getPostcode(),
                            'store_id' => $dealer->getStoreId(),
                            'city' => $dealer->getCity(),
                            'address' => 'store',
                        );
                    } else {
                        $hasErrors = true;
                        $this->addError('delivery_options.pick_up_in_shop.dealer_code',
                            'Dealer not found');
                    }
                    break;
                case 'home_delivery':
                    $data['deliver']['address'] = array(
                        'street' => array(
                            $this->postData['delivery_options']['home_delivery']['address']['street'],
                            $this->postData['delivery_options']['home_delivery']['address']['house_number'],
                            $this->postData['delivery_options']['home_delivery']['address']['house_number_addition'],
                        ),
                        'postcode' => $this->postData['delivery_options']['home_delivery']['address']['postal_code'],
                        'city' => $this->postData['delivery_options']['home_delivery']['address']['city'],
                        'address' => 'other_address',
                    );
                    if ($this->postData['delivery_options']['home_delivery']['address_same_as_invoice']) {
                        $billingAddress = $this->getCart()->getQuote()->getBillingAddress();
                        if ($billingAddress->getId() && $billingAddress->getCountryId()
                            !== 'NL') {
                            $hasErrors                  = true;
                            $this->addError('delivery_options.home_delivery.address.country',
                                'Not possible to make home deliveries outside NL');
                            $data['deliver']['address'] = array();
                        } else {
                            $data['deliver']['address'] = array(
                                'street' => array($billingAddress->getStreet1(),
                                    $billingAddress->getStreet2(),
                                    $billingAddress->getStreet3()
                                ),
                                'postcode' => $billingAddress->getPostcode(),
                                'city' => $billingAddress->getCity(),
                                'address' => 'billing_address',
                            );
                        }
                    } else {
                        if (empty($this->postData['delivery_options']['home_delivery']['address']['city'])){
                            $hasErrors = true;
                            $this->addError('delivery_options.home_delivery.address.city', 'City is a required field');
                        }
                        if (empty($this->postData['delivery_options']['home_delivery']['address']['street'])){
                            $hasErrors = true;
                            $this->addError('delivery_options.home_delivery.address.street', 'Street is a required field');
                        }
                        if (empty($this->postData['delivery_options']['home_delivery']['address']['house_number'])){
                            $hasErrors = true;
                            $this->addError('delivery_options.home_delivery.address.house_number', 'Housenumber is a required field');
                        }
                        if (empty($this->postData['delivery_options']['home_delivery']['address']['postal_code'])){
                            $hasErrors = true;
                            $this->addError('delivery_options.home_delivery.address.postal_code', 'Postalcode is a required field');
                        }
                    }
                    break;
                default:
                    $hasErrors = true;
                    $this->addError('delivery_options',
                        'Delivery option "'.$this->postData['delivery_options']['selected_delivery_option'].'" does not exists. Suggested values: pick_up_in_shop/home_delivery');
                    break;
            }

            // Cache result if no errors
            if (!$hasErrors) {
                $this->cacheHashes['delivery']['default'] = md5(serialize($this->getCustomerDeliveryJsonProperties()));
                $this
                    ->getCart()
                    ->getQuote()
                    ->setShippingData(Mage::helper('core')->jsonEncode($data))
                    ->setPaymentMethod($data['payment'])
                ;
            }
        }
    }

    /**
     * Map all customer data
     * @return array
     */
    protected function renderCustomerPostData()
    {

        $data = array(
            'email' => $this->postData['contracting_party']['email'],
            'type' => ($this->checkoutIsBusiness() ? 1 : 0),
            'gender' => $this->getGender($this->postData['contracting_party']['gender']),
            'firstname' => $this->postData['contracting_party']['initials'],
            'middlename' => $this->postData['contracting_party']['last_name_prefix'],
            'lastname' => $this->postData['contracting_party']['last_name'],
            'dob' => $this->getDateFromTimestamp($this->postData['contracting_party']['birthdate']),
            'is_business' => (int) $this->postData['is_business'],
            'id_type' => $this->getIdType($this->postData['identity']['identity_type']),
            'id_number' => $this->postData['identity']['identity_number'],
            'valid_until' => $this->getDateFromTimestamp($this->postData['identity']['identity_expiry_date']),
            'issuing_country' => $this->postData['identity']['nationality'],
            'company_name' => $this->postData['business_details']['company']['name'],
            'company_date' => $this->getDateFromTimestamp($this->postData['business_details']['company']['establish_date']),
            'company_coc' => $this->postData['business_details']['company']['coc_number'],
            'company_vat_id' => $this->postData['business_details']['company']['vat_number'],
            'company_legal_form' => $this->getCompanyLegalForm($this->postData['business_details']['company']['legal_form']),
            'company_address' => 'other_address',
            'foreignAddress' => array(),
            'otherAddress' => array(),
            'contractant_gender' => $this->getGender($this->postData['contracting_party']['gender']),
            'contractant_firstname' => $this->postData['contracting_party']['initials'],
            'contractant_middlename' => $this->postData['contracting_party']['last_name_prefix'],
            'contractant_lastname' => $this->postData['contracting_party']['last_name'],
            'contractant_dob' => $this->getDateFromTimestamp($this->postData['contracting_party']['birthdate']),
            'contractant_id_type' => $this->getIdType($this->postData['identity']['identity_type']),
            'contractant_id_number' => $this->postData['identity']['identity_number'],
            'contractant_valid_until' => $this->getDateFromTimestamp($this->postData['identity']['identity_expiry_date']),
            'contractant_issuing_country' => $this->postData['identity']['nationality'],
            'privacy_email' => ($this->postData['privacy']['marketing'] == "true"
                    ? true : false),
            'privacy_sms' => ($this->postData['privacy']['marketing'] == "true" ? true
                    : false),
            'privacy_number_information' => ($this->postData['privacy']['number_info']
            == "true" ? true : false),
            'privacy_phone_books' => ($this->postData['privacy']['number_info'] == "true"
                    ? true : false),
        );

        $data['correspondence_email'] = $this->getCorrespondenceEmail();

        // Override by business
        if ($this->checkoutIsBusiness()) {
            $useCompanyData = isset($this->postData['business_details']['contracting_party']['initials']) && !empty($this->postData['business_details']['contracting_party']['initials']);
            $customerData = $useCompanyData ? $this->postData['business_details']['contracting_party'] : $this->postData['contracting_party'];

            $data['contractant_gender']     = $this->getGender($customerData['gender']);
            $data['contractant_firstname']  = $customerData['initials'];
            $data['contractant_middlename'] = $customerData['last_name_prefix'];
            $data['contractant_lastname']   = $customerData['last_name'];
            $data['contractant_dob']        = $this->getDateFromTimestamp($customerData['birthdate']);
            $data['email']                  = $customerData['email'];
            $data['gender']                 = $this->getGender($customerData['gender']);
            $data['firstname']              = $customerData['initials'];
            $data['middlename']             = $customerData['last_name_prefix'];
            $data['lastname']               = $customerData['last_name'];
            $data['dob']                    = $this->getDateFromTimestamp($customerData['birthdate']);

            $useCompanyAddress = isset($this->postData['business_details']['company']['address']) && !empty($this->postData['business_details']['company']['address']['city']);
            $addressData = $useCompanyAddress ? $this->postData['business_details']['company']['address']
                : $this->postData['contracting_party']['invoice_address'];

            $address              = array(
                'company_street' => $addressData['street'],
                'company_house_nr' => $addressData['house_number'],
                'company_house_nr_addition' => $addressData['house_number_addition'],
                'company_postcode' => $addressData['postal_code'],
                'company_city' => $addressData['city'],
                'company_country_id' => $this->getCountryFromCode($addressData['country'])
            );
            $data['otherAddress'] = $address;
        }
        return $data;
    }

    protected function getCorrespondenceEmail()
    {
        if ($this->checkoutIsBusiness()) {
            return $this->postData['business_details']['contracting_party']['correspondence_email'];
        } else {
           return $this->postData['contracting_party']['correspondence_email'];
        }
    }

    /**
     * Render additional data
     * @return array
     */
    protected function renderAdditionalPostData()
    {
        $data = array();
        if ($this->checkoutIsBusiness()) {
            $data['email']     = array($this->postData['business_details']['contracting_party']['email']);
            $data['fax']       = array();
            $phone             = array();
            !empty($this->postData['business_details']['contracting_party']['phone_1'])
                        ? $phone[]           = $this->postData['business_details']['contracting_party']['phone_1']
                        : '';
            !empty($this->postData['business_details']['contracting_party']['phone_2'])
                        ? $phone[]           = $this->postData['business_details']['contracting_party']['phone_2']
                        : '';
            $data['telephone'] = $phone;
            $data['correspondence_email'] = $this->getCorrespondenceEmail();
        } else {
            $data['email']     = array($this->postData['contracting_party']['email']);
            $data['fax']       = array();
            $phone             = array();
            !empty($this->postData['contracting_party']['phone_1']) ? $phone[]           = $this->postData['contracting_party']['phone_1']
                        : '';
            !empty($this->postData['contracting_party']['phone_2']) ? $phone[]           = $this->postData['contracting_party']['phone_2']
                        : '';
            $data['telephone'] = $phone;
            $data['correspondence_email'] = $this->getCorrespondenceEmail();
        }
        return $data;
    }

    /**
     * Render the address data
     * @return array
     */
    protected function renderAddressPostData()
    {
        $data = array();
        $validateHelper = Mage::helper('vznl_validators/data');
        $useCompanyAddress = $this->checkoutIsBusiness() && isset($this->postData['business_details']['company']['address']);
        $addressData = $useCompanyAddress ? $this->postData['business_details']['company']['address']
                : $this->postData['contracting_party']['invoice_address'];

        $data['address'] = 'other_address';
        if ($addressData['country'] !== 'NLD') {
            $data['address'] = 'foreign_address';
        }
        $address = [
            'street' => $addressData['street'] . "\n" . $addressData['house_number'] . "\n" . $addressData['house_number_addition'],
            'houseno' => $addressData['house_number'],
            'addition' => $addressData['house_number_addition'],
            'extra' => [],
            'city' => $addressData['city'],
            'region' => '',
            'postcode' => $addressData['postal_code'],
            'country_id' => $this->getCountryFromCode($addressData['country']),
        ];
        $data['foreignAddress'] = $address;
        $data['otherAddress']   = $address;
        $data['telephone']      = $this->checkoutIsBusiness() ? $this->postData['business_details']['contracting_party']['phone_1']
                : $this->postData['contracting_party']['phone_1'];
        $accountNo              = is_numeric($this->postData['payment_details']['account_nr'])
                ? $validateHelper->convertBicToIban($this->postData['payment_details']['account_nr'])
                : $this->postData['payment_details']['account_nr'];
        $data['account_no'] = strtoupper(preg_replace('/\s+/', '', $accountNo));
        $data['account_holder'] = $this->postData['payment_details']['account_name'];
        return $data;
    }

    /**
     * Get a customer entity for this quote
     * @return Vznl_Customer_Model_Customer_Customer
     */
    protected function getCustomerEntity()
    {
        $ban = $this->checkoutIsBusiness() ? $this->postData['business_details']['company']['billing_customer_id']
            : $this->postData['contracting_party']['billing_customer_id'];
        $customerExists = Mage::getModel('customer/customer')->getCollection()->addFieldToFilter('ban', $ban)->getFirstItem();
        if (!empty($ban) && $customerExists && $customerExists->getId()){
            $customer = Mage::getModel('customer/customer')->load($customerExists->getId());
            if (empty($ban) || !$customer->getId()) {
                $this->addError(($this->checkoutIsBusiness() ? 'business_details.company.billing_customer_id'
                    : 'contract_party.billing_customer_id'),
                    'No customer found for billing_customer_id "'.$ban.'"');
                return Mage::getModel('customer/customer');
            }

            // Validate of the CTNs belong to the customer
            foreach ($this->postData['products'] as $index => $data) {
                if ($data['salestype'] === 'retention') {
                    if (!$customer->getCustomerCtn($data['ctn'])) {
                        $this->addError('products[' . $index . '].ctn',
                            'CTN "' . $data['ctn'] . '" does not belong to the selected customer, available ctns: ' . print_r($customer->getCtn(true), true));
                        $customer = Mage::getModel('customer/customer'); // Unset te customer
                    }
                }
            }
        } else {
            $customer = Mage::getModel('customer/customer');
        }
        return $customer;
    }

    /**
     * Get all used CTNs in this order
     * @return array
     */
    protected function getShoppingcartCtns()
    {
        $ctns = array();
        foreach ($this->postData['products'] as $index => $data) {
            if (!empty($data['ctn'])) {
                $ctns[] = $data['ctn'];
            }
        }
        return $ctns;
    }

    /**
     * Fix customer address details
     */
    protected function fixCustomerAddresses($customer) {
        if ($customer->getId() && (!$customer->getDefaultBillingAddress() || ($customer->getDefaultBillingAddress() && !$customer->getDefaultBillingAddress()->getFirstname()))) {
            $data['customer']   = $this->renderCustomerPostData();
            $data['address']    = $this->renderAddressPostData();

            $defaultAddress = $customer->getDefaultBillingAddress() ?: Mage::getModel('customer/address');
            $address = $data['address']['otherAddress'] +
                [
                    'street' => [
                        $data['address']['otherAddress']['street'],
                        $data['address']['otherAddress']['houseno'],
                        $data['address']['addition']]
                ];

            $defaultAddress
                ->addData($data['customer'] + $address)
                ->setCountryId('NL')
                ->setCustomerId($customer->getId())
                ->setIsDefaultBilling('1')
                ->save();
        }

        if ($customer->getId() && (!$customer->getDefaultShippingAddress() || ($customer->getDefaultShippingAddress() && !$customer->getDefaultShippingAddress()->getFirstname()))) {
            $data['customer']   = $this->renderCustomerPostData();
            $data['address']    = $this->renderAddressPostData();

            $defaultAddress = $customer->getDefaultShippingAddress() ?: Mage::getModel('customer/address');
            $address = $data['address']['otherAddress'] +
                [
                    'street' => [
                        $data['address']['otherAddress']['street'],
                        $data['address']['otherAddress']['houseno'],
                        $data['address']['addition']
                    ]
                ];

            $defaultAddress
                ->addData($data['customer'] + $address)
                ->setCountryId('NL')
                ->setCustomerId($customer->getId())
                ->setShippingMethod('freeshipping_freeshipping')
                ->setSameAsBilling($this->postData['delivery_options']['home_delivery']['address_same_as_invoice'])
                ->setIsDefaultShipping('1')
                ->save();
        }
    }



    /**
     * Store all the customer/business details in the quote
     */
    protected function setCustomer($quote, $customer)
    {
        $data['customer']   = $this->renderCustomerPostData();
        $data['address']    = $this->renderAddressPostData();
        $data['additional'] = $this->renderAdditionalPostData();

        foreach($data as $type => $contents) {
            foreach ($contents as $key => $value) {
                if (!is_array($data[$type][$key]) && empty($data[$type][$key])) {
                    unset($data[$type][$key]);
                }
            }
        }

        // Guest
        if ($this->getSalesType() === Vznl_Checkout_Model_Sales_Quote_Item::ACQUISITION && !$customer->getId()) {

            $correspondanceEmailQuote = $this->checkoutIsBusiness() ? $this->postData['business_details']['contracting_party']['correspondence_email']
                : $this->postData['contracting_party']['correspondence_email'];

                $quote->setCorrespondenceEmail(
                    !empty($correspondanceEmailQuote) ? $correspondanceEmailQuote
                            : $data['customer']['email']
                );

                // Store
                try {
                    $this->saveCustomer($data, $customer);
                    $this->cacheHashes['customer']['default'] = md5(serialize($this->getCustomerDeliveryJsonProperties()));
                } catch (Exception $e) {
                    $errors = json_decode($e->getMessage());
                    foreach ($errors as $key => $error) {
                        $this->addError($this->getMapping($key), $error);
                    }
                }

            $customer->addData($data['customer'])
                ->setFirstname($data['customer']['firstname'])
                ->setLastname($data['customer']['lastname'])
                ->setAdditionalEmail($data['customer']['email'])
                ->setPhoneLocalAreaCode($data['customer']['prefix'])
                ->setPhoneNumber($data['additional']['telephone'])
                ->setEmail($data['customer']['email'])
                ->setIsBusiness((int)$this->checkoutIsBusiness())
                ->setDob($data['customer']['dob'])
                ;
            $customer->getIsBusiness() ?
                $customer->setContractantPrefix($this->getCustomerPrefix($data['customer']['contractant_gender']))
                : $customer->setPrefix($this->getCustomerPrefix($data['customer']['gender']));

            $customer->save();

            $quote = $this->getCheckoutSession()->getQuote();

            $quote->setCustomer($customer)
                ->addData($data['customer'])
                ->addData($data['customer']['otherAddress'])
                ->setCustomerAccountHolder($data['address']['account_holder'])
                ->setCustomerAccountNumber($data['address']['account_no'])
                ->setCustomerDob($data['customer']['dob'])
                ->setCustomerIsBusiness((int)$this->postData['is_business'])
                ->setAdditionalTelephone(implode(';',$data['additional']['telephone']))
                ->setBillingAddress(Mage::getModel('sales/quote_address')->addData($customer->getDefaultBillingAddress()
                        ? $customer->getDefaultBillingAddress()->getData()
                        : array()))
                ->setShippingAddress(Mage::getModel('sales/quote_address')->addData($customer->getDefaultShippingAddress()
                        ? $customer->getDefaultShippingAddress()->getData()
                        : array()));

        } else {
            if ($customer->getId()) {
                try {
                    // Add missing customer data
                    $customer->addData($data['customer'])
                        ->setFirstname($data['customer']['firstname'])
                        ->setLastname($data['customer']['lastname'])
                        ->setAdditionalEmail($data['customer']['email'])
                        ->setPhoneLocalAreaCode($data['customer']['prefix'])
                        ->setPhoneNumber($data['additional']['telephone'])
                        ->setEmail($data['customer']['email'])
                        ->setIsBusiness((int)$this->checkoutIsBusiness())
                        ->setDob($data['customer']['dob']);

                    // Fix customer linking for CS
                    $ban = $this->checkoutIsBusiness() ? $this->postData['business_details']['company']['billing_customer_id']
                        : $this->postData['contracting_party']['billing_customer_id'];
                    if (empty($customer->getBan()) && !empty($ban)) {
                        $customer->setBan($ban);
                    }

                    $customer->getIsBusiness() ?
                        $customer->setContractantPrefix($this->getCustomerPrefix($data['customer']['contractant_gender']))
                        : $customer->setPrefix($this->getCustomerPrefix($data['customer']['gender']));

                    $customer->save();

                    //$this->saveCustomer([], $customer);

                    $this->cacheHashes['customer']['default'] = md5(serialize($this->getCustomerDeliveryJsonProperties()));

                    // Override customer email if email is empty
                    $correspondanceEmail = $customer->getCorrespondanceEmail();
                    if (empty($correspondanceEmail)) {
                        $email = $this->checkoutIsBusiness() ? $this->postData['business_details']['contracting_party']['email']
                            : $this->postData['contracting_party']['email'];
                        $customer->setAdditionalEmail($email);
                    }
                    $quote->setCustomer($customer)
                        ->addData($data['customer'])
                        ->addData($data['customer']['otherAddress'])
                        ->setCompanyName($customer->getCompanyName())
                        ->setCustomerAccountHolder($data['address']['account_holder'])
                        ->setCustomerAccountNumber($data['address']['account_no'])
                        ->setCustomerDob($data['customer']['dob'])
                        ->setCustomerIsBusiness((int)$this->postData['is_business'])
                        ->setAdditionalTelephone(implode(';',$data['additional']['telephone']))
                        ->setBillingAddress(Mage::getModel('sales/quote_address')->addData($customer->getDefaultBillingAddress()
                            ? $customer->getDefaultBillingAddress()->getData()
                            : array()))
                        ->setShippingAddress(Mage::getModel('sales/quote_address')->addData($customer->getDefaultShippingAddress()
                            ? $customer->getDefaultShippingAddress()->getData()
                            : array()));

                } catch (Exception $e) {
                    $errors = json_decode($e->getMessage());
                    if (is_string($errors)) {
                        $errors[] = $errors;
                    }
                    foreach ($errors as $key => $error) {
                        $this->addError($this->getMapping($key), $error);
                    }
                }
            } else {
                return false;
            }
        }

            $quote->setContractAccepted(($this->postData['privacy']['terms_agreed']
                        ? true : false));
            $this->getCart()->setQuote($quote);
            $this->getCheckoutSession()->replaceQuote($quote);

    }

    /**
     * Get the sales type for the quote
     * @return string
     */
    public function getSalesType()
    {
        foreach ($this->postData['products'] as $index => $data) {
            switch ($data['salestype']) {
                case 'acquisition';
                    return Vznl_Checkout_Model_Sales_Quote_Item::ACQUISITION;
                case 'retention';
                    if (empty($data['ctn'])) {
                        $this->addError('products['.$index.'].ctn',
                            'CTN is required for a retention order');
                        return null;
                    }
                    return Vznl_Checkout_Model_Sales_Quote_Item::RETENTION;
                case 'inlife':
                    if (empty($this->postData['inlifesales']) || !isset($this->postData['inlifesales'])) {
                        $this->addError('inlifesales',
                            'There is no inlifesales block available');
                        return null;
                    }
                    return Vznl_Checkout_Model_Sales_Quote_Item::INLIFE;
                default:
                    return Vznl_Checkout_Model_Sales_Quote_Item::ACQUISITION;
            }
        }
        return null;
    }

    /**
     * Create a package or update the existing one
     * @param integer $quoteId
     * @param integer $packageId
     * @param string $salesType
     * @param string $packageType
     * @param integer $packageCreationTypeId
     * @param string $ctn
     * @return Vznl_Package_Model_Package $packageModel
     */
    protected function createPackage($quoteId, $packageId, $salesType,
                                     $packageType, $packageCreationTypeId, $ctn = null)
    {
        // Create the new package for the quote or update if already created
        /** @var Vznl_Package_Model_Package $packageModel */
        $packageModel = Mage::getModel('package/package')->getCollection()
            ->addFieldToFilter('quote_id', $quoteId)
            ->addFieldToFilter('package_id', $packageId)
            ->getFirstItem();

        if (!$packageModel->getId()) {
            $packageModel->setQuoteId($quoteId)
                ->setPackageId($packageId);
        }

        $packageModel
            ->setSaleType($salesType)
            ->setCtn($ctn)
            ->setPackageCreationTypeId($packageCreationTypeId)
            ->setType($packageType);

        if ($ctn && strtolower($salesType) == strtolower(Vznl_Checkout_Model_Sales_Quote_Item::RETENTION)) {
            // RFC-160003: When a retention package is created, also save the ctn data for the contract
            $ctnModel = Mage::getResourceModel('ctn/ctn_collection')->addFieldToFilter('ctn', $ctn)->getFirstItem();
            if ($ctnModel->getId()) {
                $packageModel
                    ->setCtnOriginalStartDate($ctnModel->getStartDate())
                    ->setCtnOriginalEndDate($ctnModel->getEndDate());
            }
        }

        $packageModel->save();
        return $packageModel;
    }

    /**
     * Add the product to the cart
     * @param Vznl_Catalog_Model_Product $product
     * @return \RestApi_Quotes_Save
     * @throws Exception
     */
    protected function addProduct($product, $packageId, $packageType, $index = 0)
    {
        $cs = Mage::getSingleton('customer/session');
        $storeId = $cs->getAgent() && $cs->getAgent()->getDealer() ? $cs->getAgent()->getDealer()->getStoreId() : Mage::app()->getStore()->getId();
        $storeCode = Mage::getStoreConfig('vodafone_service/axi/vodafone_warehouse');
        $stocks = Mage::helper('stock')->getStockForProducts(array($product->getId()), $storeCode);

        if ($product->isSellable($stocks[$product->getId()]['qty'],
                $stocks[$product->getId()]['backorder_qty'], $storeCode, $storeId)) {
            $product->setPackageType($packageType);
            $numberCtns  = count($this->getCart()->getQuote()->getPackageCtns($packageId));
            $isOneOfDeal = Mage::helper('dyna_configurator/cart')->isOneOfDealActive();
            $item        = $this->getCart()->addProduct($product,
                array(
                'qty' => $numberCtns ? : 1,
                'one_of_deal' => ($isOneOfDeal && $numberCtns) ? 1 : 0)
            );
            $item->setPackageId($packageId);
            return $item;
        } else {
            $this->addError('products['.$index.']',
                'Product "'.$product->getSku().'" ("'.$product->getName().'") is not sellable (local stock: ' . (int) $stocks[$product->getId()]['qty'] . ', backorder: ' . (int) $stocks[$product->getId()]['backorder_qty'] . ' )');
        }
    }

    /**
     * Check if the given data and type/key combination has changes compared to the hash stored in the quote
     * @param string $type
     * @param array $data
     * @param string $key
     * @return boolean
     */
    protected function jsonHasChanges($type, $data, $key = 'default')
    {
        $cacheHashes = unserialize($this->getCart()->getQuote()->getHawaiiCacheHashes());
        if ($cacheHashes[$type][$key] !== md5(serialize($data))) {
            return true;
        }
        return false;
    }

    /**
     * @param $packageIds
     * @throws Exception
     * @return $this
     */
    protected function removeProductsForPackage($packageIds)
    {
        //  If there is nothing in the input, delete nothing
        if (!$packageIds) {
            return $this;
        }

        $quote    = $this->getCart()->getQuote();
        $allItems = $quote->getAllItems();
        if (is_array($packageIds)) {

            // When an array of package ids is received, only make one query to retrieve all
            $packages = Mage::getModel('package/package')->getPackages(null,
                $quote->getId());
            foreach ($packages as $package) {
                /** @var Vznl_Package_Model_Package $package */
                if (in_array($package->getPackageId(), $packageIds)) {
                    Mage::helper('pricerules')->decrementCoupon($package->getCoupon(),
                        $quote->getCustomerId());
                    $package->delete();
                }
            }

            // Only loop once through the items
            foreach ($allItems as $item) {
                if (in_array($item->getPackageId(), $packageIds)) {
                    $item->isDeleted(true);
                }
            }
        } else {
            /** @var Vznl_Package_Model_Package $package */
            $package = Mage::getModel('package/package')->getPackages(null,
                    $quote->getId(), $packageIds)->getFirstItem();
            if ($package && $package->getId()) {
                Mage::helper('pricerules')->decrementCoupon($package->getCoupon(),
                    $quote->getCustomerId());
                $package->delete();
            }

            foreach ($allItems as $item) {
                if ($item->getPackageId() == $packageIds) {
                    $item->isDeleted(true);
                }
            }
        }
        return $this;
    }

    /**
     * Setup the products for the quote
     */
    protected function setProducts()
    {
        $packagePointer = 1;
        foreach ($this->postData['products'] as $index => $data) {
            unset($sim);
            unset($device);
            unset($subscription);
            unset($devicerc);
            $packageId = (int) $data['package_id'];

            if ($packageId != $packagePointer) {
                $this->addError('products['.$index.'].package_id',
                    'PackageID "'.$packageId.'" is ignored, expected PackageID "'.$packagePointer.'"');
            } else {
                $packagePointer++;
                // Check if package has changes and needs to be updated
                if ($this->jsonHasChanges('products',
                        $this->getProductPackageJsonProperties($data),
                        $packageId)) {
                    $hasErrors   = false;
                    $this->removeProductsForPackage($packageId);
                    $salesType   = $this->getSalesType();
                    $ctn         = !isset($data['ctn']) ? null : $data['ctn'];
                    $this->getCart()->getQuote()->setActivePackageId($packageId);
                    $packageType = $this->getPackageType($data);
                    $packageCreationTypeId = $this->getPackageCreationTypeId($packageType);

                    // Create the new package for the quote or update if already created
                    $currentPackage = $this->createPackage($this->getCart()->getQuote()->getId(),
                        $packageId, $salesType, $packageType, $packageCreationTypeId, $ctn);

                    // Setup products array
                    foreach ($this->_productTypes as $productType) {
                        if ($productType == self::PRODUCT_TYPE_EXTRAS) {
                            foreach ($data[$productType] as $key => $extraType) {
                                foreach ($extraType as $sku) {
                                    if (is_array($sku)) {
                                        $sku = $sku['sku'];
                                    }
                                    $productCollection = Mage::getModel('catalog/product')
                                        ->getCollection()
                                        ->addAttributeToSelect('*')
                                        ->addAttributeToFilter('type_id',
                                            array('eq' => 'simple'))
                                        ->addAttributeToFilter('status',
                                            array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))
                                        ->addAttributeToFilter('is_deleted',
                                            array('eq' => false))
                                        ->addAttributeToFilter('sku', $sku)
                                        ->addStoreFilter();
                                    $tempProduct       = $productCollection->getFirstItem();
                                    if (!$tempProduct->getId()) {
                                        $hasErrors = true;
                                        $this->addError('products['.$index.'].'.$productType,
                                            'Product with SKU "'.$sku.'" not found.');
                                    } else {
                                        $tempProduct = Mage::getModel('catalog/product')->load($tempProduct->getId());
                                        $this->addProduct($tempProduct,
                                            $packageId, $packageType, $index);
                                    }
                                }
                            }
                        } elseif (!empty($data[$productType])) {
                            $productCollection = Mage::getModel('catalog/product')
                                ->getCollection()
                                ->addAttributeToSelect('*')
                                ->addAttributeToFilter('type_id',
                                    array('eq' => 'simple'))
                                ->addAttributeToFilter('status',
                                    array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))
                                ->addAttributeToFilter('is_deleted',
                                    array('eq' => false))
                                ->addAttributeToFilter('sku',
                                    $data[$productType])
                                ->addStoreFilter();
                            $tempProduct       = $productCollection->getFirstItem();
                            if (!$tempProduct || !$tempProduct->getId()) {
                                $hasErrors = true;
                                $this->addError('products['.$index.'].'.$productType,
                                    'Product with SKU "'.$data[$productType].'" not found.');
                            } else {
                                $tempProduct = Mage::getModel('catalog/product')->load($tempProduct->getId());
                                $item        = $this->addProduct($tempProduct,
                                    $packageId, $packageType, $index);
                                if ($item) {
                                    switch ($productType) {
                                        case 'sim_sku':
                                            $sim          = $tempProduct;
                                            break;
                                        case 'device_sku':
                                            $device       = $tempProduct;
                                            break;
                                        case 'subscription_sku':
                                            $subscription = $tempProduct;
                                            break;
                                        case 'devicerc_sku':
                                            $devicerc     = $tempProduct;
                                            break;
                                    }
                                }
                            }
                        }
                    }
                    // Add SIM if needed
                    if (!isset($sim) || !$sim->getId()) {
                        if ($salesType == Vznl_Checkout_Model_Sales_Quote_Item::RETENTION) {
                            if (isset($subscription)) {
                                $currentPackage->setOldSim(true)->save();
                            }
                        } else {
                            if (isset($device) && Mage::helper('dyna_catalog')->is(array(
                                    Vznl_Catalog_Model_Type::SUBTYPE_DEVICE),
                                    $device) && isset($subscription)) {
                                $sim = Mage::getModel('catalog/product')
                                    ->getCollection()
                                    ->addFieldToFilter('attribute_set_id', 53)
                                    ->addAttributeToFilter('type_id',
                                        array('eq' => 'simple'))
                                    ->addFieldToFilter(Vznl_Catalog_Model_Product::DEFAULT_SIM_ATTRIBUTE,
                                        $device->getData(Vznl_Catalog_Model_Product::DEFAULT_SIM_ATTRIBUTE))
                                    ->addAttributeToFilter('is_deleted',
                                        array('eq' => false))
                                    ->addAttributeToFilter('status',
                                        array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))
                                    ->addStoreFilter()
                                    ->getFirstItem();
                                if ($sim && $sim->getId()) {
                                    $sim = Mage::getModel('catalog/product')->load($sim->getId());
                                    $this->addProduct($sim, $packageId,
                                        $packageType);
                                }
                            }
                        }
                    }
                    // Store cache hashes
                    if (!$hasErrors) {
                        $this->cacheHashes['products'][$packageId] = md5(serialize($this->getProductPackageJsonProperties($data)));
                    }
                }
                $remainingPackages[] = (int) $data['package_id'];
            }
        }

        $packagesToRemove = array();
        $packages         = Mage::getModel('package/package')->getPackages(null,
            $this->getCart()->getQuote()->getId());

        // Remove all unused packages
        foreach ($packages as $package) {
            if (!in_array($package->getPackageId(), $remainingPackages)) {
                $packagesToRemove[] = $package->getPackageId();
                unset($this->cacheHashes['products'][$package->getPackageId()]);
            }
        }
        $this->getHasSubscription();
        $this->removeProductsForPackage($packagesToRemove);
    }

    protected function getHasSubscription()
    {
        foreach ($this->postData['products'] as $index => $data) {
            $tempProduct = Mage::getModel('catalog/product')->loadByAttribute('sku',
                $data['subscription_sku']);
            if ($tempProduct && $tempProduct->getId()) {
                $this->hasSubscription = true;
                break;
            }
        }
    }

    /**
     * Get the first possible porting date for Hawaii
     * @return string
     */
    protected function getFirstPortingDate($provider, $enddateContract)
    {
        $portingDays = $provider !== self::PORTING_INTERNAL_PROVIDER ? Vznl_Validators_Helper_Data::MIN_PORTING_DAYS
                : Vznl_Validators_Helper_Data::MIN_PORTING_DAYS_PREPAID;
        $pointerDate = new DateTime();
        $contractDate = new DateTime($enddateContract);
        $i  = 0;
        while (true) {
            if (!Mage::helper('dyna_validators/data')->validateHolidayDate($pointerDate->format('d-m-Y'))) {
                $portingDays++;
            } elseif (!Mage::helper('dyna_validators/data')->validateWorkingDay($pointerDate->format('d-m-Y'))) {
                $portingDays++;
            }

            if ($i >= $portingDays) {
                if (!is_null($enddateContract) && $pointerDate < $contractDate) {
                    return $contractDate->format('d-m-Y');
                }
                return $pointerDate->format('d-m-Y');
            }
            $pointerDate->modify('+1 day');
            $i++;
        }
    }

    /**
     * Setup number porting
     */
    protected function setNumberPorting()
    {
        /** @var Vznl_Checkout_Helper_Data $checkoutHelper */
        $checkoutHelper = Mage::helper('dyna_checkout');
        if (!empty($this->postData['mobile_number_porting'])) {
            foreach ($this->postData['mobile_number_porting'] as $index => $data) {
                $packageId = $data['package_id'];
                /** @var Vznl_Package_Model_Package $package */
                $package = $this->getCart()->getQuote()->getPackageById($packageId);
                if ($data['customer_requests_porting'] == "true") {
                    if ($this->jsonHasChanges('porting', $data, $index)) {
                        try {
                            $this->getCart()->getQuote()->setActivePackageId($packageId);
                            $portingType = $data['porting_data']['validation_type'];
                            $packagePortingData = [
                                'mobile_number' => $data['porting_data']['current_msisdn'],
                                'choice' => $portingType,
                                'client_id' => isset($data['porting_data']['contract_number']) ? $data['porting_data']['contract_number'] : null,
                            ];

                            $checkoutHelper->handleNumberPortingData($package, $packagePortingData);
                            $this->cacheHashes['porting'][$index] = md5(serialize($data));
                        } catch (Exception $ex) {
                            $errorKey = sprintf('mobile_number_porting[%s].porting_data', $index);
                            $this->addError($errorKey, $ex->getMessage());
                        }
                    }
                } else {
                    $checkoutHelper->clearNumberPortingData($package);
                }
            }
        }
    }

    /**
     * Set the coupons to the packages
     */
    private function setCoupons()
    {
        $couponsArray = array();

        foreach ($this->postData['products'] as $index => $data) {
            $packageId                = $data['package_id'];
            $couponCode               = strlen($data['voucher']) ? $data['voucher']
                    : '';
            $couponsArray[$packageId] = $couponCode;
        }

        $this->getCheckoutSession()->getQuote()->setCouponsPerPackage($couponsArray);
        $this->cacheHashes['coupon'][$packageId] = md5(serialize($this->getCouponJsonProperties($data)));
    }

    /**
     * Set the promotions to the package
     * Only possible when 1 suitable promotion is found for the desired package
     */
    public function setPromotions()
    {
        $packages = Mage::getModel('package/package')->getCollection()->addFieldToFilter('quote_id',
            $this->getCart()->getQuote()->getId());
        foreach ($packages as $package) {
            $this->getCart()->getQuote()->setActivePackageId($package->getPackageId());
            $salesRuleCollection = Mage::helper('pricerules')->findApplicablePromoRules(false,
                $package->getPackageId());

            // Only accept if there is 1 possible promo per package for webshop
            if (count($salesRuleCollection) === 1) {
                $salesRule = array_shift($salesRuleCollection);
                Mage::helper('pricerules')->applyPromoRule($salesRule->getId(),
                    false, false);
            }
        }
    }

    /**
     * Connect the given dealer to the quote
     * @return Vznl_Agent_Model_Agent
     */
    protected function setDealer()
    {
        $dealer = Mage::getModel('agent/dealer')->load($this->postData['dealer_code'],
            'vf_dealer_code');
        if ($dealer->getId()) {
            $agent = Mage::getModel('agent/agent')
                ->getCollection()
                ->addFieldToFilter('dealer_id', $dealer->getId())
                ->addFieldToFilter('store_id', $dealer->getStoreId())
                ->getFirstItem();
            if (!$agent->getId()) {
                $this->addError('dealer_code',
                    'Dealer code "'.$this->postData['dealer_code'].'" has no connected Agent');
            } else {
                $agent->setDealer($dealer);
                Mage::getSingleton('customer/session')->setAgent($agent);
                $this->cacheHashes['dealer']['default'] = md5(serialize($this->postData['dealer_code']));
                return $agent;
            }
        } else {
            $this->addError(
                'dealer_code',
                'Dealer code "' . $this->postData['dealer_code'] . '" not found'
            );
        }
    }

    /**
     * Check if the checkout flow is business
     * @return boolean
     */
    protected function checkoutIsBusiness()
    {
        if ($this->postData['is_business']) {
            return true;
        } if ($this->postData['chosen_order_type_is_business']) {
            return true;
        }

        return false;
    }

    protected function setFfValues()
    {
        if (Mage::app()->getStore()->getCode() != Vznl_Agent_Model_Website::WEBSITE_FRIENDS_AND_FAMILY) {
            // Do nothing if the store is not Friends and Family
            return;
        }

        if (!isset($this->postData["friendsfamily"]["code"]) || !$this->postData["friendsfamily"]["code"]) {
            return;
        }

        if (isset($this->postData["friendsfamily"]["customer_reference"]) && $this->postData["friendsfamily"]["customer_reference"]) {
            $this->getCart()->getQuote()->setFfCustomerReference($this->postData["friendsfamily"]["customer_reference"]);
        }

        $products = [];
        $postDataPrdCount = count($this->postData['products']);
        for ($i = 0; $i < $postDataPrdCount; $i++) {
            $this->postData['products'][$i]['voucher'] = $this->postData["friendsfamily"]["code"];
            $this->postData['products'][$i]['is_ff_code'] = true;

            $products[$this->postData['products'][$i]['package_id']] = $this->postData['products'][$i];
        }

        $packages = Mage::getModel('package/package')->getPackages(null, $this->getCart()->getQuote()->getId());

        foreach ($packages as $package) {
            $package->setIsFfCode(true);
            $package->setCoupon($this->postData["friendsfamily"]["code"]);

            $package->save();
        }
    }

    protected function setIltData()
    {
        if ($this->getCart()->getQuote()->isIltRequired() && isset($this->postData['ilt'])) {
            $data = $this->postData['ilt'];
            if (!isset($data['birth_lastname']) || $data['birth_lastname'] === '') {
                $data['birth_lastname'] = $this->checkoutIsBusiness()
                    ? $this->postData['business_details']['contracting_party']['last_name']
                    : $this->postData['contracting_party']['last_name'];
            }
            if (!isset($data['birth_middlename']) || $data['birth_middlename'] === '') {
                $data['birth_middlename'] = $this->checkoutIsBusiness()
                    ? $this->postData['business_details']['contracting_party']['last_name_prefix']
                    : $this->postData['contracting_party']['last_name_prefix'];
            }
            /** @var $helper Vznl_Ilt_Helper_Data */
            $helper = Mage::helper('ilt');
            $response = $helper->validateData($data);

            foreach ($response as $key => $value) {
                $this->addError($this->getMapping($key), $value);
            }
        }
    }

    protected function setupErrors($exception)
    {
        $errors = json_decode($exception->getMessage());
        if (is_array($errors)) {
            foreach ($errors as $key => $error) {
                $this->addError($this->getMapping($key), $error);
            }
        }
    }

    /**
     * Output the quote a json string
     * @return array
     */
    protected function output()
    {
        $data['email']    = $this->getCart()->getQuote()->getAdditionalEmail();
        $data['quote_id'] = $this->getCart()->getQuote()->getId();
        $getClass         = new Vznl_RestApi_Quotes_Get($data);
        $getClass->setErrors($this->getErrors());
        $output           = $getClass->process();
        return $output;
    }

    private function getCustomerPrefix($gender)
    {
        switch ($gender) {
            case 2:
                $prefix = Vznl_Customer_Helper_Services::CUSTOMER_SALUTATION_MRS;
                break;
            case 1:
                $prefix = Vznl_Customer_Helper_Services::CUSTOMER_SALUTATION_MR;
                break;
            default: $prefix = '';
                break;
        }

        return $prefix;
    }
}
