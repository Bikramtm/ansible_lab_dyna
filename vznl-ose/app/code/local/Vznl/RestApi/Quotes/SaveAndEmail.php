<?php

class Vznl_RestApi_Quotes_SaveAndEmail extends Vznl_RestApi_Quotes_Abstract
{
    /**
     * @var array
     */
    protected $postData;
    
    /**
     * @param Mage_Core_Controller_Request_Http $request
     */
    public function __construct(Mage_Core_Controller_Request_Http $request)
    {
        $this->request = $request;
        $this->postData = Zend_Json::decode(file_get_contents("php://input"));
    }

    /**
     * Process the HTTP request.
     * 
     * @return array
     */
    public function process()
    {
        // Init save
        $save = new Vznl_RestApi_Quotes_Save($this->request);

        // Validate data 
        $email = null;
        
        if ($this->isBusinessQuote()) {
            $contractingParty = $this->postData['business_details']['contracting_party'];
        } else {
            $contractingParty = $this->postData['contracting_party'];
        }

        $email = !empty($contractingParty['correspondence_email'])
            ? $contractingParty['correspondence_email']
            : $contractingParty['email'];
        $firstName = $contractingParty['initials'];
        $middleName = $contractingParty['last_name_prefix'];
        $lastName = $contractingParty['last_name'];
        $gender = $contractingParty['gender'];
        $dob = $contractingParty[' birthday'];
        
        // Validate email
        if (is_null($email) || !filter_var($email,FILTER_VALIDATE_EMAIL)) {
            $save->addError('request', 'Email not found or not valid');
        }

        $response = $save->process();
        
        // Check if we CAN email
        $errors = [];
        $emailHelper = Mage::helper('vznl_checkout/email');

//        if ($this->postData['contracting_party']['initials'] == null || empty($this->postData['contracting_party']['initials'])) {
//            $errors[] = 'contracting_party.initials is a required field';
//        }
//        if ($this->postData['contracting_party']['last_name'] == null || empty($this->postData['contracting_party']['last_name'])) {
//            $errors = 'contracting_party.last_name is a required field';
//        }

        if (!Mage::helper('dyna_validators')->validateEmailSyntax($email)) {
            $errors = 'contracting_party.email is not a valid email';
        }
        
        if (count($errors) === 0) {
            switch ($this->getGender($gender)) {
                case 2: $prefix = Mage::helper("dyna_checkout")->__('Mrs.') ;
                    break;
                case 1: $prefix = Mage::helper("dyna_checkout")->__('Mr.') ;
                    break;
                default: $prefix = '';
                break;
            }

            $customer = Mage::getModel('customer/customer')
                ->setFirstname($firstName)
                ->setLastname($lastName)
                ->setDob($this->getDateFromTimestamp($dob))
                ->setAdditionalEmail($email)
                ->setGender($this->getGender($gender))
                ->setPrefix($prefix)
                ->setMiddlename($middleName)
                ->setEmail(uniqid() . Omnius_Customer_Model_Customer_Customer::DEFAULT_EMAIL_SUFFIX)
                ->setIsProspect(true)
                ->save();
            Mage::getSingleton('customer/session')->setCustomer($customer);

            $quote = Mage::getModel('sales/quote')->load($response['dynalean_quote_id']);
            $quote->setCartStatus(Dyna_Checkout_Model_Sales_Quote::CART_STATUS_SAVED)
                ->setCustomer($customer)
                ->setChannel(Mage::app()->getWebsite()->getCode())
                ->setCartStatusStep(Dyna_Checkout_Model_Sales_Quote::CART_STATUS_STEP_CONFIGURATOR)
                ->setCustomerFirstname($firstName)
                ->setCustomerEmail($email)
                ->setAdditionalEmail($email)
                ->setCorrespondenceEmail($email)
                ->setCustomerDob($this->getDateFromTimestamp($dob))
                ->setCustomerLastname($lastName)
                ->setCustomerGender($this->getGender($gender))
                ->setCustomerMiddlename($middleName)
            ;
            $incrementId = Mage::helper('dyna_checkout')->getQuoteIncrementPrefix() . $quote->getId();
            $quote->setData('prospect_saved_quote', $incrementId);
            $quote->save();

            if (!$emailHelper->sendConfirmShoppingCart($quote, $email)) {
                $response['errors'][] = array('key'=>'request', 'message'=>'Error while sending email to customer');
            }
        } else {
            $response['errors'][] = array('key'=>'request', 'message'=>'Email count not be sent since not all required fields are entered');
        }
        return $response;
    }

    /**
     * Checks whether the quote is for a business.
     * 
     * @return bool
     */
    private function isBusinessQuote()
    {
        return (bool) $this->postData['is_business'] || $this->postData['chosen_order_type_is_business'];
    }
}
