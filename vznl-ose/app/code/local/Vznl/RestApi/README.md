# Rest API ReadMe

This module is not implemented as an actual Magento module. 
Therefore no config file is needed in /app/etc/modules. Instead a restapi.php file is needed within the Magento install root directory.

## Unit tests
The following tests from UMP1 were removed:
Vznl_RestApi_Email_ResendConfirmationTest
Vznl_RestApi_RetentionTest

This due to the fact there were no assertions so these tests were risky and causing the build to fail.
These tests might have to be recreated properly at a later date.

## API Calls
### Catalogue
1. Get 
    
    POST request to: /rest/catalogue

    **Data:** 
    - debug: true or false
    - refreshcache: true or false
    - products: true or false
    - stock: true or false (cannot currently be tested with value true, awaiting database migration)
    - mixmatch: true or false
    - compatibility: true or false
    - promotion: true or false (will currently return soft errors, awaiting database migration for actual result)

### Customer
1. GetByUsername

    GET request: /rest/customer/get/username/:username    
    Replace :username with an actual username

### Email
1. ResendConfirmation

    POST request to: /rest/email/resend-confirmation

    **Data:**
    - email: email address
    - order_number: valid order number

2. Email

    PUT request to: /rest/email

    **Data:**
    - culture_code: optional
    - email_code: 2
    - receiver: email address
    - cc: optional email address for cc
    - bcc: optional email address for bcc
    - parameters: optional parameters
    - attachments: optional attachments

### Friends and Family
1. Request

    POST request to: /rest/friendsfamily/request

    **Data:**
    - from: email address from ff_relations db table
    - to: email address

2. Info

    GET request to: /rest/friendsfamily/info/:token
    Replace :token with a code from the db table ff_requests

3. Confirm

    POST request to: /rest/friendsfamily/confirm

    **Data:**
    - token: value from a confirmation_hash frield from ff_requests table
    - email: email addres from the same row in ff_requests table

4. Cancel

    POST request to: /rest/friendsfamily/cancel

    **Data:**
    - hashCode: token from ff_requests table
    - email: email address

### Multimapper
1. Get

    POST request to: /rest/multimapper

    **Data:**
    - promo_sku: promo_sku from db table dyna_multi_mapper
    - priceplan_sku: commercial sku from db table dyna_multi_mapper

### Orders
1. GetHawaii

    GET request to: /rest/orders/:order_id
    
    Replace:order_id with order number
    
2. Get

    GET request to: /rest/orders/get/:order_number
    
    Replace :order_number with order number

3. Overview

    GET request to: /rest/orders/overview/:ban 
    
    Replace :ban with ban number from customer_entity table
    
4. GetDeliveryOrder

    GET request to: /rest/orders/get/delivery/:id
    
    Replace :id with id
    
5. Create

    POST request to: /rest/orders
    
    **Data:**
    - dynalean_quote_id: quote id
    - ilt
        - birth_lastname: last name
    - business_details:
        - contracting_party
            - last_name
            - last_name_prefix
    - contracting_party
        - last_name
        - last_name_prefix
        
6. Build

    POST request to: /rest/orders/build
    
    **Data:**
    - inlifesales
    - adyen_payment_id
    - dynalean_order_id
    - ilt
    - order_number

7. Submit

    POST request to: /rest/orders/:order_id/submit
    Replace :order_id with order number
    
8. Modify

    POST request to: /rest/orders/:order_id/modify
    Replace :order_id with order number
    
    **Data:**
    - delivery_dealer_code: optional dealer code
    - street: street
    - city: city
    - delivery_street: street for delivery dealer (only provide when delivery_dealer_code is empty)
    - delivery_city: city for delivery dealer (only provide when delivery_dealer_code is empty)
    - delivery_postcode: zip code for delivery dealer (only provide when delivery_dealer_code is empty)
    - delivery_country: country for delivery dealer (only provide when delivery_dealer_code is empty).  Can be left empty, defaults to NL
    
9. Cancel

    POST request to: /rest/orders/:order_id/cancel
    Replace :order_id with order number

10. Search
    
    POST request to: /rest/orders/search
    
    **Data:** (*all optional and can be combined*)
    - customer_ctn
    - postcode
    - street
    - customer_ban
    - customer_firstname
    - customer_middlename
    - customer_lastname
    - customer_email
    - customer_dob
    - order_number
    - created_at_from
    - created_at_till
    - company
    - company_coc
    - invoicenr
    - code
    - sim_number
    - ciboodle_id

### Quotes
1. Shops

    GET request to: /rest/quotes/:quote_id/shops
    
    Replace :quote_id with quoteID
    
2. ListByEmail

    GET request to: /rest/quotes/:email
    
    Replace :email with email address from a customer with quotes
    
3. Save

    POST request to: 
    
    **Data:**
    - dealer_code: dealer code
    - payment_method: adyen_hpp, cashondelivery, checkmo or free
    - delivery_options
        - selected_delivery_option: pick_up_in_shop or home_delivery
    - is_business: true or false
    - chosen_order_type_is_business: true or false
    - payment_details
        - account_nr
    - contracting_party
        - correspondence_email: email address
        - phone_1: first phone number
        - phone_2: secondary phone number
        - initials
        - last_name_prefix: middle name
        - last_name: last name
    - business_details
        - contracting_party
            - correspondence_email: email address
            - phone_1: first phone number
            - phone_2: secondary phone number
            - initials
            - last_name_prefix: middle name
            - last_name: last name
    - products
        - category: product category (for example, prepaid)
        - subscription_sku
        - salestype: ACQ (for acquisition), RETBLU (for retention) or ISBLU (for inlife)
        - ctn: ctn number (requoed when salestype is RETBLU)
        - inlifesales (required when salestype is ISBLU)
        - voucher: optional, if added needs to match friends & family code
        - is_ff_code: true or false
        - package_id
        - voucher: coupon code
    - friendsfamily
        - code: friends & family code
        - customer_refernece
    - mobile_number_porting
        - package_id: package number
        - customer_requests_porting: true or false (string, not boolean)
        - porting_data
            - validation_type
            - current_msisdn
            - contract_number
    
4. Save and email

    POST request to: /rest/quotes/:quote_id/save-and-email
    
    Replace :quote_id with a quote ID
    
    **Data:**
    
    Same data as the Quotes/Save API request above. 
    
    This API call executes Quote/Save and then sends an email afterwards.

5. Get

    GET request to: /rest/quotes/:quote_id/:email
    
    Replace :quote_id with a quote id and :email with the email address for the quote
    
    [@todo: cannot be tested yet]
    
### Retention
1. Get

    GET request to: /rest/retention/get/:ban
    
    Replace :ban with ban number from the db table customer_entity

2. Check

    POST request to: /rest/retention/check

    **Data:**
    - isBusiness: true or false
    - kvk: chamber of commerce number (use only if business customer)
    - ctn: ctn number
    - dob: date of birth( use for consumer customers)