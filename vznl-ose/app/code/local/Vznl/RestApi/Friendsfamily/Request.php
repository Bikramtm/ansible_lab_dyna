<?php

class Vznl_RestApi_Friendsfamily_Request extends Vznl_RestApi_Processor
{
    /**
     * Process the HTTP request.
     * @return array
     * @todo Dependent on database migration, test after this has been executed
     */
    public function process()
    {
        $params = $this->getBodyParams();

        $response['success'] = false;
        $response['payload'] = null;
        $response['error'] = array();

        if (!isset($params["from"])) {
            $response['error'][] = array('code'=>'01', 'message'=>'The from param is not provided');
            return $response;
        }
        if (!isset($params["target"])) {
            $response['error'][] = array('code'=>'02', 'message'=>'The target param is not provided');
            return $response;
        }
        if (!filter_var($params['from'], FILTER_VALIDATE_EMAIL)){
            $response['error'][] = array('code'=>'03', 'message'=>'From param is not a valid email');
            return $response;
        }
        if (!filter_var($params['target'], FILTER_VALIDATE_EMAIL)){
            $response['error'][] = array('code'=>'04', 'message'=>'Target param is not a valid email');
            return $response;
        }

        /**
         * @var Vznl_FFHandler_Helper_Request $requestHelper
         */
        $requestHelper = Mage::helper("ffhandler/request");
        
        /**
         * @var Vznl_FFReport_Helper_Email $emailHelper
         */
        $emailHelper = Mage::helper("vznl_ffreport/email");
        
        if ($this->checkIfRequesterIsBlocked($params["from"])) {
            $response['error'][] = array('code'=>'05', 'message'=>'From email is currently blocked');
            return $response;
        }

        $relation = $requestHelper->searchRelationByEmail($params["from"]);
        
        if (!$relation) {
            $requestModel = $requestHelper->generateFailedNoRelation($params["from"], $params["target"]);
            $emailHelper->noRelationFound($requestModel, $params);
            $response['error'][] = array('code'=>'06', 'message'=>'No relation found for the from email');
            return $response;
        }

        $requestModel = $requestHelper->generateValidByEmail($params["from"], [$params["target"]], $relation);
        if ($requestHelper->blockExceededThresholdRequest($requestModel)) {
            $response['error'][] = array('code'=>'07', 'message'=>'Threshold exceeded. From email was blocked.');
            return $response;

        }
        $emailHelper->requestSuccessfullyGenerated($requestModel);
        $response['success'] = true;
        $response['payload']['token'] = $requestModel->getPublicData()['confirmation_hash'];
        return $response;
    }

    /**
     * Check if requester is blocked.
     *
     * @param $requesterEmail
     * @return bool
     */
    private function checkIfRequesterIsBlocked($requesterEmail)
    {
        /**
         * @var Vznl_FFManager_Model_Resource_Blocklist_Collection $blockListCollection
         */
        $blockListCollection = Mage::getModel("ffmanager/blocklist")->getCollection();
        
        // Check if the email is blocked for this relation
        $blockListCollection->filterByEmail($requesterEmail);

        return (bool) count($blockListCollection);
    }
}
