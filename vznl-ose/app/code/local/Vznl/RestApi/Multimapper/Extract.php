<?php

/**
 * Class Vznl_RestApi_Multimapper_Extract
 */
class Vznl_RestApi_Multimapper_Extract
{
    protected const FIXED_STACK = 'fixed';

    protected const FIXED_PACKAGE_TYPE = 'INT_TV_FIXTEL';

    protected const MOBILE_PACKAGE_TYPE = 'MOBILE';

    /**
     * @var array
     */
    protected $availableStacks = ['fixed']; // later add 'mobile' here

    /**
     * @var bool
     */
    protected $cache = true;

    /**
     * @var string
     */
    protected $cacheKey = 'hawaii_rest_mm_extract';

    /**
     * @var int
     */
    protected $cacheTTL = 604800; // 1 week

    /**
     * @var Mage_Core_Controller_Request_Http
     */
    protected $request;

    /**
     * @param Mage_Core_Controller_Request_Http $request
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * @return Vznl_Configurator_Model_Cache
     */
    public function getCache(): Vznl_Configurator_Model_Cache
    {
        return Mage::getSingleton('vznl_configurator/cache');
    }

    /**
     * Get MM extract
     *
     * @return array
     * @throws Exception
     */
    public function process(): array
    {
        if ($this->cache && $this->request->getParam('refreshcache', 'false') == 'false') {
            $response = json_decode($this->getCache()->load($this->cacheKey), true);
            if (!empty($response)) {
                return $response;
            }
        }

        try {
            $response = [
                'catalogVersion' => $this->getCatalogVersions(),
                'importList' => $this->getMMExtract(),
            ];

            // Cache the response
            if ($this->cache) {
                $this->getCache()->save(
                    json_encode($response),
                    $this->cacheKey,
                    [Vznl_Configurator_Model_Cache::CACHE_TAG],
                    $this->cacheTTL
                );
            }

            return $response;
        } catch (\Throwable $throwable) {
            $exception = new \Exception($throwable->getMessage(), $throwable->getCode(), $throwable);
            Mage::logException($exception);
            return [
                'error' => true,
                'code' => $exception->getCode(),
                'message' => 'An error occurred.',
            ];
        }
    }

    /**
     * @return array
     */
    protected function getMMExtract(): array
    {
        // Get all available process contexts
        $processContextsCollection = Mage::getModel('dyna_catalog/processContext')->getCollection();
        $processContexts = [];
        foreach ($processContextsCollection ?? [] as $processContextItem) {
            if (in_array($processContextItem->getCode(), ['ACQ', 'RETBLU', 'ILSBLU', 'MOVE'])) {
                $processContexts[$processContextItem->getId()] = $processContextItem->getCode();
            }
        }

        // Get mappings
        $importList = $missedProducts = [];
        $multiMapperRecords = Mage::getModel('multimapper/mapper')
            ->getCollection()
            ->addFieldToFilter('main_table.stack', $this->availableStacks);
        foreach ($multiMapperRecords ?? [] as $mapperRecord) {
            // Get package type
            $stack = strtolower($mapperRecord->getStack());
            if ($stack === self::FIXED_STACK) {
                $sku = $mapperRecord->getData('sku');
                $packageType = self::FIXED_PACKAGE_TYPE;
            } else {
                $sku = $mapperRecord->getData('pp_sku');
                $packageType = self::MOBILE_PACKAGE_TYPE;
            }

            // Get addons
            $addons = Mage::getModel('dyna_multimapper/addon')->getAddonByMapperId($mapperRecord->getId());
            foreach ($addons ?? [] as $addon) {
                // Prepare process contexts
                $processContext = [];
                foreach ($addon->getProcessContextIds() as $processContextId) {
                    $processContext[] = $processContexts[$processContextId] ?? null;
                }

                // Set import list
                $importList[] = [
                    'sku' => $sku,
                    'service_expression' => $mapperRecord->getServiceExpression(),
                    'priority' => $mapperRecord->getPriority(),
                    'stop_execution' => $mapperRecord->getStopExecution(),
                    'technicalID1' => $this->removeHardwareIndicator($addon->getTechnicalId()),
                    'natureCode1' => $addon->getNatureCode(),
                    'backend' => $addon->getBackend(),
                    'direction' => $addon->getDirection(),
                    'technicalID2' => $this->removeHardwareIndicator($addon->getTechnicalId2()),
                    'natureCode2' => $addon->getNatureCode2(),
                    'technicalID3' => $this->removeHardwareIndicator($addon->getTechnicalId3()),
                    'natureCode3' => $addon->getNatureCode3(),
                    'component_type' => $addon->getComponentType(),
                    'process_context' => implode(',', array_filter($processContext)),
                    'package_type' => $packageType,
                ];
            }
        }

        return $importList;
    }

    /**
     * Prepare catalog versions
     *
     * @return array
     */
    protected function getCatalogVersions(): array
    {
        $catalogVersions = [];
        $queryResult = Mage::getSingleton('core/resource')->getConnection('core_write')->query(
            sprintf(
                'SELECT * FROM catalog_import_log WHERE stack IN (%s) ORDER BY `date` DESC;',
                sprintf("'%s'", implode("','", $this->availableStacks))
            )
        );
        while ($row = $queryResult->fetch()) {
            $catalogVersions[] = [
                'stack' => strtoupper($row['stack']),
                'interfaceVersion' => $row['interface_version'],
                'referenceDataBuild' => $row['reference_data_build'],
                'date' => $row['date'],
                'author' => $row['author'],
            ];
        }

        return $catalogVersions;
    }

    /**
     * Remove PEAL hardware prefix "-"
     *
     * @param string|null $value
     * @return string|null
     */
    protected function removeHardwareIndicator(?string $value): ?string
    {
        return !empty($value) ? str_replace('-', '', $value) : null;
    }
}
