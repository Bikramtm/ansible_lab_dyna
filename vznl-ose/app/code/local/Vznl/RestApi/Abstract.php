<?php

abstract class Vznl_RestApi_Abstract
{
    /**
     * @var Mage_Core_Controller_Request_Http
     */
    protected $request;

    /**
     * @var array
     */
    protected $errors = array();

    /**
     * Mapping for all the errors on the API since the errors are thrown on the frontend names.
     *
     * @var array
     */
    protected $mapping = array(
        'customer\[firstname\]' => 'contracting_party.initials',
        'customer\[lastname\]' => 'contracting_party.last_name',
        'customer\[gender\]' => 'contracting_party.gender',
        'customer\[dob\]' => 'contracting_party.birthdate',
        'customer\[id_type\]'=> 'identity.identity_type',
        'customer\[id_number\]'=> 'identity.identity_number',
        'customer\[issuing_country\]'=> 'identity.nationality',
        'customer\[valid_until\]' => 'identity.id_expiry_date',
        'customer\[contractant_gender\]' => 'contracting_party.gender',
        'customer\[contractant_firstname\]' => 'contracting_party.initials',
        'customer\[contractant_lastname\]' => 'contracting_party.last_name',
        'customer\[contractant_dob\]' => 'contracting_party.birthdate',
        'customer\[contractant_id_type\]' => 'identity.identity_type',
        'customer\[contractant_id_number\]' => 'identity.identity_number',
        'customer\[contractant_valid_until\]' => 'identity.id_expiry_date',
        'customer\[contractant_issuing_country\]' => 'identity.nationality',
        'customer\[otherAddress\]\[company_street\]' => 'business_details.company.address.street',
        'customer\[otherAddress\]\[company_postcode\]' => 'business_details.company.address.postal_code',
        'customer\[otherAddress\]\[company_city\]' => 'business_details.company.address.city',
        'customer\[company_vat_id\]' => 'business_details.company.vat_number',
        'customer\[company_coc\]' => 'business_details.company.coc_number',
        'customer\[company_name\]' => 'business_details.company.name',
        'customer\[company_date\]' => 'business_details.company.establish_date',
        'customer\[company_legal_form\]' => 'business_details.company.legal_form',
        'address\[otherAddress\]\[street\]'=> 'contracting_party.invoice_address.street',
        'address\[otherAddress\]\[postcode\]'=> 'contracting_party.invoice_address.zipcode',
        'address\[otherAddress\]\[city\]'=> 'contracting_party.invoice_address.city',
        'address\[foreignAddress\]\[street\]'=> 'contracting_party.invoice_address.street',
        'address\[foreignAddress\]\[postcode\]'=> 'contracting_party.invoice_address.zipcode',
        'address\[foreignAddress\]\[city\]'=> 'contracting_party.invoice_address.city',
        'address\[foreignAddress\]\[houseno\]'=>'contracting_party.invoice_address.house_number',
        'address\[foreignAddress\]\[country_id\]'=>'contracting_party.invoice_address.country',
        'address\[account_no\]'=> 'payment_details.account_nr',
        'address\[account_holder\]'=> 'payment_details.account_name',
        'address\[telephone\]'=> 'contracting_party.phone_1',
        'address\[email\]'=> 'contracting_party.email',
        'portability\[.*\]\[mobile_number\]' => 'mobile_number_porting[%INDEX%].porting_data.current_msisdn',
        'portability\[.*\]\[validation_type\]' => 'mobile_number_porting[%INDEX%].porting_data.validation_type',
        'portability\[.*\]\[contract_number\]' => 'mobile_number_porting[%INDEX%].porting_data.contract_number',
        'additional\[email\]\[0\]' => 'contracting_party.email',
        'additional\[email\]\[1\]' => 'contracting_party.email',
        'additional\[email\]\[2\]' => 'contracting_party.email',
        'additional\[correspondence_email\]\[0\]' => 'contracting_party.correspondence_email',
        'family_type' => 'ilt.family_type',
        'income' => 'ilt.income',
        'housing_costs'=> 'ilt.housing_costs',
        'birth_lastname' => 'ilt.birth_lastname',
    );

    /**
     * Get the mapping for a given key.
     *
     * @param string $mapping
     * @return array|null
     */
    public function getMapping($mapping)
    {
        foreach($this->mapping as $key => $value) {
            if (preg_match('/'.$key.'/', $mapping)) {
                return $value;
            }
        }
        return null;
    }

    /**
     * Get the errors
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Get the Hawaii Sales Type for a shopping cart.
     *
     * @param string $saleType
     * @return string|null
     */
    public function getHawaiiSaleType($saleType)
    {
        switch ($saleType) {
            case Vznl_Checkout_Model_Sales_Quote_Item::ACQUISITION:
                return 'acquisition';
            case Vznl_Checkout_Model_Sales_Quote_Item::RETENTION:
                return 'retention';
            case Vznl_Checkout_Model_Sales_Quote_Item::INLIFE:
                return 'inlife';
            default:
                return  null;
        }
    }
    /**
     * Set already existing errors to the GET
     *
     * @param array $errors
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }

    /**
     * Register an error
     * @param string $key
     * @param string $message
     * @param int $index
     * @return Vznl_RestApi_Abstract
     */
    public function addError($key, $message = 'required', $index = null)
    {
        $this->errors[] = array('key'  => $key, 'message' => $message, 'index' => $index);
        return $this;
    }

    /**
     * Map the company legal form from UBUY to HAWAII
     * @param string $type
     * @return int|string
     */
    public function getCompanyLegalForm($type)
    {
        if (!is_numeric($type)) {
            switch ($type) {
                case '01': return 1;
                case '02': return 2;
                case '03': return 3;
                case '04': return 4;
                case '06': return 6;
                case '07': return 7;
                case '08': return 8;
                case '10': return 10;
                case '13': return 13;
                case '14': return 14;
            }
        } else {
            switch ($type) {
                case 1: return '01';
                case 2: return '02';
                case 3: return '03';
                case 4: return '04';
                case 6: return '06';
                case 7: return '07';
                case 8: return '08';
                case 10: return '10';
                case 13: return '13';
                case 14: return '14';
            }
        }
        return null;
    }

    /**
     * Get the country from the given country code
     * @param string $code
     * @return string|null
     */
    public function getCountryFromCode($code)
    {
        // TODO: Why is_null check here?
        if (is_null($code) || strlen($code) == 2) {
            switch ($code) {
                case 'NL':
                    return 'NLD';
                case 'BE':
                    return 'BEL';
                case 'DE':
                    return 'DEU';
            }
        } else {
            switch ($code) {
                case 'NLD':
                    return 'NL';
                case 'BEL':
                    return 'BE';
                case 'DEU':
                    return 'DE';
            }
        }
        return null;
    }

    /**
     * Get the timestamp (including MS) from a given date
     * @param string $date
     * @param boolean $microseconds
     * @return integer
     */
    protected function getTimestampFromDate($date, $updateTimezone = true, $microseconds = true)
    {
        if (is_null($date)) {
            return null;
        }

        $date = new DateTime($date);

        if ($updateTimezone) {
            $date->setTimezone(
                new DateTimeZone('Europe/Warsaw')
            );
        }

        if ($microseconds) {
            return (int) ($date->getTimestamp().'000');
        }

        return (int) $date->getTimestamp();
    }

    /**
     * Mapping to map the ID to a given type.
     *
     * @param string $value
     * @return string|null
     */
    protected function getIdType($value)
    {
        switch ($value) {
            case 'NL_ID_CARD':
                return 'N';
            case 'PASSPORT':
                return 'P';
            case 'NL_DRIVER_LICENSE':
            case 'NL_MOPED_DRIVERLICENSE':
                return 'R';
            case 'TYPE_EU':
                return '0';
            case 'TYPE_I':
                return '1';
            case 'TYPE_II':
                return '2';
            case 'TYPE_III':
                return '3';
            case 'TYPE_IV':
                return '4';
            // Other way around
            case 'N':
                return 'NL_ID_CARD';
            case 'P':
                return 'PASSPORT';
            case 'R':
                return 'NL_DRIVER_LICENSE';
            case '0':
                return 'TYPE_EU';
            case '1':
                return 'TYPE_I';
            case '2':
                return 'TYPE_II';
            case '3':
                return 'TYPE_III';
            case '4':
                return 'TYPE_IV';
        }
        return null;
    }

    /**
     * Convert the gender
     *
     * @param string $gender
     * @return int|null
     */
    protected function getGender($gender)
    {
        if (is_numeric($gender)) {
            switch ($gender) {
                case 1:
                    return 'male';
                case 2:
                    return 'female';
            }
        } else {
            switch (strtolower($gender)) {
                case 'male':
                    return 1;
                case 'female':
                    return 2;
            }
        }

        return null;
    }

    /**
     * Get the date string from a given timestamp
     * @param integer $timestamp
     * @param string $format
	 * @throws Exception
     * @return string|null
     */
    protected function getDateFromTimestamp($timestamp=null, $updateTimezone=true, $format='d-m-Y')
    {
        if (is_null($timestamp) || empty($timestamp) && !is_numeric($timestamp)) {
            return null;
        }
        try {
            // Microseconds to seconds
            $timestamp = (int) substr($timestamp, 0, -3);
            $date = new DateTime('@'.$timestamp);
            // Resolved timezone
            if ($updateTimezone) {
                $date->setTimezone(new DateTimeZone('Europe/Warsaw'));
            }
            // Add configured amount of settings to fix PHP date resolve object, only for default date usage
            if ($format == 'd-m-Y') {
                $addSecondsAmount = abs((int)trim(Mage::getStoreConfig('vodafone_customer/api_birthdate_countin/add_seconds')));
                if ($addSecondsAmount > 0){
                    $date->add(new DateInterval('PT' . $addSecondsAmount . 'S'));
                }
            }
            return $date->format($format);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Get the request object
     * @return Mage_Core_Controller_Request_Http
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * Set the request object
     * @return Vznl_RestApi_Abstract
     */
    public function setRequest(Mage_Core_Controller_Request_Http $request)
    {
        $this->request = $request;
        return $this;
    }
}
