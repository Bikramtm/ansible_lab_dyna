<?php

class Vznl_RestApi_Catalogue_SalesRuleInterpreter
{
    protected $salesRuleConditions;
    protected $salesRuleActions;
    protected $data = array();

    public $lifecycle = array();

    public $lifecycleDefaults = [
        Vznl_Checkout_Model_Sales_Quote_Item::ACQUISITION,
        Vznl_Checkout_Model_Sales_Quote_Item::RETENTION
    ]; // Default all

    public $combineRule;
    public $ruleOrder = array(
        'salesrule/rule_condition_combine',
        'pricerules/condition_lifecycle',
        'dyna_pricerules/condition_processContext',
        'pricerules/condition_value',
        'pricerules/condition_customersegment',
        'pricerules/condition_campaign',
        'salesrule/rule_condition_product_found',
        'vznl_pricerules/condition_packageType',
    );

    /**
     * Interpret the given sales rule
     * @param Mage_SalesRule_Model_Rule $salesRule
     * @return array
     * @throws Exception
     */
    public function interpret(Mage_SalesRule_Model_Rule $salesRule)
    {
        $this->salesRuleConditions = array(unserialize($salesRule->getConditionsSerialized()));
        //$this->salesRuleActions = array(unserialize($salesRule->getActionsSerialized()));

        try {
            $this->renderConditions();
            //$this->renderActions();
            return $this->data;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Render all the conditions
     * @return boolean
     */
    protected function renderConditions()
    {
        return !$this->renderCondition(array($this->salesRuleConditions[0]));
    }

    /**
     * Render all the actions
     * @return boolean
     */
    protected function renderActions()
    {
        return !$this->renderCondition(array($this->salesRuleActions[0]));
    }

    /**
     * Get the lifecycle from the condition
     */
    protected function retrieveLifecycle()
    {
        foreach ($this->salesRuleConditions as $condition) {
            $this->renderCondition($condition, 'pricerules/condition_lifecycle');
        }
    }

    /**
     * Order the conditions to make sure they are handled in the correct order since they are
     * dependant from eachother
     * @param array $conditions
     * @return array
     */
    protected function reorderConditionFlow($conditions)
    {
        usort($conditions, function($a, $b) {
            $keyA = array_keys($this->ruleOrder, $a['type'])[0];
            $keyB = array_keys($this->ruleOrder, $b['type'])[0];
            if ($keyA == $keyB) {
                return 0;
            } elseif ($keyA < $keyB) {
                return -1;
            } else {
                return 1;
            }
        });
        return $conditions;
    }

    /**
     * Render all conditions and actions using recursion.
     *
     * @param array $conditionCollection
     * @return boolean
     * @throws Exception
     */
    protected function renderCondition($conditionCollection)
    {
        foreach ($conditionCollection as $condition) {
            switch ($condition['type']) {
                case 'pricerules/condition_lifecycle':
                    $this->lifecycle[] = $condition['value'];
                    break;
                case 'dyna_pricerules/condition_processContext':
                    if (is_array($condition['value'])) {
                        foreach ($condition['value'] as $value) {
                            switch ($value) {
                                case 0: // ACQ
                                    $this->lifecycle[] = Vznl_Checkout_Model_Sales_Quote_Item::ACQUISITION;
                                    break;
                                case 3: // RETBLU
                                    $this->lifecycle[] = Vznl_Checkout_Model_Sales_Quote_Item::RETENTION;
                                    break;
                            }
                        }
                    }
                    break;
                case 'salesrule/rule_condition_combine':
                case 'salesrule/rule_condition_product_combine':
                    $this->combineRule = $condition['aggregator'];
                    break;
                case 'pricerules/condition_value':
                    $attribute = Mage::getModel('catalog/resource_eav_attribute')->load('customer_value', 'attribute_code');
                    $lifecycle = empty($this->lifecycle) ? $this->lifecycleDefaults : $this->lifecycle;
                    foreach ($attribute->getSource()->getAllOptions(false) as $option) {
                        if ($option['value'] == $condition['value']) {
                           // $lifecycle = count($this->lifecycle) == 0 ? $this->lifecycleDefaults : $this->lifecycle;
                            foreach ($lifecycle as $key => $lifecycle) {
                                if ($this->combineRule === 'any') {
                                    $this->data[$lifecycle]['customer_value'][] = $option['label'];
                                } else {
                                    $this->data[$lifecycle]['customer_value'] .= !empty($this->data[$lifecycle]['customer_value']) ? ',' : '';
                                    $this->data[$lifecycle]['customer_value'] .= $option['label'];
                                }
                            }
                        }
                    }
                    break;
                case 'pricerules/condition_customersegment':
                    $attribute = Mage::getModel('catalog/resource_eav_attribute')->load('segment', 'attribute_code');
                    $lifecycle = empty($this->lifecycle) ? $this->lifecycleDefaults : $this->lifecycle;
                    foreach ($attribute->getSource()->getAllOptions(false) as $option) {
                        if ($option['value'] == $condition['value']) {
                           // $lifecycle = count($this->lifecycle) == 0 ? $this->lifecycleDefaults : $this->lifecycle;
                            foreach ($lifecycle as $key => $lifecycle) {
                                if ($this->combineRule !== 'all' && ($this->combineRule === 'any' || $condition['operator'] == '()')) {
                                    $this->data[$lifecycle]['value_segment'][] = $option['label'];
                                } else {
                                    $this->data[$lifecycle]['value_segment'] .= !empty($this->data[$lifecycle]['value_segment']) ? ',' : '';
                                    $this->data[$lifecycle]['value_segment'] .= $option['label'];
                                }
                            }
                        }
                    }
                    break;
                case 'pricerules/condition_campaign':
                    $attribute = Mage::getModel('catalog/resource_eav_attribute')->load('campaign', 'attribute_code');
                    $lifecycle = empty($this->lifecycle) ? $this->lifecycleDefaults : $this->lifecycle;
                    foreach ($attribute->getSource()->getAllOptions(false) as $option) {
                        if ($option['value'] == $condition['value']) {
                    //        $lifecycle = count($this->lifecycle) == 0 ? $this->lifecycleDefaults : $this->lifecycle;
                            foreach ($lifecycle as $key => $lifecycle) {
                                if ($this->combineRule !== 'all' && ($this->combineRule === 'any' || $condition['operator'] == '()')) {
                                    $this->data[$lifecycle]['campaign'][] = $option['label'];
                                } else {
                                    $this->data[$lifecycle]['campaign'] .= !empty($this->data[$lifecycle]['campaign']) ? ',' : '';
                                    $this->data[$lifecycle]['campaign'] .= $option['label'];
                                }
                            }
                        }
                    }
                    break;
                case 'vznl_pricerules/condition_packageType':
                    $packageTypes = Mage::getModel('vznl_package/packageType')->getCollection();
                    $lifecycles = empty($this->lifecycle) ? $this->lifecycleDefaults : $this->lifecycle;
                    /** @var Vznl_Package_Model_PackageType $packageType */
                    foreach ($packageTypes as $packageType) {
                        if (in_array($packageType->getId(), $condition['value'])) {
                            foreach ($lifecycles as $lifecycle) {
                                if ($this->combineRule !== 'all' && ($this->combineRule === 'any' || $condition['operator'] == '()')) {
                                    $this->data[$lifecycle]['package_type'][] = $packageType->getPackageCode();
                                } else {
                                    $this->data[$lifecycle]['package_type'] .= !empty($this->data[$lifecycle]['package_type']) ? ',' : '';
                                    $this->data[$lifecycle]['package_type'] .= $packageType->getPackageCode();
                                }
                            }
                        }
                    }
                    break;
                case 'salesrule/rule_condition_product_found':
                    $this->combineRule = $condition['aggregator'];
                    break;
                case 'salesrule/rule_condition_product':
                    switch ($condition['attribute']) {
                        case 'attribute_set_id':
                            $productCollection = Mage::getModel('catalog/product')
                                    ->getCollection()
                                    ->addAttributeToSelect('*')
                                    ->addFieldToFilter('attribute_set_id', $condition['value'])
                                    ->addAttributeToFilter('type_id', array('eq' => 'simple'))
                                    ->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))
                                    ->addStoreFilter();
                            $lifecycle = empty($this->lifecycle) ? $this->lifecycleDefaults : $this->lifecycle;
                            foreach ($productCollection as $product) {
                            //    $lifecycle = count($this->lifecycle) == 0 ? $this->lifecycleDefaults : $this->lifecycle;
                                foreach ($lifecycle as $key => $lifecycle) {
                                    if ($this->combineRule !== 'all' && ($this->combineRule === 'any' || $condition['operator'] == '()')) {
                                        $this->data[$lifecycle]['products'][] = $product->getSku();
                                    } else {
                                        $products = is_array($this->data[$lifecycle]['products']) ? $this->data[$lifecycle]['products'] : array($this->data[$lifecycle]['products']);
                                        foreach ($products as $key => $productSku) {
                                            $this->data[$lifecycle]['products'][$key] .= !empty($this->data[$lifecycle]['products'][$key]) ? ',' : '';
                                            $this->data[$lifecycle]['products'][$key] .= $product->getSku();
                                        }
                                    }
                                }
                            }
                            break;
                        case 'sku':
                            $parts = explode(',', $condition['value']);
                            $lifecycle = empty($this->lifecycle) ? $this->lifecycleDefaults : $this->lifecycle;
                            foreach ($parts as $index => $sku) {
                                $sku = trim($sku);
                            //    $lifecycle = count($this->lifecycle) == 0 ? $this->lifecycleDefaults : $this->lifecycle;
                                foreach ($lifecycle as $key => $lifecycle) {
                                    if ($this->combineRule !== 'all' && ($this->combineRule === 'any' || $condition['operator'] == '()')) {
                                        $this->data[$lifecycle]['products'][] = $sku;
                                    } else {
                                        $products = is_array($this->data[$lifecycle]['products']) ? $this->data[$lifecycle]['products'] : array($this->data[$lifecycle]['products']);
                                        foreach ($products as $key => $productSku) {
                                            $this->data[$lifecycle]['products'][$key] .= !empty($this->data[$lifecycle]['products'][$key]) ? ',' : '';
                                            $this->data[$lifecycle]['products'][$key] .= $sku;
                                        }
                                    }
                                }
                            }
                            break;
                        default:
                            throw new Exception('salesrule/rule_condition_product '.$condition['attribute'].' not supported');
                    }
                    break;
                default:
                    throw new Exception('Type not found: '.$condition['type']);
            }
        }

        foreach ($conditionCollection as $condition) {
            // Render sub conditions
            if (!empty($condition['conditions'])) {
                $conditionCollection = $this->reorderConditionFlow($condition['conditions']);
                $this->renderCondition($conditionCollection);
            }
        }
        return true;
    }
}
