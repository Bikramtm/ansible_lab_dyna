<?php

/**
 * Class Vznl_RestApi_Catalogue_Helper_FixedParser
 */
class Vznl_RestApi_Catalogue_Helper_FixedParser extends Vznl_RestApi_Catalogue_Helper_Parser
{
    public const FIXED_BUNDLE_ATTRIBUTE_SET = 59;
    public const FIXED_TV_ATTRIBUTE_SET = 60;
    public const FIXED_INTERNET_ATTRIBUTE_SET = 61;
    public const FIXED_TEL_ATTRIBUTE_SET = 62;
    public const FIXED_GENERIC_ATTRIBUTE_SET = 63;
    public const FIXED_PROMO_ATTRIBUTE_SET = 64;

    /**
     * @var Mage_Catalog_Model_Resource_Product_Collection
     */
    protected $fixedBundleProducts;

    /**
     * @var Mage_Catalog_Model_Resource_Product_Collection
     */
    protected $fixedTvProducts;

    /**
     * @var Mage_Catalog_Model_Resource_Product_Collection
     */
    protected $fixedInternetProducts;

    /**
     * @var Mage_Catalog_Model_Resource_Product_Collection
     */
    protected $fixedTelProducts;

    /**
     * @var Mage_Catalog_Model_Resource_Product_Collection
     */
    protected $fixedGenericProducts;

    /**
     * @var Mage_Catalog_Model_Resource_Product_Collection
     */
    protected $fixedPromotions;

    /**
     * @var array
     */
    protected $bundleProductAttributes = [
        'fixed_int_part',
        'fixed_tel_part',
        'fixed_tv_part',
        'has_dtv',
        'has_internet',
        'has_fixed_tel',
        'payment_method',
        'portfolioidentifier',
        'product_status_online',
        'requires_serviceability',
        'required_serviceability_items',
        'download_speed',
        'upload_speed',
        'tv_box',
    ];

    /**
     * @var array
     */
    protected $tvProductAttributes = [
        'belongs_to_family',
        'payment_method',
        'portfolioidentifier',
        'product_status_online',
        'requires_serviceability',
        'required_serviceability_items',
        'tv_product',
        'tv_box',
    ];

    /**
     * @var array
     */
    protected $intProductAttributes = [
        'belongs_to_family',
        'payment_method',
        'portfolioidentifier',
        'product_status_online',
        'requires_serviceability',
        'required_serviceability_items',
        'download_speed',
        'upload_speed',
    ];

    /**
     * @var array
     */
    protected $telProductAttributes = [
        'belongs_to_family',
        'payment_method',
        'portfolioidentifier',
        'product_status_online',
        'requires_serviceability',
        'required_serviceability_items',
    ];

    /**
     * @var array
     */
    protected $genProductAttributes = [
        'belongs_to_family',
        'portfolioidentifier',
        'product_status_online',
    ];

    /**
     * @var array
     */
    protected $promotionProductAttributes = [
        'belongs_to_family',
        'discount_period',
        'discount_period_amount',
        'hide_original_price',
        'marketing',
        'payment_method',
        'portfolioidentifier',
        'product_status_online',
        'promotionType',
    ];

    /**
     * Set all fixed products
     */
    protected function loadProducts()
    {
        $this->fixedBundleProducts = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToSelect('*')
            ->addFieldToFilter('attribute_set_id', self::FIXED_BUNDLE_ATTRIBUTE_SET)
            ->addAttributeToFilter('type_id', ['eq' => 'simple'])
            ->addAttributeToFilter('is_deleted', ['eq' => false])
            ->addAttributeToFilter('status', ['eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED])
            ->addStoreFilter();

        $this->fixedTvProducts = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToSelect('*')
            ->addFieldToFilter('attribute_set_id', self::FIXED_TV_ATTRIBUTE_SET)
            ->addAttributeToFilter('type_id', ['eq' => 'simple'])
            ->addAttributeToFilter('is_deleted', ['eq' => false])
            ->addAttributeToFilter('status', ['eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED])
            ->addStoreFilter();

        $this->fixedInternetProducts = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToSelect('*')
            ->addFieldToFilter('attribute_set_id', self::FIXED_INTERNET_ATTRIBUTE_SET)
            ->addAttributeToFilter('type_id', ['eq' => 'simple'])
            ->addAttributeToFilter('is_deleted', ['eq' => 0])
            ->addAttributeToFilter('status', ['eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED])
            ->addStoreFilter();

        $this->fixedTelProducts = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToSelect('*')
            ->addFieldToFilter('attribute_set_id', self::FIXED_TEL_ATTRIBUTE_SET)
            ->addAttributeToFilter('type_id', ['eq' => 'simple'])
            ->addAttributeToFilter('is_deleted', ['eq' => false])
            ->addAttributeToFilter('status', ['eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED])
            ->addStoreFilter();

        $this->fixedGenericProducts = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToSelect('*')
            ->addFieldToFilter('attribute_set_id', self::FIXED_GENERIC_ATTRIBUTE_SET)
            ->addAttributeToFilter('type_id', ['eq' => 'simple'])
            ->addAttributeToFilter('is_deleted', ['eq' => false])
            ->addAttributeToFilter('status', ['eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED])
            ->addStoreFilter();

        $this->fixedPromotions = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToSelect('*')
            ->addFieldToFilter('attribute_set_id', self::FIXED_PROMO_ATTRIBUTE_SET)
            ->addAttributeToFilter('type_id', ['eq' => 'simple'])
            ->addAttributeToFilter('is_deleted', ['eq' => false])
            ->addAttributeToFilter('status', ['eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED])
            ->addStoreFilter();
    }

    /**
     * @return array
     */
    public function getProductsWithAttributeSet(): array
    {
        $this->loadProducts();

        return [
            [
                'products' => $this->fixedBundleProducts,
                'attributes' => $this->bundleProductAttributes,
            ],
            [
                'products' => $this->fixedTvProducts,
                'attributes' => $this->tvProductAttributes,
            ],
            [
                'products' => $this->fixedInternetProducts,
                'attributes' => $this->intProductAttributes,
            ],
            [
                'products' => $this->fixedTelProducts,
                'attributes' => $this->telProductAttributes,
            ],
            [
                'products' => $this->fixedGenericProducts,
                'attributes' => $this->genProductAttributes,
            ],
            [
                'products' => $this->fixedPromotions,
                'attributes' => $this->promotionProductAttributes,
            ],
        ];
    }
}
