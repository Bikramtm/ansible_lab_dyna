<?php

class Vznl_RestApi_Catalogue_Helper_SalesRuleInterpreter
{
    protected $salesRuleConditions;
    protected $salesRuleActions;
    protected $conditionsData = [];
    protected $actionsData = [];

    public $lifecycle = [];

    public $lifecycleDefaults = [
        Vznl_Checkout_Model_Sales_Quote_Item::ACQUISITION,
        Vznl_Checkout_Model_Sales_Quote_Item::RETENTION
    ]; // Default all

    public $combineRule;
    public $ruleOrder = [
        'salesrule/rule_condition_combine',
        'salesrule/rule_condition_product_combine',
        'salesrule/rule_condition_product_found',
        'salesrule/rule_condition_address',
        'pricerules/condition_lifecycle',
        'dyna_pricerules/condition_processContext',
        'pricerules/condition_value',
        'pricerules/condition_customersegment',
        'pricerules/condition_campaign',
        'pricerules/condition_dealergroup',
        'vznl_pricerules/condition_packageType',
    ];

    /**
     * Interpret the given sales rule conditions
     *
     * @param Mage_SalesRule_Model_Rule $salesRule
     * @return array
     * @throws Exception
     */
    public function interpretConditions(Mage_SalesRule_Model_Rule $salesRule)
    {
        $this->salesRuleConditions = [unserialize($salesRule->getConditionsSerialized())];

        try {
            $this->renderConditions();
            return $this->conditionsData;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Interpret the given sales rule actions
     *
     * @param Mage_SalesRule_Model_Rule $salesRule
     * @return array
     * @throws Exception
     */
    public function interpretActions(Mage_SalesRule_Model_Rule $salesRule)
    {
        $this->salesRuleActions = [unserialize($salesRule->getActionsSerialized())];

        try {
            $this->renderActions();
            return $this->actionsData;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Render all the conditions
     *
     * @return bool
     * @throws Exception
     */
    protected function renderConditions()
    {
        return !$this->renderCondition([$this->salesRuleConditions[0]]);
    }

    /**
     * Render all the actions
     *
     * @return bool
     * @throws Exception
     */
    protected function renderActions()
    {
        return !$this->renderCondition([$this->salesRuleActions[0]], 'actions');
    }

    /**
     * Order the conditions to make sure they are handled in the correct order since they are dependant from each other
     *
     * @param array $conditions
     * @return array
     */
    protected function reorderConditionFlow($conditions)
    {
        usort($conditions, function ($a, $b) {
            $keyA = array_keys($this->ruleOrder, $a['type'])[0];
            $keyB = array_keys($this->ruleOrder, $b['type'])[0];
            if ($keyA == $keyB) {
                return 0;
            } elseif ($keyA < $keyB) {
                return -1;
            } else {
                return 1;
            }
        });
        return $conditions;
    }

    /**
     * Render all conditions and actions using recursion.
     *
     * @param array $conditionCollection
     * @param string $type
     * @return bool
     * @throws Exception
     */
    protected function renderCondition($conditionCollection, $type = 'conditions')
    {
        foreach ($conditionCollection as $condition) {
            switch ($condition['type']) {
                case 'pricerules/condition_lifecycle':
                    $this->lifecycle[] = $condition['value'];
                    break;
                case 'dyna_pricerules/condition_processContext':
                    if (is_array($condition['value'])) {
                        foreach ($condition['value'] as $value) {
                            switch ($value) {
                                case 0: // ACQ
                                    $this->lifecycle[] = Vznl_Checkout_Model_Sales_Quote_Item::ACQUISITION;
                                    break;
                                case 3: // RETBLU
                                    $this->lifecycle[] = Vznl_Checkout_Model_Sales_Quote_Item::RETENTION;
                                    break;
                            }
                        }
                    }
                    break;
                case 'salesrule/rule_condition_combine':
                case 'salesrule/rule_condition_product_combine':
                    $this->combineRule = $condition['aggregator'];
                    break;
                case 'pricerules/condition_value':
                    $attribute = Mage::getModel('catalog/resource_eav_attribute')
                        ->load('customer_value', 'attribute_code');
                    $lifecycles = empty($this->lifecycle) ? $this->lifecycleDefaults : $this->lifecycle;
                    foreach ($attribute->getSource()->getAllOptions(false) as $option) {
                        if ($option['value'] == $condition['value']) {
                            foreach ($lifecycles as $lifecycle) {
                                if ($this->combineRule === 'any') {
                                    $this->{$type . 'Data'}[$lifecycle]['customer_value'][] = $option['label'];
                                } else {
                                    $this->{$type . 'Data'}[$lifecycle]['customer_value'] .= !empty($this->{$type . 'Data'}[$lifecycle]['customer_value']) ? ',' : '';
                                    $this->{$type . 'Data'}[$lifecycle]['customer_value'] .= $option['label'];
                                }
                            }
                        }
                    }
                    break;
                case 'pricerules/condition_customersegment':
                    $attribute = Mage::getModel('catalog/resource_eav_attribute')->load('segment', 'attribute_code');
                    $lifecycles = empty($this->lifecycle) ? $this->lifecycleDefaults : $this->lifecycle;
                    foreach ($attribute->getSource()->getAllOptions(false) as $option) {
                        if ($option['value'] == $condition['value']) {
                            foreach ($lifecycles as $lifecycle) {
                                if ($this->combineRule !== 'all' && ($this->combineRule === 'any' || $condition['operator'] == '()')) {
                                    $this->{$type . 'Data'}[$lifecycle]['value_segment'][] = $option['label'];
                                } else {
                                    $this->{$type . 'Data'}[$lifecycle]['value_segment'] .= !empty($this->{$type . 'Data'}[$lifecycle]['value_segment']) ? ',' : '';
                                    $this->{$type . 'Data'}[$lifecycle]['value_segment'] .= $option['label'];
                                }
                            }
                        }
                    }
                    break;
                case 'pricerules/condition_campaign':
                    $attribute = Mage::getModel('catalog/resource_eav_attribute')->load('campaign', 'attribute_code');
                    $lifecycles = empty($this->lifecycle) ? $this->lifecycleDefaults : $this->lifecycle;
                    foreach ($attribute->getSource()->getAllOptions(false) as $option) {
                        if ($option['value'] == $condition['value']) {
                            foreach ($lifecycles as $lifecycle) {
                                if ($this->combineRule !== 'all' && ($this->combineRule === 'any' || $condition['operator'] == '()')) {
                                    $this->{$type . 'Data'}[$lifecycle]['campaign'][] = $option['label'];
                                } else {
                                    $this->{$type . 'Data'}[$lifecycle]['campaign'] .= !empty($this->{$type . 'Data'}[$lifecycle]['campaign']) ? ',' : '';
                                    $this->{$type . 'Data'}[$lifecycle]['campaign'] .= $option['label'];
                                }
                            }
                        }
                    }
                    break;
                case 'pricerules/condition_dealergroup':
                    $dealerGroups = Mage::getModel('agent/dealergroup')
                        ->getCollection()
                        ->addFieldToFilter('group_id', ['in' => $condition['value']]);
                    $lifecycles = empty($this->lifecycle) ? $this->lifecycleDefaults : $this->lifecycle;
                    /** @var Dyna_Agent_Model_Dealergroup $dealerGroup */
                    foreach ($dealerGroups as $dealerGroup) {
                        foreach ($lifecycles as $lifecycle) {
                            if ($this->combineRule !== 'all' && ($this->combineRule === 'any' || $condition['operator'] == '()')) {
                                $this->{$type . 'Data'}[$lifecycle]['dealer_group'][] = $dealerGroup->getName();
                            } else {
                                $this->{$type . 'Data'}[$lifecycle]['dealer_group'] .= !empty($this->{$type . 'Data'}[$lifecycle]['dealer_group']) ? ',' : '';
                                $this->{$type . 'Data'}[$lifecycle]['dealer_group'] .= $dealerGroup->getName();
                            }
                        }
                    }
                    break;
                case 'salesrule/rule_condition_address':
                    $lifecycles = empty($this->lifecycle) ? $this->lifecycleDefaults : $this->lifecycle;
                    /** @var Vznl_Package_Model_PackageType $packageType */
                    foreach (explode(',', $condition['value']) as $postCode) {
                        $postCode = trim($postCode);
                        foreach ($lifecycles as $lifecycle) {
                            if ($this->combineRule !== 'all' && ($this->combineRule === 'any' || $condition['operator'] == '()')) {
                                $this->{$type . 'Data'}[$lifecycle]['post_codes'][] = $postCode;
                            } else {
                                $this->{$type . 'Data'}[$lifecycle]['post_codes'] .= !empty($this->{$type . 'Data'}[$lifecycle]['post_codes']) ? ',' : '';
                                $this->{$type . 'Data'}[$lifecycle]['post_codes'] .= $postCode;
                            }
                        }
                    }
                    break;
                case 'vznl_pricerules/condition_packageType':
                    $packageTypes = Mage::getModel('vznl_package/packageType')->getCollection();
                    $lifecycles = empty($this->lifecycle) ? $this->lifecycleDefaults : $this->lifecycle;
                    /** @var Vznl_Package_Model_PackageType $packageType */
                    foreach ($packageTypes as $packageType) {
                        if (in_array($packageType->getId(), $condition['value'])) {
                            foreach ($lifecycles as $lifecycle) {
                                if ($this->combineRule !== 'all' && ($this->combineRule === 'any' || $condition['operator'] == '()')) {
                                    $this->{$type . 'Data'}[$lifecycle]['package_type'][] = $packageType->getPackageCode();
                                } else {
                                    $this->{$type . 'Data'}[$lifecycle]['package_type'] .= !empty($this->{$type . 'Data'}[$lifecycle]['package_type']) ? ',' : '';
                                    $this->{$type . 'Data'}[$lifecycle]['package_type'] .= $packageType->getPackageCode();
                                }
                            }
                        }
                    }
                    break;
                case 'salesrule/rule_condition_product_found':
                    $this->combineRule = $condition['aggregator'];
                    break;
                case 'salesrule/rule_condition_product':
                    $lifecycles = empty($this->lifecycle) ? $this->lifecycleDefaults : $this->lifecycle;
                    switch ($condition['attribute']) {
                        case 'attribute_set_id':
                            $productCollection = Mage::getModel('catalog/product')
                                ->getCollection()
                                ->addAttributeToSelect('*')
                                ->addFieldToFilter('attribute_set_id', $condition['value'])
                                ->addAttributeToFilter('type_id', ['eq' => 'simple'])
                                ->addAttributeToFilter('status', [
                                    'eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED
                                ])
                                ->addStoreFilter();
                            foreach ($productCollection as $product) {
                                foreach ($lifecycles as $lifecycle) {
                                    if ($this->combineRule !== 'all' && ($this->combineRule === 'any' || $condition['operator'] == '()')) {
                                        $this->{$type . 'Data'}[$lifecycle]['products'][] = $product->getSku();
                                    } else {
                                        $products = is_array($this->{$type . 'Data'}[$lifecycle]['products']) ? $this->{$type . 'Data'}[$lifecycle]['products'] : [$this->{$type . 'Data'}[$lifecycle]['products']];
                                        foreach ($products as $key => $productSku) {
                                            $this->{$type . 'Data'}[$lifecycle]['products'][$key] .= !empty($this->{$type . 'Data'}[$lifecycle]['products'][$key]) ? ',' : '';
                                            $this->{$type . 'Data'}[$lifecycle]['products'][$key] .= $product->getSku();
                                        }
                                    }
                                }
                            }
                            break;
                        case 'sku':
                            $parts = explode(',', $condition['value']);
                            foreach ($parts as $index => $sku) {
                                $sku = trim($sku);
                                foreach ($lifecycles as $lifecycle) {
                                    if ($this->combineRule !== 'all' && ($this->combineRule === 'any' || $condition['operator'] == '()')) {
                                        $this->{$type . 'Data'}[$lifecycle]['products'][] = $sku;
                                        $this->{$type . 'Data'}[$lifecycle]['products_categories'][] = $sku;
                                    } else {
                                        $products = is_array($this->{$type . 'Data'}[$lifecycle]['products']) ? $this->{$type . 'Data'}[$lifecycle]['products'] : [$this->{$type . 'Data'}[$lifecycle]['products']];
                                        foreach ($products as $key => $productSku) {
                                            $this->{$type . 'Data'}[$lifecycle]['products'][$key] .= !empty($this->{$type . 'Data'}[$lifecycle]['products'][$key]) ? ',' : '';
                                            $this->{$type . 'Data'}[$lifecycle]['products'][$key] .= $sku;
                                            $this->{$type . 'Data'}[$lifecycle]['products_categories'][$key] .= !empty($this->{$type . 'Data'}[$lifecycle]['products_categories'][$key]) ? ',' : '';
                                            $this->{$type . 'Data'}[$lifecycle]['products_categories'][$key] .= $sku;
                                        }
                                    }
                                }
                            }
                            break;
                        case 'category_ids':
                            $parts = explode(',', $condition['value']);
                            foreach ($parts as $categoryId) {
                                $category = Mage::getModel('catalog/category')->load(trim($categoryId));
                                foreach ($lifecycles as $lifecycle) {
                                    if ($this->combineRule !== 'all' && ($this->combineRule === 'any' || $condition['operator'] == '()')) {
                                        $this->{$type . 'Data'}[$lifecycle]['categories'][] = $category->getUniqueId();
                                        $this->{$type . 'Data'}[$lifecycle]['products_categories'][] = $category->getUniqueId();
                                    } else {
                                        $categories = is_array($this->{$type . 'Data'}[$lifecycle]['categories']) ? $this->{$type . 'Data'}[$lifecycle]['categories'] : [$this->{$type . 'Data'}[$lifecycle]['categories']];
                                        foreach ($categories as $key => $id) {
                                            $this->{$type . 'Data'}[$lifecycle]['categories'][$key] .= !empty($this->{$type . 'Data'}[$lifecycle]['categories'][$key]) ? ',' : '';
                                            $this->{$type . 'Data'}[$lifecycle]['categories'][$key] .= $category->getUniqueId();
                                            $this->{$type . 'Data'}[$lifecycle]['products_categories'][$key] .= !empty($this->{$type . 'Data'}[$lifecycle]['products_categories'][$key]) ? ',' : '';
                                            $this->{$type . 'Data'}[$lifecycle]['products_categories'][$key] .= $category->getUniqueId();
                                        }
                                    }
                                }
                            }
                            break;
                        default:
                            throw new Exception('salesrule/rule_condition_product ' . $condition['attribute'] . ' not supported');
                    }
                    break;
                default:
                    throw new Exception('Type not found: ' . $condition['type']);
            }
        }

        foreach ($conditionCollection as $condition) {
            // Render sub conditions
            if (!empty($condition['conditions'])) {
                $conditionCollection = $this->reorderConditionFlow($condition['conditions']);
                $this->renderCondition($conditionCollection, $type);
            }
        }
        return true;
    }
}
