<?php

/**
 * Class Vznl_Superorder_Model_Superorder
 */
class Vznl_Superorder_Model_Superorder extends Dyna_Superorder_Model_Superorder
{
    const SO_ADDITIONAL_INFO_REQUIRED = 'Additional info required';
    const SO_VALIDATED_PARTIALLY = 'Validation Partially Approved';
    const SO_FULFILLMENT_IN_PROGRESS = 'Fulfillment in progress';
    const SO_ENRICH = 'EnrichOrder';
    const SO_TYPE_CREATE = 'Create';
    const SO_CHARGE_TYPE_HIGHER = 'Higher';
    const SO_CHARGE_TYPE_LOWER = 'Lower';
    const SO_CHARGET_YPE_EQUAL = 'Equal';
    const SO_PRE_IN_INITIAL = 'In initial';
    protected $onlyFinal;

    /**
     * Gets the delivery orders belonging to this super order.
     * @return Vznl_Checkout_Model_Sales_Order[] The delivery orders.
     */
    public function getDeliveryOrders($active=false){
        if($active) {
            $deliveryOrders = Mage::getModel('sales/order')
                ->getCollection()
                ->addAttributeToFilter('edited', false)
                ->addAttributeToFilter('superorder_id', $this->getEntityId());
        } else {
            $deliveryOrders = Mage::getModel('sales/order')
                ->getCollection()
                ->addAttributeToFilter('superorder_id', $this->getEntityId());
        }

        return $deliveryOrders;
    }

    /**
     * Check if saleorder has a delivery order that requires LOAN
     * @return bool
     */
    public function isLoanRequired() : bool
    {
        if(!is_null($this->getCustomer())&& !(bool)$this->getCustomer()->getIsBusiness()) {
            foreach($this->getDeliveryOrders(true) as $deliveryOrder){
                if($deliveryOrder->isLoanRequired()){
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Check if saleorder has a delivery order that requires ILT
     * @return bool
     */
    public function isIltRequired() : bool
    {
        if(!is_null($this->getCustomer())&& !(bool)$this->getCustomer()->getIsBusiness()) {
            foreach($this->getDeliveryOrders(true) as $deliveryOrder){
                if($deliveryOrder->isIltRequired()){
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @param $orderNumber
     * @return mixed
     *
     * Returns a superorder based on order number (which is unique)
     */
    public function getSuperOrderByOrderNumber($orderNumber)
    {
        return $this->getCollection()->addFieldToFilter('order_number', $orderNumber)->setPageSize(1)->getLastItem();
    }

    /**
     * Reads all the packages of an order and checks if there are packages that still need credit check
     */
    public function creditCheckCompleted()
    {
        $count = 0;
        $packages = $this->getPackages();
        $flippedCreditCheckStatuses = array_flip([Vznl_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_CC_APPROVED, Vznl_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_CC_PARTIAL]);
        foreach ($packages as $package) {
            $count++;
            $creditCheckStatus = $package->getCreditcheckStatus();
            if (!$package->isFixed() && !isset($flippedCreditCheckStatuses[$creditCheckStatus])) {
                return false;
            }
        }

        return $count ? true : false;
    }

    /**
     * Reads all the packages of an order and checks if there are packages that still need credit check
     */
    public function creditCheckReferredOrMoreInfoNeeded()
    {
        $count = 0;
        $packages = $this->getPackages();
        $flippedCreditCheckStatuses = array_flip([Vznl_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_CC_REFERRED, Vznl_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_CC_ADDITIONNAL_INFO_REQUIRED]);
        foreach ($packages as $package) {
            $count++;
            $creditCheckStatus = $package->getCreditcheckStatus();
            if (!$package->isFixed() && !isset($flippedCreditCheckStatuses[$creditCheckStatus])) {
                return false;
            }
        }

        return $count ? true : false;
    }

    /**
     * @param string $statusField
     * @param $statuses
     *
     * @return bool
     */
    protected function hasPackageWithStatusIn($statusField = 'status', $statuses)
    {
        $packages = $this->getPackages();
        /** @var Vznl_Package_Model_Package $package */
        $flippedStatuses = array_flip($statuses);
        foreach ($packages as $package) {
            $status = $package->getData($statusField);
            if (isset($flippedStatuses[$status])) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return bool
     */
    public function hasRunningCreditcheck()
    {
        return $this->hasPackageWithStatusIn('creditcheck_status', Vznl_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUSES_CC_RUNNING);
    }

    /**
     * @return bool
     */
    public function hasRunningNumberporting()
    {
        return $this->hasPackageWithStatusIn('porting_status', Vznl_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUSES_NP_RUNNING);
    }

    /**
     * Cancel the SuperOrder and all the linked orders
     */
    public function cancel()
    {
        $packages = $this->getPackages()->getItems();
        $cancelPackages = [];
        $cancelledCtns = [];
        $refundAmounts = [];
        $refundMethod = null;

        foreach ($packages as $package) {
            $cancelPackages[] = $package->getPackageId();
            $packageItems = $package->getPackageItems();
            $packageTotals = $package->getPackageTotals($packageItems);
            // Get order refund amount
            $refundAmounts['refundAmount'][$package->getPackageId()] = $packageTotals['total'];
            $refundAmounts['payments'][$package->getPackageId()] = $packageTotals['additional_total'];
            $cancelledCtns[] = $package->getTelNumber();
        }

        // Check if refund is required
        if (Mage::helper('agent')->checkIsWebshop(Mage::app()->getWebsite($this->getCreatedWebsiteId())->getCode())) {
            foreach ($this->getOrders() as $order) {
                if (strtolower($order->getPayment()->getMethod()) == 'adyen_hpp') {
                    $refundMethod = 'dss';
                    break;
                }
            }
        }

        // Lock the order
        Mage::helper('vznl_checkout')->lockOrder($this->getId(), Mage::helper('agent')->getAgentId(), 'Order cancel');

        // Process order
        $client = Mage::helper('vznl_service')->getClient('ebs_orchestration');
        $pollingId = $client->processCustomerOrder($this, null, null, null, null, array_unique($cancelPackages), $refundAmounts, $refundMethod); //call esb

        // Save data for later usage, after the call is performed
        $this->setData('polling_id', $pollingId);
        $this->_getCustomerSession()->setData(
            $pollingId,
            array(
                'cancelled_ctns' => $cancelledCtns,
                'packages' => $packages,
                'superorder' => $this
            )
        );

        return $this;
    }

    /**
     * Check whether or not an super order has deleted products
     *
     * @return bool
     */
    public function hasDeletedProducts()
    {
        $orders = $this->getOrders(true);
        if ($orders) {
            /** @var Vznl_Checkout_Model_Sales_Order $order */
            foreach ($orders as $order) {
                /** @var Vznl_Checkout_Model_Sales_Order_Item $orderItem */
                foreach ($order->getAllItems() as $orderItem) {
                    if ($orderItem->getProduct()->getId() === null) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * Get the url for the confirmation email banner onclick
     * This is specified per order
     * @return string
     */
    public function getConfirmationBannerUrl()
    {
        $url = Mage::getStoreConfig('vodafone_service/emails/confirmation_banner_url');
        /** @var Vznl_Customer_Model_Customer_Customer $customer */
        $customer = $this->getCustomer();
        $billingAddress = $customer->getDefaultBillingAddress();
        if (empty($billingAddress) || $billingAddress->getCountry() !== 'NL') {
            return null;
        }

        $data = [];
        $data['email'] = $customer->getCorrespondanceEmail();
        $data['postalCode'] = $billingAddress->getPostcode();
        $data['houseNumber'] = $billingAddress->getHouseNo();
        $data['houseNumberExtension'] = $billingAddress->getHouseAdd() === '' ? null : $billingAddress->getHouseAdd();
        return $url . base64_encode(json_encode($data));
    }

    /**
     * Get the first correspondance emailaddress of the delivery orders
     * @param integer|null $incrementId The increment id, when given only the delivery order with the given increment id
     * will be used.
     * @return string The correspondance emailaddress. Can be '' when no email address is found.
     */
    public function getCorrespondanceEmail($incrementId = null)
    {
        foreach ($this->getDeliveryOrders(true) as $deliveryOrder) {
            // If increment id is given skip when the delivery order does not have this increment id.
            if ($incrementId && $deliveryOrder->getIncrementId() != $incrementId) {
                continue;
            }

            $emails = explode(';',$deliveryOrder->getAdditionalEmail());
            $email = !is_null($deliveryOrder->getData('correspondence_email')) ? $deliveryOrder->getData('correspondence_email') : $emails[0];
            if ($email) {
                return $email;
            }
        }
        return '';
    }

    /**
     * Checks if a superorder has any products of type Garant
     * Parent will check for varchar, this method will check for text.
     */
    public function getGarantProductOrders()
    {
        list ($packagesWithGarant, $orderIds) = parent::getGarantProductOrders();
        //if superorder does not exist anymore, don't execute queries
        if (!$this->getId()) {
            return false;
        }

        //get attribute id for the addon_type
        $eavAttribute = new Mage_Eav_Model_Mysql4_Entity_Attribute();
        $attrId = $eavAttribute->getIdByCode('catalog_product', Omnius_Catalog_Model_Product::ADDON_TYPE_ATTR);

        //get id for the "Garant" option
        $_product = Mage::getModel('catalog/product');
        $attr = $_product->getResource()->getAttribute(Omnius_Catalog_Model_Product::ADDON_TYPE_ATTR);

        if ($attr && $attr->usesSource()) {
            $valueId = $attr->getSource()->getOptionId("Garant");
        }

        if (isset($valueId) && $valueId) {
            $read = Mage::getSingleton('core/resource')->getConnection('core_read');
            $query = "
            SELECT o.entity_id, oi.package_id 
            FROM catalog_product_entity_text cpe 
            INNER JOIN sales_flat_order_item AS oi ON cpe.entity_id=oi.product_id
            INNER JOIN sales_flat_order AS o ON o.entity_id=oi.order_id 
            WHERE cpe.attribute_id=:attr 
            AND cpe.VALUE=:value 
            AND o.edited != 1 and o.superorder_id=:orderId";

            $binds = array(
                ':attr' => $attrId,
                ':value' => $valueId,
                ':orderId' => $this->getId()
            );

            $queryResult = $read->query($query,$binds);
            $results = $queryResult->fetchAll();
            foreach ($results as $result) {
                $orderIds[] = $result['entity_id'];
                $packagesWithGarant[$result['entity_id']] = isset($packagesWithGarant[$result['entity_id']]) ? $packagesWithGarant[$result['entity_id']] : [];
                $packagesWithGarant[$result['entity_id']][] = $result['package_id'];
            }

        }

        return array($packagesWithGarant, array_unique($orderIds));
    }

    /**
     * Check if order has a mobile package in the superorder
     * @return bool
     */
    public function hasMobilePackage()
    {
        $packages = $this->getPackages()->getItems();
        /** @var Vznl_Package_Model_Package $p */
        foreach ($packages as $p) {
            if ($p->isMobile()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Checks if there is at least one retention package in the superorder
     * @return bool
     */
    public function hasRetention($onlyFinal = true)
    {
        $this->onlyFinal = $onlyFinal;
        $packages = $this->getPackages()->getItems();
        /** @var Vznl_Package_Model_Package $p */
        foreach ($packages as $p) {
            if ($p->isRetention()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Checks if there is at least one Acquisition package in the superorder
     * @return bool
     */
    public function hasAcquisition($onlyFinal = true)
    {
        $this->onlyFinal = $onlyFinal;
        $packages = $this->getPackages()->getItems();
        /** @var Vznl_Package_Model_Package $p */
        foreach ($packages as $p) {
            if ($p->isAcquisition()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Checks if there is at least one fixed package in the superorder
     * @return bool
     */
    public function hasFixed()
    {
        $packages = $this->getPackages()->getItems();
        /** @var Vznl_Package_Model_Package $p */
        foreach ($packages as $p) {
            if ($p->isFixed()) {
                return true;
            }
        }

        return false;
    }

    public function hasStoreDelivery()
    {
        /** @var Vznl_Checkout_Model_Sales_Order $order */
        foreach ($this->getOrders(true) as $order) {
            if ($order->getShippingAddress() && ($type = $order->getShippingAddress()->getDeliveryType())) {
                if ($type == 'billing_address' || $type == 'other_address') {
                    return false;
                } else {
                    return true;
                }
            }
        }
        return false;
    }

    public function hasOtherStoreDelivery()
    {
        /** @var Vznl_Checkout_Model_Sales_Order $order */
        foreach ($this->getOrders(true) as $order) {
            if ($order->getShippingAddress() && ($type = $order->getShippingAddress()->getDeliveryType())) {
                if($order->isOtherStoreDelivery()){
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Returns full order status text
     * @return bool
     */
    public function getFullStatusText() {
        $hasMobile = $hasFixed = false;
        foreach ($this->getOrders(true) as $order) {
            if ($order->hasFixed()) {
                $hasFixed = true;
            }
            if ($order->hasMobilePackage()) {
                $hasMobile = true;
            }
        }
        if( !$hasMobile && $hasFixed ) {
            return $this->getPealOrderStatus();
        }
        return $this->getOrderStatus();
    }

    /**
     * Returns translated order status for fixed orders
     * @return bool
     */
    public function getFixedSubStatusTranslated() {
        $statusSimplified = $this->getPealOrderSubStatus();
        $statusModel = Mage::getModel('validatecart/pealsubstatus')->loadBySubstatus($statusSimplified);
        if ($statusModel && !empty($statusModel->getTranslation())) {
            return $statusModel->getTranslation();
        }
        return $statusSimplified;
    }

    /**
     * Check if the superorder has delivery orders at the Point of No Return
     */
    public function hasNotEditableDeliveryOrders()
    {
        $flippedEcomStatuses = array_flip([
                    Vznl_Checkout_Model_Sales_Order::STATUS_INPROGRESS,
                    Vznl_Checkout_Model_Sales_Order::STATUS_PICKED,
                    Vznl_Checkout_Model_Sales_Order::STATUS_RDY4SHIP,
                    Vznl_Checkout_Model_Sales_Order::STATUS_RETPENDING
                ]);
        foreach ($this->getOrders(true) as $order) {
            $ecomStatus = $order->getEcomStatus();
            if (isset($flippedEcomStatuses[$ecomStatus])) {
                return true;
            }
        }

        return false;
    }
    /**
     * Tries to determine if the superorder is a result of a create or change(before/after):
     * If it's updatedAt == createdAt , means is "Create"
     * If it has children, it's a "changeAfter"
     * If none of the above, it's a "changeBefore"
     *
     * @return string Type of action
     */
    public function getSalesOrderActionType()
    {
        //if it was never updated, it's new, if it has cancelled status is cancelled and otherwise, changed
        if(!$this->getUpdatedAt() || $this->getUpdatedAt() == $this->getCreatedAt()) {
            return self::SO_TYPE_CREATE;
        }
        elseif($this->getOrderStatus() == self::SO_CANCELLED){
            return self::SO_TYPE_CANCEL;
        }
        else{
            return self::SO_TYPE_CHANGE;
        }
    }

    /**
     * Get current one time charge for an entire
     */
    public function getDeltaOneTimeCharge()
    {
        $deliveryOrders = $this->getDeliveryOrders();

        /** @var Dyna_Checkout_Model_Sales_Order $lastDeliveryOrder */
        $lastDeliveryOrder = $deliveryOrders->getLastItem();
        $parentId = $lastDeliveryOrder->getParentId();

        $oneTimeCharge = $lastDeliveryOrder->getTotals()['total'];
        if ($parentId) {
            /** @var Dyna_Checkout_Model_Sales_Order $parentDeliveryOrder */
            $parentDeliveryOrder = $deliveryOrders->getItemById($parentId);
            $oneTimeCharge = $oneTimeCharge - $parentDeliveryOrder->getTotals()['total'];
        }

        return round($oneTimeCharge, 2);
    }

    /**
     * @param $oneTimeCharge
     * @return string Type of action
     */
    public function getOneTimeChargeType($oneTimeCharge)
    {
        if ($oneTimeCharge > 0) {
            return self::SO_CHARGE_TYPE_HIGHER;
        } elseif ($oneTimeCharge < 0) {
            return self::SO_CHARGE_TYPE_LOWER;
        } elseif ($oneTimeCharge == 0) {
            return self::SO_CHARGET_YPE_EQUAL;
        }
    }

    /**
     * Check if a superorder has any delivered packages
     */
    public function hasDeliveredPackages()
    {
        $key = __METHOD__ . ' ' . $this->getId();
        if (Mage::registry('freeze_models') === true) {
            $data = Mage::registry($key);
            if ($data !== null) {
                return $data;
            }
        }

        $hasDeliveredPackages = Mage::getModel('package/package')->getCollection()
            ->addFieldToSelect('entity_id')
            ->addFieldToFilter('order_id', $this->getId())
            ->addFieldToFilter('type', array('neq' => Vznl_Configurator_Model_PackageType::PACKAGE_TYPE_FIXED))
            ->addFieldToFilter('status', Omnius_Package_Model_Package::ESB_PACKAGE_STATUS_DELIVERED)
            ->getSize();

        if (Mage::registry('freeze_models') === true) {
            Mage::unregister($key);
            Mage::register($key, $hasDeliveredPackages);
        } else {
            Mage::unregister($key);
        }

        return $hasDeliveredPackages;
    }

    /**
     * Get current one time charge for an entire
     */
    public function getCurrentOnTimeCharge()
    {
        $deliveryOrders = $this->getDeliveryOrders();

        /** @var Dyna_Checkout_Model_Sales_Order $lastDeliveryOrder */
        $lastDeliveryOrder = $deliveryOrders->getLastItem();
        $parentId = $lastDeliveryOrder->getParentId();

        $oneTimeCharge = $lastDeliveryOrder->getTotals()['total'];

        if ($parentId) {
            /** @var Dyna_Checkout_Model_Sales_Order $parentDeliveryOrder */
            $parentDeliveryOrder = $deliveryOrders->getItemById($parentId);
            $oneTimeCharge = $oneTimeCharge - $parentDeliveryOrder->getTotals()['total'];
        }

        return round($oneTimeCharge, 2);
    }

    /**
     * Check at superorder level if any of the packages are missing the sim while having number porting
     */
    public function getMissingSim()
    {
        if ($this->_missingSim === null) {
            $this->_missingSim = false;

            if (!Mage::helper('agent')->checkIsTelesalesOrWebshop(Mage::app()->getWebsite($this->getCreatedWebsiteId())->getCode())) {
                if ($this->getOrderStatus() != self::SO_VALIDATED_PARTIALLY ) {
                    foreach ($this->getPackages() as $package) {
                        if ($package->getCurrentNumber() && !$package->getSimNumber() &&
                            $package->getCreditcheckStatus() != Vznl_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_CC_PARTIAL) {
                            return ($this->_missingSim = true);
                        }
                    }
                }

            }
        }
        return $this->_missingSim;
    }

    /**
     * @param $packageTotals
     * @param $oldPackageTotals
     * @return int $oneTimeCharge
     */
    public function getOneTimeCharge($packageTotals, $oldPackageTotals)
    {
        if ($packageTotals['total'] && $oldPackageTotals['total']) {
            $oneTimeCharge = $packageTotals['total'] - $oldPackageTotals['total'];
        }
        return round($oneTimeCharge, 2);
    }

    /**
     * @param $OrderNumber
     * @return EmailList
     */
    public function getCommunicationP85List($orderNumber)
    {
        $filterOrderNumber = array('like'=>'%SaleorderNumber":"'.$orderNumber.'%');
        $filterStatusAction = array('like'=>'%SalesOrderAction":"'.Vznl_Superorder_Model_Superorder::SO_TYPE_CREATE.'%');
        $triggerCommunication = Mage::getModel('communication/job')->getCollection()
                ->addFieldToFilter('email_code',array('in'=>array(Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_ORDER_CONFIRMATION_CREATE,Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_ORDER_CONFIRMATION_OMNICHANNEL)))
                ->addFieldToFilter('parameters',$filterOrderNumber)
                ->addFieldToFilter('parameters',$filterStatusAction);
        return $triggerCommunication;
    }

    /**
     * @param DeliveryMethods
     * @return string
     */
    public function getDeliveryDutchValues($deliveryMethod)
    {
        switch ($deliveryMethod) {
            case 'SHOP':
                $deliveryValue = "Levering vanuit de winkel";
            break;
            case 'LSP_BILLING_ADD':
            case 'LSP_OTHER_ADD':
            case 'LSP_SERVICE_ADD':
                $deliveryValue = "Levering aan huis";
            break;
            case 'OTHER_SHOP':
                $deliveryValue = "Levering vanuit andere winkel";
            break;
        }
        return $deliveryValue;
    }

    /**
     * Check if the supeorder has Reffered/Pending/AdditionalInfo packages
     *
     * @return bool
     */
    public function hasPendingPackages()
    {
        $pendingStatuses = [
            Dyna_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_CC_PENDING,
            Dyna_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_CC_INITIAL,
            Dyna_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_CC_ADDITIONNAL_INFO_REQUIRED,
            Dyna_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_CC_REFERRED,
        ];
        // get all packages
        $packages = $this->getPackages();
        foreach ($packages as $package) {
            // check packages status
            if (in_array($package->getCreditcheckStatus(), $pendingStatuses)) {
                // return true if one is pending
                return true;
            }
        }

        return false;
    }

    /**
     * Checks if the SuperOrder is not already cancelled or in a pending state
     *
     * @return bool
     */
    public function superOrderPending(){
        $pendingStatuses = [
            $this::SO_PRE_INITIAL,
            $this::SO_INITIAL,
            $this::SO_ADYEN_PAYMENT,
            $this::SO_WAITING_VALIDATION_ACCEPTED_AXI,
            $this::SO_CANCELLED
        ];

        return in_array($this->getOrderStatus(), $pendingStatuses);
    }

    /**
     * Cancel the order via the dealer adapter call
     */
    public function cancelPendingOrder()
    {
        /** @var Dyna_Service_Helper_Data $h */
        $h = Mage::helper('dyna_service');
        /** @var Dyna_Service_Model_Client_CancelDuringCreditCheckClient $client */
        $client = $h->getClient('cancel_during_credit_check');

        $client->cancelSuperOrder($this->getOrderNumber());

        return $this;
    }

    /**
     * Retrieves the customer model of this superorder
     * @return Mage_Core_Model_Abstract|Vznl_Customer_Model_Customer_Customer
     */
    public function getCustomer():Vznl_Customer_Model_Customer_Customer
    {
        return Mage::getModel('customer/customer')->load($this->getCustomerId());
    }


    /**
     * Returns a list of superorders for a given customer
     * @param $customerId
     * @param null $orderId
     * @return array
     */
    protected function _getSuperordersCollection($customerId, $orderId = null)
    {
        $agent = $this->_getCustomerSession()->getSuperAgent() ?: $this->_getCustomerSession()->getAgent(true);

        $collection = Mage::getModel('package/package')->getCollection()
            // Ignore packages where there is no checksum, because they contain no items and are most likely created using multiple tabs
            ->addFieldToFilter('main_table.checksum', ['neq' => 'null']);
        $collection->getSelect()->join(
            array('superorder'),
            'main_table.order_id = superorder.entity_id',
            array('superorder.order_status', 'superorder.order_number', 'order_created_at' => 'superorder.created_at', 'customer_id' => 'superorder.customer_id')
        );
        $collection->addFieldToFilter('customer_id', $customerId);
        $collection->load();

        /** @var Omnius_Superorder_Model_Mysql4_Superorder_Collection $superOrders */
        $superOrders = Mage::getModel('superorder/superorder')->getCollection();
        $superOrders->addFieldToFilter('main_table.customer_id', $customerId);
        $superOrders->setOrder('main_table.created_at', 'DESC');

        if ($orderId) {
            $superOrders->addFieldToFilter('main_table.entity_id', $orderId);
        }

        $superOrders->getSelect()->joinRight(array('sales_flat_order'), 'main_table.entity_id = sales_flat_order.superorder_id', array())->group('main_table.entity_id');

        if ($agent && $agent->getId()) {
            $superOrders = $this->applyFiltersByAgent($superOrders);
        }

        $superOrders->load();
        $response = [];
        if (count($collection) && count($superOrders)) {
            $found = [];
            foreach ($collection as $order) {
                $found[$order->getOrderId()] = $order->getOrderNumber();
            }
            foreach ($superOrders as $superOrder) {
                if (isset($found[$superOrder->getId()])) {
                    $response[$superOrder->getId()] = $superOrder;
                }
            }
        }

        return $response;
    }


    /**
     * Release lock and set order as cancelled.
     * @param $pollingId
     * @return $this
     * @throws Exception
     */
    public function postProcessOrderCancel($pollingId)
    {
        $sessionData = $this->_getCustomerSession()->getData($pollingId);
        $cancelledCtns = $sessionData['cancelled_ctns'];
        $packages = $sessionData['packages'];
        // Release lock
        Mage::helper('omnius_checkout')->releaseLockOrder($this->getId(), Mage::helper('agent')->getAgentId(), false);

        $customerId = $this->getCustomerId();
        // Delete CTN from customer_ctn table
        foreach ($cancelledCtns as $ctn) {
            $write = Mage::getSingleton('core/resource')->getConnection('core_write');
            $write->exec("DELETE FROM customer_ctn WHERE ctn = '" . $ctn . "' AND customer_id = '" . $customerId . "'");
        }

        $orders = $this->getOrders();
        /** @var Omnius_Checkout_Model_Sales_Order $order */
        foreach ($orders->getItems() as $order) {
            $order->cancel();
            $order->save();
        }

        foreach ($packages as $package) {
            $package->setStatus(Omnius_Package_Model_Package::ESB_PACKAGE_STATUS_CANCELLED);
            $package->save();
        }

        $this->setOrderStatus(self::SO_CANCELLED);
        $this->save();

        //same method used as in Vznl_RestApi_Orders_Abstract for createOrder, but status for cancel
        $con = Mage::getSingleton('core/resource')->getConnection('core_write');
        $con->query(
            sprintf(
                'INSERT INTO status_history (superorder_id, status, type, agent_id, created_at) values(%d, "%s", "%s", "%s", "%s");',
                $this->getId(),
                self::SO_CANCELLED,
                'order_status',
                Mage::helper('agent')->getAgentId(),
                Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s')
            )
        );

        return $this;
    }
}
