<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */

/**
 * Class Vznl_Superorder_Helper_Data
 */
class Vznl_Superorder_Helper_Data extends Omnius_Superorder_Helper_Data
{
    /**
     * Check if an order was created on Webshop and is edited on telesales
     *
     * @param null $id
     * @return bool|mixed
     */
    public function checkIfSoFromOnlineEditedOnTelesales($id = null)
    {
        if (!$id) {
            // current store
            $storeId = Mage::app()->getStore()->getId();
            $id = Mage::getModel('core/store')->load($storeId)->getWebsiteId();
        }

        if (!Mage::helper('agent')->isTelesalesLine(Mage::app()->getWebsite($id)->getCode())) {
            return false;
        }

        $quoteId = Mage::getSingleton('customer/session')->getOrderEdit();
        if (!$quoteId) {
            return false;
        }

        $key = __METHOD__ . '|OrderEdit|' . $quoteId;
        if (!($so = Mage::registry($key))) {
            /** @var Dyna_Checkout_Model_Sales_Quote $quote */
            $quote = Mage::getModel('sales/quote')->load($quoteId);
            if ($so = Mage::getModel('superorder/superorder')->load($quote->getSuperOrderEditId())) {
                if ($so->getId()) {
                    Mage::unregister($key);
                    Mage::register($key, $so);
                }
            }
        }

        $soWebsiteId = $so->getCreatedWebsiteId();
        if (Mage::app()->getWebsite($soWebsiteId)->getCode() === Vznl_Agent_Model_Website::WEBSITE_WEB_SHOP_CODE) {
            return $so;
        }

        return false;
    }

    /**
     * @param $ordersCollection
     * @param $cluster
     *
     * @return array|null
     */
    public function formatOrdersInfo($ordersCollection, $cluster)
    {
        /** @var Vznl_Configurator_Model_Cache $cache */
        $cache = Mage::getSingleton('vznl_configurator/cache');
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $return = null;
        $customer = Mage::getSingleton('customer/session')->getCustomer();

        if (!empty($ordersCollection)) {
            foreach ($ordersCollection as $order) {
                $date1 = new DateTime("now");
                $currentDate = $cluster !== 'inlife' ? strtotime($order->getSuperOrderCreated()) : strtotime($order->getData('customer_i_d'));
                $date2 = new DateTime(date("Y-m-d", $currentDate));
                $dayInThisState = $date1->diff($date2)->days;

                $customerNameData = [];
                $customerNameData[] = $order->getCustomerFirstname();
                empty($order->getCustomerMiddleName()) ? null : $customerNameData[] = $order->getCustomerMiddleName();
                $customerNameData[] = $order->getCustomerLastname();
                $customerName = implode(' ', $customerNameData);
                $ban = $order->getCustomerBan();

                /** @var Vznl_Superorder_Model_Superorder $superOrder */
                $superOrder = Mage::getModel('superorder/superorder')->load((string) $order->getSuperOrderNumber(), 'order_number');

                $pealOrderStatus = $superOrder->getPealOrderStatus();
                $pealOrderSubStatus = $superOrder->getPealOrderSubStatus();
                if ($superOrder->hasFixed() && $superOrder->getPealOrderId() && strtolower($pealOrderStatus) != 'closed' && strtolower($pealOrderStatus) != 'cancelled' && $customer->getId()) {
                    $helper = Mage::helper('vznl_getorderstatus');
                    $response = $helper->getOrderStatus($superOrder->getPealOrderId());
                    if (isset($response['status']) || isset($response['subStatus'])) {
                        if (isset($response['status'])) {
                            $pealOrderStatus = $response['status'];
                            $superOrder->setPealOrderStatus($pealOrderStatus);
                        }
                        if (isset($response['subStatus'])) {
                            $pealOrderSubStatus = $response['subStatus'];
                            $superOrder->setPealOrderSubStatus($pealOrderSubStatus);
                        }
                        Mage::helper('vznl_utility')->saveModel($superOrder);
                    }
                }
                $newOrder = [
                    'bsl' => $cluster === 'inlife' ? true : false,
                    'date' => date("d-m-Y", strtotime($order->getSuperOrderCreated())),
                    'number' => $order->getSuperOrderNumber(),
                    'customer_id' => $order->getSuperCustomerId(),
                    'customer_ban' => $ban,
                    'customer_name' => $customerName,
                    'phone_number' => $order->getTelNumber() ?? $order->getFixedTelephoneNumber(),
                    'order_Status' => $order->getSuperOrderStatus(),
                    'peal_order_status' => $pealOrderStatus,
                    'peal_order_sub_status' => $pealOrderSubStatus,
                    'has_numberporting_or_creditcheck' => $superOrder->hasRunningCreditcheck() || $superOrder->hasRunningNumberporting() ? 'true' : 'false',
                    'has_sim' => 0,
                    'has_fixed' => 0,
                    'has_mobile' => 0,
                    'has_hardware' => 0,
                    'has_number_porting' => 0,
                    'details' => [
                        'days_in_status' => $dayInThisState,
                        'error' => [
                            'code' => $order->getSuperErrorCode(),
                            'message' => $this->__($this->_getErrorMessage((int)$order->getSuperErrorCode(), (string)$order->getSuperErrorDetail())),
                            'description' => ''
                        ]
                    ],
                    'packages_number' => 0,
                    'packages' => []
                ];

                // Get more data for local orders
                if ($cluster !== 'inlife') {
                    $packages = Mage::getModel('package/package')->getPackages($order->getSuperEntityId());
                    /** @var Vznl_Package_Model_Package $package */
                    foreach ($packages as $package) {
                        if (!$newOrder['has_number_porting']) {
                            if ($package->isNumberPorting()) {
                                $newOrder['has_number_porting'] = 1;
                            }
                        }
                        $has_sim = 0;
                        $has_fixed = 0;
                        $has_hardware = 0;

                        $productIds = explode(",", $package->getChecksum());
                        if ($package->getCoupon()) {
                            $couponIds = explode(",", $package->getCoupon());
                            foreach ($couponIds as $couponId) {
                                if (($key = array_search($couponId, $productIds)) !== false) {
                                    unset($productIds[$key]);
                                }
                            }
                        }
                        foreach ($productIds as $productId) {
                            $key = sprintf('oo_product_is_hardware_%s', $productId);
                            if (false && $result = $cache->load($key)) {
                                if ($result == "2") {
                                    $has_hardware = 1;
                                }
                                if ($result == "3") {
                                    $has_sim = 1;
                                }
                                if ($result == "4") {
                                    $has_fixed = 1;
                                }
                            } else {
                                $query = "SELECT
                                t3.value AS VALUE
                                FROM
                                catalog_product_entity_int t1
                                LEFT JOIN eav_attribute t2
                                ON t1.`attribute_id` = t2.`attribute_id`
                                LEFT JOIN eav_attribute_option_value t3
                                ON t3.`option_id` = t1.`value`
                                WHERE t2.entity_type_id = '4'
                                AND t2.attribute_code = '" . Omnius_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR . "'
                                AND t1.`entity_id` = '" . $productId . "'";
                                $attribute = $readConnection->fetchOne($query, null, PDO::FETCH_COLUMN);

                                switch ($attribute) {
                                    case Vznl_Catalog_Model_Type::SUBTYPE_SIMCARD:
                                        $result = "3";
                                        $has_sim = 1;
                                        break;
                                    case Dyna_Catalog_Model_Type::SUBTYPE_ACCESSORY:
                                    case Vznl_Catalog_Model_Type::SUBTYPE_DEVICE:
                                        $result = "2";
                                        $has_hardware = 1;
                                        break;
                                    case Vznl_Catalog_Model_Type::SUBTYPE_FIXED_PACKAGE:
                                        $result = "4";
                                        $has_fixed = 1;
                                    default:
                                        $result = '1';
                                        break;
                                }
                                $cache->save($result, $key, [Vznl_Configurator_Model_Cache::CACHE_TAG], $cache->getTtl());
                            }
                        }

                        // Porting
                        switch ($package->getPortingStatus()) {
                            case 'INITIAL':
                                $portingStatus = $this->__($package->getPortingStatus());
                                break;
                            case 'PENDING':
                                $portingStatus = $this->__($package->getPortingStatus());
                                break;
                            case 'REJECTED':
                                $portingStatus = $this->__($package->getPortingStatus());
                                break;
                            case 'REJECTED & CANCELLED':
                                $portingStatus = $this->__($package->getPortingStatus());
                                break;
                            case 'APPROVED':
                                $portingStatus = $this->__($package->getPortingStatus());
                                break;
                            default:
                                $portingStatus = 'N/A';
                                break;
                        }

                        // Porting references
                        $portingReferenceCode = null;
                        $portingReferenceDescription = null;
                        if ($package->getPortingReferenceId()) {
                            /** @var Dyna_Porting_Model_Reference $reference */
                            $reference = Mage::getModel('vznl_porting/reference')->load($package->getPortingReferenceId());
                            $portingReferenceCode = $reference->getCode();
                            $portingReferenceDescription = $reference->getText();
                        }

                        $history = Mage::getModel('vznl_superorder/statusHistory')
                            ->getCollection()
                            ->addFieldToFilter('package_id', $package->getId())
                            ->setPageSize(1)
                            ->getLastItem();
                        $date2 = new DateTime(date("Y-m-d", strtotime($history->getCreatedAt())));
                        $dayInThisState = $date1->diff($date2)->days;
                        $newOrder['packages'][] = [
                            'package_id' => $order->getSuperOrderNumber() . "." . $package->getPackageId(),
                            'status' => $package->getStatus(),
                            'type' => $package->getType(),
                            'order_type' => Mage::helper('vznl_checkout')->__($package->getSaleType()),
                            'has_sim' => $has_sim,
                            'has_hardware' => $has_hardware,
                            'has_fixed'   => $has_fixed,
                            'porting' => [
                                'status' => $portingStatus,
                                'code' => $portingReferenceCode,
                                'description' => $portingReferenceDescription,
                            ],
                            'details' => [
                                'days_in_status' => $dayInThisState,
                                'error' => [
                                    'code' => $package->getVfStatusCode(),
                                    'message' => $package->getVfStatusDesc(),
                                    'description' => ''
                                ]
                            ]
                        ];

                        if (!$newOrder['has_hardware']) {
                            $newOrder['has_hardware'] = $has_hardware;
                        }
                        if (!$newOrder['has_sim']) {
                            $newOrder['has_sim'] = $has_sim;
                        }
                        if (!$newOrder['has_fixed']) {
                            $newOrder['has_fixed'] = $has_fixed;
                        }

                        /* Proper check if package is fixed package */
                        if ($package->isFixed()) {
                            $newOrder['has_fixed'] = true;
                        }

                        $newOrder['packages_number']++;

                    }
                }

                if (!$newOrder['has_fixed']) {
                    $newOrder['peal_order_status'] = '';
                }
                if ($newOrder['has_fixed'] && $newOrder['packages_number']==1) {
                    $newOrder['order_Status'] = '';
                }

                $return[] = $newOrder;
            }
        }
        return $return;
    }

    /**
     * Exclude orders which has only SIM package subtype for the subscription based product or
     * in other words do not have any mobile devices in the order (but could have modems or routers which also use a SIM)
     * @param $orderPackageCollection
     * @return Omnius_Package_Model_Mysql4_Package_Collection
     */
    public function excludeSimOnlyOrders($orderPackageCollection)
    {
        $nonSimOnlyOrderPackages = $productIds = $prodOrderIds = $orders = [];
        foreach ($orderPackageCollection as $order) {
        	$orderId = $order->getOrderId();
        	$orders[$orderId] = $order;
            foreach ($order->getAllItems() as $item) {
            	$prodId = $item['product_id'];
            	$productIds[$prodId] = $item['product_id'];
            	$prodOrderIds[$prodId][] = $orderId;
            }
        }

        $productCollection = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('entity_id')
            ->addAttributeToSelect(Omnius_Catalog_Model_Product::POSTP_SIMONLY_KEY)
            ->addAttributeToFilter('entity_id', array('in' => $productIds));

        $orderCheck = [];
        foreach ($productCollection as $prd) {
        	$id = $prd->getId();
        	if (!$prd->getSimOnly()) {
        		foreach ($prodOrderIds[$id] as $ordId) {
        			if (!isset($orderCheck[$ordId])) {
        				$orderCheck[$ordId] = $ordId;
        				$nonSimOnlyOrderPackages[] = $orders[$ordId];
        			}
        		}
        	}
        }

        return $nonSimOnlyOrderPackages;
    }

    /**
     * @param int $code
     * @param string $detail
     * @return string
     */
    private function _getErrorMessage(int $code, string $detail): string
    {
        switch ($code) {
            case 10000:
                return 'Sales order ID is not unique';
            case 10001:
                return 'Order cannot be processed';
            case 10002:
                return 'Order cannot be approved';
            case 10004:
                return 'Order cannot be sent to Ecommerce';
            case 10005:
                return 'Order cannot be saved';
            case 10006:
                return 'Package cannot be changed';
            case 10008:
                return 'Order details cannot be retrieved';
            case 10009:
                return 'Add-on cannot be processed';
            case 10010:
                return 'Error while confirming the order';
            case 10011:
                return 'Cancel order was unsuccessful';
            case 10012:
                return 'Order is locked';
            case 10013:
                return 'Order cannot be unlocked';
            case 10014:
                return 'Order cannot be released';
            case 10015:
                return 'Some products could not be recognized';
            case 10016:
                return 'Order cannot be validated';
            default:
                return $detail;
        }
    }

    /**
     * Checks if the status changed for a given object by comparing the new and old data
     * @param $origData
     * @param $newData
     * @param $types
     * @param $entity
     */
    public function checkStatusesChanged($origData, $newData, $types, $entity)
    {
        $resourceName = $entity->getResourceName();
        $entityResource = ucfirst(substr($resourceName, strpos($resourceName, "/") + 1));
        $setter = 'set' . $entityResource . 'Id';
        foreach ($types as $field) {
            if (isset($newData[$field])) {
                $key = 'check_' . $field . $setter . $newData[$field] . $entity->getId();
                $origDataCond = is_null($origData)
                    || (isset($origData[$field]) && $origData[$field] !== $newData[$field])
                    || !isset($origData[$field]);
                if ($origDataCond && !Mage::registry($key)) {
                    //RFC-150742 : Save agent id in status history for cancelled orders
                    $agentId = null;
                    if (($entityResource == "Superorder") && ($newData[$field] == Omnius_Superorder_Model_Superorder::SO_CANCELLED)) {
                        $customerSession = Mage::getSingleton('customer/session');
                        $agent = $customerSession->getAgent(true);
                        $superAgent = $customerSession->getSuperAgent();
                        $agentId = ($superAgent && $superAgent->getId()) ? $superAgent->getId() : ($agent && $agent->getId() ? $agent->getId() : null);
                    }
                    Mage::unregister($key);
                    Mage::register($key, true);
                    Mage::getModel('superorder/statusHistory')
                        ->setType($field)
                        ->$setter($entity->getId())
                        ->setStatus($newData[$field])
                        ->setAgentId($agentId)
                        ->setCreatedAt(now())
                        ->save();
                }
            }
        }
    }

}
