<?php
/**
 * Copyright (c) 2019. Dynacommerce B.V.
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

/** @var Varien_Db_Adapter_Interface $connection */
$connection = $this->getConnection();
$installer->startSetup();
if ($installer->tableExists('superorder_change_history')) {

        $connection->addForeignKey(
            $installer->getFkName('superorder_change_history', 'superorder_id', 'superorder', 'entity_id'),
            $installer->getTable('superorder_change_history'),
            'superorder_id',
            $installer->getTable('superorder'),
            'entity_id'
         );

}

$installer->endSetup();
