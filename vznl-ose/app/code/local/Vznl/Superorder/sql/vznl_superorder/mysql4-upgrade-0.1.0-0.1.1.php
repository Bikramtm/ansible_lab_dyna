<?php
/**
 * Copyright (c) 2019. Dynacommerce B.V.
 *
 * Adding missing indexes
 * 
 */

/* @var $installer Vznl_SuperOrder_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();
$connection = $installer->getConnection();

$connection->addIndex($this->getTable('superorder/superorder'),'IDX_SUPERORDER_CUSTOMER_NUMBER', array('customer_number'));

$installer->endSetup();