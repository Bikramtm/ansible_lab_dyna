<?php
/**
 * Copyright (c) 2019. Dynacommerce B.V.
 */
$installer  = $this;
$connection = $installer->getConnection();
$installer->startSetup();

$tableSales = $this->getTable('superorder');

if ($connection->tableColumnExists($tableSales, 'oo_imei') === false) {
    $connection->addColumn(
        $tableSales, 'oo_imei', 'TINYINT Default 0'
    );
}

$table = $this->getTable('superorder');

if ($installer->getConnection()->isTableExists($table)) {
    $table = $installer->getConnection();
    $installer->getConnection()
        ->addIndex(
            $installer->getTable('superorder'),
            $installer->getIdxName(
                'superorder',
                array(
                    'agent_id',
                    'customer_id',
                    'is_vf_only',
                    'order_status',
                    'error_code',
                ), Varien_Db_Adapter_Interface::INDEX_TYPE_INDEX
            ),
            array(
                'agent_id',
                'customer_id',
                'is_vf_only',
                'order_status',
                'error_code',
            ), array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_INDEX)
    );
}

$table = $this->getTable('catalog_package');

if ($installer->getConnection()->isTableExists($table)) {
    $table = $installer->getConnection();
    $installer->getConnection()
        ->addIndex(
            $installer->getTable('catalog_package'),
            $installer->getIdxName(
                'catalog_package',
                array(
                    'status',
                    'vf_status_code',
                    'porting_status',
                    'creditcheck_status',
                ), Varien_Db_Adapter_Interface::INDEX_TYPE_INDEX
            ),
            array(
                'status',
                'vf_status_code',
                'porting_status',
                'creditcheck_status',
            ), array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_INDEX)
    );
}

$installer->endSetup();
