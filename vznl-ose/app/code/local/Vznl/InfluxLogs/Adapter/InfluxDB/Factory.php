<?php

use GuzzleHttp\Client;

class Vznl_InfluxLogs_Adapter_InfluxDB_Factory
{
    public static function create()
    {
        $client = new Client();
        $dataHelper = Mage::helper('vznl_influxlogs');
        return new Vznl_InfluxLogs_Adapter_InfluxDB_Adapter(
            $client,
            $dataHelper->getUrl(),
            $dataHelper->getDatabase(),
            $dataHelper->getAuthLogin(),
            $dataHelper->getAuthPass()
        );
    }
}
