<?php
class Vznl_InfluxLogs_Helper_Data extends Mage_Core_Helper_Abstract {

    /**
     * @return string|null
     **/
    public function getUrl():?string
    {
        return Mage::getStoreConfig('omnius_service/influxdb_settings/url');
    }

    /**
     * @return string|null
     **/
    public function getDatabase():?string
    {
        return Mage::getStoreConfig('omnius_service/influxdb_settings/default_db');
    }

    /**
     * @return string|null
     **/
    public function getAuthLogin():?string
    {
        return Mage::getStoreConfig('omnius_service/influxdb_settings/auth_login');
    }

    /**
     * @return string|null
     **/
    public function getAuthPass():?string
    {
        return Mage::getStoreConfig('omnius_service/influxdb_settings/auth_pass');
    }

}
