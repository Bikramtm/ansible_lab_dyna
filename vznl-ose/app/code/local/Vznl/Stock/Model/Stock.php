<?php

class Vznl_Stock_Model_Stock extends Mage_Core_Model_Abstract
{
    protected $defaultAxiCode = 500;

    const STATUS_IN_STOCK = 'in stock';

    /**
     * Init entity
     */
    protected function _construct()
    {
        $this->_init('stock/stock');
    }

    /** @var Vznl_Service_Model_Client_AxiClient */
    protected static $client;

    /**
     * Get AXI client
     * @return Vznl_Service_Model_Client_AxiClient
     */
    protected static function getAxiClient()
    {
        if ( ! static::$client) {
            static::$client = Mage::helper('vznl_service')->getClient('axi');
        }

        return static::$client;
    }

    /**
     * Returns stacks for product item
     * @param $productCollection
     * @param null $storeCode
     * @param bool $incWarehouseStock
     * @param int $maxDistance
     * @return mixed
     */
    public function getLiveStockForItem($productCollection, $storeCode = null, $incWarehouseStock = true, $maxDistance = 200)
    {
        if(is_null($storeCode)) {
            $storeCode = $this->getStoreCode();
            if (is_null($storeCode)) {
                $storeCode = $this->defaultAxiCode;
            }
        }

        if(!is_array($productCollection)) {
            $productCollection = array($productCollection);
        }

        $stockCollection = new Varien_Data_Collection();
        $accessor = $this->getAccessor();

        $productSkus = $this->getProductSkus($productCollection);
        $resultsIncWarehouse = $this->getAxiClient()->getStock($productSkus, $storeCode, $maxDistance, $incWarehouseStock);

        if (array_key_exists('product_i_d', $resultsIncWarehouse)) {
            $otherStores = $this->getOtherStores($accessor, $resultsIncWarehouse);
            $otherStoresAttr = $this->getOtherStoreAttributes($otherStores);
            $stock = $this->getStockItem($resultsIncWarehouse, $accessor, $storeCode, $otherStoresAttr);
            if (false == empty($stock)) {
                $stockCollection->addItem($stock);
            }
        } else {
            foreach ($resultsIncWarehouse as $resultIncWarehouse) {
                $otherStores = $this->getOtherStores($accessor, $resultIncWarehouse);
                $otherStoresAttr = $this->getOtherStoreAttributes($otherStores);
                $stock = $this->getStockItem($resultIncWarehouse, $accessor, $storeCode, $otherStoresAttr);
                if (false == empty($stock)) {
                    $stockCollection->addItem($stock);
                }
            }
        }

        return $stockCollection;
    }

    /**
     * Return stock object for product
     * @param $data
     * @param $accessor
     * @param $storeCode
     * @param $otherStoreArr
     * @return mixed
     */
    public function getStockItem($data, $accessor, $storeCode, $otherStoreArr)
    {
        $result = false;

        if ($data && $accessor && $storeCode && $otherStoreArr) {
            $result = Mage::getModel('stock/stock')
                ->setProductId($accessor->getValue($data, 'product_i_d'))
                ->setStoreId($storeCode)
                ->setItemCount($accessor->getValue($data, 'quantity_in_store'))
                ->setWarehouseCount($accessor->getValue($data, 'quantity_in_warehouse'))
                ->setBackorderCount($accessor->getValue($data, 'quantity_in_backorder'))
                ->setOtherStoreList($otherStoreArr);
        }

        return $result;
    }

    /**
     * Return store stock list item
     * @param $accessor
     * @param $data
     * @return mixed
     */
    protected function getOtherStores($accessor, $data)
    {
        $result = [];
        foreach ($accessor->getValue($data, 'other_store_stock_list.item') as $store) {
            if (empty($store['store_name'])) {
                continue;
            }
            $result[] = $store;
        }
        usort($result, function($a, $b) {
            return $a['store_name'] <=> $b['store_name'];
        });

        return $result;
    }

    /**
     * Return list of stores with stock
     * @param $data
     * @return array
     */
    public function getOtherStoreAttributes($data)
    {
        $result = false;

        if (is_array($data)) {
            if (array_key_exists('store', $data)) {
                $result[] = $data;
            } else {
                $result = $data;
            }
        }

        return $result;
    }

    /**
     * Retrun array with product SKU's
     * @param $productCollection
     * @return array
     */
    protected function getProductSkus($productCollection)
    {
        $productSkus = [];

        foreach ($productCollection as $product) {
            $productSkus[] = $product->getSku();
        }

        return $productSkus;
    }

    /**
     * Return accessor model
     * @return mixed
     */
    protected function getAccessor()
    {
        return Mage::getModel('omnius_service/dotAccessor');
    }

    /**
     * Return AXI store code
     * @return mixed
     */
    protected function getStoreCode()
    {
        return Mage::helper('agent')->getAxiStore();
    }
}
