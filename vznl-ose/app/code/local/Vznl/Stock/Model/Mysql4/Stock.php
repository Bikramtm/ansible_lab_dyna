<?php

/**
 * Class Vznl_Stock_Model_Mysql4_Stock
 */
class Vznl_Stock_Model_Mysql4_Stock extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init('stock/stock', 'entity_id');
    }
}
