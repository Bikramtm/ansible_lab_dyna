<?php

/**
 * Entity type model
 *
 * @category    Vznl
 * @package     Vznl_Eav
 * @author      Dynacommerce
 */
class Vznl_Eav_Model_Entity_Type extends Mage_Eav_Model_Entity_Type
{
    /**
     * Retreive new incrementId with a prefix
     *
     * @param int $storeId
     * @return string
     * @throws Exception
     */
    public function fetchNewIncrementId($storeId = null)
    {
        return Vznl_Checkout_Model_Sales_Order::INCREMENT_ID_PREFIX . parent::fetchNewIncrementId($storeId);
    }
}
