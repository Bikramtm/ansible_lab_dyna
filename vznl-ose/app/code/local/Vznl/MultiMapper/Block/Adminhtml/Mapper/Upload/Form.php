<?php

/**
 * Class Vznl_MultiMapper_Block_Adminhtml_Mapper_Upload_Form
 */
class Vznl_MultiMapper_Block_Adminhtml_Mapper_Upload_Form extends Omnius_MultiMapper_Block_Adminhtml_Mapper_Upload_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form([
                "id" => "upload_form",
                "action" => $this->getUrl("*/*/upload"),
                "method" => "post",
                "enctype" => "multipart/form-data",
            ]
        );
        $form->setUseContainer(true);
        $this->setForm($form);

        $fieldset = $form->addFieldset("multimapper_form", ["legend" => Mage::helper("multimapper")->__("MultiMapper File Info")]);

        $fieldset->addField('file', 'file', [
            'label' => Mage::helper('multimapper')->__('CSV or XLSX File'),
            'name' => 'file',
            "class" => "required-entry",
            "required" => true,
        ]);

        return $this;
    }
}
