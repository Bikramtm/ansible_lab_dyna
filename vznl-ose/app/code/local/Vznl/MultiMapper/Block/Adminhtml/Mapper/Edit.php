<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Vznl_MultiMapper_Block_Adminhtml_Mapper_Edit extends Omnius_MultiMapper_Block_Adminhtml_Mapper_Edit
{
    /**
     * Vznl_MultiMapper_Block_Adminhtml_Mapper_Edit constructor.
     */
    public function __construct()
    {

        parent::__construct();
        $this->_objectId = "entity_id";
        $this->_blockGroup = "multimapper";
        $this->_controller = "adminhtml_mapper";

        $request = $this->getRequest()->getParams();
        if (!isset($request["createStack"]) && !isset($request["id"])) {
            $this->removeButton("save");
            $this->removeButton("delete");
            $this->removeButton("saveandcontinue");
        } else {
            $this->_updateButton("save", "label", Mage::helper("multimapper")->__("Save Item"));
            $this->_updateButton("delete", "label", Mage::helper("multimapper")->__("Delete Item"));

            $this->_addButton("saveandcontinue", array(
                "label" => Mage::helper("multimapper")->__("Save And Continue Edit"),
                "onclick" => "saveAndContinueEdit()",
                "class" => "save",
            ), -100);
        }

        $this->_formScripts[] = "

                     function saveAndContinueEdit(){
                        editForm.submit($('edit_form').action+'back/edit/');
                     }
                  ";
    }

    /**
     * @return string
     */
    public function getHeaderText()
    {
        if (Mage::registry("mapper_data") && Mage::registry("mapper_data")->getId()) {

            return Mage::helper("multimapper")->__("Edit Item '%s'", $this->htmlEscape(Mage::registry("mapper_data")->getId()));

        } else {

            return Mage::helper("multimapper")->__("Add Item");

        }
    }
}