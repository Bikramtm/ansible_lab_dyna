<?php

/**
 * Class Vznl_MultiMapper_Model_Resource_Addon
 */
class Vznl_MultiMapper_Model_Resource_Addon extends Dyna_MultiMapper_Model_Resource_Addon
{
    public function _construct()
    {
        $this->_init("dyna_multimapper/addon", "entity_id");
    }
}
