<?php

/**
 * Class Vznl_MultiMapper_Model_Resource_Addon_Collection
 */
class Vznl_MultiMapper_Model_Resource_Addon_Collection extends Dyna_MultiMapper_Model_Resource_Addon_Collection
{
    public function _construct()
    {
        $this->_init("dyna_multimapper/addon");
    }
}
