<?php

/**
 * Class Vznl_MultiMapper_Model_XlsxImporter
 */
class Vznl_MultiMapper_Model_XlsxImporter extends Mage_Core_Model_Abstract
{
    /**
     * Log operations during import
     *
     * @param $msg
     */
    private function log($msg)
    {
        Mage::log($msg, null, 'multimapper_import_' . date('Y-m-d') . '.log');
    }

    /**
     * Import uploaded file into multimapper table
     *
     * @param $path string
     * @return bool
     * @throws Exception
     */
    public function import($path)
    {
        ini_set('memory_limit', '2048M');
        try {
            $objPHPExcel = $this->validateFileType($path);
            $worksheets = $objPHPExcel->getWorksheetIterator();
            foreach ($worksheets as $ws) {
                $this->parseWorksheet($ws);
            }
        } catch (Zend_Validate_Exception $e) {
            $this->log($e->getMessage());
            throw $e;
        } catch (Exception $e) {
            throw $e;
        }

        return true;
    }

    /**
     * Validate headers for the imported file
     *
     * @param $row PHPExcel_Worksheet_Row
     * @throws Exception
     * @returns boolean
     */
    protected function validateHeaders($row)
    {
        $columns = Mage::getModel('multimapper/mapper')->getColumns();
        $columnsR = Mage::getModel('multimapper/mapper')->getRecursiveColumns();
        for ($i = 1; $i <= 10; $i++) {
            foreach ($columnsR as $column) {
                $columns[] = $column . $i;
            }
        }
        foreach($columns as $column)
        {
            if (empty($columns)){
                throw new Exception(Mage::helper('multimapper')->__('Invalid column "'.$column.'" found'));
            }
        }
        return true;
    }

    /**
     * Validate type of file and return the PHP object
     *
     * @param $path string
     * @return PHPExcel
     * @throws Exception
     */
    protected function validateFileType($path)
    {
        $objReader = PHPExcel_IOFactory::createReader('Excel2007');
        $objReader->setReadDataOnly(true);
        if (!$objReader->canRead($path)) {
            throw new Zend_Validate_Exception(Mage::helper('multimapper')->__('Invalid XLSX file provided'));
        }

        return $objReader->load($path);
    }

    /**
     * @param $row PHPExcel_Worksheet_Row
     * @return array
     * @throws Exception
     */
    protected function getHeaderMapping($row)
    {
        $result = [];
        /** @var PHPExcel_Worksheet_RowCellIterator $cells */
        $cells = $row->getCellIterator();
        do {
            if(mb_strtolower(trim($cells->current()->getValue())) !== '') {
                $result[] = mb_strtolower(trim($cells->current()->getValue()));
            }
            $cells->next();
        } while ($cells->valid());

        return $result;
    }

    /**
     * Parse one row of the imported file and process the data
     *
     * @param $row PHPExcel_Worksheet_Row
     * @param $mappings array
     * @throws Exception
     */
    protected function parseRow($row, $mappings)
    {
        try {
            /** @var PHPExcel_Worksheet_RowCellIterator $cells */
            $cells = $row->getCellIterator();
            $index = 0;
            $rowData = [];
            $countMappings = count($mappings);
            do {
                $rowData[$mappings[$index]] = trim($cells->current());
                ++$index;
                $cells->next();
                if($index > $countMappings){
                    break;
                }
            } while ($cells->valid());
            /** @var Dyna_MultiMapper_Model_Mapper $model */
            $model = Mage::getResourceModel('multimapper/mapper_collection')
                ->addFieldToFilter('pp_sku', $rowData['pp_sku'])
                ->addFieldToFilter('promo_sku', $rowData['promo_sku'])
                ->getFirstItem();
            $model->addData($rowData);
            if (!$model->getId()) {
                $model->validateData();
            }
            $model->save();
            unset($model);
            unset($cells);
        } catch (Zend_Validate_Exception $e) {
            $msg = sprintf(Mage::helper('multimapper')->__("For priceplan sku \"%s\" and promo sku \"%s\" combination the following error was received: %s"), $rowData['pp_sku'], $rowData['promo_sku'], $e->getMessage());
            $this->log($msg);
            Mage::getSingleton('adminhtml/session')->addError($msg);
        } catch (Exception $e) {
            $this->log($e->getMessage());
            throw $e;
        }
    }

    /**
     * Process worksheet
     *
     * @param $ws PHPExcel_Worksheet
     */
    protected function parseWorksheet($ws)
    {
        $rows = $ws->getRowIterator();
        $headerRow = $rows->current();
        $columns = $this->validateHeaders($headerRow);
        $mappings = $this->getHeaderMapping($headerRow);
        $rows->next();
        while ($rows->valid()) {
            $this->parseRow($rows->current(), $mappings);
            $rows->next();
        }
    }
}
