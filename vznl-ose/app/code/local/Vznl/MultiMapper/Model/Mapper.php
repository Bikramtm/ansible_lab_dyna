<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Vznl_MultiMapper_Model_Mapper
 * @method setStack($string) Set stack for current mapper
 */
class Vznl_MultiMapper_Model_Mapper extends Dyna_MultiMapper_Model_Mapper
{
    CONST DIRECTION_IN = 'in';
    CONST DIRECTION_OUT = 'out';
    CONST DIRECTION_BOTH = 'both';

    protected function _construct()
    {
        $this->_init("multimapper/mapper");
        $this->getSessionProcessContextId();
    }

    /** @var Dyna_Cache_Model_Cache */
    protected $_cache;
    private $sessionProcessContextId;

    /**
     * Get a list of records for a given article number
     * @param $soc
     * @return mixed
     */
    public function getSkusForSoc($soc, $productSkus = [])
    {
        /**
         * @var $addonModel Dyna_MultiMapper_Model_Addon
         */
        $addonModel = Mage::getModel("dyna_multimapper/addon");
        $serviceExpressionObjects = array(
            'customer' => Mage::getModel('dyna_configurator/expression_customer'),
            'order' => Mage::getModel('vznl_configurator/expression_order')
        );
        $dynaCoreHelper = Mage::helper('dyna_core');
        $cache_key = sprintf('multimapper_addons_for_soc_%s_%s', md5($soc), $this->sessionProcessContextId);
        if (!($addons = unserialize($this->getCache()->load($cache_key)))) {
            $addons = $addonModel->getAddonsByTechnicalId($soc);
            $this->getCache()->save(serialize($addons), $cache_key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
        }

        $mapperIds = [];
        foreach ($addons as $addon) {
            if(!$this->checkServiceExpresion($addon, $dynaCoreHelper, $serviceExpressionObjects)){
                continue;
            }

            $mapperIds[] = $addon->getMapperId();
        }
        $mappers = $this->getMappersWithInDirection($mapperIds);
        $skus = [];
        foreach ($addons as $addon) {
            foreach ($mappers as $mapper) {
                if(!$this->checkServiceExpresion($mapper, $dynaCoreHelper, $serviceExpressionObjects)){
                    continue;
                }

                if ($addon->getMapperId() == $mapper->getId()) {
                    if(in_array($mapper->getSku(), $productSkus)) {
                        continue;
                    }
                    $skus[] = trim($mapper->getSku());
                    return $skus;
                }
            }
        }
        return $skus;
    }

    /**
     * Check if addon or mapper have expressions and validate them if needed
     * @param $mapperOrAddon
     * @param $dynaCoreHelper
     * @param $serviceExpressionObjects
     * @return bool
     */
    private function checkServiceExpresion($mapperOrAddon, $dynaCoreHelper, $serviceExpressionObjects)
    {
        if($ssExpression = $mapperOrAddon->getServiceExpression()){
            $passed = $dynaCoreHelper->evaluateExpressionLanguage($ssExpression, $serviceExpressionObjects);
            return $passed;
        }

        return true;
    }

    /**
     *  OMNVFDE-2582: Return only IN and BOTH multimappers to 360 View
     * The In direction mappers should not be filtered with the process context ID
     * since these are used at the panel display
     *
     * @param $mapperIds
     * @return mixed
     */
    public function getMappersWithInDirection($mapperIds)
    {
        if(!$mapperIds) {
            return null;
        }
        $cache_key = sprintf('multimappers_for_given_ids_%s', md5(implode(',', $mapperIds)));
        $cachedValues = $this->getCache()->load($cache_key);
        if (!$cachedValues) {
            // OMNVFDE-2582: Return only IN and BOTH multimappers to 360 View
            $mappersCollection = $this->getCollection();

            $mappersCollection->addFieldToFilter('entity_id', array('in' => $mapperIds))
                ->addFieldToFilter('direction', array('in' => array('in', 'both')))
                ->getItems();

            $this->getCache()->save(serialize($mappersCollection->exportToArray()), $cache_key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
        } else {
            $mappersCollection = $this->getCollection();
            $mappersCollection->setIsLoaded(true);

            $mappersArray = unserialize($cachedValues);
            $mappersCollection->importFromArray($mappersArray);
        }

        return $mappersCollection;
    }

    /**
     * @param $quoteSkus
     * @return mixed
     */
    public function getMappersBySkusWithOutDirection($quoteSkus)
    {
        $mappers = $this->getCollection();

        return $mappers->addFieldToFilter('direction', array('in' => ['out', 'both']))
            ->addFieldToFilter('sku', array('in' => $quoteSkus));
    }

    /**
     * Get entity id of first mapper from collection
     * @param $sku
     * @return string
     */
    public function getEntityIdBySku($sku)
    {
        $record = $this->getMappersBySku($sku)->getFirstItem();
        if ($record->getId()) {
            return $record->getData('entity_id');
        }
        return null;
    }

    /**
     * Get multimapper records by SKU and optionally by service expression
     * @param $sku
     * @param $service_expression
     * @param $import
     * @return mixed
     */
    public function getMappersBySku($sku, $service_expression = null, $import = false)
    {
        $collection = $this->getCollection();

        if (is_array($sku)) {
            $collection->addFieldToFilter('sku', array('in' => $sku));
        } else {
            $collection->addFieldToFilter('sku', $sku);
        }

        // OMNVFDE-2582: If we're not importing multimappers, Return only OUT and BOTH multimappers to Submit Order
        if (!$import) {
            $collection->addFieldToFilter('direction', array('in' => array('out', 'both')));
        }
        if ($service_expression) {
            $collection->addFieldToFilter('service_expression', $service_expression);
        } elseif ($import) {
            $collection->addFieldToFilter('service_expression', array('null' => true));
        }

        return $collection;
    }

    public function getMappersBySkus($skus)
    {
        $collection = $this->getCollection();

        return $collection->addFieldToFilter('sku', array('in' => $skus));
    }

    public function getTechnicalIdWithDirection($sku, $direction, $processcontextId = null)
    {
         return $this->getCollection()
                ->addFieldToFilter('main_table.sku',$sku)
                ->addFieldToFilter('main_table.direction', array('IN' => ['both', $direction]))
                ->join(array('addons' => 'dyna_multimapper/addon'), 'main_table.entity_id = addons.mapper_id', array('addons.technical_id'))
                ->join(array('processcontext' => 'dyna_multimapper/addonProcessContext'),
                'processcontext.addon_id = addons.entity_id', array('processcontext.process_context_id'))
                ->addFieldToFilter('processcontext.process_context_id', $processcontextId);
    }

    public function getSingleMappersBySku($sku)
    {
        return $this->getMappersBySku($sku)->getFirstItem();
    }

    /**
     * @return Dyna_Cache_Model_Cache
     */
    protected function getCache()
    {
        if (!$this->_cache) {
            $this->_cache = Mage::getSingleton('dyna_cache/cache');
        }

        return $this->_cache;
    }

    /**
     * Validate data for the current model
     */
    public function validateData()
    {
        $this->validateSku($this->getData('pp_sku'));
        if ($this->hasData('promo_sku') && $this->getData('promo_sku')) {
            $this->validateSku($this->getData('promo_sku'));
        }

        return $this;
    }

    /**
     * Validate there is a product with this sku
     *
     * @param $sku string
     * @throws Exception
     */
    private function validateSku($sku)
    {
        if (Mage::getModel('catalog/product')->getIdBySku($sku) === false) {
            throw new Zend_Validate_Exception(Mage::helper('multimapper')->__('Invalid sku provided'));
        }
    }

    private function getSessionProcessContextId()
    {
        /** @var Dyna_Catalog_Helper_ProcessContext $processContextHelper */
        $processContextHelper = Mage::helper("dyna_catalog/processContext");
        $this->sessionProcessContextId = $processContextHelper->getProcessContextId();
    }

    /**
     * Return a list of the simple columns
     *
     * @return array
     */
    public function getColumns()
    {
        return [
            "pp_sku",
            "pp_sku_description",
            "promo_sku",
            "promo_sku_description",
            "oms_bo_code",
            "oms_bo_type",
            "oms_pbo_code",
            "oms_pbo_type",
            "oms_product_offering_code",
            "oms_product_offering_id",
            "oms_product_code",
            "oms_mc_code",
            "oms_comp_code_path",
            "oms_comp_id",
        ];
    }

    /**
     * Return a list of the recursive columns
     * @return array
     */
    public function getRecursiveColumns()
    {
        return [
            "oms_component_attr_",
            "oms_attr_code_",
            "oms_attr_value_",
        ];
    }

    /**
     * Get multimapper records by ExtrenalId and optionally by service expression
     * @param $ExternalId
     * @param $oms_attr_value_1
     * @param $service_expression
     * @return array
     */
    public function getSkuByExternalID($ExternalId, $oms_attr_value_1 = null, $service_expression = null)
    {
        $collection = $this->getCollection();

        if (is_array($ExternalId)) {
            $collection->addFieldToFilter('oms_bo_code', array('in' => $ExternalId));
        } else {
            $collection->addFieldToFilter('oms_bo_code', $ExternalId);
        }
        if ($oms_attr_value_1) {
            $collection->addFieldToFilter('oms_attr_value_1', $oms_attr_value_1);
        }
        $collection->addFieldToFilter('direction', array('IN' => ['both', 'in']));

        if ($service_expression) {
            $collection->addFieldToFilter('service_expression', $service_expression);
        }

        return $collection->getFirstItem();
    }

    /**
     * Get multimapper records by bomId
     * @param $bomId
     * @return array
     */
    public function getSkuOnBomId($bomId, $limit)
    {
        $collection = $this->getCollection();

        if ($bomId) {
            $collection->addFieldToFilter('bom_id', $bomId)
                ->addFieldToFilter('direction', 'in')
                ->addFieldToSelect('sku')
                ->addFieldToSelect('priority')
                ->addFieldToSelect('service_expression')
                ->addFieldToSelect('direction')
                ->addFieldToSelect('bom_id');
        }
        $collection->getSelect()->order('priority ASC');
        $collection->getSelect()->limit($limit);
        return $collection;
    }

    /**
     * Get multimapper records by SKU and optionally by service expression
     * @param $uniqueRecord
     * @param $import
     * @return mixed
     */
    public function getMapperByUniqueIndex($uniqueRecord = array(), $path)
    {
        $collection = $this->getCollection();

        if(strpos($path,"Mobile") !== false) {
            $collection->addFieldToFilter('pp_sku', $uniqueRecord['pp_sku'])
                ->addFieldToFilter('promo_sku', $uniqueRecord['promo_sku'])
                ->addFieldToFilter('oms_attr_value_1', $uniqueRecord['oms_attr_value_1'])
                ->addFieldToFilter('direction', $uniqueRecord['direction'])
                ->addFieldToFilter('oms_bo_code', $uniqueRecord['oms_bo_code'])
                ->addFieldToFilter('oms_comp_id', $uniqueRecord['oms_comp_id'])
                ->addFieldToFilter('oms_product_offering_id', $uniqueRecord['oms_product_offering_id']);
        } elseif(strpos($path,"Fixed") !== false) {
            $collection->addFieldToFilter('sku', $uniqueRecord['sku'])
                ->addFieldToFilter('direction', $uniqueRecord['direction'])
                ->addFieldToFilter('bom_id', $uniqueRecord['bom_id'])
                ->addFieldToFilter('service_expression', $uniqueRecord['service_expression']);
        }

        return $collection;
    }

    public function getMappersBySoc($soc)
    {
        /**
         * @var $addonModel Dyna_MultiMapper_Model_Addon
         */
        $addonModel = Mage::getModel("vznl_multimapper/addon");
        $cache_key = sprintf('multimapper_addons_for_soc_%s_%s', md5($soc), $this->sessionProcessContextId);
        if (!($addons = unserialize($this->getCache()->load($cache_key)))) {
            $addons = $addonModel->getAddonsByTechnicalId($soc);
            $this->getCache()->save(serialize($addons), $cache_key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
        }
        $mapperIds = [];
        foreach ($addons as $addon) {
            $mapperIds[] = $addon->getMapperId();
        }
        return $mappers = $this->getMappersWithInDirection($mapperIds);
    }
}
