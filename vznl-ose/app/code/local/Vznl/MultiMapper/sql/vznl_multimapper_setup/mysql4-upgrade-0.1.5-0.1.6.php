<?php
$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer->startSetup();
$connection = $installer->getConnection();

$connection->addColumn($this->getTable('multimapper/mapper'),
    'service_expression',
    array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'comment' => 'Service expression',
        'required' => false,
        'nullable' => true
    )
);

$connection->addColumn($this->getTable('multimapper/mapper'),
    'priority',
    array(
        'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'comment' => 'MultiMapper priority',
        'length' => 11
    )
);

$connection->addColumn($this->getTable('multimapper/mapper'),
    'stop_execution',
    array(
        'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'comment' => 'Stop Execution',
        'length' => 1
    )
);

$connection->addColumn($this->getTable('multimapper/mapper'),
    'comment',
    array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'comment' => 'comment',
        'length' => 500,
        'required' => false,
        'nullable' => true
    )
);

$connection->addColumn($this->getTable('multimapper/mapper'),
    'sku',
    array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'comment' => 'SKU',
        'length' => 255 ,
        'required' => false,
        'nullable' => true
    )
);

$connection->addColumn($this->getTable('multimapper/mapper'),
    'xpath_outgoing',
    array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'comment' => 'xpath_outgoing',
        'length' => 255,
        'required' => false,
        'nullable' => true
    )
);

$connection->addColumn($this->getTable('multimapper/mapper'),
    'xpath_incomming',
    array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'comment' => 'xpath_incomming',
        'comment' => 'xpath_incomming',
        'length' => 255,
        'required' => false,
        'nullable' => true
    )
);

$connection->addColumn($this->getTable('multimapper/mapper'),
    'component_type',
    array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'comment' => 'Component type field used by FN',
        'required' => false,
        'nullable' => true
    )
);

$connection->addColumn($this->getTable('multimapper/mapper'),
    'direction',
    array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'comment' => 'Direction used to differentiate Reverse Multimappers',
        'length' => 4,
        'required' => false,
        'default' => 'both'
    )
);

$connection->addColumn($this->getTable('multimapper/mapper'),
    'defaulted',
    array(
        'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'comment' => 'manually or automatically by an agent',
        'length' => 1,
        'required' => false
    )
);

$connection->addColumn($this->getTable('multimapper/mapper'),
    'ogw_component_name',
    array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'comment' => 'OGW Component Name',
        'length' => 255,
        'required' => false,
        'nullable' => true
    )
);

$connection->addColumn($this->getTable('multimapper/mapper'),
    'source_component_id',
    array(
        'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'comment' => 'Source Component ID',
        'length' => 10,
        'required' => false,
        'nullable' => true
    )
);

$connection->addColumn($this->getTable('multimapper/mapper'),
    'system_name',
    array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'comment' => 'system_name',
        'length' => 255,
        'required' => false,
        'nullable' => true
    )
);

$connection->addColumn($this->getTable('multimapper/mapper'),
    'element_id',
    array(
        'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'comment' => 'element_id',
        'length' => 10,
        'required' => false,
        'nullable' => true
    )
);

$connection->addColumn($this->getTable('multimapper/mapper'),
    'ogw_component_id_reference',
    array(
        'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'comment' => 'OGW Component ID Reference',
        'length' => 10,
        'required' => false,
        'after' => 'promo_sku_description',
        'nullable' => true
    )
);

$connection->addColumn($this->getTable('multimapper/mapper'),
    'stack',
    array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'comment' => 'Stack',
        'length' => 10,
        'required' => false,
        'nullable' => true
    )
);

$this->getConnection()->addIndex($this->getTable('multimapper/mapper'), "IDX_DYNA_MULTI_MAPPER_DIRECTION", ['direction'], Varien_Db_Adapter_Interface::INDEX_TYPE_INDEX);
$this->getConnection()->addIndex($this->getTable('multimapper/mapper'), "IDX_SKU_SERVICE_EXPRESSION_RULE_TITLE_EXPORT_INDEXER", ['sku'], Varien_Db_Adapter_Interface::INDEX_TYPE_INDEX);

$installer->endSetup();