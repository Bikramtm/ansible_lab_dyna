<?php
/** @var Mage_Core_Model_Resource_Setup $this */
$this->startSetup();
$tableName = $this->getTable('multimapper/mapper');
// Define columns and indexes
$columnsToDrop = [
    "comment",
    "promo_id",
    "priceplan_id",
    "addon%_desc",
    "addon%_name",
    "addon%_nature",
    "addon%_soc",
];
$indexesToDrop = [
    "IDX_DYNA_MULTIMAPPER_PRICEPLAN_ID_PROMO_ID",
    "IDX_DYNA_MULTIMAPPER_PRICEPLAN_ID",
];

$columnsToAdd = [
    "pp_sku_description",
    "promo_sku_description",
    "oms_bo_type",
    "oms_pbo_code",
    "oms_pbo_type",
    "oms_product_offering_code",
    "oms_product_offering_id",
    "oms_product_cod",
    "oms_mc_code",
    "oms_comp_code_path",
    "oms_comp_id",
    "oms_component_attr_%",
    "oms_attr_code_%",
    "oms_attr_value_%",
];

// Remove old indexes
foreach ($indexesToDrop as $indexToDrop) {
    $this->getConnection()->dropIndex($tableName, $indexToDrop);
}

// Remove old unused fields
foreach ($columnsToDrop as $columnToDrop) {
    if (strpos($columnToDrop, "%") !== false) {
        for ($i = 1; $i <= 10; $i++) {
            $this->getConnection()->dropColumn($tableName, str_replace("%", $i, $columnToDrop));
        }
    } else {
        $this->getConnection()->dropColumn($tableName, $columnToDrop);
    }
}

// Add the new columns
foreach ($columnsToAdd as $columnToAdd) {
    if (strpos($columnToAdd, "%") !== false) {
        for ($i = 1; $i <= 10; $i++) {
            $this->getConnection()->addColumn($tableName, str_replace("%", $i, $columnToAdd), 'VARCHAR(255)');
        }
    } else {
        $this->getConnection()->addColumn($tableName, $columnToAdd, 'VARCHAR(255)');
    }
}

// If the column pp_sku has old name, rename it
if ($this->getConnection()->tableColumnExists($tableName, 'priceplan_sku')) {
    $this->getConnection()->changeColumn($tableName, 'priceplan_sku', 'pp_sku', 'VARCHAR(64)');
} elseif (!$this->getConnection()->tableColumnExists($tableName, 'pp_sku')) {
    // If it was not renamed, create it
    $this->getConnection()->addColumn($tableName, 'pp_sku', 'VARCHAR(64)');
}

// Add the new indexes
$this->getConnection()->addIndex($tableName, "IDX_DYNA_MULTIMAPPER_PRICEPLAN_SKU_PROMO_SKU", ['promo_sku', 'pp_sku'], Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE);
$this->getConnection()->addIndex($tableName, "IDX_DYNA_MULTIMAPPER_PRICEPLAN_SKU", ['pp_sku'], Varien_Db_Adapter_Interface::INDEX_TYPE_INDEX);

$this->endSetup();
