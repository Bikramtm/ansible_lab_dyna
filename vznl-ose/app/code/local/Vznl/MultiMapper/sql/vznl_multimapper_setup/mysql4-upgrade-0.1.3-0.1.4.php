<?php
/** @var Mage_Core_Model_Resource_Setup $this */
$this->startSetup();
$tableName = $this->getTable('multimapper/mapper');

$columnsToUpdate = [
    "oms_bo_type",
    "oms_pbo_code",
    "oms_pbo_type",
    "oms_product_offering_code",
    "oms_product_offering_id",
    "oms_product_cod",
    "oms_mc_code",
    "oms_comp_code_path",
    "oms_comp_id",
    "oms_component_attr_%",
    "oms_attr_code_%",
    "oms_attr_value_%",
];

// Update the columns
foreach ($columnsToUpdate as $columnToUpdate) {
    if (strpos($columnToUpdate, "%") !== false) {
        for ($i = 1; $i <= 10; $i++) {
            $this->getConnection()->changeColumn($tableName, str_replace("%", $i, $columnToUpdate), str_replace("%", $i, $columnToUpdate), 'VARCHAR(80)');
        }
    } else {
        $this->getConnection()->changeColumn($tableName, $columnToUpdate, $columnToUpdate, 'VARCHAR(80)');
    }
}

$this->endSetup();
