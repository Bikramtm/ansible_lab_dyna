<?php

require_once Mage::getModuleDir('controllers', 'Dyna_MultiMapper') . DS . 'Adminhtml' . DS . 'MapperController.php';

/**
 * Class Vznl_MultiMapper_Adminhtml_MapperController
 */
class Vznl_MultiMapper_Adminhtml_MapperController extends Dyna_MultiMapper_Adminhtml_MapperController
{
    private $adminSession = null;

    /**
     * Upload and run import file
     */
    public function uploadAction()
    {
        if ($this->getRequest()->getPost()) {
            if (isset($_FILES['file']['tmp_name'])) {
                try {
                    $this->parseFile();
                } catch (Exception $e) {
                    Mage::getSingleton('adminhtml/session')->addError(
                        $this->__($e->getMessage())
                    );
                    $this->_redirect('*/*');
                }
            }
        } else {
            Mage::getSingleton('adminhtml/session')->addError(
                $this->__('Invalid POST data (please check post_max_size and upload_max_filesize settings in your php.ini file).')
            );
            $this->_redirect('*/*');
        }
    }

    protected function parseFile()
    {
        if ($file = $_FILES['file']['tmp_name']) {
            $uploader = new Mage_Core_Model_File_Uploader('file');
            $uploader->setAllowedExtensions(array('csv', 'xlsx'));
            $path = Mage::app()->getConfig()->getTempVarDir() . '/import/';
            $uploader->save($path);
            if ($uploadFile = $uploader->getUploadedFileName()) {
                $newFilename = 'import-' . date('YmdHis') . '_' . $uploadFile;
                rename($path . $uploadFile, $path . $newFilename);
            }
        }

        if (isset($newFilename) && $newFilename) {
            /** @var Vznl_MultiMapper_Model_XlsxImporter | Omnius_MultiMapper_Model_Importer $importer */
            $importer = $uploader->getFileExtension() == 'xlsx' ? Mage::getModel('vznl_multimapper/xlsxImporter') : $importer = Mage::getModel('multimapper/importer');
            $success = $importer->import($path . $newFilename);
            if ($success) {
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    $this->__('Import file successfully parsed and imported into database')
                );
            }
            $this->_redirect('*/*');
        }
    }

    /**
     *  Export order grid to Excel XML format
     */
    public function exportExcelAction()
    {
        $fileName = 'MultiMapper-' . date("Ymdhis") . '.xlsx';
        /** @var Dyna_MultiMapper_Block_Adminhtml_Mapper_Grid $grid */
        $grid = $this->getLayout()->createBlock('vznl_multimapper/adminhtml_mapper_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }

    public function editAction()
    {
        $this->_title($this->__("MultiMapper"));
        $this->_title($this->__("Mapper"));
        $this->_title($this->__("Edit Item"));

        $id = $this->getRequest()->getParam("id");

        $collection = Mage::getModel("multimapper/mapper")->getCollection()->addFieldToFilter('main_table.entity_id', $id);

        $collection->getSelect()
            ->joinLeft(
                array('dyna_multi_mapper_addons'=>'dyna_multi_mapper_addons'),
                'dyna_multi_mapper_addons.mapper_id = main_table.entity_id',
                array('addonID' => 'dyna_multi_mapper_addons.entity_id','technical_id' => 'dyna_multi_mapper_addons.technical_id','nature_code' => 'dyna_multi_mapper_addons.nature_code','backend' => 'dyna_multi_mapper_addons.backend','technical_id_2' => 'dyna_multi_mapper_addons.technical_id_2','nature_code_2' => 'dyna_multi_mapper_addons.nature_code_2','technical_id_3' => 'dyna_multi_mapper_addons.technical_id_3','nature_code_3' => 'dyna_multi_mapper_addons.nature_code_3')
            )
            ->joinLeft(
                array('dyna_multi_mapper_index_process_context'=>'dyna_multi_mapper_index_process_context'),
                'dyna_multi_mapper_index_process_context.addon_id = dyna_multi_mapper_addons.entity_id',
                array('processContextAddonID' => 'dyna_multi_mapper_index_process_context.addon_id','processContextID' => 'dyna_multi_mapper_index_process_context.process_context_id')
            )
            ->joinLeft(
                array('process_context'=>'process_context'),
                'process_context.entity_id = dyna_multi_mapper_index_process_context.process_context_id',
                array('name' => 'process_context.name')
            )
            ->group('main_table.entity_id');

        $model = $collection->getFirstItem();

        if ($model->getEntityId()) {
            Mage::register("mapper_data", $model);
            $this->loadLayout();
            $this->_setActiveMenu("multimapper/mapper");
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Mapper Manager"), Mage::helper("adminhtml")->__("Mapper Manager"));
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Mapper Description"), Mage::helper("adminhtml")->__("Mapper Description"));
            $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock("multimapper/adminhtml_mapper_edit"))->_addLeft($this->getLayout()->createBlock("multimapper/adminhtml_mapper_edit_tabs"));
            $this->renderLayout();
        } else {
            Mage::getSingleton("adminhtml/session")->addError(Mage::helper("multimapper")->__("Item does not exist."));
            $this->_redirect("*/*/");
        }
    }

    /**
     * Save mapper addon data
     */
    public function saveAction()
    {
        $post_data = $this->getRequest()->getPost();
        $isLoaded = false;
        if ($this->getRequest()->getParam("id")) {
            $isLoaded = true;
        }

        if ($post_data) {
            try {
                /** Unlink addons from mapper data */
                /** @var $multiMapperModel Dyna_MultiMapper_Model_Mapper */
                $multiMapperModel = Mage::getModel("multimapper/mapper");
                $mapper = $this->getRequest()->getParam("id") ?  $multiMapperModel->load($this->getRequest()->getParam("id")) : $multiMapperModel;
                $newAddons = [];
                $addonProcessContexts = [];

                $i=1;
                while (true) {
                    if (array_key_exists("addon" . $i . "_technical_id", $post_data)) {
                        $newAddons["addon" . $i . "_id"] = [
                            "mapper_id" => $mapper->getId(),
                            "addon_id" => !empty($post_data["addon" . $i . "_id"]) ? (int)$post_data["addon" . $i . "_id"] : null,
                            "addon_name" => !empty($post_data["addon" . $i . "_name"]) ? trim($post_data["addon" . $i . "_name"]) : "",
                            "addon_value" => !empty($post_data["addon" . $i . "_value"]) ? trim($post_data["addon" . $i . "_value"]) : "",
                            "addon_additional_value" => !empty($post_data["addon" . $i . "_additional_value"]) ? trim($post_data["addon" . $i . "_additional_value"]) : "",
                            "nature_code" => !empty($post_data["addon" . $i . "_nature_code"]) ? trim($post_data["addon" . $i . "_nature_code"]) : "",
                            "promo_id" => !empty($post_data["addon" . $i . "_promo_id"]) ? trim($post_data["addon" . $i . "_promo_id"]) : "",
                            "technical_id" => !empty($post_data["addon" . $i . "_technical_id"]) ? trim($post_data["addon" . $i . "_technical_id"]) : "",
                            "backend" => !empty($post_data["addon" . $i . "_backend"]) ? trim($post_data["addon" . $i . "_backend"]) : "",
                        ];
                        $addonProcessContexts[$post_data['addon' . $i . '_id']] = $post_data["addon" . $i . "_process_context"] ?? [];
                        unset($post_data["addon" . $i . "_id"]);
                        unset($post_data["addon" . $i . "_name"]);
                        unset($post_data["addon" . $i . "_value"]);
                        unset($post_data["addon" . $i . "_additional_value"]);
                        unset($post_data["addon" . $i . "_nature_code"]);
                        unset($post_data["addon" . $i . "_promo_id"]);
                        unset($post_data["addon" . $i . "_technical_id"]);
                        unset($post_data["addon" . $i . "_backend"]);
                        unset($post_data["addon" . $i . "_process_context"]);
                        $i++;
                    } else {
                        break;
                    }
                }

                if (isset($post_data["sku"])) {
                    $post_data["stack"] = "fixed";
                }
                /** Saving multiMapper model */
                $mapper->addData($post_data)
                    ->save();

                /** Going further to saving addons */
                $this->_saveNewAddon($newAddons, $mapper, $addonProcessContexts);               

                if (isset($post_data["stack"]) && $post_data["stack"] == "fixed") {
                    if ($isLoaded) {
                        $addon = Mage::getModel("dyna_multimapper/addon")->load($mapper->getId(), "mapper_id");
                    } else {
                        $addon = Mage::getModel("dyna_multimapper/addon");
                    }

                    $addon->setStack($post_data["stack"]);
                    $addon->setData('mapper_id', $mapper->getId());
                    $addon->setData('technical_id', $post_data['technical_id']);
                    $addon->setData('nature_code', $post_data['nature_code']);
                    $addon->setData('backend', $post_data['backend']);
                    $addon->setData('technical_id_2', $post_data['technical_id_2']);
                    $addon->setData('nature_code_2', $post_data['nature_code_2']);
                    $addon->setData('technical_id_3', $post_data['technical_id_3']);
                    $addon->setData('nature_code_3', $post_data['nature_code_3']);

                    $processContextIds = array_values($post_data['name']);

                    $addon->save();
                    $addon->setProcessContextsIds($processContextIds);


                }

                $this->getAdminSession()->addSuccess(Mage::helper("adminhtml")->__("Mapper was successfully saved"));
                $this->getAdminSession()->setMapperData(false);

                if ($this->getRequest()->getParam("back")) {
                    $this->_redirect("*/*/edit", array("id" => $mapper->getId()));

                    return;
                }
                $this->_redirect("*/*/");

                return;
            } catch (Exception $e) {
                $this->getAdminSession()->addError($e->getMessage());
                $this->getAdminSession()->setMapperData($this->getRequest()->getPost());
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));

                return;
            }

        }
        $this->_redirect("*/*/");
    }
    
    protected function _saveNewAddon($newAddons, $mapper, $addonProcessContexts)
    {
    	foreach ($newAddons as $newAddonData) {
    		foreach ($newAddonData as $key => $value) {
    			if ($key != 'mapper_id' && !empty(trim($value))) {
    				// Save the addon if at least one field contains a value
    				/** @var Dyna_MultiMapper_Model_Addon $addon */
    				$addon = Mage::getModel("dyna_multimapper/addon")
    				    ->setId($newAddonData['addon_id'])
    				    ->addData($newAddonData)
    				    ->setMapperId($mapper->getId()); // get mapper id for new entries
    				$addon->save();
    				$addon->setProcessContextsIds($addonProcessContexts[$addon->getId()]);
    				break;
    			}
    		}
    	}
    }
}
