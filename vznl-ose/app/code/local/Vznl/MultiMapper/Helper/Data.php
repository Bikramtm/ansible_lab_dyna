<?php

class Vznl_MultiMapper_Helper_Data extends Dyna_MultiMapper_Helper_Data
{
    /** @var Dyna_Configurator_Model_Cache */
    protected $cache;

    /**
     * @return Dyna_Configurator_Model_Cache
     */
    protected function getCache()
    {
        if (!$this->cache) {
            return $this->cache = Mage::getSingleton('vznl_configurator/cache');
        }

        return $this->cache;
    }

    public function validateSocCodesByPackageFromQuote($quote, $packageNr)
    {
        return $this->validateSocCodes($quote->getAllItems(), $packageNr);
    }

    public function validateSocCodesByPackageFromOrder($order, $packageNr)
    {
        return $this->validateSocCodes($order->getAllItems(), $packageNr);
    }

    public function getReasonCodeBySubscription($subscription, $customer)
    {
        $customerValueOptionId = $customer->getCustomerValue();
        $subscriptonReasoncodeTypeOptionId = $subscription->getProduct()->getData(Dyna_Catalog_Model_Product::REASONCODE_TYPE_ATTRIBUTE);

        if (!$customerValueOptionId) // No value know, use value WA6A
        {
            $attribute = Mage::getModel('eav/config')->getAttribute('customer', 'customer_value');
            $customerValueOptionId = $attribute->getSource()->getOptionId('WA6A');
        }

        $reasonCodeMatch = Mage::getModel('multimapper/reasoncode')->getCollection()
            ->addFieldToFilter('customer_value_option_id', $customerValueOptionId)
            ->addFieldToFilter('catalog_product_reasoncode_option_id', $subscriptonReasoncodeTypeOptionId)
            ->getLastItem();

        if ($reasonCodeMatch && $reasonCodeMatch->getId()) {
            return $reasonCodeMatch->getReasonCode();
        } else {
            return '';
        } // Or throw exception?
    }

    protected function validateSocCodes($items, $packageNr)
    {
        $catHelper = Mage::helper('dyna_catalog');
        $packages = [];
        $promoProductIds = [];

        /** @var Mage_Sales_Model_Order $order */
        foreach ($items as $item) {
            $packages[$item->getPackageId()]['items'][] = $item;
            if ($item->isPromo()) {
                $promoProductIds[] = $item->getProduct()->getId();
            }
        }
        if (!$packages || !array_key_exists($packageNr, $packages)) {
            return '';
        } // No package found

        $promoProductCollection = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToFilter('entity_id', ['in' => $promoProductIds])
            ->addAttributeToSelect('promo_ignore_multimapper');

        $package = $packages[$packageNr];
        $networkProductId = 0;
        $promoProductId = 0;
        $rcProductId = 0;
        $rcPromoProductId = 0;
        $commercialAddonIds = [];
        $this->buildPackageData($package, $catHelper, $commercialAddonIds, $promoProductCollection, $networkProductId, $rcProductId, $rcPromoProductId, $promoProductId);
        if ($networkProductId) {
            $mapping = $this->getMappingInstanceById($networkProductId, $promoProductId);
            if ($rcProductId) {
                $rcMapping = $this->getMappingInstanceById($rcProductId, $rcPromoProductId);
            }
            if ($mapping && $mapping->getEntityId()) {
                if (isset($rcMapping) && !($rcMapping && $rcMapping->getEntityId())) {
                    //throw new LogicException("Product missing SOC mapping! Product ID: " . $rcProductId); // TODO Best way?
                    $dummy = ''; // dummy statement to avoid code sniffer warning
                }
            } else {
                if (!Mage::helper('dyna_service')->useStubs()) {
                   // throw new LogicException("Product missing SOC mapping! Product ID: " . $networkProductId); // TODO Best way?
                	$dummy = ''; // dummy statement to avoid code sniffer warning
                }
            }
        }

        return [];
    }

    public function getMappingInstanceBySku($priceplanSku, $promoSku)
    {
        return $this->_getMappingInstanceByField($priceplanSku, $promoSku, 'pp_sku', 'promo_sku');
    }

    public function getMappingInstanceById($priceplanId, $promoId)
    {
        $priceplanSku = $this->convertIdToSku($priceplanId);
        $promoSku = $this->convertIdToSku($promoId);

        return $this->getMappingInstanceBySku($priceplanSku, $promoSku);
    }

    public function getMappingByPriceplanAndPromo($priceplanSku, $promoSku = null)
    {
        $key = md5(__METHOD__) . '_' . $priceplanSku . '_' . $promoSku;
        if ($result = unserialize($this->getCache()->load($key))) {
            return $result;
        } else {
            $promo = Mage::getModel('catalog/product')->loadByAttribute('sku', $promoSku);
            if($promo && $promo->getPromoIgnoreMultimapper()){
                $promoSku = null;
            }
            $result = Mage::getModel('multimapper/mapper')->getCollection()
                ->addFieldToFilter('pp_sku', $priceplanSku)
                ->addFieldToFilter('promo_sku', $promoSku);
            $this->getCache()->save(serialize(@utf8_encode($result)), $key, [Vznl_Configurator_Model_Cache::CACHE_TAG], $this->getCache()->getTtl());

            return $result;
        }
    }

    /**
     * Iterate through items and read the values
     *
     * @param $package
     * @param $catHelper
     * @param $commercialAddonIds
     * @param $promoProductCollection
     * @param $networkProductId
     * @param $rcProductId
     * @param $rcPromoProductId
     * @param $promoProductId
     */
    protected function buildPackageData($package, $catHelper, $commercialAddonIds, $promoProductCollection, &$networkProductId, &$rcProductId, &$rcPromoProductId, &$promoProductId)
    {
        foreach ($package['items'] as $item) {
            /** @var Vznl_Catalog_Model_Product $product */
            $product = $item->getProduct();
            // SREQ-06149 - If product is simonly on indirect, it should not be sent
            if ((Mage::app()->getWebsite()->getCode() == Vznl_Agent_Model_Website::WEBSITE_INDIRECT_CODE) && $product->getPromoIgnoreMultimapper()) {
                continue;
            }
            // Ignore hollandsenieuwe products from multimapper according to DF-006271
            if ($product->isHollandsNieuweNetworkProduct()) {
                continue;
            }
            if ($product->isSubscription()) {
                $networkProductId = $product->getId();
            }
            if ($product->isDeviceSubscription()) {
                $rcProductId = $product->getId();
            }
            if ($catHelper->is([Vznl_Catalog_Model_Type::SUBTYPE_ADDON], $product)) {
                $commercialAddonIds[$product->getId()] = 0;
            }
        }
        // Do a second iteration to make sure the sub and deviceRc are set
        foreach ($package['items'] as $item) {
            /** @var Dyna_Catalog_Model_Product $product */
            $product = $item->getProduct();
            if ($item->isPromo()) {
                $promoProduct = $promoProductCollection->getItemByColumnValue('entity_id', $item->getProduct()->getId());
                if (!$promoProduct->getPromoIgnoreMultimapper()) {
                    if ($rcProductId == $item->getTargetId()) {
                        $rcPromoProductId = $product->getId();
                    } elseif ($item->getTargetId() == $networkProductId) {
                        $promoProductId = $product->getId();
                    } else {
                        $commercialAddonIds[$item->getTargetId()] = $product->getId();
                    }
                }
            }
        }
    }

    /**
     * Load product and get the SKU by id
     *
     * @param $productId
     * @return string
     */
    private function convertIdToSku($productId)
    {
        $key = md5(__METHOD__) . '_' . $productId;
        if (!($productSku = unserialize($this->getCache()->load($key)))) {
            $productSku = Mage::getModel('catalog/product')->getCollection()
                ->addFieldToFilter('entity_id', $productId)->getFirstItem()->getSku();
            $this->getCache()->save(serialize($productSku), $key, [Vznl_Configurator_Model_Cache::CACHE_TAG], $this->getCache()->getTtl());

        }

        return $productSku;
    }
}
