<?php

class Vznl_FFManager_Model_Resource_Blocklist extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init("ffmanager/blocklist", "blocked_id");
    }
}