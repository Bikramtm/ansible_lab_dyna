<?php

/**
 * Class Vznl_FFManager_Model_Resource_Relation_Auth_Value
 */
class Vznl_FFManager_Model_Resource_Relation_Auth_Value extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init("ffmanager/relation_auth_value", "relation_auth_value_id");
    }
}