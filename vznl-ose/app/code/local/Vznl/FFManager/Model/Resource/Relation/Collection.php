<?php

class Vznl_FFManager_Model_Resource_Relation_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    public function _construct()
    {
        $this->_init("ffmanager/relation");
    }

    /**
     * @param int $authType
     * @param string $authValue
     * @return $this
     */
    public function filterActiveUnexpiredRelations($authType, $authValue)
    {
        $dateNow = Zend_Date::now();

        $this->join(array(
            "ra" => "ffmanager/relation_auth_value"
        ), "main_table.relation_id = ra.relation_id");

        $this->addFieldToFilter("enabled", true);
        $this->getSelect()->where(
            "(`main_table`.`valid_from` <= ? " . Zend_Db_Select::SQL_AND ." `main_table`.`valid_to` >= ?) " // If valid_from and valid_to is available
            . Zend_Db_Select::SQL_OR  . " `main_table`.`created_at` >= DATE_SUB(?, INTERVAL `main_table`.`validation_period` month) " // If not and validation_period is available
            . Zend_Db_Select::SQL_OR . " (`main_table`.`valid_from` IS NULL " . Zend_Db_Select::SQL_AND . " `main_table`.`valid_to` IS NULL) " . Zend_Db_Select::SQL_AND . " (`main_table`.`validation_period` IS NULL  " . Zend_Db_Select::SQL_OR . " `main_table`.`validation_period` = 0 ) ",
            $dateNow->toString("YYYY-MM-dd hh:mm:ss")
        );

        $this->addFieldToFilter("main_table.auth_type", $authType);
        $this->addFieldToFilter("ra.value", $authValue);

        return $this;
    }

    /**
     * Get already active relations for a given Authentication value (ex.: email extension)
     *
     * @param object|string $authValue
     * @param array $excluded
     * @return $this
     */
    public function getActiveRelationsByEmailExt($authValue, $excluded = null)
    {
        Mage::log('[ffManager/Model/Resource/RelationCollection][getActiveRelationsByEmailExt] excluded =' . var_export($excluded, true));
        if(is_object($authValue)) {
            Mage::log('[ffManager/Model/Resource/RelationCollection][getActiveRelationsByEmailExt] obj authvalue =' . var_export($authValue->toArray(), true));
            $value = $authValue->getValue();
            $excluded = array($authValue->getRelationId());
            Mage::log('[ffManager/Model/Resource/RelationCollection][getActiveRelationsByEmailExt] new excluded =' . var_export($excluded, true));
        }
        else {
            Mage::log('[ffManager/Model/Resource/RelationCollection][getActiveRelationsByEmailExt] array authvalue =' . var_export($authValue, true));
            $value = $authValue;
        }

        if(!$value) {
            Mage::log('[ffManager/Model/Resource/RelationCollection][getActiveRelationsByEmailExt] no value. relation = ' . var_export($this->toArray(), true));
            return $this;
        }

        $start = strpos($value, '@');
        if ($start === false) {
            Mage::log('[ffManager/Model/Resource/RelationCollection][getActiveRelationsByEmailExt] no $start. relation = ' . var_export($this->toArray(), true));
            return $this;
        }

        $value = trim(substr($value, $start));

        if(!$value) {
            Mage::log('[ffManager/Model/Resource/RelationCollection][getActiveRelationsByEmailExt] no value after trim. relation = ' . var_export($this->toArray(), true));
            return $this;
        }


        $this->join(array(
            "ra" => "ffmanager/relation_auth_value"
        ), "main_table.relation_id = ra.relation_id");

        $this->addFieldToFilter("enabled", true);
        $this->addFieldToFilter("ra.value", array(
            'like' => '%'.$value,
            'eq' => $value,
        ));

        if($excluded && is_array($excluded) && count($excluded)) {
            $this->addFieldToFilter("main_table.relation_id", array('nin' => $excluded));
        }

        Mage::log('[ffManager/Model/Resource/RelationCollection][getActiveRelationsByEmailExt] after filtering. relation = ' . var_export($this->toArray(), true));
        return $this;
    }
}