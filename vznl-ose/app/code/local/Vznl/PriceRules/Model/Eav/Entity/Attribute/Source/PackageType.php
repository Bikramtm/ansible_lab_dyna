<?php

/**
 * Class Vznl_PriceRules_Model_Eav_Entity_Attribute_Source_PackageType
 */
class Vznl_PriceRules_Model_Eav_Entity_Attribute_Source_PackageType extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
    /**
     * Retrieve all options array
     *
     * @return array
     */
    public function getAllOptions()
    {
        if (is_null($this->_options)) {
            /** @var Vznl_Package_Model_PackageType $packageTypeModel */
            $packageTypeModel = Mage::getModel("vznl_package/packageType");
            $this->_options = $packageTypeModel->getAllPackageTypesForForm();
        }

        return $this->_options;
    }

    /**
     * Retrieve option array
     *
     * @return array
     */
    public function getOptionArray()
    {
        $_options = array();
        foreach ($this->getAllOptions() as $option) {
            $_options[$option["value"]] = $option["label"];
        }
        return $_options;
    }

    /**
     * Get a text for option value
     *
     * @param string|integer $value
     * @return string
     */
    public function getOptionText($value)
    {
        $options = $this->getAllOptions();
        foreach ($options as $option) {
            if ($option["value"] == $value) {
                return $option["label"];
            }
        }
        return false;
    }
}
