<?php

/**
 * Class Vznl_PriceRules_Model_ImportPriceRules
 */
class Vznl_PriceRules_Model_ImportPriceRules extends Dyna_PriceRules_Model_ImportPriceRules
{
    /** @var array $rulesWithMutuallyExclusiveProducts */
    protected static $rulesWithMutuallyExclusiveProducts = array();
    protected $_packageTypes;

    const ALL_PACKAGE_TYPES = "*";

    protected $existingRules;

    public function __construct()
    {
        parent::__construct();
        /** @var Vznl_Import_Helper_Data _helper */
        $this->_helper = Mage::helper("vznl_import");
        $this->setPackageTypes();

        //clear unused Coupons
        $coupons = Mage::getModel('salesrule/coupon')->getCollection()
            ->addFieldToFilter('rule_id', 0);
        foreach ($coupons as $coupon) {
            $coupon->delete();
        }
    }

    /**
     * @param $rawData
     * @return bool
     */
    public function process($rawData, $lineCount = 0)
    {
        $this->_error = 0;
        $this->_totalFileRows++;

        $rawData = array_combine($this->_header, $rawData);
        if (!array_filter($rawData)) {
            $this->_skippedFileRows++;
            $this->_logError("[ERROR] Inconsistency in import file, as the row (".($lineCount - 1).") columns count does not match with the headers column count. [Skipping row : ".($lineCount - 1)."]");
            return false;
        }

        $this->_log("Saving rule: " . $rawData['rule_title'], true);

        try {
            /** process basic data like name, start date, end date etc */
            $this->clearData();
            if (isset($rawData['sr_uniqueid']) && trim($rawData['sr_uniqueid']) != '') {
                if (($key = array_search($rawData['sr_uniqueid'], $this->existingRules)) !== false) {
                    unset($this->existingRules[$key]);
                }
                $salesRuleModel = Mage::getModel('salesrule/rule')->load($rawData['sr_uniqueid'], 'sr_unique_id');
                $this->_data['rule_id'] = $salesRuleModel->getData('rule_id');
            } else {
                $this->_skippedFileRows++;
                $this->_logError("[ERROR] Skipped row because: sr_unique_id is missing for " . $rawData['rule_title'] . ".");
                return false;
            }

            $showHint = Dyna_PriceRules_Model_Validator::HINT_NONE;
            if (isset($rawData['show_hint_once'])) {
                if (in_array(trim(strtolower($rawData['show_hint_once'])), ["yes", "true"])) {
                    $showHint = Dyna_PriceRules_Model_Validator::HINT_ONCE;
                } else if (in_array(trim(strtolower($rawData['show_hint_once'])), ["no", "false"])) {
                    $showHint = Dyna_PriceRules_Model_Validator::HINT_ALWAYS;
                }
            }

            $serviceSourceExpression = trim($rawData['service_source_expression'] ?? '');
            if ($serviceSourceExpression != "") {
                $dynaImportHelper = Mage::getModel('productmatchrule/importer');
                if (!$dynaImportHelper->validateServiceSourceExpression($serviceSourceExpression)) {
                    $this->_skippedFileRows++;
                    $this->_logError("[ERROR] Skipped row because: Invalid expression for ".$rawData['rule_title'].".");
                    return false;
                }
            }

            $this->_data['stack'] = $this->stack;
            $this->_data['name'] = trim($rawData['rule_title']);
            $rawData['service_source_expression'] = $rawData['service_source_expression'] ?? '';
            $this->_data['service_source_expression'] = trim($rawData['service_source_expression']);
            $this->_data['show_hint'] = $showHint;
            $this->_data['sort_order'] = isset($rawData['priority']) ? trim($rawData['priority']) : null;
            $this->_data['from_date'] = $this->formatDate($rawData['effective_date']);
            $this->_data['to_date'] = $this->formatDate($rawData['expiration_date']);
            $this->_data['is_active'] = empty($rawData['is_active']) || strtolower($rawData['is_active']) == 'active' ? 1 : 0;
            $this->_data['website_ids'] = $this->setWebsitesIds($rawData['channel']);
            $this->_data['sr_unique_id'] = isset($rawData['sr_uniqueid']) ? trim($rawData['sr_uniqueid']) : null;

            //if rule_type is Coupon Code then check for some mandatory fields
            $ruleType = strtolower(trim($rawData['rule_type']));
            $code = !empty($rawData['discount_code']) ? trim($rawData['discount_code']) : null;
            if ($ruleType == 'coupon_code' && $code == '') {
                $this->_skippedFileRows++;
                $this->_logError("[ERROR] Skipped row because: No Coupon Code found for ".$rawData['rule_title'].".");
                return false;
            } elseif ($ruleType == 'coupon_code' && isset($code)) {

                $couponDiscountType = isset($rawData['coupon_discount_type']) ? $this->validateCouponDiscountType($rawData['coupon_discount_type']) : null;

                if ($couponDiscountType  == false) {
                    $this->_skippedFileRows++;
                    $this->_logError("[ERROR] Skipped row because: Coupon Disount Type is not valid for ". $rawData['rule_title'] .
                        ", the valid ones are". implode(',', $this->getCouponDiscountTypes()) . " .");
                    return false;
                }

                $freeShipping = $rawData['free_shipping'] !== "" ? $this->validateFreeShipping($rawData['free_shipping']) : null;

                if ($freeShipping === false) {
                    $this->_skippedFileRows++;
                    $this->_logError("[ERROR] Skipped row because: Free Shipping is not valid for ". $rawData['rule_title'] .
                        ", the valid ones are". implode(',', $this->getValidateFreeShipping()) . " .");
                    return false;
                }

                $couponType = isset($rawData['coupon_type']) ? $this->validateCouponType($rawData['coupon_type']) : null;

                if ($couponType == false) {
                    $this->_skippedFileRows++;
                    $this->_logError("[ERROR] Skipped row because: Coupon Type is not valid for ". $rawData['coupon_type'] .
                        " , the valid ones are". implode(',', $this->getValidTypes()) . " .");
                    return false;
                }

                //retain the coupon usage related data.
                if (isset($rawData['discount_code']) && !isset($this->_data['rule_id'])) {
                    $couponModel = Mage::getModel('salesrule/coupon')->loadByCode(trim($rawData['discount_code']));

                    if ($couponModel->getData('coupon_id')) {
                        $couponModel->setRuleId("");
                        $couponModel->setIsPrimary("");
                        $write = Mage::getSingleton('core/resource')->getConnection('core_write');
                        $write->query("SET FOREIGN_KEY_CHECKS = 0;");
                        $couponModel->save();
                        $write->query("SET FOREIGN_KEY_CHECKS = 1;");
                        $this->_data['coupon_id'] = $couponModel->getCouponId();
                    }
                }

                /** Import Coupon related data */
                $this->_data['coupon_code'] = isset($rawData['discount_code']) ? trim($rawData['discount_code']) : null;
                $this->_data['uses_per_coupon'] = isset($rawData['uses_per_coupon']) ? trim($rawData['uses_per_coupon']) : null;
                $this->_data['uses_per_customer'] = isset($rawData['uses_per_customer']) ? trim($rawData['uses_per_customer']) : null;
                $this->_data['is_rss'] = $this->checkValue($rawData['publish_in_rss_feed']);
                $this->_data['simple_action'] = $couponDiscountType;
                $this->_data['discount_amount'] = isset($rawData['coupon_discount_size']) ? trim($rawData['coupon_discount_size']) : null;
                $this->_data['discount_qty'] = isset($rawData['maximum_qty_discount']) ? trim($rawData['maximum_qty_discount']) : null;
                $this->_data['discount_step'] = isset($rawData['discount_shelf_quantity']) ? trim($rawData['discount_shelf_quantity']) : null;
                $this->_data['apply_to_shipping'] = $this->checkValue($rawData['apply_to_shipping_amount']);
                $this->_data['simple_free_shipping'] = $freeShipping;
                $this->_data['coupon_type'] = $couponType;
                $this->_data['stop_rules_processing'] = $this->checkValue($rawData['stop_further_rules_processing']);
            }


            /** fixed sales rules login */
            if (!empty($rawData['usage_discount'])) {
                $this->_data['usage_discount'] = str_replace(["-", ","], ["", "."], $rawData['usage_discount']);
            }
            if (!empty($rawData['usage_discount_percent'])) {
                $this->_data['usage_discount_percent'] = str_replace(["-", ","], ["", "."], $rawData['usage_discount_percent']);
            }
            /** end fixed sales rules logic */
            if ($rawData['is_promo'] == 'yes') {
                $this->_data['is_promo'] = $this->setIsPromo($rawData['is_promo']);
                $sku = trim(explode('=', $rawData['action'], 2)[1]);
            }
            if (!$this->_data['website_ids']) {
                $this->_logError("[ERROR] " . $rawData['rule_title'] . " does not have specified websites");
                $this->_skippedFileRows++;
                return false;
            }
            if (!empty($rawData['discount_period'])) {
                $this->_data['discount_period'] = trim($rawData['discount_period']);
            }
            if (!empty($rawData['discount_period_amount'])) {
                $this->_data['discount_period_amount'] = (int)$rawData['discount_period_amount'];
            }
            if (!empty($rawData['rule_description'])) {
                $this->_data['description'] = $rawData['rule_description'];
            }
            if (!empty($rawData['rule_type'])) {
                $this->_data['rule_type'] = $rawData['rule_type'];
            }
            if (!empty($rawData['package_type_id'])) {
                $this->_data['package_type'] = trim($rawData['package_type_id']);
            }
            // handle promotion labels (for stackable promotions)
            if (!empty($rawData['promotion_label'])) {
                $promoLabelsArray = array_map('trim',explode(',', $rawData['promotion_label']));
                $maxPromotionLabels = 3;
                $foundLabels = [];
                $countFoundLabels = 0;
                foreach ($promoLabelsArray as $promotionLabel) {
                    if ($countFoundLabels >= $maxPromotionLabels) {
                        break;
                    }
                    if (in_array($promotionLabel, ['Friends and Family', 'Dealcloser', 'Default']) && !in_array($promotionLabel, $foundLabels)) {
                        $foundLabels[] = $promotionLabel;
                        $countFoundLabels++;
                    }
                }
                $this->_data['promotion_label'] = implode(',', $foundLabels);
            }

            /** leave rules processing to other method */
            $processedRule = $this->processRulesData($rawData);
            if (!$processedRule || $this->_skipRow) {
                $this->_logError("[ERROR] Skipping rule: " . $rawData['rule_title']);
                $this->_skippedFileRows++;
            } else {
                /** if rule is a promo one, check that the promo product isn't part of a mutually exclusive family,
                /* family which has other promo product (whose rule has been already imported) */
                $ruleIsValid = ((!empty($this->_data['promo_sku'])) && ($rawData['is_promo'] == 'yes')) ?
                    $this->checkPromoProductFamilyValidity($this->_data, $rawData) : true;

                if ($ruleIsValid) {
                    $processedRule->saveRule();
                    if (!is_null($couponModel) && $couponModel->getData('coupon_id')) {
                        $salesRuleModel = Mage::getModel('salesrule/rule')->load($rawData['sr_uniqueid'], 'sr_unique_id');
                        $couponModel->setRuleId($salesRuleModel->getData('rule_id'));
                        $couponModel->setIsPrimary(1);
                        $couponModel->save();
                    }
                } else {
                    // if it is, skip the rule
                    $this->_skippedFileRows++;
                }
            }
        } catch (Exception $e) {
            $this->_skippedFileRows++;
            $this->_logError("[ERROR] Skipped row because: " . $e->getMessage());
        }
    }

    /**
     * Try to set the websites for the rule;
     * Go through each website id or website code provided
     * (the list provided in the csv can be of the form "id1,id2" or "code1,code2"
     * and see if the code/id provided is in the list of websites codes/ids that are possible.
     * If one id/code is not found in the possible list => skip the row, error
     *
     * @param $websites
     * @return array
     */
    protected function setWebsitesIds($websites)
    {
        $websitesData = explode(',', str_replace(' ','', strtolower($websites)));
        $websitesToAdd = [];
        foreach ($websitesData as $key => $possibleWebsite) {
            // if one of the possible websites provided in the csv is "*"
            // the rule will be available for all websites; skip the rest of the code
            if ($possibleWebsite == self::ALL_WEBSITES) {
                return array_keys($this->_websites);
            }
            // the website provided is the code and we should store the id
            if ($validWebsite = array_search($possibleWebsite, $this->_websites)) {
                $websitesToAdd[$key] = $validWebsite;
            } elseif (array_key_exists($possibleWebsite, $this->_websites)) {
                // the website provided is id and we should store it
                $websitesToAdd[$key] = $possibleWebsite;
            } else {
                // not match can be found between the given website and a code or an id
                $this->_logError('[ERROR]The website ' . var_export($websitesData, true) . ' is not present/valid. skipping row');
                return false;
            }
        }

        return $websitesToAdd;
    }

    /**
     * @return $this
     */
    protected function _processActionScopeCategory()
    {
        /** Adding product combination condition */
        if (!empty(trim($this->_rawData['action_scope_category']))) {
            $actionScopeCategory = $this->_replaceCategoryPaths(trim($this->_rawData['action_scope_category']));
            if(!$actionScopeCategory){
                $this->_skipRow = true;
                $this->_logError("[ERROR] Category ". $this->_rawData['action_scope_category']." was not found");
                return false;
            }

            if (stripos($actionScopeCategory, self::CONDITION_ACTION_AND) !== false ||
                stripos($actionScopeCategory, self::CONDITION_ACTION_OR) !== false
            ) {
                $this->_setRuleWithOrAnd(trim($actionScopeCategory), false, false);
            } else {
                $this->setSimpleActionCategoryRule($actionScopeCategory);
            }
        }

        return $this;
    }

    /**
     * @return $this
     */
    protected function _processCategoryConditionsRules()
    {
        /** Adding product combination condition */
        if (!empty(trim($this->_rawData['based_on_category']))) {
            $basedOnCategory = $this->_replaceCategoryPaths(trim($this->_rawData['based_on_category']));
            if(!$basedOnCategory){
                $this->_skipRow = true;
                $this->_logError("[ERROR] Category ". $this->_rawData['based_on_category']." was not found");
                return false;
            }
            if (stripos($basedOnCategory, self::CONDITION_ACTION_AND) !== false ||
                stripos($basedOnCategory, self::CONDITION_ACTION_OR) !== false
            ) {
                $this->_setRuleWithOrAnd(trim($basedOnCategory), false, true);
            } else {
                $this->setSimpleConditionCategoryRule($basedOnCategory);
            }
        }

        return $this;
    }

    public function getFileLogName()
    {
        return strtolower($this->stack) . '_' . $this->_logFileName . '.' . $this->_logFileExtension;
    }

    /**
     * @return $this
     */
    protected function _processAction()
    {
        $action = isset($this->_rawData['action']) ? trim(str_replace(' ', '', strtolower($this->_rawData['action']))) : null;

        if ($action && stripos($action, "productwheresku") !== false) {
            $this->__addProduct();
        } elseif ($action && $action == "autoremoveproducts") {
            $this->_addRemoveProducts();
        } elseif (isset($this->_rawData['rule_type']) && !in_array(trim($this->_rawData['rule_type']), array('message_hint', 'coupon_code'))) {
            /** Error */
            $this->_helper->log(new Exception(sprintf("Unknown action '%s' type for: %s", $this->_rawData['rule_type'], $this->_rawData['rule_title'])));
            $this->_error = 1;
        }

        if (isset($this->_rawData['rule_type']) && $this->_rawData['rule_type'] == 'message_hint') {
            $this->__addSalesHint();
        }

        return $this;
    }

    /**
     * Check if a promo product is part of a mutually exclusive family
     * If so, check if there was another rule inserted before who's promo product is part of the same family
     * If the above conditions is yes, don't allow current rule to be inserted (returning false)
     * @param $ruleProcessedData
     * @param $rawImportData
     *
     * @return bool
     */
    protected function checkPromoProductFamilyValidity($ruleProcessedData, $rawImportData)
    {
        // firstly, get all mutually exclusive products
        $mutuallyExclusiveProducts = Mage::helper('vznl_catalog')->getMutuallyExclusiveProducts();

        $productCategoryId = $ruleIsValid = true;
        // current promo product is a mutually exclusive product?
        if ($this->isProductMutuallyExclusive($mutuallyExclusiveProducts, $ruleProcessedData['promo_sku'], $productCategoryId)) {
            // check if a rule for the same family has been previously imported
            if (!empty(self::$rulesWithMutuallyExclusiveProducts[$productCategoryId])) {
                // check if current promo is different than the last one stored in this ME family
                $samePromo = in_array($ruleProcessedData['promo_sku'], self::$rulesWithMutuallyExclusiveProducts[$productCategoryId]['promo_sku']);
                // check if current target is different than the last one stored in this mutually exclusive family
                $previousTargetSku = !empty(self::$rulesWithMutuallyExclusiveProducts[$productCategoryId]['action_scope'])
                    ? self::$rulesWithMutuallyExclusiveProducts[$productCategoryId]['action_scope'] : array();
                $previousTargetCategory = !empty(self::$rulesWithMutuallyExclusiveProducts[$productCategoryId]['action_scope_category'])
                    ? self::$rulesWithMutuallyExclusiveProducts[$productCategoryId]['action_scope_category'] : array();
                $sameTarget = (!empty($rawImportData['action_scope']) ?
                        in_array($rawImportData['action_scope'], $previousTargetSku) : false)
                    || (!empty($rawImportData['action_scope_category']) ?
                        in_array($rawImportData['action_scope_category'], $previousTargetCategory) : false);

                // if in this family the target is different, then rule is invalid
                // (so a previous rule with a promo from this family has been imported while the current one has
                // a different promo, from the same family, and a different target)
                if (!$samePromo && !$sameTarget) {
                    // give a warning in this case, as 2 targets can't have different promos from the same mutually exclusive family
                    $this->_logError("[INFO] Possible issue with rule (".$rawImportData['rule_title']."): Promo product of this rule is part of a mutually exclusive family (category ".$productCategoryId.") whose other product is a promo for an already inserted rule (rule which has a different target than the current one).");
                }
            }

            if ($ruleIsValid) {
                // save current rule's target to check against in next iterations of the import
                if ($ruleProcessedData['promo_sku']) {
                    if (!empty(self::$rulesWithMutuallyExclusiveProducts[$productCategoryId]['promo_sku'])) {
                        array_push(self::$rulesWithMutuallyExclusiveProducts[$productCategoryId]['promo_sku'], $ruleProcessedData['promo_sku']);
                    } else {
                        self::$rulesWithMutuallyExclusiveProducts[$productCategoryId]['promo_sku'] = array($ruleProcessedData['promo_sku']);
                    }
                }
                if ($rawImportData['action_scope']) {
                    self::$rulesWithMutuallyExclusiveProducts[$productCategoryId]['action_scope'] = array($rawImportData['action_scope']);
                }
                if ($rawImportData['action_scope_category']) {
                    self::$rulesWithMutuallyExclusiveProducts[$productCategoryId]['action_scope_category'] = array($rawImportData['action_scope_category']);
                }
            }
        }

        return $ruleIsValid;
    }

    /**
     * Check if current promo product is within the given list of mutually exclusive products
     * @param $mutuallyExclusiveCategories
     * @param $productSku
     * @param $productCategoryId
     *
     * @return bool
     */
    protected function isProductMutuallyExclusive($mutuallyExclusiveCategories, $productSku, &$productCategoryId)
    {
        foreach ($mutuallyExclusiveCategories as $categoryId => $mutuallyExclusiveCategoryProducts) {
            foreach ($mutuallyExclusiveCategoryProducts as $skuProduct) {
                if ($productSku == $skuProduct) {
                    // also, save the category id
                    $productCategoryId = $categoryId;
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Method to check if the simple action is valid or not
     * @param $action
     * @return string
     * @internal param $value
     */
    protected function validateCouponDiscountType($action)
    {
        $allowedActions = $this->getCouponDiscountTypes();

        if (in_array($action, $allowedActions)) {
            return $action;
        } else {
            return false;
        }
    }

    /**
     * Get all the Valid Actions For a Coupon
     * @return array
     */
    public function getCouponDiscountTypes() {
        return array(
            Mage_SalesRule_Model_Rule::BUY_X_GET_Y_ACTION,
            Mage_SalesRule_Model_Rule::BY_FIXED_ACTION,
            Mage_SalesRule_Model_Rule::BY_PERCENT_ACTION,
            Mage_SalesRule_Model_Rule::CART_FIXED_ACTION,
            'process_add_product',
            'process_remove_product',
            'process_replace_product',
            'ampromo_items',
            'ampromo_remove_items',
            'ampromo_replace_items'
        );
    }

    /**
     * Validate the given Coupon and return the corresponding option value.
     * @param $type
     * @return bool|int
     */
    protected function validateCouponType($type)
    {
        $allowedTypes = $this->getValidTypes();

        if (in_array($type, $allowedTypes)) {
            if ($type == "no_coupon") {
                return 1;
            } elseif ($type == "specific_coupon") {
                return 2;
            }
        }
        return false;
    }

    /**
     * Return the list of Valid coupon types.
     * @return array
     */
    public function getValidTypes()
    {
        return array('specific_coupon', 'no_coupon');
    }

    /**
     * Validate the given Coupon Type and return the corresponding option value.
     * @param $value
     * @return int
     */
    public function checkValue($value) {
        if (isset($value)) {
            if (strtolower($value) == "yes") {
                return 1;
            } else {
                return 0;
            }
        }
        return 0;
    }

    /**
     * Return the list of Valid coupon types.
     * @return array
     */
    public function getValidateFreeShipping()
    {
        return array('free_shipping_related_item' => 1,'free_shipping_relevant_items' => 2);
    }

    /**
     * Validate the Allowed Free shipping values and return the corresponding option value
     * @param $freeShipping
     * @return int|mixed
     */
    protected function validateFreeShipping($freeShipping)
    {
        $allowedShipping = $this->getValidateFreeShipping();

        if (in_array($freeShipping, array_keys($allowedShipping))) {
            return $allowedShipping[$freeShipping];
        }
        return false;
    }

    /**
     * Reset rule information
     */
    public function clearData()
    {
        $this->_data = array(
            'rule_id' => null,
            'product_ids' => '',
            'name' => '',
            'rule_description' => '',
            'is_active' => '',
            'website_ids' => '',
            'customer_group_ids' => array(0, 1),
            'coupon_type' => '1',
            'coupon_code' => '',
            'uses_per_coupon' => '',
            'uses_per_customer' => '',
            'from_date' => '',
            'to_date' => '',
            'sort_order' => '',
            'is_rss' => '',
            'conditions' => array(),
            'actions' => array(),
            'simple_action' => '',
            'discount_amount' => null,
            'discount_qty' => '',
            'discount_step' => '',
            'apply_to_shipping' => '',
            'simple_free_shipping' => '',
            'stop_rules_processing' => '',
            'store_labels' => array(),
            'show_hint' => Dyna_PriceRules_Model_Validator::HINT_NONE,
            'package_type' => ''
        );
        $this->_skipRow = false;
    }

    protected function setPackageTypes()
    {
        $packageTypes = Mage::getModel('vznl_package/packageType')
            ->getCollection();

        /** @var Dyna_Package_Model_PackageType $packageType */
        foreach ($packageTypes as $packageType) {
            $this->_packageTypes[strtoupper($packageType->getPackageCode())] = $packageType->getId();
        }

        return $this;
    }

    /**
     * @param $rawData
     * @return mixed
     */
    public function processRulesData($rawData)
    {
        $this->setRawData($rawData);
        $conditionsAndActions = $this->processRule()->getProcessedRules();
        /** fixed sales rules login */
        if (!$conditionsAndActions) {
            return false;
        }
        $this->_data = array_merge($this->_data, $conditionsAndActions);

        return $this;
    }

    /**
     * @return $this
     */
    public function processRule()
    {
        $this->setDefaultAction();
        $this->setDefaultCondition();

        $this->_processSkusConditionsRules();
        $this->_processCategoryConditionsRules();
        $this->_processDealerCampaigns();
        $this->_processProcessContext();
        $this->_processLifeCycleDetail();
        $this->_processCustomerSegment();
        $this->_processDealer();
        $this->_processAction();
        $this->_processActionScopeProduct();
        $this->_processActionScopeCategory();
        $this->_processPackageTypes();

        return $this;
    }

    /**
     * @return $this
     */
    protected function _processPackageTypes()
    {
        if (!empty($this->_rawData['package_type_id'])) {
            if (trim($this->_rawData['package_type_id']) == self::ALL_PACKAGE_TYPES) {
                $parsedPackageTypes = array_values($this->_packageTypes);
            } else {
                $packageTypes = explode(',', $this->_rawData['package_type_id']);
                $packageTypes = array_map('trim', $packageTypes);
                $parsedPackageTypes = [];

                foreach ($packageTypes as $packageType) {
                    $parsedPackageTypes[] = $this->_packageTypes[strtoupper($packageType)];
                }
            }

            $this->_processRule['package_type'] = implode(',', array_unique($parsedPackageTypes));

            $this->_processRule['conditions']["1--" . $this->_currentCondition] = array(
                'type' => 'vznl_pricerules/condition_packageType',
                'attribute' => 'package_type',
                'operator' => self::CONDITION_ACTION_ONE_OF,
                'value' => $parsedPackageTypes,
            );

            $this->_currentCondition++;
        }

        return $this;
    }

    /**
     * Delete the unused rules which were not in the Import file, active Quote and Order.
     */
    public function removeUnusedRules()
    {
        foreach ($this->existingRules as $unused) {
            $unusedRule = Mage::getModel('salesrule/rule')->load($unused, 'sr_unique_id');
            $quoteRules = Mage::getModel('sales/quote')->getCollection()
                ->distinct(true)
                ->addFieldToSelect('applied_rule_ids')
                ->addFieldToFilter('applied_rule_ids', array('finset' => $unusedRule->getData('rule_id')));

            $orderRules = Mage::getModel('sales/order')->getCollection()
                ->distinct(true)
                ->addFieldToSelect('applied_rule_ids')
                ->addFieldToFilter('status', array('notnull' => true))
                ->addAttributeToFilter('status', array('neq' => Omnius_Checkout_Model_Sales_Order::STATUS_CANCELLED))
                ->addFieldToFilter('applied_rule_ids', array('finset' => $unusedRule->getData('rule_id')));
            $allowDelete = ($quoteRules->getSize() > 0) ? false : true;

            if ($allowDelete) {
                $allowDelete = ($orderRules->getSize() > 0) ? false : true;
                $coupons = Mage::getModel('salesrule/coupon')->getCollection()
                    ->distinct(true)
                    ->addFieldToSelect('coupon_id')
                    ->addFieldToFilter('rule_id', $unusedRule->getData('rule_id'));
                if ($coupons->getSize() > 0 && $allowDelete) {
                    foreach ($coupons as $coupon) {
                        $write = Mage::getSingleton('core/resource')->getConnection('core_write');
                        if ($write->fetchOne("select * from salesrule_coupon_usage where coupon_id=" . (int)$coupon->getData('coupon_id'))) {
                            $allowDelete = false;
                        }
                    }
                }
            }

            if ($allowDelete) {
                $unusedRule->delete();
            }
        }
    }

    /**
     * @return $this
     */
    protected function _processSkusConditionsRules()
    {
        /** Adding product combination condition */
        if (!empty(trim($this->_rawData['based_on_product']))) {
            $basedOnAProduct = trim($this->_rawData['based_on_product']);

            if (stripos($basedOnAProduct, self::CONDITION_ACTION_AND) !== false ||
                stripos($basedOnAProduct, self::CONDITION_ACTION_OR) !== false
            ) {
                $this->_setRuleWithOrAnd($basedOnAProduct, true, true);
            } else {
                if (strpos($basedOnAProduct, ",") !== false) {
                    $this->setSimpleConditionProductRule($basedOnAProduct, self::CONDITION_ACTION_ONE_OF);
                } else {
                    $this->setSimpleConditionProductRule($basedOnAProduct, self::CONDITION_ACTION_IS);
                }
            }
        }

        return $this;
    }

    public function setExistingRules()
    {
        $rules = Mage::getModel('salesrule/rule')->getCollection()
            ->addFieldToSelect('sr_unique_id')
            ->addFieldToFilter('stack', $this->stack);

        foreach ($rules as $rule) {
            $this->existingRules[] = $rule['sr_unique_id'];
        }
    }
}
