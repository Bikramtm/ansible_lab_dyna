<?php

/**
 * Class Vznl_PriceRules_Model_Validator
 */
class Vznl_PriceRules_Model_Validator extends Dyna_PriceRules_Model_Validator
{
    /** @var Vznl_Configurator_Model_Expression_Serviceability|null */
    protected $expressionServiceability = null;
    /** @var Dyna_Configurator_Model_Expression_Agent|null */
    protected $expressionAgent = null;

    /**
     * Vznl_PriceRules_Model_Validator constructor.
     */
    public function _construct()
    {
        //prevent calling the parent constructor as it initializes un-needed objects
        $this->expressionCart = Mage::getModel('dyna_configurator/expression_cart');
    }

    /**
     * Check what objects the service expression needs and only initialize them on demand, if needed
     * This method is used to prevent initializing all usable objects if no rule uses that object
     * @param $expression
     */
    private function getNeededInputParams($expression)
    {
        $params = [];

        if(strpos($expression, 'availability.') !== FALSE){
            if(!$this->expressionAvailability){
                $this->expressionAvailability = Mage::getModel('dyna_configurator/expression_availability');
            }
            $params['availability'] = $this->expressionAvailability;
        }

        if(strpos($expression, 'customer.') !== FALSE){
            if(!$this->expressionCustomer){
                $this->expressionCustomer = Mage::getModel('dyna_configurator/expression_customer');
            }
            $params['customer'] = $this->expressionCustomer;
        }

        if(strpos($expression, 'cart.') !== FALSE){
            $params['cart'] = $this->expressionCart;
        }

        if(strpos($expression, 'activeInstallBase.') !== FALSE){
            if(!$this->expressionActiveInstallBase){
                $this->expressionActiveInstallBase = Mage::getModel('dyna_configurator/expression_activeInstallBase');
            }
            $params['activeInstallBase'] = $this->expressionActiveInstallBase;
        }

        if(strpos($expression, 'installBase.') !== FALSE){
            if(!$this->expressionInstallBase){
                $this->expressionInstallBase = Mage::getModel('vznl_configurator/expression_installBase');
            }
            $params['installBase'] = $this->expressionInstallBase;
        }

        if(strpos($expression, 'agent.') !== FALSE){
            if(!$this->expressionAgent){
                $this->expressionAgent = Mage::getModel('dyna_configurator/expression_agent');
            }
            $params['agent'] = $this->expressionAgent;
        }

        if(strpos($expression, 'serviceability.') !== FALSE){
            if(!$this->expressionServiceability){
                $this->expressionServiceability = Mage::getModel('vznl_configurator/expression_serviceability');
            }
            $params['serviceability'] = $this->expressionServiceability;
        }

        return $params;
    }

    /**
     * @param Omnius_Checkout_Model_SalesRule_Rule $rule
     * @param bool $doNotSkipHints
     * @return bool
     */
    public function validateRuleServiceSourceExpression(Omnius_Checkout_Model_SalesRule_Rule $rule, $doNotSkipHints = false)
    {
        if ($rule->getSimpleAction() != self::SALES_HINT && $rule->getShowHint() && !$doNotSkipHints) {
            return true;
        }
        try {
            $this->expressionCart->setRule($rule);
            /**
             * @var $dynaCoreHelper Dyna_Core_Helper_Data
             */
            $dynaCoreHelper = Mage::helper('dyna_core');
            $ssExpression = $rule->getServiceSourceExpression();
            $inputParams = $this->getNeededInputParams($ssExpression);

            $passed = false;

            if (empty(trim($ssExpression)) || $dynaCoreHelper->evaluateExpressionLanguage($ssExpression, $inputParams)) {
                $passed = true;
            }
            return $passed;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @param Omnius_Checkout_Model_Sales_Quote_Item|Mage_Sales_Model_Quote_Item_Abstract $item
     * @param $isOverridden
     * @param $allRules
     * @param $appliedRuleIds
     * @param $address
     * @return array
     */
    protected function composeRules($item, $isOverridden, $allRules, $appliedRuleIds, $address)
    {
        if (!$isOverridden) {
            $promoRules = [];
            $rules = [];
            $couponRules = [];

            $quote = $address->getQuote();


            $foundValidRule = false;
            /** @var Omnius_Checkout_Model_SalesRule_Rule $rule */
            foreach ($allRules as $rule) {
                if (!$this->validateItemByRule($rule, $item)) {
                    continue;
                }
                $this->processRule($item->getPackageId(), $rule, $appliedRuleIds, $address, $promoRules, $couponRules, $rules);
                $foundValidRule = true;
            }

            if ($foundValidRule && $this->getCouponCode()) {
                /** @var Dyna_Package_Model_Package $packageModel */
                if ($orderEditQuoteId = Mage::getSingleton('customer/session')->getOrderEdit()) {
                    $orderEditQuote = Mage::getModel('sales/quote')->load($orderEditQuoteId);
                    $packageModel = Mage::getModel('package/package')->getCollection()
                        ->addFieldToFilter('package_id', $item->getPackageId())
                        ->addFieldToFilter('order_id', $orderEditQuote->getSuperOrderEditId())
                        ->getFirstItem();
                } else {
                    $packageModel = $quote->getCartPackage($item->getPackageId());
                }

                /** @var Omnius_PriceRules_Helper_External_Coupon_Validator $externalValidator */
                $externalValidator = Mage::helper('pricerules/external_coupon_validator');

                if($externalValidator->isValid($this->getCouponCode(), $quote, $packageModel)) {
                    $this->couponsApplied[$item->getPackageId()] = $this->getCouponCode();
                }
            }

            $rules = array_merge($rules, $couponRules, $promoRules);
        } else {
            $appliedRuleIds = $this->getPriceRulesHelper()->explodeTrimmed($item->getAppliedRuleIds());
            if ($item->getAppliedRuleIds() == $item->getManualRuleId() && $item->getManualRuleId() != null) {
                $appliedRuleIds = $this->getPriceRulesHelper()->explodeTrimmed($item->getPreviousRules());
            }
            $rules = Mage::getModel('salesrule/rule')->getCollection()->addFieldToFilter('rule_id', array('in' => $appliedRuleIds));
            $this->couponsApplied[$item->getPackageId()] = $this->getCouponCode();
        }

        return $rules;
    }

    /**
     * Quote item discount calculation process
     *
     * Footnote products should be excluded from rule processing
     *
     * @param   Omnius_Checkout_Model_Sales_Quote_Item|Mage_Sales_Model_Quote_Item_Abstract $item
     * @return  Mage_SalesRule_Model_Validator
     */
    public function process(Mage_Sales_Model_Quote_Item_Abstract $item)
    {
        if($item->getProduct()->getFootnoteId()) {
            return $this;
        }

        if ($item->getIsPromo()) {
            return $this;
        }
        if ($item->getPackageId() == $this->getActivePackage()->getPackageId()) {
            // Get applied rules ids from active package
            $appliedRuleIds = array_map('trim', explode(',', $this->getActivePackage()->getAppliedRuleIds()));
            /** @var Omnius_PriceRules_Helper_Data $priceHelper */
            $priceHelper = $this->getPriceRulesHelper();
            // We have a max accepted iteration for the no of applied rules per packet
            if(count($appliedRuleIds) >=  $priceHelper::$maxIterations){
                return $this;
            }

            $allRules = $this->_getRules();
            if (!count($allRules)) {
                return $this;
            }

            //temporarily store the applied rule ids to determine if save needs to be called
            $beforeRulesIds = $this->getActivePackage()->getAppliedRuleIds();

            $address = $this->_getAddress($item);
            /** @var Omnius_Checkout_Model_Sales_Quote $quote */
            $quote = $address->getQuote();
            $initialItems = Mage::getSingleton('customer/session')->getOrderEditMode() ? Mage::getSingleton('checkout/session')->getInitialItems() : [];
            $existingRules = $this->getPriceRulesHelper()->explodeTrimmed($item->getAppliedRuleIds());
            $isDelivered = $this->getActivePackage()->isDelivered();
            $deliveredNotPromoCond = $isDelivered && Mage::getSingleton('customer/session')->getOrderEditMode() && !$item->isPromo();
            $deliveredPromoCond = $isDelivered && Mage::getSingleton('customer/session')->getOrderEditMode() && $item->isPromo();
            $itemProdCond = $deliveredNotPromoCond && isset($initialItems[$item->getProductId()]) && $quote->hasProductId($item->getProductId());
            $itemTargetCond = $deliveredPromoCond && isset($initialItems[$item->getTargetId()]) && $quote->hasProductId($item->getTargetId());

            $overrideCond = Mage::registry('is_override_promos') || $quote->getIsOverrulePromoMode();

            $isOverridden = $overrideCond || $itemProdCond || $itemTargetCond;
            $rules = $this->composeRules($item, $isOverridden, $allRules, $existingRules, $address);

            // overwrite rules
            $key = $this->getWebsiteId() . '_' . $this->getCustomerGroupId() . '_' . $this->getCouponCode() . '_' . $item->getPackageId() . '_' . $item->getProductId();
            $this->_rules[$key] = $rules;

            $allowedActions = [
                'ampromo_items' =>"promo_process_items_after",
                'ampromo_remove_items' =>"promo_process_items_remove",
                'ampromo_replace_items' =>"promo_process_items_replace",

                'process_add_product' => "process_add_product",
                'process_remove_product' => "process_remove_product",
                'process_replace_product' =>"process_replace_product",
                'sales_hint' => "sales_hint"
            ];

            $productActions = [
                'process_add_product', 'process_replace_product'
            ];

            $validHints = array();
            foreach ($rules as $rule) {
                $action = $rule->getSimpleAction();
                if (!isset($allowedActions[$action])
                    || (in_array($rule->getId(), $appliedRuleIds) && in_array($rule->getSimpleAction(), $productActions))
                ) {
                    continue;
                }

                if ($rule->getShowHint() && $rule->getServiceSourceExpression() && $rule->getSimpleAction() !== self::SALES_HINT) {
                    $validHints[] = $rule->getId();
                } else {
                    Mage::dispatchEvent($allowedActions[$rule->getSimpleAction()], array(
                        'rule' => $rule,
                        'item' => $item,
                        'quote' => $quote,
                        'address' => $address
                    ));

                    $this->getActivePackage()->setAppliedRuleIds($this->mergeIds($this->getActivePackage()->getAppliedRuleIds(), $rule->getId()));
                }
            }

            // iterating again for hinting (@see VFDED1W3S-869) for validating each rule
            $flippedValidHints = array_flip($validHints);
            foreach ($rules as $rule) {
                $ruleId = $rule->getId();
                if (empty($flippedValidHints[$ruleId])) {
                    continue;
                }
                // do a real validation by evaluation expression
                $this->validateRuleServiceSourceExpression($rule, true);
            }

            // if there was any change in the applied rule ids, call the package save
            $afterRulesIds = $this->getActivePackage()->getAppliedRuleIds();
            if (!empty($beforeRulesIds)) {
                if (!is_array($beforeRulesIds)) {
                    $beforeRulesIds = [$beforeRulesIds];
                }
            } else {
                $beforeRulesIds = [];
            }
            if (!empty($afterRulesIds)) {
                if (!is_array($afterRulesIds)) {
                    $afterRulesIds = [$afterRulesIds];
                }
            } else {
                $afterRulesIds = [];
            }
            if (array_intersect($beforeRulesIds, $afterRulesIds)) {
                $this->getActivePackage()->save();
            }
        }

        return $this;
    }

    /**
     * Promo rule validation for items and caches the result for the current collect totals execution.
     * @param Omnius_Checkout_Model_SalesRule_Rule $rule
     * @param Omnius_Checkout_Model_Sales_Quote_Item $item
     * @return bool
     */
    public function validateItemByRule($rule, $item)
    {
        // if rule is defaulted (is_promo=no) and adds a promo
        if (!$rule->getIsPromo() && ($promoSku = $rule->getPromoSku()) && ($rule->getId() != $item->getAppliedRuleIds())) {
            // check if the promo is not in a mutually exclusive family whose other promo product is already in cart
            $mutuallyExclusiveCartProducts = Mage::helper('vznl_pricerules')->getMutuallyExclusiveCartPromos();
            // if so, skip this rule
            if (in_array($promoSku, $mutuallyExclusiveCartProducts)) {
                return false;
            }
        }

        if (isset($this->_validated[$rule->getId()]) && isset($this->_validated[$rule->getId()][$item->getProductId()])) {
            return $this->_validated[$rule->getId()][$item->getProductId()];
        } else {
            $this->_validated[$rule->getId()][$item->getProductId()] = $rule->getActions()->validate($item);
            return $this->_validated[$rule->getId()][$item->getProductId()];
        }
    }
}
