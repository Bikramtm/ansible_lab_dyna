<?php

/**
 * Class Vznl_PriceRules_Model_Condition_PackageType
 */
class Vznl_PriceRules_Model_Condition_PackageType extends Omnius_PriceRules_Model_Condition_Abstract
{
    public function loadAttributeOptions()
    {
        $this->setupSelectCondition('Package type', 'package_type');
        return $this;
    }

    public function getValueSelectOptions()
    {
        /** @var Vznl_PriceRules_Model_Eav_Entity_Attribute_Source_PackageType $optionsModel */
        $optionsModel = Mage::getModel('vznl_pricerules/eav_entity_attribute_source_packageType');
        $options = $optionsModel->getAllOptions();

        return $this->setupOptions($options);
    }

    public function validate(Varien_Object $object)
    {
        /** @var Dyna_Catalog_Helper_Data $catalogHelper */
        $catalogHelper = Mage::helper("dyna_catalog");
        $packageId = $catalogHelper->getPackageTypeId();
        if ($packageId !== null) {
            return $this->validateAttribute($packageId);
        }
        return false;
    }

    /**
     * Returns types of condition operators.
     * @return array
     */
    public function getOperatorSelectOptions()
    {
        $options = array(
            '()' => Mage::helper('rule')->__('is one of'),
            '!()' => Mage::helper('rule')->__('is not one of')
        );

        $operatorOptions = array();
        foreach ($options as $value => $label) {
            $operatorOptions[] = array(
                'value' => $value,
                'label' => $label,
            );
        }

        return $operatorOptions;
    }

    /**
     * @return string
     */
    public function getInputType()
    {
        return 'multiselect';
    }

    /**
     * @return string
     */
    public function getValueElementType()
    {
        return 'multiselect';
    }
}
