<?php
/**
 * Installer that adds package_type column to salesrule
 */


/** @var Mage_Sales_Model_Entity_Setup $this */
$installer = $this;
$installer->startSetup();

$table = $installer->getTable('salesrule');
$columnName = 'package_type';

// add package_type to salesrule table
if (!$installer->getConnection()->tableColumnExists($table, $columnName)) {
    $installer->getConnection()->addColumn($table, $columnName, [
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'length' => 255,
        'default' => null,
        'nullable' => true,
        'required' => false,
        'comment' => "Package Type",
    ]);
}

$installer->endSetup();
