<?php
/**
 * Installer that adds package_type column to salesrule
 */


/** @var Mage_Sales_Model_Entity_Setup $this */
$installer = $this;
$installer->startSetup();

$table = $installer->getTable('salesrule');
$columnName = 'sr_unique_id';

// add package_type to salesrule table
if (!$installer->getConnection()->tableColumnExists($table, $columnName)) {
    $installer->getConnection()->addColumn($table, $columnName, [
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'length' => 255,
        'after' => 'rule_id',
        'comment' => "Unique Identifier of a Sales Rule",
    ]);
}

$installer->endSetup();
