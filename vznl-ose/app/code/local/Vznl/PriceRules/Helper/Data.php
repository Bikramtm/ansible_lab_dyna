<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Vznl_PriceRules_Helper_Data
 */
class Vznl_PriceRules_Helper_Data extends Omnius_PriceRules_Helper_Data
{
    protected $promoRulesByTarget = array();
    protected $promoRulesSkus = array();

    /**
     * Increments times used for all the rules from the provided quote.
     * @param $quote
     */
    public function reinitSalesRules($quote)
    {
        $packageCollection = Mage::getResourceModel('package/package_collection')
            ->addFieldToFilter('quote_id', $quote->getId());
        $couponsPerPackage = array();
        $retainableCtns = array();

        foreach ($packageCollection as $package) {

            $couponsPerPackage[$package->getPackageId()] = $package->getCoupon();
            $package->setCoupon(null);
            // Check if the ctn from retention packages is still retainable
            $retentionChecked = Mage::getSingleton('customer/session')->getRetainableCtnsChecked() == $quote->getCustomer()->getCustomerNumber();
            if ($package->isRetention() && !$retentionChecked) {

                //if package has at least one retainable package, try to get retainable ctns
                if (empty($retainableCtns))
                    $retainableCtns = Mage::helper('omnius_customer')->getRetainableCtns($quote);

                if (!isset($ctnsList)) {
                    $extractCtns = function ($arr) {
                        return $arr['ctn'];
                    };
                    $retainableCtns['ctns'] = !empty($retainableCtns['ctns']) ? $retainableCtns['ctns'] : [];
                    // Create an array with all the retainable ctns for this customer
                    $ctnsList = array_map($extractCtns, $retainableCtns['ctns']);
                }

                $flippedCtnsList = array_flip($ctnsList);
                $packageCtn = $package->getCtn();
                if (!isset($flippedCtnsList[$packageCtn])) {
                    // Set package type to aquisition if the ctn is not retainable anymore
                    $package->setSaleType(Omnius_Checkout_Model_Sales_Quote_Item::ACQUISITION)
                        ->setCtn(null);
                }
            }
            $package->save();
        }

        $quote->setCouponsPerPackage($couponsPerPackage);

        foreach ($quote->getAllItems() as $item) {
            $item->setManualRuleId($item->getAppliedRuleIds());
            $ruleIds = $this->explodeTrimmed($item->getAppliedRuleIds());
            $keep = [];
            foreach ($ruleIds as $ruleIdentifier) {
                $rule = Mage::getModel('salesrule/rule')->load($ruleIdentifier);
                if ($rule->getId() && $rule->getIsPromo()) {
                    $keep[] = $rule->getId();
                }
            }
            $item->setAppliedRuleIds(implode(',', $keep));
        }

        $quote->getBillingAddress();
        $quote->getShippingAddress()->setCollectShippingRates(true);
        $quote->collectTotals();
        try {
            $quote->save();
        } catch (Zend_Db_Statement_Exception $e) {
            if (stripos($e->getMessage(), 'SQLSTATE[40001]') !== false) {
                $retries = 2;
                do {
                    Mage::getSingleton('core/logger')->logException($e);
                    sleep(1);
                    try {
                        $quote->save();
                        $retries = false;
                    } catch (Zend_Db_Statement_Exception $e) {
                        $retries--;
                    }
                } while ($retries > 0);

                if ($retries !== false) {
                    throw $e;
                }
            }
        }                 

        return $quote;
    }

    /**
     * Checks for applicable promo rules on the current package.
     * Group by product
     * @param $activePackageId
     * @param $quoteItem
     * @param $appliedPromoRules
     * @param $rules
     * @param $validator
     * @param $shippingAddress
     * @param $applicableRules
     * @param $allExclusiveFamilyCategoryIds
     */
    protected function parseApplicableRuleForTargetProduct($activePackageId, $quoteItem, $appliedPromoRules, $rules, $validator, $shippingAddress, &$applicableRules, $allExclusiveFamilyCategoryIds)
    {
        $targetProductId = $quoteItem->getProductId();

        if (!$quoteItem->isPromo() && $quoteItem->getAppliedRuleIds()) {
            $appliedPromoRules = array_merge(
                $appliedPromoRules,
                explode(",", $quoteItem->getAppliedRuleIds())
            );
        }
        $flippedAppliedPromoRules = array_flip($appliedPromoRules);
        foreach ($rules as $rule) {
            $ruleActions = $rule->getActions();
            if ($rule->getIsPromo() == 1
                && !empty($ruleActions->getConditions())
                && $validator->canProcessRules($rule, $shippingAddress, $activePackageId)
                && $validator->validateItemByRule($rule, $quoteItem)
            ) {
                $ruleId = $rule->getRuleId();
                if (isset($flippedAppliedPromoRules[$ruleId])) {
                    $rule->setIsCurrentlyApplied(true);
                }

                if (!isset($applicableRules[$targetProductId])) {
                    $applicableRules[$targetProductId] = array();
                }

                $promoIds = [];
                $promoSkus = explode(',', $rule->getPromoSku());
                foreach ($promoSkus as $promoSku) {
                    $promoSku = trim($promoSku);
                    $promoProduct = Mage::getModel("catalog/product")->load(Mage::getModel("catalog/product")->getIdBySku($promoSku));
                    $promoIds[$promoSku] = $promoProduct->getId();
                    $rule->setProductPromoIds($promoIds);

                    if ($categoriesExclusive = array_intersect($promoProduct->getCategoryIds(), $allExclusiveFamilyCategoryIds)) {
                        if (!isset($applicableRules[$targetProductId]['exclusiveFamilies'])) {
                            $applicableRules[$targetProductId]['exclusiveFamilies'] = array();
                        }

                        foreach ($categoriesExclusive as $categoryExclusive) {
                            $rule->setExclusiveFamily($categoryExclusive);
                            $applicableRules[$targetProductId]['exclusiveFamilies'][$categoryExclusive] = !isset($applicableRules[$targetProductId]['exclusiveFamilies'][$categoryExclusive]) ?
                                array($rule->getRuleId()) : array_merge($applicableRules[$targetProductId]['exclusiveFamilies'][$categoryExclusive], array($rule->getRuleId()));
                        }
                    }
                }
                $applicableRules[$targetProductId][$rule->getRuleId()] = $rule;
            }
        }
    }

    /**
     * @param int $activePackageId
     * @return array|int
     *
     * Finds what promo rules can be applied for a certain package
     */
    public function findApplicablePromoRulesByTarget($activePackageId = null)
    {
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        if (!$activePackageId) {
            $activePackageId = $quote->getActivePackageId();
        }

        $quoteItems = $quote->getAllItems();
        $cacheKey = $activePackageId . '_' . implode('_', array_keys($quoteItems));

        if (!isset($this->promoRulesByTarget[$cacheKey])) {
            $applicableRules = [];
            $appliedPromoRules = [];

            /** @var Omnius_PriceRules_Model_Validator $validator */
            $validator = Mage::getModel('pricerules/validator');
            $shippingAddress = $quote->getShippingAddress();

            $allExclusiveFamilyCategoryIds = Mage::getModel('catalog/category')->getAllExclusiveFamilyCategoryIds();

            $rules = Mage::helper('rulematch')
                ->setActivePackage(Mage::getSingleton('checkout/cart')->getQuote()->getCartPackage($activePackageId))
                ->getApplicableRules(true);

            foreach ($quoteItems as $quoteItem) {
                if ($quoteItem->getPackageId() == $activePackageId && !$quoteItem->isDeleted()) {
                    $this->parseApplicableRuleForTargetProduct($activePackageId, $quoteItem, $appliedPromoRules, $rules, $validator, $shippingAddress, $applicableRules, $allExclusiveFamilyCategoryIds);
                }
            }

            $this->promoRulesByTarget[$cacheKey] = $applicableRules;
        }

        return $this->promoRulesByTarget[$cacheKey];
    }

    /**
     * @param Vznl_Checkout_Model_Sales_Quote $quote
     * @param $promoRules
     * @return array
     */
    public function checkBundleAddedPromosExist($quote, $promoRules)
    {
        $allExclusiveFamilyCategoryIds = Mage::getModel('catalog/category')->getAllExclusiveFamilyCategoryIds();
        /** @var Vznl_Checkout_Model_Sales_Quote_Item $item */
        foreach ($quote->getPackageItems($quote->getActivePackageId()) as $item) {
            if ($item->isBundlePromo()) {
                $categoryIds = $item->getProduct()->getCategoryIds();
                $targetProductId = $item->getTargetItem()->getProductId();
                if (isset($promoRules[$targetProductId])) {
                    foreach ($promoRules[$targetProductId] as $id => $promoRule) {
                        if (is_numeric($id)) {
                            $categoriesExclusive[0] = $promoRule->getExclusiveFamily();
                            if ((array_intersect($categoryIds, $categoriesExclusive) && array_intersect($categoryIds, $allExclusiveFamilyCategoryIds))
                            || (in_array($item->getProductId(), $promoRule->getProductPromoIds()))) {
                                unset($promoRules[$targetProductId][$promoRule->getId()]);
                                //if any rule is unset from $promoRules array due to existing bundle promo action then promo info icon should be displayed
                                $promoRules[$targetProductId]['disabledPromosText'] = Mage::getStoreConfig('omnius_service/general/promo_info_box');
                            }
                        }
                    }
                }
            }
        }

        return $promoRules;
    }

    /**
     * Manually apply promo rules
     *
     * @param int $ruleId
     * @param bool $returnJsonData
     * @param bool $collect
     * @return array
     *
     * Try to apply a promo rule
     */
    public function applyPromoRule($ruleId, $returnJsonData = false, $collect = true)
    {
        $packageId = Mage::registry('targetPackageId');
        Mage::unregister('targetPackageId');

        $exclusivFamilyId = Mage::registry('exclusivFamilyId');
        Mage::unregister('exclusivFamilyId');

        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $activePackageId = $quote->getActivePackageId();

        $promoRules = $this->findApplicablePromoRulesByTarget($packageId);
        $promoRules = $this->checkBundleAddedPromosExist($quote, $promoRules);

        $currentAppliedRuleIds = explode(',',$quote->getAppliedRuleIds());
        $isRuleBeingRemoved = in_array($ruleId, $currentAppliedRuleIds);

        $updated = false;
        $itemsToDelete = array();
        /** @var Vznl_Checkout_Model_Sales_Quote_Item $item */
        foreach ($quote->getItemsCollection() as $item) {
                $item->setRemoveRuleId($item->getAppliedRuleIds());
                if (!$item->isDeleted() && !$item->getParentItemId() && !$item->getIsPromo()) { //only visible items
                    if ($item->getPackageId() != $packageId) {
                        continue;
                    }

                    $item->setManualRuleId($ruleId);
                    $item->setPreviousRules($item->getAppliedRuleIds());

                    // handle multiple ruling
                if (!empty($promoRules[$item->getProductId()][$ruleId]) || ($ruleId == 0)) {
                    $this->applyMultipleRuleIds($item, $ruleId, $promoRules, $itemsToDelete, $exclusivFamilyId);
                    $item->setAppliedRuleIds($ruleId);
                }
                // save item and update error flag
                $item->save();
                $updated = true;
            }
        }
        if (!$updated) {
            return array(
                'error' => true,
                'message' => 'Could not apply promo rule',
            );
        }

        $package = $quote->getCartPackage($packageId);
        $previousPromosOrder = json_decode($package->getAppliedPromosOrder(), true);
        $previousPromosOrder = !empty($previousPromosOrder) ? $previousPromosOrder : array();

        if ($collect) {
            $quote->setTotalsCollectedFlag(false)
                ->collectTotals()
                ->save();
        }

        $newPackageProductsCollection = Mage::getModel('sales/quote_item')->getCollection()
            ->addFieldToFilter('quote_id', $quote->getId())
            ->addFieldToFilter('package_id', $packageId);

        $newPackageProducts = $newPackageProductsCollection
            ->getColumnValues('product_id');
        $flippedNewPackageProducts = array_flip($newPackageProducts);

        // first of all, delete all entries from previous added promos if an
        // older promo has been removed from new products array (most likely by deselecting the rule from drawer)
        if (!empty($previousPromosOrder)) {
            foreach ($previousPromosOrder as $key => $previousProductId) {
                if (!isset($flippedNewPackageProducts[$previousProductId])) {
                    unset($previousPromosOrder[$key]);
                }
            }
        }
        $flippedItemsToDelete = array_flip($itemsToDelete);
        $flippedPreviousPromosOrder = array_flip($previousPromosOrder);
        // then, check the new added promo and store it in previous promos array
        foreach ($newPackageProducts as $newProductId) {
            // firstly, if a promo of a default rule has been found in cart and is part
            // of a mutually exclusive category whose other promo is now being added
            if (isset($flippedItemsToDelete[$newProductId]) && !$isRuleBeingRemoved) {
                foreach ($newPackageProductsCollection as $newPackageProduct) {
                    if ($newPackageProduct->getProductId() == $newProductId) {
                        // then remove it
                        $newPackageProduct->isDeleted(true);
                        $newPackageProduct->save();
                    }
                }
                continue;
            }
            if (!isset($flippedPreviousPromosOrder[$newProductId])) {
                array_push($previousPromosOrder, $newProductId);
            }
        }

        // and now, save the updated previous promos array
        $package->setAppliedPromosOrder(json_encode($previousPromosOrder));
        $package->save();

        $quote->setData('collect_totals_cache_items', false);

        $newPromoRules = $this->findApplicablePromoRulesByTarget($packageId);
        $newPromoRules = $this->checkBundleAddedPromosExist($quote, $newPromoRules);
        $promoRulesTemplate = Mage::getSingleton('core/layout')->createBlock('core/template')
            ->setPackage($package)
            ->setPromoRules($newPromoRules)
            ->setTemplate('configurator/mobile/sections/partials/general/promo_rules.phtml')->toHtml();

        return array(
            'error' => false,
            'stackablePromotionsDrawer' => $promoRulesTemplate,
            'rightBlock' => Mage::getSingleton('core/layout')
                ->createBlock('checkout/cart_totals')
                ->setTemplate('configurator/generic/sections/partials/package_block.phtml')
                ->setCurrentPackageId($activePackageId)
                ->toHtml(),
            'totals' => Mage::getSingleton('core/layout')
                ->createBlock('checkout/cart_totals')
                ->setTemplate('checkout/cart/totals.phtml')
                ->toHtml()
        );
    }

    /**
     * In case of multiple ruling, add/remove the selected rule
     * @param $item
     * @param &$ruleId
     * @param $promoRules
     */
    protected function applyMultipleRuleIds($item, &$ruleId, $promoRules, &$itemsToDelete, $exclusivFamilyId = null)
    {
        $rule = ($ruleId == 0) ? null : $promoRules[$item->getProductId()][$ruleId];
        // if there are already other promos applied on cart, add/remove the selected one
        if (!empty($item->getAppliedRuleIds())) {
            $appliedRules = explode(',', $item->getAppliedRuleIds());

            $targetProductExclusiveFamilyIds = [];
            $targetMutuallyExclusiveFamilyId = 0;
            if (!empty($promoRules[$item->getProductId()]['exclusiveFamilies'])) {
                foreach ($promoRules[$item->getProductId()]['exclusiveFamilies'] as $targetFamilyId => $targetFamilyRules) {
                    $targetMutuallyExclusiveFamilyId = $targetFamilyId;
                    $flippedTargetFamilyRules = array_flip($targetFamilyRules);
                    if (isset($flippedTargetFamilyRules[$ruleId])) {
                        $targetProductExclusiveFamilyIds = $targetFamilyRules;
                        break;
                    }
                }
            }

            // No selection button handling (mutually exclusive rules)
            if (($ruleId == 0) && (!empty($exclusivFamilyId)) && array_key_exists($exclusivFamilyId, $promoRules[$item->getProductId()]['exclusiveFamilies'])) {
                $this->handleExclusiveFamilies($promoRules[$item->getProductId()]['exclusiveFamilies'][$exclusivFamilyId], $promoRules[$item->getProductId()], $appliedRules);
            }

            // Mutually exclusive promo rules handling
            if (($ruleId != 0) && (!empty($targetProductExclusiveFamilyIds))) {
                // first, get all exclusive families from currently applied promos
                if (in_array($ruleId, $targetProductExclusiveFamilyIds) && count($targetProductExclusiveFamilyIds) > 1) {
                    $this->handleExclusiveFamilies($targetProductExclusiveFamilyIds, $promoRules[$item->getProductId()], $appliedRules);
                }
                // if a default rule is still active and its promo is part of a mutually exclusive category
                // whose other product is now being added by a promo rule, remove from cart the default rule's promo
                if (!empty($appliedRules)) {
                    $itemsToDelete = array_merge($itemsToDelete, $this->getMutuallyExclusivePromosOfDefaulted($targetMutuallyExclusiveFamilyId));
                    // if somehow promo of current promo rule is the same as one from the above defaulted promos,
                    // remove the corresponding sku from the above array in order to not delete that after cart evaluation
                    $promoRuleSkus = explode(',', $rule->getPromoSku());
                    foreach($promoRuleSkus as $promoRuleSku){
                        $currentPromoRuleSku = Mage::getModel('dyna_catalog/product')->getIdBySku($promoRuleSku);
                        if (in_array($currentPromoRuleSku, $itemsToDelete)
                            && (($key = array_search($currentPromoRuleSku, $itemsToDelete)) !== false)) {
                            unset($itemsToDelete[$key]);
                        }
                    }
                }
            }

            // Non mutually exclusive promo rules handling
            if (in_array($ruleId, $appliedRules) && ($rule)) {
                if (($key = array_search($ruleId, $appliedRules)) !== false) {
                    unset($appliedRules[$key]);
                    // if deselected, this means that rule should not be marked as applied anymore
                    if ($rule) {
                        $rule->setIsCurrentlyApplied(false);
                    }
                }

                // if the only left rule in cart is a defaulted one, then don't mark its promo for deletion
                // (in order to restore it in cart if current action is the removal of a stackable promo)
                if (count($appliedRules) == 1) {
                    $this->handleLeftDefaultedRule($appliedRules, $itemsToDelete);
                }

                $ruleId = implode(',', $appliedRules);
            } else {
                // mark rule as applied
                if ($rule) {
                    $rule->setIsCurrentlyApplied(true);
                }
                // if regular rule has been selected, add it to already applied rules
                // otherwise (No selection btn) return just the cleaned applied rules (without mutually exclusive ones)
                $appliedRules = !empty($appliedRules) ? implode(',', $appliedRules) : '';

                $ruleId = ($ruleId == 0) ?
                    $appliedRules : (($appliedRules != '') ? $appliedRules . ',' . $ruleId : $ruleId);
            }
            unset($appliedRules);
        } else {
            // if No selection btn, apply no rules (although this scenario couldn't be triggered(?))
            if ($ruleId != 0) {
                $rule->setIsCurrentlyApplied(true);
            }
        }
    }

    /**
     * Handle exclusive families
     *
     * @param $targetProductExclusiveFamilyIds
     * @param $allPromoRules
     * @param $appliedRules
     */
    protected function handleExclusiveFamilies($targetProductExclusiveFamilyIds, &$allPromoRules, &$appliedRules)
    {
        // loop through all exclusive family ids
        foreach ($targetProductExclusiveFamilyIds as $targetProductExclusiveFamilyId) {
            // loop through all rules
            foreach ($allPromoRules as $ruleId => $rule) {
                // if current rule is a mutually exclusive one
                // and family rule has been already applied in cart
                if ($targetProductExclusiveFamilyId == $ruleId
                    && (($key = array_search($ruleId, $appliedRules)) !== false)) {
                    // remove it from appliedRules array
                    unset($appliedRules[$key]);
                    // and then mark it as not currently applied
                    $rule->setIsCurrentlyApplied(false);
                }
            }
        }
    }

    /**
     * Get select options for promo labels
     * @return array
     */
    public function getPromotionLabelsOptionValues()
    {
        $values = array('Friends and Family', 'Dealcloser', 'Default');
        $selectOutput = array();

        foreach ($values as $value) {
            $selectOutput[] = array(
                'label' => $value,
                'value' => $value
            );
        }

        return $selectOutput;
    }

    /**
     * Get mutually exclusive promos from the cart added by the defaulted (is_promo=no) sales rules
     * @param $targetMutuallyExclusiveFamilyId
     * @return array
     */
    protected function getMutuallyExclusivePromosOfDefaulted($targetMutuallyExclusiveFamilyId)
    {
        $itemsToDelete = array();
        $defaultRulePromoSku = array();
        $promoRulePromoSku = array();
        $quoteAppliedRules = explode(',', Mage::getSingleton('checkout/cart')->getQuote()->getAppliedRuleIds());
        $allMutuallyExclusiveProducts = Mage::helper('vznl_catalog')->getMutuallyExclusiveProducts();
        $flippedallMutuallyExclusiveProductsTargetId = array_flip($allMutuallyExclusiveProducts[$targetMutuallyExclusiveFamilyId]);

        foreach ($quoteAppliedRules as $key => $defaultedRuleId) {
            $defaultedRule = Mage::getModel('salesrule/rule')->load($defaultedRuleId);
            $promoSku = explode(',', $defaultedRule->getPromoSku());
            if (!$defaultedRule->getIsPromo()) {
                foreach ($promoSku as $defaultedPromo) {
                    $trimmedDefaultPromo = trim($defaultedPromo);
                    if (isset($flippedallMutuallyExclusiveProductsTargetId[$trimmedDefaultPromo])) {
                        // if promo of default rule is in the same mutually exclusive family of the target product,
                        // then mark this product for deletion
                        $defaultRulePromoSku[] = Mage::getModel('dyna_catalog/product')->getIdBySku($trimmedDefaultPromo);
                    }
                }
            } else {
                foreach ($promoSku as $defaultedPromo) {
                    $trimmedDefaultPromo = trim($defaultedPromo);
                    if (isset($flippedallMutuallyExclusiveProductsTargetId[$trimmedDefaultPromo])) {
                        // if promo of prom rule is in the same mutually exclusive family of the target product,
                        //prevent deletion if it was the same product added by both default and promo rules
                        $promoRulePromoSku[] = Mage::getModel('dyna_catalog/product')->getIdBySku($trimmedDefaultPromo);
                    }
                }
            }
            $itemsToDelete[] = array_merge(array_diff($defaultRulePromoSku, $promoRulePromoSku), array_diff($promoRulePromoSku, $defaultRulePromoSku));
        }
        return array_unique($itemsToDelete);
    }

    /**
     * Get all mutually exclusive products from the categories whose product/s are in the active cart package
     * @return array
     */
    public function getMutuallyExclusiveCartPromos($packageId = null)
    {
        /** @var Vznl_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $packageId = $packageId ?: $quote->getActivePackageId();

        $quoteItems = $quote->getPackageItems($packageId);
        // get all mutually exclusive products
        $allMutuallyExclusiveProducts = Mage::helper('vznl_catalog')->getMutuallyExclusiveProducts();

        $mutuallyExclusiveCartPromos = array();
        $sortProducts = function($products, &$mutuallyExclusiveCartPromos) {
            foreach ($products as $product) {
                if (!in_array($product, $mutuallyExclusiveCartPromos)) {
                    array_push($mutuallyExclusiveCartPromos, $product);
                }
            }
        };
        foreach ($quoteItems as $quoteItem) {
            foreach ($allMutuallyExclusiveProducts as $categoryId => $products) {
                foreach ($products as $product) {
                        if ($quoteItem->getSku() == $product) {
                        // if cart promo is mutually exclusive, get all the products of that family
                        $sortProducts($allMutuallyExclusiveProducts[$categoryId], $mutuallyExclusiveCartPromos);
                    }
                }
            }
        }

        return $mutuallyExclusiveCartPromos;
    }

    /**
     * Check if defaulted sales rule's (is_promo=no) promo product is part of a mutually exclusive category
     * whose another promo product is already part of the cart (same category of the defaulted promo)
     * @param $ruleId
     * @param $promoProduct
     * @return bool
     */
    public function defaultedPromoIsExclusiveEligible($ruleId, $promoProduct)
    {
        /** @var Vznl_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        /** @var Vznl_Checkout_Model_Sales_Quote_Item $quoteItems */
        $quoteItems = Mage::getModel('sales/quote_item')->getCollection()
            ->addFieldToFilter('quote_id', $quote->getId())
            ->addFieldToFilter('package_id', $quote->getActivePackageId());

        /* Handle promo products */

        $mutuallyExclusiveCartPromos = array();
        $sortProductsByCategory = function($products, &$mutuallyExclusiveCartPromos) {
            foreach ($products as $product) {
                if (!in_array($product, $mutuallyExclusiveCartPromos)) {
                    array_push($mutuallyExclusiveCartPromos, $product);
                }
            }
        };

        // get all mutually exclusive products
        $allMutuallyExclusiveProducts = Mage::helper('vznl_catalog')->getMutuallyExclusiveProducts();

        foreach ($allMutuallyExclusiveProducts as $categoryId => $products) {
            foreach ($products as $product) {
                if ($product == $promoProduct) {
                    // first, get all mutually exclusive products that have the same category as those from
                    // the defaulted promo rule (the product that is given as parameter to this method)
                    $sortProductsByCategory($allMutuallyExclusiveProducts[$categoryId], $mutuallyExclusiveCartPromos);
                }
            }
        }

        /* --------------------- */

        /* Handle applied rules of target */

        $otherRuleFromSameCategory = false;
        // now check the other applied rules on the target
        foreach ($quoteItems as $quoteItem) {
            if (!empty($appliedRulesIds = $quoteItem->getAppliedRuleIds())) {
                $appliedRulesIds = explode(',', $appliedRulesIds);
                $flippedAppliedRulesIds = array_flip($appliedRulesIds);
                if (isset($flippedAppliedRulesIds[$ruleId])) {
                    $this->existsRuleFromSameCategory($ruleId, $appliedRulesIds, $mutuallyExclusiveCartPromos, $otherRuleFromSameCategory, $promoProduct);
                }
            }
        }
        /* ------------------------------ */

        if ($otherRuleFromSameCategory) {
            // so, if other applied rule has been found on target, this means that there is or there will be added
            // another promo from the same ME category of the given defaulted promo, making therefore this
            // defaulted promo invalid for the current cart
            return false;
        }

        return true;
    }

    /**
     * Check if in cart there is another applied sales rule whose promo product is
     * in the same ME category of the given defaulted promo
     * @param $ruleId
     * @param $appliedRulesIds
     * @param $mutuallyExclusiveCartPromos
     * @param $otherRuleFromSameCategory
     * @param $promoProduct
     */
    protected function existsRuleFromSameCategory($ruleId, &$appliedRulesIds, $mutuallyExclusiveCartPromos, &$otherRuleFromSameCategory, $promoProduct)
    {
        $flippedMutuallyExclusiveCartPromos = array_flip($mutuallyExclusiveCartPromos);
        // remove the current rule in order to have just the others in one array
        if (($key = array_search($ruleId, $appliedRulesIds)) !== false) {
            unset($appliedRulesIds[$key]);
        }
        // iterate through all applied sales rules
        foreach ($appliedRulesIds as $appliedRuleId) {
            $salesRule = Mage::getModel('salesrule/rule')->load($appliedRuleId);
            $salesRulePromoSku = $salesRule->getPromoSku();
            $salesRulePromos = array();
            // get the promo/s of current rule
            if (strpos($salesRulePromoSku, ',') !== false) {
                $salesRulePromos = explode(',', $salesRulePromoSku);
            } elseif (!empty($salesRulePromoSku)) {
                array_push($salesRulePromos, $salesRulePromoSku);
            }

            // if there is another rule which adds the same promo product,
            // then don't mark this rule as part of the same ME family
            if (in_array($promoProduct, $salesRulePromos)) {
                continue;
            }

            // if any of these promos is in the ME family as the promo of the defaulted
            // rule (the one given as parameter)
            foreach ($salesRulePromos as $salesRulePromo) {
                if (isset($flippedMutuallyExclusiveCartPromos[$salesRulePromo])) {
                    // then there is another applied rule in cart whose promo product is in the
                    // same ME category as the received promo product parameter
                    $otherRuleFromSameCategory = true;
                }
            }
        }
    }

    /**
     * If a stackable rule has been unselected and the only one remaining rule in cart is a defaulted one
     * then don't mark the defaulted's promo products for deletion
     * @param $appliedRules
     * @param $itemsToDelete
     */
    public function handleLeftDefaultedRule($appliedRules, &$itemsToDelete)
    {
        $salesRule = Mage::getModel('salesrule/rule')->load($appliedRules);
        // only if a defaulted rule is in cart
        if (!($salesRule->getIsPromo())) {
            $defaultedPromos = array_map('trim', explode(',', $salesRule->getPromoSku()));
            foreach ($defaultedPromos as $defaultedPromo) {
                $defaultedPromo = Mage::getModel("catalog/product")->getIdBySku($defaultedPromo);
                // if promo product of the defaulted has been already marked for deletion, then make it allowed again
                if (in_array($defaultedPromo, $itemsToDelete)
                    && (($key = array_search($defaultedPromo, $itemsToDelete)) !== false)) {
                    unset($itemsToDelete[$key]);
                }
            }
        }
    }

    /**
     * Returns a list of non promo rules with their promo skus
     * If a specific promo sku is provided, method will try to return the rules that adds that promo
     * @param null $specificSku
     * @return array
     */
    public function getSkuRulesList($specificSku = null)
    {
        if(empty($this->promoRulesSkus)){
            $rules = Mage::getModel('salesrule/rule')->getCollection()
                ->addFieldToFilter('promo_sku', array('notnull' => true))
                ->addFieldToFilter('is_promo', 0);

            foreach($rules as $rule){
                $this->promoRulesSkus[$rule->getId()] = $rule->getPromoSku();
            }
        }

        if($specificSku){
            $specificRules = [];
            foreach($this->promoRulesSkus as $ruleId => $promoSku){
                if(strcasecmp($promoSku, $specificSku) === 0){
                    $specificRules[] = $ruleId;
                }
            }
            return $specificRules;
        }

        return $this->promoRulesSkus;
    }
}
