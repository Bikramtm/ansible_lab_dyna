<?php
/**
 * Copyright (c) 2018. Dynacommerce B.V.
 */

require_once Mage::getModuleDir('controllers', 'Omnius_PriceRules') . DS . 'PromoController.php';

/**
 * Class Vznl_PriceRules_PromoController
 */
class Vznl_PriceRules_PromoController extends Omnius_PriceRules_PromoController
{
  /**
   * Apply or remove coupon code in shopping cart
   * @throws Exception
   */
  public function couponAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }

        $remove = $this->getRequest()->getParam('remove');
        $couponCode = $remove ? '' : $this->getRequest()->getParam('coupon'); // Empty coupon code param when removing
        $packageId = $this->getRequest()->getParam('packageId');
        $parentClass = $this->getRequest()->getParam('parentClass');
        $isCheckout = $this->getRequest()->getParam('isCheckout') == 'true' ? true : false;

        /** @var Omnius_PriceRules_Helper_External_Coupon_Validator $externalValidator */
        $externalValidator = Mage::helper('pricerules/external_coupon_validator');

        // Throw error when required parameters are not provided
        if (!$packageId || (!$couponCode && !$remove)) {
            return $this->getResponse()
                ->setHeader('Content-Type', 'application/json')
                ->setBody(Mage::helper('core')->jsonEncode(array(
                    'error' => true,
                    'message' => $this->__('Please provide the Coupon code and package ID')
                )));
        }

        $quote = Mage::getSingleton('checkout/cart')->getQuote();

        /** @var Omnius_Package_Model_Package $packageModel */
        $packageModel = Mage::getModel('package/package')->getCollection()
            ->addFieldToFilter('quote_id', $quote->getId())
            ->addFieldToFilter('package_id', $packageId)
            ->setPageSize(1, 1)
            ->getLastItem();

        Mage::log(sprintf('Setting Coupon Create %s for quote %s Id %s', $parentClass."_".$packageId."_".$couponCode."_".$remove, ';' , $quote->getId(), __FILE__), null, 'toPayAmount.log');

        if (Mage::getSingleton('customer/session')->getOrderEdit()) {
            $orderEditQuote = Mage::getModel('sales/quote')->load(Mage::getSingleton('customer/session')->getOrderEdit());

            /** @var Dyna_Package_Model_Package $packageModel */
            $oldPackage = Mage::getModel('package/package')->getCollection()
                ->addFieldToFilter('package_id', $packageId)
                ->addFieldToFilter('order_id', $orderEditQuote->getSuperOrderEditId())
                ->setPageSize(1, 1)
                ->getLastItem();

            $originalPackage = $oldPackage->getId();
        } else {
            $originalPackage = clone $packageModel;
        }

        if (!$remove) {
            $coupon = Mage::getModel('salesrule/coupon')->load($couponCode, 'code');
            if ((!$coupon->getId() && !$externalValidator->isValid($couponCode, $quote, $originalPackage)) || $coupon->getCode() != $couponCode) {
                $this->getResponse()
                    ->setHeader('Content-Type', 'application/json')
                    ->setBody(Mage::helper('core')->jsonEncode(array(
                        'error' => true,
                        'message' => $this->__('Invalid coupon code')
                    )));

                return;
            }
        }

        $quote->setCurrentStep(null);
        $customerSession = Mage::getSingleton('customer/session');

        $quote->getShippingAddress()
            ->setCollectShippingRates(true);

        $prevActivePackageId = $quote->getActivePackageId();
        $quote->setCouponsPerPackage(array($packageId => $couponCode));

        if ($externalValidator->isValid($couponCode, $quote, $originalPackage)) { //Coupon is used by other modules
            $packageModel->setCoupon($couponCode)->save();

            if (!Mage::getSingleton('customer/session')->getOrderEdit()) {
                Mage::dispatchEvent('pricerules_promo_coupon_update_external_usage', [
                    'quote' => $quote,
                    'package' => $packageModel
                ]);
            }
            $packageModel->save();
        }

        if ($remove) {
            Mage::dispatchEvent('pricerules_promo_removed_coupon', [
                'package' => $packageModel
            ]);
        }

        $quote->setActivePackageId($prevActivePackageId != $packageId ? $prevActivePackageId : $packageId)
            ->collectTotals()
            ->save();

        /** @var Omnius_Package_Model_Package $checkPackage */
        $checkPackage = Mage::getModel('package/package')->getCollection()
            ->addFieldToFilter('quote_id', $quote->getId())
            ->addFieldToFilter('package_id', $packageId)
            ->setPageSize(1, 1)
            ->getLastItem();

        $packageTotals = Mage::helper('omnius_configurator')->getActivePackageTotals($packageId, !($customerSession->getCustomer() && $customerSession->getCustomer()->getIsBusiness()),
            $quote->getId());

        $quoteTotals = array(
            'quoteGrandTotal' => array(
                'rawValue' => $quote->getGrandTotal(),
                'formatted' => Mage::helper('core')->currency($quote->getGrandTotal(), true, false)
            ),
            'packageTotal' => Mage::helper('core')->currency($packageTotals['totalprice'], true, false)
        );

        $quoteCouponCode = $checkPackage->getCoupon();//get the applied coupon code from the quote
        $rightBlockCollapsed = $this->getLayout()->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/partials/package_block.phtml');
        switch ($parentClass) {
            case '.package-expand':
            case '.coupon-class':
                /** @var Dyna_Configurator_Block_Rightsidebar */
                $rightBlock = $this->getLayout()->createBlock('dyna_configurator/rightsidebar')->setCheckout($isCheckout)->setTemplate('customer/expanded_cart.phtml');

                $bodyResponse = [
                    'error' => false,
                    'message' => $remove ? $this->__('Coupon removed') : $this->__('Coupon applied'),
                    'totals' => $this->getLayout()->createBlock('checkout/cart_totals')->setCheckoutButton($isCheckout)->setTemplate('checkout/cart/totals.phtml')->toHtml(),
                    'rightBlock' => $rightBlock->setCurrentPackageId($packageId)->toHtml(),
                    'rightBlockCollapsed' => $rightBlockCollapsed->setCurrentPackageId($packageId)->toHtml(),
                    'quoteTotals' => $quoteTotals
                ];
                break;
            case '.package-item-description-coupon':
                /** @var Dyna_Configurator_Block_Rightsidebar */
                $rightBlock = $this->getLayout()->createBlock('dyna_configurator/rightsidebar')->setCheckout($isCheckout)->setTemplate('customer/right_sidebar.phtml');

                $bodyResponse = [
                    'error' => false,
                    'message' => $remove ? $this->__('Coupon removed') : $this->__('Coupon applied'),
                    'totals' => $this->getLayout()->createBlock('checkout/cart_totals')->setCheckoutButton($isCheckout)->setTemplate('checkout/cart/totals_checkout.phtml')->toHtml(),
                    'rightBlock' => $rightBlock->setCurrentPackageId($packageId)->toHtml(),
                    'rightBlockCollapsed' => $rightBlockCollapsed->setCurrentPackageId($packageId)->toHtml(),
                    'quoteTotals' => $quoteTotals
                ];
                break;
            case '.side-cart':
                $rightBlock = $this->getLayout()->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/partials/package_block.phtml');
                $bodyResponse = [
                    'error' => false,
                    'message' => $remove ? $this->__('Coupon removed') : $this->__('Coupon applied'),
                    'totals' => $this->getLayout()->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/totals.phtml')->toHtml(),
                    'rightBlock' => $rightBlock->setCurrentPackageId($packageId)->toHtml(),
                    'rightBlockCollapsed' => $rightBlockCollapsed->setCurrentPackageId($packageId)->toHtml(),
                ];
                break;
            default:
                break;
        }

        if ($quoteCouponCode) {
            $this->getResponse()
                ->setHeader('Content-Type', 'application/json')
                ->setBody(Mage::helper('core')->jsonEncode($bodyResponse));
        } else {
            if (!$remove) {
                $this->getResponse()
                    ->setHeader('Content-Type', 'application/json')
                    ->setBody(Mage::helper('core')->jsonEncode(array(
                        'error' => true,
                        'message' => $this->__('Invalid coupon code')
                    )));
            } else {
                $this->getResponse()
                    ->setHeader('Content-Type', 'application/json')
                    ->setBody(Mage::helper('core')->jsonEncode($bodyResponse));
            }
        }
    }

    /**
     * Tries to apply a promo rule
     */
    public function addRuleAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }

        $ruleId = $this->getRequest()->getParam('promoRuleId');
        $packageId = $this->getRequest()->getParam('packageId');
        $exclusiveFamilyId = $this->getRequest()->getParam('exclusiveFamilyId');

        if ($packageId) {
            Mage::register('targetPackageId', $packageId);
            Mage::register('exclusivFamilyId', $exclusiveFamilyId);
            $returnData = Mage::helper('vznl_pricerules')->applyPromoRule($ruleId, true);

            $offerRuleIds = Mage::getModel('salesrule/rule')->getCollection()
                ->addFieldToFilter('service_source_expression', array('like' => '%cart.package.isOfferPackage()%'))
                ->getAllIds();

            //also check if we need to remove any rules
            if($ruleIdsToBeRemoved = Mage::registry('remove_applied_rule_ids')){
                $ruleIdsToBeRemoved = array_intersect($ruleIdsToBeRemoved, $offerRuleIds);
                $quoteItems = Mage::getSingleton('checkout/cart')->getQuote()->getAllItems();
                foreach($quoteItems as $quoteItem){
                    if($quoteItem->getPackageId() == $packageId){
                        $appliedRuleIds = explode(',',$quoteItem->getAppliedRuleIds());

                        //if item has an applied rule id that must be removed, remove it
                        if(array_intersect($appliedRuleIds, $ruleIdsToBeRemoved)){
                            $newRuleIds = array_diff($appliedRuleIds, $ruleIdsToBeRemoved);
                            $quoteItem->setAppliedRuleIds(implode(',', $newRuleIds));
                            $quoteItem->save();
                        }
                    }
                }
            }

            $this->jsonResponse($returnData);
            Mage::helper('vznl_checkout/cart')->validateCampaignOffer($packageId);
        } else {
            $this->jsonResponse(array(
                'error' => true,
                'message' => 'No existing package',
            ));
        }
    }
}
