<?php

/**
 * Class Vznl_Service_Helper_Price
 */
class Vznl_Service_Helper_Price extends Mage_Core_Helper_Data
{
    protected $priceCache = ['old' => [], 'new' => []];

    public function hasPriceChanged($sku, $packageId)
    {
        $priceCache = $this->priceCache;

        return isset($priceCache['old'][$packageId][$sku]) && isset($priceCache['new'][$packageId][$sku]) && $priceCache['new'][$packageId][$sku] != $priceCache['old'][$packageId][$sku];
    }

    public function addPrice($type, $sku, $packageId, $price)
    {
        $this->priceCache[$type][$packageId][$sku] = $price;
    }
}
