<?php

/**
 * Class Vznl_Service_Helper_Data
 */
class Vznl_Service_Helper_Data extends Omnius_Service_Helper_Data
{
    const GENERAL_CONFIG_PATH = 'vodafone_service/general/';
    const AXI_CONFIG_PATH = 'vodafone_service/axi/';
    const DEALER_ADAPTER_CONFIG_PATH = 'vodafone_service/dealer_adapter/';
    const CANCEL_DURING_CREDIT_CHECK_CONFIG_PATH = 'vodafone_service/cancel_during_credit_check/';
    const EBS_ORCHESTRATION_CONFIG_PATH = 'vodafone_service/ebs_orchestration/';
    const APPROVAL_CONFIG_PATH = 'vodafone_service/approval/';
    const PORTAL_CONFIG_PATH = 'vodafone_service/portal/';
    const INLIFE_CONFIG_PATH = 'vodafone_service/inlife/';
    const IBAN_CONFIG_PATH = 'vodafone_service/iban/';
    const KVK_CONFIG_PATH = 'vodafone_service/kvk/';
    const POSTCODE_CONFIG_PATH = 'vodafone_service/postcode/';
    const WORKCLIENT_CONFIG_PATH = 'vodafone_service/work_item/';
    const PRIORITIZE_CONFIG_PATH = 'vodafone_service/prioritize/';
    const CLEANUPCUSTOMER_CONFIG_PATH = 'vodafone_service/cleanup_customer/';
    const SYNC_CONFIG_PATH = 'vodafone_service/sync/';
    const LOCK_MANAGER_CONFIG_PATH = 'vodafone_service/lock_manager/';
    const DEALER_ADAPTER_IS_OVERRIDE = 'vodafone_service/general/override';
    const CUSTOMER_INFO_CONFIG_PATH = 'omnius_service/customer_info/';
    const XML_PATH_COOKIE_SECURE = 'web/cookie/cookie_secure';

    CONST LOCK_USER_ESB = 'ESB';

    /** @var Vznl_Service_Model_ClientFactory */
    protected $factory;

    /**
     * @var Mage_Core_Model_Store|null
     */
    protected $_store;

    /**
     * @return bool
     */
    public function isDev()
    {
        return Mage::helper('vznl_core/service')->isDev();
    }

    /**
     * Returns if use ILS stubs
     * @return bool
     */
    public function useIlsStubs()
    {
        return (bool) $this->getGeneralConfig('ils_use_stubs');
    }

    /**
     * Returns factory Vznl_Service_Model_ClientFactory
     * @return Vznl_Service_Model_ClientFactory
     */
    protected function _getFactory()
    {
        if (!$this->factory) {
            return $this->factory = new Vznl_Service_Model_ClientFactory();
        }

        return $this->factory;
    }

    /**
     * Return default options
     * @return array
     */
    protected function getDefaultOptions()
    {
        $coreServiceHelper = Mage::helper('vznl_core/service');
        $optionsArray = [];
        $optionsArray['axi'] = $coreServiceHelper->getZendSoapClientOptions('axi') ? : [];
        $optionsArray['dealer_adapter'] = $coreServiceHelper->getZendSoapClientOptions('dealer_adapter') ? : [];
        $optionsArray['cancel_during_credit_check'] = $coreServiceHelper->getZendSoapClientOptions('cancel_during_credit_check') ? : [];
        $optionsArray['ebs_orchestration'] = $coreServiceHelper->getZendSoapClientOptions('ebs_orchestration') ? : [];
        $optionsArray['approval'] = $coreServiceHelper->getZendSoapClientOptions('approval') ? : [];
        $optionsArray['portal_internal'] = $coreServiceHelper->getZendSoapClientOptions('portal') ? : [];
        $optionsArray['inlife'] = $coreServiceHelper->getZendSoapClientOptions('inlife') ? : [];
        $optionsArray['work_item'] = $coreServiceHelper->getZendSoapClientOptions('work_item') ? : [];
        $optionsArray['iban'] = [];
        $optionsArray['prioritize'] = $coreServiceHelper->getZendSoapClientOptions('prioritize') ? : [];
        $optionsArray['cleanup_customer'] = $coreServiceHelper->getZendSoapClientOptions('cleanup_customer') ? : [];
        $optionsArray['sync'] = $coreServiceHelper->getZendSoapClientOptions('sync') ? : [];
        $optionsArray['lock_manager'] = $coreServiceHelper->getZendSoapClientOptions('lock_manager') ? : [];
        $optionsArray['retrieve_customer_info'] = [];

        // Set default values that are the same for every service
        foreach (array_keys($optionsArray) as $key) {
            $optionsArray[$key]['soap_version'] = SOAP_1_2;
            $optionsArray[$key]['trace'] = true;
            $optionsArray[$key]['keep_alive'] = false;
            $optionsArray[$key]['connection_timeout'] = $this->getGeneralConfig('timeout');
        }

        // axi
        $options = $optionsArray['axi'];
        $options['wsdl'] = $this->getAxiConfig('wsdl');
        $options['endpoint'] = $this->getAxiConfig('usage_url');
        $options['login'] = $this->getAxiConfig('basic_auth_username');
        $options['password'] = $this->getAxiConfig('basic_auth_password');
        $optionsArray['axi'] = $options;

        // dealer_adapter
        $options = $optionsArray['dealer_adapter'];
        $options['wsdl'] = $this->getDealerAdapterConfig('wsdl');
        $options['endpoint'] = $this->getDealerAdapterConfig('usage_url');
        $options['login'] = $this->getDealerAdapterConfig('basic_auth_username');
        $options['password'] = $this->getDealerAdapterConfig('basic_auth_password');
        $optionsArray['dealer_adapter'] = $options;

        // cancel_during_credit_check
        $options = $optionsArray['cancel_during_credit_check'];
        $options['wsdl'] = $this->getCancelDuringCreditCheckConfig('wsdl');
        $options['endpoint'] = $this->getCancelDuringCreditCheckConfig('usage_url');
        $options['login'] = $this->getCancelDuringCreditCheckConfig('basic_auth_username');
        $options['password'] = $this->getCancelDuringCreditCheckConfig('basic_auth_password');
        $optionsArray['cancel_during_credit_check'] = $options;

        // ebs_orchestration
        $options = $optionsArray['ebs_orchestration'];
        $options['wsdl'] = $this->getEbsConfig('wsdl');
        $options['endpoint'] = $this->getEbsConfig('usage_url');
        $options['login'] = $this->getEbsConfig('basic_auth_username');
        $options['password'] = $this->getEbsConfig('basic_auth_password');
        $optionsArray['ebs_orchestration'] = $options;

        // approval
        $options = $optionsArray['approval'];
        $options['wsdl'] = $this->getApprovalConfig('wsdl');
        $options['endpoint'] = $this->getApprovalConfig('usage_url');
        $options['login'] = $this->getApprovalConfig('basic_auth_username');
        $options['password'] = $this->getApprovalConfig('basic_auth_password');
        $optionsArray['approval'] = $options;

        // portal_internal
        $options = $optionsArray['portal_internal'];
        $options['wsdl'] = $this->getPortalConfig('wsdl');
        $options['endpoint'] = $this->getPortalConfig('usage_url');
        $options['login'] = $this->getPortalConfig('basic_auth_username');
        $options['password'] = $this->getPortalConfig('basic_auth_password');
        $optionsArray['portal_internal'] = $options;

        // inlife
        $options = $optionsArray['inlife'];
        $options['wsdl'] = $this->getInlifeConfig('wsdl');
        $options['endpoint'] = $this->getInlifeConfig('usage_url');
        $options['bsl_username'] = $this->getInlifeConfig('bsl_username');
        $options['bsl_password'] = $this->getInlifeConfig('bsl_password');
        $options['login'] = $this->getInlifeConfig('basic_auth_username');
        $options['password'] = $this->getInlifeConfig('basic_auth_password');
        $optionsArray['inlife'] = $options;

        // work_item
        $options = $optionsArray['work_item'];
        $options['wsdl'] = $this->getWorkClientConfig('wsdl');
        $options['endpoint'] = $this->getWorkClientConfig('usage_url');
        $options['login'] = $this->getWorkClientConfig('basic_auth_username');
        $options['password'] = $this->getWorkClientConfig('basic_auth_password');
        $optionsArray['work_item'] = $options;

        // iban
        $options = $optionsArray['iban'];
        $options['wsdl'] = $this->getIbanConfig('wsdl');
        $options['endpoint'] = $this->getIbanConfig('usage_url');
        $optionsArray['iban'] = $options;

        // prioritize
        $options = $optionsArray['prioritize'];
        $options['wsdl'] = $this->getPrioritizeConfig('wsdl');
        $options['endpoint'] = $this->getPrioritizeConfig('usage_url');
        $options['login'] = $this->getPrioritizeConfig('basic_auth_username');
        $options['password'] = $this->getPrioritizeConfig('basic_auth_password');
        $optionsArray['prioritize'] = $options;

        // cleanup_customer
        $options = $optionsArray['cleanup_customer'];
        $options['wsdl'] = $this->getCleanupCustomerConfig('wsdl');
        $options['endpoint'] = $this->getCleanupCustomerConfig('usage_url');
        $options['login'] = $this->getCleanupCustomerConfig('basic_auth_username');
        $options['password'] = $this->getCleanupCustomerConfig('basic_auth_password');
        $optionsArray['cleanup_customer'] = $options;

        // sync
        $options = $optionsArray['sync'];
        $options['wsdl'] = $this->getSyncConfig('wsdl');
        $options['endpoint'] = $this->getSyncConfig('usage_url');
        $options['login'] = $this->getSyncConfig('basic_auth_username');
        $options['password'] = $this->getSyncConfig('basic_auth_password');
        $optionsArray['sync'] = $options;

        // lock_manager
        $options = $optionsArray['lock_manager'];
        $options['wsdl'] = $this->getLockManagerConfig('wsdl');
        $options['endpoint'] = $this->getLockManagerConfig('usage_url');
        $options['login'] = $this->getLockManagerConfig('basic_auth_username');
        $options['password'] = $this->getLockManagerConfig('basic_auth_password');
        $optionsArray['lock_manager'] = $options;

        // retrieve customer info
        $options = $optionsArray['retrieve_customer_info'];
        $options['wsdl'] = $this->getCustomerConfig('wsdl_customer_info');
        $options['endpoint'] = $this->getCustomerConfig('endpoint_customer_info');
        $options['soap_version'] = SOAP_1_1;
        $options['login'] = $this->getCustomerConfig('basic_auth_username');
        $options['password'] = $this->getCustomerConfig('basic_auth_password');
        $optionsArray['retrieve_customer_info'] = $options;

        return $optionsArray;
    }

    /**
     * @param string $config
     * @return string|null
     */
    public function getCustomerConfig($config)
    {
        return Mage::getStoreConfig(self::CUSTOMER_INFO_CONFIG_PATH . $config);
    }

    /**
     * @param string $config
     * @return string|null
     */
    public function getGeneralConfig($config)
    {
        return Mage::getStoreConfig(self::GENERAL_CONFIG_PATH . $config);
    }

    /**
     * @param string $config
     * @return string|null
     */
    public function getAxiConfig($config)
    {
        return Mage::getStoreConfig(self::AXI_CONFIG_PATH . $config);
    }
    /**
     * @param string $config
     * @return string|null
     */
    public function getLockManagerConfig($config)
    {
        return Mage::getStoreConfig(self::LOCK_MANAGER_CONFIG_PATH . $config);
    }

    /**
     * @param string $config
     * @return string|null
     */
    public function getDealerAdapterConfig($config)
    {
        return Mage::getStoreConfig(self::DEALER_ADAPTER_CONFIG_PATH . $config);
    }

    /**
     * @param string $config
     * @return string|null
     */
    public function getCancelDuringCreditCheckConfig($config)
    {
        return Mage::getStoreConfig(self::CANCEL_DURING_CREDIT_CHECK_CONFIG_PATH . $config);
    }

    /**
     * @param string $config
     * @return string|null
     */
    public function getEbsConfig($config)
    {
        return Mage::getStoreConfig(self::EBS_ORCHESTRATION_CONFIG_PATH . $config);
    }

    /**
     * @param string $config
     * @return string|null
     */
    public function getApprovalConfig($config)
    {
        return Mage::getStoreConfig(self::APPROVAL_CONFIG_PATH . $config);
    }

    /**
     * @param string $config
     * @return string|null
     */
    public function getPortalConfig($config)
    {
        return Mage::getStoreConfig(self::PORTAL_CONFIG_PATH . $config);
    }

    /**
     * @param string $config
     * @return string|null
     */
    public function getInlifeConfig($config)
    {
        return Mage::getStoreConfig(self::INLIFE_CONFIG_PATH . $config);
    }

    /**
     * @param string $config
     * @return string|null
     */
    public function getIbanConfig($config)
    {
        return Mage::getStoreConfig(self::IBAN_CONFIG_PATH . $config);
    }

    /**
     * @param string $config
     * @return string|null
     */
    public function getKvkConfig($config)
    {
        return Mage::getStoreConfig(self::KVK_CONFIG_PATH . $config);
    }

    /**
     * @param string $config
     * @return string|null
     */
    public function getPostcodeConfig($config)
    {
        return Mage::getStoreConfig(self::POSTCODE_CONFIG_PATH . $config);
    }

    /**
     * @param string $config
     * @return string|null
     */
    public function getWorkClientConfig($config)
    {
        return Mage::getStoreConfig(self::WORKCLIENT_CONFIG_PATH . $config);
    }

    /**
     * @param string $config
     * @return string|null
     */
    public function getPrioritizeConfig($config)
    {
        return Mage::getStoreConfig(self::PRIORITIZE_CONFIG_PATH . $config);
    }


    /**
     * @param string $config
     * @return string|null
     */
    public function getCleanupCustomerConfig($config)
    {
        return Mage::getStoreConfig(self::CLEANUPCUSTOMER_CONFIG_PATH . $config);
    }

    /**
     * @param string $config
     * @return string|null
     */
    public function getSyncConfig($config)
    {
        return Mage::getStoreConfig(self::SYNC_CONFIG_PATH . $config);
    }

    public function stripPlusChar($string)
    {
        return str_replace('+', '', $string);
    }

    public function checkSecureFrontend()
    {
        $store = $this->getStore();
        if (!$store->getConfig(self::XML_PATH_COOKIE_SECURE) || !$store->getConfig(Mage_Core_Model_Store::XML_PATH_SECURE_IN_FRONTEND)) {
            return false;
        }
        $baseLinkUrl = $store->getConfig(Mage_Core_Model_Store::XML_PATH_UNSECURE_BASE_LINK_URL);
        return (substr($baseLinkUrl, 0, 8) == 'https://');
    }

    /**
     * Retrieve Store object
     *
     * @return Mage_Core_Model_Store
     */
    public function getStore()
    {
        if (is_null($this->_store)) {
            $this->_store = Mage::app()->getStore();
        }
        return $this->_store;
    }

}
