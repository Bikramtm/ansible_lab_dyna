<?php

/**
 * Class Vznl_Service_Model_XmlMapper
 */
class Vznl_Service_Model_XmlMapper extends Omnius_Service_Model_XmlMapper
{
    /**
     * Map
     *
     * @param array $data - Data to be mapped
     * @param SimpleXMLElement &$xml - SimpleXMLElement object
     * @param bool $throw - Thrown error
     *
     * @return SimpleXMLElement
     */
    public static function map(array $data, SimpleXMLElement &$xml, $throw = false)
    {
        $callback = function ($paramName,
            $paramVal,
            &$el,
            $namespace = null
        ) use ($throw) {
            $failed = true;
            if (is_callable($paramVal)) {
                $el[0] = $paramVal($el, $namespace);
                $failed = false;
            } elseif (is_array($paramVal)) {
                $el[0] = self::map($paramVal, $el[0]);
                $failed = false;
            } elseif (is_scalar($paramVal)) {
                $failed = false;
                $el[0][0] = $paramVal;
            }
            if ($failed && $throw) {
                throw new InvalidArgumentException(
                    sprintf(
                        'Invalid value given for key "%s". Expected callable, array or scalar, %s given.', $paramName, self::getVariableType($paramVal)
                    )
                );
            }
        };
        foreach ($data as $paramName => $paramVal) {
            $namespaces = $xml->getNamespaces('true');
            if (!empty($namespaces)) {
                foreach ($namespaces as $prefix => $uri) {
                    /**
                     * When the namespace prefix is empty
                     * generate a prefix based on the uri
                     * so that xpath does not complain
                     */
                    if (!$prefix) {
                        $prefix = md5($uri);
                    }
                    $xml->registerXPathNamespace($prefix, $uri);

                    if (false !== strpos($paramName, '/')
                        && false === strpos($paramName, ':')
                    ) {
                        $xpath = sprintf("//%s:", $prefix) .
                            str_replace("/", sprintf("/%s:", $prefix), $paramName);
                        if (($el = @$xml->xpath($xpath))
                            || ($el = @$xml->xpath(sprintf("//%s", $paramName)))
                        ) {
                            $callback($paramName, $paramVal, $el, $uri);
                        }
                    } else {
                        if (($el = @$xml->xpath(
                            sprintf("//%s:%s", $prefix, $paramName)
                        )) || ($el = @$xml->xpath(
                            sprintf("//%s", $paramName)
                        )) || ($el = @$xml->getElementsByTagName($paramName))
                        ) {
                            $callback($paramName, $paramVal, $el, $uri);
                        }
                    }
                }
            } else {
                if ($el = @$xml->xpath(sprintf("//%s", $paramName))) {
                    $callback($paramName, $paramVal, $el);
                }
            }
        }
        return $xml;
    }

    /**
     * Returns the type of the var passed.
     *
     * @param mixed $var Variable
     * @return string Type of variable
     */
    static protected function getVariableType($var)
    {
        if (is_array($var)) return "array";
        if (is_bool($var)) return "boolean";
        if (is_float($var)) return "float";
        if (is_int($var)) return "integer";
        if (is_null($var)) return "NULL";
        if (is_numeric($var)) return "numeric";
        if (is_object($var)) return "object";
        if (is_resource($var)) return "resource";
        if (is_string($var)) return "string";
        return "unknown type";
    }

}
