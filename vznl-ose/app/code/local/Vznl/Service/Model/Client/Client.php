<?php

/**
 * Class Vznl_Service_Model_Client_Client
 */
abstract class Vznl_Service_Model_Client_Client extends SoapClient
{
    const SOAP_QUEUE_ID = 'default';
    const STATIC_USERNAME_BSL = 'U-BUY';
    const MODULE_NAME = 'Vznl_Service';
    const CONFIG_HEADER_PATH = 'omnius_service/service_settings_header/';

    /** @var Vznl_Service_Helper_Request */
    protected $_requestBuilder;

    /** @var string */
    protected $_namespace;

    /** @var Omnius_Service_Model_Normalizer */
    protected $_normalizer;

    /** @var array */
    protected $_options = array();

    /** @var resource  */
    protected $_context;

    /** @var null|string */
    protected $_wsdl = null;

    /** @var bool */
    protected $_loaded = false;

    /** @var string */
    protected $_soapDebug = '';

    /** @var bool */
    protected $_forceUseStubs = false;

    /** @var  string */
    protected $_lastResponse;

    /** @var bool */
    protected $logSoapException = false;

    /** @var bool */
    protected $prioritizeCall = false;

    /**
     * @param string|null $wsdl
     * @param array $options
     */
    public function __construct($wsdl = null, $options = array())
    {
        $this->_wsdl = $wsdl;
        $this->_context = stream_context_create();
        $this->_options = array_merge($options, array('stream_context' => $this->_context));
        $this->_soapDebug = 'services' . DS . 'soap_debug_%s.log';
    }

    /**
     * @param bool $bool
     */
    public function setUseStubs($bool = true)
    {
        $this->_forceUseStubs = $bool;
    }

    /**
     * @param bool $ils
     * @return bool
     */
    public function getUseStubs($ils = false)
    {
        return $this->_forceUseStubs || ($ils ? $this->getHelper()->useIlsStubs() : $this->getHelper()->useStubs());
    }

    /**
     * Lazy load WSDL so that when adding a call to
     * the job queue, no exception is thrown if the
     * service is down at that moment.
     */
    protected function _init()
    {
        if ( ! $this->_loaded) {
            /**
             * If in stub mode, don't try to connect to the service for the WSDL
             */
            $isInlifeCall = $this->needsILSHeader();
            if ((!$this->getUseStubs() && !$isInlifeCall) ||
                (!$this->getUseStubs(true) && $isInlifeCall)
            ) {
                if (!$this->_validateCredentials()) {
                    unset($this->_options['login']);
                    unset($this->_options['password']);
                }
                parent::__construct($this->_wsdl, $this->_options);
            }

            $this->_loaded = true;
        }
    }

    /**
     * @param string $name
     * @param string $arguments
     * @return array|mixed
     * @throws Zend_Soap_Client_Exception
     * @throws Exception
     */
    public function __call($name, $arguments)
    {
        $args = func_get_args();
        $isJob = isset($args[2]) ? $args[2] : false;
        $forceJob = isset($args[3]) ? $args[3] : false;

        try {
            /** @var Vznl_Job_Model_Queue $queue */
            $queue = Mage::helper('job')->getQueue(self::SOAP_QUEUE_ID);
            if (($queue->supportsCall($name, $this->getNamespace()) && !$isJob) || $forceJob) {
                return $this->call($name, $arguments);
            } else {
                return $this->proxyCall($name, $arguments, $isJob);
            }
        } catch (Exception $e) {
            if (!is_array($arguments)) {
                $arguments = array($arguments);
            }
            $this->debugCall($e, $name, $arguments);
            $e->logSoapException = $this->logSoapException;
            throw $e;
        }
    }

    /**
     * @param $name
     * @param $params
     * @return array|mixed
     * @throws Exception
     * @throws SoapFault
     * @throws Zend_Soap_Client_Exception
     */
    public function call($name, $params, $xml = null)
    {
        /** @var Vznl_Job_Model_Queue $queue */
        $queue = Mage::helper('vznl_job')->getQueue(self::SOAP_QUEUE_ID);
        $job = $this->buildJob($name, $params, $xml);
        /** @var Vznl_Job_Model_Job_Soap $job */
        $job->hasPriority = $this->prioritizeCall;
        $queue->push($job);
        return $job->getId();
    }

    /**
     * @param $name
     * @param $params
     * @return Vznl_Job_Model_Job_Abstract
     */
    protected function buildJob($name, $params, $xml = null)
    {
        /** @var Vznl_Job_Model_Job_Abstract $job */
        $job = Mage::getModel('vznl_job/job_soap');
        $id = sha1(uniqid(sprintf('soap_%s_%s_', $name, microtime(true))));
        $xml = $xml ? $xml : $this->buildRequest($name, $params[0]);
        $job->addData(array(
            'id' => $id,
            'namespace' => $this->getNamespace(),
            'call_name' => $name,
            'queue_id' => self::SOAP_QUEUE_ID,
            'request' => $xml,
        ));

        // Check if there is additional data to set to the job
        if($additionalInfo = Mage::registry('job_additional_information')) {
            $job->setData('additional_info', $additionalInfo);
            Mage::unregister('job_additional_information');
        }

        return $job;
    }

    /**
     * @param $searchParams
     */
    protected function setRequestHeaderInfo(&$searchParams)
    {
        $searchParams['HeaderInfo']['TransactionId'] = Dyna_Core_Helper_Data::getRequestUniqueTransactionId();
        $searchParams['HeaderInfo']['UserId'] = Mage::helper('agent')->getAgentId();
        $searchParams['HeaderInfo']['ApplicationId'] = $this->getApplicationId();
        $searchParams['HeaderInfo']['PartyName'] = $this->getPartyName();
        $searchParams['HeaderInfo']['CorrelationId'] = Mage::getSingleton("core/session")->getEncryptedSessionId();
    }

    /**
     * Get application id from the configurable input in Admin
     * @return bool
     */
    public function getApplicationId()
    {
        return $this->getServiceSettingsHeaderConfig('application_id');
    }

    /**
     * @param string $config
     * @return string|null
     */
    public function getServiceSettingsHeaderConfig($config)
    {
        return Mage::getStoreConfig(static::CONFIG_HEADER_PATH . $config);
    }

    /**
     * Get party_name from the configurable input in Admin
     * @return bool
     */
    public function getPartyName()
    {
        return $this->getServiceSettingsHeaderConfig('party_name');
    }

    /**
     * @param $name
     * @param $arguments
     * @param bool $isJob
     * @throws Exception
     * @throws SoapFault
     * @throws Zend_Soap_Client_Exception
     * @return array|mixed
     */
    protected function proxyCall($name, $arguments, $isJob = false)
    {
        // If the client needs the special ILS headers, build and assign them
        if ($isInlifeCall = $this->needsILSHeader()) {
            $this->addInlifeHeaders();
        }

        // If the client needs the special headers, build and assign them
        if ($this->needsWSAHeader()) {
            $this->addWSAHeader();
        }

        if (($this->getUseStubs() && !$isInlifeCall) ||
            ($this->getUseStubs(true) && $isInlifeCall)
        ) {
            $requestData = $isJob ? $arguments : $this->buildRequest($name, $arguments[0]);
            $stubClient = $this->getStubClient();
            $stubClient->setNamespace(static::MODULE_NAME);
            $responseData = $stubClient->call($name, ($isJob ? array($arguments) : $arguments));
            $this->logStubTransfer($requestData, $responseData);
            return $responseData;
        }

        $oldTimeout = ini_get('default_socket_timeout');
        $asyncTimeout = (int) $this->getHelper()->getGeneralConfig('async_timeout');
        $newTimeout = ($isJob && $asyncTimeout)
            ? $asyncTimeout
            : ($this->getHelper()->getGeneralConfig('timeout')
                ? (int) $this->getHelper()->getGeneralConfig('timeout')
                : 60);
        ini_set('default_socket_timeout', $newTimeout);

        //load WSDL
        $this->_init();

        $logDone = false;
        try {
            if ($isJob) {
                $callArgs = array(new SoapVar($arguments, XSD_ANYXML));
            } else {
                $callArgs = $this->_prepareArguments($name, $arguments[0]);
            }

            $r = parent::__call($name, $callArgs);

            $handleMethods = array(
                'updateProductSettings',
                'ReceiveResponseApprovePackage',
                'GetEligiblePricePlans',
                'RetrieveCustomerCreditProfile',
                'RetrieveCustomerInfo',
                'CancelOrder',
                'RetrieveOrderDetails',
                'SearchAvailableLogicalResources'
            );
            $handleMethodsIfNull = array(
                'CreateOrder',
                'SearchAvailableLogicalResources',
                'GetLockInfo'
            );

            if (in_array($name, $handleMethods) || in_array($name, $handleMethodsIfNull)) {
                if(!in_array($name, $handleMethodsIfNull) || $r === null) {

                    //handle ESB inconsistency: some methods have a response node that differs. E.g. CreateOrder call response node is ProcessOrderResponse
                    $name = str_replace('CreateOrder','ProcessOrder',$name);
                    $name = str_replace('ReceiveResponseApprovePackage','ApprovePackages',$name);

                    $reg = '/\<\?xml-multiple\s(.*)\s?\?>(\n?)/';
                    $content = $this->__getLastResponse();
                    $r = $this->getXmlError(preg_replace($reg, '', $content));
                    $r = $r->Body->{$name . 'Response'};
                }
            }

            if($name == 'RetrieveCustomerInfo') {
                $response = $this->getNormalizer()
                        ->objectToArray($r);
            } else {
                $response = $this->getNormalizer()
                    ->normalizeKeys(
                        $this->getNormalizer()
                            ->objectToArray($r)
                    );
            }
        } catch (Exception $e) {
            if (isset($isInlifeCall) && $isInlifeCall) {
                $this->logTransfer(); //log request and response
                $logDone = true;
                $specialCase = false;
                $reg = '/\<\?xml-multiple\s(.*)\s?\?>(\n?)/';
                $content = $this->_lastResponse ?: trim($e->getMessage());
                if (preg_match($reg, $content)) { //special case when response contains invalid nodes
                    try {
                        $response = $this->getNormalizer()
                            ->normalizeKeys(
                                $this->getNormalizer()
                                    ->objectToArray($this->getXmlError(preg_replace($reg, '', $content))));
                        $specialCase = true;
                    } catch (Exception $xmlEx) {
                        $this->log($xmlEx->getMessage() . "\n" . $xmlEx->getTraceAsString());
                        $xmlEx->logSoapException = $this->logSoapException;
                        throw $xmlEx;
                    }
                }
                if (!$specialCase) {
                    if (strlen(trim($content))) {
                        try {
                            $response = $this->getNormalizer()
                                ->normalizeKeys(
                                    $this->getNormalizer()
                                        ->objectToArray($this->getXmlError($content)));
                        } catch (Exception $xmlEx) {
                            $this->log($xmlEx->getMessage() . "\n" . $xmlEx->getTraceAsString());
                            $xmlEx->logSoapException = $this->logSoapException;
                            throw $xmlEx;
                        }
                    } else {
                        $this->throwSoapException('Empty response from service');
                    }
                }

                ini_set('default_socket_timeout', $oldTimeout);
            } else {
                ini_set('default_socket_timeout', $oldTimeout);
                $this->logTransfer(); //log request and response
                $e->logSoapException = $this->logSoapException;
                throw $e;
            }
        }

        ini_set('default_socket_timeout', $oldTimeout);
        if (!$logDone) {
            $this->logTransfer(); //log request and response if not already logged
        }

        // Execute extra logic based on caller class
        $this->_validateResponseCode($response);

        if (0 !== (int) $this->getAccessor()->getValue($response, 'status.response_code')) {
            if ($message = Mage::getModel('field/message')->getMessageForCode($response['status']['code'])) {
                $a = new Zend_Soap_Client_Exception($message->getResponseMessage(),
                    $response['status']['response_code']);
                $a->responseCode = $response['status']['code'];
                $a->logSoapException = $this->logSoapException;
                throw $a;
            } else {
                $this->throwSoapException($response['status']['description'] . ' - ' . $response['status']['code'],
                    $response['status']['response_code']);
            }
        } elseif ($this->getAccessor()->getValue($response, 'body.fault.code')) {
            $a = new Zend_Soap_Client_Exception(
                $this->getAccessor()->getValue($response, 'body.fault.detail.error.error_code') . ' - ' . $this->getAccessor()->getValue($response, 'body.fault.detail.error.error_message'),
                $this->getAccessor()->getValue($response, 'body.fault.detail.error.error_code')
            );
            $a->response = $response;
            $a->logSoapException = $this->logSoapException;
            throw $a;
        }

        if ($this->getHelper()->containsNode($this, 'ApprovePackagesResponse')) {
            if (!isset($response['process_result']['success']) || ($response['process_result']['success'] !== true && $response['process_result']['success'] !== 'true')) {
                $description = !empty($response['process_result']['description']) ? $response['process_result']['description'] : $response['process_result']['status'];
                $this->throwSoapException($description);
            }
        }

        /**
         * Special case for ProcessOrderResponse
         */
        if ($this->getHelper()->containsNode($this, 'ProcessOrderResponse')) {

            if (!isset($response['process_result']['success']) || ($response['process_result']['success'] !== true && $response['process_result']['success'] !== 'true')) {
                $description = !empty($response['process_result']['description']) ? $response['process_result']['description'] : $response['process_result']['status'];
                $this->throwSoapException($description, 0, true);
            }
            //check format to make sure that we have sales_ordernumber and packages in the response
            if (isset($response['process_result'])) {
                if (!isset($response['process_result']['status'])) {
                    if (!empty($response['process_result']['description'])) {
                        $this->throwSoapException(sprintf('Description: "%s"',
                            $response['process_result']['description']));
                    } else {
                        $this->throwSoapException('Invalid response format: no status found');
                    }
                }

                if (!in_array((string)$response['process_result']['status'], array(
                    'completed succesfully',
                    'completed with warnings',
                    'successfully completed',
                    'ProcessOrderCallSkipped'
                ))
                ) {
                    $this->throwSoapException(sprintf('Status: "%s" , Description: "%s"',
                        $response['process_result']['status'], $response['process_result']['description']));
                }

                if (!isset($response['order_result'])) {
                    $this->throwSoapException('Invalid response format: no order_result node found');
                } else {
                    if (!isset($response['order_result']['sales_ordernumber'])) {
                        $this->throwSoapException('Invalid response format: no sales_ordernumber node found');
                    }
                    if (!isset($response['order_result']['packages'])) {
                        $this->throwSoapException('Invalid response format: no packages node found');
                    }
                }
            } else {
                $this->throwSoapException('Invalid response format: no process_result node found');
            }
        }

        /**
         * Special case for ExecuteRequestWithLogin call
         */
        $responseValues = array_values($response);
        $responseValues = reset($responseValues);
        if (strlen($this->getAccessor()->getValue($responseValues, 'string.2'))) {
            $this->throwSoapException('ExecuteRequestWithLogin: ' . $responseValues['string'][2]);
        }

        return $response;
    }

    protected function throwSoapException($message = '', $code = 0, $setFail = false)
    {
        $e = new Zend_Soap_Client_Exception($message, $code);
        $e->logSoapException = $this->logSoapException;
        if ($setFail) {
            // Set when we the response returns success false
            $e->setFailStatus = true;
        }
        throw $e;
    }

    protected function logTransfer()
    {
        $requestData = sprintf(
            "Request Headers:\n%s\n\nRequest Body:\n%s\n\n",
            $this->__getLastRequestHeaders(),
            $this->format($this->__getLastRequest())
        );
        $responseData = sprintf(
            "Response Headers:\n%s\n\nResponse Body:\n%s\n\n",
            $this->__getLastResponseHeaders(),
            $this->format($this->__getLastResponse())
        );
        $this->_lastResponse = $this->__getLastResponse();
        $this->getLogger()->logTransfer(sprintf("%s\n%s", $requestData, $responseData), 'services' . DS . $this->getNamespace(), sha1(rand(0, 1000) . time()), 'log');
    }

    protected function logStubTransfer($requestData, $responseData)
    {
        $requestData = sprintf(
            "Request Headers:\n%s\n\nRequest Body:\n%s\n\n",
            '',
            var_export($requestData, true)
        );
        $responseData = sprintf(
            "Response Headers:\n%s\n\nResponse Body:\n%s\n\n",
            '',
            var_export($responseData, true)
        );
        $this->getLogger()->logTransfer(sprintf("%s\n%s", $requestData, $responseData), 'services' . DS . $this->getNamespace(), sha1(rand(0, 1000) . time()), 'log');
    }

    /**
     * @param $data
     * @return Varien_Object
     */
    public function normalize($data)
    {
        return $this->getNormalizer()->normalize($data);
    }

    /**
     * @param $method
     * @param array $params
     * @return mixed
     */
    public function buildRequest($method, array $params = array())
    {
        return $this->_getRequestBuilder()->create($this->getNamespace(), $method, array_merge($params, $this->_getDefaultParams()));
    }

    /**
     * @return string
     */
    public function getNamespace()
    {
        if ( ! $this->_namespace) {
            preg_match('/(.*)_(.*)Client$/', get_called_class(), $parts);
            if (isset($parts[2])) {
                $this->_namespace = $parts[2];
            } else {
                Mage::throwException(sprintf('Client class (%s) does not follow naming convention', get_called_class()));
            }
        }
        return $this->_namespace;
    }

    /**
     * Should return the system wide defaults (applicationId, channelName, etc)
     * @return array
     */
    protected function _getDefaultParams()
    {
        return array();
    }

    /**
     * @param $name
     * @param $arguments
     * @return array
     */
    protected function _prepareArguments($name, $arguments)
    {
        return array(new SoapVar($this->buildRequest($name, $arguments), XSD_ANYXML));
    }

    /**
     * @return Vznl_Service_Helper_Request
     */
    protected function _getRequestBuilder()
    {
        if ( ! $this->_requestBuilder) {
            $this->_requestBuilder = Mage::helper('vznl_service/request');
        }
        return $this->_requestBuilder;
    }

    /**
     * @return Vznl_Service_Model_Normalizer
     */
    protected function getNormalizer()
    {
        if ( ! $this->_normalizer) {
            $this->_normalizer = Mage::getSingleton('omnius_service/normalizer');
        }
        return $this->_normalizer;
    }

    /**
     * @return Vznl_Service_Model_DotAccessor
     */
    protected function getAccessor()
    {
        return Mage::getSingleton('omnius_service/dotAccessor');
    }

    /**
     * @return Vznl_Service_Helper_Data
     */
    protected function getHelper()
    {
        return Mage::helper('vznl_service');
    }

    /**
     * @return Vznl_Service_Model_Logger
     */
    protected function getLogger()
    {
        return Mage::getSingleton('omnius_service/logger');
    }

    /**
     * @return Vznl_Service_Model_Client_StubClient
     */
    public function getStubClient()
    {
        return Mage::getSingleton('omnius_service/client_stubClient');
    }

    /**
     * @param $message
     * @param int $level
     */
    public function log($message, $level = Zend_Log::ERR)
    {
        Mage::log($message, $level, sprintf($this->_soapDebug, strtolower($this->getNamespace())), true);
    }

    /**
     * @param Exception $e
     * @param $name
     * @param array $arguments
     */
    protected function debugCall(Exception $e, $name, array $arguments)
    {
        $soapRequestHeaders = parent::__getLastRequestHeaders();
        $soapRequest = parent::__getLastRequest();
        if ($soapRequest) {
            $soapRequest = new Omnius_Core_Model_SimpleDOM($soapRequest);
            $soapRequest = $soapRequest->asPrettyXML();
        }
        $soapResponseHeaders = parent::__getLastResponseHeaders();
        $soapResponse = parent::__getLastResponse();
        if ($soapResponse) {
            $reg = '/\<\?xml-multiple\s(.*)\s?\?>(\n?)/';
            if (preg_match($reg, $soapResponse)) {
                $soapResponse = preg_replace($reg, '', $soapResponse);
            }
            $soapResponse = new Omnius_Core_Model_SimpleDOM($soapResponse);
            $soapResponse = $soapResponse->asPrettyXML();
        }
        if ($e) {
            $eMessage = $e->getMessage();
            $eTrace = $e->getTraceAsString();
            $dump = "\n";
            $dump .= sprintf("Exception (%s): %s\n", get_class($e), $eMessage);
            $dump .= sprintf("Trace: %s\n\n", $eTrace);
        }

        //Avoid warning: json_encode(): type is unsupported, encoded as null
        $options = $this->_options;
        unset($options['stream_context']);

        // Debug for error: `Warning: json_encode(): type is unsupported, encoded as null
        if (! is_array($this->_options) || json_encode($options) === false) {
            Mage::log('Empty Service/Model/Client/Client.php options: ' . var_export($options, true), null, 'debug_service_client.log', true);
            Mage::log('Trace: ' . mageDebugBacktrace(true, false, true), null, 'debug_service_client.log', true);
        }

        $dump .= sprintf("Client:\n%s\n\n", $this->jsonPretty(json_encode($options)));
        $dump .= sprintf("Method: %s\n\n", $name);
        $dump .= sprintf("Arguments:\n%s\n\n", $this->jsonPretty(json_encode($arguments)));
        $dump .= sprintf("Request headers:\n%s", $soapRequestHeaders);
        $dump .= sprintf("Request:\n%s\n\n", $this->format($soapRequest));
        $dump .= sprintf("Response headers:\n%s\n\n", $soapResponseHeaders);
        $dump .= sprintf("Response:\n%s", $soapResponse);
        $this->log($dump, Zend_Log::CRIT);
    }

    /**
     * Format the given xml file
     *
     * @param $mixed
     * @return string
     */
    protected function format($mixed)
    {
        try {
            $dom = new DOMDocument();
            $dom->preserveWhiteSpace = false;
            $dom->formatOutput = false;
            if (!empty($mixed)) {
                $dom->loadXML($mixed);
            }
            $dom = $this->hidePrivacySensitiveData($dom);
            return preg_replace("/\n/", "", $dom->saveXML());
        }
        catch(Exception $e){
            Mage::logException($e);
            return $mixed;
        }
    }

    /**
     * Hide privacy sensitive data, replacing it with '***'.
     * @param DOMDocument $dom The dom to manipulate.
     * @return DOMDocument the manipulated dom.
     */
    protected function hidePrivacySensitiveData(DOMDocument $dom)
    {
        $xpath = new DOMXPath($dom);

        $expressions = $this->getXpathExpressions();
        foreach($expressions as $expression) {
            if(!empty($expression)) {
                /** @var DOMNodeList $nodes */
                $nodes = $xpath->query($expression);
                /** @var DOMNode $node */
                foreach ($nodes as $node) {
                    $node->nodeValue = '****';
                }
            }
        }
        return $dom;
    }

    /**
     * Get the xpath expressions from the privacy sensitive settings
     * @return array
     */
    protected function getXpathExpressions()
    {
        $configData = Mage::getStoreConfig('storage_settings/privacy/xpath');
        $expressions = explode(PHP_EOL, $configData);
        return $expressions;
    }

    /**
     * Pretty print json
     *
     * @param $json
     * @param string $istr
     * @return string
     */
    protected function jsonPretty($json, $istr='  ')
    {
        $result = '';
        for ($p = $q = $i = 0; isset($json[$p]); $p++) {
            $json[$p] == '"' && ($p > 0 ? $json[$p - 1] : '') != '\\' && $q = !$q;
            if (!$q && strchr(" \t\n", $json[$p])) {
                continue;
            }
            if (strchr('}]', $json[$p]) && !$q && $i--) {
                strchr('{[', $json[$p - 1]) || $result .= "\n" . str_repeat($istr, $i);
            }
            $result .= $json[$p];
            if (strchr(',{[', $json[$p]) && !$q) {
                $i += strchr('{[', $json[$p]) === FALSE ? 0 : 1;
                strchr('}]', $json[$p + 1]) || $result .= "\n" . str_repeat($istr, $i);
            }
        }
        return $result;
    }

    /**
     * @param $str
     * @return SimpleXMLElement
     */
    protected function getXmlError($str)
    {
        try {
            $xml = new SimpleXMLElement($str);
            foreach ($xml->getNamespaces(true) as $ns => $uri) {
                $str = str_replace(sprintf('%s:', $ns), '', $str);
            }
            $xml = new SimpleXMLElement($str);

            return json_decode(json_encode($xml));
        } catch (Exception $e) {
            $e->logSoapException = $this->logSoapException;
            throw $e;
        }
    }

    /**
     * @param $key
     * @param $value
     * @return $this
     */
    public function setOption($key, $value)
    {
        $this->_options[$key] = $value;

        return $this;
    }

    /**
     * @param $key
     * @return $this
     */
    public function removeOption($key)
    {
        if (isset($this->_options[$key])) {
            unset($this->_options[$key]);
        }

        return $this;
    }

    /**
     * @param string $request
     * @param string $location
     * @param string $action
     * @param int $version
     * @param int $one_way
     * @return string
     */
    public function __doRequest($request, $location, $action, $version, $one_way = 0)
    {

        // Set the HTTP headers.
        stream_context_set_option($this->_context, array('http' => array('header' => 'X-START-DATE: ' . date('D, d M Y H:i:s e'))));
        return (parent::__doRequest($request, $location, $action, $version, $one_way));
    }

    /**
     * Check if the client should send the special headers
     *
     * @return bool
     */
    protected function needsWSAHeader()
    {
        return false;
    }

    /**
     * Do nothing unless the client implements this function
     *
     * @return $this
     */
    protected function addWSAHeader()
    {
        return $this;
    }

    /**
     * Check if the client should send the special ILS headers
     *
     * @return bool
     */
    protected function needsILSHeader()
    {
        return false;
    }

    /**
     * Do nothing unless the client implements this function
     *
     * @return $this
     */
    protected function addInlifeHeaders()
    {
        return $this;
    }

    /**
     * Extra logic needed in order to identify a certain type of response
     *
     * @param $response
     * @return $this
     */
    protected function _validateResponseCode($response)
    {
        return $this;
    }

    /**
     * Validate if basic auth credentials are set
     * @return bool
     */
    protected function _validateCredentials()
    {
        return $this->_options['login'] && $this->_options['password'];
    }
}
