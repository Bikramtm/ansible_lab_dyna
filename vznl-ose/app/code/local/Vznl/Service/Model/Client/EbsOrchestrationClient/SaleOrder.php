<?php

class Vznl_Service_Model_Client_EbsOrchestrationClient_SaleOrder extends Mage_Core_Model_Abstract
{
    private $_packages = array(); // Array with packages
    private $_esbActionRequired = ''; // ChangeOrderAfterDelivery, ChangeOrderBeforeDelivery, CreateSaleOrder
    private $_axiState = ''; // Reserve, Nothing, Change
    private $_renumbered = false;
    private $_superorderId = null;
    private $_parentId = null;
    private $_websiteId = null;
    private $_esbAction = '';

    /**
     * @return null|int
     */
    public function getWebsiteId()
    {
        return $this->_websiteId;
    }

    /**
     * @return string
     */
    public function getEsbActionRequired()
    {
        return $this->_esbActionRequired;
    }

    /**
     * @return string
     */
    public function getAxiState()
    {
        return $this->_axiState;
    }

    /**
     * @return bool
     */
    public function getRenumbered()
    {
        return $this->_renumbered;
    }

    /**
     * @return Vznl_Service_Model_Client_EbsOrchestrationClient_Package[]
     */
    public function getPackages()
    {
        return $this->_packages;
    }

    public function getSuperorderId()
    {
        return $this->_superorderId;
    }

    public function getParentId()
    {
        return $this->_parentId;
    }

    public function getEsbAction()
    {
        return $this->_esbAction;
    }

    /**
     * @param Vznl_Superorder_Model_Superorder $superOrder
     * @param Vznl_Checkout_Model_Sales_Order[]|null $changedOrders
     * @param int[]|null $esbChangedPackageIds
     * @param Vznl_Superorder_Model_Superorder|null $esbOldSuperOrder
     * @param null $cancelledPackages
     * @param Vznl_Checkout_Model_Sales_Order[]|null $esbOrdersWithAddressChanged
     * @param bool $enrichOrder Whether or not it is meant to be an enrich order request
     * @return void
     */
    public function parse(
        $superOrder,
        $changedOrders,
        $esbChangedPackageIds,
        $esbOldSuperOrder,
        $cancelledPackages,
        $esbOrdersWithAddressChanged,
        $enrichOrder
    ) {
        $changedCollection = null;
        $toBeDeliveryorders = $superOrder->getOrders(true);
        $allToBeDeliveryorders = $oldPackagesIds = $prevPackagesIds = $newPackagesIds = array();

        foreach ($toBeDeliveryorders as $toBeDeliveryorder) {
            $toBeDeliveryorder->setIsCurrent(true);
            $allToBeDeliveryorders[$toBeDeliveryorder->getId()] = $toBeDeliveryorder;
        }

        $toBeDeliveryorders = $allToBeDeliveryorders;

        $this->_superorderId = $superOrder->getId();
        $this->_parentId = $superOrder->getParentId();
        $this->_websiteId = $superOrder->getCreatedWebsiteId();

        if ($esbOldSuperOrder) {
            $superOrderPackages = $superOrder->getPackages();
            foreach ($superOrderPackages as $superOrderPackage) {
                if ($superOrderPackage->getOldPackageId()) {
                    $oldPackagesIds[$superOrderPackage->getPackageId()] = $superOrderPackage->getOldPackageId();
                    $newPackagesIds[$superOrderPackage->getOldPackageId()] = $superOrderPackage->getPackageId();
                }
            }
        }

        if ($cancelledPackages) {
            $allToBeDeliveryorders = array();
            foreach ($toBeDeliveryorders as $tbOrder) {
                $orderParentId = $tbOrder->getParentId();
                $initialPackages = array();
                if (!empty($orderParentId) && $tbOrder->getEdited() == 0) {
                    $parentOrder = Mage::getModel('sales/order')->load($orderParentId);
                    $initialPackages = $parentOrder->getPackageIds();
                    $allToBeDeliveryorders[$parentOrder->getId()] = $parentOrder;
                }
                $tmpCurrentPackages = $tbOrder->getPackageIds();
                $currentPackages = array();
                foreach ($tmpCurrentPackages as $currentPackageId) {
                    $currentPackages[] = isset($oldPackagesIds[$currentPackageId]) ? $oldPackagesIds[$currentPackageId] : $currentPackageId;
                }
                $persistedPackages = array_intersect($initialPackages, $currentPackages);
                $allToBeDeliveryorders[$tbOrder->getId()] = $tbOrder;
            }
            $toBeDeliveryorders += $allToBeDeliveryorders;
        }

        if ($esbOldSuperOrder) {
            foreach ($esbOldSuperOrder->getOrders(true) as $changedOrder) {
                foreach ($changedOrder->getAllItems() as $orderItem) {
                    $changedCollection[$orderItem->getPackageId()][] = $orderItem;
                }
            }

            if ($cancelledPackages) {
                $allToBeDeliveryorders = array();
                $oldDeliveryorders = $esbOldSuperOrder->getOrders(true);
                $flippedCancelledPackages = array_flip($cancelledPackages);

                $superOrderPackages = $esbOldSuperOrder->getPackages();
                foreach ($superOrderPackages as $superOrderPackage) {
                    if ($superOrderPackage->getOldPackageId()) {
                        $prevPackagesIds[$superOrderPackage->getPackageId()] = $superOrderPackage->getOldPackageId();
                    }
                }

                foreach ($oldDeliveryorders as $tbOrder) {
                    $foundDeletedPackage = false;
                    $orderParentId = $tbOrder->getParentId();
                    $initialPackages = array();
                    if (!empty($orderParentId) && $tbOrder->getEdited() == 0) {
                        $parentOrder = Mage::getModel('sales/order')->load($orderParentId);
                        $initialPackages = $parentOrder->getPackageIds();
                        foreach ($initialPackages as $packId) {
                            if (isset($flippedCancelledPackages[$packId])) {
                                $foundDeletedPackage = true;
                                $allToBeDeliveryorders[$parentOrder->getId()] = $parentOrder;
                            }
                        }
                    }
                    if ($foundDeletedPackage) {
                        $currentPackages = array();
                        $tmpCurrentPackages = $tbOrder->getPackageIds();
                        foreach ($tmpCurrentPackages as $currentPackageId) {
                            $currentPackages[] = isset($prevPackagesIds[$currentPackageId]) ? $prevPackagesIds[$currentPackageId] : $currentPackageId;
                        }
                        $persistedPackages = array_intersect($initialPackages, $currentPackages);
                    }

                }
                $toBeDeliveryorders += $allToBeDeliveryorders;
            }
        } else {
            if (count($changedOrders) == 2 && count($changedOrders[1]) > 0) {
                $esbEditedOrders = $changedOrders[0];
                $esbNewOrders = $changedOrders[1];
                $flippedEsbChangedPackageIds = array_flip($esbChangedPackageIds);
                foreach ($esbEditedOrders as $changedOrderId => $changedOrder) {
                    foreach ($changedOrder->getAllItems() as $orderItem) {
                        $orderItemPackageId = $orderItem->getPackageId();
                        if (isset($flippedEsbChangedPackageIds[$orderItemPackageId])) {
                            $changedCollection[$orderItem->getPackageId()][] = $orderItem;
                        }
                    }
                }

                foreach ($esbNewOrders as $oldOrderId => $newOrders) {
                    foreach ($newOrders as $newOrder) {
                        $toBeDeliveryorders[$newOrder->getId()] = $newOrder;
                    }
                }
            }
        }

        $renumberedPackages = 0;
        $cancelledPackagesCount = count($cancelledPackages);
        $hasEsbChangedPackages = $esbChangedPackageIds && count($esbChangedPackageIds) > 0;
        foreach ($toBeDeliveryorders as $order) {
            $packageCollection = array();
            foreach ($order->getAllItems() as $orderItem) {
                if (!$cancelledPackages && (isset($persistedPackages) && $order->getEdited() == 1 && in_array($orderItem->getPackageId(), $persistedPackages) !== false)) {
                    continue; // Skip order items that were persisted to the new order
                } elseif($cancelledPackages && $order->getEdited() == 0 && isset($toBeDeliveryorders[$order->getParentId()])) {
                    continue; // Skip order items in case of cancel that was changed before cacnel
                }
                $packageCollection[$orderItem->getPackageId()][] = $orderItem;
            }
            foreach ($packageCollection as $packageId => $packageItems) {
                // skip previous cancelled packages
                if (in_array($order->getPackages($packageId)->getStatus(), Vznl_Package_Model_Package::getCancelledTypePackages())) {
                    continue;
                }
                if (isset($oldPackagesIds[$packageId]) && $order->getIsCurrent()) {
                    $initialPackageId = $oldPackagesIds[$packageId];
                    $renumberedPackages = $packageId;
                } else {
                    $initialPackageId = $packageId;
                }

                if (isset($newPackagesIds[$packageId]) && !$order->getIsCurrent()) {
                    $renumberedPackages = $newPackagesIds[$packageId];
                }

                $package = new Vznl_Service_Model_Client_EbsOrchestrationClient_Package();
                $oldPackageItems = null;
                if ($changedCollection && array_key_exists($initialPackageId, $changedCollection)) {
                    $oldPackageItems = $changedCollection[$initialPackageId];
                }

                if ($changedCollection && array_key_exists($initialPackageId, $changedCollection)) {
                    $action = 'change';
                } elseif ($cancelledPackages && in_array($initialPackageId, $cancelledPackages)) {
                    $action = 'cancel';
                } elseif ($esbOrdersWithAddressChanged && array_key_exists($order->getParentId(), $esbOrdersWithAddressChanged)) {
                    $action = 'addresschange';
                } else {
                    // Checks if has any kind of change
                    $hasChange = $cancelledPackagesCount > 0 || ($changedCollection && !array_key_exists($initialPackageId, $changedCollection)) || ($esbOrdersWithAddressChanged && !array_key_exists($order->getParentId(), $esbOrdersWithAddressChanged));// TODO: Will this always work? When hybrid is possible?
                    // if has any kind of change or is partial set state to nothing, else reserve.
                    if ($hasChange || in_array($superOrder->getOrderStatus(), [Vznl_Superorder_Model_Superorder::SO_VALIDATED_PARTIALLY])) {
                        $action = 'nothing';
                    } else {
                        $action = 'reserve';
                    }
                }

                //$deliveryAddressChanged = $esbOrdersWithAddressChanged && array_key_exists($order->getParentId(), $esbOrdersWithAddressChanged);

                if ($action === 'cancel') {
                    $this->_axiState = 'Nothing';
                }

                $this->_axiState = (
                $esbOldSuperOrder
                    ? 'Reserve'
                    :
                    (
                        ($hasEsbChangedPackages)
                            ? 'Nothing'
                            :
                            (
                            $action === 'cancel'
                            || ($action === 'addresschange' && $superOrder->getOnlyPaymentChanged())
                                ? 'Nothing'
                                : 'Reserve'
                            )
                    )
                );

                $package->parse($packageId, $packageItems, $oldPackageItems, $order, $action, $this->_axiState, $superOrder);

                if (($action === 'change' || ($action === 'cancel' && $esbChangedPackageIds)) && $package->getChangeType() === 'after') {
                    $package->setPackageId($renumberedPackages);
                    $this->_renumbered = true;
                }

                $this->_esbActionRequired = $this->getEsbActionRequiredForPackage($package, $action, $enrichOrder);
                $esbActions[$packageId] = $this->_esbActionRequired;
                $this->_packages[] = $package;
            }
        }

        if(in_array(Vznl_Superorder_Model_Superorder::SO_CHANGE_BEFORE, $esbActions)) {
            $this->_esbAction = Vznl_Superorder_Model_Superorder::SO_CHANGE_BEFORE;
        } else if (in_array(Vznl_Superorder_Model_Superorder::SO_CHANGE_AFTER, $esbActions)) {
            $this->_esbAction = Vznl_Superorder_Model_Superorder::SO_CHANGE_AFTER;
        }
        else {
            $this->_esbAction = $this->_esbActionRequired;
        }

        //Reset the statuses if previous status was Partial
        if ($superOrder->getOrderStatus() == Vznl_Superorder_Model_Superorder::SO_VALIDATED_PARTIALLY) {
            /** @var Vznl_Checkout_Model_Sales_Order $deliveryOrder */
            $superOrder->setOrderStatus(Vznl_Superorder_Model_Superorder::SO_WAITING_FOR_VALIDATION);
            $superOrder->save();
            foreach ($superOrder->getDeliveryOrders() as $deliveryOrder) {
                /** @var Vznl_Package_Model_Package $package */
                foreach ($deliveryOrder->getPackages() as $package) {
                    if ($package->getCreditcheckStatus() == Vznl_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_CC_PARTIAL) {
                        $package->setCreditcheckStatus(Vznl_Package_Model_Package::ESB_PACKAGE_NETWORK_STATUS_CC_INITIAL);
                        $package->setVfStatusDesc(null);
                        $package->save();
                    }
                }
            }
        }

        $allCancel = true;
        foreach ($this->_packages as $package) {
            if ($package->getAction() !== 'cancel' && $package->getAction() !== 'nothing') {
                $allCancel = false;
            }
        }
        if ($allCancel) {
            $this->_axiState = 'Nothing';
        }
    }

    /**
     * Gets the esb action required for the given package and method
     *
     * @param Vznl_Service_Model_Client_EbsOrchestrationClient_Package $package The package
     * @param string $action The action of the package
     * @param boolean $enrichOrder Whether or not it was ment to be an enrich order request.
     * @return string The esb action
     */
    public function getEsbActionRequiredForPackage($package, $action, $enrichOrder = false)
    {
        $esbActionRequired = Vznl_Superorder_Model_Superorder::SO_CREATE;
        // If something is partially approved, there are no changes but it should still be handled as ChangeBeforeDelivery
        if(!empty($package->getPackageModel()->getSuperOrder()) && in_array($package->getPackageModel()->getSuperOrder()->getOrderStatus(), [Vznl_Superorder_Model_Superorder::SO_VALIDATED_PARTIALLY])){
            $esbActionRequired = Vznl_Superorder_Model_Superorder::SO_CHANGE_BEFORE;
        }

        // Change action if not only reserve packages
        if ($action === 'change' || $action === 'cancel' || $action === 'addresschange') {
            if ($package->getChangeType() === 'before') {
                $esbActionRequired = Vznl_Superorder_Model_Superorder::SO_CHANGE_BEFORE;
            } elseif ($package->getChangeType() === 'after') {
                $esbActionRequired = Vznl_Superorder_Model_Superorder::SO_CHANGE_AFTER;
            }
        } elseif ($enrichOrder) {
            $esbActionRequired = Vznl_Superorder_Model_Superorder::SO_ENRICH;
        }
        return $esbActionRequired;
    }
}
