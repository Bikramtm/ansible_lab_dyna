<?php


/**
 * Class Vznl_Service_Model_Client_AxiClient
 */
class Vznl_Service_Model_Client_AxiClient extends Vznl_Service_Model_Client_Client
{
    const ENV_VHOST_CODE = 'MAGE_ENV_CODE';

    /**
     * @param string|integer|array $productId
     * @param integer $storeId
     * @param int $maxDistOtherStores
     * @param bool $incWarehouseStock
     * @return mixed
     */
    public function getStock($productId, $storeId, $maxDistOtherStores = 0, $incWarehouseStock = false)
    {
        $incWarehouseStock = $incWarehouseStock ? 'true' : 'false'; // Convert boolean to string
        $products = is_scalar($productId) ? array($productId) : $productId;
        $source = $this->getSource();

        $response = $this->GetProductStock(
            array(
                'Products/ProductID' => function ($nodes, $ns) use ($products) {
                    /** @var Omnius_Core_Model_SimpleDOM $baseAddress */
                    $baseProduct = array_shift($nodes);
                    foreach ($nodes as $line) {
                        unset($line[0]);
                    }

                    foreach ($products as $prodId) {
                        $productNode = new Omnius_Core_Model_SimpleDOM('<root></root>');
                        $product = $productNode->addChild('ProductID', $prodId, $ns);
                        $baseProduct->insertAfterSelf($product);
                    }

                    unset($baseProduct[0]);
                },
                'Source' => $source,
                'StoreID' => $storeId,
                'MaxDistanceOtherStores' => $maxDistOtherStores,
                'IncludeWarehouseStock' => $incWarehouseStock,
            )
        );

        if ($stockInfo = $this->getAccessor()->getValue($response, 'stock.stock_info')) {
            $result = array();
            if (count($products) > 1) {
                foreach ($stockInfo as $productInfo) {
                    $productId = $productInfo['product_i_d'];
                    $result[$productId] = $productInfo;
                }
            } else {
                return $stockInfo;
            }

            if (Mage::helper('vznl_service')->useStubs()) {
                foreach ($products as $productSku) {
                    if (!isset($result[$productSku])) {
                        $result[$productSku] = $productInfo;
                    }
                }
            }

            return $result;
        } else {
            return $response;
        }
    }

    /**
     * @return mixed
     */
    protected function getSource()
    {
        $source = $_SERVER;
    	return isset($source[self::ENV_VHOST_CODE])
            ? $source[self::ENV_VHOST_CODE]
            : Mage::helper('vznl_service')->getGeneralConfig('default_source_value');
    }
}
