<?php
/**
 * Class Vznl_Service_Model_Client_EbsOrchestrationClient
 */
class Vznl_Service_Model_Client_EbsOrchestrationClient extends Vznl_Service_Model_Client_Client
{
    const DUMMY_PRODUCT_SKU = '4600700'; // Dummy service item product with price 0.00
    const ADYEN_PAYMENT_METHOD = 'ADYEN';
    const REKUBUY_PAYMENT_METHOD = 'REKUBUY';
    const FEE_AIKIDO_PAYMENT_METHOD = 'FEE';
    const ORIGINALREFERENCE_REFERENCE_TYPE = 'ORIGINALREFERENCE';
    const PACKAGELINE_REFERENCE_TYPE = 'PACKAGELN';
    const ENV_VHOST_CODE = 'MAGE_ENV_CODE';

    private $_hasAddressChange = false;
    private $omniusServiceHelper = null;
    private $superOrder = null;
    protected $logSoapException = true;
    protected $_payments = [];
    protected $_deviceLineItems = [];
    protected $_email = false;
    private $firstOrder;
    protected $_pakketLineItems = [];
    private $mobileFee = '';

    /**
     * @param Vznl_Superorder_Model_Superorder $superOrder
     * @param Vznl_Superorder_Model_Superorder|null $esbOldSuperOrder
     * @param Vznl_Checkout_Model_Sales_Order[]|null $changedOrders
     * @param int[]|null $esbChangedPackageIds
     * @param Vznl_Checkout_Model_Sales_Order[]|null $esbOrdersWithAddressChanged
     * @param null $cancelledPackages
     * @param float|null $refundAmount
     * @param string|null $refundMethod
     * @param string|null $refundReason
     * @param array|null $toBePaidAmount
     * @param bool $returnOnlyXml
     * @param bool $enrichOrder
     * @return mixed
     * @throws Mage_Core_Exception
     */
    public function processCustomerOrder(
        $superOrder,
        $esbOldSuperOrder = null,
        $changedOrders = null,
        $esbChangedPackageIds = null,
        $esbOrdersWithAddressChanged = null,
        $cancelledPackages = null,
        $refundAmount = null,
        $refundMethod = null,
        $refundReason = null,
        $toBePaidAmount = array(),
        $returnOnlyXml = false,
        $enrichOrder = false,
        $customer = null
    ) {
        if (!count($orders = $superOrder->getOrders(true))) {
            Mage::throwException('Super order does contain any final orders.');
        }

        $this->_clearLocalValues();

        // Check if GUID exist, else create one
        if (!$superOrder->getOrderGuid()) {
            $superOrder->setOrderGuid(Mage::helper('vznl_core')->generateRandomGuid());
            $superOrder->save();
        }
        $this->mobileFee = Mage::getStoreConfig('vodafone_service/on_bill_pay/mobile_fee');
        $this->superOrder = $superOrder;
        $superOrder->setTimesProcessed($superOrder->getTimesProcessed() + 1);

        /** @var Mage_Sales_Model_Order $order */
        $order = $orders->setPageSize(1, 1)->setCurPage(1)->getLastItem();
        $this->firstOrder = $order;
        /** @var Vznl_Customer_Model_Customer_Customer $customer */
        if(empty($customer)){
            $customer = Mage::getModel('customer/customer')->load($superOrder->getCustomerId());
        }

        $salesOrder = new Vznl_Service_Model_Client_EbsOrchestrationClient_SaleOrder();
        $salesOrder->parse($superOrder, $changedOrders, $esbChangedPackageIds, $esbOldSuperOrder, $cancelledPackages, $esbOrdersWithAddressChanged, $enrichOrder);

        if ((in_array($superOrder->getOrderStatus(), [Vznl_Superorder_Model_Superorder::SO_INITIAL, Vznl_Superorder_Model_Superorder::SO_WAITING_VALIDATION_ACCEPTED_AXI])) &&
            (!$esbOldSuperOrder && !$changedOrders && !$esbChangedPackageIds && !$esbOrdersWithAddressChanged && !$cancelledPackages)) {
            Mage::log(sprintf('Double create type PO for already processed order: (id:%s)', $superOrder->getId()), Zend_Log::WARN);
            Mage::throwException('Double create type PO for already processed order.');
        }

        // We cannot return null as it break the cancelling of multi-package orders
        // if(($superOrder->getActionType() === Dyna_Superorder_Model_Superorder::SO_TYPE_CHANGE) &&
        //   ($salesOrder->getEsbActionRequired() === Omnius_Superorder_Model_Superorder::SO_CREATE)) {
        //    return null;
        // }

        //$this->_getChangeType($superOrder, $customer, $changedOrders, $esbChangedPackageIds, $esbOldSuperOrder, $cancelledPackages);
        $this->_hasAddressChange = ($esbOrdersWithAddressChanged && count($esbOrdersWithAddressChanged) > 0);
        $this->_hasCancelledPackages = ($cancelledPackages !== null);
        $this->_hasChangedDeliveryID = (!$changedOrders && !Mage::registry('order_has_other_packages')) ? false : true;

        $params = array();
        $params = array_merge($params, $this->getGeneralNodes($superOrder, $order, $salesOrder, $customer, $esbOldSuperOrder, $refundMethod, $refundAmount));
        $params = array_merge($params, $this->getGeneralSalesNodes($superOrder));
        $params = array_merge($params, $this->getCustomerDataNodes($customer, $order));
        $params = array_merge($params, $this->getSellerSupplierNodes($order));
        $params = array_merge($params, $this->getPackageNodes($salesOrder, $customer, $refundReason, $esbOldSuperOrder));
        $params = array_merge($params, $this->getDeliveryNodes($superOrder, $esbOrdersWithAddressChanged, $cancelledPackages, $salesOrder, $toBePaidAmount));
        $params = $params + $this->getApprovePackages(
                $superOrder,
                $esbOldSuperOrder,
                $changedOrders,
                $esbChangedPackageIds,
                $esbOrdersWithAddressChanged,
                $cancelledPackages,
                $order->getAgentId()
            );

        $params = array_merge($params, $this->getPaymentNodes($superOrder, $cancelledPackages, $refundAmount, $refundMethod, $refundReason, $salesOrder, $esbOldSuperOrder));    
        $params = $this->retractDevicercFromDeliveryAmount($params);
   
        //when editing a delivered order, only create the xml as the call itself will be called when the hardware is returned
        if ($returnOnlyXml) {
            return $this->buildRequest('CreateOrder', $params);
        }

        $additionalData = array(
            'run_superorder_history' => true,
            'superorder_id' => $superOrder->getId(),
        );
        // Only run history from job when a new superorder is created
        if (!$esbOldSuperOrder && !$changedOrders && !$esbChangedPackageIds && !$esbOrdersWithAddressChanged && !$cancelledPackages) {
            $additionalData['new_order'] = true;
            $additionalData['initial_state'] = true;
        } else {
            if (!$esbOldSuperOrder && ($changedOrders || $esbChangedPackageIds || $esbOrdersWithAddressChanged || $cancelledPackages)) {
                // Change before delivery
                $additionalData['run_check_lock'] = true;
                $additionalData['do_hand_over'] = true;
                $additionalData['hand_over_from'] = Mage::helper('agent')->getAgentId();
                $additionalData['hand_over_to'] = 'ESB';
                $additionalData['superorder_number'] = $superOrder->getOrderNumber();
            } elseif ($esbOldSuperOrder) {
                // Change after delivery, handover on old superorder
                $additionalData['do_hand_over'] = true;
                $additionalData['hand_over_from'] = Mage::helper('agent')->getAgentId();
                $additionalData['hand_over_to'] = 'ESB';
                $additionalData['superorder_number'] = $esbOldSuperOrder->getOrderNumber();
                $additionalData['initial_state'] = true;
            }
            $additionalData['run_release_lock'] = true;
            $additionalData['superorder_parent_id'] = $esbOldSuperOrder ? $esbOldSuperOrder->getId() : null;
            $additionalData['agent_id'] = Mage::helper('agent')->getAgentId();
        }
        Mage::register(
            'job_additional_information',
            $additionalData
        );

        /** @var Vznl_Checkout_Helper_Data $helper */
        $this->prioritizeCall = $cancelledPackages && count($cancelledPackages);
        $result = $this->CreateOrder($params);
        return $result;
    }

    /**
     * @param Omnius_Core_Model_SimpleDOM $packageLine
     * @param int $packageLineCount
     * @param Vznl_Service_Model_Client_EbsOrchestrationClient_PackageItem $salesOrderPackageLineItem
     * @param Vznl_Service_Model_Client_EbsOrchestrationClient_Package $salesOrderPackage
     * @param string $ns
     * @param string $historyItemState
     */
    private function _addLineItemDetailNodes($packageLine, $packageLineCount, $salesOrderPackageLineItem, $salesOrderPackage, $ns, $historyItemState = null)
    {
        if ($salesOrderPackageLineItem->isDeviceSubscription()) {
            $packageLineState = current($packageLine->firstChild('State'));
            if (
                (
                    isset($this->_deviceLineItems[$salesOrderPackage->getPackageId()][$packageLineState])
                    && $this->_deviceLineItems[$salesOrderPackage->getPackageId()][$packageLineState]['LineId'] < $packageLineCount
                )
                || !isset($this->_deviceLineItems[$salesOrderPackage->getPackageId()][$packageLineState])
            ) {
                $this->_payments[$salesOrderPackage->getPackageId()][$packageLineState] = [
                    'State' => $packageLineState,
                    'Reference' => $salesOrderPackageLineItem,
                    'LineId' => $packageLineCount,
                    'Package' => $salesOrderPackage
                ];
            }
        }

        if ($salesOrderPackageLineItem->isDevice()) {
            $packageLineState = current($packageLine->firstChild('State'));
            $deviceLineItemKey = $historyItemState ?? $packageLineState;
            if (
                (
                    isset($this->_deviceLineItems[$salesOrderPackage->getPackageId()][$deviceLineItemKey])
                    && $this->_deviceLineItems[$salesOrderPackage->getPackageId()][$deviceLineItemKey]['LineId'] < $packageLineCount
                )
                || !isset($this->_deviceLineItems[$salesOrderPackage->getPackageId()][$deviceLineItemKey])
            ) {
                $this->_deviceLineItems[$salesOrderPackage->getPackageId()][$deviceLineItemKey] = [
                    'LineId' => $packageLineCount,
                    'Reference' => $salesOrderPackageLineItem,
                    'State' => $packageLineState,
                    'Package' => $salesOrderPackage
                ];
            }
        }

        if ($salesOrderPackageLineItem->isPakketDetails()) {
            $packageLineState = current($packageLine->firstChild('State'));
            if (
                (
                    isset($this->_pakketLineItems[$salesOrderPackage->getPackageId()][$packageLineState])
                    && $this->_pakketLineItems[$salesOrderPackage->getPackageId()][$packageLineState]['LineId'] < $packageLineCount
                )
                || !isset($this->_pakketLineItems[$salesOrderPackage->getPackageId()][$packageLineState])
            ) {
                $this->_pakketLineItems[$salesOrderPackage->getPackageId()][$packageLineState] = [
                    'LineId' => $packageLineCount,
                    'Reference' => $salesOrderPackageLineItem,
                    'State' => $packageLineState,
                    'Package' => $salesOrderPackage
                ];
            }
        }

        $packageLine->addChild('OrderLineID', $packageLineCount, $ns);
        $packageLine->addChild('ProductID', $salesOrderPackageLineItem->getSku(), $ns);
        $packageLine->addChild('ServiceID', null, $ns); // Not used

        $lineItemTotals = $salesOrderPackageLineItem->getTotals();

        // Remove TKH towards backend systems, as this is added as a new line item
        $oneTimeChargeInc = $lineItemTotals['total'];
        $oneTimeChargeEx = $lineItemTotals['subtotal_price'];
        if ($lineItemTotals['total'] > 0) { // If there is a one time charge, substract TKH as we send is at seperate product
            $oneTimeChargeInc = round($lineItemTotals['total'] - $salesOrderPackageLineItem->getTkhInc(), 2);
            $oneTimeChargeEx = round($lineItemTotals['subtotal_price'] - $salesOrderPackageLineItem->getTkhEx(), 2);
        }

        $oneTimeChargeVat = $oneTimeChargeInc - $oneTimeChargeEx;
        if ($salesOrderPackageLineItem->getMixmatchSubtotal() !== null && $salesOrderPackageLineItem->isAikido()) {
            $oneTimeChargeEx = round($salesOrderPackageLineItem->getMixmatchSubtotal(), 2);
            $oneTimeChargeVat = round($salesOrderPackageLineItem->getMixmatchTax(), 2);
            $oneTimeChargeInc = round($salesOrderPackageLineItem->getMixmatchSubtotal() + $salesOrderPackageLineItem->getMixmatchTax(), 2);
        }


        $packageLine->addChild('SalesPrice', $oneTimeChargeInc, $ns);
        $packageLine->addChild('Quantity', 1, $ns);

        $packageLine->addChild('BasketItemID', $salesOrderPackageLineItem->getBasketItemId(), $ns);
        $packageLine->addChild('OfferID', $salesOrderPackageLineItem->getOfferId(), $ns);
        $packageLine->addChild('Action', $salesOrderPackageLineItem->getAction(), $ns);
        $packageLine->addChild('BomID', $salesOrderPackageLineItem->getBomId(), $ns);
        $packageLine->addChild('ProductName', $salesOrderPackageLineItem->getProductName(), $ns);
        $packageLine->addChild('OfferType', $salesOrderPackageLineItem->getOfferType(), $ns);
        $packageLine->addChild('HardwareName', $salesOrderPackageLineItem->getHardwareName(), $ns);
        $packageLine->addChild('SerialNumber', $salesOrderPackageLineItem->getSerialNumber(), $ns);

        $natureCode = Mage::helper('agent')->getDaDealerCode() == Vznl_Agent_Model_Dealer::MOBILE_MOVE_DEALERCODE ? 'migration' : $salesOrderPackageLineItem->getNatureCode();
        $packageLine->addChild('NatureCode', $natureCode, $ns);
        if ($targetSku = $salesOrderPackageLineItem->getTargetSku()) {
            $packageLine->addChild('TargetId', $targetSku, $ns);
        }

        $packageLine->addChild('OneTimePriceExcludingVAT', $oneTimeChargeEx, $ns);
        $packageLine->addChild('OneTimePriceIncludingVAT', $oneTimeChargeInc, $ns);
        $packageLine->addChild('OneTimePriceVAT', $oneTimeChargeVat, $ns);
        $packageLine->addChild('SubscriptionPriceExcludingVAT', round($lineItemTotals['subtotal_maf'], 2), $ns);
        $packageLine->addChild('SubscriptionPriceIncludingVAT', round($lineItemTotals['total_maf'], 2), $ns);
        $packageLine->addChild('SubscriptionPriceVAT', $lineItemTotals['tax_maf'], $ns);
    }

    protected function getPackageNodes($salesOrder, $customer, $refundReason, $esbOldSuperOrder) {
        $self = $this;

        return array('ns0:Packages/ns0:Package' =>
            function($nodes, $ns) use ($self, $salesOrder, $customer, $refundReason, $esbOldSuperOrder) {
            $basePackage = array_shift($nodes);
            foreach ($nodes as $packageNode) {
                unset($packageNode[0]);
            }

            $namespaces = $basePackage[0]->getNamespaces(true);
            $ns = isset($namespaces['dt']) ? $namespaces['dt'] : $ns;
            $processedPackages = $newPackagesIds = array();

            $afterDeliveryEdit = false;

            /** @var $historyHelper Omnius_Superorder_Helper_History */
            $historyHelper = Mage::helper('superorder/history');

            if ($esbOldSuperOrder) {
                $superOrderPackages = $esbOldSuperOrder->getPackages();
                foreach ($superOrderPackages as $superOrderPackage) {
                    $newPackagesIds[$superOrderPackage->getPackageId()] = $superOrderPackage->getOldPackageId();
                }
            }

            $tempOrderLine = '';
            $allPackages = $salesOrder->getPackages();

            //get smallest package id that is of type OneOfDeal
            $firstOneOff = array();
            $allPackagesOrdered = array();
            foreach ($allPackages as $pack) {
                $allPackagesOrdered[$pack->getPackageModel()->getPackageId()] = $pack;
                if ($pack->getPackageModel()->getOneOfDeal()) {
                    $firstOneOff[] = $pack->getPackageModel()->getPackageId();
                }
            }
            $firstOneOffId = $firstOneOff ? min($firstOneOff) : 0;
            ksort($allPackagesOrdered);
            $groupCounter = 0;

            /** @var Vznl_Service_Model_Client_EbsOrchestrationClient_Package $salesOrderPackage */

            $flippedActions = array_flip(['change', 'cancel']);
            foreach(array_reverse($allPackagesOrdered) as $salesOrderPackage) {
                $action = strtolower($salesOrderPackage->getAction());
                $replaceMe = false;
                // Skip already processed packages. Useful in cancel scenario
                $processedPackageId = ($salesOrderPackage->getReturnPackageId()) ?: $salesOrderPackage->getPackageId();
                if (isset($processedPackages[$processedPackageId])) {
                    continue;
                }
                $processedPackages[$processedPackageId] = 1;
                $subscription =  $salesOrderPackage->getVfSubscription();
                $deliveryOrder = $salesOrderPackage->getDeliveryOrder();
                $isFixed = $deliveryOrder->hasFixed();

                $packageRoot = new Omnius_Core_Model_SimpleDOM('<root></root>');
                $package = $packageRoot->addChild('Package', null, 'http://DynaLean/ProcessOrder/V1-0');

                $package->addChild('State', $salesOrderPackage->getAxiState(), $ns);
                $package->addChild('PackageID', $salesOrderPackage->getPackageId(), $ns);
                $package->addChild('TransactionType', $salesOrderPackage->getTransactionType(), $ns);
                if ($deliveryOrder->getFixedWrittenConsent() == 1 && $deliveryOrder->getPealOutbondFlag() == 1) {
                    $package->addChild('OutboundCallWrittenConsent', 1, $ns);
                } else {
                    $package->addChild('OutboundCallWrittenConsent', 0, $ns);
                }
                $package->addChild('CampaignID', $deliveryOrder->getCampaignId(), $ns);

                $package->addChild('BasketID', $deliveryOrder->getBasketId(), $ns);

                if ($isFixed) {
                    if ($deliveryOrder->getConsolidate()) {
                        $package->addChild('Consolidate', 'CONSOLIDATE_YES', $ns);
                    } elseif (is_null($deliveryOrder->getConsolidate())) {
                        $package->addChild('Consolidate', 'CONSOLIDATE_NONE', $ns);
                    } else {
                        $package->addChild('Consolidate', 'CONSOLIDATE_NO', $ns);
                    }
                } else {
                    $package->addChild('Consolidate', null, $ns);
                }


                $package->addChild('ReturnPackageID', $salesOrderPackage->getReturnPackageId(), $ns);

                $websiteCode = Mage::app()->getWebsite($salesOrder->getWebsiteId())->getCode();
                $virtualStoreDelivery = ($deliveryOrder->isVirtualDelivery() &&
                    ($websiteCode == Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE ||
                        $websiteCode == Vznl_Agent_Model_Website::WEBSITE_BELCOMPANY_CODE
                    )
                );

                $returnWarehouse = (
                    ($action == 'cancel' || $action == 'change') &&
                    $salesOrderPackage->getChangeType() === 'after' &&
                    ($deliveryOrder->isHomeDelivery() ||$deliveryOrder->isVirtualDelivery()) &&
                    !$virtualStoreDelivery
                );

                // Special conditions for Indirect channel
                if ($websiteCode == Vznl_Agent_Model_Website::WEBSITE_INDIRECT_CODE && $salesOrderPackage->getChangeType() === 'after') {
                    $returnWarehouse = isset($flippedActions[$action]);
                }
                $package->addChild('ReturnedToWareHouse', $returnWarehouse ? 'true' : 'false', $ns);

                // RFC-150907 - When a change after delivery, send the selected return channel
                $returnChannel = $salesOrderPackage->getPackageModel()->getReturnChannel();
                if (
                    ($action === 'cancel' || $action === 'change')
                    && $salesOrderPackage->getChangeType() === 'after'
                    && $returnChannel
                ) {
                    $package->addChild('ReturnChannel', $returnChannel, $ns);
                }

                $package->addChild('PackageType', $salesOrderPackage->getPackageType(), $ns);

                if ($salesOrderPackage->getPackageType() == "INT_TV_FIXTEL") {
                    if ($deliveryOrder->getFixedDeliveryMethod() == "SHOP") {
                        $package->addChild('DeliveryType', $salesOrderPackage->getDeliveryType(), $ns);
                    } else {
                        $package->addChild('DeliveryType', "home delivery", $ns);
                    }
                } else {
                    $package->addChild('DeliveryType', $salesOrderPackage->getDeliveryType(), $ns);
                }
                if (Mage::app()->getStore()->getCode() == Vznl_Agent_Model_Website::WEBSITE_INDIRECT_CODE) {
                    $package->addChild('DeliveryStore', Mage::helper('agent')->getWharehouseCode(), $ns);
                } else {
                    $package->addChild('DeliveryStore', $salesOrderPackage->getDeliveryStoreId(), $ns);
                }

                if ($isFixed) {
                    $package->addChild('DeliveryDate', date('Y-m-d', strtotime($deliveryOrder->getFixedDeliveryWishDate())), $ns);
                    $deliveryMethod = $deliveryOrder->getFixedDeliveryMethod();
                    if ($deliveryMethod == "TECHNICIAN DELIVERY ASAP") {
                        $deliveryMethod = "TECHNICIAN DELIVERY";
                    }
                    $package->addChild('FixedDeliveryMethod', $deliveryMethod, $ns);
                    $package->addChild('FixedDeliveryType', $deliveryOrder->getFixedDeliveryType(), $ns);
                } else {
                    $package->addChild('DeliveryDate', date('Y-m-d', strtotime('+2days')), $ns);
                }
                $package->addChild('PackageDescription', $salesOrderPackage->getPackageDescription(), $ns);
                // RFC 140468
                if ($subscription) {
                    $package->addChild('CommitmentPeriod', $subscription->getProduct()->getSubscriptionDuration(), $ns);
                }
                // RFC 140468
                $package->addChild('VirtualDelivery', $deliveryOrder->isVirtualDelivery() ? 'true' : 'false', $ns);

                if ($deliveryOrder && $deliveryOrder->getId()) {
                    $package->addChild('OriginalDeliveryOrderID', $deliveryOrder->getIncrementId(), $ns);
                }
                $package->addChild('ActivateExceptionalCase', ($salesOrderPackage->getActivateExceptionalCase()) ? 'true' : 'false', $ns);
                $package->addChild('ActivateExceptionalCaseComment', $salesOrderPackage->getComments(), $ns);
                $package->addChild('HasInsurance', $salesOrderPackage->hasInsurance() ? 'true' : 'false', $ns);

                // Only send the manual activation tag on order creation and if it's present
                if (($salesOrder->getEsbActionRequired() === 'CreateSaleOrder') && $salesOrderPackage->getPackageModel()->isManualActivation()) {
                    $manualConnect = $package->addChild('ManualConnect', null, $ns);
                    $manualConnect->addChild('ManualConnect', 'true', $ns);
                    $manualConnect->addChild('ReasonCode', $salesOrderPackage->getPackageModel()->getManualActivationReason(), $ns);
                    $manualConnect->addChild('Note', $salesOrderPackage->getPackageModel()->getManualActivationUserInput(), $ns);
                    $manualConnect->addChild('SimcardNumber', $salesOrderPackage->getPackageModel()->getSimNumber(), $ns);
                }

                $ctnCode = null;
                if ($ctnVal = $salesOrderPackage->getPackageModel()->getTelephoneNumber(true)) {
                    $ctnModel = Mage::getModel('ctn/ctn')->loadCtn($ctnVal, $customer->getId());
                    if ($ctnModel->getId()) {
                        $ctnCode = $ctnModel->getCode();
                    }
                }
                $package->addChild('AssignedProductId', trim($ctnCode), $ns);
                $package->addChild('DeliveryDealerCode', trim($salesOrderPackage->getDeliveryDealerCode()), $ns);
                $package->addChild('ReturnDealerCode', Mage::helper('agent')->getDaDealerCode(), $ns);
                $packageLines = $package->addChild('PackageLines', null, $ns);
                $packageLineCount = 1;

                // Loop trough old line items in case of changes in order
                // TODO make this recursive in history to preserve line positions??
                $salesOrderOldPackageLineItems = $salesOrderPackage->getOldPackageItems();
                $oldSalesPackage = $salesOrderPackage->getOldPackage();

                if ($salesOrderPackage->getChangeType() === 'after' && $salesOrder->getAxiState() != 'Nothing') {
                    $afterDeliveryEdit = true;
                }

                $superOrderId = $salesOrder->getSuperorderId();
                $historyPackageId = $salesOrderPackage->getPackageId();

                if ($salesOrder->getParentId() && $salesOrderPackage->getChangeType() === 'after' && $salesOrderPackage->getAxiState() != 'Return') {

                    $superOrderId = $salesOrder->getParentId();
                    if ($oldSalesPackage) {
                        $historyPackageId = $oldSalesPackage->getPackageId();
                    }
                }

                // Get history items for this package
                $historyItems = $historyHelper->getHistoryItems($superOrderId, $historyPackageId);

                // Helper function for adding a XML node
                $addNode = function($node, $value, $nodeLine) use ($ns, &$addNode) {
                    $node = ucfirst(Mage::helper('vznl_core')->snakeToCamel($node));
                    if (is_array($value) && !empty($value)) {
                        $tmp = $nodeLine->addChild($node, null, $ns);
                        foreach ($value as $n => $v) {
                            $addNode($n, $v, $tmp);
                        }
                    } else {
                        if (is_array($value) && empty($value)) {
                            $value = null;
                        }
                        $nodeLine->addChild($node, $value, $ns);
                    }
                };

                $salesOrderPackageLineItems = $salesOrderPackage->getPackageItems();
                $dummyRows = array();
                $returnedItems = 0;
                $editedItems = 0;
                $salesOrderPackageSkus = $salesOrderPackage->getSkusAssoc(); // Get current skus and their count
                if ($salesOrderPackage) {
                    $flippedSalesOrderSkus = array_flip($salesOrderPackage->getSkus());
                }
                if ($oldSalesPackage) {
                    $flippedOldSalesPackageSkus = array_flip($oldSalesPackage->getSkus());
                }

                foreach ($historyItems as $key => $historyItem) {
                    if (($salesOrderPackage->getChangeType() !== 'after') && ($salesOrder->getEsbActionRequired() !== Vznl_Superorder_Model_Superorder::SO_CHANGE_AFTER)) {
                        // TODO: set old package items as removed based on line_id ?!
                        $packageLineCount = $historyItem['line_id'];
                    }

                    // If an item has been removed in the past, we send it in the current edit session with status 'Nothing'
                    if ($historyItem['removed'] == 1) {
                        if ((($salesOrderPackage->getChangeType() === 'after') || ($salesOrder->getEsbActionRequired() == Vznl_Superorder_Model_Superorder::SO_CHANGE_AFTER))
                            &&  $salesOrderPackage->getAxiState() != 'Return') {
                            continue; // Skip removed items in edit after delivery
                        }

                        $packageLine = $packageLines->addChild('PackageLine', null, $ns);
                        $dataArray = unserialize($historyItem['data']);
                        if (is_array($dataArray)) {
                            foreach ($dataArray as $node => $value) {

                                if ($node == 'state') {
                                    $value = ($salesOrderPackage->getAxiState() == 'Cancel' || $salesOrderPackage->getAxiState() == 'Return') ? $salesOrderPackage->getAxiState() : 'Nothing';
                                }
                                if ($node == 'order_line_i_d') {
                                    $value = $packageLineCount;
                                }
                                if ($node == 'return_reason') {
                                    if ($salesOrderPackage->getAxiState() == 'Return') {
                                        // Set a return reason, we are returning this package
                                        $value = $salesOrderPackage->getReturnRefundReason();
                                    }
                                }

                                $addNode($node, $value, $packageLine);
                                unset($historyItems[$key]);
                            }
                        }

                        if ($dataArray['state'] == 'Return' && $salesOrder->getEsbActionRequired() == Vznl_Superorder_Model_Superorder::SO_CHANGE_BEFORE) {
                            foreach ($salesOrderPackageLineItems as $i => $salesOrderPackageLineItem) {
                                if ($historyItem['product_id'] == $salesOrderPackageLineItem->getSku()) {
                                    $self->_addLineItemDetailNodes($packageLine, $packageLineCount, $salesOrderPackageLineItem, $salesOrderPackage, $ns, $dataArray['state']);
                                }
                            }
                        }
                        $packageLineCount++;
                    } else {
                        $foundInOldItems = false;
                        // If the item from the history is active (removed=0) on the current package, we process it normally
                        // First we search for this item in old package items if they exist
                        if ($this->getArrayCount($salesOrderOldPackageLineItems) && !empty($salesOrderOldPackageLineItems)) {
                            foreach ($salesOrderOldPackageLineItems as $itemKey => $oldLineItem) {
                                if ($historyItem['package_id'] != $oldLineItem->getPackageId()) {
                                    continue; // skip item if in different package
                                }
                                if (($oldLineItem->getSku() != $historyItem['product_id']) || $foundInOldItems) {
                                    continue;
                                }
                                $foundInOldItems = true;
                                $OldLineItemSku = $oldLineItem->getSku();
                                $packageLine = $packageLines->addChild('PackageLine', null, $ns);
                                $returnReason = '';
                                switch ($salesOrderPackage->getAction()) {
                                    case 'change':
                                        if ($salesOrderPackage->getChangeType() === 'before') {
                                            if (!isset($flippedSalesOrderSkus[$OldLineItemSku])) {
                                                    // Change before delivery, this item was changed so we return it
                                                    $packageLine->addChild('State', Vznl_Service_Model_Client_EbsOrchestrationClient_PackageItem::PACKAGE_LINE_STATE_CANCEL, $ns);
                                            } else {
                                                if ($this->hasPriceChanged($oldLineItem->getSku(), $oldLineItem->getPackageId())) {
                                                    $oldLineItem->setKeepOldPayment(true);
                                                    $oldLineItem->getPackageLineItem()->setKeepOldPayment(true);
                                                    // Change before delivery, there is the exception for phones when we change the subscription, there price will change so we set state to Change
                                                    $packageLine->addChild('State', Vznl_Service_Model_Client_EbsOrchestrationClient_PackageItem::PACKAGE_LINE_STATE_CHANGE, $ns);
                                                } else {
                                                    // Change before delivery, the item is still here, it was not changed
                                                    $packageLine->addChild('State', Vznl_Service_Model_Client_EbsOrchestrationClient_PackageItem::PACKAGE_LINE_STATE_NOTHING, $ns);
                                                }
                                            }
                                        } elseif ($salesOrderPackage->getChangeType() === 'after') {

                                            // Check if item has changed
                                            if ($salesOrderPackage->getAxiState() == 'Return'
                                                || ($oldSalesPackage && (!array_key_exists($oldLineItem->getSku(), $salesOrderPackageSkus) || $oldLineItem->getItemDoa()))
                                                || ($this->hasPriceChanged($oldLineItem->getSku(), $oldLineItem->getPackageId()))
                                            ) {
                                                // Do nothing
                                                // Set return reason because this item will be returned and not reserved in the new order
                                                $returnReason = $salesOrderPackage->getReturnRefundReason($oldLineItem);
                                            } else {
                                                // Decrease sku count from the new package
                                                $salesOrderPackageSkus[$oldLineItem->getSku()]--;
                                                if ($salesOrderPackageSkus[$oldLineItem->getSku()] == 0) {
                                                    unset($salesOrderPackageSkus[$oldLineItem->getSku()]);
                                                }
                                            }

                                            $packageLine->addChild('State', 'Return', $ns); // Change after deliver, entire package is always returned
                                        }
                                        break;
                                    default:
                                        $packageLine->addChild('State', 'Nothing', $ns); // Delivery address changed or other non package related change
                                        break;
                                }

                                if ($this->hasPriceChanged($oldLineItem->getSku(), $oldLineItem->getPackageId()) && $salesOrderPackage->getChangeType() === 'before') {
                                    // Here send the new phone, so we get correct mix match in price
                                    $phoneInNewPackage = $salesOrderPackage->getItemBySku($oldLineItem->getSku());
                                    if ($phoneInNewPackage) {
                                        $self->_addLineItemDetailNodes($packageLine, $packageLineCount, $phoneInNewPackage, $salesOrderPackage, $ns);
                                    } else {
                                        $self->_addLineItemDetailNodes($packageLine, $packageLineCount, $oldLineItem, $salesOrderPackage, $ns); // Both phone and subscription changed
                                    }
                                } else {
                                    $self->_addLineItemDetailNodes($packageLine, $packageLineCount, $oldLineItem, $salesOrderPackage, $ns);
                                }

                                if ($oldLineItem->hasReference()) {
                                    $productReference = $packageLine->addChild('ProductReferences', null, $ns);
                                    $item = $productReference->addChild('Item', null, $ns);
                                    $item->addChild('ReferenceID', $oldLineItem->getReference(), $ns);
                                    $item->addChild('ReferenceType', $oldLineItem->getReferenceType(), $ns);
                                }

                                $packageLine->addChild('TechnicalCodes', $oldLineItem->getTechnicalCodes(), $ns);
                                $packageLine->addChild('LineInfo', null, $ns);
                                $packageLine->addChild('ReturnReason', $returnReason, $ns);
                                $packageLineState = current($packageLine->firstChild('State'));
                                $packageLine->addChild('IsCurrent', $packageLineState === 'Nothing' || $packageLineState === 'Reserve' ? 'true' : 'false', $ns);

                                $packageLineCount++;
                                unset($historyItems[$key]);
                                unset($salesOrderOldPackageLineItems[$itemKey]);
                            }
                        }

                        if ($foundInOldItems) {
                            continue;
                        }

                        // Loop trough new line items (what was added or changed)
                        $foundInSecondSearch = false;
                        /**
                         * @var int $i
                         * @var Vznl_Service_Model_Client_EbsOrchestrationClient_PackageItem $salesOrderPackageLineItem
                         */
                        foreach ($salesOrderPackageLineItems as $i => $salesOrderPackageLineItem) {
                            $returnReason = '';
                            if (isset($newPackagesIds[$historyItem['package_id']]) && $newPackagesIds[$historyItem['package_id']] != $salesOrderPackageLineItem->getPackageId()) {
                                continue; // skip item if in different package
                            }
                            if (($historyItem['product_id'] == $salesOrderPackageLineItem->getSku()) && ! $foundInSecondSearch) {
                                if (($salesOrderPackage->getChangeType() !== 'after') && ($salesOrder->getEsbActionRequired() !== Vznl_Superorder_Model_Superorder::SO_CHANGE_AFTER)) {
                                    $packageLineCount = $historyItem['line_id'];
                                }
                                $foundInSecondSearch = true;
                                unset($historyItems[$key]);
                            } else {
                                continue;
                            }

                            $state = 'Nothing';

                            if ($salesOrderPackage->getAction() === 'change') {
                                if ($salesOrderPackage->getChangeType() === 'before') {
                                    //if ($salesOrderPackageLineItem->hasChanged()) {
                                    $salesOrderPackageLineItemSku = $salesOrderPackageLineItem->getSku();
                                    if ($oldSalesPackage && !isset($flippedOldSalesPackageSkus[$salesOrderPackageLineItemSku])) {
                                        $state = 'Reserve';  // This was cancelled in the previous loop, reserve the new item
                                    } else {
                                        continue; // Item has not change, skip it
                                    }
                                } elseif ($salesOrderPackage->getChangeType() === 'after') {
                                    $state = 'Reserve'; // Old package was returned in the above, now reserving new item
                                    $editedItems++;
                                }
                            } elseif ($salesOrderPackage->getAction() === 'cancel') {
                                if ($salesOrderPackage->getChangeType() === 'before') {
                                    $state = 'Cancel'; // Cancel this item from the order
                                } elseif ($salesOrderPackage->getChangeType() === 'after') {
                                    $state = 'Return'; // Return this item to store, no new lines are created resulting in effective cancel
                                    $returnedItems++;

                                    // Set a return reason, we are returning this package
                                    $returnReason = $salesOrderPackage->getReturnRefundReason($salesOrderPackageLineItem);
                                }
                            } elseif ($salesOrderPackage->getAction() === 'reserve') {
                                $state = 'Reserve'; // Normal aquisition
                                $editedItems++;
                            } else {
                                $state = 'Nothing'; // Delivery address changed or other non package related change
                            }

                            $packageLine = $packageLines->addChild('PackageLine', null, $ns);
                            $packageLine->addChild('State', $state, $ns);

                            $self->_addLineItemDetailNodes($packageLine, $packageLineCount, $salesOrderPackageLineItem, $salesOrderPackage, $ns);

                            $productReference = $packageLine->addChild('ProductReferences', null, $ns);
                            $item = $productReference->addChild('Item', null, $ns);
                            if (
                                (
                                    (
                                        $salesOrderPackageLineItem->getReferenceType()
                                        && $oldSalesPackage
                                        && isset($flippedOldSalesPackageSkus[$salesOrderPackageLineItemSku])
                                    )
                                    || $salesOrderPackageLineItem->isSubscription()
                                    || $state === 'Return'
                                )
                                && ($state !== 'Reserve' || !$salesOrderPackageLineItem->getItemDoa())
                            ) {
                                $item->addChild('ReferenceID', $salesOrderPackageLineItem->getReference(), $ns);
                            } else {
                                $item->addChild('ReferenceID', null, $ns);
                            }
                            $item->addChild('ReferenceType', $salesOrderPackageLineItem->getReferenceType(), $ns);

                            $packageLine->addChild('TechnicalCodes', $salesOrderPackageLineItem->getTechnicalCodes(), $ns);
                            $packageLine->addChild('LineInfo', null, $ns);
                            $packageLine->addChild('ReturnReason', $returnReason, $ns);
                            $packageLineState = current($packageLine->firstChild('State'));
                            $packageLine->addChild('IsCurrent', $packageLineState === 'Nothing' || $packageLineState === 'Reserve' ? 'true' : 'false', $ns);

                            $packageLineCount++;

                            unset($salesOrderPackageLineItems[$i]);
                        }

                    }
                }

                // Loop trough new line items (what was added or changed)
                foreach($salesOrderPackageLineItems as $i => $salesOrderPackageLineItem) {
                    $returnReason = '';
                    $state = 'Nothing';
                    $salesOrderPackageLineItemSku = $salesOrderPackageLineItem->getSku();
                    if ($salesOrderPackage->getAction() === 'change') {
                        if ($salesOrderPackage->getChangeType() === 'before') {
                            //if ($salesOrderPackageLineItem->hasChanged()) {
                            if ($oldSalesPackage && !isset($flippedOldSalesPackageSkus[$salesOrderPackageLineItemSku])) {
                                $state = 'Reserve';  // This was cancelled in the previous loop, reserve the new item
                            } else {
                                continue; // Item has not change, skip it
                            }
                        }
                        elseif ($salesOrderPackage->getChangeType() === 'after') {
                            $state = 'Reserve'; // Old package was returned in the above, now reserving new item
                            $editedItems++;
                        }
                    } elseif ($salesOrderPackage->getAction() === 'cancel') {
                        if ($salesOrderPackage->getChangeType() === 'before') {
                            $state = 'Cancel'; // Cancel this item from the order
                        } elseif ($salesOrderPackage->getChangeType() === 'after') {
                            $state = 'Return'; // Return this item to store, no new lines are created resulting in effective cancel
                            $returnedItems++;
                            // Set a return reason, we are returning this package
                            $returnReason = $salesOrderPackage->getReturnRefundReason($salesOrderPackageLineItem);
                        }
                    } elseif ($salesOrderPackage->getAction() === 'reserve') {
                        $state = 'Reserve'; // Normal aquisition
                        $editedItems++;
                    } else {
                        $state = 'Nothing'; // Delivery address changed or other non package related change
                    }

                    $packageLine = $packageLines->addChild('PackageLine', null, $ns);
                    $packageLine->addChild('State', $state, $ns);
                    if (($state == 'Cancel' || $state == 'Nothing') && !empty($historyItems)) {
                        $foundItem = false;
                        foreach ($historyItems as $key => $historyItem) {
                            if (! $foundItem && ($historyItem['removed'] == 0) && ($historyItem['product_id'] == $salesOrderPackageLineItem->getSku())) {
                                $foundItem = true;
                                $packageLineCount = $historyItem['line_id'];
                                unset($historyItems[$key]);
                            }
                        }
                    }

                    $self->_addLineItemDetailNodes($packageLine, $packageLineCount, $salesOrderPackageLineItem, $salesOrderPackage, $ns);

                    $productReference = $packageLine->addChild('ProductReferences', null, $ns);
                    $item = $productReference->addChild('Item', null, $ns);
                    if (
                        (
                            ($salesOrderPackageLineItem->getReferenceType() && $oldSalesPackage && isset($flippedOldSalesPackageSkus[$salesOrderPackageLineItemSku]))
                            ||
                            ($salesOrderPackageLineItem->isSubscription())
                            || $state === 'Return'
                        )
                        && ($state !== 'Reserve' || !$salesOrderPackageLineItem->getItemDoa())
                    ) {
                        $item->addChild('ReferenceID',  $salesOrderPackageLineItem->getReference(), $ns);
                    } else {
                        $item->addChild('ReferenceID', null, $ns);
                    }
                    $item->addChild('ReferenceType', $salesOrderPackageLineItem->getReferenceType(), $ns);

                    $packageLine->addChild('TechnicalCodes', $salesOrderPackageLineItem->getTechnicalCodes(), $ns);
                    $packageLine->addChild('LineInfo', null, $ns);
                    $packageLine->addChild('ReturnReason', $returnReason, $ns);
                    $packageLineState = current($packageLine->firstChild('State'));
                    $packageLine->addChild('IsCurrent', $packageLineState === 'Nothing' || $packageLineState === 'Reserve' ? 'true' : 'false', $ns);

                    $packageLineCount++;
                    unset($salesOrderPackageLineItems[$i]);
                }

                if ($salesOrder->getEsbActionRequired() == Vznl_Superorder_Model_Superorder::SO_CHANGE_AFTER
                    && $salesOrder->getAxiState() == 'Reserve'
                    && $salesOrderPackage->getAxiState() == 'Reserve'
                    && $esbOldSuperOrder
                    && $returnedItems > 0
                ) {
                    $dummyRows[] = $this->addDummyProduct($historyPackageId);
                }

                // Insert dummy products in the request XML when products are returned after delivery
                if ($dummyRows) {
                    foreach($dummyRows as $dummyRow) {
                        $dummyRow['line_id'] = $packageLineCount;
                        $packageLine = $packageLines->addChild('PackageLine', null, $ns);
                        $dataArray = unserialize($dummyRow['data']);
                        if (is_array($dataArray)) {
                            foreach ($dataArray as $node => $value) {

                                if ($node == 'order_line_i_d') {
                                    $value = $packageLineCount;
                                }

                                $addNode($node, $value, $packageLine);
                                unset($dummyRows[$key]);
                            }
                        }
                        $packageLineCount++;
                    }

                    if (($websiteCode == Vznl_Agent_Model_Website::WEBSITE_INDIRECT_CODE) && ($salesOrderPackage->getChangeType() === 'after') && ($salesOrderPackage->getAction() === 'cancel')) {
                        if ($returnWarehouse === false) {
                            // Update the ReturnedToWarehouse node to true, as requested in DF-006266
                            $package->ReturnedToWareHouse = "true";
                        }
                    }
                }

                // TODO refactor below
                $order = $salesOrderPackage->getDeliveryOrder();
                $customer = $order->getCustomer();
                $isBusiness = $customer->getIsBusiness();
                $isFixed = $order->hasFixed();
                if ($subscription || $isFixed) {
                    $telecomLines = $package->addChild('TelecomLines', null, $ns);

                    $packageModel = $salesOrderPackage->getPackageModel();
                    $telecomLines->addChild('CustomerOrdernumber', null, $ns);

                    $orderReference = $telecomLines->addChild('OrderReferenceIdentification', null, $ns);
                    $orderReference->addChild('IdentifyingEntity', 'SOAP', $ns);
                    $orderReference->addChild('ID', null, $ns);

                    $telecomLines->addChild('OrderType', strtoupper($salesOrderPackage->getTransactionType()), $ns);
                    $telecomLines->addChild('OrderSubType', strtoupper($salesOrderPackage->getPackageType()), $ns);
                    $telecomLines->addChild('OrderSource', 'DA', $ns);
                    $createdAt = new DateTime($order->getCreatedAt('Y-m-d H:i:s.uP'));
                    $telecomLines->addChild('IssueDate', $createdAt->format('Y-m-d'), $ns);
                    $telecomLines->addChild('IssueTime', $createdAt->format('H:i:s.uP'), $ns);

                    $orderProperty = $telecomLines->addChild('OrderProperty', '', $ns);
                    $orderProperty->addChild('Name', 'EXISTING-CUSTOMER-ORDER', $ns);
                    $orderProperty->addChild('Value', ($customer->getBan()) ? 'true' : 'false', $ns);

                    $orderLines = $telecomLines->addChild('OrderLines', null, $ns);

                    $orderLinesCounter = ($firstOneOffId && isset($orderLinesCounter)) ? $orderLinesCounter : 1;

                    $telecomItem = $subscription;
                    // Check if package has set SOC codes form Offer

                    if ($subscription) {
                        try {
                            Mage::helper('multimapper')->validateSocCodesByPackageFromOrder($order, $telecomItem->getPackageId());
                        } catch (LogicException $e) {
                            $this->superOrder->setErrorDetail($e->getMessage())->save();
                            throw new LogicException($e->getMessage());
                        }

                        if ($packageModel->getCurrentNumber()) { // Porting requested
                            $currentNr=$packageModel->getCurrentNumber();
                            if (strpos($currentNr, '06') === 0) {
                                $currentNr='316' . substr($currentNr, 2);
                            }
                        }
                        if ($packageModel->getCtn()) { // Retention
                            $currentNr=$packageModel->getCtn();
                            if (strpos($currentNr, '06') === 0) {
                                $currentNr='316' . substr($currentNr, 2);
                            }
                        }
                    }
                    // Commitment
                    $orderLine = $orderLines->addChild('OrderLine', null, $ns);
                    $orderLine->addChild('Linenumber', $orderLinesCounter, $ns);
                    $orderLine->addChild('OrderLineType', strtoupper($salesOrderPackage->getTransactionType()), $ns);
                    $orderLine->addChild('OrderLineSubType', strtoupper($salesOrderPackage->getPackageType()), $ns);
                    $orderLine->addChild('FulfillmentGroupID', $groupCounter, $ns);
                    $lineItem = $orderLine->addChild('LineItem', null, $ns);
                    $lineItem->addChild('Quantity', 1, $ns);
                    $item = $lineItem->addChild('Item', null, $ns);
                    $item->addChild('Name', null, $ns);
                    $item->addChild('Description', null, $ns);
                    $item->addChild('PromotionIndicator', 'false', $ns);
                    $item->addChild('PromotionForItem', null, $ns);
                    $commodityClassification = $item->addChild('CommodityClassification', null, $ns);
                    $commodityClassification->addChild('NatureCode', 'COMMITMENT', $ns);
                    $itemAction = $item->addChild('ItemAction', null, $ns);
                    $itemAction->addChild('Code', 'CONNECT', $ns);
                    if ($subscription) {
                        $itemAction->addChild('ReasonCode', Mage::helper('multimapper')->getReasonCodeBySubscription($telecomItem, $customer), $ns);
                    }
                    $inventoryItem = $item->addChild('InventoryItem', null, $ns);
                    if ($isFixed) {
                        $inventoryItem->addChild('MSISDN', $order->getFixedTelephoneNumber(), $ns);
                        $inventoryItem->addChild('ResourceID', $order->getFixedTelephoneNumber(), $ns);
                        $inventoryItem->addChild('ResourceType', 'MSISDN', $ns);
                    } else {
                        if ($packageModel->getCurrentNumber()) {
                            $inventoryItem->addChild('MSISDN', $currentNr, $ns);
                            $inventoryItem->addChild('ResourceID', $currentNr, $ns);
                            $inventoryItem->addChild('ResourceType', 'MSISDN', $ns);
                        } else {
                            $inventoryItem->addChild('MSISDN', $packageModel->getCtn(), $ns);
                            $inventoryItem->addChild('ResourceID', $packageModel->getCtn(), $ns);
                            $inventoryItem->addChild('ResourceType', 'MSISDN', $ns);
                        }
                    }
                    if ($subscription) {
                        $subscriptionItem=$item->addChild('SubscriptionItem', null, $ns);
                        if ($packageModel->getCurrentNumber()) {
                            $subscriptionItem->addChild('MSISDN', $currentNr, $ns);
                            $subscriptionItem->addChild('ResourceID', $currentNr, $ns);
                            $subscriptionItem->addChild('ResourceType', 'MSISDN', $ns);
                        } else {
                            $subscriptionItem->addChild('MSISDN', $packageModel->getCtn(), $ns);
                            $subscriptionItem->addChild('ResourceID', $packageModel->getCtn(), $ns);
                            $subscriptionItem->addChild('ResourceType', 'MSISDN', $ns);
                        }
                        $subscriptionItem->addChild('CommitmentPeriod', $telecomItem->getProduct()->getSubscriptionDuration(), $ns);
                        $orderLinesCounter++;
                        if ($packageModel->getOneOfDeal()) {
                            $tempOrderLine.=$orderLine->asXML();
                        }
                    }
                    if ($isFixed) {
                        $isFixedPorting = $packageModel->getFixedNumberPortingStatus();
                        $isSwitchProvider = $packageModel->getFixedOverstappenStatus();
                        if ($isFixedPorting) {
                            if ($packageModel->getFixedFirstLinePhone()) {
                                $portingInformation = $lineItem->addChild('PortingInformation', null, $ns);
                                $portingInformation->addChild('MSISDN', $packageModel->getFixedFirstLinePhone(), $ns);
                                $portingInformation->addChild('OfferType', 'Digital Telephony', $ns);
                            }
                            if ($packageModel->getFixedSecondLinePhone()) {
                                $portingInformation = $lineItem->addChild('PortingInformation', null, $ns);
                                $portingInformation->addChild('MSISDN', $packageModel->getFixedSecondLinePhone(), $ns);
                                $portingInformation->addChild('OfferType', 'Telephony second line', $ns);
                            }
                        } elseif(is_null($isFixedPorting)) {
                            /*not to send anything if it is null*/
                        } else {
                            $portingInformation = $lineItem->addChild('PortingInformation', null, $ns);
                            $portingInformation->addChild('MSISDN', null, $ns);
                        }

                        if ($isSwitchProvider) {
                            $switchProvider = $lineItem->addChild('SwitchProvider', null, $ns);
                            $switchProvider->addChild('SwitchProviderIndicator','true', $ns);
                            $switchProvider->addChild('DonorProvider', $packageModel->getFixedOverstappenCurrentProvider(), $ns);
                            $switchProvider->addChild('PortingWishDate', date('Y-m-d', strtotime($packageModel->getFixedOverstappenDeliveryDate())), $ns);
                            $switchProvider->addChild('CurrentContractID', $packageModel->getFixedOverstappenContractId(), $ns);
                            if ($isBusiness) {
                                $switchProvider->addChild('TerminationFeeFlag', ($packageModel->getFixedOverstappenTerminationFee())?'yes':'no', $ns);
                            }
                        }
                    }

                    if ($packageModel->getCurrentNumber()) { // Porting requested
                        $smartValidation = ($packageModel->getNumberPortingValidationType() == Vznl_Package_Model_Package::NP_VALIDATION_TYPE_SMART_VALIDATION) ? 'true' : 'false';
                        // Add porting information
                        $portingInformation = $lineItem->addChild('PortingInformation', null, $ns);
                        $portingInformation->addChild('SmartValidation', $smartValidation, $ns);
                        $portingInformation->addChild('PrepaidIndicator', ($packageModel->getConnectionType() == 1) ? 'true' : 'false', $ns);
                        if (($packageModel->getNumberPortingValidationType() === Vznl_Package_Model_Package::NP_VALIDATION_TYPE_CLIENT_ID) || ($packageModel->getNumberPortingValidationType() ===
                            Vznl_Package_Model_Package::NP_VALIDATION_TYPE_API_CLIENT_ID)) {
                            $portingInformation->addChild('Customernumber', $packageModel->getNumberPortingClientId(), $ns);
                        }
                        $portingInformation->addChild('MSISDN', $currentNr, $ns);
                        $portingInformation->addChild('ResourceID', $currentNr, $ns);
                        $portingInformation->addChild('ResourceType', 'MSISDN', $ns);

                        // Add porting orderline
                        $orderLine = $orderLines->addChild('OrderLine', null, $ns);
                        $orderLine->addChild('Linenumber', $orderLinesCounter, $ns);
                        $orderLine->addChild('OrderLineType', strtoupper($salesOrderPackage->getTransactionType()), $ns);
                        $orderLine->addChild('OrderLineSubType', strtoupper($salesOrderPackage->getPackageType()), $ns);
                        $orderLine->addChild('FulfillmentGroupID', $groupCounter, $ns);
                        $lineItem = $orderLine->addChild('LineItem', null, $ns);
                        $lineItem->addChild('Quantity', 1, $ns);
                        $portingInformation = $lineItem->addChild('PortingInformation', null, $ns);
                        $portingInformation->addChild('SmartValidation', $smartValidation, $ns);
                        $portingInformation->addChild('PrepaidIndicator', ($packageModel->getConnectionType() == 1) ? 'true' : 'false', $ns);
                        if ($packageModel->getNumberPortingValidationType() == Vznl_Package_Model_Package::NP_VALIDATION_TYPE_CLIENT_ID) {
                            $portingInformation->addChild('Customernumber', $packageModel->getNumberPortingClientId(), $ns);
                        }
                        $portingInformation->addChild('MSISDN', $currentNr, $ns);
                        $portingInformation->addChild('ResourceID', $currentNr, $ns);
                        $portingInformation->addChild('ResourceType', 'MSISDN', $ns);

                        $orderLinesCounter++;
                        if ($packageModel->getOneOfDeal()) {
                            $tempOrderLine .= $orderLine->asXML();
                        }
                    }

                    if ($packageModel->getPackageId() == $firstOneOffId && $tempOrderLine != '') {
                        $newOrderLines = $telecomLines->addChild('OrderLines');
                        $replaceMe = $newOrderLines->addChild('replaceMe');
                    }
                    if ($packageModel->getOneOfDeal()) {
                        //$tempOrderLine .= $orderLines->asXML();
                        foreach($telecomLines->children() as $c) {
                            if ($c->getName() == 'OrderLines') {
                                $telecomLines->removeChild($c);
                            }
                        }
                        if ($packageModel->getPackageId() != $firstOneOffId) {
                            $tmpOL = $telecomLines->addChild('OrderLines');
                            $tmpOL->addChild('OrderLine');
                        }
                    }
                }

                $packageModel = $salesOrderPackage->getPackageModel();
                $handsetDetails = $packageModel->getHandsetDetails($salesOrderPackage->getDeliveryOrder());

                // Add the imei information
                $deviceInformation = $package->addChild('DeviceInformation', null, $ns);
                $deviceInformation->addChild('SEManufacturer', isset($handsetDetails['manufacturer']) ? $handsetDetails['manufacturer'] : '', $ns);
                $deviceInformation->addChild('SEModelName', isset($handsetDetails['name']) ? $handsetDetails['name'] : '', $ns);
                $deviceInformation->addChild('SEHandsetCategory', isset($handsetDetails['category']) ? $handsetDetails['category'] : '', $ns);
                $deviceInformation->addChild('DeviceIMEI', $packageModel->getImei() ? : null, $ns);

                if ($replaceMe !== false && $tempOrderLine != '') {
                    $package = str_replace('<replaceMe/>',$tempOrderLine,$package->asXML());
                    $package = new SimpleXMLElement($package);
                }

                $basePackage->insertBeforeSelf($package);
                if ($firstOneOffId) {
                    $groupCounter++;
                }
            }

            // Add all old deleted packages in before delivery scenario
            $historyDeletedPackages = $historyHelper->getDeletedPackages($superOrderId);
            if (count($historyDeletedPackages) && ! $afterDeliveryEdit) {
                foreach ($historyDeletedPackages as $packageId => $historyPackage) {
                    // skip already processed packages
                    if (isset($processedPackages[$packageId])) {
                        continue;
                    }

                    $packageRoot = new Omnius_Core_Model_SimpleDOM('<root></root>');
                    $package = $packageRoot->addChild('Package', null, 'http://DynaLean/ProcessOrder/V1-0');

                    $deletedHistoryItems = $historyHelper->getHistoryItems($superOrderId, $historyPackage['package_id']);

                    $dataArray = unserialize($historyPackage['data']);
                    if (is_array($dataArray)) {
                        foreach ($dataArray as $node => $value) {

                            if ($node == 'state') {
                                $value = 'Nothing';
                            }

                            if ($node == 'return_package_i_d') {
                                $value = '';
                            }

                            $addNode($node, $value, $package);
                            unset($historyDeletedPackages[$packageId]);

                            if ($node == 'return_dealer_code') {
                                // Add package lines
                                $packageLines = $package->addChild('PackageLines', null, $ns);
                                foreach ($deletedHistoryItems as $k => $deletedHistoryItem) {
                                    $packageLine = $packageLines->addChild('PackageLine', null, $ns);
                                    $dataArray = unserialize($deletedHistoryItem['data']);
                                    if (is_array($dataArray)) {
                                        foreach ($dataArray as $node => $value) {

                                            if ($node == 'state') {
                                                $value = 'Nothing';
                                            }
                                            if ($node == 'order_line_i_d') {
                                                $value = $deletedHistoryItem['line_id'];
                                            }

                                            $addNode($node, $value, $packageLine);
                                            unset($deletedHistoryItems[$k]);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    $basePackage->insertAfterSelf($package);
                }
            }

            unset($basePackage[0]);
        }
        );
    }

    public function getArrayCount($dataArray)
    {
        return count($dataArray);
    }
    /**
     * @param $customer
     * @param null $quoteOrOrder
     * @return string
     *
     * Returns customer correspondence email if exists or first available email in additional email
     */
    private function _getCustomerEmail($customer, $quoteOrOrder = null, $strUpper = false)
    {
        if (!$this->_email) {
            $this->_email = $customer->getCorrespondanceEmail($quoteOrOrder);
        }
        return $strUpper ? strtoupper($this->_email) : $this->_email;
    }

    private function _getCustomerPhone($customer)
    {
        $telCsv = $customer->getData('additional_telephone');
        $telArr = explode(';', $telCsv);
        return Mage::helper('vznl_service')->stripPlusChar($telArr[0]);
    }

    /**
     * @param $refundAmount
     * @return int
     */
    protected function getRefundAmountTotal($refundAmount, Mage_Sales_Model_Order $order)
    {
        // Set to 0 when not delivered yet
        if ($order->isHomeDelivery() && strtolower($order->getPayment()->getMethod()) != 'adyen_hpp') {
            foreach ($order->getDeliveryOrderPackages() as $package) {
                if ($package->getStatus() == Vznl_Package_Model_Package::ESB_PACKAGE_STATUS_NOT_DELIVERED) {
                    return 0;
                }
            }
        }
        // Pick the correct refund amount
        $refundAmountTotal = 0;
        if ($refundAmount && isset($refundAmount['refundAmount']) && is_array($refundAmount['refundAmount'])) {
            foreach ($refundAmount['refundAmount'] as $amount) {
                $refundAmountTotal += $amount;
            }
        }
        return $refundAmountTotal;
    }

    /**
     * @param Vznl_Superorder_Model_Superorder $superOrder
     * @param Mage_Sales_Model_Order $order
     * @param Vznl_Service_Model_Client_EbsOrchestrationClient_SaleOrder $saleOrder
     * @param Mage_Customer_Model_Customer $customer
     * @param Vznl_Superorder_Model_Superorder|null $esbOldSuperOrder
     * @param string|null $refundMethod
     * @param array|null $refundAmount
     * @return array
     */
    protected function getGeneralNodes(
        Vznl_Superorder_Model_Superorder $superOrder,
        Mage_Sales_Model_Order $order,
        Vznl_Service_Model_Client_EbsOrchestrationClient_SaleOrder $saleOrder,
        Mage_Customer_Model_Customer $customer,
        Vznl_Superorder_Model_Superorder $esbOldSuperOrder = null,
        $refundMethod = null,
        $refundAmount = null
    ) {
        $agent = Mage::getModel('agent/agent')->load($order->getAgentId());
        $source = Mage::app()->getRequest()->getServer(self::ENV_VHOST_CODE) ?? Mage::helper('omnius_service')->getGeneralConfig('default_source_value');
        $refundAmountTotal = $this->getRefundAmountTotal($refundAmount, $order);
        $callAxi = true;
        $agentHelper = Mage::helper('agent');
        if ($superOrder->hasFixed() && ($agentHelper->checkIsTelesalesOrWebshop() || $agentHelper->isIndirectStore()) && !($superOrder->hasMobilePackage())) {
            $callAxi = false;
        }
        return array(
            //Header
            'ns0:Header/dt:TransactionId' => time(),
            'ns0:Header/dt:PartyName' => Mage::getStoreConfig('vodafone_service/general/partyname'),
            'ns0:Header/dt:UserId' => $agentHelper->getAgentId(),
            'ns0:Channel/dt:ChannelName' => Mage::getStoreConfig('vodafone_service/general/channelname'),
            'ns0:Action' => $saleOrder->getEsbAction(),
            'ns0:RequestID' => $superOrder->getOrderGuid() ? : '',
            'ns0:BusinessIdentifier' => (Mage::app()->getStore()->getCode() == Vznl_Agent_Model_Website::WEBSITE_BELCOMPANY_CODE) ? 'BelCompany' : 'Vodafone',
            'ns0:State' => $saleOrder->getAxiState(),
            'ns0:InOverrideMode' => Mage::getStoreConfig('vodafone_service/general/override') ? 'true' : 'false',
            'ns0:SalesOrderNumber' => $superOrder->getOrderNumber(),
            'ns0:ReturnSalesOrderNumber' => ($esbOldSuperOrder ? $esbOldSuperOrder->getOrderNumber() : ''),
            'ns0:OrderStoreNumber' => (Mage::app()->getStore()->getCode() == Vznl_Agent_Model_Website::WEBSITE_INDIRECT_CODE) ?  Mage::helper('agent')->getWharehouseCode() : Mage::helper('agent')->getAxiStore(),
            'ns0:OrderDate' => date('Y-m-d'), // TODO strtotime($superOrder->getCreatedAt())), Seems this is not persisted
            'ns0:StoreUser' => $agent->getId(),
            'ns0:MMLStatus' => function($status, $ns) use ($customer) {
                if ($label = $customer->getCustomerLabel()) {
                    $status[0][0] = $label;
                } else {
                    unset($status[0][0]);
                }
            },
            'ns0:RefundViaDSS' => ($refundMethod && $refundMethod === 'dss') ? 'true' : 'false',
            'ns0:RefundAmount' => ($refundMethod && $refundMethod === 'dss' && $refundAmountTotal && $refundAmountTotal != 0) ? $refundAmountTotal : 0,
            'ns0:SalesChannel' => Mage::getModel('agent/channeloverride')->getStoreByDealerCode(Mage::helper('agent')->getDaDealerCode()),
            'ns0:UBuyCustomerID' => $customer->getId(),
            'ns0:Pan' => $customer->getPan(),
            'ns0:Source' => $source,
            'ns0:CallAxi' => $callAxi ? 'true' : 'false',
        );
    }

    /**
     * @param Vznl_Superorder_Model_Superorder $superOrder
     */
    protected function getGeneralSalesNodes(Vznl_Superorder_Model_Superorder $superOrder) {
        $self = $this;
        return array(
            'ns0:DealerInformation' => function($DealerInformation, $ns) use ($superOrder) {
                $baseNode = array_shift($DealerInformation);
                $namespaces = $baseNode->getNamespaces(true);
                $ns = isset($namespaces['dt']) ? $namespaces['dt'] : $ns;
                $agentHelper = Mage::helper('agent');

                if ($agentHelper->getAgent()->getData('sales_code')) {
                   $salesCode = $agentHelper->getAgent()->getData('sales_code');
                } else {
                   $salesCode = $agentHelper->getDealerData()->getData('sales_code');
                }

                $DealerInformationNode = new Omnius_Core_Model_SimpleDOM('<root></root>');
                $DealerInfoNode = $DealerInformationNode->addChild('DealerInformation', null, $namespaces['ns0']);
                $DealerInfoNode->addChild('DealerChannel', 'BC', $ns);
                $DealerInfoNode->addChild('DealerCode', $agentHelper->getDaDealerCode(), $ns);
                $DealerInfoNode->addChild('AgentName', $agentHelper->getAgentName(), $ns);

                if ($superOrder->hasFixed()) {
                    $DealerInfoNode->addChild('Salescode', $salesCode, $ns);
                    $DealerInfoNode->addChild('SalesChannel', $agentHelper->getDealerData()->getData('sales_channel'), $ns);
                    if ($agentHelper->isRetailStore()) {
                        $DealerInfoNode->addChild('SalesLocation', $agentHelper->getDealerData()->getData('sales_location'), $ns);
                    }
                }

                $baseNode->insertAfterSelf($DealerInfoNode);
                unset($baseNode[0]);
            }
        );
    }

    public static function _mapAxiLanguage($mageCountryCode) {
        // TODO
        return 'NED';
    }

    /**
     * @param Mage_Customer_Model_Customer $customer
     * @param Mage_Sales_Model_Order $order
     * @return array
     */
    protected function getCustomerDataNodes(Mage_Customer_Model_Customer $customer, $order)
    {
        $self = $this;
        return array(
            'ns0:CustomerData' => function($customerData, $ns) use ($customer, $order, $self) {
                $baseNode = array_shift($customerData);
                $namespaces = $baseNode->getNamespaces(true);
                $ns = isset($namespaces['dt']) ? $namespaces['dt'] : $ns;
                $checkoutHelper = Mage::helper('dyna_checkout');

                $customerDataNode = new Omnius_Core_Model_SimpleDOM('<root></root>');
                $customerNode = $customerDataNode->addChild('CustomerData', null, $namespaces['ns0']);
                $customerNode->addChild('CustomerID', $customer->getId(), $namespaces['dt']);
                $customerNode->addChild('CustomerLanguage', $self::_mapAxiLanguage($customer->getCountry()), $namespaces['dt']);
                $customerNode->addChild('PartyType', $customer->getIsBusiness() ? 'BUSINESS' : 'CONSUMER', $namespaces['dt']);
                if ($customer->getBan()) {
                    $paNode = $customerNode->addChild('PartyIdentification',null, $namespaces['dt']);
                    $paNode->addChild('ID',  $customer->getBan(), $namespaces['dt']);
                }

                $customerNode->addChild('VATnumberPrefix', '', $namespaces['dt']); // TODO, AXI?? WAT IS DIT?
                $customerNode->addChild('VATnumber', $customer->getCompanyVatId(), $namespaces['dt']);
                $customerNode->addChild('Debtornumber', (!is_null($customer->getBan()) ? $customer->getBan() : $customer->getId()), $namespaces['dt']);

                if ($customer->getIsBusiness()) {

                    $firstName = $checkoutHelper->formatNameForEbsOrchestration($customer->getContractantFirstname() ?: $customer->getFirstname(), 12);
                    $middleName = $checkoutHelper->formatNameForEbsOrchestration($customer->getContractantMiddlename() ?: $customer->getMiddlename(), 100);
                    $surName = $checkoutHelper->formatNameForEbsOrchestration($customer->getContractantLastname() ?: $customer->getLastname(), 60);

                    $customerNode->addChild('DayOfBirth', date('Y-m-d', strtotime($customer->getData('contractant_dob') ?: $customer->getData('dob'))), $ns);
                    $customerNode->addChild('SurName', $surName, $namespaces['dt']);
                    $customerNode->addChild('MiddleName', $middleName, $namespaces['dt']);
                    $customerNode->addChild('FirstName', $firstName, $namespaces['dt']);
                    $customerNode->addChild('CompanyName', $customer->getData('company_name'), $namespaces['dt']);
                    $customerNode->addChild('Cocnumber', $customer->getData('company_coc'), $namespaces['dt']);
                } else {
                    $customerNode->addChild('DayOfBirth', date('Y-m-d', strtotime($customer->getData('dob'))), $ns);
                    $customerNode->addChild('SurName', $customer->getLastname(), $namespaces['dt']);
                    $customerNode->addChild('MiddleName', $customer->getMiddlename(), $namespaces['dt']);
                    $customerNode->addChild('FirstName', $customer->getFirstname(), $namespaces['dt']);
                }
                $address = $order->getBillingAddress();
                $telNr = $address->getData('telephone');
                if (strpos($address->getData('telephone'), '31') === 0) {
                    $telNr = Mage::helper('dyna_checkout')->formatCtn($address->getData('telephone'), false);

                }
                $telNr = Mage::helper('vznl_service')->stripPlusChar($telNr);

                $customerNode->addChild('Phonenumber', $telNr, $namespaces['dt']);
                $customerNode->addChild('Mobilenumber', $self->_getCustomerPhone($customer), $namespaces['dt']);
                $customerNode->addChild('Faxnumber', Mage::helper('vznl_service')->stripPlusChar($customer->getFax()), $namespaces['dt']);
                $customerNode->addChild('Email', $self->_getCustomerEmail($customer, $order), $namespaces['dt']);
                $customerNode->addChild('UpdateEmailPeal', 'false', $namespaces['dt']);
                $customerNode->addChild('CustomerType', $customer->getIsBusiness() ? 'business' : 'private', $namespaces['dt']); // RFC 140468
                $customerNode->addChild('BillingArrangementId', $customer->getBillingArrangementId(), $namespaces['dt']);

                $prefix = '';
                if ($customer->getIsBusiness()) {
                    if ($customer->getContractantGender() == 1) {
                        $prefix = 'DHR';
                    } else if ($customer->getContractantGender() == 2) {
                        $prefix = 'MEVR';
                    } else if ($customer->getContractantGender() == 3) {
                        $prefix = 'DHR/MEVR';
                    }
                } else {
                    $prefix = strtoupper($checkoutHelper->__($customer->getPrefix()));
                    $prefix = str_replace('.','', $prefix);
                }

                $gender = null;
                switch($prefix) {
                    case 'DHR':
                        $gender = 'MALE';
                        break;
                    case 'MEVR':
                        $gender = 'FEMALE';
                        break;
                }

                $daAccountNode = $customerNode->addChild('Account', null, $namespaces['dt']);
                $daAccountIdentificationNode = $daAccountNode->addChild('AccountIdentification', null, $namespaces['dt']);
                if ($customer->getBan()) {
                    $daAccountIdentificationNode->addChild('IdentifyingEntity', 'GEMINI', $namespaces['dt']);
                    $daAccountIdentificationNode->addChild('ID', $customer->getBan(), $namespaces['dt']);
                }
                $daAccountNode->addChild('Type', $customer->getIsBusiness() ? 'C' : 'D', $namespaces['dt']);
                $daAccountNode->addChild('Category', $customer->getIsBusiness() ? 'C' : 'D', $namespaces['dt']);
                $daAccountNode->addChild('FraudSuspect', $order->getSuspectFraud() ? 'true' : 'false', $namespaces['dt']);

                // BILLING ADDRESS
                $addition = null;
                $street = $address->getStreet(1);
                $houseNr = $address->getStreet(2);
                $addition = $address->getStreet(3);
                $zipCode = str_replace(' ', '', $address->getData('postcode'));
                $country = $address->getData('country_id');
                $countryCode = $address->getData('country_id');
                $city = $address->getData('city');
                $addressId = $address->getData('region_id');

                $isFixed = $this->superOrder->hasFixed();
                $isMobile = $this->superOrder->hasMobilePackage();

                if ($isFixed) {
                    $serviceAddress = unserialize($order->getData('service_address'));
                    $addressNode = $daAccountNode->addChild('Address', null, $namespaces['dt']);
                    $addressNode->addChild('AddressID', $serviceAddress["addressId"], $ns);
                    $addressNode->addChild('FootPrint', 'ZIGGO', $ns);
                    $addressNode->addChild('StreetName', $serviceAddress["street"], $ns);
                    $addressNode->addChild('Housenumber', $serviceAddress["housenumber"], $ns);
                    $addressNode->addChild('ZipCode', str_replace(' ', '', $serviceAddress["postalcode"]), $ns);
                    $addressNode->addChild('City', $serviceAddress["city"], $ns);
                    $addressNode->addChild('AddressType', 'SERVICE', $ns);
                    $addressNode->addChild('Country', "Netherlands", $ns);
                    $addressNode->addChild('CountryCode', "NL", $ns);
                }

                if ($isMobile) {
                    $addressNode = $daAccountNode->addChild('Address', null, $namespaces['dt']);
                    if ($addressId) {
                        $addressNode->addChild('AddressID', $addressId, $ns);
                    }
                    $addressNode->addChild('StreetName', $street, $ns);
                    $addressNode->addChild('Housenumber', $houseNr, $ns);
                    $addressNode->addChild('ZipCode', $zipCode, $ns);
                    $addressNode->addChild('City', $city, $ns);
                    $addressNode->addChild('AddressType', 'BILLING-VODAFONE', $ns);
                    $addressNode->addChild('Country', $country, $ns);
                    $addressNode->addChild('CountryCode', $countryCode, $ns);
                    if ($addition)
                        $addressNode->addChild('HousenumberAddition', $addition, $ns);
                    $addressNode->addChild('PObox', '', $ns);
                    $addressNode->addChild('POboxZipCode', '', $ns);
                    $addressNode->addChild('POboxCity', '', $ns);
                }
                if($isFixed)
                {
                    $addressNode = $daAccountNode->addChild('Address', null, $namespaces['dt']);
                    $fixedStreet = $address->getData('fixed_street');
                    $fixedStreetArr = is_array($fixedStreet) ? $fixedStreet : explode("\n", $fixedStreet);
                    $addressNode->addChild('AddressID', isset($fixedStreetArr['3']) ? $fixedStreetArr['3']: null, $ns);
                    $addressNode->addChild('FootPrint', 'ZIGGO', $ns);
                    $addressNode->addChild('StreetName', isset($fixedStreetArr['0']) ? $fixedStreetArr['0']: null, $ns);
                    $addressNode->addChild('Housenumber', isset($fixedStreetArr['1']) ? $fixedStreetArr['1']: null, $ns);
                    $addressNode->addChild('ZipCode', str_replace(' ', '', $address->getData('fixed_postcode')), $ns);
                    $addressNode->addChild('City', $address->getData('fixed_city'), $ns);
                    $addressNode->addChild('AddressType', 'BILLING-ZIGGO', $ns);
                    $addressNode->addChild('Country', $country, $ns);
                    $addressNode->addChild('CountryCode', $countryCode, $ns);
                    if (isset($fixedStreetArr['2']))
                        $addressNode->addChild('HousenumberAddition', $fixedStreetArr['2'], $ns);
                    $addressNode->addChild('PObox', '', $ns);
                    $addressNode->addChild('POboxZipCode', '', $ns);
                    $addressNode->addChild('POboxCity', '', $ns);
                }

                // CUSTOMER ADDRESS
                if ($customer->getIsBusiness()) {
                    $addition = null;
                    $street = $order->getData('company_street');
                    $houseNr = $order->getData('company_house_nr');
                    $addition = $order->getData('company_house_nr_addition');
                    $zipCode = str_replace(' ', '', $order->getData('company_postcode'));
                    $country = $order->getData('company_country_id');
                    $countryCode = $order->getData('company_country_id');
                    $city = $order->getData('company_city');
                } else {
                    $addition = null;
                    $street = $address->getStreet(1);
                    $houseNr = $address->getStreet(2);
                    $addition = $address->getStreet(3);
                    $zipCode = str_replace(' ', '', $address->getData('postcode'));
                    $country = $address->getData('country_id');
                    $countryCode = $address->getData('country_id');
                    $city = $address->getData('city');
                }

                $addressNode = $daAccountNode->addChild('Address', null, $namespaces['dt']);
                $addressNode->addChild('StreetName', $street, $ns);
                $addressNode->addChild('Housenumber', $houseNr, $ns);
                $addressNode->addChild('ZipCode', $zipCode, $ns);
                $addressNode->addChild('City', $city, $ns);
                $addressNode->addChild('AddressType', 'CUSTOMER', $ns);
                $addressNode->addChild('Country', $country, $ns);
                $addressNode->addChild('CountryCode', $countryCode, $ns);
                if ($addition)
                    $addressNode->addChild('HousenumberAddition', $addition, $ns);
                $addressNode->addChild('PObox', '', $ns);
                $addressNode->addChild('POboxZipCode', '', $ns);
                $addressNode->addChild('POboxCity', '', $ns);

                // CUSTOMER CONTACT
                $contactNode = $daAccountNode->addChild('Contact', null, $namespaces['dt']);
                $contactNode->addChild('ContactType', 'CUSTOMER', $ns);
                $nameNode = $contactNode->addChild('Name', null, $ns);
                if (!$customer->getIsBusiness()) {
                    $nameNode->addChild('Title', $prefix, $ns);
                    $nameNode->addChild('FirstName', $address->getData('firstname'), $ns);
                    $nameNode->addChild('MiddleName', $address->getData('middlename'), $ns);
                    $nameNode->addChild('LastName', $address->getData('lastname'), $ns);
                } else {
                    $nameNode->addChild('LastName', $order->getData('company_name'), $ns);
                }
                $phoneNode = $contactNode->addChild('Telephone', null, $ns);
                $phoneNode->setAttributes(array('type'=>'VODAFONE'));
                $phoneNode->addChild('Home', $telNr, $ns);
                if ($order->getZiggoTelephone()) {
                    $phoneNode = $contactNode->addChild('Telephone', null, $ns);
                    $phoneNode->setAttributes(array('type'=>'ZIGGO'));
                    $phoneNode->addChild('Home', $order->getZiggoTelephone(), $ns);
                } elseif ($isFixed) {
                    $phoneNode = $contactNode->addChild('Telephone', null, $ns);
                    $phoneNode->setAttributes(array('type'=>'ZIGGO'));
                    $phoneNode->addChild('Home', $telNr, $ns);
                }
                $emailNode = $contactNode->addChild('EMail', $self->_getCustomerEmail($customer, $order), $ns);
                $emailNode->setAttributes(array('type'=>'VODAFONE'));
                if ($order->getZiggoEmail()) {
                    $emailNode = $contactNode->addChild('EMail', $order->getZiggoEmail(), $ns);
                    $emailNode->setAttributes(array('type'=>'ZIGGO'));
                } elseif ($isFixed) {
                    $emailNode = $contactNode->addChild('EMail', $self->_getCustomerEmail($customer, $order), $ns);
                    $emailNode->setAttributes(array('type'=>'ZIGGO'));
                }
                if ($gender)
                    $contactNode->addChild('Gender', $gender, $ns);
                $contactNode->addChild('DateOfBirth', date('Y-m-d', strtotime($customer->getData('dob'))), $ns);

                // CONTACT CONTACT
                //if (!$customer->getBan()) {
                $contactNode = $daAccountNode->addChild('Contact', null, $namespaces['dt']);
                $contactNode->addChild('ContactType', 'CONTACT', $ns);
                $nameNode = $contactNode->addChild('Name', null, $ns);
                $nameNode->addChild('Title', $prefix, $ns);

                $firstName = $checkoutHelper->formatNameForEbsOrchestration($address->getData('firstname'), 12);
                $middleName = $checkoutHelper->formatNameForEbsOrchestration($address->getData('middlename'), 100);
                $lastName = $checkoutHelper->formatNameForEbsOrchestration($address->getData('lastname'), 60);

                $nameNode->addChild('FirstName', $firstName, $ns);
                $nameNode->addChild('MiddleName', $middleName, $ns);
                $nameNode->addChild('LastName', $lastName, $ns);
                $phoneNode = $contactNode->addChild('Telephone', null, $ns);
                $phoneNode->setAttributes(array('type'=>'VODAFONE'));
                $phoneNode->addChild('Home', $telNr, $ns);
                if ($order->getZiggoTelephone()) {
                    $phoneNode = $contactNode->addChild('Telephone', null, $ns);
                    $phoneNode->setAttributes(array('type'=>'ZIGGO'));
                    $phoneNode->addChild('Home', $order->getZiggoTelephone(), $ns);
                } elseif ($isFixed) {
                    $phoneNode = $contactNode->addChild('Telephone', null, $ns);
                    $phoneNode->setAttributes(array('type'=>'ZIGGO'));
                    $phoneNode->addChild('Home', $telNr, $ns);
                }
                $emaiNode = $contactNode->addChild('EMail', $self->_getCustomerEmail($customer, $order), $ns);
                $emaiNode->setAttributes(array('type'=>'VODAFONE'));

                if ($order->getZiggoEmail()) {
                    $emaiNode = $contactNode->addChild('EMail', $order->getZiggoEmail(), $ns);
                    $emaiNode->setAttributes(array('type'=>'ZIGGO'));
                } elseif ($isFixed) {
                    $emaiNode = $contactNode->addChild('EMail', $self->_getCustomerEmail($customer, $order), $ns);
                    $emaiNode->setAttributes(array('type'=>'ZIGGO'));
                }

                if ($gender)
                    $contactNode->addChild('Gender', $gender, $ns);
                $contactNode->addChild('DateOfBirth', date('Y-m-d', strtotime($customer->getData('dob'))), $ns);
                //}

                if (/*!$customer->getBan() && */$customer->getIsBusiness()) {
                    // DIRECTOR CONTACT
                    $contactNode = $daAccountNode->addChild('Contact', null, $namespaces['dt']);
                    $contactNode->addChild('ContactType', 'DIRECTOR', $ns);
                    $nameNode = $contactNode->addChild('Name', null, $ns);
                    $nameNode->addChild('Title', $prefix, $ns);

                    $nameNode->addChild('FirstName', $firstName, $ns);
                    $nameNode->addChild('MiddleName', $middleName, $ns);
                    $nameNode->addChild('LastName', $lastName, $ns);
                    $phoneNode = $contactNode->addChild('Telephone', null, $ns);
                    $phoneNode->setAttributes(array('type'=>'VODAFONE'));
                    $phoneNode->addChild('Home', $telNr, $ns);
                    if ($order->getZiggoTelephone()) {
                        $phoneNode = $contactNode->addChild('Telephone', null, $ns);
                        $phoneNode->setAttributes(array('type'=>'ZIGGO'));
                        $phoneNode->addChild('Home', $order->getZiggoTelephone(), $ns);
                    } elseif ($isFixed) {
                        $phoneNode = $contactNode->addChild('Telephone', null, $ns);
                        $phoneNode->setAttributes(array('type'=>'ZIGGO'));
                        $phoneNode->addChild('Home', $telNr, $ns);
                    }
                    $emaiNode = $contactNode->addChild('EMail', $self->_getCustomerEmail($customer, $order), $ns);
                    $emaiNode->setAttributes(array('type'=>'VODAFONE'));
                    if ($order->getZiggoEmail()) {
                        $emaiNode = $contactNode->addChild('EMail', $order->getZiggoEmail(), $ns);
                        $emaiNode->setAttributes(array('type'=>'ZIGGO'));
                    } elseif ($isFixed) {
                        $emaiNode = $contactNode->addChild('EMail', $self->_getCustomerEmail($customer, $order), $ns);
                        $emaiNode->setAttributes(array('type'=>'ZIGGO'));
                    }

                    if ($gender)
                        $contactNode->addChild('Gender', $gender, $ns);
                    $contactNode->addChild('DateOfBirth', date('Y-m-d', strtotime($customer->getData('dob'))), $ns);
                }

                if ($isMobile)
                {
                    $paymentInformationNode = $daAccountNode->addChild('PaymentInformation', null, $namespaces['dt']);
                    $paymentInformationNode->addChild('PaymentMethod', 'DIRECT-DEBIT', $ns);
                    $paymentInfoNode = $paymentInformationNode->addChild('DirectDebit', null, $ns);
                    $paymentInfoNode->addChild('BankAccountNumber', $order->getData('customer_account_number'), $ns);
                    $paymentInfoNode->addChild('BankAccountHolderName', $order->getData('customer_account_holder'), $ns);
                    $paymentInformationNode->addChild('OneOffPaymentMethod', $order->getData('mobile_oneoff_payment'), $ns);
                    $paymentInformationNode->addChild('Type', 'Vodafone', $ns);
                }

                if ($isFixed) {
                    $paymentInformationNode = $daAccountNode->addChild('PaymentInformation', null, $namespaces['dt']);
                    $paymentInformationNode->addChild('PaymentMethod', $order->getData('fixed_payment_method_monthly_charges'), $ns);
                    if ($order->getData('fixed_bill_distribution_method') == "Verplichte elektr.fakt.") {
                         $dispatchMethod="EMAIL";
                    } else if ($order->getData('fixed_bill_distribution_method') == "Forced - Paper") {
                        $dispatchMethod="MAIL";
                    } else {
                        $dispatchMethod=$order->getData('fixed_bill_distribution_method');
                    }
                    $paymentInformationNode->addChild('DispatchMethod', $dispatchMethod, $ns);
                    $paymentInfoNode = $paymentInformationNode->addChild('DirectDebit', null, $ns);
                    $paymentInfoNode->addChild('BankAccountNumber', $order->getData('fixed_account_number'), $ns);
                    $paymentInfoNode->addChild('BankAccountHolderName', $order->getData('fixed_account_holder'), $ns);
                    $paymentInformationNode->addChild('OneOffPaymentMethod', $order->getData('fixed_payment_method_one_time_charge'), $ns);
                    $paymentInformationNode->addChild('Type', 'Ziggo', $ns);
                }
                if ($customer->getIsBusiness()) {
                    $legalCodeDesc = 'EENMANSZAAK';
                    switch ($order->getData('company_legal_form')) {
                        case '01':
                            $legalCodeDesc = 'EENMANSZAAK';
                            break;
                        case '02':
                            $legalCodeDesc = 'B.V.';
                            break;
                        case '03':
                            $legalCodeDesc = 'N.V.';
                            break;
                        case '04':
                            $legalCodeDesc = 'C.V.';
                            break;
                        case '06':
                            $legalCodeDesc = 'BUITENLANDSE ONDERNEMING';
                            break;
                        case '07':
                            $legalCodeDesc = 'V.O.F.';
                            break;
                        case '08':
                            $legalCodeDesc = 'CO-OPERATIEVE VENNOOTSCHAP';
                            break;
                        case '10':
                            $legalCodeDesc = 'VERENIGING';
                            break;
                        case '13':
                            $legalCodeDesc = 'STICHTING';
                            break;
                    }

                    $addition = null;
                    $street = $order->getData('company_street');
                    $houseNr = $order->getData('company_house_nr');
                    $addition = $order->getData('company_house_nr_addition');
                    $zipCode = str_replace(' ', '', $order->getData('company_postcode'));
                    $country = $order->getData('company_country_id');
                    $countryCode = $order->getData('company_country_id');
                    $city = $order->getData('company_city');

                    $businessInformationNode = $daAccountNode->addChild('BusinessInformation', null, $namespaces['dt']);
                    $businessInformationNode->addChild('CompanyName', $order->getData('company_name'), $ns);
                    if ($order->getData('company_date'))
                        $businessInformationNode->addChild('EstablishedDate', $order->getData('company_date'), $ns);
                    $businessInformationNode->addChild('KVKnumber', $order->getData('company_coc'), $ns);
                    $businessInformationNode->addChild('LegalForm', $legalCodeDesc, $ns);
                    $businessInformationNode->addChild('LegalFormCode', strtoupper($order->getData('company_legal_form')), $ns);
                    $businessInformationAddressNode = $businessInformationNode->addChild('Address', null, $namespaces['dt']);
                    $businessInformationAddressNode->addChild('StreetName', $street, $ns);
                    $businessInformationAddressNode->addChild('Housenumber', $houseNr, $ns);
                    $businessInformationAddressNode->addChild('ZipCode', $zipCode, $ns);
                    $businessInformationAddressNode->addChild('City', $city, $ns);
                    $businessInformationAddressNode->addChild('Country', $country, $ns);
                    $businessInformationAddressNode->addChild('CountryCode', $countryCode, $ns);
                    if ($addition)
                        $businessInformationAddressNode->addChild('HousenumberAddition', $addition, $ns);
                }

                $identificationInformationNode = $daAccountNode->addChild('IdentificationInformation', null, $namespaces['dt']);
                $identificationInformationNode->addChild('DateOfBirth', date('Y-m-d', strtotime($customer->getData('dob'))), $ns);
                $identificationNode = $identificationInformationNode->addChild('Identification', null, $namespaces['dt']);
                if ($customer->getIsBusiness()) {
                    $identificationNode->addChild('Type', strtoupper($order->getData('contractant_id_type')), $ns);
                    $identificationNode->addChild('ID', strtoupper($order->getData('contractant_id_number')), $ns);
                    if ($isFixed && !$isMobile) {
                        $contractantCountry = 'NL';
                    } else {
                        $contractantExpiryDate = !empty($order->getData('contractant_valid_until')) ? date('Y-m-d', strtotime($order->getData('contractant_valid_until'))) : null;
                        $contractantCountry = $order->getData('contractant_issuing_country');
                        if (!is_null($contractantExpiryDate)) {
                            $identificationNode->addChild('ExpiryDate', $contractantExpiryDate, $ns);
                        }
                    }
                    $identificationCountryNode = $identificationNode->addChild('Country', null, $namespaces['dt']);
                    $identificationCountryNode->addChild('Code', strtoupper($contractantCountry), $ns);
                    $identificationCountryNode->addChild('Name', strtoupper($contractantCountry), $ns);
                } else {
                    $identificationNode->addChild('Type', strtoupper($customer->getIdType()), $ns);
                    $identificationNode->addChild('ID', $customer->getIdNumber(true), $ns);
                    if ($isFixed && !$isMobile) {
                        $customerCountry = 'NL';
                    } else {
                        $customerExpiryDate = !empty($customer->getValidUntil()) ? date('Y-m-d', strtotime($customer->getValidUntil())) : null;
                        $customerCountry = $order->getData('customer_issuing_country');
                        if (!is_null($customerExpiryDate)) {
                            $identificationNode->addChild('ExpiryDate', $customerExpiryDate, $ns);
                        }
                    }
                    $identificationCountryNode = $identificationNode->addChild('Country', null, $namespaces['dt']);
                    $identificationCountryNode->addChild('Code', $customerCountry, $ns);
                    $identificationCountryNode->addChild('Name', $customerCountry, $ns);
                }

                if ($isMobile)
                {
                    $privacySettingsNode = $daAccountNode->addChild('PrivacySettings', null, $ns);

                    /** @var Vznl_Customer_Helper_Data $customerHelper */
                    $customerHelper = Mage::helper('vznl_customer');

                    $smsNode = $privacySettingsNode->addChild('Setting', null, $ns);
                    $smsNode->addChild('Key', 'SMS', $ns);
                    $smsNode->addChild('Value', $customerHelper->getPrivacyDirectoryValue('SMS', $customer) ? 1 : 0, $ns);
                    $smsNode->addChild('Type', 'Vodafone', $ns);

                    $emailNode = $privacySettingsNode->addChild('Setting', null, $ns);
                    $emailNode->addChild('Key', 'EMAIL', $ns);
                    $emailNode->addChild('Value', $customerHelper->getPrivacyDirectoryValue('EMAIL', $customer) ? 1 : 0, $ns);
                    $emailNode->addChild('Type', 'Vodafone', $ns);

                    $dlNode = $privacySettingsNode->addChild('Setting', null, $ns);
                    $dlNode->addChild('Key', 'DIRECTORY_LISTING', $ns);
                    $dlNode->addChild('Value', $customerHelper->getPrivacyDirectoryValue('DIRECTORY_LISTING', $customer), $ns);
                    $dlNode->addChild('Type', 'Vodafone', $ns);

                    $dlNode = $privacySettingsNode->addChild('Setting', null, $ns);
                    $dlNode->addChild('Key', 'PHONEBOOK_LISTING', $ns);
                    $dlNode->addChild('Value', $customerHelper->getPrivacyDirectoryValue('PHONEBOOK_LISTING', $customer), $ns);
                    $dlNode->addChild('Type', 'Vodafone', $ns);
                }

                if ($isFixed) {

                    $privacySettingsNode = $daAccountNode->addChild('PrivacySettings', null, $ns);

                    $dlNode = $privacySettingsNode->addChild('Setting', null, $ns);
                    $dlNode->addChild('Key', 'PrivacyAddPhoneBook', $ns);
                    $dlNode->addChild('Value', ($this->firstOrder->getPrivacyAddPhoneBook() == 1 ? "YES" : "NO"), $ns);
                    $dlNode->addChild('Type', 'Ziggo', $ns);

                    $dlNode = $privacySettingsNode->addChild('Setting', null, $ns);
                    $dlNode->addChild('Key', 'PrivacyBlockCallerId', $ns);
                    $dlNode->addChild('Value', ($this->firstOrder->getPrivacyBlockCallerId() == 1 ? "YES" : "NO"), $ns);
                    $dlNode->addChild('Type', 'Ziggo', $ns);

                    $dlNode = $privacySettingsNode->addChild('Setting', null, $ns);
                    $dlNode->addChild('Key', 'PrivacyAddInfo', $ns);
                    $dlNode->addChild('Value', ($this->firstOrder->getPrivacyAddInfo() == 1 ? "YES" : "NO"), $ns);
                    $dlNode->addChild('Type', 'Ziggo', $ns);

                    $dlNode = $privacySettingsNode->addChild('Setting', null, $ns);
                    $dlNode->addChild('Key', 'PrivacyHideNumber', $ns);
                    $dlNode->addChild('Value', ($this->firstOrder->getPrivacyHideNumber() == 1 ? "YES" : "NO"), $ns);
                    $dlNode->addChild('Type', 'Ziggo', $ns);
                }

                $baseNode->insertAfterSelf($customerNode);
                unset($baseNode[0]);
            }
        );
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @return string
     */
    protected function _getSellerChannelName(Mage_Sales_Model_Order $order)
    {
        $channelName = 'Dealer Channel';
        $storeCode = Mage::app()->getWebsite($order->getWebsiteId())->getCode();

        switch($storeCode) {
            case Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE:
                $channelName = 'UBUY-VFW';
                break;
            case Vznl_Agent_Model_Website::WEBSITE_TELESALES_CODE:
            case Vznl_Agent_Model_Website::WEBSITE_BC_WEBSHOP_CODE:
            case Vznl_Agent_Model_Website::WEBSITE_TRADERS_CODE:
                $channelName = 'UBUY-TELESALES';
                break;
            case Vznl_Agent_Model_Website::WEBSITE_BELCOMPANY_CODE:
                $channelName = 'UBUY-BELCO';
                break;
            case Vznl_Agent_Model_Website::WEBSITE_INDIRECT_CODE:
                $channelName = 'UBUY-INDIRECT';
                break;
            case Vznl_Agent_Model_Website::WEBSITE_WEB_SHOP_CODE:
                $channelName = 'UBUY-ONLINE';
                break;
        }

        return $channelName;
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @return array
     */
    protected function getSellerSupplierNodes(Mage_Sales_Model_Order $order)
    {
        return array(
            'ns0:SellerSupplierParty/dt:Channel/dt:Code' => 'DA',
            'ns0:SellerSupplierParty/dt:Channel/dt:Name' => $this->_getSellerChannelName($order),
            'ns0:SellerSupplierParty/dt:Channel/dt:Type' => 'Dealer',
            'ns0:SellerSupplierParty/dt:Dealer/dt:Code' => Mage::helper('agent')->getDaDealerCode(),
            'ns0:SellerSupplierParty/dt:Dealer/dt:Name' => 'BC',
        );
    }

    protected function getDeliveryNodes(Vznl_Superorder_Model_Superorder $superOrder, $esbOrdersWithAddressChanged, $cancelledPackages, $salesOrder, $toBePaidAmount)
    {
        return array(
            'ns0:Deliveries/ns0:Delivery' => function($deliveries, $ns) use ($superOrder, $esbOrdersWithAddressChanged, $cancelledPackages, $salesOrder, $toBePaidAmount) {
                /** @var Omnius_Core_Model_SimpleDOM $baseDelivery */
                $baseDelivery = array_shift($deliveries);
                foreach ($deliveries as $addr) {
                    unset($addr[0]);
                }

                $namespaces = $baseDelivery->getNamespaces(true);
                $ns = isset($namespaces['dt']) ? $namespaces['dt'] : $ns;

                $orders = $superOrder->getOrders(true);
                foreach ($orders->getItems() as $order) {
                    $packages = array();
                    foreach ($order->getAllItems() as $orderItem) {
                        if (!isset($packages[$orderItem->getPackageId()]))
                            $packages[$orderItem->getPackageId()] = $orderItem->getPackageId();
                    }

                    $totalAmountToBePaid = 0;
                    foreach($packages as $packageId) {
                        if ($toBePaidAmount && array_key_exists($packageId, $toBePaidAmount)) {
                            $totalAmountToBePaid += $toBePaidAmount[$packageId];
                        }
                    }
                    $totalAmountToBePaid = round($totalAmountToBePaid,2);

                    /** @var Mage_Sales_Model_Order_Address $address */
                    $deliveryNode = new Omnius_Core_Model_SimpleDOM('<root></root>');
                    $delivery = $deliveryNode->addChild('Delivery', null, 'http://DynaLean/ProcessOrder/V1-0');
                    $delivery->addChild('DeliveryID', $order->getIncrementId(), 'http://DynaLean/ProcessOrder/V1-0');

                    if ($this->_hasChangedDeliveryID && $order->getParentId()) {
                        $previousOrder = Mage::getModel('sales/order')->load($order->getParentId());
                        $delivery->addChild('ChangedDeliveryID', $previousOrder->getIncrementId(), 'http://DynaLean/ProcessOrder/V1-0');
                    }

                    $deliveryAddress = $delivery->addChild('DeliveryAddress', null, 'http://DynaLean/ProcessOrder/V1-0');
                    $address = $order->getShippingAddress();
                    $street = $address->getStreet(1);
                    $houseNr = str_replace("undefined", "", $address->getStreet(2));
                    $addition = $address->getStreet(3);
                    $addressId = $order->getPealDifferentAddressId();
                    $isFixed = $order->hasFixed();

                    if ($isFixed && $addressId) {
                        $deliveryAddress->addChild('AddressID', $addressId, $ns);
                    }
                    if ($isFixed) {
                        $deliveryAddress->addChild('FootPrint', 'ZIGGO', $ns);
                    }
                    $deliveryAddress->addChild('StreetName', $street, $ns);
                    $deliveryAddress->addChild('Housenumber', $houseNr, $ns);
                    $deliveryAddress->addChild('ZipCode', str_replace(' ', '', $address->getData('postcode')), $ns);
                    $deliveryAddress->addChild('City', $address->getData('city'), $ns);
                    $deliveryAddress->addChild('AddressType', $address->getData('address_type') == 'shipping' ? 'CUSTOMER' : 'BILLING', $ns);
                    $deliveryAddress->addChild('Country', $address->getData('country_id'), $ns);
                    $deliveryAddress->addChild('CountryCode', $address->getData('country_id'), $ns);
                    $deliveryAddress->addChild('HousenumberAddition', $addition, $ns);
                    $deliveryAddress->addChild('PObox', null, $ns);
                    $deliveryAddress->addChild('POboxZipCode', null, $ns);
                    $deliveryAddress->addChild('POboxCity', null, $ns);

                    $delivery = $this->addDeliveryChannelNode($order, $delivery);

                    $delivery->addChild('ContractSigningRequired', (!$superOrder->getIsIls() && $order->needsContractSigning()) ? 'true' : 'false', 'http://DynaLean/ProcessOrder/V1-0');
                    if ($salesOrder->getEsbActionRequired() == Vznl_Superorder_Model_Superorder::SO_CHANGE_AFTER ) {
                        $deliveryType = $this->getDeliveryType($order);
                        if ($deliveryType == "Virtual") {
                            $delivery->addChild('PayOnBill', 'true', 'http://DynaLean/ProcessOrder/V1-0');
                        } elseif ($deliveryType == "Store") {
                            $delivery->addChild('PayOnBill', 'false', 'http://DynaLean/ProcessOrder/V1-0');
                        } else {
                            $delivery->addChild('PayOnBill', ($order->getPayment() && $order->getPayment()->getMethodInstance() && $order->getPayment()->getMethodInstance()->getCode() === 'checkmo') ? 'true' : 'false', 'http://DynaLean/ProcessOrder/V1-0');
                        }
                    } else {
                        $delivery->addChild('PayOnBill', ($order->getPayment() && $order->getPayment()->getMethodInstance() && $order->getPayment()->getMethodInstance()->getCode() === 'checkmo') ? 'true' : 'false', 'http://DynaLean/ProcessOrder/V1-0');
                    }
                    $delivery->addChild('PayOnline', ($order->getPayment() && $order->getPayment()->getMethodInstance() && $order->getPayment()->getMethodInstance()->getCode() === 'adyen_hpp') ? 'true' : 'false', 'http://DynaLean/ProcessOrder/V1-0');

                    foreach($packages as $packageId) {
                        $packageReference = $delivery->addChild('PackageReference', null, 'http://DynaLean/ProcessOrder/V1-0');
                        $packageReference->addChild('ReferenceID', $packageId, $ns);
                        $packageReference->addChild('ReferenceType', null, $ns);
                    }

                    $delivery->addChild('DeliveryAddressChanged', ($esbOrdersWithAddressChanged && array_key_exists($order->getParentId(), $esbOrdersWithAddressChanged)) ? 'true' : 'false', 'http://DynaLean/ProcessOrder/V1-0');
                    $delivery->addChild('AmountToBePaid', $totalAmountToBePaid, 'http://DynaLean/ProcessOrder/V1-0');
                    $delivery->addChild('DealerAdapterMode', $order->getDealerAdapterMode(), 'http://DynaLean/ProcessOrder/V1-0');

                    $baseDelivery->insertAfterSelf($delivery);
                }

                unset($baseDelivery[0]);
            },
        );
    }

    protected function getPaymentNodes(
        Vznl_Superorder_Model_Superorder $superOrder,
        $cancelPackages,
        $refundAmount,
        $refundMethod,
        $refundReason,
        $salesOrder,
        $esbOldSuperOrder
    ) {
        $self = $this;

        return array(
            'ns0:Payments/ns0:Payment' => function ($payments, $ns) use (
                $superOrder,
                $self,
                $refundMethod,
                $refundAmount,
                $cancelPackages,
                $refundReason,
                $salesOrder,
                $esbOldSuperOrder
            ) {
                /** @var Omnius_Core_Model_SimpleDOM $baseAddress */
                $basePayment = array_shift($payments);

                foreach ($payments as $pay) {
                    unset($pay[0]);
                }
                $namespaces = $basePayment->getNamespaces(true);
                $ns = isset($namespaces['dt']) ? $namespaces['dt'] : $ns;
                $counter = 1;
                $hasPayments = false;
                // Add online payments
                $orders = $superOrder->getOrders();
                $websiteCode = Mage::app()->getWebsite($salesOrder->getWebsiteId())->getCode();
                /** @var Vznl_Checkout_Model_Sales_Order $order */
                foreach ($orders as $order) {
                    $payment = $order->getPayment();
                    if ($payment && ($order->getPayment()->getMethodInstance()->getCode() === 'adyen_hpp')) {

                        $pspReference = $order->getPayment()->getAdyenPspReference();
                        $orderTotalsData = $order->getTotals();
                        $orderTotal = round($orderTotalsData['total'], 2);
                        $tender = $order->getPayment()->getMethodInstance()->getCode() === 'checkmo' ? self::REKUBUY_PAYMENT_METHOD : self::ADYEN_PAYMENT_METHOD;

                        if ($salesOrder->getEsbActionRequired() === Vznl_Superorder_Model_Superorder::SO_CREATE && $orderTotal > 0) {
                            $paymentData = [
                                'State' => Vznl_Service_Model_Client_EbsOrchestrationClient_PackageItem::PAYMENT_STATE_ADD,
                                'PaymentId' => $counter++,
                                'Amount' => $orderTotal,
                                'Tender' => $tender,
                                'ReferenceId' => $pspReference,
                                'ReferenceType' => self::ORIGINALREFERENCE_REFERENCE_TYPE
                            ];
                            $this->decoratePaymentNode($basePayment, $paymentData, $ns);
                            $hasPayments = true;
                        }
                    }
                }

                $packageIds = [];
                foreach($this->_deviceLineItems as $key1 => $i) {
                    $packageIds[] = $key1;
                }
                foreach($this->_payments as $key2 => $i) {
                    $packageIds[] = $key2;
                }
                $packageIds = array_unique($packageIds);

                foreach($this->_pakketLineItems as $key3 => $i) {
                    $pakketIds[] = $key3;
                }


                //if there is a one time payment for fixed product
                if ($this->firstOrder->hasFixed() && $websiteCode == Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE && $this->firstOrder->getFixedDeliveryMethod() != Vznl_Checkout_Helper_Data::FIXED_METHOD_LSP)
                {
                    foreach ($pakketIds as $packageId) {
                        $ordersItems = isset($this->_pakketLineItems[$packageId]) ? $this->_pakketLineItems[$packageId] : [];
                        $packageTotals = Mage::helper('dyna_configurator')->getActivePackageTotals($packageId, true);
                        $state = ($salesOrder->getEsbAction() == Vznl_Superorder_Model_Superorder::SO_CREATE) ? Vznl_Service_Model_Client_EbsOrchestrationClient_PackageItem::PAYMENT_STATE_ADD : Vznl_Service_Model_Client_EbsOrchestrationClient_PackageItem::PAYMENT_STATE_NOTHING;
                        if ($packageTotals['totalprice'] > 0) {
                            foreach ($ordersItems as $ordersItem) {

                                $paymentData = [
                                    'State' => $state,
                                    'PaymentId' => $counter++,
                                    'Amount' => $packageTotals['totalprice'],
                                    'Tender' => Mage::getStoreConfig('vodafone_service/on_bill_pay/fixed_fee'),
                                    'ReferenceId' => $packageId . '.' . $ordersItem['LineId'],
                                    'ReferenceType' => self::PACKAGELINE_REFERENCE_TYPE
                                ];
                                $this->decoratePaymentNode($basePayment, $paymentData, $ns);
                                $hasPayments = true;
                            }
                        }
                    }
                }
                $flippedPaymentStates = array_flip(                                        [
                                            Vznl_Service_Model_Client_EbsOrchestrationClient_PackageItem::PAYMENT_STATE_CHANGE,
                                            Vznl_Service_Model_Client_EbsOrchestrationClient_PackageItem::PAYMENT_STATE_NOTHING,
                                            Vznl_Service_Model_Client_EbsOrchestrationClient_PackageItem::PAYMENT_STATE_ADD
                                        ]);
                if ($websiteCode != Vznl_Agent_Model_Website::WEBSITE_INDIRECT_CODE) {
                    foreach ($packageIds as $packageId) {
                        $ordersItems = isset($this->_deviceLineItems[$packageId]) ? $this->_deviceLineItems[$packageId] : [];
                        $deviceStates = array_keys($ordersItems);
                        $orderCount = $this->getArrayCount($ordersItems) ;
                        $paymentCount = isset($this->_payments[$packageId]) ? $this->getArrayCount($this->_payments[$packageId]) : 0;
                        if (
                            $orderCount > 1
                            || (
                                $orderCount == 1
                                && (
                                    isset($ordersItems[Vznl_Service_Model_Client_EbsOrchestrationClient_PackageItem::PACKAGE_LINE_STATE_RETURN])
                                    ||
                                    isset($ordersItems[Vznl_Service_Model_Client_EbsOrchestrationClient_PackageItem::PACKAGE_LINE_STATE_CANCEL])
                                )
                            )
                        ) {
                            // Device has changed
                            foreach ($ordersItems as $ordersItem) {
                                if(!$ordersItem['Reference']->isAikido()) {
                                    continue;
                                }

                                $state = $this->processDevicePaymentState($ordersItem['State'], $packageId);
                                if (isset($flippedPaymentStates[$state])
                                    && !$ordersItem['Reference']->shouldSendAsPayment()
                                ) {
                                    continue;
                                }

                                if (
                                    !(
                                        (($ordersItem['Package']->getAction() === 'change') || ($ordersItem['Package']->getAction() === 'cancel'))
                                        && $state == Vznl_Service_Model_Client_EbsOrchestrationClient_PackageItem::PAYMENT_STATE_NOTHING
                                        && $ordersItem['Package']->getChangeType() == 'after'
                                    )
                                ) {
                                    $paymentAmount = $this->processDevicePaymentAmount($ordersItem['State'], $packageId, $ordersItem['Reference']->getDeviceRCCumulated());
                                    $paymentData = [
                                        'State' => $state,
                                        'PaymentId' => $counter++,
                                        'Amount' => $paymentAmount,
                                        'Tender' => $this->mobileFee,
                                        'ReferenceId' => $packageId . '.' . $ordersItem['LineId'],
                                        'ReferenceType' => self::PACKAGELINE_REFERENCE_TYPE
                                    ];
                                    $this->decoratePaymentNode($basePayment, $paymentData, $ns);
                                    $hasPayments = true;
                                }


                                // Display for the cancel after delivery scenario.
                                if ($this->duplicateForNegativeAmount($ordersItem, $packageId)) {
                                    $paymentData = [
                                        'State' => Vznl_Service_Model_Client_EbsOrchestrationClient_PackageItem::PAYMENT_STATE_ADD,
                                        'PaymentId' => $counter++,
                                        'Amount' => -1 * $this->processDevicePaymentAmount($ordersItem['State'],
                                                $packageId, $ordersItem['Reference']->getDeviceRCCumulated()),
                                        'Tender' => $this->mobileFee,
                                        'ReferenceId' => $packageId . '.' . $ordersItem['LineId'],
                                        'ReferenceType' => self::PACKAGELINE_REFERENCE_TYPE
                                    ];
                                    $this->decoratePaymentNode($basePayment, $paymentData, $ns);
                                    $hasPayments = true;
                                }
                            }
                        } elseif (
                            isset($this->_payments[$packageId])
                            && (
                                $paymentCount > 1
                                || (
                                    $paymentCount == 1
                                    && isset($this->_payments[$packageId][Vznl_Service_Model_Client_EbsOrchestrationClient_PackageItem::PACKAGE_LINE_STATE_CANCEL])
                                )
                            )
                        ) { //Only deviceRC is changed
                            $states = array_keys($this->_payments[$packageId]);
                            $zeroToPositive = false;
                            if (isset($this->_payments[$packageId][Vznl_Service_Model_Client_EbsOrchestrationClient_PackageItem::PACKAGE_LINE_STATE_CANCEL])) {
                                $zeroToPositive = $this->_payments[$packageId][Vznl_Service_Model_Client_EbsOrchestrationClient_PackageItem::PACKAGE_LINE_STATE_CANCEL]['Reference']->isDeviceRCZero();
                            }
                            foreach ($this->_payments[$packageId] as $deviceRCPay) {
                                if (
                                    $deviceRCPay['State'] != Vznl_Service_Model_Client_EbsOrchestrationClient_PackageItem::PACKAGE_LINE_STATE_CANCEL
                                    || (
                                        $paymentCount == 1
                                        && $deviceRCPay['Reference']->shouldSendAsPayment()
                                    )
                                ) {
                                    $paymentAmount = $this->processDeviceRCPaymentAmount($deviceRCPay['State'], $deviceRCPay['Reference']->getDeviceRCCumulated());
                                    $state = $this->processDeviceRCPaymentState($deviceRCPay['State'], $states);
                                    if ($state == Vznl_Service_Model_Client_EbsOrchestrationClient_PackageItem::PAYMENT_STATE_CHANGE && $zeroToPositive) {
                                        $state = Vznl_Service_Model_Client_EbsOrchestrationClient_PackageItem::PAYMENT_STATE_ADD;
                                    }
                                    $deviceReference = current($this->_deviceLineItems[$packageId]);
                                    $paymentData = [
                                        'State' => $state,
                                        'PaymentId' => $counter++,
                                        'Amount' => $paymentAmount,
                                        'Tender' => $this->mobileFee,
                                        'ReferenceId' => $packageId . '.' . $deviceReference['LineId'],
                                        'ReferenceType' => self::PACKAGELINE_REFERENCE_TYPE
                                    ];
                                    $this->decoratePaymentNode($basePayment, $paymentData, $ns);
                                    $hasPayments = true;
                                }
                            }
                        } else { // No device or deviceRC changed[Add/Nothing]
                            $paymentNow = isset($this->_payments[$packageId]) ? $this->_payments[$packageId] : [];
                            $deviceRCTest = reset($paymentNow);
                            foreach ($ordersItems as $ordersItem) {
                                $amount = $this->processDevicePaymentAmount($ordersItem['State'], $packageId, $ordersItem['Reference']->getDeviceRCCumulated());
                                if ((!$ordersItem['Reference']->shouldSendAsPayment() || !$ordersItem['Reference']->isAikido())) {
                                    continue;
                                } elseif (
                                    $ordersItem['State'] != Vznl_Service_Model_Client_EbsOrchestrationClient_PackageItem::PACKAGE_LINE_STATE_CHANGE
                                    || (
                                        ($ordersItem['State'] == Vznl_Service_Model_Client_EbsOrchestrationClient_PackageItem::PACKAGE_LINE_STATE_CHANGE)
                                        && isset($deviceRCTest['State'])
                                        && ($deviceRCTest['State'] == Vznl_Service_Model_Client_EbsOrchestrationClient_PackageItem::PACKAGE_LINE_STATE_RESERVE)
                                    )
                                ) {
                                    $paymentData = [
                                        'State' => $this->processDevicePaymentState($ordersItem['State'], $packageId),
                                        'PaymentId' => $counter++,
                                        'Amount' => $amount,
                                        'Tender' => $this->mobileFee,
                                        'ReferenceId' => $packageId . '.' . $ordersItem['LineId'],
                                        'ReferenceType' => self::PACKAGELINE_REFERENCE_TYPE
                                    ];
                                    $this->decoratePaymentNode($basePayment, $paymentData, $ns);
                                    $hasPayments = true;
                                } elseif ($ordersItem['Reference']->getOldLineItem() && $ordersItem['Reference']->getOldLineItem()->getKeepOldPayment()) {
                                    $paymentData = [
                                        'State' => Vznl_Service_Model_Client_EbsOrchestrationClient_PackageItem::PAYMENT_STATE_NOTHING,
                                        'PaymentId' => $counter++,
                                        'Amount' => $amount,
                                        'Tender' => $this->mobileFee,
                                        'ReferenceId' => $packageId . '.' . $ordersItem['LineId'],
                                        'ReferenceType' => self::PACKAGELINE_REFERENCE_TYPE
                                    ];
                                    $this->decoratePaymentNode($basePayment, $paymentData, $ns);
                                    $hasPayments = true;
                                }
                            }
                        }
                    }
                }

                // Add refund payments
                $orderPackages = $superOrder->getPackages();
                $flippedCancelPackages = !empty($cancelPackages) ? array_flip($cancelPackages) : null;
                /** @var Vznl_Package_Model_Package $orderPackage */
                foreach ($orderPackages as $orderPackage) {
                    $orderPackageId = $orderPackage->getPackageId();
                    if (($cancelPackages !== null && !empty($cancelPackages) && !isset($flippedCancelPackages[$orderPackageId]))
                        || $refundMethod !== 'dss' || $refundAmount['payments'][$esbOldSuperOrder ? $orderPackage->getOldPackageId() : $orderPackage->getPackageId()] == 0
                    ) {
                        continue;
                    }
                    $refundAmountTotal = $this->getRefundAmountTotal($refundAmount, $orderPackage->getDeliveryOrder());
                    if ($refundAmountTotal != 0) {
                        $paymentNode = new Omnius_Core_Model_SimpleDOM('<root></root>');
                        $payment = $paymentNode->addChild('Payment', null, 'http://DynaLean/ProcessOrder/V1-0');
                        $payment->addChild('State', Vznl_Service_Model_Client_EbsOrchestrationClient_PackageItem::PAYMENT_STATE_ADD, $ns);
                        $payment->addChild('PaymentId', $counter++, $ns);
                        $payment->addChild('Amount', -1 * (float)$refundAmountTotal, $ns);
                        $payment->addChild('Tender', 'BANKREF', $ns);
                        $basePayment->insertAfterSelf($payment);
                        $hasPayments = true;
                    }
                }

                if (!$hasPayments) {
                    $basePayments = $basePayment->xpath('..');
                    unset($basePayments[0][0]);
                    return;
                }

                unset($basePayment[0]);
            },
        );
    }

    protected function processDeviceRCPaymentState($state, $allStates)
    {
        switch ($state) {
            case Vznl_Service_Model_Client_EbsOrchestrationClient_PackageItem::PACKAGE_LINE_STATE_CANCEL: //'Cancel'
                return Vznl_Service_Model_Client_EbsOrchestrationClient_PackageItem::PAYMENT_STATE_CHANGE; //'Cancel'
            case Vznl_Service_Model_Client_EbsOrchestrationClient_PackageItem::PACKAGE_LINE_STATE_RESERVE: //'Reserve'
                if (in_array(Vznl_Service_Model_Client_EbsOrchestrationClient_PackageItem::PACKAGE_LINE_STATE_RETURN, $allStates)) {
                    return Vznl_Service_Model_Client_EbsOrchestrationClient_PackageItem::PAYMENT_STATE_ADD; //'add'
                } else {
                    return Vznl_Service_Model_Client_EbsOrchestrationClient_PackageItem::PAYMENT_STATE_CHANGE; //'add'
                }
            case Vznl_Service_Model_Client_EbsOrchestrationClient_PackageItem::PACKAGE_LINE_STATE_RETURN: //'Return'
                return Vznl_Service_Model_Client_EbsOrchestrationClient_PackageItem::PAYMENT_STATE_ADD; //'add'
            default:
                return Vznl_Service_Model_Client_EbsOrchestrationClient_PackageItem::PAYMENT_STATE_NOTHING; //'nothing'
        }
    }

    protected function processDeviceRCPaymentAmount($state, $amount)
    {
        switch ($state) {
            case Vznl_Service_Model_Client_EbsOrchestrationClient_PackageItem::PACKAGE_LINE_STATE_RETURN: //'Return'
                return (-1 * $amount);
            case Vznl_Service_Model_Client_EbsOrchestrationClient_PackageItem::PACKAGE_LINE_STATE_CANCEL: //'Return'
                return 0;
            default:
                return $amount;
        }
    }

    protected function processDevicePaymentState($state, $packageId)
    {
        switch ($state) {
            case Vznl_Service_Model_Client_EbsOrchestrationClient_PackageItem::PACKAGE_LINE_STATE_NOTHING://'Nothing'
                return Vznl_Service_Model_Client_EbsOrchestrationClient_PackageItem::PAYMENT_STATE_NOTHING; //'nothing'
            case Vznl_Service_Model_Client_EbsOrchestrationClient_PackageItem::PACKAGE_LINE_STATE_CHANGE: //'Change'
            case Vznl_Service_Model_Client_EbsOrchestrationClient_PackageItem::PACKAGE_LINE_STATE_RESERVE: //'Reserve'
                return Vznl_Service_Model_Client_EbsOrchestrationClient_PackageItem::PAYMENT_STATE_ADD; //'add'
            case Vznl_Service_Model_Client_EbsOrchestrationClient_PackageItem::PACKAGE_LINE_STATE_RETURN: //'Return'
                if (count($this->_deviceLineItems[$packageId]) == 1) {
                    return Vznl_Service_Model_Client_EbsOrchestrationClient_PackageItem::PAYMENT_STATE_NOTHING; //'nothing'
                } else {
                    return Vznl_Service_Model_Client_EbsOrchestrationClient_PackageItem::PAYMENT_STATE_ADD; //'add'
                }
            case Vznl_Service_Model_Client_EbsOrchestrationClient_PackageItem::PACKAGE_LINE_STATE_CANCEL: //'Cancel'
                    return Vznl_Service_Model_Client_EbsOrchestrationClient_PackageItem::PAYMENT_STATE_CHANGE; //'change'
            default:
                return Vznl_Service_Model_Client_EbsOrchestrationClient_PackageItem::PAYMENT_STATE_NOTHING; //'nothing'
        }
    }

    protected function duplicateForNegativeAmount($reference, $packageId)
    {
        if(
            count($this->_deviceLineItems[$packageId]) == 1
            && $reference['State'] == Vznl_Service_Model_Client_EbsOrchestrationClient_PackageItem::PACKAGE_LINE_STATE_RETURN
            && !$reference['Reference']->isDeviceRCZero()
        ) { //'Return'
            return true;
        } else {
            return false;
        }
    }

    protected function processDevicePaymentAmount($state, $packageId, $amount)
    {
        switch($state) {
            case Vznl_Service_Model_Client_EbsOrchestrationClient_PackageItem::PACKAGE_LINE_STATE_RETURN: //'Return'
                if (count($this->_deviceLineItems[$packageId]) == 1) {
                    return $amount;
                } else {
                    return (-1 * $amount);
                }
            case Vznl_Service_Model_Client_EbsOrchestrationClient_PackageItem::PACKAGE_LINE_STATE_CANCEL:
                return 0;
            default:
                return $amount;
        }
    }

    /**
     * @param Omnius_Core_Model_SimpleDOM $basePayment
     * @param array $paymentObject
     * @param string $ns
     */
    protected function decoratePaymentNode($basePayment, array $paymentObject, $ns)
    {
        $paymentNode = new Omnius_Core_Model_SimpleDOM('<root></root>');
        $payment = $paymentNode->addChild('Payment', null, 'http://DynaLean/ProcessOrder/V1-0');
        $payment->addChild('State', $paymentObject['State'], $ns);
        $payment->addChild('PaymentId', $paymentObject['PaymentId'], $ns);
        $payment->addChild('Amount', (float)$paymentObject['Amount'], $ns);
        $payment->addChild('Tender', $paymentObject['Tender'], $ns);
        $references = $payment->addChild('References', null, $ns);
        $reference = $references->addChild('Reference');
        $reference->addChild('ReferenceID', $paymentObject['ReferenceId'], $ns);
        $reference->addChild('ReferenceType', $paymentObject['ReferenceType'], $ns);
        $basePayment->insertAfterSelf($payment);
    }

    protected function addDummyProduct($historyPackageId)
    {
        $tmpPackage = new Vznl_Service_Model_Client_EbsOrchestrationClient_PackageItem();
        $dummyProductSku = self::DUMMY_PRODUCT_SKU;
        $technical_codes = $tmpPackage->getTechnicalCodes($dummyProductSku);

        return array(
            'package_id' => $historyPackageId,
            'data' => serialize(array(
                'state' => 'Reserve',
                'order_line_i_d' => '',
                'product_i_d' => $dummyProductSku,
                'service_i_d' =>
                    array (
                    ),
                'sales_price' => '0.00',
                'quantity' => '1',
                'nature_code' => 'service_item',
                'one_time_price_excluding_v_a_t' => '0.00',
                'one_time_price_including_v_a_t' => '0.00',
                'one_time_price_v_a_t' => '0.00',
                'subscription_price_excluding_v_a_t' => '0',
                'subscription_price_including_v_a_t' => '0',
                'subscription_price_v_a_t' => '0',
                'product_references' => array (
                    'item' => array (
                        'reference_i_d' => array (),
                        'reference_type' => array (),
                    ),
                ),
                'technical_codes' => $technical_codes,
                'line_info' =>
                    array (
                    ),
                'return_reason' =>
                    array (
                    ),
            )),
            'line_id' => '',
            'removed' => 0,
            'product_id' => $dummyProductSku
        );
    }

    /**\
     * @param $order Vznl_Checkout_Model_Sales_Order
     * @param $delivery
     * @param null $comparePrevious
     * @return mixed
     *
     * Add delivery channel node and also check if this has changed from the previous delivery order
     */
    protected function addDeliveryChannelNode($order, $delivery, $comparePrevious = null)
    {
        $deliveryType = $this->getDeliveryType($order,$comparePrevious);

        if ($comparePrevious) {
            if ($comparePrevious != $deliveryType) {
                $delivery->addChild('PreviousDeliveryChannel', $deliveryType, 'http://DynaLean/ProcessOrder/V1-0');
            }
        } else {
            $delivery->addChild('DeliveryChannel', $deliveryType, 'http://DynaLean/ProcessOrder/V1-0');
            if ($order->getEdited() == 0 && $order->getParentId()) {
                /** @var Vznl_Checkout_Model_Sales_Order $parentOrder */
                $parentOrder = Mage::getModel('sales/order')->load($order->getParentId());
                return $this->addDeliveryChannelNode($parentOrder, $delivery, $deliveryType);
            }
        }

        return $delivery;
    }

    /**
     * @param $order Vznl_Checkout_Model_Sales_Order
     * @param null $comparePrevious
     * @return mixed
     */
    public function getDeliveryType($order,$comparePrevious = null)
    {
        $isVirtual = $comparePrevious ? $order->doesNotContainsHardware() : $order->isVirtualDelivery();
        if ($order->hasMobilePackage()) {
            if ($isVirtual) {
                if (in_array($order->getWebsiteCode(), array(Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE, Vznl_Agent_Model_Website::WEBSITE_BELCOMPANY_CODE))) {
                    $deliveryType = 'Store';
                } else {
                    $deliveryType = 'Virtual';
                }
            } elseif ($order->isStoreDelivery()) {
                $deliveryType = 'Store';
            } else {
                $deliveryType = 'Home';
            }
        } else {
            if ($order->getFixedDeliveryMethod() == "SHOP") {
                $deliveryType = 'Store';
            } else {
                $deliveryType = 'Home';
            }
        }
        return $deliveryType;
    }

    public function getApprovePackages(
        $superOrder,
        $esbOldSuperOrder,
        $changedOrders,
        $esbChangedPackageIds,
        $esbOrdersWithAddressChanged,
        $cancelledPackages,
        $agentId
    ) {
        $serviceHelper = $this->getServiceHelper();
        return [
            'n1:ApprovePackagesRequest' =>
                function($approve, $ns) use (
                    $superOrder,
                    $esbOldSuperOrder,
                    $changedOrders,
                    $esbChangedPackageIds,
                    $esbOrdersWithAddressChanged,
                    $cancelledPackages,
                    $agentId,
                    $serviceHelper
                ) {
                    $isFixed = $superOrder->hasFixed();
                    $ns = '';
                    /** @var $approvalClient Vznl_Service_Model_Client_ApprovalClient */
                    $approvalClient = $serviceHelper->getClient('approval');
                    $orderHasOtherPackages = Mage::registry('order_has_other_packages');

                    Mage::unregister('approve_order_already_triggered');
                    if (!$esbOldSuperOrder && !$changedOrders && !$esbChangedPackageIds && !$esbOrdersWithAddressChanged && !$cancelledPackages) {
                        Mage::register('approve_order_already_triggered', false);
                        $hasApprove = false;
                    } elseif (!$esbOldSuperOrder && ($esbChangedPackageIds || $esbOrdersWithAddressChanged || $orderHasOtherPackages)) {
                        $approvePayload = $approvalClient->approveOrder($superOrder, $agentId, true);
                        if ($approvePayload) {
                            $element = array_shift($approve);
                            /** @var Vznl_Service_Model_XmlMapper $xmlMapper */
                            $xmlMapper = Mage::getSingleton('vznl_service/xmlMapper');
                            $xmlMapper::map($approvePayload, $element);
                            Mage::register('approve_order_already_triggered', true);
                            $hasApprove = true;
                        } else {
                            Mage::register('approve_order_already_triggered', false);
                            $hasApprove = false;
                        }
                    } else {
                        Mage::register('approve_order_already_triggered', false);
                        $hasApprove = false;
                    }

                    if (!$hasApprove) {
                        unset($approve[0][0]);
                        return;
                    }
                }
            ];
    }

    private function getServiceHelper()
    {
        if ($this->omniusServiceHelper === null) {
            $this->omniusServiceHelper = Mage::helper('omnius_service');
        }

        return $this->omniusServiceHelper;
    }

    /**
     * Check if the price of an item is changed compared to the price it had before edit
     *
     * @param $sku
     * @param $packageId
     * @return bool
     */
    protected function hasPriceChanged($sku, $packageId)
    {
        return Mage::helper('vznl_service/price')->hasPriceChanged($sku, $packageId);
    }

    /**
     * Make sure all internal attributes are cleared before running next PO
     * @return void
     */
    private function _clearLocalValues():void {
        $this->_hasAddressChange = false;
        $this->_payments = [];
        $this->_deviceLineItems = [];
        $this->_email = false;
        $this->_pakketLineItems = [];
        $this->mobileFee = '';
    }
    
    /**
     * retract deviceRC from delivery amount to be paid
     * @return array
     */
    private function retractDevicercFromDeliveryAmount($params) {
        foreach ($this->_payments as $payment) {
            if (strpos($payment['ReferenceId'], '.') !== false) {
                $packageId = strtok($payment['ReferenceId'], '.');
                $ordersItems = isset($this->_deviceLineItems[$packageId]) ? $this->_deviceLineItems[$packageId] : [];
                foreach ($ordersItems as $ordersItem) {
                    if ($ordersItem['Reference']->shouldSendAsPayment()) {
                        $paymentAmount = $this->processDevicePaymentAmount($ordersItem['State'], $packageId, $ordersItem['Reference']->getDeviceRCCumulated());
                        foreach ($params['Deliveries'] as $delivery) {
                            if($delivery['ReferenceId'] == $packageId) {
                                if ((int)$paymentAmount > 0) {
                                    $delivery['AmountToBePaid'] -= $paymentAmount;
                                }
                            }
                        }
                    }
                }
            }
 
        }
        return $params;
    }   
}
