<?php

/**
 * Class Vznl_Service_Model_Client_PortalInternalClient
 */
class Vznl_Service_Model_Client_PortalInternalClient extends Vznl_Service_Model_Client_Client
{
    /**
     * Example payload:
     * $payload = new Varien_Object(array(
     *     'email_template_id' => 1,
     *     'email_template_row_key' => 1,
     *     'culture_code' => 'nl-NL',
     *     'to' => 'foo@bar.tld',
     *     'name_lists' => array(
     *         array('name1' => 'value1'),
     *         array('name2' => 'value2'),
     *         array('name3' => 'value3'),
     *     ),
     *     'attachments' => array(
     *         'full/path/to/file.ext',
     *     ),
     *     'reports' => array(
     *         array(
     *             'id' => 123,
     *             'report_id' => 123,
     *             'output_type' => 123,
     *             'reports_parameters' => array(
     *                 array('name1' => 'value1'),
     *                 array('name2' => 'value2'),
     *                 array('name3' => 'value3'),
     *             ),
     *             'state' => 'Unchanged',
     *         ),
     *     ),
     * ));
     *
     * @param Varien_Object $payload
     * @return mixed
     */
    public function dispatchEmail(Varien_Object $payload)
    {
        //check if file path exists or To param not set to prevent empty transfer log files
        $attachments = $payload->getAttachments();

        //check if email has indeed attachment to make sure the attachments are not loaded from another source
        if($attachments){
            $connection = Mage::getSingleton('core/resource')->getConnection('read');
            if (!count($connection->fetch(sprintf('SELECT * FROM dyna_communication_job where id = %s and (attachments != "" OR attachments is not null)', $payload->getEntityId())))){
                $payload->setAttachments(null);
                $attachments=array();
            }
        }

        foreach ($attachments as $filePath) {
            if(!is_file($filePath)){
                Mage::throwException(sprintf('Invalid attachment file given (%s).', $filePath));
            }
        }
        if(!$payload->getTo()){
            Mage::throwException('Receiver email ("TO" node) missing. Please provide a receiver email address.');
        }

        /** @var Dyna_Service_Model_DotAccessor $accessor */
        $accessor = Mage::getSingleton('dyna_service/dotAccessor');

        $parameters = $this->prepareParameters($payload, $accessor);

        return $this->SendEmail($parameters);
    }

    /**
     * Reduce cyclomatic complexity of dispatchEmail()
     * @return array
     */
    private function prepareParameters($payload, $accessor)
    {
        return array(
            'Source' => function($node) use ($payload) {
                $node[0][0] = $payload->getSource();
            },
            'EmailTemplateID' => function($node) use ($payload) {
                if ($templateId = $payload->getEmailTemplateId()) {
                    $node[0][0] = $templateId;
                }
            },
            'EmailTemplateRowKey' => function($node) use ($payload) {
                if ($templateKey = $payload->getEmailTemplateRowKey()) {
                    $node[0][0] = $templateKey;
                } else {
                    unset($node[0][0]);
                }
            },
            'CultureCode' => function($node) use ($payload) {
                $culture = $payload->hasCultureCode() ? $payload->getCultureCode() : 'nl-NL'; //default
                $node[0][0] = $culture;
            },
            'TO' => function($node) use ($payload) {
                $node[0][0] = $payload->getTo();
            },
            'CC' => function($node) use ($payload) {
                if ($email = $payload->getCc()) {
                    $node[0][0] = $email;
                }
            },
            'BCC' => function($node) use ($payload) {
                if ($email = Mage::getStoreConfig(Vznl_Service_Helper_Data::PORTAL_CONFIG_PATH . 'bcc_email')) {
                    $node[0][0] = $email;
                }
            },
            'ListOfNameValuePair' => function($lists) use ($payload) {
                /** @var Vznl_Service_Model_SimpleDOM $list */
                $list = array_shift($lists);
                $nameLists = $payload->getNameLists();
                if (is_array($nameLists) && count($nameLists)) {
                    $list->removeNodes('//NameValuePair');
                    $pairCount = 0;
                    foreach ($nameLists as $nameList) {
                        foreach ((is_array($nameList) ? $nameList : array()) as $name => $value) {
                            $pair = $list->addChild('NameValuePair', null);
                            $pair->addChild('Name', $name);
                            //$pair->addChild('Value', $value);
                            $pair->addChild('Value')->addCData($value);
                        }
                        $pairCount++;
                    }
                    //$list->addAttribute('count', $pairCount);
                } else {
                    $attr = $list->attributes();
                    if ( ! (isset($attr['count']))) {
                        $list->removeNodes('//NameValuePair');
                        $list->addAttribute('count', 0);
                    }
                    //unset($list[0][0]);
                }
            },
            'ListOfAsyncEmailAttachment' => function($lists) use ($payload) {
                /** @var Vznl_Service_Model_SimpleDOM $list */
                $list = array_shift($lists);
                $attachments = $payload->getAttachments();
                if (is_array($attachments) && count($attachments)) {
                    /**
                     * @param $filename
                     * @return mixed|null|string
                     */
                    $getMimeType = function($filename) {
                        $mime = null;
                        $info = @finfo_open(FILEINFO_MIME_TYPE);
                        if ($info && $mime = @finfo_file($info, $filename)) {
                            @finfo_close($info);
                        }
                        return $mime ? $mime : 'application/text'; //fallback
                    };
                    $list->removeNodes('//AsyncEmailAttachment');
                    $count = 0;
                    $flippedAllowedToSendContracts = array_flip(array_map('strtolower', Vznl_Communication_Model_Job::ROWKEYS_ALLOWED_TO_SEND_CONTRACTS));
                    $payloadEmailTemplateRowKey = $payload->getEmailTemplateRowKey();
                    foreach ($attachments as $filePath) {
                        $io = new Varien_Io_File();
                        if (!$io->fileExists($filePath)) {
                            Mage::throwException(sprintf('Invalid attachment file given (%s).', $filePath));
                        } else {
                            $io->open(array('path' => $io->dirname($filePath)));
                            $content = $io->streamOpen($filePath, 'r');
                            //if a contract is being attached, make sure the correct template is being used
                            $info = pathinfo($filePath);
                            if(strpos($info['basename'], 'contract_') === 0 && !isset($flippedAllowedToSendContracts[$payloadEmailTemplateRowKey])) {
                                continue;
                            }
                            $attachmentNode = $list->addChild('AsyncEmailAttachment', null);
                            $attachmentNode->addChild('FileName', $info['basename']);
                            $attachmentNode->addChild('Content', base64_encode($content));
                            $attachmentNode->addChild('MimeType', $getMimeType($filePath));
                        }
                        $io->streamClose();
                        $count++;
                    }
                    //$list->addAttribute('count', $count);
                } else {
                    $attr = $list->attributes();
                    if ( !(isset($attr['count']))) {
                        $list->removeNodes('//AsyncEmailAttachment');
                        $list->addAttribute('count', 0);
                    }
                    //unset($list[0][0]);
                }
            },
            'ListOfReportAttachment' => function($lists) use ($payload, $accessor) {
                /** @var Vznl_Service_Model_SimpleDOM $list */
                $list = array_shift($lists);
                if (is_array($reports = $payload->getReports())) {
                    $list->removeNodes('//ReportAttachment');
                    $reportCount = 0;
                    foreach ($reports as $report) {
                        $reportNode = $list->addChild('ReportAttachment', null);
                        $reportNode->addChild('ID', $accessor->getValue($report, 'id'));
                        $reportNode->addChild('ReportID', $accessor->getValue($report, 'report_id'));
                        $reportNode->addChild('OutputType', $accessor->getValue($report, 'output_type'));
                        $reportParameters = $accessor->getValue($report, 'reports_parameters', array());
                        $reportList = $reportNode->addChild('ListOfReportParameter', null);
                        $paramCount = 0;
                        foreach ($reportParameters as $parameter) {
                            foreach ((is_array($parameter) ? $parameter : array()) as $name => $value) {
                                $parameterNode = $reportList->addChild('Parameter', null);
                                $parameterNode->addChild('Name', $name);
                                $parameterNode->addChild('Value', $value);
                                $paramCount++;
                            }
                        }
                        $reportList->addAttribute('count', $paramCount);
                        $reportNode->addChild('ObjectState', $accessor->getValue($report, 'state', 'Unchanged'));
                        $reportCount++;
                    }
                } else {
                    $attr = $list->attributes();
                    if ( ! (isset($attr['count']))) {
                        $list->removeNodes('//ReportAttachment');
                        $list->addAttribute('count', 0);
                    }
                    //unset($list[0][0]);
                }
            },
            'LanguageName' => function($languageNodes) use ($payload) {
                /** @var Vznl_Service_Model_SimpleDOM $language */
                $languageNode = array_shift($languageNodes);
                if ($language = $payload->getLanguage()) {
                    $languageNode[0][0] = $language;
                } else {
                    unset($languageNode[0][0]);
                }
            },
        );
    }

    /**
     * @param Dyna_Customer_Model_Customer $customer
     * @param Vznl_Checkout_Model_Sales_Quote $quote
     * @param Vznl_Agent_Model_Agent $agent
     * @return mixed
     */
    public function riskShoppingCart(Dyna_Customer_Model_Customer $customer, Vznl_Checkout_Model_Sales_Quote $quote, $agent)
    {
        $dataItems = $this->buildDataItems($customer, $quote, $agent);
        $that = $this;
        $params = array(
            'OrderType' => $this->getRiskWorkItemId(),
            'ListOfListRow' => function(array $node) use ($that, $dataItems) {
                /** @var Vznl_Service_Model_SimpleDOM $list */
                $list = $node[0];
                foreach ($dataItems as $dataItem) {
                    $list->insertXML($that->dataItem($dataItem)->asXML());
                }
            },
        );

        return $this->CreateWorkItem($params);
    }

    /**
     * @param Dyna_Customer_Model_Customer $customer
     * @param Vznl_Checkout_Model_Sales_Quote $quote
     * @param $agent
     * @return mixed
     */
    public function storeIltFailed(Dyna_Customer_Model_Customer $customer, Vznl_Checkout_Model_Sales_Quote $quote, $agent){
        $dataItems = $this->buildDataItems($customer, $quote, $agent);
        $that = $this;
        $params = array(
            'OrderType' => $this->getIltWorkItemId(),
            'ListOfListRow' => function(array $node) use ($that, $dataItems) {
                /** @var Vznl_Service_Model_SimpleDOM $list */
                $list = $node[0];
                foreach ($dataItems as $dataItem) {
                    $list->insertXML($that->dataItem($dataItem)->asXML());
                }
            },
        );
        return $this->CreateWorkItem($params);
    }

    /**
     * @param Dyna_Customer_Model_Customer $customer
     * @param Vznl_Checkout_Model_Sales_Quote $quote
     * @param Vznl_Agent_Model_Agent $agent
     * @return mixed
     */
    public function moveMobileAddress(Dyna_Customer_Model_Customer $customer, Vznl_Checkout_Model_Sales_Quote $quote, $agent)
    {
        $dataItems = $this->buildDataItems($customer, $quote, $agent);
        $that = $this;
        $params = array(
            'OrderType' => $this->getMoveMobileWorkItemId(),
            'ListOfListRow' => function(array $node) use ($that, $dataItems) {
                /** @var Vznl_Service_Model_SimpleDOM $list */
                $list = $node[0];
                foreach ($dataItems as $dataItem) {
                    $list->insertXML($that->dataItem($dataItem)->asXML());
                }
            },
        );

        return $this->CreateWorkItem($params);
    }

    /**
     * Builds all data items needed for the request
     * TODO make this configurable
     *
     * @param Dyna_Customer_Model_Customer $customer
     * @param Vznl_Checkout_Model_Sales_Quote $quote
     * @param Vznl_Agent_Model_Agent $agent
     * @return array
     */
    protected function buildDataItems(Dyna_Customer_Model_Customer $customer, Vznl_Checkout_Model_Sales_Quote $quote, $agent)
    {
        /** @var Mage_Customer_Model_Address $address */
        $address = Mage::getModel('customer/address')->load($customer->getDefaultBilling());
        $newServiceAddress = unserialize($quote->getData('service_address'));

        $items = array(
            array(
                'ChannelCode' => 'default',
            ),
            array(
                'Customer_BAN' => $customer->getBan(),
            ),
            array(
                'Customer_City' => trim($address->getData('city')),
            ),
            array(
                'Customer_DateOfBirth' => date('Y-m-d H:i:s', strtotime($customer->getDob())),
            ),
            array(
                'Customer_Email' => $customer->getEmail(),
            ),
            array(
                'Customer_FirstName' => $customer->getFirstname(),
            ),
            array(
                'Customer_LastName' => $customer->getLastname(),
            ),
            array(
                'Customer_Phone' => function() use ($address) {
                    $telNr = $address->getData('telephone');
                    if (strpos($address->getData('telephone'), '31') === 0) {
                        $telNr = str_replace('31', '0', $address->getData('telephone'));
                    }
                    return Mage::helper('vznl_service')->stripPlusChar($telNr);
                },
            ),
            array(
                'Customer_SaleOrderID' => null,
            ),
            array(
                'Customer_Street' => trim($address->getStreet(1)),
            ),
            array(
                'Customer_ZipCode' => trim($address->getData('postcode')),
            ),
            array(
                'Customer_New_Street' => $newServiceAddress['street'],
            ),
            array(
                'Customer_New_Zipcode' => $newServiceAddress['postalcode'],
            ),
            array(
                'Customer_New_HouseNumber' => $newServiceAddress['housenumber'],
            ),
            array(
                'Customer_New_HouseNumber_Addition' => $newServiceAddress['housenumberaddition'],
            ),
            array(
                'DealerCode' => $agent->getDealerCode(),
            ),
            array(
                'DeliveryOrder_1_City' => null, //null
            ),

            array(
                'DeliveryOrder_1_ContractSigningRequired' => null,
            ),
            array(
                'DeliveryOrder_1_DeliverChannel' => 'Store',
            ),
            array(
                'DeliveryOrder_1_DeliveryOrderID' => null,
            ),
            array(
                'DeliveryOrder_1_HouseNumber' => null,
            ),
            array(
                'DeliveryOrder_1_Street' => null, //null
            ),
            array(
                'MMLStatus' => ($customer->getCustomerLabel() === Dyna_Customer_Model_Customer::LABEL_UNIFY) ? Dyna_Customer_Model_Customer::LABEL_UNIFY : Dyna_Customer_Model_Customer::LABEL_GEMINI,
            ),
            array(
                'SaleOrder_ConfirmDateTime' => null,
            ),
            array(
                'SaleOrder_SaleAgent' => $agent->getId(),
            ),
            array(
                'WI_Wishdate_Fixed_Move_Order' => $quote->getFixedDeliveryWishDate(),
            ),
        );
        /**
         * Build data items for packages and package items
         */
        foreach ($quote->getPackages() as $packageId => $package) {
            $items = array_merge($items, array(
                array(
                    sprintf('Package_%s__DealerAdapterOrderID', $packageId) => null,
                ),
                array(
                    sprintf('Package_%s__DeliveryOrderID', $packageId) => null,
                ),
                array(
                    sprintf('Package_%s__MSISDN', $packageId) => null,
                ),
                array(
                    sprintf('Package_%s__PackageID', $packageId) => null,
                ),
                array(
                    sprintf('Package_%s__PackageType', $packageId) => mb_strtoupper($package['type']),
                ),
            ));
            $itemCounter = 0;
            foreach ($package['items'] as $itemId => $item) {
                /** @var Vznl_Checkout_Model_Sales_Quote_Item $item */
                $itemCounter++;
                $items = array_merge($items, array(
                    array(
                        sprintf('Package_%s_OrderLine_%s_ArticleID', $packageId, $itemCounter) => $item->getSku(),
                    ),
                    array(
                        sprintf('Package_%s_OrderLine_%s_OneTimePriceInclVat', $packageId, $itemCounter) => ($item->getPriceInclTax() ? $item->getPriceInclTax() : '0.0000'),
                    ),
                ));
            }
        }
        return $items;
    }

    /**
     * Build a "ListOfDataItem" node
     * example returned node:
     *  <ListOfDataItem>
     *      <Item>ChannelCode</Item>
     *      <Item>default</Item>
     *      <Item>ChannelCode</Item>
     *      <Item>true</Item>
     *      <Item>false</Item>
     *  </ListOfDataItem>
     *
     * @param array $data
     * @return SimpleXMLElement
     */
    protected function dataItem(array $data)
    {
        $root = new Vznl_Service_Model_SimpleDOM('<root/>');
        $dataItem = $root->addChild('ListOfDataItem');
        if (count($data) == 1) {
            $itemKey = key($data);
            $dataItem->addChild('Item', $itemKey);
            $dataItem->addChild('Item', is_callable($data[$itemKey]) ? $data[$itemKey]() : $data[$itemKey]);
            $dataItem->addChild('Item', $itemKey);
            $dataItem->addChild('Item', 'true');
            $dataItem->addChild('Item', 'false');
        } else {
            foreach ($data as $value) {
                $val = is_callable($value) ? $value() : $value;
                $dataItem->addChild('Item', is_bool($val) ? ($val ? 'true' : 'false') : $val);
            }
        }
        return $dataItem;
    }

    public function sendFrontwaveWorkItem($xml)
    {
        $result = $this->xmlToXpath($xml);
        $dataItems = array();
        foreach($result as $k=>$v){
            $dataItems[] = array($k => $v);
        }
        $that = $this;
        $params = array(
            'ListOfHeaderItem' => function(array $node){
                $node[0]->removeSelf();
            },
            'OrderType' => $this->getFrontwaveWorkItemId(),
            'ListOfListRow' => function(array $node) use ($that, $dataItems) {
                /** @var Vznl_Service_Model_SimpleDOM $list */
                $list = $node[0];
                foreach ($dataItems as $dataItem) {
                    $list->insertXML($that->dataItem($dataItem)->asXML());
                }
            },
        );
        return $this->CreateWorkItem($params);
    }

    private function sxiToXpath($sxi, $key = null, &$tmp = null)
    {
        $keys_arr = array();
        //get the keys count array
        $sxi->rewind();
        while ($sxi->valid())
        {
            $sk = $sxi->key();
            if (array_key_exists($sk, $keys_arr)) {
                $keys_arr[$sk]+=1;
            } else {
                $keys_arr[$sk] = 1;
            }
            $sxi->next();
        }
        //create the xpath
        $sxi->rewind();
        while ($sxi->valid())
        {
            $sk = $sxi->key();
            if (!isset($$sk)) {
                $$sk = 1;
            }
            if ($keys_arr[$sk] >= 1) {
                $spk = $sk . '_' . $$sk;
                $keys_arr[$sk] = $keys_arr[$sk] - 1;
                $$sk++;
            } else {
                $spk = $sk;
            }
            $kp = $key ? $key . '_' . $spk : $sxi->getName() . '_' . $spk;
            if ($sxi->hasChildren()) {
                $this->sxiToXpath($sxi->getChildren(), $kp, $tmp);
            } else {
                $tmp[$kp] = strval($sxi->current());
            }
            $sxi->next();
        }
        return $tmp;
    }

    private function xmlToXpath($xml)
    {
        $sxi = new SimpleXmlIterator($xml);
        return $this->sxiToXpath($sxi);
    }

    /**
     * Override method to never use stubs for the email service, until proper backend variable is added
     *
     * @param bool|false $ils
     * @return bool
     */
    public function getUseStubs($ils = false)
    {
        return false;
    }

    /**
     * Retrieve the id for risk work items from backend
     *
     * @return int
     */
    public function getRiskWorkItemId()
    {
        return Mage::getStoreConfig(Vznl_Service_Helper_Data::PORTAL_CONFIG_PATH . 'risk_work_item');
    }

    /**
     * Retrieve the id for ilt workitem from backend
     * @return int
     */
    public function getIltWorkItemId()
    {
        return Mage::getStoreConfig(Vznl_Service_Helper_Data::PORTAL_CONFIG_PATH . 'ilt_work_item');
    }

    /**
     * Retrieve the id for frontwave work items from backend
     *
     * @return int
     */
    public function getFrontwaveWorkItemId()
    {
        return Mage::getStoreConfig(Vznl_Service_Helper_Data::PORTAL_CONFIG_PATH . 'frontwave_work_item');
    }

    /**
     * Retrieve the id for move mobile work items from backend
     *
     * @return int
     */
    public function getMoveMobileWorkItemId()
    {
        return Mage::getStoreConfig(Vznl_Service_Helper_Data::PORTAL_CONFIG_PATH . 'move_mobile_work_item');
    }
}
