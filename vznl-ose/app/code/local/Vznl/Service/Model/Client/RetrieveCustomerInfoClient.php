<?php

/**
 * Class Vznl_Customer_Model_Client_RetrieveCustomerInfo
 */
class Vznl_Service_Model_Client_RetrieveCustomerInfoClient extends Vznl_Service_Model_Client_Client
{

    CONST CONFIG_STUB_PATH = 'customer_info/use_stubs'; // KUD
    CONST WSDL_CONFIG_KEY = 'customer_info/wsdl_customer_info';
    CONST ENDPOINT_CONFIG_KEY = 'customer_info/endpoint_customer_info';

    protected $hasOnlyExcludedCable = false;
    protected $hasExcludedCable = false;
    protected $hasOnlyExcludedMobile = false;
    protected $hasExcludedMobile = false;

    /**
     * @param $data
     * @param int $mode
     * @return Dyna_Customer_Model_Customer
     * @throws Exception
     */
    public function mapCustomer($data, $mode = 1)
    {
        /**
         * must exist both in mode1 and mode2
         */
        if (!isset($data["Customer"][0]["PartyIdentification"]["ID"])) {
            $data["customer_number"] = $data["Customer"]["PartyIdentification"]["ID"];
        } else {
            $data["customer_number"] = ($data["Customer"][0]["PartyIdentification"]["ID"]) ?? null;
        }

        if (!$data["customer_number"]) {
            $message = "No customer id received after executing RetrieveCustomerInfo service call (['Customer'][0]['PartyIdentification']['ID'] on mode " . $mode . ")";
            if ($mode == 1) {
                Mage::logException(new Exception($message));
            } else {
                Mage::log($message, null, "exception.log", true);
                return false;
            }
        }

        /** @var Dyna_Customer_Model_Customer $customerObject */
        $customerObject = Mage::getModel("customer/customer");
        if ($mode == 1) {
            $parsedData = $this->mapMode1($data);
        } else {
            $parsedData = $this->mapMode2($data);
        }
        //OMNVFDE-3315: All further activity for this account should be blocked if there is only a Cable account with status EN/NE
        // or a Mobile one with status T (VFDED1W3S-3657)
        if ($this->hasOnlyExcludedCable) {
            Mage::getSingleton('dyna_customer/session')->setHasOnlyExcludedCable($this->hasOnlyExcludedCable);
            return $customerObject;
        } elseif ($this->hasOnlyExcludedMobile) {
            Mage::getSingleton('dyna_customer/session')->setHasOnlyExcludedMobile($this->hasOnlyExcludedMobile);
            return $customerObject;
        }

        // Save all returned results to session
        Mage::getSingleton('dyna_customer/session')->setServiceCustomers($parsedData);

        $firstContract = null;
        $firstCustomer = [];

        foreach ($this->customersPriority as $priority) {
            if (isset($parsedData[$priority])) {
                $firstContract = array_shift($parsedData[$priority]);
                if (isset($parsedData[$priority]) && (isset($firstContract['is_temporary']) && $firstContract['is_temporary'] == 0)) {
                    $firstCustomer = $firstContract;
                    break;
                }
            }
        }

        if ($firstContract && (isset($firstContract['is_temporary']) && $firstContract['is_temporary'] == 1)) {
            // Verify if response contains only one customer and that customer is a temporary customer
            throw new Exception('Customer cannot be loaded as it is a temporary account: ' . $firstContract['customer_number']);
        }

        // by now $customerObject should be empty, so we'll try to load it from magento
        $customerNumber = isset($firstCustomer['customer_number']) ? $firstCustomer['customer_number'] : null;
        // check if current customer is prospect; if so, dont't retrieve it from magento
        $customerSession = Mage::getSingleton('customer/session');
        $prospectCustomer = $customerSession->getCustomer()->getIsProspect();

        $customerId = ($prospectCustomer || is_null($customerNumber)) ? '' : $customerObject->getIdByCustomerNumber($customerNumber);

        // If there is a customer id load customer from db and overwrite its data
        !$customerId ?: $customerObject = Mage::getModel('customer/customer')->load($customerId);

        $customerObject->addData($firstCustomer);


        if ($mode == 1 && !empty($addresses = $firstCustomer['postal_addresses'])) {
            foreach ($addresses as $addressData) {
                $address = Mage::getModel('customer/address');
                // Address collection will be updated by appending another address
                $address->addData($addressData->getData());
                // Add address to customer
                $customerObject->addSoapAddress($address);
            }
        }

        return $customerObject;
    }

    /**
     * Map customer orders for mobile/cable/dsl from mode2
     *
     * @param $customerData
     * @return array
     */
    public function mapCustomerOrdersData($customerData)
    {
        $dot = $this->getAccessor();
        $data = [];

        if (array_key_exists("PendingOrder", $customerData)) {
            $pendingOrders = $customerData['PendingOrder'];

            // if is a single result make it an array with 1 element
            if ($dot->getValue($pendingOrders, 'LineOfBusiness')) {
                $pendingOrders = [0 => $pendingOrders];
            }

            foreach ($pendingOrders as $order) {
                switch (strtolower($dot->getValue($order, 'LineOfBusiness'))) {
                    case strtolower(self::NON_OSF_ORDER_LINE_OF_BUSINESS_MOBILE):
                    case strtolower(self::NON_OSF_ORDER_LINE_OF_BUSINESS_MOBILE_POSTPAID);
                    case strtolower(self::NON_OSF_ORDER_LINE_OF_BUSINESS_MOBILE_PREPAID):
                        $data['orders'][strtolower(self::NON_OSF_ORDER_LINE_OF_BUSINESS_MOBILE)][] = [
                            'created_date' => date("d.m.Y", strtotime($dot->getValue($order, 'created_date'))),
                            'order_id' => $dot->getValue($order, 'order_id'),
                            'order_status' => $dot->getValue($order, 'order_status'),
                            'order_description' => $dot->getValue($order, 'order_description'),
                            'line_of_business' => $dot->getValue($order, 'LineOfBusiness'),
                            'legacy_customer_id' => $dot->getValue($order, 'LegacyCustomerID'),
                            'subscriber_number' => $dot->getValue($order, 'Detail.SubscriberNumber'),
                            'activity_code' => $dot->getValue($order, 'OrderActivity'),
                            'future_activity_code' => $dot->getValue($order, 'Detail.FutureRequestActivityCode'),
                            'future_reason_code' => $dot->getValue($order, 'Detail.FutureRequestReasonCode'),
                        ];
                        break;
                    case strtolower(self::NON_OSF_ORDER_LINE_OF_BUSINESS_CABLE):
                        $data['orders'][strtolower(self::NON_OSF_ORDER_LINE_OF_BUSINESS_CABLE)][] = [
                            'created_date' => date("d.m.Y", strtotime($dot->getValue($order, 'created_date'))),
                            'order_id' => $dot->getValue($order, 'order_id'),
                            'order_status' => $dot->getValue($order, 'order_status'),
                            'order_description' => $dot->getValue($order, 'order_description'),
                            'line_of_business' => $dot->getValue($order, 'LineOfBusiness'),
                            'legacy_customer_id' => $dot->getValue($order, 'LegacyCustomerID'),
                            'subscriber_number' => $dot->getValue($order, 'Detail.SubscriberNumber'),
                        ];
                        break;
                    case strtolower(strtolower(self::NON_OSF_ORDER_LINE_OF_BUSINESS_DSL)):
                        $data['orders'][strtolower(self::NON_OSF_ORDER_LINE_OF_BUSINESS_DSL)][] = [
                            'created_date' => date("d.m.Y", strtotime($dot->getValue($order, 'created_date'))),
                            'order_id' => $dot->getValue($order, 'order_id'),
                            'order_status' => $dot->getValue($order, 'order_status'),
                            'order_description' => $dot->getValue($order, 'order_description'),
                            'line_of_business' => $dot->getValue($order, 'LineOfBusiness'),
                            'legacy_customer_id' => $dot->getValue($order, 'LegacyCustomerID'),
                            'subscriber_number' => $dot->getValue($order, 'Detail.SubscriberNumber'),
                            'order_activity' => $dot->getValue($order, 'OrderActivity'),
                        ];
                        break;
                }
            }
        }

        return $data;
    }

    protected function _getBANDetails($dot, $kiasCustomers)
    {
    	$return = null;
    	$marketCode = "";
    	foreach ($kiasCustomers as $kiasCustomer) {
    		$customerNumber = $dot->getValue($kiasCustomer, 'customer_number');
    		if (!empty($kiasCustomer['bans'])) {
    			foreach ($kiasCustomer['bans'] as $key => $ban) {
    				if ($ban == $customerNumber) {
    					$marketCode = $kiasCustomer['contract_types'][$key];
    				}
    			}
    		}

    		$return[] = [
    			'BAN' => $customerNumber,
    			'MarketCode' => $marketCode
    		];
    	}

    	return $return;
    }

    /**
     * @param $params
     * @param $mode
     * @return mixed
     */
    public function executeRetrieveCustomerInfo($params = [], $mode = 1)
    {

        $dot = $this->getAccessor();
        $customerSession = Mage::getSingleton('customer/session');
        // In mode 1 the customer data is retrieved, other modes retrieve additional customer info, so customer should already be set on session
        $params['BANDetails'] = null;
        if ($mode == 2) {
            /** @var Dyna_Customer_Model_Customer $customer */
            $customer = $customerSession->getCustomer();
            $params["customerNumber"] = $customer->getCustomerNumber();
            $params["global_id"] = $customer->getCustomerGlobalId();

            $serviceCustomers = Mage::getSingleton('dyna_customer/session')->getServiceCustomers();
            $kiasCustomers = $dot->getValue($serviceCustomers, 'KIAS') ?: [];
            $marketCode = "";
            $params['BANDetails'] = $this->_getBANDetails($dot, $kiasCustomers);
            /*
            foreach ($kiasCustomers as $kiasCustomer) {
                $customerNumber = $dot->getValue($kiasCustomer, 'customer_number');
                if (!empty($kiasCustomer['bans'])) {
                    foreach ($kiasCustomer['bans'] as $key => $ban) {
                        if ($ban == $customerNumber) {
                            $marketCode = $kiasCustomer['contract_types'][$key];
                        }
                    }
                }

                $params['BANDetails'][] = [
                    'BAN' => $customerNumber,
                    'MarketCode' => $marketCode
                ];
            } */
        }
        //$this->mapRequestParams($params, $mode);

        $this->setRequestHeaderInfo($params);
        $params['Mode'] = $mode;

        if ($mode == 2 && ($customerData = $customerSession->getCustomerMode2())) {
            return $customerData;
        }

        $agentHelper = Mage::helper('agent');
        $params['DealerInformation']['DealerChannel'] ='BC';
        $params['DealerInformation']['DealerCode'] = $agentHelper->checkIsWebshop() ?
            Mage::getStoreConfig('vodafone_service/dealer_adapter/dealer_special_code') : $agentHelper->getDaDealerCode();

        $params['PartyCustomer'] = function ($nodes) use ($params) {
            /** @var Omnius_Core_Model_SimpleDOM $basePartyCustomer */
            $basePartyCustomer = array_shift($nodes);
            foreach ($nodes as $line) {
                unset($line[0]);
            }

            if (!empty($params['ban'])) {
                $customerNode = new Omnius_Core_Model_SimpleDOM('<root></root>');
                $customer = $customerNode->addChild('PartyCustomer', null, 'http://www.dynalean.eu/Customer/V1-0');
                $customeraccount = $customer->addChild('CustomerAccount', null, 'http://www.dynalean.eu/Customer/V1-0');
                $customeraccount->addChild('ID', $params['ban'], 'http://DynaLean/CoreComponents/Schemas/DynaLeanTypes/V1-0');
                $customeraccount->addChild('AccountCategory', 'BSL', 'http://DynaLean/CoreComponents/Schemas/DynaLeanTypes/V1-0');
                $customeraccount->addChild('CustomerPassword', $params['customer_password'], 'http://DynaLean/CoreComponents/Schemas/DynaLeanTypes/V1-0');

                if ($params['kvk']) {
                    $validation_criterial = $customer->addChild('ValidationCriterial', null, 'http://www.dynalean.eu/Customer/V1-0');
                    $customer_validation_criterial = $validation_criterial->addChild('Business', null, 'http://www.dynalean.eu/Customer/V1-0');
                    $customer_validation_criterial_party = $customer_validation_criterial->addChild('PartyLegalEntity', null, 'http://DynaLean/CoreComponents/Schemas/DynaLeanTypes/V1-0');
                    $customer_validation_criterial_party->addChild('CompanyId',$params['kvk'],'http://DynaLean/CoreComponents/Schemas/DynaLeanTypes/V1-0');
                } else {
                    if ($params['dob']) {
                        $endUser = $customer->addChild('EndUser', null, 'http://www.dynalean.eu/Customer/V1-0');
                        $customerSubscription = $endUser->addChild('Subscription', null, 'http://www.dynalean.eu/Customer/V1-0');
                        $customerSubscription->addChild('Ctn', $params['phone'], 'http://DynaLean/CoreComponents/Schemas/DynaLeanTypes/V1-0');

                        //DOB
                        $params['dob'] = date("Y-m-d",strtotime($params['dob']));
                        $validation_criterial = $customer->addChild('ValidationCriterial', null, 'http://www.dynalean.eu/Customer/V1-0');
                        $customer_validation_criterial = $validation_criterial->addChild('Individual', null, 'http://www.dynalean.eu/Customer/V1-0');
                        $customer_validation_criterial_party = $customer_validation_criterial->addChild('AliveDuring', null, 'http://DynaLean/CoreComponents/Schemas/DynaLeanTypes/V1-0');
                        $customer_validation_criterial_party->addChild('StartDateTime', $params['dob'], 'http://DynaLean/CoreComponents/Schemas/DynaLeanTypes/V1-0');
                    }
                }
                $basePartyCustomer->insertAfterSelf($customer);
            }

            if (!empty($params['pan'])) {
                $customerNode = new Omnius_Core_Model_SimpleDOM('<root></root>');
                $customer = $customerNode->addChild('PartyCustomer', null, 'http://www.dynalean.eu/Customer/V1-0');
                $customeraccount = $customer->addChild('CustomerAccount', null, 'http://www.dynalean.eu/Customer/V1-0');
                $customeraccount->addChild('ID', $params['pan'], 'http://DynaLean/CoreComponents/Schemas/DynaLeanTypes/V1-0');
                $customeraccount->addChild('AccountCategory', 'PEAL', 'http://DynaLean/CoreComponents/Schemas/DynaLeanTypes/V1-0');
                $basePartyCustomer->insertAfterSelf($customer);
            }

            unset($basePartyCustomer[0]);
        };

        /**
         * call service for $mode (1 or 2)
         */
        $customerData = $this->RetrieveCustomerInfo($params, 'mode' . $mode);
        if ($mode == 2) {
            //$this->mapCustomer($customerData, 2);
            //save mode2 data for feature usage if we need it
            $customerSession->setCustomerMode2($customerData);
        }

        return $customerData;
    }

    /**
     * Map contract type (from mode1) to mode2 for KIAS
     * @param $customerData
     */
    protected function mapContractType(&$customerData)
    {
        $bans = $customerData['bans'] ?? [];
        if (!empty($customerData['contracts'])) {
            foreach ($customerData['contracts'] as &$contract) {
                foreach ($contract['subscriptions'] as &$subscription) {
                    foreach ($bans as $banKey => $ban) {
                        if ($subscription['id'] == $ban) {
                            $subscription['contract_type'] = $customerData['contract_types'][$banKey];
                            $contract['contract_type'] = $customerData['contract_types'][$banKey];
                        }
                    }
                }
            }
        }
    }
}
