<?php

/**
 * Class Vznl_Inlife_Model_ClientFactory
 */
class Vznl_Inlife_Model_ClientFactory extends Vznl_Service_Model_ClientFactory
{
    /**
     * @param string $type
     * @param array $arguments
     * @return mixed
     * @throws InvalidArgumentException
     */
    public function buildClient($type, array $arguments)
    {
        if (!isset($arguments['options'])) {
            throw new InvalidArgumentException('Invalid arguments provided. Missing required "options" key (must be an array).');
        }

        $cacheKey = md5(serialize(array($type, $arguments)));

        if (!isset($this->_registry[$cacheKey])) {

            preg_match('/^(.*)_Model_/', get_class($this), $parts);

            $prefix = $parts[0] . 'Client';

            if (strtolower($type) == 'inlife') {
                $clientClass = 'Vznl_Inlife_Model_Client_InlifeClient';
            } else {
                $clientClass = sprintf('%s_%s', $prefix, str_replace(" ", "", ucwords(strtr($type, "_-", "  "))) . 'Client');
            }

            if (class_exists($clientClass)) {
                $wsdl = isset($arguments['options']['wsdl']) ? trim($arguments['options']['wsdl']) : null;
                $endpoint = isset($arguments['options']['endpoint']) ? trim($arguments['options']['endpoint']) : null;
                try {
                    $this->_registry[$cacheKey] = @new $clientClass($wsdl, $arguments['options']);
                    if (!$this->_registry[$cacheKey]) {
                        Mage::throwException('Failed to create SOAP client. Please review logs.');
                    }
                } catch (Exception $e) {
                    $lastErr = error_get_last();
                    if (false !== strpos($lastErr['message'], 'Couldn\'t load from')) {
                        $message = 'Could not connect to host';
                    } else {
                        $message = 'Failed to create SOAP client. Please review logs.';
                    }
                    Mage::log($e->getMessage() . "\n" . $e->getTraceAsString() . "\n" . "Last error:\n" . json_encode(error_get_last()), null, strtolower('soap_debug_' . $type . '.log'));
                    Mage::throwException($message);
                }

                if ($endpoint && method_exists($this->_registry[$cacheKey], '__setLocation')) {
                    $this->_registry[$cacheKey]->__setLocation($endpoint);
                }
            } else {
                throw new InvalidArgumentException(sprintf('Invalid type argument provided. No client class "%s" found.', $clientClass));
            }
        }

        return $this->_registry[$cacheKey];
    }
}
