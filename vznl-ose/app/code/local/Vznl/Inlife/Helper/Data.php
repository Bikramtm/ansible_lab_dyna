<?php

/**
 * Class Vznl_Inlife_Helper_Data
 */
class Vznl_Inlife_Helper_Data extends Vznl_Service_Helper_Data
{
    const INLIFE_CONFIG_PATH = 'vodafone_service/inlife/';

    /**
     * Returns factory Vznl_Service_Model_ClientFactory
     * @return Vznl_Service_Model_ClientFactory
     */
    protected function _getFactory()
    {
        if (!$this->factory) {
            return $this->factory = new Vznl_Inlife_Model_ClientFactory();
        }

        return $this->factory;
    }

    /**
     * Process addon data received from the InLife service call to usable data
     *
     * @param $rawData
     * @return array
     * @throws Exception
     */
    public function processAddonData($rawData)
    {
        $result = [];

        foreach ($rawData as $addonCatalog) {
            $addon = [];

            $addon['assigned_component_id'] = $addonCatalog['component_identifier']['assigned_id'] ?? null;
            $addon['component_code'] = $addonCatalog['component_identifier']['catalog_code'];
            $addon['parent_assigned_component_id'] = $addonCatalog['component_identifier']['assigned_parent_id'] ?? '';

            if ($addonCatalog['component_identifier']['catalog_code'] == 'VF_Garant_SPSlink') {
                $addon['vf_garant'] = true;
                $insuranceCategory = [];

                if (isset($addonCatalog['component_identifier']['item_attributes']['catalog_id'])) {
                    $addonCatalog['component_identifier']['item_attributes'] = [$addonCatalog['component_identifier']['item_attributes']];
                }

                foreach ($addonCatalog['component_identifier']['item_attributes'] as $itemAttr) {
                    if ($itemAttr['catalog_code'] == 'Commitment_Period') {
                        foreach ($itemAttr['valid_values'] as $value) {
                            $Commitment_Period[$value['code']] = $value['name'];
                        }
                    } elseif ($itemAttr['catalog_code'] == 'Insurance_Service_Variant') {
                        foreach ($itemAttr['valid_values'] as $value) {
                            $Insurance_Service_Variant[$value['code']] = $value['name'];
                        }
                    }
                }
            } elseif ($addonCatalog['component_identifier']['catalog_code'] == 'Additional_Resource') {
                $addon['multisim'] = true;
            } elseif ($addonCatalog['component_identifier']['catalog_code'] == 'Vast_Op_Mobiel') {
                $addon['vom'] = true;
                $vomOptions = [];
                if (isset($addonCatalog['component_identifier']['item_attributes'])) {
                    foreach ($addonCatalog['component_identifier']['item_attributes'] as $iAttr) {
                        if (isset($iAttr['catalog_code'])) {
                            switch ($iAttr['catalog_code']) {
                                case 'MAX_Fax_Lines_Number_Allowed':
                                    $addon['max_fax_numbers'] = isset($iAttr['selected_value']) ? (int)$iAttr['selected_value'] : 0;
                                    break;
                                case 'Total_Vast_Numbers_Quantity':
                                    $addon['max_vom_numbers'] = isset($iAttr['selected_value']) ? (int)$iAttr['selected_value'] : (int)$iAttr['default_value'];
                                    break;
                                case 'Former_provider':
                                    $addon['providers'] = $iAttr['valid_values'];
                                    break;
                            }
                        }
                    }
                }
            }

            if (!isset($addonCatalog['component_identifier']['pricing_elements'])) {
                continue;
            }

            if (isset($addonCatalog['component_identifier']['pricing_elements']['catalog_pricing_id'])) {
                $addonCatalog['component_identifier']['pricing_elements'] = [$addonCatalog['component_identifier']['pricing_elements']];
            }

            $addonOptions = [];
            foreach ($addonCatalog['component_identifier']['pricing_elements'] as $_addon) {
                if (isset($_addon['dynamic_attributes']) && is_array($_addon['dynamic_attributes'])) {
                    $addon['disable'] = array_filter($_addon['dynamic_attributes'], function ($val) {
                        return (
                            (strtolower($val['name']) == 'restrict removal' && isset($val['value']) && (strtolower($val['value']) != 'system')) ||
                            (strtolower($val['name']) == 'is eligible for removal via online' && isset($val['value']) && strtolower($val['value']) == 'n')
                        );
                    });

                    /** @var Vznl_Validators_Helper_Data $dataValidator */
                    $dataValidator = Mage::helper('vznl_validators/data');

                    // Ignore certain addons
                    if ((!$dataValidator->validateAddonCode($_addon['catalog_pricing_code'])) ||
                        (!$dataValidator->validateAddonAttributes($_addon['dynamic_attributes'], 'UI Display Category'))) {
                        continue;
                    }

                    foreach ($_addon['dynamic_attributes'] as $attr) {

                        if ($attr['name'] == 'Offer Type' && $attr['value'] == 'AddOn') {

                            $_addon['ui_display_category'] = array_filter($_addon['dynamic_attributes'], function ($val) {
                                return ($val['name'] == 'UI Display Category' && isset($val['value']));
                            });
                            $_addon['ui_display_category'] = reset($_addon['ui_display_category']);

                            if (isset($_addon['pricing_info']) && isset($_addon['pricing_info']['prices']['rate'])) {
                                $_addon['pricing_info']['prices'] = [$_addon['pricing_info']['prices']];
                            }
                            if (isset($_addon['pricing_info']) && !isset($_addon['pricing_info']['prices']['rate']) && is_array($_addon['pricing_info'])) {
                                foreach ($_addon['pricing_info']['prices'] as $price) {
                                    if (isset($price['dimensions_atr_value'])) {
                                        $nowValue = $price['dimensions_atr_value'];
                                        $nowValueCount = count($nowValue);
                                        if ($nowValueCount == 2 && isset($nowValue[1]) && $nowValue[1]['name'] == 'Insurance category' && isset($addon['vf_garant']) && $addon['vf_garant']) {
                                            $insuranceCategory[$nowValue[1]['value']][$nowValue[0]['value']] = [
                                                'name' => $Insurance_Service_Variant[$nowValue[0]['value']],
                                                'price' => $price['rate']
                                            ];
                                        } elseif ($nowValueCount == 2 && isset($nowValue['name']) && $nowValue['name'] == 'Quantity scale' && isset($addon['vom']) && $addon['vom']) {
                                            $vomOptions[$nowValue['value']] = [
                                                'name' => $nowValue['value'],
                                                'price' => $price['rate']
                                            ];
                                        }
                                    }
                                }

                                if (isset($_addon['pricing_info']['price_type'])) {
                                    $addon['price_type'] = $_addon['pricing_info']['price_type'];
                                }
                            }

                            if (isset($addon['vf_garant']) && $addon['vf_garant']) {
                                $addon['insurance_category'] = $insuranceCategory;
                                ksort($Commitment_Period);
                                $addon['commitment_period'] = $Commitment_Period;
                            }
                            if (isset($addon['vom']) && $addon['vom']) {
                                $addon['vom_options'] = $vomOptions;
                            }

                            unset($_addon['dynamic_attributes']);

                            if (isset($addon['multisim']) && $addon['multisim']) {
                                $addonOptions[] = $_addon;
                            } else {
                                $addon['billing_offer_code'] = $_addon['catalog_pricing_code'];
                                $addon['addon_name'] = $_addon['catalog_pricing_description'];
                                $addon['addon_options'] = [$_addon];
                                $addon['context'] = $addonCatalog['context'];
                                $result[] = $addon;
                            }
                        }
                    }
                }
            }

            if (isset($addon['multisim']) && $addon['multisim'] && !empty($addonOptions)) {
                $addon['billing_offer_code'] = $addonOptions[0]['catalog_pricing_code'];
                $addon['addon_name'] = $addonOptions[0]['catalog_pricing_description'];
                $addon['addon_options'] = $addonOptions;
                $addon['context'] = $addonCatalog['context'];
                $result[] = $addon;
            }
        }

        return $result;
    }

    /**
     * Used to display service errors in a formatted way
     *
     * @param $array
     * @return string
     */
    public function array2ul($array)
    {
        $output = '<ul class="list-unstyled">';
        foreach ($array as $key => $value) {
            $function = is_array($value) ? __FUNCTION__ : 'htmlspecialchars';
            if (!empty($value)) {
                $output .= '<li>' . $this->$function($value) . '</li>';
            }
        }
        return $output . '</ul>';
    }

    /**
     * Check if the current telesales logged in agent can migrate price plans
     *
     * @return bool
     */
    public function canMigratePricePlans()
    {
        return (
            (Mage::helper('agent')->isTelesalesLine())
            && Mage::getSingleton('customer/session')->getAgent(true)->isGranted(['ILS_MIGRATE_PRICE_PLANS'])
        );
    }

    /**
     * @param $text
     * @return string
     */
    public function htmlspecialchars($text)
    {
        return htmlspecialchars($text);
    }

}
