<?php

/**
 * Class Vznl_Inlife_Helper_Request
 */
class Vznl_Inlife_Helper_Request extends Vznl_Service_Helper_Request
{
    /**
     * Vznl_Service_Helper_Request constructor.
     */
    public function __construct()
    {
        $this->_schemaDir = realpath(__DIR__ . $this->_schemaDir);
    }

    /**
     * @param $namespace
     * @param $method
     * @return mixed
     * @throws RuntimeException
     * @throws Zend_Soap_Client_Exception
     */
    protected function _loadSchema($namespace, $method)
    {
        $path = realpath($this->_schemaDir . DIRECTORY_SEPARATOR . $method . '.xml');

        if (!$path) {
            throw new \RuntimeException(sprintf(
                'No schema defined for method "%s" in directory "%s"',
                $method,
                $this->_schemaDir . DIRECTORY_SEPARATOR . $namespace
            ));
        }

        try {
            $xmlContent = trim(Mage::helper("vznl_data/data")->getFileContents($path));
            $schemaXml = $this->convertEncoding($xmlContent);
            $result = new Omnius_Core_Model_SimpleDOM($schemaXml);
            unset($xmlContent);
            unset($schemaXml);
        } catch (\Exception $e) {
            throw new Zend_Soap_Client_Exception(sprintf('Invalid schema file found at "%s"'));
        }

        return $result;
    }
}
