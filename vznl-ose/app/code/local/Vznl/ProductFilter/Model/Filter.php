<?php

/**
 * Class Vznl_ProductFilter_Model_Filter
 */
class Vznl_ProductFilter_Model_Filter extends Mage_Core_Model_Abstract
{
    const STRATEGY_INCLUDE = 'include';
    const STRATEGY_INCLUDE_ALL = 'include_all';
    const STRATEGY_EXCLUDE = 'exclude';

    protected function _construct()
    {
        $this->_init('productfilter/filter');
    }

    public function getAllowedProductIds()
    {
        /**
         * When on backend there's no agent logged in
         */
        if (Mage::app()->getStore()->isAdmin() || Mage::getDesign()->getArea() == 'adminhtml') {
            return Mage::getResourceModel('catalog/product_collection')->getAllIds();
        }

        $groupIds = Mage::helper('agent')->getCurrentDealerGroups();

        /** @var Varien_Data_Collection $filters */
        $filters = Mage::getResourceModel('productfilter/filter_collection')
            ->addFieldToFilter(
                array('group_id', 'group_id'),
                array(
                    array('in' => $groupIds),
                    array('null' => true)
                )
            )
            ->load();

        return array_unique($filters->getColumnValues('product_id'));
    }

    public function addAllowedProductFilters(Varien_Data_Collection_Db $collection)
    {
        /**
         * When on backend there's no agent logged in
         */
        if (Mage::app()->getStore()->isAdmin() || Mage::getDesign()->getArea() == 'adminhtml') {
            $groupIds = Mage::getResourceModel('agent/dealergroup_collection')->getColumnValues('group_id');
        } else {
            $groupIds = Mage::helper('agent')->getCurrentDealerGroups();
        }

        if ( ! count($groupIds)) {
            $collection
                ->addAttributeToFilter('product_visibility_strategy', array('nlike' => '%"strategy":"include"%'));
            return $collection;
            //return $collection->addFieldToFilter('entity_id', -1); //empty collection if no groups were found
        }

        $collection->joinTable(
            array('pv' => 'productfilter/filter'),
            'product_id=entity_id',
            array('group_id' => 'group_id')
        );
        $collection->addFieldToFilter(array(
            array('attribute' => 'group_id', 'null' => true),
            array('attribute' => 'group_id', 'in' => $groupIds)
        ));

        $collection->getSelect()->group('entity_id');

        return $collection;
    }

    public function createRule($productId, $strategy, $groups = array())
    {
        if (!in_array($strategy, array(self::STRATEGY_EXCLUDE, self::STRATEGY_INCLUDE, self::STRATEGY_INCLUDE_ALL))) {
            Mage::throwException(sprintf(
                'Invalid strategy provided. Expected one of %s, got "%s"',
                join(', ', array(self::STRATEGY_EXCLUDE, self::STRATEGY_INCLUDE, self::STRATEGY_INCLUDE_ALL)),
                $strategy
            ));
        }

        /**
         * Use a transaction
         * @var Mage_Core_Model_Resource_Transaction $transaction
         */
        $transaction = Mage::getModel('core/resource_transaction');

        if ( ! ($removedItems = Mage::registry(Omnius_Sandbox_Model_Sandbox::DELETED_PRODUCT_FILTER_REGISTRY))) {
            $removedItems = array();
        }

        if ($strategy === self::STRATEGY_INCLUDE_ALL && !count(Mage::getResourceModel('productfilter/filter_collection')->addFieldToFilter('product_id', $productId)->addFieldToFilter('group_id', array('null' => true)))) {
            Mage::getResourceModel('productfilter/filter_collection')
                ->addFieldToFilter('product_id', $productId)
                ->delete();
            $transaction->addObject(Mage::getModel('productfilter/filter')->addData(array(
                'product_id' => $productId,
                'group_id' => null,
            )));
        } elseif ($strategy === self::STRATEGY_INCLUDE) {
            /** @var Vznl_ProductFilter_Model_Mysql4_Filter_Collection $filterToRemove */
            $filtersToRemove = Mage::getResourceModel('productfilter/filter_collection')
                ->addFieldToFilter('product_id', $productId)
                ->addFieldToFilter(
                    array('group_id', 'group_id'),
                    array(
                        array('nin' => $groups),
                        array('null' => true)
                    )
                );
            foreach ($filtersToRemove->getItems() as $filter) {
                $filter->isDeleted(true);
                $removedItems[$filter->getId()] = $filter->toArray();
                $transaction->addObject($filter);
            }
            /** @var Vznl_ProductFilter_Model_Mysql4_Filter_Collection $existingFilters */
            $existingFilters = Mage::getResourceModel('productfilter/filter_collection')
                ->addFieldToFilter('product_id', $productId);
            foreach ((is_array($groups) ? $groups : array()) as $groupId) {
                if (!$existingFilters->getItemByColumnValue('group_id', $groupId)) {
                    $transaction->addObject(Mage::getModel('productfilter/filter')->addData(array(
                        'product_id' => $productId,
                        'group_id' => $groupId,
                    )));
                }
            }
        } elseif ($strategy === self::STRATEGY_EXCLUDE) {
            /** @var Vznl_ProductFilter_Model_Mysql4_Filter_Collection $existingFilters */
            $existingFilters = Mage::getResourceModel('productfilter/filter_collection')
                ->addFieldToFilter('product_id', $productId);
            /** @var Dyna_Agent_Model_Mysql4_Dealergroup_Collection $dealerGroups */
            $dealerGroups = Mage::getResourceModel('agent/dealergroup_collection');

            foreach ($existingFilters->getItems() as $filter) {
                if (!$filter->getGroupId()) {
                    $filter->isDeleted(true);
                    $removedItems[$filter->getId()] = $filter->toArray();
                    $transaction->addObject($filter);
                }
            }

            foreach ((is_array($groups) ? $groups : array()) as $excludedGroupId) {
                if ($toRemove = $existingFilters->getItemByColumnValue('group_id', $excludedGroupId)) {
                    $toRemove->isDeleted(true);
                    $removedItems[$toRemove->getId()] = $toRemove->toArray();
                    $transaction->addObject($toRemove);
                }
            }

            foreach ($dealerGroups->getItems() as $group) {
                if ((is_array($groups) && !in_array($group->getId(), $groups)) //if the group is not in the excluded array
                    || !is_array($groups) //or there's no excluded array
                ) {
                    if (!$existingFilters->getItemByColumnValue('group_id', $group->getId())) {
                        $transaction->addObject(Mage::getModel('productfilter/filter')->addData(array(
                            'product_id' => $productId,
                            'group_id' => $group->getId(),
                        )));
                    }
                }
            }
        }

        /**
         * Commit the changes
         */
        try {

            Mage::register(Omnius_Sandbox_Model_Sandbox::DELETED_PRODUCT_FILTER_REGISTRY, $removedItems, true);

            $transaction->save();
        } catch (Exception $e) {
            Mage::getSingleton('core/logger')->logException($e);
            throw $e;
        }
    }
}
