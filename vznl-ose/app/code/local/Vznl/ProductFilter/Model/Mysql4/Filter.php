<?php

/**
 * Class Vznl_ProductFilter_Model_Mysql4_Filter
 */
class Vznl_ProductFilter_Model_Mysql4_Filter extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init('productfilter/filter', 'entity_id');
    }
}