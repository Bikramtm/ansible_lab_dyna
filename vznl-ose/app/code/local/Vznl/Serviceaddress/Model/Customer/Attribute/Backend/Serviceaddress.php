<?php
class Vznl_Serviceaddress_Model_Customer_Attribute_Backend_Serviceaddress extends Mage_Eav_Model_Entity_Attribute_Backend_Abstract
{
    public function beforeSave($object)
    {
        $helper = $this->_getHelper();
        $defaultStoreFinder = $helper->getDefaultServiceAddress($object);
        if (is_null($defaultStoreFinder)) {
            $object->setData('default_service_address_id', null);
        }
    }
    public function afterSave($object)
    {
        if ($defaultStoreFinder = $this->_getHelper()->getDefaultServiceAddress($object))
        {
            $addressId = false;
            /**
             * post_index set in customer save action for address
             * this is $_POST array index for address
             */
            foreach ($object->getAddresses() as $address) {
                if ($address->getPostIndex() == $defaultStoreFinder) {
                    $addressId = $address->getId();
                }
            }
            if ($addressId) {
                $object->setDefaultServiceAddressId($addressId);
                $this->getAttribute()->getEntity()
                    ->saveAttribute($object, $this->getAttribute()->getAttributeCode());
            }
        }
    }

    /**
     * @return Vznl_Storefinder_Helper_Address
     */
    protected function _getHelper()
    {
        return Mage::helper('vznl_serviceaddress/address');
    }
}