<?php
class Vznl_Serviceaddress_Helper_Address extends Mage_Core_Helper_Abstract
{
    public function getDefaultServiceAddress(Mage_Customer_Model_Customer $customer)
    {
        return $customer->getData('default_service_address_id');
    }
    public function getDefaultCustomerServiceAddress(Mage_Customer_Model_Customer $customer)
    {
        return $customer->getPrimaryAddress($customer->getData('default_service_address_id'));
    }
}