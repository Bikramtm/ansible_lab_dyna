<?php
/** @var Mage_Customer_Model_Resource_Setup $this */
$attribute = Mage::getSingleton('eav/config')->getAttribute('customer', 'default_service_address_id');
if (!$attribute->getId()) {
    $this->addAttribute(
        'customer',
        'default_service_address_id',
        array(
            'type'               => 'int',
            'label'              => 'Default Service Address',
            'input'              => 'text',
            'backend'            => 'vznl_serviceaddress/customer_attribute_backend_serviceaddress',
            'required'           => false,
            'sort_order'         => 82,
            'visible'            => false,
        )
    );
}
