<?php

/**
 * Class Dyna_Address_Model_Storage
 */
class Vznl_Address_Model_Storage extends Dyna_Address_Model_Storage
{
    /**
     * @param $address
     * @return array
     */
    protected function uniformAddress($address)
    {
        $street = !empty($address['StreetName']) ? $address['StreetName'] : $address['street'];
        $houseNo = !empty($address['BuildingNumber']) ? $address['BuildingNumber'] :  !empty($address['house_number']) ? $address['house_number'] : $address['housenumber'];
        $postCode = !empty($address['PostalZone']) ? $address['PostalZone'] : !empty($address['postcode']) ? $address['postcode'] : $address['postalcode'];
        $city = !empty($address['CityName']) ? $address['CityName'] : $address['city'];
        $addition = !empty($address['HouseNumberAddition']) ? $address['HouseNumberAddition'] : !empty($address['house_addition']) ? $address['house_addition'] : $address['houseaddition'] ?? null;
        $district = !empty($address['District']) ? $address['District'] : (!empty($address['district']) ? $address['district'] : null);
        $postbox = !empty($address['Postbox']) ? $address['Postbox'] : (!empty($address['postbox']) ? $address['postbox'] : null);

        return [
            'id' => !empty($address['ID']) ? $address['ID'] : !empty($address['addressId']) ? $address['addressId'] : null,
            "street" => $street,
            "houseno" => $houseNo,
            "addition"  => $addition,
            "postcode" => $postCode,
            "city" => $city,
            "district" => $district,
            "postbox" => $postbox,
        ];
    }

    /**
     * Return the service address for current order in the following format if asString is set to false
     * array(
     *  "street, houseNo",
     *  "postalCode city"
     * )
     * @return array|string
     */
    public function getServiceAddress($asString = false)
    {
        //If model data is empty, than no serviceability check was performed
        if (empty($this->getData())) {
            if ($asString) {
                return "";
            }
            return [];
        }

        $address = $this->getData('address');
        $address = $this->uniformAddress($address);

        //An array containing street and houseNo in first key, postCode and city in the other key
        $result = [
            $address['street'] . " " . $address['houseno'] . (empty($address['addition']) ? '' : ' ' . $address['addition']),
            $address['postcode'] . " " . $address['city'] . " " . $address['district'],
        ];

        if ($asString) {
            //One string containing entire address
            $result = implode(', ', $result);
        }

        return $result;
    }
}
