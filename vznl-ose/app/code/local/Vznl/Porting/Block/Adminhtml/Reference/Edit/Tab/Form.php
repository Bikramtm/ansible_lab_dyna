<?php

class Vznl_Porting_Block_Adminhtml_Reference_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        /** @var Dyna_Porting_Model_Reference $data */
        $data = Mage::registry('porting_reference');
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $dataFieldset = $form->addFieldset('references', array('legend' => Mage::helper('vznl_porting')->__('Porting reference details')));

        $dataFieldset->addField('code', 'text', array(
            'label' => Mage::helper('vznl_porting')->__('Code'),
            'name' => 'code',
            "class" => "required-entry",
            "required" => true,
            'value' => $data->getCode(),
        ));

        $dataFieldset->addField('description', 'text', array(
            'label' => Mage::helper('vznl_porting')->__('Description'),
            'name' => 'description',
            "class" => "required-entry",
            "required" => true,
            'value' => $data->getDescription(),
        ));

        $dataFieldset->addField('translation_nl', 'text', array(
            'label' => Mage::helper('vznl_porting')->__('Dutch Translation'),
            'name' => 'translation_nl',
            "class" => "required-entry",
            "required" => true,
            'value' => $data->getTranslationNl(),
        ));

        return parent::_prepareForm();
    }
}
