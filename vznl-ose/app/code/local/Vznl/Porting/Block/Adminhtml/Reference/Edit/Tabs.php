<?php

class Vznl_Porting_Block_Adminhtml_Reference_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

    /**
     * Vznl_Porting_Block_Adminhtml_Reference_Edit_Tabs constructor.
     * @throws Varien_Exception
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('porting_reference');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('vznl_porting')->__('Item Information'));
    }

    /**
     * @return Mage_Core_Block_Abstract
     * @throws Exception
     */
    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
            'label' => Mage::helper('vznl_porting')->__('Item Information'),
            'title' => Mage::helper('vznl_porting')->__('Item Information'),
            'content' => $this->getLayout()->createBlock('vznl_porting/adminhtml_reference_edit_tab_form')->toHtml(),
        ));

        return parent::_beforeToHtml();
    }
}
