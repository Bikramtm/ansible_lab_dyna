<?php

/**
 * Class Vznl_Warmer_Model_Warmer_Catalog
 */
class Vznl_Warmer_Model_Warmer_Catalog extends Vznl_Warmer_Model_Warmer_BaseWarmer
{
    /**
     * @param bool $verbose
     * @param bool $includeHeavy
     * @param Dyna_Sandbox_Model_Release $release
     * @return mixed|void
     */
    public function warmCache($verbose = false, $includeHeavy = false, Dyna_Sandbox_Model_Release $release = null)
    {
        $verbose = true;
        if ($verbose) {
            Mage::helper('warmer')->output(join('', array(PHP_EOL, 'Starting catalog warmer with includeHeavy  ' . (int)$includeHeavy .($release ? $release->getId() : 'not set'), PHP_EOL)));
        }

        $appEmulation = Mage::getSingleton('core/app_emulation');
        $storeIds = Mage::app()->getStores();
        foreach ($storeIds as $storeId => $store) { /** @var Mage_Core_Model_Store $store */
            $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($storeId);
            $variants = $this->generateGroupsVariants($store->getId());

            if (empty($variants)) {
                Mage::helper('warmer')->output(sprintf('>   Nothing to do for the %s store', $store->getName()) . PHP_EOL);
                $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
                continue;
            }
            foreach ($this->getCatalogTypes() as $type) {
                foreach ($variants as $variant) {
                    Mage::app()->getRequest()->setParam('dealer_groups', $variant);
                    /** @var Dyna_Configurator_Model_Catalog $model */
                    if ($model = Mage::getModel(sprintf('dyna_configurator/%s', strtolower($type)))) {
                        $time = microtime(true);
                        $model->getAll(null, [], Dyna_Catalog_Model_Product::IDENTIFIER_CONSUMER);
                        if ($verbose) {
                            Mage::helper('warmer')->output(sprintf(
                                '>   %s catalog for store %s, groups %s, customer type %s took: %ss%s',
                                $type,
                                $store->getName(),
                                $variant ? $variant : 'none',
                                'Consumer',
                                number_format(microtime(true) - $time, 3), PHP_EOL
                            ));
                        }
                        $time = microtime(true);
                        $model->getAll();
                        if ($verbose) {
                            Mage::helper('warmer')->output(sprintf(
                                '>   %s catalog for store %s, groups %s, customer type %s took: %ss%s',
                                $type,
                                $store->getName(),
                                $variant ? $variant : 'none',
                                'Business',
                                number_format(microtime(true) - $time, 3), PHP_EOL
                            ));
                        }
                    }
                }
            }
            $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
        }
    }

    /**
     * @param $storeId
     * @return array
     */
    protected function generateGroupsVariants($storeId)
    {
        $dealers = Mage::getResourceModel('agent/dealer_collection')->addFieldToFilter('store_id', $storeId);

        $variants = array();
        foreach ($dealers as $dealer) {
            /** @var Dyna_Agent_Model_Dealer $dealer */
            $groups = $dealer->getDealerGroups()->getAllIds();
            $selectQuery = "SELECT link_id, group_id from dealer_group_link where dealer_id = " . $dealer->getId();
            $groups = array_values(Mage::getSingleton('core/resource')->getConnection('core_read')->fetchPairs($selectQuery));
            if (!empty($groups)) {
                sort($groups);
                $variants[] = implode(',', $groups);
            }
        }

        return array_unique($variants);
    }

    /**
     * @return array
     */
    protected function getCatalogTypes()
    {
        return array(
            Vznl_Catalog_Model_Type::TYPE_MOBILE,
            Vznl_Catalog_Model_Type::TYPE_FIXED,
            Vznl_Catalog_Model_Type::TYPE_HARDWARE,
            Vznl_Catalog_Model_Type::TYPE_PREPAID
        );
    }
}