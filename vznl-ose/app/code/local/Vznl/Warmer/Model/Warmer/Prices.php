<?php

/**
 * Class Vznl_Warmer_Model_Warmer_Prices
 */
class Vznl_Warmer_Model_Warmer_Prices extends Vznl_Warmer_Model_Warmer_BaseWarmer
{
    /**
     * @param bool $verbose
     * @param bool $includeHeavy
     * @param Dyna_Sandbox_Model_Release $release
     * @return mixed|void
     */
    public function warmCache($verbose = false, $includeHeavy = false, Dyna_Sandbox_Model_Release $release = null)
    {
        $warmerStartTime = microtime(true);

        $this->getDynaWarmerHelper()->write('--- Starting prices warmer ---', $verbose);
        if ($this->_debugMode) {
            $this->logDebug('>>> ############################### <<<');
            $this->logDebug('>>> ##  Price cache warmer stats (includeHeavy=' . (int)$includeHeavy . ') and release ' . ($release ? $release->getId() : 'not set') . '  ## <<<');
            $this->logDebug('>>> ############################### <<<');
        }

        $appEmulation = Mage::getSingleton('core/app_emulation');
        $storeIds = Mage::app()->getStores();
        $packageTypes = array_map('strtolower', array_keys(Mage::helper('dyna_package')->getPackageTypes()));
        $processContexts = Mage::helper('dyna_catalog/processContext')->getProcessContextsByCodes();

        foreach ($storeIds as $storeId => $store) {
            /** @var Mage_Core_Model_Store $store */
            $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($storeId);
            $websiteId = $store->getWebsiteId();

            foreach ($packageTypes as $packageType) {
                foreach ($processContexts as $processContextCode => $processContextId) {

                    $time = microtime(true);
                    $this->getPrices(null, $websiteId, $processContextCode, $packageType);
                    $duration = microtime(true) - $time;

                    $outputParams = array();
                    $outputParams['Store name'] = $store->getName();
                    $outputParams['Package type'] = strtoupper($packageType);
                    $outputParams['Sales type'] = (string)$processContextCode;
                    $outputParams['duration'] = number_format($duration, 3);

                    $this->getDynaWarmerHelper()->write(sprintf(
                        '> Store %s - caching %s prices with sales type: %s, took: %ss',
                        ...array_values($outputParams)
                    ), $verbose);

                    if ($this->_debugMode) {
                        $this->collectDebugParams($outputParams);
                    }
                }
            }

            $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
        }
        $this->getDynaWarmerHelper()->write(sprintf(
            '--- Finished prices warmer. Duration: %ss ---',
            number_format(microtime(true) - $warmerStartTime, 3)
        ), $verbose);

        if ($this->_debugMode) {
            $this->logDebugParams();
        }
    }

    /**
     * Returns cusomter type to request parameter association
     *
     * @return array
     */
    protected function getRetentionParameterOptions()
    {
        return array(
            Vznl_Warmer_Helper_Data::IS_RETENTION_PARAM_NO,
            Vznl_Warmer_Helper_Data::IS_RETENTION_PARAM_YES,
        );
    }


    /**
     * Returns cusomter type to request parameter association
     *
     * @return array
     */
    protected function getSalesIds()
    {
        $agentRedSalesIds = array_unique(Mage::getModel('agent/agent')
            ->getCollection()
            ->addFieldToFilter('red_sales_id', array('notnull' => true))
            ->addFieldToSelect('red_sales_id')
            ->getColumnValues('red_sales_id'));

        $dealerCodeRedSalesIds = array_unique(Mage::getModel('dyna_bundles/campaignDealercode')
            ->getCollection()
            ->addFieldToFilter('red_sales_id', array('notnull' => true))
            ->addFieldToSelect('red_sales_id')
            ->getColumnValues('red_sales_id'));

        $salesIds = array_unique(array_merge($agentRedSalesIds, $dealerCodeRedSalesIds));
        foreach ($salesIds as $key => $salesId) {
            $salesIds[$key] = Mage::helper('bundles')->getProvisRedSalesId(array($salesId));
        }
        $salesIds = array_filter($salesIds);

        return $salesIds;
    }

    /**
     * @param null $products
     * @param null $websiteId
     * @param null $salesType
     * @param null $packageType
     * @param null $salesId
     *
     * @return array|mixed
     */
    public function getPrices($products = null, $websiteId = null, $salesType = null, $packageType = null, $salesId = null)
    {
        /** @var Dyna_Cache_Model_Cache $cache */
        $cache = Mage::getSingleton('dyna_cache/cache');

        $productIds = array();
        $cacheKey = 'configurator_get_prices_action_' . $websiteId . '_' . $salesType . '_' . $packageType . "_" . $salesId;
        if ($products) {
            $productIds = array_filter(explode(',', $products), function ($id) {
                return (int)$id;
            });
            //skip levy product if found
            $productIds = Vznl_Catalog_Helper_Data::skipLevyProduct($productIds);
            sort($productIds);
            $cacheKey .= join('_', $productIds);
        }

        if ($cachedPrices = $cache->load($cacheKey)) {
            $prices = unserialize($cachedPrices);
        } else {
            $prices = Mage::helper('dyna_checkout')->getUpdatedPrices($productIds, $websiteId, $salesType, $packageType);
            $cache->save(serialize($prices), $cacheKey, array(Dyna_Cache_Model_Cache::PRODUCT_TAG), $cache->getTtl());
        }

        return $prices;
    }
}
