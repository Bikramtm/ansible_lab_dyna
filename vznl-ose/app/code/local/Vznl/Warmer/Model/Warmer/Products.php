<?php

/**
 * Class Dyna_Warmer_Model_Warmer_Products
 */
class Vznl_Warmer_Model_Warmer_Products extends Vznl_Warmer_Model_Warmer_BaseWarmer
{
    /**
     * @param bool $verbose
     * @param bool $includeHeavy
     * @param Dyna_Sandbox_Model_Release $release
     * @return mixed|void
     */
    public function warmCache($verbose = false, $includeHeavy = false)
    {
        $appEmulation = Mage::getSingleton('core/app_emulation');
        $params = array('axiStoreCode' => null);
        $warmerStartTime = microtime(true);

        $this->getDynaWarmerHelper()->write('--- Starting products warmer with includeHeavy ' . (int)$includeHeavy . '---', $verbose);
        if ($this->_debugMode) {
            $this->logDebug('>>> ################################# <<<');
            $this->logDebug('>>> ## Products cache warmer stats ## <<<');
            $this->logDebug('>>> ################################# <<<');
        }

        $packageCreationTypes = array_column(Mage::getModel('dyna_package/packageCreationTypes')->toOptionArray(), 'label', 'value');
        $processContexts = Mage::helper('dyna_catalog/processContext')->getProcessContextsByCodes();
        $storeIds = Mage::app()->getStores();

        foreach ($storeIds as $storeId => $store) { /** @var Mage_Core_Model_Store $store */
            $this->getDynaWarmerHelper()->write("warming store ".$storeId);
            $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($storeId);
            $websiteId = $store->getWebsiteId();
            $params['websiteId'] = $websiteId;

            foreach ($packageCreationTypes as $packageCreationTypeId => $packageCreationTypeCode) {
                $params['packageCreationTypeId'] = $packageCreationTypeId;
                foreach ($processContexts as $processContextCode => $processContextId) {
                    $params['processContext'] = $processContextCode;
                    foreach ($this->getCustomerTypeParameterOptions() as $customerType => $customerTypeParam) {
                        $params['customerType'] = $customerTypeParam;
                        foreach ($this->getBtwStateParameterOptions() as $btwStateParam) {
                            $params['showPriceWithBtw'] = $btwStateParam;
                            $time = microtime(true);

                            Mage::getModel('dyna_catalog/product')->getCatalogProducts($params);

                            $duration = microtime(true) - $time;

                            $outputParams = array();
                            $outputParams['Store name'] = $store->getName();
                            $outputParams['Package type'] = strtoupper($packageCreationTypeCode);
                            $outputParams['Process context'] = $processContextCode;
                            $outputParams['Customer type'] = $customerType;
                            $outputParams['BTW state'] = $btwStateParam ? 'yes' : 'no';
                            $outputParams['duration'] = number_format($duration, 3);

                            $this->getDynaWarmerHelper()->write(sprintf(
                        '> Store %s - caching %s products with process context: %s, customer type: %s, BTW: %s, took: %ss',
                                ...array_values($outputParams)
                            ), $verbose);

                            if ($this->_debugMode) {
                                $this->collectDebugParams($outputParams);
                            }
                        }
                    }
                }
            }

            $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
        }

        $this->getDynaWarmerHelper()->write(sprintf(
            '--- Finished products warmer. Duration: %ss ---',
            number_format(microtime(true) - $warmerStartTime, 3)
        ), $verbose);

        if ($this->_debugMode) {
            $this->logDebugParams();
        }
    }

    /**
     * Returns cusomter type to request parameter association
     *
     * @return array
     */
    protected function getCustomerTypeParameterOptions()
    {
        return array(
            Dyna_Catalog_Model_Product::IDENTIFIER_CONSUMER => Vznl_Warmer_Helper_Data::PRODUCTS_CUSTOMER_PRIVATE_PARAM,
            Dyna_Catalog_Model_Product::IDENTIFIER_BUSINESS => Vznl_Warmer_Helper_Data::PRODUCTS_CUSTOMER_SOHO_PARAM,
        );
    }

    /**
     * Returns an array of btw state options
     *
     * @return array
     */
    protected function getBtwStateParameterOptions()
    {
        return array(
            Vznl_Warmer_Helper_Data::PRODUCTS_BTW_STATE_TRUE,
            Vznl_Warmer_Helper_Data::PRODUCTS_BTW_STATE_FALSE,
        );
    }
}
