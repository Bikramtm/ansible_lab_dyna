<?php

/**
 * Class Vznl_Warmer_Model_Warmer_BaseWarmer
 */
abstract class Vznl_Warmer_Model_Warmer_BaseWarmer
{
    protected $_cache;

    /** @var Vznl_Warmer_Helper_Data $_warmerHelper */
    protected $_warmerHelper = null;

    /**
     * Int value which indicates what kind of comparison is made for debugging
     * 1 - single comparison    -   DURATION < $_debugComparisonThreshold & DURATION > $_debugComparisonThreshold
     * 2 - interval comparison  -   0*$_debugComparisonThreshold < DURATION < 1*$_debugComparisonThreshold < DURATION < 2*$_debugComparisonThreshold < ...
     * 3 - precise comparison   -   $_debugComparisonThreshold[0] < DURATION < $_debugComparisonThreshold[1] < DURATION < ...
     *
     * @var int $_debugType
     */
    protected $_debugType = 3;
    protected $_debugMode = false;
    protected $_debugComparisonThreshold = 300; // time in miliseconds
    protected $_debugPreciseComparisonThresholds = [0, 5, 10, 100, 1000]; // time in miliseconds
    protected $_debugParams = null;
    protected $_debugLogFilename = 'cache_warmer_debug.log';

    /**
     * @return Vznl_Warmer_Helper_Data
     */
    protected function getDynaWarmerHelper()
    {
        if ($this->_warmerHelper === null) {
            $this->_warmerHelper = Mage::helper('warmer');
        }

        return $this->_warmerHelper;
    }

    /**
     * @param bool $verbose
     * @param bool $includeHeavy
     * @param Dyna_Sandbox_Model_Release $release
     * @return mixed
     */
    abstract public function warmCache($verbose = false, $includeHeavy = false);

    /**
     * @return Dyna_Configurator_Model_Cache
     */
    protected function getCache()
    {
        if ( ! $this->_cache) {
            return $this->_cache = Mage::getSingleton('dyna_configurator/cache');
        }
        return $this->_cache;
    }

    /**
     * Stores debug parameters
     * @param $debugParams
     */
    protected function collectDebugParams($debugParams)
    {
        if (is_null($this->_debugParams)) {
            $this->_debugParams = array();
        }

        if ($this->_debugType == 1) {
            $key = (float)$debugParams['duration']*1000 > $this->_debugComparisonThreshold
                ? $this->_debugComparisonThreshold . ' - ...'
                : '0 - ' . $this->_debugComparisonThreshold;

            $this->_debugParams[(string)$key][] = $debugParams;
        } else if ($this->_debugType == 2) {
            $key = 0;

            while ((float)$debugParams['duration']*1000 > $key) {
                $key += $this->_debugComparisonThreshold;
            }

            $this->_debugParams[($key - $this->_debugComparisonThreshold) . ' - ' . $key][] = $debugParams;
        } else if ($this->_debugType == 3) {
            $intervalFound = false;

            foreach ($this->_debugPreciseComparisonThresholds as $key => $threshold) {
                if ((float)$debugParams['duration']*1000 < $threshold) {
                    $intervalFound = true;
                    $firstPart = $key - 1 >= 0
                        ? $this->_debugPreciseComparisonThresholds[$key - 1]
                        : '...';
                    $this->_debugParams[$firstPart . ' - ' . $threshold][] = $debugParams;
                    break;
                }
            }

            if (!$intervalFound) {
                $lastIndex = count($this->_debugPreciseComparisonThresholds) - 1;
                $this->_debugParams[$this->_debugPreciseComparisonThresholds[$lastIndex] . ' - ...'][] = $debugParams;
            }
        }
    }

    /**
     * Logs debug parameters
     */
    protected function logDebugParams()
    {
        ksort($this->_debugParams, SORT_NUMERIC);

        foreach ($this->_debugParams as $interval => $paramList) {
            $parts = explode(' - ', $interval);
            $parts[0] = is_numeric($parts[0]) ? number_format($parts[0]/1000, 3) : $parts[0];
            $parts[1] = is_numeric($parts[1]) ? number_format($parts[1]/1000, 3) : $parts[1];

            $this->getDynaWarmerHelper()->logToFile(
                '>>> ## Cache generation processes with duration between: ' . $parts[0] . ' - ' . $parts[1] . ' ## <<<',
                $this->_debugLogFilename
            );

            foreach ($paramList as $params) {
                $output = array();
                foreach ($params as $key => $value) {
                    $output[] = $key . ': ' . $value;
                }
                $output = '>>> ' . implode(', ', $output);
                $this->getDynaWarmerHelper()->logToFile($output, $this->_debugLogFilename);
            }
        }
    }

    /**
     * Logs debug message
     */
    public function logDebug($message)
    {
        $this->getDynaWarmerHelper()->logToFile($message, $this->_debugLogFilename);
    }

    /**
     * Returns if the class is an indexer or not
     * 
     * @return bool
     */
    public function isIndexer()
    {
        return false;
    }


    /**
     * Returns an array of package types
     *
     * @return array
     */
    protected function getPackageTypes()
    {
        return array(
            strtolower(Vznl_Catalog_Model_Type::TYPE_PREPAID),
            strtolower(Vznl_Catalog_Model_Type::TYPE_HARDWARE),
            strtolower(Vznl_Catalog_Model_Type::TYPE_MOBILE),
            strtolower(Vznl_Catalog_Model_Type::TYPE_FIXED),

        );
    }

    /**
     * @return array
     */
    public function getDealerGroups()
    {
        $groupIds = array();

        foreach (Mage::getModel('agent/agent')->getCollection() as $agent) {
            $dealer = Mage::getModel('agent/dealer')->load($agent->getDealerCode(), 'vf_dealer_code');
            $groupIds[$agent->getId()] = array();
            foreach ($dealer->getDealerGroups() as $group) {
                array_push($groupIds[$agent->getId()], $group->getId());
            }
        }

        return array_map("unserialize", array_unique(array_map("serialize", $groupIds)));
    }
} 
