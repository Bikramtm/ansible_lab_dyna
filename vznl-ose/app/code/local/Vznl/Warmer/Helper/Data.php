<?php

/**
 * Class Vznl_Warmer_Helper_Data
 */
class Vznl_Warmer_Helper_Data extends Mage_Core_Helper_Abstract
{
    const PRODUCTS_CUSTOMER_SOHO_PARAM = '1';
    const PRODUCTS_CUSTOMER_PRIVATE_PARAM = '0';

    const PRODUCTS_BTW_STATE_TRUE = '1';
    const PRODUCTS_BTW_STATE_FALSE = '0';

    const IS_RETENTION_PARAM_YES = '1';
    const IS_RETENTION_PARAM_NO = '0';

    const DEFAULT_PACKAGE_ID_PARAM = '1';

    const LOG_TO_FILE = false;

    /** @var string $_logFileName */
    protected $_logFileName = "external_release";
    /** @var string $_logFileExtension */
    protected $_logFileExtension= "log";

    /**
     * @param $message
     * @param bool $output
     */
    public function write($message, $output = false)
    {
        if (self::LOG_TO_FILE) {
            $this->logToFile($message);
        }
        if ($output) {
            $this->output($message . PHP_EOL);
        }
    }

    /**
     * @param $message
     */
    public function output($message)
    {
        echo $message;
    }

    /**
     * @param $message
     */
    public function logToFile($message, $filename = null)
    {
        $filename = $filename ?: $this->_logFileName . '.' . $this->_logFileExtension ;
        Mage::log($message, Zend_Log::INFO, $filename);
    }
}
