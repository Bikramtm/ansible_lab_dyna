<?php

use GuzzleHttp\Client;

class Vznl_ValidateSerialNumber_Adapter_Peal_Factory
{
    public static function create()
    {
        $client = new Client();
        $helper = Mage::helper('vznl_validateserialnumber');
        return new Vznl_ValidateSerialNumber_Adapter_Peal_Adapter(
            $client,
            $helper->getEndPoint(),
            $helper->getChannel(),
            $helper->getCountry()
        );
    }
}