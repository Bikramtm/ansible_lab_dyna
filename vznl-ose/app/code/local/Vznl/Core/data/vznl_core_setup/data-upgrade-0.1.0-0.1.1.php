<?php
$path = 'design/package/name';
$write = Mage::getSingleton('core/resource')->getConnection('core_write');
$write->delete("core_config_data", sprintf("path = '%s'", $path));

$write->insert(
    "core_config_data",
    array(
        "scope" => 'default',
        "scope_id" => 0,
        "path" => $path,
        'value'=> 'vznl')
);
