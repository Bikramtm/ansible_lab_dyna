<?php

$write = Mage::getSingleton('core/resource')->getConnection('core_write');
$write->delete("core_config_data", "path = 'omnius_general/menu/links'");

$write->insert(
    "core_config_data",
    array(
        "scope" => 'default',
        "scope_id" => 0,
        "path"=> 'omnius_general/menu/links',
        'value'=> 'FAQ;https://intranet.vodafoneretail.nl/Pages/Dagelijks%20Bericht.aspx
Coach;http://coach/
GSM Arena;https://www.gsmarena.com/
GSM Wijzer;https://www.gsmwijzer.nl/
Kamer van Koophandel;https://dnb-registratiecheck.nl/
Lead aanmaken;https://vodafone.leads.ods2.com/')
);
