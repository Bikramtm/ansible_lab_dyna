<?php
$write = Mage::getSingleton('core/resource')->getConnection('core_write');
$write->delete("core_config_data", sprintf("path = '%s'", 'omnius_general/address_check/show_postal_code'));
$write->delete("core_config_data", sprintf("path = '%s'", 'omnius_general/address_check/is_postal_code_mandatory'));
$write->insert(
    "core_config_data",
    array(
        "scope" => 'default',
        "scope_id" => 0,
        "path" => 'omnius_general/address_check/show_postal_code',
        'value'=> '1')
);
$write->insert(
    "core_config_data",
    array(
        "scope" => 'default',
        "scope_id" => 0,
        "path" => 'omnius_general/address_check/is_postal_code_mandatory',
        'value'=> '1')
);

$write->delete("core_config_data", sprintf("path = '%s'", 'omnius_general/address_check/show_city'));
$write->delete("core_config_data", sprintf("path = '%s'", 'omnius_general/address_check/is_city_mandatory'));
$write->insert(
    "core_config_data",
    array(
        "scope" => 'default',
        "scope_id" => 0,
        "path" => 'omnius_general/address_check/show_city',
        'value'=> '1')
);
$write->insert(
    "core_config_data",
    array(
        "scope" => 'default',
        "scope_id" => 0,
        "path" => 'omnius_general/address_check/is_city_mandatory',
        'value'=> '0')
);

$write->delete("core_config_data", sprintf("path = '%s'", 'omnius_general/address_check/show_street'));
$write->delete("core_config_data", sprintf("path = '%s'", 'omnius_general/address_check/is_street_mandatory'));
$write->insert(
    "core_config_data",
    array(
        "scope" => 'default',
        "scope_id" => 0,
        "path" => 'omnius_general/address_check/show_street',
        'value'=> '1')
);
$write->insert(
    "core_config_data",
    array(
        "scope" => 'default',
        "scope_id" => 0,
        "path" => 'omnius_general/address_check/is_street_mandatory',
        'value'=> '0')
);

$write->delete("core_config_data", sprintf("path = '%s'", 'omnius_general/address_check/show_house_no'));
$write->delete("core_config_data", sprintf("path = '%s'", 'omnius_general/address_check/is_house_no_mandatory'));
$write->insert(
    "core_config_data",
    array(
        "scope" => 'default',
        "scope_id" => 0,
        "path" => 'omnius_general/address_check/show_house_no',
        'value'=> '1')
);
$write->insert(
    "core_config_data",
    array(
        "scope" => 'default',
        "scope_id" => 0,
        "path" => 'omnius_general/address_check/is_house_no_mandatory',
        'value'=> '1')
);

$write->delete("core_config_data", sprintf("path = '%s'", 'omnius_general/address_check/show_house_no_extension'));
$write->delete("core_config_data", sprintf("path = '%s'", 'omnius_general/address_check/is_house_no_extension_mandatory'));
$write->insert(
    "core_config_data",
    array(
        "scope" => 'default',
        "scope_id" => 0,
        "path" => 'omnius_general/address_check/show_house_no_extension',
        'value'=> '1')
);
$write->insert(
    "core_config_data",
    array(
        "scope" => 'default',
        "scope_id" => 0,
        "path" => 'omnius_general/address_check/is_house_no_extension_mandatory',
        'value'=> '0')
);
