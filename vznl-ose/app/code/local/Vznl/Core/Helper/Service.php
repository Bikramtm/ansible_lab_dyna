<?php

/**
 * Class Vznl_Core_Helper_Service
 */
class Vznl_Core_Helper_Service extends Mage_Core_Helper_Abstract
{
    const ENVIRONMENT_MODE_CONFIG_PATH = 'dev/debug/is_dev';
    const ENVIRONMENT_CACHE_TTL = 'dev/debug/cache_ttl';
    const ENVIRONMENT_FRONTEND_CACHE_PREFIX = 'dev/debug/frontend_cache_prefix';

    /**
     * Convert the given xml string to an array
     * @param $xmlString The xml string
     * @param $usePrefix <true> meaning the namespace prefix will be used, <false> meaning they won't.
     * @return array The array with the xml data
     */
    public function xmlStringToArray($xmlString, $usePrefix = true){
        $doc = new DOMDocument();
        $doc->loadXML($xmlString);
        $root = $doc->documentElement;
        $output = $this->domnodeToArray($root, $usePrefix);
        $output['@root'] = $root->tagName;
        return $output;
    }

    /**
     * Converts the given domnode to array
     * @param DOMElement $node The dom node
     * @param <boolean> $usePrefix <true> meaning the namespace prefix will be used, <false> meaning they won't
     * @return array The dom node as array
     */
    protected function domnodeToArray($node, $usePrefix) {
        $output = array();
        switch ($node->nodeType) {
            case XML_CDATA_SECTION_NODE:
            case XML_TEXT_NODE:
                $output = trim($node->textContent);
                break;
            case XML_ELEMENT_NODE:
                $output = $this->domnodeElementToArray($node, $usePrefix, $output);
                break;
        }
        return $output;
    }

    /**
     * Converts the given domnode element to array
     * @param DOMElement $node The dom node element
     * @param boolean $usePrefix <true> meaning the namespace prefix will be used, <false> meaning they won't
     * @param array $output The array to put the data in
     * @return array The dom node element data inside the output array
     */
    protected function domnodeElementToArray($node, $usePrefix, $output)
    {
        $output = $this->domnodeChildsToArray($node, $usePrefix, $output);
        if($node->attributes->length && !is_array($output)) { //Has attributes but isn't an array
            $output = array('@content'=>$output); //Change output into an array.
        }
        if(is_array($output)) {
            if($node->attributes->length) {
                $array = $this->domnodeAttributesToArray($node);
                $output['@attributes'] = $array;
            }
            foreach ($output as $key => $value) {
                if(is_array($value) && isset($value[0]) && $key != '@attributes') {
                    $output[$key] = $value[0];
                }
            }
        }
        return $output;
    }

    /**
     * Converts the childs of the given domnode to array
     * @param DOMElement $node The dom node
     * @param boolean $usePrefix <true> meaning the namespace prefix will be used, <false> meaning they won't
     * @param array $output The array to put the data in
     * @return array The dom node childs inside the output array
     */
    protected function domnodeChildsToArray($node, $usePrefix, $output)
    {
        $size = $node->childNodes->length;
        for ($index = 0; $index < $size; $index++) {
            $child = $node->childNodes->item($index);
            $value = $this->domnodeToArray($child, $usePrefix);
            if(isset($child->tagName)) {
                $key = $usePrefix ? $child->tagName : $child->localName;
                if(!isset($output[$key])) {
                    $output[$key] = array();
                }
                $output[$key][] = $value;
            }
            elseif($value || $value === '0') {
                $output = (string) $value;
            }
        }

        return $output;
    }

    /**
     * Converts the given domnode attributes to array
     * @param $node The dom node
     * @return array The attributes as array
     */
    protected function domnodeAttributesToArray($node)
    {
        $array = array();
        foreach($node->attributes as $attrName => $attrNode) {
            $array[$attrName] = (string) $attrNode->value;
        }
        return $array;
    }

    /**
     * Get the default zend soap client options for the given $namespace
     * @param string $namespace The namespace to get the default values for.
     * @return array The array with options.
     */
    public function getZendSoapClientOptions($namespace = 'communication_email')
    {
        $configPath = sprintf('vodafone_service/%s', $namespace) . '/%s';

        // Set SSL
        $verifyPeer = (Mage::getStoreConfig(sprintf($configPath, 'ssl_peer')) == 1) ? true : false;
        $selfSigned = (Mage::getStoreConfig(sprintf($configPath, 'ssl_self_signed')) == 1) ? true : false;
        $context = stream_context_create([
            'ssl' => [
                'verify_peer' => $verifyPeer,
                'verify_peer_name' => $verifyPeer,
                'allow_self_signed' => $selfSigned
            ]
        ]);

        $options = [
            'stream_context' => $context
        ];

        // Check if needed to set proxy settings
        if (Mage::getStoreConfig(sprintf($configPath, 'proxy_usage')) == 1) {
            $options['proxy_host'] = Mage::getStoreConfig(sprintf($configPath, 'proxy_url')) ? : '';
            $options['proxy_port'] = Mage::getStoreConfig(sprintf($configPath, 'proxy_port')) ? : '';
            $options['proxy_login'] = Mage::getStoreConfig(sprintf($configPath, 'proxy_login')) ? : '';
            $options['proxy_password'] = Mage::getStoreConfig(sprintf($configPath, 'proxy_password')) ? : '';
        }
        return $options;
    }

    public function isDev()
    {
        return Mage::getStoreConfigFlag(self::ENVIRONMENT_MODE_CONFIG_PATH);
    }

}
