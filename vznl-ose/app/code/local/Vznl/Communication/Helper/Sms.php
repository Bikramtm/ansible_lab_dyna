<?php

/**
 * Class Vznl_Communication_Helper_Sms
 */
class Vznl_Communication_Helper_Sms extends Mage_Core_Helper_Abstract
{
    const ORDER_CONFIRMATION_SMS = 'Orderconfirmation SMS-C';

    /**
     * Get the default communication helper
     * @return Vznl_Communication_Helper_Data
     */
    protected function getCommunicationHelper()
    {
        return Mage::helper('communication');
    }

    /**
     * Send the given job as sms
     * @param Vznl_Communication_Model_Job $job
     * @return bool <true> if send successful, <false> if not.
     */
    public function sendSms($job)
    {
        $wsdl = $this->getCommunicationHelper()->getCommunicationConfig('wsdl', 'sms');
        $loggerWriter = new Aleron75_Magemonolog_Model_Logwriter('services/Communication/sms/'.\Ramsey\Uuid\Uuid::uuid4().'.log');
        /** @var Vznl_Communication_Adapter_Sms_SendOutBoundNotificationAdapter $sendOutBoundNotificationAdapter */
        $sendOutBoundNotificationAdapter = Vznl_Communication_Adapter_Sms_SendOutBoundNotificationAdapterFactory::create($wsdl, $loggerWriter->getLogger(), $this->getDefaultOptions());
        return $sendOutBoundNotificationAdapter->send($job);
    }

    /**
     * Create an order confirmation job
     * @param Vznl_Superorder_Model_Superorder $superOrder The super order to use.
     * @return Vznl_Communication_Model_Job The created job
     */
    public function createOrderConfirmationSms($superOrder){
        /** @var Dyna_Checkout_Model_Sales_Order $firstDeliveryOrder */
        $firstDeliveryOrder = $superOrder->getOrders(true)->setPageSize(1, 1)->getLastItem();
        $billingAddress = $firstDeliveryOrder->getBillingAddress();
        $customerCTN = $billingAddress->getTelephone();

        // Set the parameters
        $parameters = [
            'dealer_code' => $superOrder->getAgent()->getDealerCode(),
            'template_parameters' => [
                'UBUY sales order number' => $superOrder->getOrderNumber()
            ]
        ];

        /** @var Vznl_Communication_Model_Job $job */
        $job = Mage::getModel('communication/job');
        $job->setType(Vznl_Communication_Model_Job::TYPE_SMS)
            ->setEmailCode(self::ORDER_CONFIRMATION_SMS)
            ->setCreatedAt(strftime('%Y-%m-%d %H:%M:%S', time()))
            ->setDeadline(strftime('%Y-%m-%d %H:%M:%S', time()))
            ->setParameters($parameters)
            ->setReceiver($customerCTN)
            ->setCultureCode(str_replace('_', '-', Mage::app()->getLocale()->getLocaleCode()))
            ->save();
        return $job;
    }

    /**
     * @return array
     */
    protected function getDefaultOptions()
    {
        $communicationHelper = $this->getCommunicationHelper();
        return array(
            'wsdl' => $communicationHelper->getCommunicationConfig('wsdl', 'sms'),
            'endpoint' => $communicationHelper->getCommunicationConfig('usage_url', 'sms'),
            'timeout' => $communicationHelper->getCommunicationConfig('timeout', 'sms'),
            'username' => $communicationHelper->getCommunicationConfig('username', 'sms'),
            'password' => $communicationHelper->getCommunicationConfig('password', 'sms'),
            'enduser' => $communicationHelper->getCommunicationConfig('enduser', 'sms'),
        );
    }
}
