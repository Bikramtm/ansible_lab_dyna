<?php

/**
 * Class Vznl_Communication_Helper_Email
 */
class Vznl_Communication_Helper_Email extends Mage_Core_Helper_Data
{
    const VODAFONE_EMAIL_PROCESS_CONFIG_PATH = 'email_configuration/email_configuration/%s';
    const SERVICE_ADDRESS_FOOTPRINT_ZIGGO = 'ZIGGO';
    const SERVICE_ADDRESS_FOOTPRINT_UPC = 'UPC';

    /**
     * Get the email payload when the order is ready for pickup in store
     *
     * @param Dyna_Checkout_Model_Sales_Order $deliveryOrder
     * @return array
     */
    public function getOrderReadyForPickupPayload(Vznl_Checkout_Model_Sales_Order $deliveryOrder)
    {
        /** @var Vznl_Superorder_Model_Superorder $superorder */
        $superorder = Mage::getModel('superorder/superorder')->load($deliveryOrder->getSuperorderId());
        /** @var Dyna_Customer_Model_Customer_Customer $customer */
        $customer = Mage::getModel('customer/customer')->load($superorder->getCustomerId());

        // Set template
        if ($customer->getIsBusiness()) {
            $emailCode = Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_READY4PICKUP_BUSINESS;
        } else {
            $emailCode = Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_READY4PICKUP;
        }

        $dealerId = Mage::helper('agent')->getDaDealerCode($superorder->getAgent());
        $dealer = Mage::getModel('agent/dealer')->load($dealerId, 'vf_dealer_code');
        $primaryBrand = $dealer->getPrimaryBrand();
        $from = $dealer->getSenderDomain();

        // Set shippingAddress
        $shippingAddress = $deliveryOrder->getShippingAddress();
        $pickupDetails = trim($shippingAddress->getCity().', '.implode(' ', $shippingAddress->getStreet()));

        return [
            'email_code' => $emailCode,
            'created_at' => strftime('%Y-%m-%d %H:%M:%S', time()),//current time
            'deadline' => strftime('%Y-%m-%d %H:%M:%S', time()),//current time, if needs to be sent in the future, change here
            'parameters' => [
                ['Contact_Gender' => Mage::helper('vznl_checkout')->__($customer->getSalutation())],
                ['Contact_LastName' => $customer->getCompleteLastName()],
                ['SSFE_SaleOrderNumber' => $superorder->getOrderNumber()],
                ['SaleOrderNumber' => $superorder->getOrderNumber()],
                ['PickupDetails' => $pickupDetails],
                ['Dealer' => $this->getCommunicationDealerData($superorder)],
            ],
            'receiver' => $customer->getCorrespondanceEmail($deliveryOrder),
            'from' => $from,
            'culture_code' => str_replace('_', '-', Mage::app()->getLocale()->getLocaleCode()),
        ];
    }

    /**
     * Get the email payload to send to the shop
     *
     * @param Dyna_Checkout_Model_Sales_Order $deliveryOrder
     * @return array|bool The payload or <false> if the store had no email address
     */
    public function getOrderReadyForPickupStorePayload(Vznl_Checkout_Model_Sales_Order $deliveryOrder, $isCancelled = false)
    {
        /** @var Vznl_Superorder_Model_Superorder $superOrder */
        $superOrder = Mage::getModel('superorder/superorder')->load($deliveryOrder->getSuperorderId());
        /** @var Dyna_Customer_Model_Customer_Customer $customer */
        $customer = Mage::getModel('customer/customer')->load($superOrder->getCustomerId());

        // Set email code
        if (!$isCancelled) {
            $emailCode = Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_READY4PICKUP_NOTICE_STORE;
        }else{
            $emailCode = Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_READY4PICKUP_CANCELLATION_STORE;
        }

        // Get delivery address
        $shippingAddress = $deliveryOrder->getShippingAddress();
        $deliveryAddress = trim(implode(' ', $shippingAddress->getStreet()) . ', ' . $shippingAddress->getPostcode() . ', ' . $shippingAddress->getCity());

        // Get the email address
        $dealerId = $shippingAddress->getDeliveryStoreId();
        $deliverStore = Mage::getModel('agent/dealer')->load($dealerId);
        $emailAddress = $deliverStore && $deliverStore->getId() ? $deliverStore->getEmail() : false;
        if(!$emailAddress){
            return false;
        }

        $dealerCode = Mage::helper('agent')->getDaDealerCode($superOrder->getAgent());
        $dealer = Mage::getModel('agent/dealer')->load($dealerCode, 'vf_dealer_code');
        $primaryBrand = $dealer->getPrimaryBrand();
        $from = $dealer->getSenderDomain();
        // Get the order date
        $saleOrderDate = Mage::app()->getLocale()->date(Mage::getModel('core/date')->timestamp(strtotime($superOrder->getCreatedAt())), null, null, false)->toString('dd-MM-y');
        if($deliveryOrder->getParentId()){
            $saleOrderDate .=  ' (' . $this->__('modified on') . ' ' . Mage::app()->getLocale()->date(Mage::getModel('core/date')->timestamp(null), null, null, false)->toString('dd-MM-y') . ')';
        }

        // Get billing address
        $billingAddress = $deliveryOrder->getBillingAddress();
        $billingAddress->setData('street', $billingAddress->getStreet());
        $invoiceAddress = Mage::helper("dyna_checkout")->formatAddressAsHtml($billingAddress, true);

        // Get the delivery method
        $deliveryMethod = Mage::helper('dyna_checkout')->__('Pick up in store');

        return [
            'email_code' => $emailCode,
            'created_at' => strftime('%Y-%m-%d %H:%M:%S', time()),//current time
            'deadline' => strftime('%Y-%m-%d %H:%M:%S', time()),//current time, if needs to be sent in the future, change here
            'parameters' => [
                ['Contact_Gender' => Mage::helper('vznl_checkout')->__($customer->getSalutation())],
                ['Contact_LastName' => $customer->getCompleteLastName()],
                ['SSFE_SaleOrderNumber' => $superOrder->getOrderNumber()],
                ['SaleorderNumber' => $superOrder->getOrderNumber()],
                ['SaleorderDate' => $saleOrderDate],
                ['InvoiceAddress' => $invoiceAddress],
                ['DeliveryMethod' => $deliveryMethod],
                ['DeliveryAddress' => $deliveryAddress],
                ['Dealer' => $this->getCommunicationDealerData($superOrder)],
                ['ShoppingCart' => Mage::helper('vznl_checkout/email')->sendOrderConfirmationEmail($superOrder->getId())]
            ],
            'receiver' => $emailAddress,
            'from' => $from,
            'culture_code' => str_replace('_', '-', Mage::app()->getLocale()->getLocaleCode()),
        ];
    }

    /**
     * @param Vznl_Superorder_Model_Superorder $superorder
     * @return array
     *
     * Get the data for the order confirmation email
     */
    public function getOrderConfirmationPayload(Vznl_Superorder_Model_Superorder $superorder)
    {
        $addresses = array('delivery' => [], 'invoice' => []);
        $isOrderEdit = false;
        $hasNumberPorting = false;
        $isSimOnly = true;
        $orderCollection = $superorder->getOrders(true);
        foreach ($orderCollection as $order) {
            $shippingAddress = $order->getShippingAddress();
            $shippingAddress->setData('street', $shippingAddress->getStreet());
            $addresses['delivery'][] = Mage::helper("dyna_checkout")->formatAddressAsHtml($shippingAddress, true);

            $billingAddress = $order->getBillingAddress();
            $billingAddress->setData('street', $billingAddress->getStreet());
            $addresses['invoice'][] = Mage::helper("dyna_checkout")->formatAddressAsHtml($billingAddress, true);

            if ($order->getParentId()) {
                $isOrderEdit = true;
            }
            $isOtherStoreDelivery = $order->isOtherStoreDelivery() ? true : false ;
            $orderPackages = $order->getPackageIds();
            $packages = $order->getPackages(array('in' =>$orderPackages));
            foreach ($packages as $package) {
                if ($package->isNumberPorting()) {
                    $hasNumberPorting = true;
                    break;
                }
                if (!$package->isSimOnly()) {
                    // If one of the packages no is sim only, the whole superorder can be handled as none sim only.
                    $isSimOnly = false;
                }
            }
        }

        $addresses['delivery'] = array_unique($addresses['delivery'], SORT_REGULAR);
        $addresses['invoice'] = array_unique($addresses['invoice'], SORT_REGULAR);

        //make sure the superorder still exists
        if (!$superorder->getId()) {
            return array();
        }

        /** @var Dyna_Customer_Model_Customer_Customer $customer */
        $customer = Mage::getModel('customer/customer')->load($superorder->getCustomerId());

        // Order Date
        $saleOrderDate = Mage::app()->getLocale()->date(Mage::getModel('core/date')->timestamp(strtotime($superorder->getCreatedAt())), null, null, false)->toString('dd-MM-y');
        if($isOrderEdit){
            $saleOrderDate .=  ' (' . $this->__('modified on') . ' ' . Mage::app()->getLocale()->date(Mage::getModel('core/date')->timestamp(null), null, null, false)->toString('dd-MM-y') . ')';
        }

        $dealerId = Mage::helper('agent')->getDaDealerCode($superorder->getAgent());
        $dealer = Mage::getModel('agent/dealer')->load($dealerId, 'vf_dealer_code');
        $primaryBrand = $dealer->getPrimaryBrand();
        $from = $dealer->getSenderDomain();
        $isMobile = $superorder->hasMobilePackage();

        $videoLink = $superorder->hasRetention() ? Mage::getStoreConfig('email_configuration/email_configuration/order_confirmation_video_link_retention') : Mage::getStoreConfig('email_configuration/email_configuration/order_confirmation_video_link_acquisition');

        $changeBefore = $superorder->getLastChangeType() == Vznl_Superorder_Model_Superorder::SO_CHANGE_BEFORE;
        $oneTimeCharge = $superorder->getCurrentOnTimeCharge();
        $paymentMethod = $superorder->getAllPaymentMethods();
        $deliveryMethod = $superorder->getAllDeliveryMethods();
        $salesOrderAction = $superorder->getSalesOrderActionType();

        $isMobileMoveDealer = Mage::helper('agent')->getDaDealerCode($superorder->getAgent()) == Vznl_Agent_Model_Dealer::MOBILE_MOVE_DEALERCODE;

        if ($customer->getIsBusiness()) {
            if(Mage::helper('agent')->checkIsTelesalesOrWebshop($superorder->getWebsiteId()) && $superorder->hasStoreDelivery() || $superorder->hasOtherStoreDelivery()){
                $emailCode = Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_ORDER_CONFIRM_STOREDELIVERY_BUSINESS;
            }
            else if ($isOrderEdit) {
                if ($paymentMethod === 'cashondelivery') {
                    if ($changeBefore) {
                        // If the one-time charge is bigger than 0, the customer needs to pay, which is negative for the customer
                        if ($oneTimeCharge > 0) {
                            $emailCode = Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_ORDER_CONFIRM_BUSINESS_CHANGEBEFORE_COD_NEGATIVE;
                        } elseif ($oneTimeCharge < 0) {
                            $emailCode = Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_ORDER_CONFIRM_BUSINESS_CHANGEBEFORE_COD_POSITIVE;
                        } else {
                            $emailCode = Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_ORDER_CONFIRM_BUSINESS_CHANGEBEFORE_COD;
                        }
                    } else {
                        // If one time charge bigger then 0, customer receives, so positive for the customer
                        $emailCode = $oneTimeCharge > 0 ? Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_ORDER_CONFIRM_BUSINESS_CHANGEAFTER_COD_POSITIVE : Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_ORDER_CONFIRM_BUSINESS_CHANGEAFTER_COD_NEGATIVE;
                    }
                } else {
                    if ($changeBefore) {
                        // If the one-time charge is bigger than 0, the customer needs to pay, which is negative for the customer
                        if ($oneTimeCharge > 0) {
                            $emailCode = Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_ORDER_CONFIRM_BUSINESS_CHANGEBEFORE_PAYONBILL_NEGATIVE;
                        } elseif ($oneTimeCharge < 0) {
                            $emailCode = Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_ORDER_CONFIRM_BUSINESS_CHANGEBEFORE_PAYONBILL_POSITIVE;
                        } else {
                            $emailCode = Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_ORDER_CONFIRM_BUSINESS_CHANGEBEFORE_PAYONBILL;
                        }
                    } else {
                        // If one time charge bigger then 0, customer receives, so positive for the customer
                        $emailCode = $oneTimeCharge > 0 ? Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_ORDER_CONFIRM_BUSINESS_CHANGEAFTER_PAYONBILL_POSITIVE : Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_ORDER_CONFIRM_BUSINESS_CHANGEAFTER_PAYONBILL_NEGATIVE;
                    }
                }
                $codValue = $oneTimeCharge;
            } elseif ($hasNumberPorting) {
                $emailCode = Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_ORDER_CONFIRM_BUSINESS_ACQ_PORTING;
                if($isMobileMoveDealer){
                    $emailCode = Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_ORDER_CONFIRM_BUSINESS_ACQ_PORTING_MOBILE_MOVE;
                }
            } elseif ($superorder->hasRetention()) {
                if (!$isSimOnly) {
                    $emailCode = Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_ORDER_CONFIRM_BUSINESS_RET;
                } else {
                    $emailCode = Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_ORDER_CONFIRM_BUSINESS_RET_SO;
                }
            } else {
                $emailCode = Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_ORDER_CONFIRM_BUSINESS_ACQ_NON_PORTING;
            }
        } else {
            if(Mage::helper('agent')->checkIsTelesalesOrWebshop($superorder->getWebsiteId()) && $superorder->hasStoreDelivery() || $superorder->hasOtherStoreDelivery()){
                $emailCode = Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_ORDER_CONFIRM_STOREDELIVERY;
            }
            else if ($isOrderEdit) {
                if ($paymentMethod === 'cashondelivery') {
                    if ($changeBefore) {
                        // If the one-time charge is bigger than 0, the customer needs to pay, which is negative for the customer
                        if ($oneTimeCharge > 0) {
                            $emailCode = Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_ORDER_CONFIRM_CHANGEBEFORE_COD_NEGATIVE;
                        } elseif ($oneTimeCharge < 0) {
                            $emailCode = Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_ORDER_CONFIRM_CHANGEBEFORE_COD_POSITIVE;
                        } else {
                            $emailCode = Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_ORDER_CONFIRM_CHANGEBEFORE_COD;
                        }
                    } else {
                        // If one time charge bigger then 0, customer receives, so positive for the customer
                        $emailCode = $oneTimeCharge > 0 ? Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_ORDER_CONFIRM_CHANGEAFTER_COD_POSITIVE : Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_ORDER_CONFIRM_CHANGEAFTER_COD_NEGATIVE;
                    }
                } else {
                    if ($changeBefore) {
                        // If the one-time charge is bigger than 0, the customer needs to pay, which is negative for the customer
                        if ($oneTimeCharge > 0) {
                            $emailCode = Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_ORDER_CONFIRM_CHANGEBEFORE_PAYONBILL_NEGATIVE;
                        } elseif ($oneTimeCharge < 0) {
                            $emailCode = Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_ORDER_CONFIRM_CHANGEBEFORE_PAYONBILL_POSITIVE;
                        } else {
                            $emailCode = Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_ORDER_CONFIRM_CHANGEBEFORE_PAYONBILL;
                        }
                    } else {
                        // If one time charge bigger then 0, customer receives, so positive for the customer
                        $emailCode = $oneTimeCharge > 0 ? Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_ORDER_CONFIRM_CHANGEAFTER_PAYONBILL_POSITIVE : Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_ORDER_CONFIRM_CHANGEAFTER_PAYONBILL_NEGATIVE;
                    }
                }
                $codValue = $oneTimeCharge;
            } else {
                if ($hasNumberPorting) {
                    $emailCode = Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_ORDER_CONFIRM_ACQ_PORTING;
                    if($isMobileMoveDealer){
                        $emailCode = Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_ORDER_CONFIRM_ACQ_PORTING_MOBILE_MOVE;
                    }
                } elseif ($superorder->hasRetention()) {
                    if (!$isSimOnly) {
                        $emailCode = Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_ORDER_CONFIRM_RET;
                    } else {
                        $emailCode = Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_ORDER_CONFIRM_RET_SO;
                    }
                } else {
                    $emailCode = Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_ORDER_CONFIRM_ACQ_NON_PORTING;
                }
            }
        }

        if($isOtherStoreDelivery && $salesOrderAction == Vznl_Superorder_Model_Superorder::SO_TYPE_CREATE && $isMobile) {
            $emailCode = Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_ORDER_CONFIRMATION_OMNICHANNEL;
        } else if ($salesOrderAction == Vznl_Superorder_Model_Superorder::SO_TYPE_CREATE || $salesOrderAction == Vznl_Superorder_Model_Superorder::SO_TYPE_CANCEL) {
            $emailCode = Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_ORDER_CONFIRMATION_CREATE;
        } else if($salesOrderAction == Vznl_Superorder_Model_Superorder::SO_TYPE_CHANGE) {
            $emailCode = Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_ORDER_CONFIRMATION_CHANGE;
        }

        $onclickBannerUrl = $superorder->getConfirmationBannerUrl();
        $payload = array(
            'email_code' => $emailCode,
            'created_at' => strftime('%Y-%m-%d %H:%M:%S', time()),//current time
            'deadline' => strftime('%Y-%m-%d %H:%M:%S', time()),//current time, if needs to be sent in the future, change here
            'parameters' => $this->getCommunicationParameters($superorder),
            'receiver' => $customer->getCorrespondanceEmail($orderCollection->getFirstItem()),
            'from' => $from,
            'culture_code' => str_replace('_', '-', Mage::app()->getLocale()->getLocaleCode()),
        );

        return $payload;
    }

    public function getSoftContractPayload(Vznl_Superorder_Model_Superorder $superOrder)
    {
        $customer    = $superOrder->getCustomer();
        $emailCode   = null;
        $packages    = [];
        $videoLink   = $superOrder->hasRetention() ? Mage::getStoreConfig('email_configuration/email_configuration/order_confirmation_video_link_retention') : Mage::getStoreConfig('email_configuration/email_configuration/order_confirmation_video_link_acquisition');

        $dealerId = Mage::helper('agent')->getDaDealerCode($superOrder->getAgent());
        $dealer = Mage::getModel('agent/dealer')->load($dealerId, 'vf_dealer_code');
        $primaryBrand = $dealer->getPrimaryBrand();
        $orders = $superOrder->getOrders(true);
        $superOrderWebsiteCode = Mage::app()->getWebsite($superOrder->getWebsiteId())->getCode();
        $from = $dealer->getSenderDomain();
        /** @var Dyna_Checkout_Model_Sales_Order $deliveryOrder */
        foreach($orders as $deliveryOrder){
            foreach ($deliveryOrder->getPackages() as $package){
                $packages[] = $package;
            }
        }

        $isSinglePackage = count($packages) === 1;
        $isBusiness = $customer->getIsBusiness();
        $subscriptionStartDate = null;

        /** @var Vznl_Package_Model_Package $package */
        if ($isBusiness) {
            if ($isSinglePackage) {
                $package = $packages[0];
                $subscriptionStartDate = $package->getSubscriptionStartDate()->format('d-m-Y');
                if ($package->isFixed()) {
                    if ($superOrderWebsiteCode == Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE) {
                        $emailCode = Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_SOFTCOPY_CONTRACT_BUSINESS;
                    } elseif ($superOrderWebsiteCode == Vznl_Agent_Model_Website::WEBSITE_INDIRECT_CODE) {
                        $emailCode = Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_INDIRECT_CONTRACT_BUSINESS;
                    }
                } else {
                    if ($package->isRetention()) {
                        $emailCode = Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_SOFTCONTRACT_BUSINESS_RETENTION_SINGLE;

                    } elseif ($package->isAcquisition()) {
                        $emailCode = Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_SOFTCONTRACT_BUSINESS_ACQUISITION_SINGLE;

                    }
                }
            } else {
                $emailCode = Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_SOFTCONTRACT_BUSINESS_MULTI_PACKAGE;
            }
        } else {
            if ($isSinglePackage) {
                $package = $packages[0];
                $subscriptionStartDate = $package->getSubscriptionStartDate()->format('d-m-Y');
                if ($package->isFixed()) {
                    if ($superOrderWebsiteCode == Vznl_Agent_Model_Website::WEBSITE_RETAIL_CODE) {
                        $emailCode = Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_SOFTCOPY_CONTRACT;
                    } elseif ($superOrderWebsiteCode == Vznl_Agent_Model_Website::WEBSITE_INDIRECT_CODE) {
                        $emailCode = Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_INDIRECT_CONTRACT;
                    }
                } else {
                    if ($package->isNumberPorting() && $package->isAcquisition()) {
                        $emailCode = Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_SOFTCONTRACT_CONSUMER_PORTING_SINGLE;

                    } elseif ($package->isAcquisition() && !$package->isNumberPorting()) {
                        $emailCode = Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_SOFTCONTRACT_CONSUMER_ACQUISITION_SINGLE;

                    } elseif ($package->isRetention()) {
                        $emailCode = Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_SOFTCONTRACT_CONSUMER_RETENTION_SINGLE;

                    }
                }
            } else {
                $emailCode = Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_SOFTCONTRACT_CONSUMER_MULTI_PACKAGE;
            }
        }

        /** @var Vznl_Checkout_Helper_Pdf $pdfHelper */
        $pdfHelper = Mage::helper('vznl_checkout/pdf');
        $attachments = array();
        $loanOverviews = array();

        foreach ($superOrder->getDeliveryOrders(true) as $deliveryOrder) {
            $contractOutput = Mage::helper('vznl_checkout/sales')->generateContract($deliveryOrder, true, true);
            $attachments[] = $pdfHelper->saveContract($contractOutput, $deliveryOrder->getIncrementId(), 'pdf', true);
            if ($deliveryOrder->isLoanRequired()) {
                $loanPdf = $pdfHelper->saveLoanOverview($deliveryOrder, true);
                if ($loanPdf) {
                    $loanOverviews[] = $loanPdf;
                }
            }
        }
        $attachments[] = $pdfHelper->getTermsAndConditionsPath();

        foreach ($loanOverviews as $loanOverview) {
            $attachments[] = $loanOverview;
        }

        $contractPayload = [
            'email_code' => $emailCode,
            'created_at' => strftime('%Y-%m-%d %H:%M:%S', time()),//current time
            'deadline' => strftime('%Y-%m-%d %H:%M:%S', time()),
            'parameters' => [
                'Link_OrderStatusPage' => Mage::helper('vznl_checkout/email')->generateOrderUrl($superOrder, $customer),
                'Link_Video' => $videoLink,
                'Dealer'     => $this->getCommunicationDealerData($superOrder),
                'Subscription' => $this->getSubscriptionDate($subscriptionStartDate),
                'Customer'   => $this->getCommunicationCustomerData($superOrder),
                'Order'      => $this->getCommunicationOrderData($superOrder),
            ],
            'receiver' => $customer->getCorrespondanceEmail($superOrder->getOrders()->getFirstItem()),
            'from' => $from,
            'culture_code' => str_replace('_', '-', Mage::app()->getLocale()->getLocaleCode()),
            'attachments' => $attachments,
        ];
        return $contractPayload;
    }

    /**
     * Get the data for the Garant polis email
     * @param Vznl_Superorder_Model_Superorder $superOrder
     * @param array $attachmentPaths
     * @return array
     */
    public function getGarantPolisPayload(Vznl_Superorder_Model_Superorder $superOrder, $attachmentPaths = array())
    {
        /** @var Vznl_Checkout_Helper_Pdf $pdfHelper */
        $pdfHelper = Mage::helper('vznl_checkout/pdf');
        /** @var Vznl_Customer_Model_Customer_Customer $customer */
        $customer = Mage::getModel('customer/customer')->load($superOrder->getCustomerId());

        $dealerId = Mage::helper('agent')->getDaDealerCode($superOrder->getAgent());
        $dealer = Mage::getModel('agent/dealer')->load($dealerId, 'vf_dealer_code');
        $primaryBrand = $dealer->getPrimaryBrand();
        $from = $dealer->getSenderDomain();

        $payload = [
            'email_code' => $customer->getIsBusiness() ? Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_GARANT_MAIL_BUSINESS : Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_GARANT_MAIL,
            'created_at' => strftime('%Y-%m-%d %H:%M:%S', time()), //current time
            'deadline' => strftime('%Y-%m-%d %H:%M:%S', time()), //current time, if needs to be sent in the future, change here
            'parameters' => [
                ['Contact_Gender' => Mage::helper('vznl_checkout')->__($customer->getSalutation())],
                ['Contact_LastName' => $customer->getIsBusiness() ? $customer->getContractantLastname() : $customer->getLastname()],
                ['SSFE_SaleOrderNumber' => $superOrder->getOrderNumber()],
                ['SaleOrderNumber' => $superOrder->getOrderNumber()],
                ['Dealer' => $this->getCommunicationDealerData($superOrder)],
            ],
            'reports' => [
                [
                    'id' => -1,
                    'report_id' => Mage::getStoreConfig('vodafone_service/portal/garant_report_id'),
                    'output_type' => 'PDF',
                    'reports_parameters' => [],
                    'state' => 'Unchanged',
                ],
            ],
            'receiver' => $customer->getCorrespondanceEmail($superOrder->getOrders(true)->setPageSize(1, 1)->getLastItem()),
            'from' => $from,
            'culture_code' => str_replace('_', '-', Mage::app()->getLocale()->getLocaleCode()),
        ];

        if (!is_array($attachmentPaths)) {
            $attachmentPaths = [];
        }
        array_unshift($attachmentPaths, $pdfHelper->getAssurantIpidPath($superOrder));
        array_unshift($attachmentPaths, $pdfHelper->getGarantTermsPath($superOrder));
        $payload['attachments'] = array_values(array_filter($attachmentPaths));

        return $payload;
    }

    /**
     * Get the data for the Return Goods email
     * @param Vznl_Superorder_Model_Superorder $superOrder
     * @return array
     */
    public function getReturnGoodsPayload(Vznl_Superorder_Model_Superorder $superOrder)
    {
        /** @var Dyna_Customer_Model_Customer_Customer $customer */
        $customer = Mage::getModel('customer/customer')->load($superOrder->getCustomerId());

        $dealerId = Mage::helper('agent')->getDaDealerCode($superOrder->getAgent());
        $dealer = Mage::getModel('agent/dealer')->load($dealerId, 'vf_dealer_code');
        $primaryBrand = $dealer->getPrimaryBrand();
        $from = $dealer->getSenderDomain();

        return array(
            'email_code' => $customer->getIsBusiness() ? Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_RETURN_GOODS_BUSINESS : Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_RETURN_GOODS,
            'created_at' => strftime('%Y-%m-%d %H:%M:%S', time()),//current time
            'deadline' => strftime('%Y-%m-%d %H:%M:%S', time()),//current time, if needs to be sent in the future, change here
            'parameters' => array(
                array('Contact_Gender' => Mage::helper('vznl_checkout')->__($customer->getSalutation())),
                array('Contact_LastName' => $customer->getIsBusiness() ? $customer->getContractantLastname() : $customer->getLastname()),
                array('SSFE_SaleOrderNumber' => $superOrder->getOrderNumber()),
                array('SaleOrderNumber' => $superOrder->getOrderNumber()),
                array('Dealer' => $this->getCommunicationDealerData($superOrder)),
            ),
            'receiver' => $customer->getCorrespondanceEmail($superOrder->getOrders(true)->setPageSize(1, 1)->getLastItem()),
            'from' => $from,
            'culture_code' => str_replace('_', '-', Mage::app()->getLocale()->getLocaleCode()),
        );
    }

    /**
     * @param Dyna_Customer_Model_Customer_Customer $customer
     * @return array
     *
     * Get data for the Ils changing number
     */
    public function getIlsChangeNumberPayload($customer, $newNumber = '', $updateSettings = [])
    {
        $cartTemplate = "<div style='padding: 10px; border: solid 1px #000; font-family: Arial, Helvetica; color: rgb(125,125,125)'>
                            <div style='font-size: 16px; font-weight: bold; margin-bottom: 15px;'>
                                " . $this->__("Packages") . " <span style='font-size: 12px;'>(" . $this->__("ammounts including VAT") . ")</span>
                            </div>
                            <table style='color: rgb(125,125,125);'>
                                <tr>
                                    <td colspan='2' style='padding-bottom: 5px; font-weight: bold;'>" . $this->__("Your order") . "</td>
                                    <td style='padding-left: 15px; padding-bottom: 5px; font-weight: bold; text-align: right;'>" . $this->__("One-off") . "</td>
                                </tr>
                                <tr>
                                    <td style='width: 51px;'>
                                        <img src='data:image/jpg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCABJACkDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD95PE/i0+G5YEXTNV1OS4JASxgD+WAM5dmKqo9MnJ7A1QHxGuP+hX8Tf8AfmH/AOO1d07P/CfarycfYrQgZ4Hz3H+fwrVu5xbw7j64A9T2FAHO/wDCxrjP/Ir+Jv8AvzB/8dqHUPiudIsZbq68N+J4ba3UyTSfZY38tB1bashY4HOFBPHANfOfj/8A4KoeHNI/bP034NaNceHZdSAlbVb3UdR+zxRSJjFnbgD97cuWwqkgcMcHAB+ovBXi+08c+HY9QsydjsyOjfehdThkb3BrCjiadW/s3ezs/U9TM8lx2Xqk8bTcPaxU436xez9H08tTSsruO/tIp4XSWKZA6OhyrqRkEH0IqWuEtZH+EnimKxbd/wAIzrdxss27aXdNk+SfSKU52f3XJXoyAd1mtzyzLs49vjHUH/vWluPyab/GovHktza+HXubWNppbR1n8pesiqckD3xn8q4T9oz49J+zf4U8QeMLnS7vVtK0GCxm1RLXJmtbN5pUluFUAmTy1O8oOSFODXgeqf8ABWG905NUtL34dyaPqdnFEuy+1gJHpUrvbr5moN5X+jW5+0KY5Pm8zy34XFc1XF0qT5ZvU9jL8gx+Op+1w0OaN7bpaq1930uvvPyq/ah/4Jp/FHWP2rNdlsfsd14a1nXpNVg8WyajGi2cDzmXdIhbzRPGDjaFOWUYOOR+5n7GukalZfCR77U0nhbWr6S+gjmXbJ5JVERmHYvs3fRhXxi3/BQi/wDiJb+FY9N+Gfht/HV62nPfX72qT3D8W8l2VtCuYvNWU/Z3Z23bWOBxXrPhX/grRc+J7NL23+G15dabZok+pz2WrLcbIpZbaKIW37oC4lV7pEljyvlvHIuWK14mV08BgnUeHb993d7n6px9mPFvE9PCRzanD/Z4ezjyuKdlZJvX7VrLzWx9XfFSBLj4easGAIEBcZGeVIYH8CB+VdDXg/wE/abf9rH9lrU/FsugSeG2kaa1Fo92l1kKEYNuUDB+cKysAVZWGMAE+8V9FCanFSjsz8bxWFq4atLD1laUXZrfX1Wh8l/tu/8ABUH4e/8ABP740waV42sfEeoT+J9Fgu7WPS7SOZUjjmuEYuXdQMlhgDPQ15PD/wAHFvwN1GKR08I/Ei4jlxHIy6NA4cDgAnzuevT3r5X/AODnL5P2svAJGc/8Ice//T5PX1J8C/D2n6F8AvBtrp9pbW1pHa6QY0hQBDua3JPHXJJJPcmvy3PuMMfhMwnhaVuVOy08kf0ouBeEss4JyriDH4erWr4v2l7VeSK5JNaLkl5E4/4OI/gjBJuHg34lo5wAf7EhB46f8te1Mj/4OL/gdANi+EfiSmOcDRoBjnOf9d6810vxFtUfW9IUquDPL26fuJK5LSPA+n3HxTbfBE3/ABLFblR/z2avAl4h5lGXK1H7v+CfP06PA8leeXVv/Cn/AO5Frw//AMF5/gr8dta07wNomkeNNN1LxXfwabaS3Omwx26zSzKq7ykpIBJwTg9ea/Qqv55fjx4V0zwX/wAFltHsdHt7e1tP+E10KZo4AAiyySWzyYA4GXLEj1Jr+hqv0bhLOcRmNGpPE2vF20VuhxeNHBWR5D/ZmJyKM408XQVVqcuZpt7Xsj8Sv+DnRsftYeAf+xOP/pZPX078G7C38MfCDQzMrP4fv9LsUubePIls5nii2ywBfmOXwSi8hvnUfeB+Yv8Ag515/az8A/8AYnH/ANLJ6+o/gTFeR/Cjw/qtwsIvl0ywttNt5PnisDLHFEJXx9+QhsnB4Hyg8sx/MOKv+RxV9V+SP0niz/k13Dn/AHH/APSy7cWN94qW8uz4h1Nv7M1C4hti1nDG21V2DeGjBJwx6gZ61h/DfUX8SXMMNtf3ralfWUNxqWoTRGGSGIg4SBCqjli3zKNq5JJLFRXTeK/DF1oeoQwR67qjrql3NLcs0dvl2MTMSP3fy8qOO1ZHhrw5NBrcWk/bpTNplqlxpd88S+dbrkxtHIFAEiHYuRgbgeeQGr5CtNOrr8j8Xh8B8C/tMafBo/8AwWf8P21rEkFvB4r8OKiIOFGbX8z3JPJJJNf0GV/Ph+0nc3F1/wAFnNAe8t1tbr/hLfDoliV96BgbUZVu6nqM84IyAa/oPr9q8Pv93rf4l+R+hfSA/wCRfw5/2Bx/M/F//g5i8B61q/7S/wAPtSttJ1K606Tws9oLmC2eWMSrdyMyEqCNwV1OD2NL8Hv+Co3hXRvhFoGm674T8fWesadHYxXMdto7TQH7O0W5kfIJDKmQCARnHvX7Gaz/AMeI/wCun9KyvX6mqzjgyni8bLFuq03ra3kl3PIwXivgsTwtgeHM0y72scLzcs41XBvmbbuuSS6r7j8rPFn/AAVV+H2uanp8sPh34kBLaV3fd4fYHBjdRj5vUis/Sv8AgqD4CsvGhv5PD/xH8j7ELcAaAxbcJGbpu6YNfrK3UUjdB9RXk/8AEN6DfO6z+7/gnBHjHhtK39lS/wDCh/8Ayo/ADSfEuu/tgf8ABVfwn4q0fwpr1nb6v4x0mS3gntJN8NvbywgvK2NoOyMs3OBX9Eu4ViaJ/wAfp/3WrYr7DIcnjldKUIy5uZ32t0+Z4/if4hLimeCp0sMsPTwtJUoxUnO6T3baX5H/2Q=='>
                                    </td>
                                    <td style='font-weight: bold;'>
                                        " . $this->__("New easy-to-remember number") . "
                                    </td>
                                    <td style='font-weight: bold; padding-left: 15px;'>
                                        " . Mage::helper('core')->currency($updateSettings['quotation_information']['total_o_c'], true, false) . "
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan='3' style='padding-top: 10px; font-size: 12px; font-weight: normal;'>
                                        <b>" . $this->__("Payment method") . ":</b> automatisch incasso
                                    </td>
                                </tr>
                            </table>
                        </div>";

        $payload = array(
            'email_code' => Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_CHANGE_ILS_MSISDN,
            'NewNumberActivated' => $newNumber,
            'created_at' => strftime('%Y-%m-%d %H:%M:%S', time()), //current time
            'deadline' => strftime('%Y-%m-%d %H:%M:%S', time()), //current time, if needs to be sent in the future, change here
            'parameters' => array(
                array('Contact_Gender' => Mage::helper('vznl_checkout')->__($customer->getSalutation())),
                array('Contact_LastName' => $customer->getIsBusiness() ? $customer->getContractantLastname() : $customer->getLastname()),
                array('ShoppingCart' => $cartTemplate),
            ),
            'receiver' => $customer->getCorrespondanceEmail(),
            'culture_code' => str_replace('_', '-', Mage::app()->getLocale()->getLocaleCode()),
        );

        return $payload;
    }

    /**
     * Get the email payload when a package has a credit check rejected status
     *
     * @param Vznl_Package_Model_Package $package The package with credit check rejected
     * @return array The payload
     */
    public function getCreditCheckRejectedPayload(Vznl_Package_Model_Package $package)
    {
        $deliveryOrder = $package->getDeliveryOrder();
        $superOrder = $package->getSuperOrder();
        /** @var Dyna_Customer_Model_Customer_Customer $customer */
        $customer = $deliveryOrder->getCustomer();
        // Set template
        $emailCode = $customer->getIsBusiness() ? Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_CC_REJECTED_BUSINESS : Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_CC_REJECTED;

        $dealerId = Mage::helper('agent')->getDaDealerCode($superOrder->getAgent());
        $dealer = Mage::getModel('agent/dealer')->load($dealerId, 'vf_dealer_code');
        $primaryBrand = $dealer->getPrimaryBrand();
        $from = $dealer->getSenderDomain();

        $payload = array(
            'email_code' => $emailCode,
            'created_at' => strftime('%Y-%m-%d %H:%M:%S', time()),
            'deadline' => strftime('%Y-%m-%d %H:%M:%S', time()),
            'parameters' => $this->getCommunicationParameters($superOrder),
            'receiver' => $customer->getCorrespondanceEmail($deliveryOrder),
            'from' => $from,
            'culture_code' => str_replace('_', '-', Mage::app()->getLocale()->getLocaleCode()),
        );
        return $payload;
    }

    /**
     * Get the email payload when a package has a order validation failed status
     *
     * @param Vznl_Package_Model_Package $package The package with corder validation failed
     * @return array The payload
     */
    public function getOrderValidationFailedPayload(Vznl_Package_Model_Package $package)
    {
        $deliveryOrder = $package->getDeliveryOrder();
        $superOrder = $package->getSuperOrder();
        /** @var Dyna_Customer_Model_Customer_Customer $customer */
        $customer = $deliveryOrder->getCustomer();
        // Set template
        $emailCode = Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_ORDER_VALIDATION_FAILED;

        $dealerId = Mage::helper('agent')->getDaDealerCode($superOrder->getAgent());
        $dealer = Mage::getModel('agent/dealer')->load($dealerId, 'vf_dealer_code');
        $primaryBrand = $dealer->getPrimaryBrand();
        $from = $dealer->getSenderDomain();

        $payload = array(
            'email_code' => $emailCode,
            'created_at' => strftime('%Y-%m-%d %H:%M:%S', time()),
            'deadline' => strftime('%Y-%m-%d %H:%M:%S', time()),
            'parameters' => $this->getCommunicationParameters($superOrder),
            'receiver' => $customer->getCorrespondanceEmail($deliveryOrder),
            'from' => $from,
            'culture_code' => str_replace('_', '-', Mage::app()->getLocale()->getLocaleCode()),
        );
        return $payload;
    }

    /**
     * Gets the email payload for the email that is triggered when an order is cancelled during provisioning.
     *
     * @param Vznl_Superorder_Model_Superorder $superOrder The sales order that is cancelled.
     * @return array The payload.
     */
    public function getCancelledDuringProvisioningPayload(Vznl_Superorder_Model_Superorder $superOrder){
        /** @var Dyna_Customer_Model_Customer_Customer $customer */
        $customer = $superOrder->getCustomer();
        // Set template
        $emailCode = $customer->getIsBusiness() ? Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_CANCEL_DURING_PROVISIONING_BUSINESS : Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_CANCEL_DURING_PROVISIONING;

        $dealerId = Mage::helper('agent')->getDaDealerCode($superOrder->getAgent());
        $dealer = Mage::getModel('agent/dealer')->load($dealerId, 'vf_dealer_code');
        $primaryBrand = $dealer->getPrimaryBrand();
        $from = $dealer->getSenderDomain();

        return array(
            'email_code' => $emailCode,
            'created_at' => strftime('%Y-%m-%d %H:%M:%S', time()),//current time
            'deadline' => strftime('%Y-%m-%d %H:%M:%S', time()),//current time, if needs to be sent in the future, change here
            'parameters' => array(
                array('Contact_Gender' => Mage::helper('vznl_checkout')->__($customer->getSalutation())),
                array('Contact_LastName' => $customer->getCompleteLastName()),
                array('SSFE_SaleOrderNumber' => $superOrder->getOrderNumber()),
                array('SaleOrderNumber' => $superOrder->getOrderNumber()),
                array('Dealer' => $this->getCommunicationDealerData($superOrder))
            ),
            'receiver' => $customer->getCorrespondanceEmail($superOrder->getOrders(true)->setPageSize(1, 1)->getLastItem()),
            'from' => $from,
            'culture_code' => str_replace('_', '-', Mage::app()->getLocale()->getLocaleCode()),
        );
    }

    /**
     * Get the email payload for noticing backorder orders
     * @param Vznl_Package_Model_Package $package The package to trigger for.
     * @param $emailCode The backorder email code to use
     * @return array The payload
     */
    public function getBackOrderPayload(Vznl_Package_Model_Package $package, $emailCode)
    {
        // Check if the given email code is accepted as back order email
        if(!in_array($emailCode, Vznl_Communication_Model_Job::ROWKEYS_ALL_BACKORDER_EMAILS)){
            return [];
        }

        /** @var Dyna_Checkout_Model_Sales_Order $deliveryOrder */
        $deliveryOrder = $package->getDeliveryOrder();
        /** @var Vznl_Superorder_Model_Superorder $superOrder */
        $superOrder = $package->getSuperOrder();
        /** @var Dyna_Customer_Model_Customer_Customer $customer */
        $customer = $deliveryOrder->getCustomer();

        $dealerId = Mage::helper('agent')->getDaDealerCode($superOrder->getAgent());
        $dealer = Mage::getModel('agent/dealer')->load($dealerId, 'vf_dealer_code');
        $primaryBrand = $dealer->getPrimaryBrand();
        $from = $dealer->getSenderDomain();


        $payload = array(
            'email_code' => $emailCode,
            'created_at' => strftime('%Y-%m-%d %H:%M:%S', time()),//current time
            'deadline' => strftime('%Y-%m-%d %H:%M:%S', time()),//current time, if needs to be sent in the future, change here
            'parameters' => array(
                array('Contact_Gender' => Mage::helper('vznl_checkout')->__($customer->getSalutation())),
                array('Contact_LastName' => $customer->getCompleteLastName()),
                array('SSFE_SaleOrderNumber' => $superOrder->getOrderNumber()),
                array('SaleOrderNumber' => $superOrder->getOrderNumber()),
            ),
            'receiver' => $customer->getCorrespondanceEmail($deliveryOrder),
            'from' => $from,
            'culture_code' => str_replace('_', '-', Mage::app()->getLocale()->getLocaleCode()),
        );

        if (in_array($emailCode, Vznl_Communication_Model_Job::ROWKEYS_ALL_BACKORDER_EMAILS_REQUIRE_DEALER_INFO)) {
            $payload['parameters'] = array('Dealer' => $this->getCommunicationDealerData($superOrder));
        }
        return $payload;
    }



    /**
     * @param Vznl_Superorder_Model_Superorder $superorder
     * @return array
     *
     * Get the data for the fixed order confirmation email
     */
    public function getFixedOrderConfirmationPayload(Vznl_Superorder_Model_Superorder $superorder) : array
    {
        $addresses = ['delivery' => [], 'invoice' => []];
        $orderCollection = $superorder->getOrders(true);
        foreach($orderCollection as $order) {
            $shippingAddress = $order->getShippingAddress();
            $shippingAddress->setData('street', $shippingAddress->getStreet());
            $addresses['delivery'][] = Mage::helper("dyna_checkout")->formatAddressAsHtml($shippingAddress, true);

            $billingAddress = $order->getBillingAddress();
            $billingAddress->setData('street', $billingAddress->getStreet());
            $addresses['invoice'][] = Mage::helper("dyna_checkout")->formatAddressAsHtml($billingAddress, true);
        }
        $dealerId = Mage::helper('agent')->getDaDealerCode($superorder->getAgent());
        $dealerChannel = Mage::helper('agent')->getWebsiteCode($superorder->getWebsiteId());
        $dealer = Mage::getModel('agent/dealer')->load($dealerId, 'vf_dealer_code');
        $primaryBrand = $dealer->getPrimaryBrand();
        $from = $dealer->getSenderDomain();

        $addresses['delivery'] = array_unique($addresses['delivery'], SORT_REGULAR);
        $addresses['invoice'] = array_unique($addresses['invoice'], SORT_REGULAR);

        //make sure the superorder still exists
        if (!$superorder->getId() || !$superorder->getPealOrderId()) {
            return array();
        }

        /** @var Dyna_Customer_Model_Customer_Customer $customer */
        $customer = Mage::getModel('customer/customer')->load($superorder->getCustomerId());
        $customerType = 'residential';
        if ($customer->getIsBusiness()) {
            $customerType = 'soho';
        }
        // Order Date
        $saleOrderDate = Mage::app()
            ->getLocale()
            ->date(
                Mage::getModel('core/date')
                    ->timestamp(
                        strtotime($superorder->getCreatedAt())
                    ), null, null, false)
            ->toString('dd-MM-y');

        $videoLink = $superorder->hasRetention() ? Mage::getStoreConfig('email_configuration/email_configuration/order_confirmation_video_link_retention') : Mage::getStoreConfig('email_configuration/email_configuration/order_confirmation_video_link_acquisition');

        $deliveryMethod = $superorder->getAllDeliveryMethods();
        $WrittenConsentFlag = 'No';
        if ($superorder->getFixedWrittenConsent() == 1) {
            $WrittenConsentFlag = 'Yes';
        }
        $emailCode = Vznl_Communication_Model_Job::TEMPLATE_ROW_KEY_ORDER_CONFIRMATION_CREATE;

        $payload = array(
            'email_code' => $emailCode,
            'created_at' => strftime('%Y-%m-%d %H:%M:%S', time()),//current time
            'deadline' => strftime('%Y-%m-%d %H:%M:%S', time()),//current time, if needs to be sent in the future, change here
            'parameters' => $this->getCommunicationParameters($superorder),
            'receiver' => $customer->getCorrespondanceEmail($orderCollection->setPageSize(1, 1)->getLastItem()),
            'from' => $from,
            'culture_code' => str_replace('_', '-', Mage::app()->getLocale()->getLocaleCode()),
        );

        return $payload;
    }

    /**
     * @param Vznl_Superorder_Model_Superorder $superorder
     * @return array
     *
     * Get the data for the order confirmation email
     */
    public function getCommunicationParameters(Vznl_Superorder_Model_Superorder $superorder) : array
    {
        $parameters = array(
            'Dealer' => $this->getCommunicationDealerData($superorder),
            'Customer' => $this->getCommunicationCustomerData($superorder),
            'Order' => $this->getCommunicationOrderData($superorder)
        );

        return $parameters;
    }

    /**
     * @param Vznl_Checkout_Model_Sales_Quote $quote
     * @return array
     *
     * Get the data for the order confirmation email
     */
    public function getCommunicationParametersForQuote(Vznl_Checkout_Model_Sales_Quote $quote) : array
    {
        $parameters = array(
            'Dealer' => $this->getCommunicationDealerData($quote), // the same as for superorder
            'Customer' => $this->getCommunicationCustomerData($quote), // the same as for superorder
            'packages' => $this->getCommunicationPackagesData(null, $quote),
            'ShoppingCart' => [
                'ShoppingCart' => Mage::helper('vznl_checkout/email')->sendOrderConfirmationEmail(null, $quote->getId())
            ]
        );

        return $parameters;
    }

    /**
     * @param $superOrder
     * @return array
     */
    public function getCommunicationCustomerData($superOrder)
    {
        $customer = Mage::getModel('customer/customer')->load($superOrder->getCustomerId());
        $customerType = 'residential';
        if ($customer->getIsBusiness()) {
            $customerType = 'soho';
        }

        $customerData = [
            'CustomerType'     => $customerType,
            'Contact_Gender'   => Mage::helper('vznl_checkout')->__($customer->getSalutation()),
            'Contact_LastName' => $customer->getCompleteLastName(),
        ];

        return $customerData;
    }

    /**
     * @param $superOrder
     * @return array
     */
    public function getCommunicationDealerData($superOrder)
    {
        $agentHelper = Mage::helper('agent');

        $dealerId = $agentHelper->getDaDealerCode($superOrder->getAgent());
        $dealerChannel = $agentHelper->getWebsiteCode($superOrder->getWebsiteId());
        $dealer = Mage::getModel('agent/dealer')->load($dealerId, 'vf_dealer_code');
        $primaryBrand = $dealer->getPrimaryBrand();

        $dealerData = [
            'DealerGroup'        => $dealerId,
            'DealerGroupChannel' => $dealerChannel,
            'PrimaryBrand'       => $primaryBrand,
        ];

        return $dealerData;
    }

    /**
     * @param $subscriptionStartDate
     * @return array
     */
    public function getSubscriptionDate($subscriptionStartDate) {
        $subscriptionDate = [
            'Subscription_Date' => $subscriptionStartDate,
        ];

        return $subscriptionDate;
    }

    /**
     * @param $superOrder
     * @param $quote
     * @return array
     */
    public function getCommunicationOrderData($superOrder, $quote = null)
    {
        $customer = Mage::getModel('customer/customer')->load($superOrder->getCustomerId());
        $Packages = $this->getCommunicationPackagesData($superOrder, $quote);

        $OrderData = [
            'SaleorderNumber' => $superOrder ? $superOrder->getOrderNumber() : 'NULL',
            'SaleorderType'   => $this->getCommunicationSalesOrderType($superOrder ?? $quote),
            'SaleorderDate'    => Mage::app()->getLocale()->date(Mage::getModel('core/date')->timestamp(strtotime($superOrder ? $superOrder->getCreatedAt() : $quote->getCreatedAt())), null, null, false)->toString('dd-MM-y'),
            'Packages'         => $Packages,
            'Link_OrderStatusPage' => Mage::helper('vznl_checkout/email')->generateOrderUrl($superOrder, $customer),
            'ShoppingCart'    => Mage::helper('vznl_checkout/email')->sendOrderConfirmationEmail($superOrder->getId()),
        ];

        return $OrderData;
    }

    /**
     * @param $superOrder
     * @return string
     */
    public function getCommunicationSalesOrderType($superOrder)
    {
        $salesOrderType = '';

        if ($superOrder->hasMobilePackage() && !$superOrder->hasFixed()) {
            $salesOrderType = 'Mobile';
        }

        if (!$superOrder->hasMobilePackage() && $superOrder->hasFixed()) {
            $salesOrderType = 'Fixed';
        }

        if ($superOrder->hasMobilePackage() && $superOrder->hasFixed()) {
            $salesOrderType = 'Converged';
        }

        return $salesOrderType;
    }

    /**
     * @param $superOrder
     * @param $quote
     * @return array
     */
    public function getCommunicationPackagesData($superOrder, $quote)
    {
        $packagesData = [];

        if ($superOrder && $superOrder != null) {
            $orders = $superOrder->getOrders(true);

            /** @var Vznl_Checkout_Model_Sales_Order $order */
            foreach ($orders as $order) {
                $orderPackages = $order->getPackageIds();
                $packages = $order->getPackages(array('in' => $orderPackages));

                foreach ($packages as $package) {
                    $packagesData[] = $this->getCommunicationPackageData($superOrder, $package, $order);
                }
            }
        } else if ($quote) {
            $packageOrderData = [
                'DeliveryAddress' => 'NULL',
                'IsStoreDelivery' => 'NULL',
            ];
            foreach ($quote->getCartPackages() as $package) {
                $packagesData[] = array_merge($this->getCommunicationPackageData(null, $package, $quote), $packageOrderData);
            }
        }

        return $packagesData;
    }

    /**
     * @param $superOrder
     * @param $package
     * @param $order
     * @return array
     */
    public function getCommunicationPackageData($superOrder, $package, $order)
    {
        $salesOrderType = "NULL";
        $deltaOneTimeCharge = "NULL";
        $salesOrderAction = "NULL";
        $COD_Value = "NULL";
        $CODIndicator = "FALSE";
        $salesOrderType = "NULL";
        $simOnly = "FALSE";
        $isWrittenConsent = 'FALSE';
        $priceIncrease = $package->isMobile() ? "NULL" : "FALSE";
        $isPortingIndicator = $package->isNumberPorting() ? 'TRUE' : 'FALSE';
        $addresses['delivery'] = $this->getCommunicationAddressData($order->getShippingAddress());
        if ($package->getSaleType() == "RETBLU") {
            $processContextIndicator = "RET";
        } else if($package->getSaleType() == Vznl_Catalog_Model_ProcessContext::MOVE) {
            $processContextIndicator = "MOV";
        } else if ($package->getSaleType() == Vznl_Catalog_Model_ProcessContext::ILSBLU) {
            $processContextIndicator = "ILS";
        } else {
            $processContextIndicator = $package->getSaleType();
        }

        if($superOrder) {
            $deliveryOrders = $superOrder->getDeliveryOrders();
            /** @var Dyna_Checkout_Model_Sales_Order $lastDeliveryOrder */
            $lastDeliveryOrder = $deliveryOrders->getLastItem();
            $parentId = $lastDeliveryOrder->getParentId();
            $packageTotals = $package->getPackageTotals($package->getItems());
            $oldPackageTotals = Mage::getModel('vznl_package/package')->getOldPackageOneTimeCharge($parentId, $package->getPackageId());
            $packagesIds = explode(',', $order->getEditedPackagesIds());
            if ($package->isMobile()) {
                $salesOrderAction = $superOrder->getSalesOrderActionType();
                if ($order->getMobileOneoffPayment() == "Pay on delivery" || $order->getMobileOneoffPayment() == "cashondelivery") {
                    $CODIndicator = "TRUE";
                }

                if ($superOrder->getSalesOrderActionType() == Vznl_Superorder_Model_Superorder::SO_TYPE_CHANGE) {
                    if (in_array($package->getPackageId(), $packagesIds)) {
                        $oneTimeCharge = $superOrder->getOneTimeCharge($packageTotals, $oldPackageTotals);
                        $deltaOneTimeCharge = $superOrder->getOneTimeChargeType($oneTimeCharge);
                        $COD_Value = "&euro; " . number_format($oneTimeCharge, 2);
                    } else {
                        $CODIndicator = "FALSE";
                        $deltaOneTimeCharge = Vznl_Superorder_Model_Superorder::SO_CHARGET_YPE_EQUAL;
                        $COD_Value = "&euro; " . number_format('0.00', 2);
                    }
                }
            }
        }

        $items = count($package->getPackagesItems())>0 ? $package->getPackagesItems() : $package->getItems();
        foreach ($items as $item) {
            $ProductId = $item->getProductId();
            $StoreId = $item->getStoreId();
            $productResource = Mage::getResourceModel('catalog/product');
            $product = $item->getProduct();
            $onclickBannerUrl = "NULL";
            $PromotionalBannerVisability = "NULL";
            $overStappenIndicator = 'FALSE';
            $isStoreDelivery  = 'NULL';
            $addresses['invoice'] = $this->getCommunicationAddressData($order->getBillingAddress());
            $footPrintValue = "NULL";

            $deliveryType   = $order->getShippingAddress()->getDeliveryType();
            $deliveryStoreId   = $order->getShippingAddress()->getDeliveryStoreId();
            if ($product->getSimOnly()) {
                $simOnly = 'TRUE';
            }

            if ($package->isMobile()) {
                $isWrittenConsent = 'NULL';
                $overStappenIndicator = 'NULL';
                $priceIncreaseScope = "NULL";
                if ($order->getMobileOneoffPayment() == "Pay on delivery" || $order->getMobileOneoffPayment() == "cashondelivery") {
                    $CODIndicator = "TRUE";
                }
                if ($superOrder) {
                    $onclickBannerUrl = $superOrder->getConfirmationBannerUrl();
                }

                $isStoreDelivery = ($order instanceof Mage_Sales_Model_Order && $order->isStoreDelivery()) ? 'TRUE' : 'FALSE';
                if($superOrder) {
                    $DeliveryMethod = $this->getConvergedDeliveryMethods($deliveryType, $package, $deliveryStoreId, $superOrder);
                    $DeliveryDutchValues = $superOrder->getDeliveryDutchValues($DeliveryMethod);
                }
                $InvoiceAddresses = $this->getCommunicationAddressData($order->getBillingAddress());
                $PromotionalBannerVisability = $onclickBannerUrl === null ? 'mso-hide: all; overflow: hidden; max-height: 0; width: 0; display: none; line-height: 0; visibility: hidden;' : null;
            }
            if ($package->isFixed()) {
                $simOnly = 'NULL';
                $CODIndicator = "NULL";
                $salesOrderType = "Fixed";
                $footPrint = $order->getFootprint() ?: null;
                if ($package->getFixedOverstappenStatus() == 1) {
                    $overStappenIndicator = "TRUE";
                }
                $priceIncreaseScope = $productResource->getAttributeRawValue($ProductId, 'price_increase_scope', $StoreId);
                if ($priceIncreaseScope) {
                    $priceIncrease = "TRUE";
                }
                if ($order->getFixedWrittenConsent() == 1) {
                    $isWrittenConsent = "TRUE";
                }
                if ($superOrder) {
                    $DeliveryMethod = $this->getConvergedDeliveryMethods($deliveryType, $package, $deliveryStoreId, $superOrder);
                    $DeliveryDutchValues = $superOrder->getDeliveryDutchValues($DeliveryMethod);
                }
                if($footPrint == self::SERVICE_ADDRESS_FOOTPRINT_ZIGGO) {
                    $footPrintValue = "ZiggoBV";
                } else if($footPrint == self::SERVICE_ADDRESS_FOOTPRINT_UPC) {
                    $footPrintValue = "ZiggoServicesBV";
                }
                $InvoiceAddresses = $this->getCommunicationBillingAddressData($order->getBillingAddress());
            } elseif ($package->isMobilePostpaid()) {
                $salesOrderType = 'Mobile Postpaid';
            } elseif ($package->isHardware()) {
                $salesOrderType = 'Mobile Hardware';
            } elseif ($package->isPrepaid()) {
                $salesOrderType = 'Mobile Prepaid';
            }
        }

        $packageData = [
            'PackageID'                 => $package->getPackageId(),
            'PackageType'               => $salesOrderType ?? null,
            'SalesOrderAction'          => $salesOrderAction,
            'ProcessContextIndicator'   => $processContextIndicator,
            'PortingIndicator'          => $isPortingIndicator,
            "OverstappenIndicator"      => $overStappenIndicator ?? null,
            'WrittenConsentFlag'        => $isWrittenConsent,
            'IsStoreDelivery'           => $isStoreDelivery ?? null,
            'DeliveryMethod'            => $DeliveryDutchValues ?? null,
            'DeliveryAddress'           => $addresses['delivery'],
            'InvoiceAddress'            => $InvoiceAddresses ?? null,
            "CODIndicator"              => $CODIndicator,
            "COD_Value"                 => $COD_Value,
            'DeltaOneTimeCharge'        => $deltaOneTimeCharge,
            "SimOnly"                   => $simOnly,
            'PriceIncrease'             => $priceIncrease,
            'Footprint'                 => $footPrintValue,
            'BannerOnclickUrl'          => $onclickBannerUrl ?? null,
            'PromotionalBannerVisability'=> $PromotionalBannerVisability ?? null,
        ];

        return $packageData;
    }

    /**
     * @param $address
     * @return mixed
     */
    public function getCommunicationAddressData($address)
    {
        $checkoutHelper = Mage::helper("dyna_checkout");

        $address->setData('street', $address->getStreet());
        return $checkoutHelper->formatAddressAsHtml($address, true);
    }

    private function streetAddressFix($address)
    {
        $address = explode("\n", $address);
        if (isset($address[3]) && strlen($address[3]) > 3)
            unset($address[3]);
        return implode(" ", array_filter($address));
    }

    /**
     * @param $address
     * @return mixed
     */
    public function getCommunicationBillingAddressData($address)
    {
        $html = (null !== $address->getFixedStreet()) ? preg_replace('~[\r\n]+~', ' ', $this->streetAddressFix($address->getFixedStreet())) : '';
        $html .= ($html) ? ', ' : '';
        $html .= $address->getFixedPostcode() . ', ' . $address->getFixedCity();

        $html = trim($html);
        if (strip_tags($html) == ',') return '';

        return $html;
    }

    /**
     * Get a list of delivery methods of a superorder
     * return Comma separated string with delivery methods
     *
     * @param string
     * @param package
     * @param deliveryStoreId
     * @param superOrder
     * @return string
     */
    public function getConvergedDeliveryMethods($deliveryType, $package, $deliveryStoreId, $superOrder)
    {
        $agentHelper = Mage::helper('agent');
        $dealerId = $superOrder->getDealerId();
        $dealerChannel = $agentHelper->getWebsiteCode($superOrder->getWebsiteId());
        if ($package->isFixed()) {
            if ($deliveryType == 'billing_address') {
                $deliveries = Mage::helper('dyna_checkout')->__('LSP_SERVICE_ADD');
            } elseif ($deliveryType == 'other_address') {
                $deliveries = Mage::helper('dyna_checkout')->__('LSP_OTHER_ADD');
            } else {
                $deliveries = Mage::helper('dyna_checkout')->__('SHOP');
            }
        } else {
            if ($deliveryType == 'billing_address') {
                $deliveries = Mage::helper('dyna_checkout')->__('LSP_BILLING_ADD');
            } elseif ($deliveryType == 'other_address') {
                $deliveries = Mage::helper('dyna_checkout')->__('LSP_OTHER_ADD');
            } elseif ($deliveryType == 'store' && ($deliveryStoreId != $dealerId) && ($dealerChannel != Vznl_Agent_Model_Website::WEBSITE_TELESALES_CODE)) {
                $deliveries = Mage::helper('dyna_checkout')->__('OTHER_SHOP');
            } else {
                $deliveries = Mage::helper('dyna_checkout')->__('SHOP');
            }
        }
        return $deliveries;
    }
}
