<?php

use Psr\Log\LoggerInterface;
use Zend\Soap\Client;

/**
 * Class Vznl_Communication_Adapter_Email_StubAdapterFactory
 */
class Vznl_Communication_Adapter_Email_StubAdapterFactory implements Vznl_Communication_Adapter_AdapterFactoryInterface
{
    /**
     * @param LoggerInterface $logger
     * @return Vznl_Communication_Adapter_Email_StubAdapter
     */
    public static function create(array $options) : Vznl_Communication_Adapter_AdapterInterface
    {
        $options = array_merge(self::getOptions(), $options);
        return new Vznl_Communication_Adapter_Email_StubAdapter($options['logger']);
    }

    /**
     * @return array
     */
    public static function getOptions() : array
    {
        return [];
    }
}
