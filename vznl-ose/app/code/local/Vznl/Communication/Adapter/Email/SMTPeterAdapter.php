<?php

use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;

/**
 * Class Vznl_Communication_Adapter_Email_SMTPeterAdapter
 */
class Vznl_Communication_Adapter_Email_SMTPeterAdapter implements Vznl_Communication_Adapter_AdapterInterface
{
    const DEALER_GROUP = 'DealerGroup';
    const PRIMARY_BRAND = 'PrimaryBrand';
    const SALE_ORDER_TYPE = 'SaleorderType';
    const CUSTOMER_TYPE = 'CustomerType';
    const CONVERGED = 'Converged';
    const RESIDENTIAL = 'residential';
    const DEFAULT_VALUE = 'unknown';
    const BUSINESS = 'soho';

    /**
     * Logger object to log
     *
     * @var LoggerInterface
     */
    private $logger;

    /**
     * Exception variable
     *
     * @var Exception
     */
    private $exception;

    /**
     * Options array
     *
     * @var array
     */
    private $options = [];

    /**
     *  The endpoint
     *
     *  @var string
     */
    private $endPoint;

    /**
     *  The access token
     *
     *  @var string
     */
    private $token;

    /**
     * Payload Array
     *
     * @var array
     */
    private $payload = [];

    public function __construct(LoggerInterface $logger, array $options=[])
    {
        $this->logger = $logger;
        $this->options = array_merge($this->options, $options);
        $this->token = array_key_exists('access_token', $this->options) ? $this->options['access_token'] : '';
        $this->endPoint = array_key_exists('endpoint', $this->options) ? $this->options['endpoint'] : '';
    }

    /**
     * Get the last exception
     *
     * @return Exception
     */
    public function getLastException()
    {
        return $this->exception;
    }

    /**
     * LogStatus function
     *
     * @param Message needs to pass $message
     * @return null
     */
    private function logStatus($message)
    {
        $this->logger->log(LogLevel::DEBUG, $message);
    }

    /**
     * Get Mime Type function
     *
     * @param Filename $filename
     * @return mixed|null|string
     */
    private function getMimeType($filename)
    {
        $mime = null;
        $info = @finfo_open(FILEINFO_MIME_TYPE);
        if ($info && $mime = @finfo_file($info, $filename)) {
            @finfo_close($info);
        }
        return $mime ? $mime : 'application/text'; //fallback
    }

    /**
     * @param Vznl_Communication_Model_Job $job
     * @throws Exception
     */
    private function setRecipients(Vznl_Communication_Model_Job $job)
    {
        $this->payload['recipient'] = $job->getReceiver();
        $this->payload['from'] = $job->getFrom();
        $this->payload['to'] = $job->getReceiver();

        if (!is_null($job->getCc())) {
            throw new Exception('CC not implemented');
        }
        if (!is_null($job->getBcc())) {
            throw new Exception('BCC not implemented');
        }

    }

    /**
     * @param Vznl_Communication_Model_Job $job
     */
    private function setGenericPayloadSettings(Vznl_Communication_Model_Job $job)
    {
        $this->payload['template'] = $job->getEmailCode();
        $this->payload['inlinecss'] = true;
        $this->payload['trackclicks'] = true;
        $this->payload['trackopens'] = true;
        $this->payload['trackbounces'] = true;
        $this->payload['preventscam'] = true;
        $this->payload['images'] = 'hosted';
    }

    /**
     * @param Vznl_Communication_Model_Job $job
     */
    private function setTemplateParameters(Vznl_Communication_Model_Job $job)
    {
        $this->payload['tags'] = [
            'SaleorderNumber' => '',
            'DealerCode' => '',
            'PrimaryBrand' => '',
            'SalesOrderType' => '',
            'CustomerType' => ''
        ];

        foreach($job->getParameters() as $parameter) {
            foreach ((is_array($parameter) ? $parameter : array()) as $name => $value) {
                $this->payload['data'][$name] = $value;

                if ($name === "SaleorderNumber" || $name === "SSFE_SaleOrderNumber") {
                    $this->payload['tags']['SaleorderNumber'] = $value ?: self::DEFAULT_VALUE;
                }
                if($name === self::DEALER_GROUP) {
                    $this->payload['tags']['DealerCode'] = $value ?: self::DEFAULT_VALUE;
                }
                if($name === self::PRIMARY_BRAND) {
                    $this->payload['tags']['PrimaryBrand'] = $value ?: self::DEFAULT_VALUE;
                }
                if($name === self::SALE_ORDER_TYPE) {
                    if($value == self::CONVERGED) {
                        $this->payload['tags']['SalesOrderType'] = $value ? self::CONVERGED : self::DEFAULT_VALUE;
                    } else {
                        $this->payload['tags']['SalesOrderType'] = $value ?: self::DEFAULT_VALUE;
                    }
                }
                if($name === self::CUSTOMER_TYPE) {
                    if($value == self::RESIDENTIAL) {
                        $this->payload['tags']['CustomerType'] = $value ? self::RESIDENTIAL : self::DEFAULT_VALUE;
                    } else {
                        $this->payload['tags']['CustomerType'] = $value ? self::BUSINESS : self::DEFAULT_VALUE;
                    }
                }
            }
        }
        $this->payload['tags'] = array_values($this->payload['tags']);
    }

    /**
     * @param Vznl_Communication_Model_Job $job
     */
    private function setAttachments(Vznl_Communication_Model_Job $job)
    {
        foreach($job->getAttachments() as $path)
        {
            if (strpos($path, '\/') !== false) {
                $path = str_replace('\/', '/', $path);
            }
            if(!file_exists($path)) {
                throw new Exception(sprintf('Invalid attachment file given (%s).', $path));
            }
            $data['data'] = base64_encode(Mage::helper("vznl_data/data")->getFileContents($path));
            $data['name'] = basename($path);
            $data['type'] = $this->getMimeType($path);
            $this->payload['attachments'][] = $data;
        }
    }

    /**
     * @param Vznl_Communication_Model_Job $job
     */
    public function send(Vznl_Communication_Model_Job $job)
    {
        try {
            $this->setRecipients($job);
            $this->setGenericPayloadSettings($job);
            $this->setTemplateParameters($job);
            $this->setAttachments($job);
            $output = $this->post("send", $this->payload);

            $this->cleanupAttachments();
            $this->logStatus("\nRequest:\n" . json_encode($this->payload) . "\n\nResponse:\n" . var_export($output, true));
        } catch(Exception $e) {
            $output = $e->getCode() . ': ' . $e->getMessage();

            $this->cleanupAttachments();
            $this->logStatus("\nRequest:\n" . json_encode($this->payload) . "\n\nResponse:\n" . var_export($output, true));
            Mage::logException($e);
            return false;
        }
        return true;
    }

    /**
     * Cleanup attachments
     */
    private function cleanupAttachments()
    {
        $attCount = count($this->payload['attachments']);
        for ($i = 0 ; $i < $attCount; $i++) {
            $this->payload['attachments'][$i]["data"] = "<base_64>";
        }
    }

    /**
     *  Send a HTTP POST call to the rest API
     *
     *  @param  string      The method to call (for example: "send")
     *  @param  array       Structure with all fields to send
     *  @return array       The returned data from the API
     */
    public function post($method, array $fields)
    {
        // construct a CURL resource
        $curl = curl_init($this->endPoint.'/'.$method.'?access_token='.urlencode($this->token));

        // set curl options
        curl_setopt_array($curl, array(
            CURLOPT_POSTFIELDS      =>  json_encode($fields),   // the data to post - JSON encoded to allow deeply nested arrays
            CURLOPT_HTTPHEADER      =>  array("content-type: application/json"),    // tell server that we send JSON
            CURLOPT_RETURNTRANSFER  =>  true,                   // let curl_exec return the result
            CURLOPT_FOLLOWLOCATION  =>  true,                   // follow 'location:' headers
            CURLOPT_MAXREDIRS       =>  10,                     // max number of redirects to follow
        ));

        if (isset($this->options['proxy_usage']) && $this->options['proxy_usage']) {
            $proxyUrl = $this->options['proxy_url'] ?? null;
            curl_setopt($curl, CURLOPT_PROXY, $proxyUrl);
            $proxyPort = $this->options['proxy_port'] ?? null;
            curl_setopt($curl, CURLOPT_PROXYPORT, $proxyPort);
        }
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, (bool)$this->options['ssl_peer']); // false for https

        // execute and get result
        $result = curl_exec($curl);

        // Catch curl exception
        if (curl_error($curl)) {
            throw new Exception(curl_error($curl));
        }

        // get the content type
        $type = $this->getCurlInfo($curl, CURLINFO_CONTENT_TYPE);
        $httpcode =$this->getCurlInfo($curl, CURLINFO_HTTP_CODE);

        // close the curl handle
        curl_close($curl);

        // leap out on failure
        if ($result === false) return false;

        // if the answer from the server was json-decoded, we decode the json data
        if ($type == "application/json") return json_decode($result, true);

        // Catch errors
        if ($httpcode !== 200) {
            throw new Exception($result, $httpcode);
        }

        // expose raw output
        return $result;
    }

    /**
     *  Set a HTTP GET call to the rest API
     *
     *  @param  Method      The method to call (for example "stats/2016-03-03")
     *  @param  Params      Optional parameters
     *  @return mixed       The returned data
     */
    public function get($method, array $params = array())
    {
        // include access token in parameters
        $params["access_token"] = $this->token;

        // construct the curl
        //$curl = curl_init($url = $this->endPoint.'/'.$method.'?'.http_build_query($params));
        $url = $this->endPoint.'/'.$method.'?'.http_build_query($params);

        // url options to set
        /*curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER  =>  true,                   // let curl_exec return the result
            CURLOPT_FOLLOWLOCATION  =>  true,                   // follow 'location:' headers
            CURLOPT_MAXREDIRS       =>  10,                     // max number of redirects to follow
        ));*/

        $options = array(
            CURLOPT_RETURNTRANSFER  =>  true,                   // let curl_exec return the result
            CURLOPT_FOLLOWLOCATION  =>  true,                   // follow 'location:' headers
            CURLOPT_MAXREDIRS       =>  10,
        );

        if (isset($this->options['proxy_usage']) && $this->options['proxy_usage']) {
            $proxyUrl = $this->options['proxy_usage'] ?? null;
            //curl_setopt($curl, CURLOPT_PROXY, $proxyUrl);
            $options[CURLOPT_PROXY] = $proxyUrl;
            $proxyPort = $this->options['proxy_port'] ?? null;
            //curl_setopt($curl, CURLOPT_PROXYPORT, $proxyPort);
            $options[CURLOPT_PROXYPORT] = $proxyPort;
        }
        //curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, (bool)$this->options['ssl_peer']); // false for https
        $options[CURLOPT_SSL_VERIFYPEER] = (bool)$this->options['ssl_peer'];

        // execute and get the result
        $curl = new Varien_Http_Adapter_Curl();
        $curl->setOptions($options);
        $curl->write(Zend_Http_Client::GET, $url, '1.0');
        $result = $curl->read();
        //$result = curl_exec($curl);

        // Catch curl exception
        /*if (curl_error($curl)) {
            throw new Exception(curl_error($curl));
        }*/
        if ($curl->getError()) {
            throw new Exception($curl->getError());
        }
        // get the content type

        //$type = curl_getinfo($curl, CURLINFO_CONTENT_TYPE);
        $type = $curl->getInfo(CURLINFO_CONTENT_TYPE);//$this->getCurlInfo($curl, CURLINFO_CONTENT_TYPE);
        $httpcode = $curl->getInfo(CURLINFO_HTTP_CODE); //$this->getCurlInfo($curl, CURLINFO_HTTP_CODE);
        //$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        // close the curl handle
        //curl_close($curl);
        $curl->close();

        // leap out on failure
        if ($result === false) return false;

        // if the answer from the server was json-decoded, we decode the json data
        if ($type == "application/json") return json_decode($result, true);

        // Catch errors
        if ($httpcode !== 200) {
            throw new Exception($result, $httpcode);
        }

        // expose raw output
        return $result;
    }



    public function getCurlInfo($curl,$param=''){

        if($param != '')
           return curl_getinfo($curl, $param);
       else
           return '';
    }

}
