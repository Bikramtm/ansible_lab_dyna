<?php

/**
 * Interface Vznl_Communication_Adapter_AdapterFactoryInterface
 */
interface Vznl_Communication_Adapter_AdapterFactoryInterface
{
    public static function create(array $options) : Vznl_Communication_Adapter_AdapterInterface;

    public static function getOptions() : array;
}
