<?php

/* @var $customerInstaller Mage_Customer_Model_Resource_Setup */
$customerInstaller = new Mage_Customer_Model_Resource_Setup('core_setup');
$customerInstaller->startSetup();
$setup = Mage::getSingleton('core/resource')->getConnection('core_read');

// Fixes performance on SELECT queries
$customerInstaller->run("ALTER TABLE `dyna_communication_job` ADD INDEX `idx_dyna_communication_job_deadline_tries_status` (deadline, tries, status);");

$customerInstaller->endSetup();
