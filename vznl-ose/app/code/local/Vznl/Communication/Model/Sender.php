<?php

/**
 * Class Vznl_Communication_Model_Sender
 */
class Vznl_Communication_Model_Sender
{
    /** @var Omnius_Service_Model_Client_PortalInternalClient */
    protected $_client;
    const ENV_VHOST_CODE = 'MAGE_ENV_CODE';

    /**
     * @param Vznl_Communication_Model_Job $email
     */
    public function send(Vznl_Communication_Model_Job $email)
    {
        $sourceArray = $_SERVER;
    	$source = isset($sourceArray[self::ENV_VHOST_CODE]) && $sourceArray[self::ENV_VHOST_CODE]
            ? $sourceArray[self::ENV_VHOST_CODE]
            : Mage::helper('omnius_service')->getGeneralConfig('default_source_value');

        $this->_getClient()->dispatchEmail(new Varien_Object(array(
            'source' => $source,
            'email_template_id' => $email->getTemplateId(),
            'email_template_row_key' => $email->getEmailCode(),
            'culture_code' => $email->getLocale(),
            'to' => $email->getReceiver(),
            'name_lists' => $email->getParameters(),
            'attachments' => $email->getAttachments(),
            'reports' => $email->getReports(),
            'entity_id' => $email->getId()
        )));
    }

    /**
     * @return Omnius_Service_Model_Client_PortalInternalClient
     */
    protected function _getClient()
    {
        if ( ! $this->_client) {
            return $this->_client = Mage::helper('omnius_service')->getClient('portal_internal');
        }
        return $this->_client;
    }
}
