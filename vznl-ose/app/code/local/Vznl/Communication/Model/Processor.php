<?php

/**
 * Class Vznl_Communication_Model_Processor
 */
class Vznl_Communication_Model_Processor
{
    /** @var Vznl_Communication_Model_Sender */
    protected $sender;

    /** @var Varien_Db_Adapter_Interface */
    protected $connection;

    protected function log($message)
    {
        Mage::log($message, null, 'communication.log');
    }

    /**
     * @param Vznl_Communication_Model_Job $job
     * @return Vznl_Communication_Model_Job
     */
    public function process(Vznl_Communication_Model_Job $job)
    {
        $this->log('-----');
        $this->log('Acquired lock for: '.$job->getId());
        if ($this->lockItem($job->getId())) {

            $job->setStatus(Vznl_Communication_Model_Job::STATUS_RUNNING)
                ->addMessage('Job locked')
                ->save();
            $this->log('Job locked');

            /**
             * Update execution time
             */
            $job->setExecutedAt(strftime('%Y-%m-%d %H:%M:%S', time()));

            try {
                $adapter = $this->getAdapter($job->getType());
                $this->log('Trigger adapter: '.get_class($adapter));
                $res = $adapter->send($job);
                if (!$res) {
                    throw new Exception($adapter->getLastException());
                }

                /**
                 * Return job after updating finish timestamp and status
                 */
                $return = $job
                    ->setFinishedAt(strftime('%Y-%m-%d %H:%M:%S', time()))
                    ->setStatus(Vznl_Communication_Model_Job::STATUS_SUCCESS)
                    ->addMessage('Job successfully processed (' . get_class($adapter) . ')', Vznl_Communication_Model_Job::MESSAGE_SUCCESS)
                    ->save();
                $this->log('Job finished');

                //remove attachments if job was sent sucesfully
                foreach($job->getAttachments() as $attachment) {
                    $io = new Varien_Io_File();
                    if ($io->fileExists($attachment)
                        && !strpos($attachment, '/media/')
                        && !strpos($attachment, '/contract/')
                    ) {
                        unlink($attachment);
                        $this->log('Purged attachment: '.$attachment);
                    }
                }
                return $return;
            } catch(Exception $e) {
                $this->log('Exception received: '.$e->getMessage());
                $job
                    ->addMessage($e->getMessage(), Vznl_Communication_Model_Job::MESSAGE_ERROR)
                    ->setStatus(Vznl_Communication_Model_Job::STATUS_ERROR)
                    ->incrementTries()
                    ->save();
                return $job;
            }
        }
        $job->addMessage(sprintf(
                    'Could not acquire lock for job (ID: %s). Already running or successfully processed.',
                    $job->getId()
                ), Vznl_Communication_Model_Job::MESSAGE_ERROR)
            ->save();
        return $job;
    }

    /**
     * Grab the adapter
     * @return Vznl_Communication_Adapter_AdapterInterface
     */
    protected function getAdapter($type)
    {
        $logger = $this->getLogWriter($type)->getLogger();
        switch($type) {
            case Vznl_Communication_Model_Job::TYPE_EMAIL:
                $adapter = Mage::getStoreConfig('vodafone_service/communication_email/adapter');
                break;
            case Vznl_Communication_Model_Job::TYPE_SMS:
                $adapter = Mage::getStoreConfig('vodafone_service/communication_sms/adapter');
                break;
            default:
                throw new Exception('No suitable adapter found for type: '.$type);
        }

        // Check if class exists
        if(!class_exists($adapter)) {
            throw new Exception('No suitable adapter found with name: '.$adapter);
        }

        return $adapter::create(['logger'=>$logger]);
    }

    /**
     * Get the log writer
     * @param $type
     * @return Aleron75_Magemonolog_Model_Logwriter
     */
    protected function getLogWriter($type)
    {
        return new Aleron75_Magemonolog_Model_Logwriter('services/communication/'.$type.'/'.\Ramsey\Uuid\Uuid::uuid4().'.log');
    }

    /**
     * @param $jobId
     * @return bool
     */
    protected function lockItem($jobId)
    {
        $tableName = Mage::getSingleton('core/resource')->getTableName('communication/job');
        $bind  = array('status' => Vznl_Communication_Model_Job::STATUS_RUNNING);
        $where = array('id = (?)' => $jobId, 'status NOT IN (?)' => array(Vznl_Communication_Model_Job::STATUS_RUNNING, Vznl_Communication_Model_Job::STATUS_SUCCESS));
        $rowsCount = $this->getConnection()->update($tableName, $bind, $where);
        return $rowsCount > 0;
    }

    /**
     * @return Vznl_Communication_Model_Sender
     */
    protected function getSender()
    {
        if (!$this->sender) {
            return $this->sender = Mage::getSingleton('communication/sender');
        }
        return $this->sender;
    }

    /**
     * @return Varien_Db_Adapter_Interface
     */
    protected function getConnection()
    {
        if (!$this->connection) {
            return $this->connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        }
        return $this->connection;
    }
}
