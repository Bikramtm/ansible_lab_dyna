<?php

class Vznl_Communication_Model_Observer extends Varien_Object {

    /**
    * Expects observer with data:
    * 'to', 'subject', 'template',
    * 'html', 'email_body'
    *
    * @param $observer
    */
    public function log($observer) {

        $event = $observer->getEvent();
        Mage::helper('communication/smtp')->logEmailSent(
        $event->getTo(),
        $event->getTemplate(),
        $event->getSubject(),
        $event->getEmailBody(),
        $event->getHtml());
    }

    /**
    * Expects observer with:
    * 'mail', the mail about to be sent
    * 'email', the email object initiating the send
    * 'transport', an initially empty transport Object, will be used if set.
    *
    * @param $observer
    */
    public function beforeSend($observer) {
        $observer->getEvent()->getTransport()->setTransport(Mage::helper('communication/smtp')->getTransport());
    }

    /**
    * Expects observer with:
    * 'mail', the mail about to be sent
    * 'template', the template being used
    * 'variables', the variables used in the template
    * 'transport', an initially empty transport Object, will be used if set.
    *
    * @param $observer
    */
    public function beforeSendTemplate($observer) {
        Mage::helper('communication/smtp')->log($observer->getEvent()->getMail());
        $observer->getEvent()->getTransport()->setTransport(Mage::helper('communication/smtp')->getTransport());
    }

}