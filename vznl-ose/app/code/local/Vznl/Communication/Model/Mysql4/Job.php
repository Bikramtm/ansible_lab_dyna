<?php

/**
 * Class Vznl_Communication_Model_Mysql4_Job
 */
class Vznl_Communication_Model_Mysql4_Job extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init('communication/job', 'id');
    }
}
