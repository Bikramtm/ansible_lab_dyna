<?php

/**
 * Class Vznl_Communication_Model_Mysql4_Job_Collection
 */
class Vznl_Communication_Model_Mysql4_Job_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        $this->_init('communication/job');
    }
}
