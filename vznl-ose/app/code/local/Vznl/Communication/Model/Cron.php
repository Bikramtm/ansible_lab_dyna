<?php

/**
 * Class Vznl_Communication_Model_Cron
 */
class Vznl_Communication_Model_Cron
{
    /** @var Vznl_Communication_Model_BatchProcessor */
    protected $_batchProcessor;

    /** @var Vznl_Communication_Model_Processor */
    protected $_processor;

    /** @var Vznl_Communication_Helper_Email */
    protected $_helper;
    protected $jobIds;

    /**
     * Retrieves all jobs that follow the limits
     * set on the backend and tries to process them
     * @param array|null $jobIds If given only the jobs with the given id's will be processed
     */
    public function processJobs($jobIds = null)
    {
        $jobs = $this->_getJobs($jobIds);
        $this->_getBatchProcessor()->process($jobs, $this->_getProcessor());
    }

    /**
     * @param array|null $jobIds If given only the jobs with the given id's will be returned
     * @return Vznl_Communication_Model_Mysql4_Job_Collection The collection of jobs
     */
    protected function _getJobs($jobIds = null)
    {
        $this->jobIds = $jobIds;
        /** @var Vznl_Communication_Model_Mysql4_Job_Collection $emails */
        $jobs = Mage::getResourceModel('communication/job_collection');
        //if (is_array($jobIds) && count(array_filter($jobIds))) {
       //     $emails->addFieldToFilter('id', array('in' => $jobIds));
        //}
        $jobs->getSelect()
            ->where(
                sprintf(
                    '(deadline <= TIMESTAMP("%s")) AND (tries < %s) AND (status IN ("%s", "%s"))',
                    now(),
                    $this->_getHelper()->getTriesLimit(),
                    Vznl_Communication_Model_Job::STATUS_PENDING,
                    Vznl_Communication_Model_Job::STATUS_ERROR
                )
            )
        ;
        return $jobs;
    }

    /**
     * @return Vznl_Communication_Model_BatchProcessor
     */
    protected function _getBatchProcessor()
    {
        if ( ! $this->_batchProcessor) {
            return $this->_batchProcessor = Mage::getSingleton('communication/batchProcessor');
        }
        return $this->_batchProcessor;
    }

    /**
     * @return Vznl_Communication_Model_Processor
     */
    protected function _getProcessor()
    {
        if ( ! $this->_processor) {
            return $this->_processor = Mage::getSingleton('communication/processor');
        }
        return $this->_processor;
    }

    /**
     * @return Vznl_Communication_Helper_Job
     */
    protected function _getHelper()
    {
        if ( ! $this->_helper) {
            return $this->_helper = Mage::helper('communication/job');
        }
        return $this->_helper;
    }
}
