<?php

/**
 * Class Vznl_Communication_Block_Adminhtml_Email
 */
class Vznl_Communication_Block_Adminhtml_Email extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_email';
        $this->_blockGroup = 'communication';
        $this->_headerText = Mage::helper('communication/job')->__('Communication Manager');
        $this->_addButtonLabel = Mage::helper('communication/job')->__('Add New Item');

        parent::__construct();
    }
}
